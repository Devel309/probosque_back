package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.EnviarConsultaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface EnviarConsultaService {
    ResultClassEntity EnviarConsulta(EnviarConsultaEntity obj);
}
