package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.OrdenProduccionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ProdTerminadoEntity;

public interface OrdenProduccionService {
    ResultClassEntity registrarOrdenProduccionProductoTerminado(List<ProdTerminadoEntity> items) throws Exception;
    ResultClassEntity listarOrdenProduccionProductoTerminado(ProdTerminadoEntity param)throws Exception;
    
    ResultClassEntity registrarOrdenProduccion(List<OrdenProduccionEntity> list)throws Exception;
    ResultClassEntity listarOrdenProduccion(OrdenProduccionEntity param)throws Exception;
}
