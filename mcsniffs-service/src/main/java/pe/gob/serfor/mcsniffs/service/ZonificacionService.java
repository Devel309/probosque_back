package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Parametro.ZonificacionDTO;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ZonificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ZonaEntity;

import java.util.List;

public interface ZonificacionService {
    
    ResultClassEntity ListarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception;
    ResultClassEntity listarZona(ZonaEntity item) throws Exception;
    ResultClassEntity registrarZona( List<ZonaEntity> items) throws Exception;
    ResultClassEntity eliminarZona(ZonaEntity item) throws Exception;
    ResultClassEntity RegistrarZonificacion( List<ZonificacionDTO> zonificacionDTOList) throws Exception;
//    ResultClassEntity ActualizarZonificacion( List<ZonificacionDTO> zonificacionDTOList) throws Exception;
    ResultClassEntity EliminarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception;

}
