package pe.gob.serfor.mcsniffs.service;


import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.*;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

import java.util.List;

public interface SolicitudConcesionEvaluacionDetalleService {
    ResultClassEntity registrarSolicitudConcesionEvaluacionDetalle(List<SolicitudConcesionEvaluacionDetalleDto> lsobj) throws Exception;
    ResultClassEntity actualizarSolicitudConcesionEvaluacionDetalle(List<SolicitudConcesionEvaluacionDetalleDto> obj) throws Exception;

}
