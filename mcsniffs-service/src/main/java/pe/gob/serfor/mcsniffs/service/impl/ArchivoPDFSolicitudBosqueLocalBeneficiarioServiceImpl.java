package pe.gob.serfor.mcsniffs.service.impl;


import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalBeneficiarioEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DecargarPDFSolBosqueLocalBeneficiario.DescargarPDFSolBeneficiario;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolicitudPlantacionForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalBeneficarioRepository;
import pe.gob.serfor.mcsniffs.service.ArchivoPDFSolicitudBosqueLocalBeneficiarioService;

@Service("ArchivoPDFSolicitudBosqueLocalBeneficiarioService")
public class ArchivoPDFSolicitudBosqueLocalBeneficiarioServiceImpl implements ArchivoPDFSolicitudBosqueLocalBeneficiarioService {

    @Autowired
    private SolicitudBosqueLocalBeneficarioRepository solicitudBosqueLocalBeneficarioRepository;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 12f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    static Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font subTituloTabla= new Font(Font.HELVETICA, 9f, Font.COURIER);

    Font normal= new Font(Font.HELVETICA, 10f);

    @Override
    public ByteArrayResource ArchivoPDFSolicitudBosqueLocalBeneficiario(DescargarPDFSolBeneficiario obj) throws Exception {

        File archivo = File.createTempFile("ArchivoPDFSolicitudBosqueLocalBeneficiario", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPDFSolBosLoBeneficiario(os, obj);
        os.flush();
        os.close();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);
    }


    public  void createPDFSolBosLoBeneficiario(FileOutputStream os,DescargarPDFSolBeneficiario obj) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        Rectangle two = new Rectangle(842.0F,595.0F);
        document.setPageSize(two);
        document.setMargins(60, 60, 40, 40);

        try {

            PdfWriter writer = PdfWriter.getInstance(document, os);
            document.open();
            Paragraph preface = new Paragraph();

            // TITULO & SUB TITULO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("Anexo N° 01", titulo));
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("LISTADO DE BENEFICIARIOS", subTitulo));
            addEmptyLine(preface, 1);
            preface.setAlignment(Element.ALIGN_CENTER);
            document.add(preface);

            //1. TABLA BENEFICIARIOS
            PdfPTable tableInfoBeneficiarios = obtInfoBeneficiarios(obj);
            document.add(tableInfoBeneficiarios);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }
    
    public PdfPTable obtInfoBeneficiarios(DescargarPDFSolBeneficiario obj) throws Exception{
        //---DATA
        SolicitudBosqueLocalBeneficiarioEntity param = new SolicitudBosqueLocalBeneficiarioEntity();
        param.setIdSolBosqueLocal(obj.getIdSolBosqueLocal());
        param.setPageNum(1);
        param.setPageSize(1000);
        ResultClassEntity result = solicitudBosqueLocalBeneficarioRepository.listarSolicitudBosqueLocalBeneficiario(param);
        List<SolicitudBosqueLocalBeneficiarioEntity> lista = new ArrayList<>();

        if(result!=null && result.getData()!=null) lista = (List<SolicitudBosqueLocalBeneficiarioEntity>) result.getData(); 


        //--TABLA
        PdfPTable table = new PdfPTable(9);
        float[] medidaCeldas = {0.3f,1.5f,0.7f,1.5f,0.9f,1.4f,1f,0.9f,0.6f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(100);

        setearBodyStatic("N°", 1, table);
        setearBodyStatic("Nombres y apellidos", 1, table);
        setearBodyStatic("DNI", 1, table);
        setearBodyStatic("Residencia actual", 1, table);
        setearBodyStatic("Condición", 1, table);
        setearBodyStatic("Teléfono/e mail", 1, table);
        setearBodyStatic("Carga Familiar", 1, table);
        setearBodyStatic("Firma", 1, table);
        setearBodyStatic("Huella digital", 1, table);

        Integer index=1;
        for (SolicitudBosqueLocalBeneficiarioEntity e : lista) {
            if(e.getFlagListaFinal() && (e.getTieneOposicion()==null || e.getTieneOposicion()==false)){
                setearBodyData(index.toString(), 1, table);
                setearBodyData(e.getNombreCompleto()==null?"":e.getNombreCompleto(), 1, table);
                setearBodyData(e.getNumeroDocumento()==null?"":e.getNumeroDocumento(),1, table);
                setearBodyData(e.getResidencia()==null?"":e.getResidencia(), 1, table);
                setearBodyData(e.getCondicion()==null?"":e.getCondicion(),1, table);
                setearBodyData((e.getTelefono()==null?"":e.getTelefono())+" / "+(e.getEmail()==null?"":e.getEmail()) ,1, table);
                setearBodyData(e.getCargaFamiliar()==null?"":e.getCargaFamiliar(), 1, table);
                setearBodyData(" ", 1, table);
                setearBodyData(" ", 1, table);
                index++;
            }
        }

        return table;
    }
   

    //-------------------------------------------------------------------------
    private static void setearCellcabezera(String dato, Integer colum, PdfPTable table){

        Font title = new Font(Font.HELVETICA, 12,Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato,title));
        Color colorHeader = new Color(189 , 214 , 238);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setColspan(colum);
        c.setBackgroundColor(colorHeader);
        c.setPadding(5);
        table.addCell(c);
        c.setMinimumHeight(20);
        table.setHeaderRows(1);
    }
    private static void setearSubTitle(String dato, Integer colum, PdfPTable table){
        Font subTitle = new Font(Font.HELVETICA, 9,Font.BOLD);
        Color colorSubTitle = new Color(242 , 242 , 242);
        PdfPCell c = new PdfPCell(new Phrase(dato,subTitle));
        c.setColspan(colum);
        c.setBackgroundColor(colorSubTitle);
        c.setPadding(5);
        table.addCell(c);
        c.setMinimumHeight(20);
        table.setHeaderRows(1);
    }
    private static void setearBodyStatic(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }   
    private static void setearBodyData(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        c.setColspan(colum);
        c.setPadding(5);
        c.setMinimumHeight(30);
        table.addCell(c);
    }
    private static void setearBodyDataPreface(Paragraph preface, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(preface);
        c.setColspan(colum);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void setearRowsColspanStatic(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        c.setMinimumHeight(20);
        table.addCell(c);
    }
    private static void setearRowsColspanData(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        c.setMinimumHeight(20);
        table.addCell(c);
    }
    private static void setearBodyStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    } 
    private static void setearDataStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        c.setColspan(colum);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }  
    private static void setearBodyDataSborder(String dato,Integer row, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        c.setRowspan(row);
        c.setColspan(colum);
        c.setPadding(5);
        c.setBorder(0);
        c.setMinimumHeight(20);
        table.addCell(c);
    }

    private Integer sumaTotalArbolesExistentes (List<SolicitudPlantacionForestalDetalleEntity> listaEsp){
        Integer suma = 0;
        for (SolicitudPlantacionForestalDetalleEntity element : listaEsp) {
            suma += element.getTotalArbolesExistentes();
        }
        return suma;
    }

}
