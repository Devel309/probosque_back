package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;
import pe.gob.serfor.mcsniffs.repository.ParametroRepository;
import pe.gob.serfor.mcsniffs.service.ParametroService;

import java.util.List;

@Service("ParametroValorService")
public class ParametroServiceImpl implements ParametroService {
    private static final Logger log = LogManager.getLogger(ParametroServiceImpl.class);

    @Autowired
    private ParametroRepository parametroValorRepository;

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para consultar Parámetros por código}
     * @param: Objeto ParametroValorEntity
     *
     * @return: List<ParametroValorEntity>
     */
    @Override
    public List<ParametroValorEntity> ListarPorCodigoParametroValor(ParametroValorEntity param) throws Exception {
        return parametroValorRepository.ListarPorCodigoParametroValor(param);
    }
}
