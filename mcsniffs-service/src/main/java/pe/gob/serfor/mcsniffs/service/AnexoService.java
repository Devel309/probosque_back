package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.AdjuntoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.EliminarDocAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.GeneralAnexoEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaDto;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UsuarioSolicitanteEntity;
import pe.gob.serfor.mcsniffs.entity.ValidarDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ObtenerDocumentoDto;

public interface AnexoService {
    ResultClassEntity<UsuarioSolicitanteEntity> obtenerUsuarioSolicitante(AnexoRequestEntity filtro);
    ResultArchivoEntity generarAnexo1(AnexoRequestEntity filtro);
    ResultArchivoEntity generarAnexo2(AnexoRequestEntity filtro);
    ResultArchivoEntity generarAnexo3(AnexoRequestEntity filtro);
    ResultArchivoEntity descargarAnexo4();
    ResultEntity<GeneralAnexoEntity>ObtenerAnexo(AnexoRequestEntity filtro);
    ResultClassEntity AdjuntarAnexo(MultipartFile file, Integer IdProcesoPostulacion, Integer IdUsuarioAdjunta, String CodigoAnexo,String NombreArchivo,Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto);
    ResultEntity<DocumentoAdjuntoEntity> ObtenerAdjuntos(AdjuntoRequestEntity filtro);
    ResultClassEntity ValidarAnexo(ValidarDocumentoEntity anexo);
    ResultClassEntity eliminarDocumentoAdjunto(EliminarDocAdjuntoEntity obj);
    ResultClassEntity insertarEstatusAnexo(AnexoRequestEntity filtro);
    ResultClassEntity actualizarEstatusAnexo(AnexoRequestEntity filtro);
    ResultEntity<ValidarDocumentoEntity> obtenerEstatusAnexo(AnexoRequestEntity filtro);
    ResultEntity<ValidarDocumentoEntity> obtenerDetalleObservacion(AnexoRequestEntity filtro);
    ResultClassEntity listarDocumentos(AdjuntoRequestEntity filtro);
    ResultClassEntity guardarObservacion(List<DocumentoAdjuntoEntity> params);
    ResultClassEntity adjuntarListAnexo(List<DocumentoAdjuntoDto> lista);
    ResultArchivoEntity generarInformeGeneral(ObtenerDocumentoDto dto, String token) throws Exception;
    ResultClassEntity<PersonaDto> obtenerUsuario(AnexoRequestEntity obj);
}
