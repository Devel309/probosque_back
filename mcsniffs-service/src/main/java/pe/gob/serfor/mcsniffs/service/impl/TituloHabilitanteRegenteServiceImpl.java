package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHRegenteDto;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.TituloHabilitanteRegenteRepository;
import pe.gob.serfor.mcsniffs.repository.TituloHabilitanteRepository;
import pe.gob.serfor.mcsniffs.service.MonitoreoService;
import pe.gob.serfor.mcsniffs.service.TituloHabilitanteRegenteService;
import pe.gob.serfor.mcsniffs.service.TituloHabilitanteService;

import java.util.List;

/**
 * @autor: Rafael Azaña [12-02-2022]
 * @modificado:
 *
 */
@Service("TituloHabilitanteRegenteService")
public class TituloHabilitanteRegenteServiceImpl implements TituloHabilitanteRegenteService {


    private static final Logger log = LogManager.getLogger(ProteccionBosqueServiceImpl.class);

    @Autowired
    private TituloHabilitanteRegenteRepository tituloHabilitanteRegenteRepository;

    @Override
    public ResultClassEntity listarInformacionTHRegente(InformacionTHRegenteDto param) throws Exception {
        return  tituloHabilitanteRegenteRepository.listarInformacionTHRegente(param);
    }

}

