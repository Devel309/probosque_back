package pe.gob.serfor.mcsniffs.service.constant;

/**
 * 
 * @author Alexis Salgado
 */
public final class DemaType {

	private DemaType() {
	}

	// public static final String COMUNIDAD_NOMBRE = "#COMUNIDAD_NOMBRE#";
	// public static final String JEFE_NOMBRE = "#JEFE_NOMBRE#";
	// public static final String JEFE_DNI = "#JEFE_DNI#";
	// public static final String FEDERACION = "#FEDERACION#";
	// public static final String DEPARTAMENTO = "#DEPARTAMENTO#";
	// public static final String PROVINCIA = "#PROVINCIA#";
	// public static final String DISTRITO = "#DISTRITO#";
	// public static final String CUENCA = "#CUENCA#";
	// public static final String SUB_CUENCA = "#SUB_CUENCA#";
	// public static final String NRO_ANEXOS = "#NRO_ANEXOS#";
	// public static final String NRO_RESOLUCION = "#NRO_RESOLUCION#";
	// public static final String NRO_TITULO = "#NRO_TITULO#";
	// public static final String NRO_FAMILIAS = "#NRO_FAMILIAS#";
	// public static final String NRO_RUC = "#NRO_RUC#";
	// public static final String FECHA_INICIO = "#FECHA_INICIO#";
	// public static final String FECHA_FIN = "#FECHA_FIN#";
	// public static final String ELABORADOR_NOMBRE = "#ELABORADOR_NOMBRE#";
	// public static final String ELABORADOR_DNI = "#ELABORADOR_DNI#";
	// public static final String NRO_FAMILIAS_DEMA = "#NRO_FAMILIAS_DEMA#";
	// public static final String MADERABLE_TOTAL = "#MADERABLE_TOTAL#";
	// public static final String MADERABLE_SEMILLERO = "#MADERABLE_SEMILLERO#";
	// public static final String MADERABLE_SUPERFICIE = "#MADERABLE_SUPERFICIE#";
	// public static final String MADERABLE_VOLUMEN = "#MADERABLE_VOLUMEN#";
	// public static final String NO_MADERABLE_TOTAL = "#NO_MADERABLE_TOTAL#";
	// public static final String NO_MADERABLE_SEMILLEROS = "#NO_MADERABLE_SEMILLEROS#";
	// public static final String NO_MADERABLE_SUPERFICIE = "#NO_MADERABLE_SUPERFICIE#";
	// public static final String NO_MADERABLE_VOLUMEN = "#NO_MADERABLE_VOLUMEN#";
	// public static final String TOTAL_INGRESOS = "#TOTAL_INGRESOS#";
	// public static final String TOTAL_COSTOS = "#TOTAL_COSTOS#";
	// public static final String TOTAL_UTILIDADES = "#TOTAL_UTILIDADES#";

	// public static final String COMUNIDAD_NOMBRE = "[COMUNIDAD_NOMBRE]";
	// public static final String JEFE_NOMBRE = "[JEFE_NOMBRE]";
	// public static final String JEFE_DNI = "[JEFE_DNI]";
	// public static final String FEDERACION = "[FEDERACION]";
	// public static final String DEPARTAMENTO = "[DEPARTAMENTO]";
	// public static final String PROVINCIA = "[PROVINCIA]";
	// public static final String DISTRITO = "[DISTRITO]";
	// public static final String CUENCA = "[CUENCA]";
	// public static final String SUB_CUENCA = "[SUB_CUENCA]";
	// public static final String NRO_ANEXOS = "[NRO_ANEXOS]";
	// public static final String NRO_RESOLUCION = "[NRO_RESOLUCION]";
	// public static final String NRO_TITULO = "[NRO_TITULO]";
	// public static final String NRO_FAMILIAS = "[NRO_FAMILIAS]";
	// public static final String NRO_RUC = "[NRO_RUC]";
	// public static final String FECHA_INICIO = "[FECHA_INICIO]";
	// public static final String FECHA_FIN = "[FECHA_FIN]";
	// public static final String ELABORADOR_NOMBRE = "[ELABORADOR_NOMBRE]";
	// public static final String ELABORADOR_DNI = "[ELABORADOR_DNI]";
	// public static final String NRO_FAMILIAS_DEMA = "[NRO_FAMILIAS_DEMA]";
	// public static final String MADERABLE_TOTAL = "[MADERABLE_TOTAL]";
	// public static final String MADERABLE_SEMILLERO = "[MADERABLE_SEMILLERO]";
	// public static final String MADERABLE_SUPERFICIE = "[MADERABLE_SUPERFICIE]";
	// public static final String MADERABLE_VOLUMEN = "[MADERABLE_VOLUMEN]";
	// public static final String NO_MADERABLE_TOTAL = "[NO_MADERABLE_TOTAL]";
	// public static final String NO_MADERABLE_SEMILLEROS = "[NO_MADERABLE_SEMILLEROS]";
	// public static final String NO_MADERABLE_SUPERFICIE = "[NO_MADERABLE_SUPERFICIE]";
	// public static final String NO_MADERABLE_VOLUMEN = "[NO_MADERABLE_VOLUMEN]";
	// public static final String TOTAL_INGRESOS = "[TOTAL_INGRESOS]";
	// public static final String TOTAL_COSTOS = "[TOTAL_COSTOS]";
	// public static final String TOTAL_UTILIDADES = "[TOTAL_UTILIDADES]";

	public static final String COMUNIDAD_NOMBRE = "CAMPO0001";
	public static final String JEFE_NOMBRE = "CAMPO0002";
	public static final String JEFE_DNI = "CAMPO0003";
	public static final String FEDERACION = "CAMPO0004";
	public static final String DEPARTAMENTO = "CAMPO0005";
	public static final String PROVINCIA = "CAMPO0006";
	public static final String DISTRITO = "CAMPO0007";
	public static final String CUENCA = "CAMPO0008";
	public static final String SUB_CUENCA = "CAMPO0009";
	public static final String NRO_ANEXOS = "CAMPO0010";
	public static final String NRO_RESOLUCION = "CAMPO0011";
	public static final String NRO_TITULO = "CAMPO0012";
	public static final String NRO_FAMILIAS = "CAMPO0013";
	public static final String NRO_RUC = "CAMPO0014";
	public static final String FECHA_INICIO = "CAMPO0015";
	public static final String FECHA_FIN = "CAMPO0016";
	public static final String ELABORADOR_NOMBRE = "CAMPO0017";
	public static final String ELABORADOR_DNI = "CAMPO0018";
	public static final String NRO_FAMILIAS_DEMA = "CAMPO0019";
	public static final String MADERABLE_TOTAL = "CAMPO0020";
	public static final String MADERABLE_SEMILLERO = "CAMPO0021";
	public static final String MADERABLE_SUPERFICIE = "CAMPO0022";
	public static final String MADERABLE_VOLUMEN = "CAMPO0023";
	public static final String NO_MADERABLE_TOTAL = "CAMPO0024";
	public static final String NO_MADERABLE_SEMILLEROS = "CAMPO0025";
	public static final String NO_MADERABLE_SUPERFICIE = "CAMPO0026";
	public static final String NO_MADERABLE_VOLUMEN = "CAMPO0027";
	public static final String TOTAL_INGRESOS = "CAMPO0028";
	public static final String TOTAL_COSTOS = "CAMPO0029";
	public static final String TOTAL_UTILIDADES = "CAMPO0030";


}
