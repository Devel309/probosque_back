package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;
import pe.gob.serfor.mcsniffs.repository.AreaSistemaPlantacionRepository;
import pe.gob.serfor.mcsniffs.service.AreaSistemaPlantacionServiceService;

@Service
public class AreaSistemaPlantacionServiceImpl implements AreaSistemaPlantacionServiceService {

    @Autowired
    private AreaSistemaPlantacionRepository areaSistemaPlantacionRepository;
    
    @Override
    public ResultClassEntity registrarAreaSistemaPlantacion(List<AreaSistemaPlantacionDto> obj) {
        //return areaSistemaPlantacionRepository.registrarAreaSistemaPlantacion(obj);

        ResultClassEntity result = new ResultClassEntity();

        for (AreaSistemaPlantacionDto element : obj) {
            result = areaSistemaPlantacionRepository.registrarAreaSistemaPlantacion(element);
        }
         return result;
    }

    @Override
    public ResultClassEntity listarAreaSistemaPlantacion(AreaSistemaPlantacionDto obj) {
        return areaSistemaPlantacionRepository.listarAreaSistemaPlantacion(obj);
    }

    @Override
    public ResultClassEntity eliminarAreaSistemaPlantacion(AreaSistemaPlantacionDto obj) throws Exception {
        return areaSistemaPlantacionRepository.eliminarAreaSistemaPlantacion(obj);
    }

    @Override
    public ResultClassEntity ListarEspecieForestalSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto obj) throws Exception {
        return areaSistemaPlantacionRepository.ListarEspecieForestalSistemaPlantacionEspecie(obj);
    }

    @Override
    public ResultClassEntity eliminarSistemaPlantacionEspecieLista(List<EspecieForestalSistemaPlantacionEspecieDto> obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        for (EspecieForestalSistemaPlantacionEspecieDto element : obj) {
            result = areaSistemaPlantacionRepository.eliminarSistemaPlantacionEspecie(element);
        }
        return result;
    }

    @Override
    public ResultClassEntity registrarSistemaPlantacionEspecie(List<EspecieForestalSistemaPlantacionEspecieDto> obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        for (EspecieForestalSistemaPlantacionEspecieDto element : obj) {
            result = areaSistemaPlantacionRepository.registrarSistemaPlantacionEspecie(element);
        }
        return result;
    }

}
