package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.service.EvaluacionDetalleService;



@Service("EvaluacionDetalleService")
public class EvaluacionDetalleServiceImpl implements EvaluacionDetalleService {

    private static final Logger log = LogManager.getLogger(EvaluacionServiceImpl.class);


    
    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;
 

    @Override
    public ResultClassEntity<List<EvaluacionDetalleDto>> listarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception {        
      
        List<EvaluacionDetalleDto> lista = evaluacionDetalleRepository.listarEvaluacionDetalle(dto);
        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");

        return  result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarEvaluacionDetalle(List<EvaluacionDetalleDto> dto) throws Exception {

        ResultClassEntity result = null;
        for (EvaluacionDetalleDto element : dto) {
            result = evaluacionDetalleRepository.registrarEvaluacionDetalle(element);
        }

        return  result;
    }


    @Override
    public ResultClassEntity eliminarEvaluacionDetalle(List<EvaluacionDetalleDto> dto) throws Exception{
        ResultClassEntity result = null;
        for (EvaluacionDetalleDto element : dto) {
            result = evaluacionDetalleRepository.eliminarEvaluacionDetalle(element);
        }
        return  result;
    }
    
}
