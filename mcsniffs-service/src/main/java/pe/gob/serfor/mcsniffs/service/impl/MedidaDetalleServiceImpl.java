package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDetalleDto;
import pe.gob.serfor.mcsniffs.repository.MedidaDetalleRepository;
import pe.gob.serfor.mcsniffs.service.MedidaDetalleService;



@Service("MedidaDetalleService")
public class MedidaDetalleServiceImpl implements MedidaDetalleService {

    private static final Logger log = LogManager.getLogger(MedidaDetalleServiceImpl.class);


    
    @Autowired
    private MedidaDetalleRepository medidadetalleRepository;

 
    @Override
    public ResultClassEntity<List<MedidaDetalleDto>> listarMedidaDetalle(MedidaDetalleDto dto) throws Exception {   
        
        List<MedidaDetalleDto> lista = medidadetalleRepository.listarMedidaDetalle(dto);       

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarMedidaDetalle(List<MedidaDetalleDto> dto) throws Exception {

        ResultClassEntity result = null;
        for (MedidaDetalleDto medidaDetalleDto : dto) {
            result = medidadetalleRepository.registrarMedidaDetalle(medidaDetalleDto);
        }

        return  result;
    }

    @Override
    public ResultClassEntity eliminarMedidaDetalle(List<MedidaDetalleDto> dto) throws Exception{
        ResultClassEntity result = null;
        for (MedidaDetalleDto medidaDetalleDto : dto) {
            result = medidadetalleRepository.eliminarMedidaDetalle(medidaDetalleDto);
        }
        return  result;
    }
 
 
}
