package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ProduccionRecursoForestalService {
    ResultClassEntity RegistrarProduccionRecursoForestal(List<ProduccionRecursoForestalDto> list)throws Exception;
    ResultClassEntity ListarProduccionRecursoForestal(ProduccionRecursoForestalEntity param)throws Exception;
    ResultClassEntity EliminarProduccionRecursoForestal(ProduccionRecursoForestalEntity param)throws Exception;
    ResultClassEntity EliminarProduccionRecursoForestalEspecie(ProduccionRecursoForestalEspecieEntity param) throws Exception;
}
