package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDto;
import pe.gob.serfor.mcsniffs.repository.MedidaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.MedidaRepository;
import pe.gob.serfor.mcsniffs.service.MedidaService;



@Service("MedidaService")
public class MedidaServiceImpl implements MedidaService {

    private static final Logger log = LogManager.getLogger(MedidaServiceImpl.class);
    
    @Autowired
    private MedidaRepository medidaRepository;

    @Autowired
    private MedidaDetalleRepository medidaDetalleRepository;

 
    @Override
    public ResultClassEntity<List<MedidaDto>> listarMedida(MedidaDto dto) throws Exception {

        List<MedidaDto> lista = medidaRepository.listarMedida(dto);
        MedidaDetalleDto m = null;
        for (MedidaDto element : lista) {
            m = new MedidaDetalleDto(); 
            m.setIdMedida(element.getIdMedida());            
            element.setListaMedidaDetalle(medidaDetalleRepository.listarMedidaDetalle(m));
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarMedida(MedidaDto dto) throws Exception {

        ResultClassEntity result = null;
        result = medidaRepository.registrarMedida(dto);

        if (dto.getListaMedidaDetalle()!=null){
            for (MedidaDetalleDto element : dto.getListaMedidaDetalle()) {            
                element.setIdMedida(dto.getIdMedida());
                element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
                result = medidaDetalleRepository.registrarMedidaDetalle(element);
            }
        }
        return  result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity eliminarMedida(MedidaDto dto) throws Exception{
        ResultClassEntity result = null;
        result = medidaRepository.eliminarMedida(dto);

        if (dto.getListaMedidaDetalle()!=null){
            for (MedidaDetalleDto element : dto.getListaMedidaDetalle()) {            
                element.setIdMedida(dto.getIdMedida());
                element.setIdUsuarioElimina(dto.getIdUsuarioElimina());
                result = medidaDetalleRepository.eliminarMedidaDetalle(element);
            }
        }
        return  result;
    }
 
 
}
