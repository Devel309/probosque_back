package pe.gob.serfor.mcsniffs.service.constant;

/**
 * 
 * @author Rafael Azaña
 */
public final class PgmfaType {

	private PgmfaType() {
	}

	public static final String COMUNIDAD_NOMBRE = "1comunidad";
	public static final String JEFE_NOMBRE = "1nombreJefeComunidad";
	public static final String JEFE_DNI = "1numPartida";
	public static final String JEFE_RUC = "1numRuc";
	public static final String FEDERACION = "1FederacionOrganiza";
	public static final String REGENTE = "1Regente";
	public static final String NRO_CERTIFICADO = "1Certificado";
	public static final String NRO_INSCRIPCION = "1numInscripcion";
	public static final String FECHA_PRESTACION = "1fechaPresentacion";
	public static final String FECHA_INICIO = "1FechaInicio";
	public static final String FECHA_FIN = "1FechaFinaliza";
	public static final String OBJETIVOS_ESPECIFICOS = "1ObjetivosEspecificos";
	public static final String POTENCIAL_MADERABLE = "1potencialMaderable";
	public static final String DEPARTAMENTO = "1Depar";
	public static final String PROVINCIA = "1Provi";
	public static final String DISTRITO = "1Distrito";
	public static final String NRO_SECTORES = "1nroSectores";
	public static final String SUPERFICIE_COMUNIDAD = "1AreaTitu";
	public static final String AREA_MANEJO = "1AreaSig";

}
