package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaDetalleRepository;
import pe.gob.serfor.mcsniffs.service.ProcesoOfertaDetalleService;

@Service
public class ProcesoOfertaDetalleServiceImpl implements ProcesoOfertaDetalleService{
    /**
     * @autor: Abner Valdez [28-06-2021]
     * @modificado:
     * @descripción: {Servicio de Proceso oferta detalle en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    ProcesoOfertaDetalleRepository repository;

    /**
     * @autor: Abner Valdez [28-06-2021]
     * @modificado:
     * @descripción: {Guarda e proceso en oferta Detalle}
     * @param:consulta
     */
    @Override
    public ResultClassEntity guardarProcesoOfertaDetalle(ProcesoOfertaDetalleEntity obj) {
        return repository.guardarProcesoOfertaDetalle(obj);
    }
}
