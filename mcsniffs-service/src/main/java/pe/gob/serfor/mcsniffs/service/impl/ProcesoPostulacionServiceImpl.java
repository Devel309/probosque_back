package pe.gob.serfor.mcsniffs.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.ResolucionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ProcesoPostulacionService;

@Service
public class ProcesoPostulacionServiceImpl implements ProcesoPostulacionService {
    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Repository de los procesos de postulacion}
     *
     */
    @Autowired
    ProcesoPostulacionRepository repository;

    @Autowired
    ResolucionRepository resolucionRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Autowired
    private EmailService email;
    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Guardar proceso postulacion}
     * @param:ProcesoPostulacionResquestEntity
     */
    @Override
    public ResultClassEntity guardaProcesoPostulacion(ProcesoPostulacionResquestEntity proceso) throws Exception{




        ResultClassEntity resultClassEntity = repository.guardaProcesoPostulacion(proceso);


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_PROCESO_POSTULACION,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MAD_PROCESO_POSTULACION + resultClassEntity.getCodigo(),
                proceso.getProcesopostulacion().getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);


        return resultClassEntity;
    }

    /**
     * @autor: JaquelineDB [24-06-2021]
     * @modificado:
     * @descripción: {Listar proceso de porstulacion por usuario}
     * @param:ListarProcesoPosEntity
     * @throws Exception
     */
    @Override
    public ResultEntity<ProcesoPostulacionEntity> listarProcesoPostulacion(ListarProcesoPosEntity filtro) throws Exception {
        ResultEntity result = repository.listarProcesoPostulacion(filtro);        
        return result;
    }

    /**
     * @autor: JaquelineDB [30-06-2021]
     * @modificado:
     * @descripción: {Envia el documento de autorizacion para la publicacion}
     * @param:file
     */
    @Override
    public ResultClassEntity<DocumentoRespuestaEntity> autorizarPublicacion(MultipartFile file, String CodigoDoc,String NombreArchivo, Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto) {
        return repository.autorizarPublicacion(file,CodigoDoc,NombreArchivo,IdProcesoPostulacion,IdSolicitante,IdUsuarioRegistro,IdTipoDocumento,IdPostulacionPFDM, idDocumentoAdjunto);
    }

    /**
     * @autor: JaquelineDB [01-06-2021]
     * @modificado:
     * @descripción: {EObtiene las autorizaciones de publicacion necesarias}
     * @param:file
     */
    @Override
    public ResultEntity<DocumentoRespuestaEntity> obtenerAutorizacion(AnexoRequestEntity filtro) {
        return repository.obtenerAutorizacion(filtro);
    }

    /**
     * @autor: JaquelineDB [01-06-2021]
     * @modificado:
     * @descripción: {Adjunta la resolucion terminando con el proceso de postulacion}
     * @param:file
     */
    @Override
    public ResultClassEntity AdjuntarResolucionDenegada(MultipartFile file, String CodigoDoc,String NombreArchivo, Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto) {
        return repository.AdjuntarResolucionDenegada(file,CodigoDoc, NombreArchivo,IdProcesoPostulacion,IdSolicitante,IdUsuarioRegistro,IdTipoDocumento,IdPostulacionPFDM, idDocumentoAdjunto);
    }

     /**
     * @autor: JaquelineDB [01-07-2021]
     * @modificado:
     * @descripción: {Actualiza el estado del proceso de proceso de postulacion}
     * @param:file
     */
    @Override
    public ResultClassEntity actualizarEstadoProcesoPostulacion(EstatusProcesoPostulacionEntity obj) {

        ResultClassEntity result = new ResultClassEntity();

        result = repository.actualizarEstadoProcesoPostulacion(obj.getIdProcesoPostulacion(), obj.getIdStatus(), obj.getIdUsuarioModificacion(),null);

        //Se le envia un correo al solicitandole que su solicitud fue aprobada o rechazada
        //y que tiene que resolverla
        //Generación Contrato

        if(result.getSuccess() && (obj.getIdStatus().equals(5) || obj.getIdStatus().equals(27)) ) {
         String estado="";
         if(obj.getIdStatus().equals(5)){
            estado="infudanda";
         }
         else{
            estado="fundada";
         }


            EmailEntity mail = new EmailEntity();
            String[] mails = new String[1];
            if (obj.getCorreoPostulante() != null) {
                mails[0] = obj.getCorreoPostulante();
                mail.setEmail(mails);
                mail.setContent("Se le informa que se registro una oposición al Proceso de Postulación: " + obj.getIdProcesoPostulacion().toString()+
                        "el cual es "+estado);
                mail.setSubject("Registro de Oposición");
                Boolean enviarmail = email.sendEmail(mail);
                result.setInformacion(enviarmail ? "Se envio el correo correctamente" : "Ocurrió un error el enviar el correo");
            }
        }



        if(result.getSuccess() && obj.getIdStatus().equals(11)) {
            EmailEntity mail = new EmailEntity();
            String[] mails = new String[1];
            if (obj.getCorreoPostulante() != null) {
                mails[0] = obj.getCorreoPostulante();
                mail.setEmail(mails);
                mail.setContent("Se le informa que los resultados fueron modificados, y usted es el nuevo" +
                        "ganador del proceso en oferta. Debe comenzar a llenar su contrato");
                mail.setSubject("Actualización de los resultados");
                Boolean enviarmail = email.sendEmail(mail);
                result.setInformacion(enviarmail ? "Se envio el correo correctamente" : "Ocurrió un error el enviar el correo");
            }
        }

        //Con Observación
        if(result.getSuccess() && obj.getIdStatus().equals(4)){ //falta implementar por lo del correo
            //Enviar correo
            EmailEntity mail = new EmailEntity();
            String[] mails =new String[1];
            if(obj.getCorreoPostulante()!=null){
                mails[0]=obj.getCorreoPostulante();
                mail.setEmail(mails);
                mail.setContent("Se le informa que sus anexos han sido observados por favor corrija las observaciones.");
                mail.setSubject("Envio de anexos");
                Boolean enviarmail = email.sendEmail(mail);
                result.setInformacion(enviarmail? "Se envio el correo correctamente" : "Ocurrió un error el enviar el correo");
            }

        }

        //Entrega de expedientes
        if(result.getSuccess() && obj.getIdStatus().equals(14)){
            //Enviar correo
            EmailEntity mail = new EmailEntity();
            String[] mails =new String[1];
            if(obj.getCorreoPostulante()!=null){
                mails[0]=obj.getCorreoPostulante();
                mail.setEmail(mails);
                mail.setContent("Se le informa que sus anexos fueron enviados para su evaluacion. Por favor acerquese con los documentos fisicos.");
                mail.setSubject("Envio de anexos");
                Boolean enviarmail = email.sendEmail(mail);
                result.setInformacion(enviarmail? "Se envio el correo correctamente" : "Ocurrió un error el enviar el correo");
            }
        }

        //cambia a solicitud cancelada notificada
        if(result.getSuccess() && obj.getIdStatus().equals(33)){

            ResolucionDto resolucion =new ResolucionDto();
            resolucion.setTipoDocumentoGestion("THPASOL");
            resolucion.setNroDocumentoGestion(obj.getIdProcesoPostulacion());
            resolucion.setIdArchivoResolucion(obj.getIdArchivoResolucion());
            resolucion.setFechaResolucion(new Date());
            resolucion.setEstadoResolucion("ERESGEN");
            //resolucion.setComentarioResolucion();
            //resolucion.setFechaCancelacion();
            resolucion.setIdUsuarioRegistro(obj.getIdUsuarioModificacion());
            //resolucion.setIdResolucionPrevia();
            //resolucion.setIdPlanManejoEvaluacion();
            resolucionRepository.registrarResolucion(resolucion);



            //Enviar correo
            EmailEntity mail = new EmailEntity();
            String[] mails =new String[1];
            if(obj.getCorreoPostulante()!=null){
                mails[0]=obj.getCorreoPostulante();
                mail.setEmail(mails);
                mail.setContent("Se informa que su solicitud de postulación nro. "+obj.getIdProcesoPostulacion()+" ha sido cancelada por cumplirse el plazo límite para la subsanación. Puede ver la resolución en la bandeja de Solicitudes de Postulación.");
                mail.setSubject("Notificación");
                Boolean enviarmail = email.sendEmail(mail);
                result.setInformacion(enviarmail? "Se envio el correo correctamente" : "Ocurrió un error el enviar el correo");
            }
        }

        return result;

    }

    /**
     * @autor: JaquelineDB [01-07-2021]
     * @modificado:
     * @descripción: {Actualiza el estado del proceso de proceso de postulacion}
     * @param:file
     */
    @Override
    public ResultClassEntity actualizarObservacionARFFSProcesoPostulacion (ProcesoPostulacionDto obj) {

        return repository.actualizarObservacionARFFSProcesoPostulacion(obj);

    }

    @Override
    public ResultClassEntity actualizarResolucionProcesoPostulacion (ProcesoPostulacionDto obj) {

        return repository.actualizarResolucionProcesoPostulacion(obj);

    }




    @Override
    public ResultClassEntity obtenerProcesoPostulacion(ProcesoPostulacionDto obj) {

        return  repository.obtenerProcesoPostulacion(obj);
        
    }
/*
    @Override
    public ResultClassEntity obtenerInformacionProcesoPostulacion(ProcesoPostulacionDto obj) {

        return  repository.obtenerInformacionProcesoPostulacion(obj);

    }
*/
    @Override
    public ResultEntity obtenerInformacionProcesoPostulacion(ProcesoPostulacionDto request) {

        return repository.obtenerInformacionProcesoPostulacion(request);
    }


    @Override
    public ResultClassEntity actualizarProcesoPostulacion(ProcesoPostulacionDto obj) {

        return  repository.actualizarProcesoPostulacion(obj);

    }

    @Override
    public ResultClassEntity tieneObservacion(ProcesoPostulacionDto obj) {

        return  repository.tieneObservacion(obj);

    }

    @Override
    public ResultClassEntity obtenerProcesoPostulacionValidarRequisito(ProcesoPostulacionDto obj) {
        return repository.obtenerProcesoPostulacionValidarRequisito(obj);
    }

    @Override
    public ResultClassEntity actualizarEnvioPrueba(ProcesoPostulacionDto obj){

        return repository.actualizarEnvioPrueba(obj);
    }

    @Override
    public ResultClassEntity validarPublicacionId(ProcesoPostulacionDto obj){

        return repository.validarPublicacionId(obj);
    }

}
