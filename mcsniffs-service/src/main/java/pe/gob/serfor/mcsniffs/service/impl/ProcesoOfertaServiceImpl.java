package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.EnviarConsultaRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ProcesoOfertaService;

import java.util.List;

@Service
public class ProcesoOfertaServiceImpl implements ProcesoOfertaService {
    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Servicio de Proceso oferta en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    ProcesoOfertaRepository repository;

    @Autowired
    LogAuditoriaService logAuditoriaService;


    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Guarda e proceso en oferta}
     * @param:consulta
     */
    @Override
    public ResultClassEntity guardarProcesoOferta(ProcesoOfertaEntity obj) throws Exception {

        ResultClassEntity resultClassEntity = repository.guardarProcesoOferta(obj);

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_PROCESO_OFERTA,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MAD_PROCESO_OFERTA + resultClassEntity.getCodigo(),
                obj.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    /**
     * @autor: JaquelineDB [15-06-2021]
     * @modificado:
     * @descripción: {Lista los procesos en oferta}
     * @param:consulta
     */
    @Override
    public ResultClassEntity listarProcesoOferta(ProcesoOfertaRequestEntity obj) {
        return repository.listarProcesoOferta(obj);
    }

    /**
     * @autor: Julio Meza [16-11-2021]
     * @modificado:
     * @descripción: {Elimina un Proceso de oferta, tb elimina el registro de las uas seleccionadas}
     * @param:ProcesoOfertaRequestEntity
     */
    @Override
    public ResultClassEntity actualizarEsttatusProcesoOferta(Integer IdProcesoOferta, Integer IdStatus,
            Integer IdUsuarioModificacion) {
        return repository.actualizarEsttatusProcesoOferta(IdProcesoOferta, IdStatus, IdUsuarioModificacion);
    }

    @Override
    public ResultClassEntity eliminarProcesoOferta(ProcesoOfertaRequestEntity procesoOfertaRequestEntity) throws Exception {

        ResultClassEntity resultClassEntity = repository.eliminarProcesoOferta(procesoOfertaRequestEntity);


        LogAuditoriaEntity logAuditoriaEntityUnidadAprovechamiento = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAC_UNIDAD_APROVECHAMIENTO,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MAC_UNIDAD_APROVECHAMIENTO + procesoOfertaRequestEntity.getIdProcesoOferta(),
                procesoOfertaRequestEntity.getIdUsuarioElimina());

        LogAuditoriaEntity logAuditoriaEntityProcesoOferta = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_PROCESO_OFERTA,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MAD_PROCESO_OFERTA + procesoOfertaRequestEntity.getIdProcesoOferta(),
                procesoOfertaRequestEntity.getIdUsuarioElimina());


        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntityUnidadAprovechamiento);
        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntityProcesoOferta);

        return resultClassEntity;
    }

}
