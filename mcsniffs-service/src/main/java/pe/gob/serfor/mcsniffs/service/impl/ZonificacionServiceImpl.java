package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Parametro.ZonificacionDTO;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ZonificacionEntity;
import pe.gob.serfor.mcsniffs.repository.ZonificacionRepository;
import pe.gob.serfor.mcsniffs.service.ZonificacionService;
import pe.gob.serfor.mcsniffs.entity.ZonaEntity;
import pe.gob.serfor.mcsniffs.entity.ZonaAnexoEntity;

import java.util.ArrayList;
import java.util.List;

@Service("zonificacionServiceImpl")
public class ZonificacionServiceImpl implements ZonificacionService {

    @Autowired
    private ZonificacionRepository zonificacionRepository;

    @Override
    public ResultClassEntity ListarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception {

        ResultClassEntity resultClassEntity = zonificacionRepository.ListarZonificacion(zonificacionEntity);
        List<ZonificacionEntity> zonificacionEntityList = (List<ZonificacionEntity>)resultClassEntity.getData();

        List<ZonificacionDTO> zonificacionDTOList = new ArrayList<>();
        List<ZonificacionEntity> listPadres = getListZonificacionPadres(zonificacionEntityList);

        for (ZonificacionEntity ze :listPadres){
            ZonificacionDTO zonificacionDTO = new ZonificacionDTO();
            zonificacionDTO.setIdZonificacion(ze.getIdZonificacion());
            zonificacionDTO.setIdPlanManejo(ze.getIdPlanManejo());
            zonificacionDTO.setIdZonificacionPadre(ze.getIdZonificacionPadre());
            zonificacionDTO.setCodZonificacion(ze.getCodZonificacion());
            zonificacionDTO.setZona(ze.getZona());
            zonificacionDTO.setAnexo1(ze.getAnexo1());
            zonificacionDTO.setAnexo2(ze.getAnexo2());
            zonificacionDTO.setAnexo3(ze.getAnexo3());
            zonificacionDTO.setTotal(ze.getTotal());
            zonificacionDTO.setPorcentaje(ze.getPorcentaje());
            //List<ZonificacionEntity> subzonas = new ArrayList<>();
            List<ZonificacionEntity> listHijos = getListZonificacionHijasByIdPadre(zonificacionEntityList, ze.getIdZonificacion());
            zonificacionDTO.setListSubzonas(listHijos);
            zonificacionDTOList.add(zonificacionDTO);
        }
        ZonificacionDTO zdto = zonificacionDTOList.get(0);
        if(zdto.getIdPlanManejo() == null){
            List<ZonificacionDTO> lista = setDefaultList(zonificacionDTOList);
            resultClassEntity.setData(lista);
        }else{
            resultClassEntity.setData(zonificacionDTOList);
        }


        return resultClassEntity;
    }

    public List<ZonificacionDTO> setDefaultList(List<ZonificacionDTO> lista) {
        for (ZonificacionDTO zona:lista) {
            zona.setIdZonificacion(0);
            zona.setIdZonificacionPadre(0);
            for (ZonificacionEntity ze:zona.getListSubzonas()) {
                ze.setIdZonificacion(0);
                ze.setIdZonificacionPadre(0);
            }
        }
        return lista;
    }

    public List<ZonificacionEntity> getListZonificacionPadres(List<ZonificacionEntity> zonificacionEntityList){
        List<ZonificacionEntity> returnListZonasPadres = new ArrayList<>();
        for (ZonificacionEntity zona:zonificacionEntityList) {
                if(zona.getIdZonificacionPadre() == 0){
                    returnListZonasPadres.add(zona);
                }
        }
        return returnListZonasPadres;
    }

    public List<ZonificacionEntity> getListZonificacionHijasByIdPadre(List<ZonificacionEntity> zonificacionEntityList, Integer idZonificacionPadre){
        List<ZonificacionEntity> returnListZonasHijas = new ArrayList<>();
        for (ZonificacionEntity zona:zonificacionEntityList) {
            if(zona.getIdZonificacionPadre() == idZonificacionPadre){
                returnListZonasHijas.add(zona);
            }
        }
        return returnListZonasHijas;
    }

    @Override
    public ResultClassEntity RegistrarZonificacion( List<ZonificacionDTO> zonificacionDTOList) throws Exception {

        return zonificacionRepository.RegistrarZonificacion(zonificacionDTOList);
    }

//    @Override
//    public ResultClassEntity ActualizarZonificacion( List<ZonificacionDTO> zonificacionDTOList) throws Exception {
//        return zonificacionRepository.ActualizarZonificacion(zonificacionDTOList);
//    }

    @Override
    public ResultClassEntity EliminarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception {
        return zonificacionRepository.EliminarZonificacion(zonificacionEntity);
    }
    @Override
    public ResultClassEntity listarZona(ZonaEntity item) throws Exception {
        ResultClassEntity resultClassEntity = zonificacionRepository.listarZona(item);
        List<ZonaEntity> items = (List<ZonaEntity>)resultClassEntity.getData();
        items.forEach(t -> {
            try {
                ResultClassEntity resultClassEntity2 = zonificacionRepository.listarZonaAnexo(t.getIdZona());
                List<ZonaAnexoEntity> zonaAnexo = (List<ZonaAnexoEntity>)resultClassEntity2.getData();
                t.setZonaAnexo(zonaAnexo);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        resultClassEntity.setData(items);
        return resultClassEntity;
    }
    @Override
    public ResultClassEntity registrarZona( List<ZonaEntity> items) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            for (ZonaEntity zona : items) {
                ZonaEntity ze = new ZonaEntity();
                ze.setIdZona(zona.getIdZona());
                ze.setIdPlanManejo(zona.getIdPlanManejo());
                ze.setCodigoZona(zona.getCodigoZona());
                ze.setNombre(zona.getNombre());
                ze.setIdZonaPadre(zona.getIdZonaPadre());
                ze.setTotal(zona.getTotal());
                ze.setPorcentaje(zona.getPorcentaje());
                ze.setIdUsuarioRegistro(zona.getIdUsuarioRegistro());
                ze.setIdUsuarioModificacion(zona.getIdUsuarioModificacion());
                Integer idZona = zonificacionRepository.registrarZona(ze);
                if(zona.getIdZona() == 0){
                   ze.setIdZona(idZona);
                }
                if(zona.getZonaAnexo().size() > 0){
                    for (ZonaAnexoEntity zonaAnexo : zona.getZonaAnexo()) {
                        zonaAnexo.setIdUsuarioRegistro(ze.getIdUsuarioRegistro());
                        zonaAnexo.setIdZona(ze.getIdZona());
                       zonificacionRepository.registrarZonaAnexo(zonaAnexo);
                    }
                }
            }

            result.setData(items);
            result.setSuccess(true);
            result.setMessage("Se registró la zona correctamente");
            return  result;

        }catch (Exception e ){
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }
    @Override
    public ResultClassEntity eliminarZona(ZonaEntity item) throws Exception {
        return zonificacionRepository.eliminarZona(item);
    }
}
