package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface PotencialProduccionForestalService {
    ResultClassEntity registrar(PotencialProduccionForestalEntity item,MultipartFile file)throws Exception;
    ResultClassEntity listar(Integer idPlanManejo)throws Exception;

    /**
     * @autor: Rafael Azaña [16/10-2021]
     * @modificado:
     * @descripción: {CRUD DE POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */

    ResultClassEntity<List<PotencialProduccionForestalEntity>> ListarPotencialProducForestal(Integer idPlanManejo,String codigo ) throws Exception;
    ResultClassEntity<List<PotencialProduccionForestalEntity>> ListarPotencialProducForestalTitular (String tipoDocumento,String nroDocumento,String codigoProcesoTitular,Integer idPlanManejo,String codigoProceso) throws Exception;
    ResultClassEntity RegistrarPotencialProducForestal(List<PotencialProduccionForestalEntity> list) throws Exception;
    ResultClassEntity EliminarPotencialProducForestal(PotencialProduccionForestalDto potencialProduccionForestalDto) throws Exception;
    ResultClassEntity registrarPotencialProduccionExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                        String codigoTipo, String  codigoSubTipo, String codigoTipoDet, String codigoSubTipoDet,
                                                        int idPlanManejo, int idUsuarioRegistro);
}
