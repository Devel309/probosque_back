package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.CensoForestalListarDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ContratoTHDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.CensoForestalListarRepository;
import pe.gob.serfor.mcsniffs.service.CensoForestalListarService;

@Service("censoForestalListarService")
public class CensoForestalListarServiceImpl implements CensoForestalListarService {
    @Autowired
    
    private CensoForestalListarRepository censoForestalListarRepository;
   
    @Override
    public ResultClassEntity<List<CensoForestalListarDto>> CensoForestalListar(CensoForestalListarDto dto) throws Exception {        
        
        List<CensoForestalListarDto> lista = censoForestalListarRepository.censoForestalListar(dto);
        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");

        return  result;
    }

    @Override
    public ResultClassEntity<List<ContratoTHDto>> listaTHContratoPorFiltro(ContratoTHDto data) throws Exception {
        ResultClassEntity<List<ContratoTHDto>> result = new ResultClassEntity<>();

        List<ContratoTHDto> lista = censoForestalListarRepository.listaTHContratoPorFiltro(data);

        result.setData(lista);
        result.setSuccess(true);
        result.setMessage("Lista de contratos");
        result.setTotalRecord(lista==null ? 0 : lista.isEmpty() ? 0 : lista.get(0) ==null ? 0 : lista.get(0).getTotalRecord());
        return result;
    }


    
    }

