package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.ArchivoSolicitudAccesoRepository;
import pe.gob.serfor.mcsniffs.repository.ConsultaEntidadRepository;
import pe.gob.serfor.mcsniffs.repository.FileServerRepository;
import pe.gob.serfor.mcsniffs.service.ArchivoSolicitudAccesoService;

import java.io.File;


/**
 * @autor: Victor Matos [14-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar y obtener una solicitud de archivo}
 */
@Service
public class ArchivoSolicitudAccesoServiceImpl implements ArchivoSolicitudAccesoService {

    @Autowired
    private FileServerRepository fileServerRepository;

    @Autowired
    private ArchivoSolicitudAccesoRepository archivoSolicitudRepository;

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Transfiere archivo y guarda informacion del archivo}
     * @param: Archivo tipo MultipartFile y numero de solicitud asociado
     */
    @Override
    public ResultClassEntity<ArchivoEntity> insertarArchivoSolicitudAcceso(MultipartFile file, Integer idSolicitudAcceso,String tipoArchivo) {
        String nombreArchivoGenerado = fileServerRepository.insertarArchivoSolicitudAcceso(file);
        return archivoSolicitudRepository.insertarArchivoSolicitudAcceso(nombreArchivoGenerado,file.getOriginalFilename(), idSolicitudAcceso, tipoArchivo);
    }

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Consulta archivo y descarga el archivo del file server}
     * @param: Codigo de solicitud de acceso.
     */
    @Override
    public ResultArchivoEntity descargarArchivoSolicitudAcceso(Integer idSolicitudAcceso) {
        return fileServerRepository.descargarArchivoSolicitudAcceso(idSolicitudAcceso);
    }

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Consulta la solicitud en base de datos}
     * @param: Codigo de solicitud de acceso.
     */
    @Override
    public ResultClassEntity<ArchivoEntity> obtenerArchivoSolicitud(Integer idSolicitudAcceso) {
        return archivoSolicitudRepository.obtenerArchivoSolicitudAcceso(idSolicitudAcceso);
    }

    @Override
    public ResultClassEntity registrarDetalleArchivo(ArchivoSolicitudEntity item) throws Exception {
        return archivoSolicitudRepository.registrarDetalleArchivo(item);
    }

    @Override
    public ResultEntity<ArchivoSolicitudEntity> listarDetalleArchivo(Integer idSolicitud) {
        return archivoSolicitudRepository.listarDetalleArchivo(idSolicitud);
    }

    @Override
    public ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception {
        return archivoSolicitudRepository.eliminarDetalleArchivo(idArchivo, idUsuario);
    }
}
