package pe.gob.serfor.mcsniffs.service.util;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class DocUtilPopac {

    private DocUtilPopac(){
    }

    /**
     * @autor: Rafael azaña  - 11-02-2022
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static void processFile(Map<String, String> keys, XWPFDocument doc,
                                   List<InformacionGeneralDetalle> lstResult) throws IOException {
        List<XWPFParagraph> xwpfParagraphs = doc.getParagraphs();
        replaceTextInParagraphs(keys, xwpfParagraphs);
        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable xwpfTable : tables) {
            List<XWPFTableRow> tableRows = xwpfTable.getRows();
            for (XWPFTableRow xwpfTableRow : tableRows) {
                List<XWPFTableCell> tableCells = xwpfTableRow.getTableCells();
                for (XWPFTableCell xwpfTableCell : tableCells) {
                    xwpfParagraphs = xwpfTableCell.getParagraphs();
                    replaceTextInParagraphs(keys, xwpfParagraphs);
                }
            }
        }
    }

    /**
     * @autor: Rafael azaña  - 11-02-2022
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static void replaceTextInParagraphs(Map<String, String> keys, List<XWPFParagraph> xwpfParagraphs) {
        for (XWPFParagraph xwpfParagraph : xwpfParagraphs) {
            List<XWPFRun> xwpfRuns = xwpfParagraph.getRuns();
            for (XWPFRun xwpfRun : xwpfRuns) {
                String xwpfRunText = xwpfRun.getText(xwpfRun.getTextPosition());
                for (Map.Entry<String, String> entry : keys.entrySet()) {
                    if (xwpfRunText != null && xwpfRunText.contains(entry.getKey())) {
                        xwpfRunText = xwpfRunText.replaceAll(entry.getKey(), MapBuilder.toString(entry.getValue()));
                    }
                }
                xwpfRun.setText(xwpfRunText, 0);
            }
        }
    }

    /**
     * @autor: Rafael azaña  - 11-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word POPAC, reemplazando valores marcados
     *               en el tab de objetivos
     */

    /**
     * @autor: Jason Retamozo 08-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de objetivos
     */

    public static void procesarObjetivos(List<ObjetivoDto> lstLista, XWPFDocument doc) {
        try {
            // onjetivo general
            XWPFTable tableObjGen = doc.getTables().get(1);
            XWPFTableRow row0 = tableObjGen.getRow(0);
            CTTrPr trPrObjGen = row0.getCtRow().addNewTrPr();
            CTHeight htObjGen = trPrObjGen.addNewTrHeight();
            htObjGen.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : row0.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("2objetivo")) {
                                    text5 = text5.replace("2objetivo", lstLista.get(0).getDescripcion()+ "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            // objetivos especificos
            XWPFTable table = doc.getTables().get(2);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(0);
            List<ObjetivoDto> maderables = lstLista.stream().filter(m -> m.getDetalle().equals("Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> noMaderables = lstLista.stream().filter(n -> n.getDetalle().equals("No Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> lstOrdenada = new ArrayList<>();
            lstOrdenada.addAll(maderables);
            lstOrdenada.addAll(noMaderables);
            int i = 0;
            for (ObjetivoDto detalleEntity : lstOrdenada) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("2tipo")) {
                                        String detalle = "";
                                        if (i == 0)
                                            detalle = "Maderable";
                                        if (i == maderables.size())
                                            detalle = "No Maderable";
                                        text5 = text5.replace("2tipo", detalle);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2marca")) {
                                        String check;
                                        if (detalleEntity.getActivo().toString().equals("A")) {
                                            check = "(X)";
                                        } else {
                                            check = "( )";
                                        }
                                        text5 = text5.replace("2marca", check);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2objespecifico")) {
                                        text5 = text5.replace("2objespecifico", detalleEntity.getDescripcionDetalle() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, i);
                i++;
            }
            table.removeRow(lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 09-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de aspectos fisicos - hidrografia
     */
    public static void procesarHidrografia(List<InfBasicaAereaDetalleDto> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(9);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("41rio")) {
                                        text5 = text5.replace("41rio",
                                                detalleEntity.getNombreRio() != null
                                                        ? detalleEntity.getNombreRio().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("41quebrada")) {
                                        text5 = text5.replace("41quebrada",
                                                detalleEntity.getNombreQuebrada() != null
                                                        ? detalleEntity.getNombreQuebrada().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("41cocha")) {
                                        text5 = text5.replace("41cocha","");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 09-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de aspectos fisicos - fisiografia
     */
    public static void procesarFisiografia(List<InfBasicaAereaDetalleDto> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(10);
            XWPFTableRow row = table.getRow(2);
            XWPFTableRow rowTotal = table.getRow(3);
            System.out.println(lstLista.size());
            int i=0;
            double totalArea=0.00,totalPorc=100.00;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("42unidad")) {
                                        text5 = text5.replace("42unidad",
                                                detalleEntity.getNombreRio() != null
                                                        ? detalleEntity.getNombreRio().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42area")) {
                                        text5 = text5.replace("42area",
                                                detalleEntity.getAreaHa() != null
                                                        ? detalleEntity.getAreaHa().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                        totalArea+=detalleEntity.getAreaHa().doubleValue();
                                    } else if (text5.contains("42porc")) {
                                        text5 = text5.replace("42porc",
                                                detalleEntity.getAreaHaPorcentaje() != null
                                                        ? detalleEntity.getAreaHaPorcentaje().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42observaciones")) {
                                        text5 = text5.replace("42observaciones",
                                                detalleEntity.getObservaciones() != null
                                                        ? detalleEntity.getObservaciones().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow,3+i);
                i++;
            }
            table.removeRow(2);
            //totales
            CTTrPr trPrObjGen = rowTotal.getCtRow().addNewTrPr();
            CTHeight htObjGen = trPrObjGen.addNewTrHeight();
            htObjGen.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : rowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("42areatotal")) {
                                    text5 = text5.replace("42areatotal", totalArea+ "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("42porctotal")) {
                                    text5 = text5.replace("42porctotal", totalPorc+ " %");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 09-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de aspectos biologicos - fauna
     */
    public static void procesarFauna(List<InfBasicaAereaDetalleDto> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(11);
            XWPFTableRow row = table.getRow(1);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("51ncomun")) {
                                        text5 = text5.replace("51ncomun",
                                                detalleEntity.getNombreComun() != null
                                                        ? detalleEntity.getNombreComun().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51nnativo")) {
                                        text5 = text5.replace("51nnativo",
                                                detalleEntity.getNombreLaguna() != null
                                                        ? detalleEntity.getNombreLaguna().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51ncientifico")) {
                                        text5 = text5.replace("51ncientifico",
                                                detalleEntity.getNombreCientifico() != null
                                                        ? detalleEntity.getNombreCientifico().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51categoria")) {
                                        text5 = text5.replace("51categoria",
                                                detalleEntity.getZonaVida() != null
                                                        ? detalleEntity.getZonaVida().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51apendice")) {
                                        text5 = text5.replace("51apendice",
                                                detalleEntity.getEpoca() != null
                                                        ? detalleEntity.getEpoca().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51observaciones")) {
                                        text5 = text5.replace("51observaciones",
                                                detalleEntity.getDescripcion() != null
                                                        ? detalleEntity.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 09-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de aspectos biologicos - tipos bosque
     */
    public static void procesarTiposBosque(List<InfBasicaAereaDetalleDto> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(12);
            XWPFTableRow row = table.getRow(2);
            XWPFTableRow rowTotal = table.getRow(3);
            System.out.println(lstLista.size());
            int i=1;
            double totalArea=0.00,totalPorc=100.00;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("52tipobosque")) {
                                        text5 = text5.replace("52tipobosque",
                                                detalleEntity.getNombreRio() != null
                                                        ? detalleEntity.getNombreRio().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("52subparcela")) {
                                        text5 = text5.replace("52subparcela",
                                                detalleEntity.getAreaHa() != null
                                                        ? detalleEntity.getAreaHa().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                        totalArea+=detalleEntity.getAreaHa().doubleValue();
                                    } else if (text5.contains("52area")) {
                                        text5 = text5.replace("52area",
                                                detalleEntity.getAreaHa() != null
                                                        ? detalleEntity.getAreaHa().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                        totalArea+=detalleEntity.getAreaHa().doubleValue();
                                    } else if (text5.contains("52porc")) {
                                        text5 = text5.replace("52porc",
                                                detalleEntity.getAreaHaPorcentaje() != null
                                                        ? detalleEntity.getAreaHaPorcentaje().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("52observaciones")) {
                                        text5 = text5.replace("52observaciones",
                                                detalleEntity.getObservaciones() != null
                                                        ? detalleEntity.getObservaciones().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow,2+i);
                i++;
            }
            table.removeRow(2);
            //totales
            CTTrPr trPrObjGen = rowTotal.getCtRow().addNewTrPr();
            CTHeight htObjGen = trPrObjGen.addNewTrHeight();
            htObjGen.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : rowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("52totalarea")) {
                                    text5 = text5.replace("52totalarea", totalArea+ "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("52totalporc")) {
                                    text5 = text5.replace("52totalporc", totalPorc+ " %");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 10-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de informacion socioeconomica - infra
     */
    public static void procesarInfraestructuraServicios(List<InformacionBasicaEntity> lstLista, XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(14);
            XWPFTableRow row = table.getRow(3);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    for (InformacionBasicaDetalleEntity detalleEntity : lstLista.get(0).getListInformacionBasicaDet()) {
                        XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                        CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                        CTHeight ht = trPr.addNewTrHeight();
                        ht.setVal(BigInteger.valueOf(240));

                        for (XWPFTableCell cell : copiedRow.getTableCells()) {
                            for (XWPFParagraph p : cell.getParagraphs()) {
                                for (XWPFRun r : p.getRuns()) {
                                    if (r != null) {
                                        String text5 = r.getText(0);
                                        if (text5 != null) {
                                            if (text5.contains("62infra")) {
                                                text5 = text5.replace("62infra",
                                                        detalleEntity.getActividad() != null
                                                                ? detalleEntity.getActividad().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("62nombre")) {
                                                text5 = text5.replace("62nombre",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("62este")) {
                                                text5 = text5.replace("62este",
                                                        detalleEntity.getCoordenadaEsteIni() != null
                                                                ? detalleEntity.getCoordenadaEsteIni().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("62norte")) {
                                                text5 = text5.replace("62norte",
                                                        detalleEntity.getCoordenadaNorteIni() != null
                                                                ? detalleEntity.getCoordenadaNorteIni().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("62obs")) {
                                                text5 = text5.replace("62obs",
                                                        detalleEntity.getObservaciones() != null
                                                                ? detalleEntity.getObservaciones().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        table.addRow(copiedRow);
                    }
                    table.removeRow(3);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 10-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de informacion socioeconomica - antecedentes
     */
    public static void procesarAntecedentes(List<InformacionBasicaEntity> lstLista, XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(15);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            for (InformacionBasicaEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("63actividad")) {
                                        text5 = text5.replace("63actividad",
                                                detalleEntity.getComunidad() != null
                                                        ? detalleEntity.getComunidad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("63especie")) {
                                        text5 = text5.replace("63especie","");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("63conflicto")) {
                                        text5 = text5.replace("63conflicto",
                                                detalleEntity.getDistrito() != null
                                                        ? detalleEntity.getDistrito().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("63propuesta")) {
                                        text5 = text5.replace("63propuesta",
                                                detalleEntity.getDepartamento() != null
                                                        ? detalleEntity.getDepartamento().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("63obs")) {
                                        text5 = text5.replace("63obs",
                                                detalleEntity.getProvincia() != null
                                                        ? detalleEntity.getProvincia().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 11-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de Ordenamiento y Proteccion - Superficie Bloques
     */
    public static void procesarSuperficieBloques(List<OrdenamientoProteccionDetalleEntity> lstLista, XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(16);
            XWPFTableRow row = table.getRow(3);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    for (OrdenamientoProteccionDetalleEntity detalleEntity : lstLista) {
                        XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                        CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                        CTHeight ht = trPr.addNewTrHeight();
                        ht.setVal(BigInteger.valueOf(240));

                        for (XWPFTableCell cell : copiedRow.getTableCells()) {
                            for (XWPFParagraph p : cell.getParagraphs()) {
                                for (XWPFRun r : p.getRuns()) {
                                    if (r != null) {
                                        String text5 = r.getText(0);
                                        if (text5 != null) {
                                            if (text5.contains("71pc")) {
                                                text5 = text5.replace("71pc",
                                                        detalleEntity.getActividad() != null
                                                                ? detalleEntity.getActividad().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("71area")) {
                                                text5 = text5.replace("71area",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("71vertice")) {
                                                text5 = text5.replace("71vertice",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("71este")) {
                                                text5 = text5.replace("71este",
                                                        detalleEntity.getCoordenadaEste() != null
                                                                ? detalleEntity.getCoordenadaEste().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("71norte")) {
                                                text5 = text5.replace("71norte",
                                                        detalleEntity.getCoordenadaNorte() != null
                                                                ? detalleEntity.getCoordenadaNorte().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("71obs")) {
                                                text5 = text5.replace("71obs",
                                                        detalleEntity.getObservacion() != null
                                                                ? detalleEntity.getObservacion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        table.addRow(copiedRow);
                    }
                    table.removeRow(3);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 11-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de Ordenamiento y Proteccion - Superficie Area Aprovechamiento
     */
    public static void procesarSuperficieArea(List<OrdenamientoProteccionDetalleEntity> lstLista, XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(17);
            XWPFTableRow row = table.getRow(3);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    for (OrdenamientoProteccionDetalleEntity detalleEntity : lstLista) {
                        XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                        CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                        CTHeight ht = trPr.addNewTrHeight();
                        ht.setVal(BigInteger.valueOf(240));

                        for (XWPFTableCell cell : copiedRow.getTableCells()) {
                            for (XWPFParagraph p : cell.getParagraphs()) {
                                for (XWPFRun r : p.getRuns()) {
                                    if (r != null) {
                                        String text5 = r.getText(0);
                                        if (text5 != null) {
                                            if (text5.contains("72pc")) {
                                                text5 = text5.replace("72pc",
                                                        detalleEntity.getActividad() != null
                                                                ? detalleEntity.getActividad().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("72area")) {
                                                text5 = text5.replace("72area",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("72vertice")) {
                                                text5 = text5.replace("72vertice",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("72este")) {
                                                text5 = text5.replace("72este",
                                                        detalleEntity.getCoordenadaEste() != null
                                                                ? detalleEntity.getCoordenadaEste().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("72norte")) {
                                                text5 = text5.replace("72norte",
                                                        detalleEntity.getCoordenadaNorte() != null
                                                                ? detalleEntity.getCoordenadaNorte().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("72obs")) {
                                                text5 = text5.replace("72obs",
                                                        detalleEntity.getObservacion() != null
                                                                ? detalleEntity.getObservacion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        table.addRow(copiedRow);
                    }
                    table.removeRow(3);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 11-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de Ordenamiento y Proteccion - Superficie Parcela corta
     */
    public static void procesarSuperficieParcela(List<OrdenamientoProteccionDetalleEntity> lstLista, XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(18);
            XWPFTableRow row = table.getRow(3);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    for (OrdenamientoProteccionDetalleEntity detalleEntity : lstLista) {
                        XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                        CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                        CTHeight ht = trPr.addNewTrHeight();
                        ht.setVal(BigInteger.valueOf(240));

                        for (XWPFTableCell cell : copiedRow.getTableCells()) {
                            for (XWPFParagraph p : cell.getParagraphs()) {
                                for (XWPFRun r : p.getRuns()) {
                                    if (r != null) {
                                        String text5 = r.getText(0);
                                        if (text5 != null) {
                                            if (text5.contains("73pc")) {
                                                text5 = text5.replace("73pc",
                                                        detalleEntity.getActividad() != null
                                                                ? detalleEntity.getActividad().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("73area")) {
                                                text5 = text5.replace("73area",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("73vertice")) {
                                                text5 = text5.replace("73vertice",
                                                        detalleEntity.getDescripcion() != null
                                                                ? detalleEntity.getDescripcion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("73este")) {
                                                text5 = text5.replace("73este",
                                                        detalleEntity.getCoordenadaEste() != null
                                                                ? detalleEntity.getCoordenadaEste().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("73norte")) {
                                                text5 = text5.replace("73norte",
                                                        detalleEntity.getCoordenadaNorte() != null
                                                                ? detalleEntity.getCoordenadaNorte().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("73obs")) {
                                                text5 = text5.replace("73obs",
                                                        detalleEntity.getObservacion() != null
                                                                ? detalleEntity.getObservacion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        table.addRow(copiedRow);
                    }
                    table.removeRow(3);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 11-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de ordenamiento - medidas de proteccion
     */
    public static void procesarMedidasProteccion(SistemaManejoForestalDto lstLista, XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(19);
            XWPFTableRow row = table.getRow(1);
            if(lstLista.getDetalle()!=null){
                for (SistemaManejoForestalDetalleEntity detalleEntity : lstLista.getDetalle()) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("72medidas")) {
                                            text5 = text5.replace("72medidas",
                                                    detalleEntity.getActividades() != null
                                                            ? detalleEntity.getActividades().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("72descripcion")) {
                                            text5 = text5.replace("72descripcion",
                                                    detalleEntity.getDescripcionSistema() != null
                                                            ? detalleEntity.getDescripcionSistema().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow);
                }
                table.removeRow(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos  3 PMFIC}
     * @param: Integer idPlanManejo
     */

    public static XWPFDocument generarAnexo4(List<Anexo3PGMFDto> lstLista, XWPFDocument doc) {
        try {
            List<Anexo3PGMFDto> lista = new ArrayList<Anexo3PGMFDto>();

            for(Anexo3PGMFDto detalle : lstLista){
                Anexo3PGMFDto data = new Anexo3PGMFDto();
                data.setNombreComun(detalle.getNombreComun());
                data.setBloque(1);
                data.setArbHaDap10a19(detalle.getArbHaDap10a19());
                data.setArbHaDap20a29(detalle.getArbHaDap20a29());
                data.setArbHaTotalTipoBosque(detalle.getArbHaTotalTipoBosque());
                data.setArbHaTotalPorArea(detalle.getArbHaTotalPorArea());
                lista.add(data);
                Anexo3PGMFDto data2 = new Anexo3PGMFDto();
                data2.setNombreComun(detalle.getNombreComun());
                data2.setBloque(2);
                data2.setArbHaDap10a19(detalle.getAbHaDap10a19());
                data2.setArbHaDap20a29(detalle.getAbHaDap20a29());
                data2.setArbHaTotalTipoBosque(detalle.getAbHaTotalTipoBosque());
                data2.setArbHaTotalPorArea(detalle.getAbHaTotalPorArea());
                lista.add(data2);
            }
            String variable="";

            XWPFTable table = doc.getTables().get(1);
            XWPFTableRow row = table.getRow(2);
            for (Anexo3PGMFDto detalleEntity : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("4comn")) {
                                            text = text.replace("4comn",
                                                    detalleEntity.getNombreComun() != null
                                                            ? detalleEntity.getNombreComun()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("4var")) {
                                            if(detalleEntity.getBloque()==1){variable="N";}
                                            else if(detalleEntity.getBloque()==2){variable="AB m2";}
                                            text = text.replace("4var",
                                                    variable != null
                                                            ? variable
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("41")) {
                                            text = text.replace("41",
                                                    detalleEntity.getArbHaDap10a19() != null
                                                            ? detalleEntity.getArbHaDap10a19()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("42")) {
                                            text = text.replace("42",
                                                    detalleEntity.getArbHaDap20a29() != null
                                                            ? detalleEntity.getArbHaDap20a29()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("4totalbosque")) {
                                            text = text.replace("4totalbosque",
                                                    detalleEntity.getArbHaTotalTipoBosque() != null
                                                            ? detalleEntity.getArbHaTotalTipoBosque()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("4to")) {
                                            text = text.replace("4to",
                                                    detalleEntity.getArbHaTotalPorArea() != null
                                                            ? detalleEntity.getArbHaTotalPorArea()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos  5 PMFIC}
     * @param: Integer idPlanManejo
     */

    public static XWPFDocument generarAnexo5(List<Anexo2Dto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(2);
            for (Anexo2Dto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("5faja")) {
                                            text = text.replace("5faja",
                                                    detalleEntity.getNumeroFaja() != null
                                                            ? detalleEntity.getNumeroFaja()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("5arbol")) {
                                            text = text.replace("5arbol",
                                                    detalleEntity.getnArbol() != null
                                                            ? detalleEntity.getnArbol()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5especie")) {
                                            text = text.replace("5especie",
                                                    detalleEntity.getNombreEspecies() != null
                                                            ? detalleEntity.getNombreEspecies()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5dap")) {
                                            text = text.replace("5dap",
                                                    detalleEntity.getDap() != null
                                                            ? detalleEntity.getDap()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5altura")) {
                                            text = text.replace("5altura",
                                                    detalleEntity.getAlturaComercial() != null
                                                            ? detalleEntity.getAlturaComercial()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5calidad")) {
                                            text = text.replace("5calidad",
                                                    detalleEntity.getCalidadFuste() != null
                                                            ? detalleEntity.getCalidadFuste()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5este")) {
                                            text = text.replace("5este",
                                                    detalleEntity.getEste() != null
                                                            ? detalleEntity.getEste()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5norte")) {
                                            text = text.replace("5norte",
                                                    detalleEntity.getNorte() != null
                                                            ? detalleEntity.getNorte()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5vc")) {
                                            text = text.replace("5vc",
                                                    detalleEntity.getVolumen() != null
                                                            ? detalleEntity.getVolumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("5semillero")) {
                                            text = text.replace("5semillero",
                                                    detalleEntity.getCategoria() != null
                                                            ? detalleEntity.getCategoria()
                                                            : "");
                                            r.setText(text, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }


    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos  6 PMFIC}
     * @param: Integer idPlanManejo
     */

    public static XWPFDocument generarAnexo6(List<ResultadosAnexo6> lstLista, XWPFDocument doc) {
        try {

            List <TablaAnexo6Dto> listaDetalle=new ArrayList<TablaAnexo6Dto>();
            if (lstLista.size() > 0) {
                ResultadosAnexo6 PMFICLista = lstLista.get(0);
                listaDetalle = PMFICLista.getListaTablaAnexo6Dto();
            }
            List <TablaAnexo6Dto> listaDetalle2=new ArrayList<TablaAnexo6Dto>();
            for(TablaAnexo6Dto detalle : listaDetalle){
                TablaAnexo6Dto data = new TablaAnexo6Dto();
                data.setNombreComun(detalle.getNombreComun());
                data.setVariable("N");
                data.setnTotalPorTipoBosque(detalle.getnTotalPorTipoBosque());
                data.setnTotalPorHa(detalle.getnTotalPorHa());
                listaDetalle2.add(data);
                TablaAnexo6Dto data2 = new TablaAnexo6Dto();
                data2.setNombreComun(detalle.getNombreComun());
                data2.setVariable("C**");
                data2.setnTotalPorTipoBosque(detalle.getcTotalPorTipoBosque());
                data2.setnTotalPorHa(detalle.getcTotalPorHa());
                listaDetalle2.add(data2);
            }


            XWPFTable table = doc.getTables().get(3);
            XWPFTableRow row = table.getRow(1);
            for (TablaAnexo6Dto detalleEntity : listaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("6tipobosque")) {
                                            text = text.replace("6tipobosque",
                                                    "");
                                            r.setText(text, 0);
                                        } else if (text.contains("6comun")) {
                                            text = text.replace("6comun",
                                                    detalleEntity.getNombreComun() != null
                                                            ? detalleEntity.getNombreComun()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("6var")) {
                                            text = text.replace("6var",
                                                    detalleEntity.getVariable() != null
                                                            ? detalleEntity.getVariable()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("6totipobosque")) {
                                            text = text.replace("6totipobosque",
                                                    detalleEntity.getnTotalPorTipoBosque() != null
                                                            ? detalleEntity.getnTotalPorTipoBosque()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("6totalha")) {
                                            text = text.replace("6totalha",
                                                    detalleEntity.getnTotalPorHa() != null
                                                            ? detalleEntity.getnTotalPorHa()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }




    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de organizacion para el desarrollo - maderable
     */
    public static void procesarOrganizacionMaderable(List<ActividadSilviculturalDetalleEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(38);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            for (ActividadSilviculturalDetalleEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("111act")) {
                                        text5 = text5.replace("111act",
                                                detalleEntity.getActividad() != null
                                                        ? detalleEntity.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("111forma")) {
                                        text5 = text5.replace("111forma",
                                                detalleEntity.getDescripcionDetalle() != null
                                                        ? detalleEntity.getDescripcionDetalle().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - zonificacion umf
     */
    public static void procesarZonificacionUMF(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(25);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("9categoria")) {
                                                    text5 = text5.replace("9categoria",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("9uso")) {
                                                    text5 = text5.replace("9uso",
                                                            detalleEntity.getNuTotalVcp() != null
                                                                    ? detalleEntity.getNuTotalVcp().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("9actividad")) {
                                                    text5 = text5.replace("9actividad",
                                                            detalleEntity.getActividad() != null
                                                                    ? detalleEntity.getActividad().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(2);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - fines maderables
     */
    public static void procesarSMFinesMaderables(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(26);
            XWPFTableRow row = table.getRow(0);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("911sistema")) {
                                                    text5 = text5.replace("911sistema",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(0);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - fines no maderables
     */
    public static void procesarSMFinesNoMaderables(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(29);
            XWPFTableRow row = table.getRow(0);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("921sistema")) {
                                                    text5 = text5.replace("921sistema",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(0);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - Ciclo corte
     */
    public static void procesarSMCicloCorte(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(27);
            XWPFTableRow row = table.getRow(0);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("912ciclo")) {
                                                    text5 = text5.replace("912ciclo",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(0);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - Ciclo aprovechamiento
     */
    public static void procesarSMCicloAprovechamiento(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(30);
            XWPFTableRow row = table.getRow(0);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("922ciclo")) {
                                                    text5 = text5.replace("922ciclo",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(0);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - actividades aprovechamiento
     */
    public static void procesarSMActividadesAprovechamiento(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(28);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("913actividad")) {
                                                    text5 = text5.replace("913actividad",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("913sistema")) {
                                                    text5 = text5.replace("913sistema",
                                                            detalleEntity.getDescripcionTratamiento() != null
                                                                    ? detalleEntity.getDescripcionTratamiento().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("913maquinaria")) {
                                                    text5 = text5.replace("913maquinaria",
                                                            detalleEntity.getDescripcionCamino() != null
                                                                    ? detalleEntity.getDescripcionCamino().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("913personal")) {
                                                    text5 = text5.replace("913personal",
                                                            detalleEntity.getDescripcionManoObra() != null
                                                                    ? detalleEntity.getDescripcionManoObra().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("913obs")) {
                                                    text5 = text5.replace("913obs",
                                                            detalleEntity.getDescripcionOtro() != null
                                                                    ? detalleEntity.getDescripcionOtro().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(2);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - actividades aprovechamiento no maderable
     */
    public static void procesarSMActividadesAprovechamientoNM(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(31);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("923actividad")) {
                                                    text5 = text5.replace("923actividad",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("923sistema")) {
                                                    text5 = text5.replace("923sistema",
                                                            detalleEntity.getDescripcionTratamiento() != null
                                                                    ? detalleEntity.getDescripcionTratamiento().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("923maquinaria")) {
                                                    text5 = text5.replace("923maquinaria",
                                                            detalleEntity.getDescripcionCamino() != null
                                                                    ? detalleEntity.getDescripcionCamino().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("923personal")) {
                                                    text5 = text5.replace("923personal",
                                                            detalleEntity.getDescripcionManoObra() != null
                                                                    ? detalleEntity.getDescripcionManoObra().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("923obs")) {
                                                    text5 = text5.replace("923obs",
                                                            detalleEntity.getDescripcionOtro() != null
                                                                    ? detalleEntity.getDescripcionOtro().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(2);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - actividades obligatorias
     */
    public static void procesarSMLaboresObligatorias(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(32);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("931practica")) {
                                                    text5 = text5.replace("931practica",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("931descripcion")) {
                                                    text5 = text5.replace("931descripcion",
                                                            detalleEntity.getDescripcionOtro() != null
                                                                    ? detalleEntity.getDescripcionOtro().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(2);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de sistema manejo forestal - actividades opcionales
     */
    public static void procesarSMLaboresOpcionales(List<ManejoBosqueEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(33);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            if(lstLista!=null){
                if(lstLista.size()>0){
                    if(lstLista.get(0).getListManejoBosqueDetalle()!=null){
                        for (ManejoBosqueDetalleEntity detalleEntity : lstLista.get(0).getListManejoBosqueDetalle()) {
                            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                            CTHeight ht = trPr.addNewTrHeight();
                            ht.setVal(BigInteger.valueOf(240));

                            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("931practica")) {
                                                    text5 = text5.replace("931practica",
                                                            detalleEntity.getTratamientoSilvicultural() != null
                                                                    ? detalleEntity.getTratamientoSilvicultural().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("931descripcion")) {
                                                    text5 = text5.replace("931descripcion",
                                                            detalleEntity.getDescripcionOtro() != null
                                                                    ? detalleEntity.getDescripcionOtro().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRow);
                        }
                        table.removeRow(2);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de organizacion para el desarrollo - NO maderable
     */
    public static void procesarOrganizacionNoMaderable(List<ActividadSilviculturalDetalleEntity> lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(39);
            XWPFTableRow row = table.getRow(2);
            System.out.println(lstLista.size());
            for (ActividadSilviculturalDetalleEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("112act")) {
                                        text5 = text5.replace("112act",
                                                detalleEntity.getActividad() != null
                                                        ? detalleEntity.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("112forma")) {
                                        text5 = text5.replace("112forma",
                                                detalleEntity.getDescripcionDetalle() != null
                                                        ? detalleEntity.getDescripcionDetalle().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 15-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados en
     *               el
     *               documento Cronograma de Actividades}
     */
    public static void generarCronogramaActividadesPOCC(List<CronogramaActividadEntity> lstCrono, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(40);
            XWPFTableRow row1 = table.getRow(4);

            int i = 0;
            for (CronogramaActividadEntity actividad : lstCrono) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("12act")) {
                                        text5 = text5.replace("12act",
                                                actividad.getActividad() != null ? actividad.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("12X")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                                        if (lstDetalle != null) {
                                            boolean breaker = false;
                                            for (CronogramaActividadDetalleEntity detalle : lstDetalle) {
                                                if (text5.contains("12X"+detalle.getAnio()+"m"+detalle.getMes())){
                                                    text5 = text5.replace("12X"+detalle.getAnio()+"m"+detalle.getMes(), "X");
                                                    r.setText(text5, 0);
                                                    breaker = true;
                                                    break;
                                                }
                                            }
                                            if (!breaker) {
                                                r.setText("", 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 4 + i);
                i++;
            }
            table.removeRow(4 + lstCrono.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 16-02-2022
     *         * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados
     *               en el tab de aspectos complementarios
     */
    public static void procesarAspectosComplementarios(InformacionGeneralDEMAEntity lstLista,XWPFDocument doc){
        try {
            XWPFTable table = doc.getTables().get(44);
            XWPFTableRow row = table.getRow(0);

            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("141OBS")) {
                                    text5 = text5.replace("141OBS",
                                            lstLista.getDetalle() != null
                                                    ? lstLista.getDetalle().toString()
                                                    : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRow);
            table.removeRow(0);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 16-02-2022
     * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados en
     *               el
     *               documento Rentabilidad - Ingresos}
     */
    public static void generarIngresoRentabilidad(List<RentabilidadManejoForestalDto> lstIngresos,
                                                  XWPFDocument doc, Double[] total) {
        try {
            total[0] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstIngresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(42);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("131INGRMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("131INGRTOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto ingreso : lstIngresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("131INGRRUB")) {
                                        text5 = text5.replace("131INGRRUB",
                                                ingreso.getRubro() != null ? ingreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("INGR131DESC")) {
                                        text5 = text5.replace("INGR131DESC",
                                                ingreso.getDescripcion() != null ? ingreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("131INGRMONTOT")) {
                                        text5 = text5.replace("131INGRMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[0] += totalFila;
                                    } else if (text5.contains("131INGRMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : ingreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("131INGRMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("131INGRMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("131INGRMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstIngresos.size());

            // totales de ingresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("131INGRTOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("131INGRTOTALA" + integer.toString())) {
                                                text5 = text5.replace("131INGRTOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("131INGRTOTALT")) {
                                    text5 = text5.replace("131INGRTOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstIngresos.size());
            table.removeRow(2 + lstIngresos.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 16-02-2022
     * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados en
     *               el
     *               documento Rentabilidad- Egresos}
     */
    public static void generarEgresoRentabilidad(List<RentabilidadManejoForestalDto> lstEgresos,
                                                 XWPFDocument doc, Double[] total) {
        try {
            total[1] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstEgresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(43);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("131EGREMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("131EGRETOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto egreso : lstEgresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("131EGRERUB")) {
                                        text5 = text5.replace("131EGRERUB",
                                                egreso.getRubro() != null ? egreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("131EGREDESC")) {
                                        text5 = text5.replace("121EGREDESC",
                                                egreso.getDescripcion() != null ? egreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("131EGREMONTOT")) {
                                        text5 = text5.replace("131EGREMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[1] += totalFila;
                                    } else if (text5.contains("131EGREMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : egreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("131EGREMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("131EGREMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("131EGREMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstEgresos.size());

            // totales de egresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("131EGRETOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("131EGRETOTALA" + integer.toString())) {
                                                text5 = text5.replace("131EGRETOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("131EGRETOTALT")) {
                                    text5 = text5.replace("131EGRETOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstEgresos.size());
            table.removeRow(2 + lstEgresos.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 16-02-2022
     * @creado:
     * @descripción: {Procesa un archivo word PMFIC, reemplazando valores marcados en
     *               el
     *               documento Rentabilidad - Totales}
     */
    public static void generarTotalesRentabilidad(Double[] totales, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(41);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            XWPFTableRow row3 = table.getRow(3);

            // total ingresos
            XWPFTableRow copiedRowTotalIngr = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            CTTrPr trPrIngr = copiedRowTotalIngr.getCtRow().addNewTrPr();
            CTHeight htIngr = trPrIngr.addNewTrHeight();
            htIngr.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalIngr.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("13INI")) {
                                text5 = text5.replace("13INI", totales[0] != null ? totales[0].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalIngr, 1);
            table.removeRow(2);
            // total egresos
            XWPFTableRow copiedRowTotalEgre = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPrEgre = copiedRowTotalEgre.getCtRow().addNewTrPr();
            CTHeight htEgre = trPrEgre.addNewTrHeight();
            htEgre.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalEgre.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("13INE")) {
                                text5 = text5.replace("13INE", totales[1] != null ? totales[1].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalEgre, 2);
            table.removeRow(3);
            // total neto
            XWPFTableRow copiedRowTotalNeto = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table);
            CTTrPr trPrNeto = copiedRowTotalNeto.getCtRow().addNewTrPr();
            CTHeight htNeto = trPrNeto.addNewTrHeight();
            htNeto.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalNeto.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("13BN")) {
                                Double totalNeto = (totales[0] != null ? totales[0] : 0.00)
                                        - (totales[1] != null ? totales[1] : 0.00);
                                text5 = text5.replace("13BN", totalNeto != null ? totalNeto.toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalNeto, 3);
            table.removeRow(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos  6 PMFIC}
     * @param: Integer idPlanManejo
     */

    public static XWPFDocument generarAnexo7(List<Anexo2Dto> lstLista, XWPFDocument doc) {
        try {


            XWPFTable table = doc.getTables().get(4);
            XWPFTableRow row = table.getRow(2);
            for (Anexo2Dto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("7faja")) {
                                            text = text.replace("7faja",
                                                    detalleEntity.getNumeroFaja() != null
                                                            ? detalleEntity.getNumeroFaja()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("7arbol")) {
                                            text = text.replace("7arbol",
                                                    detalleEntity.getnArbol() != null
                                                            ? detalleEntity.getnArbol()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("7especie")) {
                                            text = text.replace("7especie",
                                                    detalleEntity.getNombreEspecies() != null
                                                            ? detalleEntity.getNombreEspecies()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("7este")) {
                                            text = text.replace("7este",
                                                    detalleEntity.getEste() != null
                                                            ? detalleEntity.getEste()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("7norte")) {
                                            text = text.replace("7norte",
                                                    detalleEntity.getNorte() != null
                                                            ? detalleEntity.getNorte()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("7vc")) {
                                            text = text.replace("7vc",
                                                    detalleEntity.getC() != null
                                                            ? detalleEntity.getC()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("7semillero")) {
                                            text = text.replace("7semillero",
                                                    detalleEntity.getCategoria() != null
                                                            ? detalleEntity.getCategoria()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }


    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos  6 PMFIC}
     * @param: Integer idPlanManejo
     */

    public static XWPFDocument generarAnexo8(List<ListaEspecieDto> lstLista, XWPFDocument doc) {
        try {


            XWPFTable table = doc.getTables().get(5);
            XWPFTableRow row = table.getRow(1);
            for (ListaEspecieDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("8comun")) {
                                            text = text.replace("8comun",
                                                    detalleEntity.getTextNombreComun() != null
                                                            ? detalleEntity.getTextNombreComun()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("8nativo")) {
                                            text = text.replace("8nativo",
                                                    detalleEntity.getNombreNativo() != null
                                                            ? detalleEntity.getNombreNativo()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("8cientifico")) {
                                            text = text.replace("8cientifico",
                                                    detalleEntity.getTextNombreCientifico() != null
                                                            ? detalleEntity.getTextNombreCientifico()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }



}
