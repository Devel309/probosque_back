package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface ServicioExternoService {
    ResultEntity ejecutarServicio(String url, String json,String token);
}
