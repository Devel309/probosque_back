package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.AspectoFisicoFisiografiaRepository;
import pe.gob.serfor.mcsniffs.service.AspectoFisicoFisiografiaService;

import java.util.List;

@Service("AspectoFisicoFisiografiaService")
public class AspectoFisicoFisiografiaServiceImpl implements AspectoFisicoFisiografiaService {

    @Autowired
    private AspectoFisicoFisiografiaRepository aspectoFisicoFisiografiaRepository;

    @Override
    public ResultClassEntity ListarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception {
        return aspectoFisicoFisiografiaRepository.ListarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
    }

    @Override
    public ResultClassEntity RegistrarAspectoFisicoFisiografia(List<AspectoFisicoFisiografiaEntity> listAspectoFisicoFisiografiaEntity) throws Exception {
        return aspectoFisicoFisiografiaRepository.RegistrarAspectoFisicoFisiografia(listAspectoFisicoFisiografiaEntity);
    }

    @Override
    public ResultClassEntity EliminarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception {
        return aspectoFisicoFisiografiaRepository.EliminarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
    }

    @Override
    public ResultClassEntity ConfigurarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception {
        return aspectoFisicoFisiografiaRepository.ConfigurarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
    }


}
