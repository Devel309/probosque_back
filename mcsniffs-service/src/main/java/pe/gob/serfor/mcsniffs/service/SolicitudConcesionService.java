package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ClonarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarResAdminFavDesfavDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;

public interface SolicitudConcesionService {
    ResultClassEntity listarSolicitudConcesionPorFiltro(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity listarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity obtenerInformacionSolicitanteSolicitado(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity registrarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity registrarInformacionSolicitante(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity registrarInformacionSobreSolicitado(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity actualizarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity actualizarInformacionSolicitante(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity actualizarInformacionSobreSolicitado(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity eliminarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity obtenerArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception;

    ResultClassEntity registrarArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception;

    ResultClassEntity registrarArchivosSolicitudConcesion(List<SolicitudConcesionArchivoEntity> items) throws Exception;

    ResultClassEntity actualizarArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception;

    ResultClassEntity eliminarArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception;

    ResultArchivoEntity descargarSolicitudConcesionAnexo(DescargarSolicitudConcesionDto obj) throws Exception;

    ResultClassEntity listarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto) throws Exception;

    ResultClassEntity registrarSolicitudConcesionAnexo(List<SolicitudConcesionAnexoDto> obj) throws Exception;

    ResultClassEntity enviarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity enviarComunicado(SolicitudConcesionDto obj) throws Exception;

    ResultArchivoEntity descargarPlantillaManejo();

    ResultArchivoEntity descargarPlantillaPresupuesto();

    ResultClassEntity clonarSolicitudConcesion(ClonarSolicitudConcesionDto obj) throws Exception;

    ResultClassEntity listarSolicitudConcesionCalificacionPorFiltro(SolicitudConcesionCalificacionDto obj)
            throws Exception;

    ResultArchivoEntity descargarPlantillaCriteriosCalificacionPropuesta(DescargarSolicitudConcesionDto obj)
            throws Exception;

    ResultArchivoEntity descargarPlantillaResolucionEvaluacionSolictud(DescargarResAdminFavDesfavDto obj) throws Exception;

}
