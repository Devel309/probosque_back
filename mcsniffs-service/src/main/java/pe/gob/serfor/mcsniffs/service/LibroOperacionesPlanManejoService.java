package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesPlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface LibroOperacionesPlanManejoService {

    ResultClassEntity registrarLibroOperacionesPlanManejo(List<LibroOperacionesPlanManejoDto> param) throws Exception;
    ResultClassEntity eliminarLibroOperacionesPlanManejo(LibroOperacionesPlanManejoDto dto) throws Exception;

}
