package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ProcesoOfertaDetalleService {
    ResultClassEntity guardarProcesoOfertaDetalle(ProcesoOfertaDetalleEntity obj);
}
