package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPMFIService {
    ByteArrayResource consolidadoPMFI_PDF(Integer idPlanManejo) throws Exception;
}
