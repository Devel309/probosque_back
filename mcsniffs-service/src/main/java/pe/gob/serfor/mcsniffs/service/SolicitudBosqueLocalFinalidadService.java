package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFinalidadDto;

public interface SolicitudBosqueLocalFinalidadService {
    ResultClassEntity registrarSolicitudBosqueLocalFinalidad(List<SolicitudBosqueLocalFinalidadDto> param) throws Exception;
    ResultClassEntity eliminarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto param) throws Exception;
    ResultClassEntity listarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto param) throws Exception;
    
}
