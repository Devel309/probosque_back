package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralPlanificacionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface InformacionGeneralPlanificacionBosqueService {
    ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralPlanificacionBosqueEntity param) throws Exception;
}
