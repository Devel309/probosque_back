package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.AprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.AprovechamientoService;
import pe.gob.serfor.mcsniffs.service.CensoForestalDetalleService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service("AprovechamientoService")
public class AprovechamientoServiceImpl implements AprovechamientoService {

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Override
    public ResultClassEntity RegistrarAprovechamiento(AprovechamientoEntity param) throws Exception {
        return aprovechamientoRepository.RegistrarAprovechamiento(param);
    }



    @Override
    public ResultClassEntity<List<AprovechamientoEntity>> ListarAprovechamiento(Integer idPlanManejo)
            throws Exception {

        ResultClassEntity<List<AprovechamientoEntity>> result = new ResultClassEntity<>();

        result.setData(aprovechamientoRepository.ListarAprovechamiento(idPlanManejo));
        result.setSuccess(true);
        result.setMessage("Lista Aprovechamiento");

        return result;
    }


}
