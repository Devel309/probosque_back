package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionCriterioAreaEntity;

public interface SolicitudConcesionCriterioAreaService {

    ResultClassEntity listarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception;
    ResultClassEntity registrarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception;
    ResultClassEntity actualizarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception;
    ResultClassEntity eliminarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception;
}
