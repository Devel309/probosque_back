package pe.gob.serfor.mcsniffs.service.impl;


import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.constant.PoccType;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ArchivoPDFPOCCService")
public class ArchivoPDFPOCCServiceImpl implements ArchivoPDFPOCCService {

    @Autowired
    private  SolicitudAccesoServiceImpl acceso;

    @Autowired
    private PlanManejoRepository planManejoRepository;

    @Autowired
    private PlanManejoContratoRepository planManejoContratoRepository;

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private SistemaManejoForestalRepository smfRepo;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;

    @Autowired
    private SistemaManejoForestalRepository sistema;

    @Autowired
    private ImpactoAmbientalService ambiental;

    @Autowired
    CensoForestalRepository censoForestalDetalleRepo;

    @Autowired
    ZonificacionRepository zonificacionRepository;

    @Autowired
    CoreCentralRepository corecentral;

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

    @Autowired
    private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ArchivoRepository archivoRepository;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;


    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private SistemaManejoForestalService sistemaManejoForestalService;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font cabecera= new Font(Font.HELVETICA, 9f, Font.BOLD);
    Font subTituloTabla= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font letraPeque = new Font(Font.HELVETICA, 6f, Font.COURIER);

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: Rafael Azaña [26-02-2022]
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con Apache POI}
     */

    @Override
    public ByteArrayResource consolidadoPOCC(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPOCC.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("consolidadoPOCC", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPdfPOCC(os,idPlanManejo);
        os.flush();
        os.close();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }


    public  void createPdfPOCC(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);
        try {

            PdfWriter writer = PdfWriter.getInstance(document, os);

            document.open();

            Paragraph titlePara2 = new Paragraph("PLAN OPERATIVO ANUAL (PO) PARA CONCESIONES CON FINES MADERABLES",titulo);
            titlePara2.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph(titlePara2));
            document.add(new Paragraph("\r\n\r\n"));


            document.add(new Paragraph("\t\t1. INFORMACIÓN GENERAL",subTitulo));
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable table = createTableInformacionGeneral(writer,idPlanManejo);
            document.add(table);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t2. RESUMEN DE ACTIVIDADES Y RECOMENDACIONES DEL PO ANTERIOR ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t2.1. Resumen de actividades operativas en el PO.",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table2 = createResumenActividades(writer,idPlanManejo);
            document.add(table2);
            document.add(new Paragraph("\r\n"));
            PdfPTable table3 = createResumenActividadFrenteCortaPOCC(writer,idPlanManejo);
            document.add(table3);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t2.2. Resumen de los resultados del PO ejecutado",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table4 = createResumenActividadesR(writer,idPlanManejo);
            document.add(table4);
            document.add(new Paragraph("\t\t3.\tOBJETIVOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table5 = createObjetivoEspecifico(writer,idPlanManejo);
            document.add(table5);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t4. INFORMACIÓN BASICA DE LA PARCELA DE CORTA (PC)",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t4.1\tUbicación y extensión de la PC",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t(Mapa 01  \uF078  Anexo 1)",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table6 = createInfBasicaUbicacion(writer,idPlanManejo);
            document.add(table6);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tPO con dos o más PC o frentes de corta: Si  \uF06F    No \uF078 \n" +
                    "Frente de corta 1:\n" +
                    "PC 01 \t\uF078\t\tÁrea (ha): 700.00        Coordenadas UTM            (Zona 18 WGS 84)\n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.2 Tipos de bosque",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("(Mapa 01  \uF0FE  Anexo 1)",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table7 = createInfBasicaTipoBosque(writer,idPlanManejo);
            document.add(table7);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.3 Accesibilidad",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Rutas o vías de acceso terrestre o fluvial a la PC.\n" +
                    "(Incluir en el Mapa 1 Anexo 1, y adjuntar el registro de los puntos recorridos y georreferenciados en formato digital “trackear).",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table9 = createInfoAreaAccesibilidad1PGMFA(writer,idPlanManejo);
            document.add(table9);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5.\tORDENAMIENTO DE LAS PARCELAS DE CORTA Y PROTECCION DE UMF ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5.1 Categorías de ordenamiento",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table10 = createOrdenamientoInternoPGMFA(writer,idPlanManejo);
            document.add(table10);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5.2. Frentes de Corta ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Se debe señalar si el aprovechamiento se realiza en uno o más frentes de corta. Los frentes de corta deben de delimitarse en el Mapa 3 del Anexo 1 y deberá proponerse respetando el principio de sostenibilidad del ciclo de corta. ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5.3. Protección y vigilancia de la UMF:",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table11 = createOrdenamientoProteccionPOCC(writer,idPlanManejo);
            document.add(table11);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.\tACTIVIDADES DE APROVECHAMIENTO\n" +
                    "6.1. Censo comercial\n" +
                    "\n" +
                    "6.1.1. Metodología ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("De acuerdo a la metodología que se haya utilizada describir aspectos relacionados con: tamaño y número de fajas, distanciamiento entre fajas, otros. ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.1.2. Delimitación y división interna de las parcelas de corta (PC)",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Se delimitaron los linderos de las PC:\n" +
                    "\tTrochas  \uF078\t\tPostes de madera \uF078\t\t\tLímites naturales  •\t\n" +
                    "Distanciamiento entre trochas de orientación (m)    100.00 m\n" +
                    "Subdivisión de las PC en unidades de trabajo (UT):\tSi   •\t\tNo \uF078\n" +
                    "\t\tMapa 2 \uF078\t(Anexo 1)\n" +
                    "Si se delimitaron UT: \tDimensiones (m):     Superficie:     \tNumero:\n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.1.3. Lista de especies ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Especies incluidas para el censo, con sus DMC y líneas de producción ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table12 = createActividadAprovechamientoPOCC(writer,idPlanManejo);
            document.add(table12);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.1.4. Variables e información registrada ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Las variables e información que se registren durante el censo forestal deben incluirse en el cuadro del Anexo 2.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.1.5. Marcado y limpieza de arboles ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("El marcado de los árboles se reporta según Io indicado en el cuadro del Anexo 2. Cuando se realice la corta de lianas de algún árbol (aprovechable, semillero o futura cosecha) se debe registrar en la última columna del cuadro del Anexo 2.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            Rectangle two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            document.add(new Paragraph("6.1.6. Resultados de recursos forestales maderables ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Consignar el número de árboles aprovechables (N) y volumen comercial (Vc) por especie y clase  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table13 = createActividadAprovechamientoRecursosMPOCC(writer,idPlanManejo);
            document.add(table13);
             two = new Rectangle(595.0F,842.0F);
            document.setPageSize(two);
            document.setMargins(60, 60, 40, 40);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.1.7. Aprovechamiento de recursos forestales no maderables o fauna silvestre (de ser el caso)",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Para este plan operativo, no se considera el aprovechamiento de recursos forestales no maderables o de fauna silvestre.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table14 = createActividadAprovechamientoRecursosNMPOCC(writer,idPlanManejo);
            document.add(table14);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.2. Operaciones de aprovechamiento",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Consignar información de las operaciones vinculadas con el aprovechamiento.\n" +
                    "Además, se debe mostrar el trazado de camino principal y de acceso en el Mapa 1 y Mapa 2.\n" +
                    "En el Mapa 2, además debe incluirse el trazado de caminos secundarios, patios de acopio y principales viales de arrastre.\n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table15 = createActividadAprovechamientoOperaPOCC(writer,idPlanManejo);
            document.add(table15);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.\tACTIVIDADES SILVICULTURALES",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.1. Necesidad de Intervenciones silviculturales",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Monitoreo Silvicultural: Si  •\t  No  \uF078\n" +
                    "\tEn caso que se realizó, se incluye la metodología y resultados en el Anexo 3: Si  •\tNo  \uF078\n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.2. Tratamientos silviculturales",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table16 = createActividadSilviculturalTratamientosPOCC(writer,idPlanManejo);
            document.add(table16);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.3. Enriquecimiento de áreas degradas (opcional)",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Incluye: Si  •\t  No  \uF078\n" +
                    "\tNo existen áreas degradadas dentro de la PC N° 01, por tal motivo no se realizara esta actividad.\n" +
                    "\uF046  Se amplía información en el Anexo 4: Si  \uF06F\tNo  \uF078\t\n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table17 = createActividadSilviculturalEnriqPOCC(writer,idPlanManejo);
            document.add(table17);
            document.add(new Paragraph("\r\n"));
             two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            document.add(new Paragraph("8.\tEVALUACION DE IMPACTOS AMBIENTALES",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En este ítem el concesionario debe precisar las acciones que deben ejecutarse para obtener y mantener " +
                    "la viabilidad ambiental del Plan \n" +
                    "General de Manejo Forestal, una vez puesto en marcha, con medidas apropiadas para la prevención y corrección de impactos " +
                    "ambientales.\n" +
                    "debe incluir un programa de acción preventivo corrector, programa de vigilancia y seguimiento a los impactos y de ser el caso, " +
                    "un programa de contingencias:",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.1. Identificación de impactos ambientales",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table17_ = createEvaluacionAmbientalA(writer,idPlanManejo);
            document.add(table17_);
             two = new Rectangle(595.0F,842.0F);
            document.setPageSize(two);
            document.setMargins(60, 60, 40, 40);
            document.newPage();
            document.add(new Paragraph("8.2. Plan de Acción preventivo Corrector",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table18 = createPlanAccionEvaluacionImpactoAmbiental(writer,idPlanManejo);
            document.add(table18);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.3. Plan de Vigilancia y de Seguimiento Ambiental",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table19 = createPlanVigilanciaEvaluacionImpactoAmbiental(writer,idPlanManejo);
            document.add(table19);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.4. Plan de Contingencia ambiental (de ser el caso):",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table20 = createPlanContingenciaEvaluacionImpactoAmbiental(writer,idPlanManejo);
            document.add(table20);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.\tMONITOREO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Describir las acciones del monitoreo (Actividades de Aprovechamiento, silviculturales, impactos de flora y fauna silvestres, administrativo, entre otros).",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table21 = createMonitoreoPGMFA(writer,idPlanManejo);
            document.add(table21);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("10.  PARTICIPACIÓN CIUDADANA",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En la implementación del Plan Operativo:",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table22 = createParticipacionComunalPOCC(writer,idPlanManejo);
            document.add(table22);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En los cómites de Gestión Forestal y de Fauna Silvestre:",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table222 = createParticipacionComunalPOCC2(writer,idPlanManejo);
            document.add(table222);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Plan de relacionamiento comunitario (de ser el caso)",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table223 = createParticipacionComunalPOCC3(writer,idPlanManejo);
            document.add(table223);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("11.  CAPACITACION",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Actividades de capacitación previstas para el año operativo:",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table23 = createCapacitacionPOCC(writer,idPlanManejo);
            document.add(table23);
            two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            document.add(new Paragraph("12.\tPROGRAMA DE INVERSIONES",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("12.1\tFLUJO DE CAJA",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Incluir cuadro de costos e ingresos para el periodo de duración del PO.",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("INGRESOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table24 = createRentabilidadIngresos(writer,idPlanManejo);
            document.add(table24);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("EGRESOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table25 = createRentabilidadEgresos(writer,idPlanManejo);
            document.add(table25);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("12.2. NECESIDADES DE FINANCIAMIENTO",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table252 = createRentabilidad2(writer,idPlanManejo);
            document.add(table252);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("13. CRONOGRAMA DE ACTIVIDADES",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table26 = createCronogramaActividadesPOACAnios(writer,idPlanManejo);
            document.add(table26);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }


    public  PdfPTable createTableInformacionGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        //1 - Resumen Ejecutivo
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POCC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);

        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle= new ArrayList<>();
        if(lstInformacionBasica.getData().size()>0){
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }
        String vigencia="";
        for (InformacionGeneralDetalle detalleEntity : lstInformacionBasicaDetalle) {
            vigencia=detalleEntity.getVigenciaDet().toString();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso= acceso.ListarSolicitudAcceso(param);
        if(lstacceso.size()>0){
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }



        cell = new PdfPCell(new Paragraph("Nombre deL Titular: ",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(45);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getNombreElaboraDema()!=null? o.getNombreElaboraDema():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(45);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre deL Representante Legal: ",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getRepresentanteLegal()!=null? o.getRepresentanteLegal():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Consolidado Forestal \n N° de contrato de la concesión:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(45);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getNroResolucionComunidad()!=null? o.getNroResolucionComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(45);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Número de DNI: ",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getDniElaboraDema()!=null? o.getDniElaboraDema():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Número de RUC: ",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getNroRucComunidad()!=null? o.getNroRucComunidad():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Domicilio Legal / Distrito: ",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getDistrito()!=null? o.getDistrito():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Departamento: ",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getDepartamentoRepresentante()!=null? o.getDepartamentoRepresentante():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Provincia: ",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getProvinciaRepresentante()!=null? o.getProvinciaRepresentante():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Del Plan Operativo PO",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Objetivo General del PO: ",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getDetalle()!=null? o.getDetalle():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Numero de PO: ",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getObservacion()!=null? o.getObservacion():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Fecha de presentación del PO:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Duración del PO:",contenido));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( vigencia != null ? vigencia : "",contenido));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Fecha de Inicio:",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Fecha de finalización:",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Numero de PC a las que aplica el PO:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getNroAnexosComunidad() != null ? o.getNroAnexosComunidad().toString() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área total de la PC (s) a aprovechar (ha):",contenido));
        cell.setFixedHeight(45);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getSuperficieHaMaderables() != null ? o.getSuperficieHaMaderables() : "",contenido));
        cell.setFixedHeight(45);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área de Bosque de\n" +
                "producción forestal (ha):\n",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getSuperficieHaNoMaderables() != null ? o.getSuperficieHaNoMaderables() : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área de protección (ha):",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getVolumenM3rMaderables() != null ? o.getVolumenM3rMaderables() : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Volumen total solicitado (m3):",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getVolumenM3rNoMaderables() != null ? o.getVolumenM3rNoMaderables().toString() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Volumen, cantidad, peso (kg) total de productos forestales\n" +
                "no maderables solicitado:\n",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getTotalCostoEstimado() != null ? o.getTotalCostoEstimado().toString() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Del Regente Forestal:",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre del Regente Forestal del PO:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getRegente() != null ? o.getRegente().getNombres() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Domicilio Legal:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getDireccionLegalRepresentante() != null ? o.getDireccionLegalRepresentante() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Número de registro de CIP:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getRegente() != null ? o.getRegente().getNumeroLicencia() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Contrato suscrito con el titular del título habilitante:",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getRegente() != null ? o.getRegente().getContratoSuscrito() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° inscripción en el registro de regentes que conduce el\n" +
                "SERFOR\n",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( o.getRegente() != null ? o.getRegente().getNumeroInscripcion() : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(40);
        table.addCell(cell);

        return table;
    }

    public  PdfPTable createResumenActividades(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //2 - Resumen de actividades 2
        //2.1
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POCC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen =  resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);
       if(lstResumen!=null) {
           List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream()
                   .filter(eval -> eval.getCodigoSubResumen().equals("POCCRARPOARARPO")).collect(Collectors.toList());
           if(listPGMFEVALI!=null) {
               for (ResumenActividadPoEntity resumen : listPGMFEVALI) {


                   cell = new PdfPCell(new Paragraph("Nro Resolución que aprueba el PO", contenido));
                   cell.setFixedHeight(size);
                   table.addCell(cell);
                   cell = new PdfPCell(new Paragraph(resumen.getNroResolucion() != null ? resumen.getNroResolucion().toString() : "", contenido));
                   cell.setFixedHeight(size);
                   table.addCell(cell);

                   cell = new PdfPCell(new Paragraph("Nro PC", contenido));
                   cell.setFixedHeight(size);
                   table.addCell(cell);
                   cell = new PdfPCell(new Paragraph(resumen.getNroPcs() != null ? resumen.getNroPcs().toString() : "", contenido));
                   cell.setFixedHeight(size);
                   table.addCell(cell);

                   cell = new PdfPCell(new Paragraph("Área (ha)", contenido));
                   cell.setFixedHeight(size);
                   table.addCell(cell);
                   cell = new PdfPCell(new Paragraph(resumen.getAreaHa() != null ? resumen.getAreaHa().toString() : "", contenido));
                   cell.setFixedHeight(size);
                   table.addCell(cell);

               }
           }
       }
        return table;
    }


    public  PdfPTable createResumenActividadFrenteCortaPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //2 - Resumen de actividades 2
        //2.1
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POCC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen =  resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);

        cell = new PdfPCell(new Paragraph( "Actividades",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Ejemplo de Indicadores",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Programado",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Realizado",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstResumen!=null) {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POCCRARPOARARPO")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            if(lstListaDetalle!=null) {
                for (ResumenActividadPoDetalleEntity resumen : lstListaDetalle) {

                    cell = new PdfPCell(new Paragraph(resumen.getActividad() != null ? resumen.getActividad() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph(resumen.getIndicador() != null ? resumen.getIndicador() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph(resumen.getProgramado() != null ? resumen.getProgramado() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph(resumen.getRealizado() != null ? resumen.getRealizado() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }


    public  PdfPTable createResumenActividadesR(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 30;

        //2 - Resumen de actividades 2
        //2.1
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POCC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen =  resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);

        if(lstResumen!=null) {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POCCRARPOARRPOE")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            if(lstListaDetalle!=null) {
                for (ResumenActividadPoDetalleEntity resumen : lstListaDetalle) {

                    cell = new PdfPCell(new Paragraph(resumen.getResumen() != null ? resumen.getResumen() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);


                }
            }
        }
        return table;
    }


    public  PdfPTable createObjetivoEspecifico(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        //2 - Objetivos de Manejo
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("POCC", idPlanManejo);
        List<ObjetivoDto> maderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("Productos a obtener del manejo ",cabecera));
        cell.setColspan(3);
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maderable",subTituloTabla));
        cell.setRowspan(maderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaM="";
        for(ObjetivoDto obM : maderables){
            if(obM.getActivo().equals("A")){
                marcaM="(X)";
            }else{marcaM="";}
            cell = new PdfPCell(new Paragraph(marcaM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }


        List<ObjetivoDto> NMaderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("No Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("No Maderable",subTituloTabla));
        cell.setRowspan(NMaderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaNM="";
        for(ObjetivoDto obNM : NMaderables){
            if(obNM.getActivo().equals("I")){
                marcaNM="(X)";
            }else{marcaNM="";}
            cell = new PdfPCell(new Paragraph(marcaNM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obNM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }


    public  PdfPTable createInfBasicaUbicacion(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //4 - INFORMACIÓN BASICA DE LA PARCELA DE CORTA (PC)
        //4.1  -- 4
        List<InfBasicaAereaDetalleDto> lstInfoBasicaUEPC = informacionBasicaRepository.listarInfBasicaAerea("POCC", idPlanManejo, "POCCIBPCUEPC");
        if(lstInfoBasicaUEPC!=null){
            String departamento="",provincia="",distrito="";
            for (InfBasicaAereaDetalleDto detalleEntityUbigeo : lstInfoBasicaUEPC) {
                List<InfBasicaAereaDetalleDto> lstInfoAreaUbigeo = informacionBasicaRepository.listarInfBasicaAereaUbigeo(detalleEntityUbigeo.getDepartamento(), detalleEntityUbigeo.getProvincia(), detalleEntityUbigeo.getDistrito());
                for (InfBasicaAereaDetalleDto detalleEntityUbigeo2 : lstInfoAreaUbigeo) {
                    departamento=detalleEntityUbigeo2.getDepartamento();
                    provincia=detalleEntityUbigeo2.getProvincia();
                    distrito=detalleEntityUbigeo2.getDistrito();
                }
            }

            String nroPc="",areatotal="",cuenca="",nroHoja="",nombreHoja="";

            for(InfBasicaAereaDetalleDto detalle: lstInfoBasicaUEPC){
                if(detalle.getNumeroPc()!=null || detalle.getNumeroPc().equals("")){
                    nroPc="Número de la(s) PC: "+detalle.getNumeroPc();
                }else{
                    nroPc="Número PC: ";
                }

                if(detalle.getAreaHa()!=null || detalle.getAreaHa().equals("")){
                    areatotal="Área total (ha) "+detalle.getAreaHa().toString();
                }else{
                    areatotal="Área total (ha):";
                }

                if(detalle.getNroHojaCatastral()!=null || detalle.getNroHojaCatastral().equals("")){
                    nroHoja="Nº Hoja Catastral: "+detalle.getNroHojaCatastral().toString();
                }else{
                    nroHoja="Nº Hoja Catastral: ";
                }
                if(detalle.getNombreHojaCatastral()!=null || detalle.getNombreHojaCatastral().equals("")){
                    nombreHoja="Nombre de la Hoja Catastral: "+detalle.getNombreHojaCatastral().toString();
                }else{
                    nombreHoja="Nombre de la Hoja Catastral: ";
                }

                if(detalle.getCuenca()!=null || detalle.getCuenca().equals("")){
                    cuenca="Cuenca / SubCuenca: "+detalle.getCuenca().toString();
                }else{
                    cuenca="Cuenca / SubCuenca:  ";
                }
                cell = new PdfPCell(new Paragraph(nroPc,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(areatotal,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(nroHoja,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(nombreHoja,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(distrito,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(cuenca,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }


        return table;
    }


    public  PdfPTable createInfBasicaTipoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //4.2  --6
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTIBO = informacionBasicaRepository.listarInfBasicaAerea("POCC", idPlanManejo, "POCCIBPCTIBO");

        cell = new PdfPCell(new Paragraph("Tipo de Bosque",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoBasicaTIBO!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaTIBO) {

                cell = new PdfPCell(new Paragraph(detalle.getNombre() != null ? detalle.getNombre().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHaPorcentaje() != null ? detalle.getAreaHaPorcentaje().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createInfoAreaAccesibilidad1PGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.3  --7
        List<InfBasicaAereaDetalleDto> lstInfoBasicaACCE = informacionBasicaRepository.listarInfBasicaAerea("POCC", idPlanManejo, "POCCIBPCACCE");


        cell = new PdfPCell(new Paragraph("Punto de referencia\n (carretera, rio o\n quebrada,etc) ",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distancia \n (km)",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tiempo\n (horas)",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medio de\n Transporte",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoBasicaACCE!=null){
                for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaACCE) {

                    cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getDistanciaKm() != null ? detalle.getDistanciaKm().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getTiempo() != null ? detalle.getTiempo().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMedioTransporte() != null ? detalle.getMedioTransporte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
        }
        return table;
    }


    public  PdfPTable createOrdenamientoInternoPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        float[] values = new float[4];
        values[0] = 150;
        values[1] = 50;
        values[2] = 50;
        values[3] = 50;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        //5.	ORDENAMIENTO INTERNO
        //5.1 --8
        OrdenamientoProteccionEntity ordenaRequest = new OrdenamientoProteccionEntity();
        ordenaRequest.setIdPlanManejo(idPlanManejo);
        ordenaRequest.setCodTipoOrdenamiento("POCC");
        ResultEntity<OrdenamientoProteccionEntity> lstOrdenamientoCategoria = ordenamientoRepo.ListarOrdenamientoInternoDetalle(ordenaRequest);

        cell = new PdfPCell(new Paragraph("Categoría de ordenamiento",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Area (ha)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        if(lstOrdenamientoCategoria!=null) {

            List<OrdenamientoProteccionDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (lstOrdenamientoCategoria.getData().size() > 0) {
                OrdenamientoProteccionEntity PGMFEVALI = lstOrdenamientoCategoria.getData().get(0);
                lstListaDetalle = PGMFEVALI.getListOrdenamientoProteccionDet();
            }
            if(lstListaDetalle!=null) {
                List<OrdenamientoProteccionDetalleEntity> lstListaCate = lstListaDetalle.stream()
                        .filter(eval -> eval.getCodigoTipoOrdenamientoDet().equals("POCCOPCPUMFCO"))
                        .collect(Collectors.toList());
                if(lstListaCate!=null) {
                    for (OrdenamientoProteccionDetalleEntity detalle : lstListaCate) {

                        cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph( detalle.getAccion() != false ? "X" : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getAreaHA() != null ? detalle.getAreaHA().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getAreaHAPorcentaje() != null ? detalle.getAreaHAPorcentaje().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createOrdenamientoProteccionPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //5.	ORDENAMIENTO INTERNO
        //5.1 --8
        OrdenamientoProteccionEntity ordenaRequest = new OrdenamientoProteccionEntity();
        ordenaRequest.setIdPlanManejo(idPlanManejo);
        ordenaRequest.setCodTipoOrdenamiento("POCC");
        ResultEntity<OrdenamientoProteccionEntity> lstOrdenamientoCategoria = ordenamientoRepo.ListarOrdenamientoInternoDetalle(ordenaRequest);

        cell = new PdfPCell(new Paragraph("Actividades propuestas en el PGMF",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Metas durante la ejecución del presente PO",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        if(lstOrdenamientoCategoria!=null) {

            List<OrdenamientoProteccionDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (lstOrdenamientoCategoria.getData().size() > 0) {
                OrdenamientoProteccionEntity PGMFEVALI = lstOrdenamientoCategoria.getData().get(0);
                lstListaDetalle = PGMFEVALI.getListOrdenamientoProteccionDet();
            }
            if(lstListaDetalle!=null) {
                List<OrdenamientoProteccionDetalleEntity> lstListaCate = lstListaDetalle.stream()
                        .filter(eval -> eval.getCodigoTipoOrdenamientoDet().equals("POCCOPCPUMFPV"))
                        .collect(Collectors.toList());
                if(lstListaCate!=null) {
                    for (OrdenamientoProteccionDetalleEntity detalle : lstListaCate) {

                        cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createActividadAprovechamientoPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(90);
        float[] values = new float[6];
        values[0] = 30;
        values[1] = 120;
        values[2] = 120;
        values[3] = 120;
        values[4] = 120;
        values[5] = 40;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        //6.1.3 --10
        ActividadAprovechamientoParam aprovechamiento = new ActividadAprovechamientoParam();
        aprovechamiento.setIdPlanManejo(idPlanManejo);
        aprovechamiento.setCodActvAprove("POCC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(aprovechamiento);


        cell = new PdfPCell(new Paragraph("N°",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre Común",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre Científico",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Familia",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Linea de \nProducción",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DMC \n(cm)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

int contador=1;
        if(lstAprovechamiento!=null) {

            List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROCC")).collect(Collectors.toList());

            if(lstListaFil!=null) {
                List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstListaFil.size() > 0) {
                    ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                    lstListaDetalle = PGMFEVALI.getLstactividadDet();
                }
                if(lstListaDetalle!=null) {
                    List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                            .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROOALE")).collect(Collectors.toList());
                    if(lstListaDet!=null) {
                        for (ActividadAprovechamientoDetEntity detalle : lstListaDet) {
                            cell = new PdfPCell(new Paragraph(contador+"", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getNombreComun() != null ? detalle.getNombreComun().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getNombreCientifica() != null ? detalle.getNombreCientifica().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getFamilia() != null ? detalle.getFamilia().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getLineaProduccion() != null ? detalle.getLineaProduccion().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getDcm() != null ? detalle.getDcm().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            contador++;
                        }
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createActividadAprovechamientoRecursosMPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(11);
        table.setWidthPercentage(90);
        float[] values = new float[11];
        values[0] = 120;
        values[1] = 50;
        values[2] = 40;
        values[3] = 40;
        values[4] = 40;
        values[5] = 40;
        values[6] = 40;
        values[7] = 40;
        values[8] = 40;
        values[9] = 50;
        values[10] = 50;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        //6.1.3 --10
        ActividadAprovechamientoParam aprovechamiento = new ActividadAprovechamientoParam();
        aprovechamiento.setIdPlanManejo(idPlanManejo);
        aprovechamiento.setCodActvAprove("POCC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(aprovechamiento);


        cell = new PdfPCell(new Paragraph("Especie",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Var",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("CLASE DIAMETRICA",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(7);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Total\n" +
                "en PC\n",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total \npor\n ha",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("30a40",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("40a50",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("50a60",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("60a70",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("70a80",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("80a90",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("90a+",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;
        if(lstAprovechamiento!=null) {

            List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROCC")).collect(Collectors.toList());

            if (lstListaFil != null) {
                List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstListaFil.size() > 0) {
                    ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                    lstListaDetalle = PGMFEVALI.getLstactividadDet();
                }
                if (lstListaDetalle != null) {
                    List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                            .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROOARR")).collect(Collectors.toList());

                    for (ActividadAprovechamientoDetEntity detalle : lstListaDet) {
                        cell = new PdfPCell(new Paragraph(detalle.getNombreCientifica() != null ? detalle.getNombreCientifica().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        if (lstListaDet != null) {
                            List<ActividadAprovechamientoDetSubEntity> lstListaDetalleSub = new ArrayList<>();
                            if (lstListaDet.size() > 0) {
                                ActividadAprovechamientoDetEntity PGMFEVALIDET = lstListaDet.get(i);
                                lstListaDetalleSub = PGMFEVALIDET.getLstactividadDetSub();
                            }
                            if (lstListaDetalleSub != null) {
                                for (ActividadAprovechamientoDetSubEntity detalle2 : lstListaDetalleSub) {

                                    cell = new PdfPCell(new Paragraph(detalle2.getVar() != null ? detalle2.getVar().toString() : "", contenido));
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("30-40") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("40-50") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("50-60") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("60-70") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("70-80") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("80-90") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    if (detalle2.getDapCM() != null || detalle2.getDapCM().equals("")) {
                                        cell = new PdfPCell(new Paragraph(detalle2.getDapCM().equals("90+") ? "X" : "", contenido));
                                    } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                    }
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    cell = new PdfPCell(new Paragraph(detalle2.getTotalHa() != null ? detalle2.getTotalHa().toString() : "", contenido));
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                    cell = new PdfPCell(new Paragraph(detalle2.getTotalPC() != null ? detalle2.getTotalPC().toString() : "", contenido));
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);

                                }
                            }
                        }
                        i++;
                    }
                }
            }
        }
        return table;
    }

    public  PdfPTable createActividadAprovechamientoRecursosNMPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //6.1.3 --10
        ActividadAprovechamientoParam aprovechamiento = new ActividadAprovechamientoParam();
        aprovechamiento.setIdPlanManejo(idPlanManejo);
        aprovechamiento.setCodActvAprove("POCC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(aprovechamiento);


        cell = new PdfPCell(new Paragraph("Especie:",cabecera));
        cell.setFixedHeight(40);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Producto a aprovechar:",cabecera));
        cell.setFixedHeight(40);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Sector",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área (ha)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("N° Total \nIndividuos",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Individuos (ha)",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Volumen, peso o\nnúmero\naprovechable",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);

        int contador=1;
        if(lstAprovechamiento!=null) {

            List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROCC")).collect(Collectors.toList());

            if(lstListaFil!=null) {
                List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstListaFil.size() > 0) {
                    ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                    lstListaDetalle = PGMFEVALI.getLstactividadDet();
                }
                if(lstListaDetalle!=null) {
                    List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                            .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROCCAR")).collect(Collectors.toList());
                    if(lstListaDet!=null) {
                        for (ActividadAprovechamientoDetEntity detalle : lstListaDet) {
                            cell = new PdfPCell(new Paragraph(detalle.getSector() != null ? detalle.getSector().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getArea() != null ? detalle.getArea().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getTotalIndividuos() != null ? detalle.getTotalIndividuos().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getIndividuosHA() != null ? detalle.getIndividuosHA().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getVolumen() != null ? detalle.getVolumen().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            contador++;
                        }
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createActividadAprovechamientoOperaPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        float[] values = new float[5];
        values[0] = 110;
        values[1] = 60;
        values[2] = 110;
        values[3] = 110;
        values[4] = 110;
        table.setWidths(values);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //6.1.3 --10
        ActividadAprovechamientoParam aprovechamiento = new ActividadAprovechamientoParam();
        aprovechamiento.setIdPlanManejo(idPlanManejo);
        aprovechamiento.setCodActvAprove("POCC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(aprovechamiento);

        cell = new PdfPCell(new Paragraph("Operaciones",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Unidad",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Características técnicas \ny planificación",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal \n(por función)",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maquinarias,equipos\ny materiales",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);

        int contador=1;
        if(lstAprovechamiento!=null) {

            List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROOA")).collect(Collectors.toList());

            if(lstListaFil!=null) {
                List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstListaFil.size() > 0) {
                    ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                    lstListaDetalle = PGMFEVALI.getLstactividadDet();
                }
                if(lstListaDetalle!=null) {
                    List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                            .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROOA")).collect(Collectors.toList());
                    if(lstListaDet!=null) {
                        for (ActividadAprovechamientoDetEntity detalle : lstListaDet) {
                            cell = new PdfPCell(new Paragraph(detalle.getOperaciones() != null ? detalle.getOperaciones().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getUnidad() != null ? detalle.getUnidad().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getCarcteristicaTec() != null ? detalle.getCarcteristicaTec().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getPersonal() != null ? detalle.getPersonal().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getMaquinariaMaterial() != null ? detalle.getMaquinariaMaterial().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            contador++;
                        }
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createActividadSilviculturalTratamientosPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //7.2 --15
        ActividadSilviculturalEntity act= new ActividadSilviculturalEntity();
        act.setIdPlanManejo(idPlanManejo);
        act.setCodigoTipoActSilvicultural("POCC");
        List<ActividadSilviculturalEntity> lstActSilvicultural2= actividadSilviculturalRepository.ListarActSilviCulturalDetalle(act);

        cell = new PdfPCell(new Paragraph("Localización del Área a tratar silviculturalmente",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Parcela de corta N° 01",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstActSilvicultural2!=null) {

            List<ActividadSilviculturalEntity> lstListaFil = lstActSilvicultural2.stream()
                    .filter(eval -> eval.getMonitoreo().equals("B")).collect(Collectors.toList());

            if(lstListaFil!=null) {
                List<ActividadSilviculturalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstListaFil.size() > 0) {
                    ActividadSilviculturalEntity PGMFEVALI = lstListaFil.get(0);
                    lstListaDetalle = PGMFEVALI.getListActividadSilvicultural();
                }
                if(lstListaDetalle!=null) {
                        for (ActividadSilviculturalDetalleEntity detalle : lstListaDetalle) {
                            cell = new PdfPCell(new Paragraph(detalle.getTratamiento() != null ? detalle.getTratamiento().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                }
            }
        }

        return table;
    }


    public  PdfPTable createActividadSilviculturalEnriqPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //7.2 --15
        ActividadSilviculturalEntity act= new ActividadSilviculturalEntity();
        act.setIdPlanManejo(idPlanManejo);
        act.setCodigoTipoActSilvicultural("POCC");
        List<ActividadSilviculturalEntity> lstActSilvicultural2= actividadSilviculturalRepository.ListarActSilviCulturalDetalle(act);

        if(lstActSilvicultural2!=null) {

            List<ActividadSilviculturalEntity> lstListaFil = lstActSilvicultural2.stream()
                    .filter(eval -> eval.getMonitoreo().equals("C")).collect(Collectors.toList());

            if(lstListaFil!=null) {
                List<ActividadSilviculturalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstListaFil.size() > 0) {
                    ActividadSilviculturalEntity PGMFEVALI = lstListaFil.get(0);
                    lstListaDetalle = PGMFEVALI.getListActividadSilvicultural();
                }

                if(lstListaDetalle!=null) {
                    for (ActividadSilviculturalDetalleEntity detalle : lstListaDetalle) {
                        cell = new PdfPCell(new Paragraph(detalle.getTratamiento() != null ? detalle.getTratamiento().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createEvaluacionAmbientalA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        EvaluacionAmbientalActividadEntity actividadEntity = new EvaluacionAmbientalActividadEntity();
        actividadEntity.setIdPlanManejo(idPlanManejo);
        actividadEntity.setTipoActividad("FACTOR");
        List<EvaluacionAmbientalActividadEntity> lista = evaluacionAmbientalRepository.ListarEvaluacionActividadFiltro(actividadEntity);
        String actividad="";
        int cantAprove=0,preApro=0,aprove=0,postAprov=0;
        PdfPTable table =null;
        if(lista!=null){
            EvaluacionAmbientalAprovechamientoEntity aprovechamientoEntity= new EvaluacionAmbientalAprovechamientoEntity();
            aprovechamientoEntity.setIdPlanManejo(idPlanManejo);
            aprovechamientoEntity.setTipoAprovechamiento("");
            List<EvaluacionAmbientalAprovechamientoEntity> listaAprovecha = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(aprovechamientoEntity);
            if(listaAprovecha.size()>0){

                List<EvaluacionAmbientalAprovechamientoEntity> lstPreApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("PREAPR")).collect(Collectors.toList());
                if(lstPreApro.size()==0){
                    cantAprove++;
                }else{
                    cantAprove+=lstPreApro.size();
                }
                List<EvaluacionAmbientalAprovechamientoEntity> lstApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("APROVE")).collect(Collectors.toList());
                if(lstApro.size()==0){
                    cantAprove++;
                }else{
                    cantAprove+=lstApro.size();
                }
                List<EvaluacionAmbientalAprovechamientoEntity> lstPostApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("POSTAP")).collect(Collectors.toList());
                if(lstPostApro.size()==0){
                    cantAprove++;
                }else{
                    cantAprove+=lstPostApro.size();
                }
            }


            table = new PdfPTable(cantAprove+3);
            PdfPCell cell;
            float[] values = new float[cantAprove+3];
            for (int j = 0; j < values.length; j++) {
                if (j == 0 ) {
                    values[j] = 80;
                }else if(j>0 && j<values.length-2) {
                    values[j] = 60;
                }else  {
                    values[j] = 40;
                }
            }

            cell = new PdfPCell(new Paragraph("IDENTIFICACIÓN DE IMPACTOS AMBIENTALES",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(cantAprove+3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Factores \n ambientales",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Caracterización \nde impactos",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Actividades",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(cantAprove);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Medidas \nde Mitigación",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);

            List<EvaluacionAmbientalAprovechamientoEntity> lstPreApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("PREAPR")).collect(Collectors.toList());
            List<EvaluacionAmbientalAprovechamientoEntity> lstApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("APROVE")).collect(Collectors.toList());
            List<EvaluacionAmbientalAprovechamientoEntity> lstPostApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("POSTAP")).collect(Collectors.toList());
            if(lstPreApro.size()>0){
                cell = new PdfPCell(new Paragraph("Pre \nAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(lstPreApro.size());
                table.addCell(cell);
            }else{
                cell = new PdfPCell(new Paragraph("Pre \nAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(1);
                table.addCell(cell);
            }

            if(lstApro.size()>0){
                cell = new PdfPCell(new Paragraph("Aprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(lstApro.size());
                table.addCell(cell);
            }else{
                cell = new PdfPCell(new Paragraph("Aprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(1);
                table.addCell(cell);
            }

            if(lstPostApro.size()>0){
                cell = new PdfPCell(new Paragraph("Post \nAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(lstPostApro.size());
                table.addCell(cell);
            }else{
                cell = new PdfPCell(new Paragraph("Post \nAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(1);
                table.addCell(cell);
            }


            if(lstPreApro.size()>0){
                for(EvaluacionAmbientalAprovechamientoEntity detalle1:lstPreApro){
                    cell = new PdfPCell(new Paragraph(detalle1.getNombreAprovechamiento() != null ? detalle1.getNombreAprovechamiento().toString() : "",cabecera));
                    cell.setFixedHeight(40);
                    table.addCell(cell);
                }
            }else{
                cell = new PdfPCell(new Paragraph("",cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
            }

            if(lstApro.size()>0){
                for(EvaluacionAmbientalAprovechamientoEntity detalle1:lstApro){
                    cell = new PdfPCell(new Paragraph(detalle1.getNombreAprovechamiento() != null ? detalle1.getNombreAprovechamiento().toString() : "",cabecera));
                    cell.setFixedHeight(40);
                    table.addCell(cell);
                }
            }else{
                cell = new PdfPCell(new Paragraph("",cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
            }

            if(lstPostApro.size()>0){
                for(EvaluacionAmbientalAprovechamientoEntity detalle1:lstPostApro){
                    cell = new PdfPCell(new Paragraph(detalle1.getNombreAprovechamiento() != null ? detalle1.getNombreAprovechamiento().toString() : "",cabecera));
                    cell.setFixedHeight(40);
                    table.addCell(cell);
                }
            }else{
                cell = new PdfPCell(new Paragraph("",cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
            }

            /*********************************************************** CONTENIDO  **********************************************************************************/



        }else{
            table = new PdfPTable(6);
            PdfPCell cell;
            float[] values = new float[6];
            for (int j = 0; j < values.length; j++) {
                if (j == 0 ) {
                    values[j] = 80;
                }else if(j>0 && j<values.length-2) {
                    values[j] = 60;
                }else  {
                    values[j] = 40;
                }
            }

            cell = new PdfPCell(new Paragraph("IDENTIFICACIÓN DE IMPACTOS AMBIENTALES",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(6);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Factores \n ambientales",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Caracterización \nde impactos",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Actividades",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(cantAprove);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Medidas \nde Mitigación",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Pre \nAprovechamiento",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Aprovechamiento",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Post \nAprovechamiento",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
        }

        return table;

    }

    public  PdfPTable createPlanAccionEvaluacionImpactoAmbiental(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph("Acción preventivo corrector",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del Impacto",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de control ambiental",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //8.2 Plan de Accion Preventivo-Corrector 18 POAPRO
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        if(lstEvaluacion!=null){
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion = lstEvaluacion.stream()
                    .filter(p->p.getCodTipoAprovechamiento().equals("POAPRO")).collect(Collectors.toList());

            if(lstPlanAccion!=null) {
                for (EvaluacionAmbientalAprovechamientoEntity detalle : lstPlanAccion) {
                    cell = new PdfPCell(new Paragraph(detalle.getNombreAprovechamiento() != null ? detalle.getNombreAprovechamiento().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMedidasControl() != null ? detalle.getMedidasControl().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }
        return table;
    }


    public  PdfPTable createPlanVigilanciaEvaluacionImpactoAmbiental(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Descripción de \nlos Impactos", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de control\n ambiental", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de\n monitoreo", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Frecuencia", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsable",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);

        //8.2 Plan de Accion Preventivo-Corrector 18 POAPRO
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        if(lstEvaluacion!=null){
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion = lstEvaluacion.stream()
                    .filter(p->p.getCodTipoAprovechamiento().equals("POAPRO")).collect(Collectors.toList());

            if(lstPlanAccion!=null) {
                //8.3 Plan de Vigilancia y Seguimiento Ambiental 19 POAPRO
                List<EvaluacionAmbientalAprovechamientoEntity> lstPlanVigilancia = lstPlanAccion.stream()
                        .filter(p-> !p.getImpacto().trim().equals("")).collect(Collectors.toList());
                if(lstPlanVigilancia!=null) {
                    for (EvaluacionAmbientalAprovechamientoEntity detalle : lstPlanVigilancia) {
                        cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMedidasControl() != null ? detalle.getMedidasControl().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMedidasMonitoreo() != null ? detalle.getMedidasMonitoreo().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getFrecuencia() != null ? detalle.getFrecuencia().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createPlanContingenciaEvaluacionImpactoAmbiental(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Plan de Contingencia ambiental", cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("CONTINGENCIA", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("ACCIONES A REALIZAR", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("RESPONSABLE", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //8.2 Plan de Accion Preventivo-Corrector 18 POAPRO
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        if(lstEvaluacion!=null){
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanContingencia = lstEvaluacion.stream()
                    .filter(p->p.getCodTipoAprovechamiento().equals("POEAPR")).collect(Collectors.toList());

                if(lstPlanContingencia!=null) {
                    for (EvaluacionAmbientalAprovechamientoEntity detalle : lstPlanContingencia) {
                        cell = new PdfPCell(new Paragraph(detalle.getContingencia() != null ? detalle.getContingencia().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getAcciones() != null ? detalle.getAcciones().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getResponsableSecundario() != null ? detalle.getResponsableSecundario().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
        }
        return table;
    }


    public  PdfPTable createMonitoreoPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //9  Monitoreo 21
        List<MonitoreoEntity> lstMonitoreoPOCC = monitoreoRepository.listarMonitoreoPOCC(idPlanManejo,"POCC");

        cell = new PdfPCell(new Paragraph("Monitoreo",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Operaciones",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Indicador",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Frecuencia",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsable",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        String operaciones = "";
        List<MonitoreoDetalleEntity> lstListaDetalle = new ArrayList<>();
        if(lstMonitoreoPOCC!=null) {
            for (MonitoreoEntity detalle : lstMonitoreoPOCC) {

                cell = new PdfPCell(new Paragraph(detalle.getMonitoreo() != null ? detalle.getMonitoreo().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<MonitoreoEntity> lstListaCate = lstMonitoreoPOCC.stream()
                        .filter(eval -> eval.getIdMonitoreo() == detalle.getIdMonitoreo())
                        .collect(Collectors.toList());
                if(lstListaCate!=null) {
                    if (lstListaCate.size() > 0) {
                        MonitoreoEntity PGMFEVALI = lstListaCate.get(0);
                        lstListaDetalle = PGMFEVALI.getLstDetalle();
                    }
                    operaciones = "";
                    if (lstListaDetalle.size() > 0) {
                        for (MonitoreoDetalleEntity detalleEntity2 : lstListaDetalle) {
                            operaciones += "* "+detalleEntity2.getOperacion() + "\n";
                        }
                    }
                }

                cell = new PdfPCell(new Paragraph(operaciones, contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getIndicador() != null ? detalle.getIndicador().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getFrecuencia() != null ? detalle.getFrecuencia().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }
        return table;
    }


    public  PdfPTable createParticipacionComunalPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //10 Participación ciudadana 22 deberia ser 10.3
        ParticipacionComunalParamEntity participacion = new ParticipacionComunalParamEntity();
        participacion.setIdPlanManejo(idPlanManejo);
        participacion.setCodTipoPartComunal("POCCPO");
        ResultEntity<ParticipacionComunalEntity> lstParticipacionComunal = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(participacion);

        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstParticipacionComunal.getData()!=null) {
            if(lstParticipacionComunal!=null) {
                List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstParticipacionComunal.getData().size() > 0) {
                    ParticipacionComunalEntity PGMFEVALI = lstParticipacionComunal.getData().get(0);
                    lstListaDetalle = PGMFEVALI.getLstDetalle();
                }
                if(lstListaDetalle!=null) {
                    for (ParticipacionComunalDetEntity detalle : lstListaDetalle) {
                        cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }



    public  PdfPTable createParticipacionComunalPOCC2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //10 Participación ciudadana 22 deberia ser 10.3
        ParticipacionComunalParamEntity participacion = new ParticipacionComunalParamEntity();
        participacion.setIdPlanManejo(idPlanManejo);
        participacion.setCodTipoPartComunal("POCCGF");
        ResultEntity<ParticipacionComunalEntity> lstParticipacionComunal = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(participacion);

        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstParticipacionComunal.getData()!=null) {
            if(lstParticipacionComunal!=null) {
                List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstParticipacionComunal.getData().size() > 0) {
                    ParticipacionComunalEntity PGMFEVALI = lstParticipacionComunal.getData().get(0);
                    lstListaDetalle = PGMFEVALI.getLstDetalle();
                }
                if(lstListaDetalle!=null) {
                    for (ParticipacionComunalDetEntity detalle : lstListaDetalle) {
                        cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }



    public  PdfPTable createParticipacionComunalPOCC3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //10 Participación ciudadana 22 deberia ser 10.3
        ParticipacionComunalParamEntity participacion = new ParticipacionComunalParamEntity();
        participacion.setIdPlanManejo(idPlanManejo);
        participacion.setCodTipoPartComunal("POCCPC");
        ResultEntity<ParticipacionComunalEntity> lstParticipacionComunal = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(participacion);

        cell = new PdfPCell(new Paragraph("Mecanismos \nde participación",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Metodología",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Lugar",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Fecha",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);

        if(lstParticipacionComunal.getData()!=null) {
            if(lstParticipacionComunal!=null) {
                List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
                if (lstParticipacionComunal.getData().size() > 0) {
                    ParticipacionComunalEntity PGMFEVALI = lstParticipacionComunal.getData().get(0);
                    lstListaDetalle = PGMFEVALI.getLstDetalle();
                }
                if(lstListaDetalle!=null) {
                    for (ParticipacionComunalDetEntity detalle : lstListaDetalle) {
                        cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMetodologia() != null ? detalle.getMetodologia().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getLugar() != null ? detalle.getLugar().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getFecha() != null ? formatter.format(detalle.getFecha()): "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createCapacitacionPOCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);
        float[] values = new float[7];
        values[0] = 30;
        values[1] = 110;
        values[2] = 110;
        values[3] = 110;
        values[4] = 110;
        values[5] = 110;
        values[6] = 110;
        table.setWidths(values);
        PdfPCell cell;
        int size = 20;

        //11 CAPACITACION 23
        CapacitacionDto capacitacion = new CapacitacionDto();
        capacitacion.setIdPlanManejo(idPlanManejo);
        capacitacion.setCodTipoCapacitacion("POCC");
        List<CapacitacionDto> lstCapacitacion = capacitacionRepository.ListarCapacitacion(capacitacion);

        cell = new PdfPCell(new Paragraph("Item", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Temas o actividades", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal a capacitar", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modalidad de capacitación  ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Lugar", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Periodo", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsable", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

    int contador=1;
        String temas="";
        if(lstCapacitacion!=null) {
            for (CapacitacionDto detalle : lstCapacitacion) {

                for (CapacitacionDetalleEntity t : detalle.getListCapacitacionDetalle()) {
                    temas += t.getActividad() != null ? "* "+t.getActividad() + "\n" : "";
                }
                cell = new PdfPCell(new Paragraph(temas, contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(contador+"", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getPersonaCapacitar() != null ? detalle.getPersonaCapacitar().toString() : "", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getIdTipoModalidad() != null ? detalle.getIdTipoModalidad().toString() : "", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getLugar() != null ? detalle.getLugar().toString() : "", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getPeriodo() != null ? detalle.getPeriodo().toString() : "", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);

                contador++;
            }
        }

        return table;
    }



    public  PdfPTable createRentabilidad2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;
        int i=0;
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POCC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();

        cell = new PdfPCell(new Paragraph("Lista de Necesidades",cabecera));
        cell.setFixedHeight(25);
        table.addCell(cell);

        if(lstRMF!=null) {

            List<RentabilidadManejoForestalDto> lstIngresos = lstRMF.stream().filter(p -> p.getIdTipoRubro() == 3).collect(Collectors.toList());
            if(lstIngresos!=null) {
                for (RentabilidadManejoForestalDto renta : lstIngresos) {
                    cell = new PdfPCell(new Paragraph(renta.getDescripcion() != null ? renta.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                }
            }
        }
        return table;
    }

    public  PdfPTable createRentabilidadIngresos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);// Genera una tabla de dos columnas
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] =80;
            }
            else{
                values[i] =15;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);


        Paragraph titlePara2 = new Paragraph("RUBRO",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.isUseAscender() ;
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("Año 1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("Año 2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("Año 3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POCC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        if(lstRMF!=null){

            List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream().filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());


            for(RentabilidadManejoForestalDto renta : lstIngresos) {
                cell = new PdfPCell(new Paragraph(renta.getRubro() != null ? renta.getRubro() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }

                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle = lstListaDetalle.stream().filter(p -> p.getAnio() == 1).collect(Collectors.toList());

                if (lstIngresosDetalle != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }


                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2).collect(Collectors.toList());

                if (lstIngresosDetalle2 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3).collect(Collectors.toList());

                if (lstIngresosDetalle3 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }
        }


        return table;
    }


    public  PdfPTable createRentabilidadEgresos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);// Genera una tabla de dos columnas
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] =80;
            }
            else{
                values[i] =15;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);


        Paragraph titlePara2 = new Paragraph("RUBRO",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(2);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("Año 1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("Año 2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("Año 3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;

        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POCC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();

        if(lstRMF!=null){

            List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream().filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());

            for(RentabilidadManejoForestalDto renta : lstIngresos) {
                cell = new PdfPCell(new Paragraph(renta.getRubro()!=null?renta.getRubro():"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);


                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }



                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle= lstListaDetalle.stream().filter(p-> p.getAnio()==1).collect(Collectors.toList());

                if (lstIngresosDetalle != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle2= lstListaDetalle.stream().filter(p-> p.getAnio()==2).collect(Collectors.toList());

                if (lstIngresosDetalle2 != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }

                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3).collect(Collectors.toList());

                if (lstIngresosDetalle3 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }


        }


        return table;
    }


    public  PdfPTable createCronogramaActividadesPOACAnios(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(15);
        table.setWidthPercentage(90);
        PdfPCell cell;
        float[] values = new float[15];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] = 80;
            }else{
                values[i] = 15;
            }
        }

        table.setWidths(values);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        Paragraph titlePara1 = new Paragraph("FORMULACIÓN PLAN OPERATIVO EN BOSQUES DE LA COMUNIDAD NATIVA SAN JUAN DE INUYA CON FINES DE COMERCIALIZACIÓN A ALTA ESCALA",cabecera);
        titlePara1.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara1);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(37);
        cell.setFixedHeight(30);
        table.addCell(cell);

        Paragraph titlePara2 = new Paragraph("Actividad",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(3);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara3 = new Paragraph("Años",cabecera);
        titlePara3.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara3);
        cell.setColspan(14);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara5 = new Paragraph("2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara6 = new Paragraph("3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);




        //13. Cronograma de Actividades 36
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("POCC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();


        String marca="";
        if(lstcrono!=null) {
            for (CronogramaActividadEntity detalle : lstcrono) {
                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                List<CronogramaActividadDetalleEntity> lstDetalle = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                if (lstDetalle != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                if (lstDetalle2 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                if (lstDetalle3 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }


    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }

}
