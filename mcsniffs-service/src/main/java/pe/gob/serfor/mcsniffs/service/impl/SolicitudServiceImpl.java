package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;
import pe.gob.serfor.mcsniffs.entity.Enum;
import pe.gob.serfor.mcsniffs.repository.CoreCentralRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudRepository;
import pe.gob.serfor.mcsniffs.service.CoreCentralService;
import pe.gob.serfor.mcsniffs.service.ProcesoPostulacionService;
import pe.gob.serfor.mcsniffs.service.SolicitudService;

import java.util.List;

/**
* @autor: Ivan Minaya [09-06-2021]
* @modificado:
* @descripción: {Clase de servicio relacionada a la entidad  TipoConcesion de la base de datos maestra CoreCentral}
*
*/
@Service("SolicitudService")
public class SolicitudServiceImpl implements SolicitudService {


    private static final Logger log = LogManager.getLogger(SolicitudServiceImpl.class);

	@Autowired
	private SolicitudRepository repository;
	@Autowired
	private ProcesoPostulacionRepository procesoPostulacionrepository;
	/**
	 * @autor: IVAN MINAYA [09-06-2021]
	 * @descripción: {Lista por filtro UnidadMedida}
	 * @param: UnidadMedidaEntity
	 */
	@Override
	public ResponseEntity ListarPorFiltroSolicitud() throws Exception {
		return repository.ListarPorFiltroSolicitud();
	}


	/**
	 * @autor: JaquelineDB [02-07-2021]
	 * @modificado:
	 * @descripción: {Registrar solicitud de ampliacion de plazo de entrega de pruebas de publicacion}
	 * @param:file
	 */

	@Override
	public ResultClassEntity ActualizarSolicitudAmpliacion(SolicitudDto obj) {
	return repository.RegistrarSolicitudAmpliacion(obj);
	}
	@Override
	public ResultClassEntity RegistrarSolicitudAmpliacion(SolicitudDto obj) {
		ProcesoPostulacionDto dato = new ProcesoPostulacionDto();
		dato.setIdProcesoPostulacion(obj.getIdProcesoPostulacion());
		ResultClassEntity result= new ResultClassEntity();
		ResultClassEntity response= new ResultClassEntity();
		 response=	procesoPostulacionrepository.obtenerProcesoPostulacion(dato);
		if(response.getSuccess())
		{
			ProcesoPostulacionEntity valid = (ProcesoPostulacionEntity) response.getData();
			if(valid.getIdProcesoPostulacion()==null){
				response.setMessage("El ID Proceso Postulación no se encuentra registrado.");
				response.setValidateBusiness(false);
				return response;
			}
			else{
				response=ObtenerSolicitudAmpliacion(obj);

                 if(response.getSuccess()){

					 SolicitudDto validSolicitud = (SolicitudDto) response.getData();
					 if(validSolicitud.getIdSolicitud()==null ){
						 switch (obj.getIdTipoSolicitud()) {
							 case 3:
								 result=	repository.RegistrarSolicitudAmpliacion(obj);
								 result.setValidateBusiness(true);
								 break;
							 case 4:
								 if(valid.getAutorizacionExploracion()==null)
								 {
									 result=	repository.RegistrarSolicitudAmpliacion(obj);
									 result.setValidateBusiness(true);
								 }
								 else{
									 response.setMessage("El ID Proceso Postulación no esta vigente para la autorización de exploración.");
									 response.setValidateBusiness(false);
								 }
								 break;
						 }

						 return result;
					 }
					 else{
						 response.setMessage("El ID Proceso Postulación ya se encuentra registrado.");
						 response.setValidateBusiness(false);
						 return response;
					 }



				 }
                 else{
                 	return response;
				 }

			}

		}

		else{

			return response;
		}
	}

	/**
	 * @autor: JaquelineDB [07-07-2021]
	 * @modificado:
	 * @descripción: {Relaciona los archivos adjuntos con la solicitud}
	 * @param:SolicitudRequestEntity
	 */
	@Override
	public ResultClassEntity RegistrarDocumentoAdjunto(SolicitudEntity obj) {
		return repository.RegistrarDocumentoAdjunto(obj);
	}

	/**
	 * @autor: JaquelineDB [07-07-2021]
	 * @modificado:
	 * @descripción: {Obtener archivos de las solicitudes}
	 * @param:IdSolicitud
	 */
	@Override
	public ResultEntity<SolicitudAdjuntosEntity> ObtenerArchivosSolicitudes(Integer IdSolicitud) {
		return repository.ObtenerArchivosSolicitudes(IdSolicitud);
	}

	/**
	 * @autor: JaquelineDB [03-07-2021]
	 * @modificado:
	 * @descripción: {Aprobar o rechazar las solicitudes de ampliacion de plazos}
	 * @param:file
	 */
	@Override
	public ResultClassEntity AprobarRechazarSolicitud(SolicitudDto obj) {
		return repository.AprobarRechazarSolicitud(obj);
	}

	/**
	 * @autor: JaquelineDB [03-07-2021]
	 * @modificado:
	 * @descripción: {Listar las solicitudes de ampliacion}
	 * @param:file
	 */
	@Override
	public ResultClassEntity listarSolicitudesAmpliacion(SolicitudDto param) {
		return repository.listarSolicitudesAmpliacion(param);
	}

	/**
	 * @autor: JaquelineDB [03-07-2021]
	 * @modificado:
	 * @descripción: {Obtener una solicitud de ampliacion}
	 * @param:file
	 */
	@Override
	public ResultClassEntity<SolicitudDto> ObtenerSolicitudAmpliacion(SolicitudDto request) {
		return repository.ObtenerSolicitudAmpliacion(request);
	}

	/**
	 * @autor: JaquelineDB [05-07-2021]
	 * @modificado:
	 * @descripción: {Listar tipo solicitud}
	 * @param:file
	 */
	@Override
	public ResultEntity<TipoSolicitudEntity> ListarTipoSolicitud() {
		return repository.ListarTipoSolicitud();
	}

	/**
	 * @autor: JaquelineDB [05-07-2021]
	 * @modificado:
	 * @descripción: {Listar tipo documento}
	 * @param:file
	 */
	@Override
	public ResultEntity<TipoDocumentoEntity> ListarTipoDocumento() {
		return repository.ListarTipoDocumento();
	}

	/**
	 * @autor: JaquelineDB [05-07-2021]
	 * @modificado:
	 * @descripción: {descargar declaracion jurada}
	 * @param:file
	 */
	@Override
	public ResultArchivoEntity DescagarDeclaracionJurada() {
		return repository.DescagarDeclaracionJurada();
	}

	/**
	 * @autor: JaquelineDB [05-07-2021]
	 * @modificado:
	 * @descripción: {Descargar propuesta exploracion}
	 * @param:file
	 */
	@Override
	public ResultArchivoEntity DescargarPropuestaExploracion() {
		return repository.DescargarPropuestaExploracion();
	}

	/**
	 * @autor: JaquelineDB [05-07-2021]
	 * @modificado:
	 * @descripción: {Descargar formato de aprobacion}
	 * @param:file
	 */
	@Override
	public ResultArchivoEntity DescargarFormatoAprobacion() {
		return repository.DescargarFormatoAprobacion();
	}

	/**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Se obtiene al nuevo usuario ganador, luego de ser aprobada una inpugnacion al anterior ganador}
     * @param:FiltroSolicitudEntity
     */
	@Override
	public ResultClassEntity<ImpugnacionNuevoGanadorEntity> ObtenerNuevoUsuarioGanador(FiltroSolicitudEntity filtro) {
		return repository.ObtenerNuevoUsuarioGanador(filtro);
	}

	@Override
	public ResultClassEntity ActualizarAprobadoSolicitud(SolicitudDto obj){
		return repository.ActualizarAprobadoSolicitud(obj);
	}
	@Override
	public ResultClassEntity RegistrarObservacionDocumentoAdjunto(List<DocumentoAdjuntoDto> obj){
		return repository.RegistrarObservacionDocumentoAdjunto(obj);
	}
	@Override
	public ResultArchivoEntity DescagarPlantillaResolucionResumen() {
		return repository.DescagarPlantillaResolucionResumen();
	}
}
