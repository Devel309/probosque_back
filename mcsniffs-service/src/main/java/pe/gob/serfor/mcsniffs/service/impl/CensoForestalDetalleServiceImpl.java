package pe.gob.serfor.mcsniffs.service.impl;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CensoForestal.CensoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.CensoForestalDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.CensoForestalDetalleService;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Service("CensoForestalDetalleService")
public class CensoForestalDetalleServiceImpl implements CensoForestalDetalleService {

    @Autowired
    private CensoForestalDetalleRepository censoForestalDetalleRepository;
    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Override
    public ResultClassEntity RegistrarCensoForestalDetalle(CensoForestalDetalleEntity param) throws Exception {
        return censoForestalDetalleRepository.RegistrarCensoForestalDetalle(param);
    }

    @Override
    public ResultClassEntity<String> RegistrarMasivoCensoForestalDetalle(MultipartFile file, int IdUsuarioRegistro,String tipoCenso)
            throws Exception {
        ResultClassEntity<String> result = new ResultClassEntity<>();

        File_Util fl = new File_Util();
        JSONObject jo = new JSONObject();
        JSONArray dataCollection = new JSONArray();
        JSONObject data = null;

        XSSFWorkbook workbook = fl.getWorkBook(file);

        XSSFSheet sheet = workbook.getSheetAt(0);

        int rowNum = sheet.getLastRowNum() + 1;
        int colNum = sheet.getRow(0).getLastCellNum();
        Row row = null;
        Cell cell = null;
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

        Map<String, Integer> colMapByName = new HashMap<String, Integer>();
        if (sheet.getRow(0).cellIterator().hasNext()) {
            for (int j = 0; j < colNum; j++) {
                DataFormatter formatter = new DataFormatter();
                colMapByName.put(formatter.formatCellValue(sheet.getRow(0).getCell(j), evaluator), j);
            }
        }
        System.out.println(colMapByName);

        for (int i = 1; i < rowNum; i++) {
            row = sheet.getRow(i);
            data = new JSONObject();
            for (Entry<String, Integer> colData : colMapByName.entrySet()) {
                DataFormatter formatter = new DataFormatter();
                cell = row.getCell(colMapByName.get(colData.getKey()));
                data.put(colData.getKey(), formatter.formatCellValue(cell, evaluator));
            }
            dataCollection.put(data);
        }

        Integer idCensoForestal = censoForestalRepository.RegistrarCensoForestal(IdUsuarioRegistro, "",tipoCenso);

        for (int i = 0; i < dataCollection.length(); i++) {
            JSONObject jsonRow = dataCollection.getJSONObject(i);
            CensoForestalDetalleEntity censoForestalDetalle = new CensoForestalDetalleEntity();

            censoForestalDetalle.setParcelaCorte(jsonRow.getInt("NU_PARCELACORTE"));
            censoForestalDetalle.setBloque(jsonRow.getInt("NU_BLOQUE"));
            censoForestalDetalle.setFaja(jsonRow.getInt("NU_FAJA"));
            censoForestalDetalle.setIdTipoRecurso(jsonRow.getString("NU_ID_TIPO_RECURSO"));
            censoForestalDetalle.setIdTipoBosque(jsonRow.getInt("NU_ID_TIPO_BOSQUE"));
            censoForestalDetalle.setIdTipoEvaluacion(jsonRow.getInt("NU_ID_TIPO_EVALUACION"));
            censoForestalDetalle.setCorrelativo(jsonRow.getString("TX_CORRELATIVO"));
            censoForestalDetalle.setIdCodigoEspecie(jsonRow.getInt("NU_ID_CODIGO_ESPECIE"));

            censoForestalDetalle.setIdNombreComun(jsonRow.getString("NU_ID_NOMBRE_COMUN"));
            censoForestalDetalle.setIdNombreCientifico(jsonRow.getString("NU_ID_NOMBRE_CIENTIFICO"));

            censoForestalDetalle.setDescripcionOtros(jsonRow.getString("TX_DESCRIPCION_OTROS"));
            censoForestalDetalle.setIdEspecieCodigo(jsonRow.getInt("NU_ID_ESPECIE_CODIGO"));
            censoForestalDetalle.setIdTipoArbol(jsonRow.getString("NU_ID_TIPO_ARBOL"));
            censoForestalDetalle.setDap(jsonRow.getDouble("NU_DAP"));
            censoForestalDetalle.setAlturaComercial(jsonRow.getDouble("NU_ALTURA_COMERCIAL"));
            censoForestalDetalle.setAlturaTotal(jsonRow.getDouble("NU_ALTURA_TOTAL"));
            censoForestalDetalle.setCodigoArbolCalidad(jsonRow.getString("TX_CODIGO_ARBOL_CALIDAD"));
            censoForestalDetalle.setVolumen(jsonRow.getDouble("NU_VOLUMEN"));
            censoForestalDetalle.setFactorForma(jsonRow.getDouble("NU_FACTOR_FORMA"));
            censoForestalDetalle.setCategoriaDiametrica(jsonRow.getDouble("NU_CATEGORIA_DIAMETRICA"));
            // censoForestalDetalle.setEste(jsonRow.getString("GE_ESTE"));
            // censoForestalDetalle.setNorte(jsonRow.getString("GE_NORTE"));
            censoForestalDetalle.setZona(jsonRow.getInt("NU_ZONA"));
            censoForestalDetalle.setNota(jsonRow.getString("TX_NOTA"));
            censoForestalDetalle.setCantidad(jsonRow.getDouble("NU_CANTIDAD"));
            censoForestalDetalle.setCodigoUnidadMedida(jsonRow.getString("TX_CODIGO_UNIDAD_MEDIDA"));
            censoForestalDetalle.setProductoTipo(jsonRow.getString("TX_PRODUCTO_TIPO"));
            censoForestalDetalle.setIdUsuarioRegistro(IdUsuarioRegistro);
            censoForestalDetalle.setIdCensoForestal(idCensoForestal);

            censoForestalDetalleRepository.RegistrarCensoForestalDetalle(censoForestalDetalle);
        }

        jo.put("tableData", dataCollection);

        result.setData(jo.toString());
        return result;
    }

    @Override
    public   ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>    ListaCensoComercialAprovechamientoMaderable(Integer idPlanDeManejo, String tipoPlan)  throws Exception {
        ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> result = new ResultClassEntity<>();
        ResultadosRecursosForestalesMaderablesDto  datos =new ResultadosRecursosForestalesMaderablesDto();
        datos=censoForestalRepository.ObtenerDatosCensoComercial(idPlanDeManejo,tipoPlan);
        String areaPC=datos.getAreaTotalCensada();
        if(areaPC!=null && !areaPC.trim().equals("null"))datos.setResultadosRFMTablas(censoForestalRepository.ResultadosRecursosForestalesMaderablesDto(idPlanDeManejo,tipoPlan,areaPC) );
        result.setData(datos);
        result.setSuccess(true);
        if(datos!=null )
            result.setMessage("Lista Censo Comercial Aprovechamiento Maderable ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public ResultClassEntity<List<ParametroValorEntity>> ListarTipoBosque() throws Exception {
        ResultClassEntity<List<ParametroValorEntity>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListarTipoBosque());
        result.setSuccess(true);
        result.setMessage("Lista de tipo bosque");

        return result;
    }

    @Override
    public ResultClassEntity<EspecieNombreDto> ListarEspecie() throws Exception {
        EspecieNombreDto dto = this.ListarEspecieObtener();
        ResultClassEntity<EspecieNombreDto> result = new ResultClassEntity<>();

        result.setData(dto);
        result.setSuccess(true);
        result.setMessage("Lista de especies");

        return result;
    }
    private EspecieNombreDto ListarEspecieObtener() throws Exception {
        List<ParametroValorEntity> listNombreComun = censoForestalRepository.ListarEspecieNombreComun();
        List<ParametroValorEntity> listNombreCientifico = censoForestalRepository.ListarEspecieNombreCientifico();

        EspecieNombreDto dto = new EspecieNombreDto();
        dto.setListaEspecieNombreCientifico(listNombreCientifico);
        dto.setListaEspecieNombreComun(listNombreComun);

        return dto;
    }

    @Override
    public ResultClassEntity<List<TablaAnexo3PGMF>>  ObtenerTablaAnexo3PGMFFustales(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<TablaAnexo3PGMF>> result = new ResultClassEntity<>();
        List<TablaAnexo3PGMF>  datos =new ArrayList<>();
        datos=censoForestalRepository.ObtenerTablaAnexo3PGMFFustales(idPlanDeManejo,tipoPlan);

        if(datos !=null && datos.size()>0 ) {
            result.setData(datos);
            result.setSuccess(true);
            result.setMessage("Obtener Tabla Anexo3 PGMF Fustales");
        }
        else {
            result.setSuccess(false);
            result.setMessage("No existen datos registrdos  para el Plan de Manejo " + idPlanDeManejo + " Tipo de plan " + tipoPlan);
        }
        return result;
    }

    @Override
    public ResultClassEntity<List<TablaAnexo3PGMF>>  ObtenerTablaAnexo3PGMFVolumenPotencial(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<TablaAnexo3PGMF>> result = new ResultClassEntity<>();
        List<TablaAnexo3PGMF>  datos =new ArrayList<>();
        datos=censoForestalRepository.ObtenerTablaAnexo3PGMFVolumenPotencial(idPlanDeManejo,tipoPlan);
        if(datos !=null && datos.size()>0 ) {
            result.setData(datos);
            result.setSuccess(true);
            result.setMessage("Obtener Tabla Anexo3 PGMF Volumen Potencial");
        }
        else {
            result.setSuccess(false);
            result.setMessage("No existen datos registrdos  para el Plan de Manejo " + idPlanDeManejo + " Tipo de plan " + tipoPlan);
        }
        return result;
    }

    @Override
    public ResultClassEntity<Map<Integer, String>> RegistrarIndividualCensoForestalDetalle(List<CensoForestalDetalleEntity> obj,
            Integer idUsuarioRegistro, Integer IdCensoForestal) throws Exception {

        ResultClassEntity<Map<Integer, String>> result = new ResultClassEntity<>();
        Map<Integer, String> dictionary = new HashMap();
        
        Integer index = 1;
        for (CensoForestalDetalleEntity censoForestalDetalleEntity : obj) {

            try {
                censoForestalDetalleEntity.setIdUsuarioRegistro(idUsuarioRegistro);
                censoForestalDetalleEntity.setIdCensoForestal(IdCensoForestal);
                censoForestalDetalleRepository.RegistrarCensoForestalDetalle(censoForestalDetalleEntity);    

                dictionary.put(index, "Se registró la linea con exito.");
            } catch (Exception e) {
                dictionary.put(index, "Ocurrió un error | " + e.getMessage());
            }finally{
                index++;
            }
        }
        result.setData(dictionary);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity RegistrarCensoForestalDetalle(Integer idUsuarioRegistro,Integer  idPlanManejo,CensoForestalDetalleEntity censoForestalDetalleEntity) throws Exception
    {
        ResultClassEntity  result;
        censoForestalDetalleEntity.setIdUsuarioRegistro(idUsuarioRegistro);
        censoForestalDetalleEntity.setIdPlanManManejo(idPlanManejo);
        result=censoForestalDetalleRepository.RegistrarCensoForestalDetalle(censoForestalDetalleEntity);
        result.setData(null);
        if(result!=null && result.getSuccess()){
            result.setSuccess(true);
            result.setMessage("Registro  correcta");
        }
        else{
            result.setSuccess(false);
            result.setMessage("No se registro.");
        }
        return result;
    }


    @Override
    public ResultClassEntity ActualizarIndividualCensoForestalDetalle(Integer idUsuarioRegistro,Integer idCensoForestalDetalle,CensoForestalDetalleEntity obj) throws Exception
    {
        ResultClassEntity  result;
        CensoForestalDetalleEntity  censoForestalDetalleEntity=obj;
        censoForestalDetalleEntity.setIdUsuarioRegistro(idUsuarioRegistro);
        result=censoForestalDetalleRepository.actualizarCensoForestalDetalle(censoForestalDetalleEntity,idCensoForestalDetalle);
        result.setData(null);
        if(result!=null && result.getSuccess()){
            result.setSuccess(true);
            result.setMessage("Actualizacion correcta");
        }
        else{
            result.setSuccess(false);
            result.setMessage("No se actualizo .");
        }
        return result;
    }

    @Override
    public ResultClassEntity EliminarCensoForestalDetalle(Integer idUsuarioElimina,Integer idCensoForestalDetalle) throws Exception
    {
        ResultClassEntity  result;
        result=censoForestalDetalleRepository.eliminarCensoForestalDetalle(idUsuarioElimina,idCensoForestalDetalle);
        result.setData(null);
        if(result!=null && result.getSuccess()){
            result.setSuccess(true);
            result.setMessage("Borrado correcto");
        }
        else{
            result.setSuccess(false);
            result.setMessage("No se elimino .");
        }
        return result;
    }

    @Override
    public ResultClassEntity<CensoForestalDto> CensoForestalObtener() throws Exception {
        ResultClassEntity<CensoForestalDto> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.CensoForestalObtener());
        result.setSuccess(true);
        result.setMessage("Lista de tipo bosque");

        return result;
    }

    @Override
    public ResultClassEntity<List<ContratoTHDto>> ListaTHContratosObtener(ContratoTHDto data) throws Exception {
        ResultClassEntity<List<ContratoTHDto>> result = new ResultClassEntity<>();

        List<ContratoTHDto> lista = censoForestalRepository.ListaTHContratosObtener(data);

        result.setData(lista);
        result.setSuccess(true);
        result.setMessage("Lista de contratos");
        result.setTotalRecord(lista==null ? 0 : lista.isEmpty() ? 0 : lista.get(0) ==null ? 0 : lista.get(0).getTotalRecord());
        return result;
    }

    @Override
    public ResultClassEntity<List<PlanManejoAppEntity>> ListaPlanManejoPorContratoObtener(Integer idContrato,String tipoPlanManejo,String tipoCensoForestal) throws Exception {
        ResultClassEntity<List<PlanManejoAppEntity>> result = new ResultClassEntity<>();
        result.setData(censoForestalRepository.ListaPlanManejoPorContratoObtener(idContrato,tipoPlanManejo,tipoCensoForestal));
        result.setSuccess(true);
        result.setMessage("Lista de plan de manejo por contrato");
        return result;
    }

    @Override
    public ResultClassEntity<List<ListaEspecieDto>> ListaEspeciesInventariadasMaderablesObtener(Integer idPlanDeManejo,String tipoPlan) throws Exception {
        ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();
        result.setData(censoForestalRepository.ListaEspeciesInventariadasMaderables(idPlanDeManejo,tipoPlan));
        result.setSuccess(true);
        result.setMessage("Lista Especies Inventariadas Maderables Obtener");
        return result;
    }

    @Override
    public ResultClassEntity<DatosTHDto> ListaDatosTHObtener(Integer idPlanDeManejo) throws Exception {
        ResultClassEntity<DatosTHDto> result = new ResultClassEntity<>();

        DatosTHDto datos = censoForestalRepository.ContratoObtener(idPlanDeManejo);
       // datos.setListaEspecies(this.ListarEspecieObtener());
        datos.setListaTipoBosque(censoForestalRepository.ListarTipoBosque());
        datos.setListaPoligonos(censoForestalRepository.ContratolistaPoligono(idPlanDeManejo));
        result.setData(datos);
        result.setSuccess(true);
        result.setMessage("Lista de datos TH");
        return result;
    }

    @Override
    public ResultClassEntity<ActividadesDeAprovechamientoDto>  ListaDatosActividadesAprovechamiento(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<ActividadesDeAprovechamientoDto> result = new ResultClassEntity<>();

        ActividadesDeAprovechamientoDto datos =new ActividadesDeAprovechamientoDto();
        datos.setCensoComercialDto(this.CensoComercialObtener(idPlanDeManejo,tipoPlan));

        result.setData(datos);
        result.setSuccess(true);
        result.setMessage("Lista Datos Actividades Aprovechamiento");

        return result;
    }

    private CensoComercialDto CensoComercialObtener(Integer idPlanDeManejo, String tipoPlan) throws Exception {

        List<ListaEspecieDto> listaEspecieDtos = censoForestalRepository.ListaEspecieDto(idPlanDeManejo,tipoPlan);

        ResultadosRecursosForestalesMaderablesDto  resultadosRecursosForestalesMaderablesDto =new ResultadosRecursosForestalesMaderablesDto();
        resultadosRecursosForestalesMaderablesDto=censoForestalRepository.ObtenerDatosCensoComercial(idPlanDeManejo,tipoPlan);
        String areaPC=resultadosRecursosForestalesMaderablesDto.getAreaTotalCensada();
        if(areaPC!=null && !areaPC.trim().equals("null"))resultadosRecursosForestalesMaderablesDto.setResultadosRFMTablas(censoForestalRepository.ResultadosRecursosForestalesMaderablesDto(idPlanDeManejo,tipoPlan,areaPC) );

        VolumenCortaDto volumenCortaDto=new VolumenCortaDto();
        volumenCortaDto=censoForestalRepository.ObtenerDatosVolumenCorta(idPlanDeManejo,tipoPlan);
        volumenCortaDto.setResultadosVolumenDeCortaTablas(censoForestalRepository.ResultadosVolumenDeCorta(idPlanDeManejo,tipoPlan,volumenCortaDto.getContadorPc()));

        CensoComercialDto censoComercialDto = new CensoComercialDto();
        censoComercialDto.setListaEspecieDtos(listaEspecieDtos);
        censoComercialDto.setResultadosRecursosForestalesMaderablesDto(resultadosRecursosForestalesMaderablesDto);
        censoComercialDto.setVolumenCortaDto(volumenCortaDto);
        return censoComercialDto;
    }

    @Override
    public   ResultClassEntity<ResultadosArbolesAprovechablesMaderables>    ListaResultadosArbolesAprovechablesMaderables(Integer idPlanDeManejo, String tipoPlan)  throws Exception {
        ResultClassEntity<ResultadosArbolesAprovechablesMaderables> result = new ResultClassEntity<>();
        ResultadosArbolesAprovechablesMaderables datos =new ResultadosArbolesAprovechablesMaderables();
        datos=censoForestalRepository.ResultadosArbolesAprovechables(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos!=null )
            result.setMessage("lista ResultadosArboles Aprovechables Maderables ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }


    @Override
    public   ResultClassEntity<ResultadosArbolesAprovechablesMaderables>    ListaResultadosAprovechableForestalNoMaderable(Integer idPlanDeManejo, String tipoPlan)  throws Exception {
        ResultClassEntity<ResultadosArbolesAprovechablesMaderables> result = new ResultClassEntity<>();
        ResultadosArbolesAprovechablesMaderables datos =new ResultadosArbolesAprovechablesMaderables();
        datos=censoForestalRepository.ResultadosAprovechableForestalNoMaderable(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos!=null )
            result.setMessage("lista ResultadosArboles Aprovechables No Maderables ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public   ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>    ListaResultadosCensoComercialAprovechamientoNoMaderable(Integer idPlanDeManejo, String tipoPlan)  throws Exception {
        ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> result = new ResultClassEntity<>();
        ResultadosRecursosForestalesMaderablesDto  datos =new ResultadosRecursosForestalesMaderablesDto();
        datos=censoForestalRepository.ObtenerDatosCensoComercial(idPlanDeManejo,tipoPlan);
        String areaPC=datos.getAreaTotalCensada();
        if(areaPC!=null && !areaPC.trim().equals("null"))datos.setResultadosRFMTablas(censoForestalRepository.ResultadosCensoComercialAprovechamientoNoMaderable(idPlanDeManejo,tipoPlan,areaPC) );
        result.setData(datos);
        result.setSuccess(true);
        if(datos!=null )
            result.setMessage("Lista Resultados Censo Comercial Aprovechamiento No Maderable ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public   ResultClassEntity<ResultadosArbolesAprovechablesMaderables>    ListaResultadosArbolesAprovechablesFustales(Integer idPlanDeManejo, String tipoPlan)  throws Exception {
        ResultClassEntity<ResultadosArbolesAprovechablesMaderables> result = new ResultClassEntity<>();
        ResultadosArbolesAprovechablesMaderables datos =new ResultadosArbolesAprovechablesMaderables();
        datos=censoForestalRepository.ResultadosArbolesAprovechablesFustales(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos!=null )
            result.setMessage("lista Resultados Arboles Aprovechables Fustales ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public ResultClassEntity<List<Anexo2Dto>>  listaDatosPOConcesionesAnexo2(Integer idPlanDeManejo, String tipoPlan,Integer tipoProceso) throws Exception {
        ResultClassEntity<List<Anexo2Dto>> result = new ResultClassEntity<>();
        List<Anexo2Dto> datos =new ArrayList<Anexo2Dto>();
        datos=censoForestalRepository.ResultadosPOConcesionesAnexos2(idPlanDeManejo,tipoPlan,tipoProceso,null);
        result.setData(datos);
        result.setSuccess(true);

        if(datos.size()>0 )
            result.setMessage("lista Datos POConcesiones Anexo2");
        else
        result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public ResultClassEntity<List<Anexo2Dto>>  ListaAnexo7NoMaderable(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<Anexo2Dto>> result = new ResultClassEntity<>();
        List<Anexo2Dto> datos =new ArrayList<Anexo2Dto>();
        datos=censoForestalRepository.ResultadosAnexo7NoMaderable(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("Lista Anexo No Maderable");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public ResultClassEntity<List<Anexo2Dto>>  BuscarAnexo7NoMaderable(Integer idPlanDeManejo, String tipoPlan,Anexo2Dto data) throws Exception {
        ResultClassEntity<List<Anexo2Dto>> result = new ResultClassEntity<>();
        List<Anexo2Dto> datos =new ArrayList<Anexo2Dto>();
        datos=censoForestalRepository.ResultadosBuscarAnexo7NoMaderable(idPlanDeManejo,tipoPlan,data);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("Buscar datos Anexo 7  No Maderable");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public   ResultClassEntity<List<Anexo2PGMFDto>>    ListaFormatoPGMFAnexo2(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque)  throws Exception {
        ResultClassEntity<List<Anexo2PGMFDto>> result = new ResultClassEntity<>();
        List<Anexo2PGMFDto> datos =new ArrayList<Anexo2PGMFDto>();
        datos=censoForestalRepository.ResultadosFormatoPGMFAnexo2(idPlanDeManejo,tipoPlan,idTipoBosque,"");
        result.setData(datos);
        result.setSuccess(true);

        if(datos.size()>0 )
            result.setMessage("lista Datos Formato PGMF Anexo 2 ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
        return result;
    }


    @Override
    public   ResultClassEntity<List<List<Anexo2PGMFDto>>>   ListaTablasPMFICAnexo3(Integer idPlanDeManejo, String tipoPlan)  throws Exception {
        ResultClassEntity<List<List<Anexo2PGMFDto>>>  result = new ResultClassEntity<>();
        List<List<Anexo2PGMFDto>> datos =new ArrayList<List<Anexo2PGMFDto>>();
        datos=censoForestalRepository.ResultadosPMFICAnexo3(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);

        if(datos.size()>0 )
            result.setMessage("lista Datos Anexo 3 PMFIC ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public   ResultClassEntity<List<List<Anexo3PGMFDto>>> ListaTablasPMFICAnexo4(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<List<Anexo3PGMFDto>>>  result = new ResultClassEntity<>();
        List<List<Anexo3PGMFDto>> datos =new ArrayList<List<Anexo3PGMFDto>>();
        datos=censoForestalRepository.ResultadosPMFICAnexo4(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista Datos Anexo 4 PMFIC ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public   ResultClassEntity<List<Anexo2PGMFDto>>  PGMFInventarioExploracionVolumenP(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque)  throws Exception {
        ResultClassEntity<List<Anexo2PGMFDto>> result = new ResultClassEntity<>();
        List<Anexo2PGMFDto> datos =new ArrayList<Anexo2PGMFDto>();
        datos=censoForestalRepository.PGMFInventarioExploracionVolumenP(idPlanDeManejo,tipoPlan,idTipoBosque);
        result.setData(datos);
        result.setSuccess(true);

        if(datos.size()>0 )
            result.setMessage("lista Datos Formato PGMF Inventario Exploración Volumen ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
        return result;
    }

    @Override
    public   ResultClassEntity<List<Anexo3PGMFDto>>    ListaFormatoPGMFAnexo3(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque)  throws Exception {
        ResultClassEntity<List<Anexo3PGMFDto>> result = new ResultClassEntity<>();
        List<Anexo3PGMFDto> datos =new ArrayList<Anexo3PGMFDto>();
        datos=censoForestalRepository.ResultadosFormatoPGMFAnexo3(idPlanDeManejo,tipoPlan,idTipoBosque,"");
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista Datos Formato PGMF Anexo 3 ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
        return result;
    }

    @Override
    public   ResultClassEntity<List<Anexo3PGMFDto>> PGMFInventarioExploracionFustales(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque)  throws Exception {
        ResultClassEntity<List<Anexo3PGMFDto>> result = new ResultClassEntity<>();
        List<Anexo3PGMFDto> datos =new ArrayList<Anexo3PGMFDto>();
        datos=censoForestalRepository.PGMFInventarioExploracionFustales(idPlanDeManejo,tipoPlan,idTipoBosque);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista Datos Formato PGMF Anexo 3 ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
        return result;
    }

    @Override
    public    ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> ListaResultadosEspecieMuestreo(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
        List<ResultadosEspeciesMuestreoDto> datos =new ArrayList<ResultadosEspeciesMuestreoDto>();
        datos=censoForestalRepository.ResultadosEspecieMuestreo(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista Datos Especie Muestreo ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public    ResultClassEntity<List<ResultadosAnexo6>>  ListaAnexo6(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<ResultadosAnexo6> > result = new ResultClassEntity<>();
        List<ResultadosAnexo6> datos =new ArrayList<ResultadosAnexo6>();
        datos=censoForestalRepository.ResultadosAnexo6(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista Datos Anexo 6 ");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public    ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> ListaResultadosEspecieFrutales(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
        List<ResultadosEspeciesMuestreoDto> datos =new ArrayList<ResultadosEspeciesMuestreoDto>();
        datos=censoForestalRepository.ResultadosEspecieFrutales(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista Datos Especie Muestreo Frutales");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }

    @Override
    public    ResultClassEntity<List<AprovechamientoRFNMDto>> ListaResultadosAprovechamientoRFNM(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultClassEntity<List<AprovechamientoRFNMDto>> result = new ResultClassEntity<>();
        List<AprovechamientoRFNMDto> datos =new ArrayList<AprovechamientoRFNMDto>();
        datos=censoForestalRepository.ResultadosAprovechamientoRFNM(idPlanDeManejo,tipoPlan);
        result.setData(datos);
        result.setSuccess(true);
        if(datos.size()>0 )
            result.setMessage("lista de ResultadosA provechamiento RFNM");
        else
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
        return result;
    }
    
    /**
     * @autor: Danny Nazario [07-01-2022]
     * @modificado:
     * @descripción: { Lista Potencial Producción Resultados Fustales }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarResultadosFustales(Integer idPlanManejo, String tipoPlan) throws Exception {
        return censoForestalRepository.listarResultadosFustales(idPlanManejo, tipoPlan);
    }

    @Override
    public ResultClassEntity<List<ListaEspecieDto>> ListaTipoRecursoForestal(String tipoEspecie,String tipoCenso)
            throws Exception {

            ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();

            result.setData(censoForestalRepository.ListaTipoRecursoForestal(tipoEspecie, tipoCenso));
            result.setSuccess(true);
            result.setMessage("Lista recurso forestal maderable");
    
            return result;
    }

    @Override
    public ResultClassEntity  ListaAprovechamientoForestalNMResumen(Integer idPlanManejo, String tipoPlan,Integer tipoProceso)
            throws Exception {
        return censoForestalRepository.AprovechamientoForestalNMResumen(idPlanManejo,  tipoPlan, tipoProceso);
    }

    @Override
    public ResultClassEntity  ListarResultadosInventarios(Integer idPlanManejo, String tipoPlan,Integer tipoProceso)
            throws Exception {
        return censoForestalRepository.ResultadoInventarioPorParcela(idPlanManejo,  tipoPlan, tipoProceso);
    }

    @Override
    public ResultClassEntity<List<ListaEspecieDto>> ListaTipoRecursoForestalManejo(Integer idPlanManejo,String tipoCenso,String tipoEspecie)
            throws Exception {

        ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListaTipoRecursoForestalManejo(idPlanManejo, tipoCenso,tipoEspecie));
        result.setSuccess(true);
        result.setMessage("Lista Tipo recurso forestal ");

        return result;
    }

    @Override
    public ResultClassEntity<List<ListaEspecieDto>> ListaResumenEspecies(Integer idPlanManejo,String tipoCenso,String tipoEspecie)
            throws Exception {

        ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListaResumenEspecies(idPlanManejo, tipoCenso,tipoEspecie));
        result.setSuccess(true);
        result.setMessage("Lista Resumen Especie");

        return result;
    }

    @Override
    public ResultClassEntity<List<ListaEspecieDto>> ListaResumenEspeciesInventario(Integer idPlanManejo,String tipoRecurso,String tipoInventario)
            throws Exception {

        ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListaResumenEspeciesInventario(idPlanManejo, tipoRecurso,tipoInventario));
        result.setSuccess(true);
        result.setMessage("Lista Resumen Especie Inventario");

        return result;
    }

    @Override
    public ResultClassEntity<List<CensoForestalDetalleEntity>> ListarCensoForestalDetalle(Integer idPlanManejo,String tipoCenso,String tipoRecurso)
            throws Exception {

        ResultClassEntity<List<CensoForestalDetalleEntity>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListarCensoForestalDetalle(idPlanManejo, tipoCenso,tipoRecurso));
        result.setSuccess(true);
        result.setMessage("Lista Censo Forestal Detalle");

        return result;
    }

    @Override
    public ResultClassEntity<List<CensoForestalEntity>> SincronizacionCenso(Integer idPlanManejo,String tipoCenso,String tipoRecurso)
            throws Exception {

        ResultClassEntity<List<CensoForestalEntity>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.SincronizacionCenso(idPlanManejo, tipoCenso,tipoRecurso));
        result.setSuccess(true);
        result.setMessage("Sincronizacion del censo");

        return result;
    }


    @Override
    public ResultClassEntity<List<Anexo2CensoEntity>> ListarCensoForestalAnexo2(Integer idPlanManejo, String tipoRecurso)
            throws Exception {

        ResultClassEntity<List<Anexo2CensoEntity>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListarCensoForestalAnexo2(idPlanManejo,tipoRecurso));
        result.setSuccess(true);
        result.setMessage("Lista Anexo 2 ");

        return result;
    }

    @Override
    public ResultClassEntity  registrarFormatoPGMFAAnexo2Excel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<CensoForestalDetalleEntity> listCensoForestal=new ArrayList<>();
        CensoForestalDetalleEntity censoForestalDetalleEntity;
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, "Libro1", 2, 1);
            for(ArrayList obj : listaExcel) {
                censoForestalDetalleEntity = new CensoForestalDetalleEntity();
                String idPlanManejoExcel=censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(36));
                String valorIdPlanManejo=""+idPlanDeManejo;
                Double valorDap=Double.parseDouble(""+obj.get(10));
                if(tipoPlan.trim().equals("PGMFA") &&  valorIdPlanManejo.equals(idPlanManejoExcel) && valorDap>=30)
                {
                    censoForestalDetalleEntity =censoForestalDetalleEntity.lenarCensoForestal(obj);
                    censoForestalDetalleEntity.setIdCensoForestalCabecera(censoForestalRepository.obtenerCensoForestalCabecera(idPlanDeManejo));
                    listCensoForestal.add(censoForestalDetalleEntity);
                }
                else if(tipoPlan.trim().equals("POCC")){
                    censoForestalDetalleEntity =censoForestalDetalleEntity.lenarCensoForestal(obj);
                    censoForestalDetalleEntity.setIdCensoForestalCabecera(censoForestalRepository.obtenerCensoForestalCabecera(idPlanDeManejo));
                   listCensoForestal.add(censoForestalDetalleEntity);
                }
            }
            if(listCensoForestal.size()>0){
                ResultClassEntity responseListaCenso = new ResultClassEntity<>();
                responseListaCenso=RegistrarIndividualCensoForestalDetalle(listCensoForestal, 0, 2);
                if(responseListaCenso.getData()!=null && responseListaCenso.getSuccess() && responseListaCenso.getData().toString().contains("Se registró la linea con exito.")){
                    result.setSuccess(true);
                    result.setMessage("Se registró correctamente.");
                }
                else{
                    result.setSuccess(false);
                    result.setMessage("No se registro."+responseListaCenso.getMessage());
                }
            }
            else{
                result.setSuccess(true);
                result.setMessage("No se encontro registros asociados al plan de manejo "+idPlanDeManejo+" .Con el dap mayor a 30");
            }
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
        }
        return result;
    }

    @Override
    public ResultClassEntity  registrarFormatoPGMFAAnexo3Excel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<CensoForestalDetalleEntity> listCensoForestal=new ArrayList<>();
        CensoForestalDetalleEntity censoForestalDetalleEntity;
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, "Libro1", 2, 1);
            for(ArrayList obj : listaExcel) {
                censoForestalDetalleEntity = new CensoForestalDetalleEntity();
                String idPlanManejoExcel=censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(36));
                String valorIdPlanManejo=""+idPlanDeManejo;
                Double valorDap=Double.parseDouble(""+obj.get(10));
                if(valorIdPlanManejo.equals(idPlanManejoExcel) &&
                        (valorDap>=10 && valorDap<30 )
                )
                {
                    censoForestalDetalleEntity =censoForestalDetalleEntity.lenarCensoForestal(obj);
                    censoForestalDetalleEntity.setIdCensoForestalCabecera(censoForestalRepository.obtenerCensoForestalCabecera(idPlanDeManejo));
                    listCensoForestal.add(censoForestalDetalleEntity);
                }
            }
            if(listCensoForestal.size()>0){
                ResultClassEntity responseListaCenso = new ResultClassEntity<>();
                responseListaCenso=RegistrarIndividualCensoForestalDetalle(listCensoForestal, 0, 2);
                if(responseListaCenso.getData()!=null && responseListaCenso.getSuccess() && responseListaCenso.getData().toString().contains("Se registró la linea con exito.")){
                    result.setSuccess(true);
                    result.setMessage("Se registró correctamente.");
                }
                else{
                    result.setSuccess(false);
                    result.setMessage("No se registro."+responseListaCenso.getMessage());
                }
            }
            else{
                result.setSuccess(true);
                result.setMessage("No se encontro registros asociados al plan de manejo "+idPlanDeManejo+" .Con el dap menor a 30");
            }
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
        }
        return result;
    }


    @Override
    public ResultClassEntity<List<CensoForestalDetalleEntity>> cargaTipoPlanMasivoExcel(MultipartFile file)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<CensoForestalDetalleEntity> listCensoForestal=new ArrayList<>();
        CensoForestalDetalleEntity censoForestalDetalleEntity;
        int filaCelda=0;
        try {
            File_Util fl = new File_Util();
            int numeroFilaCabecera=5;
            String nombreHoja="Hoja1";
            List<ArrayList<String>> listaExcelCabecera= fl.getExcel(file, nombreHoja, numeroFilaCabecera, 1);
            CensoForestalDetalleEntity   tempotal = new CensoForestalDetalleEntity();
            System.out.println("idPlanDeManejo  "+tempotal.cadenaRetificadaEnteros(""+listaExcelCabecera.get(0).get(1)));
            System.out.println("tipoCensoForestal "+ listaExcelCabecera.get(1).get(1));
            Integer  idPlanDeManejo=Integer.parseInt(tempotal.cadenaRetificadaEnteros(""+listaExcelCabecera.get(0).get(1)));
            String  tipoCensoForestal=""+ listaExcelCabecera.get(1).get(1);
            int numeroFila=12;
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, 1);
            java.text.DateFormat fecha=new java.text.SimpleDateFormat("yyyyMMddHHmmss");
            String codigofecha=""+fecha.format(new java.util.Date());
             filaCelda=numeroFila;
            for(ArrayList obj : listaExcel) {
                if(obj.get(0)==null || obj.get(0).equals(""))
                    break;

                for(int i=0;i<obj.size();i++){
                    System.out.println("EXCELCE3LDA F="+filaCelda+"  T="+obj.size()+" i="+i+"  "+obj.get(i));
                }
                censoForestalDetalleEntity = new CensoForestalDetalleEntity();
                System.out.println("censoFo gg");
                censoForestalDetalleEntity.setCodigoTipoCensoForestal(tipoCensoForestal);
                censoForestalDetalleEntity.setIdCensoForestalCabecera(0);
                System.out.println("censoFo 0");
                censoForestalDetalleEntity.setIdPlanManManejo(idPlanDeManejo);
                System.out.println("censoFo idpan");
                censoForestalDetalleEntity.setIdCodigoEspecie(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(0))));
                System.out.println("censoFo  codEspecie");
                censoForestalDetalleEntity.setNombreCientifico(""+obj.get(1));
                System.out.println("censoFo 1");
                censoForestalDetalleEntity.setNombreComun(""+obj.get(2));
                System.out.println("censoFo 2");
                censoForestalDetalleEntity.setNombreNativo(""+obj.get(3));
                System.out.println("censoFo 3");
                censoForestalDetalleEntity.setNombreComercial(""+obj.get(4));
                System.out.println("censoFo 4");
                censoForestalDetalleEntity.setNombreAlterno(""+obj.get(5));
                System.out.println("censoFo 5");
                censoForestalDetalleEntity.setFamilia(""+obj.get(6));
                System.out.println("censoFo 6");
                censoForestalDetalleEntity.setCodigoDeArbol(""+obj.get(7));
                System.out.println("censoFo 7");
                censoForestalDetalleEntity.setNumeroCorrelativoArbol(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(8))));
                System.out.println("censoFo 8");
                censoForestalDetalleEntity.setDap(Double.parseDouble(""+obj.get(9)));
                System.out.println("censoFo 9");
                censoForestalDetalleEntity.setAlturaComercial(Double.parseDouble(""+obj.get(10)));
                System.out.println("censoFo 10");
                censoForestalDetalleEntity.setAlturaTotal(Double.parseDouble(""+obj.get(11)));
                System.out.println("censoFo 11");
                censoForestalDetalleEntity.setVolumen(Double.parseDouble(""+obj.get(12)));
                System.out.println("censoFo 12");
                censoForestalDetalleEntity.setCantidad(Double.parseDouble(""+obj.get(13)));
                System.out.println("censoFo 13");
                censoForestalDetalleEntity.setIdTipoArbol(""+obj.get(14));
                System.out.println("censoFo 14");
                censoForestalDetalleEntity.setFactorForma(Double.parseDouble(""+obj.get(15)));
                censoForestalDetalleEntity.setEste(""+obj.get(16));
                censoForestalDetalleEntity.setNorte(""+obj.get(17));
                System.out.println("censoFo 17");
                censoForestalDetalleEntity.setZona(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(18))));
                censoForestalDetalleEntity.setCodigoArbolCalidad(""+obj.get(19));
                System.out.println("censoFo 19");
                censoForestalDetalleEntity.setDescripcionOtros(""+obj.get(20));
                censoForestalDetalleEntity.setIdTipoRecurso(""+obj.get(21));
                censoForestalDetalleEntity.setProductoTipo(""+obj.get(22));
                System.out.println("censoFo 22");
                censoForestalDetalleEntity.setCategoriaDiametrica(Double.parseDouble(""+obj.get(23)));
                System.out.println("censoFo 23");
                censoForestalDetalleEntity.setFaja(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(24))));
                System.out.println("censoFo 24");
                censoForestalDetalleEntity.setUnidadTrabajo(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(25))));
                System.out.println("censoFo 25");
                censoForestalDetalleEntity.setParcelaCorta(""+obj.get(26));
                System.out.println("censoFo 26");
                censoForestalDetalleEntity.setCodigoUnidadMedida(""+obj.get(27));
                System.out.println("censoFo  27");
                censoForestalDetalleEntity.setMortanda(("SI".equals(""+obj.get(28)) || "Si".equals(""+obj.get(28))) ?true:false);
                censoForestalDetalleEntity.setBloqueQuinquenal(""+obj.get(30));
                System.out.println("censoFo 30");
                censoForestalDetalleEntity.setSuperficieComercial(Double.parseDouble(""+obj.get(31)));
                System.out.println("censoFo");
                censoForestalDetalleEntity.setCondicionArbol(""+obj.get(32));
                System.out.println("censoFo 32");
                censoForestalDetalleEntity.setIdTipoBosque(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(33))));
                System.out.println("censoFo 33");
                censoForestalDetalleEntity.setEstadoArbol(""+obj.get(34));
                System.out.println("censoFo 34");
                //  censoForestalDetalleEntity.setContrato(""+0);
                censoForestalDetalleEntity.setBosqueSecundario(""+obj.get(35));
                System.out.println("censoFo 35");
                if(tipoCensoForestal.trim().equals("Inventario"))
                    censoForestalDetalleEntity.setCodigoUnico("INV-00000-"+codigofecha);
                else
                censoForestalDetalleEntity.setCodigoUnico(censoForestalDetalleEntity.getCodigoDeArbol()+"-00000-"+codigofecha);
                System.out.println("censoFo setCodigoUnico");
                censoForestalDetalleEntity.setImagen("");
                censoForestalDetalleEntity.setAudio("");
                System.out.println("censoForestalDetalleEntity  "+censoForestalDetalleEntity.toString());
                listCensoForestal.add(censoForestalDetalleEntity);
                filaCelda++;
            }
            if(listCensoForestal.size()>0){
                ResultClassEntity responseListaCenso = new ResultClassEntity<>();
                responseListaCenso=RegistrarIndividualCensoForestalDetalle(listCensoForestal, 0, 2);
                if(responseListaCenso.getData()!=null && responseListaCenso.getSuccess() && responseListaCenso.getData().toString().contains("Se registró la linea con exito.")){
                    result.setData(listCensoForestal);
                    result.setSuccess(true);
                    result.setMessage("Se registró correctamente.");
                }
                else{
                    result.setSuccess(false);
                    result.setMessage("No se registro."+responseListaCenso.getMessage());
                }
            }
            else{
                result.setSuccess(true);
                result.setMessage("No se encontro registros asociados");
            }
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage()+"\nFila :"+filaCelda);
        }
        return result;
    }

    @Override
    public ResultClassEntity<List<CensoForestalDetalleEntity>> cargaMasivaTipoPlanExcel(MultipartFile file, Integer idPlanManejo)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<CensoForestalDetalleEntity> listCensoForestal=new ArrayList<>();
        CensoForestalDetalleEntity censoForestalDetalleEntity;
        int filaCelda=0;
        try {
            File_Util fl = new File_Util();
            int numeroFilaCabecera=5;
            String nombreHoja="Hoja1";
            List<ArrayList<String>> listaExcelCabecera= fl.getExcelRow(file, nombreHoja, numeroFilaCabecera, numeroFilaCabecera + 1,1);
            CensoForestalDetalleEntity tempotal = new CensoForestalDetalleEntity();
            System.out.println("idPlanDeManejo  "+tempotal.cadenaRetificadaEnteros(""+listaExcelCabecera.get(0).get(1)));
            System.out.println("tipoCensoForestal "+ listaExcelCabecera.get(1).get(1));
            Integer idPlanDeManejo = Integer.parseInt(tempotal.cadenaRetificadaEnteros(""+listaExcelCabecera.get(0).get(1)));
            String tipoCensoForestal = ""+ listaExcelCabecera.get(1).get(1);

            if(!idPlanManejo.toString().equals(idPlanDeManejo.toString())){
                result.setSuccess(false);
                result.setMessage("No se puede procesar el excel de carga masiva.\nEl Nro. de la solicitud asociada debe ser el mismo que el Nro. de Plan definido en el archivo excel procesado.");
                return result;
            }
            int numeroFila = 12;
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, 1);
            java.text.DateFormat fecha=new java.text.SimpleDateFormat("yyyyMMddHHmmss");
            String codigofecha=""+fecha.format(new java.util.Date());
            filaCelda=numeroFila;
            for(ArrayList obj : listaExcel) {
                if(obj.get(0)==null || obj.get(0).equals(""))
                    break;

                for(int i=0;i<obj.size();i++){
                    System.out.println("EXCELCE3LDA F="+filaCelda+"  T="+obj.size()+" i="+i+"  "+obj.get(i));
                }
                censoForestalDetalleEntity = new CensoForestalDetalleEntity();
                System.out.println("censoFo gg");
                censoForestalDetalleEntity.setCodigoTipoCensoForestal(tipoCensoForestal);
                censoForestalDetalleEntity.setIdCensoForestalCabecera(0);
                System.out.println("censoFo 0");
                censoForestalDetalleEntity.setIdPlanManManejo(idPlanDeManejo);
                System.out.println("censoFo idpan");
                censoForestalDetalleEntity.setIdCodigoEspecie(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(0))));
                System.out.println("censoFo  codEspecie");
                censoForestalDetalleEntity.setNombreCientifico(""+obj.get(1));
                System.out.println("censoFo 1");
                censoForestalDetalleEntity.setNombreComun(""+obj.get(2));
                System.out.println("censoFo 2");
                censoForestalDetalleEntity.setNombreNativo(""+obj.get(3));
                System.out.println("censoFo 3");
                censoForestalDetalleEntity.setNombreComercial(""+obj.get(4));
                System.out.println("censoFo 4");
                censoForestalDetalleEntity.setNombreAlterno(""+obj.get(5));
                System.out.println("censoFo 5");
                censoForestalDetalleEntity.setFamilia(""+obj.get(6));
                System.out.println("censoFo 6");
                censoForestalDetalleEntity.setCodigoDeArbol(""+obj.get(7));
                System.out.println("censoFo 7");
                censoForestalDetalleEntity.setNumeroCorrelativoArbol(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(8))));
                System.out.println("censoFo 8");
                censoForestalDetalleEntity.setDap(Double.parseDouble(""+obj.get(9)));
                System.out.println("censoFo 9");
                censoForestalDetalleEntity.setAlturaComercial(Double.parseDouble(""+obj.get(10)));
                System.out.println("censoFo 10");
                censoForestalDetalleEntity.setAlturaTotal(Double.parseDouble(""+obj.get(11)));
                System.out.println("censoFo 11");
                censoForestalDetalleEntity.setVolumen(Double.parseDouble(""+obj.get(12)));
                System.out.println("censoFo 12");
                censoForestalDetalleEntity.setCantidad(Double.parseDouble(""+obj.get(13)));
                System.out.println("censoFo 13");
                censoForestalDetalleEntity.setIdTipoArbol(""+obj.get(14));
                System.out.println("censoFo 14");
                censoForestalDetalleEntity.setFactorForma(Double.parseDouble(""+obj.get(15)));
                censoForestalDetalleEntity.setEste(""+obj.get(16));
                censoForestalDetalleEntity.setNorte(""+obj.get(17));
                System.out.println("censoFo 17");
                censoForestalDetalleEntity.setZona(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(18))));
                censoForestalDetalleEntity.setCodigoArbolCalidad(""+obj.get(19));
                System.out.println("censoFo 19");
                censoForestalDetalleEntity.setDescripcionOtros(""+obj.get(20));
                censoForestalDetalleEntity.setIdTipoRecurso(""+obj.get(21));
                censoForestalDetalleEntity.setProductoTipo(""+obj.get(22));
                System.out.println("censoFo 22");
                censoForestalDetalleEntity.setCategoriaDiametrica(Double.parseDouble(""+obj.get(23)));
                System.out.println("censoFo 23");
                censoForestalDetalleEntity.setFaja(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(24))));
                System.out.println("censoFo 24");
                censoForestalDetalleEntity.setUnidadTrabajo(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(25))));
                System.out.println("censoFo 25");
                censoForestalDetalleEntity.setParcelaCorta(""+obj.get(26));
                System.out.println("censoFo 26");
                censoForestalDetalleEntity.setCodigoUnidadMedida(""+obj.get(27));
                System.out.println("censoFo  27");
                censoForestalDetalleEntity.setMortanda(("SI".equals(""+obj.get(28)) || "Si".equals(""+obj.get(28))) ?true:false);
                censoForestalDetalleEntity.setBloqueQuinquenal(""+obj.get(30));
                System.out.println("censoFo 30");
                censoForestalDetalleEntity.setSuperficieComercial(Double.parseDouble(""+obj.get(31)));
                System.out.println("censoFo");
                censoForestalDetalleEntity.setCondicionArbol(""+obj.get(32));
                System.out.println("censoFo 32");
                censoForestalDetalleEntity.setIdTipoBosque(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(33))));
                System.out.println("censoFo 33");
                censoForestalDetalleEntity.setEstadoArbol(""+obj.get(34));
                System.out.println("censoFo 34");
                //  censoForestalDetalleEntity.setContrato(""+0);
                censoForestalDetalleEntity.setBosqueSecundario(""+obj.get(35));
                System.out.println("censoFo 35");
                if(tipoCensoForestal.trim().equals("Inventario"))
                    censoForestalDetalleEntity.setCodigoUnico("INV-00000-"+codigofecha);
                else
                    censoForestalDetalleEntity.setCodigoUnico(censoForestalDetalleEntity.getCodigoDeArbol()+"-00000-"+codigofecha);
                System.out.println("censoFo setCodigoUnico");
                censoForestalDetalleEntity.setImagen("");
                censoForestalDetalleEntity.setAudio("");
                System.out.println("censoForestalDetalleEntity  "+censoForestalDetalleEntity.toString());
                listCensoForestal.add(censoForestalDetalleEntity);
                filaCelda++;
            }
            if(listCensoForestal.size()>0){
                ResultClassEntity responseListaCenso = new ResultClassEntity<>();
                responseListaCenso=RegistrarIndividualCensoForestalDetalle(listCensoForestal, 0, 2);
                if(responseListaCenso.getData()!=null && responseListaCenso.getSuccess() && responseListaCenso.getData().toString().contains("Se registró la linea con exito.")){
                    result.setData(listCensoForestal);
                    result.setSuccess(true);
                    result.setMessage("Se registró correctamente.");
                }
                else{
                    result.setSuccess(false);
                    result.setMessage("No se registro."+responseListaCenso.getMessage());
                }
            }
            else{
                result.setSuccess(true);
                result.setMessage("No se encontro registros asociados");
            }
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage()+"\nFila :"+filaCelda);
        }
        return result;
    }

    @Override
    public ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>   registrarRecursosForestalesMaderableExcel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)
    throws Exception {
        ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> result = new ResultClassEntity<>();

        ResultadosRecursosForestalesMaderablesDto resultadosRecursosForestalesMaderablesDto=new  ResultadosRecursosForestalesMaderablesDto();
        resultadosRecursosForestalesMaderablesDto.setAreaTotalCensada("");
        resultadosRecursosForestalesMaderablesDto.setContadorEspecies("");
        resultadosRecursosForestalesMaderablesDto.setNumeroArbolesMaderables("");
        resultadosRecursosForestalesMaderablesDto.setNumeroArbolesNoMaderables("");
        resultadosRecursosForestalesMaderablesDto.setNumeroArbolesTotal("");
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, "Hoja1", 3, 1);
            List<ResultadosRFMTabla> lista = new ArrayList<ResultadosRFMTabla>();
            ResultadosRFMTabla temp =null;
            String nombreEspecie="";
            for(ArrayList obj : listaExcel) {
                nombreEspecie=""+obj.get(0);
                if(!nombreEspecie.equals("") ){
                    lista.add(temp);
                    temp = new ResultadosRFMTabla();
                    temp.setNombreEspecie(nombreEspecie);
                    temp.setDap30a39(""+ obj.get(2));
                    temp.setDap40a49(""+ obj.get(3));
                    temp.setDap50a59(""+ obj.get(4));
                    temp.setDap60a69(""+ obj.get(5));
                    temp.setDap70a79(""+ obj.get(6));
                    temp.setDap80a89(""+ obj.get(7));
                    temp.setDap90aMas(""+ obj.get(8));
                    temp.setDapTotalPC(""+ obj.get(9));
                    temp.setDapTotalporHa(""+ obj.get(10));
                }
                temp.setVol30a39(""+ obj.get(2));
                temp.setVol40a49(""+ obj.get(3));
                temp.setVol50a59(""+ obj.get(4));
                temp.setVol60a69(""+ obj.get(5));
                temp.setVol70a79(""+ obj.get(6));
                temp.setVol80a89(""+ obj.get(7));
                temp.setVol90aMas(""+ obj.get(8));
                temp.setVolTotalPC(""+ obj.get(9));
                temp.setVolTotalporHa(""+ obj.get(10));
            }
            if(lista !=null && lista.size()>0 ) {
                lista.add(temp);
                resultadosRecursosForestalesMaderablesDto.setResultadosRFMTablas(lista);
                result.setData(resultadosRecursosForestalesMaderablesDto);
                result.setSuccess(true);
                result.setMessage("Se registro Recursos Forestales Maderables");
            }
            else {
                result.setSuccess(false);
                result.setMessage("No existen datos registrdos  para el Plan de Manejo " + idPlanDeManejo + " Tipo de plan " + tipoPlan);
            }

            return result;
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
        }
        return result;
    }


    @Override
    public ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>   registrarEspecieMuestreoExcel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)
            throws Exception {
        ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
        List<ResultadosEspeciesMuestreoDto> lista = new ArrayList<ResultadosEspeciesMuestreoDto>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, "Hoja1", 3, 1);
            ResultadosEspeciesMuestreoDto  temp =null;
            List<TablaEspeciesMuestreo> listaEspeciesMuestreo = null;
            String tipoBosque="";
            for(ArrayList obj : listaExcel) {
                tipoBosque=""+obj.get(0);
                if(!tipoBosque.equals("") ){
                    if(listaEspeciesMuestreo!=null && listaEspeciesMuestreo.size()>0){
                        temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                        lista.add(temp);
                    }
                    temp = new ResultadosEspeciesMuestreoDto();
                    temp.setIdTipoBosque("");
                    temp.setNombreBosque(""+obj.get(0));
                    listaEspeciesMuestreo = new ArrayList<TablaEspeciesMuestreo>();
                }
                TablaEspeciesMuestreo temporal = new TablaEspeciesMuestreo();
                        temporal.setEspecies(""+obj.get(1));
                        temporal.setNumeroArbolesTotalBosque(""+obj.get(2));
                        temporal.setAreaBasalTotalBosque(""+obj.get(3));
                        temporal.setVolumenTotalBosque(""+obj.get(4));
                        temporal.setNumeroArbolesTotalHa(""+obj.get(5));
                        temporal.setAreaBasalTotalHa(""+obj.get(6));
                        temporal.setVolumenTotalHa(""+obj.get(7));
                        listaEspeciesMuestreo.add(temporal);

            }
            if(lista !=null && lista.size()>0 && listaEspeciesMuestreo!=null && listaEspeciesMuestreo.size()>0){
                    temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                    lista.add(temp);
            }else if(temp.getNombreBosque()!=null && listaEspeciesMuestreo!=null && listaEspeciesMuestreo.size()>0 ){
                temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                lista.add(temp);
            }

            if( lista!=null && lista.size()>0) {
                result.setData(lista);
                result.setSuccess(true);
                result.setMessage("lista Datos Especie Muestreo ");
            }
            else {
                result.setSuccess(false);
                result.setMessage("No existen datos registrdos  para el Plan de Manejo " + idPlanDeManejo + " Tipo de plan " + tipoPlan);
            }
            return result;
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
        }
        return result;
    }

    @Override
    public ResultClassEntity<List<ContratoTHDto>> ListarContratoTHMovil(ContratoTHDto data) throws Exception {
        ResultClassEntity<List<ContratoTHDto>> result = new ResultClassEntity<>();

        result.setData(censoForestalRepository.ListarContratoTHMovilObtener(data));
        result.setSuccess(true);
        result.setMessage("Listar Contrato TH Movil");

        return result;
    }

    @Override
    public ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>   registrarEspecieFrutalesExcel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)
            throws Exception {
        ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
        List<ResultadosEspeciesMuestreoDto> lista = new ArrayList<ResultadosEspeciesMuestreoDto>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, "Hoja1", 3, 1);
            ResultadosEspeciesMuestreoDto  temp =null;
            List<TablaEspeciesMuestreo> listaEspeciesMuestreo = null;
            String tipoBosque="";
            for(ArrayList obj : listaExcel) {
                tipoBosque=""+obj.get(0);
                if(!tipoBosque.equals("") ){
                    if(listaEspeciesMuestreo!=null && listaEspeciesMuestreo.size()>0){
                        temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                        lista.add(temp);
                    }
                    temp = new ResultadosEspeciesMuestreoDto();
                    temp.setIdTipoBosque("");
                    temp.setNombreBosque(""+obj.get(0));
                    listaEspeciesMuestreo = new ArrayList<TablaEspeciesMuestreo>();
                }
                TablaEspeciesMuestreo temporal = new TablaEspeciesMuestreo();
                temporal.setEspecies(""+obj.get(1));
                temporal.setNumeroArbolesTotalBosque(""+obj.get(2));
                temporal.setAreaBasalTotalBosque(""+obj.get(3));
                temporal.setNumeroArbolesTotalHa(""+obj.get(4));
                temporal.setAreaBasalTotalHa(""+obj.get(5));
                listaEspeciesMuestreo.add(temporal);

            }
            if(lista !=null && lista.size()>0 && listaEspeciesMuestreo!=null && listaEspeciesMuestreo.size()>0){
                temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                lista.add(temp);
            }else if(temp.getNombreBosque()!=null && listaEspeciesMuestreo!=null && listaEspeciesMuestreo.size()>0 ){
                temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                lista.add(temp);
            }

            if( lista!=null && lista.size()>0) {
                result.setData(lista);
                result.setSuccess(true);
                result.setMessage("lista Datos Especie Frutales ");
            }
            else {
                result.setSuccess(false);
                result.setMessage("No existen datos registrdos  para el Plan de Manejo " + idPlanDeManejo + " Tipo de plan " + tipoPlan);
            }
            return result;
        }catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
        }
        return result;
    }


    @Override
    public ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo2VolumenTotal(Integer idPlanManejo, String tipoEspecie) {
        return censoForestalRepository.ListarCensoForestalAnexo2VolumenTotal(idPlanManejo,tipoEspecie);
    }

    @Override
    public ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo3(Integer idPlanManejo, String tipoEspecie) {
        return censoForestalRepository.ListarCensoForestalAnexo3(idPlanManejo,tipoEspecie);
    }

    @Override
    public ResultEntity<CensoForestalDetalleDto> listarEspecieXCenso(CensoForestalDto censoForestalDto)  throws Exception{
        return censoForestalRepository.listarEspecieXCenso(censoForestalDto);
    }

    @Override
    public  ResultClassEntity ListarProyeccionCosecha(Integer idPlanManejo, String tipoPlan){
        return censoForestalRepository.ListarProyeccionCosecha(idPlanManejo,tipoPlan);
    }
    @Override
    public  ResultClassEntity listarVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan){
        return censoForestalRepository.listarVolumenComercialPromedio(idPlanManejo,tipoPlan);
    }

    @Override
    public  ResultClassEntity listarEspecieVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan){
        return censoForestalRepository.listarEspecieVolumenComercialPromedio(idPlanManejo,tipoPlan);
    }

    @Override
    public  ResultClassEntity listarEspecieVolumenCortaAnualPermisible(Integer idPlanManejo, String tipoPlan){
        return censoForestalRepository.listarEspecieVolumenCortaAnualPermisible(idPlanManejo,tipoPlan);
    }

    @Override
    public  ResultClassEntity listarRecursosMaderablesCensoForestalDetalle(CensoForestalDetalleEntity request)throws Exception {
        return censoForestalDetalleRepository.listarRecursosMaderablesCensoForestalDetalle(request);
    }
    @Override
    public  ResultClassEntity listarRecursosDiferentesCensoForestalDetalle(CensoForestalDetalleEntity request)throws Exception {
        return censoForestalDetalleRepository.listarRecursosDiferentesCensoForestalDetalle(request);
    }
    @Override
    public  ResultClassEntity listarCensoForestalDetalleAnexo(CensoForestalDetalleEntity request) throws Exception {
        return censoForestalDetalleRepository.listarCensoForestalDetalleAnexo(request);
    }
    @Override
    public  ResultClassEntity actualizarCensoForestal(CensoForestalEntity request) throws Exception {

        return censoForestalRepository.actualizarCensoForestal(request);
    }

    @Override
    public  ResultClassEntity listarSincronizacionCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception {
        return censoForestalDetalleRepository.listarSincronizacionCensoForestalDetalle(request);
    }
}
