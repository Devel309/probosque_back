package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;
import pe.gob.serfor.mcsniffs.repository.MesaPartesDetalleRepository;
import pe.gob.serfor.mcsniffs.service.MesaPartesDetalleService;



@Service("MesaPartesDetalleService")
public class MesaPartesDetalleServiceImpl implements MesaPartesDetalleService {

    private static final Logger log = LogManager.getLogger(MesaPartesDetalleServiceImpl.class);


    
    @Autowired
    private MesaPartesDetalleRepository mesaPartesDetalleRepository;

    @Override
    public ResultClassEntity<List<MesaPartesDetalleDto>> listarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception {   

        List<MesaPartesDetalleDto> lista = mesaPartesDetalleRepository.listarMesaPartesDetalle(dto);
        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
             
        return result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarMesaPartesDetalle(List<MesaPartesDetalleDto> dto) throws Exception {

        ResultClassEntity result = null;
        for (MesaPartesDetalleDto element : dto) {
            result = mesaPartesDetalleRepository.registrarMesaPartesDetalle(element);
        }

        return  result;
    }


    @Override
    public ResultClassEntity eliminarMesaPartesDetalle(List<MesaPartesDetalleDto> dto) throws Exception{
        ResultClassEntity result = null;
        for (MesaPartesDetalleDto element : dto) {
            result = mesaPartesDetalleRepository.eliminarMesaPartesDetalle(element);
        }
        return  result;
    }
 
 
 
}
