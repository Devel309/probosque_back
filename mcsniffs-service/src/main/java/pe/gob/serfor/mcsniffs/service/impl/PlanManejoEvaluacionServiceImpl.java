package pe.gob.serfor.mcsniffs.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.NotificacionDTO;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.PlanManejoEvaluacionService;
import pe.gob.serfor.mcsniffs.service.PlanManejoService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;



@Service("PlanManejoEvaluacionService")
public class PlanManejoEvaluacionServiceImpl implements PlanManejoEvaluacionService {

    private static final Logger log = LogManager.getLogger(PlanManejoEvaluacionServiceImpl.class);


    @Autowired
    private GenericoRepository genericoRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private PlanManejoEvaluacionSubDetalleRepository planManejoEvaluacionSubDetalleRepository;
    @Autowired
    private PlanManejoRepository planManejoRepository;
    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    private PlanManejoService planManejoService;

    @Autowired
    private ResolucionRepository resolucionRepository;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

    @Override
    public ResultClassEntity<List<PlanManejoEvaluacionDto>> listarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto)
            throws Exception {

        ResultClassEntity<List<PlanManejoEvaluacionDto>> result = new ResultClassEntity<>();

        result.setData(planManejoEvaluacionRepository.listarPlanManejoEvaluacion(planManejoEvaluacionDto));
        result.setSuccess(true);
        result.setMessage("Lista Plan Manejo Evaluacion");

        return result;
    }


    @Override
    public ResultClassEntity EliminarPlanManejoEvaluacion(PlanManejoEvaluacionDto param) throws Exception {
        return  planManejoEvaluacionRepository.eliminarPlanManejoEvaluacion(param);
    }

    @Override
    public ResultClassEntity ActualizarPlanManejoEvaluacionEstado(PlanManejoEvaluacionDto param) throws Exception {
        return planManejoEvaluacionRepository.ActualizarPlanManejoEvaluacionEstado(param);
    }

    @Override
    public ResultClassEntity ActualizarEvaluacionCampoPlanManejoEvaluacion(PlanManejoEvaluacionDto param){
        return planManejoEvaluacionRepository.ActualizarEvaluacionCampoPlanManejoEvaluacion(param);
    }

    /**
     * @autor: Renzo Gabriel Meneses Gamarra [24-09-2021]
     * @modificado:
     * @descripción: {}
     *
     */

    @Override
    public ResultEntity<PlanManejoEvaluacionDetalleDto> ListarPlanManejoEvaluacionDetalle(Integer idPlanManejoEval) {

        ResultEntity<PlanManejoEvaluacionDetalleDto> result = new ResultEntity<PlanManejoEvaluacionDetalleDto>();

        List<PlanManejoEvaluacionDetalleDto> lista = planManejoEvaluacionDetalleRepository.listarPlanManejoEvaluacionDet(idPlanManejoEval);

            for (PlanManejoEvaluacionDetalleDto element : lista) {
                
                element.setLstPlanManejoEvaluacionSubDetalle(planManejoEvaluacionSubDetalleRepository.listarPlanManejoEvaluacionSubDetalle(element.getIdPlanManejoEvalDet()));
            }

        result.setData(lista);
        result.setIsSuccess(true);
        result.setMessage("Se listo la información de plan manejo evaluacion detalle");


        return result;
    }

    @Override
    public ResultClassEntity ListarPlanManejoEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param) {
        return planManejoEvaluacionDetalleRepository.ListarPlanManejoEvaluacionLineamiento(param);
    }

    @Override
    public ResultClassEntity RegistrarPlanManejoEvaluacionLineamiento(List<PlanManejoEvaluacionDetalleDto> param) {
        return planManejoEvaluacionDetalleRepository.RegistrarPlanManejoEvaluacionLineamiento(param);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarPlanManejoEvaluacionDetalle(List<PlanManejoEvaluacionDetalleDto> listaPlanManejoEvaluacionDetalle) throws Exception {

        ResultClassEntity result = null;
        for (PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalle : listaPlanManejoEvaluacionDetalle) {
            result = planManejoEvaluacionDetalleRepository.registrarPlanManejoEvaluacionDet(planManejoEvaluacionDetalle);


            if (planManejoEvaluacionDetalle.getLstPlanManejoEvaluacionSubDetalle()!=null) {
                for (PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalle : planManejoEvaluacionDetalle.getLstPlanManejoEvaluacionSubDetalle()) {
                    planManejoEvaluacionSubDetalle.setIdUsuarioRegistro(planManejoEvaluacionDetalle.getIdUsuarioRegistro());
                    planManejoEvaluacionSubDetalle.setIdPlanManejoEvalDet(planManejoEvaluacionDetalle.getIdPlanManejoEvalDet());
                    planManejoEvaluacionSubDetalleRepository.registrarPlanManejoEvaluacionSubDetalle(planManejoEvaluacionSubDetalle);
                }
            }
        }

        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarPlanManejoEvaluacionDetalle(List<PlanManejoEvaluacionDetalleDto> listaPlanManejoEvaluacionDetalle) throws Exception {

        ResultClassEntity result = null;

        if(listaPlanManejoEvaluacionDetalle.size()>0){
            PlanManejoEvaluacionDto planManejoEvaluacion = new PlanManejoEvaluacionDto();
            planManejoEvaluacion.setIdPlanManejoEval(listaPlanManejoEvaluacionDetalle.get(0).getIdPlanManejoEvaluacion());
            planManejoEvaluacion.setPageNum(1);
            planManejoEvaluacion.setPageSize(1);
            List<PlanManejoEvaluacionDto> rsEvaluacion = planManejoEvaluacionRepository.listarPlanManejoEvaluacion(planManejoEvaluacion);
            PlanManejoEvaluacionDto planEval=new PlanManejoEvaluacionDto();
            if (rsEvaluacion!=null && rsEvaluacion.size()>0) {
                planEval= rsEvaluacion.get(0);
            }

            if(planEval.getEstadoPlanManejoEvaluacion()!=null) {
                if(planEval.getEstadoPlanManejoEvaluacion().equals("EEVARECI")){
                    cambiarEstadoPlanManejoEvaluacion(planEval);
                }
            }
        }


        for (PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalle : listaPlanManejoEvaluacionDetalle) {
            result = planManejoEvaluacionDetalleRepository.actualizarPlanManejoEvaluacionDet(planManejoEvaluacionDetalle);
            if (planManejoEvaluacionDetalle.getLstPlanManejoEvaluacionSubDetalle()!=null) {
                for (PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalle : planManejoEvaluacionDetalle.getLstPlanManejoEvaluacionSubDetalle()) {
                    planManejoEvaluacionSubDetalle.setIdUsuarioModificacion(planManejoEvaluacionDetalle.getIdUsuarioModificacion());
                    planManejoEvaluacionSubDetalleRepository.actualizarPlanManejoEvaluacionSubDetalle(planManejoEvaluacionSubDetalle);
                }
            }
        }
        return result;
    }

    @Override
    public ResultClassEntity obtenerEvaluacionPlanManejo(Integer idPlanManejo) throws Exception {
        return planManejoEvaluacionDetalleRepository.obtenerEvaluacionPlanManejo(idPlanManejo);
    }

    @Override
    public ResultClassEntity notificarObservacionesAlTitular(NotificacionDTO notificacionDTO) throws Exception{

        ResultClassEntity envio = new ResultClassEntity();
        String correo = notificacionDTO.getCorreoTitularRegente();
        String nombreTitular = notificacionDTO.getNombreTitularRegente();
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

        if(notificacionDTO.getNroObservaciones() > 0) {
            if (correo!=null) {
                String[] mails=new String[1];
                mails[0]= correo;
                EmailEntity mail=new EmailEntity();
                mail.setSubject("Notificación de Observaciones");
                mail.setEmail(mails);
                String body="<html><head>"
                        +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                        +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                        +"</head>"
                        +"<body lang=ES-PE style='word-wrap:break-word'>"
                        +"<div class=WordSection1>"
                        +"<p class=MsoNormal><span lang=ES>Buenos días  </span> "+nombreTitular+":</p>"
                        +"<p class=MsoNormal><span lang=ES>  Se le informa que el plan de manejo nro "+notificacionDTO.getIdPlanManejo()+" ha sido observado por el evaluador</span></p>"
                        +"<br><br><br>"
                        +"<p class=MsoNormal><b><i><span lang=ES>Atte. MCSniffs</span></i></b><i></i></p>"
                        +"</div></body></html>";

                mail.setContent(body);
                Boolean smail = emailService.sendEmail(mail);
                if(smail){
                    envio.setMessage("Correo Enviado Correctamente.");
                    envio.setSuccess(true);
                }
            }

        }else{
            if (correo!=null) {
                String[] mails=new String[1];
                mails[0]= correo;
                EmailEntity mail=new EmailEntity();
                mail.setSubject("Notificación de Aprobación");
                mail.setEmail(mails);
                String body="<html><head>"
                        +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                        +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                        +"</head>"
                        +"<body lang=ES-PE style='word-wrap:break-word'>"
                        +"<div class=WordSection1>"
                        +"<p class=MsoNormal><span lang=ES>Buenos días  </span> "+nombreTitular+":</p>"
                        +"<p class=MsoNormal><span lang=ES>  Se le informa que el plan de manejo nro "+notificacionDTO.getIdPlanManejo()+" ha sido aprobado por el evaluador</span></p>"
                        +"<br><br><br>"
                        +"<p class=MsoNormal><b><i><span lang=ES>Atte. MCSniffs</span></i></b><i></i></p>"
                        +"</div></body></html>";

                mail.setContent(body);
                Boolean smail = emailService.sendEmail(mail);
                if(smail){
                    envio.setMessage("Correo Enviado Correctamente.");
                    envio.setSuccess(true);
                }
            }
        }


        return envio;
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarPlanManejoEvaluacionArchivo(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto, MultipartFile fileEvaluacion, String idTipoDocumento) throws Exception {

        ResultClassEntity result = null;

        
        if (fileEvaluacion!=null) {
            result = serArchivo.RegistrarArchivoGeneral(fileEvaluacion, planManejoEvaluacionDetalleDto.getIdUsuarioRegistro(), idTipoDocumento);
            if(result.getSuccess()){
                planManejoEvaluacionDetalleDto.setIdArchivo(result.getCodigo());
            } 
        }

        if (planManejoEvaluacionDetalleDto.getIdPlanManejoEvalDet()==null) {
            result = planManejoEvaluacionDetalleRepository.registrarPlanManejoEvaluacionDet(planManejoEvaluacionDetalleDto);
        }else{
            result = planManejoEvaluacionDetalleRepository.actualizarPlanManejoEvaluacionDet(planManejoEvaluacionDetalleDto);
        }     
        return result;
    }

    @Override
    public ResultClassEntity actualizarEstadoPlanManejoEvaluacion(PlanManejoEvaluacionDto dto) throws Exception {
        return planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(dto);
    }

    @Override
    public ResultClassEntity obtenerUltimaEvaluacion(Integer idPlanManejo) throws Exception{
    
        return planManejoEvaluacionRepository.obtenerUltimaEvaluacion(idPlanManejo);
    }

    @Override
    public ResultClassEntity enviarEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception{

        ResultClassEntity result = new ResultClassEntity();
        if(planManejoEvaluacionDto.getEvaluado()){


            String estadoPlanFin="";
            String estadoEvaluacionFin="";
            PlanManejoEvaluacionDto planManejoEvaluacion = new PlanManejoEvaluacionDto();
            planManejoEvaluacion.setIdPlanManejoEval(planManejoEvaluacionDto.getIdPlanManejoEval());
            planManejoEvaluacion.setPageNum(1);
            planManejoEvaluacion.setPageSize(1);
            List<PlanManejoEvaluacionDto> rsEvaluacion = planManejoEvaluacionRepository.listarPlanManejoEvaluacion(planManejoEvaluacion);
            PlanManejoEvaluacionDto planEval=new PlanManejoEvaluacionDto();
            if (rsEvaluacion!=null && rsEvaluacion.size()>0) {
                planEval= rsEvaluacion.get(0);
            }

            if(planEval.getEstadoPlanManejoEvaluacion()!=null) {
                PlanManejoDto planManejoActualiza = new PlanManejoDto();
                Date fechaActual=new Date();
                ResolucionDto param=new ResolucionDto();
                switch (planManejoEvaluacionDto.getEstadoEvaluacion()) {


                    case "EPLMAPROB":
                            estadoPlanFin = "EPLMAPROB";
                            estadoEvaluacionFin = "EEVARESOLA";
                            planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                            planManejoActualiza.setCodigoEstado(estadoPlanFin);
                            planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);
                            planEval.setIdPlanManejoEval(planEval.getIdPlanManejoEval());
                            planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                            planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planEval);


                            param.setTipoDocumentoGestion("TPMPGMF");
                            param.setNroDocumentoGestion(planEval.getIdPlanManejo());
                            param.setIdPlanManejoEvaluacion(planEval.getIdPlanManejoEvaluacion());

                            param.setFechaResolucion(fechaActual);
                            param.setEstadoResolucion("ERESGEN");
                            String comentarioRes=estadoEvaluacionFin.equals("EEVARESOLA")?"RESOLUCIÓN APROBADA":"RESOLUCIÓN DENEGADA";

                            param.setComentarioResolucion(comentarioRes);
                            param.setIdUsuarioRegistro(planManejoEvaluacionDto.getIdUsuarioModificacion());
                            resolucionRepository.registrarResolucion(param);
                            break;

                        case "EPLMBORD":
                            estadoPlanFin = "EPLMBORD";
                            estadoEvaluacionFin = "EEVARESOLD";
                            planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                            planManejoActualiza.setCodigoEstado(estadoPlanFin);
                            planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);
                            planEval.setIdPlanManejoEval(planEval.getIdPlanManejoEval());
                            planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                            planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planEval);


                            param.setTipoDocumentoGestion("TPMPGMF");
                            param.setNroDocumentoGestion(planEval.getIdPlanManejo());

                            param.setFechaResolucion(fechaActual);
                            param.setEstadoResolucion("ERESGEN");
                            param.setComentarioResolucion("RESOLUCIÓN DENEGADA");
                            param.setIdUsuarioRegistro(planManejoEvaluacionDto.getIdUsuarioModificacion());
                            param.setIdPlanManejoEvaluacion(planManejoEvaluacionDto.getIdPlanManejoEval());
                            resolucionRepository.registrarResolucion(param);


                            break;

                        case "EPLMBORN":
                            estadoPlanFin = "EPLMBORN";
                            estadoEvaluacionFin = "EEVANPRE";;
                            planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                            planManejoActualiza.setCodigoEstado(estadoPlanFin);
                            planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);
                            planEval.setIdPlanManejoEval(planEval.getIdPlanManejoEval());
                            planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                            planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planEval);
                            break;
                }
            }

        }
        else{
            cambiarEstadoPlanManejoEvaluacion(planManejoEvaluacionDto);
        }
        result.setMessage("Se envia evaluación correctamente.");
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity actualizarIdSolicitudOpcionPlanManejoEvaluacion(PlanManejoEvalSolicitudOpinionDto planManejoEvalSolicitudOpinionDto) throws Exception {
        return planManejoEvaluacionRepository.actualizarIdSolicitudOpcionPlanManejoEvaluacion(planManejoEvalSolicitudOpinionDto);
    }
    @Override
    public ResultClassEntity listarplanManejoEvaluacionEspecie(PlanManejoEvaluacionDto request) throws Exception {
        return planManejoEvaluacionSubDetalleRepository.listarplanManejoEvaluacionEspecie(request);
    }

    public  ResultClassEntity cambiarEstadoPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception{

        ResultClassEntity result = new  ResultClassEntity();

        String estadoPlanFin="";
        String estadoEvaluacionFin="";
        PlanManejoEvaluacionDto planManejoEvaluacion = new PlanManejoEvaluacionDto();
        planManejoEvaluacion.setIdPlanManejoEval(planManejoEvaluacionDto.getIdPlanManejoEval());
        planManejoEvaluacion.setPageNum(1);
        planManejoEvaluacion.setPageSize(1);
        List<PlanManejoEvaluacionDto> rsEvaluacion = planManejoEvaluacionRepository.listarPlanManejoEvaluacion(planManejoEvaluacion);
        PlanManejoEvaluacionDto planEval=new PlanManejoEvaluacionDto();
        if (rsEvaluacion!=null && rsEvaluacion.size()>0) {
            planEval= rsEvaluacion.get(0);
        }

        if(planEval.getEstadoPlanManejoEvaluacion()!=null) {
            PlanManejoDto planManejoActualiza = new PlanManejoDto();
            Integer maximoObsEval = 0;
            ParametroEntity objParametroEntity = new ParametroEntity();
            objParametroEntity.setIdTipoParametro(93);
            List<ParametroEntity> listaParametro = genericoRepository.ListarPorFiltroParametro(objParametroEntity);

            if (listaParametro==null || !listaParametro.isEmpty()) {
                objParametroEntity = listaParametro.get(0);
            }
            switch (planEval.getEstadoPlanManejoEvaluacion()) {

                case "EEVAPRES": //presentado


                    if (planManejoEvaluacionDto.getConformeReqTupa()!=null && planManejoEvaluacionDto.getConformeReqTupa()) {
                        estadoEvaluacionFin="EEVARECI"; //recibido

                        maximoObsEval=0;
                    }else{
                        estadoEvaluacionFin="EEVAOBS"; //observado
                        maximoObsEval= planEval.getMaximoObs()==null? 1 : planEval.getMaximoObs()+1;

                    }

                    if (Integer.parseInt(objParametroEntity.getValorSecundario()) == maximoObsEval) {
                        estadoEvaluacionFin ="EEVARESOLD";
                    }

                    planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                    planEval.setMaximoObs(maximoObsEval);
                    planEval.setIdUsuarioModificacion(planManejoEvaluacionDto.getIdUsuarioModificacion());
                    planManejoEvaluacionRepository.actualizarPlanManejoEvaluacion(planEval);

                    if (planManejoEvaluacionDto.getConformeReqTupa()!=null && planManejoEvaluacionDto.getConformeReqTupa()) {
                        estadoPlanFin="EPLMRECI";
                    } else {
                        estadoPlanFin="EPLMOBSM";
                        Gson gson = new Gson();
                        UsuarioDto user= new UsuarioDto();
                        user.setIdusuario(planEval.getIdUsuarioRegistro());
                        String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
                        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), token);
                        ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);

                        String correoElectronico = null;
                        String nombreCompleto = null;
                        for(Object item: resultApiExt.getData().stream().toArray()){
                            LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
                            correoElectronico = val.get("correoElectronico").toString();
                            nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
                        }

                        if(correoElectronico!=null && nombreCompleto!=null) {
                            String[] mails = new String[1];
                            mails[0] = correoElectronico;
                            EmailEntity mail = new EmailEntity();
                            mail.setSubject("SERFOR::OBSERVADO POR MESA DE PARTES PLAN Nº "+planManejoEvaluacionDto.getIdPlanManejo());
                            mail.setEmail(mails);
                            String body="<html><head>"
                                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                                    +"</head>"
                                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                                    +"<div class=WordSection1>"
                                    +"<p class=MsoNormal><span lang=ES>Estimado(a): "+nombreCompleto.toUpperCase()+" </span></p>"
                                    +"<p class=MsoNormal><span lang=ES>  Resolver las observaciones del Plan Manejo N° : "+planManejoEvaluacionDto.getIdPlanManejo()+" </span></p>"
                                    +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                                    +"</div></body></html>";
                            mail.setContent(body);
                            Boolean smail = emailService.sendEmail(mail);
                        }
                    }
                    
                    if (Integer.parseInt(objParametroEntity.getValorSecundario()) == maximoObsEval) {
                        estadoPlanFin ="EPLMBORD";
                        planManejoService.actualizarConformePlanManejoArchivo(planEval.getIdPlanManejo(),planManejoEvaluacionDto.getIdUsuarioModificacion());
                    }
                    planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                    planManejoActualiza.setCodigoEstado(estadoPlanFin);

                    planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);

                    if(estadoEvaluacionFin.equals("EEVARESOLD")){
                        ResolucionDto param=new ResolucionDto();

                        param.setTipoDocumentoGestion("TPMPGMF");
                        param.setNroDocumentoGestion(planEval.getIdPlanManejo());
                        Date fechaActual=new Date();
                        param.setFechaResolucion(fechaActual);
                        param.setEstadoResolucion("ERESGEN");
                        param.setComentarioResolucion("RESOLUCIÓN DENEGADA");
                        param.setIdUsuarioRegistro(planManejoEvaluacionDto.getIdUsuarioModificacion());
                        param.setIdPlanManejoEvaluacion(planManejoEvaluacionDto.getIdPlanManejoEval());
                        resolucionRepository.registrarResolucion(param);
                    }

                break;

                case "EEVAEVAL":

                    if (planManejoEvaluacionDto.getConformeConsideraciones()) {

                            estadoPlanFin = "EPLMAPROB";
                            estadoEvaluacionFin = "EEVARESOLA";
                            planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                            planManejoActualiza.setCodigoEstado(estadoPlanFin);
                            planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);
                            planEval.setIdPlanManejoEval(planEval.getIdPlanManejoEval());
                            planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                            planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planEval);


                    } else {
                        //CONSIDERACIONES RESTRICTIVAS
                        if(planManejoEvaluacionDto.getObservacionRestrictiva() !=null && planManejoEvaluacionDto.getObservacionRestrictiva()){
                            estadoPlanFin = "EPLMBORD";
                            estadoEvaluacionFin = "EEVARESOLD";

                        } else {

                            estadoPlanFin = "EPLMOBSG";
                            estadoEvaluacionFin = "EEVAOBS";

                            Gson gson = new Gson();
                            UsuarioDto user= new UsuarioDto();
                            user.setIdusuario(planEval.getIdUsuarioRegistro());
                            String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
                            ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), token);
                            ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);

                            String correoElectronico = null;
                            String nombreCompleto = null;
                            for(Object item: resultApiExt.getData().stream().toArray()){
                                LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
                                correoElectronico = val.get("correoElectronico").toString();
                                nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
                            }

                            //esto es solo pruebas
                            if(correoElectronico!=null && nombreCompleto!=null) {
                                String[] mails = new String[1];
                                mails[0] = correoElectronico;
                                EmailEntity mail = new EmailEntity();
                                mail.setSubject("SERFOR::OBSERVADO PLAN Nº "+planManejoEvaluacionDto.getIdPlanManejo());
                                mail.setEmail(mails);
                                String body="<html><head>"
                                        +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                                        +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                                        +"</head>"
                                        +"<body lang=ES-PE style='word-wrap:break-word'>"
                                        +"<div class=WordSection1>"
                                        +"<p class=MsoNormal><span lang=ES>Estimado(a): "+nombreCompleto.toUpperCase()+" </span></p>"
                                        +"<p class=MsoNormal><span lang=ES>  Resolver las observaciones del Plan Manejo N° : "+planManejoEvaluacionDto.getIdPlanManejo()+" </span></p>"
                                        +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                                        +"</div></body></html>";
                                mail.setContent(body);
                                Boolean smail = emailService.sendEmail(mail);
                            }
                            maximoObsEval= planEval.getMaximoObs()==null? 1 : planEval.getMaximoObs()+1;
                            planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                            planEval.setMaximoObs(maximoObsEval);
                            planEval.setIdUsuarioModificacion(planManejoEvaluacionDto.getIdUsuarioModificacion());
                            planManejoEvaluacionRepository.actualizarPlanManejoEvaluacion(planEval);

                            if (Integer.parseInt(objParametroEntity.getValorSecundario()) == maximoObsEval) {
                                estadoPlanFin = "EPLMBORD";
                                estadoEvaluacionFin = "EEVARESOLD";
                            }
                        }

                        planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                        planManejoActualiza.setCodigoEstado(estadoPlanFin);
                        planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);
                        planEval.setIdPlanManejoEval(planEval.getIdPlanManejoEval());
                        planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                        planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planEval);

                    }

                    if(estadoEvaluacionFin.equals("EEVARESOLA") || estadoEvaluacionFin.equals("EEVARESOLD")){
                        ResolucionDto param=new ResolucionDto();

                        param.setTipoDocumentoGestion("TPMPGMF");
                        param.setNroDocumentoGestion(planEval.getIdPlanManejo());
                        param.setIdPlanManejoEvaluacion(planEval.getIdPlanManejoEvaluacion());
                        Date fechaActual=new Date();
                        param.setFechaResolucion(fechaActual);
                        param.setEstadoResolucion("ERESGEN");

                        String comentarioRes=estadoEvaluacionFin.equals("EEVARESOLA")?"RESOLUCIÓN APROBADA":"RESOLUCIÓN DENEGADA";

                        param.setComentarioResolucion(comentarioRes);
                        param.setIdUsuarioRegistro(planManejoEvaluacionDto.getIdUsuarioModificacion());
                        resolucionRepository.registrarResolucion(param);
                    }

                break;
                case "EEVARECI":

                        estadoPlanFin = "EPLMEEVAL";
                        estadoEvaluacionFin = "EEVAEVAL";
                        planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                        planManejoActualiza.setCodigoEstado(estadoPlanFin);
                        planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);
                        planEval.setIdPlanManejoEval(planEval.getIdPlanManejoEval());
                        planEval.setEstadoPlanManejoEvaluacion(estadoEvaluacionFin);
                        planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planEval);

                    break;
                case "EEVARESOLA": //Resuelto Aprobado

                    estadoPlanFin = "EPLMAPRON";

                    planManejoActualiza.setIdPlanManejo(planEval.getIdPlanManejo());
                    planManejoActualiza.setCodigoEstado(estadoPlanFin);

                    planManejoRepository.actualizarPlanManejoEstado(planManejoActualiza);

                    Gson gson = new Gson();
                    UsuarioDto user= new UsuarioDto();
                    user.setIdusuario(planEval.getIdUsuarioRegistro());
                    String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
                    ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), token);
                    ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);

                    String correoElectronico = null;
                    String nombreCompleto = null;
                    for(Object item: resultApiExt.getData().stream().toArray()){
                        LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
                        correoElectronico = val.get("correoElectronico").toString();
                        nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
                    }


                    if(correoElectronico!=null && nombreCompleto!=null) {
                        String[] mails = new String[1];
                        mails[0] = correoElectronico;
                        EmailEntity mail = new EmailEntity();
                        mail.setSubject("SERFOR::APROBADO PLAN Nº "+planManejoEvaluacionDto.getIdPlanManejo());
                        mail.setEmail(mails);
                        String body="<html><head>"
                                +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                                +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                                +"</head>"
                                +"<body lang=ES-PE style='word-wrap:break-word'>"
                                +"<div class=WordSection1>"
                                +"<p class=MsoNormal><span lang=ES>Estimado(a): "+nombreCompleto.toUpperCase()+" </span></p>"
                                +"<p class=MsoNormal><span lang=ES>  El Plan Manejo N° : "+planManejoEvaluacionDto.getIdPlanManejo()+" a sido Aprobado</span></p>"
                                +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                                +"</div></body></html>";
                        mail.setContent(body);
                        Boolean smail = emailService.sendEmail(mail);
                    }


                break;
            }
        }

        return result;

    }

    @Override
    public ResultClassEntity remitirNotificacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception{

        ResultClassEntity result = new ResultClassEntity();

        cambiarEstadoPlanManejoEvaluacion(planManejoEvaluacionDto);

        result.setMessage("Se remite notificación");
        result.setSuccess(true);
        return result;
    }
    @Override
    public ResultClassEntity notificarTitular(PlanManejoEvaluacionDto planEval) throws Exception{
        ResultClassEntity envio = new ResultClassEntity();
        Gson gson = new Gson();
        UsuarioDto user= new UsuarioDto();
        user.setIdusuario(planEval.getIdUsuarioRegistro());
        String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), token);
        ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);

        String correoElectronico = null;
        String nombreCompleto = null;
        for(Object item: resultApiExt.getData().stream().toArray()){
            LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
            correoElectronico = val.get("correoElectronico").toString();
            nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
        }
        if(correoElectronico!=null && nombreCompleto!=null) {
            String[] mails = new String[1];
            mails[0] = correoElectronico;
            EmailEntity mail = new EmailEntity();
            mail.setSubject("SERFOR::PLAN Nº "+planEval.getIdPlanManejo());
            mail.setEmail(mails);
            String body="<html><head>"
                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                    +"</head>"
                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                    +"<div class=WordSection1>"
                    +"<p class=MsoNormal><span lang=ES>Estimado(a): "+nombreCompleto.toUpperCase()+" </span></p>"
                    +"<p class=MsoNormal><span lang=ES>  No se encontraron observaciones Plan Manejo N° : "+planEval.getIdPlanManejo()+" </span></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                    +"</div></body></html>";
            mail.setContent(body);
            Boolean smail = emailService.sendEmail(mail);

            envio=   planManejoEvaluacionRepository.ActualizarNotificacionPlanManejoEvaluacion(planEval);
        }

        return envio;
    }

    @Override
    public ResultClassEntity validarLineamiento(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception {

        return planManejoEvaluacionRepository.validarLineamiento(planManejoEvaluacionDto);

    }
}
