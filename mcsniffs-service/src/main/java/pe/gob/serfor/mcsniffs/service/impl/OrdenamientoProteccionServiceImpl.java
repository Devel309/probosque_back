package pe.gob.serfor.mcsniffs.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionCategoriaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionDivAdmUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProyecionVigilanciaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.entity.util.codigos.CodigosPGMF;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoProteccionRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.repository.util.Urls.ordenamientoProteccion;
import pe.gob.serfor.mcsniffs.service.OrdenamientoProteccionService;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad   de la base de datos mcsniffs}
 *
 */
@Service("OrdenamientoProteccionService")
public class OrdenamientoProteccionServiceImpl implements OrdenamientoProteccionService {
    // private static final Logger log = LogManager.getLogger(OrdenamientoProteccionServiceImpl.class);

    @Autowired
    private OrdenamientoProteccionRepository repo;


    @Override
    public ResultEntity RegistrarOrdenamientoProteccionCategoria(List<OrdenProteccionCategoriaUMFEntity> request) {
        // TODO Auto-generated method stub
        return repo.RegistrarOrdenamientoProteccionCategoria(request);
    }

    @Override
    public ResultEntity EliminarOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) {
        // TODO Auto-generated method stub
        return repo.EliminarOrdenamientoProteccionCategoria(request);
    }

    @Override
    public ResultEntity ObtenerOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) {
        // TODO Auto-generated method stub
        return repo.ObtenerOrdenamientoProteccionCategoria(request);
    }

    @Override
    public ResultEntity RegistrarOrdenamientoProteccionDivAdm(
            List<OrdenProteccionDivAdmUMFEntity> request) {
        // TODO Auto-generated method stub
        return repo.RegistrarOrdenamientoProteccionDivAdm(request);
    }

    @Override
    public ResultEntity EliminarOrdenamientoProteccionDivAdm(
            OrdenProteccionDivAdmUMFEntity request) {
        // TODO Auto-generated method stub
        return repo.EliminarOrdenamientoProteccionDivAdm(request);
    }

    @Override
    public ResultEntity ObtenerOrdenamientoProteccionDivAdm(
            OrdenProteccionDivAdmUMFEntity request) {
        // TODO Auto-generated method stub
        return repo.ObtenerOrdenamientoProteccionDivAdm(request);
    }

    @Override
    public ResultEntity RegistrarOrdenamientoProteccionVigilancia(List<OrdenProyecionVigilanciaUMFEntity> request) {
        // TODO Auto-generated method stub
        return repo.RegistrarOrdenamientoProteccionVigilancia(request);
    }

    @Override
    public ResultEntity EliminarOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request) {
        // TODO Auto-generated method stub
        return repo.EliminarOrdenamientoProteccionVigilancia(request);
    }

    @Override
    public ResultEntity ObtenerOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request) {
        // TODO Auto-generated method stub
        return repo.ObtenerOrdenamientoProteccionVigilancia(request);
    }

    @Override
    public ResultEntity ListarOrdenamientoProteccion(OrdenamientoProteccionEntity request) {

        return repo.ListarOrdenamientoProteccion(request);
    }

    @Override
    public ResultEntity ListarOrdenamientoInterno(OrdenamientoProteccionEntity request) {

        return repo.ListarOrdenamientoInterno(request);
    }

    @Override
    public ResultEntity ListarOrdenamientoInternoDetalle(OrdenamientoProteccionEntity request) {

        return repo.ListarOrdenamientoInternoDetalle(request);
    }

    @Override
    public ResultEntity ListarOrdenamientoInternoDetalleTitular(OrdenamientoProteccionEntity request) {

        return repo.ListarOrdenamientoInternoDetalleTitular(request);
    }

    @Override
    public ResultClassEntity RegistrarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request) {

        return repo.RegistrarOrdenamientoProteccion(request);
    }
    @Override
    public ResultClassEntity ActualizarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request) {

        return repo.ActualizarOrdenamientoProteccion(request);
    }

    @Override
    public ResultClassEntity ListarOrdenamientoProteccionPorFiltro(Integer idPlanManejo, String codTipoOrdenamiento) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        List<OrdenamientoProteccionEntity> lista = repo.ListarOrdenamientoProteccionFiltro(idPlanManejo, codTipoOrdenamiento);

        for (OrdenamientoProteccionEntity ordenamientoProteccionEntity : lista) {
            ordenamientoProteccionEntity.setListOrdenamientoProteccionDet(
                repo.ListarOrdenamientoProteccionDetalle(ordenamientoProteccionEntity.getIdOrdenamientoProteccion(), null)
                );            
        }

        result.setData(lista);
        result.setSuccess(true);

        return result;
    }

    @Override
    @Transactional
    public ResultClassEntity ActualizarOrdenamientoProteccionDetalle(List<OrdenamientoProteccionEntity> request) throws Exception {

        ResultClassEntity result = null;
        try {          
            
            List<OrdenamientoProteccionDetalleEntity> list = new ArrayList<>();
            OrdenamientoProteccionDetalleEntity obj = null;
    
            for (OrdenamientoProteccionEntity ordenamientoProteccionEntity : request) {
                
                for (OrdenamientoProteccionDetalleEntity element : ordenamientoProteccionEntity.getListOrdenamientoProteccionDet()) {
    
                    if (element.getEstado()!=null && element.getEstado().equals("I") && element.getIdOrdenamientoProteccionDet()!=null) {
                        result = repo.EliminarOrdenamientoProteccionDetalle(element.getIdOrdenamientoProteccionDet(), ordenamientoProteccionEntity.getIdUsuarioModificacion());
                    }else if((element.getEstado()==null || !element.getEstado().equals("I")) && element.getIdOrdenamientoProteccionDet()==null){                        
                        element.setIdUsuarioRegistro(ordenamientoProteccionEntity.getIdUsuarioModificacion());
                        element.setIdOrdenamientoProtecccion(ordenamientoProteccionEntity.getIdOrdenamientoProteccion());
                        result = repo.RegistrarOrdenamientoProteccionDetalle(element);
                    }else if(!element.getEstado().equals("I")){
                        element.setIdUsuarioRegistro(ordenamientoProteccionEntity.getIdUsuarioModificacion());
                        element.setIdOrdenamientoProtecccion(ordenamientoProteccionEntity.getIdOrdenamientoProteccion());
                        list = repo.ListarOrdenamientoProteccionDetalle(ordenamientoProteccionEntity.getIdOrdenamientoProteccion(), element.getIdOrdenamientoProteccionDet());
    
                        if (list!=null && !list.isEmpty()) {
                            obj= list.get(0);
                            obj.setIdUsuarioModificacion(ordenamientoProteccionEntity.getIdUsuarioModificacion());
                            if (element.getDescripcion()!=null) {
                                obj.setDescripcion(element.getDescripcion());
                            }
                            if (element.getAreaHA()!=null) {
                                obj.setAreaHA(element.getAreaHA());
                            }
                        }    
                        result = repo.ActualizarOrdenamientoProteccionDetalle(obj);
                    }                   
                }
            }

        } catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
        return result;
    }

    @Override
    public ResultClassEntity proteccionVigilanciaCargarExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna, String codTipoOrdenamiento,String subCodTipoOrdenamiento, String codigoTipoOrdenamientoDet, int idPlanManejo, int idUsuarioRegistro) {
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            List<OrdenamientoProteccionEntity> listRegistro = new ArrayList<>();

            //obtener id
            OrdenamientoProteccionEntity ordenActual = (OrdenamientoProteccionEntity)repo.obtenerOrdenamientoInterno(idPlanManejo,codTipoOrdenamiento ).getData();

            OrdenamientoProteccionEntity ordenamientoProteccionEntity = new OrdenamientoProteccionEntity();
            List<OrdenamientoProteccionDetalleEntity> listOrdenamientoProteccionDet = new ArrayList<>();
            ordenamientoProteccionEntity.setIdPlanManejo(ordenActual.getIdPlanManejo());
            ordenamientoProteccionEntity.setCodTipoOrdenamiento(codTipoOrdenamiento);
            ordenamientoProteccionEntity.setSubCodTipoOrdenamiento(subCodTipoOrdenamiento);
            ordenamientoProteccionEntity.setIdOrdenamientoProteccion(ordenActual.getIdOrdenamientoProteccion());
            ordenamientoProteccionEntity.setIdUsuarioRegistro(idUsuarioRegistro);

            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                OrdenamientoProteccionDetalleEntity ordenamientoProteccionDetalleEntity = new OrdenamientoProteccionDetalleEntity();
                ordenamientoProteccionDetalleEntity.setIdOrdenamientoProteccionDet(0);
                ordenamientoProteccionDetalleEntity.setCodigoTipoOrdenamientoDet(codigoTipoOrdenamientoDet);
                ordenamientoProteccionDetalleEntity.setIdUsuarioRegistro(idUsuarioRegistro);
                if(codigoTipoOrdenamientoDet.equals(CodigosPGMF.PGMF_TAB_4_3_1) || codigoTipoOrdenamientoDet.equals(CodigosPGMF.PGMF_TAB_4_3_2)){
                    ordenamientoProteccionDetalleEntity.setVerticeBloque(row.get(0));
                    ordenamientoProteccionDetalleEntity.setCoordenadaEste(BigDecimal.valueOf(Double.parseDouble(row.get(1))));
                    ordenamientoProteccionDetalleEntity.setCoordenadaNorte(BigDecimal.valueOf(Double.parseDouble(row.get(2))));
                    ordenamientoProteccionDetalleEntity.setZonaUTM(row.get(3));
                    ordenamientoProteccionDetalleEntity.setDescripcion(row.get(4));
                    ordenamientoProteccionDetalleEntity.setObservacion(row.get(5));
                }else if(codigoTipoOrdenamientoDet.equals(CodigosPGMF.PGMF_TAB_4_3_3) || codigoTipoOrdenamientoDet.equals(CodigosPGMF.PGMF_TAB_4_3_4)){
                    ordenamientoProteccionDetalleEntity.setActividad(row.get(0));
                    ordenamientoProteccionDetalleEntity.setDescripcion(row.get(1));
                    ordenamientoProteccionDetalleEntity.setObservacion(row.get(2));
                }

                listOrdenamientoProteccionDet.add(ordenamientoProteccionDetalleEntity);
            }
            ordenamientoProteccionEntity.setListOrdenamientoProteccionDet(listOrdenamientoProteccionDet);
            listRegistro.add(ordenamientoProteccionEntity);
            return repo.registrarOrdenamientoInterno(listRegistro);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    public ResultClassEntity registrarOrdenamientoInterno(List<OrdenamientoProteccionEntity> list) throws Exception {
        return repo.registrarOrdenamientoInterno(list);
    }

    @Override
    public ResultClassEntity EliminarOrdenamientoInterno(OrdenamientoProteccionDetalleEntity param) throws Exception {
        return  repo.EliminarOrdenamientoInterno(param);
    }

}
