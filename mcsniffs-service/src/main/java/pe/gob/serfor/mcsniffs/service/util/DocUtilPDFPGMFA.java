package pe.gob.serfor.mcsniffs.service.util;

import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralDemaRepository;
import pe.gob.serfor.mcsniffs.service.ObjetivoManejoService;
import pe.gob.serfor.mcsniffs.service.impl.SolicitudAccesoServiceImpl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class DocUtilPDFPGMFA {

    private DocUtilPDFPGMFA() {
    }

    @Autowired
    private SolicitudAccesoServiceImpl acceso;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;



    public PdfPTable createTableInformacionGeneral(PdfWriter writer, Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(4);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font subTitulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 11f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 10f, Font.COURIER);

        //1 - Resumen Ejecutivo
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PGMFA");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            //o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            //o.setProvincia(lstacceso.get(0).getNombreProvincia());
            //o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }

        cell = new PdfPCell(new Paragraph("1. Del Titular del Permiso",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre de la comunidad: "+ o.getNombreElaboraDema()!=null? o.getNombreElaboraDema():"",contenido));
        cell.setColspan(4);
        cell.setFixedHeight(45);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre del Jefe de la comunidad o representante legal: ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRepresentanteLegal()!=null?o.getRepresentanteLegal():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nro de la credencial",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDescripcion()!=null? o.getDescripcion():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nro de RUC",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroRucComunidad()!=null? o.getNroRucComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Federación u organización a la que pertenece la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFederacionComunidad()!=null? o.getFederacionComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("2. Del Plan General de Manejo Forestal",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Ing. Forestal que elaboró el PGMF: ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!= null ? o.getRegente().getNombres() + " " + o.getRegente().getApellidos():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Certificado de habilitación del Ing. Forestal:  ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(""));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("N° inscripción en el registro de consultores de  INRENA:   ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroResolucionComunidad()!=null? o.getNroResolucionComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha de presentación del PGMF:    ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha de inicio: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Fecha de finalización: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Potencial maderable (m3 totales):   ",subTitulo2));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Departamento: "+o.getDepartamento()!=null? o.getDepartamento():"",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Provincia: " +o.getProvincia()!=null? o.getProvincia():"",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distrito: "+o.getDistrito()!=null? o.getDistrito():"",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº de sectores o anexos:  ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroAnexosComunidad()!= null ? o.getNroAnexosComunidad().toString():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Superficie de la comunidad (ha):",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getSuperficieHaMaderables()!=null? o.getSuperficieHaMaderables():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área de manejo forestal  (ha): ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getAreaTotal()!=null? o.getAreaTotal().toString():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        return table;
    }

    public  PdfPTable createObjetivoGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font contenido= new Font(Font.HELVETICA, 11f, Font.COURIER);

        //2 - Objetivos de Manejo
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PGMFA", idPlanManejo);

        int contador=0;
        for(ObjetivoDto ob : lstObjetivos.getData()){
            if(contador==0){
                cell = new PdfPCell(new Paragraph(ob.getDescripcion()!=null?ob.getDescripcion():"",contenido));
                cell.setFixedHeight(50);
                table.addCell(cell);
            }

            contador++;
        }

        return table;
    }

    public  PdfPTable createObjetivoEspecifico(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font contenido= new Font(Font.HELVETICA, 10f, Font.COURIER);

        //2 - Objetivos de Manejo
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PGMFA", idPlanManejo);
        List<ObjetivoDto> maderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("Productos a obtener del manejo ",titulo));
        cell.setColspan(3);
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maderable",subTitulo));
        cell.setRowspan(maderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaM="";
        for(ObjetivoDto obM : maderables){
            if(obM.getActivo().equals("A")){
                marcaM="(X)";
            }else{marcaM="";}
            cell = new PdfPCell(new Paragraph(marcaM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }


        List<ObjetivoDto> NMaderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("No Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("No Maderable",subTitulo));
        cell.setRowspan(NMaderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaNM="";
        for(ObjetivoDto obNM : NMaderables){
            if(obNM.getActivo().equals("I")){
                marcaNM="(X)";
            }else{marcaNM="";}
            cell = new PdfPCell(new Paragraph(marcaNM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obNM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }

    public  PdfPTable createDuracion(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font contenido= new Font(Font.HELVETICA, 10f, Font.COURIER);

        //3 - DURACION Y REVISION DEL PLAN
        InformacionGeneralDEMAEntity D = new InformacionGeneralDEMAEntity();
        D.setIdPlanManejo(idPlanManejo);
        D.setCodigoProceso("PGMFA");
        D = infoGeneralDemaRepo.listarInformacionGeneralDema(D).getData().get(0);
        String fechaInicio="";
        String fechaFinal="";
        if(D.getFechaInicioDema()!=null || D.getFechaInicioDema().equals("")){
            fechaInicio="Fecha de Inicio: "+formatter.format(D.getFechaInicioDema());
        }
        if(D.getFechaFinDema()!=null || D.getFechaFinDema().equals("")){
            fechaFinal="Fecha de Finalización: "+formatter.format(D.getFechaFinDema());
        }

        cell = new PdfPCell(new Paragraph(fechaInicio,contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(fechaFinal,contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        return table;
    }

    public  PdfPTable createInfBasicaAcreditacion(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font contenido= new Font(Font.HELVETICA, 11f, Font.COURIER);

        //4 -INFORMACION BASICA DEL AREA DE MANEJO
        //4.1
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMATC");

        cell = new PdfPCell(new Paragraph("Productos a obtener del manejo ",contenido));
        cell.setColspan(3);
        cell.setFixedHeight(20);
        table.addCell(cell);

        for(InfBasicaAereaDetalleDto detalle: lstInfoAreaAcreditacionTerritorio){
            cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null? detalle.getReferencia(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null? detalle.getAreaHa().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }



}