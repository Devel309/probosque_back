package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;

public interface ArchivoService {
    ResultEntity<ArchivoEntity> registroArchivo(ArchivoEntity request);
    ResultEntity<ArchivoEntity> listarArchivo(ArchivoEntity request);
    ResultClassEntity<ArchivoEntity> obtenerArchivo(Integer IdArchivo);
    ResultClassEntity<ArchivoEntity> ObtenerArchivoGeneral(Integer idArchivo) throws Exception;
    ResultClassEntity<Integer> EliminarArchivoGeneral(Integer idArchivo, Integer idUsuario) throws Exception;
    ResultClassEntity<Integer> RegistrarArchivoGeneral(MultipartFile file, Integer IdUsuarioCreacion, String TipoDocumento) throws Exception;
    ResultClassEntity DescargarArchivoGeneral(ArchivoEntity param);
    ResultClassEntity downloadFile(String name);

    ResultClassEntity eliminarArchivoGeometria(ArchivoEntity param) throws Exception;

}
