package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueDemarcacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.UnidadManejoArchivoRepository;
import pe.gob.serfor.mcsniffs.service.UnidadManejoArchivoService;

import java.util.List;

@Service("UnidadManejoArchivoService")
public class UnidadManejoArchivoServiceImpl implements UnidadManejoArchivoService {

    @Autowired
    private UnidadManejoArchivoRepository unidadManejoArchivoRepository;
    @Autowired
    private ArchivoRepository archivoRepository;
    @Override
    @Transactional
    public ResultClassEntity RegistrarUnidadManejo(UnidadManejoArchivoEntity unidadManejoArchivoEntity) throws Exception {
        // ResultClassEntity result = null;
        // for (MultipartFile element:files) {
            return unidadManejoArchivoRepository.RegistrarUnidadManejoArchivo(unidadManejoArchivoEntity);
        // }
        // return result;
    }

    @Override
    public ResultClassEntity EliminarUnidadManejoArchivo(UnidadManejoArchivoEntity unidadManejoArchivoEntity) throws Exception {
        unidadManejoArchivoRepository.EliminarUnidadManejoArchivo(unidadManejoArchivoEntity);

        return unidadManejoArchivoRepository.EliminarUnidadManejoArchivo(unidadManejoArchivoEntity);
    }

    @Override
    public ResultClassEntity ListarUnidadManejoArchivo(
            UnidadManejoArchivoEntity request) {
        // TODO Auto-generated method stub

        return  unidadManejoArchivoRepository.ListarUnidadManejoArchivo(request);

    }

}
