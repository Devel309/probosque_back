package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;

public interface ProcesoPostulacionService {
    ResultClassEntity guardaProcesoPostulacion(ProcesoPostulacionResquestEntity proceso) throws Exception;
    ResultClassEntity<DocumentoRespuestaEntity> autorizarPublicacion(MultipartFile file, String CodigoDoc,String NombreArchivo, Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM,Integer idDocumentoAdjunto);
    ResultEntity<DocumentoRespuestaEntity> obtenerAutorizacion(AnexoRequestEntity filtro);
    ResultClassEntity AdjuntarResolucionDenegada(MultipartFile file,String CodigoDoc,String NombreArchivo,Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto);
    ResultClassEntity actualizarEstadoProcesoPostulacion(EstatusProcesoPostulacionEntity obj);
    ResultClassEntity actualizarObservacionARFFSProcesoPostulacion (ProcesoPostulacionDto obj);
    ResultClassEntity actualizarResolucionProcesoPostulacion (ProcesoPostulacionDto obj);
    ResultClassEntity obtenerProcesoPostulacion(ProcesoPostulacionDto obj);
    //ResultClassEntity obtenerInformacionProcesoPostulacion(ProcesoPostulacionDto obj);
    ResultEntity obtenerInformacionProcesoPostulacion(ProcesoPostulacionDto request);
    ResultClassEntity actualizarProcesoPostulacion(ProcesoPostulacionDto obj);
    ResultClassEntity tieneObservacion(ProcesoPostulacionDto obj);
    ResultClassEntity obtenerProcesoPostulacionValidarRequisito(ProcesoPostulacionDto obj);
    ResultClassEntity actualizarEnvioPrueba(ProcesoPostulacionDto obj);
    ResultEntity<ProcesoPostulacionEntity> listarProcesoPostulacion(ListarProcesoPosEntity filtro) throws Exception;
    ResultClassEntity validarPublicacionId(ProcesoPostulacionDto obj);
    
}
