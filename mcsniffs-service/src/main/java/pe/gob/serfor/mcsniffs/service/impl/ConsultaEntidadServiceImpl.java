package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ConsultaEntidadEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.ConsultaEntidadRepository;
import pe.gob.serfor.mcsniffs.service.ConsultaEntidadService;

@Service
public class ConsultaEntidadServiceImpl implements ConsultaEntidadService {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {repositorio de la tabla Consulta entidad}
     *
     */
    @Autowired
    ConsultaEntidadRepository repo;

    @Override
    public ResultEntity<ConsultaEntidadEntity> listarEntidades() {
        /**
         * @autor: JaquelineDB [11-06-2021]
         * @modificado:
         * @descripción: {Lista las entidades a las cuales se les puede hacer consultas}
         * @param:
         */
        return repo.listarEntidades();
    }
}
