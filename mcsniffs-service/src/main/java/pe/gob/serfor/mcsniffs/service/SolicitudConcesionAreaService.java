package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAreaDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;

public interface SolicitudConcesionAreaService {
    ResultClassEntity listarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) throws Exception;
    ResultClassEntity obtenerInformacionAreaSolicitada(SolicitudConcesionAreaEntity obj) throws Exception;
    ResultClassEntity registrarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) throws Exception;
    ResultClassEntity registrarInformacionAreaSolicitada(SolicitudConcesionAreaEntity obj) throws Exception;
    ResultClassEntity actualizarInformacionAreaSolicitada(SolicitudConcesionAreaEntity obj) throws Exception;
    ResultClassEntity actualizarInformacionAreaSolicitudConcesion(SolicitudConcesionAreaDto dto) throws Exception;
    ResultClassEntity eliminarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) throws Exception;
}
