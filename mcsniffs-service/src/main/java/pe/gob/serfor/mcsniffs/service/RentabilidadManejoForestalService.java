package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;

import java.util.List;

public interface RentabilidadManejoForestalService {
    List<Dropdown> ComboRubroRentabilidadForestal(ParametroEntity param) throws Exception;
    ResultClassEntity RegistrarRentabilidadManejoForestal(List<RentabilidadManejoForestalDto> list)throws Exception;
    ResultClassEntity ListarRentabilidadManejoForestal(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarRentabilidadManejoForestal(RentabilidadManejoForestalEntity param)throws Exception;
    ResultArchivoEntity descargarFormato() throws Exception;
    ResultClassEntity registrarProgramaInversionesExcel(MultipartFile file, String codigoRentabilidad,Integer idPlanManejo, Integer idUsuarioRegistro) throws Exception;
    ResultClassEntity registrarRentabilidadManejoForestalExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                               String codigoRentabilidad, int idPlanManejo, int idUsuarioRegistro, int vigencia) throws Exception;
    ResultClassEntity ListarRentabilidadManejoForestalTitular(PlanManejoEntity param)throws Exception;

}
