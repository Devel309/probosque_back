package pe.gob.serfor.mcsniffs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.Dto.InformacionBasicaDetalle.InformacionBasicaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.FaunaEntity;
import pe.gob.serfor.mcsniffs.entity.HidrografiaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TipoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.PoblacionAledaniaDto;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaUbigeoRepository;
import pe.gob.serfor.mcsniffs.service.InformacionBasicaService;

@Service("InformacionBasicaService")
public class InformacionBasicaServiceImpl implements InformacionBasicaService {
    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private InformacionBasicaDetalleRepository informacionBasicaDetalleRepository;

    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Override
    @Transactional
    public ResultClassEntity registrarInformacionBasica(List<InformacionBasicaEntity> lista) throws Exception {

        ResultClassEntity result = null;

        for (InformacionBasicaEntity element : lista) {
            result = informacionBasicaRepository.registrarInformacionBasica(element);
           
            if (element.getListInformacionBasicaDet()!=null) {
                for (InformacionBasicaDetalleEntity informacionBasicaDetalleEntity : element.getListInformacionBasicaDet()) {
                    informacionBasicaDetalleEntity.setIdInfBasica(element.getIdInfBasica());
                    informacionBasicaDetalleEntity.setIdUsuarioRegistro(element.getIdUsuarioRegistro());
                    informacionBasicaDetalleRepository.RegistrarInformacionBasicaDetalle(informacionBasicaDetalleEntity);
                }
            }

            if (element.getListInformacionBasicaDistrito()!=null) {
                for (InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity : element.getListInformacionBasicaDistrito()) {
                    informacionBasicaUbigeoEntity.setIdInfBasica(element.getIdInfBasica());
                    informacionBasicaUbigeoEntity.setIdUsuarioRegistro(element.getIdUsuarioRegistro());
                    informacionBasicaUbigeoRepository.RegistrarInformacionBasicaDistrito(informacionBasicaUbigeoEntity);
                }
            }
        }    

        return result;
    }

    @Override
    public ResultClassEntity EliminarInfBasicaAerea(InfBasicaAereaDetalleDto param) throws Exception {
        return  informacionBasicaRepository.EliminarInfBasicaAerea(param);
    }



    @Override
    public ResultClassEntity<List<InfBasicaAereaDetalleDto>> listarInfBasicaAerea(String idInfBasica, Integer idPlanManejo, String codCabecera)
            throws Exception {

        ResultClassEntity<List<InfBasicaAereaDetalleDto>> result = new ResultClassEntity<>();

        result.setData(informacionBasicaRepository.listarInfBasicaAerea(idInfBasica,idPlanManejo,codCabecera));
        result.setSuccess(true);
        result.setMessage("Lista Información Básica Aérea");

        return result;
    }

    @Override
    public ResultClassEntity<List<InfBasicaAereaDetalleDto>> listarInfBasicaAereaTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso)
            throws Exception {

        ResultClassEntity<List<InfBasicaAereaDetalleDto>> result = new ResultClassEntity<>();

        result.setData(informacionBasicaRepository.listarInfBasicaAereaTitular(tipoDocumento,nroDocumento,codigoProcesoTitular,subcodigoProcesoTitular,idPlanManejo,codigoProceso,subCodigoProceso));
        result.setSuccess(true);
        result.setMessage("Lista Información Básica Aérea");

        return result;
    }
    @Override
    public ResultClassEntity registrarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception {
        return informacionBasicaRepository.registrarInformacionBasicaDetalle(list);
    }

    @Override
    public ResultClassEntity ActualizarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception {
        return informacionBasicaRepository.ActualizarInformacionBasicaDetalle(list);
    }


    @Override
    public ResultClassEntity<List<FaunaEntity>> obtenerFauna(Integer idPlanManejo, String tipoFauna, String nombre,
            String nombreCientifico, String familia, String estatus, String codigoTipo) throws Exception {
        return informacionBasicaRepository.obtenerFauna(idPlanManejo, tipoFauna, nombre, nombreCientifico, familia, estatus, codigoTipo);
    }

    @Override
    public ResultClassEntity registrarSolicitudFauna(FaunaEntity faunaEntity) throws Exception {
        return informacionBasicaRepository.registrarSolicitudFauna(faunaEntity);
    }

    @Override
    public ResultClassEntity<List<PoblacionAledaniaDto>> obtenerPoblacion() throws Exception {
        return informacionBasicaRepository.obtenerPoblacion();
    }

    @Override
    public ResultClassEntity<List<TipoBosqueEntity>> obtenerTipoBosque(Integer idTipoBosque, String descripcion,
            String region) throws Exception {
        return informacionBasicaRepository.obtenerTipoBosque(idTipoBosque, descripcion, region);
    }



    @Override
    public ResultClassEntity<List<HidrografiaEntity>> obtenerHidrografia(Integer idHidrografia, String tipoHidrografia)
            throws Exception {

        ResultClassEntity<List<HidrografiaEntity>> result = new ResultClassEntity<>();

        result.setData(informacionBasicaRepository.obtenerHidrografia(idHidrografia, tipoHidrografia));
        result.setSuccess(true);
        result.setMessage("Lista Censo Forestal Detalle");

        return result;
    }

    @Override
    public ResultClassEntity registrarArchivoInfBasica(InformacionBasicaEntity param,MultipartFile file) throws Exception {
        return informacionBasicaRepository.registrarArchivoInfBasica(param,file);
    }
    @Override
    public ResultClassEntity listarInfBasicaArchivo(Integer id) throws Exception {
        return informacionBasicaRepository.listarInfBasicaArchivo(id);
    }
    @Override
    public ResultClassEntity eliminarInfBasicaArchivo(Integer id, Integer idArchivo,Integer idUsuario) throws Exception {
        return informacionBasicaRepository.eliminarInfBasicaArchivo(id,idArchivo,idUsuario);
    }

    /**
     * @autor:  Jason Retamozo [26-01-2022]
     * @descripción: {Listado de cabecera,detalle,sub para informacion socioeconomica}
     * @param: idPlanManejo y codProceso
     * @return: ResponseEntity<InformacionBasicaEntity>
     */
    @Override
    public  ResultClassEntity listarInformacionSocioeconomica(Integer idPlanManejo, String codigoTipoInfBasica,String subCodigoTipoInfBasica) throws Exception{
        List<List<InformacionBasicaEntity>> listData = new ArrayList<>();

        listData.add(informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,codigoTipoInfBasica,subCodigoTipoInfBasica));
        ResultClassEntity result = new ResultClassEntity();
        result.setData(listData);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity ListarInformacionBasica(Integer idPlanManejo) throws Exception {

        List<InformacionBasicaEntity> listData = informacionBasicaRepository.listarInformacionBasica(idPlanManejo);

        if (listData!=null && !listData.isEmpty()) {

            InformacionBasicaEntity i = new InformacionBasicaEntity();

            for (InformacionBasicaEntity informacionBasicaEntity : listData) {
                i = new InformacionBasicaEntity();
                i.setIdInfBasica(informacionBasicaEntity.getIdInfBasica());
                informacionBasicaEntity.setListInformacionBasicaDet(informacionBasicaDetalleRepository.ListarInformacionBasicaDetalle(
                    informacionBasicaEntity.getIdPlanManejo(), informacionBasicaEntity.getIdInfBasica()));

                informacionBasicaEntity.setListInformacionBasicaDistrito(informacionBasicaUbigeoRepository.ListarInformacionBasicaDistrito(i));
            }
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(listData);
        result.setSuccess(true);

        return result;
    }

    @Override
    @Transactional
    public ResultClassEntity actualizarInformacionBasica(List<InformacionBasicaEntity> listaInformacionBasicaEntity) throws Exception {

        ResultClassEntity result = null;

        for (InformacionBasicaEntity element : listaInformacionBasicaEntity) {
            result = informacionBasicaRepository.actualizarInformacionBasica(element);

            for (InformacionBasicaDetalleEntity informacionBasicaEntityDetalleEntity : element.getListInformacionBasicaDet()) {
                informacionBasicaEntityDetalleEntity.setIdInfBasica(element.getIdInfBasica());
                informacionBasicaDetalleRepository.ActualizarInformacionBasicaDetalle(informacionBasicaEntityDetalleEntity);
            }

            for (InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity : element.getListInformacionBasicaDistrito()) {
                informacionBasicaUbigeoEntity.setIdInfBasica(element.getIdInfBasica());
                informacionBasicaUbigeoRepository.ActualizarInformacionBasicaDistrito(informacionBasicaUbigeoEntity);
            }
        }
        return result;
    }

    @Override
    public ResultClassEntity ListarInformacionBasicaPorFiltro(Integer idPlanManejo, String codTipoInfBasica) throws Exception {

        List<InformacionBasicaEntity> listData = informacionBasicaRepository.listarInformacionBasicaPorFiltro(idPlanManejo, codTipoInfBasica);

        if (listData!=null && !listData.isEmpty()) {

            InformacionBasicaEntity i = new InformacionBasicaEntity();
            
            for (InformacionBasicaEntity informacionBasicaEntity : listData) {
                i = new InformacionBasicaEntity();
                i.setIdInfBasica(informacionBasicaEntity.getIdInfBasica());
                informacionBasicaEntity.setListInformacionBasicaDet(informacionBasicaDetalleRepository.ListarInformacionBasicaDetalle(
                    informacionBasicaEntity.getIdPlanManejo(), informacionBasicaEntity.getIdInfBasica()));

                informacionBasicaEntity.setListInformacionBasicaDistrito(informacionBasicaUbigeoRepository.ListarInformacionBasicaDistrito(i));
            }
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(listData);
        result.setSuccess(true);

        return result;
    }

    /**
	 * @autor: Jaqueline Diaz Barrientos [04-10-2021]
	 * @modificado:
	 * @descripción: {Eliminar fauna}
	 * @param: InfBasicaAereaDetalleDto
	 */
    @Override
    public ResultClassEntity EliminarFauna(InfBasicaAereaDetalleDto infBasicaAereaDetalleDto) {
        return informacionBasicaRepository.EliminarFauna(infBasicaAereaDetalleDto);
    }

    /**
     * @autor: Danny Nazario [22-12-2021]
     * @modificado:
     * @descripción: {Listar Detalle Información Básica Área}
     * @param: InfBasicaAereaDetalleDto
     */
    @Override
    public ResultClassEntity listarInfBasicaAreaDetalle(String codTipo, String codSubTipo, Integer idInfBasica) throws Exception {
        return informacionBasicaRepository.listarInfBasicaAereaDetalle(codTipo, codSubTipo, idInfBasica);
    }

    @Override
    public ResultClassEntity listarPorFiltrosInfBasicaAerea(InfBasicaAereaDetalleDto param) throws Exception {

        return informacionBasicaRepository.listarPorFiltrosInfBasicaAerea(param);
    }

    @Override
    @Transactional
    public ResultClassEntity registrarInformacionBasicaUbigeo(InformacionBasicaEntity dto) throws Exception {        

        ResultClassEntity result = null;

        if (dto.getIdInfBasica()==null || dto.getIdInfBasica()==0) {
            result = informacionBasicaRepository.registrarInformacionBasica(dto);
        }        

        for (InformacionBasicaUbigeoEntity element : dto.getListInformacionBasicaDistrito()) {
            element.setIdInfBasica(dto.getIdInfBasica());
            element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
            result = informacionBasicaUbigeoRepository.RegistrarInformacionBasicaDistrito(element);
        }   

        return result;
    }

    @Override
    public ResultClassEntity<List<InformacionBasicaUbigeoEntity>> listarInfBasicaUbigeo(InformacionBasicaEntity obj) throws Exception {

        ResultClassEntity<List<InformacionBasicaUbigeoEntity>> result = new ResultClassEntity<>();

        result.setData(informacionBasicaUbigeoRepository.ListarInformacionBasicaDistrito(obj));
        result.setSuccess(true);
        result.setMessage("Lista Información Básica Ubigeo");

        return result;
    } 


    @Override
    @Transactional
    public ResultClassEntity eliminarInformacionBasicaUbigeo(InformacionBasicaUbigeoEntity dto)throws Exception {
 
        return informacionBasicaUbigeoRepository.eliminarInformacionBasicaUbigeo(dto);
    }

    @Override
    public ResultClassEntity ListarFaunaInformacionBasicaDetalle(InformacionBasicaDetalleDto param)  throws Exception {
        return informacionBasicaRepository.ListarFaunaInformacionBasicaDetalle(param);
    }


}
