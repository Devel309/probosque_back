package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Oposicion.OposicionArchivoDTO;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.repository.GenericoRepository;
import pe.gob.serfor.mcsniffs.repository.OposicionRepository;
import pe.gob.serfor.mcsniffs.repository.PersonaRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.OposicionService;


@Service
public class OposicionServiceImpl implements OposicionService {
    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Reposotorio donde se implementa
     *               todo lo referente a las opociones hacia una postulacion}
     *
     */
    @Autowired
    OposicionRepository repository;

    @Autowired
    PersonaRepository _repositoryPersona;

    @Autowired
    ProcesoPostulacionRepository procesoPostulacionRepository;

    @Autowired
    private GenericoRepository genericoRepository;

    @Autowired
    LogAuditoriaServiceImpl logAuditoriaService;

    @Autowired
    private EmailService email;

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Abner Valdez [06-12-2021]
     * @descripción: {Registrar la oposicion hacia una postulacion}
     * @param:OposicionEntity
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarOposicion(OposicionEntity item) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        try {

            int idPersona = 0;

            PersonaEntity persona = new PersonaEntity();
            persona = this._repositoryPersona.ObtnerPersonaPorNumDocumento(item.getPersona().getNumeroDocumento());
            if (persona.getIdPersona() == null) {
                idPersona = this._repositoryPersona.registrarPersona(item.getPersona());

                LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MVC_PERSONA,
                        LogAuditoria.REGISTRAR,
                        LogAuditoria.DescripcionAccion.Registrar.T_MVC_PERSONA + idPersona,
                        item.getPersona().getIdUsuarioRegistro());



                logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);




            } else {
                idPersona = persona.getIdPersona();
            }


            OposicionRequestEntity req = new OposicionRequestEntity();
            //req.setIdProcesoPostulacion(item.getIdProcesoPostulacion());
            req.setTipoDocumentoGestion(item.getTipoDocumentoGestion());
            req.setNumDocumentoGestion(item.getDocumentoGestion());
            ResultEntity<OposicionEntity> d = repository.listarOposicion(req);

            if (d!=null && d.getData()!=null && item.getIdOposicion() ==null) {
                List<OposicionEntity> list = d.getData();
                if (list!=null && !list.isEmpty()) {
                    result = new ResultClassEntity<>();
                    result.setSuccess(true);
                    result.setValidateBusiness(false);
                    result.setMessage("Ya se encuentra registrado el documento de gestión N° "+item.getDocumentoGestion());
                    return result;
                }
            }

            if (item.getTipoDocumentoGestion()!=null && item.getTipoDocumentoGestion().equals("TPMPFDM")) {

                item.getPersona().setIdPersona(idPersona);
                result = repository.registrarOposicion(item);

                if (item.getListaOposicionArchivo()!=null && !item.getListaOposicionArchivo().isEmpty()) {
                    for (OposicionArchivoDTO archivo : item.getListaOposicionArchivo()) {
                        archivo.setIdOposicion(item.getIdOposicion());
                        archivo.setIdUsuarioRegistro(item.getIdUsuarioRegistro());
    
                        repository.registrarOposicionArchivo(archivo);
                    }
                }else{ 
                        result.setSuccess(false);
                        result.setMessage("No se pudo realizar la transaccion, consulte con el administrador.");
                        return result;
                    }
            }

            else{

                OposicionEntity o = new OposicionEntity();
                o.setIdOposicion(item.getIdOposicion());

                if (idPersona != 0) {
                    item.getPersona().setIdPersona(idPersona);
                    result = repository.registrarOposicion(item);
    
                    if (item.getListaOposicionArchivo()!=null && !item.getListaOposicionArchivo().isEmpty()) {
                        for (OposicionArchivoDTO archivo : item.getListaOposicionArchivo()) {
                            archivo.setIdOposicion(item.getIdOposicion());
                            archivo.setIdUsuarioRegistro(item.getIdUsuarioRegistro());
        
                            repository.registrarOposicionArchivo(archivo);
                        }
                    }
    
                } else {
    
                    result.setSuccess(false);
                    result.setMessage("No se pudo realizar la transaccion, consulte con el administrador.");
                    return result;
                }
    
                /*Envio de correo */
    
                ProcesoPostulacionDto pro = new ProcesoPostulacionDto();
                pro.setIdProcesoPostulacion(item.getDocumentoGestion());
                ResultClassEntity obtener =	procesoPostulacionRepository.obtenerProcesoPostulacion(pro);
    
                if (obtener!=null && obtener.getData()!=null && o.getIdOposicion()==null) {
    
                    ProcesoPostulacionEntity entity = (ProcesoPostulacionEntity)obtener.getData();
    
                    if (entity.getIdUsuarioPostulacion()!=null) {
                           String postulante = entity.getNombrePostulante()+" "+entity.getApellidoPaternoPostulante() +" "+ entity.getApellidoMaternoPostulante();
                            EmailEntity mail = new EmailEntity();
                            String[] mails =new String[1];
                            if(entity.getCorreoPostulante()!=null){
    
                                String contenido = "En registro una oposición al proceso oferta Id:"+entity.getIdProcesoOferta()+ 
                                " ID Proceso Postulación: "+item.getIdProcesoPostulacion()+", estimado:"+postulante;
    
                                mails[0]=entity.getCorreoPostulante();
                                mail.setEmail(mails);
                                mail.setContent(contenido);
                                mail.setSubject("En registro una oposición");
                                email.sendEmail(mail);
                            }
    
    
                        
                    }
                }

            }            


        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error" + e.getMessage());
        }
        return result;
    }

    /**
     * @autor: Abner Valdez [10-12-2021]
     * @modificado: 
     * @descripción: {Registrar el detalle de oposicion archivo}
     * @param:OposicionEntity
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarOposicionArchivo(OposicionArchivoDTO item) throws Exception {
        ResultClassEntity result = null;
        try {

            if(item.getIdArchivo() > 0){
                result = repository.registrarOposicionArchivo(item);
            }else{
                
            result.setSuccess(false);
            result.setMessage("Debe contener el identificador del archivo.");
            }

        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error" + e.getMessage());
        }
        return result;
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Lista las oposiciones activas}
     * @param:OposicionRequestEntity
     */
    @Override
    public ResultEntity<OposicionEntity> listarOposicion(OposicionRequestEntity filtro) {
        return repository.listarOposicion(filtro);
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Registrar la respuesta de la oposicion}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity registrarRespuestaOposicion(OposicionEntity obj) {
        return repository.registrarRespuestaOposicion(obj);
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Lista las oposiciones activas}
     * @param:OposicionRequestEntity
     */
    @Override
    public ResultClassEntity obtenerOposicion(Integer idOposicion) throws Exception{
       
        List<OposicionEntity> lista =  repository.obtenerOposicion(idOposicion);
        OposicionArchivoDTO dto = null;
        for (OposicionEntity oposicionEntity : lista) 
        {
            dto = new OposicionArchivoDTO();
            dto.setIdOposicion(oposicionEntity.getIdOposicion());
            List<OposicionArchivoDTO> listArchivo =  repository.listarOposicionArchivo(dto);
            oposicionEntity.setListaOposicionArchivo(listArchivo);
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Registrar la respuesta de la oposicion}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity<OposicionAdjuntosEntity> obtenerOposicionRespuesta(Integer IdOposicion) {
        return repository.obtenerOposicionRespuesta(IdOposicion);
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Descargar el formato de resolucion}
     *
     */
    @Override
    public ResultArchivoEntity descargarFormatoResulucion() {
        return repository.descargarFormatoResulucion();
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {adjuntar el formato de resolucion}
     *
     */
    @Override
    public ResultClassEntity adjuntarFormatoResolucion(OposicionEntity obj) {
        return repository.adjuntarFormatoResolucion(obj);
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {actualizar la oposicion}
     *
     */
    @Override
    public ResultClassEntity actualizarOposicion(OposicionEntity obj) {
        return repository.actualizarOposicion(obj);
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {registrar opositor}
     * @param:PersonaRequestEntity
     */
    @Override
    public ResultClassEntity regitrarOpositor(PersonaRequestEntity obj) {
        return repository.regitrarOpositor(obj);
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {Busca al opositor por su numero de documento}
     * @param:NumeroDocumento
     */
    @Override
    public ResultClassEntity<PersonaRequestEntity> obtenerOpositor(OpositorRequestEntity obj) {
        return repository.obtenerOpositor(obj);
    }

    /**
     * @autor: Abner Valdez [10-12-2021]
     * @modificado:
     * @descripción: {Eliminar oposicion archivo}
     * @param:OposicionRequestEntity
     */
    @Override
    public ResultClassEntity eliminarOposicionArchivo(Integer idArchivo, Integer idUsuario) {
        return repository.eliminarOposicionArchivo(idArchivo,idUsuario);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarNuevaOposicion(OposicionEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
    
        
        try {
                result = repository.registrarOposicion(item);

                if (item.getListaOposicionArchivo()!=null && !item.getListaOposicionArchivo().isEmpty()) {
                    for (OposicionArchivoDTO archivo : item.getListaOposicionArchivo()) {
                        archivo.setIdOposicion(item.getIdOposicion());
                        archivo.setIdUsuarioRegistro(item.getIdUsuarioRegistro());
    
                        repository.registrarOposicionArchivo(archivo);
                    }
                }else{ 
                        result.setSuccess(false);
                        result.setMessage("No se pudo realizar la transaccion, consulte con el administrador.");
                        return result;
                    }

        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error" + e.getMessage());
        }
        return result;
    }


}
