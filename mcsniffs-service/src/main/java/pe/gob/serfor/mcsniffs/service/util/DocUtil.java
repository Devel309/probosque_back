package pe.gob.serfor.mcsniffs.service.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.IRunBody;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFAbstractNum;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFNumbering;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLvl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STNumberFormat;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoDetEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoDetSubEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo2CensoEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo3CensoEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalActividadEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.FaunaEntity;
import pe.gob.serfor.mcsniffs.entity.FichaEvaluacionEntitty;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDetalle;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.MonitoreoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.MonitoreoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoEspecificoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionEntity;
import pe.gob.serfor.mcsniffs.entity.OrganizacionManejoEntity;
import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalDetEntity;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.RentabilidadManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;
import pe.gob.serfor.mcsniffs.entity.SistemaManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudOpinionEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ModeloContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ListaInformeDocumentoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionSubDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha.ProyeccionCosechaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;

public class DocUtil {

    private DocUtil() {
    }

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static void processFile(Map<String, String> keys, XWPFDocument doc,
            List<InformacionGeneralDetalle> lstResult) throws IOException {
        List<XWPFParagraph> xwpfParagraphs = doc.getParagraphs();
        replaceTextInParagraphs(keys, xwpfParagraphs);
        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable xwpfTable : tables) {
            List<XWPFTableRow> tableRows = xwpfTable.getRows();
            for (XWPFTableRow xwpfTableRow : tableRows) {
                List<XWPFTableCell> tableCells = xwpfTableRow.getTableCells();
                for (XWPFTableCell xwpfTableCell : tableCells) {
                    xwpfParagraphs = xwpfTableCell.getParagraphs();
                    replaceTextInParagraphs(keys, xwpfParagraphs);
                }
            }
        }
    }

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static void replaceTextInParagraphs(Map<String, String> keys, List<XWPFParagraph> xwpfParagraphs) {
        for (XWPFParagraph xwpfParagraph : xwpfParagraphs) {
            List<XWPFRun> xwpfRuns = xwpfParagraph.getRuns();
            for (XWPFRun xwpfRun : xwpfRuns) {
                String xwpfRunText = xwpfRun.getText(xwpfRun.getTextPosition());
                for (Map.Entry<String, String> entry : keys.entrySet()) {
                    if (xwpfRunText != null && xwpfRunText.contains(entry.getKey())) {
                        xwpfRunText = xwpfRunText.replaceAll(entry.getKey(), MapBuilder.toString(entry.getValue()));
                    }
                }
                xwpfRun.setText(xwpfRunText, 0);
            }
        }
    }

    public static String setearNull(String valor) {
        String value = "";
        if (valor != null && valor != "" && valor != "null") {
            value = valor;
        }
        return value;
    }

    // evaluacion
    public static XWPFDocument setearTitularRegentePGFMA(List<FichaEvaluacionEntitty> lstresult, XWPFDocument doc) {
        try {
            // dataPart2
            FichaEvaluacionEntitty data1 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAREEV")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data2 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAOMEV")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data3 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFADRPEV")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data4 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMATC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data5 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMUP")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data6 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMCUAM")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data7 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data8 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMAF")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data9 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMAB")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data10 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAIBAMAS")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data11 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAOAMOAM")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data12 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAPPRFCRIEMM"))
                    .collect(Collectors.toList()).get(0);
            FichaEvaluacionEntitty data13 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAPPRFCRIF")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data14 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBAPO")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data15 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBDSM")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data16 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBDPF")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data17 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBMA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data18 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBIAT")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data19 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBOCA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data20 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBEP")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data21 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMBTS")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data22 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAPBDML")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data23 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAPBAIA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data24 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAPBPGA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data25 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAMEV")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data26 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAPCPC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data27 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFACCA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data28 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAODAODA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data29 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFARMFI")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data30 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFARMFE")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data31 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFACAEV")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data32 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAACAC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data33 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAAA1")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data34 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAAA2")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data35 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("PGMFAAA3")).collect(Collectors.toList())
                    .get(0);
            XWPFTable table = doc.getTables().get(1);
            for (XWPFTableRow copiedRow : table.getRows()) {
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("1conf")) {
                                        String valor = data1 != null ? setearNull(data1.getConforme()) : "";
                                        text = text.replace("1conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("1obs")) {
                                        text = text.replace("1obs", setearNull(data1.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("2conf")) {
                                        String valor = data2 != null ? setearNull(data2.getConforme()) : "";
                                        text = text.replace("2conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("2obs")) {
                                        text = text.replace("2obs", setearNull(data2.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("3conf")) {
                                        String valor = data3 != null ? setearNull(data3.getConforme()) : "";
                                        text = text.replace("3conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("3obs")) {
                                        text = text.replace("3obs", setearNull(data3.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("4conf")) {
                                        String valor = data4 != null ? setearNull(data4.getConforme()) : "";
                                        text = text.replace("4conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("4obs")) {
                                        text = text.replace("4obs", setearNull(data4.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("5conf")) {
                                        String valor = data5 != null ? setearNull(data5.getConforme()) : "";
                                        text = text.replace("5conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("5obs")) {
                                        text = text.replace("5obs", setearNull(data5.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("6conf")) {
                                        String valor = data6 != null ? setearNull(data6.getConforme()) : "";
                                        text = text.replace("6conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("6obs")) {
                                        text = text.replace("6obs", setearNull(data6.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("7conf")) {
                                        String valor = data7 != null ? setearNull(data7.getConforme()) : "";
                                        text = text.replace("7conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("7obs")) {
                                        text = text.replace("7obs", setearNull(data7.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("8conf")) {
                                        String valor = data8 != null ? setearNull(data8.getConforme()) : "";
                                        text = text.replace("8conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("8obs")) {
                                        text = text.replace("8obs", setearNull(data8.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("9conf")) {
                                        String valor = data9 != null ? setearNull(data9.getConforme()) : "";
                                        text = text.replace("9conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("9obs")) {
                                        text = text.replace("9obs", setearNull(data9.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("10conf")) {
                                        String valor = data10 != null ? setearNull(data10.getConforme()) : "";
                                        text = text.replace("10conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("10obs")) {
                                        text = text.replace("10obs", setearNull(data10.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("11campx")) {
                                        String valor = data11 != null ? setearNull(data11.getConforme()) : "";
                                        text = text.replace("11campx", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("11campy")) {
                                        text = text.replace("11campy", setearNull(data11.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("aconf12")) {
                                        String valor = data12 != null ? setearNull(data12.getConforme()) : "";
                                        text = text.replace("aconf12", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("aobs12")) {
                                        text = text.replace("aobs12", setearNull(data12.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("bconf13")) {
                                        String valor = data13 != null ? setearNull(data13.getConforme()) : "";
                                        text = text.replace("bconf13", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("bobs13")) {
                                        text = text.replace("bobs13", setearNull(data13.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("cconf14")) {
                                        String valor = data14 != null ? setearNull(data14.getConforme()) : "";
                                        text = text.replace("cconf14", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("cobs14")) {
                                        text = text.replace("cobs14", setearNull(data14.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("dconf15")) {
                                        String valor = data15 != null ? setearNull(data15.getConforme()) : "";
                                        text = text.replace("dconf15", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("dobs15")) {
                                        text = text.replace("dobs15", setearNull(data15.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("econf16")) {
                                        String valor = data16 != null ? setearNull(data16.getConforme()) : "";
                                        text = text.replace("econf16", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("eobs16")) {
                                        text = text.replace("eobs16", setearNull(data16.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("hconf17")) {
                                        String valor = data17 != null ? setearNull(data17.getConforme()) : "";
                                        text = text.replace("hconf17", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("hobs17")) {
                                        text = text.replace("hobs17", setearNull(data17.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("jconf18")) {
                                        String valor = data18 != null ? setearNull(data18.getConforme()) : "";
                                        text = text.replace("jconf18", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("jobs18")) {
                                        text = text.replace("jobs18", setearNull(data18.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("kconf19")) {
                                        String valor = data19 != null ? setearNull(data19.getConforme()) : "";
                                        text = text.replace("kconf19", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("kobs19")) {
                                        text = text.replace("kobs19", setearNull(data19.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("lconf20")) {
                                        String valor = data20 != null ? setearNull(data20.getConforme()) : "";
                                        text = text.replace("lconf20", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("lobs20")) {
                                        text = text.replace("lobs20", setearNull(data20.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("mconf21")) {
                                        String valor = data21 != null ? setearNull(data21.getConforme()) : "";
                                        text = text.replace("mconf21", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("mobs21")) {
                                        text = text.replace("mobs21", setearNull(data21.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("nconf22")) {
                                        String valor = data22 != null ? setearNull(data22.getConforme()) : "";
                                        text = text.replace("nconf22", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("nobs22")) {
                                        text = text.replace("nobs22", setearNull(data22.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("oconf23")) {
                                        String valor = data23 != null ? setearNull(data23.getConforme()) : "";
                                        text = text.replace("oconf23", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("oobs23")) {
                                        text = text.replace("oobs23", setearNull(data23.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("pconf24")) {
                                        String valor = data24 != null ? setearNull(data24.getConforme()) : "";
                                        text = text.replace("pconf24", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("pobs24")) {
                                        text = text.replace("pobs24", setearNull(data24.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("qconf25")) {
                                        String valor = data25 != null ? setearNull(data25.getConforme()) : "";
                                        text = text.replace("qconf25", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("qobs25")) {
                                        text = text.replace("qobs25", setearNull(data25.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("yconf26")) {
                                        String valor = data26 != null ? setearNull(data26.getConforme()) : "";
                                        text = text.replace("yconf26", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("yobs26")) {
                                        text = text.replace("yobs26", setearNull(data26.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("zconf27")) {
                                        String valor = data27 != null ? setearNull(data27.getConforme()) : "";
                                        text = text.replace("zconf27", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("zobs27")) {
                                        text = text.replace("zobs27", setearNull(data27.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("abconf28")) {
                                        String valor = data28 != null ? setearNull(data28.getConforme()) : "";
                                        text = text.replace("abconf28", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("abobs28")) {
                                        text = text.replace("abobs28", setearNull(data28.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("cdconf29")) {
                                        String valor = data29 != null ? setearNull(data29.getConforme()) : "";
                                        text = text.replace("cdconf29", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("cdobs29")) {
                                        text = text.replace("cdobs29", setearNull(data29.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("efconf30")) {
                                        String valor = data30 != null ? setearNull(data30.getConforme()) : "";
                                        text = text.replace("efconf30", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("efobs30")) {
                                        text = text.replace("efobs30", setearNull(data30.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("ghconf31")) {
                                        String valor = data31 != null ? setearNull(data31.getConforme()) : "";
                                        text = text.replace("ghconf31", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("ghobs31")) {
                                        text = text.replace("ghobs31", setearNull(data31.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("jkconf32")) {
                                        String valor = data32 != null ? setearNull(data32.getConforme()) : "";
                                        text = text.replace("jkconf32", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("jkobs32")) {
                                        text = text.replace("jkobs32", setearNull(data32.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("lmconf33")) {
                                        String valor = data33 != null ? setearNull(data33.getConforme()) : "";
                                        text = text.replace("lmconf33", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("lmobs33")) {
                                        text = text.replace("lmobs33", setearNull(data33.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("jkconf34")) {
                                        String valor = data34 != null ? setearNull(data34.getConforme()) : "";
                                        text = text.replace("jkconf34", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("jkobs34")) {
                                        text = text.replace("jkobs34", setearNull(data34.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("lmconf35")) {
                                        String valor = data35 != null ? setearNull(data35.getConforme()) : "";
                                        text = text.replace("lmconf35", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("lmobs35")) {
                                        text = text.replace("lmobs35", setearNull(data35.getObservacionDet()));
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearTitularRegentePOAC(List<FichaEvaluacionEntitty> lstresult, XWPFDocument doc) {
        try {
            // dataPart2
            FichaEvaluacionEntitty data1 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACIGDA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data2 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACRARPARA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data3 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACRARPARPA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data4 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACOBOE")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data5 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACIBAAAUP")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data6 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACIBAAACUAM")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data7 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACIBAAATB")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data8 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACIBAAAA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data9 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAADAAA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data10 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAACC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data11 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAAR")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data12 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAAPCIA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data13 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAAOC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data14 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAAOAT")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data15 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACAAPL")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data16 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACASATS")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data17 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACASR")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data18 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACPBDML")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data19 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACPBAIA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data20 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACMM")).collect(Collectors.toList()).get(0);
            FichaEvaluacionEntitty data21 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACPCPC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data22 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACCCA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data23 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACODAODA")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data24 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACRMFI")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data25 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACRMFE")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data26 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACCACAPPE")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data27 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACACAC")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data28 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACANAN1")).collect(Collectors.toList())
                    .get(0);
            FichaEvaluacionEntitty data29 = lstresult.stream()
                    .filter(eval -> eval.getCodigoEvaluacionDet().equals("POACANAN2")).collect(Collectors.toList())
                    .get(0);

            XWPFTable table = doc.getTables().get(1);
            for (XWPFTableRow copiedRow : table.getRows()) {
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("1conf")) {
                                        String valor = data1 != null ? setearNull(data1.getConforme()) : "";
                                        text = text.replace("1conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("1obs")) {
                                        text = text.replace("1obs", setearNull(data1.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("2conf")) {
                                        String valor = data2 != null ? setearNull(data2.getConforme()) : "";
                                        text = text.replace("2conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("2obs")) {
                                        text = text.replace("2obs", setearNull(data2.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("3conf")) {
                                        String valor = data3 != null ? setearNull(data3.getConforme()) : "";
                                        text = text.replace("3conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("3obs")) {
                                        text = text.replace("3obs", setearNull(data3.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("4conf")) {
                                        String valor = data4 != null ? setearNull(data4.getConforme()) : "";
                                        text = text.replace("4conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("4obs")) {
                                        text = text.replace("4obs", setearNull(data4.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("5conf")) {
                                        String valor = data5 != null ? setearNull(data5.getConforme()) : "";
                                        text = text.replace("5conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("5obs")) {
                                        text = text.replace("5obs", setearNull(data5.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("6conf")) {
                                        String valor = data6 != null ? setearNull(data6.getConforme()) : "";
                                        text = text.replace("6conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("6obs")) {
                                        text = text.replace("6obs", setearNull(data6.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("7conf")) {
                                        String valor = data7 != null ? setearNull(data7.getConforme()) : "";
                                        text = text.replace("7conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("7obs")) {
                                        text = text.replace("7obs", setearNull(data7.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("8conf")) {
                                        String valor = data8 != null ? setearNull(data8.getConforme()) : "";
                                        text = text.replace("8conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("8obs")) {
                                        text = text.replace("8obs", setearNull(data8.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("9conf")) {
                                        String valor = data9 != null ? setearNull(data9.getConforme()) : "";
                                        text = text.replace("9conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("9obs")) {
                                        text = text.replace("9obs", setearNull(data9.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("10conf")) {
                                        String valor = data10 != null ? setearNull(data10.getConforme()) : "";
                                        text = text.replace("10conf", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("10obs")) {
                                        text = text.replace("10obs", setearNull(data10.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("11campx")) {
                                        String valor = data11 != null ? setearNull(data11.getConforme()) : "";
                                        text = text.replace("11campx", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("11campy")) {
                                        text = text.replace("11campy", setearNull(data11.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("aconf12")) {
                                        String valor = data12 != null ? setearNull(data12.getConforme()) : "";
                                        text = text.replace("aconf12", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("aobs12")) {
                                        text = text.replace("aobs12", setearNull(data12.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("bconf13")) {
                                        String valor = data13 != null ? setearNull(data13.getConforme()) : "";
                                        text = text.replace("bconf13", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("bobs13")) {
                                        text = text.replace("bobs13", setearNull(data13.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("cconf14")) {
                                        String valor = data14 != null ? setearNull(data14.getConforme()) : "";
                                        text = text.replace("cconf14", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("cobs14")) {
                                        text = text.replace("cobs14", setearNull(data14.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("dconf15")) {
                                        String valor = data15 != null ? setearNull(data15.getConforme()) : "";
                                        text = text.replace("dconf15", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("dobs15")) {
                                        text = text.replace("dobs15", setearNull(data15.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("econf16")) {
                                        String valor = data16 != null ? setearNull(data16.getConforme()) : "";
                                        text = text.replace("econf16", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("eobs16")) {
                                        text = text.replace("eobs16", setearNull(data16.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("gconf17")) {
                                        String valor = data17 != null ? setearNull(data17.getConforme()) : "";
                                        text = text.replace("gconf17", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("gobs17")) {
                                        text = text.replace("gobs17", setearNull(data17.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("hconf18")) {
                                        String valor = data18 != null ? setearNull(data18.getConforme()) : "";
                                        text = text.replace("hconf18", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("hobs18")) {
                                        text = text.replace("hobs18", setearNull(data18.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("jconf19")) {
                                        String valor = data19 != null ? setearNull(data19.getConforme()) : "";
                                        text = text.replace("jconf19", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("jobs19")) {
                                        text = text.replace("jobs19", setearNull(data19.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("kconf20")) {
                                        String valor = data20 != null ? setearNull(data20.getConforme()) : "";
                                        text = text.replace("kconf20", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("kobs20")) {
                                        text = text.replace("kobs20", setearNull(data20.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("lconf21")) {
                                        String valor = data21 != null ? setearNull(data21.getConforme()) : "";
                                        text = text.replace("lconf21", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("lobs21")) {
                                        text = text.replace("lobs21", setearNull(data21.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("mconf22")) {
                                        String valor = data22 != null ? setearNull(data22.getConforme()) : "";
                                        text = text.replace("mconf22", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("mobs22")) {
                                        text = text.replace("mobs22", setearNull(data22.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("nconf23")) {
                                        String valor = data23 != null ? setearNull(data23.getConforme()) : "";
                                        text = text.replace("nconf23", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("nobs23")) {
                                        text = text.replace("nobs23", setearNull(data23.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("nncf24")) {
                                        String valor = data24 != null ? setearNull(data24.getConforme()) : "";
                                        text = text.replace("nncf24", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("nos24")) {
                                        text = text.replace("nos24", setearNull(data24.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("oconf25")) {
                                        String valor = data25 != null ? setearNull(data25.getConforme()) : "";
                                        text = text.replace("oconf25", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("oobs25")) {
                                        text = text.replace("oobs25", setearNull(data25.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("pconf26")) {
                                        String valor = data26 != null ? setearNull(data26.getConforme()) : "";
                                        text = text.replace("pconf26", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("pobs26")) {
                                        text = text.replace("pobs26", setearNull(data26.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("qconf27")) {
                                        String valor = data27 != null ? setearNull(data27.getConforme()) : "";
                                        text = text.replace("qconf27", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("qobs27")) {
                                        text = text.replace("qobs27", setearNull(data27.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("yconf28")) {
                                        String valor = data28 != null ? setearNull(data28.getConforme()) : "";
                                        text = text.replace("yconf28", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("yobs28")) {
                                        text = text.replace("yobs28", setearNull(data28.getObservacionDet()));
                                        r.setText(text, 0);
                                    } else if (text.contains("zconf29")) {
                                        String valor = data29 != null ? setearNull(data29.getConforme()) : "";
                                        text = text.replace("zconf29", valor.equals("EEVARECI") ? "Conforme"
                                                : valor.equals("") ? "" : "Observado");
                                        r.setText(text, 0);
                                    } else if (text.contains("zobs29")) {
                                        text = text.replace("zobs29", setearNull(data29.getObservacionDet()));
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearOpiniones(List<SolicitudOpinionEntity> lstresult, XWPFDocument doc) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(1);
            for (SolicitudOpinionEntity item : lstresult) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("4enti")) {
                                        text = text.replace("4enti", setearNull(item.getEntidad()) + " - "
                                                + setearNull(item.getNombreEntidad()));
                                        r.setText(text, 0);
                                    } else if (text.contains("4tipo")) {
                                        text = text.replace("4tipo", setearNull(item.getTipoDocumento()));
                                        r.setText(text, 0);
                                    } else if (text.contains("4nume")) {
                                        text = text.replace("4nume", setearNull(item.getNumDocumento()));
                                        r.setText(text, 0);
                                    } else if (text.contains("4fecha")) {
                                        text = text.replace("4fecha",
                                                setearNull(formatter.format(item.getFechaDocumento())));
                                        r.setText(text, 0);
                                    } else if (text.contains("4asun")) {
                                        text = text.replace("4asun", setearNull(item.getAsunto()));
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Jaqueline DB - 09-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word POAC yPGMFA, reemplazando valores
     *               marcados en el
     *               documento Etapas del aprovechamiento}
     */
    public static XWPFDocument setearTitularRegente(List<FichaEvaluacionEntitty> lstresult, XWPFDocument doc,
            String CodigoProceso) {
        try {
            // setear cabera
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("txttitular")) {
                                text = text.replace("txttitular", setearNull(lstresult.get(0).getTitular()));
                                r.setText(text, 0);
                            } else if (text.contains("txtelaborardorx")) {
                                text = text.replace("txtelaborardorx", setearNull(lstresult.get(0).getElaborador()));
                                r.setText(text, 0);
                            } else if (text.contains("txttipodocumento")) {
                                text = text.replace("txttipodocumento", setearNull(lstresult.get(0).getAsunto()));
                                r.setText(text, 0);
                            } else if (text.contains("idplanmanejox")) {
                                text = text.replace("idplanmanejox",
                                        setearNull(lstresult.get(0).getIdPlanManejo().toString()));
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            // dataPart1
            FichaEvaluacionEntitty tupac = null;
            if (CodigoProceso.equals("POAC")) {
                // tupac = lstresult.stream().filter(eval ->
                // eval.getTipoRequisito().equals("POACRPORT")).collect(Collectors.toList()).get(0);
            }
            FichaEvaluacionEntitty suscripcion = lstresult.stream().filter(
                    eval -> eval.getTipoRequisito().equals("POACRPORS") || eval.getTipoRequisito().equals("PGMFARPPRS"))
                    .collect(Collectors.toList()).get(0);
            FichaEvaluacionEntitty cautelares = lstresult.stream()
                    .filter(eval -> eval.getTipoRequisito().equals("POACRPORMC")
                            || eval.getTipoRequisito().equals("PGMFARPPRM"))
                    .collect(Collectors.toList()).get(0);
            FichaEvaluacionEntitty restricciones = lstresult.stream().filter(
                    eval -> eval.getTipoRequisito().equals("POACRPORR") || eval.getTipoRequisito().equals("PGMFARPPRR"))
                    .collect(Collectors.toList()).get(0);
            FichaEvaluacionEntitty correctivas = lstresult.stream()
                    .filter(eval -> eval.getTipoRequisito().equals("POACRPORMCO")
                            || eval.getTipoRequisito().equals("PGMFARPPRMC"))
                    .collect(Collectors.toList()).get(0);

            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("1subtitulo")) {
                                text = text.replace("1subtitulo",
                                        tupac != null ? setearNull(tupac.getSubtitulo()) : "Requisitos TUPAC");
                                r.setText(text, 0);
                            } else if (text.contains("1detalle")) {
                                text = text.replace("1detalle", tupac != null ? setearNull(tupac.getDetalle()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("1conforme")) {
                                String valor = tupac != null ? setearNull(tupac.getEvaluacion()) : "";
                                text = text.replace("1conforme",
                                        valor.equals("EPRECOMP") ? "Conforme" : valor.equals("") ? "" : "Observado");
                                r.setText(text, 0);
                            } else if (text.contains("1observacion")) {
                                text = text.replace("1observacion",
                                        tupac != null ? setearNull(tupac.getObservacion()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("2subtitulo")) {
                                text = text.replace("2subtitulo",
                                        suscripcion != null ? setearNull(suscripcion.getSubtitulo()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("2detalle")) {
                                text = text.replace("2detalle",
                                        suscripcion != null ? setearNull(suscripcion.getDetalle()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("2conforme")) {
                                String valor = suscripcion != null ? setearNull(suscripcion.getEvaluacion()) : "";
                                text = text.replace("2conforme",
                                        valor.equals("EPRECOMP") ? "Conforme" : valor.equals("") ? "" : "Observado");
                                r.setText(text, 0);
                            } else if (text.contains("2observacion")) {
                                text = text.replace("2observacion", setearNull(suscripcion.getObservacion()));
                                r.setText(text, 0);
                            } else if (text.contains("3subtitulo")) {
                                text = text.replace("3subtitulo",
                                        cautelares != null ? setearNull(cautelares.getSubtitulo()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("3detalle")) {
                                text = text.replace("3detalle",
                                        cautelares != null ? setearNull(cautelares.getDetalle()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("3conforme")) {
                                String valor = cautelares != null ? setearNull(cautelares.getEvaluacion()) : "";
                                text = text.replace("3conforme",
                                        valor.equals("EPRECOMP") ? "Conforme" : valor.equals("") ? "" : "Observado");
                                r.setText(text, 0);
                            } else if (text.contains("3observacion")) {
                                text = text.replace("3observacion",
                                        cautelares != null ? setearNull(cautelares.getObservacion()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("4subtitulo")) {
                                text = text.replace("4subtitulo",
                                        restricciones != null ? setearNull(restricciones.getSubtitulo()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("4detalle")) {
                                text = text.replace("4detalle",
                                        restricciones != null ? setearNull(restricciones.getDetalle()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("4conforme")) {
                                String valor = restricciones != null ? setearNull(restricciones.getEvaluacion()) : "";
                                text = text.replace("4conforme",
                                        valor.equals("EPRECOMP") ? "Conforme" : valor.equals("") ? "" : "Observado");
                                r.setText(text, 0);
                            } else if (text.contains("4observacion")) {
                                text = text.replace("4observacion",
                                        restricciones != null ? setearNull(restricciones.getObservacion()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("5subtitulo")) {
                                text = text.replace("5subtitulo",
                                        correctivas != null ? setearNull(correctivas.getSubtitulo()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("5detalle")) {
                                text = text.replace("5detalle",
                                        correctivas != null ? setearNull(correctivas.getDetalle()) : "");
                                r.setText(text, 0);
                            } else if (text.contains("5conforme")) {
                                String valor = correctivas != null ? setearNull(correctivas.getEvaluacion()) : "";
                                text = text.replace("5conforme",
                                        valor.equals("EPRECOMP") ? "Conforme" : valor.equals("") ? "" : "Observado");
                                r.setText(text, 0);
                            } else if (text.contains("5observacion")) {
                                text = text.replace("5observacion",
                                        correctivas != null ? setearNull(correctivas.getObservacion()) : "");
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            // dataPart2

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Jaqueline DB - 09-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word POAC yPGMFA, reemplazando valores
     *               marcados en el
     *               documento Etapas del aprovechamiento}
     */
    public static XWPFDocument setearMesaPartes(List<MesaPartesDto> listArchivos, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(1);
            for (MesaPartesDetalleDto detalle : listArchivos.get(0).getListarMesaPartesDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("321PR")) {
                                        text = text.replace("321PR", setearNull(detalle.getRequisito()));
                                        r.setText(text, 0);
                                    } else if (text.contains("321DA")) {
                                        text = text.replace("321DA", setearNull(detalle.getDetalle()));
                                        r.setText(text, 0);
                                    } else if (text.contains("321T")) {
                                        text = text.replace("321T",
                                                setearNull(detalle.getConforme()).equals("true") ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("321MT")) {
                                        text = text.replace("321MT", setearNull(detalle.getObservacion()));
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    // Plantillas consolidado pfmi
    /**
     * @autor: Jaqueline DB - 28-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento Etapas del aprovechamiento}
     */
    public static XWPFDocument generarEtapasPMFI(ActividadSilviculturalDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(21);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("etapasx")) {
                                        text = text.replace("etapasx",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("descripcionx")) {
                                        text = text.replace("descripcionx",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("equiposx")) {
                                        text = text.replace("equiposx",
                                                lstLista.getDetalle().get(i).getEquipo() != null
                                                        ? lstLista.getDetalle().get(i).getEquipo()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 28-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento Ciclo de corta y división }
     */
    public static XWPFDocument generarCicloCortaPMFI(List<ActividadSilviculturalDto> lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(22);
            XWPFTableRow row = table.getRow(0);
            CTTrPr trPr = row.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            XWPFTableCell lnewCell = row.getCell(0);
            for (ActividadSilviculturalDto obj : lstLista) {
                if (obj.getCodActividad().equals("DIVICAD")) {
                    XWPFParagraph lnewPara = lnewCell.addParagraph();
                    /*
                     * if (lnewCell.getParagraphs().size() > 0) {
                     * lnewPara = lnewCell.getParagraphs().get(0);
                     * } else {
                     * lnewPara = lnewCell.addParagraph();
                     * }
                     */
                    XWPFRun lnewRun = lnewPara.createRun();
                    String data = obj.getObservacion() == null ? "" : "*" + obj.getObservacion() + ". \n";
                    if (data.contains("\n")) {
                        String[] lines = data.split("\n");
                        lnewRun.setText(lines[0], 0);
                        for (int i = 1; i < lines.length; i++) {
                            lnewRun.addBreak();
                            lnewRun.setText(lines[i]);
                        }
                    } else {
                        lnewRun.setText(data, 0);
                    }
                    lnewRun.setFontFamily("Arial");
                    lnewRun.setFontSize(9);
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 28-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento Labores silviculturales }
     */
    public static XWPFDocument generarLaboresSilviculturalesPMFI(ActividadSilviculturalDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(23);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("laboresx")) {
                                        text = text.replace("laboresx",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("marcax")) {
                                        if (lstLista.getDetalle().get(i).getAccion()) {
                                            text = text.replace("marcax", "X");
                                        } else {
                                            text = text.replace("marcax", "");
                                        }
                                        r.setText(text, 0);
                                    } else if (text.contains("descripcionx")) {
                                        text = text.replace("descripcionx",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 28-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento IDENTIFICACIÓN DE IMPACTOS AMBIENTALES }
     */
    public static XWPFDocument generarIdentificacionImpactosPMFI(ImpactoAmbientalPmfiDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(25);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("actividadx")) {
                                        text = text.replace("actividadx",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("descripcionx")) {
                                        text = text.replace("descripcionx",
                                                lstLista.getDetalle().get(i).getDescripcion() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcion()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("medidasx")) {
                                        text = text.replace("medidasx",
                                                lstLista.getDetalle().get(i).getMedidasProteccion() != null
                                                        ? lstLista.getDetalle().get(i).getMedidasProteccion()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("mitigacionx")) {
                                        text = text.replace("mitigacionx",
                                                lstLista.getDetalle().get(i).getMedidasMitigacion() != null
                                                        ? lstLista.getDetalle().get(i).getMedidasMitigacion()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 28-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento MEDIDAS DE PROTECCIÓN }
     */
    public static XWPFDocument generarMedidasPMFI(ImpactoAmbientalPmfiDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(24);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("medidasx")) {
                                        text = text.replace("medidasx",
                                                lstLista.getDetalle().get(i).getMedidasProteccion() != null
                                                        ? lstLista.getDetalle().get(i).getMedidasProteccion()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("descripcionx")) {
                                        text = text.replace("descripcionx",
                                                lstLista.getDetalle().get(i).getDescripcion() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcion()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 27-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento cronograma}
     */
    public static XWPFDocument procesarCronogramaPMFI(List<CronogramaActividadEntity> lstResult, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(26);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // listar por años
            List<CronogramaActividadEntity> lstAproveMadera = new ArrayList<>();
            List<Integer> lstAnios = new ArrayList<>();
            lstResult.stream().forEach((a) -> {
                a.getDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            for (Integer integer : lstAniosFinal) {
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año" + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(8);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("Mes", 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("MX_" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            for (int i = 0; i < lstResult.size(); i++) {
                XWPFTableRow copiedRow3 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                for (CronogramaActividadDetalleEntity obj : lstResult.get(i).getDetalle()) {
                    for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("actividadx")) {
                                            text5 = text5.replace("actividadx", lstResult.get(i).getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M1")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(1))) {
                                            text5 = text5.replace("M1", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M2")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(2))) {
                                            text5 = text5.replace("M2", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M3")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(3))) {
                                            text5 = text5.replace("M3", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M4")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(4))) {
                                            text5 = text5.replace("M4", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M5")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(5))) {
                                            text5 = text5.replace("M5", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M6")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(6))) {
                                            text5 = text5.replace("M6", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M7")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(7))) {
                                            text5 = text5.replace("M7", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M8")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(8))) {
                                            text5 = text5.replace("M8", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M9")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(9))) {
                                            text5 = text5.replace("M9", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_0")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(10))) {
                                            text5 = text5.replace("M_0", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_1")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(11))) {
                                            text5 = text5.replace("M_1", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_2")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(12))) {
                                            text5 = text5.replace("M_2", "X");
                                            r.setText(text5, 0);
                                        }
                                        for (Integer integer : lstAniosFinal) {
                                            if (text5.contains("MX_" + integer) && integer.equals(obj.getAnio())) {
                                                text5 = text5.replace("MX_" + integer,
                                                        obj.getMes() == 0 ? "X" : obj.getMes().toString());
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("M1")) {
                                        text5 = text5.replace("M1", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M2")) {
                                        text5 = text5.replace("M2", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M3")) {
                                        text5 = text5.replace("M3", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M4")) {
                                        text5 = text5.replace("M4", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M5")) {
                                        text5 = text5.replace("M5", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M6")) {
                                        text5 = text5.replace("M6", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M7")) {
                                        text5 = text5.replace("M7", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M8")) {
                                        text5 = text5.replace("M8", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M9")) {
                                        text5 = text5.replace("M9", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_0")) {
                                        text5 = text5.replace("M_0", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_1")) {
                                        text5 = text5.replace("M_1", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_2")) {
                                        text5 = text5.replace("M_2", "");
                                        r.setText(text5, 0);
                                    }
                                    for (Integer integer : lstAniosFinal) {
                                        if (text5.contains("MX_" + integer)) {
                                            text5 = text5.replace("MX_" + integer, "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (lstResult.get(i).getDetalle().size() > 0) {
                    table.addRow(copiedRow3);
                }
                CTTrPr trPr = copiedRow3.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
            }

            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Actividades principales segun ordenamiento}
     */
    public static void generarActividadesOrdenamientoManejoBosque(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(23);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("71Categoria")) {
                                        text5 = text5.replace("71Categoria",
                                                detalleEntity.getOrdenamiento() != null
                                                        ? detalleEntity.getOrdenamiento().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("71Actividades")) {
                                        text5 = text5.replace("71Actividades",
                                                detalleEntity.getActividad() != null
                                                        ? detalleEntity.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Tratamiento silvicultural - Reforestacion
     */
    public static void generarTratamientoSilviculturalReforestacionManejoBosque(ManejoBosqueEntity mbe,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(35);
            XWPFTableRow row2 = table.getRow(0);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("74BREFORESTACION1")) {
                                        text5 = text5.replace("74BREFORESTACION1",
                                                detalleEntity.getTratamientoSilvicultural() != null
                                                        ? detalleEntity.getTratamientoSilvicultural().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("74BREFORESTACION2")) {
                                        text5 = text5.replace("74BREFORESTACION2",
                                                detalleEntity.getReforestacion() != null
                                                        ? detalleEntity.getReforestacion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, i);
                i++;
            }
            table.removeRow(mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Tratamiento silvicultural - tratamiento previsto}
     */
    public static void generarTratamientoSilviculturalPrevistoManejoBosque(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(34);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("74ATRATAMIENTO")) {
                                        text5 = text5.replace("74ATRATAMIENTO",
                                                detalleEntity.getTratamientoSilvicultural() != null
                                                        ? detalleEntity.getTratamientoSilvicultural().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("TP74ACCION")) {
                                        text5 = text5.replace("TP74ACCION",
                                                detalleEntity.getAccion() == true ? "X" : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("74ADESCRIPCION")) {
                                        text5 = text5.replace("74ADESCRIPCION",
                                                detalleEntity.getDescripcionTratamiento() != null
                                                        ? detalleEntity.getDescripcionTratamiento().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Infraestructura-especificaciones}
     */
    public static void generarInfraestructuraEspecificacionesManejoBosque(XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(30);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            while (i < 3) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    switch (i) {
                                        case 0: {
                                            if (text5.contains("722CINFRA")) {
                                                text5 = text5.replace("722CINFRA", "Caminos \n principales");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("722CESPECIFICACIONES")) {
                                                text5 = text5.replace("722CESPECIFICACIONES",
                                                        "Pendientes menores al 14%\n" +
                                                                " Anchura carrozable de 3 a 4 m\n" +
                                                                " Utilización normalmente restringida a los períodos mas secos del año\n"
                                                                +
                                                                " Contar con un eficiente sistema de drenaje\n" +
                                                                " Evitar atravesar cursos de agua\n" +
                                                                " Los pasos de agua son funcionales\n" +
                                                                " final de la operación se clausuran y se toman medidas para evitar la\n"
                                                                +
                                                                "erosión y restituir las funciones y procesos del sistema natural de drenajes");
                                                r.setText(text5, 0);
                                            }
                                        }
                                            break;
                                        case 1: {
                                            if (text5.contains("722CINFRA")) {
                                                text5 = text5.replace("722CINFRA", "Caminos\n" +
                                                        "secundarios");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("722CESPECIFICACIONES")) {
                                                text5 = text5.replace("722CESPECIFICACIONES",
                                                        "Pendientes menores al 14%\n" +
                                                                " Anchura carrozable de 3 a 4 m\n" +
                                                                " Utilización normalmente restringida a los períodos mas secos del año\n"
                                                                +
                                                                " Contar con un eficiente sistema de drenaje\n" +
                                                                " Evitar atravesar cursos de agua\n" +
                                                                " Los pasos de agua son funcionales\n" +
                                                                " final de la operación se clausuran y se toman medidas para evitar la\n"
                                                                +
                                                                "erosión y restituir las funciones y procesos del sistema natural de drenajes \n");
                                                r.setText(text5, 0);
                                            }
                                        }
                                            break;
                                        case 2: {
                                            if (text5.contains("722CINFRA")) {
                                                text5 = text5.replace("722CINFRA", "Patios de trozas");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("722CESPECIFICACIONES")) {
                                                text5 = text5.replace("722CESPECIFICACIONES",
                                                        "Ubicación, densidad y tamaño serán apropiados, en función al volumen de\n"
                                                                +
                                                                "madera, la maquinaria disponible y las condiciones de sitio\n"
                                                                +
                                                                "Localización próxima a las márgenes de los caminos y a un radio de\n"
                                                                +
                                                                "distancia de operación del tractor forestal de hasta 1 km.\n"
                                                                +
                                                                "Dentro de lo posible, la pendiente en el patio no deberá superar el 5% y\n"
                                                                +
                                                                "drenarán directamente a los caminos ni a los arroyos circundantes\n"
                                                                +
                                                                "deberán estar ubicados a más de 50 m de ríos o quebradas");
                                                r.setText(text5, 0);
                                            }
                                        }
                                            break;
                                    }

                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Especies por proteger FAUNA}
     */
    public static void generarEspecieProtegerFaunaManejoBosque(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(33);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("FAU732ESP")) {
                                        text5 = text5.replace("FAU732ESP",
                                                detalleEntity.getEspecie() != null
                                                        ? detalleEntity.getEspecie().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("FAU732JUST")) {
                                        text5 = text5.replace("FAU732JUST",
                                                detalleEntity.getJustificacionEspecie() != null
                                                        ? detalleEntity.getJustificacionEspecie().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Especies por proteger FLORA}
     */
    public static void generarEspecieProtegerFloraManejoBosque(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(32);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("731Especie")) {
                                        text5 = text5.replace("731Especie",
                                                detalleEntity.getEspecie() != null
                                                        ? detalleEntity.getEspecie().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("731Justificacion")) {
                                        text5 = text5.replace("731Justificacion",
                                                detalleEntity.getJustificacionEspecie() != null
                                                        ? detalleEntity.getJustificacionEspecie().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 20-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Operaciones de Corta y Arrastre}
     */
    public static void generarOperacionCortaArrastreManejoBosquePGMFA(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(31);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("723Operaciones")) {
                                        text5 = text5.replace("723Operaciones",
                                                detalleEntity.getDescripcionOrd() != null
                                                        ? detalleEntity.getDescripcionOrd().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("723MO")) {
                                        text5 = text5.replace("723MO",
                                                detalleEntity.getManoObra() != null
                                                        ? detalleEntity.getManoObra().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("723EM")) {
                                        text5 = text5.replace("723EM",
                                                detalleEntity.getMaquina() != null
                                                        ? detalleEntity.getMaquina().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("723Descripcion")) {
                                        text5 = text5.replace("723Descripcion",
                                                detalleEntity.getDescripcionOtro() != null
                                                        ? detalleEntity.getDescripcionOtro().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 17-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Especificacion sobre Caminos}
     */
    public static void generarEspecificacionCaminosManejoBosquePGMFA(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(29);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : mbe.getListManejoBosqueDetalle()) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("722CIV")) {
                                        text5 = text5.replace("722CIV",
                                                detalleEntity.getInfraestructura() != null
                                                        ? detalleEntity.getInfraestructura().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("722CCT")) {
                                        text5 = text5.replace("722CCT",
                                                detalleEntity.getCaracteristicasTecnicas() != null
                                                        ? detalleEntity.getCaracteristicasTecnicas().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("722CMC")) {
                                        text5 = text5.replace("722CMC",
                                                detalleEntity.getMetodoConstruccion() != null
                                                        ? detalleEntity.getMetodoConstruccion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("722CMO")) {
                                        text5 = text5.replace("722CMO",
                                                detalleEntity.getManoObra() != null
                                                        ? detalleEntity.getManoObra().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("722CMA")) {
                                        text5 = text5.replace("722CMA",
                                                detalleEntity.getMaquina() != null
                                                        ? detalleEntity.getMaquina().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + mbe.getListManejoBosqueDetalle().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 17-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Red de Caminos}
     */
    public static void generarRedCaminosManejoBosquePGMFA(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(28);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("722COtros")) {
                                    String desc = mbe.getListManejoBosqueDetalle()
                                            .get(mbe.getListManejoBosqueDetalle().size() - 1).getDescripcionCamino();
                                    text5 = text5.replace("722COtros", desc != null ? desc : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotal, 0);
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 17-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Especies a aprovechar}
     */
    public static void generarEspeciesAprovecharManejoBosquePGMFA(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {

            List<ManejoBosqueDetalleEntity> lstDetalle = mbe.getListManejoBosqueDetalle().stream()
                    .filter(m -> m.getCodtipoManejoDet().equals("PGMFAACPORESP"))
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(26);
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ManejoBosqueDetalleEntity detalleEntity : lstDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("712Especia")) {
                                        text5 = text5.replace("712Especia",
                                                detalleEntity.getEspecie() != null
                                                        ? detalleEntity.getEspecie().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("712Linea")) {
                                        text5 = text5.replace("712Linea",
                                                detalleEntity.getLineaProduccion() != null
                                                        ? detalleEntity.getLineaProduccion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("712Diametros")) {
                                        text5 = text5.replace("712Diametros",
                                                detalleEntity.getNuDiametro() != null
                                                        ? detalleEntity.getNuDiametro().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 16-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Duracion del Ciclo}
     */
    public static void generarDuracionCicloManejoBosquePGMFA(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(25);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("712DuracionCiclo")) {
                                    String desc = mbe.getListManejoBosqueDetalle()
                                            .get(mbe.getListManejoBosqueDetalle().size() - 1).getDescripcionOrd();
                                    text5 = text5.replace("712DuracionCiclo", desc != null ? desc : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotal, 0);
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 16-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Sistemas de Manejo}
     */
    public static void generarSistemasManejoManejoBosquePGMFA(ManejoBosqueEntity mbe, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(24);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("711OtroSistema")) {
                                    String desc = mbe.getListManejoBosqueDetalle()
                                            .get(mbe.getListManejoBosqueDetalle().size() - 1).getDescripcionOrd();
                                    text5 = text5.replace("711OtroSistema", desc != null ? desc : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotal, 0);
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 14-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Rentabilidad de Manejo Forestal}
     */
    public static void generarRentabilidadManejoForestal(List<RentabilidadManejoForestalDto> lstRentabilidad,
            XWPFDocument doc) {
        try {
            List<Integer> lstAnios = new ArrayList<>();
            lstRentabilidad.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(45);
            // table.setWidthType(TableWidthType.PCT);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            XWPFTableRow row3 = table.getRow(3);
            XWPFTableRow row4 = table.getRow(4);
            XWPFTableRow row5 = table.getRow(5);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();

                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(8);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("", 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("13AI" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                    // row3
                    XWPFTableCell cell3 = row3.createCell();
                    XWPFParagraph p3 = cell3.addParagraph();
                    cell3.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r3 = p1.createRun();
                    r3.setText("", 0);
                    r3.setFontFamily("Arial");
                    r3.setFontSize(8);
                    r3.setBold(true);
                    p3.setAlignment(ParagraphAlignment.CENTER);
                    // row4
                    XWPFTableCell cell4 = row4.createCell();
                    XWPFParagraph p4 = cell4.addParagraph();
                    cell4.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r4 = p4.createRun();
                    r4.setText("13AE" + integer, 0);
                    r4.setFontFamily("Arial");
                    r4.setFontSize(10);
                    r4.setBold(true);
                    p4.setAlignment(ParagraphAlignment.CENTER);
                    // row5
                    XWPFTableCell cell5 = row5.createCell();
                    XWPFParagraph p5 = cell5.addParagraph();
                    cell5.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r5 = p5.createRun();
                    r5.setText("13ST" + integer, 0);
                    r5.setFontFamily("Arial");
                    r5.setFontSize(10);
                    r5.setBold(true);
                    p5.setAlignment(ParagraphAlignment.CENTER);
                }
            }

            List<RentabilidadManejoForestalDto> lstIngresos = lstRentabilidad.stream()
                    .filter(p -> p.getIdTipoRubro() == 1).collect(Collectors.toList());
            List<RentabilidadManejoForestalDto> lstEgresos = lstRentabilidad.stream()
                    .filter(p -> p.getIdTipoRubro() == 2).collect(Collectors.toList());

            // ingresos
            int i = 0;
            for (RentabilidadManejoForestalDto ingreso : lstIngresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("13RUBROINGR")) {
                                        text5 = text5.replace("13RUBROINGR",
                                                ingreso.getRubro() != null ? ingreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("13AI")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : ingreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("13AI" + (integer == 1 ? "" : integer.toString()))
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace(
                                                            "13AI" + (integer == 1 ? "" : integer.toString()),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("13AI")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstIngresos.size());

            // egresos
            i = 0;
            for (RentabilidadManejoForestalDto egreso : lstEgresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row4.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("13RUBROEGRE")) {
                                        text5 = text5.replace("13RUBROEGRE",
                                                egreso.getRubro() != null ? egreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("13AE")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : egreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("13AE" + (integer == 1 ? "" : integer.toString()))
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace(
                                                            "13AE" + (integer == 1 ? "" : integer.toString()),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) - s);
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("13AE")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 3 + lstIngresos.size() + i);
                i++;
            }
            table.removeRow(3 + lstIngresos.size() + lstEgresos.size());

            // totales
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row5.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("13ST")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim()
                                                    .equals("13ST" + (integer == 1 ? "" : integer.toString()))) {
                                                text5 = text5.replace("13ST" + (integer == 1 ? "" : integer.toString()),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 3 + lstIngresos.size() + lstEgresos.size());
            table.removeRow(4 + lstIngresos.size() + lstEgresos.size());

            CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
            width.setType(STTblWidth.AUTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 30-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Potencial de Produccion Forestal}
     */
    public static void generarPotencialProduccion61aPGMFA(List<PotencialProduccionForestalEntity> lstPotencial,
            XWPFDocument doc) {
        try {
            int maxEsp61 = lstPotencial.stream().mapToInt(a -> a.getListPotencialProduccion().size()).max().getAsInt();
            System.out.println("maxEsp61=" + maxEsp61);
            XWPFTable table = doc.getTables().get(19);
            XWPFTable tableTotal = doc.getTables().get(20);
            XWPFTableRow row0Total = tableTotal.getRow(0);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row2 = table.getRow(1);

            if (maxEsp61 > 1) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
                XWPFTableCell copiedCell3 = copiedRow.getCell(3);
                XWPFTableCell copiedCell4 = copiedRow.getCell(4);
                XWPFTableRow copiedRowData = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                XWPFTableCell copiedCellData3 = copiedRowData.getCell(3);
                XWPFTableCell copiedCellData4 = copiedRowData.getCell(4);
                XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0Total.getCtRow().copy(), table);
                XWPFTableCell copiedCellTotal3 = copiedRowTotal.getCell(3);
                XWPFTableCell copiedCellTotal4 = copiedRowTotal.getCell(4);

                row0.removeCell(3);
                row0.removeCell(3);
                row2.removeCell(3);
                row2.removeCell(3);

                row0Total.removeCell(3);
                row0Total.removeCell(3);

                for (int i = 2; i <= maxEsp61; i++) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.TOP);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Especie " + i, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(10);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);

                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("61AE" + i, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);

                    // row0Total
                    XWPFTableCell cellTotal = row0Total.createCell();
                    XWPFParagraph p0Total = cellTotal.addParagraph();
                    cellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r0Total = p0Total.createRun();
                    r0Total.setText("61ATE" + i, 0);
                    r0Total.setFontFamily("Arial");
                    r0Total.setFontSize(10);
                    r0Total.setBold(true);
                    p0Total.setAlignment(ParagraphAlignment.CENTER);
                }
                // newCell 3
                XWPFTableCell newCell6 = row0.createCell();
                XWPFParagraph p6 = newCell6.addParagraph();
                newCell6.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r6 = p6.createRun();
                r6.setText(copiedCell3.getText(), 0);
                r6.setFontFamily("Arial");
                r6.setFontSize(10);
                r6.setBold(true);
                p6.setAlignment(ParagraphAlignment.CENTER);

                // newCell 4
                XWPFTableCell newCell7 = row0.createCell();
                XWPFParagraph p7 = newCell7.addParagraph();
                newCell7.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r7 = p7.createRun();
                r7.setText(copiedCell4.getText(), 0);
                r7.setFontFamily("Arial");
                r7.setFontSize(10);
                r7.setBold(true);
                p7.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 3
                XWPFTableCell newCellData6 = row2.createCell();
                XWPFParagraph pData6 = newCellData6.addParagraph();
                newCellData6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData6 = pData6.createRun();
                rData6.setText(copiedCellData3.getText(), 0);
                rData6.setFontFamily("Arial");
                rData6.setFontSize(10);
                rData6.setBold(true);
                pData6.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 4
                XWPFTableCell newCellData7 = row2.createCell();
                XWPFParagraph pData7 = newCellData7.addParagraph();
                newCellData7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData7 = pData7.createRun();
                rData7.setText(copiedCellData4.getText(), 0);
                rData7.setFontFamily("Arial");
                rData7.setFontSize(10);
                rData7.setBold(true);
                pData7.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 3
                XWPFTableCell newCellTotal6 = row0Total.createCell();
                XWPFParagraph pTotal6 = newCellTotal6.addParagraph();
                newCellTotal6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal6 = pTotal6.createRun();
                rTotal6.setText(copiedCellTotal3.getText(), 0);
                rTotal6.setFontFamily("Arial");
                rTotal6.setFontSize(10);
                rTotal6.setBold(true);
                pTotal6.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 4
                XWPFTableCell newCellTotal7 = row0Total.createCell();
                XWPFParagraph pTotal7 = newCellTotal7.addParagraph();
                newCellTotal7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal7 = pTotal7.createRun();
                rTotal7.setText(copiedCellTotal4.getText(), 0);
                rTotal7.setFontFamily("Arial");
                rTotal7.setFontSize(10);
                rTotal7.setBold(true);
                pTotal7.setAlignment(ParagraphAlignment.CENTER);
            }
            Double total61ATTTB = 0.00, total61ATTHA = 0.00;
            Double[] total61ATTTBList = new Double[3];
            Double[] total61ATTHAList = new Double[3];
            for (int ttt = 0; ttt < total61ATTHAList.length; ttt++) {
                total61ATTHAList[ttt] = 0.00;
                total61ATTTBList[ttt] = 0.00;
            }
            int i = 0;
            boolean existeEspVars = false;
            List<Map<String, Map<String, BigDecimal>>> totalesEspecie = new ArrayList<>();
            for (int tt = 0; tt < maxEsp61; tt++) {
                Map<String, Map<String, BigDecimal>> totalVars = new HashMap<>();
                Map<String, BigDecimal> totalDet = new HashMap<>();
                totalDet.put("ttb", new BigDecimal(0));
                totalDet.put("tha", new BigDecimal(0));
                totalVars.put("N", totalDet);
                Map<String, BigDecimal> totalDet2 = new HashMap<>();
                totalDet2.put("ttb", new BigDecimal(0));
                totalDet2.put("tha", new BigDecimal(0));
                totalVars.put("AB", totalDet2);
                Map<String, BigDecimal> totalDet3 = new HashMap<>();
                totalDet3.put("ttb", new BigDecimal(0));
                totalDet3.put("tha", new BigDecimal(0));
                totalVars.put("Vc", totalDet3);
                totalesEspecie.add(totalVars);
            }
            for (PotencialProduccionForestalEntity detalleEntity : lstPotencial) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                boolean filaVacia = false;
                if (detalleEntity.getListPotencialProduccion() != null) {
                    if (detalleEntity.getListPotencialProduccion().size() > 0) {
                        existeEspVars = true;
                        for (int v = 1; v <= 3; v++) {
                            XWPFTableRow copiedRowVar = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                            CTTrPr trPrVar = copiedRowVar.getCtRow().addNewTrPr();
                            CTHeight htVar = trPrVar.addNewTrHeight();
                            htVar.setVal(BigInteger.valueOf(240));
                            Double totalPorTipoBosque = 0.00, totalPorHA = 0.00;
                            int e = 1;
                            for (XWPFTableCell cell : copiedRowVar.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("61ATB")) {
                                                    if (v == 1) {
                                                        text5 = text5.replace("61ATB",
                                                                detalleEntity.getTipoBosque() != null
                                                                        ? detalleEntity.getTipoBosque().toString()
                                                                        : "");
                                                    } else {
                                                        text5 = "";
                                                    }
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("61ATTB")) {
                                                    text5 = text5.replace("61ATTB",
                                                            totalPorTipoBosque != null ? totalPorTipoBosque.toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    // total61ATTTB +=totalPorTipoBosque;
                                                    total61ATTTBList[v - 1] += totalPorTipoBosque;
                                                } else if (text5.contains("61ATHA")) {
                                                    text5 = text5.replace("61ATHA",
                                                            totalPorHA != null ? totalPorHA.toString() : "");
                                                    r.setText(text5, 0);
                                                    // total61ATTHA+= totalPorHA;
                                                    total61ATTHAList[v - 1] += totalPorHA;
                                                } else if (text5.contains("61AV")) {
                                                    if (v == 1) {
                                                        text5 = text5.replace("61AV", "N");
                                                        r.setText(text5, 0);
                                                    } else if (v == 2) {
                                                        text5 = text5.replace("61AV", "AB m2");
                                                        r.setText(text5, 0);
                                                    } else if (v == 3) {
                                                        text5 = text5.replace("61AV", "Vc m2");
                                                        r.setText(text5, 0);
                                                    }
                                                } else if (text5.contains("61AE" + e)) {
                                                    String especieTexto = "";

                                                    if (detalleEntity.getListPotencialProduccion().get(e - 1)
                                                            .getListPotencialProduccionVariable() != null) {
                                                        if (detalleEntity.getListPotencialProduccion().get(e - 1)
                                                                .getListPotencialProduccionVariable().size() >= v) {
                                                            BigDecimal ttb = (detalleEntity
                                                                    .getListPotencialProduccion().get(e - 1)
                                                                    .getListPotencialProduccionVariable().get(v - 1)
                                                                    .getNuTotalTipoBosque() != null ? detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getNuTotalTipoBosque()
                                                                            : new BigDecimal(0));
                                                            BigDecimal tha = (detalleEntity.getListPotencialProduccion()
                                                                    .get(e - 1).getListPotencialProduccionVariable()
                                                                    .get(v - 1).getTotalHa() != null ? detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getTotalHa()
                                                                            : new BigDecimal(0));
                                                            especieTexto = "TTB= "
                                                                    + (ttb != null ? ttb.toString() : "0.00")
                                                                    + "\n THA= "
                                                                    + (tha != null ? tha.toString() : "0.00");
                                                            /*
                                                             * especieTexto= "TTB= " +
                                                             * (detalleEntity.getListPotencialProduccion().get(e-1).
                                                             * getListPotencialProduccionVariable().get(v-1).
                                                             * getNuTotalTipoBosque()!=null?
                                                             * detalleEntity.getListPotencialProduccion().get(e-1).
                                                             * getListPotencialProduccionVariable().get(v-1).
                                                             * getNuTotalTipoBosque().toString():0.00)
                                                             * + "\n THA= "+(detalleEntity.getListPotencialProduccion().
                                                             * get(e-1).getListPotencialProduccionVariable().get(v-1).
                                                             * getTotalHa()!=null?
                                                             * detalleEntity.getListPotencialProduccion().get(e-1).
                                                             * getListPotencialProduccionVariable().get(v-1).getTotalHa(
                                                             * ).toString():0.00);
                                                             */
                                                            if (v == 1) {
                                                                totalesEspecie.get(e - 1).get("N").replace("ttb",
                                                                        totalesEspecie.get(e - 1).get("N").get("ttb")
                                                                                .add(ttb));
                                                                totalesEspecie.get(e - 1).get("N").replace("tha",
                                                                        totalesEspecie.get(e - 1).get("N").get("tha")
                                                                                .add(tha));
                                                            } else if (v == 2) {
                                                                totalesEspecie.get(e - 1).get("AB").replace("ttb",
                                                                        totalesEspecie.get(e - 1).get("AB").get("ttb")
                                                                                .add(ttb));
                                                                totalesEspecie.get(e - 1).get("AB").replace("tha",
                                                                        totalesEspecie.get(e - 1).get("AB").get("tha")
                                                                                .add(tha));
                                                            } else if (v == 3) {
                                                                totalesEspecie.get(e - 1).get("Vc").replace("ttb",
                                                                        totalesEspecie.get(e - 1).get("Vc").get("ttb")
                                                                                .add(ttb));
                                                                totalesEspecie.get(e - 1).get("Vc").replace("tha",
                                                                        totalesEspecie.get(e - 1).get("Vc").get("tha")
                                                                                .add(tha));
                                                            }

                                                        }
                                                    }
                                                    totalPorTipoBosque += detalleEntity.getListPotencialProduccion()
                                                            .get(e - 1).getListPotencialProduccionVariable().get(v - 1)
                                                            .getNuTotalTipoBosque() != null
                                                                    ? Double.parseDouble(detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getNuTotalTipoBosque()
                                                                            .toString())
                                                                    : 0;
                                                    totalPorHA += detalleEntity.getListPotencialProduccion().get(e - 1)
                                                            .getListPotencialProduccionVariable().get(v - 1)
                                                            .getTotalHa() != null
                                                                    ? Double.parseDouble(detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getTotalHa().toString())
                                                                    : 0;

                                                    text5 = text5.replace("61AE" + e, especieTexto);
                                                    r.setText(text5, 0);
                                                    e++;
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRowVar, 1 + i);
                            i++;
                        }
                    } else
                        filaVacia = true;
                } else
                    filaVacia = true;
                if (filaVacia) {
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("61ATB")) {
                                            text5 = text5.replace("61ATB",
                                                    detalleEntity.getTipoBosque() != null
                                                            ? detalleEntity.getTipoBosque().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61ATTB")) {
                                            text5 = text5.replace("61ATTB", toString(0.00));
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61ATHA")) {
                                            text5 = text5.replace("61ATHA", toString(0.00));
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61AV")) {
                                            text5 = text5.replace("61AV", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61AE")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }

            }
            table.removeRow(1 + i);

            // totales
            if (existeEspVars) {
                for (int t = 1; t <= 3; t++) {
                    XWPFTableRow copiedRowVar = new XWPFTableRow((CTRow) row0Total.getCtRow().copy(), table);
                    CTTrPr trPrVar = copiedRowVar.getCtRow().addNewTrPr();
                    CTHeight htVar = trPrVar.addNewTrHeight();
                    htVar.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRowVar.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("61ATTTB")) {
                                            // text5 = text5.replace("61ATTTB",
                                            // total61ATTTB!=null?total61ATTTB.toString():"");
                                            text5 = text5.replace("61ATTTB", (total61ATTTBList[t - 1]) + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("Total")) {
                                            if (t > 1) {
                                                r.setText("", 0);
                                            }
                                        } else if (text5.contains("61ATTHA")) {
                                            // text5 = text5.replace("61ATTHA",
                                            // total61ATTHA!=null?total61ATTHA.toString():"");
                                            text5 = text5.replace("61ATTHA", total61ATTHAList[t - 1] + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61ATV")) {
                                            if (t == 1) {
                                                text5 = text5.replace("61ATV", "N");
                                                r.setText(text5, 0);
                                            } else if (t == 2) {
                                                text5 = text5.replace("61ATV", "AB m2");
                                                r.setText(text5, 0);
                                            } else if (t == 3) {
                                                text5 = text5.replace("61ATV", "Vc m2");
                                                r.setText(text5, 0);
                                            }
                                        } else if (text5.contains("61ATE")) {
                                            for (int te = 1; te <= totalesEspecie.size(); te++) {
                                                if (text5.contains("61ATE" + te)) {
                                                    String textoTotalEspecie = "";
                                                    if (t == 1) {
                                                        textoTotalEspecie = "TTB= "
                                                                + totalesEspecie.get(te - 1).get("N").get("ttb")
                                                                        .toString()
                                                                + "\n THA= "
                                                                + totalesEspecie.get(te - 1).get("N").get("tha");
                                                    } else if (t == 2) {
                                                        textoTotalEspecie = "TTB= "
                                                                + totalesEspecie.get(te - 1).get("AB").get("ttb")
                                                                        .toString()
                                                                + "\n THA= "
                                                                + totalesEspecie.get(te - 1).get("AB").get("tha");
                                                    } else if (t == 3) {
                                                        textoTotalEspecie = "TTB= "
                                                                + totalesEspecie.get(te - 1).get("Vc").get("ttb")
                                                                        .toString()
                                                                + "\n THA= "
                                                                + totalesEspecie.get(te - 1).get("Vc").get("tha");
                                                    }
                                                    text5 = text5.replace("61ATE" + te, textoTotalEspecie);
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    tableTotal.addRow(copiedRowVar, t);
                }
                // remove
                tableTotal.removeRow(0);
            } else {
                CTTrPr trPr = row0Total.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : row0Total.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("61ATTTB")) {
                                        text5 = text5.replace("61ATTTB", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61ATTHA")) {
                                        text5 = text5.replace("61ATTHA", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61AT")) {
                                        r.setText("", 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 31-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Potencial de Produccion Forestal}
     */
    public static void generarPotencialProduccion61bPGMFA(List<PotencialProduccionForestalEntity> lstPotencial,
            XWPFDocument doc) {
        try {
            int maxEsp61 = lstPotencial.stream().mapToInt(a -> a.getListPotencialProduccion().size()).max().getAsInt();
            System.out.println("maxEsp61=" + maxEsp61);
            XWPFTable table = doc.getTables().get(21);
            XWPFTable tableTotal = doc.getTables().get(22);
            XWPFTableRow row0Total = tableTotal.getRow(0);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row2 = table.getRow(1);

            if (maxEsp61 > 1) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
                XWPFTableCell copiedCell3 = copiedRow.getCell(3);
                XWPFTableCell copiedCell4 = copiedRow.getCell(4);
                XWPFTableRow copiedRowData = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                XWPFTableCell copiedCellData3 = copiedRowData.getCell(3);
                XWPFTableCell copiedCellData4 = copiedRowData.getCell(4);
                XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0Total.getCtRow().copy(), table);
                XWPFTableCell copiedCellTotal3 = copiedRowTotal.getCell(3);
                XWPFTableCell copiedCellTotal4 = copiedRowTotal.getCell(4);

                row0.removeCell(3);
                row0.removeCell(3);
                row2.removeCell(3);
                row2.removeCell(3);

                row0Total.removeCell(3);
                row0Total.removeCell(3);

                for (int i = 2; i <= maxEsp61; i++) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.TOP);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Especie " + i, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(10);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);

                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("61AE" + i, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);

                    // row0Total
                    XWPFTableCell cellTotal = row0Total.createCell();
                    XWPFParagraph p0Total = cellTotal.addParagraph();
                    cellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r0Total = p0Total.createRun();
                    r0Total.setText("61ATE" + i, 0);
                    r0Total.setFontFamily("Arial");
                    r0Total.setFontSize(10);
                    r0Total.setBold(true);
                    p0Total.setAlignment(ParagraphAlignment.CENTER);
                }
                // newCell 3
                XWPFTableCell newCell6 = row0.createCell();
                XWPFParagraph p6 = newCell6.addParagraph();
                newCell6.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r6 = p6.createRun();
                r6.setText(copiedCell3.getText(), 0);
                r6.setFontFamily("Arial");
                r6.setFontSize(10);
                r6.setBold(true);
                p6.setAlignment(ParagraphAlignment.CENTER);

                // newCell 4
                XWPFTableCell newCell7 = row0.createCell();
                XWPFParagraph p7 = newCell7.addParagraph();
                newCell7.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r7 = p7.createRun();
                r7.setText(copiedCell4.getText(), 0);
                r7.setFontFamily("Arial");
                r7.setFontSize(10);
                r7.setBold(true);
                p7.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 3
                XWPFTableCell newCellData6 = row2.createCell();
                XWPFParagraph pData6 = newCellData6.addParagraph();
                newCellData6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData6 = pData6.createRun();
                rData6.setText(copiedCellData3.getText(), 0);
                rData6.setFontFamily("Arial");
                rData6.setFontSize(10);
                rData6.setBold(true);
                pData6.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 4
                XWPFTableCell newCellData7 = row2.createCell();
                XWPFParagraph pData7 = newCellData7.addParagraph();
                newCellData7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData7 = pData7.createRun();
                rData7.setText(copiedCellData4.getText(), 0);
                rData7.setFontFamily("Arial");
                rData7.setFontSize(10);
                rData7.setBold(true);
                pData7.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 3
                XWPFTableCell newCellTotal6 = row0Total.createCell();
                XWPFParagraph pTotal6 = newCellTotal6.addParagraph();
                newCellTotal6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal6 = pTotal6.createRun();
                rTotal6.setText(copiedCellTotal3.getText(), 0);
                rTotal6.setFontFamily("Arial");
                rTotal6.setFontSize(10);
                rTotal6.setBold(true);
                pTotal6.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 4
                XWPFTableCell newCellTotal7 = row0Total.createCell();
                XWPFParagraph pTotal7 = newCellTotal7.addParagraph();
                newCellTotal7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal7 = pTotal7.createRun();
                rTotal7.setText(copiedCellTotal4.getText(), 0);
                rTotal7.setFontFamily("Arial");
                rTotal7.setFontSize(10);
                rTotal7.setBold(true);
                pTotal7.setAlignment(ParagraphAlignment.CENTER);
            }
            Double total61ATTTB = 0.00, total61ATTHA = 0.00;
            Double[] total61ATTTBList = new Double[2];
            Double[] total61ATTHAList = new Double[2];
            for (int ttt = 0; ttt < total61ATTHAList.length; ttt++) {
                total61ATTHAList[ttt] = 0.00;
                total61ATTTBList[ttt] = 0.00;
            }
            int i = 0;
            boolean existeEspVars = false;
            List<Map<String, Map<String, BigDecimal>>> totalesEspecie = new ArrayList<>();
            for (int tt = 0; tt < maxEsp61; tt++) {
                Map<String, Map<String, BigDecimal>> totalVars = new HashMap<>();
                Map<String, BigDecimal> totalDet = new HashMap<>();
                totalDet.put("ttb", new BigDecimal(0));
                totalDet.put("tha", new BigDecimal(0));
                totalVars.put("N", totalDet);
                Map<String, BigDecimal> totalDet2 = new HashMap<>();
                totalDet2.put("ttb", new BigDecimal(0));
                totalDet2.put("tha", new BigDecimal(0));
                totalVars.put("AB", totalDet2);
                totalesEspecie.add(totalVars);
            }
            for (PotencialProduccionForestalEntity detalleEntity : lstPotencial) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                boolean filaVacia = false;
                if (detalleEntity.getListPotencialProduccion() != null) {
                    if (detalleEntity.getListPotencialProduccion().size() > 0) {
                        existeEspVars = true;
                        for (int v = 1; v <= 2; v++) {
                            XWPFTableRow copiedRowVar = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                            CTTrPr trPrVar = copiedRowVar.getCtRow().addNewTrPr();
                            CTHeight htVar = trPrVar.addNewTrHeight();
                            htVar.setVal(BigInteger.valueOf(240));
                            Double totalPorTipoBosque = 0.00, totalPorHA = 0.00;
                            int e = 1;
                            for (XWPFTableCell cell : copiedRowVar.getTableCells()) {
                                for (XWPFParagraph p : cell.getParagraphs()) {
                                    for (XWPFRun r : p.getRuns()) {
                                        if (r != null) {
                                            String text5 = r.getText(0);
                                            if (text5 != null) {
                                                if (text5.contains("61BTB")) {
                                                    if (v == 1) {
                                                        text5 = text5.replace("61BTB",
                                                                detalleEntity.getTipoBosque() != null
                                                                        ? detalleEntity.getTipoBosque().toString()
                                                                        : "");
                                                    } else {
                                                        text5 = "";
                                                    }
                                                    r.setText(text5, 0);
                                                } else if (text5.contains("61BTTB")) {
                                                    text5 = text5.replace("61BTTB",
                                                            totalPorTipoBosque != null ? totalPorTipoBosque.toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    // total61ATTTB +=totalPorTipoBosque;
                                                    total61ATTTBList[v - 1] += totalPorTipoBosque;
                                                } else if (text5.contains("61BTHA")) {
                                                    text5 = text5.replace("61BTHA",
                                                            totalPorHA != null ? totalPorHA.toString() : "");
                                                    r.setText(text5, 0);
                                                    // total61ATTHA+= totalPorHA;
                                                    total61ATTHAList[v - 1] += totalPorHA;
                                                } else if (text5.contains("61BV")) {
                                                    if (v == 1) {
                                                        text5 = text5.replace("61BV", "N");
                                                        r.setText(text5, 0);
                                                    } else if (v == 2) {
                                                        text5 = text5.replace("61BV", "AB m2");
                                                        r.setText(text5, 0);
                                                    }
                                                } else if (text5.contains("61BE" + e)) {
                                                    String especieTexto = "";

                                                    if (detalleEntity.getListPotencialProduccion().get(e - 1)
                                                            .getListPotencialProduccionVariable() != null) {
                                                        if (detalleEntity.getListPotencialProduccion().get(e - 1)
                                                                .getListPotencialProduccionVariable().size() >= v) {
                                                            BigDecimal ttb = (detalleEntity
                                                                    .getListPotencialProduccion().get(e - 1)
                                                                    .getListPotencialProduccionVariable().get(v - 1)
                                                                    .getNuTotalTipoBosque() != null ? detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getNuTotalTipoBosque()
                                                                            : new BigDecimal(0));
                                                            BigDecimal tha = (detalleEntity.getListPotencialProduccion()
                                                                    .get(e - 1).getListPotencialProduccionVariable()
                                                                    .get(v - 1).getTotalHa() != null ? detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getTotalHa()
                                                                            : new BigDecimal(0));
                                                            especieTexto = "TTB= "
                                                                    + (ttb != null ? ttb.toString() : "0.00")
                                                                    + "\n THA= "
                                                                    + (tha != null ? tha.toString() : "0.00");
                                                            if (v == 1) {
                                                                totalesEspecie.get(e - 1).get("N").replace("ttb",
                                                                        totalesEspecie.get(e - 1).get("N").get("ttb")
                                                                                .add(ttb));
                                                                totalesEspecie.get(e - 1).get("N").replace("tha",
                                                                        totalesEspecie.get(e - 1).get("N").get("tha")
                                                                                .add(tha));
                                                            } else if (v == 2) {
                                                                totalesEspecie.get(e - 1).get("AB").replace("ttb",
                                                                        totalesEspecie.get(e - 1).get("AB").get("ttb")
                                                                                .add(ttb));
                                                                totalesEspecie.get(e - 1).get("AB").replace("tha",
                                                                        totalesEspecie.get(e - 1).get("AB").get("tha")
                                                                                .add(tha));
                                                            }

                                                        }
                                                    }
                                                    totalPorTipoBosque += detalleEntity.getListPotencialProduccion()
                                                            .get(e - 1).getListPotencialProduccionVariable().get(v - 1)
                                                            .getNuTotalTipoBosque() != null
                                                                    ? Double.parseDouble(detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getNuTotalTipoBosque()
                                                                            .toString())
                                                                    : 0;
                                                    totalPorHA += detalleEntity.getListPotencialProduccion().get(e - 1)
                                                            .getListPotencialProduccionVariable().get(v - 1)
                                                            .getTotalHa() != null
                                                                    ? Double.parseDouble(detalleEntity
                                                                            .getListPotencialProduccion().get(e - 1)
                                                                            .getListPotencialProduccionVariable()
                                                                            .get(v - 1).getTotalHa().toString())
                                                                    : 0;

                                                    text5 = text5.replace("61BE" + e, especieTexto);
                                                    r.setText(text5, 0);
                                                    e++;
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            table.addRow(copiedRowVar, 1 + i);
                            i++;
                        }
                    } else
                        filaVacia = true;
                } else
                    filaVacia = true;
                if (filaVacia) {
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("61BTB")) {
                                            text5 = text5.replace("61BTB",
                                                    detalleEntity.getTipoBosque() != null
                                                            ? detalleEntity.getTipoBosque().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61BTTB")) {
                                            text5 = text5.replace("61BTTB", toString(0.00));
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61BTHA")) {
                                            text5 = text5.replace("61BTHA", toString(0.00));
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61BV")) {
                                            text5 = text5.replace("61BV", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61BE")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }

            }
            table.removeRow(1 + i);

            // totales
            if (existeEspVars) {
                for (int t = 1; t <= 2; t++) {
                    XWPFTableRow copiedRowVar = new XWPFTableRow((CTRow) row0Total.getCtRow().copy(), table);
                    CTTrPr trPrVar = copiedRowVar.getCtRow().addNewTrPr();
                    CTHeight htVar = trPrVar.addNewTrHeight();
                    htVar.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRowVar.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("61ATTTB")) {
                                            text5 = text5.replace("61ATTTB", (total61ATTTBList[t - 1]) + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("Total")) {
                                            if (t > 1) {
                                                r.setText("", 0);
                                            }
                                        } else if (text5.contains("61ATTHA")) {
                                            text5 = text5.replace("61ATTHA", total61ATTHAList[t - 1] + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("61ATV")) {
                                            if (t == 1) {
                                                text5 = text5.replace("61ATV", "N");
                                                r.setText(text5, 0);
                                            } else if (t == 2) {
                                                text5 = text5.replace("61ATV", "AB m2");
                                                r.setText(text5, 0);
                                            }
                                        } else if (text5.contains("61ATE")) {
                                            for (int te = 1; te <= totalesEspecie.size(); te++) {
                                                if (text5.contains("61ATE" + te)) {
                                                    String textoTotalEspecie = "";
                                                    if (t == 1) {
                                                        textoTotalEspecie = "TTB= "
                                                                + totalesEspecie.get(te - 1).get("N").get("ttb")
                                                                        .toString()
                                                                + "\n THA= "
                                                                + totalesEspecie.get(te - 1).get("N").get("tha");
                                                    } else if (t == 2) {
                                                        textoTotalEspecie = "TTB= "
                                                                + totalesEspecie.get(te - 1).get("AB").get("ttb")
                                                                        .toString()
                                                                + "\n THA= "
                                                                + totalesEspecie.get(te - 1).get("AB").get("tha");
                                                    }
                                                    text5 = text5.replace("61ATE" + te, textoTotalEspecie);
                                                    r.setText(text5, 0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    tableTotal.addRow(copiedRowVar, t);
                }
                // remove
                tableTotal.removeRow(0);
            } else {
                CTTrPr trPr = row0Total.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : row0Total.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("61BATTB")) {
                                        text5 = text5.replace("61ATTTB", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61ATTHA")) {
                                        text5 = text5.replace("61ATTHA", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61AT")) {
                                        r.setText("", 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 14-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Potencial de Produccion Forestal}
     */
    public static void generarPotencialProduccionForestal(List<PotencialProduccionForestalEntity> lstPotencial,
            XWPFDocument doc) {
        try {

            List<PotencialProduccionForestalEntity> lista61 = lstPotencial.stream()
                    .filter(p -> p.getCodigoSubTipoPotencialProdForestal().equals("PGMFAA"))
                    .collect(Collectors.toList());
            List<PotencialProduccionForestalEntity> lista62 = lstPotencial.stream()
                    .filter(p -> p.getCodigoSubTipoPotencialProdForestal().equals("PGMFAB"))
                    .collect(Collectors.toList());
            List<Integer> lst_max_esp61 = new ArrayList<>();

            // potencial maderable 6.1.a
            int maxEsp61 = lista61.stream().mapToInt(a -> a.getListPotencialProduccion().size()).max().getAsInt();
            System.out.println("maxEsp61=" + maxEsp61);

            XWPFTable table = doc.getTables().get(19);
            XWPFTable tableTotal = doc.getTables().get(20);
            XWPFTableRow row0Total = tableTotal.getRow(0);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row2 = table.getRow(1);
            if (maxEsp61 > 2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
                XWPFTableCell copiedCell6 = copiedRow.getCell(5);
                XWPFTableCell copiedCell7 = copiedRow.getCell(6);
                XWPFTableRow copiedRowData = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                XWPFTableCell copiedCellData6 = copiedRowData.getCell(5);
                XWPFTableCell copiedCellData7 = copiedRowData.getCell(6);
                XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0Total.getCtRow().copy(), table);
                XWPFTableCell copiedCellTotal6 = copiedRowTotal.getCell(5);
                XWPFTableCell copiedCellTotal7 = copiedRowTotal.getCell(6);

                row0.removeCell(4);
                row0.removeCell(4);
                row0.removeCell(4);
                row2.removeCell(4);
                row2.removeCell(4);
                row2.removeCell(4);

                row0Total.removeCell(4);
                row0Total.removeCell(4);
                row0Total.removeCell(4);

                for (int i = 3; i <= maxEsp61; i++) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.TOP);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Especie " + i, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(10);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);

                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("61AE" + i, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);

                    // row0Total
                    XWPFTableCell cellTotal = row0Total.createCell();
                    XWPFParagraph p0Total = cellTotal.addParagraph();
                    cellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r0Total = p0Total.createRun();
                    r0Total.setText("61ATE" + i, 0);
                    r0Total.setFontFamily("Arial");
                    r0Total.setFontSize(10);
                    r0Total.setBold(true);
                    p0Total.setAlignment(ParagraphAlignment.CENTER);
                }
                // newCell 6
                XWPFTableCell newCell6 = row0.createCell();
                XWPFParagraph p6 = newCell6.addParagraph();
                newCell6.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r6 = p6.createRun();
                r6.setText(copiedCell6.getText(), 0);
                r6.setFontFamily("Arial");
                r6.setFontSize(10);
                r6.setBold(true);
                p6.setAlignment(ParagraphAlignment.CENTER);

                // newCell 7
                XWPFTableCell newCell7 = row0.createCell();
                XWPFParagraph p7 = newCell7.addParagraph();
                newCell7.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r7 = p7.createRun();
                r7.setText(copiedCell7.getText(), 0);
                r7.setFontFamily("Arial");
                r7.setFontSize(10);
                r7.setBold(true);
                p7.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 6
                XWPFTableCell newCellData6 = row2.createCell();
                XWPFParagraph pData6 = newCellData6.addParagraph();
                newCellData6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData6 = pData6.createRun();
                rData6.setText(copiedCellData6.getText(), 0);
                rData6.setFontFamily("Arial");
                rData6.setFontSize(10);
                rData6.setBold(true);
                pData6.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 7
                XWPFTableCell newCellData7 = row2.createCell();
                XWPFParagraph pData7 = newCellData7.addParagraph();
                newCellData7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData7 = pData7.createRun();
                rData7.setText(copiedCellData7.getText(), 0);
                rData7.setFontFamily("Arial");
                rData7.setFontSize(10);
                rData7.setBold(true);
                pData7.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 6
                XWPFTableCell newCellTotal6 = row0Total.createCell();
                XWPFParagraph pTotal6 = newCellTotal6.addParagraph();
                newCellTotal6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal6 = pTotal6.createRun();
                rTotal6.setText(copiedCellTotal6.getText(), 0);
                rTotal6.setFontFamily("Arial");
                rTotal6.setFontSize(10);
                rTotal6.setBold(true);
                pTotal6.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 7
                XWPFTableCell newCellTotal7 = row0Total.createCell();
                XWPFParagraph pTotal7 = newCellTotal7.addParagraph();
                newCellTotal7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal7 = pTotal7.createRun();
                rTotal7.setText(copiedCellTotal7.getText(), 0);
                rTotal7.setFontFamily("Arial");
                rTotal7.setFontSize(10);
                rTotal7.setBold(true);
                pTotal7.setAlignment(ParagraphAlignment.CENTER);
            }

            Double total61ATTTB = 0.00, total61ATTHA = 0.00;
            int i = 0;
            for (PotencialProduccionForestalEntity detalleEntity : lista61) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                Double totalPorTipoBosque = 0.00, totalPorHA = 0.00;
                int e = 1;
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("61ATB")) {
                                        text5 = text5.replace("61ATB",
                                                detalleEntity.getTipoBosque() != null
                                                        ? detalleEntity.getTipoBosque().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61AV")) {
                                        text5 = text5.replace("61AV",
                                                detalleEntity.getVariable() != null
                                                        ? detalleEntity.getVariable().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61AE" + e)) {
                                        if (detalleEntity.getListPotencialProduccion().size() > 0
                                                && e <= detalleEntity.getListPotencialProduccion().size()) {
                                            text5 = text5.replace("61AE" + e,
                                                    detalleEntity.getListPotencialProduccion().get(e - 1)
                                                            .getEspecie() != null
                                                                    ? detalleEntity.getListPotencialProduccion()
                                                                            .get(e - 1).getEspecie().toString()
                                                                    : "");
                                            r.setText(text5, 0);

                                            for (PotencialProduccionForestalVariableEntity variable : detalleEntity
                                                    .getListPotencialProduccion().get(e - 1)
                                                    .getListPotencialProduccionVariable()) {
                                                totalPorTipoBosque += variable.getNuTotalTipoBosque() != null
                                                        ? Double.parseDouble(variable.getNuTotalTipoBosque().toString())
                                                        : 0;
                                                totalPorHA += variable.getTotalHa() != null
                                                        ? Double.parseDouble(variable.getTotalHa().toString())
                                                        : 0;
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }

                                        e++;
                                    }

                                    if (e <= maxEsp61) {
                                        for (int j = e; j <= maxEsp61; j++) {
                                            if (text5.contains("61AE" + j)) {
                                                text5 = text5.replace("61AE" + j, "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }

                                    if (text5.contains("61ATTB")) {
                                        text5 = text5.replace("61ATTB",
                                                totalPorTipoBosque != null ? totalPorTipoBosque.toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61ATHA")) {
                                        text5 = text5.replace("61ATHA",
                                                totalPorHA != null ? totalPorHA.toString() : "");
                                        r.setText(text5, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                total61ATTTB += totalPorTipoBosque;
                total61ATTHA += totalPorHA;

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lista61.size());

            CTTrPr trPr = row0Total.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : row0Total.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("61ATTTB")) {
                                    text5 = text5.replace("61ATTTB",
                                            total61ATTTB != null ? total61ATTTB.toString() : "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("61ATTHA")) {
                                    text5 = text5.replace("61ATTHA",
                                            total61ATTHA != null ? total61ATTHA.toString() : "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("61AT")) {
                                    r.setText("", 0);
                                }
                            }
                        }
                    }
                }
            }
            // tabla 62
            int maxEsp62 = lista62.stream().mapToInt(a -> a.getListPotencialProduccion().size()).max().getAsInt();
            System.out.println("maxEsp61=" + maxEsp62);

            XWPFTable table62 = doc.getTables().get(21);
            XWPFTable tableTotal62 = doc.getTables().get(22);
            XWPFTableRow row0Total62 = tableTotal62.getRow(0);
            XWPFTableRow row062 = table62.getRow(0);
            XWPFTableRow row262 = table62.getRow(1);
            if (maxEsp62 > 2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row062.getCtRow().copy(), table);
                XWPFTableCell copiedCell6 = copiedRow.getCell(5);
                XWPFTableCell copiedCell7 = copiedRow.getCell(6);
                XWPFTableRow copiedRowData = new XWPFTableRow((CTRow) row262.getCtRow().copy(), table);
                XWPFTableCell copiedCellData6 = copiedRowData.getCell(5);
                XWPFTableCell copiedCellData7 = copiedRowData.getCell(6);
                XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row0Total62.getCtRow().copy(), table);
                XWPFTableCell copiedCellTotal6 = copiedRowTotal.getCell(5);
                XWPFTableCell copiedCellTotal7 = copiedRowTotal.getCell(6);

                row062.removeCell(4);
                row062.removeCell(4);
                row062.removeCell(4);
                row262.removeCell(4);
                row262.removeCell(4);
                row262.removeCell(4);

                row0Total62.removeCell(4);
                row0Total62.removeCell(4);
                row0Total62.removeCell(4);

                for (int j = 3; j <= maxEsp61; j++) {
                    // row0
                    XWPFTableCell cell0 = row062.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.TOP);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Especie " + j, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(10);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);

                    // row2
                    XWPFTableCell cell2 = row262.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("61AE" + j, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);

                    // row0Total
                    XWPFTableCell cellTotal = row0Total62.createCell();
                    XWPFParagraph p0Total = cellTotal.addParagraph();
                    cellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r0Total = p0Total.createRun();
                    r0Total.setText("61ATE" + j, 0);
                    r0Total.setFontFamily("Arial");
                    r0Total.setFontSize(10);
                    r0Total.setBold(true);
                    p0Total.setAlignment(ParagraphAlignment.CENTER);
                }
                // newCell 6
                XWPFTableCell newCell6 = row062.createCell();
                XWPFParagraph p6 = newCell6.addParagraph();
                newCell6.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r6 = p6.createRun();
                r6.setText(copiedCell6.getText(), 0);
                r6.setFontFamily("Arial");
                r6.setFontSize(10);
                r6.setBold(true);
                p6.setAlignment(ParagraphAlignment.CENTER);

                // newCell 7
                XWPFTableCell newCell7 = row062.createCell();
                XWPFParagraph p7 = newCell7.addParagraph();
                newCell7.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun r7 = p7.createRun();
                r7.setText(copiedCell7.getText(), 0);
                r7.setFontFamily("Arial");
                r7.setFontSize(10);
                r7.setBold(true);
                p7.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 6
                XWPFTableCell newCellData6 = row262.createCell();
                XWPFParagraph pData6 = newCellData6.addParagraph();
                newCellData6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData6 = pData6.createRun();
                rData6.setText(copiedCellData6.getText(), 0);
                rData6.setFontFamily("Arial");
                rData6.setFontSize(10);
                rData6.setBold(true);
                pData6.setAlignment(ParagraphAlignment.CENTER);

                // newCellData 7
                XWPFTableCell newCellData7 = row262.createCell();
                XWPFParagraph pData7 = newCellData7.addParagraph();
                newCellData7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rData7 = pData7.createRun();
                rData7.setText(copiedCellData7.getText(), 0);
                rData7.setFontFamily("Arial");
                rData7.setFontSize(10);
                rData7.setBold(true);
                pData7.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 6
                XWPFTableCell newCellTotal6 = row0Total62.createCell();
                XWPFParagraph pTotal6 = newCellTotal6.addParagraph();
                newCellTotal6.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal6 = pTotal6.createRun();
                rTotal6.setText(copiedCellTotal6.getText(), 0);
                rTotal6.setFontFamily("Arial");
                rTotal6.setFontSize(10);
                rTotal6.setBold(true);
                pTotal6.setAlignment(ParagraphAlignment.CENTER);

                // newCellTotal 7
                XWPFTableCell newCellTotal7 = row0Total62.createCell();
                XWPFParagraph pTotal7 = newCellTotal7.addParagraph();
                newCellTotal7.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                XWPFRun rTotal7 = pTotal7.createRun();
                rTotal7.setText(copiedCellTotal7.getText(), 0);
                rTotal7.setFontFamily("Arial");
                rTotal7.setFontSize(10);
                rTotal7.setBold(true);
                pTotal7.setAlignment(ParagraphAlignment.CENTER);
            }
            Double total62ATTTB = 0.00, total62ATTHA = 0.00;
            int k = 0;
            for (PotencialProduccionForestalEntity detalleEntity : lista62) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row262.getCtRow().copy(), table);
                CTTrPr trPr62 = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht62 = trPr62.addNewTrHeight();
                ht62.setVal(BigInteger.valueOf(240));

                Double totalPorTipoBosque = 0.00, totalPorHA = 0.00;
                int e = 1;
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("61BTB")) {
                                        text5 = text5.replace("61BTB",
                                                detalleEntity.getTipoBosque() != null
                                                        ? detalleEntity.getTipoBosque().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61BV")) {
                                        text5 = text5.replace("61BV",
                                                detalleEntity.getVariable() != null
                                                        ? detalleEntity.getVariable().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61BE" + e)) {
                                        if (detalleEntity.getListPotencialProduccion().size() > 0
                                                && e <= detalleEntity.getListPotencialProduccion().size()) {
                                            text5 = text5.replace("61BE" + e,
                                                    detalleEntity.getListPotencialProduccion().get(e - 1)
                                                            .getEspecie() != null
                                                                    ? detalleEntity.getListPotencialProduccion()
                                                                            .get(e - 1).getEspecie().toString()
                                                                    : "");
                                            r.setText(text5, 0);

                                            for (PotencialProduccionForestalVariableEntity variable : detalleEntity
                                                    .getListPotencialProduccion().get(e - 1)
                                                    .getListPotencialProduccionVariable()) {

                                                totalPorTipoBosque += variable.getNuTotalTipoBosque() != null
                                                        ? Double.parseDouble(variable.getNuTotalTipoBosque().toString())
                                                        : 0;
                                                totalPorHA += variable.getTotalHa() != null
                                                        ? Double.parseDouble(variable.getTotalHa().toString())
                                                        : 0;
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }

                                        e++;

                                    }

                                    if (e <= maxEsp62) {
                                        for (int j = e; j <= maxEsp62; j++) {
                                            if (text5.contains("61BE" + j)) {
                                                text5 = text5.replace("61BE" + j, "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }

                                    if (text5.contains("61BTTB")) {
                                        text5 = text5.replace("61BTTB",
                                                totalPorTipoBosque != null ? totalPorTipoBosque.toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("61BTHA")) {
                                        text5 = text5.replace("61BTHA",
                                                totalPorHA != null ? totalPorHA.toString() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                total62ATTTB += totalPorTipoBosque;
                total62ATTHA += totalPorHA;

                table62.addRow(copiedRow, 1 + k);
                k++;
            }
            table62.removeRow(1 + lista62.size());
            CTTrPr trPr62 = row0Total62.getCtRow().addNewTrPr();
            CTHeight ht62 = trPr62.addNewTrHeight();
            ht62.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : row0Total62.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("61ATTTB")) {
                                    text5 = text5.replace("61ATTTB",
                                            total62ATTTB != null ? total62ATTTB.toString() : "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("61ATTHA")) {
                                    text5 = text5.replace("61ATTHA",
                                            total62ATTHA != null ? total62ATTHA.toString() : "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("61AT")) {
                                    r.setText("", 0);
                                }
                            }
                        }
                    }
                }
            }

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Plan de Contingencia Ambiental}
     */
    public static void generarPlanContingenciaEvaluacionImpactoAmbiental(
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(20);
            XWPFTableRow row1 = table.getRow(2);

            int i = 0;
            for (EvaluacionAmbientalAprovechamientoEntity planAccion : lstPlanAccion) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("82contingencia")) {
                                        text5 = text5.replace("82contingencia",
                                                planAccion.getContingencia() != null
                                                        ? planAccion.getContingencia().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82acciones")) {
                                        text5 = text5.replace("82acciones",
                                                planAccion.getAcciones() != null ? planAccion.getAcciones().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("822responsable")) {
                                        text5 = text5.replace("822responsable",
                                                planAccion.getResponsableSecundario() != null
                                                        ? planAccion.getResponsableSecundario().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstPlanAccion.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Plan de Vigilancia y Seguimiento Ambiental}
     */
    public static void generarPlanVigilanciaEvaluacionImpactoAmbiental(
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(19);
            XWPFTableRow row1 = table.getRow(1);

            int i = 0;
            for (EvaluacionAmbientalAprovechamientoEntity planAccion : lstPlanAccion) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("82descripcion")) {
                                        text5 = text5.replace("82descripcion",
                                                planAccion.getImpacto() != null ? planAccion.getImpacto().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82medidasCA")) {
                                        text5 = text5.replace("82medidasCA",
                                                planAccion.getMedidasControl() != null
                                                        ? planAccion.getMedidasControl().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82medidasMO")) {
                                        text5 = text5.replace("82medidasMO",
                                                planAccion.getMedidasMonitoreo() != null
                                                        ? planAccion.getMedidasMonitoreo().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82frecuencia")) {
                                        text5 = text5.replace("82frecuencia",
                                                planAccion.getFrecuencia() != null
                                                        ? planAccion.getFrecuencia().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82responsable")) {
                                        text5 = text5.replace("82responsable",
                                                planAccion.getResponsable() != null
                                                        ? planAccion.getResponsable().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstPlanAccion.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Plan de Accion Preventivo-Corrector}
     */
    public static void generarPlanAccionEvaluacionImpactoAmbiental(
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(18);
            XWPFTableRow row1 = table.getRow(2);

            int i = 0;
            for (EvaluacionAmbientalAprovechamientoEntity planAccion : lstPlanAccion) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("82ACT")) {
                                        text5 = text5.replace("82ACT",
                                                planAccion.getNombreAprovechamiento() != null
                                                        ? planAccion.getNombreAprovechamiento().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82IMP")) {
                                        text5 = text5.replace("82IMP",
                                                planAccion.getImpacto() != null ? planAccion.getImpacto().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("82MED")) {
                                        text5 = text5.replace("82MED",
                                                planAccion.getMedidasControl() != null
                                                        ? planAccion.getMedidasControl().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstPlanAccion.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Cronograma de Actividades}
     */
    public static void generarCronogramaActividadesPOCC(List<CronogramaActividadEntity> lstCrono, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(28);
            XWPFTableRow row1 = table.getRow(4);

            int i = 0;
            for (CronogramaActividadEntity actividad : lstCrono) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("actividadx")) {
                                        text5 = text5.replace("actividadx",
                                                actividad.getActividad() != null ? actividad.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_2")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                                        text5 = text5.replace("M_2",
                                                lstDetalle != null ? (lstDetalle.size() > 0 ? "X" : "") : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_3")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                                        text5 = text5.replace("M_3",
                                                lstDetalle != null ? (lstDetalle.size() > 0 ? "X" : "") : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                                        if (lstDetalle != null) {
                                            boolean breaker = false;
                                            for (CronogramaActividadDetalleEntity detalle : lstDetalle) {
                                                for (int mes = 1; mes <= 12; mes++) {
                                                    if (detalle.getMes() == mes && text5.trim().equals("M" + mes)) {
                                                        text5 = text5.replace("M" + mes, "X");
                                                        r.setText(text5, 0);
                                                        breaker = true;
                                                        break;
                                                    }
                                                }
                                                if (breaker)
                                                    break;
                                            }
                                            if (!breaker) {
                                                r.setText("", 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 4 + i);
                i++;
            }
            table.removeRow(4 + lstCrono.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados en
     *               el
     *               documento Cronograma de Actividades}
     */
    public static void generarCronogramaActividadesPOACAnios(List<CronogramaActividadEntity> lstCrono, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(36);
            XWPFTableRow row1 = table.getRow(4);

            int i = 0;
            for (CronogramaActividadEntity actividad : lstCrono) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("actividadx")) {
                                        text5 = text5.replace("actividadx",
                                                actividad.getActividad() != null ? actividad.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_2")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                                        text5 = text5.replace("M_2",
                                                lstDetalle != null ? (lstDetalle.size() > 0 ? "X" : "") : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_3")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                                        text5 = text5.replace("M_3",
                                                lstDetalle != null ? (lstDetalle.size() > 0 ? "X" : "") : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                                        if (lstDetalle != null) {
                                            boolean breaker = false;
                                            for (CronogramaActividadDetalleEntity detalle : lstDetalle) {
                                                for (int mes = 1; mes <= 12; mes++) {
                                                    if (detalle.getMes() == mes && text5.trim().equals("M" + mes)) {
                                                        text5 = text5.replace("M" + mes, "X");
                                                        r.setText(text5, 0);
                                                        breaker = true;
                                                        break;
                                                    }
                                                }
                                                if (breaker)
                                                    break;
                                            }
                                            if (!breaker) {
                                                r.setText("", 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 4 + i);
                i++;
            }
            table.removeRow(4 + lstCrono.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Programa de Inversion - Necesidades de
     *               Financiamiento}
     */
    public static void generarNecesidadFinanciamientoProgramaInversionPOCC(
            List<RentabilidadManejoForestalDto> lstNecesidades, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(27);
            XWPFTableRow row1 = table.getRow(1);

            int i = 0;
            for (RentabilidadManejoForestalDto necesidad : lstNecesidades) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("122NEC")) {
                                        text5 = text5.replace("122NEC",
                                                necesidad.getDescripcion() != null
                                                        ? necesidad.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstNecesidades.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Programa de Inversion - Totales}
     */
    public static void generarTotalesProgramaInversionPOCC(Double[] totales, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(24);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            XWPFTableRow row3 = table.getRow(3);

            // total ingresos
            XWPFTableRow copiedRowTotalIngr = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            CTTrPr trPrIngr = copiedRowTotalIngr.getCtRow().addNewTrPr();
            CTHeight htIngr = trPrIngr.addNewTrHeight();
            htIngr.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalIngr.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12INI")) {
                                text5 = text5.replace("12INI", totales[0] != null ? totales[0].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalIngr, 1);
            table.removeRow(2);
            // total egresos
            XWPFTableRow copiedRowTotalEgre = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPrEgre = copiedRowTotalEgre.getCtRow().addNewTrPr();
            CTHeight htEgre = trPrEgre.addNewTrHeight();
            htEgre.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalEgre.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12INE")) {
                                text5 = text5.replace("12INE", totales[1] != null ? totales[1].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalEgre, 2);
            table.removeRow(3);
            // total neto
            XWPFTableRow copiedRowTotalNeto = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table);
            CTTrPr trPrNeto = copiedRowTotalNeto.getCtRow().addNewTrPr();
            CTHeight htNeto = trPrNeto.addNewTrHeight();
            htNeto.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalNeto.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12BN")) {
                                Double totalNeto = (totales[0] != null ? totales[0] : 0.00)
                                        - (totales[1] != null ? totales[1] : 0.00);
                                text5 = text5.replace("12BN", totalNeto != null ? totalNeto.toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalNeto, 3);
            table.removeRow(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 22-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Programa de Inversion - Egresos}
     */
    public static void generarEgresoProgramaInversionPOCC(List<RentabilidadManejoForestalDto> lstEgresos,
            XWPFDocument doc, Double[] total) {
        try {
            total[1] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstEgresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(26);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("121EGREMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("121EGRETOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto egreso : lstEgresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("121EGRERUB")) {
                                        text5 = text5.replace("121EGRERUB",
                                                egreso.getRubro() != null ? egreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121EGREDESC")) {
                                        text5 = text5.replace("121EGREDESC",
                                                egreso.getDescripcion() != null ? egreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121EGREMONTOT")) {
                                        text5 = text5.replace("121EGREMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[1] += totalFila;
                                    } else if (text5.contains("121EGREMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : egreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("121EGREMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("121EGREMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("121EGREMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstEgresos.size());

            // totales de egresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("121EGRETOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("121EGRETOTALA" + integer.toString())) {
                                                text5 = text5.replace("121EGRETOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("121EGRETOTALT")) {
                                    text5 = text5.replace("121EGRETOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstEgresos.size());
            table.removeRow(2 + lstEgresos.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Jason Retamozo 21-12-2021
     * @creado:
     * @descripción: {Procesa un archivo word POCC, reemplazando valores marcados en
     *               el
     *               documento Programa de Inversion - Ingresos}
     */
    public static void generarIngresoProgramaInversionPOCC(List<RentabilidadManejoForestalDto> lstIngresos,
            XWPFDocument doc, Double[] total) {
        try {
            total[0] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstIngresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(25);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("121INGRMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("121INGRTOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto ingreso : lstIngresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("121INGRRUB")) {
                                        text5 = text5.replace("121INGRRUB",
                                                ingreso.getRubro() != null ? ingreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("INGR121DESC")) {
                                        text5 = text5.replace("INGR121DESC",
                                                ingreso.getDescripcion() != null ? ingreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121INGRMONTOT")) {
                                        text5 = text5.replace("121INGRMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[0] += totalFila;
                                    } else if (text5.contains("121INGRMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : ingreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("121INGRMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("121INGRMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("121INGRMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstIngresos.size());

            // totales de ingresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("121INGRTOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("121INGRTOTALA" + integer.toString())) {
                                                text5 = text5.replace("121INGRTOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("121INGRTOTALT")) {
                                    text5 = text5.replace("121INGRTOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstIngresos.size());
            table.removeRow(2 + lstIngresos.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 02-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Ordenamiento Interno}
     */
    public static void generarOrdenamientoInternoPGMFA(List<OrdenamientoInternoDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(18);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (OrdenamientoInternoDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("5Categoria")) {
                                        text5 = text5.replace("5Categoria",
                                                detalleEntity.getCategoria() != null
                                                        ? detalleEntity.getCategoria().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5Area")) {
                                        text5 = text5.replace("5Area",
                                                detalleEntity.getAreaHA() != null ? detalleEntity.getAreaHA().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5Porc")) {
                                        text5 = text5.replace("5Porc",
                                                detalleEntity.getAreaHAPorcentaje() != null
                                                        ? detalleEntity.getAreaHAPorcentaje().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5E")) {
                                        text5 = text5.replace("5E", "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento objetivos}
     */

    public static void procesarObjetivosPGMFA(List<ObjetivoDto> lstLista, XWPFDocument doc) {
        try {
            // objetivo general
            XWPFTable table1 = doc.getTables().get(1);
            XWPFTableRow row1 = table1.getRow(0);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table1);
            CTTrPr trPr1 = copiedRow1.getCtRow().addNewTrPr();
            CTHeight ht1 = trPr1.addNewTrHeight();
            ht1.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRow1.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("21OBJETIVOGENE")) {
                                    text5 = text5.replace("21OBJETIVOGENE",
                                            lstLista.get(0).getDescripcion() != null ? lstLista.get(0).getDescripcion()
                                                    : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table1.addRow(copiedRow1, 0);
            table1.removeRow(1);

            // objetivos especificos
            XWPFTable table = doc.getTables().get(2);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            List<ObjetivoDto> maderables = lstLista.stream().filter(m -> m.getDetalle().equals("Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> noMaderables = lstLista.stream().filter(n -> n.getDetalle().equals("No Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> lstOrdenada = new ArrayList<>();
            lstOrdenada.addAll(maderables);
            lstOrdenada.addAll(noMaderables);
            int i = 0;
            for (ObjetivoDto detalleEntity : lstOrdenada) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("1obj")) {
                                        String detalle = "";
                                        if (i == 0)
                                            detalle = "Maderable";
                                        if (i == maderables.size())
                                            detalle = "No Maderable";
                                        text5 = text5.replace("1obj", detalle);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OE")) {
                                        String check;
                                        if (detalleEntity.getActivo().toString().equals("A")) {
                                            check = "(X)";
                                        } else {
                                            check = "( )";
                                        }
                                        text5 = text5.replace("OE", check);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3obj")) {
                                        text5 = text5.replace("3obj", detalleEntity.getDescripcionDetalle() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jaqueline DB - 27-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento objetivos}
     */
    public static XWPFDocument procesarObjetivosPMFI(List<ObjetivoManejoEntity> lstLista, XWPFDocument doc)
            throws IOException {
        try {

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(0);
            CTTrPr trPr = row.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            XWPFTableCell lnewCell = row.getCell(0);
            for (ObjetivoManejoEntity obj : lstLista) {
                XWPFParagraph lnewPara = lnewCell.addParagraph();
                /*
                 * if (lnewCell.getParagraphs().size() > 0) {
                 * lnewPara = lnewCell.getParagraphs().get(0);
                 * } else {
                 * lnewPara = lnewCell.addParagraph();
                 * }
                 */
                XWPFRun lnewRun = lnewPara.createRun();
                String data = obj.getGeneral() == null ? "" : "*" + obj.getGeneral() + ". \n";
                if (data.contains("\n")) {
                    String[] lines = data.split("\n");
                    lnewRun.setText(lines[0], 0);
                    for (int i = 1; i < lines.length; i++) {
                        lnewRun.addBreak();
                        lnewRun.setText(lines[i]);
                    }
                } else {
                    lnewRun.setText(data, 0);
                }
                lnewRun.setFontFamily("Arial");
                lnewRun.setFontSize(9);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento de informacion general}
     */

    public static XWPFDocument procesoInformacionGeneralPGMFADuracion(InformacionGeneralDEMAEntity info,
            XWPFDocument doc) throws IOException {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            XWPFTable table1 = doc.getTables().get(3);
            XWPFTableRow row1 = table1.getRow(0);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table1);
            CTTrPr trPr1 = copiedRow1.getCtRow().addNewTrPr();
            CTHeight ht1 = trPr1.addNewTrHeight();
            ht1.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRow1.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("31FECHAINICIO")) {
                                    text5 = text5.replace("31FECHAINICIO",
                                            info.getFechaInicioDema() != null
                                                    ? formatter.format(info.getFechaInicioDema()).toString()
                                                    : "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("32FECHAFINAL")) {
                                    text5 = text5.replace("32FECHAFINAL",
                                            info.getFechaFinDema() != null
                                                    ? formatter.format(info.getFechaFinDema()).toString()
                                                    : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table1.addRow(copiedRow1, 0);
            table1.removeRow(1);

            // for (XWPFParagraph p : doc.getParagraphs()) {
            // XmlCursor cursor = p.getCTP().newCursor();
            // cursor.selectPath("declare namespace
            // w='http://schemas.openxmlformats.org/wordprocessingml/2006/main'
            // .//*/w:txbxContent/w:p/w:r");
            /*
             * List<XmlObject> ctrsintxtbx = new ArrayList<XmlObject>();
             * 
             * while(cursor.hasNextSelection()) {
             * cursor.toNextSelection();
             * XmlObject obj = cursor.getObject();
             * ctrsintxtbx.add(obj);
             * }
             * for (XmlObject obj : ctrsintxtbx) {
             * CTR ctr = CTR.Factory.parse(obj.xmlText());
             * XWPFRun r = new XWPFRun(ctr, (IRunBody)p);
             * String text = r.getText(0);
             * if (text != null) {
             * if (text.contains("31FECHAINICIO")) {
             * //text = text.replace("diamesaniox", formatter.format(date));
             * text = text.replace("31FECHAINICIO",info.getFechaInicioDema()!=null?info.
             * getFechaInicioDema().toString():"");
             * r.setText(text, 0);
             * }else if (text.contains("32FECHAFINAL")) {
             * text = text.replace("32FECHAFINAL",
             * info.getFechaFinDema()!=null?info.getFechaFinDema().toString():"");
             * r.setText(text, 0);
             * }
             * obj.set(r.getCTR());
             * }
             * }
             * }
             */
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 18-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento de informacion general}
     */

    public static XWPFDocument procesoInformacionGeneralPGMFA(InformacionGeneralDEMAEntity info, XWPFDocument doc)
            throws IOException {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            for (XWPFParagraph p : doc.getParagraphs()) {
                XmlCursor cursor = p.getCTP().newCursor();
                cursor.selectPath(
                        "declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//*/w:txbxContent/w:p/w:r");

                List<XmlObject> ctrsintxtbx = new ArrayList<XmlObject>();

                while (cursor.hasNextSelection()) {
                    cursor.toNextSelection();
                    XmlObject obj = cursor.getObject();
                    ctrsintxtbx.add(obj);
                }
                for (XmlObject obj : ctrsintxtbx) {
                    CTR ctr = (CTR) CTR.Factory.parse(obj.xmlText());
                    XWPFRun r = new XWPFRun(ctr, (IRunBody) p);
                    String text = r.getText(0);
                    if (text != null) {
                        if (text.contains("1comunidad")) {
                            // text = text.replace("diamesaniox", formatter.format(date));
                            text = text.replace("1comunidad",
                                    info.getNombreComunidad() != null ? info.getNombreComunidad().toString() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1nombreJefeComunidad")) {
                            text = text.replace("1nombreJefeComunidad",
                                    info.getNombresJefeComunidad() != null ? info.getNombresJefeComunidad() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1numDni")) {
                            text = text.replace("1numDni",
                                    info.getDniJefecomunidad() != null ? info.getDniJefecomunidad() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1numPartida")) {
                            text = text.replace("1numPartida",
                                    info.getNroResolucionComunidad() != null ? info.getNroResolucionComunidad() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1numRuc")) {
                            text = text.replace("1numRuc",
                                    info.getNroRucComunidad() != null ? info.getNroRucComunidad() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1FederacionOrganiza")) {
                            text = text.replace("1FederacionOrganiza",
                                    info.getFederacionComunidad() != null ? info.getFederacionComunidad() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1Regente")) {
                            text = text.replace("1Regente",
                                    info.getRegente().getNombres() != null ? info.getRegente().getNombres()
                                            : "" + " " + info.getRegente().getApellidos() != null
                                                    ? info.getRegente().getApellidos()
                                                    : "");
                            r.setText(text, 0);
                        } else if (text.contains("1Certificado")) {
                            text = text.replace("1Certificado",
                                    info.getRegente().getCertificadoHabilitacion() != null
                                            ? info.getRegente().getCertificadoHabilitacion()
                                            : "");
                            r.setText(text, 0);
                        } else if (text.contains("1numInscripcion")) {
                            text = text.replace("1numInscripcion", "");
                            r.setText(text, 0);
                        } else if (text.contains("1fechaPresentacion")) {
                            text = text.replace("1fechaPresentacion", formatter.format(info.getFechaElaboracionPmfi()));
                            r.setText(text, 0);
                        } else if (text.contains("1duracionPGMF")) {
                            text = text.replace("1duracionPGMF",
                                    info.getVigencia() != null ? info.getVigencia().toString() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1FechaInicio")) {
                            text = text.replace("1FechaInicio", formatter.format(info.getFechaInicioDema()));
                            r.setText(text, 0);
                        } else if (text.contains("1FechaFinaliza")) {
                            text = text.replace("1FechaFinaliza", formatter.format(info.getFechaFinDema()));
                            r.setText(text, 0);
                        } else if (text.contains("1ObjetivosEspecificos")) {
                            text = text.replace("1ObjetivosEspecificos",
                                    info.getObservacion() != null ? info.getObservacion() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1potencialMaderable")) {
                            text = text.replace("1potencialMaderable",
                                    info.getAreaTotal().toString() != null ? info.getAreaTotal().toString() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1Depar")) {
                            text = text.replace("1Depar", info.getDepartamento() != null ? info.getDepartamento() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1Provin")) {
                            text = text.replace("1Provin", info.getProvincia() != null ? info.getProvincia() : "");
                            r.setText(text, 0);
                        } else if (text.contains("1Distrito")) {
                            text = text.replace("1Distrito", info.getDistrito() != null ? info.getDistrito() : "");
                            r.setText(text, 0);
                        }

                        obj.set(r.getCTR());
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 27-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PMFI, reemplazando valores marcados en
     *               el
     *               documento de informacion general}
     */
    public static XWPFDocument procesoInformacionGeneralPMFI(InformacionGeneralDEMAEntity info, XWPFDocument doc)
            throws IOException {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            for (XWPFParagraph p : doc.getParagraphs()) {
                XmlCursor cursor = p.getCTP().newCursor();
                cursor.selectPath(
                        "declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//*/w:txbxContent/w:p/w:r");

                List<XmlObject> ctrsintxtbx = new ArrayList<XmlObject>();

                while (cursor.hasNextSelection()) {
                    cursor.toNextSelection();
                    XmlObject obj = cursor.getObject();
                    ctrsintxtbx.add(obj);
                }
                for (XmlObject obj : ctrsintxtbx) {
                    CTR ctr = (CTR) CTR.Factory.parse(obj.xmlText());
                    XWPFRun r = new XWPFRun(ctr, (IRunBody) p);
                    String text = r.getText(0);
                    if (text != null) {
                        if (text.contains("contratox")) {
                            // text = text.replace("diamesaniox", formatter.format(date));
                            text = text.replace("contratox",
                                    info.getIdContrato() != null ? info.getIdContrato().toString() : "");
                            r.setText(text, 0);
                        } else if (text.contains("nombretitularx")) {
                            text = text.replace("nombretitularx",
                                    info.getNombreElaboraDema() != null ? info.getNombreElaboraDema() : "");
                            r.setText(text, 0);
                        } else if (text.contains("representantex")) {
                            text = text.replace("representantex",
                                    info.getRepresentanteLegal() != null ? info.getRepresentanteLegal() : "");
                            r.setText(text, 0);
                        } else if (text.contains("numerodocumentox")) {
                            text = text.replace("numerodocumentox",
                                    info.getDniElaboraDema() != null ? info.getDniElaboraDema() : "");
                            r.setText(text, 0);
                        } else if (text.contains("domiciliotitularx")) {
                            String ubigeo = info.getDepartamento() != null ? info.getDepartamento()
                                    : ""
                                            + " - " + info.getProvincia() != null ? info.getProvincia()
                                                    : ""
                                                            + " - " + info.getDistrito() != null ? info.getDistrito()
                                                                    : "";
                            text = text.replace("domiciliotitularx", ubigeo + " - " + info.getDireccionLegalTitular());
                            // Departmento-provincia-distrito
                            r.setText(text, 0);
                        } else if (text.contains("domiciliolegalx")) {
                            // Departmento-provincia-distrito
                            String ubigeo = info.getDepartamentoRepresentante() != null
                                    ? info.getDepartamentoRepresentante()
                                    : ""
                                            + "-" + info.getProvinciaRepresentante() != null
                                                    ? info.getProvinciaRepresentante()
                                                    : ""
                                                            + "-" + info.getDistritoRepresentante() != null
                                                                    ? info.getDistritoRepresentante()
                                                                    : "";
                            text = text.replace("domiciliolegalx",
                                    ubigeo + "-" + info.getDireccionLegalRepresentante());
                            r.setText(text, 0);
                        } else if (text.contains("telefonox")) {
                            text = text.replace("telefonox",
                                    info.getCelularTitular() != null ? info.getCelularTitular() : "");
                            r.setText(text, 0);
                        } else if (text.contains("correox")) {
                            text = text.replace("correox",
                                    info.getCorreoTitular() != null ? info.getCorreoTitular() : "");
                            r.setText(text, 0);
                        } else if (text.contains("regentex")) {
                            text = text.replace("regentex",
                                    info.getRegente().getNombres() != null ? info.getRegente().getNombres()
                                            : "" + " " + info.getRegente().getApellidos() != null
                                                    ? info.getRegente().getApellidos()
                                                    : "");
                            r.setText(text, 0);
                        } else if (text.contains("licenciax")) {
                            text = text.replace("licenciax",
                                    info.getRegente().getNumeroLicencia() != null
                                            ? info.getRegente().getNumeroLicencia()
                                            : "");
                            r.setText(text, 0);
                        } else if (text.contains("fechaelaboracionx")) {
                            text = text.replace("fechaelaboracionx", formatter.format(info.getFechaElaboracionPmfi()));
                            r.setText(text, 0);
                        } else if (text.contains("vigenciax")) {
                            text = text.replace("vigenciax",
                                    info.getVigencia() != null ? info.getVigencia().toString() : "");
                            r.setText(text, 0);
                        } else if (text.contains("fechainiciox")) {
                            text = text.replace("fechainiciox", formatter.format(info.getFechaInicioDema()));
                            r.setText(text, 0);
                        } else if (text.contains("fechafinx")) {
                            text = text.replace("fechafinx", formatter.format(info.getFechaFinDema()));
                            r.setText(text, 0);
                        }
                        obj.set(r.getCTR());
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 20-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento de la grilla del anexo 3}
     */
    public static XWPFDocument generarAnexo3(List<Anexo3CensoEntity> lstLista, XWPFDocument doc) throws IOException {
        try {
            if(lstLista!=null){
                XWPFTable table = doc.getTables().get(13);
                XWPFTableRow row = table.getRow(2);
                for (int i = 0; i < lstLista.size(); i++) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null) {
                                        if (text.contains("CODIGOX")) {
                                            text = text.replace("CODIGOX", lstLista.get(i).getIdIndividuo());
                                            r.setText(text, 0);
                                        } else if (text.contains("NOMBREESX")) {
                                            text = text.replace("NOMBREESX", lstLista.get(i).getNombreComunDesc());
                                            r.setText(text, 0);
                                        } else if (text.contains("NOMBRECX")) {
                                            text = text.replace("NOMBRECX", lstLista.get(i).getNombreCientificoDesc());
                                            r.setText(text, 0);
                                        } else if (text.contains("NOMBRENX")) {
                                            text = text.replace("NOMBRENX", lstLista.get(i).getNombreNativo());
                                            r.setText(text, 0);
                                        } else if (text.contains("EX")) {
                                            text = text.replace("EX", lstLista.get(i).getEste());
                                            r.setText(text, 0);
                                        } else if (text.contains("NX")) {
                                            text = text.replace("NX", lstLista.get(i).getNorte());
                                            r.setText(text, 0);
                                        } else if (text.contains("PRODUCTOX")) {
                                            text = text.replace("PRODUCTOX", lstLista.get(i).getProducto());
                                            r.setText(text, 0);
                                        } else if (text.contains("VOLX")) {
                                            text = text.replace("VOLX", toString(lstLista.get(i).getVolumenComercial()));
                                            r.setText(text, 0);
                                        } else if (text.contains("OBSX")) {
                                            text = text.replace("OBSX", lstLista.get(i).getObservaciones());
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow);
                }
                table.removeRow(2);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Jaqueline DB - 16-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento de la grilla del anexo 2}
     */
    public static XWPFDocument generarAnexo2(List<Anexo2CensoEntity> lstLista, XWPFDocument doc) throws IOException {
        try {

            XWPFTable table = doc.getTables().get(12);
            XWPFTableRow row2 = table.getRow(1);
            XWPFTableRow total = new XWPFTableRow((CTRow) table.getRow(2).getCtRow().copy(), table);
            table.removeRow(2);
            Double totalVolumen = 0.0;
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        p.setAlignment(ParagraphAlignment.CENTER);
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("codigox")) {
                                        text5 = text5.replace("codigox", lstLista.get(i).getCodArbol());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("especiex")) {
                                        text5 = text5.replace("especiex", lstLista.get(i).getNombreComunDesc());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("diametrox")) {
                                        text5 = text5.replace("diametrox", toString(lstLista.get(i).getDiametro()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("alturax")) {
                                        text5 = text5.replace("alturax", toString(lstLista.get(i).getAltura()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("volumenx")) {
                                        text5 = text5.replace("volumenx", toString(lstLista.get(i).getVolumen()));
                                        r.setText(text5, 0);
                                        totalVolumen += lstLista.get(i).getVolumen();
                                    } else if (text5.contains("observacionx")) {
                                        text5 = text5.replace("observacionx", "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            for (XWPFTableCell cell : total.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("totalx")) {
                                    text5 = text5.replace("totalx", toString(totalVolumen));
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(total);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }

    /**
     * @autor: Jaqueline DB - 16-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento de la grilla de los impactos ambientales}
     */
    public static XWPFDocument procesarImpactosAmbientales(List<ImpactoAmbientalDto> objList, XWPFDocument doc)
            throws IOException {
        try {

            XWPFTable table = doc.getTables().get(10);
            XWPFTableRow row1 = table.getRow(1);
            setearDataActividadesEquipos(objList, row1, table, doc);
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void setearDataActividadesEquipos(List<ImpactoAmbientalDto> lstResult, XWPFTableRow row1,
            XWPFTable table, XWPFDocument doc) {

        for (int i = 0; i < lstResult.size(); i++) {
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            for (XWPFTableCell cell : copiedRow2.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("actividadx")) {
                                    text5 = text5.replace("actividadx", lstResult.get(i).getActividad() == null ? ""
                                            : lstResult.get(i).getActividad());
                                    r.setText(text5, 0);
                                } else if (text5.contains("descripcionx")) {
                                    text5 = text5.replace("descripcionx", lstResult.get(i).getDescripcion() == null ? ""
                                            : lstResult.get(i).getDescripcion());
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }

                }
            }
            CTAbstractNum cTAbstractNum = (CTAbstractNum) CTAbstractNum.Factory.newInstance();
            cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));
            CTLvl cTLvl = cTAbstractNum.addNewLvl();
            cTLvl.setIlvl(BigInteger.valueOf(0));
            cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
            cTLvl.addNewLvlText().setVal("\u2713");
            XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);
            XWPFNumbering numbering = doc.createNumbering();
            BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);
            BigInteger numID = numbering.addNum(abstractNumID);
            List<ImpactoAmbientalDetalleEntity> lstDetallito = lstResult.get(i).getDetalle();
            CTTrPr trPr = copiedRow2.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (ImpactoAmbientalDetalleEntity obj : lstDetallito) {
                XWPFTableCell lnewCell = copiedRow2.getCell(2);
                XWPFParagraph lnewPara = lnewCell.addParagraph();
                lnewPara.setNumID(numID);
                XWPFRun lnewRun = lnewPara.createRun();
                lnewRun.setFontFamily("Arial");
                lnewRun.setFontSize(9);
                lnewRun.setText(obj.getMedidasprevencion() == null ? "" : obj.getMedidasprevencion());
            }
            table.addRow(copiedRow2);
        }
    }

    /**
     * @autor: Jaqueline DB - 16-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento de la grilla actividades y equipos}
     */
    public static XWPFDocument procesarActividadesEquipos(List<SistemaManejoForestalDetalleEntity> objList,
            XWPFDocument doc) throws IOException {
        try {
            // Primero se tiene que ordenar la data
            List<SistemaManejoForestalDetalleEntity> lstAproveMadera = new ArrayList<>();
            objList.stream().forEach((a) -> {
                if (a.getCodigoTipoDetalle().equals("AAEAM")) {
                    lstAproveMadera.add(a);
                }
            });
            List<SistemaManejoForestalDetalleEntity> lstAproveNoMadera = new ArrayList<>();
            objList.stream().forEach((a) -> {
                if (a.getCodigoTipoDetalle().equals("AAEANM")) {
                    lstAproveNoMadera.add(a);
                }
            });
            List<SistemaManejoForestalDetalleEntity> lstAproveOtros = new ArrayList<>();
            objList.stream().forEach((a) -> {
                if (a.getCodigoTipoDetalle().equals("AAEOTRO")) {
                    lstAproveOtros.add(a);
                }
            });

            XWPFTable table = doc.getTables().get(9);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            setearDataActividadesEquipos(lstAproveMadera, row1, row2, table, "Aprovechamiento Maderable");
            setearDataActividadesEquipos(lstAproveNoMadera, row1, row2, table, "Aprovechamiento No Maderable");
            setearDataActividadesEquipos(lstAproveOtros, row1, row2, table, "Otros (especificar)");
            table.removeRow(1);
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void setearDataActividadesEquipos(List<SistemaManejoForestalDetalleEntity> lstResult,
            XWPFTableRow row1, XWPFTableRow row2, XWPFTable table, String actividad) {
        XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
        for (XWPFTableCell cell : copiedRow1.getTableCells()) {
            for (XWPFParagraph p : cell.getParagraphs()) {
                for (XWPFRun r : p.getRuns()) {
                    if (r != null) {
                        String text5 = r.getText(0);
                        if (text5 != null) {
                            if (text5.contains("actividadx")) {
                                text5 = text5.replace("actividadx", actividad);
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
        }
        table.addRow(copiedRow1);

        for (int i = 0; i < lstResult.size(); i++) {
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            for (XWPFTableCell cell : copiedRow2.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("detallex")) {
                                    text5 = text5.replace("detallex", lstResult.get(i).getActividades() == null ? ""
                                            : lstResult.get(i).getActividades());
                                    r.setText(text5, 0);
                                } else if (text5.contains("descripcionx")) {
                                    text5 = text5.replace("descripcionx",
                                            lstResult.get(i).getDescripcionSistema() == null ? ""
                                                    : lstResult.get(i).getDescripcionSistema());
                                    r.setText(text5, 0);
                                } else if (text5.contains("equiposx")) {
                                    text5 = text5.replace("equiposx",
                                            lstResult.get(i).getMaquinariasInsumos() == null ? ""
                                                    : lstResult.get(i).getMaquinariasInsumos());
                                    r.setText(text5, 0);
                                } else if (text5.contains("personalx")) {
                                    text5 = text5.replace("personalx",
                                            lstResult.get(i).getPersonalRequerido() == null ? ""
                                                    : lstResult.get(i).getPersonalRequerido());
                                    r.setText(text5, 0);
                                } else if (text5.contains("observacionx")) {
                                    text5 = text5.replace("observacionx", lstResult.get(i).getObservacion() == null ? ""
                                            : lstResult.get(i).getObservacion());
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }

                }
            }

            table.addRow(copiedRow2);
        }
    }

    /**
     * @autor: Jaqueline DB - 16-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento de la grilla cronograma}
     */
    public static XWPFDocument procesarCronograma(List<CronogramaActividadEntity> objList, XWPFDocument doc)
            throws IOException {
        try {
            // Primero se tiene que ordenar la data
            List<CronogramaActividadEntity> lstAproveMadera = new ArrayList<>();
            objList.stream().forEach((a) -> {
                if (a.getCodigoActividad().equals("AAEAM")) {
                    lstAproveMadera.add(a);
                }
            });
            List<CronogramaActividadEntity> lstAproveNoMadera = new ArrayList<>();
            objList.stream().forEach((a) -> {
                if (a.getCodigoActividad().equals("AAEANM")) {
                    lstAproveNoMadera.add(a);
                }
            });
            List<CronogramaActividadEntity> lstAproveOtros = new ArrayList<>();
            objList.stream().forEach((a) -> {
                if (a.getCodigoActividad().equals("AAEOTRO")) {
                    lstAproveOtros.add(a);
                }
            });

            XWPFTable table = doc.getTables().get(11);
            XWPFTableRow row3 = table.getRow(3);
            XWPFTableRow row4 = table.getRow(4);
            setearDataCronograma(lstAproveMadera, row3, row4, table, "Aprovechamiento Maderable");
            setearDataCronograma(lstAproveNoMadera, row3, row4, table, "Aprovechamiento No Maderable");
            setearDataCronograma(lstAproveOtros, row3, row4, table, "Otros (especificar)");
            table.removeRow(3);
            table.removeRow(3);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static void setearDataCronograma(List<CronogramaActividadEntity> lstResult, XWPFTableRow row3,
            XWPFTableRow row4, XWPFTable table, String actividad) {
        XWPFTableRow copiedRow3 = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table);
        for (XWPFTableCell cell : copiedRow3.getTableCells()) {
            for (XWPFParagraph p : cell.getParagraphs()) {
                for (XWPFRun r : p.getRuns()) {
                    if (r != null) {
                        String text5 = r.getText(0);
                        if (text5 != null) {
                            if (text5.contains("actividadx")) {
                                text5 = text5.replace("actividadx", actividad);
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
        }
        table.addRow(copiedRow3);

        for (int i = 0; i < lstResult.size(); i++) {
            XWPFTableRow copiedRow4 = new XWPFTableRow((CTRow) row4.getCtRow().copy(), table);
            for (CronogramaActividadDetalleEntity obj : lstResult.get(i).getDetalle()) {
                for (XWPFTableCell cell : copiedRow4.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("detallex")) {
                                        text5 = text5.replace("detallex", lstResult.get(i).getActividad());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M1")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(1))) {
                                        text5 = text5.replace("M1", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M2")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(2))) {
                                        text5 = text5.replace("M2", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M3")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(3))) {
                                        text5 = text5.replace("M3", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M4")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(4))) {
                                        text5 = text5.replace("M4", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M5")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(5))) {
                                        text5 = text5.replace("M5", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M6")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(6))) {
                                        text5 = text5.replace("M6", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M7")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(7))) {
                                        text5 = text5.replace("M7", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M8")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(8))) {
                                        text5 = text5.replace("M8", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M9")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(9))) {
                                        text5 = text5.replace("M9", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_0")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(10))) {
                                        text5 = text5.replace("M_0", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_1")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(11))) {
                                        text5 = text5.replace("M_1", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_2")
                                            && (obj.getAnio().equals(1) && obj.getMes().equals(12))) {
                                        text5 = text5.replace("M_2", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("A2") && (obj.getAnio().equals(2))) {
                                        text5 = text5.replace("A2", "X");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("A3") && (obj.getAnio().equals(3))) {
                                        text5 = text5.replace("A3", "X");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            for (XWPFTableCell cell : copiedRow4.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("M1")) {
                                    text5 = text5.replace("M1", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M2")) {
                                    text5 = text5.replace("M2", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M3")) {
                                    text5 = text5.replace("M3", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M4")) {
                                    text5 = text5.replace("M4", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M5")) {
                                    text5 = text5.replace("M5", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M6")) {
                                    text5 = text5.replace("M6", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M7")) {
                                    text5 = text5.replace("M7", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M8")) {
                                    text5 = text5.replace("M8", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M9")) {
                                    text5 = text5.replace("M9", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M_0")) {
                                    text5 = text5.replace("M_0", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M_1")) {
                                    text5 = text5.replace("M_1", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("M_2")) {
                                    text5 = text5.replace("M_2", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("A2")) {
                                    text5 = text5.replace("A2", "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("A3")) {
                                    text5 = text5.replace("A3", "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            if (lstResult.get(i).getDetalle().size() > 0) {
                table.addRow(copiedRow4);
            }
        }
    }

    public static XWPFDocument generarOrdenamientoInterno(OrdenamientoProteccionEntity lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(12);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getListOrdenamientoProteccionDet().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("4DENOMINACION")) {
                                        text = text.replace("4DENOMINACION",
                                                lstLista.getListOrdenamientoProteccionDet().get(i).getCategoria());
                                        r.setText(text, 0);
                                    } else if (text.contains("4SUPERFICIE")) {
                                        text = text.replace("4SUPERFICIE",
                                                lstLista.getListOrdenamientoProteccionDet().get(i).getAreaHA() + "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void generarIdentiEspecieNoMaderable(List<RecursoForestalDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(13);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            int j = 1;
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("nn")) {
                                        text5 = text5.replace("nn", j + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51ANOMCIENTIFICO")) {
                                        text5 = text5.replace("51ANOMCIENTIFICO", detalleEntity.getNombreComun() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51ANOMCOMUN")) {
                                        text5 = text5.replace("51ANOMCOMUN", detalleEntity.getNombreCientifico() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51APROD")) {
                                        text5 = text5.replace("51APROD", detalleEntity.getProducto() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
                j++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarIdentiEspecieMaderable(List<RecursoForestalDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(14);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            int j = 1;
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("n")) {
                                        text5 = text5.replace("n", j + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51BNOMCIEN")) {
                                        text5 = text5.replace("51BNOMCIEN", detalleEntity.getNombreComun() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51BNOMCO")) {
                                        text5 = text5.replace("51BNOMCO", detalleEntity.getNombreCientifico() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("51PRO")) {
                                        text5 = text5.replace("51PRO", detalleEntity.getProducto() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
                j++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarRealizacionNO(Integer numeroTabla, List<RecursoForestalDetalleDto> lstLista,
            XWPFDocument doc) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            XWPFTable table = doc.getTables().get(numeroTabla);
            System.out.println(lstLista.size());
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("521FECHANO")
                                                && detalleEntity.getFechaRealizacionInventario() != null) {
                                            text5 = text5
                                                    .replace("521FECHANO",
                                                            detalleEntity.getFechaRealizacionInventario() != null
                                                                    ? formatter
                                                                            .format(detalleEntity
                                                                                    .getFechaRealizacionInventario())
                                                                            + ""
                                                                    : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("521DESCFECHANO")
                                                && detalleEntity.getDescripcionFechaRealizacion() != null) {
                                            text5 = text5.replace("521DESCFECHANO",
                                                    detalleEntity.getDescripcionFechaRealizacion() != null
                                                            ? detalleEntity.getDescripcionFechaRealizacion() + ""
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarRealizacion(Integer numeroTabla, List<RecursoForestalDetalleDto> lstLista,
            XWPFDocument doc) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            XWPFTable table = doc.getTables().get(numeroTabla);
            System.out.println(lstLista.size());
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("521FECHA")
                                                && detalleEntity.getFechaRealizacionInventario() != null) {
                                            text5 = text5
                                                    .replace("521FECHA",
                                                            detalleEntity.getFechaRealizacionInventario() != null
                                                                    ? formatter
                                                                            .format(detalleEntity
                                                                                    .getFechaRealizacionInventario())
                                                                            + ""
                                                                    : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("521DESCFECHA")
                                                && detalleEntity.getDescripcionFechaRealizacion() != null) {
                                            text5 = text5.replace("521DESCFECHA",
                                                    detalleEntity.getDescripcionFechaRealizacion() != null
                                                            ? detalleEntity.getDescripcionFechaRealizacion() + ""
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInventarioA(Integer numeroTabla, List<RecursoForestalDetalleDto> lstLista,
            XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(numeroTabla);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(3);
            int i = 0;
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("N521A")) {
                                        text5 = text5.replace("N521A", detalleEntity.getNumParcela() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521APR")) {
                                        text5 = text5.replace("521APR",
                                                detalleEntity.getNumeroArbolesAprovechables() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521SEM")) {
                                        text5 = text5.replace("521SEM",
                                                detalleEntity.getNumeroArbolesSemilleros() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521PROD")) {
                                        text5 = text5.replace("521PROD", detalleEntity.getProducto() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521UM")) {
                                        text5 = text5.replace("521UM", detalleEntity.getUniMedida() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521TO")) {
                                        text5 = text5.replace("521TO", detalleEntity.getNumeroArbolesTotal() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521AP")) {
                                        text5 = text5.replace("521AP", detalleEntity.getVolumenComercial() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 3 + i);
                i++;
            }
            table.removeRow(3 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInventarioANO(Integer numeroTabla, List<RecursoForestalDetalleDto> lstLista,
            XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(numeroTabla);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(3);
            int i = 0;
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("N521A")) {
                                        text5 = text5.replace("N521A", detalleEntity.getNumParcela() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521APR")) {
                                        text5 = text5.replace("521APR",
                                                detalleEntity.getNumeroArbolesAprovechables() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521SEM")) {
                                        text5 = text5.replace("521SEM",
                                                detalleEntity.getNumeroArbolesSemilleros() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521PROD")) {
                                        text5 = text5.replace("521PROD", detalleEntity.getProducto() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521UM")) {
                                        text5 = text5.replace("521UM", detalleEntity.getUniMedida() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521TO")) {
                                        text5 = text5.replace("521TO", detalleEntity.getNumeroArbolesTotal() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521AP")) {
                                        text5 = text5.replace("521AP", detalleEntity.getVolumenComercial() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 3 + i);
                i++;
            }
            table.removeRow(3 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInventarioB(Integer numeroTabla, List<RecursoForestalDetalleDto> lstLista,
            XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(numeroTabla);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("521ES")) {
                                        text5 = text5.replace("521ES", detalleEntity.getNombreComun() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521XES")) {
                                        text5 = text5.replace("521XES", detalleEntity.getNumParcela() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("TO")) {
                                        text5 = text5.replace("TO", detalleEntity.getNumeroArbolesTotal() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("APRO")) {
                                        text5 = text5.replace("APRO",
                                                detalleEntity.getNumeroArbolesAprovechables() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("SEM")) {
                                        text5 = text5.replace("SEM", detalleEntity.getNumeroArbolesSemilleros() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521BUM")) {
                                        text5 = text5.replace("521BUM", detalleEntity.getUniMedida());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521CAN")) {
                                        text5 = text5.replace("521CAN", detalleEntity.getVolumenComercial() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInventarioBNO(Integer numeroTabla, List<RecursoForestalDetalleDto> lstLista,
            XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(numeroTabla);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (RecursoForestalDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("521ES")) {
                                        text5 = text5.replace("521ES", detalleEntity.getNombreComun() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521SXT")) {
                                        text5 = text5.replace("521SXT", detalleEntity.getNumParcela() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("TO")) {
                                        text5 = text5.replace("TO", detalleEntity.getNumeroArbolesTotal() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("APRO")) {
                                        text5 = text5.replace("APRO",
                                                detalleEntity.getNumeroArbolesAprovechables() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("SEM")) {
                                        text5 = text5.replace("SEM", detalleEntity.getNumeroArbolesSemilleros() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521BUM")) {
                                        text5 = text5.replace("521BUM", detalleEntity.getUniMedida());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("521CAN")) {
                                        text5 = text5.replace("521CAN", detalleEntity.getVolumenComercial() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreaUbiPolitica(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("311departamento")) {
                                            text5 = text5.replace("311departamento",
                                                    detalleEntity.getDepartamento() + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("311provincia")) {
                                            text5 = text5.replace("311provincia", detalleEntity.getProvincia() + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("311distrito")) {
                                            text5 = text5.replace("311distrito", detalleEntity.getDistrito() + "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("311sector")) {
                                            text5 = text5.replace("311sector", detalleEntity.getCuenca() + "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 20-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaUbiPoliticaPGMFA}
     */
    public static void generarInfoAreaUbiPoliticaPGMFA(List<InfBasicaAereaDetalleDto> lstLista,
            XWPFDocument doc, String departamento, String provincia, String distrito) {
        try {
            XWPFTable table = doc.getTables().get(5);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {

                                        if (text5.contains("42DEPARTAMENTO")) {
                                            text5 = text5.replace("42DEPARTAMENTO",
                                                    departamento != null
                                                            ? departamento
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("42PROVINCIA")) {
                                            text5 = text5.replace("42PROVINCIA",
                                                    provincia != null ? provincia
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("42DISTRI")) {
                                            text5 = text5.replace("42DISTRI",
                                                    distrito != null
                                                            ? distrito
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("42CUENCA")) {
                                            text5 = text5.replace("42CUENCA",
                                                    detalleEntity.getCuenca() != null
                                                            ? detalleEntity.getCuenca()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /*
     * public static void
     * generarInfoAreaUbiPoliticaPGMFA(List<InfBasicaAereaDetalleDto> lstLista,
     * XWPFDocument doc,String departamento,String provincia,String distrito) {
     * try {
     * XWPFTable table = doc.getTables().get(5);
     * System.out.println(lstLista.size());
     * XWPFTableRow row2 = table.getRow(1);
     * 
     * int i = 0;
     * for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
     * 
     * 
     * XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(),
     * table);
     * CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
     * CTHeight ht = trPr.addNewTrHeight();
     * ht.setVal(BigInteger.valueOf(240));
     * for (XWPFTableCell cell : copiedRow.getTableCells()) {
     * for (XWPFParagraph p : cell.getParagraphs()) {
     * for (XWPFRun r : p.getRuns()) {
     * if (r != null) {
     * String text5 = r.getText(0);
     * if (text5 != null) {
     * 
     * 
     * if (text5.contains("42DEPARTAMENTO")) {
     * text5 = text5.replace("42DEPARTAMENTO",
     * departamento != null
     * ? departamento
     * : "");
     * r.setText(text5, 0);
     * } else if (text5.contains("42PROVINCIA")) {
     * text5 = text5.replace("42PROVINCIA",
     * provincia != null ? provincia
     * : "");
     * r.setText(text5, 0);
     * } else if (text5.contains("42DISTRI")) {
     * text5 = text5.replace("42DISTRI",
     * distrito != null
     * ? distrito
     * : "");
     * r.setText(text5, 0);
     * } else if (text5.contains("42CUENCA")) {
     * text5 = text5.replace("42CUENCA",
     * detalleEntity.getCuenca() != null
     * ? detalleEntity.getCuenca().toString()
     * : "");
     * r.setText(text5, 0);
     * }
     * }
     * }
     * }
     * }
     * }
     * table.addRow(copiedRow, 1 + i);
     * i++;
     * }
     * table.removeRow(1 );
     * 
     * } catch (Exception e) {
     * e.printStackTrace();
     * }
     * 
     * }
     */

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaUbiPoliticaPGMFA}
     */
    public static void generarInfoAreaCoordenadasUTMNativaPGMFA(List<InfBasicaAereaDetalleDto> lstLista,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(6);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("43P")) {
                                        text5 = text5.replace("43P",
                                                detalleEntity.getPuntoVertice() != null
                                                        ? detalleEntity.getPuntoVertice()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("43ESTE")) {
                                        text5 = text5.replace("43ESTE",
                                                detalleEntity.getCoordenadaEste() != null
                                                        ? detalleEntity.getCoordenadaEste().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("43NORTE")) {
                                        text5 = text5.replace("43NORTE",
                                                detalleEntity.getCoordenadaNorte() != null
                                                        ? detalleEntity.getCoordenadaNorte().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("43REFERENCIA")) {
                                        text5 = text5.replace("43REFERENCIA",
                                                detalleEntity.getReferencia() != null
                                                        ? detalleEntity.getReferencia().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaCoordenadasUTMMFPGMFA}
     */
    public static void generarInfoAreaCoordenadasUTMMFPGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(7);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("432ANEXO")) {
                                        text5 = text5.replace("432ANEXO", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("432P")) {
                                        text5 = text5.replace("432P",
                                                detalleEntity.getPuntoVertice() != null
                                                        ? detalleEntity.getPuntoVertice()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("432ESTE")) {
                                        text5 = text5.replace("432ESTE",
                                                detalleEntity.getCoordenadaEste() != null
                                                        ? detalleEntity.getCoordenadaEste().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("432NORTE")) {
                                        text5 = text5.replace("432NORTE",
                                                detalleEntity.getCoordenadaNorte() != null
                                                        ? detalleEntity.getCoordenadaNorte().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("432REFERENCIA")) {
                                        text5 = text5.replace("432REFERENCIA",
                                                detalleEntity.getReferencia() != null
                                                        ? detalleEntity.getReferencia().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 20-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaCoordenadasUTMBQPGMFA}
     */
    public static void generarInfoAreaCoordenadasUTMBQPGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(8);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("4313BQS")) {
                                            text5 = text5.replace("4313BQS", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4313AREA")) {
                                            text5 = text5.replace("4313AREA",
                                                    detalleEntity.getAreaHa() != null
                                                            ? detalleEntity.getAreaHa().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4313P")) {
                                            text5 = text5.replace("4313P",
                                                    detalleEntity.getPuntoVertice() != null
                                                            ? detalleEntity.getPuntoVertice()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4313ESTE")) {
                                            text5 = text5.replace("4313ESTE",
                                                    detalleEntity.getCoordenadaEste() != null
                                                            ? detalleEntity.getCoordenadaEste().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4313NORTE")) {
                                            text5 = text5.replace("4313NORTE",
                                                    detalleEntity.getCoordenadaNorte() != null
                                                            ? detalleEntity.getCoordenadaNorte().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4313OBSERVACION")) {
                                            text5 = text5.replace("4313OBSERVACION",
                                                    detalleEntity.getReferencia() != null
                                                            ? detalleEntity.getReferencia().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 20-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaCoordenadasUTMBQPCPGMFA}
     */
    public static void generarInfoAreaCoordenadasUTMBQPCPGMFA(List<InfBasicaAereaDetalleDto> lstLista,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(9);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("4314PC")) {
                                            text5 = text5.replace("4314PC", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4314AR")) {
                                            text5 = text5.replace("4314AR",
                                                    detalleEntity.getAreaHa() != null
                                                            ? detalleEntity.getAreaHa().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4314P")) {
                                            text5 = text5.replace("4314P",
                                                    detalleEntity.getPuntoVertice() != null
                                                            ? detalleEntity.getPuntoVertice()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4314ESTE")) {
                                            text5 = text5.replace("4314ESTE",
                                                    detalleEntity.getCoordenadaEste() != null
                                                            ? detalleEntity.getCoordenadaEste().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4314NORTE")) {
                                            text5 = text5.replace("4314NORTE",
                                                    detalleEntity.getCoordenadaNorte() != null
                                                            ? detalleEntity.getCoordenadaNorte().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("4314OBS")) {
                                            text5 = text5.replace("4314OBS",
                                                    detalleEntity.getReferencia() != null
                                                            ? detalleEntity.getReferencia().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaAccesibilidad1PGMFA}
     */
    public static void generarInfoAreaAccesibilidad1PGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(8);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("44ASEC")) {
                                        text5 = text5.replace("44ASEC",
                                                detalleEntity.getDescripcion() != null
                                                        ? detalleEntity.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("44sp")) {
                                        text5 = text5.replace("44sp",
                                                detalleEntity.getPuntoVertice() != null
                                                        ? detalleEntity.getPuntoVertice().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("44PR")) {
                                        text5 = text5.replace("44PR",
                                                detalleEntity.getReferencia() != null
                                                        ? detalleEntity.getReferencia().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("44V")) {
                                        text5 = text5.replace("44V",
                                                detalleEntity.getMedioTransporte() != null
                                                        ? detalleEntity.getMedioTransporte().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("44DIS")) {
                                        text5 = text5.replace("44DIS",
                                                detalleEntity.getDistanciaKm() != null
                                                        ? detalleEntity.getDistanciaKm().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("44TF")) {
                                        text5 = text5.replace("44TF",
                                                detalleEntity.getTiempo() != null ? detalleEntity.getTiempo().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("44TVF")) {
                                        text5 = text5.replace("44TVF",
                                                detalleEntity.getZonaVida() != null
                                                        ? detalleEntity.getZonaVida().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaAccesibilidad1PGMFA}
     */
    public static void generarInfoAreaAccesibilidad2PGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(9);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(0);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("44bConstruccion")) {
                                        text5 = text5.replace("44bConstruccion",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, i);
                i++;
            }
            table.removeRow(lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaFisiografia}
     */
    public static void generarInfoAreaFisiografia(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(11);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("45BUF")) {
                                        text5 = text5.replace("45BUF",
                                                detalleEntity.getReferencia() != null ? detalleEntity.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("45BEPA")) {
                                        text5 = text5.replace("45BEPA",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("45BAAHA")) {
                                        text5 = text5.replace("45BAAHA",
                                                detalleEntity.getAreaHa() != null ? detalleEntity.getAreaHa().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaHidrografia}
     */
    public static void generarInfoAreaHidrografia(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(10);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(0);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("45quebrada")) {
                                        text5 = text5.replace("45quebrada",
                                                detalleEntity.getNombreQuebrada() != null
                                                        ? detalleEntity.getNombreQuebrada()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("45laguna")) {
                                        text5 = text5.replace("45laguna",
                                                detalleEntity.getNombreLaguna() != null
                                                        ? detalleEntity.getNombreLaguna()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("45rio")) {
                                        text5 = text5.replace("45rio",
                                                detalleEntity.getNombreRio() != null ? detalleEntity.getNombreRio()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, i);
                i++;
            }
            table.removeRow(lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 30-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreFAAUPGMFA}
     */
    public static void generarInfoAreFAAUPGMFA(List<EspecieFaunaEntity> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(12);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (EspecieFaunaEntity fauna : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("461NombreComun")) {
                                        text5 = text5.replace("461NombreComun",
                                                fauna.getNombreComun() != null ? fauna.getNombreComun() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("461NombreCientifico")) {
                                        text5 = text5.replace("461NombreCientifico",
                                                fauna.getNombreCientifico() != null ? fauna.getNombreCientifico() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("461Stat")) {
                                        text5 = text5.replace("461Stat",
                                                fauna.getCategoria() != null ? fauna.getCategoria() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreFAAUPGMFA}
     */
    public static void generarInfoAreaFaunaPGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(12);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto fauna : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("461NombreComun")) {
                                        text5 = text5.replace("461NombreComun",
                                                fauna.getNombreComun() != null ? fauna.getNombreComun() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("461NombreCientifico")) {
                                        text5 = text5.replace("461NombreCientifico",
                                                fauna.getNombreCientifico() != null ? fauna.getNombreCientifico() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("461Stat")) {
                                        text5 = text5.replace("461Stat",
                                                fauna.getFamilia() != null ? fauna.getFamilia() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 21-01-2022
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento Tipos de Bosque}
     */
    public static void generarInfoAreaTipoBosquePGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(13);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto fauna : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("462TB")) {
                                        text5 = text5.replace("462TB",
                                                fauna.getNombre() != null ? fauna.getNombre() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("462Su")) {
                                        text5 = text5.replace("462Su",
                                                fauna.getObservaciones() != null ? fauna.getObservaciones() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("462Descripcion")) {
                                        text5 = text5.replace("462Descripcion",
                                                fauna.getDescripcion() != null ? fauna.getDescripcion() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("462Area")) {
                                        text5 = text5.replace("462Area",
                                                fauna.getAreaHa() != null ? fauna.getAreaHa().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("462P")) {
                                        text5 = text5.replace("462P",
                                                fauna.getAreaHaPorcentaje() != null
                                                        ? fauna.getAreaHaPorcentaje().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 30-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreTipoBosquePGMFA}
     */
    public static void generarInfoAreTipoBosquePGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(13);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);

                                    if (text5 != null && detalleEntity.getCodSubInfBasicaDet().equals("TAB_3")) {

                                        if (text5.contains("462Anexo")) {
                                            text5 = text5.replace("462Anexo", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("462Su")) {
                                            text5 = text5.replace("462Su", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("462TB")) {
                                            text5 = text5.replace("462TB", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("462Descripcion")) {
                                            text5 = text5.replace("462Descripcion",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("462Area")) {
                                            text5 = text5.replace("462Area", "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("462P")) {
                                            text5 = text5.replace("462P", "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreCaracComunidad}
     */
    public static void generarInfoAreCaracComunidad(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(14);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null && detalleEntity.getCodSubInfBasicaDet().equals("TAB_1")) {

                                    if (text5.contains("471AcA")) {
                                        text5 = text5.replace("471AcA",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("471AcB")) {
                                        text5 = text5.replace("471AcB", "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreInfraEstructurasPGMFA}
     */
    public static void generarInfoAreInfraEstructurasPGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(15);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null && detalleEntity.getCodSubInfBasicaDet().equals("TAB_2")) {

                                    if (text5.contains("472Infraestructura")) {
                                        text5 = text5.replace("472Infraestructura",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("472M")) {
                                        text5 = text5.replace("472M", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("472Ubica")) {
                                        text5 = text5.replace("472Ubica",
                                                detalleEntity.getReferencia() != null ? detalleEntity.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 30-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento ParticipacionComunal}
     */
    public static void generarParticipacionComunal(List<ParticipacionComunalEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(42);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ParticipacionComunalEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("10ACTIVIDADES")) {
                                        text5 = text5.replace("10ACTIVIDADES",
                                                detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("10RESPONSABLE")) {
                                        text5 = text5.replace("10RESPONSABLE",
                                                detalleEntity.getResponsable() != null ? detalleEntity.getResponsable()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("10SEGUIMIENTO")) {
                                        text5 = text5.replace("10SEGUIMIENTO",
                                                detalleEntity.getSeguimientoActividad() != null
                                                        ? detalleEntity.getSeguimientoActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
     * public static void generarParticipacionComunal(ParticipacionComunalEntity
     * lstLista, XWPFDocument doc) throws IOException {
     * try {
     * XWPFTable table = doc.getTables().get(40);
     * XWPFTableRow row = table.getRow(1);
     * for (int i = 0; i < lstLista.getLstDetalle().size(); i++) {
     * XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(),
     * table);
     * for (XWPFTableCell cell : copiedRow.getTableCells()) {
     * for (XWPFParagraph p : cell.getParagraphs()) {
     * for (XWPFRun r : p.getRuns()) {
     * if (r != null) {
     * String text = r.getText(0);
     * if (text != null) {
     * if (text.contains("10ACTIVIDADES")) {
     * text = text.replace("10ACTIVIDADES",
     * lstLista.getActividad()!=null?lstLista.getActividad():"");
     * r.setText(text, 0);
     * } else if (text.contains("10RESPONSABLE")) {
     * text = text.replace("10RESPONSABLE",
     * lstLista.getResponsable()!=null?lstLista.getResponsable():"");
     * r.setText(text, 0);
     * }else if (text.contains("10SEGUIMIENTO")) {
     * text = text.replace("10SEGUIMIENTO",
     * lstLista.getSeguimientoActividad()!=null?lstLista.getSeguimientoActividad():
     * "");
     * r.setText(text, 0);
     * }
     * }
     * }
     * }
     * }
     * }
     * table.addRow(copiedRow);
     * }
     * table.removeRow(1);
     * return doc;
     * } catch (Exception e) {
     * System.out.println(e.getMessage());
     * return null;
     * }
     * }
     */

    /**
     * @autor: Rafael Azaña 02-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento CapacitacionPgmfa}
     */

    public static void generarCapacitacionPgmfa(List<CapacitacionDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(43);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (CapacitacionDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("11TEMAS")) {
                                        String temas = "";
                                        for (CapacitacionDetalleEntity t : detalleEntity.getListCapacitacionDetalle()) {
                                            if (t.getActividad() != null)
                                                if (!t.getActividad().trim().equals(""))
                                                    temas += t.getActividad() + "b" + (char) 13 + "a";
                                        }
                                        text5 = text5.replace("11TEMAS", temas);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("11PERSONAL")) {
                                        text5 = text5.replace("11PERSONAL",
                                                detalleEntity.getPersonaCapacitar() != null
                                                        ? detalleEntity.getPersonaCapacitar()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("11MODALI")) {
                                        text5 = text5.replace("11MODALI",
                                                detalleEntity.getIdTipoModalidad() != null
                                                        ? detalleEntity.getIdTipoModalidad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("11LUGAR")) {
                                        text5 = text5.replace("11LUGAR",
                                                detalleEntity.getLugar() != null ? detalleEntity.getLugar() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 02-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento OrganizacionActividadPgmfa}
     */

    public static XWPFDocument generarOrganizacionActividadPgmfa(ActividadSilviculturalDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(44);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("12ACTIVIDAD")) {
                                        text = text.replace("12ACTIVIDAD",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("12FORMA")) {
                                        text = text.replace("12FORMA",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña - 06-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento cronograma}
     */
    public static XWPFDocument procesarCronogramaPGMFA(List<CronogramaActividadEntity> lstResult, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(45);
            XWPFTableRow row0 = table.getRow(0);
            // XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(1);
            // listar por años
            List<CronogramaActividadEntity> lstAproveMadera = new ArrayList<>();
            List<Integer> lstAnios = new ArrayList<>();
            lstResult.stream().forEach((a) -> {
                a.getDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            for (Integer integer : lstAniosFinal) {
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                  //  cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(500));

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año" + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(8);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    /*
                     * XWPFTableCell cell1= row1.createCell();
                     * XWPFParagraph p1 = cell1.addParagraph();
                     * //cell1.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                     * cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                     * XWPFRun r1 = p1.createRun();
                     * r1.setText(""+integer, 0);
                     * r1.setFontFamily("Arial");
                     * r1.setFontSize(8);
                     * r1.setBold(true);
                     * p1.setAlignment(ParagraphAlignment.CENTER);
                     */

                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("MX_" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            for (int i = 0; i < lstResult.size(); i++) {
                XWPFTableRow copiedRow3 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                for (CronogramaActividadDetalleEntity obj : lstResult.get(i).getDetalle()) {
                    for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("actividadx")) {
                                            text5 = text5.replace("actividadx", lstResult.get(i).getActividad());
                                            r.setText(text5, 0);
                                        }

                                        else if (text5.contains("M_0")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(10))) {
                                            text5 = text5.replace("M_0", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_1")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(11))) {
                                            text5 = text5.replace("M_1", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_2")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(12))) {
                                            text5 = text5.replace("M_2", "X");
                                            r.setText(text5, 0);
                                        }
                                        for (Integer integer : lstAniosFinal) {
                                            if (text5.contains("MX_" + integer) && integer.equals(obj.getAnio())) {
                                                text5 = text5.replace("MX_" + integer,
                                                        obj.getMes() == 0 ? "X" : obj.getMes().toString());
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("M_0")) {
                                        text5 = text5.replace("M_0", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_1")) {
                                        text5 = text5.replace("M_1", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_2")) {
                                        text5 = text5.replace("M_2", "");
                                        r.setText(text5, 0);
                                    }
                                    for (Integer integer : lstAniosFinal) {
                                        if (text5.contains("MX_" + integer)) {
                                            text5 = text5.replace("MX_" + integer, "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (lstResult.get(i).getDetalle().size() > 0) {
                    table.addRow(copiedRow3);
                }
                CTTrPr trPr = copiedRow3.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
            }

            table.removeRow(1);

            CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
            width.setType(STTblWidth.AUTO);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    public static void procesarCronogramaPGMFAEstatico(List<CronogramaActividadEntity> lstCrono, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(45);
            XWPFTableRow row1 = table.getRow(1);

            int i = 0;
            for (CronogramaActividadEntity actividad : lstCrono) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("12act")) {
                                        text5 = text5.replace("12act",
                                                actividad.getActividad() != null ? actividad.getActividad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("12X")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                                        if (lstDetalle != null) {
                                            boolean breaker = false;
                                            for (CronogramaActividadDetalleEntity detalle : lstDetalle) {
                                                if (text5.contains("12X"+detalle.getAnio())){
                                                    text5 = text5.replace("12X"+detalle.getAnio(), "X");
                                                    r.setText(text5, 0);
                                                    breaker = true;
                                                    break;
                                                }
                                            }
                                            if (!breaker) {
                                                r.setText("", 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstCrono.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static XWPFDocument procesarCronogramaPGMFAEstatico2(List<CronogramaActividadEntity> lstResult, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(45);
            XWPFTableRow row0 = table.getRow(0);
            // XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(1);
            // listar por años
            List<CronogramaActividadEntity> lstAproveMadera = new ArrayList<>();
            List<Integer> lstAnios = new ArrayList<>();
            lstResult.stream().forEach((a) -> {
                a.getDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());

            for (int i = 0; i < lstResult.size(); i++) {
                XWPFTableRow copiedRow3 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                for (CronogramaActividadDetalleEntity obj : lstResult.get(i).getDetalle()) {
                    for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("actividadx")) {
                                            text5 = text5.replace("actividadx", lstResult.get(i).getActividad());
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_1")) {
                                            if(obj.getAnio().equals(1)){
                                                text5 = text5.replace("MX_1", "X");
                                            }else{
                                                text5 = text5.replace("MX_1", "");
                                            }
                                            r.setText(text5, 0);
                                        } else if (text5.contains("MX_2")) {
                                            if(obj.getAnio().equals(2)){
                                                text5 = text5.replace("MX_2", "X");
                                            }else{
                                                text5 = text5.replace("MX_2", "");
                                            }
                                            r.setText(text5, 0);
                                        } else if (text5.contains("MX_3")) {
                                            if(obj.getAnio().equals(3)){
                                                text5 = text5.replace("MX_3", "X");
                                            }else{
                                                text5 = text5.replace("MX_3", "");
                                            }
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_4")) {
                                            if(obj.getAnio().equals(4)){
                                                text5 = text5.replace("MX_4", "X");
                                            }else{
                                                text5 = text5.replace("MX_4", "");
                                            }
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_5")
                                                && (obj.getAnio().equals(5) )) {
                                            text5 = text5.replace("MX_5", "X");
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_6")
                                                && (obj.getAnio().equals(6) )) {
                                            text5 = text5.replace("MX_6", "X");
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_7")
                                                && (obj.getAnio().equals(7) )) {
                                            text5 = text5.replace("MX_7", "X");
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_8")
                                                && (obj.getAnio().equals(8) )) {
                                            text5 = text5.replace("MX_8", "X");
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_9")
                                                && (obj.getAnio().equals(9) )) {
                                            text5 = text5.replace("MX_9", "X");
                                            r.setText(text5, 0);
                                        }else if (text5.contains("MX_10")
                                                && (obj.getAnio().equals(10) )) {
                                            text5 = text5.replace("MX_10", "X");
                                            r.setText(text5, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                if (lstResult.get(i).getDetalle().size() > 0) {
                    table.addRow(copiedRow3);
                }
                CTTrPr trPr = copiedRow3.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


    public static XWPFDocument procesarCronogramaPGMFAResumido(List<CronogramaActividadEntity> lstResult, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row0 = table.getRow(0);
            // XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(1);
            // listar por años
            List<CronogramaActividadEntity> lstAproveMadera = new ArrayList<>();
            List<Integer> lstAnios = new ArrayList<>();
            lstResult.stream().forEach((a) -> {
                a.getDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            for (Integer integer : lstAniosFinal) {
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    //  cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(500));

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año" + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(8);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    /*
                     * XWPFTableCell cell1= row1.createCell();
                     * XWPFParagraph p1 = cell1.addParagraph();
                     * //cell1.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                     * cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                     * XWPFRun r1 = p1.createRun();
                     * r1.setText(""+integer, 0);
                     * r1.setFontFamily("Arial");
                     * r1.setFontSize(8);
                     * r1.setBold(true);
                     * p1.setAlignment(ParagraphAlignment.CENTER);
                     */

                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("MX_" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(10);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            for (int i = 0; i < lstResult.size(); i++) {
                XWPFTableRow copiedRow3 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                for (CronogramaActividadDetalleEntity obj : lstResult.get(i).getDetalle()) {
                    for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("actividadx")) {
                                            text5 = text5.replace("actividadx", lstResult.get(i).getActividad());
                                            r.setText(text5, 0);
                                        }

                                        else if (text5.contains("M_0")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(10))) {
                                            text5 = text5.replace("M_0", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_1")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(11))) {
                                            text5 = text5.replace("M_1", "X");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("M_2")
                                                && (obj.getAnio().equals(1) && obj.getMes().equals(12))) {
                                            text5 = text5.replace("M_2", "X");
                                            r.setText(text5, 0);
                                        }
                                        for (Integer integer : lstAniosFinal) {
                                            if (text5.contains("MX_" + integer) && integer.equals(obj.getAnio())) {
                                                text5 = text5.replace("MX_" + integer,
                                                        obj.getMes() == 0 ? "X" : obj.getMes().toString());
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (XWPFTableCell cell : copiedRow3.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("M_0")) {
                                        text5 = text5.replace("M_0", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_1")) {
                                        text5 = text5.replace("M_1", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M_2")) {
                                        text5 = text5.replace("M_2", "");
                                        r.setText(text5, 0);
                                    }
                                    for (Integer integer : lstAniosFinal) {
                                        if (text5.contains("MX_" + integer)) {
                                            text5 = text5.replace("MX_" + integer, "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (lstResult.get(i).getDetalle().size() > 0) {
                    table.addRow(copiedRow3);
                }
                CTTrPr trPr = copiedRow3.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
            }

            table.removeRow(1);

            CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
            width.setType(STTblWidth.AUTO);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 02-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento AspectosCompPgmfa}
     */
    public static void generarAspectosCompPgmfa(List<InformacionGeneralDEMAEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(46);
            System.out.println(lstLista.size());
            for (InformacionGeneralDEMAEntity detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("15Aspectos")) {
                                            text5 = text5.replace("15Aspectos",
                                                    detalleEntity.getDetalle() != null ? detalleEntity.getDetalle()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Danny Nazario 30-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento ProteccionBosquePGMFA}
     */
    public static void generarProteccionBosquePGMFA(List<ProteccionBosqueDetalleDto> lstLista, XWPFDocument doc) {
        try {
            // 8.1 Demarcación y Mantenimiento de Linderos
            XWPFTable table = doc.getTables().get(36);
            XWPFTableRow row = table.getRow(1);
            int i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("PGMFADEMA")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("81MARCACION")) {
                                            text5 = text5.replace("81MARCACION", detalleEntity.getTipoMarcacion());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("81ACCION")) {
                                            text5 = text5.replace("81ACCION",
                                                    detalleEntity.getNuAccion() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("81MODO")) {
                                            text5 = text5.replace("81MODO", detalleEntity.getImplementacion());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

            // 8.2 Evaluación de Impacto Ambiental
            // 8.2.1 Análisis de Impacto Ambiental
            table = doc.getTables().get(37);
            row = table.getRow(2);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("PGMFAANIM")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("821FACTOR")) {
                                            text5 = text5.replace("821FACTOR", detalleEntity.getFactorAmbiental());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821IMPACTO")) {
                                            text5 = text5.replace("821IMPACTO", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821CENSO")) {
                                            text5 = text5.replace("821CENSO",
                                                    detalleEntity.getNuCenso() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821LINDERO")) {
                                            text5 = text5.replace("821LINDERO",
                                                    detalleEntity.getNuDemarcacionLineal() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821CAMPAMENTO")) {
                                            text5 = text5.replace("821CAMPAMENTO",
                                                    detalleEntity.getNuConstruccionCampamento() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821CAMINO")) {
                                            text5 = text5.replace("821CAMINO",
                                                    detalleEntity.getNuConstruccionCamino() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821TALA")) {
                                            text5 = text5.replace("821TALA",
                                                    detalleEntity.getNuTala() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821ARRASTRE")) {
                                            text5 = text5.replace("821ARRASTRE",
                                                    detalleEntity.getNuArrastre() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("821OTRA")) {
                                            text5 = text5.replace("821OTRA",
                                                    detalleEntity.getNuOtra() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 2 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            // 8.2.2 Plan de Gestión Ambiental
            table = doc.getTables().get(38);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("PGMFAPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("PGMFAPRCO")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("822PREACT")) {
                                            text5 = text5.replace("822PREACT", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("822PREIMP")) {
                                            text5 = text5.replace("822PREIMP", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("822PREMED")) {
                                            text5 = text5.replace("822PREMED", detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            table = doc.getTables().get(39);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("PGMFAPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("PGMFAVISE")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("822VIGACT")) {
                                            text5 = text5.replace("822VIGACT", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("822VIGIMP")) {
                                            text5 = text5.replace("822VIGIMP", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("822VIGMED")) {
                                            text5 = text5.replace("822VIGMED", detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            table = doc.getTables().get(40);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("PGMFAPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("PGMFACOAM")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("822CONACT")) {
                                            text5 = text5.replace("822CONACT", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("822CONIMP")) {
                                            text5 = text5.replace("822CONIMP", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("822CONMED")) {
                                            text5 = text5.replace("822CONMED", detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Danny Nazario 01-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento MonitoreoPGMFA}
     */
    public static void generarMonitoreoPGMFA(List<MonitoreoDetalleEntity> lstLista, XWPFDocument doc) {
        try {
            // 9. Monitoreo
            XWPFTable table = doc.getTables().get(41);
            XWPFTableRow row = table.getRow(1);
            int i = 0;
            for (MonitoreoDetalleEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("9ACT")) {
                                        text5 = text5.replace("9ACT", detalleEntity.getActividad());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("9DES")) {
                                        text5 = text5.replace("9DES", detalleEntity.getDescripcion());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("9RES")) {
                                        text5 = text5.replace("9RES", detalleEntity.getResponsable());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(table.getRows().size() - 1);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreAnteUsoPGMFA}
     */
    public static void generarInfoAreAnteUsoPGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(16);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null && detalleEntity.getCodSubInfBasicaDet().equals("TAB_3")) {

                                    if (text5.contains("473Actividades")) {
                                        text5 = text5.replace("473Actividades",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("473E")) {
                                        text5 = text5.replace("473E", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("E73Especies")) {
                                        text5 = text5.replace("E73Especies",
                                                detalleEntity.getNombreLaguna() != null
                                                        ? detalleEntity.getNombreLaguna()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("473Observaciones")) {
                                        text5 = text5.replace("473Observaciones",
                                                detalleEntity.getReferencia() != null ? detalleEntity.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Jason Retamozo 29-11-2021
     * @creado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreAnteUsoPGMFA}
     */
    public static void generarInfoAreConflictoPGMFA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(17);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null && detalleEntity.getCodSubInfBasicaDet().equals("TAB_4")) {

                                    if (text5.contains("473bIdentificacion")) {
                                        text5 = text5.replace("473bIdentificacion",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("473Propuesta")) {
                                        text5 = text5.replace("473Propuesta",
                                                detalleEntity.getReferencia() != null ? detalleEntity.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 20-11-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     *               documento InfoAreaAcreditacionTerritorioPGMFA}
     */
    public static void generarInfoAreaAcreditacionTerritorioPGMFA(List<InfBasicaAereaDetalleDto> lstLista,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(4);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("41TIPODOCUMENTO")) {
                                            text5 = text5.replace("41TIPODOCUMENTO",
                                                    detalleEntity.getReferencia() != null
                                                            ? detalleEntity.getReferencia()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41NUMERO")) {
                                            text5 = text5.replace("41NUMERO",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41SUPERFICIE")) {
                                            text5 = text5.replace("41SUPERFICIE",
                                                    detalleEntity.getAreaHa() != null
                                                            ? detalleEntity.getAreaHa().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreAP(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(2);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("3131V")) {
                                        text5 = text5.replace("3131V",
                                                detalleEntity.getPuntoVertice() != null
                                                        ? detalleEntity.getPuntoVertice()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3131E")) {
                                        if (detalleEntity.getCoordenadaEste() != null) {
                                            text5 = text5.replace("3131E", detalleEntity.getCoordenadaEste() + "");
                                        } else {
                                            text5 = text5.replace("3131E", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3131N")) {
                                        if (detalleEntity.getCoordenadaNorte() != null) {
                                            text5 = text5.replace("3131N", detalleEntity.getCoordenadaNorte() + "");
                                        } else {
                                            text5 = text5.replace("3131N", "");
                                        }
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreAM(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(3);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("3141V")) {
                                        text5 = text5.replace("3141V",
                                                detalleEntity.getPuntoVertice() != null
                                                        ? detalleEntity.getPuntoVertice()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3141E")) {
                                        text5 = text5.replace("3141E",
                                                detalleEntity.getCoordenadaEste() != null
                                                        ? detalleEntity.getCoordenadaEste() + ""
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3141N")) {
                                        text5 = text5.replace("3141N",
                                                detalleEntity.getCoordenadaNorte() != null
                                                        ? detalleEntity.getCoordenadaNorte() + ""
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreACC1(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(4);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("321PR")) {
                                        text5 = text5.replace("321PR", detalleEntity.getReferencia() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("321DA")) {
                                        text5 = text5.replace("321DA", detalleEntity.getDistanciaKm() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("321T")) {
                                        text5 = text5.replace("321T", detalleEntity.getTiempo() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("321MT")) {
                                        text5 = text5.replace("321MT", detalleEntity.getMedioTransporte() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("321E")) {
                                        text5 = text5.replace("321E", detalleEntity.getEpoca() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreACC2(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(5);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("322PR")) {
                                        text5 = text5.replace("322PR", detalleEntity.getReferencia() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("322DA")) {
                                        text5 = text5.replace("322DA", detalleEntity.getDistanciaKm() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("322T")) {
                                        text5 = text5.replace("322T", detalleEntity.getTiempo() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("322MT")) {
                                        text5 = text5.replace("322MT", detalleEntity.getMedioTransporte() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("322E")) {
                                        text5 = text5.replace("322E", detalleEntity.getEpoca() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreRIO(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(6);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("331RIO")) {
                                        text5 = text5.replace("331RIO", detalleEntity.getNombreRio());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("331QUE")) {
                                        text5 = text5.replace("331QUE", detalleEntity.getNombreQuebrada());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("331LAG")) {
                                        text5 = text5.replace("331LAG", detalleEntity.getNombreLaguna());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreUF(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(7);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("332UNIFISIO")) {
                                        text5 = text5.replace("332UNIFISIO", detalleEntity.getDescripcion() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("332AREA")) {
                                        text5 = text5.replace("332AREA", detalleEntity.getAreaHa() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreZV(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(8);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(0);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("34ZV")) {
                                        text5 = text5.replace("34ZV", detalleEntity.getZonaVida());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 0 + i);
                i++;
            }
            table.removeRow(0 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreFAAU(List<EspecieFaunaEntity> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(9);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (EspecieFaunaEntity fauna : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("351NombreComun")) {
                                        text5 = text5.replace("351NombreComun",
                                                fauna.getNombreComun() != null ? fauna.getNombreComun() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("352NombreCientifico")) {
                                        text5 = text5.replace("352NombreCientifico",
                                                fauna.getNombreCientifico() != null ? fauna.getNombreCientifico() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("351Status")) {
                                        text5 = text5.replace("351Status",
                                                fauna.getCategoria() != null ? fauna.getCategoria() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreFLO(List<EspecieForestalEntity> lstFlora, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(10);
            System.out.println(lstFlora.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (EspecieForestalEntity flora : lstFlora) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("352NombreComun")) {
                                        text5 = text5.replace("352NombreComun",
                                                flora.getNombreComun() != null ? flora.getNombreComun() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("352NombreCientifico")) {
                                        text5 = text5.replace("352NombreCientifico",
                                                flora.getNombreCientifico() != null ? flora.getNombreCientifico() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("352Status")) {
                                        text5 = text5.replace("352Status",
                                                flora.getFamilia() != null ? flora.getFamilia() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstFlora.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarOrdenamientoInterno(List<OrdenamientoInternoDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(12);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (OrdenamientoInternoDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("4DENOMINACION")) {
                                        text5 = text5.replace("4DENOMINACION", detalleEntity.getCategoria() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4SUPERFICIE")) {
                                        text5 = text5.replace("4SUPERFICIE", detalleEntity.getAreaHA() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarInfoAreOrganizacion(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(11);

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("36ORGAPRO")) {
                                        text5 = text5.replace("36ORGAPRO", detalleEntity.getDescripcion());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("36NUMFAM")) {
                                        text5 = text5.replace("36NUMFAM", detalleEntity.getPuntoVertice());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("36ESPECIE")) {
                                        text5 = text5.replace("36ESPECIE", detalleEntity.getZonaVida());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("36TIP")) {
                                        text5 = text5.replace("36TIP", detalleEntity.getReferencia());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static XWPFDocument generarDocumentoEvaluacionPlanManejoForestal(String idPlanManejo,
            List<PlanManejoEvaluacionDetalleDto> lista, XWPFDocument doc) throws IOException {
        try {
            // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALA = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALA")).collect(Collectors.toList());
            String txtCodigoTramite = "", txtFechaRecepcion = "", b_txtConforme = "", b_txtObservacion = "-";
            String c_txtConforme = "", c_txtObservacion = "-", d_txtConforme = "", d_txtObservacion = "-",
                    e_txtConforme = "", e_txtObservacion = "-";
            String f_txtConforme = "", f_txtObservacion = "-", g_txtConforme = "", g_txtObservacion = "-",
                    h_txtConforme = "", h_txtObservacion = "-",
                    i_txtConforme = "", i_txtObservacion = "-";

            if (listPGMFEVALA.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALA = listPGMFEVALA.get(0);
                txtCodigoTramite = PGMFEVALA.getCodigoTramite() == null ? "-" : PGMFEVALA.getCodigoTramite();
                txtFechaRecepcion = PGMFEVALA.getFechaRecepcion() == null ? "-"
                        : PGMFEVALA.getFechaRecepcion().toString();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALB = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALB")).collect(Collectors.toList());
            if (listPGMFEVALB.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALB = listPGMFEVALB.get(0);
                b_txtConforme = PGMFEVALB.getConforme() ? "SI" : "NO";
                b_txtObservacion = PGMFEVALB.getObservacion() == null ? "-" : PGMFEVALB.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALC = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALC")).collect(Collectors.toList());
            if (listPGMFEVALC.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALC = listPGMFEVALC.get(0);
                c_txtConforme = PGMFEVALC.getConforme() ? "SI" : "NO";
                c_txtObservacion = PGMFEVALC.getObservacion() == null ? "-" : PGMFEVALC.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALD = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALD")).collect(Collectors.toList());
            if (listPGMFEVALD.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALD = listPGMFEVALD.get(0);
                d_txtConforme = PGMFEVALD.getConforme() ? "SI" : "NO";
                d_txtObservacion = PGMFEVALD.getObservacion() == null ? "-" : PGMFEVALD.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALE = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALE")).collect(Collectors.toList());
            if (listPGMFEVALE.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALE = listPGMFEVALE.get(0);
                e_txtConforme = (PGMFEVALE.getConforme() == null ? "" : (PGMFEVALE.getConforme() ? "SI" : "NO"));
                e_txtObservacion = PGMFEVALE.getObservacion() == null ? "-" : PGMFEVALE.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALF = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALF")).collect(Collectors.toList());
            if (listPGMFEVALF.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALF = listPGMFEVALF.get(0);
                f_txtConforme = (PGMFEVALF.getConforme() == null ? "" : (PGMFEVALF.getConforme() ? "SI" : "NO"));
                f_txtObservacion = PGMFEVALF.getObservacion() == null ? "-" : PGMFEVALF.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALG = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALG")).collect(Collectors.toList());
            if (listPGMFEVALG.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALG = listPGMFEVALG.get(0);
                g_txtConforme = (PGMFEVALG.getConforme() == null ? "" : (PGMFEVALG.getConforme() ? "SI" : "NO"));
                g_txtObservacion = PGMFEVALG.getObservacion() == null ? "-" : PGMFEVALG.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALH = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALH")).collect(Collectors.toList());
            if (listPGMFEVALH.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALH = listPGMFEVALH.get(0);
                h_txtConforme = PGMFEVALH.getConforme() ? "SI" : "NO";
                h_txtObservacion = PGMFEVALH.getObservacion() == null ? "-" : PGMFEVALH.getObservacion();
            }

            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALI = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALI")).collect(Collectors.toList());
            if (listPGMFEVALI.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALI = listPGMFEVALI.get(0);
                i_txtConforme = PGMFEVALI.getConforme() ? "SI" : "NO";
                i_txtObservacion = PGMFEVALI.getObservacion() == null ? "-" : PGMFEVALI.getObservacion();
            }

            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("idPlanManejo")) {
                                text = text.replace("idPlanManejo", idPlanManejo);
                                r.setText(text, 0);
                            } else if (text.contains("txtCodigoTramite")) {
                                text = text.replace("txtCodigoTramite", txtCodigoTramite);
                                r.setText(text, 0);
                            } else if (text.contains("txtFechaRecepcion")) {
                                text = text.replace("txtFechaRecepcion", txtFechaRecepcion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformeb")) {
                                if (b_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformeb", "");
                                } else {
                                    text = text.replace("txtconformeb", b_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservacionb")) {
                                text = text.replace("txtobservacionb", b_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformec")) {
                                if (c_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformec", "");
                                } else {
                                    text = text.replace("txtconformec", c_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservacionc")) {
                                text = text.replace("txtobservacionc", c_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformed")) {
                                if (d_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformed", "");
                                } else {
                                    text = text.replace("txtconformed", d_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservaciond")) {
                                text = text.replace("txtobservaciond", d_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformee")) {
                                if (e_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformee", "");
                                } else {
                                    text = text.replace("txtconformee", e_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservacione")) {
                                text = text.replace("txtobservacione", e_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformef")) {
                                if (f_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformef", "");
                                } else {
                                    text = text.replace("txtconformef", f_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservacionf")) {
                                text = text.replace("txtobservacionf", f_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformeg")) {
                                if (g_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformeg", "");
                                } else {
                                    text = text.replace("txtconformeg", g_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservaciong")) {
                                text = text.replace("txtobservaciong", g_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformeh")) {
                                if (h_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformeh", "");
                                } else {
                                    text = text.replace("txtconformeh", h_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservacionh")) {
                                text = text.replace("txtobservacionh", h_txtObservacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtconformei")) {
                                if (i_txtConforme.equals("NO")) {
                                    text = text.replace("Conforme: txtconformei", "");
                                } else {
                                    text = text.replace("txtconformei", i_txtConforme);
                                }
                                r.setText(text, 0);
                            } else if (text.contains("txtobservacioni")) {
                                text = text.replace("txtobservacioni", i_txtObservacion);
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearConsideracionesEspeciales(List<PlanManejoEvaluacionDetalleDto> lista,
            XWPFDocument doc) throws IOException {
        try {
            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALH = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALH")).collect(Collectors.toList());
            List<PlanManejoEvaluacionSubDetalleDto> planManejoEvaluacionSubDetalle = new ArrayList<>();
            if (listPGMFEVALH.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALH = listPGMFEVALH.get(0);
                planManejoEvaluacionSubDetalle = PGMFEVALH.getLstPlanManejoEvaluacionSubDetalle();
            }
            XWPFTable table = doc.getTables().get(1);
            XWPFTableRow row = table.getRow(1);
            for (PlanManejoEvaluacionSubDetalleDto detalle : planManejoEvaluacionSubDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("322PR")) {
                                        text = text.replace("322PR", detalle.getConsideracionDescripcion());
                                        r.setText(text, 0);
                                    } else if (text.contains("322MT")) {
                                        text = text.replace("322MT", detalle.getConforme() ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("322E")) {
                                        text = text.replace("322E",
                                                detalle.getObservacion() == null ? "" : detalle.getObservacion());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesInventariadas(List<PlanManejoEvaluacionDetalleDto> lista, XWPFDocument doc)
            throws IOException {
        try {
            List<PlanManejoEvaluacionDetalleDto> listPGMFEVALI = lista.stream()
                    .filter(eval -> eval.getCodigoTipo().equals("PGMFEVALI")).collect(Collectors.toList());
            List<PlanManejoEvaluacionSubDetalleDto> planManejoEvaluacionSubDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                PlanManejoEvaluacionDetalleDto PGMFEVALI = listPGMFEVALI.get(0);
                planManejoEvaluacionSubDetalle = PGMFEVALI.getLstPlanManejoEvaluacionSubDetalle();
            }

            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(1);
            for (PlanManejoEvaluacionSubDetalleDto detalle : planManejoEvaluacionSubDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("35PR")) {
                                        text = text.replace("35PR", detalle.getIdEspecie().toString());
                                        r.setText(text, 0);
                                    } else if (text.contains("35DA")) {
                                        text = text.replace("35DA", detalle.getNombreCientifico());
                                        r.setText(text, 0);
                                    } else if (text.contains("35T")) {
                                        text = text.replace("35T", detalle.getNombreComun());
                                        r.setText(text, 0);
                                    } else if (text.contains("35MT")) {
                                        text = text.replace("35MT", detalle.getConforme() ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("35E")) {
                                        text = text.replace("35E",
                                                detalle.getObservacion() == null ? "" : detalle.getObservacion());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearRequisitosTUPA(List<PlanManejoArchivoEntity> listArchivos, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(1);
            for (PlanManejoArchivoEntity detalle : listArchivos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("321PR")) {
                                        text = text.replace("321PR",
                                                detalle.getTipoDocumento() == null ? "" : detalle.getTipoDocumento());
                                        r.setText(text, 0);
                                    } else if (text.contains("321DA")) {
                                        text = text.replace("321DA",
                                                detalle.getNombre() == null ? "" : detalle.getNombre());
                                        r.setText(text, 0);
                                    } else if (text.contains("321T")) {
                                        text = text.replace("321T", detalle.getConforme() == null ? ""
                                                : detalle.getConforme() ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("321MT")) {
                                        text = text.replace("321MT",
                                                detalle.getObservacion() == null ? "" : detalle.getObservacion());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearTitularRegente(String nombreTitularEvaluacion, String nombreUsuarioArffs,
            String tipoDocumento, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("txtnombretitular")) {
                                text = text.replace("txtnombretitular", nombreTitularEvaluacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtnombrearffs")) {
                                text = text.replace("txtnombrearffs", nombreUsuarioArffs);
                                r.setText(text, 0);
                            } else if (text.contains("txttipodocumento")) {
                                text = text.replace("txttipodocumento", tipoDocumento);
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearLineamientos1y2(String s, List<PlanManejoEvaluacionDetalleDto> lista,
            XWPFDocument doc) {
        try {
            int post = 0;
            if (s.equals("90")) {
                post = 3;
            } else if (s.equals("91")) {
                post = 4;
            }

            XWPFTable table = doc.getTables().get(post);
            XWPFTableRow row = table.getRow(1);
            for (PlanManejoEvaluacionDetalleDto detalle : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("42VERI1")) {
                                        text = text.replace("42VERI1",
                                                detalle.getDescripcion() == null ? "" : detalle.getDescripcion());
                                        r.setText(text, 0);
                                    } else if (text.contains("42VERI2")) {
                                        text = text.replace("42VERI2", detalle.getConforme() == null ? ""
                                                : detalle.getConforme() ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("42VERI3")) {
                                        text = text.replace("42VERI3",
                                                detalle.getObservacion() == null ? "" : detalle.getObservacion());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearLineamientos3Y18(String s, List<PlanManejoEvaluacionDetalleDto> lista,
            XWPFDocument doc) {
        try {
            int post = 0;
            if (s.equals("92")) {
                post = 6;
            } else if (s.equals("93")) {
                post = 7;
            }

            XWPFTable table = doc.getTables().get(post);
            XWPFTableRow row = table.getRow(1);

            for (PlanManejoEvaluacionDetalleDto detalle : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN3.1")) {
                    detalle.setDescripcion(
                            "Lineamiento 3.1: Verificar que se haya incluido la información siguiente: \n " +
                                    "   1. Los datos personales del concesionario, así como de su representante legal, en caso de personas jurídicas. El domicilio debe encontrarse correctamente consignado. \n\n "
                                    +
                                    "   2. La vigencia de la concesión, la fecha de inicio y finalización. Además, verificar que no exista alguna causal de extinción del título habilitante, contemplada en el artículo 45 del Reglamento para la Gestión Forestal.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN3.2")) {
                    detalle.setDescripcion(
                            "Lineamiento 3.2: Verificar que se consigne información sobre la duración del PGMF, la que debe ser concordante con el periodo de vigencia de la concesión e"
                                    +
                                    "incluir la fecha de inicio y finalización: el área total de la concesión, que incluya la superficie correspondiente a los bosques de producción forestal y "
                                    +
                                    "áreas de protección: el potencial maderable total: y. el volumen de corta anual permisible.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN3.3")) {
                    detalle.setDescripcion(
                            "Lineamiento 3.3: Verificar que el regente se encuentre inscrito en el Registro Nacional de Regentes Forestales y de Fauna Silvestre del SERFOR, con"
                                    +
                                    "licencia vigente y categoría de regencia para productos forestales maderables; asimismo, debe encontrarse habilitado por su respectivo colegio profesional. "
                                    +
                                    "El contrato con el regente debe estipular que es para la formulación e implementación del plan manejo forestal.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN4")) {
                    detalle.setDescripcion(
                            "Lineamiento 4: Verificar que los objetivos propuestos guarden relación con las actividades de aprovechamiento del plan a desarrollar."
                                    +
                                    "Adicionalmente, si se hubiese planteado el aprovechamiento complementario de productos forestales diferentes a la madera, ecoturismo, etc., "
                                    +
                                    "revisar que exista coherencia con las actividades específicas que ello implica.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN5.6")) {
                    detalle.setDescripcion(
                            "Lineamiento 5.6: UTM, escala numérica, fuente cartográfica, poligonal georreferenciada del área de manejo y ubicación política. "
                                    +
                                    "Adicionalmente, revisar que los mapas incluyan Verificar que los mapas contengan la leyenda, el norte magnético y el membrete; "
                                    +
                                    "este último debe contener información del área total de la UMF, sistema de proyección la siguiente información: \n"
                                    +
                                    "   1. Mapa base: Límites y colindancias, red hidrográfica, fisiografía, topografía, red vial (vía de acceso a la concesión y caminos dentro de la concesión). \n"
                                    +
                                    "   2. Mapa base: Límites y colindancias, red hidrográfica, fisiografía, topografía, red vial (vía de acceso a la concesión y caminos dentro de la concesión). \n"
                                    +
                                    "   3. Mapa base: Límites y colindancias, red hidrográfica, fisiografía, topografía, red vial (vía de acceso a la concesión y caminos dentro de la concesión). \n"
                                    +
                                    "La escala de presentación de los mapas puede ser diferente a la recomendada en los lineamientos, siempre y cuando permitan la visualización de los detalles que se soliciten.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN5.1")) {
                    detalle.setDescripcion(
                            "Lineamiento 5.1: Verificar que el PGMF contenga la información sobre la ubicación política, de acuerdo con el contrato suscrito.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN5.2")) {
                    detalle.setDescripcion(
                            "Lineamiento 5.2: Verificar que se haya consignado información sobre las vías que conectan a la concesión con los principales centros poblados o ciudades,"
                                    +
                                    "así como los principales puntos de referencia, en coordenadas UTM, tomados para la accesibilidad. Las vías terrestres y/o fluviales "
                                    +
                                    "declaradas deben complementarse con información sobre distancias, tiempo y medio de transporte utilizado.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN5.3")) {
                    detalle.setDescripcion(
                            "Lineamiento 5.3: Verificar que se haya consignado la información siguiente: \n" +
                                    "   1. Ríos principales, ríos secundarios, quebradas y lagunas (cochas). \n" +
                                    "   2. Principales unidades fisiográficas. En caso de existir terrazas y colinas se debe incluir una subcategoría sobre el grado de disectación o erosión. \n"
                                    +
                                    "   3. Principales tipos de suelo, según clasificación de FAO u otra clasificación utilizada. \n"
                                    +
                                    "La información presentada debe ser contrastada con la presentada en los mapas adjuntos al PGMF.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN5.4")) {
                    detalle.setDescripcion("Lineamiento 5.4: Verificar que se cumpla lo siguiente: \n" +
                            "   1. Las especies de fauna silvestre existentes se encuentren listadas y de encontrarse categorizadas, indicar su status de acuerdo con la lista de "
                            +
                            "clasificación y categorización de las especies amenazadas de fauna silvestre legalmente protegidas7, así como, se indique si la información proviene de "
                            +
                            "evaluaciones propias o de encuestas a conocedores locales. Cuando se indique una especie que no se encuentra dentro del ámbito de distribución natural, "
                            +
                            "analizar la metodología empleada a efectos de advertir que no se trate de un error. \n" +
                            "   2. Los tipos de bosques se hayan clasificado, de conformidad con la Guía de Inventario de la Flora y Vegetación. \n"
                            +
                            "   3. Corroborar que la sumatoria de las áreas de cada tipo de bosque sea igual al área total de la UMF. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN5.5")) {
                    detalle.setDescripcion(
                            "Lineamiento 5.5: Verificar que se haya incluido la información siguiente: \n" +
                                    "   1. Descripción de comunidades, caseríos, colonos y parceleros ubicados en el entorno de la concesión. \n"
                                    +
                                    "   2. Información de la infraestructura de servicios como salud, educación, centros de producción y su ubicación (punto de referencia). \n"
                                    +
                                    "   3. Antecedentes de uso del bosque y en caso de haber existido extracción forestal maderable, indicar las especies aprovechadas. \n"
                                    +
                                    "   4. Identificación de posibles conflictos, las propuestas de solución y los actores que pueden participar. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN6")) {
                    detalle.setDescripcion(
                            "Lineamiento 6: Verificar que el PGMF contenga las categorías “Con Cobertura Boscosa” (bosques de producción forestal, bosques de protección y "
                                    +
                                    "bosque intervenido) y “Sin Cobertura Boscosa” (áreas de protección y áreas deforestadas), y considerar que se cumpla lo siguiente: \n"
                                    +
                                    "   1. La cobertura boscosa concuerde con el ordenamiento propuesto. \n" +
                                    "   2. La suma de las áreas del ordenamiento interno concuerde con el total del área de la UMF.\n"
                                    +
                                    "   3. Se hayan considerado áreas de protección que incluyan laderas con pendientes mayores al 100%; áreas en ambas márgenes de los ríos y quebradas según "
                                    +
                                    "lo siguiente: 50 m en ríos de más de 10 m de ancho con caudal permanente, 30 m para ríos y quebradas de entre 5 y 10 m de ancho; lagunas naturales y humedales; entre otros. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN7")) {
                    detalle.setDescripcion("Lineamiento 7: Verificar que se cumpla lo siguiente: \n" +
                            "   1. La división administrativa se haya realizado sobre el área forestal productiva, que no incluya áreas de protección y las no productivas. \n"
                            +
                            "   2. El número de PC corresponde al ciclo de recuperación o de corta establecido, indicando si es que usarán bloques quinquenales o parcelas de corta distribuidas en toda el área productiva.\n"
                            +
                            "   3.Las PC determinadas no sobrepasen el 20% de variación de tamaño entre sí. \n" +
                            "   4. En caso se haya incorporado bloques quinquenales deben tener un orden pre establecido, enumerado y consecutivo. \n"
                            +
                            "   5. Se consigne información sobre los frentes de corta. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN8")) {
                    detalle.setDescripcion("Lineamiento 8: Verificar que se cumpla como mínimo lo siguiente: \n" +
                            "   1. Se prevea la prohibición de caza con fines de abastecimiento del personal de la concesión. \n"
                            +
                            "   2. Se prevea la ubicación y marcado de los vértices de la concesión, la señalización en los puntos de acceso y límites estratégicos, la demarcación y "
                            +
                            "mantenimiento de linderos de la concesión en sectores críticos, y las actividades de vigilancia de la UMF a través de patrullajes periódicos.\n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN9")) {
                    detalle.setDescripcion(
                            "Lineamiento 9.1: Analizar la metodología con la que se realizó el inventario y revisar lo siguiente: \n"
                                    +
                                    "   1. Se consigne información sobre el diseño del inventario solicitado. \n" +
                                    "   2. Las unidades de muestreo se distribuyan de manera proporcional a la superficie de cada tipo de bosque o estrato del área productiva.\n"
                                    +
                                    "   3. Se haya calculado correctamente el tamaño de la muestra. \n" +
                                    "   4. Se consigne información sobre el diámetro mínimo de inventario, tamaño de la parcela, intensidad de muestreo, distancia entre parcelas. \n"
                                    +
                                    "   5. Se consigne información sobre las principales variables estadísticas del inventario como: intensidad de muestreo, media aritmética, desviación estándar, "
                                    +
                                    "coeficiente de variación, límites de confianza, error estándar, y error de muestreo del 20% y a un 95% de nivel de confianza. \n"
                                    +
                                    "   6. Se hayan inventariado las especies maderables a partir de 20 cm de DAP. \n" +
                                    "   7. El inventario de fustales se haya realizado en sub parcelas de muestreo cuya intensidad corresponda a un 20% del área de la unidad de muestreo, "
                                    +
                                    "para las especies maderables de entre 10 cm y menores a 20 cm de DAP. \n" +
                                    "   8. Se hayan previsto intensidades de aprovechamiento de hasta 10 m3/ha en áreas con pendientes de entre 60 a 100%. En este caso se prevé el uso de "
                                    +
                                    "tecnologías de bajo impacto. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN9.2")) {
                    detalle.setDescripcion("Lineamiento 9.2: Verificar lo siguiente: \n" +
                            "   1. Las especies sean citadas en nomenclatura binomial (género y especie) y en los anexos se incluya la familia botánica a la que pertenece. \n"
                            +
                            "   2. Los resultados por clase diométrica y tipo de bosque o estrato concuerden con lo presentado en los cuadros resumen que forman parte del PGMF.\n"
                            +
                            "   3. Se utilice el valor de 0.65 como factor de forma para la tabulación de datos, y en el caso de la especie Swietenia macrophylla (cooba) se considere el "
                            +
                            "factor de forma por región: 0.73 (Madre de Dios). 0.76 (Ucayali) y 0.71 (Loreto). De considerarse valores diferentes, revisar que los sustentos citados provengan de fuentes fiables. \n"
                            +
                            "   4. El inventario considere el potencial maderable por cada tipo de bosque. \n" +
                            "   5. Los resultados contemplados en el resumen correspondan a los obtenidos de la tabulación de los datos del censo comercial o inventario. \n"
                            +
                            "   6. Se presente información sobre las especies más abundantes del bosque productivo. \n"
                            +
                            "   7. Distribución de número de individuos por close diamétrica para las especies a aprovechar. \n"
                            +
                            "   8. Se hayan previsto intensidades de aprovechamiento de hasta 10 m3/ha en áreas con pendientes de entre 60 a 100%. "
                            +
                            "En este caso se prevé el uso de tecnologías de bajo impacto. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN9.3")) {
                    detalle.setDescripcion("Lineamiento 9.3: Verificar lo siguiente: \n" +
                            "   1. Se cite a las especies inventariadas. \n" +
                            "   2. Se consigne los valores promedio del número de árboles y el área basal por especie y clases diamétricas a intervalos de 5 cm. por hectárea y área total. "
                            +
                            "para cada tipo de bosque.\n" +
                            "   3. Las especies más abundantes en el bosque de producción forestal \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.1")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.1: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   ► Descripción de los actividades a realizar en las áreas establecidas para el aprovechamiento maderable y en las otras categorías de ordenamiento. ");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.2")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.1: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   ► El sistema de manejo seleccionado.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.3")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.1: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   ► Ciclo de corta de 20 años. En caso se haya propuesto un ciclo diferente, verificar que se haya sustentado adecuadamente.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.4")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.1: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   1. La relación de especies a manejar y sus respectivos diámetros mínimos de corta. \n"
                                    +
                                    "   2. Aplicación de prócticas de impacto reducido (AIR), tales como la adecuada planificación de la red vial y patios de acopio, empleo de técnicos de tala "
                                    +
                                    "dirigida, etc.\n" +
                                    "   3. Agrupamiento de las especies forestales según los objetivos de manejo y el valor o aceptación de las especies en el mercado. \n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.5")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.1: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   ► Categorización de especies de acuerdo con la lista oficial de especies amenazadas de flora silvestre.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.6.1")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.2: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   1. Volumen comercial promedio, calculado por estratos o tipos de bosque y que no considere los árboles semilleros. \n"
                                    +
                                    "   2. Volumen de corta anual permisible.\n" +
                                    "   3. Proyección de la cosecha para los años que comprende el ciclo de corta \n" +
                                    "   4. Descripción de las actividades para las tres fases del sistema de aprovechamiento (preaprovechamiento, aprovechamiento y post-aprovechamiento). "
                                    +
                                    "los recursos necesarios para su ejecución y los costos. ");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.7")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.2: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   ► Proyeccción de la cosecha para los años que comprende el ciclo de corta.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.8")) {
                    detalle.setDescripcion(
                            "Lineamiento 10.3: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   ► Proyeccción de la cosecha para los años que comprende el ciclo de corta.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10.9")) {
                    detalle.setDescripcion(
                            "Lineamiento 11: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   1. Descripción de las acciones a desarrollar para la construcción de los caminos y el método de construcción, inluyendo información sobre la mano de obra"
                                    +
                                    "necesaria, la maquinaria, herramientas y equipos a utilizar en cada operación \n" +
                                    "   2. Sistema de drenaje de las aguas de lluvia para los caminos principales y vías de acceso: con un ancho de superficie de rodaje entre 4 y 7 m "
                                    +
                                    "(según volumen de transporte programado), salvo sectores críticos que requieran tratamientos especiales: pendientes menores al 20%.\n"
                                    +
                                    "   3. Sistema de drenaje para la construcción de caminos secundarios, anchura de superficie de rodaje de 4 a 5 m -salvo sectores críticos que requieran "
                                    +
                                    "tratamientos especiales-. pendientes menores a 30% y se evite obstruir o cruzar los cursos de agua \n"
                                    +
                                    "   4. Patios de acopio localizados próximos a las márgenes de los caminos y en lo posible a un radio de distancia de operación del tractor forestal de "
                                    +
                                    "hasta 1 km, dentro de lo posible, la pendiente en el patio no deberá superar el 10% y no drenarán directamente a los caminos ni a los arroyos circundantes, "
                                    +
                                    "deben estar ubicados a más de 30 metros de ríos o quebrados.\n");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN10")) {
                    detalle.setDescripcion(
                            "Lineamiento 10: Verificar que se haya incorporado la información siguiente: \n" +
                                    "   1. Descripción de los actividades a realizar en las áreas establecidas para el aprovechamiento maderable y en las otras categorías de ordenamiento. \n"
                                    +
                                    "   2. El sistema de manejo seleccionado.\n" +
                                    "   3. Ciclo de corta de 20 años. En caso se haya propuesto un ciclo diferente, verificar que se haya sustentado adecuadamente. \n"
                                    +
                                    "   4. La relación de especies a manejar y sus respectivos diámetros mínimos de corta.\n"
                                    +
                                    "   5. Aplicación de prócticas de impacto reducido (AIR), tales como la adecuada planificación de la red vial y patios de acopio, empleo de técnicos de tala dirigida, etc.\n"
                                    +
                                    "   6. Agrupamiento de las especies forestales según los objetivos de manejo y el valor o aceptación de las especies en el mercado.\n"
                                    +
                                    "   7. Categorización de especies de acuerdo con la lista oficial de especies amenazadas de flora silvestre.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN12.1")) {
                    detalle.setDescripcion("Lineamiento 12: Revisar el cumplimiento de lo siguiente: \n" +
                            "   ► Presentación de las interacciones causa-efecto de cada una de las actividades del PGMF y los elementos del ambiente susceptibles de alteración. "
                            +
                            "Esto incluye la estimación de la magnitud de los impactos ambientales para el medio físico, biológico y socioeconómico.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN12.2")) {
                    detalle.setDescripcion("Lineamiento 12: Revisar el cumplimiento de lo siguiente: \n" +
                            "   1. La identificación de los impactos ambientales y caracterización (evaluación de los atributos asignados al tipo, magnitud, duración del impacto, entre otros) para los factores ambientales (ogua, suelo, flora, etc.) en cada etapa o fase de aprovechamiento. \n"
                            +
                            "   2. Inclusión del Plan de acción preventivo corrector, que incluya las medidas para prevenir, minimizar y rehabilitar los impactos identificados.\n"
                            +
                            "   3. Inclusión del Plon de vigilancia y seguimiento ambiental, que contenga las medidos de control ambiental. las medidas de monitoreo, la frecuencia y responsables. para cada impacto. \n"
                            +
                            "   4. Inclusión del Plan de contingencia ambiental, que contenga la identificación de la contingencia, las acciones a realizar y el responsable.\n"
                            +
                            "   5. Existencia de actividades relacionados al manejo de los residuos sólidos.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN12")) {
                    detalle.setDescripcion("Lineamiento 12: Revisar el cumplimiento de lo siguiente: \n" +
                            "   1. Presentación de las interacciones causa-efecto de cada una de las actividades del PGMF y los elementos del ambiente susceptibles de alteración. Esto incluye la estimación de la magnitud de los impactos ambientales para el medio físico, biológico y socioeconómico. \n"
                            +
                            "   2. La identificación de los impactos ambientales y caracterización (evaluación de los atributos asignados al tipo, magnitud, duración del impacto, entre otros) para los factores ambientales (ogua, suelo, flora, etc.) en cada etapa o fase de aprovechamiento.\n"
                            +
                            "   3. Inclusión del Plan de acción preventivo corrector, que incluya las medidas para prevenir, minimizar y rehabilitar los impactos identificados. \n"
                            +
                            "   4. Inclusión del Plon de vigilancia y seguimiento ambiental, que contenga las medidos de control ambiental. las medidas de monitoreo, la frecuencia y responsables. para cada impacto.\n"
                            +
                            "   5. Inclusión del Plan de contingencia ambiental, que contenga la identificación de la contingencia, las acciones a realizar y el responsable.\n"
                            +
                            "   6. Existencia de actividades relacionados al manejo de los residuos sólidos.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN13")) {
                    detalle.setDescripcion(
                            "Lineamiento 13: ► Verificar que se hoya incluido un sistema permanente de registros de información de las actividades que comprende la implementación del PGMF, con indicaciones de cómo se reoliza el registro, la manipulación, el almacenamiento y el análisis de datos.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN14.1")) {
                    detalle.setDescripcion(
                            "Lineamiento 14: ► Verificar que se haya previsto la participación de la comunidad o los pobladores locales y las formas en la que se realizará.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN14.2")) {
                    detalle.setDescripcion(
                            "Lineamiento 14: ► Verificar que se haya previsto la participación de la comunidad o los pobladores locales y las formas en la que se realizará.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN14.3")) {
                    detalle.setDescripcion(
                            "Lineamiento 14: ► Verificar que se haya previsto la participación de la comunidad o los pobladores locales y las formas en la que se realizará.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN14.4")) {
                    detalle.setDescripcion(
                            "Lineamiento 14: ► Verificar que se haya previsto la participación de la comunidad o los pobladores locales y las formas en la que se realizará.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN14")) {
                    detalle.setDescripcion(
                            "Lineamiento 14: ► Verificar que se haya previsto la participación de la comunidad o los pobladores locales y las formas en la que se realizará.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN15")) {
                    detalle.setDescripcion("Lineamiento 15: Revisar el cumplimiento de lo siguiente: \n" +
                            "1. Corroborar la existencia de un Plan de Capacitación, el cual forma parte del propio PGMF, que contenga: objetivos, temas o actividades, público objetivo y la modalidad de capacitación. \n"
                            +
                            "2. Las capacitaciones deben ser sobre los temas siguientes:\n" +
                            "   - Manejo de mapas e instrumentos cartográficos. \n" +
                            "   - Manejo y mantenimiento de motosierras. \n" +
                            "   - Tala dirigida \n" +
                            "   - Diseño y construcción de caminos forestales \n" +
                            "   - Trazabilidad de la madera \n" +
                            "   - Arrastre planificado. \n" +
                            "   - Cierre de actividades de extracción \n" +
                            "   - Operaciones silviculturales \n" +
                            "   - Salud y seguridad en el trabajo \n" +
                            "   - Sistema de registros de producción y costos.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN16.1")) {
                    detalle.setDescripcion(
                            "Lineamiento 16: ► Verificar que se incluya la información de las Responsabilidades del personal que labora en la concesión.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN16.2")) {
                    detalle.setDescripcion(
                            "Lineamiento 16: ► Verificar que se incluya la información de la cantidad de personas requeridas en cada nivel de la organización.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN16.3")) {
                    detalle.setDescripcion(
                            "Lineamiento 16: ► Verificar que se incluya la información de la maquinaria y equipo para la actividad que comprende la implementación del PGMF.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN16")) {
                    detalle.setDescripcion(
                            "Lineamiento 16: ► Verificar que se incluya la información de los Responsables del personal que labora en la concesión y la cantidad de personas requeridas en cada nivel de la organización, asi como de la maquinaria y equipo para la actividad que comprende la implementación del PGMF.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN17")) {
                    detalle.setDescripcion(
                            "Lineamiento 17: ► Revisar que se haya presentado el flujo de caja o balance de ingresos y costos proyectados como mínimo para los primeros cinco (05), así como una estructura de costos en función a las actividades previstas.");
                } else if (detalle.getCodigoTipo() != null && detalle.getCodigoTipo().equals("PGMFEVALLIN18")) {
                    detalle.setDescripcion(
                            "Lineamiento 18: ► Verificar que el cronograma contemple a las actividades descritas para la implemantación del PGMFy se encuentren calendarizadas mensualmente para los primeros conco años y quinquenalmente para los siguientes años.");
                }

                // if (detalle.getDescripcion()!=null) {
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("DLIN")) {
                                        text = text.replace("DLIN",
                                                detalle.getDescripcion() == null ? "" : detalle.getDescripcion());
                                        r.setText(text, 0);
                                    } else if (text.contains("LIN")) {
                                        text = text.replace("LIN", detalle.getConforme() == null ? ""
                                                : detalle.getConforme() ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("OLN")) {
                                        text = text.replace("OLN",
                                                detalle.getObservacion() == null ? "" : detalle.getObservacion());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
                // }
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearSolicitudesOpinionesExternas(List<SolicitudOpinionDto> lista, XWPFDocument doc)
            throws IOException {
        try {

            XWPFTable table = doc.getTables().get(5);
            XWPFTableRow row = table.getRow(1);
            for (SolicitudOpinionDto detalle : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("OEC1")) {
                                        text = text.replace("OEC1",
                                                detalle.getSiglaEntidad() == null ? "" : detalle.getSiglaEntidad());
                                        r.setText(text, 0);
                                    } else if (text.contains("OEC2")) {
                                        text = text.replace("OEC2", detalle.getEsFavorable() == null ? ""
                                                : detalle.getEsFavorable() ? "SI" : "NO");
                                        r.setText(text, 0);
                                    } else if (text.contains("OEC3")) {
                                        text = text.replace("OEC3",
                                                detalle.getDescripcion() == null ? "" : detalle.getDescripcion());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearSolicitudCampo(EvaluacionCampoEntity solicitudCampo, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("txtconformesolevalcampo")) {
                                text = text.replace("txtconformesolevalcampo",
                                        (solicitudCampo.getResultadoFavorable() == null ? ""
                                                : solicitudCampo.getResultadoFavorable() ? "SI" : "NO"));
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearCapacidadJustificacion(Boolean devolucion, String observacion, Integer tipo,
            XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("txtCapacidad") && tipo == 1) {

                                String t = devolucion == null ? null : devolucion ? "OBSERVADO: " : "CONFORME";
                                if (t != null && devolucion) {
                                    t = t + " " + observacion;
                                }
                                text = text.replace("txtCapacidad", t);
                                r.setText(text, 0);
                            } else if (text.contains("txtJustificacion") && tipo == 2) {

                                String t = devolucion == null ? null : devolucion ? "OBSERVADO: " : "CONFORME";
                                if (t != null && devolucion) {
                                    t = t + " " + observacion;
                                }

                                text = text.replace("txtJustificacion", t);
                                r.setText(text, 0);
                            } else if (text.contains("txtCapacidad") && tipo == 3) {

                                text = text.replace("txtCapacidad", observacion);
                                r.setText(text, 0);
                            } else if (text.contains("txtJustificacion") && tipo == 4) {

                                text = text.replace("txtJustificacion", observacion);
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearCondicionesMinimas(List<ListaInformeDocumentoDto> lstresult, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(1);
            for (ListaInformeDocumentoDto item : lstresult) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("DESC1")) {
                                        text = text.replace("DESC1",
                                                item.getDescripcion() == null ? "" : item.getDescripcion());
                                        r.setText(text, 0);
                                    } else if (text.contains("RES1")) {
                                        text = text.replace("RES1",
                                                item.getResultado() == null ? "" : item.getResultado());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 16-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     */

    public static void generarResumenActividadPOCC(List<ResumenActividadPoEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            System.out.println(lstLista.size());
            for (ResumenActividadPoEntity detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (detalleEntity.getCodigoSubResumen().equals("POCCRARPOARARPO")) {
                                        if (text5 != null) {
                                            if (text5.contains("21nroresolucion")) {
                                                text5 = text5.replace("21nroresolucion",
                                                        detalleEntity.getNroResolucion() != null
                                                                ? detalleEntity.getNroResolucion().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("21nroPC")) {
                                                text5 = text5.replace("21nroPC",
                                                        detalleEntity.getNroPcs() != null
                                                                ? detalleEntity.getNroPcs().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("21area")) {
                                                text5 = text5.replace("21area",
                                                        detalleEntity.getAreaHa() != null
                                                                ? detalleEntity.getAreaHa().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 16-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     */
    public static XWPFDocument generarResumenActividadFrenteCortaPOCC(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {

        try {

            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POCCRARPOARARPO")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(1);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("21actividad")) {
                                            text = text.replace("21actividad",
                                                    detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("21ejemplo")) {
                                            text = text.replace("21ejemplo",
                                                    detalleEntity.getIndicador() != null ? detalleEntity.getIndicador()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("21progra")) {
                                            text = text.replace("21progra",
                                                    detalleEntity.getProgramado() != null
                                                            ? detalleEntity.getProgramado()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("21reali")) {
                                            text = text.replace("21reali",
                                                    detalleEntity.getRealizado() != null ? detalleEntity.getRealizado()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 20-12-2021
     * @modificado:
     * @descripción: {Procesa un archivo word PGMFA, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOCC(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POCCRARPOARRPOE")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            XWPFTable table = doc.getTables().get(3);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("22resumen")) {
                                            text = text.replace("22resumen",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static void generarInfoBasicaUEPC(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(4);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    // if (detalleEntity.getCodigoSubResumen().equals("POCCRARPOARARPO")) {

                                    if (text5 != null) {
                                        if (text5.contains("41nropc")) {
                                            text5 = text5.replace("41nropc",
                                                    detalleEntity.getNumeroPc() != null
                                                            ? detalleEntity.getNumeroPc().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41area")) {
                                            text5 = text5.replace("41area",
                                                    detalleEntity.getAreaTotal() != null
                                                            ? detalleEntity.getAreaTotal().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41nrohoja")) {
                                            text5 = text5.replace("41nrohoja",
                                                    detalleEntity.getNroHojaCatastral() != null
                                                            ? detalleEntity.getNroHojaCatastral()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41nombrehoja")) {
                                            text5 = text5.replace("41nombrehoja",
                                                    detalleEntity.getNombreHojaCatastral() != null
                                                            ? detalleEntity.getNombreHojaCatastral()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41distrito")) {
                                            text5 = text5.replace("41distrito",
                                                    detalleEntity.getDistrito() != null ? detalleEntity.getDistrito()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41cuenca")) {
                                            text5 = text5.replace("41cuenca",
                                                    detalleEntity.getCuenca() != null ? detalleEntity.getCuenca() : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                    // }

                                }
                            }
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static XWPFDocument generarInfoBasicaTIBO(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(6);
            XWPFTableRow row = table.getRow(1);
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("42tipobosque")) {
                                            text = text.replace("42tipobosque",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("42area")) {
                                            text = text.replace("42area",
                                                    detalleEntity.getAreaHa() != null
                                                            ? detalleEntity.getAreaHa().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("42por")) {
                                            text = text.replace("42por",
                                                    detalleEntity.getAreaHaPorcentaje() != null
                                                            ? detalleEntity.getAreaHaPorcentaje().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarInfoBasicaACCE(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(7);
            XWPFTableRow row = table.getRow(2);
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("43punto")) {
                                            text = text.replace("43punto",
                                                    detalleEntity.getReferencia() != null
                                                            ? detalleEntity.getReferencia()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43este")) {
                                            text = text.replace("43este",
                                                    detalleEntity.getCoordenadaEste() != null
                                                            ? detalleEntity.getCoordenadaEste().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43norte")) {
                                            text = text.replace("43norte",
                                                    detalleEntity.getCoordenadaNorte() != null
                                                            ? detalleEntity.getCoordenadaNorte().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43distancia")) {
                                            text = text.replace("43distancia",
                                                    detalleEntity.getDistanciaKm() != null
                                                            ? detalleEntity.getDistanciaKm().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43tiem")) {
                                            text = text.replace("43tiem",
                                                    detalleEntity.getTiempo() != null
                                                            ? detalleEntity.getTiempo().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43medio")) {
                                            text = text.replace("43medio",
                                                    detalleEntity.getMedioTransporte() != null
                                                            ? detalleEntity.getMedioTransporte()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarOrdenamientoCategoriaPOCC(List<OrdenamientoProteccionEntity> lstLista,
            XWPFDocument doc) {
        try {

            List<OrdenamientoProteccionDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (lstLista.size() > 0) {
                OrdenamientoProteccionEntity PGMFEVALI = lstLista.get(0);
                lstListaDetalle = PGMFEVALI.getListOrdenamientoProteccionDet();
            }
            List<OrdenamientoProteccionDetalleEntity> lstListaCate = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodigoTipoOrdenamientoDet().equals("POCCOPCPUMFCO"))
                    .collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(8);
            XWPFTableRow row = table.getRow(1);
            for (OrdenamientoProteccionDetalleEntity detalleEntity : lstListaCate) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("51categoriacc")) {
                                            text = text.replace("51categoriacc",
                                                    detalleEntity.getCategoria() != null ? detalleEntity.getCategoria()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("51A")) {
                                            text = text.replace("51A", detalleEntity.getAccion() != false ? "X" : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("51arecc")) {
                                            text = text.replace("51arecc",
                                                    detalleEntity.getAreaHA() != null
                                                            ? detalleEntity.getAreaHA().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("51pcc")) {
                                            text = text.replace("51pcc",
                                                    detalleEntity.getAreaHAPorcentaje() != null
                                                            ? detalleEntity.getAreaHAPorcentaje().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarOrdenamientoProteccionPOCC(List<OrdenamientoProteccionEntity> lstLista,
            XWPFDocument doc) {
        try {

            List<OrdenamientoProteccionDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (lstLista.size() > 0) {
                OrdenamientoProteccionEntity PGMFEVALI = lstLista.get(0);
                lstListaDetalle = PGMFEVALI.getListOrdenamientoProteccionDet();
            }
            List<OrdenamientoProteccionDetalleEntity> lstListaCate = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodigoTipoOrdenamientoDet().equals("POCCOPCPUMFPV"))
                    .collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(9);
            XWPFTableRow row = table.getRow(1);
            for (OrdenamientoProteccionDetalleEntity detalleEntity : lstListaCate) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("53actividad")) {
                                            text = text.replace("53actividad",
                                                    detalleEntity.getCategoria() != null ? detalleEntity.getCategoria()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("53metas")) {
                                            text = text.replace("53metas",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarMonitoreoPOCC(List<MonitoreoEntity> lstLista, XWPFDocument doc) {
        try {

            List<MonitoreoDetalleEntity> lstListaDetalle = new ArrayList<>();

            XWPFTable table = doc.getTables().get(21);
            XWPFTableRow row = table.getRow(1);
            for (MonitoreoEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);

                                List<MonitoreoEntity> lstListaCate = lstLista.stream()
                                        .filter(eval -> eval.getIdMonitoreo() == detalleEntity.getIdMonitoreo())
                                        .collect(Collectors.toList());

                                if (lstListaCate.size() > 0) {
                                    MonitoreoEntity PGMFEVALI = lstListaCate.get(0);
                                    lstListaDetalle = PGMFEVALI.getLstDetalle();
                                }
                                String operaciones = "";
                                if (lstListaDetalle.size() > 0) {
                                    for (MonitoreoDetalleEntity detalleEntity2 : lstListaDetalle) {
                                        operaciones += " " + detalleEntity2.getOperacion() + ", \r\n ";
                                    }
                                }
                                if (text != null) {
                                    if (text.contains("9moni")) {
                                        text = text.replace("9moni",
                                                detalleEntity.getMonitoreo() != null ? detalleEntity.getMonitoreo()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("9operaciones")) {
                                        text = text.replace("9operaciones", operaciones);
                                        r.setText(text, 0);
                                    } else if (text.contains("9descripcion")) {
                                        text = text.replace("9descripcion",
                                                detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("9indicador")) {
                                        text = text.replace("9indicador",
                                                detalleEntity.getIndicador() != null ? detalleEntity.getIndicador()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("9frecuenc")) {
                                        text = text.replace("9frecuenc",
                                                detalleEntity.getFrecuencia() != null ? detalleEntity.getFrecuencia()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("9responsable")) {
                                        text = text.replace("9responsable",
                                                detalleEntity.getResponsable() != null ? detalleEntity.getResponsable()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarParticipacionComunalPOCC(List<ParticipacionComunalEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ParticipacionComunalEntity> lstListaCate = lstLista.stream()
                    .filter(eval -> eval.getCodTipoPartComunal().equals("POCCPC")).collect(Collectors.toList());

            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstLista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lstLista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(22);
            XWPFTableRow row = table.getRow(1);
            for (ParticipacionComunalDetEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("10mecanismo")) {
                                            text = text.replace("10mecanismo",
                                                    detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("10metodologia")) {
                                            text = text.replace("10metodologia",
                                                    detalleEntity.getMetodologia() != null
                                                            ? detalleEntity.getMetodologia()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("10lugar")) {
                                            text = text.replace("10lugar",
                                                    detalleEntity.getLugar() != null ? detalleEntity.getLugar() : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Jason Retamozo 17-01-2022
     * @creado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     *               documento Capacitacion}
     */
    public static void generarCapacitacionPOAC(List<CapacitacionDto> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(32);
            XWPFTableRow row = table.getRow(1);
            for (CapacitacionDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                          if (text.contains("10personal")) {
                                            text = text.replace("10personal",
                                                    detalleEntity.getPersonaCapacitar() != null
                                                            ? detalleEntity.getPersonaCapacitar()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("10modalidad")) {
                                            text = text.replace("10modalidad",
                                                    detalleEntity.getIdTipoModalidad() != null
                                                            ? detalleEntity.getIdTipoModalidad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("10lugar")) {
                                            text = text.replace("10lugar",
                                                    detalleEntity.getLugar() != null
                                                            ? detalleEntity.getLugar()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("10temas")) {
                                            String temas = "";
                                            for (CapacitacionDetalleEntity t : detalleEntity
                                                    .getListCapacitacionDetalle()) {
                                                temas += t.getActividad() != null ? t.getActividad() + ",\n" : "";
                                            }
                                            text = text.replace("10temas", temas);
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static XWPFDocument generarCapacitacionPOCC(List<CapacitacionDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(23);
            XWPFTableRow row = table.getRow(1);
            for (CapacitacionDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        /*
                                         * if (text.contains("11temas")) {
                                         * text = text.replace("11temas", detalleEntity.getActividad() != null ?
                                         * detalleEntity.getActividad() : "");
                                         * r.setText(text, 0);
                                         * }else
                                         */if (text.contains("11personal ")) {
                                            text = text.replace("11personal ",
                                                    detalleEntity.getPersonaCapacitar() != null
                                                            ? detalleEntity.getPersonaCapacitar()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("11modalidad ")) {
                                            text = text.replace("11modalidad ",
                                                    detalleEntity.getIdTipoModalidad() != null
                                                            ? detalleEntity.getIdTipoModalidad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("11lugar")) {
                                            text = text.replace("11lugar",
                                                    detalleEntity.getLugar() != null ? detalleEntity.getLugar() : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("11periodo")) {
                                            text = text.replace("11periodo",
                                                    detalleEntity.getPeriodo() != null ? detalleEntity.getPeriodo()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("11responsable")) {
                                            text = text.replace("11responsable",
                                                    detalleEntity.getResponsable() != null
                                                            ? detalleEntity.getResponsable()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovechamientoPOCC(List<ActividadAprovechamientoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROCC")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROOALE")).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(10);
            XWPFTableRow row = table.getRow(1);
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("613comun")) {
                                            text = text.replace("613comun", "");
                                            r.setText(text, 0);
                                        } else if (text.contains("613cientifico")) {
                                            text = text.replace("613cientifico",
                                                    detalleEntity.getNombreCientifica() != null
                                                            ? detalleEntity.getNombreCientifica()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("613familia")) {
                                            text = text.replace("613familia",
                                                    detalleEntity.getFamilia() != null ? detalleEntity.getFamilia()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("613linea")) {
                                            text = text.replace("613linea",
                                                    detalleEntity.getLineaProduccion() != null ? detalleEntity.getLineaProduccion()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("613dmc")) {
                                            text = text.replace("613dmc",
                                                    detalleEntity.getDcm() != null ? detalleEntity.getDcm().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovechamientoRecursosMPOCC(
            List<ActividadAprovechamientoEntity> lstLista, XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROCC")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROOARR")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetSubEntity> lstListaDetalleSub = new ArrayList<>();
            if (lstListaDet.size() > 0) {
                ActividadAprovechamientoDetEntity PGMFEVALIDET = lstListaDet.get(0);
                lstListaDetalleSub = PGMFEVALIDET.getLstactividadDetSub();
            }

            XWPFTable table = doc.getTables().get(11);
            XWPFTableRow row = table.getRow(2);
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("616especie")) {
                                            text = text.replace("616especie",
                                                    detalleEntity.getNombreCientifica() != null
                                                            ? detalleEntity.getNombreCientifica()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                        for (ActividadAprovechamientoDetSubEntity detalleSubEntity : lstListaDetalleSub) {
                                            if (text.contains("616var")) {
                                                text = text.replace("616var",
                                                        detalleSubEntity.getVar() != null ? detalleSubEntity.getVar()
                                                                : "");
                                                r.setText(text, 0);
                                            }

                                            if (detalleSubEntity.getDapCM().equals("30-40")) {
                                                if (text.contains("6161")) {
                                                    text = text.replace("6161",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (detalleSubEntity.getDapCM().equals("40-50")) {
                                                if (text.contains("6162")) {
                                                    text = text.replace("6162",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (detalleSubEntity.getDapCM().equals("50-60")) {
                                                if (text.contains("6163")) {
                                                    text = text.replace("6163",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (detalleSubEntity.getDapCM().equals("60-70")) {
                                                if (text.contains("6164")) {
                                                    text = text.replace("6164",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (detalleSubEntity.getDapCM().equals("70-80")) {
                                                if (text.contains("6165")) {
                                                    text = text.replace("6165",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (detalleSubEntity.getDapCM().equals("80-90")) {
                                                if (text.contains("6166")) {
                                                    text = text.replace("6166",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (detalleSubEntity.getDapCM().equals("90+")) {
                                                if (text.contains("6167")) {
                                                    text = text.replace("6167",
                                                            detalleSubEntity.getDapCM() != null ? "X" : "");
                                                    r.setText(text, 0);
                                                }
                                            }

                                            if (text.contains("616totpc")) {
                                                text = text.replace("616totpc",
                                                        detalleSubEntity.getTotalHa() != null
                                                                ? detalleSubEntity.getTotalHa().toString()
                                                                : "");
                                                r.setText(text, 0);
                                            } else if (text.contains("616to")) {
                                                text = text.replace("616to",
                                                        detalleSubEntity.getTotalPC() != null
                                                                ? detalleSubEntity.getTotalPC().toString()
                                                                : "");
                                                r.setText(text, 0);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovechamientoRecursosNMPOCC(
            List<ActividadAprovechamientoEntity> lstLista, XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROCC")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROCCAR")).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(12);
            XWPFTableRow row = table.getRow(2);
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("617sector")) {
                                            text = text.replace("617sector",
                                                    detalleEntity.getSector() != null ? detalleEntity.getSector() : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("617area")) {
                                            text = text.replace("617area",
                                                    detalleEntity.getArea() != null ? detalleEntity.getArea().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("617totalindi")) {
                                            text = text.replace("617totalindi",
                                                    detalleEntity.getTotalIndividuos() != null
                                                            ? detalleEntity.getTotalIndividuos().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("617individuos")) {
                                            text = text.replace("617individuos",
                                                    detalleEntity.getIndividuosHA() != null
                                                            ? detalleEntity.getIndividuosHA()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("617volumen")) {
                                            text = text.replace("617volumen",
                                                    detalleEntity.getVolumen() != null ? detalleEntity.getVolumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInfoContrato(ModeloContratoDto data, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {


                                text = text.replace("txtTitulo", data.getDepartamentoAu() == null || data.getTituloHabilitante() == null ? "------------" :
                                        data.getDepartamentoAu() + " " + data.getTituloHabilitante());

                                text = text.replace("txtDepAu", data.getDepartamentoAu() == null ? "------------" : data.getDepartamentoAu());

                                text = text.replace("txtDirAu", data.getDireccionAu() == null ? "------------" : data.getDireccionAu());

                                text = text.replace("txtNumDocAu", data.getNumeroDocumentoAu() == null ? "------------" : data.getNumeroDocumentoAu());

                                text = text.replace("txtRazSocTit", data.getRazonSocialTitularTH() == null ? "------------" : data.getRazonSocialTitularTH());


                                text = text.replace("txtNumDocTit", data.getNumeroDocumentoTitularTH() == null ? "------------" : data.getNumeroDocumentoTitularTH());

                                text = text.replace("txtNomRl", data.getNombreRepLegal() == null || data.getApellidoPaternoRepLegal() == null
                                                                || data.getApellidoMaternoRepLegal() == null  ? "------------" : data.getNombreRepLegal() + " "
                                                                + data.getApellidoPaternoRepLegal() + " " + data.getApellidoMaternoRepLegal());

                                text = text.replace("txtNumDocRl", data.getNumeroDocumentoRepLegal() == null ? "------------" : data.getNumeroDocumentoRepLegal());

                                text = text.replace("txtDirAu", data.getDireccionAu() == null ? "------------" : data.getDireccionAu());

                                text = text.replace("txtDisAu", data.getDistritoAu() == null ? "------------" : data.getDistritoAu());

                                text = text.replace("txtProvAu", data.getProvinciaAu() == null ? "------------" : data.getProvinciaAu());

                                text = text.replace("totalAreaHa", data.getAreaTotal() == null ? "------------" : data.getAreaTotal().toString());

                                text = text.replace("txtUnApr", data.getUnidadAprovechamiento() == null ? "------------" : data.getUnidadAprovechamiento());

                                text = text.replace("txtFecDias", data.getDiaLetra() == null ? "------------" : data.getDiaLetra());

                                text = text.replace("txtFecMes", data.getMesLetra() == null ? "------------" : data.getMesLetra());

                                text = text.replace("txtFecAnio", data.getAnioLetra() == null ? "------------" : data.getAnioLetra());

                                text = text.replace("txtDisAu", data.getDistritoAu() == null ? "------------" : data.getDistritoAu());

                                text = text.replace("txtDirTit", data.getDireccionTitularTH() == null ? "------------" : data.getDireccionTitularTH());

                                text = text.replace("txtDisTit", data.getDistritoTitular() == null ? "------------" : data.getDistritoTitular());

                                text = text.replace("txtProvTit", data.getProvinciaTitular() == null ? "------------" : data.getProvinciaTitular());

                                text = text.replace("txtDepTit", data.getDepartamentoTitular() == null ? "------------" : data.getDepartamentoTitular());

                                text = text.replace("txtPartRe", data.getCodigoPartidaReg() == null ? "------------" : data.getCodigoPartidaReg());
                                r.setText(text, 0);

                        }
                    }
                }
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInfoContrato2(ModeloContratoDto data, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {

                            System.out.println(text);
                             

                                text = text.replace("txtDep", data.getDepartamentoRle()==null ? "----":data.getDepartamentoRle());


                                text = text.replace("titleTxt", data.getCodigoPartidaReg() ==null ? "----":data.getCodigoPartidaReg());


                                text = text.replace("txtDni", data.getNumeroDocumentoRepLegal()==null ? "----":data.getNumeroDocumentoRepLegal());


                                text = text.replace("txtNombre", data.getNombreRepLegal() == null || data.getApellidoPaternoRepLegal() == null
                                                                || data.getApellidoMaternoRepLegal() == null  ? "----" : data.getNombreRepLegal() + " "
                                                                + data.getApellidoPaternoRepLegal() + " " + data.getApellidoMaternoRepLegal());


                                text = text.replace("txtDir", data.getDireccionRepLegal()==null ?  "----" :data.getDireccionRepLegal());


                                text = text.replace("txtDis", data.getDistritoRle()==null ? "":data.getDistritoRle());


                                text = text.replace("txtProv", data.getProvinciaRle()==null ? "":data.getProvinciaRle());
                                r.setText(text, 0);

                             
                        }
                    }
                }
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovechamientoVolumenPOCC(List<ActividadAprovechamientoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROVC")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROVC")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetSubEntity> lstListaDetalleSub = new ArrayList<>();
            if (lstListaDet.size() > 0) {
                ActividadAprovechamientoDetEntity PGMFEVALIDET = lstListaDet.get(0);
                lstListaDetalleSub = PGMFEVALIDET.getLstactividadDetSub();
            }

            XWPFTable table = doc.getTables().get(13);
            XWPFTableRow row = table.getRow(4);
            int contador = 0;
            int contadorSub = 0;
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                contador++;
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("62nro")) {
                                            text = text.replace("62nro", contador + "");
                                            r.setText(text, 0);
                                        } else if (text.contains("62comun")) {
                                            text = text.replace("62comun", "");
                                            r.setText(text, 0);
                                        } else if (text.contains("62cientifico")) {
                                            text = text.replace("62cientifico",
                                                    detalleEntity.getNombreCientifica() != null
                                                            ? detalleEntity.getNombreCientifica()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                        for (ActividadAprovechamientoDetSubEntity detalleSubEntity : lstListaDetalleSub) {
                                            if (contadorSub == 0) {
                                                if (text.contains("62nroarb1")) {
                                                    text = text.replace("62nroarb1",
                                                            detalleSubEntity.getNroArbol() != null
                                                                    ? detalleSubEntity.getNroArbol().toString()
                                                                    : "");
                                                    r.setText(text, 0);
                                                } else if (text.contains("62volumen1")) {
                                                    text = text.replace("62volumen1",
                                                            detalleSubEntity.getVolM() != null
                                                                    ? detalleSubEntity.getVolM().toString()
                                                                    : "");
                                                    r.setText(text, 0);
                                                }
                                            } else if (contadorSub == 1) {
                                                if (text.contains("62nroarb1")) {
                                                    text = text.replace("62nroarb1",
                                                            detalleSubEntity.getNroArbol() != null
                                                                    ? detalleSubEntity.getNroArbol().toString()
                                                                    : "");
                                                    r.setText(text, 0);
                                                } else if (text.contains("62volumen1")) {
                                                    text = text.replace("62volumen1",
                                                            detalleSubEntity.getVolM() != null
                                                                    ? detalleSubEntity.getVolM().toString()
                                                                    : "");
                                                    r.setText(text, 0);
                                                }
                                            }

                                            contadorSub++;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(4);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovechamientoOperaPOCC(List<ActividadAprovechamientoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POCCAAPROOA")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodActvAproveDet().equals("POCCAAPROOA")).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(14);
            XWPFTableRow row = table.getRow(1);
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("63operaciones")) {
                                            text = text.replace("63operaciones",
                                                    detalleEntity.getOperaciones() != null
                                                            ? detalleEntity.getOperaciones()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("63uni")) {
                                            text = text.replace("63uni",
                                                    detalleEntity.getUnidad() != null ? detalleEntity.getUnidad() : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("63caracteristicas")) {
                                            text = text.replace("63caracteristicas",
                                                    detalleEntity.getCarcteristicaTec() != null
                                                            ? detalleEntity.getCarcteristicaTec()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("63personal")) {
                                            text = text.replace("63personal",
                                                    detalleEntity.getPersonal() != null ? detalleEntity.getPersonal()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("63maquinarias")) {
                                            text = text.replace("63maquinarias",
                                                    detalleEntity.getMaquinariaMaterial() != null
                                                            ? detalleEntity.getMaquinariaMaterial()
                                                            : "");
                                            r.setText(text, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadSilviculturalTratamientosPOCC(
            List<ActividadSilviculturalEntity> lstLista, XWPFDocument doc) {
        try {
            List<ActividadSilviculturalEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getMonitoreo().equals("B")).collect(Collectors.toList());

            List<ActividadSilviculturalDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadSilviculturalEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getListActividadSilvicultural();
            }

            XWPFTable table = doc.getTables().get(15);
            XWPFTableRow row = table.getRow(1);
            for (ActividadSilviculturalDetalleEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("72localArea")) {
                                            text = text.replace("72localArea",
                                                    detalleEntity.getTratamiento() != null
                                                            ? detalleEntity.getTratamiento()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("72parcelaCorta")) {
                                            text = text.replace("72parcelaCorta",
                                                    detalleEntity.getDescripcionDetalle() != null
                                                            ? detalleEntity.getDescripcionDetalle()
                                                            : "");
                                            r.setText(text, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadSilviculturalEnriqPOCC(List<ActividadSilviculturalEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ActividadSilviculturalEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getMonitoreo().equals("C")).collect(Collectors.toList());

            List<ActividadSilviculturalDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadSilviculturalEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getListActividadSilvicultural();
            }

            XWPFTable table = doc.getTables().get(16);
            XWPFTableRow row = table.getRow(0);
            for (ActividadSilviculturalDetalleEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("73Enriquecimiento")) {
                                            text = text.replace("73Enriquecimiento",
                                                    detalleEntity.getTratamiento() != null
                                                            ? detalleEntity.getTratamiento()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("73enriquecimiento2")) {
                                            text = text.replace("73enriquecimiento2",
                                                    detalleEntity.getDescripcionDetalle() != null
                                                            ? detalleEntity.getDescripcionDetalle()
                                                            : "");
                                            r.setText(text, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarEvalAmbientalImpactosAmbPOCC(List<EvaluacionAmbientalActividadEntity> lstLista,
            List<EvaluacionAmbientalDto> lstEvalAmbientalAcciones, XWPFDocument doc) {
        try {
            List<EvaluacionAmbientalDto> lstAccionesFilPREAprovec = lstEvalAmbientalAcciones.stream()
                    .filter(eval -> eval.getTipoAprovechamiento().equals("PREAPR")).collect(Collectors.toList());
            List<EvaluacionAmbientalDto> lstAccionesFilAprovec = lstEvalAmbientalAcciones.stream()
                    .filter(eval -> eval.getTipoAprovechamiento().equals("APROVE")).collect(Collectors.toList());
            List<EvaluacionAmbientalDto> lstAccionesFilPOSTAprovec = lstEvalAmbientalAcciones.stream()
                    .filter(eval -> eval.getTipoAprovechamiento().equals("POSTAP")).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(17);
            XWPFTableRow row0 = table.getRow(1);
            XWPFTableRow row = table.getRow(3);
            // listar por ACCIONES
            List<EvaluacionAmbientalDto> lstAproveMadera = new ArrayList<>();
            List<String> lstCabeceraPreApro = new ArrayList<>();
            List<String> lstCabeceraApro = new ArrayList<>();
            List<String> lstCabeceraPostApro = new ArrayList<>();

            lstAccionesFilPREAprovec.stream().forEach((a) -> {
                String variableCabecera = "";
                variableCabecera = a.getNombreAprovechamiento();
                if (!a.getNombreAprovechamiento().equals(variableCabecera)) {
                    lstCabeceraPreApro.add(a.getNombreAprovechamiento());
                }
            });

            lstAccionesFilAprovec.stream().forEach((a) -> {
                String variableCabecera = "";
                variableCabecera = a.getNombreAprovechamiento();
                if (!a.getNombreAprovechamiento().equals(variableCabecera)) {
                    lstCabeceraApro.add(a.getNombreAprovechamiento());
                }
            });

            lstAccionesFilPOSTAprovec.stream().forEach((a) -> {
                String variableCabecera = "";
                variableCabecera = a.getNombreAprovechamiento();
                if (!a.getNombreAprovechamiento().equals(variableCabecera)) {
                    lstCabeceraPostApro.add(a.getNombreAprovechamiento());
                }
            });
            for (String variable : lstCabeceraPreApro) {
                System.out.println("PRE APROVECHAMIENTO: " + variable);
            }
            for (String variable2 : lstCabeceraApro) {
                System.out.println("APROVECHAMIENTO: " + variable2);
            }
            for (String variable3 : lstCabeceraPostApro) {
                System.out.println("POST APROVECHAMIENTO: " + variable3);
            }

            for (EvaluacionAmbientalActividadEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("81factor")) {
                                            text = text.replace("81factor",
                                                    detalleEntity.getNombreActividad() != null
                                                            ? detalleEntity.getNombreActividad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("81caracterizacion")) {
                                            text = text.replace("81caracterizacion",
                                                    detalleEntity.getMedidasControl() != null
                                                            ? detalleEntity.getMedidasControl()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("81medidas")) {
                                            text = text.replace("81medidas",
                                                    detalleEntity.getMedidasMonitoreo() != null
                                                            ? detalleEntity.getMedidasMonitoreo()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("81de_x")) {

                                            text = text.replace("81medidas",
                                                    detalleEntity.getMedidasMonitoreo() != null
                                                            ? detalleEntity.getMedidasMonitoreo()
                                                            : "");
                                            r.setText(text, 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarEvaluacionPFCR(List<EvaluacionPermisoForestalDto> lstLista, XWPFDocument doc) {
        try {
            // List<EvaluacionPermisoForestalDto> lstListaFil =
            // lstLista.stream().filter(eval ->
            // eval.getCodigoEvaluacion().equals("EEVPFEVAL") ||
            // eval.getCodigoEvaluacion().equals("EPSFDESF") ||
            // eval.getCodigoEvaluacion().equals("EPSFFAV")).collect(Collectors.toList());
            List<EvaluacionPermisoForestalDto> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getTipoEvaluacion().equals("EVPF") || eval.getTipoEvaluacion().equals("EEVPF"))
                    .collect(Collectors.toList());
            List<EvaluacionPermisoForestalDetalleDto> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                EvaluacionPermisoForestalDto PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getListarEvaluacionPermisoDetalle();
            }
            List<EvaluacionPermisoForestalDetalleDto> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getConforme().equals("EPSFFAV") || eval.getConforme().equals("EPSFDESF"))
                    .collect(Collectors.toList());
            lstListaDetalle2.sort(Comparator.comparing(o -> o.getDescripcion()));
            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(1);
            for (EvaluacionPermisoForestalDetalleDto detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("descrEval")) {
                                            text = text.replace("descrEval",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("detalleEval")) {
                                            text = text.replace("detalleEval",
                                                    detalleEntity.getDetalle() != null ? detalleEntity.getDetalle()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("estadoEval")) {
                                            text = text.replace("estadoEval",
                                                    detalleEntity.getConforme() != null ? detalleEntity.getConforme()
                                                            : "");
                                            if (text.equals("EPSFVAL")) {
                                                text = "Validado con éxito";
                                            } else if (text.equals("EPSFVALOB")) {
                                                text = "Observado";
                                            } else if (text.equals("EPSFFAV")) {
                                                text = "Favorable";
                                            } else if (text.equals("EPSFDESF")) {
                                                text = "Desfavorable";
                                            }
                                            r.setText(text, 0);
                                        } else if (text.contains("obserEval")) {
                                            text = text.replace("obserEval",
                                                    detalleEntity.getObservacion() != null
                                                            ? detalleEntity.getObservacion()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInformacionGeneral(InformacionGeneralDto data, XWPFDocument doc) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(0);
            Integer re = table.getRows().size() + 1;

            for (int j = 1; j < re; j++) {// 19 lineas de la tabla

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("txtNombre")) {
                                        text = text.replace("txtNombre", data.getRazonSocialTitularTH() == null ? ""
                                                : data.getRazonSocialTitularTH());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtRl")) {
                                        text = text.replace("txtRl",
                                                data.getNombreRepresentanteLegal() + " "
                                                        + data.getApellidoPaternoRepLegal() + " " +
                                                        data.getApellidoMaternoRepLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDni")) {
                                        text = text.replace("txtDni", data.getNumeroDocumentoRepLegal() == null ? ""
                                                : data.getNumeroDocumentoRepLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtRuc")) {
                                        text = text.replace("txtRuc",
                                                data.getNombreTitularTH() == null ? "" : data.getNombreTitularTH());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDom")) {
                                        text = text.replace("txtDom",
                                                data.getDomicilioLegal() == null ? "" : data.getDomicilioLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtCc")) {
                                        text = text.replace("txtCc", data.getNroContratoConcesion() == null ? ""
                                                : data.getNroContratoConcesion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDep")) {
                                        text = text.replace("txtDep", data.getNombreDepartamentoContrato() == null ? ""
                                                : data.getNombreDepartamentoContrato());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtProv")) {
                                        text = text.replace("txtProv", data.getNombreProvinciaContrato() == null ? ""
                                                : data.getNombreProvinciaContrato());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtFecha")) {
                                        text = text.replace("txtFecha",
                                                data.getFechaPresentacion() == null ? ""
                                                        : FechaUtil.obtenerFechaString(data.getFechaPresentacion(),
                                                                "dd/MM/yyyy"));
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtD")) {
                                        text = text.replace("txtD",
                                                data.getDuracion() == null ? "" : data.getDuracion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtFechI")) {
                                        text = text.replace("txtFechI", data.getFechaFin() == null ? ""
                                                : FechaUtil.obtenerFechaString(data.getFechaInicio(), "dd/MM/yyyy"));
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtFechF")) {
                                        text = text.replace("txtFechF", data.getFechaFin() == null ? ""
                                                : FechaUtil.obtenerFechaString(data.getFechaFin(), "dd/MM/yyyy"));
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtAtc")) {
                                        text = text.replace("txtAtc", data.getAreaTotalConcesion() == null ? ""
                                                : data.getAreaTotalConcesion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtAb")) {
                                        text = text.replace("txtAb", data.getAreaBosqueProduccionForestal() == null ? ""
                                                : data.getAreaBosqueProduccionForestal().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtAp")) {
                                        text = text.replace("txtAp", data.getAreaProteccion() == null ? ""
                                                : data.getAreaProteccion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtBq")) {
                                        text = text.replace("txtBq", data.getNumeroBloquesQuinquenales() == null ? ""
                                                : data.getNumeroBloquesQuinquenales());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtPm")) {
                                        text = text.replace("txtPm", data.getPotencialMaderable() == null ? ""
                                                : data.getPotencialMaderable().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtVcap")) {
                                        text = text.replace("txtVcap",
                                                data.getVolumenCortaAnualPermisible() == null ? ""
                                                        : data.getVolumenCortaAnualPermisible().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtNomR")) {
                                        text = text.replace("txtNomR", data.getNombreRegenteForestal() == null ? ""
                                                : data.getNombreRegenteForestal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDom")) {
                                        text = text.replace("txtDom", data.getDomicilioLegalRegente() == null ? ""
                                                : data.getDomicilioLegalRegente());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtCon")) {
                                        text = text.replace("txtCon",
                                                data.getContratoSuscritoTitularTituloHabilitante() == null ? ""
                                                        : data.getContratoSuscritoTitularTituloHabilitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtChprf")) {
                                        text = text.replace("txtChprf",
                                                data.getCertificadoHabilitacionProfesionalRegenteForestal() == null ? ""
                                                        : data.getCertificadoHabilitacionProfesionalRegenteForestal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtIrr")) {
                                        text = text.replace("txtIrr",
                                                data.getNumeroInscripcionRegistroRegente() == null ? ""
                                                        : data.getNumeroInscripcionRegistroRegente());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 1; j < re; j++) {
                table.removeRow(1);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearObjetivoGeneral(ObjetivoManejoEntity data, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("txtObjG")) {
                                text = text.replace("txtObjG", data.getGeneral() == null ? "" : data.getGeneral());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearObjetivoEspecifico(List<ObjetivoEspecificoManejoEntity> lista, XWPFDocument doc) {
        try {

            for (int i = 2; i < 11; i++) {// tablas de 3 a 11

                XWPFTable table = doc.getTables().get(i);
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);

                                if (text != null && text.trim().contains("txt")) {
                                    text = text.replace("txt", buscarObjetivoEspecifico(lista, i));
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.removeRow(0);
                table.addRow(copiedRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearUbicacionPolitica(List<InformacionBasicaUbigeoEntity> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(11);

            for (InformacionBasicaUbigeoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtDep")) {
                                    text = text.replace("txtDep", element.getNombreDepartamento() == null ? ""
                                            : element.getNombreDepartamento());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtPro")) {
                                    text = text.replace("txtPro",
                                            element.getNombreProvincia() == null ? "" : element.getNombreProvincia());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtDis")) {
                                    text = text.replace("txtDis",
                                            element.getNombreDistrito() == null ? "" : element.getNombreDistrito());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtCue")) {
                                    text = text.replace("txtCue",
                                            element.getCuenca() == null ? "" : element.getCuenca().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearCoordenadasUTM(List<InfBasicaAereaDetalleDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(12);

            for (InfBasicaAereaDetalleDto element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtPun")) {
                                    text = text.replace("txtPun",
                                            element.getPuntoVertice() == null ? "" : element.getPuntoVertice());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txEs")) {
                                    text = text.replace("txEs", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txNor")) {
                                    text = text.replace("txNor", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txRef")) {
                                    text = text.replace("txRef",
                                            element.getReferencia() == null ? "" : element.getReferencia());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInfoBasicDet(List<InfBasicaAereaDetalleDto> lista, XWPFDocument doc,
            Integer intTable, Integer initRow) {
        try {
            XWPFTable table = doc.getTables().get(intTable);

            for (InfBasicaAereaDetalleDto element : lista) {

                XWPFTableRow row = table.getRow(initRow);

                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("txtPr")) {
                                    text = text.replace("txtPr",
                                            element.getReferencia() == null ? "" : element.getReferencia());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtEi")) {
                                    text = text.replace("txtEi", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtNi")) {
                                    text = text.replace("txtNi", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtEf")) {
                                    text = text.replace("txtEf", element.getCoordenadaEsteFin() == null ? ""
                                            : element.getCoordenadaEsteFin().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtNf")) {
                                    text = text.replace("txtNf", element.getCoordenadaNorteFin() == null ? ""
                                            : element.getCoordenadaNorteFin().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtDis")) {
                                    text = text.replace("txtDis", element.getDistanciaKm() == null ? ""
                                            : element.getDistanciaKm().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtTi")) {
                                    text = text.replace("txtTi",
                                            element.getTiempo() == null ? "" : element.getTiempo().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtMed")) {
                                    text = text.replace("txtMed",
                                            element.getMedioTransporte() == null ? "" : element.getMedioTransporte());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txt")) {
                                    text = text.replace("txt",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtR")) {
                                    text = text.replace("txtR",
                                            element.getNombreRio() == null ? "" : element.getNombreRio());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtQ")) {
                                    text = text.replace("txtQ",
                                            element.getNombreQuebrada() == null ? "" : element.getNombreQuebrada());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtL")) {
                                    text = text.replace("txtL",
                                            element.getNombreLaguna() == null ? "" : element.getNombreLaguna());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(initRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearAspectosFisicosTab2(List<UnidadFisiograficaUMFEntity> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(16);

            for (UnidadFisiograficaUMFEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("1u")) {
                                    text = text.replace("1u",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("2u")) {
                                    text = text.replace("2u",
                                            element.getAccion() == null ? "" : element.getAccion() ? "X" : "");
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("3u")) {
                                    text = text.replace("3u",
                                            element.getArea() == null ? "" : element.getArea().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("4u")) {
                                    text = text.replace("4u",
                                            element.getPorcentaje() == null ? "" : element.getPorcentaje().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    private static String buscarObjetivoEspecifico(List<ObjetivoEspecificoManejoEntity> lista, Integer cuadro) {

        String marcar = "";

        for (ObjetivoEspecificoManejoEntity objetivoEspecificoManejoEntity : lista) {
            if (objetivoEspecificoManejoEntity.getIdTipoObjEspecifico() != null
                    && objetivoEspecificoManejoEntity.getIdTipoObjEspecifico().equals(String.valueOf(cuadro))) {
                marcar = "X";
                break;
            }
        }

        return marcar;
    }

    public static XWPFDocument setearInfoAnexo4(List<PGMFArchivoEntity> lista, XWPFDocument doc) {
        try {
            Boolean check = lista == null ? false : lista.isEmpty() ? false : true;

            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.trim().contains("txtA4")) {
                            text = text.replace("txtA4", check ? "X" : "");
                            r.setText(text, 0);
                        } else if (text != null && text.trim().contains("txtAn4")) {
                            text = text.replace("txtAn4", check ? "" : "X");
                            r.setText(text, 0);
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearFaunaSilvestre(List<FaunaEntity> listaFauna, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(19);

            for (FaunaEntity element : listaFauna) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {
                                    text = text.replace("n1", element.getNombre() == null ? "" : element.getNombre());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n2")) {
                                    text = text.replace("n2",
                                            element.getNombreCientifico() == null ? "" : element.getNombreCientifico());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("st")) {
                                    text = text.replace("st", element.getEstatus() == null ? "" : element.getEstatus());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearAspectosSocioEconomicos(List<InfBasicaAereaDetalleDto> listaAspecto,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(21);

            for (InfBasicaAereaDetalleDto element : listaAspecto) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("1")) {

                                    text = text.replace("1",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("2")) {

                                    text = text.replace("2", element.getNombre() == null ? "" : element.getNombre());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("3")) {

                                    text = text.replace("3", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("4")) {

                                    text = text.replace("4", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("5")) {

                                    text = text.replace("5",
                                            element.getAreaHa() == null ? "" : element.getAreaHa().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("6")) {

                                    text = text.replace("6", element.getNumeroFamilia() == null ? ""
                                            : element.getNumeroFamilia().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("7")) {

                                    text = text.replace("7", element.getSubsistencia() == null ? "( )"
                                            : !element.getSubsistencia() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n8")) {

                                    text = text.replace("n8", element.getPerenne() == null ? "( )"
                                            : !element.getPerenne() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n9")) {

                                    text = text.replace("n9", element.getGanaderia() == null ? "( )"
                                            : !element.getGanaderia() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n10")) {

                                    text = text.replace("n10",
                                            element.getCaza() == null ? "( )" : !element.getCaza() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n11")) {

                                    text = text.replace("n11",
                                            element.getPesca() == null ? "( )" : !element.getPesca() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n12")) {

                                    text = text.replace("n12",
                                            element.getMadera() == null ? "( )" : !element.getMadera() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n13")) {

                                    text = text.replace("n13", element.getOtroProducto() == null ? "( )"
                                            : !element.getOtroProducto() ? "( )" : "(X)");
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearAspectosSocioEconomicos2(List<InfBasicaAereaDetalleDto> listaAspecto,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(22);

            for (InfBasicaAereaDetalleDto element : listaAspecto) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("1")) {

                                    text = text.replace("1",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("2")) {

                                    text = text.replace("2", element.getNombre() == null ? "" : element.getNombre());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("3")) {

                                    text = text.replace("3", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("4")) {

                                    text = text.replace("4", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("5")) {

                                    text = text.replace("5",
                                            element.getAcceso() == null ? "" : element.getAcceso().toString());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaAntecedentes1(List<InfBasicaAereaDetalleDto> listaAnt, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(23);

            for (InfBasicaAereaDetalleDto element : listaAnt) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2",
                                            element.getMarcar() == null ? "" : element.getMarcar().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3",
                                            element.getEspecieExtraida() == null ? "" : element.getEspecieExtraida());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4",
                                            element.getObservaciones() == null ? "" : element.getObservaciones());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaAntecedentes2(List<InfBasicaAereaDetalleDto> listaAnt2, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(24);

            for (InfBasicaAereaDetalleDto element : listaAnt2) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {

                                    text = text.replace("s1",
                                            element.getConflicto() == null ? "" : element.getConflicto());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("s2")) {

                                    text = text.replace("s2",
                                            element.getSolucion() == null ? "" : element.getSolucion());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearDescripcionSistemaMonitoreo(String data, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("descr")) {
                                text = text.replace("descr", data == null ? "" : data);
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearDescripcionAccionMonitoreo(List<MonitoreoEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (MonitoreoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    primero: for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("mn")) {

                                    text = text.replace("mn",
                                            element.getMonitoreo() == null ? "" : element.getMonitoreo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("oper")) {
                                    XWPFParagraph lnewPara = cell.addParagraph();

                                    String dato = "";
                                    for (MonitoreoDetalleEntity i : element.getLstDetalle()) {
                                        dato = dato + i.getOperacion() + "\n";
                                    }
                                    text = text.replace("oper", "");
                                    r.setText(text, 0);

                                    XWPFRun lnewRun = lnewPara.createRun();
                                    if (dato.contains("\n")) {
                                        String[] lines = dato.split("\n");
                                        lnewRun.setText(lines[0], 0);
                                        for (int i = 1; i < lines.length; i++) {
                                            lnewRun.addBreak();
                                            lnewRun.setText(lines[i]);
                                        }
                                    } else {
                                        lnewRun.setText(dato, 0);
                                    }
                                    break primero;

                                } else if (text != null && text.trim().contains("descr")) {

                                    text = text.replace("descr",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument addColumnFlujoCaja(List<Integer> lastColumn, XWPFDocument doc, Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            XWPFTableRow row2 = table.createRow();
            row2.getCell(0).setText("rub");
            int i = 1;
            for (Integer e : lastColumn) {
                table = doc.getTables().get(numTable);
                XWPFTableRow row = table.getRow(0);
                row.addNewTableCell().setText("Año" + e);

                row2.createCell();
                row2.getCell(i).setText(e + "");

                i++;
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearFlujoCaja(List<RentabilidadManejoForestalDto> lista, XWPFDocument doc,
            Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            for (RentabilidadManejoForestalDto element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("rub")) {
                                    text = text.replace("rub", element.getRubro());
                                    r.setText(text, 0);
                                } else if (text != null && !text.trim().contains("rub")) {
                                    String replace = "";

                                    loop: for (RentabilidadManejoForestalDetalleEntity e : element
                                            .getListRentabilidadManejoForestalDetalle()) {
                                        if (text.trim().equals(e.getAnio().toString())) {
                                            replace = e.getMonto().toString();
                                            break loop;
                                        }
                                    }
                                    r.setText(replace, 0);
                                }

                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListarNecesidades(List<RentabilidadManejoForestalDto> lista, XWPFDocument doc,
            Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                primero: for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("n1")) {
                                String dato = "";
                                XWPFParagraph lnewPara = cell.addParagraph();

                                for (RentabilidadManejoForestalDto element : lista) {
                                    dato = dato + element.getDescripcion() + "\n";
                                }
                                text = text.replace("n1", "");
                                r.setText(text, 0);

                                XWPFRun lnewRun = lnewPara.createRun();
                                if (dato.contains("\n")) {
                                    String[] lines = dato.split("\n");
                                    lnewRun.setText(lines[0], 0);
                                    for (int i = 1; i < lines.length; i++) {
                                        lnewRun.addBreak();
                                        lnewRun.setText(lines[i]);
                                    }
                                } else {
                                    lnewRun.setText(dato, 0);
                                }
                                break primero;
                            }
                        }
                    }
                }

            }
            table.addRow(copiedRow);
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearFomulacionPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtFoPMF")) {
                                    text = text.replace("txtFoPMF",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearImplementacionPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtImPMF")) {
                                    text = text.replace("txtImPMF",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearComitesPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtGestBosq")) {
                                    text = text.replace("txtGestBosq",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearReconicimientoPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<ParticipacionComunalDetEntity>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtMecPar")) {
                                    text = text.replace("txtMecPar",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtMetod")) {
                                    text = text.replace("txtMetod",
                                            element.getMetodologia() == null ? "" : element.getMetodologia());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtLug")) {

                                    String res = element.getLugar() == null ? "" : element.getLugar() + " ";
                                    String res2 = element.getFecha() == null ? "" : element.getFecha().toString();
                                    r.setText(res + res2, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearAnexoMapa(ResultEntity<PGMFArchivoEntity> archivo, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("imagenMapa")) {
                                text = text.replace("imagenMapa", "");
                                r.setText(text, 0);
                                if (archivo.getData().size() > 0) {
                                    PGMFArchivoEntity imagen = archivo.getData().get(0);
                                    if (imagen.getDocumento() != null) {

                                        InputStream is = new ByteArrayInputStream(imagen.getDocumento());
                                        r.addBreak();
                                        r.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, "Anexo Mapa", Units.toEMU(300),
                                                Units.toEMU(300));
                                        is.close();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearCronogramaAct(List<CronogramaActividadEntity> lista, XWPFDocument doc,
            Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            for (CronogramaActividadEntity element : lista) {

                XWPFTableRow row = table.getRow(6);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("act")) {
                                    text = text.replace("act", element.getActividad());
                                    r.setText(text, 0);
                                } else if (text != null && !text.trim().contains("act")) {
                                    text = buscarCronogramaAct(element.getDetalle(), text);
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(6);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private static String buscarCronogramaAct(List<CronogramaActividadDetalleEntity> detalle, String codigo) {

        String data = "";

        for (CronogramaActividadDetalleEntity e : detalle) {
            if (codigo.equals("1") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(1)) {
                data = "X";
                break;
            } else if (codigo.equals("2") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(2)) {
                data = "X";
                break;
            } else if (codigo.equals("3") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(3)) {
                data = "X";
                break;
            } else if (codigo.equals("4") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(4)) {
                data = "X";
                break;
            } else if (codigo.equals("5") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(5)) {
                data = "X";
                break;
            } else if (codigo.equals("6") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(6)) {
                data = "X";
                break;
            } else if (codigo.equals("7") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(7)) {
                data = "X";
                break;
            } else if (codigo.equals("8") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(8)) {
                data = "X";
                break;
            } else if (codigo.equals("9") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(9)) {
                data = "X";
                break;
            } else if (codigo.equals("10") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(10)) {
                data = "X";
                break;
            } else if (codigo.equals("11") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(11)) {
                data = "X";
                break;
            } else if (codigo.equals("12") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(12)) {
                data = "X";
                break;
            } else if (codigo.equals("a") && e.getAnio() != null && e.getAnio().equals(2)) {
                data = "X";
                break;
            } else if (codigo.equals("b") && e.getAnio() != null && e.getAnio().equals(3)) {
                data = "X";
                break;
            } else if (codigo.equals("c") && e.getAnio() != null && e.getAnio().equals(4)) {
                data = "X";
                break;
            } else if (codigo.equals("d") && e.getAnio() != null && e.getAnio().equals(5)) {
                data = "X";
                break;
            } else if (codigo.equals("e") && e.getAnio() != null && e.getAnio().equals(6)) {
                data = "X";
                break;
            } else if (codigo.equals("f") && e.getAnio() != null && e.getAnio().equals(7)) {
                data = "X";
                break;
            } else if (codigo.equals("g") && e.getAnio() != null && e.getAnio().equals(8)) {
                data = "X";
                break;
            } else if (codigo.equals("h") && e.getAnio() != null && e.getAnio().equals(9)) {
                data = "X";
                break;
            } else if (codigo.equals("i") && e.getAnio() != null && e.getAnio().equals(10)) {
                data = "X";
                break;
            } else if (codigo.equals("j") && e.getAnio() != null && e.getAnio().equals(11)) {
                data = "X";
                break;
            } else if (codigo.equals("k") && e.getAnio() != null && e.getAnio().equals(12)) {
                data = "X";
                break;
            } else if (codigo.equals("l") && e.getAnio() != null && e.getAnio().equals(13)) {
                data = "X";
                break;
            } else if (codigo.equals("m") && e.getAnio() != null && e.getAnio().equals(14)) {
                data = "X";
                break;
            } else if (codigo.equals("n") && e.getAnio() != null && e.getAnio().equals(15)) {
                data = "X";
                break;
            } else if (codigo.equals("o") && e.getAnio() != null && e.getAnio().equals(16)) {
                data = "X";
                break;
            } else if (codigo.equals("p") && e.getAnio() != null && e.getAnio().equals(17)) {
                data = "X";
                break;
            } else if (codigo.equals("q") && e.getAnio() != null && e.getAnio().equals(18)) {
                data = "X";
                break;
            } else if (codigo.equals("r") && e.getAnio() != null && e.getAnio().equals(19)) {
                data = "X";
                break;
            } else if (codigo.equals("s") && e.getAnio() != null && e.getAnio().equals(20)) {
                data = "X";
                break;
            }

        }

        return data;

    }

    public static XWPFDocument setearObjetivosCapacitacionTab10(List<CapacitacionDto> lista, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (CapacitacionDto element : lista) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("obj")) {
                                    text = text.replace("obj", element.getTema() == null ? "" : element.getTema());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearActividadesPlanUMFTab10(List<CapacitacionDto> lista, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (CapacitacionDto element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("act1")) {
                                    text = text.replace("act1", element.getTema() == null ? "" : element.getTema());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("act2")) {
                                    text = text.replace("act2", element.getPersonaCapacitar() == null ? ""
                                            : element.getPersonaCapacitar().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("act3")) {
                                    text = text.replace("act3", element.getIdTipoModalidad() == null ? ""
                                            : element.getIdTipoModalidad().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("act4")) {
                                    text = text.replace("act4",
                                            element.getLugar() == null ? "" : element.getLugar().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaFuncionesTab1(List<OrganizacionManejoEntity> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrganizacionManejoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    primero: for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a",
                                            element.getNombreSubTipo() == null ? "" : element.getNombreSubTipo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {
                                    XWPFParagraph lnewPara = cell.addParagraph();

                                    String dato = "";
                                    for (OrganizacionManejoEntity i : element.getListaDetalle()) {
                                        dato = dato + i.getFuncion() + "\n";
                                    }
                                    text = text.replace("b", "");
                                    r.setText(text, 0);

                                    XWPFRun lnewRun = lnewPara.createRun();
                                    if (dato.contains("\n")) {
                                        String[] lines = dato.split("\n");
                                        lnewRun.setText(lines[0], 0);
                                        for (int i = 1; i < lines.length; i++) {
                                            lnewRun.addBreak();
                                            lnewRun.setText(lines[i]);
                                        }
                                    } else {
                                        lnewRun.setText(dato, 0);
                                    }
                                    break primero;

                                } else if (text != null && text.trim().contains("c")) {
                                    String descripcion = "";
                                    if (element.getListaDetalle() != null && !element.getListaDetalle().isEmpty()) {
                                        descripcion = element.getListaDetalle().get(0).getDescripcion();
                                    }

                                    text = text.replace("c", descripcion);
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("d")) {
                                    String numero = "";
                                    if (element.getListaDetalle() != null && !element.getListaDetalle().isEmpty()) {
                                        numero = element.getListaDetalle().get(0).getNumero().toString();
                                    }

                                    text = text.replace("d", numero);
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaFuncionesTab2(List<OrganizacionManejoEntity> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrganizacionManejoEntity element : lista) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("e")) {
                                    text = text.replace("e",
                                            element.getTipoActividad() == null ? "" : element.getTipoActividad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("f")) {
                                    text = text.replace("f",
                                            element.getFuncion() == null ? "" : element.getFuncion().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("g")) {
                                    text = text.replace("g",
                                            element.getNumero() == null ? "" : element.getNumero().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaFuncionesTab3(List<OrganizacionManejoEntity> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrganizacionManejoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("h")) {
                                    text = text.replace("h",
                                            element.getTipoActividad() == null ? "" : element.getTipoActividad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("i")) {
                                    text = text.replace("i", element.getMaquinaEquipo() == null ? ""
                                            : element.getMaquinaEquipo().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("j")) {
                                    text = text.replace("j",
                                            element.getNumero() == null ? "" : element.getNumero().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarResumenActividadPOAC(List<ResumenActividadPoEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            System.out.println(lstLista.size());
            for (ResumenActividadPoEntity detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (detalleEntity.getCodigoSubResumen().equals("POACRA")) {
                                        if (text5 != null) {
                                            if (text5.contains("2NroResolucion")) {
                                                text5 = text5.replace("2NroResolucion", "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("2NroPc")) {
                                                text5 = text5.replace("2NroPc",
                                                        detalleEntity.getNroPcs() != null
                                                                ? detalleEntity.getNroPcs().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("2Area")) {
                                                text5 = text5.replace("2Area",
                                                        detalleEntity.getAreaHa() != null
                                                                ? detalleEntity.getAreaHa().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */
    public static XWPFDocument generarResumenActividadFrenteCortaPOAC(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {

        try {

            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(1);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("2actividad")) {
                                            text = text.replace("2actividad",
                                                    detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("2indicador")) {
                                            text = text.replace("2indicador",
                                                    detalleEntity.getIndicador() != null ? detalleEntity.getIndicador()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("2programado")) {
                                            text = text.replace("2programado",
                                                    detalleEntity.getProgramado() != null
                                                            ? detalleEntity.getProgramado()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("2realizado")) {
                                            text = text.replace("2realizado",
                                                    detalleEntity.getRealizado() != null ? detalleEntity.getRealizado()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOACPos(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getTipoResumen() == 1).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(3);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("positivos")) {
                                            text = text.replace("positivos",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOACNeg(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getTipoResumen() == 2).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(4);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("negativos")) {
                                            text = text.replace("negativos",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOACRec(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getTipoResumen() == 3).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(5);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("recomendaciones")) {
                                            text = text.replace("recomendaciones",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void procesarObjetivosPOAC(List<ObjetivoDto> lstLista, XWPFDocument doc) {
        try {

            // objetivos especificos
            XWPFTable table = doc.getTables().get(6);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            List<ObjetivoDto> maderables = lstLista.stream().filter(m -> m.getDetalle().equals("Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> noMaderables = lstLista.stream().filter(n -> n.getDetalle().equals("No Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> lstOrdenada = new ArrayList<>();
            lstOrdenada.addAll(maderables);
            lstOrdenada.addAll(noMaderables);
            int i = 0;
            for (ObjetivoDto detalleEntity : lstOrdenada) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("31tipo")) {
                                        String detalle = "";
                                        if (i == 0)
                                            detalle = "Maderable";
                                        if (i == maderables.size())
                                            detalle = "No Maderable";
                                        text5 = text5.replace("31tipo", detalle);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("31E")) {
                                        String check;
                                        if (detalleEntity.getActivo().toString().equals("A")) {
                                            check = "(X)";
                                        } else {
                                            check = "( )";
                                        }
                                        text5 = text5.replace("31E", check);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("31producto")) {
                                        text5 = text5.replace("31producto", detalleEntity.getDescripcionDetalle() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */
    public static XWPFDocument generarLaboresSilviculturalesPOAC(ActividadSilviculturalDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(23);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("laboresx")) {
                                        text = text.replace("laboresx",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("marcax")) {
                                        if (lstLista.getDetalle().get(i).getAccion()) {
                                            text = text.replace("marcax", "X");
                                        } else {
                                            text = text.replace("marcax", "");
                                        }
                                        r.setText(text, 0);
                                    } else if (text.contains("descripcionx")) {
                                        text = text.replace("descripcionx",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument generarActividadSilviculturalAplicacionPOAC(ActividadSilviculturalDto lstLista,
            XWPFDocument doc) throws IOException {
        try {

            XWPFTable table = doc.getTables().get(23);//

            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("6tratamiento")) {
                                        text = text.replace("6tratamiento",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("6esta")) {
                                        if (lstLista.getDetalle().get(i).getAccion()) {
                                            text = text.replace("6esta", "X");
                                        } else {
                                            text = text.replace("6esta", "");
                                        }
                                        r.setText(text, 0);
                                    } else if (text.contains("6descripcion")) {
                                        text = text.replace("6descripcion",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarActividadSilviculturalReforestacionPOAC(ActividadSilviculturalDto lstLista,
            XWPFDocument doc) throws IOException {
        try {

            XWPFTable table = doc.getTables().get(24);//

            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("6reforestacion")) {
                                        text = text.replace("6reforestacion",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("6descripcionB")) {
                                        text = text.replace("6descripcionB",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarProteccionBosquePOAC(List<ProteccionBosqueDetalleDto> lstLista, XWPFDocument doc,String codigo) {
        try {
            // 7.1 Demarcación y Mantenimiento de Linderos
            XWPFTable table = doc.getTables().get(25);
            XWPFTableRow row = table.getRow(1);
            int i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (codigo.equals("POACPBDML")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("71sistema")) {
                                            text5 = text5.replace("71sistema", detalleEntity.getTipoMarcacion());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71accion")) {
                                            text5 = text5.replace("71accion",
                                                    detalleEntity.getNuAccion() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71modo")) {
                                            text5 = text5.replace("71modo", detalleEntity.getImplementacion());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

            // 7.2.1. Análisis de Impacto Ambiental
            table = doc.getTables().get(26);
            row = table.getRow(2);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (codigo.equals("POACPBAIA")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {

                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("721medio")) {
                                            text5 = text5.replace("721medio", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        }else if (text5.contains("71sistema")) {
                                            text5 = text5.replace("71sistema", detalleEntity.getFactorAmbiental());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71accion")) {
                                            text5 = text5.replace("71accion", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71c")) {
                                            text5 = text5.replace("71c",
                                                    detalleEntity.getNuCenso() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71dema")) {
                                            text5 = text5.replace("71dema",
                                                    detalleEntity.getNuDemarcacionLineal() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71constcamp")) {
                                            text5 = text5.replace("71constcamp",
                                                    detalleEntity.getNuConstruccionCampamento() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71constcam")) {
                                            text5 = text5.replace("71constcam",
                                                    detalleEntity.getNuConstruccionCamino() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71t")) {
                                            text5 = text5.replace("71t",
                                                    detalleEntity.getNuTala() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71a")) {
                                            text5 = text5.replace("71a",
                                                    detalleEntity.getNuArrastre() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71o")) {
                                            text5 = text5.replace("71o",
                                                    detalleEntity.getNuOtra() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 2 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            // 7.2.2. Plan de gestión ambiental
            // Programa preventivo - corrector
            table = doc.getTables().get(27);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACPBPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("POACPBPGAPPC")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("722actividad")) {
                                            text5 = text5.replace("722actividad", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722descripcion")) {
                                            text5 = text5.replace("722descripcion", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722medidas")) {
                                            text5 = text5.replace("722medidas", detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            //  Programa de Vigilancia y Seguimiento
            table = doc.getTables().get(28);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACPBPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("POACPBPGAPVS")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("722actividad2")) {
                                            text5 = text5.replace("722actividad2", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722descripcion2")) {
                                            text5 = text5.replace("722descripcion2", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722medidasAmbiental")) {
                                            text5 = text5.replace("722medidasAmbiental",
                                                    detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            //  Programa de contingencia ambiental:
            table = doc.getTables().get(29);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACPBPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("POACPBPGAPCA")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("722contin")) {
                                            text5 = text5.replace("722contin", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722descripcion3")) {
                                            text5 = text5.replace("722descripcion3", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722medidas3")) {
                                            text5 = text5.replace("722medidas3",
                                                    detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarMonitoreoPOAC(List<MonitoreoDetalleEntity> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(30);
            XWPFTableRow row = table.getRow(1);
            int i = 0;
            for (MonitoreoDetalleEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("8actividades")) {
                                        text5 = text5.replace("8actividades", detalleEntity.getActividad());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("8descripcion")) {
                                        text5 = text5.replace("8descripcion", detalleEntity.getDescripcion());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("8responsable")) {
                                        text5 = text5.replace("8responsable", detalleEntity.getResponsable());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(table.getRows().size() - 1);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarParticipacionComunalPOAC(List<ParticipacionComunalEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(31);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ParticipacionComunalEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("91actividades")) {
                                        text5 = text5.replace("91actividades",
                                                detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("91responsable")) {
                                        text5 = text5.replace("91responsable",
                                                detalleEntity.getResponsable() != null ? detalleEntity.getResponsable()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("91seguimiento")) {
                                        text5 = text5.replace("91seguimiento",
                                                detalleEntity.getSeguimientoActividad() != null
                                                        ? detalleEntity.getSeguimientoActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarActividadSilviculturalOrganizPOAC(ActividadSilviculturalDto lstLista,
            XWPFDocument doc) throws IOException {
        try {
            XWPFTable table = doc.getTables().get(32);//
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("11actividad")) {
                                        text = text.replace("11actividad",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("11forma")) {
                                        text = text.replace("11forma",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarTotalesProgramaInversionPOAC(Double[] totales, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(33);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            XWPFTableRow row3 = table.getRow(3);

            // total ingresos
            XWPFTableRow copiedRowTotalIngr = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            CTTrPr trPrIngr = copiedRowTotalIngr.getCtRow().addNewTrPr();
            CTHeight htIngr = trPrIngr.addNewTrHeight();
            htIngr.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalIngr.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12INI")) {
                                text5 = text5.replace("12INI", totales[0] != null ? totales[0].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalIngr, 1);
            table.removeRow(2);
            // total egresos
            XWPFTableRow copiedRowTotalEgre = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPrEgre = copiedRowTotalEgre.getCtRow().addNewTrPr();
            CTHeight htEgre = trPrEgre.addNewTrHeight();
            htEgre.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalEgre.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12INE")) {
                                text5 = text5.replace("12INE", totales[1] != null ? totales[1].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalEgre, 2);
            table.removeRow(3);
            // total neto
            XWPFTableRow copiedRowTotalNeto = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table);
            CTTrPr trPrNeto = copiedRowTotalNeto.getCtRow().addNewTrPr();
            CTHeight htNeto = trPrNeto.addNewTrHeight();
            htNeto.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalNeto.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12BN")) {
                                Double totalNeto = (totales[0] != null ? totales[0] : 0.00)
                                        - (totales[1] != null ? totales[1] : 0.00);
                                text5 = text5.replace("12BN", totalNeto != null ? totalNeto.toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalNeto, 3);
            table.removeRow(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */
    public static void generarIngresoProgramaInversionPOAC(List<RentabilidadManejoForestalDto> lstIngresos,
            XWPFDocument doc, Double[] total) {
        try {
            total[0] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstIngresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct().collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(34);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("121INGRMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("121INGRTOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto ingreso : lstIngresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("121INGRRUB")) {
                                        text5 = text5.replace("121INGRRUB",
                                                ingreso.getRubro() != null ? ingreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("INGR121DESC")) {
                                        text5 = text5.replace("INGR121DESC",
                                                ingreso.getDescripcion() != null ? ingreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121INGRMONTOT")) {
                                        text5 = text5.replace("121INGRMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[0] += totalFila;
                                    } else if (text5.contains("121INGRMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : ingreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("121INGRMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("121INGRMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("121INGRMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstIngresos.size());

            // totales de ingresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("121INGRTOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("121INGRTOTALA" + integer.toString())) {
                                                text5 = text5.replace("121INGRTOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("121INGRTOTALT")) {
                                    text5 = text5.replace("121INGRTOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstIngresos.size());
            table.removeRow(2 + lstIngresos.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarEgresoProgramaInversionPOAC(List<RentabilidadManejoForestalDto> lstEgresos,
            XWPFDocument doc, Double[] total) {
        try {
            total[1] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstEgresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(35);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("121EGREMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("121EGRETOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto egreso : lstEgresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("121EGRERUB")) {
                                        text5 = text5.replace("121EGRERUB",
                                                egreso.getRubro() != null ? egreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121EGREDESC")) {
                                        text5 = text5.replace("121EGREDESC",
                                                egreso.getDescripcion() != null ? egreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121EGREMONTOT")) {
                                        text5 = text5.replace("121EGREMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[1] += totalFila;
                                    } else if (text5.contains("121EGREMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : egreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("121EGREMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("121EGREMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("121EGREMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstEgresos.size());

            // totales de egresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("121EGRETOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("121EGRETOTALA" + integer.toString())) {
                                                text5 = text5.replace("121EGRETOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("121EGRETOTALT")) {
                                    text5 = text5.replace("121EGRETOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstEgresos.size());
            table.removeRow(2 + lstEgresos.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */


        public static XWPFDocument generarCronogramaActividadesPOAC(List< CronogramaActividadEntity > lstLista, XWPFDocument doc) {
            try {


                XWPFTable table = doc.getTables().get(36);
                XWPFTableRow row = table.getRow(1);
                for (CronogramaActividadEntity detalleEntity : lstLista) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null) {
                                        if (text != null) {
                                            if (text.contains("13actividad")) {
                                                text = text.replace("13actividad",
                                                        detalleEntity.getActividad() != null
                                                                ? detalleEntity.getActividad()
                                                                : "");
                                                r.setText(text, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow);
                }
                table.removeRow(1);
                return doc;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return null;
            }

        }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarAspectosCompPOAC(List<InformacionGeneralDEMAEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(37);
            System.out.println(lstLista.size());
            for (InformacionGeneralDEMAEntity detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("14ASPECTOS")) {
                                            text5 = text5.replace("14ASPECTOS",
                                                    detalleEntity.getDetalle() != null ? detalleEntity.getDetalle()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static XWPFDocument setearListaActividadesTab2(
            List<EvaluacionAmbientalAprovechamientoEntity> listaActividades, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalAprovechamientoEntity element : listaActividades) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getNombreAprovechamiento() == null ? ""
                                            : element.getNombreAprovechamiento());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b", element.getImpacto() == null ? "" : element.getImpacto());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c",
                                            element.getMedidasControl() == null ? "" : element.getMedidasControl());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaActividadesTab3(
            List<EvaluacionAmbientalAprovechamientoEntity> listaActividades, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalAprovechamientoEntity element : listaActividades) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getImpacto() == null ? "" : element.getImpacto());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b",
                                            element.getMedidasControl() == null ? "" : element.getMedidasControl());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c",
                                            element.getMedidasMonitoreo() == null ? "" : element.getMedidasMonitoreo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("d")) {

                                    text = text.replace("d",
                                            element.getFrecuencia() == null ? "" : element.getFrecuencia());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("e")) {

                                    text = text.replace("e",
                                            element.getResponsable() == null ? "" : element.getResponsable());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaActividadesTab4(
            List<EvaluacionAmbientalAprovechamientoEntity> listaActividades, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalAprovechamientoEntity element : listaActividades) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s")) {

                                    text = text.replace("s",
                                            element.getContingencia() == null ? "" : element.getContingencia());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("t")) {

                                    text = text.replace("t",
                                            element.getAcciones() == null ? "" : element.getAcciones());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("u")) {

                                    text = text.replace("u",
                                            element.getResponsable() == null ? "" : element.getResponsable());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaProduccionTab1(PotencialProduccionForestalEntity data, XWPFDocument doc,
            Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);
            Integer re = table.getRows().size() + 1;

            for (int j = 0; j < re; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().equals("a")) {
                                        text = text.replace("a", data.getDisenio() == null ? ""
                                                : data.getDisenio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("b")) {
                                        text = text.replace("b", data.getDiametroMinimoInventariadaCM() == null ? ""
                                                : data.getDiametroMinimoInventariadaCM().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("c")) {
                                        text = text.replace("c", data.getIntensidadMuestreoPorcentaje() == null ? ""
                                                : data.getIntensidadMuestreoPorcentaje().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("d")) {
                                        text = text.replace("d", data.getTamanioParcela() == null ? ""
                                                : data.getTamanioParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("e")) {
                                        text = text.replace("e", data.getDistanciaParcela() == null ? ""
                                                : data.getDistanciaParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("f")) {
                                        text = text.replace("f", data.getNroParcela() == null ? ""
                                                : data.getNroParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("g")) {
                                        text = text.replace("g", data.getTotalAreaInventariada() == null ? ""
                                                : data.getTotalAreaInventariada().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("h")) {
                                        text = text.replace("h", data.getMetodoMuestreo() == null ? ""
                                                : data.getMetodoMuestreo());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 1; j < re; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaProduccionTab2(PotencialProduccionForestalEntity data, XWPFDocument doc,
            Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);
            Integer re = table.getRows().size() + 1;

            for (int j = 0; j < re; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().equals("a")) {
                                        text = text.replace("a", data.getRangoDiametroCM() == null ? ""
                                                : data.getRangoDiametroCM().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("b")) {
                                        text = text.replace("b", data.getIntensidadMuestreoPorcentaje() == null ? ""
                                                : data.getIntensidadMuestreoPorcentaje().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("c")) {
                                        text = text.replace("c", data.getTamanioParcela() == null ? ""
                                                : data.getTamanioParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("d")) {
                                        text = text.replace("d", data.getAreaMuestreada() == null ? ""
                                                : data.getAreaMuestreada().toString());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 1; j < re; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearCabeceraImpactoAmb(
            List<EvaluacionAmbientalAprovechamientoEntity> listaPreApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaPosApro,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            XWPFTableRow row = table.getRow(3);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {

                            String text = r.getText(0);
                            if (text != null && text.trim().contains("a1")) {
                                text = text.replace("a1", getColumn(listaPreApro, 0));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a2")) {
                                text = text.replace("a2", getColumn(listaPreApro, 1));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a3")) {
                                text = text.replace("a3", getColumn(listaPreApro, 2));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a4")) {
                                text = text.replace("a4", getColumn(listaPreApro, 3));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a5")) {
                                text = text.replace("a5", getColumn(listaPreApro, 4));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a6")) {
                                text = text.replace("a6", getColumn(listaPreApro, 5));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b1")) {
                                text = text.replace("b1", getColumn(listaApro, 0));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b2")) {
                                text = text.replace("b2", getColumn(listaApro, 1));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b3")) {
                                text = text.replace("b3", getColumn(listaApro, 2));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b4")) {
                                text = text.replace("b4", getColumn(listaApro, 3));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b5")) {
                                text = text.replace("b5", getColumn(listaApro, 4));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b6")) {
                                text = text.replace("b6", getColumn(listaApro, 5));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c1")) {
                                text = text.replace("c1", getColumn(listaPosApro, 0));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c2")) {
                                text = text.replace("c2", getColumn(listaPosApro, 1));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c3")) {
                                text = text.replace("c3", getColumn(listaPosApro, 2));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c4")) {
                                text = text.replace("c4", getColumn(listaPosApro, 3));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c5")) {
                                text = text.replace("c5", getColumn(listaPosApro, 4));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c6")) {
                                text = text.replace("c6", getColumn(listaPosApro, 5));
                                r.setText(text, 0);
                            }
                        }
                    }
                }

            }

            table.removeRow(3);
            table.addRow(copiedRow, 3);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearDataImpactoAmbiental(
            List<EvaluacionAmbientalActividadEntity> lista,
            List<EvaluacionAmbientalAprovechamientoEntity> listaPreApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaPosApro,
            List<EvaluacionAmbientalDto> listaRespuesta,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalActividadEntity e : lista) {

                XWPFTableRow row = table.getRow(4);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String col = "";
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("a")) {
                                    text = text.replace("a",
                                            e.getNombreActividad() == null ? "" : e.getNombreActividad());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("b")) {
                                    text = text.replace("b",
                                            e.getMedidasControl() == null ? "" : e.getMedidasControl());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("n")) {
                                    text = text.replace("n",
                                            e.getMedidasMonitoreo() == null ? "" : e.getMedidasMonitoreo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("a1")) {
                                    col = getColumn(listaPreApro, 0);
                                    text = text.replace("a1", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a2")) {
                                    col = getColumn(listaPreApro, 1);
                                    text = text.replace("a2", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a3")) {
                                    col = getColumn(listaPreApro, 2);
                                    text = text.replace("a3", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a4")) {
                                    col = getColumn(listaPreApro, 3);
                                    text = text.replace("a4", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a5")) {
                                    col = getColumn(listaPreApro, 4);
                                    text = text.replace("a5", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a6")) {
                                    col = getColumn(listaPreApro, 5);
                                    text = text.replace("a6", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b1")) {
                                    col = getColumn(listaApro, 0);
                                    text = text.replace("b1", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b2")) {
                                    col = getColumn(listaApro, 1);
                                    text = text.replace("b2", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b3")) {
                                    col = getColumn(listaApro, 2);
                                    text = text.replace("b3", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b4")) {
                                    col = getColumn(listaApro, 3);
                                    text = text.replace("b4", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b5")) {
                                    col = getColumn(listaApro, 4);
                                    text = text.replace("b5", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b6")) {
                                    col = getColumn(listaApro, 5);
                                    text = text.replace("b6", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c1")) {
                                    col = getColumn(listaPosApro, 0);
                                    text = text.replace("c1", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c2")) {
                                    col = getColumn(listaPosApro, 1);
                                    text = text.replace("c2", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c3")) {
                                    col = getColumn(listaPosApro, 2);
                                    text = text.replace("c3", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c4")) {
                                    col = getColumn(listaPosApro, 3);
                                    text = text.replace("c4", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c5")) {
                                    col = getColumn(listaPosApro, 4);
                                    text = text.replace("c5", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c6")) {
                                    col = getColumn(listaPosApro, 5);
                                    text = text.replace("c6", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);

            }

            table.removeRow(4);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private static String getColumn(List<EvaluacionAmbientalAprovechamientoEntity> lista, int column) {
        String data = "";
        try {

            if (lista != null && !lista.isEmpty() && lista.get(column) != null) {

                data = lista.get(column).getNombreAprovechamiento();
            }
        } catch (Exception e) {
            data = "";
        }
        return data;
    }

    private static String getRespuestaImpactAmb(EvaluacionAmbientalActividadEntity eval, String nombreColumna,
            List<EvaluacionAmbientalDto> listaRespuesta, String tipoAprovechamiento) {
        String data = "";
        try {

            if (nombreColumna.equals("")) {
                return data;
            }

            for (EvaluacionAmbientalDto resp : listaRespuesta) {

                if (resp.getNombreActividad().equals(eval.getNombreActividad()) &&
                        resp.getTipoAprovechamiento().equals(tipoAprovechamiento) &&
                        resp.getMedidasControl().equals(eval.getMedidasControl()) &&
                        resp.getNombreAprovechamiento().equals(nombreColumna) &&
                        resp.getValorEvaluacion() != null && resp.getValorEvaluacion().equals("S")) {
                    data = "X";
                    break;
                }

            }

        } catch (Exception e) {
            data = "";
        }
        return data;
    }

    public static XWPFDocument setearListaProduccionTab3(PotencialProduccionForestalEntity data, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("n1")) {

                                text = text.replace("n1",
                                        data.getErrorMuestreo() == null ? "" : data.getErrorMuestreo().toString());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    // *************************** WORKING */
    /*
     * public static XWPFDocument
     * setearListaProduccionForestal2(List<ResultadosEspeciesMuestreoDto> data,
     * XWPFDocument doc, Integer numTabla) {
     * try {
     * 
     * List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
     * List<TablaEspeciesMuestreo> lista = new ArrayList<>();
     * 
     * XWPFTable table = doc.getTables().get(numTabla);
     * 
     * for (ResultadosEspeciesMuestreoDto element : data) {
     * 
     * lista = element.getListTablaEspeciesMuestreo();
     * 
     * for (int j = 1; j <= 3; j++) { // 3 lineas de la tabla
     * 
     * XWPFTableRow row = table.getRow(j);
     * 
     * if (row != null) {
     * XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(),
     * table);
     * 
     * CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
     * CTHeight ht = trPr.addNewTrHeight();
     * ht.setVal(BigInteger.valueOf(240));
     * 
     * for (XWPFTableCell cell : copiedRow.getTableCells()) {
     * for (XWPFParagraph p : cell.getParagraphs()) {
     * for (XWPFRun r : p.getRuns()) {
     * if (r != null) {
     * String text = r.getText(0);
     * if (text != null && text.trim().contains("n1")) {
     * text = text.replace("n1", element.getNombreBosque() == null ? ""
     * : element.getNombreBosque().toString());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n2")) {
     * text = text.replace("n2",
     * lista.get(0) == null ? ""
     * : lista.get(0).getEspecies() == null ? ""
     * : lista.get(0).getEspecies());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n3")) {
     * text = text.replace("n3",
     * lista.get(0) == null ? ""
     * : lista.get(0).getNumeroArbolesTotalBosque() == null ? ""
     * : lista.get(0).getNumeroArbolesTotalBosque());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n4")) {
     * text = text.replace("n4",
     * lista.get(0) == null ? ""
     * : lista.get(0).getAreaBasalTotalBosque() == null ? ""
     * : lista.get(0).getAreaBasalTotalBosque());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n5")) {
     * text = text.replace("n5",
     * lista.get(0) == null ? ""
     * : lista.get(0).getVolumenTotalBosque() == null ? ""
     * : lista.get(0).getVolumenTotalBosque());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n6")) {
     * text = text.replace("n6",
     * lista.get(0) == null ? ""
     * : lista.get(0).getNumeroArbolesTotalHa() == null ? ""
     * : lista.get(0).getNumeroArbolesTotalHa());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n7")) {
     * text = text.replace("n7",
     * lista.get(0) == null ? ""
     * : lista.get(0).getAreaBasalTotalHa() == null ? ""
     * : lista.get(0).getAreaBasalTotalHa());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n8")) {
     * text = text.replace("n8",
     * lista.get(0) == null ? ""
     * : lista.get(0).getVolumenTotalHa() == null ? ""
     * : lista.get(0).getVolumenTotalHa());
     * r.setText(text, 0);
     * }
     * }
     * }
     * }
     * }
     * listaTablaContenido.add(copiedRow);
     * }
     * }
     * }
     * 
     * for (int j = 1; j <= 3; j++) {
     * table.removeRow(1);
     * }
     * 
     * for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
     * table.addRow(xwpfTableRow);
     * }
     * return doc;
     * } catch (Exception e) {
     * System.out.println(e.getMessage());
     * return null;
     * }
     * 
     * }
     */

    public static XWPFDocument setearListaPotencialProduccion(List<AprovechamientoRFNMDto> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            List<ResultadosAprovechamientoRFNMDto> lista = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);

            for (AprovechamientoRFNMDto element : listaPotencial) {

                lista = element.getResultadosAprovechamientoRFNMDtos();

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getEspecie() == null ? "" : element.getEspecie());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b", element.getProductoAprovechar() == null ? ""
                                            : element.getProductoAprovechar());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c", lista.isEmpty() ? ""
                                            : lista.get(0) == null ? ""
                                                    : lista.get(0).getSector() == null ? "" : lista.get(0).getSector());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("d")) {

                                    text = text.replace("d", lista.isEmpty() ? ""
                                            : lista.get(0) == null ? ""
                                                    : lista.get(0).getArea() == null ? "" : lista.get(0).getArea());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("e")) {

                                    text = text.replace("e",
                                            lista.isEmpty() ? ""
                                                    : lista.get(0) == null ? ""
                                                            : lista.get(0).getnTotalIndividuos() == null ? ""
                                                                    : lista.get(0).getnTotalIndividuos());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("f")) {

                                    text = text.replace("f",
                                            lista.isEmpty() ? ""
                                                    : lista.get(0) == null ? ""
                                                            : lista.get(0).getIndividuosHa() == null ? ""
                                                                    : lista.get(0).getIndividuosHa());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("g")) {

                                    text = text.replace("g",
                                            lista.isEmpty() ? ""
                                                    : lista.get(0) == null ? ""
                                                            : lista.get(0).getVolumen() == null ? ""
                                                                    : lista.get(0).getVolumen());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearUsoPotencialPorCategoria(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {

                                    text = text.replace("s1", listaPotencial.get(0).getCatOrdenamiento() == null ? "" : listaPotencial.get(0).getCatOrdenamiento());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("s2")) {

                                    text = text.replace("s2", listaPotencial.get(0).getDescripcionOrd() == null ? ""
                                            : listaPotencial.get(0).getDescripcionOrd());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s3")) {

                                    text = text.replace("s3", listaPotencial.get(0).getActividad() == null ? ""
                                            : listaPotencial.get(0).getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearSistemaManejoEspecificarYCicloDeCorta(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("1")) {

                                    text = text.replace("1", listaPotencial.get(0).getDescripcionOrd() == null ? "" : listaPotencial.get(0).getDescripcionOrd());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesManejarDiametros(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : listaPotencial) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getEspecie() == null ? ""
                                            : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getLineaProduccion() == null ? ""
                                            : element.getLineaProduccion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getNuDiametro() == null ? ""
                                            : element.getNuDiametro().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n5")) {

                                    text = text.replace("n5", element.getNuCaminoAcceso() == null ? ""
                                            : element.getNuCaminoAcceso().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesDeFloraProteger(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : listaPotencial) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getEspecie() == null ? ""
                                            : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getDescripcionOtros() == null ? ""
                                            : element.getDescripcionOtros());
                                    r.setText(text, 0);
                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearHeaderCortaAnual(List<VolumenComercialPromedioCabeceraDto> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (VolumenComercialPromedioCabeceraDto element : lista) {
                
            
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b", element.getBloquequinquenal() == null ? "" : "Tipo de bloque: " + element.getBloquequinquenal());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getToTalAreaHa() == null ? "" : "Área (ha): " + element.getToTalAreaHa().toString());
                                    r.setText(text, 0);
                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearBodyCortaAnual(List<VolumenComercialPromedioEspecieDto> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (VolumenComercialPromedioEspecieDto element : lista) {
                
            
                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getNombreComun() == null ? "" : element.getNombreComun());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getNombreCientifico() == null ? "" : element.getNombreCientifico());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getDMC() == null ? "" : element.getDMC().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getCantidadArb() == null ? "" : element.getCantidadArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n5")) {

                                    text = text.replace("n5", element.getTotalArb() == null ? "" : element.getTotalArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n6")) {

                                    text = text.replace("n6", element.getVolumenComercialPromedio() == null ? "" : element.getVolumenComercialPromedio().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n7")) {

                                    text = text.replace("n7", element.getVolumenComercialPromedioTotal() == null ? "" : element.getVolumenComercialPromedioTotal().toString());
                                    r.setText(text, 0);
                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


    public static XWPFDocument setearCortaAnualTab2(List<VolumenComercialPromedioEspecieDto> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (VolumenComercialPromedioEspecieDto element : lista) {
                
            
                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getNombreComun() == null ? "" : element.getNombreComun());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getCantidadArb() == null ? ""
                                            : element.getCantidadArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getPorcentajeArb() == null ? ""
                                            : element.getPorcentajeArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getVolumenComercialPromedio() == null ? ""
                                            : element.getVolumenComercialPromedio().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n5")) {

                                    text = text.replace("n5", element.getVolumenComercialPromedioPonderado() == null ? ""
                                            : element.getVolumenComercialPromedioPonderado().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n6")) {

                                    text = text.replace("n6", element.getVolumenCortaAnualPermisible() == null ? ""
                                            : element.getVolumenCortaAnualPermisible().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument addColumnTableProyCos(List<ProyeccionCosechaDetalleDto> lastColumn, XWPFDocument doc, Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            XWPFTableRow row2 = table.createRow();
            //row2.getCell(0).setText("rub");


            XWPFTableRow row1  = table.getRow(0);
            row2 = table.getRow(1);
            row2.removeCell(2);
            Integer i = 1;
            for (ProyeccionCosechaDetalleDto e : lastColumn) {

                
                row2.addNewTableCell().setText("Año " + e.getAnio().toString());

                if(lastColumn.size()!=i){ row1.addNewTableCell().setText(""); }
                i++;
            }

            

            mergeCellVertically(table, 0, 0, 1);
            mergeCellVertically(table, 1, 0, 1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspecificacionesSistAprov(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getMetodoConstruccion() == null ? "" : element.getMetodoConstruccion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getManoObra() == null ? "" : element.getManoObra());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getMaquina() == null ? "" : element.getMaquina());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesImportantes(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);
            Integer i = 1;

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", i.toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getEspecie() == null ? "" : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
                i++;
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesPosibles(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);
            Integer i = 1;

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", i.toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getAccion() == false ? "" : "X");
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
                i++;
            }
        table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearManejoAreasDegradadas(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearSistemaManejoEspecificarasd(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", listaPotencial.get(0).getDescripcionOrd() == null ? "" : listaPotencial.get(0).getDescripcionOrd());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaProduccionForestal2(List<ResultadosEspeciesMuestreoDto> lista,
            XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            int addRowEspecie = 0;

            for (ResultadosEspeciesMuestreoDto e : lista) {

                int rowIni = addRowEspecie;

                for (TablaEspeciesMuestreo j : e.getListTablaEspeciesMuestreo()) {

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row = table.getRow(addRowEspecie);
                    row.getCell(0).setText(e.getNombreBosque());

                    row.getCell(1).setText(j.getEspecies());
                    row.getCell(2).setText("N");
                    row.getCell(3).setText(j.getNumeroArbolesTotalBosque());
                    row.getCell(4).setText(j.getNumeroArbolesTotalHa());

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row2 = table.getRow(addRowEspecie);

                    row2.getCell(2).setText("AB m2");
                    row2.getCell(3).setText(j.getAreaBasalTotalBosque());
                    row2.getCell(4).setText(j.getAreaBasalTotalHa());

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row3 = table.getRow(addRowEspecie);

                    row3.getCell(2).setText("Vp m3");
                    row3.getCell(3).setText(j.getVolumenTotalBosque());
                    row3.getCell(4).setText(j.getVolumenTotalHa());

                    mergeCellVertically(table, 1, addRowEspecie - 2, addRowEspecie);
                }

                mergeCellVertically(table, 0, rowIni + 1, addRowEspecie);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaProduccionTab4(List<ResultadosEspeciesMuestreoDto> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            List<TablaEspeciesMuestreo> data = new ArrayList<>();

            for (ResultadosEspeciesMuestreoDto element : lista) {

                data = element.getListTablaEspeciesMuestreo();

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("n1")) {
                                    text = text.replace("n1", data.get(0) == null ? ""
                                            : data.get(0).getEspecies() == null ? "" : data.get(0).getEspecies());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n2")) {
                                    text = text.replace("n2",
                                            data.get(0) == null ? ""
                                                    : data.get(0).getNumeroArbolesTotalHa() == null ? ""
                                                            : data.get(0).getNumeroArbolesTotalHa().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearResultadosFustales(List<ResultadosEspeciesMuestreoDto> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            int addRowEspecie = 1;

            for (ResultadosEspeciesMuestreoDto e : lista) {

                int rowIni = addRowEspecie;

                for (TablaEspeciesMuestreo j : e.getListTablaEspeciesMuestreo()) {

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row = table.getRow(addRowEspecie);
                    row.createCell();
                    row.getCell(0).setText(e.getNombreBosque());

                    row.getCell(1).setText(j.getEspecies());
                    row.getCell(2).setText("N");
                    row.getCell(3).setText(j.getNumeroArbolesDap10a14());
                    row.getCell(4).setText(j.getNumeroArbolesDap15a19());
                    row.getCell(5).setText(j.getNumeroArbolesTotalHa());
                    row.getCell(6).setText(j.getNumeroArbolesTotalBosque());

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row2 = table.getRow(addRowEspecie);
                    row2.createCell();

                    row2.getCell(2).setText("AB m2");
                    row2.getCell(3).setText(j.getAreaBasalDap10a14());
                    row2.getCell(4).setText(j.getAreaBasalDap15a19());
                    row2.getCell(5).setText(j.getAreaBasalTotalHa());
                    row2.getCell(6).setText(j.getAreaBasalTotalBosque());

                    mergeCellVertically(table, 1, addRowEspecie - 1, addRowEspecie);
                }

                mergeCellVertically(table, 0, rowIni + 1, addRowEspecie);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearResultadosFustalesE(List<PotencialProduccionForestalEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            List<PotencialProduccionForestalDto> data = new ArrayList<>();

            if (lista.get(0) != null) {
                data = lista.get(0).getListPotencialProduccion();
            }

            for (PotencialProduccionForestalDto element : data) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("a")) {
                                    text = text.replace("a", element.getEspecie() == null ? "" : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b")) {
                                    text = text.replace("b",
                                            element.getGrupoComercial() == null ? "" : element.getGrupoComercial());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c")) {
                                    text = text.replace("c",
                                            element.getTotalHa() == null ? "" : element.getTotalHa().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("d")) {
                                    text = text.replace("d",
                                            element.getPorcentaje() == null ? "" : element.getPorcentaje().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearCategoriasOrdenamiento(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a",
                                            element.getCategoria() == null ? "" : element.getCategoria());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b",
                                            element.getAreaHA() == null ? "" : element.getAreaHA().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c",
                                            element.getAreaHAPorcentaje() == null ? ""
                                                    : element.getAreaHAPorcentaje().toString());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearNBloques(List<OrdenamientoProteccionDetalleEntity> data, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("n1")) {

                                text = text.replace("n1", data.get(0).getDescripcion() == null ? ""
                                        : data.get(0).getDescripcion().toString());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearPeriodo(List<OrdenamientoProteccionDetalleEntity> data, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("p1")) {
                                text = text.replace("p1", data.get(0).getObservacion() == null ? ""
                                        : data.get(0).getObservacion().toString());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    // ***** WORKING 222 */
    public static XWPFDocument setearListaParcelaCorta(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {
                                    text = text.replace("s1", element.getParcelaCorta() == null ? ""
                                            : element.getParcelaCorta().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s2")) {
                                    text = text.replace("s2",
                                            element.getTipoBosque() == null ? "" : element.getTipoBosque().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s3")) {
                                    text = text.replace("s3",
                                            element.getAreaHA() == null ? "" : element.getAreaHA().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s4")) {
                                    text = text.replace("s4", element.getAreaHAPorcentaje() == null ? ""
                                            : element.getAreaHAPorcentaje().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListUbicacionYSenalizacion(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("1")) {
                                    text = text.replace("1", element.getVerticeBloque() == null ? ""
                                            : element.getVerticeBloque().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("2")) {
                                    text = text.replace("2", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("3")) {
                                    text = text.replace("3", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("4")) {
                                    text = text.replace("4",
                                            element.getZonaUTM() == null ? "" : element.getZonaUTM().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("5")) {
                                    text = text.replace("5", element.getDescripcion() == null ? ""
                                            : element.getDescripcion().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("6")) {
                                    text = text.replace("6", element.getObservacion() == null ? ""
                                            : element.getObservacion().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListDemarcacionYVigilancia(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {
                                    text = text.replace("s1", element.getActividad() == null ? ""
                                            : element.getActividad().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s2")) {
                                    text = text.replace("s2", element.getDescripcion() == null ? ""
                                            : element.getDescripcion().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s3")) {
                                    text = text.replace("s3", element.getObservacion() == null ? ""
                                            : element.getObservacion().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }  

    static void mergeCellVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            CTVMerge vmerge = (CTVMerge) CTVMerge.Factory.newInstance();
            if (rowIndex == fromRow) {
                // The first merged cell is set with RESTART merge value
                vmerge.setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                vmerge.setVal(STMerge.CONTINUE);
                // and the content should be removed
                for (int i = cell.getParagraphs().size(); i > 0; i--) {
                    cell.removeParagraph(0);
                }
                cell.addParagraph();
            }
            // Try getting the TcPr. Not simply setting an new one every time.
            CTTcPr tcPr = cell.getCTTc().getTcPr();
            if (tcPr == null)
                tcPr = cell.getCTTc().addNewTcPr();
            tcPr.setVMerge(vmerge);
        }
    }

    static void mergeCellHorizontally(XWPFTable table, int row, int fromCol, int toCol) {
        XWPFTableCell cell = table.getRow(row).getCell(fromCol);
        // Try getting the TcPr. Not simply setting an new one every time.
        CTTcPr tcPr = cell.getCTTc().getTcPr();
        if (tcPr == null) tcPr = cell.getCTTc().addNewTcPr();
        // The first merged cell has grid span property set
        if (tcPr.isSetGridSpan()) {
         tcPr.getGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        } else {
         tcPr.addNewGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        }
        // Cells which join (merge) the first one, must be removed
        for(int colIndex = toCol; colIndex > fromCol; colIndex--) {
         table.getRow(row).getCtRow().removeTc(colIndex);
         table.getRow(row).removeCell(colIndex);
        }
    }


    public static void generarInfoBasicaUEAA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc, String departamento, String provincia, String distrito ) {
        try {
            XWPFTable table = doc.getTables().get(7);
            System.out.println(lstLista.size());
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    // if (detalleEntity.getCodigoSubResumen().equals("POCCRARPOARARPO")) {

                                    if (text5 != null) {
                                        if (text5.contains("41nro")) {
                                            text5 = text5.replace("41nro",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41areatotal")) {
                                            text5 = text5.replace("41areatotal",
                                                    detalleEntity.getAreaHa() != null
                                                            ? detalleEntity.getAreaHa().toString()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41cuenca")) {
                                            text5 = text5.replace("41cuenca",
                                                    detalleEntity.getCuenca() != null
                                                            ? detalleEntity.getCuenca()
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41departamento")) {
                                            text5 = text5.replace("41departamento",
                                                    departamento != null
                                                            ? departamento
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41provincia")) {
                                            text5 = text5.replace("41provincia",
                                                    provincia != null ? provincia
                                                            : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("41distrito")) {
                                            text5 = text5.replace("41distrito",
                                                    distrito != null ? distrito : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                    // }

                                }
                            }
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static XWPFDocument generarInfoBasicaCOOUTM(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(8);
            XWPFTableRow row = table.getRow(1);
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("42pc")) {
                                            text = text.replace("42pc",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("42vertice")) {
                                            text = text.replace("42vertice",
                                                    detalleEntity.getPuntoVertice() != null
                                                            ? detalleEntity.getPuntoVertice()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("42este")) {
                                            text = text.replace("42este",
                                                    detalleEntity.getCoordenadaEste() != null
                                                            ? detalleEntity.getCoordenadaEste().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("42norte")) {
                                            text = text.replace("42norte",
                                                    detalleEntity.getCoordenadaNorte() != null
                                                            ? detalleEntity.getCoordenadaNorte().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("42referencia")) {
                                            text = text.replace("42referencia",
                                                    detalleEntity.getReferencia() != null
                                                            ? detalleEntity.getReferencia()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarInfoBasicaTB(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(9);
            XWPFTableRow row = table.getRow(1);
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("43tipoBosque")) {
                                            text = text.replace("43tipoBosque",
                                                    detalleEntity.getNombre() != null
                                                            ? detalleEntity.getNombre()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43area")) {
                                            text = text.replace("43area",
                                                    detalleEntity.getAreaHa() != null
                                                            ? detalleEntity.getAreaHa().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("43porc")) {
                                            text = text.replace("43porc",
                                                    detalleEntity.getAreaHaPorcentaje() != null
                                                            ? detalleEntity.getAreaHaPorcentaje().toString()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }
    public static XWPFDocument generarInfoBasicaAccA(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            // PUNTO A)
            XWPFTable table = doc.getTables().get(10);
            XWPFTableRow row = table.getRow(0);
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("5ARUTAS")) {
                                            text = text.replace("5ARUTAS",
                                                    detalleEntity.getActividad() != null
                                                            ? detalleEntity.getActividad()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarInfoBasicaAccC(List<InfBasicaAereaDetalleDto> lstLista, XWPFDocument doc) {
        try {
            // PUNTO A)
            XWPFTable table = doc.getTables().get(11);
            XWPFTableRow row = table.getRow(0);
            for (InfBasicaAereaDetalleDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("MEDIOS")) {
                                            text = text.replace("MEDIOS",
                                                    detalleEntity.getJustificacion() != null
                                                            ? detalleEntity.getJustificacion()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument generarActividadAprovPOACDeli(List<ActividadAprovechamientoEntity> lstLista,
                                                                   XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAADAAA")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodActvAproveDet().equals("POACAADAAA")).collect(Collectors.toList());
            String estado="";
            XWPFTable table = doc.getTables().get(12);
            XWPFTableRow row = table.getRow(1);
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("51sistema")) {
                                            text = text.replace("51sistema",
                                                    detalleEntity.getOperaciones() != null
                                                            ? detalleEntity.getOperaciones()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("1e")) {
                                            if(detalleEntity.getDetalle().equals("S")){
                                                estado="X";
                                            }else{estado="";}
                                            text = text.replace("1e",
                                                    detalleEntity.getDetalle() != null ? estado
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("51modoImplementacion")) {
                                            text = text.replace("51modoImplementacion",
                                                    detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovPOACDeliDis(List<ActividadAprovechamientoEntity> lstLista,
                                                             XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAADAAA")).collect(Collectors.toList());

            String estado="";
            XWPFTable table = doc.getTables().get(13);
            XWPFTableRow row = table.getRow(0);
            for (ActividadAprovechamientoEntity detalleEntity : lstListaFil) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("51dis")) {
                                            text = text.replace("51dis",
                                                    detalleEntity.getTrochas() != null
                                                            ? detalleEntity.getTrochas()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }
    public static XWPFDocument generarActividadAprovPOACPlani(List<ActividadAprovechamientoEntity> lstLista,
                                                                XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAAPCIA")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }

            String estado="";
            XWPFTable table = doc.getTables().get(18);
            XWPFTableRow row = table.getRow(1);
            int contador=0;
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDetalle) {
                contador++;
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("Nro")) {
                                            text = text.replace("Nro",contador+"");
                                            r.setText(text, 0);
                                        }else if (text.contains("53camino")) {
                                            text = text.replace("53camino",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("53construccion")) {
                                            text = text.replace("53construccion",
                                                    detalleEntity.getDetalle() != null
                                                            ? detalleEntity.getDetalle()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("53manteni")) {
                                            text = text.replace("53manteni",
                                                    detalleEntity.getFamilia() != null
                                                            ? detalleEntity.getFamilia()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("53alcan")) {
                                            text = text.replace("53alcan",
                                                    detalleEntity.getObservacion() != null
                                                            ? detalleEntity.getObservacion()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("53puentes")) {
                                            text = text.replace("53puentes",
                                                    detalleEntity.getOperaciones() != null
                                                            ? detalleEntity.getOperaciones()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("53cunetas")) {
                                            text = text.replace("53cunetas",
                                                    detalleEntity.getPersonal() != null
                                                            ? detalleEntity.getPersonal()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }
    public static XWPFDocument generarActividadAprovPOACOperaCorta(List<ActividadAprovechamientoEntity> lstLista,
                                                              XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAAOC")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }

            String estado="";
            XWPFTable table = doc.getTables().get(19);
            XWPFTableRow row = table.getRow(1);
            int contador=0;
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDetalle) {
                contador++;
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                         if (text.contains("54metodo")) {
                                            text = text.replace("54metodo",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("54personal")) {
                                            text = text.replace("54personal",
                                                    detalleEntity.getPersonal() != null
                                                            ? detalleEntity.getPersonal()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("54equipo")) {
                                            text = text.replace("54equipo",
                                                    detalleEntity.getMaquinariaMaterial() != null
                                                            ? detalleEntity.getMaquinariaMaterial()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovPOACOperaArras(List<ActividadAprovechamientoEntity> lstLista,
                                                                   XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAAOAT")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }

            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodSubActvAproveDet().equals("A")).collect(Collectors.toList());


            String estado="";
            XWPFTable table = doc.getTables().get(20);
            XWPFTableRow row = table.getRow(1);
            int contador=0;
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                contador++;
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("551METODO")) {
                                            text = text.replace("551METODO",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("551PERSONAL")) {
                                            text = text.replace("551PERSONAL",
                                                    detalleEntity.getPersonal() != null
                                                            ? detalleEntity.getPersonal()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovPOACOperaTrans(List<ActividadAprovechamientoEntity> lstLista,
                                                                   XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAAOAT")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }

            List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                    .filter(eval -> eval.getCodSubActvAproveDet().equals("T")).collect(Collectors.toList());


            String estado="";
            XWPFTable table = doc.getTables().get(21);
            XWPFTableRow row = table.getRow(1);
            int contador=0;
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDet) {
                contador++;
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("552transporte")) {
                                            text = text.replace("552transporte",
                                                    detalleEntity.getDescripcion() != null
                                                            ? detalleEntity.getDescripcion()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("552personal")) {
                                            text = text.replace("552personal",
                                                    detalleEntity.getPersonal() != null
                                                            ? detalleEntity.getPersonal()
                                                            : "");
                                            r.setText(text, 0);
                                        }else if (text.contains("552maquinaria")) {
                                            text = text.replace("552maquinaria",
                                                    detalleEntity.getMaquinariaMaterial() != null
                                                            ? detalleEntity.getMaquinariaMaterial()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument generarActividadAprovPOACOperaProce(List<ActividadAprovechamientoEntity> lstLista,
                                                                   XWPFDocument doc) {
        try {
            List<ActividadAprovechamientoEntity> lstListaFil = lstLista.stream()
                    .filter(eval -> eval.getCodSubActvAprove().equals("POACAAPL")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }


            String estado="";
            XWPFTable table = doc.getTables().get(22);
            XWPFTableRow row = table.getRow(0);
            int contador=0;
            for (ActividadAprovechamientoDetEntity detalleEntity : lstListaDetalle) {
                contador++;
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("56procesamiento")) {
                                            text = text.replace("56procesamiento",
                                                    detalleEntity.getObservacion() != null
                                                            ? detalleEntity.getObservacion()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }


}