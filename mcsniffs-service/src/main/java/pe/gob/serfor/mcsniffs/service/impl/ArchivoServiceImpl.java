package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.FileServerRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.service.ArchivoService;

@Service
public class ArchivoServiceImpl implements ArchivoService{

    @Autowired
    ArchivoRepository arRepo;
 
    @Autowired
    private FileServerConexion fileCn;

    @Value("${smb.file.server.path}")
    private String fileServerPath;
    private static final Logger log = LogManager.getLogger(ArchivoServiceImpl.class);
    @Override
    public ResultEntity<ArchivoEntity> registroArchivo(ArchivoEntity request) {     
        return arRepo.registroArchivo(request);
    }

     /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado: 
     * @descripción: {listar los archivos}
     * @param:ArchivoEntity
     */
    @Override
    public ResultEntity<ArchivoEntity> listarArchivo(ArchivoEntity request) {
        return arRepo.listarArchivo(request);
    }

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado: 
     * @descripción: {obtener un archivo}
     * @param:IdArchivo
     */
    @Override
    public ResultClassEntity<ArchivoEntity> obtenerArchivo(Integer IdArchivo) {
        /*ResultEntity res = new ResultEntity();
        ArchivoEntity item = new ArchivoEntity();
        byte[] file = fileCn.loadFileAsResource(request.getNombreGenerado());   

        if(file!=null){
            item.setFile(file);
            res.setIsSuccess(true);
            res.setMessage("Información encontrada");
        }else{
            res.setIsSuccess(false);
            res.setMessage("Informacion no encontrada");
        }
        res.setItem(item);*/

        return arRepo.obtenerArchivo(IdArchivo);
    }


    /**
     * @autor: Harry Coa  [26-08-2021]
     * @modificado:
     * @descripción: {obtener un archivo}
     * @param: idArchivo
     */
    @Override
    public ResultClassEntity<ArchivoEntity> ObtenerArchivoGeneral(Integer idArchivo) throws Exception {
        return arRepo.ObtenerArchivoGeneral(idArchivo);
    }

    /**
     * @autor: Harry Coa [26-08-2021]
     * @modificado:
     * @descripción: {eliminar un archivo}
     * @param: idArchivo, idUsuario
     */
    @Override
    public ResultClassEntity<Integer> EliminarArchivoGeneral(Integer idArchivo, Integer idUsuario) throws Exception {
        return arRepo.EliminarArchivoGeneral(idArchivo, idUsuario);
    }


    /**
     * @autor: Rogel Maquera [27-08-2021]
     * @modificado:
     * @descripción: {registrar un archivo}
     * @param: ArchivoEntity
     */
    @Override
    public ResultClassEntity<Integer> RegistrarArchivoGeneral(MultipartFile file, Integer IdUsuarioCreacion, String TipoDocumento) throws Exception {

        ResultClassEntity<Integer> res = new ResultClassEntity();

        String nombreGenerado = fileCn.uploadFile(file);


        if(!nombreGenerado.equals("")){
            ArchivoEntity archivo = new ArchivoEntity();
            archivo.setIdUsuarioRegistro(IdUsuarioCreacion);
            archivo.setTipoDocumento(TipoDocumento);
            archivo.setEstado("A");
            archivo.setRuta(fileServerPath + ( !nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()) ) ;
            archivo.setExtension(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),file.getOriginalFilename().length()));
            archivo.setNombre(file.getOriginalFilename());
            archivo.setNombreGenerado((!nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()));

            log.info("ArchivoServiceImpl - RegistrarArchivoGeneral"+ archivo.toString());
            return  arRepo.RegistrarArchivoGeneral(archivo);

        }else{
            log.error("ArchivoServiceImpl - RegistrarArchivoGeneral occurrio un error: Archivo no Cargado");
            res.setMessage("Archivo no Cargado");
            res.setSuccess(false);
            return res;
        }

    }
    @Override
    public ResultClassEntity DescargarArchivoGeneral(ArchivoEntity param) {
        return arRepo.DescargarArchivoGeneral(param);
    }

    @Override
    public ResultClassEntity downloadFile(String name) {
        return arRepo.downloadFile(name);
    }


    @Override
    public ResultClassEntity eliminarArchivoGeometria(ArchivoEntity param) throws Exception {
        return  arRepo.eliminarArchivoGeometria(param);
    }

}
