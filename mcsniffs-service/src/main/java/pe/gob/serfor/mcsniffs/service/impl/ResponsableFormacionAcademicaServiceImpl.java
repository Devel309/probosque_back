package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;

import pe.gob.serfor.mcsniffs.repository.ResponsableFormacionAcademicaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionResponsableRepository;
import pe.gob.serfor.mcsniffs.service.ResponsableFormacionAcademicaService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;

import java.util.List;

@Service("ResponsableFormacionAcademicaService")
public class ResponsableFormacionAcademicaServiceImpl implements ResponsableFormacionAcademicaService{

    @Autowired
    private ResponsableFormacionAcademicaRepository repository;

    @Autowired
    private SolicitudConcesionResponsableRepository repositorySolicitudConcesionResponsable;

    @Autowired
    private SolicitudConcesionService servicioSolicitudConcesion;

    @Override
    public ResultClassEntity obtenerInformacionFormacionProfesional(ResponsableFormacionAcademicaEntity obj) throws Exception{
        return repository.listarResponsableFormacionAcademica(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarInformacionFormacionProfesional(List<ResponsableFormacionAcademicaEntity> items) throws Exception{
        ResultClassEntity result = new ResultClassEntity<>();

        for (ResponsableFormacionAcademicaEntity element : items) {
            result = repository.registrarResponsableFormacionAcademica(element);
        }

        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity eliminarInformacionFormacionProfesional(ResponsableFormacionAcademicaEntity obj) throws Exception{
        return repository.eliminarResponsableFormacionAcademica(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarInformacionFormacionProfesional(SolicitudConcesionResponsableDto dto) throws Exception{
        ResultClassEntity result = new ResultClassEntity<>();

        result = repositorySolicitudConcesionResponsable.actualizarSolicitudConcesionResponsable(dto);

        List<ResponsableFormacionAcademicaEntity> items = dto.getLstResponsableFormacionAcademica();
        List<SolicitudConcesionArchivoEntity> adjuntos = dto.getLstResponsableFormacionAcademicaArchivo();

        if(items != null && !items.isEmpty()) {
            for (ResponsableFormacionAcademicaEntity element : items) {
                if (element.getIdResponsableFormacionAcademica() > 0)
                    result = repository.actualizarResponsableFormacionAcademica(element);
                else
                    result = repository.registrarResponsableFormacionAcademica(element);
            }
        }

        if(adjuntos != null && !adjuntos.isEmpty()) {
            for (SolicitudConcesionArchivoEntity archivo : adjuntos) {
                if (archivo.getIdSolicitudConcesionArchivo() > 0)
                    result = servicioSolicitudConcesion.actualizarArchivoSolicitudConcesion(archivo);
                else
                    result = servicioSolicitudConcesion.registrarArchivoSolicitudConcesion(archivo);
            }
        }

        return result;
    }
}
