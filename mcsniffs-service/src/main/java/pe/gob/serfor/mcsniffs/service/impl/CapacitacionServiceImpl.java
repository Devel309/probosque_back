package pe.gob.serfor.mcsniffs.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.CapacitacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;
import pe.gob.serfor.mcsniffs.service.CapacitacionService;

import java.util.List;

@Service("CapacitacionService")
public class CapacitacionServiceImpl implements CapacitacionService {

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado: Registrar capacitacion
     * @descripción: {}
     *
     */

    @Override
    public ResultClassEntity RegistrarCapacitacion(CapacitacionEntity param) throws Exception {
        return capacitacionRepository.RegistrarCapacitacion(param);
    }

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado: Actualizar capacitacion
     * @descripción: {}
     *
     */

    @Override
    public ResultClassEntity ActualizarCapacitacion(CapacitacionEntity param) throws Exception {
        return capacitacionRepository.ActualizarCapacitacion(param);
    }

    @Override
    public ResultClassEntity EliminarCapacitacion(CapacitacionEntity param) throws Exception {
        return  capacitacionRepository.EliminarCapacitacion(param);
    }

    @Override
    public List<CapacitacionDto> ListarCapacitacion(CapacitacionDto param) throws Exception {
        return capacitacionRepository.ListarCapacitacion(param);
    }

    @Override
    public ResultClassEntity RegistrarCapacitacionDetalle(List<CapacitacionDto> list) throws Exception {
        return capacitacionRepository.RegistrarCapacitacionDetalle(list);
    }

    @Override
    public ResultClassEntity EliminarCapacitacionDetalle(CapacitacionDetalleEntity param) throws Exception {
        return  capacitacionRepository.EliminarCapacitacionDetalle(param);
    }
    @Override
    public ResultClassEntity ListarCapacitacionDetalle(CapacitacionEntity capacitacionEntity) throws Exception {
        return capacitacionRepository.ListarCapacitacionDetalle(capacitacionEntity);
    }

    @Override
    public ResultClassEntity<List<CapacitacionDto>> ListarCapacitacionCabeceraDetalle(Integer idPlanManejo, String codTipoCapacitacion) throws Exception {
        ResultClassEntity<List<CapacitacionDto>> result = new ResultClassEntity<>();

        result.setData(capacitacionRepository.ListarCapacitacionCabeceraDetalle(idPlanManejo,codTipoCapacitacion));
        result.setSuccess(true);
        result.setMessage("Listado de Capacitacion");

        return result;
    }
}
