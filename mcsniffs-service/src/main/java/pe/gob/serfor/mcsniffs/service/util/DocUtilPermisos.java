package pe.gob.serfor.mcsniffs.service.util;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ModeloContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ListaInformeDocumentoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionSubDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class DocUtilPermisos {

    private DocUtilPermisos() {
    }


    public static XWPFDocument setearInfoPermiso(List<InformacionGeneralPermisoForestalEntity> data, XWPFDocument doc) {
        try {
            for (int i = 0; i < data.size(); i++) {
                for (XWPFParagraph p : doc.getParagraphs()) {
                    List<XWPFRun> runs = p.getRuns();
                    if (runs != null) {
                        for (XWPFRun r : runs) {
                            String text = r.getText(0);
                            if (text != null) {

                                if (text.contains("txProvincia")) {
                                    text = text.replace("txProvincia",data.get(i).getProvinciaTitular() != null? data.get(i).getProvinciaTitular(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txPersona")) {
                                    text = text.replace("txPersona",data.get(i).getApellidoPaternoElaborador() != null ?
                                            data.get(i).getApellidoPaternoElaborador() + " "+data.get(i).getApellidoMaternoElaborador()+", "+data.get(i).getNombreElaborador(): "");
                                    r.setText(text, 0);
                                }else  if (text.contains("txCargo")) {
                                    text = text.replace("txCargo", "");
                                    r.setText(text, 0);
                                }else if (text.contains("txTitular")) {
                                    text = text.replace("txTitular",data.get(i).getApellidoPaternoElaborador() != null ?
                                            data.get(i).getApellidoPaternoElaborador() + " "+data.get(i).getApellidoMaternoElaborador()+", "+data.get(i).getNombreElaborador(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txNumeroTitular")) {
                                    text = text.replace("txNumeroTitular",data.get(i).getDocumentoElaborador() != null ? data.get(i).getDocumentoElaborador() : "");
                                    r.setText(text, 0);
                                }else if (text.contains("txRepresentante")) {
                                    text = text.replace("txRepresentante",data.get(i).getFederacionComunidad() != null ? data.get(i).getFederacionComunidad() : "");
                                    r.setText(text, 0);
                                }else if (text.contains("txNumRepresentante")) {
                                    text = text.replace("txNumRepresentante",data.get(i).getRucComunidad() != null ? data.get(i).getRucComunidad() : "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDistrito")) {
                                    text = text.replace("txDistrito",data.get(i).getDistritoTitular() != null? data.get(i).getDistritoTitular(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txProvincia")) {
                                    text = text.replace("txProvincia",data.get(i).getProvinciaTitular() != null? data.get(i).getProvinciaTitular(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDepartamento")) {
                                    text = text.replace("txDepartamento",data.get(i).getDepartamentoTitular() != null? data.get(i).getDepartamentoTitular(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDocumentoAcredite")) {
                                    text = text.replace("txDocumentoAcredite","");
                                    r.setText(text, 0);
                                }else if (text.contains("txSuperficie")) {
                                    text = text.replace("txSuperficie","");
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
            }


            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }
}