package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesPlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.LibroOperacionesPlanManejoRepository;
import pe.gob.serfor.mcsniffs.repository.LibroOperacionesRepository;
import pe.gob.serfor.mcsniffs.repository.NotificacionRepository;
import pe.gob.serfor.mcsniffs.service.LibroOperacionesPlanManejoService;
import pe.gob.serfor.mcsniffs.service.LibroOperacionesService;

import java.sql.Timestamp;
import java.util.List;

@Service("LibroOperacionesPlanManejoService")
public class LibroOperacionesPlanManejoServiceImpl implements LibroOperacionesPlanManejoService {

    @Autowired
    private LibroOperacionesPlanManejoRepository libroOperacionesRepository;



    @Override
    public ResultClassEntity registrarLibroOperacionesPlanManejo(List<LibroOperacionesPlanManejoDto> dto) throws Exception {

        return libroOperacionesRepository.registrarLibroOperacionesPlanManejo(dto);
    }



    @Override
    public ResultClassEntity eliminarLibroOperacionesPlanManejo(LibroOperacionesPlanManejoDto param) throws Exception {
        return libroOperacionesRepository.eliminarLibroOperacionesPlanManejo(param);
    }



}
