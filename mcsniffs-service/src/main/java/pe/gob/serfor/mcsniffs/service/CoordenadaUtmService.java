package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.CoordenadaUtmEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface CoordenadaUtmService {

    ResultClassEntity RegistrarCoordenadaUtm(List<CoordenadaUtmEntity> lista) throws Exception;
    ResultClassEntity ActualizarCoordenadaUtm(List<CoordenadaUtmEntity> lista) throws Exception;
    ResultClassEntity<CoordenadaUtmEntity> ListarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception;
    ResultClassEntity<CoordenadaUtmEntity> EliminarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception;
}
