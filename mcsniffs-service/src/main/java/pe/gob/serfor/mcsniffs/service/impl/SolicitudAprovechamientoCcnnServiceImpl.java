package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudAprovechamientoCcnnRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudAprovechamientoCcnnService;


/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad solicitud  de la base de datos mcsniffs}
 *
 */
@Service("SolicitudAprovechamientoCcnnService")
public class SolicitudAprovechamientoCcnnServiceImpl implements SolicitudAprovechamientoCcnnService {


    private static final Logger log = LogManager.getLogger(SolicitudAprovechamientoCcnnServiceImpl.class);

    @Autowired
    private SolicitudAprovechamientoCcnnRepository solicitudAprovechamientoCcnnRepository;
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Obtener solicitud aprovechamiento CCNN}
     * @param: SolicitudEntity
     */
    @Override
    public ResultClassEntity ObtenerSolicitudAprovechamientoCcnn(SolicitudEntity solicitud) throws Exception {
        return solicitudAprovechamientoCcnnRepository.ObtenerSolicitudAprovechamientoCcnn(solicitud);
    }
}