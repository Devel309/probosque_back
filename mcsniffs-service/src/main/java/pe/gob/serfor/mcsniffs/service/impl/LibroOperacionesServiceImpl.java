package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.repository.LibroOperacionesRepository;
import pe.gob.serfor.mcsniffs.repository.NotificacionRepository;
import pe.gob.serfor.mcsniffs.service.LibroOperacionesService;

import java.sql.Timestamp;

@Service("LibroOperacionesService")
public class LibroOperacionesServiceImpl implements LibroOperacionesService {

    @Autowired
    private LibroOperacionesRepository libroOperacionesRepository;

    @Autowired
    private NotificacionRepository notificacionRepository

    ;@Override
    public ResultClassEntity listarLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        return libroOperacionesRepository.listarLibroOperaciones(dto);
    }

    @Override
    public ResultClassEntity registrarLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        ResultClassEntity resultClassEntity = libroOperacionesRepository.registrarLibroOperaciones(dto);

        if(resultClassEntity.getSuccess()) {
            NotificacionEntity param = new NotificacionEntity();
            param.setIdNotificacion(0);
            param.setCodigoDocgestion("TDFLIBOP");
            param.setNumDocgestion(resultClassEntity.getCodigo());
            param.setMensaje("Perfil TITULARTH Ha solicitado un registro en el libro de operaciones. N° Sol: " + resultClassEntity.getCodigo() + ".");
            param.setCodigoPerfil("ARFFS");
            param.setFechaInicio(new Timestamp(System.currentTimeMillis()));
            param.setCantidadDias(30);
            param.setUrl("/aprovechamiento/bandeja-libro-operaciones");
            param.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
            ResultClassEntity resultNotificacion = notificacionRepository.registrarNotificacion(param);
        }
        return resultClassEntity;
    }

    @Override
    public ResultClassEntity actualizarLibroOperaciones(LibroOperacionesDto param) throws Exception {
        return libroOperacionesRepository.actualizarLibroOperaciones(param);
    }

    @Override
    public ResultClassEntity eliminarLibroOperaciones(LibroOperacionesDto param) throws Exception {
        return libroOperacionesRepository.eliminarLibroOperaciones(param);
    }

    @Override
    public ResultClassEntity validarNroRegistroLibroOperaciones(LibroOperacionesDto param) throws Exception {
        return libroOperacionesRepository.validarNroRegistroLibroOperaciones(param);
    }
    @Override
    public  ResultClassEntity listarLibroOperacionesPlanManejo(PlanManejoEntity param) throws Exception {
        return libroOperacionesRepository.listarLibroOperacionesPlanManejo(param);
    }
    @Override
    public  ResultClassEntity listarEvaluacionLibroOperaciones(LibroOperacionesDto param) throws Exception {
        return libroOperacionesRepository.listarEvaluacionLibroOperaciones(param);
    }
}
