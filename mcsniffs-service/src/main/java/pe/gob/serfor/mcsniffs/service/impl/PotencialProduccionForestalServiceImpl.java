package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.PotencialProduccionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.PotencialProduccionForestalService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service("PotencialProduccionForestalService")
public class PotencialProduccionForestalServiceImpl implements PotencialProduccionForestalService {

    @Autowired
    private PotencialProduccionForestalRepository repository;

    @Override
    public ResultClassEntity registrar(PotencialProduccionForestalEntity item,MultipartFile file) throws Exception {
        return repository.registrar(item,file);
    }
    @Override
    public ResultClassEntity listar(Integer idPlanManejo) throws Exception {
        return repository.listar(idPlanManejo);
    }

    /**
     * @autor: Rafael Azaña [16/10-2021]
     * @modificado:
     * @descripción: {CRUD DE POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */


    @Override
    public ResultClassEntity<List<PotencialProduccionForestalEntity>> ListarPotencialProducForestal( Integer idPlanManejo,String codigo)
            throws Exception {

        ResultClassEntity<List<PotencialProduccionForestalEntity>> result = new ResultClassEntity<>();

        result.setData(repository.ListarPotencialProducForestal(idPlanManejo,codigo));
        result.setSuccess(true);
        result.setMessage("Lista Potencial Forestal");

        return result;
    }

    @Override
    public ResultClassEntity<List<PotencialProduccionForestalEntity>> ListarPotencialProducForestalTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,Integer idPlanManejo,String codigoProceso)
            throws Exception {

        ResultClassEntity<List<PotencialProduccionForestalEntity>> result = new ResultClassEntity<>();

        result.setData(repository.ListarPotencialProducForestalTitular(tipoDocumento,nroDocumento,codigoProcesoTitular,idPlanManejo,codigoProceso));
        result.setSuccess(true);
        result.setMessage("Lista Potencial Forestal");

        return result;
    }


    @Override
    public ResultClassEntity RegistrarPotencialProducForestal(List<PotencialProduccionForestalEntity> list) throws Exception {
        return repository.RegistrarPotencialProducForestal(list);
    }


    @Override
    public ResultClassEntity EliminarPotencialProducForestal(PotencialProduccionForestalDto param) throws Exception {
        return  repository.EliminarPotencialProducForestal(param);
    }

    /**
     * @autor:  Danny Nazario [28-12-2021]
     * @modificado:
     * @descripción: {Registra Potencial Producción Recurso Forestal Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarPotencialProduccionExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna, String codigoTipo, String codigoSubTipo, String codigoTipoDet, String codigoSubTipoDet, int idPlanManejo, int idUsuarioRegistro) {
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);

            List<PotencialProduccionForestalEntity> list = new ArrayList<>();
            List<PotencialProduccionForestalDto> listItem = new ArrayList<>();
            List<PotencialProduccionForestalVariableEntity> listDet = new ArrayList<>();
            PotencialProduccionForestalEntity cab = new PotencialProduccionForestalEntity();
            Boolean isInicio = true;

            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                listDet = new ArrayList<>();
                PotencialProduccionForestalVariableEntity det = new PotencialProduccionForestalVariableEntity();
                PotencialProduccionForestalDto item = new PotencialProduccionForestalDto();

                if (!row.get(0).isEmpty()) {
                    if(!isInicio) {
                        cab.setListPotencialProduccion(listItem);
                        list.add(cab);
                        repository.RegistrarPotencialProducForestal(list);
                        listItem = new ArrayList<>();
                        list = new ArrayList<>();
                    }
                    cab = new PotencialProduccionForestalEntity();
                    cab.setIdPotProdForestal(0);
                    cab.setCodigoTipoPotProdForestal(codigoTipo);
                    cab.setCodigoSubTipoPotencialProdForestal(codigoSubTipo);
                    cab.setTipoBosque(row.get(0));
                    cab.setIdPlanManejo(idPlanManejo);
                    cab.setIdUsuarioRegistro(idUsuarioRegistro);
                    isInicio = false;
                }

                item.setIdPotProdForestalDet(0);
                item.setCodigoTipoPotencialProdForestalDet(codigoTipoDet);
                item.setCodigoSubTipoPotencialProdForestalDet(codigoSubTipoDet);
                item.setEspecie(row.get(1));

                if (codigoSubTipo.equals("PGMFAA")) {
                    det = new PotencialProduccionForestalVariableEntity();
                    det.setAccion(true);
                    det.setVariable("N");
                    det.setNuTotalTipoBosque(BigDecimal.valueOf(Double.parseDouble(row.get(2))));
                    det.setTotalHa(BigDecimal.valueOf(Double.parseDouble(row.get(5))));
                    det.setIdVariable(0);
                    det.setIdUsuarioRegistro(idUsuarioRegistro);
                    listDet.add(det);
                    det = new PotencialProduccionForestalVariableEntity();
                    det.setAccion(true);
                    det.setVariable("AB m2");
                    det.setNuTotalTipoBosque(BigDecimal.valueOf(Double.parseDouble(row.get(3))));
                    det.setTotalHa(BigDecimal.valueOf(Double.parseDouble(row.get(6))));
                    det.setIdVariable(0);
                    det.setIdUsuarioRegistro(idUsuarioRegistro);
                    listDet.add(det);
                    det = new PotencialProduccionForestalVariableEntity();
                    det.setAccion(true);
                    det.setVariable("Vc m3");
                    det.setNuTotalTipoBosque(BigDecimal.valueOf(Double.parseDouble(row.get(4))));
                    det.setTotalHa(BigDecimal.valueOf(Double.parseDouble(row.get(7))));
                    det.setIdVariable(0);
                    det.setIdUsuarioRegistro(idUsuarioRegistro);
                    listDet.add(det);
                } else if (codigoSubTipo.equals("PGMFAB")) {
                    det = new PotencialProduccionForestalVariableEntity();
                    det.setAccion(true);
                    det.setVariable("N");
                    det.setNuTotalTipoBosque(BigDecimal.valueOf(Double.parseDouble(row.get(2))));
                    det.setTotalHa(BigDecimal.valueOf(Double.parseDouble(row.get(4))));
                    det.setIdVariable(0);
                    det.setIdUsuarioRegistro(idUsuarioRegistro);
                    listDet.add(det);
                    det = new PotencialProduccionForestalVariableEntity();
                    det.setAccion(true);
                    det.setVariable("AB m2");
                    det.setNuTotalTipoBosque(BigDecimal.valueOf(Double.parseDouble(row.get(3))));
                    det.setTotalHa(BigDecimal.valueOf(Double.parseDouble(row.get(5))));
                    det.setIdVariable(0);
                    det.setIdUsuarioRegistro(idUsuarioRegistro);
                    listDet.add(det);
                }

                item.setListPotencialProduccionVariable(listDet);
                listItem.add(item);
            }
            cab.setListPotencialProduccion(listItem);
            list.add(cab);
            repository.RegistrarPotencialProducForestal(list);
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

}
