package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;

@Service("LogAuditoriaService")
public class LogAuditoriaServiceImpl implements LogAuditoriaService {


    @Autowired
    private LogAuditoriaRepository infoLogAuditoria;

    @Override
    public ResultClassEntity RegistrarLogAuditoria(LogAuditoriaEntity request)
            throws Exception {
        return infoLogAuditoria.RegistrarLogAuditoria(request);
    }

}
