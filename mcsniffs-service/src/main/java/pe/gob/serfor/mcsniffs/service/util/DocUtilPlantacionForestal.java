package pe.gob.serfor.mcsniffs.service.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;

import pe.gob.serfor.mcsniffs.entity.PersonaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.AreaBloqueDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.AreaBloqueEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolicitudPlantacionForestalDetalleEntity;

public class DocUtilPlantacionForestal {

    public static XWPFDocument setearInformacionSolicitante_1 (PersonaEntity data, PersonaEntity data2,
    XWPFDocument doc, Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            
            XWPFTable table = doc.getTables().get(numTabla);
            Integer anexo1 = table.getRows().size(); 
            
            for (int j = 0; j < anexo1; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("nameTxt")) { // I.I INFORMACION DEL SOLICITANTE
                                        text = text.replace("nameTxt", data.getRazonSocialEmpresa() == null ? ""
                                                : data.getRazonSocialEmpresa());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("numDni")) {
                                        text = text.replace("numDni", data.getNumeroDocumento() == null ? ""
                                                : data.getNumeroDocumento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("rucNum")) {
                                        text = text.replace("rucNum", data.getRuc() == null ? ""
                                                : data.getRuc());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("DirTxt")) {
                                        text = text.replace("DirTxt", data.getDireccion() == null ? ""
                                                : data.getDireccion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("num")) {
                                        text = text.replace("num", data.getDireccionNumero() == null ? ""
                                                : data.getDireccionNumero());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("sectTxt")) {
                                        text = text.replace("sectTxt", data.getSector() == null ? ""
                                                : data.getSector());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dtoTxt")) {
                                        text = text.replace("dtoTxt", data.getnombreDistrito() == null ? ""
                                                : data.getnombreDistrito());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("proTxt")) {
                                        text = text.replace("proTxt", data.getnombreProvincia() == null ? ""
                                                : data.getnombreProvincia());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dptoTxt")) {
                                        text = text.replace("dptoTxt", data.getnombreDepartamento() == null ? ""
                                                : data.getnombreDepartamento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("telFjo1")) {
                                        text = text.replace("telFjo1", data.getTelefono() == null ? ""
                                                : data.getTelefono());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("telCel1")) {
                                        text = text.replace("telCel1", data.getCelular() == null ? ""
                                                : data.getCelular());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("mailTxt")) {
                                        text = text.replace("mailTxt", data.getCorreo() == null ? ""
                                                : data.getCorreo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("repLegal")) { // I.II INFORMACION REP LEGAL
                                        text = text.replace("repLegal", data2.getNombres() == null ? ""
                                                : data2.getNombres());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dnii")) { 
                                        text = text.replace("dnii", data2.getNumeroDocumento() == null ? ""
                                                : data2.getNumeroDocumento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("repDir")) { 
                                        text = text.replace("repDir", data2.getDireccion() == null ? ""
                                                : data2.getDireccion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("intNum")) { 
                                        text = text.replace("intNum", data2.getDireccionNumero() == null ? ""
                                                : data2.getDireccionNumero());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("cascTxt")) { 
                                        text = text.replace("cascTxt", data2.getSector() == null ? ""
                                                : data2.getSector());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("repDist")) { 
                                        text = text.replace("repDist", data2.getnombreDistrito() == null ? ""
                                                : data2.getnombreDistrito());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("repProv")) { 
                                        text = text.replace("repProv", data2.getnombreProvincia() == null ? ""
                                                : data2.getnombreProvincia());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("111")) { 
                                        text = text.replace("111", data2.getnombreDepartamento() == null ? ""
                                                : data2.getnombreDepartamento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("222")) { 
                                        text = text.replace("222", data2.getTelefono() == null ? ""
                                                : data2.getTelefono());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("333")) { 
                                        text = text.replace("333", data2.getCelular() == null ? ""
                                                : data2.getCelular());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("correoTxt")) { 
                                        text = text.replace("correoTxt", data2.getCorreo() == null ? ""
                                                : data2.getCorreo());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 0; j < anexo1; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearInformacionDelArea (SolPlantacionForestalAreaEntity data, PersonaEntity data2, SolPlantacionForestalEntity data3,
    XWPFDocument doc, Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            
            XWPFTable table = doc.getTables().get(numTabla);
            Integer anexo1 = table.getRows().size(); 
            
            for (int j = 0; j < anexo1; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("nameTxt")) { // II.I DEL PEDIDO
                                        text = text.replace("nameTxt", data.getPredio() == null ? ""
                                                : data.getPredio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("a1")) {
                                        text = text.replace("a1", data.getArea() == null ? ""
                                                : data.getArea().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("namePropi")) {
                                        text = text.replace("namePropi", data2.getRazonSocialEmpresa() == null ? ""
                                                : data2.getRazonSocialEmpresa());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("numDni")) {
                                        text = text.replace("numDni", data2.getNumeroDocumento() == null ? ""
                                                : data2.getNumeroDocumento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("rucNum")) {
                                        text = text.replace("rucNum", data2.getRuc() == null ? ""
                                                : data2.getRuc());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("ubiTxt")) {
                                        text = text.replace("ubiTxt", data.getZona() == null ? ""
                                                : data.getZona());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", data.getCaserioComunidad() == null ? ""
                                                : data.getCaserioComunidad());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n2")) {
                                        text = text.replace("n2", data.getNombreDistrito() == null ? ""
                                                : data.getNombreDistrito());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n3")) {
                                        text = text.replace("n3", data.getNombreProvincia() == null ? ""
                                                : data.getNombreProvincia());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n4")) {
                                        text = text.replace("n4", data.getNombreDepartamento() == null ? ""
                                                : data.getNombreDepartamento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("111")) {
                                        text = text.replace("111", data.getPropietario() == null || data.getPropietario() == false ? ""
                                                : "X");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("222")) {
                                        text = text.replace("222", data.getInversionista() == null || data.getInversionista() == false ? ""
                                                : "X");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("t1")) {
                                        text = text.replace("t1", data.getCodigoTipoDocPropietario() == null ? ""
                                                    : data.getCodigoTipoDocPropietario());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("s1")) {
                                        if(data.getCodigoTipoDocPropietario()!=null){
                                            text = text.replace("s1", data.getNumeroDocPropietario() == null ? ""
                                                        : data.getNumeroDocPropietario());
                                            r.setText(text, 0);
                                        }else{
                                            text = text.replace("s1", "");
                                            r.setText(text, 0);
                                        } 
                                    } else if (text != null && text.trim().contains("t2")) {
                                        text = text.replace("t2", data.getCodigoTipodocInversionista() == null ? ""
                                                    : data.getCodigoTipodocInversionista());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("s2")) {
                                        if(data.getCodigoTipodocInversionista()!=null){
                                            text = text.replace("s2", data.getNumerDocInversionista() == null ? ""
                                                        : data.getNumerDocInversionista());
                                            r.setText(text, 0);
                                        }else{
                                            text = text.replace("s2", "");
                                            r.setText(text, 0);
                                        } 
                                    } else if (text != null && text.trim().equals("555")) {
                                        if(data3.getTipoContratoTH()!=null){
                                                text = text.replace("555", data3.getTipoContratoTH() == true ? ""
                                                : "X");
                                                }else{ text = text.replace("555",""); }
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("777")) {
                                        if(data3.getTipoContratoTH()!=null){
                                        text = text.replace("777", data3.getTipoContratoTH() == true ? ""
                                                    : data3.getNumeroContrato());
                                                }else{ text = text.replace("777","");}
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("666")) {
                                        if(data3.getTipoContratoTH()!=null){
                                            text = text.replace("666", data3.getTipoContratoTH() == false ? ""
                                            : "X");
                                        }else{ text = text.replace("666",""); }
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("888")) {
                                        if(data3.getTipoContratoTH()!=null){
                                            text = text.replace("888", data3.getTipoContratoTH() == false ? ""
                                                : data3.getNumeroContrato());
                                        }else{ text = text.replace("888","");}
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 0; j < anexo1; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearInformacionGeneralDelArea (List<AreaSistemaPlantacionDto> data, SolPlantacionForestalAreaEntity data2,
    XWPFDocument doc, Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            
            XWPFTable table = doc.getTables().get(numTabla);
            Integer anexo = table.getRows().size(); 
            Integer estatico = anexo - 1;

            //Primera Parte de la tabla
            
            for (int j = 0; j < estatico; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("areaTxt")) { // II.I DEL PEDIDO
                                        text = text.replace("areaTxt", data2.getAreaTotalPlantacion() == null ? ""
                                                : data2.getAreaTotalPlantacion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("fechaTxt")) {
                                        if(data2.getMesAnhoPlantacion()!=null){
                                            Date myDate = data2.getMesAnhoPlantacion();
                                            String asd = new SimpleDateFormat("MM-yyyy").format(myDate);
                                            text = text.replace("fechaTxt", asd);
                                            r.setText(text, 0);
                                        }else{
                                            text = text.replace("fechaTxt", "---");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            //Segunda Parte de la tabla

            for (AreaSistemaPlantacionDto element: data) {

                XWPFTableRow row = table.getRow(anexo-1);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", element.getNombre() == null ? ""
                                                : element.getNombre());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n2")) {
                                        text = text.replace("n2", element.getCodigoUm() == null ? ""
                                                : element.getCodigoUm());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n3")) {
                                        text = text.replace("n3", element.getCantidad() == null ? ""
                                                : element.getCantidad().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n4")) { 
                                        text = text.replace("n4", element.getFines() == null ? ""
                                                : element.getFines());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n5")) { 
                                        if(element.getEspeciesEstablecidas()!=null){
                                            text = text.replace("n5", element.getEspeciesEstablecidas() == true ? "SI"
                                                    : "NO");
                                            r.setText(text, 0);
                                            }else{
                                                text = text.replace("n5", "");
                                                r.setText(text, 0);
                                            }
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }




            for (int j = 0; j < anexo; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearDetallePlantacionForestal (List<SolicitudPlantacionForestalDetalleEntity> data, Integer data2,
    XWPFDocument doc, Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            
            XWPFTable table = doc.getTables().get(numTabla);
            Integer filas = table.getRows().size(); 

            //Agregando filas sin editar a lista tabla
            for (int j = 0; j < filas - 2; j++) {
                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240)); 
                    listaTablaContenido.add(copiedRow);
                }
            }

            //Primera Parte de la tabla

            for (SolicitudPlantacionForestalDetalleEntity element: data) {

                XWPFTableRow row = table.getRow(filas-2);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", element.getEspecieNombreComun() == null ? ""
                                                : element.getEspecieNombreComun());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n2")) {
                                        text = text.replace("n2", element.getEspecieNombreCientifico() == null ? ""
                                                : element.getEspecieNombreCientifico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("33")) {
                                        text = text.replace("33", element.getTotalArbolesExistentes() == null ? ""
                                                : element.getTotalArbolesExistentes().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n4")) { 
                                        text = text.replace("n4", element.getProduccionEstimada() == null ? ""
                                                : element.getProduccionEstimada().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n5")) { 
                                        text = text.replace("n5", element.getCoordenadaEste() == null ? ""
                                                : element.getCoordenadaEste());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n6")) { 
                                        text = text.replace("n6", element.getCoordenadaNorte() == null ? ""
                                                : element.getCoordenadaNorte());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            // Segunda Parte: TOTAL

            XWPFTableRow row = table.getRow(filas-1);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("n7")) {
                                        text = text.replace("n7", data2 == null ? ""
                                                : data2.toString());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }


            for (int j = 0; j < filas; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearAlturaPromedio (SolPlantacionForestalAreaEntity data,
    XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(0);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("altTxt")) {
                                        text = text.replace("altTxt", data.getAlturaPromedio() == null ? ""
                                                : data.getAlturaPromedio().toString());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow);
                }
                table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument  setearBloquesSector (List<AreaBloqueEntity> data, SolPlantacionForestalAreaEntity data2,
    XWPFDocument doc, Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            
            XWPFTable table = doc.getTables().get(numTabla);
            Integer filas = table.getRows().size(); 

            // Primera Parte: TOTAL
            for (int j = 0; j < filas; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("zonTxt")) {
                                        text = text.replace("zonTxt", data2.getZona() == null ? ""
                                                : data2.getZona());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 0; j < filas; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            //Segunda Parte de la tabla

            int addRow = 1;

            for (AreaBloqueEntity e : data) {

                int rowIni = addRow;

                for (AreaBloqueDetalleEntity j : e.getAreaBloqueDetalle()) {

                    addRow++;
                    table.createRow();
                    XWPFTableRow row = table.getRow(addRow);
                    row.createCell();
                    row.createCell();
                    row.createCell();
                    row.getCell(0).setText(e.getBloque());

                    row.getCell(1).setText(j.getNroVertice().toString());
                    row.getCell(2).setText(j.getCoordenadaEste().toString());
                    row.getCell(3).setText(j.getCoordenadaNorte().toString());
                    row.getCell(4).setText(j.getObservacion());
                    row.getCell(5).setText(e.getArea().toString());

                }

                mergeCellVertically(table, 0, rowIni + 1, addRow);
                mergeCellVertically(table, 5, rowIni + 1, addRow);
            }
            
            //Tercera parte de la Tabla (Agregando total)
            
            filas = table.getRows().size(); // 38
            String total = sumAreaTotalHa(data);

            //Agregando total Area Ha()
            table.createRow();
            

            XWPFTableRow row = table.getRow(filas);
                row.createCell();
                row.createCell();
                row.createCell();

                row.getCell(0).setText("");
                row.getCell(1).setText("");
                row.getCell(2).setText("");
                row.getCell(3).setText("");
                row.getCell(4).setText("TOTAL");
                row.getCell(5).setText(total);
                
            //mergeCellHorizontally(table, filas,0,4);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**---------------------------------- FIN SOLICITUD PLANTACIONES FORESTALES------------------------------ */

    static void mergeCellVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            CTVMerge vmerge = (CTVMerge) CTVMerge.Factory.newInstance();
            if (rowIndex == fromRow) {
                // The first merged cell is set with RESTART merge value
                vmerge.setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                vmerge.setVal(STMerge.CONTINUE);
                // and the content should be removed
                for (int i = cell.getParagraphs().size(); i > 0; i--) {
                    cell.removeParagraph(0);
                }
                cell.addParagraph();
            }
            // Try getting the TcPr. Not simply setting an new one every time.
            CTTcPr tcPr = cell.getCTTc().getTcPr();
            if (tcPr == null)
                tcPr = cell.getCTTc().addNewTcPr();
            tcPr.setVMerge(vmerge);
        }
    }

    static void mergeCellHorizontally(XWPFTable table, int row, int fromCol, int toCol) {
        XWPFTableCell cell = table.getRow(row).getCell(fromCol);
        // Try getting the TcPr. Not simply setting an new one every time.
        CTTcPr tcPr = cell.getCTTc().getTcPr();
        if (tcPr == null) tcPr = cell.getCTTc().addNewTcPr();
        // The first merged cell has grid span property set
        if (tcPr.isSetGridSpan()) {
         tcPr.getGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        } else {
         tcPr.addNewGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        }
        // Cells which join (merge) the first one, must be removed
        for(int colIndex = toCol; colIndex > fromCol; colIndex--) {
         table.getRow(row).getCtRow().removeTc(colIndex);
         table.getRow(row).removeCell(colIndex);
        }
    }

    public static String bigdecimalTranfor(Double num){
        String numero = "";

        if(num!=null){
            BigDecimal num1 = new BigDecimal(num);
            BigDecimal num2 = num1.setScale(2, RoundingMode.HALF_UP);
            return numero = num2.toString();
        }else{
            return numero;
        }

    }

    public static String sumAreaTotalHa(List<AreaBloqueEntity> data){

        String numero = "";
        BigDecimal suma = new BigDecimal(0);

        for (AreaBloqueEntity e : data) {
            suma = suma.add(e.getArea());
        }
        BigDecimal total = suma.setScale(2, RoundingMode.HALF_UP);

        return numero = total.toString();
    }

}