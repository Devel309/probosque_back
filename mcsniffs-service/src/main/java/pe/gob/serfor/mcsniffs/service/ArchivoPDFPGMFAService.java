package pe.gob.serfor.mcsniffs.service;

import com.lowagie.text.DocumentException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContrato;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContratoDetalle;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

public interface ArchivoPDFPGMFAService {

    ByteArrayResource consolidadoPGMFA_PDF(Integer idPlanManejo) throws Exception;

}
