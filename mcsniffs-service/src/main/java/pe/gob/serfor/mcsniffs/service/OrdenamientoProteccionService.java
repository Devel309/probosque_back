package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionCategoriaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionDivAdmUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProyecionVigilanciaUMFEntity;

import java.util.List;

public interface OrdenamientoProteccionService {
    ResultEntity RegistrarOrdenamientoProteccionCategoria(List<OrdenProteccionCategoriaUMFEntity> request) ;
    // ResultEntity ActualizarOrdenamientoProteccion(OrdenamientoProteccionEntity ordenamientoProteccionEntity) throws Exception;
    ResultEntity EliminarOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) ;
    ResultEntity ObtenerOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) ;


    //** divicion de administracion */
    ResultEntity RegistrarOrdenamientoProteccionDivAdm(List<OrdenProteccionDivAdmUMFEntity>  request) ;
     ResultEntity EliminarOrdenamientoProteccionDivAdm(OrdenProteccionDivAdmUMFEntity request) ;
    ResultEntity ObtenerOrdenamientoProteccionDivAdm(OrdenProteccionDivAdmUMFEntity request)  ;

    //** vigilancia */
    ResultEntity RegistrarOrdenamientoProteccionVigilancia(List<OrdenProyecionVigilanciaUMFEntity>  request) ;
    ResultEntity EliminarOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request) ;
    ResultEntity ObtenerOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request)  ;

    ResultEntity ListarOrdenamientoProteccion(OrdenamientoProteccionEntity request);
    ResultEntity ListarOrdenamientoInterno(OrdenamientoProteccionEntity request);
    ResultEntity ListarOrdenamientoInternoDetalle(OrdenamientoProteccionEntity request);
    ResultEntity ListarOrdenamientoInternoDetalleTitular(OrdenamientoProteccionEntity request);
    ResultClassEntity RegistrarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request);
    ResultClassEntity ActualizarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request);

    ResultClassEntity ListarOrdenamientoProteccionPorFiltro(Integer idPlanManejo, String codTipoOrdenamiento) throws Exception;
    ResultClassEntity ActualizarOrdenamientoProteccionDetalle(List<OrdenamientoProteccionEntity> request) throws Exception;

    ResultClassEntity proteccionVigilanciaCargarExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                      String codTipoOrdenamiento,String subCodTipoOrdenamiento, String codigoTipoOrdenamientoDet, int idPlanManejo, int idUsuarioRegistro);

    ResultClassEntity registrarOrdenamientoInterno(List<OrdenamientoProteccionEntity> list) throws Exception;
    ResultClassEntity EliminarOrdenamientoInterno(OrdenamientoProteccionDetalleEntity ordenamientoProteccionDetalleEntity) throws Exception;

}
