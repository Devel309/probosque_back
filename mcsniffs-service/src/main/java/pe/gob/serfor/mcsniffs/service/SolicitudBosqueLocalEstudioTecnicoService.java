package pe.gob.serfor.mcsniffs.service;


import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEstudioTecnicoEntity;

public interface SolicitudBosqueLocalEstudioTecnicoService {

    ResultClassEntity listarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj) throws Exception;

    ResultClassEntity actualizarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj) throws Exception;

}
