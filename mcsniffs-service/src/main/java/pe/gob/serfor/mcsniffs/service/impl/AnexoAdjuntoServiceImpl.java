package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.AnexoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AnexoAdjuntoRepository;
import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;
import pe.gob.serfor.mcsniffs.service.AnexoAdjuntoService;
import pe.gob.serfor.mcsniffs.service.CapacitacionService;

import java.util.List;

@Service("AnexoAdjuntoService")
public class AnexoAdjuntoServiceImpl implements AnexoAdjuntoService {



    @Autowired
    private AnexoAdjuntoRepository anexoAdjuntoRepository;

    @Override
    public ResultClassEntity MarcarParaAmpliacionAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception {
        return anexoAdjuntoRepository.MarcarParaAmpliacionAnexoAdjunto(anexoAdjuntoEntity);
    }

    @Override
    public List<AnexoAdjuntoEntity> ListarAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception {
        return anexoAdjuntoRepository.ListarAnexoAdjunto(anexoAdjuntoEntity);
    }

}
