package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.DerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.ListarDerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;

public interface DerechoAprovechamientoService {
    Pageable<List<DerechoAprovechamientoDto>> listarDerechoAprovechamiento(ListarDerechoAprovechamientoDto dto) throws Exception;
}

