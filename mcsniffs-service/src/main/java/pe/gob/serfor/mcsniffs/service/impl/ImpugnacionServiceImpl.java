package pe.gob.serfor.mcsniffs.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.PermisoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Impugnacion.ImpugnacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.ImpugnacionService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ResolucionService;


@Service("ImpugnacionServiceImpl")
public class ImpugnacionServiceImpl implements ImpugnacionService {
    private static final Logger log = LogManager.getLogger(ImpugnacionServiceImpl.class);

    @Autowired
    private ImpugnacionRepository impugnacionRepository;
    @Autowired
    private ArchivoService archivoService;
    @Autowired
    private ResolucionService resolucionService;
    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;
    @Autowired
    private ProcesoPostulacionRepository procesoPostulacionRepository;
    @Autowired
    private PermisoForestalRepository permisoForestalRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;
    @Override
    public ResultClassEntity ValidarImpugnacionResolucion(ImpugnacionDto param) {
        return impugnacionRepository.ValidarImpugnacionResolucion(param);
    }

    @Override
    public ResultClassEntity ListarImpugnacion(ImpugnacionDto param) {
        return impugnacionRepository.ListarImpugnacion(param);
    }

    @Override
    public ResultClassEntity notificarSolicitudImpugnacion(ImpugnacionDto param){
        return impugnacionRepository.notificarSolicitudImpugnacion(param);
    }

    @Override
    public ResultClassEntity ListarResolucionImpugnada(ImpugnacionDto param){
        return impugnacionRepository.ListarResolucionImpugnada(param);
    }
    @Override
    public ResultClassEntity RegistrarImpugnacion(ImpugnacionDto param) throws Exception{

        ResultClassEntity resultClassEntity = impugnacionRepository.RegistrarImpugnacion(param);

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_IMPUGNACION,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MVC_IMPUGNACION + resultClassEntity.getCodigo(),
                param.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    @Override
    public ResultClassEntity ActualizarImpugnacion(Integer idImpugnacion,String tipoImpugnacion, Boolean esfundado , Boolean retrotraer, Integer idUsuarioModificacion, MultipartFile fileEvaluacion, MultipartFile fileFirme){
        ResultClassEntity   response = new ResultClassEntity();
        ResultClassEntity   resolucionResponse = new ResultClassEntity();
        ResultClassEntity<Integer> res = new ResultClassEntity();
        ResultClassEntity<Integer> resFirme = new ResultClassEntity();
        try {
        ImpugnacionDto obj = new ImpugnacionDto();
        ResolucionDto obj2 = new ResolucionDto();
        List<ResolucionDto> listObj = new ArrayList<ResolucionDto>();
        obj.setIdImpugnacion(idImpugnacion);
        response = impugnacionRepository.ObtenerImpugnacion(obj);
        obj = (ImpugnacionDto) response.getData();
        obj2.setIdResolucion(obj.getIdResolucion());
        resolucionResponse=  resolucionService.ObtenerResolucion(obj2);



        listObj=( List<ResolucionDto>)resolucionResponse.getData();
         for (ResolucionDto elemento : listObj){
             obj2= new ResolucionDto();
             obj2=elemento;
         }


        ResolucionDto obj3= new ResolucionDto();


            obj.setTipoImpugnacion(tipoImpugnacion==""?null:tipoImpugnacion);
            obj.setEsfundado(esfundado);
            obj.setRetrotraer(retrotraer);
            obj.setIdUsuarioModificacion(idUsuarioModificacion);
            String estado;
            res.setSuccess(true);
            res.setCodigo(null);
            if(fileEvaluacion!=null &&fileEvaluacion.getSize()>0){
                res=  archivoService.RegistrarArchivoGeneral(fileEvaluacion, idUsuarioModificacion, "TDOCIMPEVAL");
                log.info("ImpugnacionService Archivo: "+res.getMessage()+" "+res.getCodigo());

                if(res.getSuccess()){
                    obj.setIdArchivoEvaluacion(res.getCodigo());
                }
                else{
                    return res;
                }


            }
            else{
                res.setSuccess(true);
                res.setCodigo(null);
            }
            if(fileFirme!=null && fileFirme.getSize()>0){
                resFirme=  archivoService.RegistrarArchivoGeneral(fileFirme, idUsuarioModificacion, "TDOCIMPFIR");
                log.info("ImpugnacionService File Firme: "+resFirme.toString());
                if(resFirme.getSuccess())
                {
                    obj.setIdArchivoFirme(resFirme.getCodigo());
                    obj.setEsfundado(null);
                    response = impugnacionRepository.ActualizarImpugnacion(obj);
                    return response;
                }
                else {
                    return response;
                }
            }
            //RECONSIDERACION
            if(tipoImpugnacion.equals("TIMPREC")){
                log.info("ImpugnacionService Actualizar: "+obj3.toString());
                if(esfundado){
                    estado="EIMPAPROB";
                    obj3.setEstadoResolucion("ERESIMPG");
                    obj3.setIdResolucion(obj2.getIdResolucion());
                    obj3.setIdUsuarioModificacion(idUsuarioModificacion);
                    response = resolucionService.ActualizarResolucionEstado(obj3);
                }
                else{
                    estado="EIMPRECHA";
                }
                obj.setEstadoImpugnacion(estado);
            }
            //APELACION
            if(tipoImpugnacion.equals("TIMPAP")){
                if(esfundado){
                    estado="EIMPAPROB";
                    obj3.setEstadoResolucion("ERESIMPG");
                    obj3.setIdResolucion(obj.getIdResolucion());
                    obj3.setIdUsuarioModificacion(idUsuarioModificacion);
                    response = resolucionService.ActualizarResolucionEstado(obj3);
                }
                else{
                    estado="EIMPRECHA";
                }
                obj.setEstadoImpugnacion(estado);
            }



                if(res.getSuccess()){

                    switch (obj2.getTipoDocumentoGestion()) {

                        case "TDGPROCOFERTA":
                               //PROCESO OFERTA
                            response = new ResultClassEntity();
                            response = impugnacionRepository.ActualizarImpugnacion(obj);
                                 if(response.getSuccess() && esfundado){
                                     response = new ResultClassEntity();
                                     ProcesoPostulacionDto dato = new ProcesoPostulacionDto();
                                     dato.setIdProcesoOferta(obj2.getNroDocumentoGestion());
                                     dato.setIdUsuarioModificacion(idUsuarioModificacion);
                                     response =    procesoPostulacionRepository.ImpugnarGanadorProcesoPostulacion(dato);
                                 }

                            break;

                        case "PERMFOR":
                            response = new ResultClassEntity();
                             String est="";
                            response = impugnacionRepository.ActualizarImpugnacion(obj);

                            if (retrotraer!=null && retrotraer) {
                                est = "EPSFREG";
                                PermisoForestalEntity permisoForestal = new PermisoForestalEntity();
                                permisoForestal.setIdPermisoForestal(obj2.getNroDocumentoGestion());
                                permisoForestal.setCodigoEstado(est);
                                permisoForestal.setIdUsuarioRegistro(idUsuarioModificacion);
                                permisoForestalRepository.actualizarEstadoPermisoForestal(permisoForestal);
                            }
                            else{
                                  if(obj.getEstadoImpugnacion().equals("EIMPAPROB")){
                                      est = "EPSFIMP";
                                      PermisoForestalEntity permisoForestal = new PermisoForestalEntity();
                                      permisoForestal.setIdPermisoForestal(obj2.getNroDocumentoGestion());
                                      permisoForestal.setCodigoEstado(est);
                                      permisoForestal.setIdUsuarioRegistro(idUsuarioModificacion);
                                      permisoForestalRepository.actualizarEstadoPermisoForestal(permisoForestal);
                                  }

                            }

                            break;                            

                        default:
                            response = new ResultClassEntity();
                            ResultClassEntity   responseImpugnacion = new ResultClassEntity();
                            ResolucionDto param=new ResolucionDto();
                            param.setTipoDocumentoGestion(obj2.getTipoDocumentoGestion());
                            param.setNroDocumentoGestion(obj2.getNroDocumentoGestion());
                            Date fechaActual=new Date();
                            param.setFechaResolucion(fechaActual);
                            param.setEstadoResolucion("ERESGEN");
                            param.setIdResolucionPrevia(obj.getIdResolucion());
                            param.setComentarioResolucion(obj2.getComentarioResolucion());
                            param.setIdUsuarioRegistro(idUsuarioModificacion);
                            resolucionService.registrarResolucionBase(param);

                            impugnacionRepository.ActualizarImpugnacion(obj);
                            response.setMessage("Se Actualizo la Impugnación correctamente.");
                            response.setSuccess(true);

                            if(obj.getRetrotraer()){
                                PlanManejoEvaluacionDto plan = new PlanManejoEvaluacionDto();
                                plan.setIdResolucion(obj.getIdResolucion());
                                plan.setIdUsuarioModificacion(idUsuarioModificacion);
                                planManejoEvaluacionRepository.retrotraerPlanManejoEvaluacion(plan);
                            }

                            break;
                    }

                   // return response;
                }


            return response;
        } catch (Exception e) {
            log.info("ImpugnacionService Error: "+e.getMessage());
            response.setMessage("Ocurrio un error.");
            response.setSuccess(false);
           return response;
        }


    }

    @Override
    public ResultClassEntity ObtenerImpugnacion(ImpugnacionDto param) {
        return impugnacionRepository.ObtenerImpugnacion(param);
    }

    @Override
    public ResultClassEntity ActualizarImpugnacionArchivo(ImpugnacionDto param) {
        return impugnacionRepository.ActualizarImpugnacionArchivo(param);
    }

    @Override
    public ResultClassEntity EliminarImpugnacionArchivo(ImpugnacionDto param) throws Exception{

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_IMPUGNACION,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MVC_IMPUGNACION + param.getIdImpugnacion(),
                param.getIdUsuarioElimina());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);


        return impugnacionRepository.EliminarImpugnacionArchivo(param);
    }
}


