package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.ResponsableExperienciaLaboralRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionResponsableRepository;
import pe.gob.serfor.mcsniffs.service.ResponsableExperienciaLaboralService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;

@Service("ResponsableExperienciaLaboralService")
public class ResponsableExperienciaLaboralServiceImpl implements ResponsableExperienciaLaboralService {

    @Autowired
    private ResponsableExperienciaLaboralRepository responsableExperienciaLaboralRepository;

    @Autowired
    private SolicitudConcesionResponsableRepository repositorySolicitudConcesionResponsable;

    @Autowired
    private SolicitudConcesionService servicioSolicitudConcesion;

    @Override
    public ResultClassEntity obtenerResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj)
            throws Exception {
        return responsableExperienciaLaboralRepository.obtenerResponsableExperienciaLaboral(obj);

    }

    @Override
    public ResultClassEntity listarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj)
            throws Exception {
        return responsableExperienciaLaboralRepository.listarResponsableExperienciaLaboral(obj);

    }

    @Override
    public ResultClassEntity registrarResponsableExperienciaLaboral(SolicitudConcesionResponsableDto obj)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();

        // result =
        // repositorySolicitudConcesionResponsable.registrarSolicitudConcesionResponsable(obj);

        List<ResponsableExperienciaLaboralDto> items = obj.getLstResponsableExperienciaLaboral();
        List<SolicitudConcesionArchivoEntity> adjuntos = obj.getLstResponsableExperienciaLaboralArchivo();

        if (items != null && !items.isEmpty()) {
            for (ResponsableExperienciaLaboralDto element : items) {
                result = responsableExperienciaLaboralRepository.registrarResponsableExperienciaLaboral(element);
            }
        }

        if (adjuntos != null && !adjuntos.isEmpty()) {
            for (SolicitudConcesionArchivoEntity archivo : adjuntos) {
                result = servicioSolicitudConcesion.registrarArchivoSolicitudConcesion(archivo);
            }
        }

        return result;

    }

    @Override
    public ResultClassEntity actualizarResponsableExperienciaLaboral(SolicitudConcesionResponsableDto obj)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();

        // result =
        // repositorySolicitudConcesionResponsable.actualizarSolicitudConcesionResponsable(obj);

        List<ResponsableExperienciaLaboralDto> items = obj.getLstResponsableExperienciaLaboral();
        List<SolicitudConcesionArchivoEntity> adjuntos = obj.getLstResponsableExperienciaLaboralArchivo();

        if (items != null && !items.isEmpty()) {
            for (ResponsableExperienciaLaboralDto element : items) {
                result = responsableExperienciaLaboralRepository.actualizarResponsableExperienciaLaboral(element);
            }
        }

        if (adjuntos != null && !adjuntos.isEmpty()) {
            for (SolicitudConcesionArchivoEntity archivo : adjuntos) {
                if (archivo.getIdSolicitudConcesionArchivo() > 0)
                    result = servicioSolicitudConcesion.actualizarArchivoSolicitudConcesion(archivo);
                else
                    result = servicioSolicitudConcesion.registrarArchivoSolicitudConcesion(archivo);
            }
        }

        return result;

    }

    @Override
    public ResultClassEntity eliminarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj)
            throws Exception {
        return responsableExperienciaLaboralRepository.eliminarResponsableExperienciaLaboral(obj);

    }

}
