package pe.gob.serfor.mcsniffs.service;

//service interfaces
//Guardar archivos solicitudacceso /return archivo

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.io.File;

public interface ArchivoSolicitudAccesoService{
    ResultClassEntity<ArchivoEntity> insertarArchivoSolicitudAcceso(MultipartFile file, Integer idSolicitudAcceso,String tipoArchivo);
    ResultArchivoEntity descargarArchivoSolicitudAcceso(Integer idSolicitudAcceso);
    ResultClassEntity<ArchivoEntity> obtenerArchivoSolicitud(Integer idSolicitudAcceso);
    ResultClassEntity registrarDetalleArchivo(ArchivoSolicitudEntity item) throws Exception;

    ResultEntity<ArchivoSolicitudEntity>listarDetalleArchivo(Integer idPlanManejo) throws Exception;
    ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception;
}