package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalGeometriaRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalGeometriaService;

@Service("SolicitudBosqueLocalGeometriaService")
public class SolicitudBosqueLocalGeometriaServiceImpl implements SolicitudBosqueLocalGeometriaService{
    @Autowired
    private SolicitudBosqueLocalGeometriaRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarSolicitudBosqueLocalGeometria(List<SolicitudBosqueLocalGeometriaEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                repository.registrarSolicitudBosqueLocalGeometria(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se registraron correctamente.");
        result.setSuccess(true);
        return result;

    }

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalGeometria(SolicitudBosqueLocalGeometriaEntity item) {
        return repository.listarSolicitudBosqueLocalGeometria(item);
    }

    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalGeometriaArchivo(SolicitudBosqueLocalGeometriaEntity item) {
        return repository.eliminarSolicitudBosqueLocalGeometriaArchivo(item);
    }

    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocalGeometria(List<SolicitudBosqueLocalGeometriaEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        for (SolicitudBosqueLocalGeometriaEntity element : items) {
            result = repository.actualizarSolicitudBosqueLocalGeometria(element);
        }

        return result;
    }
}
