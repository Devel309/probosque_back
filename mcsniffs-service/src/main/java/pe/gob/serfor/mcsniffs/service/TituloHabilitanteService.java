package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;

import java.util.List;

public interface TituloHabilitanteService {

    ResultClassEntity registrarTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception;
    ResultClassEntity actualizarTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception;
    ResultClassEntity actualizarCodigoCifradoTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception;
    List<TituloHabilitanteEntity> listarTituloHabilitante(TituloHabilitanteEntity param) throws Exception;
    ResultClassEntity eliminarTituloHabilitante(TituloHabilitanteEntity tituloHabilitanteEntity) throws Exception;
    ResultClassEntity listarInformacionTH(InformacionTHDto param) throws Exception;
}
