package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPMFICService {
    ByteArrayResource consolidadoPMFIC_PDF(Integer idPlanManejo) throws Exception;
}
