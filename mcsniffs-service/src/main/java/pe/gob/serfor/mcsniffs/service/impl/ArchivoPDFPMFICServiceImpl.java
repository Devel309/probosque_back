package pe.gob.serfor.mcsniffs.service.impl;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.constant.PmficType;
import pe.gob.serfor.mcsniffs.service.util.DocUtilPmfic;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ArchivoPDFPMFICService")
public class ArchivoPDFPMFICServiceImpl implements  ArchivoPDFPMFICService{

    @Autowired
    private  SolicitudAccesoServiceImpl acceso;

    @Autowired
    private PlanManejoRepository planManejoRepository;

    @Autowired
    private PlanManejoContratoRepository planManejoContratoRepository;

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private SistemaManejoForestalRepository smfRepo;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;

    @Autowired
    private SistemaManejoForestalRepository sistema;

    @Autowired
    private ImpactoAmbientalService ambiental;

    @Autowired
    CensoForestalRepository censoForestalDetalleRepo;

    @Autowired
    ZonificacionRepository zonificacionRepository;

    @Autowired
    CoreCentralRepository corecentral;
/*
    @Autowired
    SolicitudAccesoRepository acceso;*/

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

    @Autowired
    private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ArchivoRepository archivoRepository;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;


    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private SistemaManejoForestalService sistemaManejoForestalService;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font cabecera= new Font(Font.HELVETICA, 9f, Font.BOLD);
    Font cabeceraPeque= new Font(Font.HELVETICA, 7f, Font.BOLD);
    Font subTituloTabla= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font letraPeque = new Font(Font.HELVETICA, 6f, Font.COURIER);


    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: RAFAEL AZAÑA
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con itext}
     */
    @Override
    public ByteArrayResource consolidadoPMFIC_PDF(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPMFIC.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("consolidadoPMFIC", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPdfPMFIC(os,idPlanManejo);
        os.flush();
        os.close();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }

    public  void createPdfPMFIC(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);
        try{
            PdfWriter writer = PdfWriter.getInstance(document, os);

            document.open();
            Paragraph titlePara1 = new Paragraph("PLAN DE MANEJO INTERMEDIO",titulo);
            titlePara1.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph(titlePara1));
            document.add(new Paragraph("1. INFORMACION GENERAL",subTitulo));
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable table1 = createTableInformacionGeneral(writer,idPlanManejo);
            document.add(table1);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("2. OBJETIVOS DEL MANEJO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("2.1 Objetivo General",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table2 = createObjetivoGeneral(writer,idPlanManejo);
            document.add(table2);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("2.2 Objetivos Especificos (marca con 'x')",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table3 = createObjetivoEspecifico(writer,idPlanManejo);
            document.add(table3);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("3. INFORMACION BASICA",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Se deben presentar como anexos los mapas del anexo 1 y 2 conteniendo la siguiente información," +
                    " de acuerdo al nivel de planificación o escala de aprovechamiento",subTitulo));
            document.add(new Paragraph("\r\n"));

            document.add(new Paragraph("3.1. Ubicación de la comunidad",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table5=createInfBasica1(writer,idPlanManejo);
            document.add(table5);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("3.2. Superficie y ubicación de la UMF y PC",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table6=createInfBasica2(writer,idPlanManejo);
            document.add(table6);


            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("3.3. Zonificación interna de la UMF",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table7=createInfBasica3(writer,idPlanManejo);
            document.add(table7);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("3.4. Accesibilidad a la UMF",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table8=createInfBasica4(writer,idPlanManejo);
            document.add(table8);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.\tASPECTOS FÍSICOS\n" +
                    "\n" +
                    "4.1.\tHidrografía de la UMF\n",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table9=createAspectosFisicos1(writer,idPlanManejo);
            document.add(table9);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.2.\tFisiografía de la UMF",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table10=createAspectosFisicos2(writer,idPlanManejo);
            document.add(table10);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5.\tASPECTOS BIOLÓGICOS\n" +
                    "\n" +
                    "5.1.\tFauna silvestre",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("La metodología empleada para la identificación de las especies de fauna silvestre fue mediante el avistamiento, observación de huellas, desechos fecales, cantos de avifauna, madrigueras, nidos, etc., que se encontraron en las unidades muestrales.\n" +
                    "También se valoró la transmisión de información de la población de la comunidad, que fue efectuado mediante entrevistas que se realizó, información que nos permite asegurar la presencia de la fauna silvestre por ser su fuente de alimentación mediante la caza de subsistencia y pesca; estos datos han sido contrastados con la revisión del Mapa Ecológico del Perú y otras publicaciones, lo cual nos permitió ratificar su existencia.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table11=createAspectosBiologicos1(writer,idPlanManejo);
            document.add(table11);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Estas especies por su clasificación de especie amenazada se encuentran descritos dentro del apéndice de CITES para Perú y en la categorización del D.S. N° 004-2014-MINAGRI; en ese sentido en cumplimiento al mencionado decreto, se prohibirá la caza, captura, tenencia y transporte de todos los especímenes de fauna silvestre durante las actividades de aprovechamiento por parte del personal, debiendo ser sancionado el incumplimiento mediante medidas correctivas dictadas.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5.2.\tTipos de bosque",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table12=createAspectosBiologicos2(writer,idPlanManejo);
            document.add(table12);
            Rectangle two = new Rectangle(842.0F,595.0F );
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.\tINFORMACION SOCIOECONÓMICA\n" +
                    "\n" +
                    "6.1.\tCaracterización de la comunidad",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table13=createInfoSocio1(writer,idPlanManejo);
            document.add(table13);
             two = new Rectangle(595.0F,842.0F);
            document.setPageSize(two);
            document.setMargins(60, 60, 40, 40);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.2.\tInfraestructura de servicios ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("La comunidad no cuenta con servicios de infraestructura establecidos por estado, siendo la localidad de Atalaya la más próxima donde se ubican los servicios básicos de educación, salud y otros de atención gubernamental.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table14=createInfoSocio2(writer,idPlanManejo);
            document.add(table14);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.3.\tAntecedentes de identificación de conflictos ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table15=createInfoSocio3(writer,idPlanManejo);
            document.add(table15);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.\tORDENAMIENTO Y PROTECCIÓN DE LA UMF\n" +
                    "\n" +
                    "7.1.\tSuperficie y ubicación de los bloques ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table16=createOrdenamiento1(writer,idPlanManejo);
            document.add(table16);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.2.\tSuperficie y ubicación de la parcela de corta del año operativo ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table17=createOrdenamiento2(writer,idPlanManejo);
            document.add(table17);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.3.\tSuperficie y ubicación de la parcela de corta del año operativo ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table18=createOrdenamiento3(writer,idPlanManejo);
            document.add(table18);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.4.\tMedidas de protección de la unidad de manejo forestal ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table19=createOrdenamiento4(writer,idPlanManejo);
            document.add(table19);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.\tSISTEMA DE MANEJO FORESTAL DE USO MULTIPLE ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table20=createSistManejo1(writer,idPlanManejo);
            document.add(table20);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.1.\tManejo forestal con fines maderables\n" +
                    "\n" +
                    "9.1.1.\tSistema de manejo forestal con fines maderables ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Este sistema consistirá en realizar la cosecha  de una parte del volumen aprovechable (por debajo de la corta anual permisible) y las cortas se efectuarán en determinados ciclos (ciclos de corta); la regulación de la producción de este sistema se hará con base al área (las parcelas de corta anual), al ciclo de corta y al diámetro mínimo de corta (DMC) por especie; además, de estas restricciones a la corta, se reservará y protegerá árboles semilleros (principalmente de las especies de mayor presión de extracción) y árboles de especies incluidas en la lista oficial de especies protegidas; siendo su objetivo producir cosechas periódicas provenientes de las clases diamétrica menores al diámetro mínimo de corta (DMC) y mantener el dosel del bosque sin cambios drásticos. \n" +
                    "Asimismo, la aplicación del sistema policíclico se regirá esencialmente por la práctica de aprovechamiento controlado, que permitan minimizar los daños al bosque (= aprovechamiento de impacto reducido) acompañado del manejo silvicultural del bosque aprovechado, que se basará principalmente en el manejo de árboles remanentes por encima de los 30 cm DAP y la regeneración de regeneración natural, considerando especies actualmente comerciales, como de aquellas con potencial comercial futuro. \n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table21=createSistManejo2(writer,idPlanManejo);
            document.add(table21);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.1.2.\tCiclo de corta ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("El plan de manejo propone un ciclo de corta de 10 años, cuyos principios estará basado en:\n" +
                    "\uF0D8\tEn los resultados del censo que reporta 38 especies maderables de interés comercial actual, todos registrados por encima del diámetro mínimo de corta.\n" +
                    "\uF0D8\tDurante el censo se ha observado la existencia especies remanentes por debajo de los diámetros de corta que aseguran la capacidad productiva del bosque en un turno próximo.\n" +
                    "\uF0D8\tAsimismo, la extracción solo alcanzará a las especies actualmente comerciales y que cubran los costos de extracción, quedando especies potenciales.\n" +
                    "\uF0D8\tSe plantea la conservación del 20% de árboles censados como semilleros, todos con buena característica fenotípicas que aseguran la diseminación de semillas.\n" +
                    "\uF0D8\tLo descrito asegura la sostenibilidad del bosque y nos asegura intervenir nuevamente el área para el aprovechamiento forestal al termino de 10 años, donde se prevé que los árboles remanentes alcancen los DMC y las especies potenciales encuentren mercado.\n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table22=createSistManejo3(writer,idPlanManejo);
            document.add(table22);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.1.3.\tActividades de aprovechamiento y equipos a utilizar con fines maderables ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table23=createSistManejo4(writer,idPlanManejo);
            document.add(table23);
            two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40,40,40,40);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.2.\tManejo forestal con fines no maderables\n" +
                    "9.2.1.\tSistema de manejo forestal con fines no maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table24=createSistManejo5(writer,idPlanManejo);
            document.add(table24);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.2.2.\tCiclo de aprovechamiento",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table25=createSistManejo6(writer,idPlanManejo);
            document.add(table25);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.2.3.\tActividades de aprovechamiento y equipos a utilizar con fines maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table26=createSistManejo7(writer,idPlanManejo);
            document.add(table26);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9.3.\tLabores silviculturales\n" +
                    "\n" +
                    "Obligatorias",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table27=createSistManejo8(writer,idPlanManejo);
            document.add(table27);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Opcionales",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table28=createSistManejo9(writer,idPlanManejo);
            document.add(table28);
            two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("10.\tEVALUACIÓN AMBIENTAL",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table17_ = createEvaluacionAmbientalA(writer,idPlanManejo);
            document.add(table17_);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("10.1. Acciones orientadas a minimizar impactos\n" +
                    "10.1.1.\tAcciones de carácter preventivo – corrector",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table29=createPlanAccionEvaluacionImpactoAmbiental(writer,idPlanManejo);
            document.add(table29);
            two = new Rectangle(595.0F,842.0F);
            document.setPageSize(two);
            document.setMargins(60, 60, 30, 30);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("10.1.2. Acciones de Vigilancia y Seguimiento ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table30=createPlanVigilanciaEvaluacionImpactoAmbiental(writer,idPlanManejo);
            document.add(table30);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("10.2.\tAcciones de contingencia ambiental",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table31=createPlanContingenciaEvaluacionImpactoAmbiental(writer,idPlanManejo);
            document.add(table31);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("11.\tORGANIZACIÓN PARA EL DESARROLLO DE ACTIVIDADES",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("La comunidad actualmente tiene como órgano de gobierno a la Asamblea General o Comunal que está compuesta por todo el comunero inscrito en el padrón; asimismo cuenta con un Concejo Directivo, responsable de gobierno y administración de la comunidad, y está constituido por el jefe de la Comunidad, Sub jefe, secretario, tesorero, fiscal y vocal por un periodo de dos años.\n" +
                    "Para la administración del bosque, se tiene proyectado la conformación de un Comité de Manejo de Bosque Comunal y la contratación de un regente forestal con mención en comunidades nativas, el mismo que se encargara de administrar, supervisar y monitorear, la implementación del PMFI.\n" +
                    "Como estrategia de operatividad para el aprovechamiento, la comunidad tiene suscrito un contrato de asociación con una empresa conexos a la actividad forestal, esta empresa se encargará de la operaciones de aprovechamiento con actividades que estarán enmarcadas en hacer el censo forestal, tala, arrastre, acopio, transporte y la transformación para la comercialización; por su lado la comunidad dentro de las operaciones forestales efectuará actividades de supervisión, monitoreo, vigilancia, cubicación, liquidación comercial al estado natural, emisión de factura, la expedición y suscripción de la  Guía de Transporte Forestal para el transporte de los productos.\n" +
                    "Dentro de su programa tiene previsto las visitas frecuentes al área bajo permiso de extracción forestal, para determinar la presencia de extractores ilegales e invasores (agricultura migratoria) y hacer la correspondiente denuncia de existir estos actos ilegales; del mismo modo, para conocer el sistema de extracción de bajo impacto que se está empleando, así como supervisar los sistemas de tratamiento silvicultural que salvaguardaran la sostenibilidad del bosque y hacerlos productivos en el tiempo.\n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("11.1. Aprovechamiento forestal maderable",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table32=createActividadSilviculturalOrganizPMFIC1(writer,idPlanManejo);
            document.add(table32);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("11.2. Aprovechamiento forestal no maderable",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table33=createActividadSilviculturalOrganizPMFIC2(writer,idPlanManejo);
            document.add(table33);
            //two = new Rectangle(842.0F,595.0F);
            two = new Rectangle(1191.0F, 842.0F);
            document.setPageSize(two);
            document.setMargins(30, 30, 30, 30);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("12.\tCRONOGRAMA DE ACTIVIDADES",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table34=createCronogramaActividadesPMFICAnios(writer,idPlanManejo);
            document.add(table34);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("13.\tRENTABILIDAD DEL MANEJO FORESTAL",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("INGRESOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table35=createRentabilidadIngresos(writer,idPlanManejo);
            document.add(table35);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Egresos",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table36=createRentabilidadEgresos(writer,idPlanManejo);
            document.add(table36);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("14: ASPECTOS COMPLEMENTARIOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table37=createAspectosCompPGMFA(writer,idPlanManejo);
            document.add(table37);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("15: ANEXOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 1: Mapa base",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 2: Mapa de ordenamiento y división administrativa",subTitulo));
            document.add(new Paragraph("\r\n\n\n\n\n\n\n"));

            document.add(new Paragraph("Anexo 3: Resultados del inventario de muestreo",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table43;
            //Anexo 3
            List<List<Anexo2PGMFDto>> listaAnexo3=censoForestalRepository.ResultadosPMFICAnexo3(idPlanManejo, "PMFIC");
            if(listaAnexo3!=null) {
                for (List<Anexo2PGMFDto> detalle : listaAnexo3) {
                    if(detalle.size()>0) {
                        for (Anexo2PGMFDto detalle2 : detalle) {
                            table43 = createAnexos3(writer,detalle2);
                            document.add(table43);
                            document.add(new Paragraph("\r\n"));
                        }
                    }
                }
            }

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 4: Resultados del inventario de fustales por especie",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table38 ;


            //Anexo 4
            List<List<Anexo3PGMFDto>> lstLista= censoForestalRepository.ResultadosPMFICAnexo4(idPlanManejo,"PMFIC");
            if(lstLista!=null) {
                for (List<Anexo3PGMFDto> detalle1 : lstLista) {
                    if (detalle1 != null) {
                        for (Anexo3PGMFDto detalle : detalle1) {
                            table38 = createAnexos4(writer,detalle);
                            document.add(table38);
                            document.add(new Paragraph("\r\n"));
                        }
                    }
                }
            }

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 5: Resultados del censo forestal comercial con fines maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table39 = createAnexos5(writer,idPlanManejo);
            document.add(table39);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 6: Resultados del inventario forestal con fines no maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table40;


            String nombreBosque="";
            String areaBosque="";
            List<TablaAnexo6Dto> listaDetalle = new ArrayList<TablaAnexo6Dto>();
            //Anexo 6
            List <ResultadosAnexo6> listaAnexo6=censoForestalRepository.ResultadosAnexo6(idPlanManejo,"PMFIC");
            int z=0;

            if(listaAnexo6!=null) {
                for (ResultadosAnexo6 detalle1 : listaAnexo6) {
                    nombreBosque = detalle1.getNombreBosque() != null ? detalle1.getNombreBosque() : "";
                    areaBosque = detalle1.getAreaBosque() != null ? detalle1.getAreaBosque() : "";
                    if (listaAnexo6.size() > 0) {
                        ResultadosAnexo6 PMFICLista = listaAnexo6.get(z);
                        listaDetalle = PMFICLista.getListaTablaAnexo6Dto();
                    }
                    if (listaDetalle != null) {

                        for (TablaAnexo6Dto detalle : listaDetalle) {
                            table40=createAnexos6(writer,detalle,nombreBosque,areaBosque);
                            document.add(table40);
                            document.add(new Paragraph("\r\n"));
                        }
                    }
                    z++;
                }
            }

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 7: Resultados del censo forestal comercial con fines no maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table41=createAnexos7(writer,idPlanManejo);
            document.add(table41);


            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 8: Lista de especies mencionadas",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table42=createAnexos8(writer,idPlanManejo);
            document.add(table42);

            document.add(new Paragraph("Anexo 9: Acta de asamblea comunal",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Acta de asamblea comunal",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("( En el que conste el acuerdo para el ordenamiento interno, la zonificación interna comunal, la decisión del aprovechamiento del recurso y el compromiso de implementar el PMFI )",subTitulo2));
            document.add(new Paragraph("\r\n"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable createTableInformacionGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        //InformacionGeneral
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PMFIC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData()!=null?infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().size()>0?infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0):null:null;
        cell = new PdfPCell(new Paragraph("1.1. De los datos generales de la Comunidad",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº de Permiso",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getIdPermisoForestal()!=null?o.getIdPermisoForestal().toString():"",contenido));
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre de la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getNombreElaboraDema()!=null?o.getNombreElaboraDema().toString():"",contenido));
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre del Jefe de la Comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(toString(o.getNombreRepresentante()+" "+o.getApellidoPaternoRepresentante()+" "+o.getApellidoMaternoRepresentante()),contenido));
        } else {
          cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº DNI del jefe de la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getDocumentoRepresentante()!=null?toString(o.getDocumentoRepresentante()):"",contenido));
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Federación u organización al que pertenece",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getFederacionComunidad()!=null?toString(o.getFederacionComunidad()):"",contenido));
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Departamento",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getDepartamento()!=null?toString(o.getDepartamento()):"",contenido));
        } else {
          cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Provincia",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getProvincia()!=null?toString(o.getProvincia()):"",contenido));
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Distrito",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
        cell = new PdfPCell(new Paragraph(o.getDistrito()!=null?toString(o.getDistrito()):"",contenido));
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Cuenca",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        String cuenca="";
        cuenca=o.getCuenca()!=null?toString(o.getCuenca()):null;
        CuencaEntity param = new CuencaEntity();
        param.setIdCuenca(Integer.parseInt(cuenca));
        ResultClassEntity listarCuencaSubcuenca= corecentral.listarCuencaSubcuenca(param);
        List<CuencaEntity> lista = new ArrayList<CuencaEntity>();
        if(listarCuencaSubcuenca.getData()!=null) {
            lista=(List<CuencaEntity>) listarCuencaSubcuenca.getData();
        }
        if (o != null) {
            if(lista.size()>0) {
                for (CuencaEntity cuencaEntity : lista) {
                    cell = new PdfPCell(new Paragraph(cuencaEntity.getCuenca()!=null?toString(cuencaEntity.getCuenca()):"",contenido));
                }
            }else{
                cell = new PdfPCell(new Paragraph("",contenido));
            }
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Subcuenca",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if (o != null) {
            if(lista.size()>0) {
                for (CuencaEntity cuencaEntity : lista) {
                    cell = new PdfPCell(new Paragraph(cuencaEntity.getSubCuenca()!=null?toString(cuencaEntity.getSubCuenca()):"",contenido));
                }
            }else{
                cell = new PdfPCell(new Paragraph("",contenido));
            }
        } else {
            cell = new PdfPCell(new Paragraph("", contenido));
        }
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Área total (ha)",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getAreaTotal()!=null?toString(o.getAreaTotal()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº de resolución de reconocimiento de la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroResolucionComunidad()!=null?toString(o.getNroResolucionComunidad()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº de título de propiedad de la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroTituloPropiedadComunidad()!=null?toString(o.getNroTituloPropiedadComunidad()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº RUC de la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroRucComunidad()!=null?toString(o.getNroRucComunidad()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº total de familias de la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroTotalFamiliasComunidad()!=null?toString(o.getNroTotalFamiliasComunidad()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.2. Datos del PMFI",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Periodo de vigencia del PGMFI de vigencia del PGMFI (mes/año a mes/año)",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getVigencia()!=null?toString(o.getVigencia()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha de inicia",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaInicioDema()!=null?formatter.format(o.getFechaInicioDema()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha de finalización",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaFinDema()!=null?formatter.format(o.getFechaFinDema()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Área total de la UMF",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getAreaBosqueProduccionForestal()!=null?toString(o.getAreaBosqueProduccionForestal()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.3. Datos del Regente",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre y apellidos",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!=null?toString(o.getRegente().getNombres()+" "+o.getRegente().getApellidos()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº DNI",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!=null?toString(o.getRegente().getNumeroDocumento()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Domicilio legal",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!=null?toString(o.getRegente().getDomicilioLegal()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Certificado de habilitación profesional",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº de inscripción en el Registro Nacional de Regentes.",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!=null?toString(o.getRegente().getNumeroLicencia()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.4. Datos del Regente",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº de familias involucradas en el aprovechamiento forestal",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroPersonasInvolucradas()!=null?toString(o.getNroPersonasInvolucradas()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº total de árboles maderables para aprovechar",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroArbolesMaderables()!=null?toString(o.getNroArbolesMaderables()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº total de árboles maderables semilleros ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroArbolesMaderablesSemilleros()!=null?toString(o.getNroArbolesMaderablesSemilleros()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Superficie para el aprovechamiento maderable (ha)",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getSuperficieHaMaderables()!=null?toString(o.getSuperficieHaMaderables()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Volumen de maderable total para aprovechar (m3) ®",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getVolumenM3rMaderables()!=null?toString(o.getVolumenM3rMaderables()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("1.5.\tInformación del Aprovechamiento No Maderable",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº total de árboles no maderables para aprovechar",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroArbolesNoMaderables()!=null?toString(o.getNroArbolesNoMaderables()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº total de árboles no maderables semilleros ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroArbolesNoMaderablesSemilleros()!=null?toString(o.getNroArbolesNoMaderablesSemilleros()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Superficie para el aprovechamiento no maderable (ha) ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getSuperficieHaNoMaderables()!=null?toString(o.getSuperficieHaNoMaderables()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Volumen no maderable total para aprovechar (m3) ® ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getVolumenM3rNoMaderables()!=null?toString(o.getVolumenM3rNoMaderables()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("1.6.\tDe los regímenes promocionales para el aprovechamiento forestal",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("SI",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("NO",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("NO APLICA",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);


        for(InformacionGeneralDetalle det: o.getLstUmf()){
            cell = new PdfPCell(new Paragraph(det.getDescripcion()!=null?toString(det.getDescripcion()):"",contenido));
            cell.setFixedHeight(25);
            table.addCell(cell);
            if(det.getDetalle()!= null || !det.getDetalle().equals("")){
                cell = new PdfPCell(new Paragraph(det.getDetalle().equals("SI")?"X":"",contenido));
            }else{cell = new PdfPCell(new Paragraph("",contenido));}
            cell.setFixedHeight(25);
            table.addCell(cell);
            if(det.getDetalle()!= null || !det.getDetalle().equals("")){
                cell = new PdfPCell(new Paragraph(det.getDetalle().equals("NO")?"X":"",contenido));
            }else{cell = new PdfPCell(new Paragraph("",contenido));}
            cell.setFixedHeight(25);
            table.addCell(cell);
            if(det.getDetalle()!= null || !det.getDetalle().equals("")){
                cell = new PdfPCell(new Paragraph(det.getDetalle().equals("No Aplica")?"X":"",contenido));
            }else{cell = new PdfPCell(new Paragraph("",contenido));}
            cell.setFixedHeight(25);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Paragraph("1.7.\tDel contrato con terceros para el aprovechamiento forestal:",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre de la empresa",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNombreElaboraDema()!=null?toString(o.getNombreElaboraDema()):"",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Domicilio fiscal de la empresa",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDireccionLegalTitular()!=null?toString(o.getDireccionLegalTitular()):"",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre del representante legal de la empresa",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNombreRepresentante()!=null?toString(o.getNombreRepresentante()+" "+o.getApellidoPaternoRepresentante()+" "+o.getApellidoMaternoRepresentante()):"",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Domicilio del representante legal de la empresa",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDireccionLegalRepresentante()!=null?toString(o.getDireccionLegalRepresentante()):"",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DNI del representante legal de la empresa",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDocumentoRepresentante()!=null?toString(o.getDocumentoRepresentante()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("RUC de la empresa",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDniElaboraDema()!=null?toString(o.getDniElaboraDema()):"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Copia del contrato para el aprovechamiento forestal",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);


        return table;
    }

    public  PdfPTable createObjetivoGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font contenido= new Font(Font.HELVETICA, 11f, Font.COURIER);

        //2 - Objetivos de Manejo
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PMFIC", idPlanManejo);

        int contador=0;
        for(ObjetivoDto ob : lstObjetivos.getData()){
            if(contador==0){
                cell = new PdfPCell(new Paragraph(ob.getDescripcion()!=null?ob.getDescripcion():"",contenido));
                cell.setFixedHeight(50);
                table.addCell(cell);
            }else{
                break;
            }

            contador++;
        }

        return table;
    }

    public  PdfPTable createObjetivoEspecifico(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font contenido= new Font(Font.HELVETICA, 10f, Font.COURIER);

        //2 - Objetivos de Manejo
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PMFIC", idPlanManejo);
        java.util.List<ObjetivoDto> maderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("Productos a obtener del manejo ",titulo));
        cell.setColspan(3);
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maderable",subTitulo));
        cell.setRowspan(maderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaM="";
        for(ObjetivoDto obM : maderables){
            if(obM.getActivo().equals("A")){
                marcaM="(X)";
            }else{marcaM="";}
            cell = new PdfPCell(new Paragraph(marcaM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }


        List<ObjetivoDto> NMaderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("No Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("No Maderable",subTitulo));
        cell.setRowspan(NMaderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaNM="";
        for(ObjetivoDto obNM : NMaderables){
            if(obNM.getActivo().equals("I")){
                marcaNM="(X)";
            }else{marcaNM="";}
            cell = new PdfPCell(new Paragraph(marcaNM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obNM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }


    public  PdfPTable createInfBasica1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //3 -INFORMACION BASICA DEL AREA DE MANEJO
        //3.1
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAUC");


        cell = new PdfPCell(new Paragraph("Vertice",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



        if(lstInfoAreaAcreditacionTerritorio!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoAreaAcreditacionTerritorio) {

                cell = new PdfPCell(new Paragraph( detalle.getPuntoVertice() != null ? detalle.getPuntoVertice().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }



    public  PdfPTable createInfBasica2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //3 -INFORMACION BASICA DEL AREA DE MANEJO
        //3.2
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBASUUMF");

        cell = new PdfPCell(new Paragraph("Anexo",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vertice",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(40);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        if(lstInfoAreaAcreditacionTerritorio!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoAreaAcreditacionTerritorio) {

                cell = new PdfPCell(new Paragraph( detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( detalle.getPuntoVertice() != null ? detalle.getPuntoVertice().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createInfBasica3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //3 -INFORMACION BASICA DEL AREA DE MANEJO
        //3.3
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAZIUMF");

        cell = new PdfPCell(new Paragraph("Zonificación de la UMF",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(4);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Categoría",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Area (ha)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);


        if(lstInfoAreaAcreditacionTerritorio!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoAreaAcreditacionTerritorio) {

                cell = new PdfPCell(new Paragraph( detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHaPorcentaje() != null ? detalle.getAreaHaPorcentaje().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getObservaciones() != null ? detalle.getObservaciones().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createInfBasica4(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //3 -INFORMACION BASICA DEL AREA DE MANEJO
        //3.4
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAAUMF");

        cell = new PdfPCell(new Paragraph("Ruta Principal",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Categoría",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Area (ha)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);


        if(lstInfoAreaAcreditacionTerritorio!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoAreaAcreditacionTerritorio) {

                cell = new PdfPCell(new Paragraph( detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDistanciaKm() != null ? detalle.getDistanciaKm().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getTiempo() != null ? detalle.getTiempo().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getMedioTransporte() != null ? detalle.getMedioTransporte().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }



    public  PdfPTable createAspectosFisicos1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //4- ASPECTOS FISICOS PMFICAF
        List<InfBasicaAereaDetalleDto> lstInfoBasica = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAAFHUMF");

        cell = new PdfPCell(new Paragraph("Hidrografia de la UTM",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Rio",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Quebrada",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Cocha",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        if(lstInfoBasica!=null) {
            //4.1 - HIDROGRAFIA 9 PMFICAF PMFICAFHUMF
            List<InfBasicaAereaDetalleDto> lstInfoBasicaHID = lstInfoBasica.stream().filter(p-> p.getCodInfBasicaDet().equals("PMFICINFBAAFHUMF")).collect(Collectors.toList());
            if(lstInfoBasicaHID!=null) {
                List<InfBasicaAereaDetalleDto> lstInfoBasicaHIDDet = lstInfoBasica.stream().filter(p-> p.getDescripcion()!=null).collect(Collectors.toList());
                if(lstInfoBasicaHIDDet!=null) {
                    List<InfBasicaAereaDetalleDto> lstInfoBasicaRio = lstInfoBasicaHIDDet.stream().filter(p -> p.getDescripcion().equals("Rio")).collect(Collectors.toList());
                    if (lstInfoBasicaRio != null) {
                        for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaRio) {
                            cell = new PdfPCell(new Paragraph(detalle.getNombreRio() != null ? detalle.getNombreRio().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph( "",contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<InfBasicaAereaDetalleDto> lstInfoBasicaQuebrada = lstInfoBasicaHIDDet.stream().filter(p -> p.getDescripcion().equals("Quebradas")).collect(Collectors.toList());
                    if (lstInfoBasicaQuebrada != null) {
                        for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaQuebrada) {
                            cell = new PdfPCell(new Paragraph(detalle.getNombreQuebrada() != null ? detalle.getNombreQuebrada().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph( "",contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<InfBasicaAereaDetalleDto> lstInfoBasicaCocha = lstInfoBasicaHIDDet.stream().filter(p -> p.getDescripcion().equals("Cochas")).collect(Collectors.toList());
                    if (lstInfoBasicaCocha != null) {
                        for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaCocha) {
                            cell = new PdfPCell(new Paragraph(detalle.getNombreLaguna() != null ? detalle.getNombreLaguna().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph( "",contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createAspectosFisicos2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //4- ASPECTOS FISICOS PMFICAF
        List<InfBasicaAereaDetalleDto> lstInfoBasica = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAAFFUMF");

        cell = new PdfPCell(new Paragraph("Fisiografía de la UTM",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(4);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Unidad Fisiográfica",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;
        double totalArea=0.00,totalPorc=100.00;
        if(lstInfoBasica!=null) {
            //4.2 - FISIOGRAFIA 10 PMFICAF PMFICAFFUMF
            List<InfBasicaAereaDetalleDto> lstInfoBasicaFIS = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("PMFICINFBAAFFUMF")).collect(Collectors.toList());
            if(lstInfoBasicaFIS!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaFIS) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    totalArea+=detalle.getAreaHa().doubleValue();
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHaPorcentaje() != null ? detalle.getAreaHaPorcentaje().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getObservaciones() != null ? detalle.getObservaciones().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }

                cell = new PdfPCell(new Paragraph("Total UMF",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(totalArea+"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(totalPorc+"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

            }
        }

        return table;
    }



    public  PdfPTable createAspectosBiologicos1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //5 - ASPECTOS BIOLOGICOS
        //5.1 - FAUNA 11 PMFICABFS
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFAU = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAABFS");

        cell = new PdfPCell(new Paragraph("Nombre \n Comun",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre \n nativo",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre \n Científico",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Categoría \nde amenaza",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Apendice \n CITES",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoBasicaFAU!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaFAU) {

                    cell = new PdfPCell(new Paragraph(detalle.getNombreComun() != null ? detalle.getNombreComun().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreLaguna() != null ? detalle.getNombreLaguna().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreCientifico() != null ? detalle.getNombreCientifico().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getZonaVida() != null ? detalle.getZonaVida().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getEpoca() != null ? detalle.getEpoca().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getObservaciones() != null ? detalle.getObservaciones().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
        }

        return table;
    }

        public  PdfPTable createAspectosBiologicos2(PdfWriter writer,Integer idPlanManejo) throws Exception {
            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(90);
            PdfPCell cell;
            int size=20;

            //4.6.2
            List<InfBasicaAereaDetalleDto> lstInfoAreaTipoBosque = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICINFBAABTB");

            cell = new PdfPCell(new Paragraph("Tipo de Bosque",cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Área (ha)",cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("%",cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Descripcion",cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
            int i=1;
            double totalArea=0.00,totalPorc=100.00;
            if(lstInfoAreaTipoBosque!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoAreaTipoBosque) {

                    cell = new PdfPCell(new Paragraph(detalle.getNombre() != null ? detalle.getNombre().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    totalArea+=detalle.getAreaHa().doubleValue();
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHaPorcentaje() != null ? detalle.getAreaHaPorcentaje().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
                cell = new PdfPCell(new Paragraph("Total UMF", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(totalArea+"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(totalPorc+"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }

            return table;
        }


    public  PdfPTable createInfoSocio1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //6 - INFORMACION SOCIOECONOMICA
        //6.1 - CARACTERIZACION 13 PMFICISCC
        List<InformacionBasicaEntity> lstInfoBasicaCC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISCC");

        cell = new PdfPCell(new Paragraph("Caracterización de la población en la UMF y su entorno",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(5);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tipo de Población",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades Principales",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Principales productos comercializados",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Ubicación",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        int contador=0;
        if(lstInfoBasicaCC!=null) {
            for (InformacionBasicaEntity detalle : lstInfoBasicaCC) {


                List<InformacionBasicaDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstInfoBasicaCC.size() > 0) {
                    InformacionBasicaEntity PGMFEVALI = lstInfoBasicaCC.get(contador);
                    lstListaDetalle = PGMFEVALI.getListInformacionBasicaDet();
                }

                if (lstListaDetalle != null) {
                    for (InformacionBasicaDetalleEntity detalle2 : lstListaDetalle) {
                        cell = new PdfPCell(new Paragraph(detalle.getComunidad() != null ? detalle.getComunidad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getActividad() != null ? detalle2.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getObservaciones() != null ? detalle2.getObservaciones().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        if(!detalle2.getDescripcion().equals("") || detalle2.getDescripcion() != null ){
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcion().equals("DUMF")?"X":"", contenido));
                        }else{cell = new PdfPCell(new Paragraph("", contenido));}
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        if(!detalle2.getDescripcion().equals("") || detalle2.getDescripcion() != null ){
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcion().equals("EUMF")?"X":"", contenido));
                        }else{cell = new PdfPCell(new Paragraph("", contenido));}
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
                contador++;
            }
        }

        return table;
    }

    public  PdfPTable createInfoSocio2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //6.2 - INFRAESTRUCTURA 14 PMFICISIS
        List<InformacionBasicaEntity> lstInfoBasicaIS = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISIS");

        cell = new PdfPCell(new Paragraph("Infraestructura de la UMF y su entorno",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(5);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Infraestructura",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int contador=0;
        if(lstInfoBasicaIS!=null) {
            for (InformacionBasicaEntity detalle : lstInfoBasicaIS) {


                List<InformacionBasicaDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstInfoBasicaIS.size() > 0) {
                    InformacionBasicaEntity PGMFEVALI = lstInfoBasicaIS.get(contador);
                    lstListaDetalle = PGMFEVALI.getListInformacionBasicaDet();
                }

                if (lstListaDetalle != null) {
                    for (InformacionBasicaDetalleEntity detalle2 : lstListaDetalle) {

                        cell = new PdfPCell(new Paragraph(detalle2.getActividad() != null ? detalle2.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getDescripcion() != null ? detalle2.getDescripcion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getCoordenadaEsteIni() != null ? detalle2.getCoordenadaEsteIni().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getCoordenadaNorteIni() != null ? detalle2.getCoordenadaNorteIni().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getObservaciones() != null ? detalle2.getObservaciones().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
                contador++;
            }
        }

        return table;
    }



    public  PdfPTable createInfoSocio3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //6.3 - ANTECEDENTES 15 PMFICISAUEC
        List<InformacionBasicaEntity> lstInfoBasicaAUEC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISAUEC");

        cell = new PdfPCell(new Paragraph("Antecedentes de uso anterior e identificación de conflictos y propuestas de solución",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(5);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividad",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especies extraídas",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Identificación de conflictos",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Propuesta de solución al conflicto",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        int contador=0;
        String especies="";
        if(lstInfoBasicaAUEC!=null) {
            for (InformacionBasicaEntity detalle : lstInfoBasicaAUEC) {

                List<InformacionBasicaDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstInfoBasicaAUEC.size() > 0) {
                    InformacionBasicaEntity PGMFEVALI = lstInfoBasicaAUEC.get(contador);
                    lstListaDetalle = PGMFEVALI.getListInformacionBasicaDet();
                }

                if (lstListaDetalle != null) {
                    for (InformacionBasicaDetalleEntity detalle2 : lstListaDetalle) {

                        especies+=detalle2.getActividad() != null ? detalle2.getActividad().toString()+"\n"  : "\n" ;

                    }
                }

                cell = new PdfPCell(new Paragraph(detalle.getComunidad() != null ? detalle.getComunidad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(especies, contenido));
                cell.setFixedHeight(50);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getProvincia() != null ? detalle.getProvincia().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDistrito() != null ? detalle.getDistrito().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDepartamento() != null ? detalle.getDepartamento().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                contador++;
                especies="";
            }
        }

        return table;
    }

    public  PdfPTable createOrdenamiento1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100);
        PdfPCell cell;
        int size=20;

        //7 - ORDENAMIENTO Y PROTECCION
        OrdenamientoProteccionEntity req = new OrdenamientoProteccionEntity();
        req.setIdPlanManejo(idPlanManejo);
        req.setCodTipoOrdenamiento("PMFIC");
        List<OrdenamientoProteccionEntity> lstOrdenProt =ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(req).getData();

        cell = new PdfPCell(new Paragraph("Superficie y Ubicación de los bloques",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(6);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Bloque",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Superficie \n (ha)",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM de la PC",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int contador=0;
        String especies="";
        if(lstOrdenProt!=null) {

            if(lstOrdenProt.size()>0) {
                for (OrdenamientoProteccionEntity detalle2 : lstOrdenProt) {
                    //7.1 - SUPERFICIE Y UBICACION BLOQUES 16 PFMICOPSUB
                    List<OrdenamientoProteccionDetalleEntity> lstDetBloq = new ArrayList<>();
                    if (lstOrdenProt.size() > 0) {
                        OrdenamientoProteccionEntity PGMFEVALI = lstOrdenProt.get(contador);
                        lstDetBloq = PGMFEVALI.getListOrdenamientoProteccionDet();
                    }
                    List<OrdenamientoProteccionDetalleEntity> lstDetBloq2 = lstDetBloq.stream().filter(a -> a.getCodigoTipoOrdenamientoDet().equals("PMFICOPSUB")).collect(Collectors.toList());

                    for (OrdenamientoProteccionDetalleEntity detalle : lstDetBloq2) {

                        cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getVerticeBloque() != null ? detalle.getVerticeBloque().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getObservacion() != null ? detalle.getObservacion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        contador++;
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createOrdenamiento2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100);
        PdfPCell cell;
        int size=20;

        //7 - ORDENAMIENTO Y PROTECCION
        OrdenamientoProteccionEntity req = new OrdenamientoProteccionEntity();
        req.setIdPlanManejo(idPlanManejo);
        req.setCodTipoOrdenamiento("PMFIC");
        List<OrdenamientoProteccionEntity> lstOrdenProt =ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(req).getData();

        cell = new PdfPCell(new Paragraph("Superficie y Ubicación de los bloques",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(6);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Bloque",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Superficie \n (ha)",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM de la PC",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int contador=0;
        String especies="";
        if(lstOrdenProt!=null) {
            for (OrdenamientoProteccionEntity detalle2 : lstOrdenProt) {
                //7.1 - SUPERFICIE Y UBICACION BLOQUES 16 PFMICOPSUB
                List<OrdenamientoProteccionDetalleEntity> lstDetBloq = new ArrayList<>();
                if (lstOrdenProt.size() > 0) {
                    OrdenamientoProteccionEntity PGMFEVALI = lstOrdenProt.get(contador);
                    lstDetBloq = PGMFEVALI.getListOrdenamientoProteccionDet();
                }
                List<OrdenamientoProteccionDetalleEntity> lstDetBloq2 = lstDetBloq.stream().filter(a -> a.getCodigoTipoOrdenamientoDet().equals("PMFICOPSUAAPB")).collect(Collectors.toList());

                for (OrdenamientoProteccionDetalleEntity detalle : lstDetBloq2) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getVerticeBloque() != null ? detalle.getVerticeBloque().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getObservacion() != null ? detalle.getObservacion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    contador++;
                }

            }
        }

        return table;
    }



    public  PdfPTable createOrdenamiento3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100);
        PdfPCell cell;
        int size=20;

        //7 - ORDENAMIENTO Y PROTECCION
        OrdenamientoProteccionEntity req = new OrdenamientoProteccionEntity();
        req.setIdPlanManejo(idPlanManejo);
        req.setCodTipoOrdenamiento("PMFIC");
        List<OrdenamientoProteccionEntity> lstOrdenProt =ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(req).getData();

        cell = new PdfPCell(new Paragraph("Superficie y Ubicación de los bloques",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(6);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Bloque",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Superficie \n (ha)",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM de la PC",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int contador=0;
        String especies="";
        if(lstOrdenProt!=null) {
            for (OrdenamientoProteccionEntity detalle2 : lstOrdenProt) {
                //7.1 - SUPERFICIE Y UBICACION BLOQUES 16 PFMICOPSUB
                List<OrdenamientoProteccionDetalleEntity> lstDetBloq = new ArrayList<>();
                if (lstOrdenProt.size() > 0) {
                    OrdenamientoProteccionEntity PGMFEVALI = lstOrdenProt.get(contador);
                    lstDetBloq = PGMFEVALI.getListOrdenamientoProteccionDet();
                }
                List<OrdenamientoProteccionDetalleEntity> lstDetBloq2 = lstDetBloq.stream().filter(a -> a.getCodigoTipoOrdenamientoDet().equals("PMFICOPSUPCAO")).collect(Collectors.toList());

                for (OrdenamientoProteccionDetalleEntity detalle : lstDetBloq2) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getVerticeBloque() != null ? detalle.getVerticeBloque().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getObservacion() != null ? detalle.getObservacion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
                contador++;
            }
        }

        return table;
    }




    public  PdfPTable createOrdenamiento4(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=60;

        //7.4 - MEDIDAS DE PROTECCION 19
        SistemaManejoForestalDto manejo= sistemaManejoForestalService.obtener(idPlanManejo,"PMFIC").getData();

        cell = new PdfPCell(new Paragraph("Medidas",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int contador=0;
        String especies="";
        if(manejo!=null) {

            for (SistemaManejoForestalDetalleEntity detalle : manejo.getDetalle()) {

                cell = new PdfPCell(new Paragraph(detalle.getActividades() != null ? detalle.getActividades().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcionSistema() != null ? detalle.getDescripcionSistema().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createSistManejo1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //9 - Sistema de manejo forestal
        //9.0 - Uso Potencial 25  PMFICSMFUMCZUMF
        List<ManejoBosqueEntity> lstMB_zumf= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMCZUMF",null);


        cell = new PdfPCell(new Paragraph("Uso potencial y actividades a realizar según la categoría de zonificación de la UMF",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Categoría de \nzonificación de la UMF",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Uso \nPotencial",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades \na realizar",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
int contador=0;
        if(lstMB_zumf!=null) {
            for (ManejoBosqueEntity detalle : lstMB_zumf) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_zumf.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_zumf.get(0);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    for (ManejoBosqueDetalleEntity detalle2 : lstListaDetalle) {
                        cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getNuTotalVcp() != null ? detalle2.getNuTotalVcp().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getActividad() != null ? detalle2.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);

                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createSistManejo2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        //9.1 - MF con fines maderables PMFICSMFUMMFFM
        //9.1.1 sistema maderable 26  PMFICSMFUMMFFMSMFCFM
        List<ManejoBosqueEntity> lstMB_FM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMSMFCFM");

        int contador=0;
        if(lstMB_FM!=null) {
            for (ManejoBosqueEntity detalle : lstMB_FM) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_FM.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_FM.get(0);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream().filter(p->p.getSubCodtipoManejoDet().equals("PMFICSMFUMMFFMSMFCFM")).collect(Collectors.toList());
                    if (lstPlanAccion != null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);

                        }
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createSistManejo3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        //9.1.2 ciclo corte 27  PMFICSMFUMMFFMCC
        List<ManejoBosqueEntity> lstMB_CC= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMCC");

        int contador=0;
        if(lstMB_CC!=null) {
            for (ManejoBosqueEntity detalle : lstMB_CC) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_CC.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_CC.get(0);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream().filter(p->p.getSubCodtipoManejoDet().equals("PMFICSMFUMMFFMCC")).collect(Collectors.toList());
                    if (lstPlanAccion != null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }
                }
            }
        }

        return table;
    }

    public  PdfPTable createSistManejo4(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //9.1.3 actividades 28  PMFICSMFUMMFFMAAEUFM
        List<ManejoBosqueEntity> lstMB_AA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMAAEUFM");

        cell = new PdfPCell(new Paragraph("Actividades de aprovechamiento con fines maderables", contenido));
        cell.setFixedHeight(size);
        cell.setColspan(5);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del sistema a utilizar", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maquinarias, equipos e insumos a utilizar", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal requerido", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);



        int contador=0;
        if(lstMB_AA!=null) {
            for (ManejoBosqueEntity detalle : lstMB_AA) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_AA.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_AA.get(contador);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream().filter(p->p.getSubCodtipoManejoDet().equals("PMFICSMFUMMFFMAAEUFM")).collect(Collectors.toList());
                    if (lstPlanAccion != null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionTratamiento() != null ? detalle2.getDescripcionTratamiento().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionCamino() != null ? detalle2.getDescripcionCamino().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionManoObra() != null ? detalle2.getDescripcionManoObra().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionOtro() != null ? detalle2.getDescripcionOtro().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);

                        }
                    }
                }
            }
        }

        return table;
    }

    public  PdfPTable createSistManejo5(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        //9.2 - MF con fines no maderables  PMFICSMFUMMFFNM
        //9.2.1 sistema no maderable 29  PMFICSMFUMMFFNMSMFNM
        List<ManejoBosqueEntity> lstMB_FNM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMSMFNM");
        int contador=0;
        if(lstMB_FNM!=null) {
            for (ManejoBosqueEntity detalle : lstMB_FNM) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_FNM.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_FNM.get(contador);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream().filter(p->p.getSubCodtipoManejoDet().equals("PMFICSMFUMMFFNMSMFNM")).collect(Collectors.toList());
                    if (lstPlanAccion != null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }
                }
            }
        }

        return table;
    }

    public  PdfPTable createSistManejo6(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        //9.2.2 ciclo aprov 30  PMFICSMFUMMFFNMCAPRO
        List<ManejoBosqueEntity> lstMB_CA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMCAPRO");

        int contador=0;
        if(lstMB_CA!=null) {
            for (ManejoBosqueEntity detalle : lstMB_CA) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_CA.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_CA.get(contador);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream().filter(p->p.getSubCodtipoManejoDet().equals("PMFICSMFUMMFFNMCAPRO")).collect(Collectors.toList());
                    if (lstPlanAccion != null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }
                }
            }
        }

        return table;
    }

    public  PdfPTable createSistManejo7(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //9.2.3 actividades 31  PMFICSMFUMMFFNMAAEFN
        List<ManejoBosqueEntity> lstMB_AANM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMAAEFN");

        cell = new PdfPCell(new Paragraph("Actividades de aprovechamiento con fines maderables", contenido));
        cell.setFixedHeight(size);
        cell.setColspan(5);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del sistema a utilizar", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maquinarias, equipos e insumos a utilizar", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal requerido", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);



        int contador=0;
        if(lstMB_AANM!=null) {
            for (ManejoBosqueEntity detalle : lstMB_AANM) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_AANM.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_AANM.get(contador);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream().filter(p->p.getSubCodtipoManejoDet().equals("PMFICSMFUMMFFNMAAEFN")).collect(Collectors.toList());
                    if (lstPlanAccion != null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionTratamiento() != null ? detalle2.getDescripcionTratamiento().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionCamino() != null ? detalle2.getDescripcionCamino().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionManoObra() != null ? detalle2.getDescripcionManoObra().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionOtro() != null ? detalle2.getDescripcionOtro().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }

                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createSistManejo8(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        //9.3 - Labores silviculturales  PMFICSMFUMLS
        //9.3.1 act obligatorias 32  OBLI
        List<ManejoBosqueEntity> lstMB_LSOB= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMLS","OBLI");

        cell = new PdfPCell(new Paragraph("Actividades", contenido));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Práctica silvicultural", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);



        int contador=0;
        if(lstMB_LSOB!=null) {
            for (ManejoBosqueEntity detalle : lstMB_LSOB) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_LSOB.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_LSOB.get(contador);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream()
                            .filter(p->p.getActividad().equals("OBLI")).collect(Collectors.toList());
                    if(lstPlanAccion!=null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionOtro() != null ? detalle2.getDescripcionOtro().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);


                        }
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createSistManejo9(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        //9.3.2 act opcionales 33  OPC
        List<ManejoBosqueEntity> lstMB_LSOP= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMLS","OPC");

        cell = new PdfPCell(new Paragraph("Actividades", contenido));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Práctica silvicultural", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción", contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);



        int contador=0;
        if(lstMB_LSOP!=null) {
            for (ManejoBosqueEntity detalle : lstMB_LSOP) {
                List<ManejoBosqueDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstMB_LSOP.size() > 0) {
                    ManejoBosqueEntity PGMFEVALI = lstMB_LSOP.get(contador);
                    lstListaDetalle = PGMFEVALI.getListManejoBosqueDetalle();
                }

                if (lstListaDetalle != null) {
                    List<ManejoBosqueDetalleEntity> lstPlanAccion = lstListaDetalle.stream()
                            .filter(p->p.getActividad().equals("OPC")).collect(Collectors.toList());
                    if(lstPlanAccion!=null) {
                        for (ManejoBosqueDetalleEntity detalle2 : lstPlanAccion) {
                            cell = new PdfPCell(new Paragraph(detalle2.getTratamientoSilvicultural() != null ? detalle2.getTratamientoSilvicultural().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle2.getDescripcionOtro() != null ? detalle2.getDescripcionOtro().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);


                        }
                    }
                }
            }
        }

        return table;
    }


    public  PdfPTable createPlanAccionEvaluacionImpactoAmbiental(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph("Acción preventivo corrector",cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del Impacto",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de control ambiental",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //10.2.1 acciones preventivo-corrector 35
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("CONTIN");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);

        if(lstEvaluacion!=null){
                for (EvaluacionAmbientalAprovechamientoEntity detalle : lstEvaluacion) {
                    cell = new PdfPCell(new Paragraph(detalle.getNombreAprovechamiento() != null ? detalle.getNombreAprovechamiento().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMedidasControl() != null ? detalle.getMedidasControl().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
        }
        return table;
    }


    public  PdfPTable createPlanVigilanciaEvaluacionImpactoAmbiental(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Descripción de \nlos Impactos", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de control\n ambiental", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de\n monitoreo", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Frecuencia", cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsable",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);

        // Plan de Accion Preventivo-Corrector 18 POAPRO
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        if(lstEvaluacion!=null){
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion = lstEvaluacion.stream()
                    .filter(p->!p.getTipoAprovechamiento().equals("")&& p.getSubCodTipoAprovechamiento()==null).collect(Collectors.toList());

            if(lstPlanAccion!=null) {
                //10.2.2 plan igilancia seguimiento 36
                List<EvaluacionAmbientalAprovechamientoEntity> lstPlanVigilancia = lstPlanAccion.stream()
                        .filter(p-> !p.getImpacto().trim().equals("")).collect(Collectors.toList());
                if(lstPlanVigilancia!=null) {
                    for (EvaluacionAmbientalAprovechamientoEntity detalle : lstPlanVigilancia) {
                        cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMedidasControl() != null ? detalle.getMedidasControl().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMedidasMonitoreo() != null ? detalle.getMedidasMonitoreo().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getFrecuencia() != null ? detalle.getFrecuencia().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createPlanContingenciaEvaluacionImpactoAmbiental(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Plan de Contingencia ambiental", cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("CONTINGENCIA", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("ACCIONES A REALIZAR", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("RESPONSABLE", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //8.2 Plan de Accion Preventivo-Corrector 18 POAPRO
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        if(lstEvaluacion!=null){
            List<EvaluacionAmbientalAprovechamientoEntity> lstPlanContingencia = lstEvaluacion.stream()
                    .filter(p->p.getTipoAprovechamiento().equals("CONTIN")).collect(Collectors.toList());

            if(lstPlanContingencia!=null) {
                for (EvaluacionAmbientalAprovechamientoEntity detalle : lstPlanContingencia) {
                    cell = new PdfPCell(new Paragraph(detalle.getContingencia() != null ? detalle.getContingencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAcciones() != null ? detalle.getAcciones().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getResponsableSecundario() != null ? detalle.getResponsableSecundario().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }
        return table;
    }


    public  PdfPTable createActividadSilviculturalOrganizPMFIC1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //11 - Organizacion para el desarrollo
        ResultClassEntity<ActividadSilviculturalDto>lstOrganizacion= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"PMFIC");


        cell = new PdfPCell(new Paragraph("Actividad realizada", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Forma de organización", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstOrganizacion.getData()!=null ) {
            //11.1 aprovechamiento maderable 38
            List<ActividadSilviculturalDetalleEntity> lstOrgAM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("MAD")).collect(Collectors.toList());

            for (ActividadSilviculturalDetalleEntity detalle : lstOrgAM) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createActividadSilviculturalOrganizPMFIC2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //11 - Organizacion para el desarrollo
        ResultClassEntity<ActividadSilviculturalDto>lstOrganizacion= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"PMFIC");


        cell = new PdfPCell(new Paragraph("Actividad realizada", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Forma de organización", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstOrganizacion.getData()!=null ) {
            //11.2 aprovechamiento no maderable 39
            List<ActividadSilviculturalDetalleEntity> lstOrgANM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("NMAD")).collect(Collectors.toList());

            for (ActividadSilviculturalDetalleEntity detalle : lstOrgANM) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createCronogramaActividadesPMFICAnios(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(37);
        table.setWidthPercentage(100);
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] = 80;
            }else{
                values[i] = 15;
            }
        }

        table.setWidths(values);
        int size = 30;

        Paragraph titlePara1 = new Paragraph("PLAN OPERATIVO ANUAL (POA) EN BOSQUES DE COMUNIDADES NATIVAS Y/O CAMPESINAS CON FINES DE COMERCIALIZACION A ALTA ESCALA",cabecera);
        titlePara1.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara1);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(37);
        cell.setFixedHeight(30);
        table.addCell(cell);

        Paragraph titlePara2 = new Paragraph("Actividad",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(3);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara3 = new Paragraph("Años",cabecera);
        titlePara3.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara3);
        cell.setColspan(36);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //13. Cronograma de Actividades 36
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PMFIC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();


        String marca="";
        if(lstcrono!=null) {
            for (CronogramaActividadEntity detalle : lstcrono) {
                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                List<CronogramaActividadDetalleEntity> lstDetalle = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                if (lstDetalle != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                if (lstDetalle2 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);


                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                if (lstDetalle3 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public  PdfPTable createRentabilidadIngresos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);// Genera una tabla de dos columnas
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] =100;
            }
            else{
                values[i] =40;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 7f, Font.COURIER);


        Paragraph titlePara2 = new Paragraph("RUBRO",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.isUseAscender() ;
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("Año 1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("Año 2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("Año 3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("PMFIC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        if(lstRMF!=null){

            List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream().filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());


            for(RentabilidadManejoForestalDto renta : lstIngresos) {
                cell = new PdfPCell(new Paragraph(renta.getRubro() != null ? renta.getRubro() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }

                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle = lstListaDetalle.stream().filter(p -> p.getAnio() == 1).collect(Collectors.toList());

                if (lstIngresosDetalle != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }


                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2).collect(Collectors.toList());

                if (lstIngresosDetalle2 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3).collect(Collectors.toList());

                if (lstIngresosDetalle3 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }
        }


        return table;
    }


    public  PdfPTable createRentabilidadEgresos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);// Genera una tabla de dos columnas
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] =100;
            }
            else{
                values[i] =40;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 7f, Font.COURIER);


        Paragraph titlePara2 = new Paragraph("RUBRO",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(2);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("Año 1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("Año 2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("Año 3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;

        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("PMFIC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();

        if(lstRMF!=null){

            List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream().filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());

            for(RentabilidadManejoForestalDto renta : lstIngresos) {
                cell = new PdfPCell(new Paragraph(renta.getRubro()!=null?renta.getRubro():"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);


                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }



                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle= lstListaDetalle.stream().filter(p-> p.getAnio()==1).collect(Collectors.toList());

                if (lstIngresosDetalle != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle2= lstListaDetalle.stream().filter(p-> p.getAnio()==2).collect(Collectors.toList());

                if (lstIngresosDetalle2 != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }

                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3).collect(Collectors.toList());

                if (lstIngresosDetalle3 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }


        }


        return table;
    }



    public  PdfPTable createEvaluacionAmbientalA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        EvaluacionAmbientalActividadEntity actividadEntity = new EvaluacionAmbientalActividadEntity();
        actividadEntity.setIdPlanManejo(idPlanManejo);
        actividadEntity.setTipoActividad("FACTOR");
        List<EvaluacionAmbientalActividadEntity> lista = evaluacionAmbientalRepository.ListarEvaluacionActividadFiltro(actividadEntity);
        String actividad="";
        int cantAprove=0,preApro=0,aprove=0,postAprov=0;
        PdfPTable table =null;
        if(lista!=null){
            EvaluacionAmbientalAprovechamientoEntity aprovechamientoEntity= new EvaluacionAmbientalAprovechamientoEntity();
            aprovechamientoEntity.setIdPlanManejo(idPlanManejo);
            aprovechamientoEntity.setTipoAprovechamiento("");
            List<EvaluacionAmbientalAprovechamientoEntity> listaAprovecha = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(aprovechamientoEntity);
            if(listaAprovecha.size()>0){

                List<EvaluacionAmbientalAprovechamientoEntity> lstPreApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("PREAPR")).collect(Collectors.toList());
                if(lstPreApro.size()==0){
                    cantAprove++;
                }else{
                    cantAprove+=lstPreApro.size();
                }
                List<EvaluacionAmbientalAprovechamientoEntity> lstApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("APROVE")).collect(Collectors.toList());
                if(lstApro.size()==0){
                    cantAprove++;
                }else{
                    cantAprove+=lstApro.size();
                }
                List<EvaluacionAmbientalAprovechamientoEntity> lstPostApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("POSTAP")).collect(Collectors.toList());
                if(lstPostApro.size()==0){
                    cantAprove++;
                }else{
                    cantAprove+=lstPostApro.size();
                }
            }


            table = new PdfPTable(cantAprove+3);
            PdfPCell cell;
            table.setWidthPercentage(100);
            float[] values = new float[cantAprove+3];
            for (int j = 0; j < values.length; j++) {
                if (j == 0 ) {
                    values[j] = 80;
                }else if(j>0 && j<values.length-2) {
                    values[j] = 60;
                }else  {
                    values[j] = 40;
                }
            }

            cell = new PdfPCell(new Paragraph("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tIDENTIFICACIÓN DE IMPACTOS AMBIENTALES",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(cantAprove+3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Factores \nambientales",cabeceraPeque));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Caracterización \nde impactos",cabeceraPeque));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tActividades",cabeceraPeque));
            cell.setFixedHeight(40);
            cell.setColspan(cantAprove);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Medidas \n\t\tde\n Mitigación",cabeceraPeque));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);

            List<EvaluacionAmbientalAprovechamientoEntity> lstPreApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("PREAPR")).collect(Collectors.toList());
            List<EvaluacionAmbientalAprovechamientoEntity> lstApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("APROVE")).collect(Collectors.toList());
            List<EvaluacionAmbientalAprovechamientoEntity> lstPostApro = listaAprovecha.stream().filter(p->p.getTipoAprovechamiento().equals("POSTAP")).collect(Collectors.toList());
            if(lstPreApro.size()>0){
                cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tPre \n\t\t\t\t\t\tAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(lstPreApro.size());
                table.addCell(cell);
            }else{
                cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tPre \n\t\t\t\t\t\tAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(1);
                table.addCell(cell);
            }

            if(lstApro.size()>0){
                cell = new PdfPCell(new Paragraph("\t\t\t\tAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(lstApro.size());
                table.addCell(cell);
            }else{
                cell = new PdfPCell(new Paragraph("\t\t\t\tAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(1);
                table.addCell(cell);
            }

            if(lstPostApro.size()>0){
                cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tPost \n\t\t\t\t\t\tAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(lstPostApro.size());
                table.addCell(cell);
            }else{
                cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tPost \n\t\t\t\t\t\tAprovechamiento",cabecera));
                cell.setFixedHeight(40);
                cell.setColspan(1);
                table.addCell(cell);
            }


            if(lstPreApro.size()>0){
                for(EvaluacionAmbientalAprovechamientoEntity detalle1:lstPreApro){
                    cell = new PdfPCell(new Paragraph(detalle1.getNombreAprovechamiento() != null ? detalle1.getNombreAprovechamiento().toString() : "",cabeceraPeque));
                    cell.setFixedHeight(40);
                    table.addCell(cell);
                }
            }else{
                cell = new PdfPCell(new Paragraph("",cabeceraPeque));
                cell.setFixedHeight(40);
                table.addCell(cell);
            }

            if(lstApro.size()>0){
                for(EvaluacionAmbientalAprovechamientoEntity detalle1:lstApro){
                    cell = new PdfPCell(new Paragraph(detalle1.getNombreAprovechamiento() != null ? detalle1.getNombreAprovechamiento().toString() : "",cabeceraPeque));
                    cell.setFixedHeight(40);
                    table.addCell(cell);
                }
            }else{
                cell = new PdfPCell(new Paragraph("",cabeceraPeque));
                cell.setFixedHeight(40);
                table.addCell(cell);
            }

            if(lstPostApro.size()>0){
                for(EvaluacionAmbientalAprovechamientoEntity detalle1:lstPostApro){
                    cell = new PdfPCell(new Paragraph(detalle1.getNombreAprovechamiento() != null ? detalle1.getNombreAprovechamiento().toString() : "",cabeceraPeque));
                    cell.setFixedHeight(40);
                    table.addCell(cell);
                }
            }else{
                cell = new PdfPCell(new Paragraph("",cabeceraPeque));
                cell.setFixedHeight(40);
                table.addCell(cell);
            }

            /*********************************************************** CONTENIDO  **********************************************************************************/

            String actividadCell="",aprovechaCell="",medidasMiti="";
            int cantidad=cantAprove+3;
            int cont=0;
            int contadoCero=0;
            EvaluacionAmbientalDto eval= new EvaluacionAmbientalDto();
            eval.setIdPlanManejo(idPlanManejo);
            List<EvaluacionAmbientalDto> listaEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAmbiental(eval);

            if(listaEvaluacion.size()>0){
                for(EvaluacionAmbientalDto evaluacionList: listaEvaluacion){
                    if(cont==cantidad){cont=0;}
                    if(cont<=0){
                        if(contadoCero==3){contadoCero=0;}
                        if(contadoCero==0) {
                            actividadCell=evaluacionList.getNombreActividad();
                            cell = new PdfPCell(new Paragraph(evaluacionList.getNombreActividad() != null ? evaluacionList.getNombreActividad() : "",contenido));
                            cell.setFixedHeight(40);
                            cell.setRowspan(3);
                            table.addCell(cell);
                        }
                        contadoCero++;
                        cont++;
                    }
                    if(cont==1){
                            aprovechaCell = evaluacionList.getMedidasControl();
                            cell = new PdfPCell(new Paragraph(evaluacionList.getMedidasControl() != null ? evaluacionList.getMedidasControl() : "", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);
                        cont++;
                    }
                    if(cont>1 && cont<cantidad-1){

                                cell = new PdfPCell(new Paragraph(evaluacionList.getValorEvaluacion().equals("N")? "" : "X",contenido));
                                cell.setFixedHeight(40);
                                table.addCell(cell);

                        cont++;
                    }
                    if (cont>=cantidad-1){
                            medidasMiti=evaluacionList.getMedidasMitigacion();
                            cell = new PdfPCell(new Paragraph(evaluacionList.getMedidasMitigacion() != null ? evaluacionList.getMedidasMitigacion() : "", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);
                        cont++;
                    }



                }

            }


        }else{
            table = new PdfPTable(6);
            PdfPCell cell;
            float[] values = new float[6];
            for (int j = 0; j < values.length; j++) {
                if (j == 0 ) {
                    values[j] = 80;
                }else if(j>0 && j<values.length-2) {
                    values[j] = 60;
                }else  {
                    values[j] = 40;
                }
            }

            cell = new PdfPCell(new Paragraph("IDENTIFICACIÓN DE IMPACTOS AMBIENTALES",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(6);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Factores \n ambientales",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Caracterización \nde impactos",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Actividades",cabecera));
            cell.setFixedHeight(40);
            cell.setColspan(cantAprove);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Medidas \nde Mitigación",cabecera));
            cell.setFixedHeight(40);
            cell.setRowspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Pre \nAprovechamiento",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Aprovechamiento",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Post \nAprovechamiento",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("",cabecera));
            cell.setFixedHeight(40);
            table.addCell(cell);
        }

        return table;

    }


    public  PdfPTable createAspectosCompPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 50;

        //15.	ASPECTOS COMPLEMENTARIOS   (47)
        InformacionGeneralDEMAEntity aspectosComRequest = new InformacionGeneralDEMAEntity();
        aspectosComRequest.setCodigoProceso("PMFIC");
        aspectosComRequest.setIdPlanManejo(idPlanManejo);
        ResultEntity<InformacionGeneralDEMAEntity> lstAspectos = infoGeneralDemaRepo.listarInformacionGeneralDema(aspectosComRequest);

        if(lstAspectos.getData()!=null) {
            for (InformacionGeneralDEMAEntity detalle : lstAspectos.getData()) {

                cell = new PdfPCell(new Paragraph(detalle.getDetalle() != null ? detalle.getDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }



    public  PdfPTable createAnexos3(PdfWriter writer,Anexo2PGMFDto detalle2) throws Exception {

        List<Anexo2PGMFDto> lista = new ArrayList<Anexo2PGMFDto>();

        Anexo2PGMFDto data = new Anexo2PGMFDto();
        data.setNombreBosque(detalle2.getNombreBosque());
        data.setAreaBosque(detalle2.getAreaBosque());
        data.setNombreComun(detalle2.getNombreComun());
        data.setBloque(1);
        data.setArbHaDap20a29(detalle2.getArbHaDap20a29());
        data.setArbHaDap30a39(detalle2.getArbHaDap30a39());
        data.setArbHaDap40a49(detalle2.getArbHaDap40a49());
        data.setArbHaDap50a59(detalle2.getArbHaDap50a59());
        data.setArbHaDap60a69(detalle2.getArbHaDap60a69());
        data.setArbHaDap70a79(detalle2.getArbHaDap70a79());
        data.setArbHaDap80a89(detalle2.getArbHaDap80a89());
        data.setArbHaDap90aMas(detalle2.getArbHaDap90aMas());
        data.setArbHaTotalTipoBosque(detalle2.getArbHaTotalTipoBosque());
        data.setArbHaTotalPorArea(detalle2.getArbHaTotalPorArea());
        lista.add(data);
        data=new Anexo2PGMFDto();
        data.setNombreBosque(detalle2.getNombreBosque());
        data.setAreaBosque(detalle2.getAreaBosque());
        data.setNombreComun(detalle2.getNombreComun());
        data.setBloque(2);
        data.setArbHaDap20a29(detalle2.getAbHaDap20a29());
        data.setArbHaDap30a39(detalle2.getAbHaDap30a39());
        data.setArbHaDap40a49(detalle2.getAbHaDap40a49());
        data.setArbHaDap50a59(detalle2.getAbHaDap50a59());
        data.setArbHaDap60a69(detalle2.getAbHaDap60a69());
        data.setArbHaDap70a79(detalle2.getAbHaDap70a79());
        data.setArbHaDap80a89(detalle2.getAbHaDap80a89());
        data.setArbHaDap90aMas(detalle2.getAbHaDap90aMas());
        data.setArbHaTotalTipoBosque(detalle2.getAbHaTotalTipoBosque());
        data.setArbHaTotalPorArea(detalle2.getArbHaTotalPorArea());
        lista.add(data);
        data=new Anexo2PGMFDto();
        data.setNombreBosque(detalle2.getNombreBosque());
        data.setAreaBosque(detalle2.getAreaBosque());
        data.setNombreComun(detalle2.getNombreComun());
        data.setBloque(3);
        data.setArbHaDap20a29(detalle2.getVolHaDap20a29());
        data.setArbHaDap30a39(detalle2.getVolHaDap30a39());
        data.setArbHaDap40a49(detalle2.getVolHaDap40a49());
        data.setArbHaDap50a59(detalle2.getVolHaDap50a59());
        data.setArbHaDap60a69(detalle2.getVolHaDap60a69());
        data.setArbHaDap70a79(detalle2.getVolHaDap70a79());
        data.setArbHaDap80a89(detalle2.getVolHaDap80a89());
        data.setArbHaDap90aMas(detalle2.getVolHaDap90aMas());
        data.setArbHaTotalTipoBosque(detalle2.getVolHaTotalTipoBosque());
        data.setArbHaTotalPorArea(detalle2.getVolHaTotalPorArea());
        lista.add(data);





        PdfPTable table = new PdfPTable(11);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;
        float[] values = new float[11];
        for(int i=0;i<values.length;i++){
            if(i>=4 && i<=10){
                values[i] =30;
            }
            else{
                values[i] =60;
            }
        }
        table.setWidths(values);


        int contador=0,cont=0;

        String variable="";
        if(lista.size()>0){
            for(Anexo2PGMFDto detalle :lista) {
                if(contador==0){
                    cell = new PdfPCell(new Paragraph( detalle.getNombreBosque() != "Tipo de Bosque: " ? "Tipo de Bosque: "+detalle.getNombreBosque().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    cell.setColspan(9);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaBosque() != "Área: " ? "Área: " +detalle.getAreaBosque().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    cell.setColspan(2);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("Nombre Comun", cabecera));
                    cell.setFixedHeight(size);
                    cell.setRowspan(2);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("Var / ha", cabecera));
                    cell.setFixedHeight(size);
                    cell.setRowspan(2);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("DAP (cm)", cabecera));
                    cell.setFixedHeight(size);
                    cell.setColspan(7);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("Total por tipo de bosque", cabecera));
                    cell.setFixedHeight(size);
                    cell.setRowspan(2);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("Total por ha", cabecera));
                    cell.setFixedHeight(size);
                    cell.setRowspan(2);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("31-40", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("41-50", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("51-60", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("61-70", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("71-80", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("81-90", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("+90", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
                    /*cell = new PdfPCell(new Paragraph(detalle.getNombreBosque() != null ? detalle.getNombreBosque().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaBosque() != null ? detalle.getAreaBosque().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);*/
                if(cont==3){cont=0;}
                if(cont==0){
                    cell = new PdfPCell(new Paragraph(detalle.getNombreComun() != null ? detalle.getNombreComun().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    cell.setRowspan(3);
                    table.addCell(cell);
                }

                    if (detalle.getBloque() == 1) {
                        variable = "N";
                    } else if (detalle.getBloque() == 2) {
                        variable = "AB M2";
                    } else if (detalle.getBloque() == 3) {
                        variable = "VC M3";
                    }

                    cell = new PdfPCell(new Paragraph(variable, cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap30a39() != null ? detalle.getArbHaDap30a39().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap40a49() != null ? detalle.getArbHaDap40a49().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap50a59() != null ? detalle.getArbHaDap50a59().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap60a69() != null ? detalle.getArbHaDap60a69().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap70a79() != null ? detalle.getArbHaDap70a79().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap80a89() != null ? detalle.getArbHaDap80a89().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaDap90aMas() != null ? detalle.getArbHaDap90aMas().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaTotalTipoBosque() != null ? detalle.getArbHaTotalTipoBosque().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getArbHaTotalPorArea() != null ? detalle.getArbHaTotalPorArea().toString() : "", cabecera));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                contador++;
                cont++;
            }
        }
        return table;
    }



    public  PdfPTable createAnexos4(PdfWriter writer,Anexo3PGMFDto detalle) throws Exception {

        String variable="";
        List<Anexo3PGMFDto> lista=new ArrayList<Anexo3PGMFDto>();
        Anexo3PGMFDto data = new Anexo3PGMFDto();
        data.setNombreBosque(detalle.getNombreBosque());
        data.setNombreComun(detalle.getNombreComun());
        data.setAreaBosque(detalle.getAreaBosque());
        data.setBloque(1);
        data.setArbHaDap10a19(detalle.getArbHaDap10a19());
        data.setArbHaDap20a29(detalle.getArbHaDap20a29());
        data.setArbHaTotalTipoBosque(detalle.getArbHaTotalTipoBosque());
        data.setArbHaTotalPorArea(detalle.getArbHaTotalPorArea());
        lista.add(data);
        Anexo3PGMFDto data2 = new Anexo3PGMFDto();
        data.setNombreBosque(detalle.getNombreBosque());
        data2.setNombreComun(detalle.getNombreComun());
        data.setAreaBosque(detalle.getAreaBosque());
        data2.setBloque(2);
        data2.setArbHaDap10a19(detalle.getAbHaDap10a19());
        data2.setArbHaDap20a29(detalle.getAbHaDap20a29());
        data2.setArbHaTotalTipoBosque(detalle.getAbHaTotalTipoBosque());
        data2.setArbHaTotalPorArea(detalle.getAbHaTotalPorArea());
        lista.add(data2);

        int contador=0,cont=0;
        PdfPTable table = new PdfPTable(6);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 25;

            if(lista!=null){
                for(Anexo3PGMFDto detalle2: lista){
                    if(contador==0){
                        cell = new PdfPCell(new Paragraph(detalle2.getNombreBosque() != "Tipo de Bosque: " ? "Tipo de Bosque: "+detalle2.getNombreBosque().toString() : "" , cabecera));
                        cell.setFixedHeight(size);
                        cell.setColspan(4);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle2.getAreaBosque() != "Área: " ? "Área: "+detalle2.getAreaBosque().toString() : "", cabecera));
                        cell.setFixedHeight(size);
                        cell.setColspan(2);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("Nombre Común", cabecera));
                        cell.setFixedHeight(size);
                        cell.setRowspan(2);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("Var/ha", cabecera));
                        cell.setFixedHeight(size);
                        cell.setRowspan(2);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("DAP(cm)", cabecera));
                        cell.setFixedHeight(size);
                        cell.setColspan(2);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("Total por tipo de bosque", cabecera));
                        cell.setFixedHeight(size);
                        cell.setRowspan(2);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("Total\nÁrea censada", cabecera));
                        cell.setFixedHeight(size);
                        cell.setRowspan(2);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("10-20", cabecera));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph("20-30", cabecera));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }



                    variable="";
                   /* cell = new PdfPCell(new Paragraph(detalle2.getNombreBosque() != null ? detalle2.getNombreBosque().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle2.getNombreComun() != null ? detalle2.getNombreComun().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);*/
                    if(cont==3){cont=0;}
                    if(cont==0){
                        cell = new PdfPCell(new Paragraph(detalle2.getNombreComun() != null ? detalle2.getNombreComun().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        cell.setRowspan(2);
                        table.addCell(cell);
                    }
                    if(detalle2.getBloque()==1){variable="N";}
                    else if(detalle2.getBloque()==2){variable="AB m2";}
                    else if(detalle2.getBloque()==3){variable="VC m3";}
                    cell = new PdfPCell(new Paragraph(detalle2.getBloque() != null ? variable : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle2.getArbHaDap10a19() != null ? detalle2.getArbHaDap10a19().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle2.getArbHaDap20a29() != null ? detalle2.getArbHaDap20a29().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle2.getArbHaTotalTipoBosque() != null ? detalle2.getArbHaTotalTipoBosque().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle2.getArbHaTotalPorArea() != null ? detalle2.getArbHaTotalPorArea().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    contador++;
                    cont++;
                }
            }



        return table;
    }



    public  PdfPTable createAnexos5(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(10);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 5
        List <Anexo2Dto> lstLista=censoForestalRepository.ResultadosPOConcesionesAnexos2(idPlanManejo,"PMFIC",null,null);


        cell = new PdfPCell(new Paragraph("N° \nFaja", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° Árbol\n(código)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DAP \n (cm)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Altura\n comercial \n(m)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Calidad \n fuste", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM ", cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vc(m3) ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Árbol Aprovechable /Semillero ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("E", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



        if(lstLista!=null){
            for (Anexo2Dto detalle : lstLista) {

                cell = new PdfPCell(new Paragraph(detalle.getNumeroFaja() != null ? detalle.getNumeroFaja().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getnArbol() != null ? detalle.getnArbol() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNombreEspecies() != null ? detalle.getNombreEspecies().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDap() != null ? detalle.getDap().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAlturaComercial() != null ? detalle.getAlturaComercial().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCalidadFuste() != null ? detalle.getCalidadFuste().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getEste() != null ? detalle.getEste().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNorte() != null ? detalle.getNorte().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getVolumen() != null ? detalle.getVolumen().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }



    public  PdfPTable createAnexos6(PdfWriter writer,TablaAnexo6Dto detalle,String nombreBosque,String areaBosque) throws Exception {


        List<TablaAnexo6Dto> listaDetalle2= new ArrayList<TablaAnexo6Dto>();

        TablaAnexo6Dto data = new TablaAnexo6Dto();
        data.setNombreBosque(nombreBosque);
        data.setNombreComun(detalle.getNombreComun());
        data.setVariable("N");
        data.setnTotalPorTipoBosque(detalle.getnTotalPorTipoBosque());
        data.setnTotalPorHa(detalle.getnTotalPorHa());
        listaDetalle2.add(data);
        TablaAnexo6Dto data2 = new TablaAnexo6Dto();
        data.setNombreBosque(nombreBosque);
        data2.setNombreComun(detalle.getNombreComun());
        data2.setVariable("C**");
        data2.setnTotalPorTipoBosque(detalle.getcTotalPorTipoBosque());
        data2.setnTotalPorHa(detalle.getcTotalPorHa());
        listaDetalle2.add(data2);


        PdfPTable table = new PdfPTable(4);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

    int contador=0,cont=0;
    if(listaDetalle2!=null) {
        for (TablaAnexo6Dto detalle2 : listaDetalle2) {
            if(contador==0){
                cell = new PdfPCell(new Paragraph(areaBosque!= null ? "Área: "+areaBosque : "Área: ", cabecera));
                cell.setFixedHeight(size);
                cell.setColspan(4);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle2.getNombreBosque() != null ? "Tipo de Bosque: "+detalle2.getNombreBosque() : "Tipo de Bosque: " , cabecera));
                cell.setFixedHeight(size);
                cell.setColspan(4);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre Comun", cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Var / ha", cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Total por\n tipo de bosque", cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Total por ha", cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
            if(cont==3){cont=0;}
            if(cont==0){
                cell = new PdfPCell(new Paragraph(detalle2.getNombreComun() != null ? detalle2.getNombreComun() : "", contenido));
                cell.setFixedHeight(25);
                cell.setRowspan(2);
                table.addCell(cell);
            }

            cell = new PdfPCell(new Paragraph(detalle2.getVariable() != null ? detalle2.getVariable().toString() : "", contenido));
            cell.setFixedHeight(25);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle2.getnTotalPorTipoBosque() != null ? detalle2.getnTotalPorTipoBosque().toString() : "", contenido));
            cell.setFixedHeight(25);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle2.getnTotalPorHa() != null ? detalle2.getnTotalPorHa().toString() : "", contenido));
            cell.setFixedHeight(25);
            table.addCell(cell);
            contador++;
            cont++;
        }
    }

        return table;
    }


    public  PdfPTable createAnexos7(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(7);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 7
        List <Anexo2Dto> listaAnexo7=censoForestalRepository.ResultadosAnexo7NoMaderable(idPlanManejo,"PMFIC");

        cell = new PdfPCell(new Paragraph("N° \nFaja", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° Árbol\n(código)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM ", cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("C* ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Árbol o individuo Semillero ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("E", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



        if(listaAnexo7!=null){

            for (Anexo2Dto detalle : listaAnexo7) {

                cell = new PdfPCell(new Paragraph(detalle.getNumeroFaja() != null ? detalle.getNumeroFaja() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getnArbol() != null ? detalle.getnArbol().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNombreEspecies() != null ? detalle.getNombreEspecies().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getEste() != null ? detalle.getEste().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNorte() != null ? detalle.getNorte().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getC() != null ? detalle.getC().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

            }
        }

        return table;
    }


    public  PdfPTable createAnexos8(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 8
        List <ListaEspecieDto> listaAnexo8=censoForestalRepository.ListaEspeciesInventariadasMaderables(idPlanManejo,"PMFIC");

        cell = new PdfPCell(new Paragraph("Nombre común", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre nativo", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre Científico", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(listaAnexo8!=null){

            for (ListaEspecieDto detalle : listaAnexo8) {

                cell = new PdfPCell(new Paragraph(detalle.getTextNombreComun() != null ? detalle.getTextNombreComun() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNombreNativo() != null ? detalle.getNombreNativo().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getTextNombreCientifico() != null ? detalle.getTextNombreCientifico().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }

}
