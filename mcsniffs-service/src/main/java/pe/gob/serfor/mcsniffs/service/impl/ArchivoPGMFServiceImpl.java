package pe.gob.serfor.mcsniffs.service.impl;


import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaInfractorDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultarRegenteDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarPGMF.DescargarPGMFDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.AprovechamientoRFNMDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown.item;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosEspeciesMuestreoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.TablaEspeciesMuestreo;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.AreaBloqueEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolicitudPlantacionForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.CronogramaActividesRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaUbigeoRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.ObjetivoManejoRepository;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoProteccionRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFAbreviadoRepository;
import pe.gob.serfor.mcsniffs.repository.ParticipacionComunalRepository;
import pe.gob.serfor.mcsniffs.repository.PotencialProduccionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.RentabilidadManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.UnidadFisiograficaRepository;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.service.ArchivoPGMFService;
import pe.gob.serfor.mcsniffs.service.client.FeignPideApi;

@Service("ArchivoPGMFService")
public class ArchivoPGMFServiceImpl implements ArchivoPGMFService {

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;
    
    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired 
    private FeignPideApi feignPideApi;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;
 
    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private CronogramaActividesRepository cronogramaActividadesRepository;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 12f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    static Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font subTituloTabla= new Font(Font.HELVETICA, 9f, Font.COURIER);

    Font normal= new Font(Font.HELVETICA, 10f);

    @Override
    public ByteArrayResource archivoConsolidadoPGMF(DescargarPGMFDto obj, String token) throws Exception {

        File archivo = File.createTempFile("ConsolidadoPGMF", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPDFSolPanFor(os, obj, token);
        os.flush();
        os.close();
        //ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }


    public  void createPDFSolPanFor(FileOutputStream os,DescargarPGMFDto obj, String token) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);

        try {

            PdfWriter writer = PdfWriter.getInstance(document, os);
            document.open();

            Paragraph preface = new Paragraph();

            //  SUB TITULO
            preface.add(new Paragraph("Anexo: \n FORMATO DEL PLAN DEL GENERAL DE MANEJO FORESTAL (PGMF) PARA CONCESIONES FORESTALES CON FINES MADERABLES", subTitulo));

            //1. INFORMACION GENERAL
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("1.   INFORMACION GENERAL", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableinfoGeneral = informacionGeneral(writer,obj, token);
            document.add(tableinfoGeneral);

            //2. OBJETIVOS DEL MANJEO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("2.   OBJETIVOS DEL MANJEO", subTitulo));
            preface.add(new Paragraph("     2.1  Objetivo general", subTituloTabla)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableObjGene = objetivoGeneral(writer,obj);
            document.add(tableObjGene);
            document.add(new Paragraph("\r\n"));
            preface = new Paragraph();
            preface.add(new Paragraph(" 	    2.2 	Objetivos específicos (marca con “x”):", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableObjEspecificos = objetivoEspecifico(writer, obj);
            document.add(tableObjEspecificos);

            //3. INFORMACION BASICA UMF
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("3.   INFORMACIÓN BÁSICA DE LA UMF", subTitulo));
            preface.add(new Paragraph(" 3.1.    Ubicación y extensión", subTituloTabla));
            preface.add(new Paragraph("     3.1.1.  Ubicación política de la Concesión: (Mapa 1)", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableUbiPolitica = ubicacionPolitica(writer,obj);
            document.add(tableUbiPolitica);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("     3.1.2.  Coordenadas UTM de la Concesión según contrato (Zona........WGS 84)", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableCoordUTM = coordenadasUTM(writer,obj);
            document.add(tableCoordUTM);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 3.2.    Accesibilidad", subTituloTabla));
            document.add(preface);
            accesibilidad(document, obj);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 3.3.    Aspectos Físicos (Hidrografía y fisiografía)", subTituloTabla));
            document.add(preface);
            aspectosFisicos(document,obj);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 3.4.    Aspectos Biológicos", subTituloTabla));
            preface.add(new Paragraph("     3.4.1    Fauna Silvestre", contenido));
            addEmptyLine(preface, 1);
            document.add(preface);
            aspectosBiologicos(document,obj);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 3.5.    Aspectos Socioeconómicos", subTituloTabla));
            document.add(preface);
            aspectosSocioeconomicos(document,obj);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 3.6.    Antecedentes de Uso e Identificación de Conflictos", subTituloTabla));
            document.add(preface);
            antecedenteIdentifConflicto(document,obj);

            //4.  ORDENAMIENTO Y PROTECCION DE LA UMF
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("4.   ORDENAMIENTO Y PROTECCION DE LA UMF", subTitulo));

            preface.add(new Paragraph("     4.1.	Categorías de ordenamiento", subTituloTabla)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            ordenamientoTab4(document,obj);

            //5.   POTENCIAL DE PRODUCCION DEL RECURSO FORESTAL MADERABLE
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("5.   POTENCIAL DE PRODUCCION DEL RECURSO FORESTAL MADERABLE", subTitulo));
            preface.add(new Paragraph("     5.1  Características del inventario forestal", subTituloTabla)); 
            preface.add(new Paragraph("        5.1.1  Potencial maderable ", subTituloTabla)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            potencialProduccionTab5(document,obj);

            //6. MANEJO FORESTAL
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("6.   MANEJO FORESTAL", subTitulo));
            document.add(preface);
            manejoForestalTab6(document,obj);

            //7.	EVALUACION DE IMPACTO AMBIENTAL
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("7.   EVALUACION DE IMPACTO AMBIENTAL", subTitulo));
            preface.add(new Paragraph("     7.2	Plan de Gestión Ambiental", subTituloTabla)); 
            preface.add(new Paragraph("Identificación de los impactos ambientales generados por la implementación o desarrollo de las actividades necesarias para el aprovechamiento forestal. Deberá incluir para cada impacto identificado una propuesta de medida de mitigación; las cuales pueden ser de carácter preventivo-corrector. Asimismo, se incluye las actividades de vigilancia y seguimiento ambiental de las medidas identificadas. Finalmente desarrolla las acciones de contingencia respecto a accidentes o situaciones no programadas.", contenido)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            identificacionImpacAmbientalesTab7(document, obj);
            

            //8.   MONITOREO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("8.   MONITOREO", subTitulo));
            preface.add(new Paragraph("     8.1  Descripción del sistema de monitoreo", subTituloTabla));      
            addEmptyLine(preface, 1);
            document.add(preface);
            monitoreoTab8(document,obj);

            //9.   MONITOREO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("9.   PARTICIPACIÓN CIUDADANA", subTitulo));
            preface.add(new Paragraph("     9.1  En la Formulación del Plan de Manejo Forestal", subTituloTabla));      
            addEmptyLine(preface, 1);
            document.add(preface);
            participacionCiudadanaTab9(document,obj);             

            //10.   MONITOREO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("10.   CAPACITACIÓN", subTitulo));
            preface.add(new Paragraph("     10.1  Objetivos del Plan de Capacitación", subTituloTabla));      
            addEmptyLine(preface, 1);
            document.add(preface);
            capacitacionTab10(document,obj);       
          
             
            //11.   ORGANIZACION DEL MANEJO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("11.   ORGANIZACIÓN DEL MANEJO", subTitulo));
            preface.add(new Paragraph("     11.1  Funciones y responsabilidades del personal de la concesión:", subTituloTabla));      
            addEmptyLine(preface, 1);
            document.add(preface);
            organizacionTab11(document,obj); 

            //12.   PROGRAMA DE INVERSIONES
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("12.   PROGRAMA DE INVERSIONES", subTitulo));
            document.add(preface);
            programaInversionesTab12(document,obj);   
             
            //13. CRONOGRAMA DE ACTIVIDADES
            Rectangle two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("13.   CRONOGRAMA DE ACTIVIDADES", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableCronogramaActividades = cronogramaActividades(writer,obj);
            document.add(tableCronogramaActividades);


             

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }
 
    public  PdfPTable informacionGeneral(PdfWriter writer, DescargarPGMFDto obj, String token) throws Exception { 
        //------DATA-------//
        InformacionGeneralDto info = new InformacionGeneralDto();
        String nombreCompleto = "";
        ResultClassEntity<InformacionGeneralDto> result = infoGeneralRepo.obtener(obj.getIdPlanManejo());
        if(result!=null && result.getSuccess() && result.getData()!=null){
            info = result.getData();
            nombreCompleto = (info.getNombreRepresentanteLegal()==null?"":info.getNombreRepresentanteLegal()) +" "+ (info.getApellidoPaternoRepLegal()==null?"":info.getApellidoPaternoRepLegal()) +" "+ (info.getApellidoMaternoRepLegal()==null?"":info.getApellidoMaternoRepLegal());
        }

        /*Obtener el nombre del regente */
        String nombreCompletoRegente = "";
        if (result!=null && result.getSuccess() && result.getData()!=null && result.getData().getIdRegenteForestal()!=null) {

            ConsultaInfractorDto dto = new ConsultaInfractorDto();
            ConsultarRegenteDto consulregente = feignPideApi.consultarRegente(token, dto);
    
            if (consulregente!=null && consulregente.getDataService() != null){
                for (ConsultarRegenteDto.Regente i : consulregente.getDataService()) {
                    if (i.getId().equals(result.getData().getIdRegenteForestal()) ){
                        nombreCompletoRegente = i.getNombres()+" "+i.getApellidos();
                        break;
                    }
                }
            }

        }
        
        InformacionGeneralDto data = new InformacionGeneralDto();
        //----------------//
        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {0.80f, 1.20f, 0.80f, 1.20f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearSubTitle("Del Contrato", 4, table); //------------------------
        setearBodyStatic("Nombre del Titular:", 2, table);
        setearBodyData(info.getRazonSocialTitularTH()==null?"":info.getRazonSocialTitularTH(),2, table);
        setearBodyStatic("Nombre del Representante Legal:", 2, table);
        setearBodyData(nombreCompleto, 2, table);
        setearBodyStatic("Número DNI:", 1, table);
        setearBodyData(info.getNumeroDocumentoRepLegal()==null?"":info.getNumeroDocumentoRepLegal(), 1, table);
        setearBodyStatic("RUC:", 1, table);
        setearBodyData(info.getNumeroDocumentoTitularTH()==null?"":info.getNumeroDocumentoTitularTH(),1, table);
        setearBodyStatic("Domicilio legal/Distrito:", 2, table);
        setearBodyData(info.getDireccionTitularTH()==null?"":info.getDireccionTitularTH(), 2, table);
        setearBodyStatic("N° de contrato de la concesión:", 2, table);
        setearBodyData(info.getNroContratoConcesion()==null?"":info.getNroContratoConcesion(), 2, table);
        setearBodyStatic("Departamento:", 1, table);
        setearBodyData(info.getNombreDepartamentoContrato()==null?"":info.getNombreDepartamentoContrato(),1,table);
        setearBodyStatic("Provincia:", 1, table);
        setearBodyData(info.getNombreProvinciaContrato()==null?"":info.getNombreProvinciaContrato(),1, table);
        setearSubTitle("Del Plan General del Manejo Forestal", 4, table); //------------------------
        setearBodyStatic("Fecha de presentación del PGMF", 2, table);
        setearBodyData(info.getFechaPresentacion()==null?"":FechaUtil.obtenerFechaString(info.getFechaPresentacion(),"dd/MM/yyyy") , 2, table);
        setearRowsColspanStatic("Duración del PGMF (años):", 2, 1, table);
        setearRowsColspanData(info.getDuracion()==null?"":info.getDuracion().toString(),2, 1, table);
        setearBodyStatic("Fecha de inicio:", 1, table);
        setearBodyData(info.getFechaInicio()==null?"":FechaUtil.obtenerFechaString(info.getFechaInicio(),"dd/MM/yyyy") , 1, table);
        setearBodyStatic("Fecha de finalización:", 1, table);
        setearBodyData(info.getFechaFin()==null?"":FechaUtil.obtenerFechaString(info.getFechaFin(),"dd/MM/yyyy") , 1, table);
        setearRowsColspanStatic("Área total de la concesión (ha):", 2, 1, table);
        setearRowsColspanData(info.getAreaTotalConcesion()==null?"": info.getAreaTotalConcesion().toString(),2, 1, table);
        setearBodyStatic("Área de bosque de producción forestal (ha):", 1, table);
        setearBodyData(info.getAreaBosqueProduccionForestal()==null?"":info.getAreaBosqueProduccionForestal().toString(),1, table);
        setearBodyStatic("Área de protección (ha):", 1, table);
        setearBodyData(info.getAreaProteccion()==null?"":info.getAreaProteccion().toString(), 1, table);
        setearBodyStatic("N° Bloques Quinquenales (de ser el caso):", 2, table);
        setearBodyData(info.getNumeroBloquesQuinquenales()==null?"":info.getNumeroBloquesQuinquenales(),2,table);
        setearBodyStatic("Potencial maderable (m3 totales):", 2, table);
        setearBodyData(info.getPotencialMaderable()==null?"":info.getPotencialMaderable().toString(), 2, table);
        setearBodyStatic("Volumen de corta anual permisible (m3)", 2, table);
        setearBodyData(info.getVolumenCortaAnualPermisible()==null?"":info.getVolumenCortaAnualPermisible().toString(),2,table);

        setearSubTitle("Del Regente Forestal del PGMF", 4, table); //------------------------
        setearBodyStatic("Nombre del Regente Forestal del PGMF:", 2, table);
        setearBodyData(nombreCompletoRegente, 2, table);
        setearBodyStatic("Domicilio legal:", 2, table);
        setearBodyData(info.getDomicilioLegalRegente()==null?"":info.getDomicilioLegalRegente(), 2, table);
        setearBodyStatic("Contrato suscrito con el titular del título habilitante:", 2, table);
        setearBodyData(info.getContratoSuscritoTitularTituloHabilitante()==null?"":info.getContratoSuscritoTitularTituloHabilitante(), 2, table);
        setearBodyStatic("Certificado de habilitación profesional del Regente Forestal:", 2, table);
        setearBodyData(info.getCertificadoHabilitacionProfesionalRegenteForestal()==null?"":info.getCertificadoHabilitacionProfesionalRegenteForestal(), 2, table);
        setearBodyStatic("N° inscripción en el registro de regentes que conduce el SERFOR:", 2, table);
        setearBodyData(info.getNumeroInscripcionRegistroRegente()==null?"":info.getNumeroInscripcionRegistroRegente(), 2, table);

        
        return table;
    }

    public  PdfPTable objetivoGeneral(PdfWriter writer, DescargarPGMFDto obj) throws Exception { 
        //------DATA-------//
        ObjetivoManejoEntity ome = new ObjetivoManejoEntity();
        ome.setPlanManejo(new PlanManejoEntity());
        ome.getPlanManejo().setIdPlanManejo(obj.getIdPlanManejo());
        ResultClassEntity resObjManejo = objetivoManejoRepository.ObtenerObjetivoManejo(ome);

        ObjetivoManejoEntity dataObjetivo = new ObjetivoManejoEntity();

        if (resObjManejo != null && resObjManejo.getSuccess()) {
            dataObjetivo = (ObjetivoManejoEntity) objetivoManejoRepository.ObtenerObjetivoManejo(ome).getData();
        }

        //----------------//
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(95);

        setearBodyData(dataObjetivo.getGeneral()==null?"":dataObjetivo.getGeneral(),1,table);

        return table;
    }
    
    public  PdfPTable objetivoEspecifico(PdfWriter writer, DescargarPGMFDto obj) throws Exception { 
        //------DATA-------//
        ObjetivoManejoEntity ome = new ObjetivoManejoEntity();
        ome.setPlanManejo(new PlanManejoEntity());
        ome.getPlanManejo().setIdPlanManejo(obj.getIdPlanManejo());
        ResultClassEntity resObjManejo = objetivoManejoRepository.ObtenerObjetivoManejo(ome);

        ObjetivoManejoEntity dataObjetivo = new ObjetivoManejoEntity();
        List<ObjetivoEspecificoManejoEntity> list = new ArrayList<>();

        ParametroEntity param = new ParametroEntity();
        param.setCodigo("OBJPGMF");
        List<Dropdown.item> Items = new ArrayList<>();
        ResultClassEntity result = objetivoManejoRepository.ComboPorFiltroObjetivoEspecifico(param);
        if(result!=null && result.getSuccess()){
            List<Dropdown> lisrResult = (List<Dropdown>) result.getData();
            Items = (List<item>) lisrResult.get(0).getItems();
        }

        if (resObjManejo != null && resObjManejo.getSuccess()) {
            dataObjetivo = (ObjetivoManejoEntity) objetivoManejoRepository.ObtenerObjetivoManejo(ome).getData();
            ResultClassEntity resultEspecifco = objetivoManejoRepository.ObtenerObjetivoEspecificoManejo(dataObjetivo);
            if (resultEspecifco != null && resultEspecifco.getSuccess()) {
                list = (ArrayList<ObjetivoEspecificoManejoEntity>) resultEspecifco.getData();
            }
        }

        //----------------//
        PdfPTable table = new PdfPTable(2);
        float[] medidaCeldas = {1.90f, 0.10f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        for (Dropdown.item item : Items) {
            setearBodyDataSborder(item.getLabel()==null?"":item.getLabel(),1,1,table);
            setearBodyData(marcarObjetivoEspecifico(item.getValue(), list ) ,1,table);
            setearBodyDataSborder("",1,2,table);
        }

        return table;
    }

    public  PdfPTable ubicacionPolitica(PdfWriter writer, DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {0.80f, 0.80f, 0.80f, 0.80f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyStatic("Departamento",1,table);
        setearBodyStatic("Provincia",1,table);
        setearBodyStatic("Distrito",1,table);
        setearBodyStatic("Cuenca/Sub cuenca",1,table);


        InformacionBasicaEntity ibe = new InformacionBasicaEntity();
        ibe.setIdPlanManejo(obj.getIdPlanManejo());
        ibe.setCodInfBasica("PGMF");
        ibe.setCodSubInfBasica("PGMFIBAM");

        List<InformacionBasicaUbigeoEntity> listaUbicacion = informacionBasicaUbigeoRepository.ListarInformacionBasicaDistrito(ibe);

        if (listaUbicacion != null && !listaUbicacion.isEmpty()) {

            for (InformacionBasicaUbigeoEntity element : listaUbicacion) {
                setearBodyData(element.getNombreDepartamento() == null ? "" : element.getNombreDepartamento(), 1, table);
                setearBodyData(element.getNombreProvincia() == null ? "" : element.getNombreProvincia(), 1, table);
                setearBodyData(element.getNombreDistrito() == null ? "" : element.getNombreDistrito(), 1, table);
                setearBodyData(element.getCuenca() == null ? "" : element.getCuenca(), 1, table);
            }
        }
        return table;
    }

    public  PdfPTable coordenadasUTM(PdfWriter writer, DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {0.50f, 0.50f, 0.50f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyStatic("Punto",1,table);
        setearBodyStatic("Este  (E)",1,table);
        setearBodyStatic("Norte  (N)",1,table);
        setearBodyStatic("Referencia",1,table);

        List<InfBasicaAereaDetalleDto> lstCoordenadas = informacionBasicaRepository.listarInfBasicaAerea("PGMF", obj.getIdPlanManejo(), "PGMFCFFMIBUMFUECCC");

        if (lstCoordenadas != null && !lstCoordenadas.isEmpty()) {

            for (InfBasicaAereaDetalleDto element : lstCoordenadas) {
                setearBodyData(element.getPuntoVertice() == null ? "" : element.getPuntoVertice(), 1, table);
                setearBodyData(element.getCoordenadaEste() == null ? "" : element.getCoordenadaEste().toString(), 1, table);
                setearBodyData(element.getCoordenadaNorte() == null ? "" : element.getCoordenadaNorte().toString(), 1, table);
                setearBodyData(element.getReferencia() == null ? "" : element.getReferencia(), 1, table);
            }
        }

        return table;
    }

    public void accesibilidad(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("     3.2.1.  Rutas o vías de acceso terrestre o fluvial a la concesión (Incluir en el mapa 1 y adjuntar el track en formato digital)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        List<InfBasicaAereaDetalleDto> lstRutaAcceso = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> lstCaminoSTFT = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> listaAccesibilidad = informacionBasicaRepository.listarInfBasicaAerea("PGMF", obj.getIdPlanManejo(), "PGMFIBAMA");

        if (listaAccesibilidad != null && !listaAccesibilidad.isEmpty()) {

            lstRutaAcceso = listaAccesibilidad.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            lstCaminoSTFT = listaAccesibilidad.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());
        }

        PdfPTable tableRutaAcceso = new PdfPTable(8);
        float[] medidaCeldasRutaAcceso = {1.40f, 0.50f, 0.50f, 0.50f, 0.50f, 0.50f, 0.50f, 0.80f};
        try {
            tableRutaAcceso.setWidths(medidaCeldasRutaAcceso);
        } catch (DocumentException e) {}
        tableRutaAcceso.setWidthPercentage(95);

        setearRowsColspanStatic("Punto de referencia (carretera, río o quebrada, etc)\n" + "ó tramos",2,1,tableRutaAcceso);
        setearRowsColspanStatic("Punto de inicio",1,2,tableRutaAcceso);
        setearRowsColspanStatic("Punto de llegada",1,2,tableRutaAcceso);
        setearRowsColspanStatic("Distancia\n(km)",2,1,tableRutaAcceso);
        setearRowsColspanStatic("Tiempo\n(horas)",2,1,tableRutaAcceso);
        setearRowsColspanStatic("Medio de transporte ",2,1,tableRutaAcceso);
        setearBodyStatic("Este",1,tableRutaAcceso);
        setearBodyStatic("Norte",1,tableRutaAcceso);
        setearBodyStatic("Este",1,tableRutaAcceso);
        setearBodyStatic("Norte",1,tableRutaAcceso);

        if (lstRutaAcceso != null && !lstRutaAcceso.isEmpty()) {

            for (InfBasicaAereaDetalleDto element : lstRutaAcceso) {
                setearBodyData(element.getReferencia() == null ? "" : element.getReferencia(), 1, tableRutaAcceso);
                setearBodyData(element.getCoordenadaEste() == null ? "" : element.getCoordenadaEste().toString(), 1, tableRutaAcceso);
                setearBodyData(element.getCoordenadaNorte() == null ? "" : element.getCoordenadaNorte().toString(), 1, tableRutaAcceso);
                setearBodyData(element.getCoordenadaEsteFin() == null ? "" : element.getCoordenadaEsteFin().toString(), 1, tableRutaAcceso);
                setearBodyData(element.getCoordenadaNorteFin() == null ? "" : element.getCoordenadaNorteFin().toString(), 1, tableRutaAcceso);
                setearBodyData(element.getDistanciaKm() == null ? "" : element.getDistanciaKm().toString(), 1, tableRutaAcceso);
                setearBodyData(element.getTiempo() == null ? "" : element.getTiempo().toString(), 1, tableRutaAcceso);
                setearBodyData(element.getMedioTransporte() == null ? "" : element.getMedioTransporte(), 1, tableRutaAcceso);
            }
        }
        document.add(tableRutaAcceso);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     3.2.2.  Necesidades de caminos para conectarse con los sistemas de transporte fluviales o terrestres", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableCaminoSTFT = new PdfPTable(1);
        float[] medidaCeldas = {1.40f};
        try {
            tableCaminoSTFT.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        tableCaminoSTFT.setWidthPercentage(95);

        if (lstCaminoSTFT != null && !lstCaminoSTFT.isEmpty()) {

            for (InfBasicaAereaDetalleDto element : lstCaminoSTFT) {
                setearBodyData(element.getDescripcion() == null ? "" : element.getDescripcion(), 1, tableCaminoSTFT);
            }
        }

        document.add(tableCaminoSTFT);
    }

    public void aspectosFisicos(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("     3.3.1.  Ríos (principales y secundarias) Quebradas, lagunas (cochas) en el área. (Incluir en el mapa 1 del Anexo 1)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        List<InfBasicaAereaDetalleDto> lstAspectosFisicosTab1 = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> lstAspectosFisicosTab3 = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> lstAspectosFisicosTab4 = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> lstAspectosFisicos = informacionBasicaRepository.listarInfBasicaAerea("PGMF", obj.getIdPlanManejo(), "PGMFIBAMAF");

        if (lstAspectosFisicos != null && !lstAspectosFisicos.isEmpty()) {

            lstAspectosFisicosTab1 = lstAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            lstAspectosFisicosTab3 = lstAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_3"))
                    .collect(Collectors.toList());

            lstAspectosFisicosTab4 = lstAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_4"))
                    .collect(Collectors.toList());
        }

        PdfPTable tableMorfologica = new PdfPTable(3);
        float[] medidaCeldasMorfologica = {1.20f, 1.20f, 1.20f};
        try {
            tableMorfologica.setWidths(medidaCeldasMorfologica);
        } catch (DocumentException e) {
        }
        tableMorfologica.setWidthPercentage(95);

        setearBodyStatic("Ríos", 1, tableMorfologica);
        setearBodyStatic("Quebradas", 1, tableMorfologica);
        setearBodyStatic("Lagunas (cochas)", 1, tableMorfologica);

        if (lstAspectosFisicosTab1 != null && !lstAspectosFisicosTab1.isEmpty()) {

            for (InfBasicaAereaDetalleDto element : lstAspectosFisicosTab1) {
                setearBodyData(element.getNombreRio() == null ? "" : element.getNombreRio(), 1, tableMorfologica);
                setearBodyData(element.getNombreQuebrada() == null ? "" : element.getNombreQuebrada().toString(), 1, tableMorfologica);
                setearBodyData(element.getNombreLaguna() == null ? "" : element.getNombreLaguna().toString(), 1, tableMorfologica);
            }
        }
        document.add(tableMorfologica);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     3.3.2.  Principales unidades fisiográficas en el área (Incluir en el Mapa 1  del Anexo 1): Se debe de utilizar la “Guía de Inventario de la Flora y Vegetación (RM N° 097-2015-MINAM)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableUnidadFisiografica = unidadesFisiograficas(obj);
        document.add(tableUnidadFisiografica);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     3.3.3.  Principales limitaciones por condiciones biofísicas del área", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableLimitacion = new PdfPTable(1);
        float[] medidaCeldasLimitacion = {1.40f};
        try {
            tableLimitacion.setWidths(medidaCeldasLimitacion);
        } catch (DocumentException e) {
        }
        tableLimitacion.setWidthPercentage(95);

        if (lstAspectosFisicosTab3 != null && !lstAspectosFisicosTab3.isEmpty()) {

            for (InfBasicaAereaDetalleDto element : lstAspectosFisicosTab3) {
                setearBodyData(element.getDescripcion() == null ? "" : element.getDescripcion(), 1, tableLimitacion);
            }
        }
        document.add(tableLimitacion);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     3.3.4.  Información de tipos de suelo del área de la concesión", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableTipoSuelo = new PdfPTable(1);
        float[] medidaCeldasTipoSuelo = {1.40f};
        try {
            tableTipoSuelo.setWidths(medidaCeldasTipoSuelo);
        } catch (DocumentException e) {
        }
        tableTipoSuelo.setWidthPercentage(95);

        if (lstAspectosFisicosTab4 != null && !lstAspectosFisicosTab4.isEmpty()) {

            for (InfBasicaAereaDetalleDto element : lstAspectosFisicosTab4) {
                setearBodyData(element.getDescripcion() == null ? "" : element.getDescripcion(), 1, tableTipoSuelo);
            }
        }
        document.add(tableTipoSuelo);
    }

    

    public PdfPTable unidadesFisiograficas(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {1.40f, 0.50f, 0.50f, 0.50f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Unidades fisiográficas", 1, table);
        setearBodyStatic("Marcar con un aspa", 1, table);
        setearBodyStatic("Área (ha)", 1, table);
        setearBodyStatic("%", 1, table);

        ResultClassEntity resultUnidadF = unidadFisiograficaRepository.listarUnidadFisiografica(1);// falta arreglar

        if (resultUnidadF != null && resultUnidadF.getSuccess()) {

            List<UnidadFisiograficaUMFEntity> lstUnidadFisiografica = (ArrayList<UnidadFisiograficaUMFEntity>) resultUnidadF.getData();

            if (lstUnidadFisiografica != null && !lstUnidadFisiografica.isEmpty()) {
                for (UnidadFisiograficaUMFEntity element : lstUnidadFisiografica) {
                    setearBodyData(element.getDescripcion() == null ? "" : element.getDescripcion(), 1, table);
                    setearBodyData(element.getAccion() == null ? "" : element.getAccion() ? "X" : "", 1, table);
                    setearBodyData(element.getArea() == null ? "" : element.getArea().toString(), 1, table);
                    setearBodyData(element.getPorcentaje() == null ? "" : element.getPorcentaje().toString(), 1, table);
                }
            }
        }

        return table;
    }

    public void aspectosBiologicos(Document document, DescargarPGMFDto obj) throws Exception {
        //----DATA---
        InfBasicaAereaDetalleDto param = new InfBasicaAereaDetalleDto();
        param.setCodInfBasica("PGMF");
        param.setIdPlanManejo(obj.getIdPlanManejo());
        param.setCodInfBasicaDet("PGMFCFFMIBUMFABIOFS");
        param.setPageNum(1);
        param.setPageSize(10);
        //("PGMF",obj.getIdPlanManejo(),"PGMFCFFMIBUMFABIOFS",1,10);
        ResultClassEntity result = informacionBasicaRepository.listarPorFiltrosInfBasicaAerea(param);
        List<InfBasicaAereaDetalleDto> listFauna = (List<InfBasicaAereaDetalleDto>) result.getData();

        //---TABLA---
        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.35f, 1.15f, 1.50f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Nombre común", 1, table);
        setearBodyStatic("Nombre científico", 1, table);
        setearBodyStatic("Familia", 1, table);

        for (InfBasicaAereaDetalleDto e : listFauna) {
            setearBodyData(e.getNombreComun()==null?"":e.getNombreComun(), 1, table);
            setearBodyData(e.getNombreCientifico()==null?"":e.getNombreCientifico(), 1, table);
            setearBodyData(e.getFamilia()==null?"":e.getFamilia(), 1, table);
        }

        document.add(table);

    }

    public PdfPTable tipoBosque(DescargarPGMFDto obj) throws Exception {
        //Se obtiene del api externo geoespacial - url: https://sniffs.serfor.gob.pe/apigeoforestal/servicios/identifica/tipos/bosques
        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.40f, 0.80f, 0.50f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Tipo de bosque", 1, table);
        setearBodyStatic("Área (ha)", 1, table);
        setearBodyStatic("%", 1, table);

        return table;
    }

    public void aspectosSocioeconomicos(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("     3.5.1.  Caracterización de la Población", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        List<InfBasicaAereaDetalleDto> lstCaractPoblacion = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> lstInfraServicio = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> lstAspectoSocioEconomico = informacionBasicaRepository.listarInfBasicaAerea("PGMF", obj.getIdPlanManejo(), "PGMFIBAMAS");

        if (lstAspectoSocioEconomico != null && !lstAspectoSocioEconomico.isEmpty()) {

            lstCaractPoblacion = lstAspectoSocioEconomico.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            lstInfraServicio = lstAspectoSocioEconomico.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());
        }

        PdfPTable tableCaractPoblacion = new PdfPTable(7);
        float[] medidaCeldasCaractPoblacion = {1.20f, 1.40f, 0.50f, 0.50f, 0.50f, 0.50f, 1.40f};
        try {
            tableCaractPoblacion.setWidths(medidaCeldasCaractPoblacion);
        } catch (DocumentException e) {
        }
        tableCaractPoblacion.setWidthPercentage(95);

        setearBodyStatic("Población aledaña o al interior de la concesión", 1, tableCaractPoblacion);
        setearBodyStatic("Nombre", 1, tableCaractPoblacion);
        setearBodyStatic("Este", 1, tableCaractPoblacion);
        setearBodyStatic("Norte", 1, tableCaractPoblacion);
        setearBodyStatic("Área (ha)", 1, tableCaractPoblacion);
        setearBodyStatic("Nº de Familias", 1, tableCaractPoblacion);
        setearBodyStatic("Descripción de actividades que realizan", 1, tableCaractPoblacion);

        if (lstCaractPoblacion != null && !lstCaractPoblacion.isEmpty()) {
            String actividadRealizar = "";
            for (InfBasicaAereaDetalleDto element : lstCaractPoblacion) {
                actividadRealizar = "";
                setearBodyData(element.getDescripcion() == null ? "" : element.getDescripcion(), 1, tableCaractPoblacion);
                setearBodyData(element.getNombre() == null ? "" : element.getNombre(), 1, tableCaractPoblacion);
                setearBodyData(element.getCoordenadaEste() == null ? "" : element.getCoordenadaEste().toString(), 1, tableCaractPoblacion);
                setearBodyData(element.getCoordenadaNorte() == null ? "" : element.getCoordenadaNorte().toString(), 1, tableCaractPoblacion);
                setearBodyData(element.getAreaHa() == null ? "" : element.getAreaHa().toString(), 1, tableCaractPoblacion);
                setearBodyData(element.getNumeroFamilia() == null ? "" : element.getNumeroFamilia().toString(), 1, tableCaractPoblacion);
                actividadRealizar = "Subsistencia " ;
                if(element.getSubsistencia() != null && element.getSubsistencia()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";
                actividadRealizar += "\nPerennes " ;
                if(element.getPerenne() != null && element.getPerenne()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";
                actividadRealizar += "\nGanadería ";
                if(element.getGanaderia() != null && element.getGanaderia()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";
                actividadRealizar += "\nCaza ";
                if(element.getCaza() != null && element.getCaza()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";
                actividadRealizar += "\nPesca ";
                if(element.getPesca() != null && element.getPesca()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";
                actividadRealizar += "\nMadera ";
                if(element.getMadera() != null && element.getMadera()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";
                actividadRealizar += "\nOtros productos ";
                if(element.getOtroProducto() != null && element.getOtroProducto()) actividadRealizar += "(X)";
                else actividadRealizar += "( )";

                setearBodyData(actividadRealizar, 1, tableCaractPoblacion);
            }
        }
        document.add(tableCaractPoblacion);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     3.5.2.  Infraestructura se servicios (en la concesión o su entorno)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableInfraServicio = new PdfPTable(5);
        float[] medidaCeldasInfraServicion = {1.20f, 1.40f, 0.50f, 0.50f, 1.20f};
        try {
            tableInfraServicio.setWidths(medidaCeldasInfraServicion);
        } catch (DocumentException e) {
        }
        tableInfraServicio.setWidthPercentage(95);

        setearBodyStatic("Infraestructura", 1, tableInfraServicio);
        setearBodyStatic("Nombre", 1, tableInfraServicio);
        setearBodyStatic("Este", 1, tableInfraServicio);
        setearBodyStatic("Norte", 1, tableInfraServicio);
        setearBodyStatic("Acceso", 1, tableInfraServicio);

        if (lstInfraServicio != null && !lstInfraServicio.isEmpty()) {
            for (InfBasicaAereaDetalleDto element : lstInfraServicio) {
                setearBodyData(element.getDescripcion() == null ? "" : element.getDescripcion(), 1, tableInfraServicio);
                setearBodyData(element.getNombre() == null ? "" : element.getNombre(), 1, tableInfraServicio);
                setearBodyData(element.getCoordenadaEste() == null ? "" : element.getCoordenadaEste().toString(), 1, tableInfraServicio);
                setearBodyData(element.getCoordenadaNorte() == null ? "" : element.getCoordenadaNorte().toString(), 1, tableInfraServicio);
                setearBodyData(element.getAcceso() == null ? "" : element.getAcceso(), 1, tableInfraServicio);
            }
        }
        document.add(tableInfraServicio);
    }

    public void antecedenteIdentifConflicto(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("     3.6.1.  Antecedentes de uso de recursos anterior al otorgamiento de la concesión", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableAntcedente = antecedenteUsoRecurso(obj);
        document.add(tableAntcedente);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     3.6.2.  Identificación y manejo de conflictos de uso de la tierra y los recursos forestales", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableManejoConflicto = identificacionConfilcto(obj);
        document.add(tableManejoConflicto);
    }

    public PdfPTable antecedenteUsoRecurso(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {1.40f, 0.50f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Actividades", 1, table);
        setearBodyStatic("Marcar con un aspa\n( X )", 1, table);
        setearBodyStatic("Especies más extraídas", 1, table);
        setearBodyStatic("Observaciones", 1, table);

        List<InfBasicaAereaDetalleDto> lstResult = informacionBasicaRepository.listarInfBasicaAerea("PGMF", obj.getIdPlanManejo(), "PGMFAUIC");

        if (lstResult != null && !lstResult.isEmpty()) {

            List<InfBasicaAereaDetalleDto> lstAntecedente  = lstResult.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            if (lstAntecedente != null && !lstAntecedente.isEmpty()) {
                for (InfBasicaAereaDetalleDto element : lstAntecedente) {
                    setearBodyData(element.getActividad() == null ? "" : element.getActividad(), 1, table);
                    setearBodyData(element.getMarcar() == null ? "" : element.getMarcar()? "X": "", 1, table);
                    setearBodyData(element.getEspecieExtraida() == null ? "" : element.getEspecieExtraida(), 1, table);
                    setearBodyData(element.getObservaciones() == null ? "" : element.getObservaciones(), 1, table);
                }
            }
        }

        return table;
    }

    public PdfPTable identificacionConfilcto(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(2);
        float[] medidaCeldas = {1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Conflicto", 1, table);
        setearBodyStatic("Propuesta de manejo o solución", 1, table);

        List<InfBasicaAereaDetalleDto> lstResult = informacionBasicaRepository.listarInfBasicaAerea("PGMF", obj.getIdPlanManejo(), "PGMFIBAMAIC");

        if (lstResult != null && !lstResult.isEmpty()) {

            List<InfBasicaAereaDetalleDto> lstConflicto = lstResult.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());

            if (lstConflicto != null && !lstConflicto.isEmpty()) {
                for (InfBasicaAereaDetalleDto element : lstConflicto) {
                    setearBodyData(element.getConflicto() == null ? "" : element.getConflicto(), 1, table);
                    setearBodyData(element.getSolucion() == null ? "" : element.getSolucion(), 1, table);
                }
            }
        }

        return table;
    }

    public PdfPTable usoPotencial(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.40f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Categoria por Ordenamiento", 1, table);
        setearBodyStatic("Uso Potencial", 1, table);
        setearBodyStatic("Actividades a Realizar", 1, table);

        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFUPCO", null);

        if (lista != null && !lista.isEmpty()) {

            List<ManejoBosqueDetalleEntity> lstDetalle = lista.get(0).getListManejoBosqueDetalle();
            if (lstDetalle != null && !lstDetalle.isEmpty()) {
                for (ManejoBosqueDetalleEntity element : lstDetalle) {
                    setearBodyData(element.getCatOrdenamiento() == null ? "" : element.getCatOrdenamiento(), 1, table);
                    setearBodyData(element.getDescripcionOrd() == null ? "" : element.getDescripcionOrd(), 1, table);
                    setearBodyData(element.getActividad() == null ? "" : element.getActividad(), 1, table);
                }
            }
        }


        return table;
    }

    public PdfPTable sistemaManejo(DescargarPGMFDto obj) throws Exception {

        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFST", null);
        ManejoBosqueDetalleEntity objDet = new ManejoBosqueDetalleEntity();
        if (lista != null && !lista.isEmpty()) {
            objDet = (ManejoBosqueDetalleEntity) lista.get(0).getListManejoBosqueDetalle().get(0);
        }

        PdfPTable table = new PdfPTable(5);
        float[] medidaCeldas = {0.20f,0.80f,0.20f,0.80f,1f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyData(objDet.getCodOpcion()==null||objDet.getCodOpcion().equals("N")?"":"X" , 1, table);
        setearBodyStatic("Sistema Policíclico Basado en la Regeneración Natural", 1, table);
        setearBodyData(objDet.getCodOpcion()==null||objDet.getCodOpcion().equals("S")?"":"X" , 1, table);
        setearBodyStatic("Otro Sistema", 1, table);
        setearBodyDataSborder("",1,1,table); 

        if(objDet.getCodOpcion().equals("N")){
            setearBodyDataSborder("Especificar",1,2,table);
            setearBodyDataSborder("",1,3,table);
            setearBodyData(objDet.getDescripcionOrd()==null?"":objDet.getDescripcionOrd(),5, table);
        }

        return table;
    }

    public PdfPTable cicloCorta(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(1);
        float[] medidaCeldas = {1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFCC", null);

        if (lista != null && !lista.isEmpty()) {

            List<ManejoBosqueDetalleEntity> lstDetalle = lista.get(0).getListManejoBosqueDetalle();
            if (lstDetalle != null && !lstDetalle.isEmpty()) {
                for (ManejoBosqueDetalleEntity element : lstDetalle) {
                    setearBodyData(element.getDescripcionOrd() == null ? "" : element.getDescripcionOrd(), 1, table);
                }
            }
        }


        return table;
    }

    public PdfPTable especiesManejo(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(5);
        float[] medidaCeldas = {1.40f, 1.40f, 1.20f, 0.80f, 0.80f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Nombre científico", 1, table);
        setearBodyStatic("Nombre común", 1, table);
        setearBodyStatic("Línea de producción", 1, table);
        setearBodyStatic("DMC Normado\n(cm)", 1, table);
        setearBodyStatic("DMC Propuesto\n(cm)", 1, table);

        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFEMDMC", null);

        if (lista != null && !lista.isEmpty()) {
            List<ManejoBosqueDetalleEntity> lstDetalle = lista.get(0).getListManejoBosqueDetalle();

            if (lstDetalle != null && !lstDetalle.isEmpty()) {
                for (ManejoBosqueDetalleEntity element : lstDetalle) {
                    setearBodyData(element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro(), 1, table);
                    setearBodyData(element.getEspecie() == null ? "" : element.getEspecie(), 1, table);
                    setearBodyData(element.getLineaProduccion() == null ? "" : element.getLineaProduccion(), 1, table);
                    setearBodyData(element.getNuDiametro() == null ? "" : element.getNuDiametro().toString(), 1, table);
                    setearBodyData(element.getNuCaminoAcceso() == null ? "" : element.getNuCaminoAcceso().toString(), 1, table);
                }
            }
        }


        return table;
    }

    public PdfPTable especiesFlora(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.40f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Nombre común", 1, table);
        setearBodyStatic("Nombre científico", 1, table);
        setearBodyStatic("Justificación", 1, table);

        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFEFP", null);

        if (lista != null && !lista.isEmpty()) {
            List<ManejoBosqueDetalleEntity> lstDetalle = lista.get(0).getListManejoBosqueDetalle();

            if (lstDetalle != null && !lstDetalle.isEmpty()) {
                for (ManejoBosqueDetalleEntity element : lstDetalle) {
                    setearBodyData(element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro(), 1, table);
                    setearBodyData(element.getEspecie() == null ? "" : element.getEspecie(), 1, table);
                    setearBodyData(element.getDescripcionOtros() == null ? "" : element.getDescripcionOtros(), 1, table);
                }
            }
        }


        return table;
    }

    public void corteVcp(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        //preface.add(new Paragraph("     Por Bloque:", subTituloTabla));
        preface.add(new Paragraph("     Total UMF:", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable table = new PdfPTable(1);
        float[] medidaCeldas = {1.40f};

        List<VolumenComercialPromedioCabeceraDto> lstHeader = new ArrayList<>();
        List<VolumenComercialPromedioEspecieDto> lstBody = new ArrayList<>();

        ResultClassEntity resultVolumenComerProm = censoForestalRepository.listarVolumenComercialPromedio(obj.getIdPlanManejo(), "PGMF");
        ResultClassEntity resultEspVolumenComerProm = censoForestalRepository.listarEspecieVolumenComercialPromedio(obj.getIdPlanManejo(), "PGMF");

        if (resultVolumenComerProm != null && resultVolumenComerProm.getSuccess() && resultEspVolumenComerProm != null && resultEspVolumenComerProm.getSuccess()) {
            lstHeader = (ArrayList<VolumenComercialPromedioCabeceraDto>) resultVolumenComerProm.getData();
            lstBody = (ArrayList<VolumenComercialPromedioEspecieDto>) resultEspVolumenComerProm.getData();
        }

        if (lstHeader != null && !lstHeader.isEmpty()) {

            Integer columnas = lstHeader.size();

            table = new PdfPTable(columnas);
            medidaCeldas = new float[columnas];

            for (Integer i = 0; i<columnas; i++) {
                medidaCeldas[i] = 1.40f;
            }

            try {
                table.setWidths(medidaCeldas);
            } catch (DocumentException e) {
            }
            table.setWidthPercentage(95);

            for (VolumenComercialPromedioCabeceraDto element : lstHeader) {
                setearBodyStatic(element.getBloquequinquenal() == null ? "" : "Tipo de Bloqueo: " + element.getBloquequinquenal(), 1, table);
                setearBodyStatic(element.getToTalAreaHa() == null ? "" : "Área (ha): " + element.getToTalAreaHa().toString(), 1, table);
            }

            document.add(table);

            preface = new Paragraph();
            addEmptyLine(preface, 1);
            document.add(preface);
        }

        table = new PdfPTable(7);
        medidaCeldas = new float[]{ 1.40f, 1.40f, 0.50f, 0.50f, 0.50f, 0.50f, 0.50f };
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Especies", 1, 2,table);
        setearBodyStatic("DCM", 1, table);
        setearRowsColspanStatic("N° árboles", 1, 2, table);
        setearRowsColspanStatic("Vcp (m2)", 1, 2, table);

        setearBodyStatic("Nombre común", 1, table);
        setearBodyStatic("Nombre científico", 1, table);
        setearBodyStatic("(cm)", 1, table);
        setearBodyStatic("ha", 1, table);
        setearBodyStatic("Total", 1, table);
        setearBodyStatic("ha", 1, table);
        setearBodyStatic("Total", 1, table);

        if (lstBody != null && !lstBody.isEmpty()) {

            for (VolumenComercialPromedioEspecieDto element : lstBody) {
                setearBodyData(element.getNombreComun() == null ? "" : element.getNombreComun(), 1, table);
                setearBodyData(element.getNombreCientifico() == null ? "" :  element.getNombreCientifico(), 1, table);
                setearBodyData(element.getDMC() == null ? "" :  element.getDMC().toString(), 1, table);
                setearBodyData(element.getCantidadArb() == null ? "" :  element.getCantidadArb().toString(), 1, table);
                setearBodyData(element.getTotalArb() == null ? "" :  element.getTotalArb().toString(), 1, table);
                setearBodyData(element.getVolumenComercialPromedio() == null ? "" :  element.getVolumenComercialPromedio().toString(), 1, table);
                setearBodyData(element.getVolumenComercialPromedioTotal() == null ? "" :  element.getVolumenComercialPromedioTotal().toString(), 1, table);
            }
        }

        document.add(table);
        /*
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     Total UMF:", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        document.add(table);
         */
    }

    public PdfPTable corteVcap(DescargarPGMFDto obj) throws Exception {
        //------DATA-----
        ResultClassEntity result = censoForestalRepository.listarEspecieVolumenCortaAnualPermisible(obj.getIdPlanManejo(), "PGMF");
        List<VolumenComercialPromedioEspecieDto> listEspecie = new ArrayList<>();
        if(result!=null && result.getSuccess() && result.getData()!=null){
            listEspecie =  (List<VolumenComercialPromedioEspecieDto>) result.getData();
        }
        
        //---TABLA----
        PdfPTable table = new PdfPTable(6);
        float[] medidaCeldas = {0.80f, 0.50f, 0.50f, 0.50f, 0.50f,0.50f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Tipo de bosque", 2,1, table);
        setearRowsColspanStatic("AFP", 1,2, table);
        setearRowsColspanStatic("Vcp\n(m3/ha)", 2,1, table);
        setearRowsColspanStatic("Vcp pond.\n(m3)", 2,1, table);
        setearRowsColspanStatic("VCAP\n(m3)", 2,1, table);
        setearBodyStatic("Ha",1, table);
        setearBodyStatic("%",1, table);

        for (VolumenComercialPromedioEspecieDto e : listEspecie) {
            setearBodyData(e.getNombreComun()==null?"":e.getNombreComun(), 1, table);
            setearBodyData(e.getTotalArb()==null?"":e.getTotalArb().toString(),1, table);
            setearBodyData(e.getPorcentajeArb()==null?"":e.getPorcentajeArb().toString() ,1, table);
            setearBodyData(e.getVolumenComercialPromedio()==null?"":e.getVolumenComercialPromedio().toString(), 1, table);
            setearBodyData(e.getVolumenComercialPromedioPonderado()==null?"":e.getVolumenComercialPromedioPonderado().toString(),1, table);
            setearBodyData(e.getVolumenCortaAnualPermisible()==null?"":e.getVolumenCortaAnualPermisible().toString(),1, table);
        }

        return table;
    }

    public PdfPTable proyeccionCosecha(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.40f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Clase diamétrica", 1, table);
        setearBodyStatic("N°/Arb/ha", 1, table);
        setearBodyStatic("Cosechas", 1, table);

        return table;
    }

    public PdfPTable especificacionesAprovechamiento(DescargarPGMFDto obj) throws Exception {

        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {1.20f, 1.40f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Operaciones", 1, table);
        setearBodyStatic("Método y diseño", 1, table);
        setearBodyStatic("Personal (tipo y número)", 1, table);
        setearBodyStatic("Maquinaria y equipos (tipo y número)", 1, table);

        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFESA", null);

        if (lista != null && !lista.isEmpty()) {
            List<ManejoBosqueDetalleEntity> lstEspecSistAprov = lista.get(0).getListManejoBosqueDetalle();

            if (lstEspecSistAprov != null && !lstEspecSistAprov.isEmpty()) {
                for (ManejoBosqueDetalleEntity element : lstEspecSistAprov) {
                    setearBodyStatic(element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural(), 1, table);
                    setearBodyData(element.getMetodoConstruccion() == null ? "" : element.getMetodoConstruccion(), 1, table);
                    setearBodyData(element.getManoObra() == null ? "" : element.getManoObra(), 1, table);
                    setearBodyData(element.getMaquina() == null ? "" : element.getMaquina(), 1, table);
                }
            }
        }
        return table;
    }
    
    public  void ordenamientoTab4(Document document, DescargarPGMFDto obj) throws Exception {
        //------DATA-------//
        OrdenamientoProteccionEntity op = new OrdenamientoProteccionEntity();
        op.setIdPlanManejo(obj.getIdPlanManejo());
        op.setCodTipoOrdenamiento("PGMF");
        ResultEntity categorias = ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(op);

        List<OrdenamientoProteccionDetalleEntity> list3 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> list2 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listBloquesQuincenales = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listaParcelaCorta1 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listaParcelaCorta2 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listUbicacionMarcador = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listSenalizacion = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listDemarcacion = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listVigilancia = new ArrayList<>();

        if (categorias != null && categorias.getIsSuccess()) {

            List<OrdenamientoProteccionEntity> listaOrdenamiento = categorias.getData();

            list3 = listaOrdenamiento.get(0).getListOrdenamientoProteccionDet()
                    .stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFCO"))
                    .collect(Collectors.toList());


            // 4.2.1 DIVISION ADMINISTRATIVA
            list2 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABBQ")
                            && c.getCategoria() != null && c.getCategoria().equals("A"))
                    .collect(Collectors.toList());


            listBloquesQuincenales = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABBQ")
                            && c.getCategoria() != null && c.getCategoria().equals("B"))
                    .collect(Collectors.toList());


            // 4.2.2 PARCELAS DE CORTA
            listaParcelaCorta1 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABPC") &&
                            c.getDescripcion().equals("OPCION1"))
                    .collect(Collectors.toList());
            listaParcelaCorta1 = calcularAreaTotalParcela(listaParcelaCorta1);


            listaParcelaCorta2 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABPC") &&
                            c.getDescripcion().equals("OPCION2"))
                    .collect(Collectors.toList());
            listaParcelaCorta2 = calcularAreaTotalParcela(listaParcelaCorta2);


            // 4.3 PROTECCION Y VIGILANCIA
            // 4.3.1
            listUbicacionMarcador = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVUMV"))
                    .collect(Collectors.toList());


            // 4.3.2
            listSenalizacion = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVS"))
                    .collect(Collectors.toList());


            // 4.3.3
            listDemarcacion = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVDML"))
                    .collect(Collectors.toList());


            // 4.3.4
            listVigilancia = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVVUMF"))
                    .collect(Collectors.toList());
            System.out.println(listVigilancia);
        }
        
        //----------------//

        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.8f, 0.60f, 0.60f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyStatic("Categoria de Ordenamiento", 1, table);
        setearBodyStatic("Área (ha)", 1, table);
        setearBodyStatic("%", 1, table);
        for (OrdenamientoProteccionDetalleEntity e : list3) {
            setearBodyData(e.getCategoria()==null?"":e.getCategoria(), 1, table);
            setearBodyData(e.getAreaHA()==null?"":e.getAreaHA().toString(), 1, table);
            setearBodyData(e.getAreaHAPorcentaje()==null?"":e.getAreaHAPorcentaje().toString(), 1, table);
        }
        
        document.add(table);
        //-------------------------------------------------------------
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("4.2.	 División administrativa del bosque", subTituloTabla));
        preface.add(new Paragraph("     4.2.1	Bloques quinquenales (opcional)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);
        float[] medidaCeldas2 = {1.20f, 0.20f, 1.60f};
        try {
            table.setWidths(medidaCeldas2);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyDataSborder("Número: ", 1,1, table);
        setearBodyData(list2.size()==0 || list2.get(0).getDescripcion()==null?"":list2.get(0).getDescripcion(), 1, table);
        setearBodyDataSborder(" ", 1,1, table);
        setearBodyDataSborder(" ",1, 3, table);
        setearBodyDataSborder("Periodo de aprovechamiento (años): ",1, 1, table);
        setearBodyData(list2.size()==0 || list2.get(0).getObservacion()==null?"":list2.get(0).getObservacion(), 1, table);
        setearBodyDataSborder(" ",1, 1, table);

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     Área forestal productiva (AFP) en los bloques quinquenales (opcional)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(4);
        float[] medidaCeldas3 = {0.50f, 1.50f, 0.50f, 0.50f};
        try {
            table.setWidths(medidaCeldas3);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Bloque", 2, 1, table);
        setearRowsColspanStatic("Tipo de Bosque", 2, 1, table);
        setearRowsColspanStatic("AFP", 1, 2, table);
        setearBodyStatic("Ha", 1, table);
        setearBodyStatic("%", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listBloquesQuincenales) {
            setearBodyData(e.getBloqueQuinquenal()==null?"":e.getBloqueQuinquenal(),1, table);
            setearBodyData(e.getTipoBosque()==null?"":e.getTipoBosque() , 1, table);
            setearBodyData(e.getAreaHA()==null?"":e.getAreaHA().toString(),1,table);
            setearBodyData(e.getAreaHAPorcentaje()==null?"":e.getAreaHAPorcentaje().toString(),1,table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     4.2.2	Parcelas de Corta (PC)", subTituloTabla));
        preface.add(new Paragraph("Área forestal productiva (AFP) en las PC por cada bloque quinquenal (primera opción) y/o parcelas de corta establecidas indistintamente en el área de la UMF (segunda opción)", subTituloTabla));
        preface.add(new Paragraph("     Primera Opción", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(4);
        table.setWidths(medidaCeldas3);
        table.setWidthPercentage(95);

        setearRowsColspanStatic("PC", 2, 1, table);
        setearRowsColspanStatic("Tipo de Bosque", 2, 1, table);
        setearRowsColspanStatic("AFP", 1, 2, table);
        setearBodyStatic("Ha", 1, table);
        setearBodyStatic("%", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listaParcelaCorta1) {
            setearBodyData(e.getParcelaCorta()==null?"":e.getParcelaCorta(),1, table);
            setearBodyData(e.getTipoBosque()==null?"":e.getTipoBosque() , 1, table);
            setearBodyData(e.getAreaHA()==null?"":e.getAreaHA().toString(),1,table);
            setearBodyData(e.getAreaHAPorcentaje()==null?"":e.getAreaHAPorcentaje().toString(),1,table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     Segunda Opción", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(4);
        table.setWidths(medidaCeldas3);
        table.setWidthPercentage(95);

        setearRowsColspanStatic("PC", 2, 1, table);
        setearRowsColspanStatic("Tipo de Bosque", 2, 1, table);
        setearRowsColspanStatic("AFP", 1, 2, table);
        setearBodyStatic("Ha", 1, table);
        setearBodyStatic("%", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listaParcelaCorta2) {
            setearBodyData(e.getParcelaCorta()==null?"":e.getParcelaCorta(),1, table);
            setearBodyData(e.getTipoBosque()==null?"":e.getTipoBosque() , 1, table);
            setearBodyData(e.getAreaHA()==null?"":e.getAreaHA().toString(),1,table);
            setearBodyData(e.getAreaHAPorcentaje()==null?"":e.getAreaHAPorcentaje().toString(),1,table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     4.2.3	Frentes de corta: ", subTituloTabla));
        preface.add(new Paragraph("Se debe señalar si el aprovechamiento se realiza en más de un frente de corta de la UMF. Los frentes de corta deben delimitarse en el Mapa 3 del Anexo 1 y proponerse respetando el principio de sostenibilidad del ciclo de corta. ", subTituloTabla));
        addEmptyLine(preface,1);
        preface.add(new Paragraph("4.3.	 Protección y vigilancia:", subTituloTabla));
        preface.add(new Paragraph("     4.3.1	Ubicación y marcado de vértices: ", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(6);
        table.setWidthPercentage(95);

        setearBodyStatic("Vértice", 1, table);
        setearBodyStatic("Este", 1, table);
        setearBodyStatic("Norte", 1, table);
        setearBodyStatic("Zona UTM", 1, table);
        setearBodyStatic("Material", 1, table);
        setearBodyStatic("Observación", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listUbicacionMarcador) {
            setearBodyData(e.getVerticeBloque()==null?"":e.getVerticeBloque() , 1, table);
            setearBodyData(e.getCoordenadaEste()==null?"":e.getCoordenadaEste().toString() ,1, table);
            setearBodyData(e.getCoordenadaNorte()==null?"":e.getCoordenadaNorte().toString() , 1, table);
            setearBodyData(e.getZonaUTM()==null?"":e.getZonaUTM().toString(), 1, table);
            setearBodyData(e.getDescripcion()==null?"":e.getDescripcion(), 1, table);
            setearBodyData(e.getObservacion()==null?"":e.getObservacion(),1,table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("     4.3.2	Señalización ", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(6);
        table.setWidthPercentage(95);

        setearBodyStatic("Lugar", 1, table);
        setearBodyStatic("Este", 1, table);
        setearBodyStatic("Norte", 1, table);
        setearBodyStatic("Zona UTM", 1, table);
        setearBodyStatic("Material", 1, table);
        setearBodyStatic("Observación", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listSenalizacion) {
            setearBodyData(e.getVerticeBloque()==null?"":e.getVerticeBloque() , 1, table);
            setearBodyData(e.getCoordenadaEste()==null?"":e.getCoordenadaEste().toString() ,1, table);
            setearBodyData(e.getCoordenadaNorte()==null?"":e.getCoordenadaNorte().toString() , 1, table);
            setearBodyData(e.getZonaUTM()==null?"":e.getZonaUTM().toString(), 1, table);
            setearBodyData(e.getDescripcion()==null?"":e.getDescripcion(), 1, table);
            setearBodyData(e.getObservacion()==null?"":e.getObservacion(),1,table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("     4.3.3	Demarcación y mantenimiento de linderos", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);
        table.setWidthPercentage(95);

        setearBodyStatic("Sector del lindero", 1, table);
        setearBodyStatic("Riesgo identificado", 1, table);
        setearBodyStatic("Longitud del lindero a marcar y mantener(km)", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listDemarcacion) {
            setearBodyData(e.getActividad()==null?"":e.getActividad(), 1, table);
            setearBodyData(e.getDescripcion()==null?"":e.getDescripcion(), 1, table);
            setearBodyData(e.getObservacion()==null?"":e.getObservacion(), 1, table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("     4.3.4	Vigilancia de la UMF", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);
        table.setWidthPercentage(95);

        setearBodyStatic("Identificación de riesgos y vulnerabilidad", 1, table);
        setearBodyStatic("Actividades de vigilancia", 1, table);
        setearBodyStatic("Infraestructura y personal requerido", 1, table);

        for (OrdenamientoProteccionDetalleEntity e : listVigilancia) {
            setearBodyData(e.getActividad()==null?"":e.getActividad(), 1, table);
            setearBodyData(e.getDescripcion()==null?"":e.getDescripcion(), 1, table);
            setearBodyData(e.getObservacion()==null?"":e.getObservacion(), 1, table);
        }

        document.add(table);

    }

    public  void potencialProduccionTab5(Document document, DescargarPGMFDto obj) throws Exception { 
        //------DATA-------//
        PotencialProduccionForestalEntity tab1 = new PotencialProduccionForestalEntity();
        PotencialProduccionForestalEntity tab2 = new PotencialProduccionForestalEntity();

        List<PotencialProduccionForestalEntity> listaProduccionForestal = potencialProduccionForestalRepository
        .ListarPotencialProducForestal(obj.getIdPlanManejo() , "PGMF");

        List<PotencialProduccionForestalEntity> respProdTab1 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMCIF"))
                .collect(Collectors.toList());

        List<PotencialProduccionForestalEntity> respProdTab2 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRPM"))
                .collect(Collectors.toList());

        if (respProdTab1 != null && !respProdTab1.isEmpty()) {
            for (PotencialProduccionForestalEntity e : respProdTab1) {
                tab1 = e;
            } 
        }

        if (respProdTab2 != null && !respProdTab2.isEmpty()) {
            for (PotencialProduccionForestalEntity e : respProdTab2) {
                tab2 = e;
            }
        }

        List<ResultadosEspeciesMuestreoDto> censoForestalEspecies = new ArrayList<>();

        List<PotencialProduccionForestalEntity> listaProduccionTab3 = new ArrayList<>();
        PotencialProduccionForestalEntity prodTab3 = new PotencialProduccionForestalEntity();

        List<ResultadosEspeciesMuestreoDto> resForEsp = censoForestalRepository
                .ResultadosEspecieMuestreo(obj.getIdPlanManejo(),"PGMF");

        List<PotencialProduccionForestalEntity> resProTab3 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRPMB"))
                .collect(Collectors.toList());

        if (resProTab3 != null && !resProTab3.isEmpty()) {
            if (resProTab3!=null && !resProTab3.isEmpty() && resProTab3.get(0)!=null) {
                listaProduccionTab3 = resProTab3;
                prodTab3 = listaProduccionTab3.get(0);
            }
            
        }

        List<ResultadosEspeciesMuestreoDto> listaProdFor = new ArrayList<>();

        List<ResultadosEspeciesMuestreoDto> listaProduccionForestal2 = censoForestalRepository
                .ResultadosEspecieMuestreo(obj.getIdPlanManejo(), "PGMF");
        if (listaProduccionForestal2 != null && !listaProduccionForestal2.isEmpty()) {
            listaProdFor = listaProduccionForestal2.stream()
                    .filter(c -> c.getListTablaEspeciesMuestreo() != null
                            && !c.getListTablaEspeciesMuestreo().isEmpty())
                    .collect(Collectors.toList());
        }

        if (resForEsp != null && !resForEsp.isEmpty()) {
            censoForestalEspecies = resForEsp;
        }

        List<ResultadosEspeciesMuestreoDto> lista53 = new ArrayList<>();

        ResultClassEntity resFustales = censoForestalRepository.listarResultadosFustales(obj.getIdPlanManejo(), "PGMF");
        if (resFustales != null && resFustales.getSuccess()) {
            List<ResultadosEspeciesMuestreoDto> objList = (ArrayList<ResultadosEspeciesMuestreoDto>) resFustales
                    .getData();
            System.out.println(objList);

            lista53 = objList.stream()
                    .filter(c -> c.getListTablaEspeciesMuestreo() != null
                            && !c.getListTablaEspeciesMuestreo().isEmpty())
                    .collect(Collectors.toList());
        }

        List<PotencialProduccionForestalEntity> listaProduccionTab532 = new ArrayList<>();

        List<PotencialProduccionForestalEntity> resProddTab532 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRFD"))
                .collect(Collectors.toList());

        if (resProddTab532 != null && !resProddTab532.isEmpty()) {
            listaProduccionTab532 = resProddTab532;
        }

        List<AprovechamientoRFNMDto> listaPotencialProduccion = censoForestalRepository
                .ResultadosAprovechamientoRFNM(obj.getIdPlanManejo(), "PGMF");

        //----------------//

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(95);

        setearBodyStatic("Diseño",1, table);
        setearRowsColspanData(tab1.getDisenio()==null?"":tab1.getDisenio(), 1, 3, table);
        setearBodyStatic("Diámetro mínimo de inventario (cm)",1, table);
        setearBodyData(tab1.getDiametroMinimoInventariadaCM()==null?"":tab1.getDiametroMinimoInventariadaCM().toString(),1, table);
        setearBodyStatic("Intensidad de muestreo (%)", 1, table);
        setearBodyData(tab1.getIntensidadMuestreoPorcentaje()==null?"":tab1.getIntensidadMuestreoPorcentaje().toString(),1, table);
        setearBodyStatic("Tamaño de parcela (m)",1, table);
        setearBodyData(tab1.getTamanioParcela()==null?"":tab1.getTamanioParcela().toString(), 1, table);
        setearBodyStatic("Distancia entre parcelas", 1, table);
        setearBodyData(tab1.getDistanciaParcela()==null?"":tab1.getDistanciaParcela().toString(), 1, table);
        setearBodyStatic("Nº Parcelas",1, table);
        setearBodyData(tab1.getNroParcela()==null?"":tab1.getNroParcela().toString(), 1, table);
        setearBodyStatic("Area total inventariada (ha)", 1, table);
        setearBodyData(tab1.getTotalAreaInventariada()==null?"":tab1.getTotalAreaInventariada().toString(),1, table);
        setearBodyStatic("Método de muestreo:",1, table);
        setearRowsColspanData(tab1.getMetodoMuestreo()==null?"":tab1.getMetodoMuestreo(), 1, 3, table);
        
        document.add(table);


        Paragraph preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("        5.1.2  Regeneración de fustales ", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(4);
        table.setWidthPercentage(95);

        setearBodyStatic("Rango diamétrico (cm):", 1, table);
        setearBodyData(tab2.getRangoDiametroCM()==null?"":tab2.getRangoDiametroCM().toString(), 1, table);
        setearBodyStatic("Intensidad de muestreo (%):", 1, table);
        setearBodyData(tab2.getIntensidadMuestreoPorcentaje()==null?"":tab2.getIntensidadMuestreoPorcentaje().toString(),1,table);
        setearBodyStatic("Tamaño de parcela (m):", 1, table);
        setearBodyData(tab2.getTamanioParcela()==null?"":tab2.getTamanioParcela().toString(),1, table);
        setearBodyStatic("Área muestreada (ha):", 1, table);
        setearBodyData(tab2.getAreaMuestreada()==null?"":tab2.getAreaMuestreada().toString(),1, table);
        
        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("     5.2  Resultados para el potencial maderable", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);

        float[] medidaCeldas = {1.5f, 0.60f, 0.90f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}

        table.setWidthPercentage(95);

        setearBodyDataSborder("a)	Lista de especies inventariadas",1, 3, table);
        setearBodyDataSborder("b)	Error de muestreo (%) sobre el volumen:  ",1, 1, table);
        setearBodyData(prodTab3.getErrorMuestreo()==null?"": prodTab3.getErrorMuestreo().toString(), 1, table);
        setearBodyDataSborder(" ",1, 1, table);
        setearBodyDataSborder("c)	Promedios del número de árboles, (N), el área basal (AB) y el volumen comercial (Vc) por especie y clases diamétricas, por hectárea y total, para cada tipo de bosque diferenciado ",1, 3, table);
        setearBodyDataSborder("d)	Resumen de valores promedio, por hectárea y total, para las variables principales, por tipo de bosque:",1, 3, table);
        document.add(table);

        table = new PdfPTable(5);
        table.setWidthPercentage(90);

        setearBodyStatic("Tipo de Bosque", 1, table);
        setearBodyStatic("Especie", 1, table);
        setearBodyStatic("Variable", 1, table);
        setearBodyStatic("Total en el tipo de bosque", 1, table);
        setearBodyStatic("Total por Ha", 1, table);

        Integer totalN=0;
        Float totalNHa = (float) 0.00, totalAb = (float) 0.00, totalAbHa = (float) 0.00, totalVp = (float) 0.00, totalVpHa = (float) 0.00;

        for (ResultadosEspeciesMuestreoDto e : listaProdFor) {
            Integer rows = e.getListTablaEspeciesMuestreo().size() * 3;
            setearRowsColspanData(e.getNombreBosque()==null?"":e.getNombreBosque(),rows, 1, table); 
            for (TablaEspeciesMuestreo det : e.getListTablaEspeciesMuestreo()) {
                setearRowsColspanData(det.getEspecies()==null?"":det.getEspecies(),3,1,table);

                setearBodyData("N", 1, table);
                setearBodyData(det.getNumeroArbolesTotalBosque()==null?"":det.getNumeroArbolesTotalBosque(), 1, table);
                totalN += Integer.parseInt(det.getNumeroArbolesTotalBosque()==null||det.getNumeroArbolesTotalBosque().equals("null")?"0":det.getNumeroArbolesTotalBosque());
                setearBodyData(det.getNumeroArbolesTotalHa()==null ?"":det.getNumeroArbolesTotalHa(), 1, table);
                totalNHa += Float.parseFloat(det.getNumeroArbolesTotalHa()==null||det.getNumeroArbolesTotalHa().equals("null")?"0":det.getNumeroArbolesTotalHa());

                setearBodyData("AB m2", 1, table);
                setearBodyData(det.getAreaBasalTotalBosque()==null?"":det.getAreaBasalTotalBosque(), 1, table);
                totalAb += Float.parseFloat(det.getAreaBasalTotalBosque()==null||det.getAreaBasalTotalBosque().equals("null")?"0":det.getAreaBasalTotalBosque());
                setearBodyData(det.getAreaBasalTotalHa()==null?"":det.getAreaBasalTotalHa(), 1, table);
                totalAbHa += Float.parseFloat(det.getAreaBasalTotalHa()==null||det.getAreaBasalTotalHa().equals("null")?"0":det.getAreaBasalTotalHa());

                setearBodyData("Vp m3", 1, table);
                setearBodyData(det.getVolumenTotalBosque()==null?"":det.getVolumenTotalBosque(), 1, table);
                totalVp += Float.parseFloat(det.getVolumenTotalBosque()==null||det.getVolumenTotalBosque().equals("null")?"0":det.getVolumenTotalBosque());
                setearBodyData(det.getVolumenTotalHa()==null?"":det.getVolumenTotalHa(), 1, table);
                totalVpHa += Float.parseFloat(det.getVolumenTotalHa()==null||det.getVolumenTotalHa().equals("null")?"0":det.getVolumenTotalHa());
            }
        }

        setearRowsColspanStatic(" ",3, 1, table);
        setearRowsColspanStatic("TOTAL",3,1, table);
        setearBodyStatic("N", 1, table);
        setearBodyStatic(totalN.toString(), 1, table);
        setearBodyStatic(totalNHa.toString(), 1, table);
        setearBodyStatic("AB m2", 1, table);
        setearBodyStatic(totalAb.toString(), 1, table);
        setearBodyStatic(totalAbHa.toString(), 1, table);
        setearBodyStatic("Vp m3", 1, table);
        setearBodyStatic(totalVp.toString(), 1, table);
        setearBodyStatic(totalVpHa.toString(), 1, table); 

        document.add(table);

        table = new PdfPTable(1);
        table.setWidthPercentage(90);
        setearBodyDataSborder("e)	Especies más abundantes (N 1/ha, y en porcentaje) en el potencial maderable:",1, 1, table);
        document.add(table);

        table = new PdfPTable(3);
        table.setWidthPercentage(90);

        float[] medidaCeldas2 = {2f, 0.50f, 0.50f};
        try {
            table.setWidths(medidaCeldas2);
        } catch (DocumentException e) {}

        setearBodyStatic("Especies", 1, table);
        setearBodyStatic("N", 1, table);
        setearBodyStatic("%", 1, table);
        Float total = (float) 0;
        String totalPoc = ".00";
        for (ResultadosEspeciesMuestreoDto e : censoForestalEspecies) {
            for (TablaEspeciesMuestreo det : e.getListTablaEspeciesMuestreo()) {
                total += Float.parseFloat(det.getNumeroArbolesTotalHa()==null ||det.getNumeroArbolesTotalHa()=="" ||det.getNumeroArbolesTotalHa().equals("null")?"0":det.getNumeroArbolesTotalHa());
            }
        }
        for (ResultadosEspeciesMuestreoDto e : censoForestalEspecies) {
            for (TablaEspeciesMuestreo det : e.getListTablaEspeciesMuestreo()) {
                setearBodyData(det.getEspecies()==null?"":det.getEspecies() ,1, table);
                DecimalFormat format = new DecimalFormat("#.00");
                setearBodyData(det.getNumeroArbolesTotalHa()==null?"":format.format(Float.parseFloat(det.getNumeroArbolesTotalHa()=="" ||det.getNumeroArbolesTotalHa().equals("null")?"0":det.getNumeroArbolesTotalHa())),1, table);
                Float result = (float) 0;
                if(total>0) {
                    result = Float.parseFloat(det.getNumeroArbolesTotalHa()==null||det.getNumeroArbolesTotalHa()=="" ||det.getNumeroArbolesTotalHa().equals("null")?"0":det.getNumeroArbolesTotalHa()) / total*100;
                    totalPoc="100.00 %"; }
                setearBodyData(format.format(result), 1, table); 
            }
        }
        setearRowsColspanStatic("TOTAL", 1, 1, table);
        setearBodyStatic(total.toString(),1, table);
        setearBodyStatic(totalPoc, 1, table);

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("     5.3  Resultados para los fustales", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(1);
        table.setWidthPercentage(95);
        setearBodyDataSborder("a)	Lista de especies inventariadas (Anexo 2).", 1, 1, table);
        setearBodyDataSborder("b)	Promedios de número de árboles, (N), área basal (AB) y volumen por clase diamétrica y especie para cada tipo de bosque diferenciado (Anexo 3).", 1, 1, table);
        setearBodyDataSborder("c)	Resumen de valores promedio, por hectárea y total, para las variables principales, por tipo de bosque:", 1, 1, table);
        document.add(table);

        table = new PdfPTable(7);
        table.setWidthPercentage(90);

        setearRowsColspanStatic("Tipo de Bosque",2, 1, table);
        setearRowsColspanStatic("Especie",2, 1, table);
        setearRowsColspanStatic("Variable",2, 1, table);
        setearRowsColspanStatic("DAP(cm)", 1, 2, table);
        setearRowsColspanStatic("Total por Ha", 2, 1, table);
        setearRowsColspanStatic("Total en el Tipo de Bosque", 2, 1, table);
        setearBodyStatic("10 - 15", 1, table);
        setearBodyStatic("15 - 20", 1, table);

        Integer totalN1014 = 0;
        Float totalN1519 = (float) 0.00; totalNHa = (float) 0.00; totalAb = (float) 0.00; totalAbHa = (float) 0.00; totalVpHa = (float) 0.00;
        Float totalNToBosq = (float) 0.00; Float totalAreaHa = (float) 0.00; Float totalAreaTotalBosque = (float) 0.00;

        for (ResultadosEspeciesMuestreoDto e : lista53) {               // FALTA TAB5 (datos)
            Integer rows = e.getListTablaEspeciesMuestreo().size() * 3;
            setearRowsColspanData(e.getNombreBosque()==null?"":e.getNombreBosque(),rows, 1, table); 
            for (TablaEspeciesMuestreo det : e.getListTablaEspeciesMuestreo()) {
                setearRowsColspanData(det.getEspecies()==null?"":det.getEspecies(),3,1,table);

                setearBodyData("N", 1, table);
                setearBodyData(det.getNumeroArbolesDap10a14()==null?"":det.getNumeroArbolesDap10a14(), 1, table);
                totalN1014 += Integer.parseInt(det.getNumeroArbolesTotalBosque()==null||det.getNumeroArbolesTotalBosque().equals("null")?"0":det.getNumeroArbolesTotalBosque());
                setearBodyData(det.getNumeroArbolesDap15a19()==null?"":det.getNumeroArbolesDap15a19(), 1, table);
                totalN1519 += Float.parseFloat(det.getNumeroArbolesDap15a19()==null||det.getNumeroArbolesDap15a19().equals("null")?"0":det.getNumeroArbolesDap15a19());
                setearBodyData(det.getNumeroArbolesTotalHa()==null?"":det.getNumeroArbolesTotalHa(), 1, table);
                totalNHa += Float.parseFloat(det.getNumeroArbolesTotalHa()==null||det.getNumeroArbolesTotalHa().equals("null") ?"0":det.getNumeroArbolesTotalHa());
                setearBodyData(det.getNumeroArbolesTotalBosque()==null?"":det.getNumeroArbolesTotalBosque(), 1, table);
                totalNToBosq += Float.parseFloat(det.getNumeroArbolesTotalBosque()==null||det.getNumeroArbolesTotalBosque().equals("null") ?"0":det.getNumeroArbolesTotalBosque());

                setearBodyData("AB m2", 1, table);
                setearBodyData(det.getAreaBasalDap10a14()==null?"":det.getAreaBasalDap10a14(), 1, table);
                totalAb += Float.parseFloat(det.getAreaBasalDap10a14()==null||det.getAreaBasalDap10a14().equals("null")?"0":det.getAreaBasalDap10a14());
                setearBodyData(det.getAreaBasalDap15a19()==null?"":det.getAreaBasalDap15a19(), 1, table);
                totalAbHa += Float.parseFloat(det.getAreaBasalDap15a19()==null||det.getAreaBasalDap15a19().equals("null")?"0":det.getAreaBasalDap15a19());
                setearBodyData(det.getAreaBasalTotalHa()==null?"":det.getAreaBasalTotalHa(), 1, table);
                totalAreaHa += Float.parseFloat(det.getAreaBasalTotalHa()==null||det.getAreaBasalTotalHa().equals("null")?"0":det.getAreaBasalTotalHa());
                setearBodyData(det.getAreaBasalTotalBosque()==null?"":det.getAreaBasalTotalBosque(), 1, table);
                totalAreaTotalBosque += Float.parseFloat(det.getAreaBasalTotalBosque()==null||det.getAreaBasalTotalBosque().equals("null")?"0":det.getAreaBasalTotalBosque());

            }
        }

        document.add(table);

        table = new PdfPTable(1);
        table.setWidthPercentage(95);
        setearBodyDataSborder("d)	Especies más abundantes (N/ha. y en porcentaje) por grupo comercial (opcional):", 1, 1, table);
        document.add(table);

        table = new PdfPTable(4);
        float[] medidaCeldas3 = {2f, 0.65f, 0.65f, 0.65f};
        try {
            table.setWidths(medidaCeldas3);
        } catch (DocumentException e) {}
        table.setWidthPercentage(90);

        setearBodyStatic("Especie", 1, table);
        setearBodyStatic("GC", 1, table);
        setearBodyStatic("N°", 1, table);
        setearBodyStatic("%", 1, table);

        List<PotencialProduccionForestalDto> data = new ArrayList<>();

            if (listaProduccionTab532.size()!=0 && listaProduccionTab532.get(0) != null) {
                data = listaProduccionTab532.get(0).getListPotencialProduccion();
            }

        for (PotencialProduccionForestalDto e : data) {
            setearBodyData(e.getEspecie()==null?"":e.getEspecie() , 1, table);
            setearBodyData(e.getGrupoComercial()==null?"":e.getGrupoComercial(), 1, table);
            setearBodyData(e.getTotalHa()==null?"":e.getTotalHa().toString(),1,table);
            setearBodyData(e.getPorcentaje()==null?"":e.getPorcentaje().toString(),1, table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface,1);
        preface.add(new Paragraph("     5.4  Potencial de producción de recursos forestales no maderables y fauna silvestre (opcional)", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(7);
        float[] medidaCeldas4 = {1.5f, 1.50f, 0.50f, 0.5f, 0.5f, 1.5f, 1f};
        try {
            table.setWidths(medidaCeldas4);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyStatic("Especie (flora o fauna)", 1, table);
        setearBodyStatic("Productos a aprovechar", 1, table);
        setearBodyStatic("Sector", 1, table);
        setearBodyStatic("Área (ha)", 1, table);
        setearBodyStatic("Nº total individuos", 1, table);
        setearBodyStatic("Individuos/ha", 1, table);
        setearBodyStatic("Volumen, peso o número aprovechable", 1, table);

        Float area = (float) 0.00;Float individuos = (float) 0.00;Float individuosHa = (float) 0.00;Float volumen = (float) 0.00;

        for (AprovechamientoRFNMDto e : listaPotencialProduccion) {
            setearBodyData(e.getEspecie()==null?"":e.getEspecie(),1, table);
            setearBodyData(e.getProductoAprovechar()==null?"":e.getProductoAprovechar(), 1, table);
            setearBodyData(e.getResultadosAprovechamientoRFNMDtos().get(0).getSector()==null?"":e.getResultadosAprovechamientoRFNMDtos().get(0).getSector() , 1, table);
            setearBodyData(e.getResultadosAprovechamientoRFNMDtos().get(0).getArea()==null?"":e.getResultadosAprovechamientoRFNMDtos().get(0).getArea(),1, table);
            area += Float.parseFloat(e.getResultadosAprovechamientoRFNMDtos().get(0).getArea()==null?"0":e.getResultadosAprovechamientoRFNMDtos().get(0).getArea());
            setearBodyData(e.getResultadosAprovechamientoRFNMDtos().get(0).getnTotalIndividuos()==null?"":e.getResultadosAprovechamientoRFNMDtos().get(0).getnTotalIndividuos(), 1, table);
            individuos += Float.parseFloat(e.getResultadosAprovechamientoRFNMDtos().get(0).getnTotalIndividuos()==null?"0":e.getResultadosAprovechamientoRFNMDtos().get(0).getnTotalIndividuos());
            setearBodyData(e.getResultadosAprovechamientoRFNMDtos().get(0).getIndividuosHa()==null?"":e.getResultadosAprovechamientoRFNMDtos().get(0).getIndividuosHa(), 1, table);
            individuosHa += Float.parseFloat(e.getResultadosAprovechamientoRFNMDtos().get(0).getIndividuosHa()==null?"0":e.getResultadosAprovechamientoRFNMDtos().get(0).getIndividuosHa());
            setearBodyData(e.getResultadosAprovechamientoRFNMDtos().get(0).getVolumen()==null?"":e.getResultadosAprovechamientoRFNMDtos().get(0).getVolumen(), 1, table);
            volumen += Float.parseFloat(e.getResultadosAprovechamientoRFNMDtos().get(0).getVolumen()==null?"0":e.getResultadosAprovechamientoRFNMDtos().get(0).getVolumen());
        }

        setearRowsColspanStatic("TOTAL",1,3,table);
        setearBodyStatic(area.toString(),1,table);
        setearBodyStatic(individuos.toString(), 1, table);
        setearBodyStatic(individuosHa.toString(), 1, table);
        setearBodyStatic(volumen.toString(), 1, table);

        document.add(table);
    }

    public void manejoForestalTab6(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("     6.1  Uso Potencial por Categoría de Ordenamiento", subTituloTabla));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     En este cuadro se precisará la información teniendo en cuenta las categorías de ordenamiento señaladas en el acápite 4.1", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableUsoPotencial = usoPotencial(obj);
        document.add(tableUsoPotencial);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.2  Sistema de manejo", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableSistemaManejo = sistemaManejo(obj);
        document.add(tableSistemaManejo);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.3  Ciclo de corta", subTituloTabla));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     Duración mínima: 20 años", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableCicloCorta = cicloCorta(obj);
        document.add(tableCicloCorta);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.4  Especies a manejar y diámetros mínimos de corta", subTituloTabla));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     Lista de especies a manejar, su uso y diámetros mínimos de corta (DMC)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableEspeciesManejo = especiesManejo(obj);
        document.add(tableEspeciesManejo);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.5  Especies de flora a proteger", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableEspeciesFlora = especiesFlora(obj);
        document.add(tableEspeciesFlora);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.6  Corta anual permisible (CAP)", subTituloTabla));
        preface.add(new Paragraph("        6.6.1  Volumen comercial promedio por hectárea (Vcp)", subTituloTabla));
        preface.add(new Paragraph("        Número de árboles y Vcp de especies comerciales incluidas en el cálculo de la VCAP para cada bloque", subTituloTabla));
        preface.add(new Paragraph("        quinquenal de la UMF, por hectárea, total y para los árboles por encima del diámetro mínimo de corta  (> DMC)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        corteVcp(document,obj);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("        6.6.2  Volumen de corta anual permisible (VCAP) para la UMF", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableCorteVcap = corteVcap(obj);
        document.add(tableCorteVcap);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.7  Proyección de cosecha", subTituloTabla));
        preface.add(new Paragraph("     En base a los principales parámetros de manejo (valor referencial de crecimiento del árbol, ciclo de corta, DMC, IC)", subTituloTabla));
        preface.add(new Paragraph("     y los resultados o referencias de la dinámica poblacional señalados en el ítem 6.6.7 de los lineamientos del", subTituloTabla));
        preface.add(new Paragraph("     PGMF, realizar la proyección de cosecha:", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableProyeccionCosecha = proyeccionCosecha(obj);
        document.add(tableProyeccionCosecha);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.8  Especificaciones sobre el sistema de aprovechamiento", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);
        PdfPTable tableEspecificacionesAprovechamiento = especificacionesAprovechamiento(obj);
        document.add(tableEspecificacionesAprovechamiento);

        //6.9
        //----DATA---
        List<ManejoBosqueDetalleEntity> ObjSubCodTipManA = new ArrayList<>();
        List<ManejoBosqueDetalleEntity> ObjSubCodTipManB = new ArrayList<>();
        List<ManejoBosqueDetalleEntity> objAreasDeg = new ArrayList<>();

        List<ManejoBosqueEntity> listaEspecImport = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(),"PGMF", "PGMFCFFMMFEPSTSA", null);
        if (listaEspecImport != null && !listaEspecImport.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaObjEspecImpor = listaEspecImport.get(0).getListManejoBosqueDetalle();
            ObjSubCodTipManA = listaObjEspecImpor.stream()
                    .filter(c -> c.getSubCodtipoManejoDet() != null && !c.getSubCodtipoManejoDet().equals("B"))
                    .collect(Collectors.toList());

            ObjSubCodTipManB = listaObjEspecImpor.stream()
                    .filter(c -> c.getSubCodtipoManejoDet() != null && c.getSubCodtipoManejoDet().equals("B"))
                    .collect(Collectors.toList());
        }

        List<ManejoBosqueEntity> manejoAreasDegradadas = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), "PGMF", "PGMFCFFMMFEPSMEAD", null);
        if (manejoAreasDegradadas != null && !manejoAreasDegradadas.isEmpty()) {
            objAreasDeg = manejoAreasDegradadas.get(0).getListManejoBosqueDetalle();
        }

        //-----
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     6.9  Especificaciones sobre Prácticas Silviculturales", subTituloTabla));
        preface.add(new Paragraph("         6.9.2  Tratamientos Silviculturales a Aplicar", subTituloTabla));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Especies más importantes a Favorecer:", contenido));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable table = new PdfPTable(3);    
        float[] medidaCeldas = {0.40f, 1.30f, 1.30f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}  
        table.setWidthPercentage(95);

        setearBodyStatic("N°", 1, table);
        setearBodyStatic("Nombre Científico", 1, table);
        setearBodyStatic("Nombre Común", 1, table);

        Integer index = 1;
        for (ManejoBosqueDetalleEntity e : ObjSubCodTipManA) {
            setearBodyData(index.toString(),1,table); 
            setearBodyData(e.getEspecie()==null?"":e.getEspecie(),1,table);
            setearBodyData(e.getDescripcionOtro()==null?"":e.getDescripcionOtro(),1,table); 
            index++;
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Las Especies Declaradas Deberán estar Identificadas (a través de muestras botánicas) por el Regente.", contenido));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(2);    
        float[] medidaCeldas2 = {1.80f, 0.20f};
        try {
            table.setWidths(medidaCeldas2);
        } catch (DocumentException e) {}  
        table.setWidthPercentage(95);

        setearBodyStatic(" Tratamientos silviculturales posibles de aplicar ", 2, table);

        for (ManejoBosqueDetalleEntity e : ObjSubCodTipManB) {
            setearBodyData(e.getTratamientoSilvicultural()==null?"":e.getTratamientoSilvicultural(),1,table); 
            if(e.getAccion()!=null) setearBodyData(e.getAccion()==true?"X":"",1,table);
            else setearBodyData("",1,table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("         6.9.3  Manejo o Enriquecimiento en Áreas degradadas", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(2);    
        float[] medidaCeldas3 = {1.40f, 0.60f};
        try {
            table.setWidths(medidaCeldas3);
        } catch (DocumentException e) {}  
        table.setWidthPercentage(95);

        setearBodyStatic(" El objetivo principal es la recuperacion del bosque a través de las siguientes acciones:",2,table);

        for (ManejoBosqueDetalleEntity e : objAreasDeg) {
            setearBodyData(e.getTratamientoSilvicultural()==null?"":e.getTratamientoSilvicultural(),1,table); 
            setearBodyData(e.getDescripcionOtro()==null?"":e.getDescripcionOtro(),1,table); 
        }

        document.add(table);

    }
 
    public  void identificacionImpacAmbientalesTab7(Document document, DescargarPGMFDto obj) throws Exception { 
        //------DATA-------//
        EvaluacionAmbientalActividadEntity ev = new EvaluacionAmbientalActividadEntity();
        ev.setIdPlanManejo(obj.getIdPlanManejo());
        ev.setTipoActividad("FACTOR");

        List<EvaluacionAmbientalActividadEntity> listaFactor = evaluacionAmbientalRepository
                .ListarEvaluacionActividadFiltro(ev);

        EvaluacionAmbientalAprovechamientoEntity eval = new EvaluacionAmbientalAprovechamientoEntity();
        eval.setIdPlanManejo(obj.getIdPlanManejo());
        eval.setTipoAprovechamiento("");

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividades = evaluacionAmbientalRepository
                .ListarEvaluacionAprovechamientoFiltro(eval);

        List<EvaluacionAmbientalAprovechamientoEntity> listaPreAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("PREAPR"))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("APROVE"))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaPostAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("POSTAP"))
                .collect(Collectors.toList());

        EvaluacionAmbientalDto evalDto = new EvaluacionAmbientalDto();
        evalDto.setIdPlanManejo(obj.getIdPlanManejo());
        List<EvaluacionAmbientalDto> listaRespuesta = evaluacionAmbientalRepository.ListarEvaluacionAmbiental(evalDto);

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab2 = listaActividades.stream()
                .filter(c -> c.getNombreAprovechamiento() != null && !c.getNombreAprovechamiento().equals(""))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab3 = listaActividades.stream()
                .filter(c -> c.getImpacto() != null && !c.getImpacto().equals(""))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab4 = listaActividades.stream()
                .filter(c -> c.getContingencia() != null && !c.getContingencia().equals(""))
                .collect(Collectors.toList());

        //----------------//

        Rectangle two = new Rectangle(842.0F,595.0F);
        document.setPageSize(two);
        document.setMargins(40, 40, 40, 40);
        document.newPage();
        document.add(new Paragraph("\r\n"));
            
        Integer numPre = listaPreAprovechamiento.size()==0?1:listaPreAprovechamiento.size();
        Integer numAprov = listaAprovechamiento.size()==0?1:listaAprovechamiento.size();
        Integer numPost = listaPostAprovechamiento.size()==0?1:listaPostAprovechamiento.size(); 
        Integer dina = numPre + numAprov + numPost ;

        PdfPTable table = new PdfPTable(dina + 3);
        table.setWidthPercentage(95);

        /**listaFactor, listaPreAprovechamiento, listaAprovechamiento,
                listaPostAprovechamiento, listaRespuesta, */
        setearRowsColspanStatic("IDENTIFICACION DE IMPACTOS AMBIENTALES",1, (dina +3), table);
        setearRowsColspanStatic("Factores Ambientales", 3, 1, table);
        setearRowsColspanStatic("Caracterización de Impactos", 3, 1, table);
        setearRowsColspanStatic("Actividades",1,dina,table);
        setearRowsColspanStatic("Medidas de Mitigación", 3, 1, table);
        setearRowsColspanStatic("Pre Aprovechamiento",1, numPre, table);
        setearRowsColspanStatic("Aprovechamiento",1, numAprov, table);
        setearRowsColspanStatic("Post Aprovechamiento",1, numPost, table);

        if(listaPreAprovechamiento.size()==0) {
            setearBodyStatic("",1,table);
        }else{
            for (EvaluacionAmbientalAprovechamientoEntity e : listaPreAprovechamiento) {
                setearBodyStatic(e.getNombreAprovechamiento()==null?"":e.getNombreAprovechamiento(),1, table);
            }
        }
        
        if(listaAprovechamiento.size()==0) {
            setearBodyStatic("", 1, table);
        }else{
            for (EvaluacionAmbientalAprovechamientoEntity e : listaAprovechamiento) {
                setearBodyStatic(e.getNombreAprovechamiento()==null?"":e.getNombreAprovechamiento(),1, table);
            }
        }
        

        if(listaPostAprovechamiento.size()==0){
            setearBodyStatic("", 1, table);
        }else{
            for (EvaluacionAmbientalAprovechamientoEntity e : listaPostAprovechamiento) {
                setearBodyStatic(e.getNombreAprovechamiento()==null?"":e.getNombreAprovechamiento(),1, table);
            }
        }

        String info[]={"ALTO","MEDIO","BAJO",};

        if(listaFactor!=null && listaFactor.size()!=0){
            Integer cont = 0; Integer i = 0;

            for (EvaluacionAmbientalActividadEntity e : listaFactor) {

                    if(cont%3==0) setearRowsColspanData(e.getNombreActividad(),3,1,table);
                    //setearRowsColspanStatic(e.getMedidasControl()==null?"":e.getMedidasControl(),1,1,table);
                    setearRowsColspanStatic(info[i], 1, 1, table); i++; if(i==3)i=0;

                    if(listaPreAprovechamiento.size()==0){
                        setearBodyData("", 1, table);
                    }else{
                        for (EvaluacionAmbientalAprovechamientoEntity det : listaPreAprovechamiento) {
                            setearRowsColspanData(getRespuestaImpactAmb(e, listaRespuesta, det.getIdEvalAprovechamiento()),1,1,table);                       
                        }
                    }

                    if(listaAprovechamiento.size()==0){
                        setearBodyData("", 1, table);
                    }else{
                        for (EvaluacionAmbientalAprovechamientoEntity det : listaAprovechamiento) {
                            setearRowsColspanData(getRespuestaImpactAmb(e, listaRespuesta, det.getIdEvalAprovechamiento()),1,1,table);                       
                        }
                    }
                    
                    if(listaPostAprovechamiento.size()==0){
                        setearBodyData("", 1, table);
                    }else{
                        for (EvaluacionAmbientalAprovechamientoEntity det : listaPostAprovechamiento) {
                            setearRowsColspanData(getRespuestaImpactAmb(e, listaRespuesta, det.getIdEvalAprovechamiento()),1,1,table);               
                        } 
                    }
                                                               

                    setearBodyData(e.getMedidasMonitoreo()==null?"":e.getMedidasMonitoreo(),1, table);
                    cont++;
                }    
        }

        document.add(table);

        Rectangle vert = new Rectangle(595.0F,842.0F);
        document.setPageSize(vert);
        document.setMargins(60, 60, 40, 40);
        document.newPage();
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("         7.2.1	Plan de acción preventivo-corrector:", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);
        float[] medidaCeldas = {0.75f, 0.75f, 1.50f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyStatic("Actividades", 1, table);
        setearBodyStatic("Descripción del Impacto",1,table);
        setearBodyStatic("Medidas de Control Ambiental",1,table);

        for (EvaluacionAmbientalAprovechamientoEntity e : listaActividadesTab2) {
            setearBodyData(e.getNombreAprovechamiento()==null?"":e.getNombreAprovechamiento(), 1, table);
            setearBodyData(e.getImpacto()==null?"":e.getImpacto(), 1, table);
            setearBodyData(e.getMedidasControl()==null?"":e.getMedidasControl(), 1, table);
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("         7.2.2	Plan de Vigilancia y Seguimiento:", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(5);
        
        table.setWidthPercentage(95);

        setearBodyStatic("Descripción del Impacto",1,table);
        setearBodyStatic("Medidas de Control Ambiental",1,table);
        setearBodyStatic("Medidas de Monitoreo", 1, table);
        setearBodyStatic("Frecuencia", 1, table);
        setearBodyStatic("Responsable", 1, table);

        for (EvaluacionAmbientalAprovechamientoEntity e : listaActividadesTab3) {
            setearBodyData(e.getImpacto()==null?"":e.getImpacto(), 1, table);
            setearBodyData(e.getMedidasControl()==null?"":e.getMedidasControl(), 1, table);
            setearBodyData(e.getMedidasMonitoreo()==null?"":e.getMedidasMonitoreo(), 1, table);
            setearBodyData(e.getFrecuencia()==null?"":e.getFrecuencia(), 1, table);
            setearBodyData(e.getResponsable()==null?"":e.getResponsable(), 1, table);
        }
        
        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("         7.2.2	Plan de contingencia ambiental (de ser el caso):", subTituloTabla)); 
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);
        float[] medidaCeldas2 = {1.50f, 0.75f, 0.75f};
        try {
            table.setWidths(medidaCeldas2);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Plan de contingencia ambiental", 1, 3, table);
        setearBodyStatic("Contingencia (Ejm.)", 1, table);
        setearBodyStatic("Acciones a realizar", 1, table);
        setearBodyStatic("Responsable", 1, table);

        for (EvaluacionAmbientalAprovechamientoEntity e : listaActividadesTab4) {
            setearBodyData(e.getContingencia()==null?"":e.getContingencia(), 1, table);
            setearBodyData(e.getAcciones()==null?"":e.getAcciones(), 1, table);
            setearBodyData(e.getResponsable()==null?"":e.getResponsable(), 1, table);
        }
        
        document.add(table);



    }
 
    public  void monitoreoTab8(Document document, DescargarPGMFDto obj) throws Exception { 

        String descripcionMonitoreo = "";
        List<MonitoreoEntity> listaMonitoreoTab2 = new ArrayList<>();

        List<MonitoreoEntity> listaMonitoreo = monitoreoRepository.listarMonitoreoPOCC(obj.getIdPlanManejo(), "PGMF");
        if (listaMonitoreo != null && !listaMonitoreo.isEmpty()) {

            List<MonitoreoEntity> listaMonitoreoTab1 = listaMonitoreo.stream()
                    .filter(c -> c.getMonitoreo().equals("TXTCabecera"))
                    .collect(Collectors.toList());

            List<MonitoreoEntity> respListaMonitoreoTab2 = listaMonitoreo.stream()
                    .filter(c -> !c.getMonitoreo().equals("TXTCabecera"))
                    .collect(Collectors.toList());

            if (listaMonitoreoTab1 != null && !listaMonitoreoTab1.isEmpty()) {

                descripcionMonitoreo = listaMonitoreoTab1.get(0).getDescripcion();
            } 

            if (respListaMonitoreoTab2 != null && !respListaMonitoreoTab2.isEmpty()) {
                listaMonitoreoTab2 = respListaMonitoreoTab2;
            }
        }


        PdfPTable table = new PdfPTable(1);      
        table.setWidthPercentage(95);
        setearBodyData(descripcionMonitoreo, 1, table);
 
        document.add(table);

        Paragraph preface = new Paragraph();
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     8.2  Descripciones de las acciones de monitoreo", subTituloTabla));
        preface.add(new Paragraph("     En este ítem actividades de aprovechamiento, silviculturales, impactos de flora y fauna silvestre, administrativo, entre otros.", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);



        
        table = new PdfPTable(3);
        setearBodyStatic("Monitoreo", 1, table);
        setearBodyStatic("Operaciones", 1, table);
        setearBodyStatic("Descripción", 1, table);

        for (MonitoreoEntity e : listaMonitoreoTab2) {
             
            setearBodyStatic(e.getMonitoreo(), 1, table);

            String dato = "";
            for (MonitoreoDetalleEntity i : e.getLstDetalle()) {
                dato = dato + i.getOperacion() + "\n";
            }

            setearBodyData(dato, 1, table);
            setearBodyData(e.getDescripcion(), 1, table);
        }
        
        document.add(table);
    }    

    public  void participacionCiudadanaTab9(Document document, DescargarPGMFDto obj) throws Exception { 

        Integer idPlanManejo = obj.getIdPlanManejo();
        ParticipacionComunalParamEntity pcp = new ParticipacionComunalParamEntity();
        pcp.setIdPlanManejo(idPlanManejo);
        pcp.setCodTipoPartComunal("PGMFFO");
        pcp.setIdPartComunal(null);

        List<ParticipacionComunalEntity> listaFormulacion = new ArrayList<>();
        List<ParticipacionComunalEntity> listaFormulacion2 = new ArrayList<>();
        List<ParticipacionComunalEntity> listaFormulacion3 = new ArrayList<>();
        List<ParticipacionComunalEntity> listaFormulacion4 = new ArrayList<>();

        List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();

        ResultEntity formulacion = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp);
        if (formulacion != null && formulacion.getIsSuccess()) {
            listaFormulacion = (ArrayList<ParticipacionComunalEntity>) formulacion
                    .getData();
        }

        ParticipacionComunalParamEntity pcp2 = new ParticipacionComunalParamEntity();
        pcp2.setIdPlanManejo(idPlanManejo);
        pcp2.setCodTipoPartComunal("PGMFIM");
        pcp2.setIdPartComunal(null);
        ResultEntity implementacion = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp2);
        if (implementacion != null && implementacion.getIsSuccess()) {
            listaFormulacion2 = (ArrayList<ParticipacionComunalEntity>) implementacion
                    .getData();
        }

        ParticipacionComunalParamEntity pcp3 = new ParticipacionComunalParamEntity();
        pcp3.setIdPlanManejo(idPlanManejo);
        pcp3.setCodTipoPartComunal("PGMFCO");
        pcp3.setIdPartComunal(null);
        ResultEntity comites = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp3);
        if (comites != null && comites.getIsSuccess()) {
            listaFormulacion3 = (ArrayList<ParticipacionComunalEntity>) comites
                    .getData();
        }

        ParticipacionComunalParamEntity pcp4 = new ParticipacionComunalParamEntity();
        pcp4.setIdPlanManejo(idPlanManejo);
        pcp4.setCodTipoPartComunal("PGMFRE");
        pcp4.setIdPartComunal(null);
        ResultEntity reconocimiento = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp4);
        if (reconocimiento != null && reconocimiento.getIsSuccess()) {
            listaFormulacion4 = (ArrayList<ParticipacionComunalEntity>) reconocimiento
                    .getData();
        }

        lstListaDetalle = new ArrayList<>();
        if (listaFormulacion!=null && !listaFormulacion.isEmpty() && listaFormulacion.get(0)!=null && 
            listaFormulacion.get(0).getLstDetalle()!=null && !listaFormulacion.get(0).getLstDetalle().isEmpty()) {
            lstListaDetalle = listaFormulacion.get(0).getLstDetalle();
        }

        PdfPTable table = new PdfPTable(1);      
        table.setWidthPercentage(95);
        

        setearBodyStatic("Actividades", 1, table);
        
        for (ParticipacionComunalDetEntity e : lstListaDetalle) {
            setearBodyData(e.getActividad()==null ? "" : e.getActividad(), 1, table);            
        }
        document.add(table);


        lstListaDetalle = new ArrayList<>();
        if (listaFormulacion2!=null && !listaFormulacion2.isEmpty() && listaFormulacion2.get(0)!=null && 
            listaFormulacion2.get(0).getLstDetalle()!=null && !listaFormulacion2.get(0).getLstDetalle().isEmpty()) {
            lstListaDetalle = listaFormulacion2.get(0).getLstDetalle();
        }

        Paragraph preface = new Paragraph();
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     9.2  En la implementación del Plan de Manejo Forestal", subTituloTabla));            
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(1);      
        table.setWidthPercentage(95);
        setearBodyStatic("Actividades", 1, table);

        for (ParticipacionComunalDetEntity e : lstListaDetalle) {
            setearBodyData(e.getActividad()==null ? "" : e.getActividad(), 1, table);            
        }
        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     9.3  En los Comités de Gestión de Bosques", subTituloTabla));            
        addEmptyLine(preface, 1);
        document.add(preface);


        lstListaDetalle = new ArrayList<>();
        if (listaFormulacion3!=null && !listaFormulacion3.isEmpty() && listaFormulacion3.get(0)!=null && 
            listaFormulacion3.get(0).getLstDetalle()!=null && !listaFormulacion3.get(0).getLstDetalle().isEmpty()) {
            lstListaDetalle = listaFormulacion3.get(0).getLstDetalle();
        }

        table = new PdfPTable(1);      
        table.setWidthPercentage(95);
        setearBodyStatic("Actividades", 1, table);

        for (ParticipacionComunalDetEntity e : lstListaDetalle) {
            setearBodyData(e.getActividad()==null ? "" : e.getActividad(), 1, table);            
        }
        document.add(table);


        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     9.4  Plan de Reconocimiento Comunitario", subTituloTabla));            
        addEmptyLine(preface, 1);
        document.add(preface);


        lstListaDetalle = new ArrayList<>();
        if (listaFormulacion4!=null && !listaFormulacion4.isEmpty() && listaFormulacion4.get(0)!=null && 
            listaFormulacion4.get(0).getLstDetalle()!=null && !listaFormulacion4.get(0).getLstDetalle().isEmpty()) {
            lstListaDetalle = listaFormulacion4.get(0).getLstDetalle();
        }

        table = new PdfPTable(4);      
        table.setWidthPercentage(95);
        setearBodyStatic("Mecanismos de Participación", 1, table);
        setearBodyStatic("Metodología", 1, table);
        setearBodyStatic("Lugar", 1, table);        
        setearBodyStatic("Fecha", 1, table);        
 
        for (ParticipacionComunalDetEntity e : lstListaDetalle) {
            setearBodyData(e.getActividad()==null ? "" : e.getActividad(), 1, table);
            setearBodyData(e.getMetodologia()==null ? "" : e.getMetodologia(), 1, table);  
            setearBodyData(e.getLugar()==null ? "" : e.getLugar(), 1, table);  
            setearBodyData(FechaUtil.obtenerFechaString(e.getFecha(),"dd/MM/yyyy"), 1, table);   
        }
        document.add(table);


    }    


    public  void organizacionTab11(Document document, DescargarPGMFDto obj) throws Exception { 

        Integer idPlanManejo = obj.getIdPlanManejo();

        OrganizacionManejoEntity organizacionManejoEntity = new OrganizacionManejoEntity();
        organizacionManejoEntity.setIdPGMF(idPlanManejo);
        ResultClassEntity<List<OrganizacionManejoEntity>> result = pgmfaBreviadoRepository
                .OrganizacionManejo(organizacionManejoEntity);

        List<OrganizacionManejoEntity> listaFuncionesTab1Cab = new ArrayList<>();
        List<OrganizacionManejoEntity> listaFuncionesTab2 = new ArrayList<>();
        List<OrganizacionManejoEntity> listaFuncionesTab3 = new ArrayList<>();

        if (result != null && result.getSuccess()) {

            List<OrganizacionManejoEntity> listaFunciones = result.getData();

            List<OrganizacionManejoEntity> listaFuncionesTab1 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("FUNRESP"))
                    .collect(Collectors.toList());

            listaFuncionesTab1Cab = listaFuncionesTab1.stream()
                    .filter(c -> c.getEsCabecera())
                    .collect(Collectors.toList());

            for (OrganizacionManejoEntity e : listaFuncionesTab1Cab) {

                List<OrganizacionManejoEntity> listaDetalle = listaFuncionesTab1.stream()
                        .filter(c -> !c.getEsCabecera() && c.getSubTipo().equals(e.getSubTipo()))
                        .collect(Collectors.toList());

                e.setListaDetalle(listaDetalle);
            }

            listaFuncionesTab2 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("REQPERS"))
                    .collect(Collectors.toList());

            listaFuncionesTab3 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("REQMAQU"))
                    .collect(Collectors.toList());
        }

        PdfPTable table = new PdfPTable(4);      
        table.setWidthPercentage(95);
        setearBodyStatic("Nivel", 1, table);
        setearBodyStatic("Función", 1, table);
        setearBodyStatic("Responsabilidades", 1, table);
        setearBodyStatic("Número", 1, table);

        String funcion = "";
        String responsabilidades = "";
        String numero = "";

        for (OrganizacionManejoEntity e : listaFuncionesTab1Cab) {
            
            setearBodyData(e.getNombreSubTipo()==null ? "" : e.getNombreSubTipo(), 1, table);

            funcion = "";
            responsabilidades = "";
            numero = "";

            for (OrganizacionManejoEntity j : e.getListaDetalle()) {
                
                funcion += "\n" + (j.getFuncion()==null || j.getFuncion().equals("") ? "" : "- " + j.getFuncion());
                responsabilidades +=  "\n" + (j.getDescripcion()==null || j.getDescripcion().equals("") ? "" : "- " + j.getDescripcion());
                numero +=  "\n" + (j.getNumero()==null || j.getNumero().equals(0) ? "" : "- " + j.getNumero());
            }
            setearBodyData(funcion, 1, table);
            setearBodyData(responsabilidades, 1, table);
            setearBodyData(numero, 1, table);
        }
        document.add(table);


        Paragraph preface = new Paragraph();
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     11.2  Requerimientos de personal por actividad de manejo ", subTituloTabla));            
        addEmptyLine(preface, 1);
        document.add(preface);

        
        table = new PdfPTable(3);      
        table.setWidthPercentage(95);
        setearRowsColspanStatic("Tipo de Actividad", 2, 1, table);
        setearRowsColspanStatic("Personal", 1, 2, table);
        setearRowsColspanStatic("Función", 1, 1, table);
        setearRowsColspanStatic("Número", 1, 1, table);

        for (OrganizacionManejoEntity e : listaFuncionesTab2) {
            setearBodyData(e.getTipoActividad()==null ? "" : e.getTipoActividad(), 1, table);
            setearBodyData(e.getFuncion()==null ? "" : e.getFuncion(), 1, table);
            setearBodyData(e.getNumero()==null || e.getNumero().equals(0) ? "" : e.getTipoActividad().toString(), 1, table);
        }
        document.add(table);



        preface = new Paragraph();
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     11.3  Requerimientos de maquinaria y equipos", subTituloTabla));            
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(3);      
        table.setWidthPercentage(95);
        setearRowsColspanStatic("Tipo de Actividad", 1, 1, table);
        setearRowsColspanStatic("Maquinaria /Equipo", 1, 1, table);
        setearRowsColspanStatic("Número", 1, 1, table);

        for (OrganizacionManejoEntity e : listaFuncionesTab3) {
            setearBodyData(e.getTipoActividad()==null ? "" : e.getTipoActividad(), 1, table);
            setearBodyData(e.getMaquinaEquipo()==null ? "" : e.getMaquinaEquipo(), 1, table);
            setearBodyData(e.getNumero()==null || e.getNumero().equals(0) ? "" : e.getNumero().toString(), 1, table);
        }
        document.add(table);
    }

    public  void capacitacionTab10(Document document, DescargarPGMFDto obj) throws Exception { 

        Integer idPlanManejo = obj.getIdPlanManejo();

        CapacitacionDto capacitacionDto = new CapacitacionDto();
        capacitacionDto.setIdPlanManejo(idPlanManejo);
        capacitacionDto.setCodTipoCapacitacion("PGMFOBJ");

        List<CapacitacionDto> listaC = new ArrayList<>();

        List<CapacitacionDto> listaCapacitacion = capacitacionRepository.ListarCapacitacion(capacitacionDto);

        CapacitacionDto actividadPlanDto = new CapacitacionDto();
        actividadPlanDto.setIdPlanManejo(idPlanManejo);
        actividadPlanDto.setCodTipoCapacitacion("PGMF");

        List<CapacitacionDto> listaActividadPlan = capacitacionRepository.ListarCapacitacion(actividadPlanDto);
 
        if (listaCapacitacion != null && !listaCapacitacion.isEmpty()) {
            listaC = listaCapacitacion;
        }

        PdfPTable table = new PdfPTable(1);
        String dato = "";
        for (CapacitacionDto e : listaC) {
            dato = dato + "- "+e.getTema() + "\n\n";
        }
        setearBodyData(dato, 1, table);
        document.add(table);

        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     10.2  Actividades Previstas en el Plan de UMF", subTituloTabla));            
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(4);
        setearBodyStatic("Temas o Actividades", 1, table);
        setearBodyStatic("Personal por Capacitar", 1, table);
        setearBodyStatic("Modalidad de Capacitación", 1, table);
        setearBodyStatic("Lugar de Capacitación", 1, table);

        String temaActividades = "";
        for (CapacitacionDto e : listaActividadPlan) {
            temaActividades = e.getTema()==null ? "" : e.getTema();

            if(e.getListCapacitacionDetalle()!=null &&  !e.getListCapacitacionDetalle().isEmpty()){

                for (CapacitacionDetalleEntity j : e.getListCapacitacionDetalle()) {
                    temaActividades +=  "\n" + "- " +j.getActividad();
                }
            }
            setearBodyData(temaActividades, 1, table);
            setearBodyData(e.getPersonaCapacitar()==null ? "" : e.getPersonaCapacitar(), 1, table);
            setearBodyData(e.getIdTipoModalidad()==null ? "" : e.getIdTipoModalidad(), 1, table);
            setearBodyData(e.getLugar()==null ? "" : e.getLugar(), 1, table);
        }

        document.add(table);
    }

    public void programaInversionesTab12(Document document, DescargarPGMFDto obj) throws Exception {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("     12.1  Flujo de Caja", subTituloTabla));
        preface.add(new Paragraph("        12.1.1  Ingreso ", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PlanManejoEntity param = new PlanManejoEntity();
        param.setIdPlanManejo(obj.getIdPlanManejo());
        param.setCodigoParametro("PGMF");
        ResultClassEntity resultRenta = rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(param);

        List<RentabilidadManejoForestalDto> listaFlujoCajaIngreso = new ArrayList<>();
        List<RentabilidadManejoForestalDto> listaFlujoCajaEgreso = new ArrayList<>();
        List<RentabilidadManejoForestalDto> listaNecesidadesFinanciamiento = new ArrayList<>();

        List<Integer> listaAnioIngreso = new ArrayList<>();
        List<Integer> listaAnioEgreso = new ArrayList<>();

        if (resultRenta != null && resultRenta.getSuccess()) {

            List<RentabilidadManejoForestalDto> listaProgramaInversiones = (ArrayList<RentabilidadManejoForestalDto>) resultRenta.getData();

            // FLUJO CAJA INGRESO
            listaFlujoCajaIngreso = listaProgramaInversiones.stream()
                    .filter(c -> c.getIdTipoRubro().equals(1))
                    .collect(Collectors.toList());

            listaAnioIngreso = new ArrayList<>();

            for (RentabilidadManejoForestalDto e : listaFlujoCajaIngreso) {
                for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                    listaAnioIngreso.add(el.getAnio());
                }
            }

            listaAnioIngreso = listaAnioIngreso.stream().distinct().collect(Collectors.toList());
            Collections.sort(listaAnioIngreso);

            RentabilidadManejoForestalDto rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
            rentabilidadManejoForestalDto.setRubro("TOTAL");
            RentabilidadManejoForestalDetalleEntity rentabilidadManejoForestalDetalleEntity = null;
            List<RentabilidadManejoForestalDetalleEntity> listRentabilidadManejoForestalDetalle = new ArrayList<>();
            for (Integer e : listaAnioIngreso) {
                rentabilidadManejoForestalDetalleEntity = new RentabilidadManejoForestalDetalleEntity();
                rentabilidadManejoForestalDetalleEntity.setAnio(e);
                rentabilidadManejoForestalDetalleEntity.setMonto(
                        sumarColumna(listaFlujoCajaIngreso, e));
                listRentabilidadManejoForestalDetalle.add(rentabilidadManejoForestalDetalleEntity);
            }

            rentabilidadManejoForestalDto.setListRentabilidadManejoForestalDetalle(listRentabilidadManejoForestalDetalle);
            listaFlujoCajaIngreso.add(rentabilidadManejoForestalDto);

            // FIN FLUJO CAJA INGRESO

            // FLUJO CAJA EGRESO
            listaFlujoCajaEgreso = listaProgramaInversiones.stream()
                    .filter(c -> c.getIdTipoRubro().equals(2))
                    .collect(Collectors.toList());

            listaAnioEgreso = new ArrayList<>();

            for (RentabilidadManejoForestalDto e : listaFlujoCajaEgreso) {
                for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                    listaAnioEgreso.add(el.getAnio());
                }
            }

            listaAnioEgreso = listaAnioEgreso.stream().distinct().collect(Collectors.toList());
            Collections.sort(listaAnioEgreso);

            rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
            rentabilidadManejoForestalDto.setRubro("TOTAL");
            rentabilidadManejoForestalDetalleEntity = null;
            listRentabilidadManejoForestalDetalle = new ArrayList<>();
            for (Integer e : listaAnioEgreso) {
                rentabilidadManejoForestalDetalleEntity = new RentabilidadManejoForestalDetalleEntity();
                rentabilidadManejoForestalDetalleEntity.setAnio(e);
                rentabilidadManejoForestalDetalleEntity.setMonto(sumarColumna(listaFlujoCajaEgreso, e));
                listRentabilidadManejoForestalDetalle.add(rentabilidadManejoForestalDetalleEntity);
            }

            rentabilidadManejoForestalDto.setListRentabilidadManejoForestalDetalle(listRentabilidadManejoForestalDetalle);
            listaFlujoCajaEgreso.add(rentabilidadManejoForestalDto);

            // FIN FLUJO CAJA EGRESO

            // NECESIDADES DE FINANCIAMIENTO
            if (listaProgramaInversiones != null && !listaProgramaInversiones.isEmpty()) {

                listaNecesidadesFinanciamiento = listaProgramaInversiones.stream()
                        .filter(c -> c.getIdTipoRubro().equals(3))
                        .collect(Collectors.toList());

            }
        }

        PdfPTable table = new PdfPTable(1);
        float[] medidaCeldas = {1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Rubro", 1, table);

        if (listaFlujoCajaIngreso != null && !listaFlujoCajaIngreso.isEmpty()) {
            Integer columnas = listaAnioIngreso.size();

            table = new PdfPTable(columnas + 1);
            medidaCeldas = new float[columnas + 1];
            medidaCeldas[0] = 1.40f;
            for (Integer i = 1; i<=columnas; i++) {
                medidaCeldas[i] = 0.50f;
            }
            try {
                table.setWidths(medidaCeldas);
            } catch (DocumentException e) {
            }
            table.setWidthPercentage(95);

            setearBodyStatic("Rubro", 1, table);
            for (Integer anio : listaAnioIngreso) {
                setearBodyStatic("Año " + anio.toString(), 1, table);
            }

            for (RentabilidadManejoForestalDto element : listaFlujoCajaIngreso) {
                setearBodyData(element.getRubro() == null ? "" : element.getRubro(), 1, table);
                BigDecimal monto = BigDecimal.ZERO;
                for (Integer anio : listaAnioIngreso) {
                    monto = BigDecimal.ZERO;
                    for (RentabilidadManejoForestalDetalleEntity e : element.getListRentabilidadManejoForestalDetalle()) {
                        if(anio == e.getAnio()){
                            monto = monto.add(new BigDecimal(e.getMonto(), MathContext.DECIMAL64));
                        }
                    }
                    setearBodyData( monto.toString(), 1, table);
                }
            }
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("        12.1.2  Egreso ", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(1);
        medidaCeldas = new float[]{1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        setearBodyStatic("Rubro", 1, table);

        if (listaFlujoCajaEgreso != null && !listaFlujoCajaEgreso.isEmpty()) {
            Integer columnas = listaAnioEgreso.size();

            table = new PdfPTable(columnas + 1);
            medidaCeldas = new float[columnas + 1];
            medidaCeldas[0] = 1.40f;
            for (Integer i = 1; i<=columnas; i++) {
                medidaCeldas[i] = 0.50f;
            }
            try {
                table.setWidths(medidaCeldas);
            } catch (DocumentException e) {
            }
            table.setWidthPercentage(95);

            setearBodyStatic("Rubro", 1, table);
            for (Integer anio : listaAnioEgreso) {
                setearBodyStatic("Año " + anio.toString(), 1, table);
            }

            for (RentabilidadManejoForestalDto element : listaFlujoCajaEgreso) {
                setearBodyData(element.getRubro() == null ? "" : element.getRubro(), 1, table);
                BigDecimal monto = BigDecimal.ZERO;
                for (Integer anio : listaAnioEgreso) {
                    monto = BigDecimal.ZERO;
                    for (RentabilidadManejoForestalDetalleEntity e : element.getListRentabilidadManejoForestalDetalle()) {
                        if(anio == e.getAnio()){
                            monto = monto.add(new BigDecimal(e.getMonto(), MathContext.DECIMAL64));
                        }
                    }
                    setearBodyData( monto.toString(), 1, table);
                }
            }
        }

        document.add(table);

        preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("     12.2  Necesidades de Financiamiento", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        table = new PdfPTable(1);
        medidaCeldas = new float[]{ 1.40f };

        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {
        }
        table.setWidthPercentage(95);

        if(listaNecesidadesFinanciamiento != null && !listaNecesidadesFinanciamiento.isEmpty()){
            for (RentabilidadManejoForestalDto eleement :listaNecesidadesFinanciamiento) {
                setearBodyData( eleement.getDescripcion(), 1, table);
            }

        }

        document.add(table);
    }
    
    public  PdfPTable cronogramaActividades(PdfWriter writer, DescargarPGMFDto obj) throws Exception { 
        //------DATA-------//
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setIdPlanManejo(obj.getIdPlanManejo());
        request.setCodigoProceso("PGMF");
        ResultEntity<CronogramaActividadEntity> resultCronograma = cronogramaActividadesRepository.ListarCronogramaActividad(request);
        List<CronogramaActividadEntity> listaCronogramaPlan = new ArrayList<>();
        List<CronogramaActividadEntity> listaCronogramaAprov = new ArrayList<>();

        if (resultCronograma != null && resultCronograma.getIsSuccess()) {

            List<CronogramaActividadEntity> listaCronograma = resultCronograma.getData();

            System.out.println(listaCronograma);

            listaCronogramaPlan = listaCronograma.stream()
                    .filter(c -> c.getCodigoActividad().equals("PLANIFICACION"))
                    .collect(Collectors.toList());

            listaCronogramaAprov = listaCronograma.stream()
                    .filter(c -> c.getCodigoActividad().equals("APROVECHAMIENTO"))
                    .collect(Collectors.toList());
        }

        //----------------//
        Integer meses = 12;
        Integer añoMax = 20;

        PdfPTable table = new PdfPTable(meses + añoMax);
        float[] medidaCeldas = new float[meses+añoMax];
        medidaCeldas[0] = 5f;
        for (int i = 1; i < meses+añoMax ; i++) {
            medidaCeldas[i] = 0.75f;
        }
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}

        table.setWidthPercentage(95);

        setearRowsColspanStatic("Actividad", 3, 1, table);
        setearRowsColspanStatic("AÑO 1",1,meses,table);
        setearRowsColspanStatic("AÑOS",1,añoMax-1,table);
        setearRowsColspanStatic("Meses",1,meses,table);

        for (int i = 2; i <= añoMax; i++) {
            setearRowsColspanStatic((""+i),2,1, table);
        }
        for (int i = 1; i <= meses; i++) {
            setearBodyStatic((""+i), 1, table);
        }

        setearBodyStatic("Fase de Planificación",meses+añoMax,table);

        for (CronogramaActividadEntity e : listaCronogramaPlan) {
            setearBodyData(e.getActividad()==null?"":e.getActividad() ,1, table);

            for (int i = 1; i <= añoMax ; i++) {

                if(i==1){
                    for (int j = 1; j <= meses; j++) {
                        Boolean completado = false;
                        for (CronogramaActividadDetalleEntity f : e.getDetalle()) {
                            if(f.getAnio()!=null && f.getAnio()==1 && f.getMes()==j){
                                setearBodyData("X", 1, table);
                                completado = true;
                            }
                        }
                        if(!completado) setearBodyData(" ", 1, table);
                    }
                }
                else{
                    Boolean completado = false;
                    for (CronogramaActividadDetalleEntity f : e.getDetalle()) {
                        if(f.getAnio()!=null && f.getAnio()==i){
                            setearBodyData("X", 1, table);
                            completado = true;
                        }
                    }
                    if(!completado) setearBodyData(" ", 1, table);
                }
            }
        }

        setearBodyStatic("Fase de Aprovechamiento",meses+añoMax,table);

        for (CronogramaActividadEntity e : listaCronogramaAprov) {
            setearBodyData(e.getActividad()==null?"":e.getActividad() ,1, table);

            for (int i = 1; i <= añoMax ; i++) {

                if(i==1){
                    for (int j = 1; j <= meses; j++) {
                        Boolean completado = false;
                        for (CronogramaActividadDetalleEntity f : e.getDetalle()) {
                            if(f.getAnio()!=null && f.getAnio()==1 && f.getMes()!=null && f.getMes()==j){
                                setearBodyData("X", 1, table);
                                completado = true;
                                break;
                            }
                        }
                        if(!completado) setearBodyData(" ", 1, table);
                    }
                }
                else{
                    Boolean completado = false;
                    for (CronogramaActividadDetalleEntity f : e.getDetalle()) {
                        if(f.getAnio()!=null && f.getAnio()==i){
                            setearBodyData("X", 1, table); 
                            completado = true;
                            break;
                        }
                    }
                    if(!completado) setearBodyData(" ", 1, table);
                }
            }
        }

        

        return table;
    }


    //-------------------------------------------------------------------------
    private static void setearCellcabezera(String dato, Integer colum, PdfPTable table){

        Font title = new Font(Font.HELVETICA, 12,Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato,title));
        Color colorHeader = new Color(189 , 214 , 238);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setColspan(colum);
        c.setBackgroundColor(colorHeader);
        c.setPadding(5);
        table.addCell(c);
        c.setMinimumHeight(20);
        table.setHeaderRows(1);
    }
    private static void setearSubTitle(String dato, Integer colum, PdfPTable table){
        Font subTitle = new Font(Font.HELVETICA, 9,Font.BOLD);
        Color colorSubTitle = new Color(242 , 242 , 242);
        PdfPCell c = new PdfPCell(new Phrase(dato,subTitle));
        c.setColspan(colum);
        c.setBackgroundColor(colorSubTitle);
        c.setPadding(5);
        table.addCell(c);
        c.setMinimumHeight(20);
        table.setHeaderRows(1);
    }
    private static void setearBodyStatic(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }   
    private static void setearBodyData(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        c.setColspan(colum);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }
    private static void setearBodyDataPreface(Paragraph preface, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(preface);
        c.setColspan(colum);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void setearRowsColspanStatic(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        c.setMinimumHeight(20);
        table.addCell(c);
    }
    private static void setearRowsColspanData(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        c.setMinimumHeight(20);
        table.addCell(c);
    }
    private static void setearBodyStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    } 
    private static void setearDataStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        c.setColspan(colum);
        c.setPadding(5);
        c.setMinimumHeight(20);
        table.addCell(c);
    }  
    private static void setearBodyDataSborder(String dato,Integer row, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        c.setRowspan(row);
        c.setColspan(colum);
        c.setPadding(5);
        c.setBorder(0);
        c.setMinimumHeight(20);
        table.addCell(c);
    }

    private Integer sumaTotalArbolesExistentes (List<SolicitudPlantacionForestalDetalleEntity> listaEsp){
        Integer suma = 0;
        for (SolicitudPlantacionForestalDetalleEntity element : listaEsp) {
            suma += element.getTotalArbolesExistentes();
        }
        return suma;
    }
    public static String sumAreaTotalHa(List<AreaBloqueEntity> data){

        String numero = "";
        BigDecimal suma = new BigDecimal(0);

        for (AreaBloqueEntity e : data) {
            suma = suma.add(e.getArea() == null ? new BigDecimal(0) : e.getArea());
        }
        BigDecimal total = suma.setScale(2, RoundingMode.HALF_UP);

        return numero = total.toString();
    }

    public static String sumaAreaHa(List<OrdenamientoProteccionDetalleEntity> lista){
        String numero="";
        BigDecimal cont = new BigDecimal(0);

        for (OrdenamientoProteccionDetalleEntity e : lista) {
            cont = cont.add(e.getAreaHA() == null ? new BigDecimal(0) : e.getAreaHA());
        }
        BigDecimal total = cont.setScale(2, RoundingMode.HALF_UP);
        return numero = total.toString();
    }

    private Double sumarColumna (List<RentabilidadManejoForestalDto> listaFlujoCaja, int columna){
        BigDecimal bd = BigDecimal.ZERO;
        for (RentabilidadManejoForestalDto e : listaFlujoCaja) {
            for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                if (el.getAnio().equals(columna)) {
                    BigDecimal b = new BigDecimal(el.getMonto(), MathContext.DECIMAL64);
                    bd = bd.add(b);
                }
            }
        }

        return bd.doubleValue();
    }

    private List<OrdenamientoProteccionDetalleEntity> calcularAreaTotalParcela(List<OrdenamientoProteccionDetalleEntity> lista) {
        BigDecimal bdSumaArea = BigDecimal.ZERO;
        for (OrdenamientoProteccionDetalleEntity e : lista) {
            bdSumaArea = bdSumaArea.add(e.getAreaHA());
        }
        for (OrdenamientoProteccionDetalleEntity e : lista) {
            if (e.getAreaHA() != null) {
                e.setAreaHAPorcentaje(
                        e.getAreaHA().multiply(new BigDecimal(100)).divide(bdSumaArea, 2, RoundingMode.HALF_UP));
            }
        }
        return lista;
    }


    
    private String marcarObjetivoEspecifico(String value,  List<ObjetivoEspecificoManejoEntity> list){
        String data ="";

        for (ObjetivoEspecificoManejoEntity e : list) {
            if(e.getIdTipoObjEspecifico().equals(value)) {
                data = "X";
                break;
            }
        }
        return data;
    }


    
    private static String getRespuestaImpactAmb(EvaluacionAmbientalActividadEntity eval, 
            List<EvaluacionAmbientalDto> listaRespuesta, Integer idEvalAprovechamiento) {
        String data = "";
        try {

            for (EvaluacionAmbientalDto resp : listaRespuesta) {
                if (resp.getIdEvalActividad().equals(eval.getIdEvalActividad()) &&
                        resp.getIdEvalAprovechamiento().equals(idEvalAprovechamiento) &&                
                        resp.getValorEvaluacion() != null && resp.getValorEvaluacion().equals("S")) {
                    data = "X";
                    break;
                }
            }

        } catch (Exception e) {
            data = "";
        }
        return data;
    }
}
