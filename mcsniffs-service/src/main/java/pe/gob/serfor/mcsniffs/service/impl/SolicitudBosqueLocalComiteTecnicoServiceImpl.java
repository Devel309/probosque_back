package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalComiteTecnicoEntity;

import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalComiteTecnicoRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalComiteTecnicoService;

import java.util.List;

@Service("SolicitudBosqueLocalComiteTecnicoService")
public class SolicitudBosqueLocalComiteTecnicoServiceImpl implements SolicitudBosqueLocalComiteTecnicoService{

    @Autowired
    private SolicitudBosqueLocalComiteTecnicoRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarComiteTecnicoSolicitudBosqueLocal(List<SolicitudBosqueLocalComiteTecnicoEntity> items) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (items != null && !items.isEmpty()) {
            for (SolicitudBosqueLocalComiteTecnicoEntity item : items) {
               result = repository.registrarSolicitudBosqueLocalComiteTecnico(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen comités técnicos por registrar.");
        }

        return result;

    }
    public ResultClassEntity listarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj) throws Exception {
        return repository.listarSolicitudBosqueLocalComiteTecnico(obj);
    }

    public ResultClassEntity eliminarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj) throws Exception {
        return repository.eliminarSolicitudBosqueLocalComiteTecnico(obj);
    }


}
