package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.RequisitoEvaluacionRepository;
import pe.gob.serfor.mcsniffs.service.RequisitoEvaluacionService;

@Service
public class RequisitoEvaluacionServiceImpl implements RequisitoEvaluacionService{
    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {Repository de los requisitos de las evaluaciones}
     *
     */
    @Autowired
    RequisitoEvaluacionRepository respository;

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {registra los requisitos de las evaluaciones}
     * @param:RequisitoEvaluacionEntity
     */
    @Override
    public ResultClassEntity<List<RequisitoEvaluacionEntity>> registrarRequisito(List<RequisitoEvaluacionEntity> requisito) {
        return respository.registrarRequisito(requisito);
    }

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {lista los requisitos de las evaluaciones}
     * @param:RequisitoEvaluacionEntity
     */
    @Override
    public ResultEntity<RequisitoEvaluacionEntity> listarRequisito(RequisitoEvaluacionEntity param) {
        return respository.listarRequisito(param);
    }

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {elimina los requisitos detalle de las evaluaciones}
     * @param:RequisitoEvaluacionDetalleEntity
     */
    @Override
    public ResultClassEntity<RequisitoEvaluacionDetalleEntity> eliminarRequisitoDetalle(
            RequisitoEvaluacionDetalleEntity param) {
        return respository.eliminarRequisitoDetalle(param);
    }
}
