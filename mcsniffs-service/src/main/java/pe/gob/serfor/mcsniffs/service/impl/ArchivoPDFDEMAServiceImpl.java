package pe.gob.serfor.mcsniffs.service.impl;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ImpactoAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ListaEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.SistemaManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ArchivoPDFDEMAService")
public class ArchivoPDFDEMAServiceImpl implements ArchivoPDFDEMAService {
        @Autowired
        private SolicitudAccesoServiceImpl acceso;

        @Autowired
        private PlanManejoRepository planManejoRepository;

        @Autowired
        private PlanManejoContratoRepository planManejoContratoRepository;

        @Autowired
        private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

        @Autowired
        private PGMFArchivoRepository pgmfArchivoRepo;

        @Autowired
        private InformacionGeneralDemaRepository infoGeneralDemaRepo;

        @Autowired
        private SistemaManejoForestalRepository smfRepo;

        @Autowired
        private ActividadSilviculturalRepository actividadSilviculturalRepository;

        @Autowired
        private CronogramaActividesRepository cronograma;

        @Autowired
        private SistemaManejoForestalRepository sistema;

        @Autowired
        private ImpactoAmbientalService ambiental;

        @Autowired
        CensoForestalRepository censoForestalDetalleRepo;

        @Autowired
        ZonificacionRepository zonificacionRepository;

        @Autowired
        CoreCentralRepository corecentral;
        /*
         * @Autowired
         * SolicitudAccesoRepository acceso;
         */

        @Autowired
        private ArchivoService serArchivo;

        @Autowired
        private ObjetivoManejoService objetivoservice;

        @Autowired
        private InformacionBasicaRepository informacionBasicaRepository;

        @Autowired
        private RecursoForestalRepository recursoForestalRepository;

        @Autowired
        private OrdenamientoProteccionRepository ordenamientoRepo;

        @Autowired
        private AprovechamientoRepository aprovechamientoRepository;

        @Autowired
        private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

        @Autowired
        private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

        @Autowired
        private PlanManejoEvaluacionService planManejoEvaluacionService;

        @Autowired
        private InformacionGeneralRepository infoGeneralRepo;

        @Autowired
        private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

        @Autowired
        private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

        @Autowired
        private ArchivoRepository archivoRepository;

        @Autowired
        private SolicitudOpinionRepository solicitudOpinionRepository;

        @Autowired
        private EvaluacionCampoRespository evaluacionCampoRespository;

        @Autowired
        private ProteccionBosqueRepository proteccionBosqueRespository;

        @Autowired
        private MonitoreoRepository monitoreoRepository;

        @Autowired
        private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

        @Autowired
        private ParticipacionComunalRepository participacionComunalRepository;

        @Autowired
        private CapacitacionRepository capacitacionRepository;

        @Autowired
        private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

        @Autowired
        private ManejoBosqueRepository manejoBosqueRepository;

        @Autowired
        private ResumenActividadPoRepository resumenActividadPoRepository;

        @Autowired
        private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

        @Autowired
        private EvaluacionRepository evaluacionRepository;

        @Autowired
        private EvaluacionDetalleRepository evaluacionDetalleRepository;

        @Autowired
        private ObjetivoManejoRepository objetivoManejoRepository;

        @Autowired
        private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

        @Autowired
        private UnidadFisiograficaRepository unidadFisiograficaRepository;

        @Autowired
        private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

        @Autowired
        private CensoForestalRepository censoForestalRepository;

        @Autowired
        private PGMFAbreviadoRepository pgmfaBreviadoRepository;

        @Autowired
        private CronogramaActividesRepository repCronAct;

        @Autowired
        private SistemaManejoForestalService sistemaManejoForestalService;

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Font titulo = new Font(Font.HELVETICA, 11f, Font.BOLD);
        Font subTitulo = new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font contenido = new Font(Font.HELVETICA, 8f, Font.COURIER);
        Font cabecera = new Font(Font.HELVETICA, 9f, Font.BOLD);
        Font subTituloTabla = new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font subTitulo2 = new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font letraPeque = new Font(Font.HELVETICA, 6f, Font.COURIER);

        private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
                InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
                return new XWPFDocument(file);
        }


        /**
         * @autor: Rafael Azaña / 18-03-2022
         * @modificado:
         * @descripción: {consolidado PDF con itext}
         */
        @Override
        public ByteArrayResource consolidadoDEMA_PDF(Integer idPlanManejo) throws Exception {
                XWPFDocument doc = getDoc("dema-consolidado.docx");

                ByteArrayOutputStream b = new ByteArrayOutputStream();
                doc.write(b);
                doc.close();

                /* ***********************************   USO DE LIBRERIA PDF       *************************************/
                InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
                WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
                File archivo = File.createTempFile("consolidadoDEMA", ".pdf");

                FileOutputStream os = new FileOutputStream(archivo);

                createPdfDEMA(os, idPlanManejo);
                os.flush();
                os.close();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] fileContent = Files.readAllBytes(archivo.toPath());
                return new ByteArrayResource(fileContent);
        }

        public void createPdfDEMA(FileOutputStream os, Integer idPlanManejo) {
                Document document = new Document(PageSize.A4, 40, 40, 40, 40);
                document.setMargins(60, 60, 40, 40);
                try {
                        PdfWriter writer = PdfWriter.getInstance(document, os);

                        document.open();
                        Paragraph titlePara1 = new Paragraph(
                                        "FORMATO DE LA DECLARACIÓN DE MANEJO PARA PERMISOS DE APROVECHAMIENTO FORESTAL EN COMUNIDADES NATIVAS Y COMUNIDADES CAMPESINAS\n",
                                        titulo);
                        titlePara1.setAlignment(Element.ALIGN_CENTER);
                        document.add(new Paragraph(titlePara1));
                        document.add(new Paragraph("\r\n\r\n"));
                        document.add(new Paragraph("1. INFORMACION GENERAL", subTitulo));
                        document.add(new Paragraph("\r\n\r\n"));
                        PdfPTable table1 = createTableInformacionGeneral(writer, idPlanManejo);
                        document.add(table1);

                        document.add(new Paragraph("\r\n\r\n"));
                        document.add(new Paragraph("2. ZONIFICACIÓN U ORDENAMIENTO INTERNO DEL ÁREA", subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table2 = createZonificacion(writer, idPlanManejo);
                        document.add(table2);

                        document.add(new Paragraph("\r\n\r\n"));
                        document.add(new Paragraph("3. APROVECHAMIENTO DE RECURSOS FORESTALES MADERABLES", subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table3 = createAprovechamientoMaderable(writer, idPlanManejo);
                        document.add(table3);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("Resumen agrupado por especie:", subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table4 = createAprovechamientoPorEspecie(writer, idPlanManejo);
                        document.add(table4);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("4.	APROVECHAMIENTO DE RECURSOS FORESTALES NO MADERABLES",subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table5 = createAprovechamientoNMaderable(writer, idPlanManejo);
                        document.add(table5);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("Resumen agrupado por especie:", subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table6 = createAprovechamientoPorEspecieNM(writer, idPlanManejo);
                        document.add(table6);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("5. SISTEMA DE MANEJO Y LABORES SILVICULTURALES",subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table7 = setearTabSistemaManejoLaboresSiviculturales(writer, idPlanManejo);
                        document.add(table7);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("6. MEDIDAS DE PROTECCION DE LA UNIAD DE MANEJO FORESTAL",subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table8 = setearMedidasDeProteccionUnidadManejoForestal(writer, idPlanManejo);
                        document.add(table8);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("7. DESCRIPCIÓN DE LAS ACTIVIDADES DE APROVECHAMIENTO Y EQUIPOS A UTILIZAR",subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table9 = procesarActividadesEquipos(writer, idPlanManejo);
                        document.add(table9);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("8. IDENTIFICACIÓN DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCIÓN Y MITIGACIÓN",subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table10 = procesarImpactosAmbientales(writer, idPlanManejo);
                        document.add(table10);
                        document.add(new Paragraph("\r\n"));
                        document.add(new Paragraph("9.\tCRONOGRAMA DE ACTIVIDADES",subTitulo));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table11 = procesarImpactosAmbientales(writer, idPlanManejo);
                        document.add(table11);
                        document.add(new Paragraph("\r\n"));
                        Paragraph titlePara2 = new Paragraph("Anexo 2",titulo);
                        titlePara2.setAlignment(Element.ALIGN_CENTER);
                        document.add(new Paragraph(titlePara2));
                        document.add(new Paragraph("\r\n"));
                        Paragraph titlePara3 = new Paragraph("Detalle de los volúmenes comerciales maderables\n" +
                                "(Resultados del mateo por anexo o sector)\n",titulo);
                        titlePara3.setAlignment(Element.ALIGN_CENTER);
                        document.add(new Paragraph(titlePara3));
                        document.add(new Paragraph("\r\n"));
                        PdfPTable table12 = anexos2(writer, idPlanManejo);
                        document.add(table12);



                } catch (Exception e) {
                        e.printStackTrace();
                } finally {
                        document.close();
                }
        }

        public PdfPTable createAprovechamientoMaderable(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(8);
                table.setWidthPercentage(90);
                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Especie", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Coordenadas UTM", subTitulo));
                cell.setColspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Árbol aprovechable", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Volumen comercial m3", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Árbol semillero", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre común español", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre en idioma nativo", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Este", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Norte", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<CensoForestalDetalleEntity> lstLista = censoForestalDetalleRepo.ListarCensoForestalDetalle(idPlanManejo,"", "1");
                if(lstLista.size()>0) {
                        for (CensoForestalDetalleEntity detalleEntity : lstLista) {
                                cell = new PdfPCell(new Paragraph(
                                        detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreCientifico() != null ? detalleEntity.getNombreCientifico().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreNativo() != null ? detalleEntity.getNombreNativo().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getIdTipoArbol().equals("1") ? detalleEntity.getIdTipoArbol().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getVolumen() != null ? String.valueOf(detalleEntity.getVolumen()) : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getIdTipoArbol().equals("2") ? detalleEntity.getIdTipoArbol().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                        }
                }

                return table;
        }

        public PdfPTable createZonificacion(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(6);
                table.setWidthPercentage(90);
                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Zonas", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Superficie estimada (ha)", subTitulo));
                cell.setColspan(5);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Anexo o Sector 1", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Anexo o Sector 2", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Anexo o Sector 3", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Total", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("%", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                ZonificacionEntity zoni = new ZonificacionEntity();
                zoni.setIdPlanManejo(idPlanManejo);
                ResultClassEntity resultClassEntity = zonificacionRepository.ListarZonificacion(zoni);
                if(resultClassEntity!=null) {
                        List<ZonificacionEntity> zonificacionEntityList = (List<ZonificacionEntity>) resultClassEntity.getData();
                        if(zonificacionEntityList.size()>0) {
                                for (ZonificacionEntity detalleEntity : zonificacionEntityList) {
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getZona() != null ? detalleEntity.getZona().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getAnexo1() != null ? detalleEntity.getAnexo1().toString() : "", contenido));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getAnexo2() != null ? detalleEntity.getAnexo2().toString() : "", contenido));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getAnexo3() != null ? detalleEntity.getAnexo3().toString() : "", contenido));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getTotal() != null ? detalleEntity.getTotal().toString() : "", contenido));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getPorcentaje() != null ? detalleEntity.getPorcentaje().toString() : "", contenido));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                }
                        }
                }

                return table;
        }

        public PdfPTable createTableInformacionGeneral(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(90);
                PdfPCell cell;
                int size = 20;
                // InformacionGeneral
                InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
                o.setIdPlanManejo(idPlanManejo);
                o.setCodigoProceso("DEMA");
                o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
                SolicitudAccesoEntity param = new SolicitudAccesoEntity();
                param.setNumeroDocumento(o.getDniJefecomunidad());
                List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
                if (lstacceso.size() > 0) {
                        o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
                        o.setProvincia(lstacceso.get(0).getNombreProvincia());
                        o.setDistrito(lstacceso.get(0).getNombreDistrito());
                        o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
                        o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
                        o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
                        o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
                        o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
                }
                cell = new PdfPCell(new Paragraph("1.1. De los datos generales", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("De la Comunidad", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Nombre de la Comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(
                                new Paragraph(o.getNombreComunidad() != null ? o.getNombreComunidad().toString() : "",
                                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Nombre y Apellidos del jefe de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                String nombres = toString(o.getNombresJefeComunidad()) + " "
                                + toString(o.getApellidoPaternoJefeComunidad())
                                + " " + toString(o.getApellidoMaternoJefeComunidad());
                cell = new PdfPCell(new Paragraph(nombres, contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("DNI del jefe de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(
                                new Paragraph(o.getDniJefecomunidad() != null ? o.getDniJefecomunidad().toString() : "",
                                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(
                                new Paragraph("Federación u organización a la que pertenece la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getFederacionComunidad() != null ? o.getFederacionComunidad().toString() : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Departamento", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getDepartamento()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Provincia", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getProvincia()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Distrito", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getDistrito()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Cuenca", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(o.getCuenca() != null ? o.getCuenca().toString() : "", contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Subcuenca", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(
                                new Paragraph(o.getSubCuenca() != null ? o.getSubCuenca().toString() : "", contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° de anexos de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroAnexosComunidad()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° de resolución de reconocimiento de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getNroResolucionComunidad() != null ? o.getNroResolucionComunidad().toString() : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° de título de propiedad de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getNroTituloPropiedadComunidad() != null
                                                ? o.getNroTituloPropiedadComunidad().toString()
                                                : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° total de familias de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroTotalFamiliasComunidad()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° de RUC de la comunidad", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(
                                new Paragraph(o.getNroRucComunidad() != null ? o.getNroRucComunidad().toString() : "",
                                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("De la DEMA", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Periodo de Vigencia de la DEMA (de mes/año a mes/año)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(
                                new Paragraph(toString(o.getFechaInicioDema()) + " a " + toString(o.getFechaFinDema()),
                                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Nombre de la persona que elabora la DEMA", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                String elaborador = toString(o.getNombreElaboraDema()) + " "
                                + toString(o.getApellidoPaternoElaboraDema()) + " "
                                + toString(o.getApellidoMaternoElaboraDema());
                cell = new PdfPCell(new Paragraph(toString(elaborador), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("DNI de la persona que elabora la DEMA", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(
                                new Paragraph(o.getDniElaboraDema() != null ? o.getDniElaboraDema().toString() : "",
                                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° de familias involucradas en el aprovechamiento forestal",
                                subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroTotalFamiliasComunidad()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Aprovechamiento Maderable", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° total de árboles maderables para aprovechar", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroArbolesMaderables()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° total de árboles maderables semilleros", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroArbolesMaderablesSemilleros()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Superficie para el aprovechamiento maderable (ha)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getSuperficieHaMaderables() != null ? o.getSuperficieHaMaderables().toString() : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Volumen maderable total para aprovechar [m3 (r)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getVolumenM3rMaderables() != null ? o.getVolumenM3rMaderables().toString() : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Aprovechamiento No Maderable", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° total de árboles no maderables para aprovechar", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroArbolesNoMaderables()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("N° total de árboles no maderables semilleros", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getNroArbolesNoMaderablesSemilleros()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Superficie para el aprovechamiento no maderable (ha)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getSuperficieHaNoMaderables() != null ? o.getSuperficieHaNoMaderables().toString()
                                                : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Volumen no maderable total para aprovechar [m3 (r)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(
                                o.getVolumenM3rNoMaderables() != null ? o.getVolumenM3rNoMaderables().toString() : "",
                                contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Utilidades", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Total de ingresos estimado (S/)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getTotalIngresosEstimado()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Total de costos estimado (S/)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getTotalCostoEstimado()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Total de utilidades estimado (S/)", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                if(o!=null) {
                cell = new PdfPCell(new Paragraph(toString(o.getTotalUtilidadesEstimado()), contenido));
                }else{cell = new PdfPCell(new Paragraph("", contenido));}
                cell.setColspan(2);
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(
                                "1.2. De los regímenes promocionales aplicables al aprovechamiento forestal (si ó no)",
                                subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(
                                "La UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR",
                                subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("Si1"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("No1"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("La UMF se ubica en zona prioritaria para el Estado", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("Si2"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("No2"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("En la UMF se realizan proyectos integrales de transformación",
                                subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("Si3"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("No3"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(
                                new Paragraph("La UMF cuenta con certificación forestal voluntaria de buenas prácticas",
                                                subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("Si4"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("No4"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(
                                "En al UMF se protege, conserva y/o recupera áreas no destinadas al aprovechamiento forestal.",
                                subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("Si5"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("No5"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("La UMF se encuentra bajo manejo de uso múltiple.", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("Si6"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(toString("No6"), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);

                return table;
        }

        public PdfPTable createAprovechamientoPorEspecie(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(7);
                table.setWidthPercentage(90);
                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Especie", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nº de árboles aprovechables", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Volumen comercial m3", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nº de árboles semilleros", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nº total de árboles", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre común español", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre en idioma nativo", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<ListaEspecieDto> lstLista = censoForestalDetalleRepo.ListaResumenEspecies(idPlanManejo, "", "1");
                if (lstLista.size() > 0) {
                        for (ListaEspecieDto detalleEntity : lstLista) {
                                cell = new PdfPCell(new Paragraph( detalleEntity.getNombre() != null ? detalleEntity.getNombre().toString(): "",subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getCientifico() != null? detalleEntity.getCientifico().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNativo() != null ? detalleEntity.getNativo().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesAprovechables() != null? detalleEntity.getNumeroArbolesAprovechables().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getVolumenComercial() != null? detalleEntity.getVolumenComercial().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolSemilleros() != null? detalleEntity.getNumeroArbolSemilleros().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolTotal() != null? String.valueOf(detalleEntity.getNumeroArbolTotal()): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                        }
                }
                return table;
        }

        public PdfPTable createAprovechamientoNMaderable(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(10);
                table.setWidthPercentage(90);
                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Especie", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Coordenadas UTM", subTitulo));
                cell.setColspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Árbol aprovechable", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Producto", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Unidad de medida", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Cantidad de producto", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Árbol semillero", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre común español", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre en idioma nativo", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Este", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Norte", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<CensoForestalDetalleEntity> lstLista = censoForestalDetalleRepo.ListarCensoForestalDetalle(idPlanManejo, "", "2");
                if(lstLista.size() > 0) {
                        for (CensoForestalDetalleEntity detalleEntity : lstLista) {
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreCientifico() != null ? detalleEntity.getNombreCientifico().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreNativo() != null ? detalleEntity.getNombreNativo().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getIdTipoArbol().equals("1") ? detalleEntity.getIdTipoArbol().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getProductoTipo() != null ? String.valueOf(detalleEntity.getProductoTipo()) : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getCodigoUnidadMedida() != null? detalleEntity.getCodigoUnidadMedida().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getCantidad() != null? detalleEntity.getCantidad().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getIdTipoArbol().equals("2") ? detalleEntity.getIdTipoArbol().toString() : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                        }
                }

                return table;
        }

        public PdfPTable createAprovechamientoPorEspecieNM(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(9);
                table.setWidthPercentage(90);

                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Especie", subTitulo));
                cell.setColspan(3);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nº de árboles aprovechables", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Producto", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Unidad de medida", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Cantidad de producto", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nº de árboles semilleros", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nº total de árboles", subTitulo));
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("Nombre común español", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Nombre en idioma nativo", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<ListaEspecieDto> lstLista = censoForestalDetalleRepo.ListaResumenEspecies(idPlanManejo, "", "2");
                if (lstLista.size() > 0) {
                        for (ListaEspecieDto detalleEntity : lstLista) {
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNombre() != null ? detalleEntity.getNombre().toString(): "",subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getCientifico() != null ? detalleEntity.getCientifico().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNativo() != null ? detalleEntity.getNativo().toString() : "",subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesAprovechables() != null? detalleEntity.getNumeroArbolesAprovechables().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph("", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getCantProducto() != null? detalleEntity.getCantProducto().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolSemilleros() != null? detalleEntity.getNumeroArbolSemilleros().toString(): "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolTotal() != null? String.valueOf(detalleEntity.getNumeroArbolTotal()) : "", subTitulo2));
                                cell.setFixedHeight(25);
                                table.addCell(cell);
                        }
                }

                return table;
        }

        public PdfPTable setearTabSistemaManejoLaboresSiviculturales(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(6);
                table.setWidthPercentage(90);
                float[] values = new float[6];
                values[0] = 110;
                values[1] = 80;
                values[2] = 60;
                values[3] = 60;
                values[4] = 60;
                values[5] = 60;
                table.setWidths(values);

                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Labores Silviculturales", subTitulo));
                cell.setColspan(6);
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Actividades", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Descripción", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Equipos", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Insumos", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Personal", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Observaciones", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<ActividadSilviculturalDetalleEntity> list = ((ActividadSilviculturalDto) actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo, "4").getData()).getDetalle();


                if(list!=null) {
                        cell = new PdfPCell(new Paragraph("Obligatorias", subTitulo2));
                        cell.setFixedHeight(25);
                        cell.setColspan(6);
                        table.addCell(cell);

                        List<ActividadSilviculturalDetalleEntity> obligatorias = getObligatoriasOpcionales(list, "1");

                        if (obligatorias.size() > 0) {
                                for (ActividadSilviculturalDetalleEntity detalleEntity : obligatorias) {
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getActividad() != null ? detalleEntity.getActividad().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionDetalle() != null ? detalleEntity.getDescripcionDetalle().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getEquipo() != null ? detalleEntity.getEquipo().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getInsumo() != null ? detalleEntity.getInsumo().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getPersonal() != null ? detalleEntity.getPersonal().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getObservacionDetalle() != null ? detalleEntity.getObservacionDetalle().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                }
                        }


                        cell = new PdfPCell(new Paragraph("Opcionales", subTitulo2));
                        cell.setFixedHeight(25);
                        cell.setColspan(6);
                        table.addCell(cell);

                        List<ActividadSilviculturalDetalleEntity> opcionales = getObligatoriasOpcionales(list, "2");

                        if (opcionales.size() > 0) {
                                for (ActividadSilviculturalDetalleEntity detalleEntity : opcionales) {
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getActividad() != null ? detalleEntity.getActividad().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionDetalle() != null ? detalleEntity.getDescripcionDetalle().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getEquipo() != null ? detalleEntity.getEquipo().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getInsumo() != null ? detalleEntity.getInsumo().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getPersonal() != null ? detalleEntity.getPersonal().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getObservacionDetalle() != null ? detalleEntity.getObservacionDetalle().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                }
                        }

                }

                return table;
        }

        private List<ActividadSilviculturalDetalleEntity> getObligatoriasOpcionales(List<ActividadSilviculturalDetalleEntity> list, String tipo) {
                List<ActividadSilviculturalDetalleEntity> lista = new ArrayList<>();
                for (ActividadSilviculturalDetalleEntity ac : list) {
                        if (ac.getIdTipoTratamiento().equals(tipo)) {
                                lista.add(ac);
                        }
                }
                return lista;
        }


        public PdfPTable setearMedidasDeProteccionUnidadManejoForestal(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(2);
                table.setWidthPercentage(90);

                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Medidas", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Descripción", subTitulo));
                cell.setFixedHeight(size);
                table.addCell(cell);

                SistemaManejoForestalDto list = (SistemaManejoForestalDto) smfRepo.obtener(idPlanManejo, "MPUMF").getData();
                if (list !=null) {
                        List<SistemaManejoForestalDetalleEntity> listaDetalle = list.getDetalle();
                        if (listaDetalle.size() > 0) {
                                for (SistemaManejoForestalDetalleEntity detalleEntity : listaDetalle) {
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getActividades() != null ? detalleEntity.getActividades().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                        cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionSistema() != null ? detalleEntity.getDescripcionSistema().toString() : "", subTitulo2));
                                        cell.setFixedHeight(25);
                                        table.addCell(cell);
                                }
                        }
                }

                return table;
        }

        public PdfPTable procesarActividadesEquipos(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(5);
                table.setWidthPercentage(90);

                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Actividades", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Descripción", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Equipos e insumos a utilizar", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Personal\n" +
                        "requerido\n", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Observaciones", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);

                //7 -  actividades y equipos a  utilizae
                ResultClassEntity<SistemaManejoForestalDto> lstSistema = sistema.obtener(idPlanManejo, "AAE");
                if (lstSistema.getData().getDetalle().size() >0) {
                        List<SistemaManejoForestalDetalleEntity> objList=lstSistema.getData().getDetalle();

                        if(objList.size()>0) {

                                // Primero se tiene que ordenar la data
                                List<SistemaManejoForestalDetalleEntity> lstAproveMadera = new ArrayList<>();
                                objList.stream().forEach((a) -> {
                                        if (a.getCodigoTipoDetalle().equals("AAEAM")) {
                                                lstAproveMadera.add(a);
                                        }
                                });
                                List<SistemaManejoForestalDetalleEntity> lstAproveNoMadera = new ArrayList<>();
                                objList.stream().forEach((a) -> {
                                        if (a.getCodigoTipoDetalle().equals("AAEANM")) {
                                                lstAproveNoMadera.add(a);
                                        }
                                });
                                List<SistemaManejoForestalDetalleEntity> lstAproveOtros = new ArrayList<>();
                                objList.stream().forEach((a) -> {
                                        if (a.getCodigoTipoDetalle().equals("AAEOTRO")) {
                                                lstAproveOtros.add(a);
                                        }
                                });

                                cell = new PdfPCell(new Paragraph("Aprovechamiento Maderable", subTitulo2));
                                cell.setFixedHeight(25);
                                cell.setColspan(5);
                                table.addCell(cell);

                                if (lstAproveMadera.size() > 0) {
                                        for (SistemaManejoForestalDetalleEntity detalleEntity : lstAproveMadera) {
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getActividades() != null ? detalleEntity.getActividades().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionSistema() != null ? detalleEntity.getDescripcionSistema().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getMaquinariasInsumos() != null ? detalleEntity.getMaquinariasInsumos().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getPersonalRequerido() != null ? detalleEntity.getPersonalRequerido().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getObservacion() != null ? detalleEntity.getObservacion().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                        }
                                }

                                cell = new PdfPCell(new Paragraph("Aprovechamiento No Maderable", subTitulo2));
                                cell.setFixedHeight(25);
                                cell.setColspan(5);
                                table.addCell(cell);

                                if (lstAproveNoMadera.size() > 0) {
                                        for (SistemaManejoForestalDetalleEntity detalleEntity : lstAproveNoMadera) {
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getActividades() != null ? detalleEntity.getActividades().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionSistema() != null ? detalleEntity.getDescripcionSistema().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getMaquinariasInsumos() != null ? detalleEntity.getMaquinariasInsumos().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getPersonalRequerido() != null ? detalleEntity.getPersonalRequerido().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getObservacion() != null ? detalleEntity.getObservacion().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                        }
                                }

                                cell = new PdfPCell(new Paragraph("Otros (especificar)", subTitulo2));
                                cell.setFixedHeight(25);
                                cell.setColspan(5);
                                table.addCell(cell);

                                if (lstAproveOtros.size() > 0) {
                                        for (SistemaManejoForestalDetalleEntity detalleEntity : lstAproveOtros) {
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getActividades() != null ? detalleEntity.getActividades().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionSistema() != null ? detalleEntity.getDescripcionSistema().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getMaquinariasInsumos() != null ? detalleEntity.getMaquinariasInsumos().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getPersonalRequerido() != null ? detalleEntity.getPersonalRequerido().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getObservacion() != null ? detalleEntity.getObservacion().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                        }
                                }
                        }
                }

                return table;
        }

        public PdfPTable procesarImpactosAmbientales(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(90);
                float[] values = new float[3];
                values[0] = 80;
                values[1] = 80;
                values[2] = 120;
                table.setWidths(values);
                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("Actividades que \ngeneran impacto", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Descripción del\n impacto", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Medidas de prevención y/o mitigación, incluido el \nmanejo de residuos sólidos", subTitulo));
                cell.setFixedHeight(40);
                table.addCell(cell);

                //8 - Impactod ambientales dengativos
                ImpactoAmbientalEntity impactoAmbientalEntity = new ImpactoAmbientalEntity();
                impactoAmbientalEntity.setIdPlanManejo(idPlanManejo);
                String medidas="";
                ResultClassEntity response = ambiental.ListarImpactoAmbiental(impactoAmbientalEntity);
                if(response!=null) {
                        List<ImpactoAmbientalDto> impactoAmbientalEntityList = (List<ImpactoAmbientalDto>) response.getData();
                        if (impactoAmbientalEntityList != null) {
                                        for (ImpactoAmbientalDto detalleEntity : impactoAmbientalEntityList) {
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getActividad() != null ? detalleEntity.getActividad().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion().toString() : "", subTitulo2));
                                                cell.setFixedHeight(25);
                                                table.addCell(cell);
                                                if(detalleEntity.getDetalle()!=null) {
                                                        List<ImpactoAmbientalDetalleEntity> lstDetallito = detalleEntity.getDetalle();
                                                        for (ImpactoAmbientalDetalleEntity obj : lstDetallito) {
                                                                medidas+= obj.getMedidasprevencion() != null ? obj.getMedidasprevencion()+"\n": "";
                                                        }
                                                        cell = new PdfPCell(new Paragraph(medidas, subTitulo2));
                                                        cell.setFixedHeight(25);
                                                        table.addCell(cell);
                                                }else{
                                                        cell = new PdfPCell(new Paragraph("", subTitulo2));
                                                        cell.setFixedHeight(25);
                                                        table.addCell(cell);
                                                }
                                        }
                        }
                }

                return table;
        }


        public  PdfPTable createCronogramaActividadesAnios(PdfWriter writer,Integer idPlanManejo) throws Exception {

                PdfPTable table = new PdfPTable(15);
                table.setWidthPercentage(90);
                PdfPCell cell;
                float[] values = new float[15];
                for(int i=0;i<values.length;i++){
                        if(i==0){
                                values[i] = 80;
                        }else{
                                values[i] = 15;
                        }
                }

                table.setWidths(values);
                int size = 30;



                Paragraph titlePara2 = new Paragraph("Actividad",cabecera);
                titlePara2.setAlignment(Element.ALIGN_CENTER);
                cell = new PdfPCell(titlePara2);
                cell.setRowspan(3);
                cell.setVerticalAlignment(3);
                cell.isUseAscender() ;
                cell.setFixedHeight(size);
                table.addCell(cell);
                Paragraph titlePara3 = new Paragraph("Años",cabecera);
                titlePara3.setAlignment(Element.ALIGN_CENTER);
                cell = new PdfPCell(titlePara3);
                cell.setColspan(14);
                cell.setVerticalAlignment(3);
                cell.isUseAscender() ;
                cell.setFixedHeight(size);
                table.addCell(cell);

                Paragraph titlePara4 = new Paragraph("1",cabecera);
                titlePara4.setAlignment(Element.ALIGN_CENTER);
                cell = new PdfPCell(titlePara4);
                cell.setColspan(12);
                cell.setFixedHeight(size);
                table.addCell(cell);
                Paragraph titlePara5 = new Paragraph("2",cabecera);
                titlePara5.setAlignment(Element.ALIGN_CENTER);
                cell = new PdfPCell(titlePara5);
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);
                Paragraph titlePara6 = new Paragraph("3",cabecera);
                titlePara6.setAlignment(Element.ALIGN_CENTER);
                cell = new PdfPCell(titlePara6);
                cell.setRowspan(2);
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph( "1",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "2",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "3",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "4",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "5",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "6",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "7",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "8",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "9",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "10",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "11",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( "12",cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);




                //13. Cronograma de Actividades 36
                CronogramaActividadEntity request = new CronogramaActividadEntity();
                request.setCodigoProceso("DEMA");
                request.setIdPlanManejo(idPlanManejo);
                request.setIdCronogramaActividad(null);
                List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();

                String marca="";
                if(lstcrono!=null) {
                        for (CronogramaActividadEntity detalle : lstcrono) {
                                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                                List<CronogramaActividadDetalleEntity> lstDetalle = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                                if (lstDetalle != null) {
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==1).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==2).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==3).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==4).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==5).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==6).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==7).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==8).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==9).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==10).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==11).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==12).collect(Collectors.toList());
                                        cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);

                                } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                        table.addCell(cell);
                                }
                                List<CronogramaActividadDetalleEntity> lstDetalle2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                                if (lstDetalle2 != null) {
                                        cell = new PdfPCell(new Paragraph(lstDetalle2.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                }
                                List<CronogramaActividadDetalleEntity> lstDetalle3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                                if (lstDetalle3 != null) {
                                        cell = new PdfPCell(new Paragraph(lstDetalle3.size()>0 ? "X" : "", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);

                                } else {
                                        cell = new PdfPCell(new Paragraph("", contenido));
                                        cell.setFixedHeight(size);
                                        table.addCell(cell);
                                }
                        }
                }

                return table;
        }



        public PdfPTable anexos2(PdfWriter writer, Integer idPlanManejo) throws Exception {
                PdfPTable table = new PdfPTable(6);
                table.setWidthPercentage(90);

                PdfPCell cell;
                int size = 20;
                cell = new PdfPCell(new Paragraph("N° Árbol \n(Código)", subTitulo));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Especie (nombre \ncomun en español)", subTitulo));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Diametro \n(m)", subTitulo));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Altura comercial \n(m)", subTitulo));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Volumen \nComercial m3(r) ", subTitulo));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Observaciones", subTitulo));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);

                List<Anexo2CensoEntity> list= censoForestalRepository.ListarCensoForestalAnexo2(idPlanManejo,"1");
                if (list !=null) {
                        for (Anexo2CensoEntity detalleEntity : list) {
                            cell = new PdfPCell(new Paragraph(detalleEntity.getCodArbol() != null ? detalleEntity.getCodArbol().toString() : "", contenido));
                            cell.setFixedHeight(25);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComunDesc() != null ? detalleEntity.getNombreComunDesc().toString() : "", contenido));
                            cell.setFixedHeight(25);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalleEntity.getDiametro() != null ? detalleEntity.getDiametro().toString() : "", contenido));
                            cell.setFixedHeight(25);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalleEntity.getAltura() != null ? detalleEntity.getAltura().toString() : "", contenido));
                            cell.setFixedHeight(25);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalleEntity.getVolumen() != null ? detalleEntity.getVolumen().toString() : "", contenido));
                            cell.setFixedHeight(25);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph("", contenido));
                            cell.setFixedHeight(25);
                            table.addCell(cell);
                        }
                }

                return table;
        }




        public static String toString(Object o) {
                if (o == null) {
                        return "";
                }
                return o.toString();
        }
}
