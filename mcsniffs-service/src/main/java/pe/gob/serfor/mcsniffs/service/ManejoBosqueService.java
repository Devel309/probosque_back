package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueService {
    ResultClassEntity RegistrarManejoBosque(ManejoBosqEntity manejoBosque) throws Exception;
    ResultClassEntity ObtenerManejoBosque(ManejoBosqEntity manejoBosque) throws Exception;

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {CRUD DE MANEJO DEL BOSQUE}
     * @param:ManejoBosqueDto
     * * @param:ManejoBosqueEntity
     */

    ResultClassEntity<List<ManejoBosqueEntity>> ListarManejoBosque(Integer idPlanManejo, String codigo ,String subCodigoGeneral,String subCodigoGeneralDet) throws Exception;
    ResultClassEntity<List<ManejoBosqueEntity>> ListarManejoBosqueTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso) throws Exception;

    ResultClassEntity RegistrarManejoBosque(List<ManejoBosqueEntity> list) throws Exception;
    ResultClassEntity EliminarManejoBosque(ManejoBosqueDto manejoBosqueDto) throws Exception;
    ResultClassEntity registrarManejoBosqueExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                 String codigoManejo, String subCodigoManejo, String codtipoManejoDet,
                                                 int idPlanManejo, int idManejoBosque, int idUsuarioRegistro) throws Exception;
}
