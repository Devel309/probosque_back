package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ManejoBosqueAprovechamientoService {
    ResultClassEntity RegistrarManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception;
    ResultClassEntity ObtenerManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception;
}
