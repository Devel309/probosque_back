/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Locale;

import com.google.gson.Gson;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JsonDataSource;
import pe.gob.serfor.mcsniffs.service.AuxReporteService;

/**
 *
 * @author Carlos Tang
 */
@Service("AuxReporteServiceImpl")
public class AuxReporteServiceImpl implements AuxReporteService {

    private final static Logger LOGGER = LoggerFactory.getLogger(AuxReporteServiceImpl.class);


    @Override
    public String generarReporte(Object objecto, String rutaReporte) throws Exception {
        try {
            JasperPrint print = llenarReporte(objecto, rutaReporte);
            return generarPdfEncode(print);
        } catch (JRException e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public void generarReporteTest(Object objecto, String rutaReporte, String rutaDestino) throws Exception {
        try {
            JasperPrint print = llenarReporte(objecto, rutaReporte);
            JasperExportManager.exportReportToPdfFile(print, rutaDestino);
        } catch (JRException e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public String obtenerImagen() {
        String ruta = null;
        try {
            URL rutaLogo = getClass().getResource("VariablesEstaticasReporte.IMG");
            File logo = new File(rutaLogo.getPath());
            ruta = this.codificado(logo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return ruta;
    }
//<editor-fold defaultstate="collapsed" desc="METODOS PRIVADOS">

    private JasperPrint llenarReporte(Object objecto, String rutaReporte) throws Exception {
        try {
            String json = new Gson().toJson(objecto);
            System.out.println("****JSON GENERADO***");
            System.out.println(json);
            JsonDataSource dataSource = new JsonDataSource(new ByteArrayInputStream(json.getBytes()));
            dataSource.setLocale(Locale.US);
            String rutaJasperReport = getClass().getResource(rutaReporte).getPath();
            return JasperFillManager.fillReport(rutaJasperReport, null, dataSource);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }

    }

    private String generarPdfEncode(JasperPrint print) {
        String codificado = null;
        try {
            byte[] pdfByte = JasperExportManager.exportReportToPdf(print);
            File pdf = File.createTempFile("Reportes", "Reportes");
            try (OutputStream out = new FileOutputStream(pdf)) {
                out.write(pdfByte);
                out.close();
            }
            codificado = this.codificado(pdf);
            pdf.delete();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return codificado;
    }
   
    public String codificado(File file) throws Exception {
        String codificado = null;
        try {
            byte[] fileArray = new byte[(int) file.length()];
            InputStream inputStream = new FileInputStream(file);
            inputStream.read(fileArray);
            codificado = Base64.encodeBase64String(fileArray);
            inputStream.close();

        } catch (UnsupportedEncodingException ex) {
            LOGGER.error(ex.getMessage());
        }
        return codificado;
    }
}
