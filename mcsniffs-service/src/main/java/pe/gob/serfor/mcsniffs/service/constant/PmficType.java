package pe.gob.serfor.mcsniffs.service.constant;

/**
 *
 * @author Jason Retamozo
 */
public final class PmficType {

    private PmficType(){
    }
    //1.1
    public static final String PERMISO = "1permiso";
    public static final String COMUNIDAD_NOMBRE = "1nombre";
    public static final String REPRESENTANTE_NOMBRE = "1representante";
    public static final String DNI = "1dni";
    public static final String FEDERACION = "1federacion";
    public static final String DEPARTAMENTO = "1departamento";
    public static final String PROVINCIA = "1provincia";
    public static final String DISTRITO = "1distrito";
    public static final String CUENCA = "1cuenca";
    public static final String SUB_CUENCA = "1subcuenca";
    public static final String AREA = "1area";
    public static final String NRO_ANEXOS= "1nroanexos";
    public static final String NRO_RESOLUCION= "1nroresolucion";
    public static final String TITULOS= "1titulos";
    public static final String RUC= "1ruc";
    public static final String FAMILIAS= "1familias";
    //1.2
    public static final String VIGENCIA = "1vigencia";
    public static final String FECHA_INICIO = "1fechainicio";
    public static final String FECHA_FINAL = "1fechafin";
    public static final String AREA_TOTAL_UMF = "1totalumf";
    //1.3
    public static final String REGENTE = "1regente";
    public static final String DNI_REGENTE = "1dniregente";
    public static final String DOMICILIO = "1domicilio" ;
    public static final String CERTIFICADO = "1certificado" ;
    public static final String INSCRIPCION = "1inscripcion";
    //1.4
    public static final String NRO_FAMILIAS_APROV = "1familiaaprov";
    public static final String NRO_ARBOLES_MADERABLES = "1arbolesma";
    public static final String NRO_ARBOLES_SEMILLEROS = "1arbolesms";
    public static final String SUPERFICIE = "1superficie";
    public static final String VOLUMEN_TOTAL = "1volumen";
    //1.5
    public static final String NRO_ARBOLES_NO_MADERABLES = "1arbolesnoma";
    public static final String NRO_ARBOLES_SEMILLEROS_NOM = "1arbolesnoms";
    public static final String SUPERFICIE_NOM = "1superficienom";
    public static final String VOLUMEN_TOTAL_NOM = "1volnom";
    //1.7
    public static final String EMPRESA = "1empresa";
    public static final String DOMICILIO_FISCAL = "1domiciliofiscal";
    public static final String REPRESENTANTE_EMPRESA = "1repempresa";
    public static final String DOMICILIO_REPRESENTANTE = "1direccionrep";
    public static final String DNI_REPRESENTANTE = "1dnirep";
    public static final String RUC_EMPRESA = "1rucempresa";
    public static final String COPIA_CONTRATO = "1copiacontrato";
}
