package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;
import pe.gob.serfor.mcsniffs.repository.TalaRepository;
import pe.gob.serfor.mcsniffs.service.TalaService;

@Service("serviceTala")
public class TalaServiceImpl implements TalaService {

    @Autowired
    private TalaRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarTala(List<TalaEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                repository.registrarTala(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se sincronizaron correctamente los registros.");
        result.setSuccess(true);
        return result;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarTalaDetalle(List<TalaDetalleEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                repository.registrarTalaDetalle(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se sincronizaron correctamente los registros.");
        result.setSuccess(true);
        return result;

    }
}
