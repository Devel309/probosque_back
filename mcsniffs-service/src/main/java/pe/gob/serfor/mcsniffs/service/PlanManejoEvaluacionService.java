package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.AprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvalSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.RecursoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.NotificacionDTO;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;



public interface PlanManejoEvaluacionService {
    ResultClassEntity<List<PlanManejoEvaluacionDto>> listarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;

    ResultClassEntity EliminarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;

    ResultClassEntity ActualizarPlanManejoEvaluacionEstado(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;

    ResultEntity<PlanManejoEvaluacionDetalleDto> ListarPlanManejoEvaluacionDetalle(Integer idPlanManejoEval);

    ResultClassEntity ListarPlanManejoEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param);

    ResultClassEntity RegistrarPlanManejoEvaluacionLineamiento(List<PlanManejoEvaluacionDetalleDto> param);

    ResultClassEntity ActualizarEvaluacionCampoPlanManejoEvaluacion(PlanManejoEvaluacionDto param);

    ResultClassEntity registrarPlanManejoEvaluacionDetalle(List<PlanManejoEvaluacionDetalleDto> listaPlanManejoEvaluacionDetalle) throws Exception;

    ResultClassEntity actualizarPlanManejoEvaluacionDetalle(List<PlanManejoEvaluacionDetalleDto> listaPlanManejoEvaluacionDetalle) throws Exception;

    ResultClassEntity obtenerEvaluacionPlanManejo(Integer idPlanManejo) throws Exception;

    ResultClassEntity notificarObservacionesAlTitular(NotificacionDTO notificacionDTO) throws Exception;

    ResultClassEntity registrarPlanManejoEvaluacionArchivo(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto, MultipartFile fileEvaluacion, String idTipoDocumento) throws Exception;

    ResultClassEntity actualizarEstadoPlanManejoEvaluacion(PlanManejoEvaluacionDto dto) throws Exception;

    ResultClassEntity obtenerUltimaEvaluacion(Integer idPlanManejo) throws Exception;

    ResultClassEntity enviarEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;

    ResultClassEntity actualizarIdSolicitudOpcionPlanManejoEvaluacion(PlanManejoEvalSolicitudOpinionDto planManejoEvalSolicitudOpinionDto) throws Exception;

    ResultClassEntity listarplanManejoEvaluacionEspecie(PlanManejoEvaluacionDto request)throws Exception;

    ResultClassEntity remitirNotificacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;

    ResultClassEntity notificarTitular(PlanManejoEvaluacionDto planEval) throws Exception;

    ResultClassEntity validarLineamiento(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;
}
