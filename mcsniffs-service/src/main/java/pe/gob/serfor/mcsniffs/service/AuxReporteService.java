/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.serfor.mcsniffs.service;

/**
 *
 * @author Carlos Tang
 */
public interface AuxReporteService {

    String generarReporte(Object objecto, String rutaReporte) throws Exception;

    void generarReporteTest(Object objecto, String rutaReporte, String rutaDestino) throws Exception;

    String obtenerImagen();

}
