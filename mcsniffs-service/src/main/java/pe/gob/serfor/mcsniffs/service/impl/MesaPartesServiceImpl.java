package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.repository.MesaPartesDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.MesaPartesRepository;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.MesaPartesService;



@Service("MesaPartesService")
public class MesaPartesServiceImpl implements MesaPartesService {

    private static final Logger log = LogManager.getLogger(MesaPartesServiceImpl.class);


    
    @Autowired
    private MesaPartesRepository mesaPartesRepository;

    @Autowired
    private MesaPartesDetalleRepository mesaPartesDetalleRepository;

    @Autowired
    private EmailService emailService;


    /**
     * @autor: Jason Retamozo [15-03-2022]
     * @creado:
     * @descripción: { listar mesa de partes PMFI-DEMA }
     */
    @Override
    public ResultClassEntity<List<MesaPartesDto>> listarMesaPartesPMFIDEMA(MesaPartesDto dto) throws Exception{
        List<MesaPartesDto> lista = mesaPartesRepository.listarMesaPartes(dto);
        MesaPartesDetalleDto m = null;

        if(lista!=null){
            if(lista.size()>0){
                for (MesaPartesDto mesaPartesDto : lista) {
                    m = new MesaPartesDetalleDto();
                    m.setIdMesaPartes(mesaPartesDto.getIdMesaPartes());
                    m.setFecha(dto.getFechaTramite());
                    m.setDetalle(dto.getDetalle());
                    List<MesaPartesDetalleDto> detalle= mesaPartesDetalleRepository.listarMesaPartesDetalle(m);
                    if(detalle!=null){
                        //listar req generales
                        List<MesaPartesDetalleDto> reqGenerales = detalle.stream().filter(g->g.getTipoRequisito().equals("REQGENE")).collect(Collectors.toList());
                        mesaPartesDto.setListarMesaPartesDetalle(reqGenerales);
                        //listar req tupa
                        List<MesaPartesDetalleDto> reqTupa = detalle.stream().filter(t->t.getTipoRequisito().equals("REQTUPA")).collect(Collectors.toList());
                        mesaPartesDto.setListarMesaPartesRequisitosTUPA(reqTupa);
                    }
                }
            }
        }


        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }

    @Override
    public ResultClassEntity<List<MesaPartesDto>> listarMesaPartes(MesaPartesDto dto) throws Exception {   
        
        List<MesaPartesDto> lista = mesaPartesRepository.listarMesaPartes(dto);
        MesaPartesDetalleDto m = null;
        for (MesaPartesDto mesaPartesDto : lista) {
            m = new MesaPartesDetalleDto(); 
            m.setIdMesaPartes(mesaPartesDto.getIdMesaPartes());            
            mesaPartesDto.setListarMesaPartesDetalle(mesaPartesDetalleRepository.listarMesaPartesDetalle(m));
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }


    /**
     * @autor: Jason Retamozo [15-03-2022]
     * @creado:
     * @descripción: { Registrar mesa de partes PMFI-DEMA }
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarMesaPartesPMFIDEMA(MesaPartesDto dto) throws Exception {

        ResultClassEntity result = null;
        result = mesaPartesRepository.registrarMesaPartes(dto);

        //registro requisitos generales
        if (dto.getListarMesaPartesDetalle()!=null) {
            for (MesaPartesDetalleDto element : dto.getListarMesaPartesDetalle()) {
                element.setIdMesaPartes(dto.getIdMesaPartes());
                element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
                result = mesaPartesDetalleRepository.registrarMesaPartesDetalle(element);
            }
        }
        //registro requisitos tupa
        if (dto.getListarMesaPartesRequisitosTUPA()!=null) {
            for (MesaPartesDetalleDto element : dto.getListarMesaPartesRequisitosTUPA()) {
                element.setIdMesaPartes(dto.getIdMesaPartes());
                element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
                result = mesaPartesDetalleRepository.registrarMesaPartesDetalle(element);
            }
        }

        /*
        if (dto.getConforme()!=null && dto.getConforme().equals("EMDOBS")) {
            enviarCorreo(dto.getIdPlanManejo());
        }
        */
        //enviarCorreo();

        return  result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarMesaPartes(MesaPartesDto dto) throws Exception {
        
        ResultClassEntity result = null;
        result = mesaPartesRepository.registrarMesaPartes(dto);

        if (dto.getListarMesaPartesDetalle()!=null) {
            for (MesaPartesDetalleDto element : dto.getListarMesaPartesDetalle()) {            
                element.setIdMesaPartes(dto.getIdMesaPartes());
                element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
                result = mesaPartesDetalleRepository.registrarMesaPartesDetalle(element);
            }
        }

        if (dto.getConforme()!=null && dto.getConforme().equals("EMDOBS")) {
            enviarCorreo(dto.getIdPlanManejo());
        }

        //enviarCorreo();

        return  result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity eliminarMesaPartes(MesaPartesDto dto) throws Exception{

        ResultClassEntity result = null;
        result = mesaPartesRepository.eliminarMesaPartes(dto);

        if (dto.getListarMesaPartesDetalle()!=null) {
            for (MesaPartesDetalleDto element : dto.getListarMesaPartesDetalle()) {            
                element.setIdMesaPartes(dto.getIdMesaPartes());
                element.setIdUsuarioElimina(dto.getIdUsuarioElimina());
                result = mesaPartesDetalleRepository.eliminarMesaPartesDetalle(element);
            }
        }
        if (dto.getListarMesaPartesRequisitosTUPA()!=null) {
            for (MesaPartesDetalleDto element : dto.getListarMesaPartesRequisitosTUPA()) {
                element.setIdMesaPartes(dto.getIdMesaPartes());
                element.setIdUsuarioElimina(dto.getIdUsuarioElimina());
                result = mesaPartesDetalleRepository.eliminarMesaPartesDetalle(element);
            }
        }

        return  result;
    }
 
    
    public void enviarCorreo(Integer idPlanManejo) throws Exception{
        
        List<MesaPartesDto>lista = mesaPartesRepository.listarMesaPartesSendEmail(idPlanManejo);

        for (MesaPartesDto mesaPartesDto : lista) {
            String[] e = {mesaPartesDto.getCorreo()};

            EmailEntity email = new EmailEntity();
             
            email.setSubject("SERFOR:: Evaluación plan manejo :"+mesaPartesDto.getIdPlanManejo());
            email.setEmail(e);
            String body="<html><head>"
                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                    +"</head>"
                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                    +"<div class=WordSection1>"
                    +"<p class=MsoNormal><span lang=ES>Estimado(a):</span></p>"
                    +"<p class=MsoNormal><span lang=ES>  El Código Plan de Manejo : "+   (mesaPartesDto.getIdPlanManejo().toString())+" </span></p>"
                    +"<p class=MsoNormal><span lang=ES>  Se encuentra observado, tiene 2 días hábiles para subsanarlo</span></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                    +"</div></body></html>";
            email.setContent(body);
            
            emailService.sendEmail(email);
        }
    }
 
}
