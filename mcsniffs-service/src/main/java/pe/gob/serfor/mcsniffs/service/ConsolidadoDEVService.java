package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ConsolidadoDEVService {

    //ByteArrayResource consolidadoPOACDEV(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoPMFICDEV(Integer idPlanManejo) throws Exception;

}
