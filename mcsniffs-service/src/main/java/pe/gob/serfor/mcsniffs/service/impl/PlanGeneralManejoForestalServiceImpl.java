package pe.gob.serfor.mcsniffs.service.impl;

import java.text.SimpleDateFormat;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaEntity;
import pe.gob.serfor.mcsniffs.entity.PlanGeneralManejoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.UbigeoArffsEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.PGMFEnvioCorreo;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoForestalContratoDto;
import pe.gob.serfor.mcsniffs.repository.GenericoRepository;
import pe.gob.serfor.mcsniffs.repository.PlanGeneralManejoForestalRepository;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.PlanGeneralManejoForestalService;
import pe.gob.serfor.mcsniffs.service.PlanManejoService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;

import javax.servlet.http.HttpServletRequest;

@Service
public class PlanGeneralManejoForestalServiceImpl implements PlanGeneralManejoForestalService{
    /**
     * @autor: JaquelineDB [27-08-2021]
     * @modificado: 
     * @descripción: {repositorio del plan de manejo forestal}
     */
    @Autowired
    PlanGeneralManejoForestalRepository repo;

    @Autowired
    private EmailService emailService;

    @Autowired
    private GenericoRepository genericoRepository;

    @Autowired
    PlanManejoService planManejoService;

    @Autowired
    private ServicioExternoService serExt;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;
    /**
     * @autor: JaquelineDB [27-08-2021]
     * @modificado: Harry Coa [01-09-2021]
     * @descripción: { registrar el plan de manejo forestal }
     * @param: List<PlanGeneralManejoForestalEntity>
     */
    @Override
    public ResultClassEntity registrarPlanManejoForestal(List<PlanGeneralManejoForestalEntity> list) throws Exception {
        return repo.registrarPlanManejoForestal(list);
    }

    /**
     * @autor: Harry Coa [31-08-2021]
     * @modificado:
     * @descripción: { ontiene el contrato seleccionado para PGMF}
     * @param: idContrato
     */
    @Override
    public ResultClassEntity obtenerContrato(ContratoEntity obj) throws Exception {
        return repo.obtenerContrato(obj);
    }

    /**
     * @autor: Harry Coa [31-08-2021]
     * @modificado:
     * @descripción: { lista el filtro de contratos para PGMF}
     * @param: idContrato = null o 0
     */
    @Override
    public ResultClassEntity<List<PlanManejoForestalContratoDto>> listarFiltroContrato() throws Exception {
        return repo.listarFiltroContrato();
    }


    /**
     * @autor: JaquelineDB [31-08-2021]
     * @modificado: 
     * @descripción: {lista los contratos vigentes sin plan de manejo}
     * @param:idContrato
     */
    @Override
    public ResultEntity<PlanManejoContratoEntity> listarContratosVigentes(Integer idContrato) {
        return repo.listarContratosVigentes(idContrato);
    }
    
    /**
     * @autor: JaquelineDB [31-08-2021]
     * @modificado: 
     * @descripción: {actualizar el plan de manejo forestal}
     * @param:PlanGeneralManejoForestalEntity
     */
    @Override
    public ResultClassEntity actualizarPlanManejoForestal(PlanGeneralManejoForestalEntity obj) {
        return repo.actualizarPlanManejoForestal(obj);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity enviarEvaluacion(PGMFEnvioCorreo obj, HttpServletRequest request1) throws Exception{

        ResultClassEntity envio = new ResultClassEntity();
        Gson gson = new Gson();


        UsuarioDto user= new UsuarioDto();
        user.setIdusuario(obj.getIdUsuario());
        String token= request1.getHeader("Authorization");
        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), token);
        ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
        String correo = null;
        String nombreCompleto = null;
        for(Object item: resultApiExt.getData().stream().toArray()) {
            LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
            correo= val.get("correoElectronico").toString();
            nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
        }
        String fecha = "";
        if(obj.getFecha() != null)
            fecha = new SimpleDateFormat("dd/MM/yyyy").format(obj.getFecha());

        if (correo!=null) {
            String[] mails=new String[1];
            mails[0]= correo;
            EmailEntity mail=new EmailEntity();
            mail.setSubject("SERFOR:: Plan General de Manejo Forestal");
            mail.setEmail(mails);

            String tiempo = "1 a&ntilde;o ";
            String codRegion="25";//region ucayali
            if (obj.getIdDepartamento()!=null && obj.getIdDepartamento().equals(codRegion)) {
                tiempo = "6 meses";
            }

            String body="<html><head>"
            +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
            +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
            +"</head>"
            +"<body lang=ES-PE style='word-wrap:break-word'>"
            +"<div class=WordSection1>"
            +"<p class=MsoNormal><span lang=ES>Estimado(s) </span> "+nombreCompleto+":</p>"
            +"<p class=MsoNormal><span lang=ES>  Se le informa que el Plan de Manejo "+obj.getIdPlanManejo().toString()+" fue enviado a evaluaci&oacute;n con &eacute;xito."
            +"Recordarle que el plan tiene vigencia de "+tiempo+" partiendo desde la fecha de firma de contrato "+fecha+"</span></p>"
            +"<br><br><br>"
            +"<p class=MsoNormal><b><i><span lang=ES>Atte. MCSniffs</span></i></b><i></i></p>"
            +"</div></body></html>";
          
            mail.setContent(body);
            Boolean smail = emailService.sendEmail(mail);
            if(smail){                
                envio.setMessage("Correo Enviado Correctamente.");
            }
        }


        planManejoService.cambiarEstadoPlanManejo(obj.getIdPlanManejo(), obj.getIdUsuario());
        return envio;
    }

    /**
     * @autor: JaquelineDB [29-09-2021]
     * @modificado: 
     * @descripción: {listar el ubigeo de un contrato}
     * @param:idContrato
     */
    @Override
    public ResultEntity<UbigeoArffsEntity> listarUbigeoContratos(Integer idContrato) {
        return repo.listarUbigeoContratos(idContrato);
    }

}
