package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.OtorgamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface OtorgamientoService {
    ResultClassEntity registrarOtorgamiento(OtorgamientoEntity obj) throws Exception;
    ResultClassEntity<OtorgamientoEntity> listarOtorgamiento(OtorgamientoEntity filtro) throws Exception;
    ResultClassEntity eliminarOtorgamiento(OtorgamientoEntity obj) throws Exception;
    ResultClassEntity listarPorFiltroOtorgamiento(String nroDocumento, String nroRucEmpresa, Integer idPlanManejo) throws Exception;
}
