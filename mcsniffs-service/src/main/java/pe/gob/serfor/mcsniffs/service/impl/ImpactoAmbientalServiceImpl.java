package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ImpactoAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ImpactoAmbientalRepository;
import pe.gob.serfor.mcsniffs.service.ImpactoAmbientalService;

import java.util.ArrayList;
import java.util.List;

@Service("impactoAmbientalServiceImpl")
public class ImpactoAmbientalServiceImpl implements ImpactoAmbientalService {

    @Autowired
    private ImpactoAmbientalRepository impactoAmbientalRepository;


    @Override
    public ResultClassEntity ListarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) throws Exception {

        ResultClassEntity<List<ImpactoAmbientalDto>> result = new ResultClassEntity<List<ImpactoAmbientalDto>>();
        List<ImpactoAmbientalDto> impactoAmbientalDtoList = new ArrayList<>();
        List<ImpactoAmbientalEntity> impactoAmbientalEntityList = (List<ImpactoAmbientalEntity>) impactoAmbientalRepository.ListarImpactoAmbiental(impactoAmbientalEntity).getData();

        for (ImpactoAmbientalEntity impacto:impactoAmbientalEntityList) {
            ImpactoAmbientalDto impactoDto = new ImpactoAmbientalDto();
            impactoDto.setIdImpactoambiental(impacto.getIdImpactoambiental());
            if(impacto.getIdPlanManejo() == null){
                impactoDto.setIdImpactoambiental(0);
            }
            impactoDto.setIdPlanManejo(impactoAmbientalEntity.getIdPlanManejo());
            impactoDto.setActividad(impacto.getActividad());
            impactoDto.setDescripcion(impacto.getDescripcion());
            List<ImpactoAmbientalDetalleEntity> impactoAmbientalDetalleEntityList = (List<ImpactoAmbientalDetalleEntity>) impactoAmbientalRepository.ListarImpactoAmbientalDetalle(impacto).getData();
            if(impacto.getIdPlanManejo()==null){
                impactoAmbientalDetalleEntityList.forEach(detalle -> {detalle.setIdImpactoambiental(0);detalle.setIdImpactoambientaldetalle(0);});
            }
            impactoDto.setDetalle(impactoAmbientalDetalleEntityList);
            impactoAmbientalDtoList.add(impactoDto);

        }

        result.setData(impactoAmbientalDtoList);
        result.setSuccess(true);
        result.setMessage("Se listó el impacto ambiental correctamente.");
        return  result;

    }

    @Override
    public ResultClassEntity RegistrarImpactoAmbiental(List<ImpactoAmbientalDto> impactoAmbientalDtoList) throws Exception {
        ResultClassEntity<List<ImpactoAmbientalDto>> result = new ResultClassEntity<>();
            impactoAmbientalDtoList.forEach(impactoDto -> {
                ImpactoAmbientalEntity iae = new ImpactoAmbientalEntity();
                iae.setIdImpactoambiental(impactoDto.getIdImpactoambiental());
                iae.setIdPlanManejo(impactoDto.getIdPlanManejo());
                iae.setActividad(impactoDto.getActividad());
                iae.setDescripcion(impactoDto.getDescripcion());
                iae.setIdUsuarioRegistro(impactoDto.getIdUsuarioRegistro());
                iae.setIdUsuarioModificacion(impactoDto.getIdUsuarioModificacion());

                if (iae.getIdImpactoambiental() == 0) {
                    iae = (ImpactoAmbientalEntity) impactoAmbientalRepository.RegistrarImpactoAmbiental(iae).getData();
                }else {
                    impactoAmbientalRepository.ActualizarImpactoAmbiental(iae);
                }

                ImpactoAmbientalEntity finalIae = iae;
                impactoDto.getDetalle().forEach(detalle -> {
                    if(detalle.getIdImpactoambientaldetalle()==0){
                        detalle.setIdImpactoambiental(finalIae.getIdImpactoambiental());
                        impactoAmbientalRepository.RegistrarImpactoAmbientalDetalle(detalle);
                    }else{
                        impactoAmbientalRepository.ActualizarImpactoAmbientalDetalle(detalle);
                    }
                });

            });

        result.setData(impactoAmbientalDtoList);
        result.setSuccess(true);
        result.setMessage("Se registró el impacto ambiental correctamente.");
        return  result;
    }

    @Override
    public ResultClassEntity EliminarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) {
        return impactoAmbientalRepository.EliminarImpactoAmbiental(impactoAmbientalEntity);
    }

    @Override
    public ResultClassEntity EliminarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity) {
        return impactoAmbientalRepository.EliminarImpactoAmbientalDetalle(impactoAmbientalDetalleEntity);
    }

    @Override
    public ResultClassEntity ListarImpactoAmbientalPmfi(MedidasPmfiDto medidasPmfiDto) throws Exception {
        return impactoAmbientalRepository.ListarImpactoAmbientalPmfi(medidasPmfiDto);
    }

    @Override
    public ResultClassEntity RegistrarImpactoAmbientalPmfi(ImpactoAmbientalPmfiDto impactoAmbientalPmfiDto) throws Exception {
        return impactoAmbientalRepository.RegistrarImpactoAmbientalPmfi(impactoAmbientalPmfiDto);
    }

}
