package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

import pe.gob.serfor.mcsniffs.entity.Dto.DescargarPGMF.DescargarPGMFDto;

public interface ArchivoPGMFService{

    ByteArrayResource archivoConsolidadoPGMF(DescargarPGMFDto obj, String token) throws Exception;

}
