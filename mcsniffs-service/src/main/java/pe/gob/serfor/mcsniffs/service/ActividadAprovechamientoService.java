package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;

public interface ActividadAprovechamientoService {
    ResultEntity<ActividadAprovechamientoEntity> ListarActvidadAprovechamiento(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity RegistrarActvidadAprovechamiento(List<ActividadAprovechamientoEntity>  request) throws Exception;
    ResultClassEntity EliminarActvidadAprovechamiento(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity EliminarActvidadAprovechamientoDet(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity EliminarActvidadAprovechamientoDetSub(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity registrarCensoComercialEspecieExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                          String codActvAprove, String  codSubActvAprove, String codActvAproveDet,
                                                          int idActvAprove, int idPlanManejo, int idUsuarioRegistro);

    ResultClassEntity<VolumenCortaEntity> ObtenerTotalVolumenCorta(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity<CensoAprovechamientoNoMaderables> ObtenerTotalAprovechamientoNoMaderable(ActividadAprovechamientoParam request) throws Exception;
}
