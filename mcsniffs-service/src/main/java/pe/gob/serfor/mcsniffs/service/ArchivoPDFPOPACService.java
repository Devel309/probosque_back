package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPOPACService {
    ByteArrayResource consolidadoPOPAC_PDF(Integer idPlanManejo) throws Exception;
}
