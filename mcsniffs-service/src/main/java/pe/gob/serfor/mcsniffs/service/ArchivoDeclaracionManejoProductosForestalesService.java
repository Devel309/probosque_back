package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

import pe.gob.serfor.mcsniffs.entity.Dto.DescargarDeclaracionManejoProductosForestales.DescargarArchivoDeclaracionManejoProductosForestalesDto;

public interface ArchivoDeclaracionManejoProductosForestalesService{

    ByteArrayResource ArchivoDeclaracionManejoProductosForestales(DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception;

}
