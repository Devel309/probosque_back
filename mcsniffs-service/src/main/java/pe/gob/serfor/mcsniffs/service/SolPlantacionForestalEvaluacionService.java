package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDetalleDto;

import java.util.List;

public interface SolPlantacionForestalEvaluacionService {

    ResultClassEntity obtenerEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDto obj) throws Exception;
    ResultClassEntity obtenerDetalleEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDetalleDto obj) throws Exception;
    ResultClassEntity registrarEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDto obj) throws Exception;
    ResultClassEntity actualizarEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDto obj) throws Exception;
    ResultClassEntity registrarDetalleEvaluacionSolicitudPlantacionForestal(List<SolPlantacionForestalEvaluacionDetalleDto> lst) throws Exception;
}
