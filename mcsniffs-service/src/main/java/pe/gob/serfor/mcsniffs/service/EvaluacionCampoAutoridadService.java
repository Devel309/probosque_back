package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad.EvaluacionCampoAutoridadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;


public interface EvaluacionCampoAutoridadService {
    ResultClassEntity ComboPorFiltroAutoridad(EvaluacionCampoAutoridadDto request) throws Exception;
    ResultClassEntity RegistrarEvaluacionCampoAutoridad(List<EvaluacionCampoAutoridadDto> list);
}
