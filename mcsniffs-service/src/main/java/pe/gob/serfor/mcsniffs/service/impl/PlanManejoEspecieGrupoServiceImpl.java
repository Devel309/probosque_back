package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEspecieGrupoEntity;


import pe.gob.serfor.mcsniffs.repository.PlanManejoEspecieGrupoRepository;
import pe.gob.serfor.mcsniffs.service.PlanManejoEspecieGrupoService;

@Service("PlanManejoEspecieGrupoService")
public class PlanManejoEspecieGrupoServiceImpl implements PlanManejoEspecieGrupoService{

    @Autowired
    private PlanManejoEspecieGrupoRepository repository;

    @Override
    public ResultClassEntity registrarPlanManejoEspecieGrupo(PlanManejoEspecieGrupoEntity obj) throws Exception {
        return repository.registrarPlanManejoEspecieGrupo(obj);
    }
}
