package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.AspectoHidrograficoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AspectoHidrograficoRepository;
import pe.gob.serfor.mcsniffs.service.AspectoHidrograficoService;

@Service("AspectoHidrograficoService")
public class AspectoHidrograficoServiceImpl implements AspectoHidrograficoService {
    @Autowired
    private AspectoHidrograficoRepository aspectoHidrograficoRepository;

    @Override
    @Transactional
    public ResultClassEntity RegistrarAspectoHidrografico(List<AspectoHidrograficoEntity> lista) throws Exception {
        ResultClassEntity result = null;
        for (AspectoHidrograficoEntity element : lista) {
            if(element.getIdAspectoHidrografico()==0){
                result = aspectoHidrograficoRepository.RegistrarAspectoHidrografico(element);
            }
           else{
                result = aspectoHidrograficoRepository.ActualizarAspectoHidrografico(element);
            }


        }
        return result;
    }

    @Override
    @Transactional
    public ResultClassEntity ActualizarAspectoHidrografico(List<AspectoHidrograficoEntity> lista) throws Exception {
        ResultClassEntity result = null;
        for (AspectoHidrograficoEntity element : lista) {
            result = aspectoHidrograficoRepository.ActualizarAspectoHidrografico(element);
        }
        return result;
    }

    @Override
    public ResultClassEntity<AspectoHidrograficoEntity> ListarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception {
        return aspectoHidrograficoRepository.ListarAspectoHidrografico(aspectoHidrograficoEntity);
    }

    @Override
    public ResultClassEntity<AspectoHidrograficoEntity> EliminarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception {
        return aspectoHidrograficoRepository.EliminarAspectoHidrografico(aspectoHidrograficoEntity);
    }

}
