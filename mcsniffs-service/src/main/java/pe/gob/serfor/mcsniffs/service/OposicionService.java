package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Oposicion.OposicionArchivoDTO;

public interface OposicionService {
    ResultClassEntity registrarOposicion(OposicionEntity obj)  throws Exception ;
    ResultClassEntity registrarOposicionArchivo(OposicionArchivoDTO item)  throws Exception ;
    ResultEntity<OposicionEntity> listarOposicion(OposicionRequestEntity filtro);
    ResultClassEntity registrarRespuestaOposicion(OposicionEntity obj);
    ResultClassEntity obtenerOposicion(Integer idOposicion) throws Exception;
    ResultClassEntity<OposicionAdjuntosEntity> obtenerOposicionRespuesta(Integer IdOposicion);
    ResultArchivoEntity descargarFormatoResulucion();
    ResultClassEntity adjuntarFormatoResolucion(OposicionEntity obj);
    ResultClassEntity actualizarOposicion(OposicionEntity obj);
    ResultClassEntity regitrarOpositor(PersonaRequestEntity obj);
    ResultClassEntity<PersonaRequestEntity> obtenerOpositor(OpositorRequestEntity obj);

    ResultClassEntity eliminarOposicionArchivo (Integer idArchivo, Integer idUsuario);
    ResultClassEntity registrarNuevaOposicion(OposicionEntity item) throws Exception;
}
