package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.PGMFAbreviadoRepository;
import pe.gob.serfor.mcsniffs.service.PGMFAbreviadoService;

@Service
public class PGMFAbreviadoServiceImpl implements PGMFAbreviadoService {
    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @Autowired
    PGMFAbreviadoRepository repo;

    /**
     * @autor: Miguel F. [31-07-2021]
     * @modificado:
     * @descripción: {obtener anexo4 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List<OrdenamientoDetalleEntity>> protecionVigilanciaObtener ( OrdenamientoDetalleEntity params) {
        return repo.protecionVigilanciaObtener( params);
    }

    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {insertar anexo2 PGMF}
     *
     */
    @Override
    public ResultClassEntity<String> protecionVigilanciaArchivo (MultipartFile filtro, OrdenamientoDetalleEntity params) throws Exception {
        return repo.protecionVigilanciaArchivo(filtro, params);
    }

    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @Override
    public ResultClassEntity<PGMFAbreviadoObjetivo2Entity> obtenerObjetivo2 (PGMFAbreviadoObjetivo2Entity filtro) {
        return repo.obtenerObjetivo2(filtro);
    }
    
    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @Override
    public ResultClassEntity<String> insertarObjetivo2 (PGMFAbreviadoObjetivo2Entity filtro) {
        return repo.insertarObjetivo2(filtro);
    }

    /**
     * @autor: Miguel F. [06-07-2021]
     * @modificado:
     * @descripción: {obtener anexo1 PGMF}
     *
     */
    @Override
    public ResultClassEntity<PGMFAbreviadoObjetivo1Entity> pgmfAbreviadoObj1 (PGMFAbreviadoObjetivo1Entity filtro) {
        return repo.pgmfAbreviadoObj1(filtro);
    }

    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List< ParticipacionCiudadanaEntity>> participacionCiudadana (ParticipacionCiudadanaEntity param) {
        return repo.participacionCiudadana(param);
    }


    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List<CapacitacionPGMFAbreviadoEntity>> Capacitacion (CapacitacionPGMFAbreviadoEntity param) {
        return repo.Capacitacion(param);
    }

    /**
     * @autor: Miguel F. [08-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<String> PGMFAbreviadoDetalleRegistrar (PGMFAbreviadoDetalleRegistrarEntity param) {
        return repo.PGMFAbreviadoDetalleRegistrar(param);
    }

    /**
     * @autor: Miguel F. [08-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity PGMFAbreviadoDetalleEliminar (PGMFAbreviadoDetalleEntity param) {
        return repo.PGMFAbreviadoDetalleEliminar(param);
    }

    /**
     * @autor: Miguel F. [12-07-2021]
     * @modificado:
     * @descripción: {obtener anexo8 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List< MonitoreoAbreviadoEntity>> Monitoreo (MonitoreoAbreviadoEntity param) {
        return repo.Monitoreo(param);
    }

    /**
     * @autor: Miguel F. [23-07-2021]
     * @modificado:
     * @descripción: {actualizar anexo8 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> MonitoreoActualizar (List< MonitoreoAbreviadoEntity> param) {
        return repo.MonitoreoActualizar(param);
    }

    /**
     * @autor: Miguel F. [13-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List< OrganizacionManejoEntity>> OrganizacionManejo (OrganizacionManejoEntity param) {
        return repo.OrganizacionManejo(param);
    }

    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {obtener anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<ProgramaInversionEntity> ProgramaInversionObtener(ProgramaInversionEntity param) {
        
        return repo.ProgramaInversionObtener(param);
    }

    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {registar anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> ProgramaInversionRegistrar(ProgramaInversionEntity param) {
        
        return repo.ProgramaInversionRegistrar(param);
    }

    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> ProgramaInversionEliminar(ProgramaInversionEntity param) {
        
        return repo.ProgramaInversionEliminar(param);
    }

    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadRegistrar(CronogramaActividadEntity param) {
        
        return repo.cronogramaActividadRegistrar(param);
    }

    /**
     * @autor: Miguel F. [21-07-2021]
     * @modificado:
     * @descripción: {actualizar anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadActualizar(CronogramaActividadEntity param) {
        
        return repo.cronogramaActividadActualizar(param);
    }

    /**
     * @autor: Miguel F. [22-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadEliminar(CronogramaActividadEntity param) {
        
        return repo.cronogramaActividadEliminar(param);
    }

    /**
     * @autor: Miguel F. [22-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadDetalleRegistar(CronogramaActividadDetalleEntity param) {
        
        return repo.cronogramaActividadDetalleRegistar(param);
    }

    /**
     * @autor: Miguel F. [24-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List<CronogramaActividadDetalleEntity>> cronogramaActividadDetalle_ListarDetalle(CronogramaActividadDetalleEntity param) {
        
        return repo.cronogramaActividadDetalle_ListarDetalle(param);
    }
}
