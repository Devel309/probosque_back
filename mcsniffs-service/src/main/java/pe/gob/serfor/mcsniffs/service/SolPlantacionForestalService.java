package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarSolicitudPlantacionForestal.DescargarSolicitudPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.*;

public interface SolPlantacionForestalService {
        ResultEntity<SolPlantacionForestalEntity> registroSolicitudPlantacionForestal(
                        SolPlantacionForestalEntity request);



        ResultClassEntity eliminarSolicitudPlantacionForestal(
                        SolPlantacionForestalEntity request) throws Exception;

        ResultClassEntity listarSolicitudPlantacionForestal(SolPlantacionForestalEntity request);

        ResultClassEntity obtenerSolicitudPlantacionForestal(
                SolPlantacionForestalDto request);

        ResultClassEntity clonarPlantacionForestal (SolPlantacionForestalDto request);

        ResultClassEntity validarClonPlantacionForestal(SolPlantacionForestalDto request);

        ResultClassEntity obtenerInformacionTitular(SolPlantacionForestalEntity request);

        ResultClassEntity actualizarInformacionTitular(SolPlantacionForestalEntity request);

        ResultEntity<Dropdown> listarTipoComboSistemaPlantacionForestal(SistemaPlantacionEntity request);

        ResultClassEntity listarBandejaSolicitudPlantacionForestal(
                SolPlantacionForestalDto request);

        ResultEntity<SolPlantacionForestalEstadoEntity> RegistrarSolicitudPlantacionForestalEstado(
                        SolPlantacionForestalEstadoEntity request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> registroSolicitudPlantacionForestalAreaPlantada(
                        List<SolPlantacionForestalAreaPlantadaEntity> request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> eliminarSolicitudPlantacionForestalAreaPlantada(
                        SolPlantacionForestalAreaPlantadaEntity request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> listarSolicitudPlantacionForestalAreaPlantada(
                        SolPlantacionForestalAreaPlantadaEntity request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> obtenerSolicitudPlantacionForestalAreaPlantada(
                        SolPlantacionForestalAreaPlantadaEntity request);

        ResultClassEntity registroSolPlantacionForestalArea(
                        SolPlantacionForestalAreaEntity request);

        ResultEntity<SolPlantacionForestalAreaEntity> listarSolPlantacionForestalArea(
                        SolPlantacionForestalAreaEntity request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> registroSolicitudPlantacionForestalDetCoord(
                        List<SolPlantacionForestalDetCoordenadaEntity> request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> eliminarSolicitudPlantacionForestalDetCoord(
                        SolPlantacionForestalDetCoordenadaEntity request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> listarSolicitudPlantacionForestalDetCoord(
                        SolPlantacionForestalDetCoordenadaEntity request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> obtenerSolicitudPlantacionForestalDetCoord(
                        SolPlantacionForestalDetCoordenadaEntity request);

        ResultEntity<SolPlantacionForestalDetalleEntity> registroSolicitudPlantacionForestalDet(
                        List<SolPlantacionForestalDetalleEntity> request);

        ResultEntity<SolPlantacionForestalDetalleEntity> eliminarSolicitudPlantacionForestalDet(
                        SolPlantacionForestalDetalleEntity request);

        ResultEntity<SolPlantacionForestalDetalleEntity> listarSolicitudPlantacionForestalDet(
                        SolPlantacionForestalDetalleEntity request);

        ResultEntity<SolPlantacionForestalDetalleEntity> obtenerSolicitudPlantacionForestalDet(
                        SolPlantacionForestalDetalleEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> registroSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request, MultipartFile file);

        ResultEntity<SolPlantacionForestalAnexoEntity> eliminarSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> listarSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> obtenerSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalObservacionEntity> registroSolPlantacionForestalObservacion(
                        List<SolPlantacionForestalObservacionEntity> request);

        ResultEntity<SolPlantacionForestalObservacionEntity> eliminarSolPlantacionForestalObservacion(
                        SolPlantacionForestalObservacionEntity request);

        ResultEntity<SolPlantacionForestalObservacionEntity> listarSolPlantacionForestalObservacion(
                        SolPlantacionForestalObservacionEntity request);

        ResultEntity<SolPlantacionForestalObservacionEntity> obtenerSolPlantacionForestalObservacion(
                        SolPlantacionForestalObservacionEntity request);

        ResultClassEntity obtenerArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj) throws Exception;


        ResultClassEntity registrarArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj)
                        throws Exception;

        ResultClassEntity registrarArchivosSolPlantacionForestal(List<SolPlantacionForestalArchivoEntity> items)
                        throws Exception;

        ResultClassEntity actualizarArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj)
                        throws Exception;

        ResultClassEntity eliminarArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj) throws Exception;

        ResultClassEntity registrarSolPlantacacionForestal(SolicitudPlantacionForestalEntity obj) throws Exception;

        ResultClassEntity actualizarSolicitudPlantacionForestal(SolPlantacionForestalEntity request);

        ResultClassEntity enviarSolicitud(SolPlantacionForestalDto obj) throws Exception;

        ResultClassEntity listarEspeciesSistemaPlantacion(Integer id) throws Exception;

        ResultClassEntity registrarSolPlantacacionForestalDetalle(List<SolicitudPlantacionForestalDetalleEntity> obj) throws Exception;

        ResultClassEntity notificarSolicitante(SolPlantacionForestalEntity request);

        ResultClassEntity notificarPasSolicitante(SolPlantacionForestalEntity request);

        ResultClassEntity notificarPassSerfor(SolPlantacionForestalEntity param);

        ResultClassEntity registrarAreaBloque(AreaBloqueEntity param);

        ResultClassEntity listarAreaBloque(AreaBloqueEntity param);
        
        ResultArchivoEntity descargarSolicitudPlantacionesForestales(DescargarSolicitudPlantacionForestalDto obj) throws Exception;

        ResultClassEntity generarNroRegistro(SolPlantacionForestalEntity obj)  throws Exception;

        ResultClassEntity registrarSolPlantacionForestalPersona(List<SolPlantacionForestalPersonaEntity> request) throws Exception;

        ResultClassEntity listarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request) throws Exception;

        ResultClassEntity eliminarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request) throws Exception;


}
