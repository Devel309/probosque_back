package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ConsultaEntidadEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface ConsultaEntidadService {
    ResultEntity<ConsultaEntidadEntity> listarEntidades();
}
