package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionArchivoDto;

public interface EvaluacionArchivoService {
    ResultClassEntity<List<EvaluacionArchivoDto>> listarEvaluacionArchivo(EvaluacionArchivoDto dto) throws Exception;
    ResultClassEntity registrarEvaluacionArchivo(List<EvaluacionArchivoDto> dto) throws Exception;
    ResultClassEntity eliminarEvaluacionArchivo(List<EvaluacionArchivoDto> dto) throws Exception;
}
