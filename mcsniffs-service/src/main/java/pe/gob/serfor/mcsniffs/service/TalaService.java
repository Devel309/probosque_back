package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;

public interface TalaService {
    ResultClassEntity registrarTala( List<TalaEntity> items)  throws Exception;
    ResultClassEntity registrarTalaDetalle( List<TalaDetalleEntity> items)  throws Exception;
}
