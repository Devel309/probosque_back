package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;

import java.util.List;

public interface InformacionGeneralService {
	ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity) throws Exception;

	ResultClassEntity ActualizarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity) throws Exception;

	ResultClassEntity EliminarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity) throws Exception;

	ResultClassEntity<InformacionGeneralDto> obtener(Integer idPlanManejo) throws Exception;

	ResultClassEntity<InformacionGeneralDto> guardar(InformacionGeneralDto dto) throws Exception;

	ResultEntity obtenerInformacionPlanOperativo(InformacionGeneralDEMAEntity request);

	ResultClassEntity actualizarInfGeneralResumido(List<InfGeneralEntity> list) throws Exception;

	List<InfGeneralEntity> listarInfGeneralResumido(InfGeneralEntity param) throws Exception;

}
