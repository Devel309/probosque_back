package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionResultadoDto;

public interface ResolucionService {

    ResultClassEntity ListarResolucion(ResolucionDto param);
    ResultClassEntity ObtenerResolucion(ResolucionDto param)throws Exception;;
    ResultClassEntity RegistrarResolucion(ResolucionDto param);
    ResultClassEntity ActualizarResolucion(ResolucionDto param);
    ResultClassEntity ActualizarResolucionEstado(ResolucionDto param)  throws Exception;
    ResultClassEntity registrarResolucionResultado(ResolucionResultadoDto param) throws Exception;
    ResultClassEntity resolucionValidar(Integer idProcesoOferta);
    ResultClassEntity registrarResolucionBase(ResolucionDto param);
}
