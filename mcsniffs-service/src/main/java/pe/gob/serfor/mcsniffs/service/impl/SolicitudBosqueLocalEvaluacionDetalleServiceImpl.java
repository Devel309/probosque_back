package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;

import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalEvaluacionDetalleService;

import java.util.List;

@Service("SolicitudBosqueLocalEvaluacionDetalleService")
public class SolicitudBosqueLocalEvaluacionDetalleServiceImpl implements SolicitudBosqueLocalEvaluacionDetalleService{

    @Autowired
    private SolicitudBosqueLocalEvaluacionDetalleRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarEvaluacionDetallesSolicitudBosqueLocal(List<SolicitudBosqueLocalEvaluacionDetalleDto> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (items != null && !items.isEmpty()) {
            for (SolicitudBosqueLocalEvaluacionDetalleDto item : items) {
                result = repository.registrarSolicitudBosqueLocalEvaluacionDetalle(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen detalles de la evaluación por registrar.");
        }

        return result;
    }

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj) throws Exception {
        return  repository.listarSolicitudBosqueLocalEvaluacionDetalle(obj);
    }
}
