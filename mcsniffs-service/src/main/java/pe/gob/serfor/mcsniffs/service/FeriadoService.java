package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

import java.util.List;
import java.util.Map;

public interface FeriadoService {

    ResultClassEntity RegistrarFeriados(FeriadoEntity feriadoEntity) throws Exception;

    ResultClassEntity<List<FeriadoEntity>> ListarFeriados(String codUbigeo) throws Exception;

}
