package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiltroFiscalizacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Fiscalizacion.FiscalizacionDto;
import pe.gob.serfor.mcsniffs.repository.FiscalizacionArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.FiscalizacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.FiscalizacionService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;

@Service
public class FiscalizacionServiceImpl  implements FiscalizacionService{
 
    @Autowired
    private FiscalizacionRepository repo;

    @Autowired
    private FiscalizacionArchivoRepository fiscalizacionArchivoRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;
 
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarFiscalizacion(FiscalizacionDto obj) throws Exception {

        ResultClassEntity result = repo.registrarFiscalizacion(obj);
        
        for (FiscalizacionArchivoDto element : obj.getListaFiscalizacionArchivo()) {
            element.setIdFiscalizacion(obj.getIdFiscalizacion());
            element.setIdUsuarioRegistro(obj.getIdUsuarioRegistro());
            fiscalizacionArchivoRepository.registrarFiscalizacionArchivo(element);
        }

        return result;
    }

 
    @Override
    public ResultClassEntity AdjuntarArchivosFiscalizacion(FiscalizacionEntity obj) {
        return repo.AdjuntarArchivosFiscalizacion(obj);
    }

 
    @Override
    public ResultClassEntity listarFiscalizacion(FiscalizacionDto obj) {
        return repo.listarFiscalizacion(obj);
    }

 
    @Override
    public ResultEntity<FiscalizacionAdjuntosEntity> ObtenerArchivosFiscalizacion(Integer IdFiscalizacion) {
        return repo.ObtenerArchivosFiscalizacion(IdFiscalizacion);
    }
    @Override
    public ResultClassEntity ObtenerFiscalizacion(FiscalizacionDto obj) {

        ResultClassEntity result = new ResultClassEntity();
        FiscalizacionDto response = repo.ObtenerFiscalizacion(obj);
        FiscalizacionArchivoDto fa = null;

            fa = new FiscalizacionArchivoDto();
            fa.setIdFiscalizacion(response.getIdFiscalizacion());
            response.setListaFiscalizacionArchivo(fiscalizacionArchivoRepository.listarFiscalizacionArchivo(fa));

 
        result.setSuccess(true);
        result.setData(response);


        return result;
    }

    @Override
    public ResultClassEntity eliminarFiscalizacionArchivo(FiscalizacionArchivoDto obj) throws Exception{


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVD_FISCALIZACION_ARCHIVO,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MVD_FISCALIZACION_ARCHIVO + obj.getIdArchivo(),
                obj.getIdUsuarioElimina());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);



        return fiscalizacionArchivoRepository.eliminarFiscalizacionArchivo(obj);
    }

    @Override
    public  ResultArchivoEntity DescagarPlantillaFormatoFiscalizacion(){
        return repo.DescagarPlantillaFormatoFiscalizacion();
    }
}
