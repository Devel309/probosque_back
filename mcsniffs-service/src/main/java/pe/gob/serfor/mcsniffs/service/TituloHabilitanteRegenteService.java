package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHRegenteDto;

import java.util.List;

public interface TituloHabilitanteRegenteService {


    ResultClassEntity listarInformacionTHRegente(InformacionTHRegenteDto param) throws Exception;
}
