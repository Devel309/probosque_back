package pe.gob.serfor.mcsniffs.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralPlanificacionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralPlanificacionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.RegenteRepository;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralPlanificacionBosqueService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;


/**
 * @autor: Harry Coa [03-08-2021]
 * @modificado: Service Regente
 * @descripción: {}
 *
 */
@Service("InformacionGeneralPlanificacionBosqueService")
public class InformacionGeneralPlanificacionBosqueServiceImpl implements InformacionGeneralPlanificacionBosqueService {

    private static final Logger log = LogManager.getLogger(InformacionGeneralPlanificacionBosqueServiceImpl.class);

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infPlaniRepository;

    @Autowired
    private RegenteRepository regenteRepository;

    @Override
    public ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralPlanificacionBosqueEntity param) throws Exception {

        if (param.getIdRegente()!=0) {
            regenteRepository.RegistrarRegente(null);
        }
 

        return infPlaniRepository.RegistrarInformacionGeneral(param);
    }
}
