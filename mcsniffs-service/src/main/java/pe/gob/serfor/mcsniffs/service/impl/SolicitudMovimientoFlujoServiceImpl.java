package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudMovimientoFlujoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudMovimientoFlujoRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudMovimientoFlujoService;

@Service
public class SolicitudMovimientoFlujoServiceImpl implements SolicitudMovimientoFlujoService {

     @Autowired
    private SolicitudMovimientoFlujoRepository repository;

    @Override
    public ResultClassEntity registroSolicitudMovimientoFlujo(SolicitudMovimientoFlujoEntity entity) {
        return repository.registroSolicitudMovimientoFlujo(entity);
    }
}
