package pe.gob.serfor.mcsniffs.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CensoForestal.CensoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

public interface CensoForestalDetalleService {
    ResultClassEntity RegistrarCensoForestalDetalle(CensoForestalDetalleEntity censoForestalEntity) throws Exception;
    ResultClassEntity<String> RegistrarMasivoCensoForestalDetalle(MultipartFile file, int IdUsuarioRegistro,String tipoCenso) throws Exception;
    ResultClassEntity<Map<Integer, String>> RegistrarIndividualCensoForestalDetalle(List<CensoForestalDetalleEntity> obj, Integer idUsuarioRegistro, Integer IdCensoForestal) throws Exception;
   // ResultClassEntity RegistrarIndividualCensoForestalDetalle(CensoForestalDetalleEntity censoForestalDetalleEntity, int IdUsuarioRegistro) throws Exception;
    ResultClassEntity<List<ParametroValorEntity>> ListarTipoBosque() throws Exception;
    ResultClassEntity<EspecieNombreDto> ListarEspecie() throws Exception;
    ResultClassEntity<CensoForestalDto> CensoForestalObtener() throws Exception;
    ResultClassEntity<List<ContratoTHDto>> ListaTHContratosObtener(ContratoTHDto data) throws Exception;
    ResultClassEntity<List<PlanManejoAppEntity>> ListaPlanManejoPorContratoObtener(Integer idContrato,String tipoPlanManejo,String tipoCensoForestal) throws Exception;
    ResultClassEntity<DatosTHDto> ListaDatosTHObtener(Integer idPlanDeManejo) throws Exception;
    ResultClassEntity<List<ListaEspecieDto>> ListaTipoRecursoForestal(String tipoEspecie,String tipoCenso) throws Exception;
    ResultClassEntity<List<ListaEspecieDto>> ListaTipoRecursoForestalManejo(Integer idPlanManejo,String tipoCenso,String tipoEspecie) throws Exception;
    ResultClassEntity<List<ListaEspecieDto>> ListaResumenEspecies(Integer idPlanManejo,String tipoCenso,String tipoEspecie) throws Exception;
    ResultClassEntity<List<ListaEspecieDto>> ListaResumenEspeciesInventario(Integer idPlanManejo,String tipoRecurso,String tipoInventario) throws Exception;
    ResultClassEntity<List<CensoForestalDetalleEntity>> ListarCensoForestalDetalle(Integer idPlanManejo,String tipoCenso,String tipoRecurso) throws Exception;
    ResultClassEntity<List<CensoForestalEntity>> SincronizacionCenso(Integer idPlanManejo,String tipoCenso,String tipoRecurso) throws Exception;
    ResultClassEntity<List<Anexo2CensoEntity>> ListarCensoForestalAnexo2(Integer idPlanManejo, String tipoEspecie) throws Exception;
    ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo2VolumenTotal(Integer idPlanManejo, String tipoEspecie);
    ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo3(Integer idPlanManejo, String tipoEspecie);
    ResultEntity<CensoForestalDetalleDto> listarEspecieXCenso(CensoForestalDto censoForestalDto) throws Exception;
    ResultClassEntity<ActividadesDeAprovechamientoDto> ListaDatosActividadesAprovechamiento(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<Anexo2Dto>> listaDatosPOConcesionesAnexo2(Integer idPlanDeManejo, String tipoPlan,Integer tipoProceso) throws Exception;
    ResultClassEntity<List<Anexo2PGMFDto>> ListaFormatoPGMFAnexo2(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque) throws Exception;
    ResultClassEntity<List<List<Anexo2PGMFDto>>>   ListaTablasPMFICAnexo3(Integer idPlanDeManejo, String tipoPlan)  throws Exception;
    ResultClassEntity<List<Anexo2PGMFDto>> PGMFInventarioExploracionVolumenP(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque) throws Exception;
    ResultClassEntity<List<Anexo3PGMFDto>> ListaFormatoPGMFAnexo3(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque) throws Exception;
    ResultClassEntity<List<Anexo3PGMFDto>> PGMFInventarioExploracionFustales(Integer idPlanDeManejo, String tipoPlan,String  idTipoBosque) throws Exception;
   ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> ListaResultadosEspecieMuestreo(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> ListaResultadosEspecieFrutales(Integer idPlanDeManejo, String tipoPlan) throws Exception;
 ResultClassEntity registrarFormatoPGMFAAnexo2Excel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)throws Exception;
 ResultClassEntity registrarFormatoPGMFAAnexo3Excel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan)throws Exception;
 ResultClassEntity<List<AprovechamientoRFNMDto>> ListaResultadosAprovechamientoRFNM(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity listarResultadosFustales(Integer idPlanManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>   registrarEspecieMuestreoExcel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>   registrarEspecieFrutalesExcel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan) throws Exception;

    ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> registrarRecursosForestalesMaderableExcel(MultipartFile file, Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<ResultadosAnexo6> > ListaAnexo6(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<ListaEspecieDto>> ListaEspeciesInventariadasMaderablesObtener(Integer idPlanDeManejo,String tipoPlan) throws Exception;
    ResultClassEntity<List<TablaAnexo3PGMF>>  ObtenerTablaAnexo3PGMFVolumenPotencial(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<List<TablaAnexo3PGMF>>  ObtenerTablaAnexo3PGMFFustales(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity cargaTipoPlanMasivoExcel(MultipartFile file)  throws Exception;
    ResultClassEntity<List<CensoForestalDetalleEntity>> cargaMasivaTipoPlanExcel(MultipartFile file, Integer idPlanManejo) throws Exception;
    ResultClassEntity ActualizarIndividualCensoForestalDetalle(Integer idUsuarioRegistro,Integer idCensoForestalDetalle,CensoForestalDetalleEntity obj) throws Exception;
    ResultClassEntity EliminarCensoForestalDetalle(Integer idUsuarioElimina,Integer idCensoForestalDetalle) throws Exception;
    ResultClassEntity RegistrarCensoForestalDetalle(Integer idUsuarioRegistro,Integer idPlanManejo,CensoForestalDetalleEntity censoForestalDetalleEntity) throws Exception;
    ResultClassEntity<ResultadosArbolesAprovechablesMaderables>    ListaResultadosArbolesAprovechablesMaderables(Integer idPlanDeManejo, String tipoPlan)  throws Exception;
    ResultClassEntity<ResultadosArbolesAprovechablesMaderables>    ListaResultadosAprovechableForestalNoMaderable(Integer idPlanDeManejo, String tipoPlan)  throws Exception;
    ResultClassEntity<ResultadosArbolesAprovechablesMaderables>    ListaResultadosArbolesAprovechablesFustales(Integer idPlanDeManejo, String tipoPlan)  throws Exception;
    ResultClassEntity<List<Anexo2Dto>>  ListaAnexo7NoMaderable(Integer idPlanDeManejo, String tipoPlan) throws Exception;
    ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>    ListaCensoComercialAprovechamientoMaderable(Integer idPlanDeManejo, String tipoPlan)  throws Exception;
    ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>    ListaResultadosCensoComercialAprovechamientoNoMaderable(Integer idPlanDeManejo, String tipoPlan)  throws Exception;
    ResultClassEntity<List<ContratoTHDto>> ListarContratoTHMovil(ContratoTHDto data) throws Exception;

    ResultClassEntity ListarProyeccionCosecha(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity listarVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity listarEspecieVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity listarEspecieVolumenCortaAnualPermisible(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity<List<Anexo2Dto>> BuscarAnexo7NoMaderable(Integer idPlanDeManejo, String tipoPlan, Anexo2Dto data) throws Exception;;

    ResultClassEntity<List<List<Anexo3PGMFDto>>> ListaTablasPMFICAnexo4(Integer idPlanDeManejo, String tipoPlan) throws Exception;;

    ResultClassEntity listarRecursosMaderablesCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception;

    ResultClassEntity listarRecursosDiferentesCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception;

    ResultClassEntity listarCensoForestalDetalleAnexo(CensoForestalDetalleEntity request) throws Exception;

    ResultClassEntity actualizarCensoForestal(CensoForestalEntity request) throws Exception;
    ResultClassEntity  ListaAprovechamientoForestalNMResumen(Integer idPlanManejo, String tipoPlan,Integer tipoProceso)  throws Exception;
    ResultClassEntity  ListarResultadosInventarios(Integer idPlanManejo, String tipoPlan,Integer tipoProceso) throws Exception;

    ResultClassEntity listarSincronizacionCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception;
}
