package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.OrganizacionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.OrganizacionActividadRepository;
import pe.gob.serfor.mcsniffs.service.OrganizacionActividadService;
import pe.gob.serfor.mcsniffs.service.ProteccionBosqueService;

import java.util.List;

/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad OrganizacionActividad  de la base de datos mcsniffs}
 *
 */
@Service("OrganizacionActividadService")
public class OrganizacionActividadServiceImpl implements OrganizacionActividadService {


    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.service.impl.OrganizacionActividadServiceImpl.class);

    @Autowired
    private OrganizacionActividadRepository organizacionActividadRepository;

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Configuracion ProteccionBosqueDemarcacion}
     * @param: ProteccionBosqueDemarcacionEntity
     */

    @Override
    public ResultClassEntity RegistrarOrganizacionActividad(List<OrganizacionActividadEntity> list)throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - ListarPorFiltroProteccionBosqueImpactoAmbiental",list.toString());
        try {
            response=   organizacionActividadRepository.RegistrarOrganizacionActividad(list);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -ListarPorFiltroProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }

    @Override
    public ResultClassEntity ListarPorFiltroOrganizacionActividad(OrganizacionActividadEntity param)throws Exception {
        return organizacionActividadRepository.ListarPorFiltroOrganizacionActividad(param);
    }

    @Override
    public ResultClassEntity EliminarOrganizacionActividad(OrganizacionActividadEntity param) throws Exception{
        return organizacionActividadRepository.EliminarOrganizacionActividad(param);
    }

    @Override
    public ResultClassEntity RegistrarOrganizacionActividadArchivo(MultipartFile file, PlanManejoEntity planManejo) {
        return organizacionActividadRepository.RegistrarOrganizacionActividadArchivo(file,planManejo);
    }

    @Override
    public ResultClassEntity ListarOrganizacionActividadArchivo(PlanManejoEntity planManejo) {
        return organizacionActividadRepository.ListarOrganizacionActividadArchivo(planManejo);
    }


    @Override
    public ResultEntity ListarPorFiltroOrganizacionActividadArchivo(PlanManejoEntity planManejo) {
        return organizacionActividadRepository.ListarPorFiltroOrganizacionActividadArchivo(planManejo);
    }
 
}
