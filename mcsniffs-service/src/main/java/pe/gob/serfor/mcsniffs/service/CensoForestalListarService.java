package pe.gob.serfor.mcsniffs.service;
import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Parametro.ContratoTHDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.CensoForestalListarDto;

public interface CensoForestalListarService {

    ResultClassEntity<List<CensoForestalListarDto>> CensoForestalListar(CensoForestalListarDto dto) throws Exception;
    ResultClassEntity<List<ContratoTHDto>> listaTHContratoPorFiltro(ContratoTHDto data) throws Exception;
}
