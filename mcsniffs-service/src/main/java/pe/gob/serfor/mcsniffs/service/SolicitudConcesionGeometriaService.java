package pe.gob.serfor.mcsniffs.service;


import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionGeometriaEntity;

import java.util.List;

public interface SolicitudConcesionGeometriaService {

    ResultClassEntity registrarSolicitudConcesionGeometria( List<SolicitudConcesionGeometriaEntity> items)  throws Exception ;
    ResultClassEntity listarSolicitudConcesionGeometria (SolicitudConcesionGeometriaEntity item);
    ResultClassEntity obtenerInformacionAreaGeometriaSolicitada(SolicitudConcesionGeometriaEntity item);
    ResultClassEntity registrarInformacionAreaGeometriaSolicitada(List<SolicitudConcesionGeometriaEntity> items) throws Exception;
    ResultClassEntity actualizarInformacionAreaGeometriaSolicitada(List<SolicitudConcesionGeometriaEntity> items) throws Exception;
    ResultClassEntity eliminarSolicitudConcesionGeometriaArchivo (SolicitudConcesionGeometriaEntity item);
    ResultClassEntity eliminarInformacionAreaGeometriaSolicitada(SolicitudConcesionGeometriaEntity item);


}
