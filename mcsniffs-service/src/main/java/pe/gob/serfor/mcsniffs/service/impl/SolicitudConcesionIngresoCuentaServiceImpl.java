package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionAreaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionIngresoCuentaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionIngresoCuentaService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;

import java.util.List;

@Service
public class SolicitudConcesionIngresoCuentaServiceImpl implements SolicitudConcesionIngresoCuentaService {

    @Autowired
    private SolicitudConcesionIngresoCuentaRepository solicitudConcesionIngresoCuentaRepository;


    @Override
    public ResultClassEntity listarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) throws Exception{
        return solicitudConcesionIngresoCuentaRepository.listarSolicitudConcesionIngresoCuenta(obj);

    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionIngresoCuenta(List<SolicitudConcesionIngresoCuentaDto> lsObj) throws Exception{

        ResultClassEntity result = new ResultClassEntity();

        for(SolicitudConcesionIngresoCuentaDto obj: lsObj){
            solicitudConcesionIngresoCuentaRepository.registrarSolicitudConcesionIngresoCuenta(obj);
        }

        result.setSuccess(true);
        result.setMessage("Se resgistró el Ingreso correctamente.");

        return result;
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) throws Exception{
        return solicitudConcesionIngresoCuentaRepository.eliminarSolicitudConcesionIngresoCuenta(obj);
    }

}
