package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.FileServerRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudImpugnacionRepository;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.SolicitudImpugnacionService;
import pe.gob.serfor.mcsniffs.entity.*;

@Service
public class SolicitudImpugnacionServiceImpl implements SolicitudImpugnacionService {

    @Autowired
    private SolicitudImpugnacionRepository repo;
    @Autowired
    private EmailService email;
    @Autowired
    private ArchivoRepository archivoRepository;
    @Autowired
    private FileServerRepository fileServer;

    private final String CONST_EMAIL_ARFFS = "miguel.fernandez@zeit.com.pe";

    @Override
    public ResultClassEntity<List<Solicitud2Entity>> listarSolicitudesVencidas(){
        return repo.listarSolicitudesVencidas();
    }

    @Override
    public ResultClassEntity<Boolean> SolicitudImpugnacionActualizar(SolicitudImpugnacionEntity params){
        return repo.SolicitudImpugnacionActualizar(params);
    }

    @Override
    public ResultClassEntity<List<SolicitudImpugnacionEntity>> ListaSolicitudesVencidasObtener() throws Exception {
        return repo.ListaSolicitudesVencidasObtener();
    }

    private String correoARFFObtener() {
        return this.CONST_EMAIL_ARFFS;
    }

    @Override
    public ResultClassEntity SolicitudImpugnacionObtener(SolicitudImpugnacionEntity solicitudImpugnacionEntity)
            throws Exception {

        return repo.SolicitudImpugnacionObtener(solicitudImpugnacionEntity);
    }

    @Override
    public ResultClassEntity<Boolean> SolicitudImpugnacionEnviarCorreo(SolicitudImpugnacionEntity params) throws Exception {

        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        this.SolicitudImpugnacionRegistrarActualizar(params);

        EmailEntity mail = new EmailEntity();

        String[] mails =new String[0];
        mails[0]=this.correoARFFObtener();
        mail.setEmail(mails);
        mail.setContent("Datos a enviar");
        mail.setSubject("Solicitud de impugnación");

        Boolean enviarmail = email.sendEmail(mail);
        result.setInformacion(enviarmail ? "Se envio el correo correctamente" : "Ocurrió un error el enviar el correo");

        return result;
    }

    @Override
    public ResultClassEntity<Boolean> SolicitudImpugnacionRegistrarActualizar(SolicitudImpugnacionEntity params)  {
        //if (file != null) 
            // String nombreArchivoGenerado = fileServer.insertarArchivoSolicitud(file);
            // ArchivoEntity archivoEntity = new ArchivoEntity();
            // archivoEntity.setEstado("A");
            // archivoEntity.setRuta("");
            // archivoEntity.setExtension(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),
            //         file.getOriginalFilename().length()));
            // archivoEntity.setNombre(file.getOriginalFilename());
            // archivoEntity.setNombreGenerado(
            //         (!nombreArchivoGenerado.equals("") ? nombreArchivoGenerado : file.getOriginalFilename()));
            // // archivoEntity.setIdArchivo(id_archivo);
            // archivoEntity.setIdUsuarioRegistro(params.getIdUsuarioRegistro());
            // archivoEntity.setIdUsuarioModificacion(params.getIdUsuarioRegistro());
            // archivoEntity.setTipoDocumento("");
            // ResultEntity<ArchivoEntity> result = archivoRepository.registroArchivo(archivoEntity);
            //params.setIdDocumentoAdjunto(result.getCodigo());        

        return repo.SolicitudImpugnacionRegistrarActualizar(params);
    }

    @Override
    public ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> listarArchivoSolicitudImpugnacion(
            SolicitudImpugnacionArchivo2Entity param) {
        return repo.listarArchivoSolicitudImpugnacion(param);
    }

    @Override
    public ResultClassEntity<List<SolicitudImpugnacionEntity>> solicitudImpugnacionListar(
            SolicitudImpugnacionEntity param) {
        
        return repo.solicitudImpugnacionListar(param);
    }

}
