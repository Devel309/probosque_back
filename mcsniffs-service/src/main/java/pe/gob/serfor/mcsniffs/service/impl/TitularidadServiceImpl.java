package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaInfractorDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaTitularidadDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultarRegenteDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.TitularidadDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.client.FeignPideApi;
import pe.gob.serfor.mcsniffs.service.util.DocUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


@Service("TitularidadService")
public class TitularidadServiceImpl implements TitularidadService {

    private static final Logger log = LogManager.getLogger(TitularidadServiceImpl.class);



    @Autowired 
    private FeignPideApi feignPideApi;


    public ResultClassEntity consultarTitularidad(String tipoParticipante,String apellidoPaterno,String apellidoMaterno,String nombres,String razonSocial, String token) throws Exception {
       

        TitularidadDto dto = new TitularidadDto();
        dto.setTipoParticipante(tipoParticipante);
        dto.setRazonSocial(razonSocial);
        ConsultaTitularidadDto consulTitu = feignPideApi.consultarTitularidad(token, dto);
        ConsultaTitularidadDto.Titularidad titularidad=new ConsultaTitularidadDto.Titularidad();
        if (consulTitu!=null && consulTitu.getDataService() != null){
            for (ConsultaTitularidadDto.Titularidad i : consulTitu.getDataService()) {

                titularidad.setLibro(i.getLibro());
                titularidad.setTipoDocumento(i.getTipoDocumento());
                titularidad.setZona(i.getZona());
                titularidad.setRazonSocial(i.getRazonSocial());
                titularidad.setEstado(i.getEstado());
                titularidad.setOficina(i.getOficina());
                titularidad.setDireccion(i.getDireccion());
                titularidad.setNumeroDocumento(i.getNumeroDocumento());
                titularidad.setRegistro(i.getRegistro());
                titularidad.setNumeroPartida(i.getNumeroPartida());

            }
        }
        
        ResultClassEntity result = new ResultClassEntity();
        result.setSuccess(true);
        result.setData(titularidad);

        return result;
    }


}
