package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad ManejoBosque  de la base de datos mcsniffs}
 *
 */
@Service("ManejoBosqueService")
public class ManejoBosqueServiceImpl implements ManejoBosqueService {


    private static final Logger log = LogManager.getLogger(ManejoBosqueServiceImpl.class);

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Override
    public ResultClassEntity RegistrarManejoBosque(ManejoBosqEntity manejoBosque) throws Exception {
        return manejoBosqueRepository.RegistrarManejoBosque(manejoBosque);
    }

    @Override
    public ResultClassEntity ObtenerManejoBosque(ManejoBosqEntity manejoBosque) throws Exception {
        return manejoBosqueRepository.ObtenerManejoBosque(manejoBosque);
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {Listado de cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueDto
     */

    @Override
    public ResultClassEntity<List<ManejoBosqueEntity>> ListarManejoBosque(Integer idPlanManejo, String codigo,String subCodigoGeneral,String subCodigoGeneralDet)
            throws Exception {

        ResultClassEntity<List<ManejoBosqueEntity>> result = new ResultClassEntity<>();

        result.setData(manejoBosqueRepository.ListarManejoBosque(idPlanManejo,codigo,subCodigoGeneral,subCodigoGeneralDet));
        result.setSuccess(true);
        result.setMessage("Lista Manejo del Bosque");

        return result;
    }

    @Override
    public ResultClassEntity<List<ManejoBosqueEntity>> ListarManejoBosqueTitular (String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso)
            throws Exception {

        ResultClassEntity<List<ManejoBosqueEntity>> result = new ResultClassEntity<>();

        result.setData(manejoBosqueRepository.ListarManejoBosqueTitular(tipoDocumento,nroDocumento,codigoProcesoTitular,subcodigoProcesoTitular,idPlanManejo,codigoProceso,subCodigoProceso));
        result.setSuccess(true);
        result.setMessage("Lista Manejo del Bosque");

        return result;
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {Registra y actualizar la cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueEntity
     */

    @Override
    public ResultClassEntity RegistrarManejoBosque(List<ManejoBosqueEntity> list) throws Exception {
        return manejoBosqueRepository.RegistrarManejoBosque(list);
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {REliminar la cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueDto
     */

    @Override
    public ResultClassEntity EliminarManejoBosque(ManejoBosqueDto manejoBosqueDto) throws Exception {
        return  manejoBosqueRepository.EliminarManejoBosque(manejoBosqueDto);
    }

    /**
     * @autor:  Danny Nazario [28-12-2021]
     * @modificado:
     * @descripción: {Registra Manejo Bosque Especies a Aprovechar Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarManejoBosqueExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna, String codigoManejo, String subCodigoManejo, String codtipoManejoDet, int idPlanManejo, int idManejoBosque, int idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        List<ManejoBosqueEntity> list = new ArrayList<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);

            ManejoBosqueEntity cab = new ManejoBosqueEntity();
            cab.setCodigoManejo(codigoManejo);
            cab.setSubCodigoManejo(subCodigoManejo);
            cab.setIdManejoBosque(idManejoBosque);
            cab.setIdPlanManejo(idPlanManejo);
            cab.setIdUsuarioRegistro(idUsuarioRegistro);

            List<ManejoBosqueDetalleEntity> listDet = new ArrayList<>();
            ManejoBosqueDetalleEntity det = new ManejoBosqueDetalleEntity();
            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                det = new ManejoBosqueDetalleEntity();
                det.setIdManejoBosqueDet(0);
                det.setCodtipoManejoDet(codtipoManejoDet);
                det.setEspecie(row.get(0));
                det.setLineaProduccion(row.get(1));
                det.setNuDiametro(BigDecimal.valueOf(Double.parseDouble(row.get(2))));
                listDet.add(det);
            }
            cab.setListManejoBosqueDetalle(listDet);
            list.add(cab);
            manejoBosqueRepository.RegistrarManejoBosque(list);
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }
}
