package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.TituloHabilitanteRepository;
import pe.gob.serfor.mcsniffs.service.MonitoreoService;
import pe.gob.serfor.mcsniffs.service.TituloHabilitanteService;

import java.util.List;

/**
 * @autor: Rafael Azaña [12-02-2022]
 * @modificado:
 *
 */
@Service("TituloHabilitanteService")
public class TituloHabilitanteImpl implements TituloHabilitanteService {


    private static final Logger log = LogManager.getLogger(ProteccionBosqueServiceImpl.class);

    @Autowired
    private TituloHabilitanteRepository tituloHabilitanteRepository;

    @Override
    public ResultClassEntity registrarTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception {

        return tituloHabilitanteRepository.registrarTituloHabilitante(list);
    }

    @Override
    public ResultClassEntity actualizarTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception {

        return tituloHabilitanteRepository.actualizarTituloHabilitante(list);
    }

    @Override
    public ResultClassEntity actualizarCodigoCifradoTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception {

        return tituloHabilitanteRepository.actualizarCodigoCifradoTituloHabilitante(list);
    }

    @Override
    public List<TituloHabilitanteEntity> listarTituloHabilitante(TituloHabilitanteEntity param) throws Exception {
        return tituloHabilitanteRepository.listarTituloHabilitante(param);
    }


    @Override
    public ResultClassEntity eliminarTituloHabilitante(TituloHabilitanteEntity param) throws Exception {
        return  tituloHabilitanteRepository.eliminarTituloHabilitante(param);
    }

    @Override
    public ResultClassEntity listarInformacionTH(InformacionTHDto param) throws Exception {
        return  tituloHabilitanteRepository.listarInformacionTH(param);
    }

}

