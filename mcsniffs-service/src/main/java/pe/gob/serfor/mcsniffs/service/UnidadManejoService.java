package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoEntity;

public interface UnidadManejoService {

    ResultClassEntity RegistrarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception;
    ResultClassEntity ActualizarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception;
    ResultClassEntity ListarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception;

}
