package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoInfraccionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoInfraccionRepository;
import pe.gob.serfor.mcsniffs.service.EvaluacionCampoInfraccionService;

import java.util.List;


@Service("EvaluacionCampoInfraccionService")
public class EvaluacionCampoInfraccionServiceImpl implements EvaluacionCampoInfraccionService {


    private static final Logger log = LogManager.getLogger(EvaluacionCampoInfraccionServiceImpl.class);

    @Autowired
    private EvaluacionCampoInfraccionRepository evaluacionCampoInfraccionRepository;

    @Override
    public ResultClassEntity ObtenerEvaluacionCampoInfraccion(EvaluacionCampoInfraccionDto param) {
        return evaluacionCampoInfraccionRepository.ObtenerEvaluacionCampoInfraccion(param);
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionCampoInfraccion(List<EvaluacionCampoInfraccionDto> list){
        return evaluacionCampoInfraccionRepository.RegistrarEvaluacionCampoInfraccion(list);
    }

}
