package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoBiologicoFaunaSilvestrePgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActEconomicaPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActividadesPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaConfictosPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaInfraestructuraPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaPgmf;
import pe.gob.serfor.mcsniffs.repository.InfoEconomicaPgmfRepository;
import pe.gob.serfor.mcsniffs.service.InfoEconomicaPgmfService;

@Service
public class InfoEconomicaPgmfServiceImpl implements InfoEconomicaPgmfService {

@Autowired
InfoEconomicaPgmfRepository repo;

@Override
public ResultEntity<InfoEconomicaPgmf> registroInfoEconomicaPgmf(InfoEconomicaPgmf request) {
    // TODO Auto-generated method stub
    return repo.registroInfoEconomicaPgmf(request);
}

@Override
public ResultClassEntity obtenerInfoEconomicaPgmf(InfoEconomicaPgmf request) {
    // TODO Auto-generated method stub
    return repo.obtenerInfoEconomicaPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaActEconomicaPgmf> registroInfoEconomicaActEconomicaPgmf(
        List<InfoEconomicaActEconomicaPgmf> request) {
    // TODO Auto-generated method stub
    return repo.registroInfoEconomicaActEconomicaPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaActEconomicaPgmf> obtenerInfoEconomicaActEconomicaPgmf(
        InfoEconomicaActEconomicaPgmf request) {
    // TODO Auto-generated method stub
    return repo.obtenerInfoEconomicaActEconomicaPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaInfraestructuraPgmf> registroInfoEconomicaInfraestructuraPgmf(
        List<InfoEconomicaInfraestructuraPgmf> request) {
    // TODO Auto-generated method stub
    return repo.registroInfoEconomicaInfraestructuraPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaInfraestructuraPgmf> obtenerInfoEconomicaInfraestructuraPgmf(
        InfoEconomicaInfraestructuraPgmf request) {
    // TODO Auto-generated method stub
    return repo.obtenerInfoEconomicaInfraestructuraPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaActividadesPgmf> registroInfoEconomicaActividadesPgmf(
        List<InfoEconomicaActividadesPgmf> request) {
    // TODO Auto-generated method stub
    return repo.registroInfoEconomicaActividadesPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaActividadesPgmf> obtenerInfoEconomicaActividadesPgmf(
        InfoEconomicaActividadesPgmf request) {
    // TODO Auto-generated method stub
    return repo.obtenerInfoEconomicaActividadesPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaConfictosPgmf> registroInfoEconomicaConfictosPgmf(List<InfoEconomicaConfictosPgmf> request) {
    // TODO Auto-generated method stub
    return repo.registroInfoEconomicaConfictosPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaConfictosPgmf> obtenerInfoEconomicaConfictosPgmf(InfoEconomicaConfictosPgmf request) {
    // TODO Auto-generated method stub
    return repo.obtenerInfoEconomicaConfictosPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaConfictosPgmf> EliminarInfoEconomicaConfictosPgmf(InfoEconomicaConfictosPgmf request) {
    // TODO Auto-generated method stub
    return repo.EliminarInfoEconomicaConfictosPgmf(request);
}




@Override
public ResultEntity<InfoBiologicoFaunaSilvestrePgmf> registroInfoBiologicoFaunaSilvestrePgmf(
        List<InfoBiologicoFaunaSilvestrePgmf> request) {
    // TODO Auto-generated method stub
    return repo.registroInfoBiologicoFaunaSilvestrePgmf(request);
}    

@Override
public ResultEntity<InfoBiologicoFaunaSilvestrePgmf> obtenerInfoBiologicoFaunaSilvestrePgmf(
        InfoBiologicoFaunaSilvestrePgmf request) {
    // TODO Auto-generated method stub
    return repo.obtenerInfoBiologicoFaunaSilvestrePgmf(request);
}

@Override
public ResultEntity<InfoBiologicoFaunaSilvestrePgmf> EliminarBiologicoFaunaSilvestrePgmf(
        InfoBiologicoFaunaSilvestrePgmf request) {
    // TODO Auto-generated method stub
    return repo.EliminarBiologicoFaunaSilvestrePgmf(request);
}

@Override
public ResultEntity<InfoEconomicaActEconomicaPgmf> EliminarInfoEconomicaActEconomicaPgmf(
        InfoEconomicaActEconomicaPgmf request) {
    // TODO Auto-generated method stub
    return repo.EliminarInfoEconomicaActEconomicaPgmf(request);
}

@Override
public ResultEntity<InfoEconomicaInfraestructuraPgmf> EliminarInfoEconomicaInfraestructuraPgmf(
        InfoEconomicaInfraestructuraPgmf request) {
    // TODO Auto-generated method stub
    return repo.EliminarInfoEconomicaInfraestructuraPgmf(request);
}


    
}
