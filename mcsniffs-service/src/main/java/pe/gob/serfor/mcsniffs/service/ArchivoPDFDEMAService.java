package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFDEMAService {
    ByteArrayResource consolidadoDEMA_PDF(Integer idPlanManejo) throws Exception;
}
