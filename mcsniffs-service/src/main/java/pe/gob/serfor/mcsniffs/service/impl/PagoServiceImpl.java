package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;
import pe.gob.serfor.mcsniffs.repository.PagoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudSANRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.PagoService;
import pe.gob.serfor.mcsniffs.service.SolicitudSANService;

import java.util.List;


@Service("PagoService")
public class PagoServiceImpl implements PagoService {


    private static final Logger log = LogManager.getLogger(PagoServiceImpl.class);

    @Autowired
    private PagoRepository pagoRespository;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Override
    public ResultClassEntity RegistrarPago(List<PagoEntity> list) throws Exception {

        return pagoRespository.RegistrarPago(list);
    }

    /*
    @Override
    public List<PagoEntity> ListarPago(PagoEntity param) throws Exception {
        return pagoRespository.ListarPago(param);
    }*/
    @Override
    public ResultEntity ListarPago(PagoEntity request) {

        return pagoRespository.ListarPago(request);
    }


    @Override
    public ResultClassEntity EliminarPago(PagoPeriodoEntity param) throws Exception {

        return  pagoRespository.EliminarPago(param);
    }

    @Override
    public ResultClassEntity RegistrarDescuento(List<DescuentoEntity> list) throws Exception {

        return pagoRespository.RegistrarDescuento(list);
    }


    @Override
    public List<DescuentoEntity> ListarDescuento(DescuentoEntity param) throws Exception {
        return pagoRespository.ListarDescuento(param);
    }


    @Override
    public ResultClassEntity EliminarDescuento(DescuentoEntity param) throws Exception {

        return  pagoRespository.EliminarDescuento(param);
    }
    /*
        @Override
        public List<THEntity> listarTH(THEntity param) throws Exception {
            return pagoRespository.listarTH(param);
        }
    */
    @Override
    public ResultEntity listarTH(THEntity request) {

        return pagoRespository.listarTH(request);
    }

    @Override
    public ResultClassEntity RegistrarArchivo(List<PagoArchivoEntity> list) throws Exception {
        return pagoRespository.RegistrarArchivo(list);
    }

    @Override
    public List<PagoArchivoEntity> ListarArchivo(PagoArchivoEntity param) throws Exception {
        return pagoRespository.ListarArchivo(param);
    }


    @Override
    public ResultClassEntity EliminarArchivo(PagoArchivoEntity param) throws Exception {
        return  pagoRespository.EliminarArchivo(param);
    }

    @Override
    public ResultEntity listarPagoCronogramaAnios(PagoCronogramaEntity request) {

        return pagoRespository.listarPagoCronogramaAnios(request);
    }

    @Override
    public ResultClassEntity RegistrarGuiaPago(List<PagoGuiaEntity> list) throws Exception {

        return pagoRespository.RegistrarGuiaPago(list);
    }

    @Override
    public ResultEntity ListarGuiaPago(PagoGuiaEntity request) {

        return pagoRespository.ListarGuiaPago(request);
    }

    @Override
    public List<PagoEntity> listarModalidad(PagoEntity param) throws Exception {
        return pagoRespository.listarModalidad(param);
    }

    @Override
    public List<THEntity> listarOperativosPorPGM(THEntity param) throws Exception {
        return pagoRespository.listarOperativosPorPGM(param);
    }

}
