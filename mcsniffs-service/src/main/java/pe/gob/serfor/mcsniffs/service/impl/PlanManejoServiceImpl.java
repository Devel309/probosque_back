package pe.gob.serfor.mcsniffs.service.impl;


import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.commons.lang3.SerializationUtils;


import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.docx4j.Docx4J;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.NotificacionDTO;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContrato;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContratoDetalle;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.DescargarContratoPersonaJuridicaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.ActividadAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.ActividadSilviculturalRepository;
import pe.gob.serfor.mcsniffs.repository.AprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.CoreCentralRepository;
import pe.gob.serfor.mcsniffs.repository.CronogramaActividesRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoRespository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaUbigeoRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralDemaRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralPlanificacionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.ObjetivoManejoRepository;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoProteccionRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFAbreviadoRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ParticipacionComunalRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoContratoRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionIterRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoRepository;
import pe.gob.serfor.mcsniffs.repository.PotencialProduccionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.ProteccionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.RecursoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.RentabilidadManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.ResumenActividadPoRepository;
import pe.gob.serfor.mcsniffs.repository.SistemaManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudAccesoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudOpinionRepository;
import pe.gob.serfor.mcsniffs.repository.UnidadFisiograficaRepository;
import pe.gob.serfor.mcsniffs.repository.ZonificacionRepository;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.util.DocUtil;
import pe.gob.serfor.mcsniffs.service.util.DocUtilPermisos;
import pe.gob.serfor.mcsniffs.service.util.DocUtilPmfic;
import pe.gob.serfor.mcsniffs.service.util.DocUtilPopac;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;
//Librerías POI XWPFConverterException
import org.apache.poi.xwpf.*;
//import org.apache.poi.xwpf.converter.pdf.PdfConverter;
//import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import javax.print.Doc;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service("PlanManejoService")
public class PlanManejoServiceImpl implements PlanManejoService {

    @Autowired
    private PlanManejoRepository planManejoRepository;

    @Autowired
    private PlanManejoContratoRepository planManejoContratoRepository;

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private SistemaManejoForestalRepository smfRepo;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;

    @Autowired
    private SistemaManejoForestalRepository sistema;

    @Autowired
    private ImpactoAmbientalService ambiental;

    @Autowired
    CensoForestalRepository censoForestalDetalleRepo;

    @Autowired
    ZonificacionRepository zonificacionRepository;

    @Autowired
    CoreCentralRepository corecentral;

    @Autowired
    SolicitudAccesoRepository acceso;

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

    @Autowired
    private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ArchivoRepository archivoRepository;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;


    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private SistemaManejoForestalService sistemaManejoForestalService;


    @Override
    public ResultClassEntity<PlanManejoEntity> ListarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        return planManejoRepository.ListarPlanManejo(planManejoEntity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity<PlanManejoEntity> RegistrarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {

        ResultClassEntity<PlanManejoEntity> result = planManejoRepository.RegistrarPlanManejo(planManejoEntity);
        if (result.getSuccess()) {
            if (planManejoEntity.getListaPlanManejoContrato() != null) {
                for (PlanManejoContratoEntity element : planManejoEntity.getListaPlanManejoContrato()) {
                    element.setIdPlanManejo(planManejoEntity.getIdPlanManejo());
                    element.setIdUsuarioRegistro(planManejoEntity.getIdUsuarioRegistro());
                    result = planManejoContratoRepository.RegistrarPlanManejoContrato(element);
                }
            }

            if (planManejoEntity.getInfoGeneral() != null) {
                planManejoEntity.getInfoGeneral().setIdPlanManejo(planManejoEntity.getIdPlanManejo());
                //infoGeneralPlanRepo.RegistrarInformacionGeneral(planManejoEntity.getInfoGeneral());
                if (planManejoEntity.getInfoGeneral().getConstanciaRepLegal() != null
                        && planManejoEntity.getInfoGeneral().getConstanciaRepLegal().getIdArchivo() != null) {
                    planManejoEntity.getInfoGeneral().getConstanciaRepLegal().setIdPlanManejo(planManejoEntity.getIdPlanManejo());
                    pgmfArchivoRepo.registrarPlanManejoArchivo(planManejoEntity.getInfoGeneral().getConstanciaRepLegal());
                }
            }
            result.setData(planManejoEntity);
            result.setMessage("Se registró el plan de manejo " + planManejoEntity.getIdPlanManejo().toString() + " correctamente");
        }
        return result;
    }

    @Override
    public ResultClassEntity ActualizarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        return planManejoRepository.ActualizarPlanManejo(planManejoEntity);
    }

    @Override
    @Transactional
    public ResultClassEntity EliminarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {

        planManejoRepository.EliminarPlanManejo(planManejoEntity);

        ResultClassEntity result = null;

        if (planManejoEntity.getListaPlanManejoContrato() != null
                && !planManejoEntity.getListaPlanManejoContrato().isEmpty()) {

            for (PlanManejoContratoEntity element : planManejoEntity.getListaPlanManejoContrato()) {
                element.setIdUsuarioElimina(planManejoEntity.getIdUsuarioElimina());
                result = planManejoContratoRepository.EliminarPlanManejoContrato(element);
            }
        }
        return result;
    }

    @Override
    public Pageable<List<PlanManejoEntity>> filtrar(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
                                                    Integer idContrato, String dniElaborador, String rucComunidad, String nombreTitular, String codigoEstado, Page page) throws Exception {

        return planManejoRepository.filtrar(idPlanManejo, idTipoProceso, idTipoPlan, idContrato, dniElaborador,
                rucComunidad, nombreTitular, codigoEstado, page);
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {Guardar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @Override
    public ResultClassEntity registrarEstadoPlanManejo(List<PlanManejoEstadoEntity> obj) {

        ResultClassEntity result = new ResultClassEntity();

        for (PlanManejoEstadoEntity plan : obj) {
            if (plan.getIdPlanManejoEstado() == null) {
                planManejoRepository.registrarEstadoPlanManejo(plan);
            } else {
                planManejoRepository.actualizarEstadoPlanManejo(plan);
            }
        }
        result.setData(obj);
        result.setSuccess(true);
        result.setMessage("Se registró los estados del Plan de Manejo correctamente.");
        return result;
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {actualizar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @Override
    public ResultClassEntity actualizarEstadoPlanManejo(PlanManejoEstadoEntity obj) {
        return planManejoRepository.actualizarEstadoPlanManejo(obj);
    }

    /**
     * @autor: Jason Retamozo [30-03-2022]
     * @modificado:
     * @descripción: {listar informacion del plan de manejo de un titular ingresando nro y tipo de documento}
     * @param:PlanManejoInformacionEntity
     */
    @Override
    public ResultEntity<PlanManejoEntity> obtenerPlanesManejoTitular(String nroDocumento, String tipoDocumento) throws Exception{
        return planManejoRepository.obtenerPlanesManejoTitular(nroDocumento,tipoDocumento);
    }

    /**
     * @autor: Jason Retamozo [17-03-2022]
     * @modificado:
     * @descripción: {listar informacion del plan de manejo}
     * @param:PlanManejoInformacionEntity
     */
    @Override
    public ResultEntity<PlanManejoInformacionEntity> obtenerInformacionPlan(Integer idPlanManejo,String tipoPlan) throws Exception{
        return planManejoRepository.obtenerInformacionPlan(idPlanManejo,tipoPlan);
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {listar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @Override
    public ResultEntity<PlanManejoEstadoEntity> listarEstadoPlanManejo(PlanManejoEstadoEntity filtro) {
        return planManejoRepository.listarEstadoPlanManejo(filtro);
    }

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado DEMA}
     * @param: Integer idPlanManejo
     */
    @Override
    public ByteArrayResource consolidado(Integer idPlanManejo) throws Exception {

        XWPFDocument doc = getDoc("dema-consolidado.docx");
        //1 - informacion general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("DEMA");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        Map<String, String> keys = MapBuilder.keysInfoGeneral(o);
        // 2 - 3 - 4
        generarZonificacion(idPlanManejo, doc); //2
        generarRecursoForestalMadderablePGMF(idPlanManejo, doc);//3
        generarResumenRecursoForestalMadderablePGMF(idPlanManejo, doc);//3
        generarRecursoForestalNoMadderablePGMF(idPlanManejo, doc);//4
        generarResumenRecursoForestalNoMadderablePGMF(idPlanManejo, doc);//4

        //5 y 6
        setearMedidasDeProteccionUnidadManejoForestal(idPlanManejo, doc);
        setearTabSistemaManejoLaboresSiviculturales(idPlanManejo, doc);
        //7 -  actividades y equipos a  utilizae
        ResultClassEntity<SistemaManejoForestalDto> lstSistema = sistema.obtener(idPlanManejo, "AAE");
        DocUtil.procesarActividadesEquipos(lstSistema.getData().getDetalle(), doc);
        //8 - Impactod ambientales dengativos
        ImpactoAmbientalEntity impactoAmbientalEntity = new ImpactoAmbientalEntity();
        impactoAmbientalEntity.setIdPlanManejo(idPlanManejo);
        ResultClassEntity response = ambiental.ListarImpactoAmbiental(impactoAmbientalEntity);
        List<ImpactoAmbientalDto> impactoAmbientalEntityList = (List<ImpactoAmbientalDto>) response.getData();
        DocUtil.procesarImpactosAmbientales(impactoAmbientalEntityList, doc);
        // 9 - cronograma
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("DEMA");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        ResultEntity<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request);
        DocUtil.procesarCronograma(lstcrono.getData(), doc);
        // 10 - anexo 2
        List<Anexo2CensoEntity> lstLista = censoForestalDetalleRepo.ListarCensoForestalAnexo2(idPlanManejo, "1");
        DocUtil.generarAnexo2(lstLista, doc);
        // 11 - anexo 3 
        ResultEntity<Anexo3CensoEntity> lstLista3 = censoForestalDetalleRepo.ListarCensoForestalAnexo3(idPlanManejo, "2");
        if(lstLista3!=null)
            DocUtil.generarAnexo3(lstLista3.getData(), doc);
        //Final
        DocUtil.processFile(keys, doc, o.getLstUmf());
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    @Override
    public ResultClassEntity ListarPorFiltroPlanManejoArchivo(PlanManejoArchivoEntity planManejoEntity) {
        return planManejoRepository.ListarPorFiltroPlanManejoArchivo(planManejoEntity);
    }
    //organizar la data de cronograma

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Obtiene un archivo word}
     */
    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    public void setearMedidasDeProteccionUnidadManejoForestal(Integer idPlanManejo, XWPFDocument doc) {
        try {
            SistemaManejoForestalDto list = (SistemaManejoForestalDto) smfRepo.obtener(idPlanManejo, "MPUMF").getData();
            List<SistemaManejoForestalDetalleEntity> listaDetalle = list.getDetalle();

            XWPFTable table = doc.getTables().get(8);
            XWPFTableRow row2 = table.getRow(1);
            for (SistemaManejoForestalDetalleEntity detalleEntity : listaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("6.Medidas")) {
                                        text5 = text5.replace("6.Medidas", detalleEntity.getActividades());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("6.Descripcion")) {
                                        text5 = text5.replace("6.Descripcion", detalleEntity.getDescripcionSistema());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            /*ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);*/
            //doc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //tab5
    public void setearTabSistemaManejoLaboresSiviculturales(Integer idPlanManejo, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(7);
            List<ActividadSilviculturalDetalleEntity> list = ((ActividadSilviculturalDto) actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo, "4").getData()).getDetalle();
            List<ActividadSilviculturalDetalleEntity> obligatorias = getObligatoriasOpcionales(list, "1");
            System.out.println(obligatorias.size());

            List<ActividadSilviculturalDetalleEntity> opcionales = getObligatoriasOpcionales(list, "2");
            System.out.println(opcionales.size());

            XWPFTableRow row2 = table.getRow(3);
            int i = 0;
            for (ActividadSilviculturalDetalleEntity detalleEntity : obligatorias) {

                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("5ObligatoriasActividades")) {
                                        text5 = text5.replace("5ObligatoriasActividades", detalleEntity.getActividad());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5ObligatoriasDescripcion")) {
                                        text5 = text5.replace("5ObligatoriasDescripcion", detalleEntity.getDescripcionDetalle());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5ObligatoriasEquipos")) {
                                        text5 = text5.replace("5ObligatoriasEquipos", detalleEntity.getEquipo());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5ObligatoriasInsumos")) {
                                        text5 = text5.replace("5ObligatoriasInsumos", detalleEntity.getInsumo());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5ObligatoriasPersonal")) {
                                        text5 = text5.replace("5ObligatoriasPersonal", detalleEntity.getPersonal());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5ObligatoriasObservaciones")) {
                                        text5 = text5.replace("5ObligatoriasObservaciones", detalleEntity.getObservacionDetalle());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 4 + i);
                i++;
            }
            table.removeRow(3);

            /*ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);*/


            XWPFTable table2 = doc.getTables().get(7);
            XWPFTableRow row3 = table2.getRow(4 + obligatorias.size());
            for (ActividadSilviculturalDetalleEntity detalleEntity : opcionales) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table2);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("5OpcionalesActividades")) {
                                        text5 = text5.replace("5OpcionalesActividades", detalleEntity.getActividad() != null ? detalleEntity.getActividad() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5OpcionalesDescripcion")) {
                                        text5 = text5.replace("5OpcionalesDescripcion", detalleEntity.getDescripcionDetalle() != null ? detalleEntity.getDescripcionDetalle() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5OpcionalesEquipos")) {
                                        text5 = text5.replace("5OpcionalesEquipos", detalleEntity.getEquipo() != null ? detalleEntity.getEquipo() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5OpcionalesInsumos")) {
                                        text5 = text5.replace("5OpcionalesInsumos", detalleEntity.getInsumo() != null ? detalleEntity.getInsumo() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5OpcionalesPersonal")) {
                                        text5 = text5.replace("5OpcionalesPersonal", detalleEntity.getPersonal() != null ? detalleEntity.getPersonal() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("5OpcionalesObservaciones")) {
                                        text5 = text5.replace("5OpcionalesObservaciones", detalleEntity.getObservacionDetalle() != null ? detalleEntity.getObservacionDetalle() : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table2.addRow(copiedRow);
            }
            //table.removeRow(3);
            table.removeRow(4 + obligatorias.size());

           /*ByteArrayOutputStream b1 = new ByteArrayOutputStream();
            doc.write(b1);*/
            //doc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<ActividadSilviculturalDetalleEntity> getObligatoriasOpcionales(List<ActividadSilviculturalDetalleEntity> list, String tipo) {
        List<ActividadSilviculturalDetalleEntity> lista = new ArrayList<>();
        for (ActividadSilviculturalDetalleEntity ac : list) {
            if (ac.getIdTipoTratamiento().equals(tipo)) {
                lista.add(ac);
            }
        }
        return lista;
    }

    public void generarRecursoForestalMadderablePGMF(Integer idPlanManejo, XWPFDocument doc) {
        try {
            List<CensoForestalDetalleEntity> lstLista = censoForestalDetalleRepo.ListarCensoForestalDetalle(idPlanManejo, "", "1");

            XWPFTable table = doc.getTables().get(3);
            XWPFTableRow row2 = table.getRow(2);
            int j = 0;
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("3ncomun")) {
                                        if (lstLista.get(i).getNombreComun() != null) {
                                            text5 = text5.replace("3ncomun", lstLista.get(i).getNombreComun());
                                        } else {
                                            text5 = text5.replace("3ncomun", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3ncientifico")) {
                                        if (lstLista.get(i).getNombreCientifico() != null) {
                                            text5 = text5.replace("3ncientifico", lstLista.get(i).getNombreCientifico());
                                        } else {
                                            text5 = text5.replace("3ncientifico", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3nNativo")) {
                                        if (lstLista.get(i).getNombreNativo() != null) {
                                            text5 = text5.replace("3nNativo", lstLista.get(i).getNombreNativo());
                                        } else {
                                            text5 = text5.replace("3nNativo", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3este")) {
                                        text5 = text5.replace("3este", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3Norte")) {
                                        text5 = text5.replace("3Norte", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3apr")) {
                                        if (lstLista.get(i).getIdTipoArbol().equals("1")) {
                                            text5 = text5.replace("3apr", lstLista.get(i).getIdTipoArbol());
                                            r.setText(text5, 0);
                                        } else {
                                            text5 = text5.replace("3apr", "");
                                            r.setText(text5, 0);
                                        }

                                    } else if (text5.contains("3vol")) {
                                        text5 = text5.replace("3vol", String.valueOf(lstLista.get(i).getVolumen()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("3se")) {
                                        if (lstLista.get(i).getIdTipoArbol().equals("2")) {
                                            text5 = text5.replace("3se", lstLista.get(i).getIdTipoArbol());
                                            r.setText(text5, 0);
                                        } else {
                                            text5 = text5.replace("3se", "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 2 + j);
                j++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            //doc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generarResumenRecursoForestalMadderablePGMF(Integer idPlanManejo, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(4);

            List<ListaEspecieDto> lstLista = censoForestalDetalleRepo.ListaResumenEspecies(idPlanManejo, "", "1");

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (ListaEspecieDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("32comun")) {
                                        text5 = text5.replace("32comun", detalleEntity.getNombre() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("32cientifico")) {
                                        text5 = text5.replace("32cientifico", detalleEntity.getCientifico() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("32nativo")) {
                                        text5 = text5.replace("32nativo", detalleEntity.getNativo() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("32a")) {
                                        text5 = text5.replace("32a", detalleEntity.getNumeroArbolesAprovechables() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("32vol")) {
                                        text5 = text5.replace("32vol", detalleEntity.getVolumenComercial() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("32s")) {
                                        text5 = text5.replace("32s", detalleEntity.getNumeroArbolSemilleros() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("32to")) {
                                        text5 = text5.replace("32to", detalleEntity.getNumeroArbolTotal() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generarRecursoForestalNoMadderablePGMF(Integer idPlanManejo, XWPFDocument doc) {
        try {
            List<CensoForestalDetalleEntity> lstLista = censoForestalDetalleRepo.ListarCensoForestalDetalle(idPlanManejo, "", "2");

            XWPFTable table = doc.getTables().get(5);
            XWPFTableRow row2 = table.getRow(2);
            int j = 0;
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("4comun")) {
                                        if (lstLista.get(i).getNombreComun() != null) {
                                            text5 = text5.replace("4comun", lstLista.get(i).getNombreComun());
                                        } else {
                                            text5 = text5.replace("4comun", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4cientifico")) {
                                        if (lstLista.get(i).getNombreCientifico() != null) {
                                            text5 = text5.replace("4cientifico", lstLista.get(i).getNombreCientifico());
                                        } else {
                                            text5 = text5.replace("4cientifico", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4nativo")) {
                                        if (lstLista.get(i).getNombreNativo() != null) {
                                            text5 = text5.replace("4nativo", lstLista.get(i).getNombreNativo());
                                        } else {
                                            text5 = text5.replace("4nativo", "");
                                        }
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4este")) {
                                        text5 = text5.replace("4este", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4nort")) {
                                        text5 = text5.replace("4nort", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4aprove")) {
                                        if (lstLista.get(i).getIdTipoArbol().equals("1")) {
                                            text5 = text5.replace("4aprove", lstLista.get(i).getIdTipoArbol());
                                            r.setText(text5, 0);
                                        } else {
                                            text5 = text5.replace("4aprove", "");
                                            r.setText(text5, 0);
                                        }
                                    } else if (text5.contains("4pr")) {
                                        text5 = text5.replace("4pr", lstLista.get(i).getProductoTipo());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4u")) {
                                        text5 = text5.replace("4u", lstLista.get(i).getCodigoUnidadMedida());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4c")) {
                                        text5 = text5.replace("4c", String.valueOf(lstLista.get(i).getCantidad()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("4se")) {
                                        if (lstLista.get(i).getIdTipoArbol().equals("2")) {
                                            text5 = text5.replace("4se", lstLista.get(i).getIdTipoArbol());
                                            r.setText(text5, 0);
                                        } else {
                                            text5 = text5.replace("4se", "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 2 + j);
                j++;
            }
            //System.out.println(lstLista.size());
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            //doc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generarResumenRecursoForestalNoMadderablePGMF(Integer idPlanManejo, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(6);

            List<ListaEspecieDto> lstLista = censoForestalDetalleRepo.ListaResumenEspecies(idPlanManejo, "", "2");

            System.out.println(lstLista.size());

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (ListaEspecieDto detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("42co")) {
                                        text5 = text5.replace("42co", detalleEntity.getNombre() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42cienti")) {
                                        text5 = text5.replace("42cienti", detalleEntity.getCientifico() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42nativo")) {
                                        text5 = text5.replace("42nativo", detalleEntity.getNativo() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42apro")) {
                                        text5 = text5.replace("42apro", detalleEntity.getNumeroArbolesAprovechables() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42p")) {
                                        text5 = text5.replace("42p", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42u")) {
                                        text5 = text5.replace("42u", "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42c")) {
                                        text5 = text5.replace("42c", String.valueOf(detalleEntity.getCantProducto()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42s")) {
                                        text5 = text5.replace("42s", String.valueOf(detalleEntity.getNumeroArbolSemilleros()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("42t")) {
                                        text5 = text5.replace("42t", String.valueOf(detalleEntity.getNumeroArbolTotal()));
                                        r.setText(text5, 0);
                                    }

                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 2 + i);
                i++;
            }
            table.removeRow(2 + lstLista.size());
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generarZonificacion(Integer idPlanManejo, XWPFDocument doc) {
        try {
            /******************************************************************************************/
            ZonificacionEntity zoni = new ZonificacionEntity();
            zoni.setIdPlanManejo(idPlanManejo);
            XWPFTable table = doc.getTables().get(2);

            ResultClassEntity resultClassEntity = zonificacionRepository.ListarZonificacion(zoni);
            List<ZonificacionEntity> zonificacionEntityList = (List<ZonificacionEntity>) resultClassEntity.getData();

            XWPFTableRow row2 = table.getRow(2);
            int i = 0;
            for (ZonificacionEntity detalleEntity : zonificacionEntityList) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("2zonas")) {
                                        text5 = text5.replace("2zonas", detalleEntity.getZona());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2Anexo1")) {

                                        text5 = text5.replace("2Anexo1", detalleEntity.getAnexo1() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2Anexo2")) {

                                        text5 = text5.replace("2Anexo2", detalleEntity.getAnexo2() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2Anexo3")) {

                                        text5 = text5.replace("2Anexo3", detalleEntity.getAnexo3() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2Total")) {
                                        text5 = text5.replace("2Total", detalleEntity.getTotal() + "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("2Porc")) {
                                        text5 = text5.replace("2Porc", detalleEntity.getPorcentaje() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 3 + i);
                i++;
            }
            table.removeRow(2);

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

            /******************************************************************************************/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 18-11-2021
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado PGMFA}
     * @param: Integer idPlanManejo
     */


    /*********************************************************************************************************************************************************** */
    @Override
    public ByteArrayResource consolidadoPGMF(Integer idPlanManejo) throws Exception {


        XWPFDocument doc = getDoc("ModeloConcesionesMaderables.docx");

        /* INFORMACION GENERAL 1 */
        ResultClassEntity<InformacionGeneralDto> info = infoGeneralRepo.obtener(idPlanManejo);
        InformacionGeneralDto data = new InformacionGeneralDto();

        if (info.getSuccess()) {
            data = info.getData();
        }

        DocUtil.setearInformacionGeneral(data, doc);

        ObjetivoManejoEntity ome = new ObjetivoManejoEntity();
        ome.setPlanManejo(new PlanManejoEntity());
        ome.getPlanManejo().setIdPlanManejo(idPlanManejo);

        /* OBJETIVOS DE MANEJO 2 */
        ResultClassEntity resObjManejo = objetivoManejoRepository.ObtenerObjetivoManejo(ome);

        ObjetivoManejoEntity dataObjetivo = new ObjetivoManejoEntity();
        List<ObjetivoEspecificoManejoEntity> list = new ArrayList<>();

        if (resObjManejo != null && resObjManejo.getSuccess()) {
            dataObjetivo = (ObjetivoManejoEntity) objetivoManejoRepository
                    .ObtenerObjetivoManejo(ome).getData();


            ResultClassEntity resultEspecifco = objetivoManejoRepository.ObtenerObjetivoEspecificoManejo(dataObjetivo);

            if (resultEspecifco != null && resultEspecifco.getSuccess()) {
                list = (ArrayList<ObjetivoEspecificoManejoEntity>) resultEspecifco.getData();
            }
        }

        DocUtil.setearObjetivoGeneral(dataObjetivo, doc);
        DocUtil.setearObjetivoEspecifico(list, doc);

        /* INFORMACION BASICA 3 */
        InformacionBasicaEntity ibe = new InformacionBasicaEntity();
        ibe.setIdPlanManejo(idPlanManejo);
        ibe.setCodInfBasica("PGMF");
        ibe.setCodSubInfBasica("PGMFIBAM");

        /* INFORMACION BASICA 3.1 */

        List<InformacionBasicaUbigeoEntity> listaUbicacion = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> listaCoordenadas = new ArrayList<>();

        List<InformacionBasicaUbigeoEntity> resp = informacionBasicaUbigeoRepository.ListarInformacionBasicaDistrito(ibe);

        if (resp != null && !resp.isEmpty()) {
            listaUbicacion = resp;
        }
        DocUtil.setearUbicacionPolitica(listaUbicacion, doc);

        List<InfBasicaAereaDetalleDto> resp2 = informacionBasicaRepository.listarInfBasicaAerea("PGMF", idPlanManejo, "PGMFCFFMIBUMFUECCC");

        if (resp2 != null && !resp2.isEmpty()) {
            listaCoordenadas = resp2;
        }
        DocUtil.setearCoordenadasUTM(listaCoordenadas, doc);

        /* INFORMACION BASICA 3.2 */

        List<InfBasicaAereaDetalleDto> lista = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> lista2 = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> listaAccesibilidad = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFIBAMA");
        if (listaAccesibilidad != null && !listaAccesibilidad.isEmpty()) {

            lista = listaAccesibilidad.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            lista2 = listaAccesibilidad.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());
        }

        DocUtil.setearInfoBasicDet(lista, doc, 13, 2);
        DocUtil.setearInfoBasicDet(lista2, doc, 14, 0);

        /* INFORMACION BASICA 3.3 */

        List<UnidadFisiograficaUMFEntity> listaUnidadFisiografica = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> listaAspectosFisicosTab1 = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> listaAspectosFisicosTab3 = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> listaAspectosFisicosTab4 = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> listaAspectosFisicos = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFIBAMAF");
        ResultClassEntity resultUnidadF = unidadFisiograficaRepository.listarUnidadFisiografica(1);// falta arreglar
        // front

        if (resultUnidadF != null && resultUnidadF.getSuccess()) {

            listaUnidadFisiografica = (ArrayList<UnidadFisiograficaUMFEntity>) resultUnidadF.getData();
        }

        DocUtil.setearAspectosFisicosTab2(listaUnidadFisiografica, doc);

        if (listaAspectosFisicos != null && !listaAspectosFisicos.isEmpty()) {

            listaAspectosFisicosTab1 = listaAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            listaAspectosFisicosTab3 = listaAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_3"))
                    .collect(Collectors.toList());

            listaAspectosFisicosTab4 = listaAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_4"))
                    .collect(Collectors.toList());
        }
        DocUtil.setearInfoBasicDet(listaAspectosFisicosTab1, doc, 15, 1);
        DocUtil.setearInfoBasicDet(listaAspectosFisicosTab3, doc, 17, 0);
        DocUtil.setearInfoBasicDet(listaAspectosFisicosTab4, doc, 18, 0);

        /* INFORMACION BASICA 3.4 */
        List<FaunaEntity> listaFauna = new ArrayList<>();
        ResultClassEntity fauna = informacionBasicaRepository.obtenerFauna(idPlanManejo, "", "", "", "", "", "PGMF");

        if (fauna != null && fauna.getSuccess()) {
            listaFauna = (ArrayList<FaunaEntity>) fauna.getData();
        }

        DocUtil.setearFaunaSilvestre(listaFauna, doc);

        /* INFORMACION BASICA 3.5 */
        List<InfBasicaAereaDetalleDto> listaTab1 = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> listaTab2 = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> listaAspectosSocioEconomicos = informacionBasicaRepository
                .listarInfBasicaAerea("PGMF", idPlanManejo, "PGMFIBAMAS");
        if (listaAspectosSocioEconomicos != null &&
                !listaAspectosSocioEconomicos.isEmpty()) {

            listaTab1 = listaAspectosSocioEconomicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            listaTab2 = listaAspectosSocioEconomicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());
        }

        DocUtil.setearAspectosSocioEconomicos(listaTab1, doc);
        DocUtil.setearAspectosSocioEconomicos2(listaTab2, doc);

        /* INFORMACION BASICA 3.6 */
        List<InfBasicaAereaDetalleDto> listaAntecedentesTab1 = new ArrayList<>();
        List<InfBasicaAereaDetalleDto> listaAntecedentes2tab1 = new ArrayList<>();

        List<InfBasicaAereaDetalleDto> listaAntecedentes = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFAUIC");
        if (listaAntecedentes != null && !listaAntecedentes.isEmpty()) {

            listaAntecedentesTab1 = listaAntecedentes.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());
        }

        DocUtil.setearListaAntecedentes1(listaAntecedentesTab1, doc);

        List<InfBasicaAereaDetalleDto> listaAntecedentes2 = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFIBAMAIC");
        if (listaAntecedentes2 != null && !listaAntecedentes2.isEmpty()) {

            listaAntecedentes2tab1 = listaAntecedentes2.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());
        }

        DocUtil.setearListaAntecedentes2(listaAntecedentes2tab1, doc);

        // 4 ORDENAMIENTO Y PROTECCIÓN DE LA UMF

        // 4.1 CATEGORIAS DE ORDENAMIENTO

        OrdenamientoProteccionEntity op = new OrdenamientoProteccionEntity();
        op.setIdPlanManejo(idPlanManejo);
        op.setCodTipoOrdenamiento("PGMF");
        ResultEntity categorias = ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(op);

        List<OrdenamientoProteccionDetalleEntity> list3 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> list2 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listBloquesQuincenales = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listaParcelaCorta1 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listaParcelaCorta2 = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listUbicacionMarcador = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listSenalizacion = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listDemarcacion = new ArrayList<>();
        List<OrdenamientoProteccionDetalleEntity> listVigilancia = new ArrayList<>();

        if (categorias != null && categorias.getIsSuccess()) {

            List<OrdenamientoProteccionEntity> listaOrdenamiento = categorias.getData();

            list3 = listaOrdenamiento.get(0).getListOrdenamientoProteccionDet()
                    .stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFCO"))
                    .collect(Collectors.toList());


            // 4.2.1 DIVISION ADMINISTRATIVA
            list2 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABBQ")
                            && c.getCategoria() != null && c.getCategoria().equals("A"))
                    .collect(Collectors.toList());


            listBloquesQuincenales = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABBQ")
                            && c.getCategoria() != null && c.getCategoria().equals("B"))
                    .collect(Collectors.toList());


            // 4.2.2 PARCELAS DE CORTA
            listaParcelaCorta1 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABPC") &&
                            c.getDescripcion().equals("OPCION1"))
                    .collect(Collectors.toList());
            listaParcelaCorta1 = calcularAreaTotalParcela(listaParcelaCorta1);


            listaParcelaCorta2 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABPC") &&
                            c.getDescripcion().equals("OPCION2"))
                    .collect(Collectors.toList());
            listaParcelaCorta2 = calcularAreaTotalParcela(listaParcelaCorta2);


            // 4.3 PROTECCION Y VIGILANCIA
            // 4.3.1
            listUbicacionMarcador = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVUMV"))
                    .collect(Collectors.toList());


            // 4.3.2
            listSenalizacion = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVS"))
                    .collect(Collectors.toList());


            // 4.3.3
            listDemarcacion = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVDML"))
                    .collect(Collectors.toList());


            // 4.3.4
            listVigilancia = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVVUMF"))
                    .collect(Collectors.toList());
            System.out.println(listVigilancia);
        }
        DocUtil.setearCategoriasOrdenamiento(list3, doc, 25);
        DocUtil.setearNBloques(list2, doc, 26);
        DocUtil.setearPeriodo(list2, doc, 27);
        DocUtil.setearListaParcelaCorta(listBloquesQuincenales, doc, 28);
        DocUtil.setearListaParcelaCorta(listaParcelaCorta1, doc, 29);
        DocUtil.setearListaParcelaCorta(listaParcelaCorta2, doc, 30);
        DocUtil.setearListUbicacionYSenalizacion(listUbicacionMarcador, doc, 31);
        DocUtil.setearListUbicacionYSenalizacion(listSenalizacion, doc, 32);
        DocUtil.setearListDemarcacionYVigilancia(listDemarcacion, doc, 33);
        DocUtil.setearListDemarcacionYVigilancia(listVigilancia, doc, 34);

        // 5.1 INVENTARIO FORESTAL

        PotencialProduccionForestalEntity listaProduccionTab1 = new PotencialProduccionForestalEntity();
        PotencialProduccionForestalEntity listaProduccionTab2 = new PotencialProduccionForestalEntity();

        List<PotencialProduccionForestalEntity> listaProduccionForestal = potencialProduccionForestalRepository
                .ListarPotencialProducForestal(idPlanManejo, "PGMF");

        List<PotencialProduccionForestalEntity> respProdTab1 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMCIF"))
                .collect(Collectors.toList());

        List<PotencialProduccionForestalEntity> respProdTab2 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRPM"))
                .collect(Collectors.toList());

        if (respProdTab1 != null && !respProdTab1.isEmpty()) {
            listaProduccionTab1 = respProdTab1.get(0);
        }

        if (respProdTab2 != null && !respProdTab2.isEmpty()) {
            listaProduccionTab2 = respProdTab2.get(0);
        }

        DocUtil.setearListaProduccionTab1(listaProduccionTab1, doc, 35);
        DocUtil.setearListaProduccionTab2(listaProduccionTab2, doc, 36);

        // 5.2 INVENTARIO FORESTAL
        // 5.2.1 ERROR MUESTREO 
        List<ResultadosEspeciesMuestreoDto> censoForestalEspecies = new ArrayList<>();

        List<PotencialProduccionForestalEntity> listaProduccionTab3 = new ArrayList<>();
        PotencialProduccionForestalEntity prodTab3 = new PotencialProduccionForestalEntity();

        List<ResultadosEspeciesMuestreoDto> resForEsp = censoForestalRepository
                .ResultadosEspecieMuestreo(idPlanManejo, "PGMF");

        List<PotencialProduccionForestalEntity> resProTab3 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRPMB"))
                .collect(Collectors.toList());

        if (resProTab3 != null && !resProTab3.isEmpty()) {
            if (resProTab3!=null && !resProTab3.isEmpty() && resProTab3.get(0)!=null) {
                listaProduccionTab3 = resProTab3;
                prodTab3 = listaProduccionTab3.get(0);
            }
            
        }

        DocUtil.setearListaProduccionTab3(prodTab3, doc, 37);

        // 5.2.2 RESUMEN VALORES
        List<ResultadosEspeciesMuestreoDto> listaProdFor = new ArrayList<>();

        List<ResultadosEspeciesMuestreoDto> listaProduccionForestal2 = censoForestalRepository
                .ResultadosEspecieMuestreo(idPlanManejo, "PGMF");
        if (listaProduccionForestal2 != null && !listaProduccionForestal2.isEmpty()) {
            listaProdFor = listaProduccionForestal2.stream()
                    .filter(c -> c.getListTablaEspeciesMuestreo() != null
                            && !c.getListTablaEspeciesMuestreo().isEmpty())
                    .collect(Collectors.toList());
        }

        DocUtil.setearListaProduccionForestal2(listaProdFor, doc, 38);

        // 5.2.3 ESPECIES ABUNDANTES
        if (resForEsp != null && !resForEsp.isEmpty()) {
            censoForestalEspecies = resForEsp;
        }

        DocUtil.setearListaProduccionTab4(censoForestalEspecies, doc, 39);

        // 5.3.1 RESULTADOS FUSTALES
        List<ResultadosEspeciesMuestreoDto> lista53 = new ArrayList<>();

        ResultClassEntity resFustales = censoForestalRepository.listarResultadosFustales(idPlanManejo, "PGMF");
        if (resFustales != null && resFustales.getSuccess()) {
            List<ResultadosEspeciesMuestreoDto> objList = (ArrayList<ResultadosEspeciesMuestreoDto>) resFustales
                    .getData();
            System.out.println(objList);

            lista53 = objList.stream()
                    .filter(c -> c.getListTablaEspeciesMuestreo() != null
                            && !c.getListTablaEspeciesMuestreo().isEmpty())
                    .collect(Collectors.toList());
        }

        DocUtil.setearResultadosFustales(lista53, doc, 40);
        // 5.3.2 RESULTADOS FUSTALES
        List<PotencialProduccionForestalEntity> listaProduccionTab532 = new ArrayList<>();

        List<PotencialProduccionForestalEntity> resProddTab532 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRFD"))
                .collect(Collectors.toList());

        if (resProddTab532 != null && !resProddTab532.isEmpty()) {
            listaProduccionTab532 = resProddTab532;
        }

        DocUtil.setearResultadosFustalesE(listaProduccionTab532, doc, 41);

        // 5.4 POTENCIAL DE PRODUCCIÓN

        List<AprovechamientoRFNMDto> listaPotencialProduccion = censoForestalRepository
                .ResultadosAprovechamientoRFNM(idPlanManejo, "PGMF");

        DocUtil.setearListaPotencialProduccion(listaPotencialProduccion, doc, 42);

        /** 6. MANEJO FORESTAL */

        // 6.1.
        List<ManejoBosqueDetalleEntity> modelo = new ArrayList<>();

        List<ManejoBosqueEntity> listaManejoBosqueTab1 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFUPCO", null);

        if (listaManejoBosqueTab1 != null && !listaManejoBosqueTab1.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaBosqueTab1 = listaManejoBosqueTab1.get(0).getListManejoBosqueDetalle();
            DocUtil.setearUsoPotencialPorCategoria(listaBosqueTab1, doc, 43);
        } else {
            DocUtil.setearUsoPotencialPorCategoria(modelo, doc, 43);
        }
        // 6.2.
        List<ManejoBosqueEntity> listaManejoBosqueTab2 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFST", null);
        if (listaManejoBosqueTab2 != null && !listaManejoBosqueTab2.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaBosqueTab2 = listaManejoBosqueTab2.get(0).getListManejoBosqueDetalle();
            DocUtil.setearSistemaManejoEspecificarYCicloDeCorta(listaBosqueTab2, doc, 44);
        } else {
            DocUtil.setearSistemaManejoEspecificarYCicloDeCorta(modelo, doc, 44);
        }
        // 6.3.
        List<ManejoBosqueEntity> listaManejoBosqueTab3 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFCC", null);
        if (listaManejoBosqueTab3 != null && !listaManejoBosqueTab3.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaBosqueTab3 = listaManejoBosqueTab3.get(0).getListManejoBosqueDetalle();
            DocUtil.setearSistemaManejoEspecificarYCicloDeCorta(listaBosqueTab3, doc, 45);
        } else {
            DocUtil.setearSistemaManejoEspecificarYCicloDeCorta(modelo, doc, 45);
        }
        // 6.4.
        List<ManejoBosqueEntity> listaManejoBosqueTab4 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEMDMC", null);
        if (listaManejoBosqueTab4 != null && !listaManejoBosqueTab4.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaBosqueTab4 = listaManejoBosqueTab4.get(0).getListManejoBosqueDetalle();
            DocUtil.setearEspeciesManejarDiametros(listaBosqueTab4, doc, 46);
        } else {
            DocUtil.setearEspeciesManejarDiametros(modelo, doc, 46);
        }
        // 6.5.
        List<ManejoBosqueEntity> listaManejoBosqueTab5 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEFP", null);
        if (listaManejoBosqueTab5 != null && !listaManejoBosqueTab5.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaBosqueTab5 = listaManejoBosqueTab5.get(0).getListManejoBosqueDetalle();
            DocUtil.setearEspeciesDeFloraProteger(listaBosqueTab5, doc, 47);
        } else {
            DocUtil.setearEspeciesDeFloraProteger(modelo, doc, 47);
        }

        //Se usa hasta la tabla N° 57

        // 6.6.1
        List<VolumenComercialPromedioCabeceraDto> header = new ArrayList<>();
        List<VolumenComercialPromedioEspecieDto> body = new ArrayList<>();

        ResultClassEntity listVolumenComerProm = censoForestalRepository.listarVolumenComercialPromedio(idPlanManejo, "PGMF");
        ResultClassEntity listEspVolumenComerProm = censoForestalRepository.listarEspecieVolumenComercialPromedio(idPlanManejo, "PGMF");

        if (listVolumenComerProm != null && listVolumenComerProm.getSuccess() && listEspVolumenComerProm != null && listEspVolumenComerProm.getSuccess()) {
            header = (ArrayList<VolumenComercialPromedioCabeceraDto>) listVolumenComerProm.getData();
            body = (ArrayList<VolumenComercialPromedioEspecieDto>) listEspVolumenComerProm.getData();
        }
        DocUtil.setearHeaderCortaAnual(header, doc, 50);
        DocUtil.setearBodyCortaAnual(body, doc, 51);

        ResultClassEntity listCortaAnualPerm = censoForestalRepository.listarEspecieVolumenCortaAnualPermisible(idPlanManejo, "PGMF");

        // 6.6.2
        List<VolumenComercialPromedioEspecieDto> objListCortAnPer = new ArrayList<>();

        if (listCortaAnualPerm != null && listCortaAnualPerm.getSuccess()) {
            objListCortAnPer = (ArrayList<VolumenComercialPromedioEspecieDto>) listCortaAnualPerm
                    .getData();

            objListCortAnPer = addCortaAnualTotales(objListCortAnPer);
        }

        DocUtil.setearCortaAnualTab2(objListCortAnPer, doc, 52);

        //6.7

        /* ResultClassEntity proyectoCosecha = censoForestalRepository.ListarProyeccionCosecha(idPlanManejo, "PGMF");
        if (proyectoCosecha != null && proyectoCosecha.getSuccess()) {
            List<ProyeccionCosechaCabeceraDto> listProyCos = (ArrayList<ProyeccionCosechaCabeceraDto>) proyectoCosecha
                        .getData();
            List<ProyeccionCosechaDetalleDto> cabezeraList = listProyCos.get(0).getDetalle();

            DocUtil.addColumnTableProyCos(cabezeraList, doc, 53);
        } */

        //6.8
        List<ManejoBosqueDetalleEntity> listaObjSisAprov = new ArrayList<>();

        List<ManejoBosqueEntity> listaEspecSistAprov = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFESA", null);
        if (listaEspecSistAprov != null && !listaEspecSistAprov.isEmpty()) {
            listaObjSisAprov = listaEspecSistAprov.get(0).getListManejoBosqueDetalle();
        }

        DocUtil.setearEspecificacionesSistAprov(listaObjSisAprov, doc, 54);

        //6.9
        //6.9.2 2 tablas
        List<ManejoBosqueDetalleEntity> ObjSubCodTipManA = new ArrayList<>();
        List<ManejoBosqueDetalleEntity> ObjSubCodTipManB = new ArrayList<>();

        List<ManejoBosqueEntity> listaEspecImport = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEPSTSA", null);
        if (listaEspecImport != null && !listaEspecImport.isEmpty()) {
            List<ManejoBosqueDetalleEntity> listaObjEspecImpor = listaEspecImport.get(0).getListManejoBosqueDetalle();

            ObjSubCodTipManA = listaObjEspecImpor.stream()
                    .filter(c -> c.getSubCodtipoManejoDet() != null && !c.getSubCodtipoManejoDet().equals("B"))
                    .collect(Collectors.toList());

            ObjSubCodTipManB = listaObjEspecImpor.stream()
                    .filter(c -> c.getSubCodtipoManejoDet() != null && c.getSubCodtipoManejoDet().equals("B"))
                    .collect(Collectors.toList());
        }
        DocUtil.setearEspeciesImportantes(ObjSubCodTipManA, doc, 55);
        DocUtil.setearEspeciesPosibles(ObjSubCodTipManB, doc, 56);

        //6.9.3 1 tabla
        List<ManejoBosqueDetalleEntity> objAreasDeg = new ArrayList<>();

        List<ManejoBosqueEntity> manejoAreasDegradadas = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEPSMEAD", null);
        if (manejoAreasDegradadas != null && !manejoAreasDegradadas.isEmpty()) {
            objAreasDeg = manejoAreasDegradadas.get(0).getListManejoBosqueDetalle();
        }

        DocUtil.setearManejoAreasDegradadas(objAreasDeg, doc, 57);



        /* EVALUACION IMPACTO 7 */
        EvaluacionAmbientalActividadEntity ev = new EvaluacionAmbientalActividadEntity();
        ev.setIdPlanManejo(idPlanManejo);
        ev.setTipoActividad("FACTOR");

        List<EvaluacionAmbientalActividadEntity> listaFactor = evaluacionAmbientalRepository
                .ListarEvaluacionActividadFiltro(ev);

        EvaluacionAmbientalAprovechamientoEntity eval = new EvaluacionAmbientalAprovechamientoEntity();
        eval.setIdPlanManejo(idPlanManejo);
        eval.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> listaActividades = evaluacionAmbientalRepository
                .ListarEvaluacionAprovechamientoFiltro(eval);

        List<EvaluacionAmbientalAprovechamientoEntity> listaPreAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("PREAPR"))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("APROVE"))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaPostAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("POSTAP"))
                .collect(Collectors.toList());

        EvaluacionAmbientalDto evalDto = new EvaluacionAmbientalDto();
        evalDto.setIdPlanManejo(idPlanManejo);
        List<EvaluacionAmbientalDto> listaRespuesta = evaluacionAmbientalRepository.ListarEvaluacionAmbiental(evalDto);

        DocUtil.setearCabeceraImpactoAmb(listaPreAprovechamiento, listaAprovechamiento, listaPostAprovechamiento, doc,
                58);
        DocUtil.setearDataImpactoAmbiental(listaFactor, listaPreAprovechamiento, listaAprovechamiento,
                listaPostAprovechamiento, listaRespuesta, doc, 58);

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab2 = listaActividades.stream()
                .filter(c -> c.getNombreAprovechamiento() != null && !c.getNombreAprovechamiento().equals(""))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab3 = listaActividades.stream()
                .filter(c -> c.getImpacto() != null && !c.getImpacto().equals(""))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab4 = listaActividades.stream()
                .filter(c -> c.getContingencia() != null && !c.getContingencia().equals(""))
                .collect(Collectors.toList());

        DocUtil.setearListaActividadesTab2(listaActividadesTab2, doc, 59);
        DocUtil.setearListaActividadesTab3(listaActividadesTab3, doc, 60);
        DocUtil.setearListaActividadesTab4(listaActividadesTab4, doc, 61);

        /* MONITOREO 8 */
        String descripcionMonitoreo = "";
        List<MonitoreoEntity> listaMonitoreoTab2 = new ArrayList<>();

        List<MonitoreoEntity> listaMonitoreo = monitoreoRepository.listarMonitoreoPOCC(idPlanManejo, "PGMF");
        if (listaMonitoreo != null && !listaMonitoreo.isEmpty()) {

            List<MonitoreoEntity> listaMonitoreoTab1 = listaMonitoreo.stream()
                    .filter(c -> c.getMonitoreo().equals("TXTCabecera"))
                    .collect(Collectors.toList());

            List<MonitoreoEntity> respListaMonitoreoTab2 = listaMonitoreo.stream()
                    .filter(c -> !c.getMonitoreo().equals("TXTCabecera"))
                    .collect(Collectors.toList());

            if (listaMonitoreoTab1 != null && !listaMonitoreoTab1.isEmpty()) {

                descripcionMonitoreo = listaMonitoreoTab1.get(0).getDescripcion();
            }

            DocUtil.setearDescripcionSistemaMonitoreo(descripcionMonitoreo, doc, 62);//62

            if (respListaMonitoreoTab2 != null && !respListaMonitoreoTab2.isEmpty()) {
                listaMonitoreoTab2 = respListaMonitoreoTab2;
            }

            DocUtil.setearDescripcionAccionMonitoreo(listaMonitoreoTab2, doc, 63);//63

            // ResultEntity<DocumentoAdjuntoEntity> archivo =
            // null;//ObtenerAdjuntos(filtro2);
            // DocUtil.setearAnexoMapa(archivo, doc);
        }

        /* PARTICIPACION CIUDADANA 9 */
        ParticipacionComunalParamEntity pcp = new ParticipacionComunalParamEntity();
        pcp.setIdPlanManejo(idPlanManejo);
        pcp.setCodTipoPartComunal("PGMFFO");
        pcp.setIdPartComunal(null);

        List<ParticipacionComunalEntity> listaFormulacion = new ArrayList<>();
        List<ParticipacionComunalEntity> listaFormulacion2 = new ArrayList<>();
        List<ParticipacionComunalEntity> listaFormulacion3 = new ArrayList<>();
        List<ParticipacionComunalEntity> listaFormulacion4 = new ArrayList<>();

        ResultEntity formulacion = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp);
        if (formulacion != null && formulacion.getIsSuccess()) {
            listaFormulacion = (ArrayList<ParticipacionComunalEntity>) formulacion
                    .getData();
        }

        DocUtil.setearFomulacionPMF(listaFormulacion, doc, 64);//64

        ParticipacionComunalParamEntity pcp2 = new ParticipacionComunalParamEntity();
        pcp2.setIdPlanManejo(idPlanManejo);
        pcp2.setCodTipoPartComunal("PGMFIM");
        pcp2.setIdPartComunal(null);
        ResultEntity implementacion = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp2);
        if (implementacion != null && implementacion.getIsSuccess()) {
            listaFormulacion2 = (ArrayList<ParticipacionComunalEntity>) implementacion
                    .getData();
        }
        DocUtil.setearImplementacionPMF(listaFormulacion2, doc, 65);//65

        ParticipacionComunalParamEntity pcp3 = new ParticipacionComunalParamEntity();
        pcp3.setIdPlanManejo(idPlanManejo);
        pcp3.setCodTipoPartComunal("PGMFCO");
        pcp3.setIdPartComunal(null);
        ResultEntity comites = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp3);
        if (comites != null && comites.getIsSuccess()) {
            listaFormulacion3 = (ArrayList<ParticipacionComunalEntity>) comites
                    .getData();
        }

        DocUtil.setearComitesPMF(listaFormulacion3, doc, 66);//66

        ParticipacionComunalParamEntity pcp4 = new ParticipacionComunalParamEntity();
        pcp4.setIdPlanManejo(idPlanManejo);
        pcp4.setCodTipoPartComunal("PGMFRE");
        pcp4.setIdPartComunal(null);
        ResultEntity reconocimiento = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp4);
        if (reconocimiento != null && reconocimiento.getIsSuccess()) {
            listaFormulacion4 = (ArrayList<ParticipacionComunalEntity>) reconocimiento
                    .getData();
        }

        DocUtil.setearReconicimientoPMF(listaFormulacion4, doc, 67);//67

        /* CAPACITACION 10 */
        CapacitacionDto capacitacionDto = new CapacitacionDto();
        capacitacionDto.setIdPlanManejo(idPlanManejo);
        capacitacionDto.setCodTipoCapacitacion("PGMFOBJ");

        List<CapacitacionDto> listaCapacitacion = capacitacionRepository.ListarCapacitacion(capacitacionDto);

        if (listaCapacitacion != null && !listaCapacitacion.isEmpty()) {
            DocUtil.setearObjetivosCapacitacionTab10(listaCapacitacion, doc, 68);//68
        }

        CapacitacionDto actividadPlanDto = new CapacitacionDto();
        capacitacionDto.setIdPlanManejo(idPlanManejo);
        capacitacionDto.setCodTipoCapacitacion("PGMF");

        List<CapacitacionDto> listaActividadPlan = capacitacionRepository.ListarCapacitacion(capacitacionDto);

        if (listaActividadPlan != null && !listaActividadPlan.isEmpty()) {
            DocUtil.setearActividadesPlanUMFTab10(listaActividadPlan, doc, 69);//69
        }

        // 11.1
        OrganizacionManejoEntity organizacionManejoEntity = new OrganizacionManejoEntity();
        organizacionManejoEntity.setIdPGMF(idPlanManejo);
        ResultClassEntity<List<OrganizacionManejoEntity>> result = pgmfaBreviadoRepository
                .OrganizacionManejo(organizacionManejoEntity);

        if (result != null && result.getSuccess()) {

            List<OrganizacionManejoEntity> listaFunciones = result.getData();

            List<OrganizacionManejoEntity> listaFuncionesTab1 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("FUNRESP"))
                    .collect(Collectors.toList());

            List<OrganizacionManejoEntity> listaFuncionesTab1Cab = listaFuncionesTab1.stream()
                    .filter(c -> c.getEsCabecera())
                    .collect(Collectors.toList());

            for (OrganizacionManejoEntity e : listaFuncionesTab1Cab) {

                List<OrganizacionManejoEntity> listaDetalle = listaFuncionesTab1.stream()
                        .filter(c -> !c.getEsCabecera() && c.getSubTipo().equals(e.getSubTipo()))
                        .collect(Collectors.toList());

                e.setListaDetalle(listaDetalle);
            }

            List<OrganizacionManejoEntity> listaFuncionesTab2 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("REQPERS"))
                    .collect(Collectors.toList());

            List<OrganizacionManejoEntity> listaFuncionesTab3 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("REQMAQU"))
                    .collect(Collectors.toList());

            DocUtil.setearListaFuncionesTab1(listaFuncionesTab1Cab, doc, 70);
            DocUtil.setearListaFuncionesTab2(listaFuncionesTab2, doc, 71);
            DocUtil.setearListaFuncionesTab3(listaFuncionesTab3, doc, 72);
        }

        /* PROGRAMA DE INVERSIONES 12 */
        PlanManejoEntity p = new PlanManejoEntity();
        p.setIdPlanManejo(idPlanManejo);
        p.setCodigoParametro("PGMF");
        ResultClassEntity resultRenta = rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(p);

        if (resultRenta != null && resultRenta.getSuccess()) {

            List<RentabilidadManejoForestalDto> listaProgramaInversiones = (ArrayList<RentabilidadManejoForestalDto>) resultRenta
                    .getData();

            // FIN NECESIDADES DE FINANCIAMIENTO
            // FLUJO CAJA INGRESO
            List<RentabilidadManejoForestalDto> listaFlujoCajaIngreso = listaProgramaInversiones.stream()
                    .filter(c -> c.getIdTipoRubro().equals(1))
                    .collect(Collectors.toList());

            List<Integer> listaAnioIngreso = new ArrayList<>();

            for (RentabilidadManejoForestalDto e : listaFlujoCajaIngreso) {
                for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                    listaAnioIngreso.add(el.getAnio());
                }
            }

            listaAnioIngreso = listaAnioIngreso.stream().distinct().collect(Collectors.toList());
            Collections.sort(listaAnioIngreso);

            RentabilidadManejoForestalDto rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
            rentabilidadManejoForestalDto.setRubro("TOTAL");
            RentabilidadManejoForestalDetalleEntity rentabilidadManejoForestalDetalleEntity = null;
            List<RentabilidadManejoForestalDetalleEntity> listRentabilidadManejoForestalDetalle = new ArrayList<>();
            for (Integer e : listaAnioIngreso) {
                rentabilidadManejoForestalDetalleEntity = new RentabilidadManejoForestalDetalleEntity();
                rentabilidadManejoForestalDetalleEntity.setAnio(e);
                rentabilidadManejoForestalDetalleEntity.setMonto(
                        sumarColumna(listaFlujoCajaIngreso, e));
                listRentabilidadManejoForestalDetalle.add(rentabilidadManejoForestalDetalleEntity);
            }

            rentabilidadManejoForestalDto
                    .setListRentabilidadManejoForestalDetalle(listRentabilidadManejoForestalDetalle);
            listaFlujoCajaIngreso.add(rentabilidadManejoForestalDto);

            DocUtil.addColumnFlujoCaja(listaAnioIngreso, doc, 73);
            DocUtil.setearFlujoCaja(listaFlujoCajaIngreso, doc, 73);

            // FIN FLUJO CAJA INGRESO

            // FLUJO CAJA EGRESO
            List<RentabilidadManejoForestalDto> listaFlujoCajaEgreso = listaProgramaInversiones.stream()
                    .filter(c -> c.getIdTipoRubro().equals(2))
                    .collect(Collectors.toList());

            List<Integer> listaAnioEgreso = new ArrayList<>();

            for (RentabilidadManejoForestalDto e : listaFlujoCajaEgreso) {
                for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                    listaAnioEgreso.add(el.getAnio());
                }
            }

            listaAnioEgreso = listaAnioEgreso.stream().distinct().collect(Collectors.toList());
            Collections.sort(listaAnioEgreso);

            rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
            rentabilidadManejoForestalDto.setRubro("TOTAL");
            rentabilidadManejoForestalDetalleEntity = null;
            listRentabilidadManejoForestalDetalle = new ArrayList<>();
            for (Integer e : listaAnioEgreso) {
                rentabilidadManejoForestalDetalleEntity = new RentabilidadManejoForestalDetalleEntity();
                rentabilidadManejoForestalDetalleEntity.setAnio(e);
                rentabilidadManejoForestalDetalleEntity.setMonto(
                        sumarColumna(listaFlujoCajaEgreso, e));
                listRentabilidadManejoForestalDetalle.add(rentabilidadManejoForestalDetalleEntity);
            }

            rentabilidadManejoForestalDto
                    .setListRentabilidadManejoForestalDetalle(listRentabilidadManejoForestalDetalle);
            listaFlujoCajaEgreso.add(rentabilidadManejoForestalDto);

            DocUtil.addColumnFlujoCaja(listaAnioEgreso, doc, 74);
            DocUtil.setearFlujoCaja(listaFlujoCajaEgreso, doc, 74);
            // FIN FLUJO CAJA EGRESO

            // NECESIDADES DE FINANCIAMIENTO

            if (listaProgramaInversiones != null && !listaProgramaInversiones.isEmpty()) {

                List<RentabilidadManejoForestalDto> listaNecesidadesFinanciamiento = listaProgramaInversiones.stream()
                        .filter(c -> c.getIdTipoRubro().equals(3))
                        .collect(Collectors.toList());

                DocUtil.setearListarNecesidades(listaNecesidadesFinanciamiento, doc, 75);

            }

        }

        /* CRONOGRAMA 13 */
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setIdPlanManejo(idPlanManejo);
        request.setCodigoProceso("PGMF");
        ResultEntity<CronogramaActividadEntity> resultCronograma = repCronAct.ListarCronogramaActividad(request);

        if (resultCronograma != null && resultCronograma.getIsSuccess()) {

            List<CronogramaActividadEntity> listaCronograma = resultCronograma.getData();

            System.out.println(listaCronograma);

            List<CronogramaActividadEntity> listaCronogramaPlan = listaCronograma.stream()
                    .filter(c -> c.getCodigoActividad().equals("PLANIFICACION"))
                    .collect(Collectors.toList());

            List<CronogramaActividadEntity> listaCronogramaAprov = listaCronograma.stream()
                    .filter(c -> c.getCodigoActividad().equals("APROVECHAMIENTO"))
                    .collect(Collectors.toList());

            DocUtil.setearCronogramaAct(listaCronogramaPlan, doc, 76);
            DocUtil.setearCronogramaAct(listaCronogramaAprov, doc, 77);

        }

 /*        AdjuntoRequestEntity filtro2 = new AdjuntoRequestEntity();
        filtro2.setIdProcesoPostulacion(idPlanManejo);
        filtro2.setCodigoAnexo("Anexo");
        ResultEntity<PGMFArchivoEntity> archivo = this.repositoryAnexo.ObtenerArchivoAnexoMapa(filtro2);
        if (archivo != null) {

            DocUtil.setearAnexoMapa(archivo, doc);//79
        } */


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    /*********************************************************************************************************************************************************** */

    @Override
    public ByteArrayResource consolidadoPGMFA(Integer idPlanManejo, String extension) throws Exception {
        XWPFDocument doc = getDoc("formatoPGMFA.docx");
        //1 - Resumen Ejecutivo
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PGMFA");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            //o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            //o.setProvincia(lstacceso.get(0).getNombreProvincia());
            //o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        Map<String, String> keys = MapBuilder.keysInfoGeneralPGMFA(o);
        DocUtil.processFile(keys, doc, o.getLstUmf());

        //2 - Objetivos de Manejo
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PGMFA", idPlanManejo);
        DocUtil.procesarObjetivosPGMFA(lstObjetivos.getData(), doc);
        //3 - DURACION Y REVISION DEL PLAN
        InformacionGeneralDEMAEntity D = new InformacionGeneralDEMAEntity();
        D.setIdPlanManejo(idPlanManejo);
        D.setCodigoProceso("PGMFA");
        D = infoGeneralDemaRepo.listarInformacionGeneralDema(D).getData().get(0);
        DocUtil.procesoInformacionGeneralPGMFADuracion(D, doc);
        //4 -INFORMACION BASICA DEL AREA DE MANEJO
        //4.1
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMATC");
        DocUtil.generarInfoAreaAcreditacionTerritorioPGMFA(lstInfoAreaAcreditacionTerritorio, doc);
        //4.2
        List<InfBasicaAereaDetalleDto> lstInfoAreaUbiPolitica = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMUP");
        String departamento = "", provincia = "", distrito = "";
        for (InfBasicaAereaDetalleDto detalleEntityUbigeo : lstInfoAreaUbiPolitica) {
            List<InfBasicaAereaDetalleDto> lstInfoAreaUbigeo = informacionBasicaRepository.listarInfBasicaAereaUbigeo(detalleEntityUbigeo.getDepartamento(), detalleEntityUbigeo.getProvincia(), detalleEntityUbigeo.getDistrito());
            for (InfBasicaAereaDetalleDto detalleEntityUbigeo2 : lstInfoAreaUbigeo) {
                departamento = detalleEntityUbigeo2.getDepartamento();
                provincia = detalleEntityUbigeo2.getProvincia();
                distrito = detalleEntityUbigeo2.getDistrito();
            }
        }


        DocUtil.generarInfoAreaUbiPoliticaPGMFA(lstInfoAreaUbiPolitica, doc, departamento, provincia, distrito);
        //4.3.1
        //Coordenadas UTM Comunidad Nativa
        List<InfBasicaAereaDetalleDto> lstInfoAreaCoordenadasUTMNativa = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFMC");
        DocUtil.generarInfoAreaCoordenadasUTMNativaPGMFA(lstInfoAreaCoordenadasUTMNativa, doc);
        //4.3.2 Coordenadas UTM de la Unidad de Manejo Forestal de la Comunidad
        List<InfBasicaAereaDetalleDto> lstInfoAreaCoordenadasUTMMF = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFMD");
        DocUtil.generarInfoAreaCoordenadasUTMMFPGMFA(lstInfoAreaCoordenadasUTMMF, doc);

        //4.4
        List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMA");
        //accesos 8
        List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad1 = lstInfoAreaAccesibilidad.stream().filter(a -> a.getCodSubInfBasicaDet().equals("TAB_1")).collect(Collectors.toList());
        DocUtil.generarInfoAreaAccesibilidad1PGMFA(lstInfoAreaAccesibilidad1, doc);
        //caminos 9
        List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad2 = lstInfoAreaAccesibilidad.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2")).collect(Collectors.toList());
        DocUtil.generarInfoAreaAccesibilidad2PGMFA(lstInfoAreaAccesibilidad2, doc);

        //4.5
        List<InfBasicaAereaDetalleDto> lstInfoAreaHF = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAF");
        //HIDROGRAFIA 10
        List<InfBasicaAereaDetalleDto> lstInfoAreaHidrografia = lstInfoAreaHF.stream().filter(h -> h.getCodSubInfBasicaDet().equals("TAB_1")).collect(Collectors.toList());
        DocUtil.generarInfoAreaHidrografia(lstInfoAreaHidrografia, doc);
        //FISIOGRAFIA 11
        List<InfBasicaAereaDetalleDto> lstInfoAreaFisiografia = lstInfoAreaHF.stream().filter(f -> f.getCodSubInfBasicaDet().equals("TAB_2")).collect(Collectors.toList());
        DocUtil.generarInfoAreaFisiografia(lstInfoAreaFisiografia, doc);


        //4.6
        //4.61
        //FAUNA SILVESTRE 12
        List<InfBasicaAereaDetalleDto> lstInfoAreaFAU = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAB");
        /*List<EspecieFaunaEntity> lstFauna = new ArrayList<>();
        for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lstInfoAreaFAU) {
            if(infBasicaAereaDetalleDto.getCodSubInfBasicaDet().equals("TAB_1")){
                EspecieFaunaEntity request = new EspecieFaunaEntity();
                short idEspecie =infBasicaAereaDetalleDto.getIdFauna().shortValue();
                request.setIdEspecie(idEspecie);
                ResultEntity<EspecieFaunaEntity>lstListdoFauna= corecentral.ListaPorFiltroEspecieFauna(request);
                lstFauna.add(lstListdoFauna.getData().get(0));
            }
        }
        DocUtil.generarInfoAreFAAUPGMFA(lstFauna,doc);
         */
        DocUtil.generarInfoAreaFaunaPGMFA(lstInfoAreaFAU, doc);

        //4.6.2
        List<InfBasicaAereaDetalleDto> lstInfoAreaTipoBosque = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMABTB");
        DocUtil.generarInfoAreaTipoBosquePGMFA(lstInfoAreaTipoBosque, doc);


        //4.7 Aspectos socioeconómicos
        List<InfBasicaAereaDetalleDto> lstInfoAreaAspectosSocio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAS");
        //4.7.1 Caracterización de la Comunidad 14
        List<InfBasicaAereaDetalleDto> lstCaracCom = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1")).collect(Collectors.toList());
        DocUtil.generarInfoAreCaracComunidad(lstCaracCom, doc);
        //4.7.2 Infraestructura de Servicios 15
        List<InfBasicaAereaDetalleDto> lstInfra = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2")).collect(Collectors.toList());
        DocUtil.generarInfoAreInfraEstructurasPGMFA(lstInfra, doc);
        //4.7.3 Antecedentes de uso del bosque e identificación de conflictos
        //a 16
        List<InfBasicaAereaDetalleDto> lstAnteUso = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_3")).collect(Collectors.toList());
        DocUtil.generarInfoAreAnteUsoPGMFA(lstAnteUso, doc);
        //b 17
        List<InfBasicaAereaDetalleDto> lstConflic = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_4")).collect(Collectors.toList());
        DocUtil.generarInfoAreConflictoPGMFA(lstConflic, doc);

        //5.	ORDENAMIENTO INTERNO
        OrdenamientoInternoDetalleDto ordenaRequest = new OrdenamientoInternoDetalleDto();
        ordenaRequest.setIdPlanManejo(idPlanManejo);
        ordenaRequest.setCodTipoOrdenamiento("PGMFA");
        List<OrdenamientoInternoDetalleDto> lstOrdenamiento = ordenamientoRepo.ListaROrdenamientoInternoPMFI(ordenaRequest);
        DocUtil.generarOrdenamientoInternoPGMFA(lstOrdenamiento, doc);


        //6. Potencial de Produccion del Recurso Forestal
        List<PotencialProduccionForestalEntity> lst = potencialProduccionForestalRepository.ListarPotencialProducForestal(idPlanManejo, "PGMFA");
        //DocUtil.generarPotencialProduccionForestal(lst,doc);
        //6.1.a
        List<PotencialProduccionForestalEntity> lst61a = lst.stream().filter(a -> a.getCodigoSubTipoPotencialProdForestal().equals("PGMFAA")).collect(Collectors.toList());
        DocUtil.generarPotencialProduccion61aPGMFA(lst61a, doc);
        //6.1.b
        List<PotencialProduccionForestalEntity> lst61b = lst.stream().filter(a -> a.getCodigoSubTipoPotencialProdForestal().equals("PGMFAB")).collect(Collectors.toList());
        DocUtil.generarPotencialProduccion61bPGMFA(lst61b, doc);

        //7. MANEJO DEL BOSQUE 	ORDENAMIENTO INTERNO
        // 7.1 Actividades principales según ordenamiento  23 PGMFAMBAPO,PGMFAMBAPO
        ManejoBosqueEntity oManejoBosque = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBAPO", "PGMFAMBDSM").get(0);
        ManejoBosqueEntity o71 = SerializationUtils.clone(oManejoBosque);
        o71.setListManejoBosqueDetalle(o71.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBAPO")).collect(Collectors.toList()));
        DocUtil.generarActividadesOrdenamientoManejoBosque(o71, doc);
        // 7.1.1 sistema manejo 24  PGMFAMBAPO,PGMFAMBDSM
        ManejoBosqueEntity o711 = SerializationUtils.clone(oManejoBosque);
        o711.setListManejoBosqueDetalle(o711.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBDSM")).collect(Collectors.toList()));
        DocUtil.generarSistemasManejoManejoBosquePGMFA(o711, doc);
        // 7.1.2 duracion del ciclo 25  PGMFAMBAPO,PGMFAMBDPF
        ManejoBosqueEntity o712 = SerializationUtils.clone(oManejoBosque);
        o712.setListManejoBosqueDetalle(o712.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBDPF")).collect(Collectors.toList()));
        DocUtil.generarDuracionCicloManejoBosquePGMFA(o712, doc);
        // 7.1.2 especies a aprovechar 26  PGMFAMBAPO,PGMFAACPORESP
        ManejoBosqueEntity o712b = SerializationUtils.clone(oManejoBosque);
        o712b.setListManejoBosqueDetalle(o712b.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAACPORESP")).collect(Collectors.toList()));
        DocUtil.generarEspeciesAprovecharManejoBosquePGMFA(o712b, doc);
        // 7.1.2 CAP 27
        // 7.2 aprovechamiento
        // 7.2.1 red de caminos 28  PGMFAAPRO,PGMFAMBIAT
        ManejoBosqueEntity oManejo2 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAAPRO", "PGMFAMBIAT").get(0);
        ManejoBosqueEntity o721 = SerializationUtils.clone(oManejo2);
        o721.setListManejoBosqueDetalle(o721.getListManejoBosqueDetalle().stream()
                .filter(q -> q.getCodtipoManejoDet().contains("PGMFAMBIAT")).collect(Collectors.toList()));
        DocUtil.generarRedCaminosManejoBosquePGMFA(o721, doc);
        // 7.2.1.b especif sobre caminos 29  PGMFAAPRO,PGMFAAPROCA
        ManejoBosqueEntity o721b = SerializationUtils.clone(oManejo2);
        o721b.setListManejoBosqueDetalle(o721b.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAAPROCA")).collect(Collectors.toList()));
        DocUtil.generarEspecificacionCaminosManejoBosquePGMFA(o721b, doc);
        // 7.2.1.c infra especificaciones 30 data fija
        DocUtil.generarInfraestructuraEspecificacionesManejoBosque(doc);
        // 7.2.2 operaciones corta arrastre 31  PGMFAAPRO,PGMFAMBOCA
        ManejoBosqueEntity o722 = SerializationUtils.clone(oManejo2);
        o722.setListManejoBosqueDetalle(o722.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBOCA")).collect(Collectors.toList()));
        DocUtil.generarOperacionCortaArrastreManejoBosquePGMFA(o722, doc);
        // 7.3 especies a proteger
        // 7.3.a flora 32  PGMFAMBEP,PGMFAESPRFLO
        ManejoBosqueEntity oManejo3 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBEP", "PGMFAESPRFLO").get(0);
        ManejoBosqueEntity o731a = SerializationUtils.clone(oManejo3);
        o731a.setListManejoBosqueDetalle(o731a.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAESPRFLO")).collect(Collectors.toList()));
        DocUtil.generarEspecieProtegerFloraManejoBosque(o731a, doc);
        // 7.3.b fauna 33  PGMFAMBEP,PGMFAESPRFAU
        ManejoBosqueEntity o731b = SerializationUtils.clone(oManejo3);
        o731b.setListManejoBosqueDetalle(o731b.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFAESPRFAU")).collect(Collectors.toList()));
        DocUtil.generarEspecieProtegerFaunaManejoBosque(o731b, doc);
        // 7.4 tratamientos silviculturales
        // 7.4.a tratam previstos 34  PGMFAMBTS,PGMFATRASILT
        ManejoBosqueEntity oManejo4 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBTS", "PGMFATRASILT").get(0);
        ManejoBosqueEntity o74a = SerializationUtils.clone(oManejo4);
        o74a.setListManejoBosqueDetalle(o74a.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFATRASILT")).collect(Collectors.toList()));
        DocUtil.generarTratamientoSilviculturalPrevistoManejoBosque(o74a, doc);
        // 7.4.b reforestacion 35  PGMFAMBTS,PGMFATRASILR
        ManejoBosqueEntity o74b = SerializationUtils.clone(oManejo4);
        o74b.setListManejoBosqueDetalle(o74b.getListManejoBosqueDetalle().stream()
                .filter(p -> p.getCodtipoManejoDet().contains("PGMFATRASILR")).collect(Collectors.toList()));
        DocUtil.generarTratamientoSilviculturalReforestacionManejoBosque(o74b, doc);


        //8. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosque = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "PGMFA", "AMB");
        DocUtil.generarProteccionBosquePGMFA(lstProteccionBosque, doc);

        //9. Monitoreo 41
        MonitoreoEntity monitoreo = new MonitoreoEntity();
        monitoreo.setIdPlanManejo(idPlanManejo);
        monitoreo.setCodigoMonitoreo("PGMFA");
        List<MonitoreoDetalleEntity> lstMonitoreo = monitoreoRepository.listarMonitoreo(monitoreo).getData().getLstDetalle();
        DocUtil.generarMonitoreoPGMFA(lstMonitoreo, doc);

        //10.	PARTICIPACION COMUNAL 42
        ParticipacionComunalEntity participacionRequest = new ParticipacionComunalEntity();
        participacionRequest.setCodTipoPartComunal("PGMFA");
        participacionRequest.setIdPlanManejo(idPlanManejo);
        ResultClassEntity<List<ParticipacionComunalEntity>> lstParticipacion = participacionComunalRepository.ListarPorFiltroParticipacionComunal(participacionRequest);
        DocUtil.generarParticipacionComunal(lstParticipacion.getData(), doc);

        //11.	CAPACITACION 43
        CapacitacionDto capacitacionRequest = new CapacitacionDto();
        capacitacionRequest.setCodTipoCapacitacion("PGMFA");
        capacitacionRequest.setIdPlanManejo(idPlanManejo);
        List<CapacitacionDto> lstCapacitacion = capacitacionRepository.ListarCapacitacion(capacitacionRequest);
        DocUtil.generarCapacitacionPgmfa(lstCapacitacion, doc);

        //12.	ORGANIZACIÓN PARA EL DESARROLLO DE LA ACTIVIDAD 44

        ResultClassEntity<ActividadSilviculturalDto> lstOrganizacion = actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo, "PGMFA");
        DocUtil.generarOrganizacionActividadPgmfa(lstOrganizacion.getData(), doc);

       /* //13. RENTABILIDAD DEL MANEJO FORESTAL 45
        RentabilidadManejoForestalEntity r = new RentabilidadManejoForestalEntity();
        PlanManejoEntity pm = new PlanManejoEntity();
        pm.setIdPlanManejo(idPlanManejo);
        pm.setCodigoParametro("PGMFA");
        List<RentabilidadManejoForestalDto> lstRentabilidadManejoForestal = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pm).getData();
        DocUtil.generarRentabilidadManejoForestal(lstRentabilidadManejoForestal, doc);*/

        //14.	CRONOGRAMA DE ACTIVIDADES 46

        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PGMFA");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();
        DocUtil.procesarCronogramaPGMFA(lstcrono,doc);

        //15.	ASPECTOS COMPLEMENTARIOS   (47)
        InformacionGeneralDEMAEntity aspectosComRequest = new InformacionGeneralDEMAEntity();
        aspectosComRequest.setCodigoProceso("PGMFA");
        aspectosComRequest.setIdPlanManejo(idPlanManejo);
        ResultEntity<InformacionGeneralDEMAEntity> lstAspectos = infoGeneralDemaRepo.listarInformacionGeneralDema(aspectosComRequest);
        DocUtil.generarAspectosCompPgmfa(lstAspectos.getData(), doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        if (extension != null) {
            if (extension.equals("PDF")) {
                InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
                WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
                File archivo = File.createTempFile("consolidadoPGMFA", ".pdf");
                String ruta = archivo.getAbsolutePath();
                FileOutputStream os = new FileOutputStream(archivo);
                Docx4J.toPDF(wordMLPackage, os);
                os.flush();
                os.close();
                byte[] fileContent = Files.readAllBytes(archivo.toPath());
                return new ByteArrayResource(fileContent);
            } else {
                return new ByteArrayResource(b.toByteArray());
            }
        }
        else{
        return new ByteArrayResource(b.toByteArray());
        }

}
    @Override
    public ByteArrayResource consolidadoPFCR(Integer idPermisoForestal) throws Exception {
        XWPFDocument doc = getDoc("formatoPFCR.docx");

        //4.	RESULTADOS OBTENIDOS
        EvaluacionPermisoForestalDto d = new EvaluacionPermisoForestalDto();
        d.setIdPermisoForestal(idPermisoForestal);

        List<EvaluacionPermisoForestalDto> lista = evaluacionRepository.listarEvaluacionPermisoForestal(d);
        EvaluacionPermisoForestalDetalleDto m = null;

        for (EvaluacionPermisoForestalDto element : lista) {
            m = new EvaluacionPermisoForestalDetalleDto();

            m.setIdEvaluacionPermiso(element.getIdEvaluacionPermiso());
            element.setListarEvaluacionPermisoDetalle(evaluacionDetalleRepository.listarEvaluacionPermisoForestaDetalle(m));
        }
        DocUtil.generarEvaluacionPFCR(lista, doc);



        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }


    @Override
    public ByteArrayResource consolidadoPOCC(Integer idPlanManejo,String extension) throws Exception {
        XWPFDocument doc = getDoc("formatoPOCC.docx");
        //1 - Resumen Ejecutivo
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POCC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);

        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle= new ArrayList<>();
        if(lstInformacionBasica.getData().size()>0){
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }
        String vigencia="";
        for (InformacionGeneralDetalle detalleEntity : lstInformacionBasicaDetalle) {
            vigencia=detalleEntity.getVigenciaDet().toString();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso= acceso.ListarSolicitudAcceso(param);
        if(lstacceso.size()>0){
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        Map<String, String> keys = MapBuilder.keysInfoGeneralPOCC(o,vigencia);
        DocUtil.processFile(keys,doc,o.getLstUmf());

        //2 - Resumen de actividades 2
        //2.1
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POCC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen =  resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);
        DocUtil.generarResumenActividadPOCC(lstResumen, doc);
        //2.1.1  -- 2
        DocUtil.generarResumenActividadFrenteCortaPOCC(lstResumen, doc);
        //2.2  -- 3
        DocUtil.generarResumenActividadResultadosPOCC(lstResumen, doc);


        //4 - INFORMACIÓN BASICA DE LA PARCELA DE CORTA (PC)
        //4.1  -- 4
        List<InfBasicaAereaDetalleDto> lstInfoBasicaUEPC = informacionBasicaRepository.listarInfBasicaAerea("POCC", idPlanManejo, "POCCIBPCUEPC");
        DocUtil.generarInfoBasicaUEPC(lstInfoBasicaUEPC, doc);
        //4.2  --6
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTIBO = informacionBasicaRepository.listarInfBasicaAerea("POCC", idPlanManejo, "POCCIBPCTIBO");
        DocUtil.generarInfoBasicaTIBO(lstInfoBasicaTIBO, doc);
        //4.3  --7
        List<InfBasicaAereaDetalleDto> lstInfoBasicaACCE = informacionBasicaRepository.listarInfBasicaAerea("POCC", idPlanManejo, "POCCIBPCACCE");
        DocUtil.generarInfoBasicaACCE(lstInfoBasicaACCE, doc);

        //5.	ORDENAMIENTO INTERNO
        //5.1 --8
        OrdenamientoProteccionEntity ordenaRequest = new OrdenamientoProteccionEntity();
        ordenaRequest.setIdPlanManejo(idPlanManejo);
        ordenaRequest.setCodTipoOrdenamiento("POCC");
        ResultEntity<OrdenamientoProteccionEntity> lstOrdenamientoCategoria = ordenamientoRepo.ListarOrdenamientoInternoDetalle(ordenaRequest);
        DocUtil.generarOrdenamientoCategoriaPOCC(lstOrdenamientoCategoria.getData(), doc);
        //5.3 --9
        DocUtil.generarOrdenamientoProteccionPOCC(lstOrdenamientoCategoria.getData(), doc);

        //6.1.3 --10
        ActividadAprovechamientoParam aprovechamiento = new ActividadAprovechamientoParam();
        aprovechamiento.setIdPlanManejo(idPlanManejo);
        aprovechamiento.setCodActvAprove("POCC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(aprovechamiento);
        DocUtil.generarActividadAprovechamientoPOCC(lstAprovechamiento.getData(), doc);
        //6.1.6 --11
        DocUtil.generarActividadAprovechamientoRecursosMPOCC(lstAprovechamiento.getData(), doc);
        //6.1.7 --12
        DocUtil.generarActividadAprovechamientoRecursosNMPOCC(lstAprovechamiento.getData(), doc);
        //6.2 --13
        DocUtil.generarActividadAprovechamientoVolumenPOCC(lstAprovechamiento.getData(), doc);
        //6.3 --14
        DocUtil.generarActividadAprovechamientoOperaPOCC(lstAprovechamiento.getData(), doc);

        //7.2 --15
        ActividadSilviculturalEntity act= new ActividadSilviculturalEntity();
        act.setIdPlanManejo(idPlanManejo);
        act.setCodigoTipoActSilvicultural("POCC");
        List<ActividadSilviculturalEntity> lstActSilvicultural2= actividadSilviculturalRepository.ListarActSilviCulturalDetalle(act);
        DocUtil.generarActividadSilviculturalTratamientosPOCC(lstActSilvicultural2, doc);
        //7.3 --16
        DocUtil.generarActividadSilviculturalEnriqPOCC(lstActSilvicultural2, doc);
        //8.1 --17
        EvaluacionAmbientalActividadEntity evalAmb=new EvaluacionAmbientalActividadEntity();
        evalAmb.setIdPlanManejo(idPlanManejo);
        evalAmb.setTipoActividad("FACTOR");
        List<EvaluacionAmbientalActividadEntity> lstEvalAmbiental= evaluacionAmbientalRepository.ListarEvaluacionActividadFiltro(evalAmb);

        EvaluacionAmbientalDto evalAmbAcciones=new EvaluacionAmbientalDto();
        evalAmbAcciones.setIdPlanManejo(idPlanManejo);
        List<EvaluacionAmbientalDto> lstEvalAmbientalAcciones= evaluacionAmbientalRepository.ListarEvaluacionAmbiental(evalAmbAcciones);
        DocUtil.generarEvalAmbientalImpactosAmbPOCC(lstEvalAmbiental,lstEvalAmbientalAcciones, doc);
        //8.2 Plan de Accion Preventivo-Corrector 18 POAPRO
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion = lstEvaluacion.stream()
                .filter(p->p.getCodTipoAprovechamiento().equals("POAPRO")).collect(Collectors.toList());
        DocUtil.generarPlanAccionEvaluacionImpactoAmbiental(lstPlanAccion,doc);
        //8.3 Plan de Vigilancia y Seguimiento Ambiental 19 POAPRO
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanVigilancia = lstPlanAccion.stream()
                .filter(p-> !p.getImpacto().trim().equals("")).collect(Collectors.toList());
        DocUtil.generarPlanVigilanciaEvaluacionImpactoAmbiental(lstPlanVigilancia,doc);
        //8.4 Plan de Contingencia Ambiental 20 POEAPR
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanContingencia = lstEvaluacion.stream()
                .filter(p->p.getCodTipoAprovechamiento().equals("POEAPR")).collect(Collectors.toList());
        DocUtil.generarPlanContingenciaEvaluacionImpactoAmbiental(lstPlanContingencia,doc);
        //9  Monitoreo 21
        List<MonitoreoEntity> lstMonitoreoPOCC = monitoreoRepository.listarMonitoreoPOCC(idPlanManejo,"POCC");
        DocUtil.generarMonitoreoPOCC(lstMonitoreoPOCC, doc);
        //10 Participación ciudadana 22 deberia ser 10.3
        ParticipacionComunalParamEntity participacion = new ParticipacionComunalParamEntity();
        participacion.setIdPlanManejo(idPlanManejo);
        participacion.setCodTipoPartComunal("POCCPC");
        ResultEntity<ParticipacionComunalEntity> lstParticipacionComunal = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(participacion);
        DocUtil.generarParticipacionComunalPOCC(lstParticipacionComunal.getData(), doc);
        //11 Ciudadania 23
        CapacitacionDto capacitacion = new CapacitacionDto();
        capacitacion.setIdPlanManejo(idPlanManejo);
        capacitacion.setCodTipoCapacitacion("POCC");
        List<CapacitacionDto> lstCapacitacion = capacitacionRepository.ListarCapacitacion(capacitacion);
        DocUtil.generarCapacitacionPOCC(lstCapacitacion, doc);

        //12. Programa de Inversiones
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POCC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        //12.1.a Totales 24
        Double[] totales = new Double[2];
        //12.1.b Ingresos 25
        List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());
        DocUtil.generarIngresoProgramaInversionPOCC(lstIngresos,doc,totales);
        //12.1.c Egresos 26
        List<RentabilidadManejoForestalDto> lstEgresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());
        DocUtil.generarEgresoProgramaInversionPOCC(lstEgresos,doc,totales);
        //12.1.a Totales 24
        DocUtil.generarTotalesProgramaInversionPOCC(totales,doc);
        //12.2 Necesidades de Financiamiento 27
        List<RentabilidadManejoForestalDto> lstNecesidades= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==3).collect(Collectors.toList());
        DocUtil.generarNecesidadFinanciamientoProgramaInversionPOCC(lstNecesidades,doc);
        //13. Cronograma de Actividades 28
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("POCC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();
        DocUtil.generarCronogramaActividadesPOCC(lstcrono,doc);


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        if(extension!=null){
            if(extension.equals("PDF")){
                InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
                WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
                File archivo = File.createTempFile("consolidadoPOCC", ".pdf");
                FileOutputStream os = new FileOutputStream(archivo);
                Docx4J.toPDF(wordMLPackage,os);
                os.flush();
                os.close();
                byte[] fileContent = Files.readAllBytes(archivo.toPath());
                return new ByteArrayResource(fileContent);
            }else{
                return new ByteArrayResource(b.toByteArray());
            }
        }else{
            return new ByteArrayResource(b.toByteArray());
        }

    }


    @Override
    public ByteArrayResource consolidadoPOAC(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPOAC.docx");
        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POAC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);

        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);

        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle = new ArrayList<>();
        if (lstInformacionBasica.getData().size() > 0) {
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        Map<String, String> keys = MapBuilder.keysInfoGeneralPOAC(o);
        DocUtil.processFile(keys,doc,o.getLstUmf());
        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);
        //2.1  -- 1
        DocUtil.generarResumenActividadPOAC(lstResumen, doc);
        //2.1 --2
        DocUtil.generarResumenActividadFrenteCortaPOAC(lstResumen, doc);
        //2.2  -- 3 positivo
        DocUtil.generarResumenActividadResultadosPOACPos(lstResumen, doc);
        //2.2  -- 4 negativo
        DocUtil.generarResumenActividadResultadosPOACNeg(lstResumen, doc);
        //2.2  -- 5 recomendacuin
        DocUtil.generarResumenActividadResultadosPOACRec(lstResumen, doc);

        //3 - Objetivos  -- 6
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos= objetivoservice.listarObjetivos("POAC",idPlanManejo);
        DocUtil.procesarObjetivosPOAC(lstObjetivos.getData(), doc);

        //4 - INFORMACIÓN BASICA DE LA PARCELA DE CORTA (PC)
        //4.1  -- 7
        List<InfBasicaAereaDetalleDto> lstInfoBasicaUEPC = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMATC");
        String departamento="",provincia="",distrito="";
        for (InfBasicaAereaDetalleDto detalleEntityUbigeo : lstInfoBasicaUEPC) {
            List<InfBasicaAereaDetalleDto> lstInfoAreaUbigeo = informacionBasicaRepository.listarInfBasicaAereaUbigeo(detalleEntityUbigeo.getDepartamento(), detalleEntityUbigeo.getProvincia(), detalleEntityUbigeo.getDistrito());
            for (InfBasicaAereaDetalleDto detalleEntityUbigeo2 : lstInfoAreaUbigeo) {
                departamento=detalleEntityUbigeo2.getDepartamento();
                provincia=detalleEntityUbigeo2.getProvincia();
                distrito=detalleEntityUbigeo2.getDistrito();
            }
        }
        DocUtil.generarInfoBasicaUEAA(lstInfoBasicaUEPC, doc, departamento,provincia, distrito );
        //4.2  --8
        List<InfBasicaAereaDetalleDto> lstInfoBasicaCOOUTM = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMCUMD");
        DocUtil.generarInfoBasicaCOOUTM (lstInfoBasicaCOOUTM, doc);
        //4.3  --9
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTB = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMABTB");
        DocUtil.generarInfoBasicaTB(lstInfoBasicaTB, doc);
        //4.4  --10
        List<InfBasicaAereaDetalleDto> lstInfoBasicaAcc = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMA");
        DocUtil.generarInfoBasicaAccA(lstInfoBasicaAcc, doc); // 10
        DocUtil.generarInfoBasicaAccC(lstInfoBasicaAcc, doc); // 11

        // 5.1  // 12
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);
        DocUtil.generarActividadAprovPOACDeli(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACDeliDis(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACPlani(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaCorta(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaArras(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaTrans(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaProce(lstAprovechamiento.getData(), doc);

        //6 A --23--
        ResultClassEntity<ActividadSilviculturalDto>lstActSilvicultural= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC1");
        DocUtil.generarActividadSilviculturalAplicacionPOAC(lstActSilvicultural.getData(), doc);
        //6 B --24--
        ResultClassEntity<ActividadSilviculturalDto>lstActSilviculturalB= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC2");
        DocUtil.generarActividadSilviculturalReforestacionPOAC(lstActSilviculturalB.getData(), doc);

        //7. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosqueBDML = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBDML");
        DocUtil.generarProteccionBosquePOAC(lstProteccionBosqueBDML, doc,"POACPBDML");
        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBAIA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBAIA");
        DocUtil.generarProteccionBosquePOAC(lstProteccionBosquePBAIA, doc,"POACPBAIA");
        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBPGA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBPGA");
        DocUtil.generarProteccionBosquePOAC(lstProteccionBosquePBPGA, doc,"POACPBPGA");


        //8  Monitoreo -- 30
        MonitoreoEntity monitoreo = new MonitoreoEntity();
        monitoreo.setIdPlanManejo(idPlanManejo);
        monitoreo.setCodigoMonitoreo("POAC");
        List<MonitoreoDetalleEntity> lstMonitoreo = monitoreoRepository.listarMonitoreo(monitoreo).getData().getLstDetalle();
        DocUtil.generarMonitoreoPOAC(lstMonitoreo, doc);

        //9. PARTICIPACION COMUNAL 31
        ParticipacionComunalEntity participacionRequest = new ParticipacionComunalEntity();
        participacionRequest.setCodTipoPartComunal("POAC");
        participacionRequest.setIdPlanManejo(idPlanManejo);
        ResultClassEntity<List<ParticipacionComunalEntity>>lstParticipacion= participacionComunalRepository.ListarPorFiltroParticipacionComunal(participacionRequest);
        DocUtil.generarParticipacionComunalPOAC(lstParticipacion.getData(), doc);


        //10. Capacitacion 32

        CapacitacionDto obj = new CapacitacionDto();
        obj.setIdPlanManejo(idPlanManejo);
        obj.setCodTipoCapacitacion("POAC");
        List<CapacitacionDto> lstCapacitacion= capacitacionRepository.ListarCapacitacion(obj);
        DocUtil.generarCapacitacionPOAC(lstCapacitacion,doc);


        //11. Organizacion para el desarrollo 32
        ResultClassEntity<ActividadSilviculturalDto>lstActSilviculturalOrg= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC");
        DocUtil.generarActividadSilviculturalOrganizPOAC(lstActSilviculturalOrg.getData(), doc);

        //12. Rentabilidad Manejo Forestal 33,34,35
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POAC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        //12.1.a Totales 33
        Double[] totales = new Double[2];
        DocUtil.generarTotalesProgramaInversionPOAC(totales,doc);
        //12.1.b Ingresos 34
        List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());
        DocUtil.generarIngresoProgramaInversionPOAC(lstIngresos,doc,totales);
        //12.1.c Egresos 35
        List<RentabilidadManejoForestalDto> lstEgresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());
        DocUtil.generarEgresoProgramaInversionPOAC(lstEgresos,doc,totales);


        //13. Cronograma de Actividades 36
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("POAC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();
        DocUtil.generarCronogramaActividadesPOACAnios(lstcrono,doc);

        //14.	ASPECTOS COMPLEMENTARIOS   (37)
        InformacionGeneralDEMAEntity aspectosComRequest = new InformacionGeneralDEMAEntity();
        aspectosComRequest.setCodigoProceso("POAC");
        aspectosComRequest.setIdPlanManejo(idPlanManejo);
        ResultEntity<InformacionGeneralDEMAEntity> lstAspectos= infoGeneralDemaRepo.listarInformacionGeneralDema(aspectosComRequest);
        DocUtil.generarAspectosCompPOAC(lstAspectos.getData(), doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();



        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        /*
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("DetalleRegente", ".pdf");
        String ruta =archivo.getAbsolutePath();
        FileOutputStream os = new FileOutputStream(archivo);
        Docx4J.toPDF(wordMLPackage,os);
        os.flush();
        os.close();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);
        */

        return new ByteArrayResource(b.toByteArray()); // COMENTAR PARA PROBAR PDF
    }


    public ByteArrayResource consolidadoPOPAC(Integer idPlanManejo) throws Exception{
        XWPFDocument doc = getDoc("formatoPOPAC.docx");

        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POPAC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        /*
        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle = new ArrayList<>();
        if (lstInformacionBasica.getData().size() > 0) {
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        */
        Map<String, String> keys = MapBuilder.keysInfoGeneralPMFIC(o);
        DocUtilPmfic.processFile(keys, doc, o.getLstUmf());

        //2- OBJETIVOS 2,3
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos= objetivoservice.listarObjetivos("POPAC",idPlanManejo);
        DocUtilPopac.procesarObjetivos(lstObjetivos.getData(), doc);

        //3- INFORMACION BASICA

        //4- ASPECTOS FISICOS PMFICAF
        List<InfBasicaAereaDetalleDto> lstInfoBasica = informacionBasicaRepository.listarInfBasicaAerea("POPAC", idPlanManejo, "POPACAF");
        //4.1 - HIDROGRAFIA 9 PMFICAF PMFICAFHUMF
        List<InfBasicaAereaDetalleDto> lstInfoBasicaHID = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("POPACAFHUMF")).collect(Collectors.toList());
        DocUtilPopac.procesarHidrografia(lstInfoBasicaHID,doc);
        //4.2 - FISIOGRAFIA 10 PMFICAF PMFICAFFUMF
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFIS = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("POPACAFFUMF")).collect(Collectors.toList());
        DocUtilPopac.procesarFisiografia(lstInfoBasicaFIS,doc);

        //5 - ASPECTOS BIOLOGICOS
        //5.1 - FAUNA 11 PMFICABFS
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFAU = informacionBasicaRepository.listarInfBasicaAerea("POPAC", idPlanManejo, "POPACABFS");
        DocUtilPopac.procesarFauna(lstInfoBasicaFAU,doc);
        //5.2 - TIPOS DE BOSQUE 12 PMFICABTB
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTB = informacionBasicaRepository.listarInfBasicaAerea("POPAC", idPlanManejo, "POPACABTB");
        DocUtilPopac.procesarTiposBosque(lstInfoBasicaTB,doc);

        //6 - INFORMACION SOCIOECONOMICA
        //6.1 - CARACTERIZACION 13 PMFICISCC
        List<InformacionBasicaEntity> lstInfoBasicaCC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"POPAC", "POPACISCC");

        //6.2 - INFRAESTRUCTURA 14 PMFICISIS
        List<InformacionBasicaEntity> lstInfoBasicaIS = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"POPAC", "POPACISIS");
        DocUtilPopac.procesarInfraestructuraServicios(lstInfoBasicaIS,doc);
        //6.3 - ANTECEDENTES 15 PMFICISAUEC
        List<InformacionBasicaEntity> lstInfoBasicaAUEC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"POPAC", "POPACISAUEC");
        DocUtilPopac.procesarAntecedentes(lstInfoBasicaAUEC,doc);

        //7 - ORDENAMIENTO Y PROTECCION
        OrdenamientoProteccionEntity req = new OrdenamientoProteccionEntity();
        req.setIdPlanManejo(idPlanManejo);
        req.setCodTipoOrdenamiento("POPAC");
        List<OrdenamientoProteccionEntity> lstOrdenProt =ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(req).getData();
        if(lstOrdenProt.size()>0){

        }
        //7.1 - SUPERFICIE Y UBICACION BLOQUES 16 PFMICOPSUB
        List<OrdenamientoProteccionDetalleEntity> lstDetBloq = null;
        if(lstOrdenProt.size()>0) lstDetBloq=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("POPACOPSUB")).collect(Collectors.toList());
        DocUtilPopac.procesarSuperficieBloques(lstDetBloq,doc);
        //7.2 - SUPERFICIE Y UBICACION AREA APROV 17 PFMICOPSUAAPB
        List<OrdenamientoProteccionDetalleEntity> lstDetArea = null;
        if(lstOrdenProt.size()>0) lstDetArea=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("POPACOPSUAAPB")).collect(Collectors.toList());
        DocUtilPopac.procesarSuperficieArea(lstDetArea,doc);
        //7.3 - SUPERFICIE Y UBICACION PARCELA CORTA 18 PFMICOPSUPCAO
        List<OrdenamientoProteccionDetalleEntity> lstDetParc = null;
        if(lstOrdenProt.size()>0) lstDetParc=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("POPACOPSUPCAO")).collect(Collectors.toList());
        DocUtilPopac.procesarSuperficieParcela(lstDetParc,doc);
        //7.4 - MEDIDAS DE PROTECCION 19
        SistemaManejoForestalDto manejo= sistemaManejoForestalService.obtener(idPlanManejo,"POPAC").getData();
        DocUtilPopac.procesarMedidasProteccion(manejo,doc);

        //8 - Potencial de produccion
        //8.1 - Potencial con fines maderables
        //8.1.1a arboles 20
        //8.1.1b fustales 21
        //8.1.2 censo comercial 22
        //8.2 - Potencial con fines no maderables
        //8.2.1 inventario 23
        //8.2.2 censo comercial 24

        //9 - Sistema de manejo forestal
        //9.0 - Uso Potencial 25  PMFICSMFUMCZUMF
        List<ManejoBosqueEntity> lstMB_zumf= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMCZUMF",null);
        DocUtilPopac.procesarZonificacionUMF(lstMB_zumf,doc);
        //9.1 - MF con fines maderables PMFICSMFUMMFFM
        //9.1.1 sistema maderable 26  PMFICSMFUMMFFMSMFCFM
        List<ManejoBosqueEntity> lstMB_FM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMMFFM","POPACSMFUMMFFMSMFCFM");
        DocUtilPopac.procesarSMFinesMaderables(lstMB_FM,doc);
        //9.1.2 ciclo corte 27  PMFICSMFUMMFFMCC
        List<ManejoBosqueEntity> lstMB_CC= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMMFFM","POPACSMFUMMFFMCC");
        DocUtilPopac.procesarSMCicloCorte(lstMB_CC,doc);
        //9.1.3 actividades 28  PMFICSMFUMMFFMAAEUFM
        List<ManejoBosqueEntity> lstMB_AA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMMFFM","POPACSMFUMMFFMAAEUFM");
        DocUtilPopac.procesarSMActividadesAprovechamiento(lstMB_AA,doc);
        //9.2 - MF con fines no maderables  PMFICSMFUMMFFNM
        //9.2.1 sistema no maderable 29  PMFICSMFUMMFFNMSMFNM
        List<ManejoBosqueEntity> lstMB_FNM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMMFFNM","POPACSMFUMMFFNMSMFNM");
        DocUtilPopac.procesarSMFinesNoMaderables(lstMB_FNM,doc);
        //9.2.2 ciclo aprov 30  PMFICSMFUMMFFNMCAPRO
        List<ManejoBosqueEntity> lstMB_CA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMMFFNM","POPACSMFUMMFFNMCAPRO");
        DocUtilPopac.procesarSMCicloAprovechamiento(lstMB_CA,doc);
        //9.2.3 actividades 31  PMFICSMFUMMFFNMAAEFN
        List<ManejoBosqueEntity> lstMB_AANM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","POPACSMFUMMFFNM","POPACSMFUMMFFNMAAEFN");
        DocUtilPopac.procesarSMActividadesAprovechamientoNM(lstMB_AANM,doc);
        //9.3 - Labores silviculturales  PMFICSMFUMLS
        //9.3.1 act obligatorias 32  OBLI
        List<ManejoBosqueEntity> lstMB_LSOB= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","PMFICSMFUMLS","OBLI");
        DocUtilPopac.procesarSMLaboresObligatorias(lstMB_LSOB,doc);
        //9.3.2 act opcionales 33  OPC
        List<ManejoBosqueEntity> lstMB_LSOP= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"POPAC","PMFICSMFUMLS","OPC");
        DocUtilPopac.procesarSMLaboresOpcionales(lstMB_LSOP,doc);

        //10 - Evaluacion Ambiental
        //10.1 identificacion impactos 34
        //10.2.1 acciones preventivo-corrector 35
        //10.2.2 plan igilancia seguimiento 36
        //10.3 acciones contingencia 37

        //11 - Organizacion para el desarrollo
        ResultClassEntity<ActividadSilviculturalDto>lstOrganizacion= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POPAC");
        //11.1 aprovechamiento maderable 38
        List<ActividadSilviculturalDetalleEntity> lstOrgAM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("MAD")).collect(Collectors.toList());
        DocUtilPopac.procesarOrganizacionMaderable(lstOrgAM,doc);
        //11.2 aprovechamiento no maderable 39
        List<ActividadSilviculturalDetalleEntity> lstOrgANM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("NMAD")).collect(Collectors.toList());
        DocUtilPopac.procesarOrganizacionNoMaderable(lstOrgANM,doc);

        //12 - Cronograma 40
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("POPAC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();
        DocUtil.generarCronogramaActividadesPOCC(lstcrono,doc);

        //13 - Rentabilidad de manejo forestal 41,42,43
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POPAC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        //12.1.a Totales 41
        Double[] totales = new Double[2];
        //12.1.b Ingresos 42
        List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());
        DocUtilPopac.generarIngresoRentabilidad(lstIngresos,doc,totales);
        //12.1.c Egresos 43
        List<RentabilidadManejoForestalDto> lstEgresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());
        DocUtilPopac.generarEgresoRentabilidad(lstEgresos,doc,totales);
        //12.1.a Totales 41
        DocUtilPopac.generarTotalesRentabilidad(totales,doc);

        //14 - Aspectos Complementarios 44
        InformacionGeneralDEMAEntity reqDema = new InformacionGeneralDEMAEntity();
        DocUtilPopac.procesarAspectosComplementarios(o,doc);


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }


    /**
     * @autor: Jason Retamozo 07-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado PMFIC}
     * @param: Integer idPlanManejo
     */

    public ByteArrayResource consolidadoPMFIC(Integer idPlanManejo) throws Exception{
        XWPFDocument doc = getDoc("formatoPMFIC.docx");

        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PMFIC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData()!=null?
                infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().size()>0?infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0):null:null;
        /*
        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle = new ArrayList<>();
        if (lstInformacionBasica.getData().size() > 0) {
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        */
        Map<String, String> keys = MapBuilder.keysInfoGeneralPMFIC(o);
        DocUtilPmfic.processFile(keys, doc, o.getLstUmf());

        //2- OBJETIVOS 2,3
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos= objetivoservice.listarObjetivos("PMFIC",idPlanManejo);
        DocUtilPmfic.procesarObjetivos(lstObjetivos.getData(), doc);

        //3- INFORMACION BASICA

        //4- ASPECTOS FISICOS PMFICAF
        List<InfBasicaAereaDetalleDto> lstInfoBasica = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICAF");
        //4.1 - HIDROGRAFIA 9 PMFICAF PMFICAFHUMF
        List<InfBasicaAereaDetalleDto> lstInfoBasicaHID = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("PMFICAFHUMF")).collect(Collectors.toList());
        DocUtilPmfic.procesarHidrografia(lstInfoBasicaHID,doc);
        //4.2 - FISIOGRAFIA 10 PMFICAF PMFICAFFUMF
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFIS = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("PMFICAFFUMF")).collect(Collectors.toList());
        DocUtilPmfic.procesarFisiografia(lstInfoBasicaFIS,doc);

        //5 - ASPECTOS BIOLOGICOS
        //5.1 - FAUNA 11 PMFICABFS
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFAU = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICABFS");
        DocUtilPmfic.procesarFauna(lstInfoBasicaFAU,doc);
        //5.2 - TIPOS DE BOSQUE 12 PMFICABTB
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTB = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICABTB");
        DocUtilPmfic.procesarTiposBosque(lstInfoBasicaTB,doc);

        //6 - INFORMACION SOCIOECONOMICA
        //6.1 - CARACTERIZACION 13 PMFICISCC
        List<InformacionBasicaEntity> lstInfoBasicaCC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISCC");
        DocUtilPmfic.generarCaracterizacionComunidad(lstInfoBasicaCC,doc);
        //6.2 - INFRAESTRUCTURA 14 PMFICISIS
        List<InformacionBasicaEntity> lstInfoBasicaIS = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISIS");
        DocUtilPmfic.procesarInfraestructuraServicios(lstInfoBasicaIS,doc);
        //6.3 - ANTECEDENTES 15 PMFICISAUEC
        List<InformacionBasicaEntity> lstInfoBasicaAUEC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISAUEC");
        DocUtilPmfic.procesarAntecedentes(lstInfoBasicaAUEC,doc);

        //7 - ORDENAMIENTO Y PROTECCION
        OrdenamientoProteccionEntity req = new OrdenamientoProteccionEntity();
        req.setIdPlanManejo(idPlanManejo);
        req.setCodTipoOrdenamiento("PMFIC");
        List<OrdenamientoProteccionEntity> lstOrdenProt =ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(req).getData();
        if(lstOrdenProt.size()>0){
            //7.1 - SUPERFICIE Y UBICACION BLOQUES 16 PFMICOPSUB
            List<OrdenamientoProteccionDetalleEntity> lstDetBloq = null;
            if(lstOrdenProt.size()>0) lstDetBloq=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("PFMICOPSUB")).collect(Collectors.toList());
            DocUtilPmfic.procesarSuperficieBloques(lstDetBloq,doc);
            //7.2 - SUPERFICIE Y UBICACION AREA APROV 17 PFMICOPSUAAPB
            List<OrdenamientoProteccionDetalleEntity> lstDetArea = null;
            if(lstOrdenProt.size()>0) lstDetArea=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("PFMICOPSUAAPB")).collect(Collectors.toList());
            DocUtilPmfic.procesarSuperficieArea(lstDetArea,doc);
            //7.3 - SUPERFICIE Y UBICACION PARCELA CORTA 18 PFMICOPSUPCAO
            List<OrdenamientoProteccionDetalleEntity> lstDetParc = null;
            if(lstOrdenProt.size()>0) lstDetParc=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("PFMICOPSUPCAO")).collect(Collectors.toList());
            DocUtilPmfic.procesarSuperficieParcela(lstDetParc,doc);
        }
        //7.4 - MEDIDAS DE PROTECCION 19
        SistemaManejoForestalDto manejo= sistemaManejoForestalService.obtener(idPlanManejo,"PMFIC").getData();
        DocUtilPmfic.procesarMedidasProteccion(manejo,doc);

        //8 - Potencial de produccion
        //8.1 - Potencial con fines maderables
        //8.1.1a arboles 20
        //8.1.1b fustales 21
        //8.1.2 censo comercial 22
        //8.2 - Potencial con fines no maderables
        //8.2.1 inventario 23
        //8.2.2 censo comercial 24

        //9 - Sistema de manejo forestal
        //9.0 - Uso Potencial 25  PMFICSMFUMCZUMF
        List<ManejoBosqueEntity> lstMB_zumf= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMCZUMF",null);
        DocUtilPmfic.procesarZonificacionUMF(lstMB_zumf,doc);
        //9.1 - MF con fines maderables PMFICSMFUMMFFM
        //9.1.1 sistema maderable 26  PMFICSMFUMMFFMSMFCFM
        List<ManejoBosqueEntity> lstMB_FM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMSMFCFM");
        DocUtilPmfic.procesarSMFinesMaderables(lstMB_FM,doc);
        //9.1.2 ciclo corte 27  PMFICSMFUMMFFMCC
        List<ManejoBosqueEntity> lstMB_CC= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMCC");
        DocUtilPmfic.procesarSMCicloCorte(lstMB_CC,doc);
        //9.1.3 actividades 28  PMFICSMFUMMFFMAAEUFM
        List<ManejoBosqueEntity> lstMB_AA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMAAEUFM");
        DocUtilPmfic.procesarSMActividadesAprovechamiento(lstMB_AA,doc);
        //9.2 - MF con fines no maderables  PMFICSMFUMMFFNM
        //9.2.1 sistema no maderable 29  PMFICSMFUMMFFNMSMFNM
        List<ManejoBosqueEntity> lstMB_FNM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMSMFNM");
        DocUtilPmfic.procesarSMFinesNoMaderables(lstMB_FNM,doc);
        //9.2.2 ciclo aprov 30  PMFICSMFUMMFFNMCAPRO
        List<ManejoBosqueEntity> lstMB_CA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMCAPRO");
        DocUtilPmfic.procesarSMCicloAprovechamiento(lstMB_CA,doc);
        //9.2.3 actividades 31  PMFICSMFUMMFFNMAAEFN
        List<ManejoBosqueEntity> lstMB_AANM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMAAEFN");
        DocUtilPmfic.procesarSMActividadesAprovechamientoNM(lstMB_AANM,doc);
        //9.3 - Labores silviculturales  PMFICSMFUMLS
        //9.3.1 act obligatorias 32  OBLI
        List<ManejoBosqueEntity> lstMB_LSOB= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMLS","OBLI");
        DocUtilPmfic.procesarSMLaboresObligatorias(lstMB_LSOB,doc);
        //9.3.2 act opcionales 33  OPC
        List<ManejoBosqueEntity> lstMB_LSOP= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMLS","OPC");
        DocUtilPmfic.procesarSMLaboresOpcionales(lstMB_LSOP,doc);

        //10 - Evaluacion Ambiental
        //10.1 identificacion impactos 34
        EvaluacionAmbientalActividadEntity evalAmb=new EvaluacionAmbientalActividadEntity();
        evalAmb.setIdPlanManejo(idPlanManejo);
        evalAmb.setTipoActividad("FACTOR");
        List<EvaluacionAmbientalActividadEntity> lstEvalAmbiental= evaluacionAmbientalRepository.ListarEvaluacionActividadFiltro(evalAmb);

        EvaluacionAmbientalDto evalAmbAcciones=new EvaluacionAmbientalDto();
        evalAmbAcciones.setIdPlanManejo(idPlanManejo);
        List<EvaluacionAmbientalDto> lstEvalAmbientalAcciones= evaluacionAmbientalRepository.ListarEvaluacionAmbiental(evalAmbAcciones);
        DocUtilPmfic.generarEvalAmbientalImpactosAmb(lstEvalAmbiental,lstEvalAmbientalAcciones,doc);
        //10.2.1 acciones preventivo-corrector 35
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion = lstEvaluacion.stream()
                .filter(p->p.getCodTipoAprovechamiento().equals("POAPRO")).collect(Collectors.toList());
        DocUtilPmfic.generarPlanAccionEvaluacionImpactoAmbiental(lstPlanAccion,doc);
        //10.2.2 plan igilancia seguimiento 36
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanVigilancia = lstPlanAccion.stream()
                .filter(p-> !p.getImpacto().trim().equals("")).collect(Collectors.toList());
        DocUtilPmfic.generarPlanVigilanciaEvaluacionImpactoAmbiental(lstPlanVigilancia,doc);
        //10.3 acciones contingencia 37
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanContingencia = lstEvaluacion.stream()
                .filter(p->p.getCodTipoAprovechamiento().equals("POEAPR")).collect(Collectors.toList());
        DocUtilPmfic.generarPlanContingenciaEvaluacionImpactoAmbiental(lstPlanContingencia,doc);

        //11 - Organizacion para el desarrollo
        ResultClassEntity<ActividadSilviculturalDto>lstOrganizacion= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"PMFIC");
        //11.1 aprovechamiento maderable 38
        List<ActividadSilviculturalDetalleEntity> lstOrgAM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("MAD")).collect(Collectors.toList());
        DocUtilPmfic.procesarOrganizacionMaderable(lstOrgAM,doc);
        //11.2 aprovechamiento no maderable 39
        List<ActividadSilviculturalDetalleEntity> lstOrgANM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("NMAD")).collect(Collectors.toList());
        DocUtilPmfic.procesarOrganizacionNoMaderable(lstOrgANM,doc);

        //12 - Cronograma 40
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PMFIC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();
        DocUtilPmfic.generarCronogramaActividadesPOCC(lstcrono,doc);

        //13 - Rentabilidad de manejo forestal 41,42,43
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("PMFIC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        //12.1.a Totales 41
        Double[] totales = new Double[2];
        //12.1.b Ingresos 42
        List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());
        DocUtilPmfic.generarIngresoRentabilidad(lstIngresos,doc,totales);
        //12.1.c Egresos 43
        List<RentabilidadManejoForestalDto> lstEgresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());
        DocUtilPmfic.generarEgresoRentabilidad(lstEgresos,doc,totales);
        //12.1.a Totales 41
        DocUtilPmfic.generarTotalesRentabilidad(totales,doc);

        //14 - Aspectos Complementarios 44
        InformacionGeneralDEMAEntity reqDema = new InformacionGeneralDEMAEntity();
        DocUtilPmfic.procesarAspectosComplementarios(o,doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos PMFIC}
     * @param: Integer idPlanManejo
     */

    public ByteArrayResource consolidadoAnexosPMFIC(Integer idPlanManejo) throws Exception{
        XWPFDocument doc = getDoc("formatoAnexoPMFIC.docx");

        //Anexo 4
        List <Anexo3PGMFDto> listaAnexo4=censoForestalRepository.ResultadosFormatoPGMFAnexo3(idPlanManejo,"PMFIC","2","");
        DocUtilPmfic.generarAnexo4 (listaAnexo4, doc);

        //Anexo 5
        List <Anexo2Dto> listaAnexo5=censoForestalRepository.ResultadosPOConcesionesAnexos2(idPlanManejo,"PMFIC",null,null);
        DocUtilPmfic.generarAnexo5 (listaAnexo5, doc);

        //Anexo 6
        List <ResultadosAnexo6> listaAnexo6=censoForestalRepository.ResultadosAnexo6(idPlanManejo,"PMFIC");
        DocUtilPmfic.generarAnexo6 (listaAnexo6, doc);

        //Anexo 7
        List <Anexo2Dto> listaAnexo7=censoForestalRepository.ResultadosAnexo7NoMaderable(idPlanManejo,"PMFIC");
        DocUtilPmfic.generarAnexo7 (listaAnexo7, doc);

        //Anexo 8
        List <ListaEspecieDto> listaAnexo8=censoForestalRepository.ListaEspeciesInventariadasMaderables(idPlanManejo,"PMFIC");
        DocUtilPmfic.generarAnexo8 (listaAnexo8, doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado de anexos PMFIC}
     * @param: Integer idPlanManejo
     */

    public ByteArrayResource consolidadoAnexosPOPAC(Integer idPlanManejo) throws Exception{
        XWPFDocument doc = getDoc("formatoAnexoPOPAC.docx");

        //Anexo 4
        List <Anexo3PGMFDto> listaAnexo4=censoForestalRepository.ResultadosFormatoPGMFAnexo3(idPlanManejo,"POPAC","2","");
        DocUtilPopac.generarAnexo4 (listaAnexo4, doc);

        //Anexo 5
        List <Anexo2Dto> listaAnexo5=censoForestalRepository.ResultadosPOConcesionesAnexos2(idPlanManejo,"POPAC",null,null);
        DocUtilPopac.generarAnexo5 (listaAnexo5, doc);

        //Anexo 6
        List <ResultadosAnexo6> listaAnexo6=censoForestalRepository.ResultadosAnexo6(idPlanManejo,"POPAC");
        DocUtilPopac.generarAnexo6 (listaAnexo6, doc);

        //Anexo 7
        List <Anexo2Dto> listaAnexo7=censoForestalRepository.ResultadosAnexo7NoMaderable(idPlanManejo,"POPAC");
        DocUtilPopac.generarAnexo7 (listaAnexo7, doc);

        //Anexo 8
        List <ListaEspecieDto> listaAnexo8=censoForestalRepository.ListaEspeciesInventariadasMaderables(idPlanManejo,"POPAC");
        DocUtilPopac.generarAnexo8 (listaAnexo8, doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }


    /**
     * @autor: Jaqueline Diaz Barrientos 27-09-2021
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado PMFI}
     * @param: Integer idPlanManejo
     */
    @Override
    public ByteArrayResource consolidadoPMFI(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPFMI.docx");
        //1 - informacion general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PMFI");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        DocUtil.procesoInformacionGeneralPMFI(o, doc);
        //2 - Objetivos
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        objetivo.setCodigoObjetivo("PMFI");
        objetivo.setIdPlanManejo(idPlanManejo);
        ResultEntity<ObjetivoManejoEntity> lstObjetivos= objetivoservice.listarObjetivoManejo(objetivo);
        DocUtil.procesarObjetivosPMFI(lstObjetivos.getData(), doc);
        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaUbiPolitica = informacionBasicaRepository.listarInfBasicaAerea("PMFI", idPlanManejo, "PMFIAM");
        DocUtil.generarInfoAreaUbiPolitica(lstInfoAreaUbiPolitica, doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaAP = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIAP");
        DocUtil.generarInfoAreAP(lstInfoAreaAP,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaAM = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIAM");
        DocUtil.generarInfoAreAM(lstInfoAreaAM,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaACC1 = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFACC1");
        DocUtil.generarInfoAreACC1(lstInfoAreaACC1,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaACC2 = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFACC2");
        DocUtil.generarInfoAreACC2(lstInfoAreaACC2,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaRio = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFRIO");
        DocUtil.generarInfoAreRIO(lstInfoAreaRio,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaUF = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIUF");
        DocUtil.generarInfoAreUF(lstInfoAreaUF,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaZV = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIZV");
        DocUtil.generarInfoAreZV(lstInfoAreaZV,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaFAU = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIFAU");
        List<EspecieFaunaEntity> lstFauna = new ArrayList<>();
        for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lstInfoAreaFAU) {
            EspecieFaunaEntity request = new EspecieFaunaEntity();
            short idEspecie =infBasicaAereaDetalleDto.getIdFauna().shortValue();
            request.setIdEspecie(idEspecie);
            ResultEntity<EspecieFaunaEntity>lstListdoFauna= corecentral.ListaPorFiltroEspecieFauna(request);
            if(lstListdoFauna!=null)
                lstFauna.add(lstListdoFauna.getData().get(0));
        }  
        DocUtil.generarInfoAreFAAU(lstFauna,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaFLO = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIFLOR");
        List<EspecieForestalEntity> lstFlora = new ArrayList<>();
        for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lstInfoAreaFLO) {
            EspecieForestalEntity request = new EspecieForestalEntity();
            short idEspecie =infBasicaAereaDetalleDto.getIdFlora().shortValue();
            request.setIdEspecie(idEspecie);
            ResultEntity<EspecieForestalEntity>lstFloraTotal= corecentral.ListaPorFiltroEspecieForestal(request);
            if(lstFloraTotal!=null)
                lstFlora.add(lstFloraTotal.getData().get(0));
        }   
        DocUtil.generarInfoAreFLO(lstFlora,doc);
        List<InfBasicaAereaDetalleDto> lstInfoAreaOrganiza = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIASP");
        DocUtil.generarInfoAreOrganizacion(lstInfoAreaOrganiza,doc);


        //4.	ORDENAMIENTO INTERNO
        OrdenamientoInternoDetalleDto ordenaRequest = new OrdenamientoInternoDetalleDto();
        ordenaRequest.setIdPlanManejo(idPlanManejo);
        ordenaRequest.setCodTipoOrdenamiento("PMFI");
        //ResultEntity res= ordenamientoRepo.ListarOrdenamientoInterno(ordenaRequest);
        List<OrdenamientoInternoDetalleDto> lstOrdenamiento = ordenamientoRepo.ListaROrdenamientoInternoPMFI(ordenaRequest);
        DocUtil.generarOrdenamientoInterno(lstOrdenamiento,doc);
        /*OrdenamientoProteccionEntity ordenamientoEntityList =  (OrdenamientoProteccionEntity)res.getData();
        DocUtil.generarOrdenamientoInterno(ordenamientoEntityList,doc);*/

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaRecursoNoMaderable = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"IDEN");
        DocUtil.generarIdentiEspecieNoMaderable(lstListaRecursoNoMaderable,doc);
        List<RecursoForestalDetalleDto> lstListaRecursoMaderable = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"IDEN");
        DocUtil.generarIdentiEspecieMaderable(lstListaRecursoMaderable,doc);
        List<RecursoForestalDetalleDto> lstListaFechaRealizacionNO = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"INVE");
        DocUtil.generarRealizacionNO(15,lstListaFechaRealizacionNO,doc);

        List<RecursoForestalDetalleDto> lstListaInventarioANO = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"INVE");
        DocUtil.generarInventarioANO(16,lstListaInventarioANO,doc);
        List<RecursoForestalDetalleDto> lstListaInventarioBNO = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"RESU");
        DocUtil.generarInventarioBNO(17,lstListaInventarioBNO,doc);

        List<RecursoForestalDetalleDto> lstListaFechaRealizacion = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"INVE");
        DocUtil.generarRealizacion(18,lstListaFechaRealizacion,doc);
        List<RecursoForestalDetalleDto> lstListaInventarioA = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"INVE");
        DocUtil.generarInventarioA(19,lstListaInventarioA,doc);
        List<RecursoForestalDetalleDto> lstListaInventarioB = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"RESU");
        DocUtil.generarInventarioB(20,lstListaInventarioB,doc);

        //List<RecursoForestalDetalleDto> resultListInventarioB = Stream.concat(lstListaInventarioB.stream(), lstListaInventarioBNO.stream()).collect(Collectors.toList());

        //6.	SISTEMA DE MANEJO, APROVECHAMIENTO Y LABORES SILVICULTURALES
        //6.1
        ResultClassEntity<ActividadSilviculturalDto>lstEtapastotal= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"5");
        DocUtil.generarEtapasPMFI(lstEtapastotal.getData(), doc);
        //6.2
        ResultClassEntity<List<ActividadSilviculturalDto>> lstSivicultural = actividadSilviculturalRepository.ListarActividadSilviculturalCabecera(idPlanManejo);
        DocUtil.generarCicloCortaPMFI(lstSivicultural.getData(), doc);
        //6.3
        ResultClassEntity<ActividadSilviculturalDto>lstLabores= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"6");
        DocUtil.generarLaboresSilviculturalesPMFI(lstLabores.getData(), doc);
        //7.	MEDIDAS DE PROTECCIÓN DE LA UNIDAD DE MANEJO FORESTAL
        MedidasPmfiDto medidas = new MedidasPmfiDto();
        medidas.setIdPlanManejo(idPlanManejo);
        medidas.setCodigoImpactoAmbiental("PMFIMP");
        medidas.setCodigoImpactoAmbientalDet("PMFIMPPRO");
        ResultClassEntity response = ambiental.ListarImpactoAmbientalPmfi(medidas);
        ImpactoAmbientalPmfiDto impactoAmbientalEntityList =  (ImpactoAmbientalPmfiDto)response.getData();
        DocUtil.generarMedidasPMFI(impactoAmbientalEntityList, doc);
        //8.	IDENTIFICACIÓN DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCIÓN Y MITIGACIÓN
        MedidasPmfiDto impacto = new MedidasPmfiDto();
        impacto.setIdPlanManejo(idPlanManejo);
        impacto.setCodigoImpactoAmbiental( "PMFIIA");
        impacto.setCodigoImpactoAmbientalDet("PMFIIAMIT");
        ResultClassEntity responseImpacto = ambiental.ListarImpactoAmbientalPmfi(impacto);
        ImpactoAmbientalPmfiDto identificacionImpactos =  (ImpactoAmbientalPmfiDto)responseImpacto.getData();
        DocUtil.generarIdentificacionImpactosPMFI(identificacionImpactos, doc);
        //9.	CRONOGRAMA DE ACTIVIDADES PARA EL PERIODO DE EJECUCIÓN
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PMFI");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        ResultEntity<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request);
        DocUtil.procesarCronogramaPMFI(lstcrono.getData(), doc);
       
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }


    @Override
    public ByteArrayResource generarEvaluacionCompiladaPGMF(Integer idPlanManejo,Integer idPlanManejoEvaluacion, String nombreUsuarioArffs, String tipoDocumento) throws Exception {
        XWPFDocument doc = getDoc("plantillaPGMF.docx");

        ResultEntity<PlanManejoEvaluacionDetalleDto> result = planManejoEvaluacionService.ListarPlanManejoEvaluacionDetalle(idPlanManejoEvaluacion);
        PlanManejoArchivoEntity planManejoEntity = new PlanManejoArchivoEntity();
        planManejoEntity.setIdPlanManejo(idPlanManejo);
        List<PlanManejoArchivoEntity> listArchivos = (List<PlanManejoArchivoEntity>)planManejoRepository.ListarPorFiltroPlanManejoArchivo(planManejoEntity).getData();

        List<PlanManejoEvaluacionDetalleDto> lista = result.getData();

        if(listArchivos.size()==0) {
            listArchivos.add(new PlanManejoArchivoEntity());
        }
        DocUtil.setearRequisitosTUPA(listArchivos, doc);

        DocUtil.generarDocumentoEvaluacionPlanManejoForestal(idPlanManejo.toString(),lista, doc);

        DocUtil.setearConsideracionesEspeciales(lista, doc);

        DocUtil.setearEspeciesInventariadas(lista, doc);

        //Setear nombre de titular
       // PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto = new PlanManejoEvaluacionIterDto();
        //planManejoEvaluacionIterDto.setIdPlanManejo(idPlanManejo);
        //planManejoEvaluacionIterDto.setPageNum(1);
        ///////planManejoEvaluacionIterDto.setPageSize(10);
        //List<PlanManejoEvaluacionIterDto> resultDet = planManejoEvaluacionIterRepository.listarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto).getData();

        //Integer idPlanManejoEvaluacion = planManejoEvaluacionRepository.obtenerUltimaEvaluacion(idPlanManejo).getCodigo();
        //idPlanManejoEvaluacion = 22;
        if(idPlanManejoEvaluacion!=null){
            PlanManejoEvaluacionDetalleDto param90 = new PlanManejoEvaluacionDetalleDto();
            param90.setIdPlanManejoEvaluacion(idPlanManejoEvaluacion);
            param90.setIdTipoParametro(90);

            PlanManejoEvaluacionDetalleDto param91 = new PlanManejoEvaluacionDetalleDto();
            param91.setIdPlanManejoEvaluacion(idPlanManejoEvaluacion);
            param91.setIdTipoParametro(91);
            

            List<PlanManejoEvaluacionDetalleDto> list90 = (List<PlanManejoEvaluacionDetalleDto>) planManejoEvaluacionRepository.ListarPlanManejoEvaluacionLineamiento(param90).getData();
            List<PlanManejoEvaluacionDetalleDto> list91 = (List<PlanManejoEvaluacionDetalleDto>) planManejoEvaluacionRepository.ListarPlanManejoEvaluacionLineamiento(param91).getData();

            DocUtil.setearLineamientos1y2("90", list90, doc);
            DocUtil.setearLineamientos1y2("91", list91, doc);


            List<PlanManejoEvaluacionDetalleDto> listaLin = planManejoEvaluacionDetalleRepository.listarPlanManejoEvaluacionDet(idPlanManejoEvaluacion);
            String strLin="PGMFEVALLIN3.1|PGMFEVALLIN3.2|PGMFEVALLIN3.3|PGMFEVALLIN4|PGMFEVALLIN5.6|PGMFEVALLIN5.1|PGMFEVALLIN5.2|PGMFEVALLIN5.3|PGMFEVALLIN5.4|PGMFEVALLIN5.5|PGMFEVALLIN6|PGMFEVALLIN7|PGMFEVALLIN8|PGMFEVALLIN9|PGMFEVALLIN9.2|PGMFEVALLIN9.3|PGMFEVALLIN10.1|PGMFEVALLIN10.2|PGMFEVALLIN10.3|PGMFEVALLIN10.4|PGMFEVALLIN10.5|PGMFEVALLIN10.6.1|PGMFEVALLIN10.7|PGMFEVALLIN10.8|PGMFEVALLIN10.9|PGMFEVALLIN10|PGMFEVALLIN12.1|PGMFEVALLIN12.2|PGMFEVALLIN13|PGMFEVALLIN14.1|PGMFEVALLIN14.2|PGMFEVALLIN14.3|PGMFEVALLIN14.4|PGMFEVALLIN14|PGMFEVALLIN15|PGMFEVALLIN16.1|PGMFEVALLIN16.2|PGMFEVALLIN16.3|PGMFEVALLIN16|PGMFEVALLIN17|PGMFEVALLIN18|PGMFEVALLIN12";
            List<PlanManejoEvaluacionDetalleDto> listaLinFinal =listaLin.stream().filter(eval -> eval.getCodigoTipo().matches(strLin)).collect(Collectors.toList());

            if(listaLinFinal.size()==0) {
                listaLinFinal.add(new PlanManejoEvaluacionDetalleDto());
            }
            DocUtil.setearLineamientos3Y18("92", listaLinFinal, doc);


            ResultClassEntity resultPlanEval = planManejoEvaluacionService.obtenerEvaluacionPlanManejo(idPlanManejo);
            List<NotificacionDTO> resultPlanEval2= (List<NotificacionDTO>) resultPlanEval.getData();
            NotificacionDTO evaluacion=resultPlanEval2.get(0);

            List<SolicitudOpinionDto> listaSol=new ArrayList<>();
            SolicitudOpinionDto  solicitudOpinionDto= new SolicitudOpinionDto();
            solicitudOpinionDto.setIdSolicitudOpinion(evaluacion.getIdAna());
            ResultClassEntity resultOpinionAna=solicitudOpinionRepository.ObtenerSolicitudOpinion(solicitudOpinionDto);
            if(resultOpinionAna.getSuccess()){
                SolicitudOpinionDto  solicitudOpinionAnaRs=(SolicitudOpinionDto) resultOpinionAna.getData();
                if(solicitudOpinionAnaRs.getIdSolicitudOpinion()!=null){
                    listaSol.add(solicitudOpinionAnaRs);
                }

            }


            SolicitudOpinionDto  solicitudOpinionDto2= new SolicitudOpinionDto();
            solicitudOpinionDto2.setIdSolicitudOpinion(evaluacion.getIdMincul());
            ResultClassEntity resultOpinionMincul=solicitudOpinionRepository.ObtenerSolicitudOpinion(solicitudOpinionDto2);
            if(resultOpinionMincul.getSuccess()) {
                SolicitudOpinionDto  solicitudOpinionMinculRs=(SolicitudOpinionDto) resultOpinionMincul.getData();
                if(solicitudOpinionMinculRs.getIdSolicitudOpinion()!=null) {
                    listaSol.add(solicitudOpinionMinculRs);
                }
            }

            SolicitudOpinionDto  solicitudOpinionDto3= new SolicitudOpinionDto();
            solicitudOpinionDto3.setIdSolicitudOpinion(evaluacion.getIdSernap());
            ResultClassEntity resultOpinionSernap=solicitudOpinionRepository.ObtenerSolicitudOpinion(solicitudOpinionDto3);
            if(resultOpinionSernap.getSuccess()) {
                SolicitudOpinionDto  solicitudOpinionSernapRs=(SolicitudOpinionDto) resultOpinionSernap.getData();
                if(solicitudOpinionSernapRs.getIdSolicitudOpinion()!=null) {
                    listaSol.add(solicitudOpinionSernapRs);
                }
            }
            if(listaSol.size()==0) {
                listaSol.add(new SolicitudOpinionDto());
            }
            DocUtil.setearSolicitudesOpinionesExternas(listaSol, doc);

            EvaluacionCampoEntity evaluacionCampoEntity=new EvaluacionCampoEntity();
            evaluacionCampoEntity.setIdEvaluacionCampo(evaluacion.getIdEvaluacionCampo());
            ResultClassEntity resulEvalCampo=evaluacionCampoRespository.obtenerEvaluacionCampo(evaluacionCampoEntity);
            if(resulEvalCampo.getSuccess()) {
                DocUtil.setearSolicitudCampo((EvaluacionCampoEntity) resulEvalCampo.getData(), doc);
            }

        }


        InformacionGeneralDto info = infoGeneralRepo.obtener(idPlanManejo).getData();
        String nombreTitularEvaluacion =  "-";
        if(info.getIdPlanManejo()!=null) {
            nombreTitularEvaluacion = info.getNombreTitular();
        }

        DocUtil.setearTitularRegente(nombreTitularEvaluacion, nombreUsuarioArffs, tipoDocumento, doc);
        //1 - informacion general
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    @Override
    public ResultClassEntity obtenerArchivosCompiladosRelacionados(CompiladoPgmfDto compiladoPgmfDto)
            throws Exception {

        ResultClassEntity response = new ResultClassEntity();
        ByteArrayResource bytes = generarEvaluacionCompiladaPGMF(compiladoPgmfDto.getIdPlanManejo(), compiladoPgmfDto.getIdPlanManejoEvaluacion(),
                compiladoPgmfDto.getNombreUsuarioArffs(), compiladoPgmfDto.getTipoDocumento());

        List<ResultArchivoEntity> archivos = new ArrayList<ResultArchivoEntity>();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("evaluacionCompiladaPGMF.docx");
        word.setTipoDocumento("COMPILADO");
        word.setArchivo(bytes.getByteArray());
        archivos.add(word);

        Integer idArchivoVerificacionCampo = (Integer)planManejoRepository.obtenerArchivoVerificacionCampo(compiladoPgmfDto.getIdPlanManejoEvaluacion()).getData();
        if(idArchivoVerificacionCampo!=0 && idArchivoVerificacionCampo!=null){
            ArchivoEntity archivo = new ArchivoEntity();
            archivo.setIdArchivo(idArchivoVerificacionCampo);
            ResultClassEntity evaluacionCampo = archivoRepository.DescargarArchivoGeneral(archivo);
            archivos.add((ResultArchivoEntity)evaluacionCampo.getData());
        }
        Integer[] idsArchivosSolicitudOpinion = (Integer[])planManejoRepository.obtenerArchivoSolicitudOpinion(compiladoPgmfDto.getIdPlanManejoEvaluacion()).getData();
        if(idsArchivosSolicitudOpinion !=null) {
            if(idsArchivosSolicitudOpinion[0] !=null)  {
                ArchivoEntity archivo = new ArchivoEntity();
                archivo.setIdArchivo((Integer)idsArchivosSolicitudOpinion[0]);
                ResultClassEntity fileMincul = archivoRepository.DescargarArchivoGeneral(archivo);
                archivos.add((ResultArchivoEntity)fileMincul.getData());
            }
            if(idsArchivosSolicitudOpinion[1] !=null) {
                ArchivoEntity archivo = new ArchivoEntity();
                archivo.setIdArchivo((Integer)idsArchivosSolicitudOpinion[1]);
                ResultClassEntity  fileAna = archivoRepository.DescargarArchivoGeneral(archivo);
                archivos.add((ResultArchivoEntity)fileAna.getData());
            }
            if(idsArchivosSolicitudOpinion[2] !=null) {
                ArchivoEntity archivo = new ArchivoEntity();
                archivo.setIdArchivo((Integer)idsArchivosSolicitudOpinion[2]);
                ResultClassEntity fileSenarp = archivoRepository.DescargarArchivoGeneral(archivo);
                archivos.add((ResultArchivoEntity)fileSenarp.getData());
            }
        }

        response.setSuccess(true);
        response.setData(archivos);
        response.setMessage("Archivos descargados correctamente");
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity regitrarArchivosPlanManejo(Integer idUsuarioRegistro, Integer idPlanManejo, String idTipoDocumento, MultipartFile[] attachments) throws Exception{

        ResultClassEntity response = new ResultClassEntity();

        PGMFArchivoEntity obj = new PGMFArchivoEntity();
        
        for( MultipartFile listaFile : attachments){
            response = serArchivo.RegistrarArchivoGeneral(listaFile, idUsuarioRegistro, idTipoDocumento);

            if(response.getSuccess()){
                obj.setIdArchivo(response.getCodigo());
                obj.setEstado("A");
                obj.setCodigoTipoPGMF(null);
                obj.setCodigoSubTipoPGMF(null);
                obj.setIdPlanManejo(idPlanManejo);
                obj.setIdUsuarioRegistro(idUsuarioRegistro);
                
                response = pgmfArchivoRepo.registrarPlanManejoArchivo(obj);
            }
            
        }
        return response;
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarArchivoPlanManejo(Integer idPlanManejoArchivo,Integer idUsuarioRegistro, Integer idPlanManejo, String codTipo,String codTipoDocumento,String subCodTipoDocumento, MultipartFile file) throws Exception{

        ResultClassEntity response = new ResultClassEntity();

        PGMFArchivoEntity obj = new PGMFArchivoEntity();

            response = serArchivo.RegistrarArchivoGeneral(file, idUsuarioRegistro, codTipoDocumento);
            if(response.getSuccess()){
                obj.setIdArchivo(response.getCodigo());
                obj.setEstado("A");
                obj.setCodigoTipoPGMF(codTipo);
                obj.setCodigoSubTipoPGMF(subCodTipoDocumento);
                obj.setIdPlanManejo(idPlanManejo);
                obj.setIdUsuarioRegistro(idUsuarioRegistro);
                response = pgmfArchivoRepo.registrarPlanManejoArchivo(obj);


        }
        return response;
    }
    @Override
    public ResultClassEntity eliminarPlanManejoArchivo(PlanManejoArchivoDto request ) throws Exception{

        return planManejoRepository.eliminarPlanManejoArchivo(request);
    }

    @Override
    public ResultClassEntity obtenerPlanManejoArchivo(PlanManejoArchivoDto request ) throws Exception{

        return planManejoRepository.obtenerPlanManejoArchivo(request);
    }
    @Override
    public ResultClassEntity actualizarPlanManejoArchivo(PlanManejoArchivoDto request ) throws Exception{

        return planManejoRepository.actualizarPlanManejoArchivo(request);
    }

    @Override
    public ResultEntity<PlanManejoEvaluacionIterDto> listarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) {
        return planManejoEvaluacionIterRepository.listarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
    }

    @Override
    public ResultClassEntity registrarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception {
        return planManejoEvaluacionIterRepository.registrarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
    }

    @Override
    public ResultClassEntity actualizarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception {
        return planManejoEvaluacionIterRepository.actualizarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
    }

    @Override
    public ResultClassEntity eliminarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception {
        return  planManejoEvaluacionIterRepository.eliminarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
    }

    @Override
    public ResultClassEntity cambiarEstadoPlanManejo(Integer idPlanManejo, Integer idUsuario) throws Exception {

        ResultClassEntity result = new  ResultClassEntity();

        ResultClassEntity rsObtenerPlan =planManejoRepository.obtenerPlanManejoTitular(idPlanManejo);

        PlanManejoDto plan= (PlanManejoDto) rsObtenerPlan.getData();

        PlanManejoEvaluacionDto planManejoEvaluacionDto;
        PlanManejoDto planManejo;

        ResultClassEntity rsObtenerUltEval = null;
        Integer ultIdEval = null;

        if(plan.getCodigoEstado()!=null) {
            switch (plan.getCodigoEstado()) {

                    case "EPLMBOR": //borrador
                    case "EPLMCOMP": //completado

                    planManejoEvaluacionDto = new PlanManejoEvaluacionDto();
                    planManejoEvaluacionDto.setIdPlanManejo(plan.getIdPlanManejo());
                    planManejoEvaluacionDto.setIdTitularRegente(plan.getIdTitular());
                    Date fechaActual = new Date();
                    planManejoEvaluacionDto.setFechaPresentacion(fechaActual);
                    planManejoEvaluacionDto.setEstadoPlanManejoEvaluacion("EEVAPRES");//presentado
                    planManejoEvaluacionDto.setFechaEstadoEvaluacion(fechaActual);
                    planManejoEvaluacionDto.setIdUsuarioRegistro(idUsuario);
                    planManejoEvaluacionRepository.registrarPlanManejoEvaluacion(planManejoEvaluacionDto);

                    planManejo = new PlanManejoDto();
                    planManejo.setIdPlanManejo(idPlanManejo);
                    planManejo.setCodigoEstado("EPLMPRES");//presentado
                    planManejo.setIdUsuarioModificacion(idUsuario);
                    planManejoRepository.actualizarPlanManejoEstado(planManejo);

                    break;
                    case "EPLMOBSM": //observado

                        planManejoEvaluacionDto = new PlanManejoEvaluacionDto();

                        rsObtenerUltEval =planManejoEvaluacionRepository.obtenerUltimaEvaluacion(plan.getIdPlanManejo());
                        ultIdEval= (Integer) rsObtenerUltEval.getCodigo();
                        planManejoEvaluacionDto.setIdPlanManejoEval(ultIdEval);
                        planManejoEvaluacionDto.setEstadoPlanManejoEvaluacion("EEVAPRES");//presentado
                        planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planManejoEvaluacionDto);

                        planManejo = new PlanManejoDto();
                        planManejo.setIdPlanManejo(idPlanManejo);
                        planManejo.setCodigoEstado("EPLMPRES");//presentado
                        planManejo.setIdUsuarioModificacion(idUsuario);
                        planManejoRepository.actualizarPlanManejoEstado(planManejo);

                    break;

                    case "EPLMOBSG": //observado t

                        planManejoEvaluacionDto = new PlanManejoEvaluacionDto();

                        rsObtenerUltEval =planManejoEvaluacionRepository.obtenerUltimaEvaluacion(plan.getIdPlanManejo());
                        ultIdEval= (Integer) rsObtenerUltEval.getCodigo();
                        planManejoEvaluacionDto.setIdPlanManejoEval(ultIdEval);
                        planManejoEvaluacionDto.setEstadoPlanManejoEvaluacion("EEVARECI");//recibido
                        planManejoEvaluacionRepository.actualizarEstadoPlanManejoEvaluacion(planManejoEvaluacionDto);

                        planManejo = new PlanManejoDto();
                        planManejo.setIdPlanManejo(idPlanManejo);
                        planManejo.setCodigoEstado("EPLMRECI");//recibido
                        planManejo.setIdUsuarioModificacion(idUsuario);
                        planManejoRepository.actualizarPlanManejoEstado(planManejo);

                    break;                    



            }
        }

        result.setMessage("Se actualizó estado");
        return result;


    }

    @Override
    public ResultClassEntity<PlanManejoObtenerContrato> listarPlanManejoContrato(Integer idPlanManejo) throws Exception {
        return planManejoRepository.listarPlanManejoContrato(idPlanManejo);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarConformePlanManejoArchivo(Integer idPlanManejo, Integer idUsuarioModificacion) throws Exception{

        PlanManejoArchivoEntity planManejoEntity = new PlanManejoArchivoEntity();
        planManejoEntity.setIdPlanManejo(idPlanManejo);
        List<PlanManejoArchivoEntity> listArchivos = (List<PlanManejoArchivoEntity>)planManejoRepository.ListarPorFiltroPlanManejoArchivo(planManejoEntity).getData();

        PlanManejoArchivoDto dto = null;

        for (PlanManejoArchivoEntity planManejoArchivoEntity : listArchivos) {
            dto = new PlanManejoArchivoDto();
            dto.setIdPlanManejoArchivo(planManejoArchivoEntity.getIdPlanManejoArchivo());
            dto.setIdUsuarioModificacion(idUsuarioModificacion);
            planManejoRepository.actualizarConformePlanManejoArchivo(dto);
        }

        return planManejoRepository.actualizarPlanManejoArchivo(dto);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity guardarArchivoPlanManejoArchivo(PlanManejoArchivoDto dto, MultipartFile file) throws Exception{

        ResultClassEntity result = null;       
 
        if (file!=null) {
            result = serArchivo.RegistrarArchivoGeneral(file, dto.getIdUsuarioRegistro(), dto.getIdTipoDocumento());
            if(result.getSuccess()){
                dto.setIdArchivo(result.getCodigo());
            } 
        }
        result = planManejoRepository.actualizarArchivoPlanManejoArchivo(dto);

        ArchivoEntity a = new ArchivoEntity();
        a.setIdArchivo(dto.getIdArchivo());
        List<ArchivoEntity> lista = archivoRepository.listarArchivo(a).getData();

        if (lista!=null && !lista.isEmpty()) {
            a = lista.get(0);            
        }

        a.setTipoDocumento(dto.getIdTipoDocumento());

        archivoRepository.actualizarArchivo(a);

        return result;

    }
    @Override
    public ResultClassEntity ObtenerPlanManejo(PlanManejoDto plan) throws Exception{
        return planManejoRepository.ObtenerPlanManejo(plan);
    }    

    @Override
    public ResultClassEntity actualizarPlanManejoEstado(PlanManejoDto request ) throws Exception{
        return planManejoRepository.actualizarPlanManejoEstado(request);
    }

    @Override
    public ResultClassEntity<List<PlanManejoObtenerContratoDetalle>>obtenerPlanManejoDetalleContrato(PlanManejoArchivoDto filtro) {
        return planManejoRepository.obtenerPlanManejoDetalleContrato(filtro);
    }

    /**
     * @autor:  Rafael Azaña [03-12-2021]
     * @descripción: {Listar Plan manejo archivo}
     * @param: PlanManejoDto
     * @return: ResponseEntity<ResponseVO>
     */

    @Override
    public ResultClassEntity<List<PlanManejoArchivoDto>> listarPlanManejoListar(Integer idPlanManejo, Integer idArchivo, String tipoDocumento,String codigoProceso, String extension)
            throws Exception {

        ResultClassEntity<List<PlanManejoArchivoDto>> result = new ResultClassEntity<>();

        result.setData(planManejoRepository.listarPlanManejoListar(idPlanManejo,idArchivo,tipoDocumento,codigoProceso,extension));
        result.setSuccess(true);
        result.setMessage("Lista Plan Manejo Archivo");

        return result;
    }

    @Override
    public ResultClassEntity<List<PlanManejoArchivoDto>> PlanManejoArchivoEntidad(Integer idPlanManejoArchivo)
            throws Exception {

        ResultClassEntity<List<PlanManejoArchivoDto>> result = new ResultClassEntity<>();

        result.setData(planManejoRepository.PlanManejoArchivoEntidad(idPlanManejoArchivo));
        result.setSuccess(true);
        result.setMessage("Lista Plan Manejo Archivo");

        return result;
    }

    /**
     * @autor: Danny Nazario [17-12-2021]
     * @modificado:
     * @descripción: {Listar Plan Manejo Anexo Archivo}
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPlanManejoAnexoArchivo(PlanManejoArchivoEntity filtro) throws Exception {
        return planManejoRepository.listarPlanManejoAnexoArchivo(filtro);
    }

    /**
     * @autor:  Danny Nazario [29-03-2022]
     * @descripción: {Registrar Plan Manejo Contrato}
     * @param: PlanManejoContratoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarPlanManejoContrato(PlanManejoContratoEntity obj) throws Exception{
        return planManejoContratoRepository.RegistrarPlanManejoContrato(obj);
    }

    @Override
    public ResultClassEntity obtenerPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception{
        return planManejoContratoRepository.obtenerPlanManejoContrato(planManejoContratoEntity);
    }

    /**
     * @autor: Danny Nazario [13-01-2022]
     * @modificado: Danny Nazario [21-03-2022]
     * @descripción: {Listar PlanManejo Contrato por filtro}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPorFiltroPlanManejoContrato(String nroDocumento, String codTipoPlan) throws Exception {
        return planManejoContratoRepository.listarPorFiltroPlanManejoContrato(nroDocumento, codTipoPlan);
    }

    private Double sumarColumna (List<RentabilidadManejoForestalDto> listaFlujoCaja, int columna){
        BigDecimal bd = BigDecimal.ZERO;
        for (RentabilidadManejoForestalDto e : listaFlujoCaja) {
            for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                if (el.getAnio().equals(columna)) {
                    BigDecimal b = new BigDecimal(el.getMonto(), MathContext.DECIMAL64);
                    bd = bd.add(b);
                }
            }            
        }
        
        return bd.doubleValue();
    }
    @Override
    public ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo,String tipoPlan) {

        return planManejoRepository.listarPorPlanManejoTipoBosque(idPlanDeManejo,tipoPlan);
    }

    @Override
    public ResultClassEntity<PlanManejoDto> DatosGeneralesPlan(Integer idPlanManejo) throws Exception {
        return planManejoRepository.DatosGeneralesPlan(idPlanManejo);
    }

    @Override
    public ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo,String tipoPlan,String idTipoBosque) {

        return planManejoRepository.listarPorPlanManejoTipoBosque(idPlanDeManejo,tipoPlan,idTipoBosque);
    }


    private List<OrdenamientoProteccionDetalleEntity> calcularAreaTotalParcela(
        List<OrdenamientoProteccionDetalleEntity> lista) {

    BigDecimal bdSumaArea = BigDecimal.ZERO;

    for (OrdenamientoProteccionDetalleEntity e : lista) {
        bdSumaArea = bdSumaArea.add(e.getAreaHA());
    }

    for (OrdenamientoProteccionDetalleEntity e : lista) {

        if (e.getAreaHA() != null) {
            e.setAreaHAPorcentaje(
                    e.getAreaHA().multiply(new BigDecimal(100)).divide(bdSumaArea, 2, RoundingMode.HALF_UP));
        }
    }

    return lista;

}

private List<VolumenComercialPromedioEspecieDto> addCortaAnualTotales(
        List<VolumenComercialPromedioEspecieDto> lista) {
    
    VolumenComercialPromedioEspecieDto e = new VolumenComercialPromedioEspecieDto();

    Integer sumaHa = 0;
    Double sumaPorc = 0.0;
    BigDecimal sumaVcp = BigDecimal.ZERO;
    BigDecimal sumaVcpPond = BigDecimal.ZERO;
    BigDecimal sumaVolAnual = BigDecimal.ZERO;

    for (VolumenComercialPromedioEspecieDto i : lista) {
        sumaHa += i.getCantidadArb();
        sumaPorc += i.getPorcentajeArb();
        sumaVcp = sumaVcp.add(i.getVolumenComercialPromedio());
        sumaVcpPond = sumaVcpPond.add(i.getVolumenComercialPromedioPonderado());
        sumaVolAnual = sumaVolAnual.add(i.getVolumenCortaAnualPermisible());
    }

    e.setNombreComun("Total UMF");
    e.setCantidadArb(sumaHa);
    e.setPorcentajeArb(sumaPorc);
    e.setVolumenComercialPromedio(sumaVcp);
    e.setVolumenComercialPromedioPonderado(sumaVcpPond);
    e.setVolumenCortaAnualPermisible(sumaVolAnual);

    lista.add(e);
    
    return lista;
}

    @Override
    public List<RegistroTabEntity> listarTabsRegistrados(RegistroTabEntity param) throws Exception {
        return planManejoRepository.listarTabsRegistrados(param);
    }

    @Override
    public ByteArrayResource descargarFormatoPlantacionesForestales() throws Exception {
        XWPFDocument doc = getDoc("FormatoPlantacionesForestales.docx");


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    @Override
    public ByteArrayResource descargarPlantillaFormatoFiscalizacion2() throws Exception {
        XWPFDocument doc = getDoc("PlantillaFormatoFiscalizacion2.docx");


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }

    /**
     * @autor: Jason Retamozo [21-02-2022]
     * @creado:
     * @descripción: {Ejemplo de uso PDF con Apache POI para PMFIC}
     */
    @Override
    public ByteArrayResource consolidadoPMFIC_Resumido(Integer idPlanManejo) throws Exception {
        XWPFDocument doc =  getDoc("formatoPMFIC_Resumido.docx");

        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PMFIC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData()!=null?
                infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().size()>0?infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0):null:null;
        /*
        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle = new ArrayList<>();
        if (lstInformacionBasica.getData().size() > 0) {
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        */

        Map<String, String> keys = MapBuilder.keysInfoGeneralPMFIC(o);
        DocUtilPmfic.processFile(keys, doc, o.getLstUmf());

        //2- OBJETIVOS 2,3
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos= objetivoservice.listarObjetivos("PMFIC",idPlanManejo);
        DocUtilPmfic.procesarObjetivos(lstObjetivos.getData(), doc);

        //3- INFORMACION BASICA

        //4- ASPECTOS FISICOS PMFICAF
        List<InfBasicaAereaDetalleDto> lstInfoBasica = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICAF");
        //4.1 - HIDROGRAFIA 9 PMFICAF PMFICAFHUMF
        List<InfBasicaAereaDetalleDto> lstInfoBasicaHID = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("PMFICAFHUMF")).collect(Collectors.toList());
        DocUtilPmfic.procesarHidrografia(lstInfoBasicaHID,doc);
        //4.2 - FISIOGRAFIA 10 PMFICAF PMFICAFFUMF
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFIS = lstInfoBasica.stream().filter(p-> p.getCodSubInfBasicaDet().equals("PMFICAFFUMF")).collect(Collectors.toList());
        DocUtilPmfic.procesarFisiografia(lstInfoBasicaFIS,doc);

        //5 - ASPECTOS BIOLOGICOS
        //5.1 - FAUNA 11 PMFICABFS
        List<InfBasicaAereaDetalleDto> lstInfoBasicaFAU = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICABFS");
        DocUtilPmfic.procesarFauna(lstInfoBasicaFAU,doc);
        //5.2 - TIPOS DE BOSQUE 12 PMFICABTB
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTB = informacionBasicaRepository.listarInfBasicaAerea("PMFIC", idPlanManejo, "PMFICABTB");
        DocUtilPmfic.procesarTiposBosque(lstInfoBasicaTB,doc);

        //6 - INFORMACION SOCIOECONOMICA
        //6.1 - CARACTERIZACION 13 PMFICISCC
        List<InformacionBasicaEntity> lstInfoBasicaCC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISCC");
        DocUtilPmfic.generarCaracterizacionComunidad(lstInfoBasicaCC,doc);
        //6.2 - INFRAESTRUCTURA 14 PMFICISIS
        List<InformacionBasicaEntity> lstInfoBasicaIS = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISIS");
        DocUtilPmfic.procesarInfraestructuraServicios(lstInfoBasicaIS,doc);
        //6.3 - ANTECEDENTES 15 PMFICISAUEC
        List<InformacionBasicaEntity> lstInfoBasicaAUEC = informacionBasicaRepository.listarInformacionSocioeconomica(idPlanManejo,"PMFIC", "PMFICISAUEC");
        DocUtilPmfic.procesarAntecedentes(lstInfoBasicaAUEC,doc);

        //7 - ORDENAMIENTO Y PROTECCION
        OrdenamientoProteccionEntity req = new OrdenamientoProteccionEntity();
        req.setIdPlanManejo(idPlanManejo);
        req.setCodTipoOrdenamiento("PMFIC");
        List<OrdenamientoProteccionEntity> lstOrdenProt =ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(req).getData();
        if(lstOrdenProt.size()>0){
            //7.1 - SUPERFICIE Y UBICACION BLOQUES 16 PFMICOPSUB
            List<OrdenamientoProteccionDetalleEntity> lstDetBloq = null;
            if(lstOrdenProt.size()>0) lstDetBloq=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("PFMICOPSUB")).collect(Collectors.toList());
            DocUtilPmfic.procesarSuperficieBloques(lstDetBloq,doc);
            //7.2 - SUPERFICIE Y UBICACION AREA APROV 17 PFMICOPSUAAPB
            List<OrdenamientoProteccionDetalleEntity> lstDetArea = null;
            if(lstOrdenProt.size()>0) lstDetArea=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("PFMICOPSUAAPB")).collect(Collectors.toList());
            DocUtilPmfic.procesarSuperficieArea(lstDetArea,doc);
            //7.3 - SUPERFICIE Y UBICACION PARCELA CORTA 18 PFMICOPSUPCAO
            List<OrdenamientoProteccionDetalleEntity> lstDetParc = null;
            if(lstOrdenProt.size()>0) lstDetParc=lstOrdenProt.get(0).getListOrdenamientoProteccionDet().stream().filter(a->a.getCodigoTipoOrdenamientoDet().equals("PFMICOPSUPCAO")).collect(Collectors.toList());
            DocUtilPmfic.procesarSuperficieParcela(lstDetParc,doc);
        }
        //7.4 - MEDIDAS DE PROTECCION 19
        SistemaManejoForestalDto manejo= sistemaManejoForestalService.obtener(idPlanManejo,"PMFIC").getData();
        DocUtilPmfic.procesarMedidasProteccion(manejo,doc);

        //8 - Potencial de produccion
        //8.1 - Potencial con fines maderables
        //8.1.1a arboles 20
        //8.1.1b fustales 21
        //8.1.2 censo comercial 22
        //8.2 - Potencial con fines no maderables
        //8.2.1 inventario 23
        //8.2.2 censo comercial 24


        //9 - Sistema de manejo forestal
        //9.0 - Uso Potencial 25  PMFICSMFUMCZUMF
        List<ManejoBosqueEntity> lstMB_zumf= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMCZUMF",null);
        DocUtilPmfic.procesarZonificacionUMF(lstMB_zumf,doc);
        //9.1 - MF con fines maderables PMFICSMFUMMFFM
        //9.1.1 sistema maderable 26  PMFICSMFUMMFFMSMFCFM
        List<ManejoBosqueEntity> lstMB_FM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMSMFCFM");
        DocUtilPmfic.procesarSMFinesMaderables(lstMB_FM,doc);
        //9.1.2 ciclo corte 27  PMFICSMFUMMFFMCC
        List<ManejoBosqueEntity> lstMB_CC= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMCC");
        DocUtilPmfic.procesarSMCicloCorte(lstMB_CC,doc);
        //9.1.3 actividades 28  PMFICSMFUMMFFMAAEUFM
        List<ManejoBosqueEntity> lstMB_AA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFM","PMFICSMFUMMFFMAAEUFM");
        DocUtilPmfic.procesarSMActividadesAprovechamiento(lstMB_AA,doc);
        //9.2 - MF con fines no maderables  PMFICSMFUMMFFNM
        //9.2.1 sistema no maderable 29  PMFICSMFUMMFFNMSMFNM
        List<ManejoBosqueEntity> lstMB_FNM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMSMFNM");
        DocUtilPmfic.procesarSMFinesNoMaderables(lstMB_FNM,doc);
        //9.2.2 ciclo aprov 30  PMFICSMFUMMFFNMCAPRO
        List<ManejoBosqueEntity> lstMB_CA= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMCAPRO");
        DocUtilPmfic.procesarSMCicloAprovechamiento(lstMB_CA,doc);
        //9.2.3 actividades 31  PMFICSMFUMMFFNMAAEFN
        List<ManejoBosqueEntity> lstMB_AANM= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMMFFNM","PMFICSMFUMMFFNMAAEFN");
        DocUtilPmfic.procesarSMActividadesAprovechamientoNM(lstMB_AANM,doc);
        //9.3 - Labores silviculturales  PMFICSMFUMLS
        //9.3.1 act obligatorias 32  OBLI
        List<ManejoBosqueEntity> lstMB_LSOB= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMLS","OBLI");
        DocUtilPmfic.procesarSMLaboresObligatorias(lstMB_LSOB,doc);
        //9.3.2 act opcionales 33  OPC
        List<ManejoBosqueEntity> lstMB_LSOP= manejoBosqueRepository.ListarManejoBosque(idPlanManejo,"PMFIC","PMFICSMFUMLS","OPC");
        DocUtilPmfic.procesarSMLaboresOpcionales(lstMB_LSOP,doc);

        //10 - Evaluacion Ambiental
        //10.1 identificacion impactos 34
        EvaluacionAmbientalActividadEntity evalAmb=new EvaluacionAmbientalActividadEntity();
        evalAmb.setIdPlanManejo(idPlanManejo);
        evalAmb.setTipoActividad("FACTOR");
        List<EvaluacionAmbientalActividadEntity> lstEvalAmbiental= evaluacionAmbientalRepository.ListarEvaluacionActividadFiltro(evalAmb);

        EvaluacionAmbientalDto evalAmbAcciones=new EvaluacionAmbientalDto();
        evalAmbAcciones.setIdPlanManejo(idPlanManejo);
        List<EvaluacionAmbientalDto> lstEvalAmbientalAcciones= evaluacionAmbientalRepository.ListarEvaluacionAmbiental(evalAmbAcciones);
        DocUtilPmfic.generarEvalAmbientalImpactosAmb(lstEvalAmbiental,lstEvalAmbientalAcciones,doc);
        //10.2.1 acciones preventivo-corrector 35
        EvaluacionAmbientalAprovechamientoEntity eaae = new EvaluacionAmbientalAprovechamientoEntity();
        eaae.setIdPlanManejo(idPlanManejo);
        eaae.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> lstEvaluacion = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(eaae);
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanAccion = lstEvaluacion.stream()
                .filter(p->p.getCodTipoAprovechamiento().equals("POAPRO")).collect(Collectors.toList());
        DocUtilPmfic.generarPlanAccionEvaluacionImpactoAmbiental(lstPlanAccion,doc);
        //10.2.2 plan igilancia seguimiento 36
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanVigilancia = lstPlanAccion.stream()
                .filter(p-> !p.getImpacto().trim().equals("")).collect(Collectors.toList());
        DocUtilPmfic.generarPlanVigilanciaEvaluacionImpactoAmbiental(lstPlanVigilancia,doc);
        //10.3 acciones contingencia 37
        List<EvaluacionAmbientalAprovechamientoEntity> lstPlanContingencia = lstEvaluacion.stream()
                .filter(p->p.getCodTipoAprovechamiento().equals("POEAPR")).collect(Collectors.toList());
        DocUtilPmfic.generarPlanContingenciaEvaluacionImpactoAmbiental(lstPlanContingencia,doc);

        //11 - Organizacion para el desarrollo
        ResultClassEntity<ActividadSilviculturalDto>lstOrganizacion= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"PMFIC");
        //11.1 aprovechamiento maderable 38
        List<ActividadSilviculturalDetalleEntity> lstOrgAM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("MAD")).collect(Collectors.toList());
        DocUtilPmfic.procesarOrganizacionMaderable(lstOrgAM,doc);
        //11.2 aprovechamiento no maderable 39
        List<ActividadSilviculturalDetalleEntity> lstOrgANM = lstOrganizacion.getData().getDetalle().stream().filter(a->a.getObservacionDetalle().equals("NMAD")).collect(Collectors.toList());
        DocUtilPmfic.procesarOrganizacionNoMaderable(lstOrgANM,doc);

        //12 - Cronograma 40
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PMFIC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();
        DocUtilPmfic.generarCronogramaActividadesPOCC(lstcrono,doc);

        //13 - Rentabilidad de manejo forestal 41,42,43
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("PMFIC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        //12.1.a Totales 41
        Double[] totales = new Double[2];
        //12.1.b Ingresos 42
        List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());
        DocUtilPmfic.generarIngresoRentabilidad(lstIngresos,doc,totales);
        //12.1.c Egresos 43
        List<RentabilidadManejoForestalDto> lstEgresos= lstRMF.stream()
                .filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());
        DocUtilPmfic.generarEgresoRentabilidad(lstEgresos,doc,totales);
        //12.1.a Totales 41
        DocUtilPmfic.generarTotalesRentabilidad(totales,doc);

        //14 - Aspectos Complementarios 44
        InformacionGeneralDEMAEntity reqDema = new InformacionGeneralDEMAEntity();
        DocUtilPmfic.procesarAspectosComplementarios(o,doc);


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        InputStream templateInputStream = new FileInputStream("C:/Gerardo/formatoPOAC.docx");
            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(templateInputStream);
            MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();

            String outputfilepath = "C:/Gerardo/Sample.pdf";
            FileOutputStream os = new FileOutputStream(outputfilepath);
            Docx4J.toPDF(wordMLPackage,os);
            os.flush();
            os.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        /* InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("harry", ".pdf");
        String ruta =archivo.getAbsolutePath();
        FileOutputStream os = new FileOutputStream(archivo); */
        //Docx4J.toPDF(wordMLPackage,os);
        //PdfOptions pdfOpt=PdfOptions.create();
        //PdfConverter.getInstance().convert(doc,os,pdfOpt);
        /* os.flush();
        os.close();
        byte[] fileContent = Files.readAllBytes(archivo.toPath()); */
        return null; //new ByteArrayResource(fileContent);
        //return new ByteArrayResource(b.toByteArray()); // COMENTAR PARA PROBAR PDF
    }

    /**
     * @autor: Rafael Azaña [23-02-2022]
     * @creado:
     * @descripción: {Ejemplo de uso PDF con Apache POI para POPAC}
     */
    @Override
    public ByteArrayResource consolidadoPOPAC_Resumido(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPOPAC.docx");

        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POPAC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData()!=null?
                infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().size()>0?infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0):null:null;
        /*
        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle = new ArrayList<>();
        if (lstInformacionBasica.getData().size() > 0) {
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }

        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        */
        Map<String, String> keys = MapBuilder.keysInfoGeneralPMFIC(o);
        DocUtilPmfic.processFile(keys, doc, o.getLstUmf());



        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("consoPOPAC", ".pdf");
        String ruta =archivo.getAbsolutePath();
        FileOutputStream os = new FileOutputStream(archivo);
        Docx4J.toPDF(wordMLPackage,os);
        os.flush();
        os.close();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);
        //return new ByteArrayResource(b.toByteArray()); // COMENTAR PARA PROBAR PDF
    }




    /**
     * @autor: Harry Coa [19-02-2022]
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con Apache POI}
     */

    @Override
    public ByteArrayResource consolidadoPOAC_Resumido(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPOAC_Resumido.docx");
        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POAC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        ResultEntity<InformacionGeneralDEMAEntity> lstInformacionBasica = infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        List<InformacionGeneralDetalle> lstInformacionBasicaDetalle = new ArrayList<>();
        if (lstInformacionBasica.getData().size() > 0) {
            InformacionGeneralDEMAEntity PGMFEVALI = lstInformacionBasica.getData().get(0);
            lstInformacionBasicaDetalle = PGMFEVALI.getLstUmf();
        }
        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            o.setProvincia(lstacceso.get(0).getNombreProvincia());
            o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }
        Map<String, String> keys = MapBuilder.keysInfoGeneralPOAC(o);
        DocUtil.processFile(keys,doc,o.getLstUmf());

        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);
        //2.1  -- 1
        DocUtil.generarResumenActividadPOAC(lstResumen, doc);
        //2.1 --2
        DocUtil.generarResumenActividadFrenteCortaPOAC(lstResumen, doc);
        //2.2  -- 3 positivo
        DocUtil.generarResumenActividadResultadosPOACPos(lstResumen, doc);
        //2.2  -- 4 negativo
        DocUtil.generarResumenActividadResultadosPOACNeg(lstResumen, doc);
        //2.2  -- 5 recomendacuin
        DocUtil.generarResumenActividadResultadosPOACRec(lstResumen, doc);

        //3 - Objetivos  -- 6
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos= objetivoservice.listarObjetivos("POAC",idPlanManejo);
        DocUtil.procesarObjetivosPOAC(lstObjetivos.getData(), doc);

        //4 - INFORMACIÓN BASICA DE LA PARCELA DE CORTA (PC)
        //4.1  -- 7
        List<InfBasicaAereaDetalleDto> lstInfoBasicaUEPC = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMATC");
        String departamento="",provincia="",distrito="";
        for (InfBasicaAereaDetalleDto detalleEntityUbigeo : lstInfoBasicaUEPC) {
            List<InfBasicaAereaDetalleDto> lstInfoAreaUbigeo = informacionBasicaRepository.listarInfBasicaAereaUbigeo(detalleEntityUbigeo.getDepartamento(), detalleEntityUbigeo.getProvincia(), detalleEntityUbigeo.getDistrito());
            for (InfBasicaAereaDetalleDto detalleEntityUbigeo2 : lstInfoAreaUbigeo) {
                departamento=detalleEntityUbigeo2.getDepartamento();
                provincia=detalleEntityUbigeo2.getProvincia();
                distrito=detalleEntityUbigeo2.getDistrito();
            }
        }
        DocUtil.generarInfoBasicaUEAA(lstInfoBasicaUEPC, doc, departamento,provincia, distrito );
        //4.2  --8
        List<InfBasicaAereaDetalleDto> lstInfoBasicaCOOUTM = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMCUMD");
        DocUtil.generarInfoBasicaCOOUTM (lstInfoBasicaCOOUTM, doc);
        //4.3  --9
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTB = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMABTB");
        DocUtil.generarInfoBasicaTB(lstInfoBasicaTB, doc);
        //4.4  --10
        List<InfBasicaAereaDetalleDto> lstInfoBasicaAcc = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMA");
        DocUtil.generarInfoBasicaAccA(lstInfoBasicaAcc, doc); // 10
        DocUtil.generarInfoBasicaAccC(lstInfoBasicaAcc, doc); // 11

        // 5.1  // 12
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);
        DocUtil.generarActividadAprovPOACDeli(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACDeliDis(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACPlani(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaCorta(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaArras(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaTrans(lstAprovechamiento.getData(), doc);
        DocUtil.generarActividadAprovPOACOperaProce(lstAprovechamiento.getData(), doc);

        //6 A --23--
        ResultClassEntity<ActividadSilviculturalDto>lstActSilvicultural= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC1");
        DocUtil.generarActividadSilviculturalAplicacionPOAC(lstActSilvicultural.getData(), doc);
        //6 B --24--
        ResultClassEntity<ActividadSilviculturalDto>lstActSilviculturalB= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC2");
        DocUtil.generarActividadSilviculturalReforestacionPOAC(lstActSilviculturalB.getData(), doc);

        //7. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosqueBDML = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBDML");
        DocUtil.generarProteccionBosquePOAC(lstProteccionBosqueBDML, doc,"POACPBDML");
        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBAIA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBAIA");
        DocUtil.generarProteccionBosquePOAC(lstProteccionBosquePBAIA, doc,"POACPBAIA");
        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBPGA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBPGA");
        DocUtil.generarProteccionBosquePOAC(lstProteccionBosquePBPGA, doc,"POACPBPGA");

        //8  Monitoreo -- 30
        MonitoreoEntity monitoreo = new MonitoreoEntity();
        monitoreo.setIdPlanManejo(idPlanManejo);
        monitoreo.setCodigoMonitoreo("POAC");
        List<MonitoreoDetalleEntity> lstMonitoreo = monitoreoRepository.listarMonitoreo(monitoreo).getData().getLstDetalle();
        DocUtil.generarMonitoreoPOAC(lstMonitoreo, doc);

        //9. PARTICIPACION COMUNAL 31
        ParticipacionComunalEntity participacionRequest = new ParticipacionComunalEntity();
        participacionRequest.setCodTipoPartComunal("POAC");
        participacionRequest.setIdPlanManejo(idPlanManejo);
        ResultClassEntity<List<ParticipacionComunalEntity>>lstParticipacion= participacionComunalRepository.ListarPorFiltroParticipacionComunal(participacionRequest);
        DocUtil.generarParticipacionComunalPOAC(lstParticipacion.getData(), doc);

        //10. Capacitacion 32
        CapacitacionDto obj = new CapacitacionDto();
        obj.setIdPlanManejo(idPlanManejo);
        obj.setCodTipoCapacitacion("POAC");
        List<CapacitacionDto> lstCapacitacion= capacitacionRepository.ListarCapacitacion(obj);
        DocUtil.generarCapacitacionPOAC(lstCapacitacion,doc);

        //11. Organizacion para el desarrollo 32
        ResultClassEntity<ActividadSilviculturalDto>lstActSilviculturalOrg= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC");
        DocUtil.generarActividadSilviculturalOrganizPOAC(lstActSilviculturalOrg.getData(), doc);
        //
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("harry", ".pdf");
        String ruta =archivo.getAbsolutePath();
        FileOutputStream os = new FileOutputStream(archivo);
        Docx4J.toPDF(wordMLPackage,os);
        os.flush();
        os.close();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);
        //return new ByteArrayResource(b.toByteArray()); // COMENTAR PARA PROBAR PDF
    }



    @Override
    public ResultArchivoEntity plantillaInformeGeneral(Integer idPermisoForestal) throws Exception {

        XWPFDocument doc = getDoc("formatoPlantillaPermisos.docx");
        List<InformacionGeneralPermisoForestalEntity> listaSol=new ArrayList<>();
        InformacionGeneralPermisoForestalEntity pfcr = new InformacionGeneralPermisoForestalEntity();
        pfcr.setCodTipoInfGeneral("PFCR");
        pfcr.setIdPermisoForestal(idPermisoForestal);
        pfcr.setIdInfGeneral(null);

       /* ResultClassEntity<InformacionGeneralPermisoForestalEntity>lstInfPermisoForestal= listarInformacionGeneral(pfcr);

        List <InformacionGeneralPermisoForestalEntity> lista = (List<InformacionGeneralPermisoForestalEntity>) lstInfPermisoForestal.getData();

        DocUtilPermisos.setearInfoPermiso(lista, doc);*/



        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("formatoPlantillaPermisos.docx");
        word.setTipoDocumento("Permiso");
        word.setArchivo(b.toByteArray());

        return word;
    }

    @Override
    public List<DivisionAdministrativaEntity> listarDivisionAdministrativa(DivisionAdministrativaEntity param) throws Exception {
        return planManejoRepository.listarDivisionAdministrativa(param);
    }
}
