package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalGeometriaEntity;

public interface SolicitudBosqueLocalGeometriaService {
    ResultClassEntity registrarSolicitudBosqueLocalGeometria( List<SolicitudBosqueLocalGeometriaEntity> items)  throws Exception;
    ResultClassEntity actualizarSolicitudBosqueLocalGeometria( List<SolicitudBosqueLocalGeometriaEntity> items)  throws Exception;
    ResultClassEntity listarSolicitudBosqueLocalGeometria(SolicitudBosqueLocalGeometriaEntity item);
    ResultClassEntity eliminarSolicitudBosqueLocalGeometriaArchivo (SolicitudBosqueLocalGeometriaEntity item);
}
