package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudOpinionEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudOpinionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.SolicitudOpinionService;

import java.util.List;


@Service("SolicitudOpinionService")
public class SolicitudOpinionServiceImpl implements SolicitudOpinionService {


    private static final Logger log = LogManager.getLogger(SolicitudOpinionServiceImpl.class);

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;


    @Override
    public ResultClassEntity ListarSolicitudOpinion(SolicitudOpinionDto param) {
        return solicitudOpinionRepository.ListarSolicitudOpinion(param);
    }

    @Override
    public ResultClassEntity RegistrarSolicitudOpinion(SolicitudOpinionDto param) throws Exception{


        ResultClassEntity resultClassEntity = solicitudOpinionRepository.RegistrarSolicitudOpinion(param);
        SolicitudOpinionDto request = (SolicitudOpinionDto) resultClassEntity.getData();

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLICITUD_OPINION + request.getIdSolicitudOpinion(),
                param.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    @Override
    public ResultClassEntity ActualizarSolicitudOpinion(SolicitudOpinionDto param) throws Exception{

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUD_OPINION + param.getIdSolicitudOpinion(),
                param.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);


        return solicitudOpinionRepository.ActualizarSolicitudOpinion(param);
    }

    @Override
    public ResultClassEntity ActualizarSolicitudOpinionArchivo(SolicitudOpinionDto param) throws Exception{


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUD_OPINION + param.getIdSolicitudOpinion(),
                param.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);



        return solicitudOpinionRepository.ActualizarSolicitudOpinionArchivo(param);
    }

    @Override
    public ResultClassEntity EliminarSolicitudOpinionArchivo(SolicitudOpinionDto param) throws Exception{

        ResultClassEntity resultClassEntity = solicitudOpinionRepository.EliminarSolicitudOpinionArchivo(param);

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MVC_SOLICITUD_OPINION + param.getIdSolicitudOpinion(),
                param.getIdUsuarioElimina());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }
    @Override
    public ResultClassEntity ObtenerSolicitudOpinion(SolicitudOpinionDto param) {

        return solicitudOpinionRepository.ObtenerSolicitudOpinion(param);
    }


    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Registrar y Actualizar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */

    @Override
    public ResultClassEntity RegistrarSolicitudOpinionEvaluacion(List<SolicitudOpinionEntity> list) throws Exception {
        return solicitudOpinionRepository.RegistrarSolicitudOpinionEvaluacion(list);
    }

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Listar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */


    @Override
    public ResultClassEntity<List<SolicitudOpinionEntity>> ListarSolicitudOpinionEvaluacion(Integer idPlanManejo, String codSolicitud, String subCodSolicitud)
            throws Exception {

        ResultClassEntity<List<SolicitudOpinionEntity>> result = new ResultClassEntity<>();

        result.setData(solicitudOpinionRepository.ListarSolicitudOpinionEvaluacion(idPlanManejo,codSolicitud,subCodSolicitud));
        result.setSuccess(true);
        result.setMessage("Lista Solicitud de Opinion ");

        return result;
    }

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Eliminar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */

    @Override
    public ResultClassEntity EliminarSolicitudOpinionEvaluacion(SolicitudOpinionEntity param) throws Exception {

        ResultClassEntity resultClassEntity = solicitudOpinionRepository.EliminarSolicitudOpinionEvaluacion(param);

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MVC_SOLICITUD_OPINION + param.getIdSolOpinion(),
                param.getIdUsuarioElimina());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return  resultClassEntity;
    }
}
