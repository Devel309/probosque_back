package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.RentabilidadManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.RentabilidadManejoForestalService;

import java.util.ArrayList;
import java.util.List;

@Service("RentabilidadManejoForestalService")
public class RentabilidadManejoForestalServiceImpl implements RentabilidadManejoForestalService {
    private static final Logger log = LogManager.getLogger(RentabilidadManejoForestalServiceImpl.class);

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;
    
    public List<Dropdown> ComboRubroRentabilidadForestal(ParametroEntity param) throws Exception{
          return rentabilidadManejoForestalRepository.ComboRubroRentabilidadForestal(param);
    }
    @Override
    public ResultClassEntity RegistrarRentabilidadManejoForestal(List<RentabilidadManejoForestalDto> list) throws Exception {
        return rentabilidadManejoForestalRepository.RegistrarRentabilidadManejoForestal(list);
    }

    @Override
    public ResultClassEntity ListarRentabilidadManejoForestal(PlanManejoEntity param) throws Exception {
        return rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(param);
    }

    @Override
    public ResultClassEntity ListarRentabilidadManejoForestalTitular(PlanManejoEntity param) throws Exception {
        return rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestalTitular(param);
    }

    @Override
    public ResultClassEntity EliminarRentabilidadManejoForestal(RentabilidadManejoForestalEntity param) throws Exception {
        return rentabilidadManejoForestalRepository.EliminarRentabilidadManejoForestal(param);
    }


    @Override
    public ResultArchivoEntity descargarFormato() throws Exception {
        return rentabilidadManejoForestalRepository.descargarFormato();
    }

    @Override
    public ResultClassEntity registrarProgramaInversionesExcel(MultipartFile file, String codigoRentabilidad, Integer idPlanManejo, Integer idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<RentabilidadManejoForestalDto> list=new ArrayList<>();
        RentabilidadManejoForestalDto rentabilidadManejoForestalDto;
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, "PROGRAMA INVERSIÓN", 2, 2);
            for(ArrayList obj : listaExcel) {
                if(!obj.get(0).equals("") && !obj.get(1).equals("")){
                    rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
                    rentabilidadManejoForestalDto.setCodigoRentabilidad(codigoRentabilidad);
                    rentabilidadManejoForestalDto.setEstado("A");
                    rentabilidadManejoForestalDto.setIdPlanManejo(idPlanManejo);

                    String tipoRubro=(String) obj.get(0);
                    Integer idTipoRubro=null;

                    if(tipoRubro.equals("INGRESOS")){
                        idTipoRubro=1;
                    }
                    if(tipoRubro.equals("EGRESOS")){
                        idTipoRubro=2;
                    }
                    rentabilidadManejoForestalDto.setIdTipoRubro(idTipoRubro);
                    rentabilidadManejoForestalDto.setIdUsuarioRegistro(idUsuarioRegistro);

                    List<RentabilidadManejoForestalDetalleEntity> listDetalle= new ArrayList<>();
                    RentabilidadManejoForestalDetalleEntity itemDetalle;

                    for(int x=2;x<=21;x++){
                        if(obj.get(x)!=null){
                            itemDetalle=new RentabilidadManejoForestalDetalleEntity();
                            itemDetalle.setAnio(x-1);
                            itemDetalle.setEstado("A");
                            itemDetalle.setIdUsuarioRegistro(idUsuarioRegistro);
                            String monto=(String) obj.get(x);
                            if(!monto.isEmpty()){
                                itemDetalle.setMonto(Double.parseDouble(monto));
                            }else{
                                itemDetalle.setMonto(0.00);
                            }


                            listDetalle.add(itemDetalle);
                        }
                    }


                    rentabilidadManejoForestalDto.setListRentabilidadManejoForestalDetalle(listDetalle);
                    rentabilidadManejoForestalDto.setRubro((String) obj.get(1));
                    list.add(rentabilidadManejoForestalDto);
                }


            }

            rentabilidadManejoForestalRepository.RegistrarRentabilidadManejoForestal(list);

            result.setSuccess(true);
            result.setMessage("Se registró correctamente.");
        }catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    /**
     * @autor: Danny Nazario [28-12-2021]
     * @modificado: Danny Nazario [25-01-2022]
     * @descripción: {Registra Rentabilidad Manejo Forestal Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarRentabilidadManejoForestalExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                                      String codigoRentabilidad, int idPlanManejo, int idUsuarioRegistro, int vigencia) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        List<RentabilidadManejoForestalDto> list = new ArrayList<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            RentabilidadManejoForestalDto item = new RentabilidadManejoForestalDto();
            RentabilidadManejoForestalDetalleEntity det = new RentabilidadManejoForestalDetalleEntity();
            String lastTipo = "";
            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                item = new RentabilidadManejoForestalDto();
                item.setIdRentManejoForestal(0);
                item.setIdPlanManejo(idPlanManejo);
                if (row.get(0).equals("Ingresos")) {
                    item.setIdTipoRubro(1);
                    lastTipo = row.get(0);
                } else if (row.get(0).equals("Egresos")) {
                    item.setIdTipoRubro(2);
                    lastTipo = row.get(0);
                } else if (row.get(0).equals("")) {
                    if (lastTipo.equals("Ingresos")) {
                        item.setIdTipoRubro(1);
                    } else if (lastTipo.equals("Egresos")) {
                        item.setIdTipoRubro(2);
                    }
                }
                item.setRubro(row.get(1));

                List<RentabilidadManejoForestalDetalleEntity> listDet = new ArrayList<>();
                if (vigencia > 0) {
                    //Años - Meses
                    item.setEstado("A");
                    item.setIdUsuarioRegistro(idUsuarioRegistro);
                    item.setCodigoRentabilidad(codigoRentabilidad);
                    for (int i = 1; i < (vigencia + 1); i++) {
                        for (int j = 1; j < 13; j++) {
                            det = new RentabilidadManejoForestalDetalleEntity();
                            int pos = ((i - 1) * 12) + (1 + j);
                            if (!row.get(pos).isEmpty()) {
                                det.setIdRentManejoForestalDet(0);
                                det.setAnio(i);
                                det.setMes(j);
                                det.setMonto(Double.parseDouble(row.get(pos)));
                                listDet.add(det);
                            }
                        }
                    }
                } else {
                    //Años
                    item.setDescripcion(row.get(2));
                    item.setEstado("A");
                    item.setIdUsuarioRegistro(idUsuarioRegistro);
                    item.setCodigoRentabilidad(codigoRentabilidad);

                    for (int i = 1; i < 21; i++) {
                        det = new RentabilidadManejoForestalDetalleEntity();
                        if (!row.get(2 + i).isEmpty()) {
                            det.setIdRentManejoForestalDet(0);
                            det.setAnio(i);
                            det.setMonto(Double.parseDouble(row.get(2 + i)));
                            listDet.add(det);
                        }
                    }
                }

                item.setListRentabilidadManejoForestalDetalle(listDet);

                list.add(item);
            }
            rentabilidadManejoForestalRepository.RegistrarRentabilidadManejoForestal(list);
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }
}
