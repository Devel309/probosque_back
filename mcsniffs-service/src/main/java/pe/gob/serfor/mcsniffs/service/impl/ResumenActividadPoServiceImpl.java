package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResumenActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;
import pe.gob.serfor.mcsniffs.repository.ResumenActividadPoRepository;
import pe.gob.serfor.mcsniffs.service.ResumenActividadPoService;

import java.util.ArrayList;
import java.util.List;

@Service("ResumenActividadPoService")
public class ResumenActividadPoServiceImpl implements ResumenActividadPoService {
    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    /**
     * @autor: Harry Coa [29-07-2021]
     * @modificado: Registrar Resumen
     * @descripción: {}
     *
     */
    @Override
    public ResultClassEntity RegistrarResumenActividad(List<ResumenActividadPoEntity> list) throws Exception {
        return resumenActividadPoRepository.RegistrarResumenActividad(list);
    }

    @Override
    public List<ResumenActividadDto> ListarResumenActividad(ResumenActividadDto param) throws Exception {
        return resumenActividadPoRepository.ListarResumenActividad(param);
    }
    @Override
    public List<ResumenActividadPoEntity> ListarResumenActividad_Detalle(ResumenActividadPoEntity param) throws Exception {
        return resumenActividadPoRepository.ListarResumenActividad_Detalle(param);
    }

    @Override
    public ResultClassEntity EliminarResumenActividad(ResumenActividadPoDetalleEntity param) throws Exception {
        return resumenActividadPoRepository.EliminarResumenActividad(param);
    }

    @Override
    public ResultClassEntity registrarResumenActividadPO(ResumenActividadPoEntity param) throws Exception {

        List<ResumenActividadPoEntity> listResumenActividadEntity = new ArrayList<>();
        listResumenActividadEntity.add(param);

        return resumenActividadPoRepository.RegistrarResumenActividad(listResumenActividadEntity);

    }

    @Override
    public List<ResumenActividadDto> ListarResumenActividadPO(ResumenActividadDto param) throws Exception {
        return resumenActividadPoRepository.ListarResumenActividadPO(param);
    }
}
