package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;

public interface AnexosPFDMService {
    ResultArchivoEntity generarAnexo1PFDM(AnexosPFDMResquestEntity filtro);
}
