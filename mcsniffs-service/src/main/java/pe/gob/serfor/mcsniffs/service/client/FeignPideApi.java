package pe.gob.serfor.mcsniffs.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.*;


@FeignClient(name = "extServClient" ,url = "${spring.urlMcsniffsExtServ}")
public interface FeignPideApi {    

    @PostMapping("/pide/consultarAntecedenteJudicial")
    AntecedenteJudicialDto consultarAntecedenteJudicial(@RequestHeader(value = "Authorization", required = true) String authorizationHeader,
    @RequestBody ConsultaAntecedenteJudicialDto consultaAntecedenteJudicialDto);


    @PostMapping("/pide/consultarAntecedentePenal")
    AntecedentePenalDto consultarAntecedentePenal(@RequestHeader(value = "Authorization", required = true) String authorizationHeader,
    @RequestBody ConsultaAntecedentePenalDto consultaAntecedentePenalDto);

    @PostMapping("/pide/consultarAPbyNumDoc")
    AntecedentePolicialDto consultarAPbyNumDoc(@RequestHeader(value = "Authorization", required = true) String authorizationHeader,
    @RequestBody ConsultaAntecedentePolicialNumDocDto consultaAntecedentePolicialNumDocDto);

    @PostMapping("/pide/consultarSancionVigente")
    SancionVigenteOsceDto consultarSancionVigente(@RequestHeader(value = "Authorization", required = true) String authorizationHeader,
    @RequestBody ConsultarSancionVigenteOsceDto lNumDocDto);

    @PostMapping("/serfor/consultarInfractor")
    InfractorDto consultarInfractor(@RequestHeader(value = "Authorization", required = true) String authorizationHeader,
    @RequestBody ConsultaInfractorDto consultaInfractorDto);

    @PostMapping("/serfor/consultarRegente")
    ConsultarRegenteDto consultarRegente(@RequestHeader(value = "Authorization", required = true) String authorizationHeader, 
    ConsultaInfractorDto consultaInfractorDto);

    @PostMapping("/pide/consultarTitularidad")
    ConsultaTitularidadDto consultarTitularidad(@RequestHeader(value = "Authorization", required = true) String authorizationHeader,
                                                @RequestBody TitularidadDto titularidadDto);

}
