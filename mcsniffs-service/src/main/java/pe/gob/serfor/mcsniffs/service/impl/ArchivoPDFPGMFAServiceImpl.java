package pe.gob.serfor.mcsniffs.service.impl;


import com.lowagie.text.Document;
import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ArchivoPDFPGMFAService")
public class ArchivoPDFPGMFAServiceImpl implements ArchivoPDFPGMFAService {

    @Autowired
    private  SolicitudAccesoServiceImpl acceso;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;


    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font cabecera= new Font(Font.HELVETICA, 9f, Font.BOLD);
    Font subTituloTabla= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font letraPeque = new Font(Font.HELVETICA, 6f, Font.COURIER);

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: Rafael Azaña [19-02-2022]
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con Apache POI}
     */

    @Override
    public ByteArrayResource consolidadoPGMFA_PDF(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPGMFA.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("consolidadoPGMFA", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPdfPGMFA(os,idPlanManejo);
        os.flush();
        os.close();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }


    public  void createPdfPGMFA(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);
        try {
            // texto chino
       /*     BaseFont bfChinese= BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            Font subjectFont=new Font(bfChinese,10,Font.BOLD);
            Font titleFont=new Font(bfChinese,8,Font.BOLD);
            Font keyFont=new Font(bfChinese,6,Font.BOLD);
            Font textFont=new Font(bfChinese,6,Font.NORMAL);*/




            PdfWriter writer = PdfWriter.getInstance(document, os);

            document.open();
            Paragraph titlePara = new Paragraph("Nivel 3a: Plan General de Manejo Forestal – PGMF en bosques de CC.NN. y CC.CC. con fines de  comercialización a alta escala ",subTitulo);
            titlePara.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph(titlePara));

            Paragraph titlePara1 = new Paragraph("ANEXO 3a");
            titlePara1.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph(titlePara1));
            document.add(new Paragraph("\r\n\r\n"));

            Paragraph titlePara2 = new Paragraph("TÉRMINOS DE REFERENCIA PARA LA FORMULACIÓN DEL PLAN GENERAL DE MANEJO FORESTAL  (PGMF) EN BOSQUES DE COMUNIDADES NATIVAS Y/O CAMPESINAS CON FINES DE  COMERCIALIZACIÓN A ALTA ESCALA",titulo);
            titlePara2.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph(titlePara2));
            document.add(new Paragraph("\r\n\r\n"));

            document.add(new Paragraph("1. RESUMEN EJECUTIVO",subTitulo));
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable table = createTableInformacionGeneral(writer,idPlanManejo);
            document.add(table);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("2. OBJETIVOS DEL MANEJO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("2.1 Objetivo General",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table2 = createObjetivoGeneral(writer,idPlanManejo);
            document.add(table2);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("2.2 Objetivos Especificos",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table3 = createObjetivoEspecifico(writer,idPlanManejo);
            document.add(table3);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("3. DURACIÓN Y REVISIÓN DEL PLAN",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("3.1 Duración del Plan",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table4 = createDuracion(writer,idPlanManejo);
            document.add(table4);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("4. INFORMACIÓN BÁSICA DEL ÁREA DE MANEJO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Indicar la información básica sobre los aspectos legales, físicos, biológicos y socioeconómicos de  la comunidad y su entorno.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.1 Acreditación del Territorio Comunal",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table5 = createInfBasicaAcreditacion(writer,idPlanManejo);
            document.add(table5);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.2 Ubicación Politica",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table6 = createInfoAreaUbiPoliticaPGMFA(writer,idPlanManejo);
            document.add(table6);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.3 Coordenadas UTM del área a manejar (Mapa 01)",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.3.1 Para área de manejo ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table7 = createInfoAreaCoordenadasUTMNativaPGMFA(writer,idPlanManejo);
            document.add(table7);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.3.2 Área de manejo dividida en anexos o sectores   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En caso la comunidad este dividida en anexos o sectores y el manejo forestal se realice en mas  de un anexo o sector  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table8 = createInfoAreaCoordenadasUTMMFPGMFA(writer,idPlanManejo);
            document.add(table8);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.4 Accesibilidad (Mapa 01) ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("a) Rutas o vías de acceso terrestre (T) o fluvial (F) al área de manejo forestal",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table9 = createInfoAreaAccesibilidad1PGMFA(writer,idPlanManejo);
            document.add(table9);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("b) Indicar si es necesaria la construcción de caminos de acceso para conectar el o las área(s)" +
                    " de manejo forestal con los sistemas fluviales o terrestres  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table10 = createInfoAreaAccesibilidad2PGMFA(writer,idPlanManejo);
            document.add(table10);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.5 Aspectos Físicos ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Hidrografía y fisiografía (Mapa 01)   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("a) Ríos (principales y secundarios.), quebradas, lagunas y cochas en el área de manejo  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table11 = createInfoAreaHidrografia(writer,idPlanManejo);
            document.add(table11);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("b) Fisiografía  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table12 = createInfoAreaFisiografia(writer,idPlanManejo);
            document.add(table12);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.6 Aspectos Biológicos",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.6.1 Fauna silvestre  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Indicar la metodología empleada para la identificación de las especies de fauna silvestre " +
                    " (información secundaria, inventarios, entre otros) y las especies que hayan sido observadas  dentro del área de manejo " +
                    "forestal o aquellas de las cuales se tengan indicios de su existencia.  Además deberá indicarse el status (grado de amenaza)" +
                    " de las poblaciones de fauna incluídas  dentro del listado publicado por el INRENA ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table13 = createInfoAreaFaunaPGMFA(writer,idPlanManejo);
            document.add(table13);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.6.2 Tipos de bosque  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En el cuadro que se muestra a continuación indicar los tipos de bosques presentes en el área de  manejo, " +
                    "mencionando además sus características más relevantes y las superficies expresadas en  hectáreas (sin decimales) y en porcentaje " +
                    "(hasta un decimal).   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table14 = createInfoAreaTipoBosquePGMFA(writer,idPlanManejo);
            document.add(table14);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.7 Aspectos Socioeconómicos",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En este acápite se deberá presentar información con relación a las poblaciones dentro y en el entorno " +
                    "de la comunidad, así como información referida a la infraestructura de servicios existentes  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.7.1 Caracterización de la Comunidad  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("③ Nº de personas empadronadas:  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("③ Nº de personas que trabajan: ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table15 = createInfoAreCaracComunidad(writer,idPlanManejo);
            document.add(table15);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("4.7.2 Infraestructura de Servicios  \n" +
                    "Indicar la infraestructura con la que cuenta la comunidad  \n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table16 = createInfoAreInfraEstructurasPGMFA(writer,idPlanManejo);
            document.add(table16);
            document.add(new Paragraph("4.7.3 Antecedentes de uso del bosque e identificación de conflictos   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Describir en forma breve los antecedentes de uso en el área de la comunidad, con respecto a " +
                    "las  intervenciones de aprovechamiento de productos del bosque.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("a) Antecedentes de uso ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table17 = createInfoAreAnteUsoPGMFA(writer,idPlanManejo);
            document.add(table17);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En base a la información colectada, al conocimiento y experiencia de los miembros de la comunidad, " +
                    "identificar los conflictos existentes referidos al uso de los recursos forestales en el  área comunal.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("b) Identificación de conflictos de uso de los recursos forestales  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table18 = createInfoAreConflictoPGMFA(writer,idPlanManejo);
            document.add(table18);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("5. ORDENAMIENTO DEL ÁREA DE MANEJO ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Previo al ordenamiento del área de manejo comunal, es necesario realizar un " +
                    "ordenamiento de todo el territorio comunal, que permita la exclusión de áreas de protección6, " +
                    "tierras agropecuarias, áreas de asentamiento de la población, entre otros.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Posterior a ello, se realiza el ordenamiento del área identificada, en donde se realizará " +
                    "el manejo forestal, para la cual se establecen las categorías que se presentan en el cuadro a continuación:   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Cuadro de categoría de ordenamiento (Mapa 02)  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table19 = createOrdenamientoInternoPGMFA(writer,idPlanManejo);
            document.add(table19);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6. POTENCIAL DE PRODUCCIÓN DEL RECURSO FORESTAL   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("De acuerdo al ordenamiento, se debe haber determinado el área efectiva de manejo del recurso  forestal, donde se deberá llevar a cabo el inventario del recurso, con el objetivo de evaluar el  potencial de madera.  \n" +
                    "En el caso que uno de los objetivos del plan de manejo sea el aprovechamiento de recursos no  maderables, deberá ampliarse el inventario a los recursos específicos de interés para el manejo.  \n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("6.1 Características del inventario forestal   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("El inventario forestal maderable a realizar en el área de manejo, será por muestreo, asimismo dicho  inventario deberá considerar individuos con diámetros a partir de 30 cm9 de dap, considerando un  error de muestreo no mayor del 20% del volumen total de las especies inventariadas. Este  inventario comprenderá un sub-muestreo de fustales (individuos entre 10 cm y 30 cm. de dap), en al  menos un 20% del área de muestreo total para maderables.  \n" +
                    "Los resultados del inventario deben incluir la siguiente información:  \n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("a) Para potencial maderable (mayores de 30 cm de dap)  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("- Lista de especies encontradas en el muestreo, incluidos sus nombres comunes y científicos.  \n" +
                    "- Cuadro de valores de número de individuos (N) área basal (AB) y volumen comercial (Vc), por hectárea y total según modelo. El detalle de estos resultados incluirá además desagregado por categorías diamétricas cada 10 cm, a partir de 30 cm, y por tipo de bosque. (Anexo 02)  \n" +
                    "Se deben presentar los cálculos para determinar el error de muestreo (%) para los valores de volumen rollizo comercial por tipo de bosque y total  \n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Cuadro de Resultados de Inventario de especies maderables por muestreo  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table20 = createPotencialProduccionA(writer,idPlanManejo);
            document.add(table20);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("b) Para fustales (entre 10cm- 30 cm de dap) ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("- Lista de especies encontradas con nombres comunes y científicos\n" +
                    "- Un cuadro incluyendo número de individuos y área basal por hectárea y total, según tipo de\n" +
                    "bosque y de acuerdo al modelo siguiente. El detalle de estos resultados ser \n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Cuadro de Resultado de Inventario de Fustal",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table21 = createPotencialProduccionB(writer,idPlanManejo);
            document.add(table21);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7. MANEJO DEL BOSQUE ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.1 Actividades principales según ordenamiento  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Inicialmente se debe indicar cuales son las acciones principales a realizar en el área de manejo, atendiendo " +
                    "a las categorías de ordenamiento consideradas en el acápite 5. Para este fin se  presenta el cuadro de referencia para el llenado " +
                    "de este requerimiento, debiendo tener en cuenta  que no es necesario incluir actividades detalladas en cada una de las categorías, " +
                    "sino sobre todo  en aquellas vinculadas con los objetivos del plan de manejo.   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Cuadro de Actividades según Ordenamiento   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table22 = createActividadesOrdenamientoManejoBosque(writer,idPlanManejo);
            document.add(table22);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.1.1 Sistema de manejo   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Sistema policíclico basado en la regeneración natural ⑥  \n" +
                    "Otro sistema ⑥ (detallar)  \n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table23 = createSistemasManejoManejoBosquePGMFA(writer,idPlanManejo);
            document.add(table23);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.1.2 Producción forestal  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Si el área de manejo se encuentra continua, ésta deberá ser dividida en tantas áreas de corta anual como número de años considere el ciclo de corta propuesto.  \n" +
                    "En el caso que el área de manejo se encuentre distribuida en más de un anexo o sector de la comunidad, el aprovechamiento anual se calculará dividiendo 1/ciclo de corta considerado.  \n" +
                    "③ Duración del ciclo de corta:  \n" +
                    "  \n" +
                    "El ciclo de corta no debe ser menor a 20 años, caso contrario deberá justificarse   - 20 años ⑥ - Otro (Justificar)  \n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table24 = createDuracionCicloManejoBosquePGMFA(writer,idPlanManejo);
            document.add(table24);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Especies a aprovechar y diámetros mínimos de corta ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Se indicarán las especies a manejar y los diámetros de aprovechamiento, dichos diámetros no   deben ser menores a los establecidos según la norma. Se deberán excluir del aprovechamiento aquellas especies que por el conocimiento de las comunidades sean claves para alimentación o nidificación de animales.  \n" +
                    "Asimismo, no se podrán incluir en lista de especies a aprovechar aquellas que se encuentran incluidas en las listas oficiales de especies protegidas del INRENA.  \n",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table25 = createEspeciesAprovecharManejoBosquePGMFA(writer,idPlanManejo);
            document.add(table25);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.2 Aprovechamiento  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("El aprovechamiento anual no debe superar la superficie que se obtiene de dividir 1/ciclo de corta considerado   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.2.1 Método de aprovechamiento  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Manual ⑥ Mecanizado ⑥ Mixto ⑥  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.2.2 Infraestructura para aprovechamiento y transporte  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("a) Sistema de transporte de madera a utilizar:  \n" +
                    "Fluvial ⑥ terrestre ⑥ mixto (fluvial y terrestre) ⑥  \n" +
                    "b) Medios de transporte a utilizar entre el área de manejo y el destino de la madera:   Flotación ⑥ Chatas ⑥ Camión ⑥ Otros ⑥ (……………………….)  \n" +
                    "c) Red de caminos de acceso y principales a construir (Mapa 02)  Caminos de acceso (Km.) Caminos principales (Km)  \n" +
                    "Otros (Especifique):  \n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table26 = createRedCaminosManejoBosquePGMFA(writer,idPlanManejo);
            document.add(table26);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Especificaciones sobre los caminos:   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table27 = createEspecificacionCaminosManejoBosquePGMFA(writer,idPlanManejo);
            document.add(table27);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Para la construcción de caminos principales, secundarios y patios de trozas se deberá tomar en cuenta las especificaciones que se señalan en el cuadro a continuación: ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table28 = createInfraestructuraEspecificacionesManejoBosque(writer,idPlanManejo);
            document.add(table28);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.2.3 Operaciones de corta y arrastre    ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Describir las características de cada una de las actividades que forman parte de las operaciones de corta y arrastre     ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Cuadro de requerimientos de mano de obra y equipos para la corta y el arrastre   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table29 = createOperacionCortaArrastreManejoBosquePGMFA(writer,idPlanManejo);
            document.add(table29);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.3 Especies a proteger   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Presentar el listado de aquellas especies de flora o fauna que se piensa proteger por estar sus poblaciones en algún grado de amenaza, incluyendo la fuente de información que justifica esa decisión. Como referencia, podría usarse la lista de especies protegidas de INRENA, resultados del inventario forestal, o el conocimiento de los propios pobladores de la comunidad.  \n" +
                    "\n" +
                    "En el Plan de Gestión Ambiental se deben precisar las acciones que se desarrollarán para proteger dichas especies.\n" +
                    "a)\tFlora\n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table30 = createEspecieProtegerFloraManejoBosque(writer,idPlanManejo);
            document.add(table30);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("b)\tFauna ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table31 = createProtegerFaunaManejoBosque(writer,idPlanManejo);
            document.add(table31);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("7.4 Tratamientos silviculturales ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("El manejo silvicultural del bosque se basará principalmente en la conservación de la regeneración natural de las especies por debajo del DMC, y el mantenimiento de un stock de reserva, incluyendo los árboles semilleros, en un número mínimo de 10% por especie bajo aprovechamiento.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("a) Tratamientos previstos aplicar  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table32 = createTratamientoSilviculturalPrevistoManejoBosque(writer,idPlanManejo);
            document.add(table32);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("La regeneración natural del bosque puede ser también complementada con la reforestación, en cuyo caso se presentará la información básica siguiendo el modelo del cuadro siguiente   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("b) Reforestación  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table33 = createTratamientoSilviculturalReforestacionManejoBosque(writer,idPlanManejo);
            document.add(table33);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8. PROTECCIÓN DEL BOSQUE ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.1 Demarcación y mantenimiento de linderos  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("La comunidad debe establecer algún sistema de señalización, sea mediante mantenimiento de trochas, letreros, árboles pintados, o hitos, que permitan distinguir claramente los límites del bosque comunal.  \n" +
                    "El Plan debe describir la forma en que se cumplirá con este requerimiento.  \n",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table34 = createProteccionBosquePGMFA1(writer,idPlanManejo);
            document.add(table34);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.2 Evaluación de impacto ambiental  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("La evaluación de impacto ambiental se realiza en base a información de diferentes componentes " +
                    "del plan de manejo, desde la caracterización del área, que sirve como línea de base, hasta la identificación de los " +
                    "principales impactos y las medidas de mitigación.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.2.1 Análisis de Impacto Ambiental  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Es necesario presentar las interacciones causa-efecto de las actividades del Plan General de " +
                    "Manejo, que se espera tendrán un mayor impacto ambiental. Para este fin se puede recurrir a matrices que identifiquen " +
                    "las causas y efectos, de las actividades siguientes, u otras que se estimen podrían tener un impacto significativo.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Modelo de Matriz de identificación de Impactos Ambientales  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table35 = createProteccionBosquePGMFA2(writer,idPlanManejo);
            document.add(table35);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("8.2.2 Plan de Gestión Ambiental  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En función a los impactos identificados en la matriz y considerando los impactos negativos de mayor" +
                    " intensidad, se deben señalar las medidas que se tomarán para mitigar cada impacto, programa de vigilancia y programa de " +
                    "contingencias ambientales, usando como ejemplo el cuadro mostrado a continuación   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Programa preventivo- corrector   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table36 = createProteccionBosquePGMFA3(writer,idPlanManejo);
            document.add(table36);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Programa de vigilancia y seguimiento   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table37 = createProteccionBosquePGMFA4(writer,idPlanManejo);
            document.add(table37);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("11 Los impactos pueden ser calificados según criterios de: tipo de impacto (positivo o negativo), magnitud del impacto: leve (1),  moderado (2), severo (3) y periodicidad (temporal o permanente). Agregar las filas que sean necesarias para identificar los  impactos en cada componente \n" +
                    "12 Añadir tantas columnas como fueran necesarias para identificar las actividades contempladas en el aprovechamiento forestal  que causen impactos\n ",letraPeque));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Programas de contingencias ambientales  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table38 = createProteccionBosquePGMFA5(writer,idPlanManejo);
            document.add(table38);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("9. MONITOREO  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Se deberá desarrollar una propuesta de monitoreo, que considere aspectos importantes, como:  ③ El registro de información de cantidades de madera o productos no maderables cosechados, de costos de producción en las diferentes actividades del plan de manejo  \n" +
                    "③ Registro de conflictos o problemas sociales presentados y formas como estos han impactado la aplicación del plan de manejo, entre otros.  \n",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table39 = createMonitoreoPGMFA(writer,idPlanManejo);
            document.add(table39);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("El detalle del monitoreo, según las actividades que se lleven a cabo, deberán detallarse en el Plan Operativo Anual.    ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("10. PARTICIPACIÓN COMUNAL   ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Debe destacarse las diferentes formas en que la comunidad participará en el Plan de Manejo Forestal, elaboración, aplicación, control, seguimiento, negociaciones con terceros, y otras actividades que se  quiera destacar en cuanto a las tareas de responsabilidad de la comunidad.   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table40 = createParticipacionComunalPGMFA(writer,idPlanManejo);
            document.add(table40);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("11. CAPACITACION",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("En este punto es necesario hacer la identificación conjunta (ingeniero responsable y la comunidad) de los temas prioritarios en los cuales la población debe capacitarse, los miembros involucrados de la comunidad que deben recibir dicha capacitación, así como la modalidad de capacitación y el lugar.",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Cuadro de actividades de capacitación ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table41 = createCapacitacionPGMFA(writer,idPlanManejo);
            document.add(table41);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("12. ORGANIZACIÓN PARA EL DESARROLLO DE LA ACTIVIDAD  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Indicar la forma de organización que tiene la Comunidad para el desarrollo de la actividad forestal, indicando los participantes, las modalidades de financiamiento, los compromisos económicos, entre otros aspectos.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table42 = createActividadSilviculturalOrganizPGMFA(writer,idPlanManejo);
            document.add(table42);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("13. FLUJO DE CAJA ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("13.1 I.INGRESOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table43 = createRentabilidadIngresos(writer,idPlanManejo);
            document.add(table43);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("13.2 II.EGRESOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table44 = createRentabilidadEgresos(writer,idPlanManejo);
            document.add(table44);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("14. CRONOGRAMA DE ACTIVIDADES ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Se deberá presentar un cronograma de actividades durante los años de vigencia del Plan General de Manejo Forestal, de acuerdo al modelo presentado en el cuadro siguiente:\n" +
                    "\n" +
                    "Cuadro de cronograma de actividades\n ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table45 = createActCronogramaPGMFA(writer,idPlanManejo);
            document.add(table45);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("15. ASPECTOS COMPLEMENTARIOS  ",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table46 = createAspectosCompPGMFA(writer,idPlanManejo);
            document.add(table46);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable createTableInformacionGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        //1 - Resumen Ejecutivo
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PGMFA");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            //o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            //o.setProvincia(lstacceso.get(0).getNombreProvincia());
            //o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }

        cell = new PdfPCell(new Paragraph("1. Del Titular del Permiso",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);



        cell = new PdfPCell(new Paragraph( o.getNombreElaboraDema()!=null? "Nombre de la comunidad: "+o.getNombreElaboraDema():"",contenido));
        cell.setColspan(4);
        cell.setFixedHeight(45);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre del Jefe de la comunidad o representante legal: ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRepresentanteLegal()!=null?o.getRepresentanteLegal():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nro de la credencial",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDescripcion()!=null? o.getDescripcion():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nro de RUC",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroRucComunidad()!=null? o.getNroRucComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Federación u organización a la que pertenece la comunidad",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFederacionComunidad()!=null? o.getFederacionComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("2. Del Plan General de Manejo Forestal",subTitulo));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Ing. Forestal que elaboró el PGMF: ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!= null ? o.getRegente().getNombres() + " " + o.getRegente().getApellidos():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Certificado de habilitación del Ing. Forestal:  ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(""));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("N° inscripción en el registro de consultores de  INRENA:   ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroResolucionComunidad()!=null? o.getNroResolucionComunidad():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha de presentación del PGMF:    ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha de inicio: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Fecha de finalización: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Potencial maderable (m3 totales):   ",subTitulo2));
        cell.setColspan(4);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(o.getDepartamento()!=null? "Departamento: "+o.getDepartamento():"",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getProvincia()!=null? "Provincia: " +o.getProvincia():"",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDistrito()!=null? "Distrito: "+o.getDistrito():"",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nº de sectores o anexos:  ",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getNroAnexosComunidad()!= null ? o.getNroAnexosComunidad().toString():"",contenido));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Superficie de la comunidad (ha):",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getSuperficieHaMaderables()!=null? o.getSuperficieHaMaderables():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área de manejo forestal  (ha): ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getAreaTotal()!=null? o.getAreaTotal().toString():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        return table;
    }

    public  PdfPTable createObjetivoGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        Font contenido= new Font(Font.HELVETICA, 11f, Font.COURIER);

        //2 - Objetivos de Manejo
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PGMFA", idPlanManejo);

        int contador=0;
        for(ObjetivoDto ob : lstObjetivos.getData()){
           if(contador==0){
               cell = new PdfPCell(new Paragraph(ob.getDescripcion()!=null?ob.getDescripcion():"",contenido));
               cell.setFixedHeight(50);
               table.addCell(cell);
           }

            contador++;
        }

        return table;
    }

    public  PdfPTable createObjetivoEspecifico(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        //2 - Objetivos de Manejo
        ResultEntity<ObjetivoDto> lstObjetivos = objetivoservice.listarObjetivos("PGMFA", idPlanManejo);
        List<ObjetivoDto> maderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("Productos a obtener del manejo ",cabecera));
        cell.setColspan(3);
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maderable",subTituloTabla));
        cell.setRowspan(maderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaM="";
        for(ObjetivoDto obM : maderables){
            if(obM.getActivo().equals("A")){
                marcaM="(X)";
            }else{marcaM="";}
            cell = new PdfPCell(new Paragraph(marcaM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }


        List<ObjetivoDto> NMaderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("No Maderable"))
                .collect(Collectors.toList());

        cell = new PdfPCell(new Paragraph("No Maderable",subTituloTabla));
        cell.setRowspan(NMaderables.size());
        cell.setFixedHeight(size);
        table.addCell(cell);
        String marcaNM="";
        for(ObjetivoDto obNM : NMaderables){
            if(obNM.getActivo().equals("I")){
                marcaNM="(X)";
            }else{marcaNM="";}
            cell = new PdfPCell(new Paragraph(marcaNM,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(obNM.getDescripcionDetalle(),contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }

    public  PdfPTable createDuracion(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;


        //3 - DURACION Y REVISION DEL PLAN
        InformacionGeneralDEMAEntity D = new InformacionGeneralDEMAEntity();
        D.setIdPlanManejo(idPlanManejo);
        D.setCodigoProceso("PGMFA");
        D = infoGeneralDemaRepo.listarInformacionGeneralDema(D).getData().get(0);
        String fechaInicio="";
        String fechaFinal="";
        if(D.getFechaInicioDema()!=null || D.getFechaInicioDema().equals("")){
            fechaInicio="Fecha de Inicio: "+formatter.format(D.getFechaInicioDema());
        }
        if(D.getFechaFinDema()!=null || D.getFechaFinDema().equals("")){
            fechaFinal="Fecha de Finalización: "+formatter.format(D.getFechaFinDema());
        }

        cell = new PdfPCell(new Paragraph(fechaInicio,contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(fechaFinal,contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        return table;
    }

    public  PdfPTable createInfBasicaAcreditacion(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;


        //4 -INFORMACION BASICA DEL AREA DE MANEJO
        //4.1
        List<InfBasicaAereaDetalleDto> lstInfoAreaAcreditacionTerritorio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMATC");

        cell = new PdfPCell(new Paragraph("Tipo de Documento ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Número ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Superficie (ha) ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);

        for(InfBasicaAereaDetalleDto detalle: lstInfoAreaAcreditacionTerritorio){
            cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null? detalle.getReferencia(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getObservaciones() != null? detalle.getObservaciones(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null? detalle.getAreaHa().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }

    public  PdfPTable createInfoAreaUbiPoliticaPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.2
        List<InfBasicaAereaDetalleDto> lstInfoAreaUbiPolitica = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMUP");
        String departamento = "", provincia = "", distrito = "";
        for (InfBasicaAereaDetalleDto detalleEntityUbigeo : lstInfoAreaUbiPolitica) {
            List<InfBasicaAereaDetalleDto> lstInfoAreaUbigeo = informacionBasicaRepository.listarInfBasicaAereaUbigeo(detalleEntityUbigeo.getDepartamento(), detalleEntityUbigeo.getProvincia(), detalleEntityUbigeo.getDistrito());
            for (InfBasicaAereaDetalleDto detalleEntityUbigeo2 : lstInfoAreaUbigeo) {
                departamento = detalleEntityUbigeo2.getDepartamento();
                provincia = detalleEntityUbigeo2.getProvincia();
                distrito = detalleEntityUbigeo2.getDistrito();
            }
        }

        cell = new PdfPCell(new Paragraph("Departamento ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Provincia ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distrito ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Cuenca / Sub Cuenca ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);

        for(InfBasicaAereaDetalleDto detalle: lstInfoAreaUbiPolitica){
            cell = new PdfPCell(new Paragraph(departamento,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(provincia,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(distrito,contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getCuenca() != null? detalle.getCuenca(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }

        return table;
    }


    public  PdfPTable createInfoAreaCoordenadasUTMNativaPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.3.1
        //Coordenadas UTM Comunidad Nativa
        List<InfBasicaAereaDetalleDto> lstInfoAreaCoordenadasUTMNativa = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMCUMC");

        cell = new PdfPCell(new Paragraph("Punto",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Referencia",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaCoordenadasUTMNativa!=null){
            for(InfBasicaAereaDetalleDto detalle: lstInfoAreaCoordenadasUTMNativa){

                cell = new PdfPCell(new Paragraph(detalle.getPuntoVertice() != null? detalle.getPuntoVertice().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null? detalle.getCoordenadaEste().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null? detalle.getCoordenadaNorte().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null? detalle.getReferencia().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }


        return table;
    }


    public  PdfPTable createInfoAreaCoordenadasUTMMFPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.3.2 Coordenadas UTM de la Unidad de Manejo Forestal de la Comunidad
        List<InfBasicaAereaDetalleDto> lstInfoAreaCoordenadasUTMMF = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMCUMD");

        cell = new PdfPCell(new Paragraph("Anexo o sector",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Punto",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este (E)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte (N)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Referencia",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaCoordenadasUTMMF!=null){
            for(InfBasicaAereaDetalleDto detalle: lstInfoAreaCoordenadasUTMMF){

                cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getPuntoVertice() != null? detalle.getPuntoVertice().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null? detalle.getCoordenadaEste().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null? detalle.getCoordenadaNorte().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null? detalle.getReferencia().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }


        return table;
    }


    public  PdfPTable createInfoAreaAccesibilidad1PGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.4
        List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMA");


        cell = new PdfPCell(new Paragraph("Anexo o sector",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("SubParcela",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Punto de Referencia (carretera, etc)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vía",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distancia (km)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tiempo (horas)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tipo de Vehículo",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaAccesibilidad!=null){
            List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad1 = lstInfoAreaAccesibilidad.stream().filter(a -> a.getCodSubInfBasicaDet().equals("TAB_1")).collect(Collectors.toList());
            if(lstInfoAreaAccesibilidad1!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoAreaAccesibilidad1) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getPuntoVertice() != null ? detalle.getPuntoVertice().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMedioTransporte() != null ? detalle.getMedioTransporte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getDistanciaKm() != null ? detalle.getDistanciaKm().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getTiempo() != null ? detalle.getTiempo().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getZonaVida() != null ? detalle.getZonaVida().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }
        return table;
    }

    public  PdfPTable createInfoAreaAccesibilidad2PGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.4
        List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMA");

        if(lstInfoAreaAccesibilidad!=null){
            List<InfBasicaAereaDetalleDto> lstInfoAreaAccesibilidad2 = lstInfoAreaAccesibilidad.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2")).collect(Collectors.toList());
            if(lstInfoAreaAccesibilidad2!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoAreaAccesibilidad2) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(50);
                    table.addCell(cell);
                }
            }
        }
        return table;
    }


    public  PdfPTable createInfoAreaHidrografia(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        cell = new PdfPCell(new Paragraph("Ríos",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Quebradas",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Lagunas",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        //4.5
        List<InfBasicaAereaDetalleDto> lstInfoAreaHF = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAF");

        if(lstInfoAreaHF!=null){
            //HIDROGRAFIA 10
            List<InfBasicaAereaDetalleDto> lstInfoAreaHidrografia = lstInfoAreaHF.stream().filter(h -> h.getCodSubInfBasicaDet().equals("TAB_1")).collect(Collectors.toList());
            if(lstInfoAreaHidrografia!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoAreaHidrografia) {

                    cell = new PdfPCell(new Paragraph(detalle.getNombreRio() != null ? detalle.getNombreRio().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreQuebrada() != null ? detalle.getNombreQuebrada().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreLaguna() != null ? detalle.getNombreLaguna().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }


    public  PdfPTable createInfoAreaFisiografia(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.5
        List<InfBasicaAereaDetalleDto> lstInfoAreaHF = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAF");

        cell = new PdfPCell(new Paragraph("Unidades Fisiográficas ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especificaciones  \n" +
                "(pendiente/altura) \n ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área  \n" +
                "aproximada (ha) \n ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaHF!=null){
            //FISIOGRAFIA 11
            List<InfBasicaAereaDetalleDto> lstInfoAreaFisiografia = lstInfoAreaHF.stream().filter(f -> f.getCodSubInfBasicaDet().equals("TAB_2")).collect(Collectors.toList());

            if(lstInfoAreaFisiografia!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoAreaFisiografia) {

                    cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }
        return table;
    }

    public  PdfPTable createInfoAreaFaunaPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.6
        //4.61
        //FAUNA SILVESTRE 12
        List<InfBasicaAereaDetalleDto> lstInfoAreaFAU = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAB");

        cell = new PdfPCell(new Paragraph("Nombre Común ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre Cientifico ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Status ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


            if(lstInfoAreaFAU!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfoAreaFAU) {

                    cell = new PdfPCell(new Paragraph(detalle.getNombreComun() != null ? detalle.getNombreComun().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreCientifico() != null ? detalle.getNombreCientifico().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getFamilia() != null ? detalle.getFamilia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        return table;
    }


    public  PdfPTable createInfoAreaTipoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.6.2
        List<InfBasicaAereaDetalleDto> lstInfoAreaTipoBosque = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMABTB");

        cell = new PdfPCell(new Paragraph("Tipo de Bosque",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("SubParcela",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripcion",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área (ha)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaTipoBosque!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoAreaTipoBosque) {

                cell = new PdfPCell(new Paragraph(detalle.getNombre() != null ? detalle.getNombre().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getObservaciones() != null ? detalle.getObservaciones().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHaPorcentaje() != null ? detalle.getAreaHaPorcentaje().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }
    public  PdfPTable createInfoAreCaracComunidad(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        float[] values = new float[2];
        values[0] = 200;
        values[1] = 15;
        table.setWidths(values);

        //4.7 Aspectos socioeconómicos
        List<InfBasicaAereaDetalleDto> lstInfoAreaAspectosSocio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAS");

        cell = new PdfPCell(new Paragraph("Principales actividades económicas ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaAspectosSocio!=null) {
            //4.7.1 Caracterización de la Comunidad 14
            List<InfBasicaAereaDetalleDto> lstCaracCom = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1")).collect(Collectors.toList());
            if(lstCaracCom!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstCaracCom) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public  PdfPTable createInfoAreInfraEstructurasPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.7 Aspectos socioeconómicos
        List<InfBasicaAereaDetalleDto> lstInfoAreaAspectosSocio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAS");

        cell = new PdfPCell(new Paragraph("Infraestructura",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Zona UTM",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Referencia",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaAspectosSocio!=null) {
            //4.7.2 Infraestructura de Servicios 15
            List<InfBasicaAereaDetalleDto> lstInfra = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2")).collect(Collectors.toList());
            if(lstInfra!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstInfra) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null ? detalle.getCoordenadaEste().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null ? detalle.getCoordenadaNorte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getZonaVida() != null ? detalle.getZonaVida().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public  PdfPTable createInfoAreAnteUsoPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;
        float[] values = new float[4];
        values[0] = 110;
        values[1] = 15;
        values[2] = 130;
        values[3] = 130;
        table.setWidths(values);

        //4.7 Aspectos socioeconómicos
        List<InfBasicaAereaDetalleDto> lstInfoAreaAspectosSocio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAS");

        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especies más extraídas",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Observaciones",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaAspectosSocio!=null) {
            //4.7.3 Antecedentes de uso del bosque e identificación de conflictos
            //a 16
            List<InfBasicaAereaDetalleDto> lstAnteUso = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_3")).collect(Collectors.toList());
            if(lstAnteUso!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstAnteUso) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreLaguna() != null ? detalle.getNombreLaguna().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }


    public  PdfPTable createInfoAreConflictoPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //4.7 Aspectos socioeconómicos
        List<InfBasicaAereaDetalleDto> lstInfoAreaAspectosSocio = informacionBasicaRepository.listarInfBasicaAerea("PGMFA", idPlanManejo, "PGMFAIBAMAS");

        cell = new PdfPCell(new Paragraph("Conflicto",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Propuesta de solución",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoAreaAspectosSocio!=null) {
            //4.7.3 Antecedentes de uso del bosque e identificación de conflictos
            //b 17
            List<InfBasicaAereaDetalleDto> lstConflic = lstInfoAreaAspectosSocio.stream().filter(c -> c.getCodSubInfBasicaDet().equals("TAB_4")).collect(Collectors.toList());
            if(lstConflic!=null) {
                for (InfBasicaAereaDetalleDto detalle : lstConflic) {

                    cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null ? detalle.getReferencia().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }


    public  PdfPTable createOrdenamientoInternoPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        float[] values = new float[3];
        values[0] = 150;
        values[1] = 50;
        values[2] = 50;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        //5.	ORDENAMIENTO INTERNO
        OrdenamientoInternoDetalleDto ordenaRequest = new OrdenamientoInternoDetalleDto();
        ordenaRequest.setIdPlanManejo(idPlanManejo);
        ordenaRequest.setCodTipoOrdenamiento("PGMFA");
        List<OrdenamientoInternoDetalleDto> lstOrdenamiento = ordenamientoRepo.ListaROrdenamientoInternoPMFI(ordenaRequest);

        cell = new PdfPCell(new Paragraph("Categoría de ordenamiento",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Area (ha)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstOrdenamiento!=null) {

                for (OrdenamientoInternoDetalleDto detalle : lstOrdenamiento) {

                    cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHA() != null ? detalle.getAreaHA().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAreaHAPorcentaje() != null ? detalle.getAreaHAPorcentaje().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
        }

        return table;
    }


    public  PdfPTable createPotencialProduccion1aPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPCell cell;
        int size=20;

        //6. Potencial de Produccion del Recurso Forestal
        List<PotencialProduccionForestalEntity> lst = potencialProduccionForestalRepository.ListarPotencialProducForestal(idPlanManejo, "PGMFA");

        int contador=0;
        if(lst!=null) {
            List<PotencialProduccionForestalEntity> lst1a = lst.stream().filter(a -> a.getCodigoSubTipoPotencialProdForestal().equals("PGMFAA")).collect(Collectors.toList());
            if(lst1a!=null) {
                for (PotencialProduccionForestalEntity detalle : lst1a) {

                    List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                    if (lst1a.size() > 0) {
                        PotencialProduccionForestalEntity PGMFEVALI = lst1a.get(contador);
                        lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                    }
                    contador++;
                }
            }

        }
        PdfPTable table = new PdfPTable(contador+2);
        table.setWidthPercentage(90);
        System.out.println(contador);
/*
        cell = new PdfPCell(new Paragraph("Tipo de Bosque",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Variable",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lst!=null) {
            List<PotencialProduccionForestalEntity> lst1a = lst.stream().filter(a -> a.getCodigoSubTipoPotencialProdForestal().equals("PGMFAA")).collect(Collectors.toList());
            if(lst1a!=null) {

            for (OrdenamientoInternoDetalleDto detalle : lstOrdenamiento) {

                cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHA() != null ? detalle.getAreaHA().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHAPorcentaje() != null ? detalle.getAreaHAPorcentaje().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }*/

        return table;
    }


    public  PdfPTable createActividadesOrdenamientoManejoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //7. MANEJO DEL BOSQUE 	ORDENAMIENTO INTERNO
        // 7.1 Actividades principales según ordenamiento  23 PGMFAMBAPO,PGMFAMBAPO
        ManejoBosqueEntity oManejoBosque = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBAPO", "PGMFAMBDSM").get(0);


        cell = new PdfPCell(new Paragraph("Categoría de ordenamiento",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Actividades Principales",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejoBosque!=null) {
            ManejoBosqueEntity o71 = SerializationUtils.clone(oManejoBosque);
            if(o71!=null) {
                o71.setListManejoBosqueDetalle(o71.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBAPO")).collect(Collectors.toList()));
                if(o71!=null && o71.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o71.getListManejoBosqueDetalle()) {
                        cell = new PdfPCell(new Paragraph(detalle.getOrdenamiento() != null ? detalle.getOrdenamiento().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createSistemasManejoManejoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        //7. MANEJO DEL BOSQUE 	ORDENAMIENTO INTERNO
        // 7.1 Actividades principales según ordenamiento  23 PGMFAMBAPO,PGMFAMBAPO
        ManejoBosqueEntity oManejoBosque = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBAPO", "PGMFAMBDSM").get(0);


        if(oManejoBosque!=null) {
            // 7.1.1 sistema manejo 24  PGMFAMBAPO,PGMFAMBDSM
            ManejoBosqueEntity o711 = SerializationUtils.clone(oManejoBosque);
            if(o711!=null) {
                o711.setListManejoBosqueDetalle(o711.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBDSM")).collect(Collectors.toList()));
                if(o711!=null && o711.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o711.getListManejoBosqueDetalle()) {
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcionOrd() != null ? detalle.getDescripcionOrd().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createDuracionCicloManejoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        //7. MANEJO DEL BOSQUE 	ORDENAMIENTO INTERNO
        // 7.1 Actividades principales según ordenamiento  23 PGMFAMBAPO,PGMFAMBAPO
        ManejoBosqueEntity oManejoBosque = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBAPO", "PGMFAMBDSM").get(0);


        if(oManejoBosque!=null) {
            // 7.1.2 duracion del ciclo 25  PGMFAMBAPO,PGMFAMBDPF
            ManejoBosqueEntity o712 = SerializationUtils.clone(oManejoBosque);

            if(o712!=null) {
                o712.setListManejoBosqueDetalle(o712.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBDPF")).collect(Collectors.toList()));
                if(o712!=null && o712.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o712.getListManejoBosqueDetalle()) {
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcionOrd() != null ? detalle.getDescripcionOrd().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createEspeciesAprovecharManejoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //7. MANEJO DEL BOSQUE 	ORDENAMIENTO INTERNO
        // 7.1 Actividades principales según ordenamiento  23 PGMFAMBAPO,PGMFAMBAPO
        ManejoBosqueEntity oManejoBosque = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBAPO", "PGMFAMBDSM").get(0);

        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Linea de producción", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Diametros (cm)", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejoBosque!=null) {
            // 7.1.2 especies a aprovechar 26  PGMFAMBAPO,PGMFAACPORESP
            ManejoBosqueEntity o712b = SerializationUtils.clone(oManejoBosque);
            if(o712b!=null) {
                o712b.setListManejoBosqueDetalle(o712b.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAACPORESP")).collect(Collectors.toList()));
                if(o712b!=null) {
                    List<ManejoBosqueDetalleEntity> lstDetalle = o712b.getListManejoBosqueDetalle().stream()
                            .filter(m -> m.getCodtipoManejoDet().equals("PGMFAACPORESP"))
                            .collect(Collectors.toList());
                    if(lstDetalle!=null) {
                        for (ManejoBosqueDetalleEntity detalle : lstDetalle) {
                            cell = new PdfPCell(new Paragraph(detalle.getEspecie() != null ? detalle.getEspecie().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getLineaProduccion() != null ? detalle.getLineaProduccion().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getNuDiametro() != null ? detalle.getNuDiametro().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }
                }
            }
        }
        return table;
    }



    public  PdfPTable createRedCaminosManejoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=50;

        // 7.1.2 CAP 27
        // 7.2 aprovechamiento
        // 7.2.1 red de caminos 28  PGMFAAPRO,PGMFAMBIAT
        ManejoBosqueEntity oManejo2 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAAPRO", "PGMFAMBIAT").get(0);


        if(oManejo2!=null) {
            ManejoBosqueEntity o721 = SerializationUtils.clone(oManejo2);

            if(o721!=null) {
                o721.setListManejoBosqueDetalle(o721.getListManejoBosqueDetalle().stream()
                        .filter(q -> q.getCodtipoManejoDet().contains("PGMFAMBIAT")).collect(Collectors.toList()));
                if(o721!=null && o721.getListManejoBosqueDetalle()!=null) {
                        for (ManejoBosqueDetalleEntity detalle : o721.getListManejoBosqueDetalle()) {
                            cell = new PdfPCell(new Paragraph(detalle.getDescripcionCamino() != null ? detalle.getDescripcionCamino().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                }
            }
        }
        return table;
    }


    public  PdfPTable createEspecificacionCaminosManejoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        // 7.1.2 CAP 27
        // 7.2 aprovechamiento
        // 7.2.1 red de caminos 28  PGMFAAPRO,PGMFAMBIAT
        ManejoBosqueEntity oManejo2 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAAPRO", "PGMFAMBIAT").get(0);

        cell = new PdfPCell(new Paragraph("Infraestructura vial", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Características Técnicas", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Métodos de construcción", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Mano de Obra (tipo y número)", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maquinaria (tipo y número", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejo2!=null) {
            ManejoBosqueEntity o721b = SerializationUtils.clone(oManejo2);
            if(o721b!=null) {
                o721b.setListManejoBosqueDetalle(o721b.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAAPROCA")).collect(Collectors.toList()));
                if(o721b!=null && o721b.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o721b.getListManejoBosqueDetalle()) {
                        cell = new PdfPCell(new Paragraph(detalle.getInfraestructura() != null ? detalle.getInfraestructura().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getCaracteristicasTecnicas() != null ? detalle.getCaracteristicasTecnicas().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMetodoConstruccion() != null ? detalle.getMetodoConstruccion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getManoObra() != null ? detalle.getManoObra().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMaquina() != null ? detalle.getMaquina().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createInfraestructuraEspecificacionesManejoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph("Infraestructura", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especificaciones", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Caminos principales", contenido));
        cell.setFixedHeight(80);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Adecuado sistema de drenaje de las aguas de lluvia Ancho de superficie de " +
                "rodaje entre 5 m y 6m (según volumen de transporte programado) Pendientes menores al 12% Obras de conservación " +
                "necesarias para minimizar la erosión y los daños al suelo y las aguas Al concluir la operación, asegurar el " +
                "mantenimiento apropiado , incluyendo la superficie de rodaje y las estructuras de drenaje De ser posible, " +
                "tener una capa de revestimiento de cascajo de al menos 5cm. de espesor.", contenido));
        cell.setFixedHeight(80);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Caminos Secundarios", contenido));
        cell.setFixedHeight(50);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Pendientes menores al 14% Anchura carrozable de 3 a 4 m Utilización normalmente restringida " +
                "a los períodos mas secos del año Contar con un eficiente sistema de drenaje Evitar atravesar cursos de agua " +
                "Los pasos de agua son funcionales final de la operación se clausuran y se toman medidas para evitar la erosión y restituir " +
                "las funciones y procesos del sistema natural de drenajes", contenido));
        cell.setFixedHeight(80);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Patios de trozas", contenido));
        cell.setFixedHeight(80);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(" Ubicación, densidad y tamaño serán apropiados, en función al volumen de madera, " +
                "la maquinaria disponible y las condiciones de sitio Localización próxima a las márgenes de los caminos y a un radio de distancia " +
                "de operación del tractor forestal de hasta 1 km. Dentro de lo posible, la pendiente en el patio no deberá superar el 5% y " +
                "drenarán directamente a los caminos ni a los arroyos circundantes deberán estar ubicados a más de 50 m de ríos o quebradas", contenido));
        cell.setFixedHeight(80);
        table.addCell(cell);

        return table;
    }


    public  PdfPTable createOperacionCortaArrastreManejoBosquePGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        // 7.2 aprovechamiento
        // 7.2.1 red de caminos 28  PGMFAAPRO,PGMFAMBIAT
        ManejoBosqueEntity oManejo2 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAAPRO", "PGMFAMBIAT").get(0);

        cell = new PdfPCell(new Paragraph("Operaciones", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Mano de Obra (tipo y número)", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Equipos/Maquinaria (tipo y número)", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejo2!=null) {
            // 7.2.2 operaciones corta arrastre 31  PGMFAAPRO,PGMFAMBOCA
            ManejoBosqueEntity o722 = SerializationUtils.clone(oManejo2);

            if(o722!=null) {
                o722.setListManejoBosqueDetalle(o722.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAMBOCA")).collect(Collectors.toList()));
                if(o722!=null && o722.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o722.getListManejoBosqueDetalle()) {

                        cell = new PdfPCell(new Paragraph(detalle.getDescripcionOrd() != null ? detalle.getDescripcionOrd().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getManoObra() != null ? detalle.getManoObra().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getMaquina() != null ? detalle.getMaquina().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcionOtro() != null ? detalle.getDescripcionOtro().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }

    public  PdfPTable createEspecieProtegerFloraManejoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        // 7.3 especies a proteger
        // 7.3.a flora 32  PGMFAMBEP,PGMFAESPRFLO
        ManejoBosqueEntity oManejo3 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBEP", "PGMFAESPRFLO").get(0);

        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Justificación", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejo3!=null) {
            ManejoBosqueEntity o731a = SerializationUtils.clone(oManejo3);
            if(o731a!=null) {
                o731a.setListManejoBosqueDetalle(o731a.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAESPRFLO")).collect(Collectors.toList()));
                if(o731a!=null && o731a.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o731a.getListManejoBosqueDetalle()) {

                        cell = new PdfPCell(new Paragraph(detalle.getEspecie() != null ? detalle.getEspecie().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getJustificacionEspecie() != null ? detalle.getJustificacionEspecie().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }

    public  PdfPTable createProtegerFaunaManejoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        // 7.3 especies a proteger
        // 7.3.a flora 32  PGMFAMBEP,PGMFAESPRFLO
        ManejoBosqueEntity oManejo3 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBEP", "PGMFAESPRFLO").get(0);

        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Justificación", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejo3!=null) {
            // 7.3.b fauna 33  PGMFAMBEP,PGMFAESPRFAU
            ManejoBosqueEntity o731b = SerializationUtils.clone(oManejo3);

            if(o731b!=null) {
                o731b.setListManejoBosqueDetalle(o731b.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFAESPRFAU")).collect(Collectors.toList()));
                if(o731b!=null && o731b.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o731b.getListManejoBosqueDetalle()) {

                        cell = new PdfPCell(new Paragraph(detalle.getEspecie() != null ? detalle.getEspecie().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getJustificacionEspecie() != null ? detalle.getJustificacionEspecie().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createTratamientoSilviculturalPrevistoManejoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        // 7.4 tratamientos silviculturales
        // 7.4.a tratam previstos 34  PGMFAMBTS,PGMFATRASILT
        ManejoBosqueEntity oManejo4 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBTS", "PGMFATRASILT").get(0);

        cell = new PdfPCell(new Paragraph("Tratamiento Silvicultural", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del Tratamiento", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(oManejo4!=null) {
            ManejoBosqueEntity o74a = SerializationUtils.clone(oManejo4);
            if(o74a!=null) {
                o74a.setListManejoBosqueDetalle(o74a.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFATRASILT")).collect(Collectors.toList()));
                if(o74a!=null && o74a.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o74a.getListManejoBosqueDetalle()) {

                        cell = new PdfPCell(new Paragraph(detalle.getTratamientoSilvicultural() != null ? detalle.getTratamientoSilvicultural().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getAccion() == true ? "X" : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcionTratamiento() != null ? detalle.getDescripcionTratamiento().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createTratamientoSilviculturalReforestacionManejoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        // 7.4 tratamientos silviculturales
        // 7.4.a tratam previstos 34  PGMFAMBTS,PGMFATRASILT
        ManejoBosqueEntity oManejo4 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMFA", "PGMFAMBTS", "PGMFATRASILT").get(0);

        if(oManejo4!=null) {
            // 7.4.b reforestacion 35  PGMFAMBTS,PGMFATRASILR
            ManejoBosqueEntity o74b = SerializationUtils.clone(oManejo4);

            if(o74b!=null) {
                o74b.setListManejoBosqueDetalle(o74b.getListManejoBosqueDetalle().stream()
                        .filter(p -> p.getCodtipoManejoDet().contains("PGMFATRASILR")).collect(Collectors.toList()));
                if(o74b!=null && o74b.getListManejoBosqueDetalle()!=null) {
                    for (ManejoBosqueDetalleEntity detalle : o74b.getListManejoBosqueDetalle()) {

                        cell = new PdfPCell(new Paragraph(detalle.getTratamientoSilvicultural() != null ? detalle.getTratamientoSilvicultural().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getReforestacion() != null ? detalle.getReforestacion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createProteccionBosquePGMFA1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;
        cell = new PdfPCell(new Paragraph( "SISTEMA DE DEMARCACIÓN", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "MODO DE IMPLEMENTACIÓN", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        //8. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosque = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "PGMFA", "AMB");

        if(lstProteccionBosque!=null) {
                    for (ProteccionBosqueDetalleDto detalle : lstProteccionBosque) {
                        if (detalle.getCodPlanGeneralDet().equals("PGMFADEMA")) {
                            cell = new PdfPCell(new Paragraph(detalle.getTipoMarcacion() != null ? detalle.getTipoMarcacion().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getNuAccion() == true ? "X" : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                            cell = new PdfPCell(new Paragraph(detalle.getImplementacion() != null ? detalle.getImplementacion().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }
        }
        return table;
    }


    public  PdfPTable createProteccionBosquePGMFA2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(9);
        table.setWidthPercentage(90);
        float[] values = new float[9];
        values[0] = 110;
        values[1] = 130;
        values[2] = 30;
        values[3] = 30;
        values[4] = 30;
        values[5] = 30;
        values[6] = 30;
        values[7] = 30;
        values[8] = 30;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;
        cell = new PdfPCell(new Paragraph( "FACTORES AMBIENTALES", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "IMPACTOS IDENTIFICADOS", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Actividades (causa)", cabecera));
        cell.setFixedHeight(size);
        cell.setColspan(7);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Censo", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Demarcación de linderos", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Construcción de campamentos", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Construcción de caminos", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Tala", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Arrastre", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Otras", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        //8. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosque = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "PGMFA", "AMB");

        if(lstProteccionBosque!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosque) {
                if (detalle.getCodPlanGeneralDet().equals("PGMFAANIM")) {
                    cell = new PdfPCell(new Paragraph(detalle.getFactorAmbiental() != null ? detalle.getFactorAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuCenso() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuDemarcacionLineal() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuConstruccionCampamento() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuConstruccionCamino() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuTala() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuArrastre() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNuOtra() == true ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }



    public  PdfPTable createProteccionBosquePGMFA3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph( "Actividades", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Descripción del Impacto", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Medidas de mitigación ambiental", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //8. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosque = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "PGMFA", "AMB");

        if(lstProteccionBosque!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosque) {
                if (detalle.getCodPlanGeneralDet().equals("PGMFAPGA") &&
                        detalle.getSubCodPlanGeneralDet().equals("PGMFAPRCO")) {
                    cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMitigacionAmbiental() != null ? detalle.getMitigacionAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }



    public  PdfPTable createProteccionBosquePGMFA4(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph( "Actividades", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Descripción del Impacto", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Medidas de mitigación ambiental", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //8. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosque = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "PGMFA", "AMB");

        if(lstProteccionBosque!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosque) {
                if (detalle.getCodPlanGeneralDet().equals("PGMFAPGA") &&
                        detalle.getSubCodPlanGeneralDet().equals("PGMFAVISE")) {
                    cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMitigacionAmbiental() != null ? detalle.getMitigacionAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }


    public  PdfPTable createProteccionBosquePGMFA5(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph( "Contingencia", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Descripción del Impacto", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Medidas de mitigación ambiental", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //8. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosque = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "PGMFA", "AMB");

        if(lstProteccionBosque!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosque) {
                if (detalle.getCodPlanGeneralDet().equals("PGMFAPGA") &&
                        detalle.getSubCodPlanGeneralDet().equals("PGMFACOAM")) {
                    cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMitigacionAmbiental() != null ? detalle.getMitigacionAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }


    public  PdfPTable createMonitoreoPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //9. Monitoreo 41
        MonitoreoEntity monitoreo = new MonitoreoEntity();
        monitoreo.setIdPlanManejo(idPlanManejo);
        monitoreo.setCodigoMonitoreo("PGMFA");
        List<MonitoreoDetalleEntity> lstMonitoreo = monitoreoRepository.listarMonitoreo(monitoreo).getData().getLstDetalle();

        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción de monitoreo ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsable",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstMonitoreo!=null) {
            for (MonitoreoDetalleEntity detalle : lstMonitoreo) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createParticipacionComunalPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //10.	PARTICIPACION COMUNAL 42
        ParticipacionComunalEntity participacionRequest = new ParticipacionComunalEntity();
        participacionRequest.setCodTipoPartComunal("PGMFA");
        participacionRequest.setIdPlanManejo(idPlanManejo);
        ResultClassEntity<List<ParticipacionComunalEntity>> lstParticipacion = participacionComunalRepository.ListarPorFiltroParticipacionComunal(participacionRequest);


        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsables ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Seguimiento, actividades ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstParticipacion.getData()!=null) {
            for (ParticipacionComunalEntity detalle : lstParticipacion.getData()) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getSeguimientoActividad() != null ? detalle.getSeguimientoActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createCapacitacionPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //10. Capacitacion 32
        CapacitacionDto obj = new CapacitacionDto();
        obj.setIdPlanManejo(idPlanManejo);
        obj.setCodTipoCapacitacion("PGMFA");
        List<CapacitacionDto> lstCapacitacion = capacitacionRepository.ListarCapacitacion(obj);

        cell = new PdfPCell(new Paragraph("Temas o actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal a capacitar", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modalidad de capacitación  ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Lugar de Capacitación ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        String temas="";
        if(lstCapacitacion!=null) {
            for (CapacitacionDto detalle : lstCapacitacion) {

                for (CapacitacionDetalleEntity t : detalle.getListCapacitacionDetalle()) {
                    temas += t.getActividad() != null ? t.getActividad() + ",\n" : "";
                }
                cell = new PdfPCell(new Paragraph(temas, contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(detalle.getPersonaCapacitar() != null ? detalle.getPersonaCapacitar().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getIdTipoModalidad() != null ? detalle.getIdTipoModalidad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getLugar() != null ? detalle.getLugar().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);


            }
        }

        return table;
    }





    public  PdfPTable createActividadSilviculturalOrganizPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        //11. Organizacion para el desarrollo 32
        ResultClassEntity<ActividadSilviculturalDto>lstActSilviculturalOrg= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"PGMFA");


        cell = new PdfPCell(new Paragraph("Actividad realizada", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Forma de organización", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstActSilviculturalOrg.getData()!=null ) {
            for (ActividadSilviculturalDetalleEntity detalle : lstActSilviculturalOrg.getData().getDetalle()) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createPotencialProduccionA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        List<PotencialProduccionForestalEntity> lista = potencialProduccionForestalRepository.ListarPotencialProducForestal(idPlanManejo,"PGMFA");
        int i=0;
        int especies=0;
        if(lista!=null){
            List<PotencialProduccionForestalEntity> lstEspecies= lista.stream().filter(p -> p.getCodigoSubTipoPotencialProdForestal().equals("PGMFAA") ).collect(Collectors.toList());
            for (PotencialProduccionForestalEntity detalle1 : lstEspecies) {
                List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                if (lstEspecies.size() > 0) {
                    PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(i);
                    lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                }
                if(lstListaDetalle!=null) {
                    if(lstListaDetalle.size()>especies){
                        especies=lstListaDetalle.size();
                    }
                }
                i++;
            }
        }

        PdfPTable table =null;
        table = new PdfPTable(especies+4);
        PdfPCell cell;
        float[] values = new float[especies+4];
        for (int j = 0; j < values.length; j++) {
            if (j == 0 ) {
                values[j] = 60;
            }else {
                values[j] = 40;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;

        cell = new PdfPCell(new Paragraph( "Tipo de Bosque", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Variable", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        String especieCabecera="";
        for(int z=1;z<=especies;z++){
            especieCabecera="E "+z;
            cell = new PdfPCell(new Paragraph( especieCabecera, cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }
        cell = new PdfPCell(new Paragraph( "Total TB", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Total ha", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        int contador1=0,contador2=0,contador3=0,contador4=0;
        double totalBosque1=0.0,totalha1=0.0;
        double totalBosque2=0.0,totalha2=0.0;
        double totalBosque3=0.0,totalha3=0.0;
        List<Double> variableN= new ArrayList<Double>();
        List<Double> variableAB= new ArrayList<Double>();
        List<Double> variableVC= new ArrayList<Double>();
        String variableNS= "";
        String variableABS= "";
        String variableVCS= "";
        if(lista!=null){
            List<PotencialProduccionForestalEntity> lstEspecies= lista.stream().filter(p -> p.getCodigoSubTipoPotencialProdForestal().equals("PGMFAA")).collect(Collectors.toList());
            for (PotencialProduccionForestalEntity detalle1 : lstEspecies) {
                cell = new PdfPCell(new Paragraph(detalle1.getTipoBosque() != null ? detalle1.getTipoBosque() : "", contenido));
                cell.setFixedHeight(size);
                cell.setRowspan(3);
                table.addCell(cell);
                contador2=0;contador3=0;contador4=0;
                if(lstEspecies!=null){
                    List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                    if (lstEspecies.size() > 0) {
                        PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(contador1);
                        lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                    }
                    if(lstListaDetalle!=null) {
                        for(PotencialProduccionForestalDto detalle2 : lstListaDetalle){
                            cell = new PdfPCell(new Paragraph("N", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);

                            List<PotencialProduccionForestalVariableEntity> lstListaDetalleVar = new ArrayList<>();
                            if (lstEspecies.size() > 0) {
                                PotencialProduccionForestalDto PGMFEVALI = lstListaDetalle.get(contador2);
                                lstListaDetalleVar = PGMFEVALI.getListPotencialProduccionVariable();
                            }
                            if(lstListaDetalleVar!=null){
                                for(PotencialProduccionForestalVariableEntity detalle3: lstListaDetalleVar){
                                    if(detalle3.getVariable().equals("N")) {
                                        variableN.add(detalle3.getTotalHa().doubleValue());
                                        variableNS = "TB= "+detalle3.getNuTotalTipoBosque()+"\n"+
                                                     "TA= "+detalle3.getTotalHa();
                                        totalBosque1+=detalle3.getNuTotalTipoBosque().doubleValue();
                                        totalha1+=detalle3.getTotalHa().doubleValue();
                                        cell = new PdfPCell(new Paragraph( variableNS, cabecera));
                                        cell.setFixedHeight(40);
                                        table.addCell(cell);
                                    }
                                }
                            }

                            contador2++;
                        }
                    }

                }
                cell = new PdfPCell(new Paragraph( totalBosque1+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( totalha1+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);

                if(lstEspecies!=null){
                    List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                    if (lstEspecies.size() > 0) {
                        PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(contador3);
                        lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                    }
                    if(lstListaDetalle!=null) {
                        for(PotencialProduccionForestalDto detalle2 : lstListaDetalle){
                            cell = new PdfPCell(new Paragraph("AB m2", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);
                            List<PotencialProduccionForestalVariableEntity> lstListaDetalleVar = new ArrayList<>();
                            if (lstEspecies.size() > 0) {
                                PotencialProduccionForestalDto PGMFEVALI = lstListaDetalle.get(contador3);
                                lstListaDetalleVar = PGMFEVALI.getListPotencialProduccionVariable();
                            }
                            if(lstListaDetalleVar!=null){
                                for(PotencialProduccionForestalVariableEntity detalle3: lstListaDetalleVar){
                                    if(detalle3.getVariable().equals("AB m2")) {
                                        variableN.add(detalle3.getTotalHa().doubleValue());
                                        variableABS = "TB= "+detalle3.getNuTotalTipoBosque()+"\n"+
                                                "TA= "+detalle3.getTotalHa();
                                        totalBosque2+=detalle3.getNuTotalTipoBosque().doubleValue();
                                        totalha2+=detalle3.getTotalHa().doubleValue();
                                        cell = new PdfPCell(new Paragraph( variableABS, cabecera));
                                        cell.setFixedHeight(40);
                                        table.addCell(cell);
                                    }
                                }
                            }

                            contador3++;
                        }
                    }

                }

                cell = new PdfPCell(new Paragraph( totalBosque2+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( totalha2+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);

                if(lstEspecies!=null){
                    List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                    if (lstEspecies.size() > 0) {
                        PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(contador3);
                        lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                    }
                    if(lstListaDetalle!=null) {
                        for(PotencialProduccionForestalDto detalle2 : lstListaDetalle){
                            cell = new PdfPCell(new Paragraph("Vc m3", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);
                            List<PotencialProduccionForestalVariableEntity> lstListaDetalleVar = new ArrayList<>();
                            if (lstEspecies.size() > 0) {
                                PotencialProduccionForestalDto PGMFEVALI = lstListaDetalle.get(contador4);
                                lstListaDetalleVar = PGMFEVALI.getListPotencialProduccionVariable();
                            }
                            if(lstListaDetalleVar!=null){
                                for(PotencialProduccionForestalVariableEntity detalle3: lstListaDetalleVar){
                                    if(detalle3.getVariable().equals("Vc m3")) {
                                        variableN.add(detalle3.getTotalHa().doubleValue());
                                        variableVCS = "TB= "+detalle3.getNuTotalTipoBosque()+"\n"+
                                                "TA= "+detalle3.getTotalHa();
                                        totalBosque3+=detalle3.getNuTotalTipoBosque().doubleValue();
                                        totalha3+=detalle3.getTotalHa().doubleValue();
                                        cell = new PdfPCell(new Paragraph( variableVCS, contenido));
                                        cell.setFixedHeight(40);
                                        table.addCell(cell);
                                    }
                                }
                            }

                            contador4++;
                        }
                    }

                }

                cell = new PdfPCell(new Paragraph( totalBosque3+"", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( totalha3+"", contenido));
                cell.setFixedHeight(40);
                table.addCell(cell);

                contador1++;
            }
        }
        return table;
    }



    public  PdfPTable createPotencialProduccionB(PdfWriter writer,Integer idPlanManejo) throws Exception {

        List<PotencialProduccionForestalEntity> lista = potencialProduccionForestalRepository.ListarPotencialProducForestal(idPlanManejo,"PGMFA");
        int i=0;
        int especies=0;
        if(lista!=null){
            List<PotencialProduccionForestalEntity> lstEspecies= lista.stream().filter(p -> p.getCodigoSubTipoPotencialProdForestal().equals("PGMFAB") ).collect(Collectors.toList());
            for (PotencialProduccionForestalEntity detalle1 : lstEspecies) {
                List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                if (lstEspecies.size() > 0) {
                    PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(i);
                    lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                }
                if(lstListaDetalle!=null) {
                    if(lstListaDetalle.size()>especies){
                        especies=lstListaDetalle.size();
                    }
                }
                i++;
            }
        }

        PdfPTable table =null;
        table = new PdfPTable(especies+4);
        PdfPCell cell;
        float[] values = new float[especies+4];
        for (int j = 0; j < values.length; j++) {
            if (j == 0 ) {
                values[j] = 60;
            }else {
                values[j] = 40;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;

        cell = new PdfPCell(new Paragraph( "Tipo de Bosque", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Variable", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        String especieCabecera="";
        for(int z=1;z<=especies;z++){
            especieCabecera="E "+z;
            cell = new PdfPCell(new Paragraph( especieCabecera, cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }
        cell = new PdfPCell(new Paragraph( "Total TB", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Total ha", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        int contador1=0,contador2=0,contador3=0,contador4=0;
        double totalBosque1=0.0,totalha1=0.0;
        double totalBosque2=0.0,totalha2=0.0;
        double totalBosque3=0.0,totalha3=0.0;
        List<Double> variableN= new ArrayList<Double>();
        List<Double> variableAB= new ArrayList<Double>();
        List<Double> variableVC= new ArrayList<Double>();
        String variableNS= "";
        String variableABS= "";
        String variableVCS= "";
        if(lista!=null){
            List<PotencialProduccionForestalEntity> lstEspecies= lista.stream().filter(p -> p.getCodigoSubTipoPotencialProdForestal().equals("PGMFAB")).collect(Collectors.toList());
            for (PotencialProduccionForestalEntity detalle1 : lstEspecies) {
                cell = new PdfPCell(new Paragraph(detalle1.getTipoBosque() != null ? detalle1.getTipoBosque() : "", contenido));
                cell.setFixedHeight(size);
                cell.setRowspan(2);
                table.addCell(cell);
                contador2=0;contador3=0;contador4=0;
                if(lstEspecies!=null){
                    List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                    if (lstEspecies.size() > 0) {
                        PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(contador1);
                        lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                    }
                    if(lstListaDetalle!=null) {
                        for(PotencialProduccionForestalDto detalle2 : lstListaDetalle){
                            cell = new PdfPCell(new Paragraph("N", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);

                            List<PotencialProduccionForestalVariableEntity> lstListaDetalleVar = new ArrayList<>();
                            if (lstEspecies.size() > 0) {
                                PotencialProduccionForestalDto PGMFEVALI = lstListaDetalle.get(contador2);
                                lstListaDetalleVar = PGMFEVALI.getListPotencialProduccionVariable();
                            }
                            if(lstListaDetalleVar!=null){
                                for(PotencialProduccionForestalVariableEntity detalle3: lstListaDetalleVar){
                                    if(detalle3.getVariable().equals("N")) {
                                        variableN.add(detalle3.getTotalHa().doubleValue());
                                        variableNS = "TB= "+detalle3.getNuTotalTipoBosque()+"\n"+
                                                "TA= "+detalle3.getTotalHa();
                                        totalBosque1+=detalle3.getNuTotalTipoBosque().doubleValue();
                                        totalha1+=detalle3.getTotalHa().doubleValue();
                                        cell = new PdfPCell(new Paragraph( variableNS, cabecera));
                                        cell.setFixedHeight(40);
                                        table.addCell(cell);
                                    }
                                }
                            }

                            contador2++;
                        }
                    }

                }
                cell = new PdfPCell(new Paragraph( totalBosque1+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( totalha1+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);

                if(lstEspecies!=null){
                    List<PotencialProduccionForestalDto> lstListaDetalle = new ArrayList<>();
                    if (lstEspecies.size() > 0) {
                        PotencialProduccionForestalEntity PGMFEVALI = lstEspecies.get(contador3);
                        lstListaDetalle = PGMFEVALI.getListPotencialProduccion();
                    }
                    if(lstListaDetalle!=null) {
                        for(PotencialProduccionForestalDto detalle2 : lstListaDetalle){
                            cell = new PdfPCell(new Paragraph("AB m2", contenido));
                            cell.setFixedHeight(40);
                            table.addCell(cell);
                            List<PotencialProduccionForestalVariableEntity> lstListaDetalleVar = new ArrayList<>();
                            if (lstEspecies.size() > 0) {
                                PotencialProduccionForestalDto PGMFEVALI = lstListaDetalle.get(contador3);
                                lstListaDetalleVar = PGMFEVALI.getListPotencialProduccionVariable();
                            }
                            if(lstListaDetalleVar!=null){
                                for(PotencialProduccionForestalVariableEntity detalle3: lstListaDetalleVar){
                                    if(detalle3.getVariable().equals("AB m2")) {
                                        variableN.add(detalle3.getTotalHa().doubleValue());
                                        variableABS = "TB= "+detalle3.getNuTotalTipoBosque()+"\n"+
                                                "TA= "+detalle3.getTotalHa();
                                        totalBosque2+=detalle3.getNuTotalTipoBosque().doubleValue();
                                        totalha2+=detalle3.getTotalHa().doubleValue();
                                        cell = new PdfPCell(new Paragraph( variableABS, cabecera));
                                        cell.setFixedHeight(40);
                                        table.addCell(cell);
                                    }
                                }
                            }

                            contador3++;
                        }
                    }

                }

                cell = new PdfPCell(new Paragraph( totalBosque2+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph( totalha2+"", cabecera));
                cell.setFixedHeight(40);
                table.addCell(cell);


                contador1++;
            }
        }
        return table;
    }


    public  PdfPTable createRentabilidadIngresos(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("PGMFA");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        int i=0;
        int anio=0;
        if(lstRMF!=null) {
            List<RentabilidadManejoForestalDto> lstIngresos = lstRMF.stream().filter(p -> p.getIdTipoRubro() == 1).collect(Collectors.toList());
            for (RentabilidadManejoForestalDto renta : lstIngresos) {
                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }
                if(lstListaDetalle!=null) {
                    for (RentabilidadManejoForestalDetalleEntity detalle : lstListaDetalle) {
                        if (detalle.getAnio() > anio) {
                            anio = detalle.getAnio();
                        }
                    }
                }

                i++;
            }
        }
        PdfPTable table =null;
        if(anio>0){
            table = new PdfPTable(anio+1);
            PdfPCell cell;
            float[] values = new float[anio+1];
            for (int j = 0; j < values.length; j++) {
                if (j == 0) {
                    values[j] = 80;
                } else {
                    values[j] = 15;
                }
            }
            table.setWidths(values);
            table.setWidthPercentage(100);
            int size = 30;

            cell = new PdfPCell(new Paragraph( "Rubro", cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
            String anioCabecera="";
            for(int z=1;z<=anio;z++){
                anioCabecera="AÑO "+z;
                cell = new PdfPCell(new Paragraph( anioCabecera, cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }

            int contador3=0;
            if(lstRMF!=null) {
                List<RentabilidadManejoForestalDto> lstIngresos = lstRMF.stream().filter(p -> p.getIdTipoRubro() == 1).collect(Collectors.toList());
                for (RentabilidadManejoForestalDto renta : lstIngresos) {
                    cell = new PdfPCell(new Paragraph(renta.getRubro() != null ? renta.getRubro() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                    if (lstIngresos.size() > 0) {
                        RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(contador3);
                        lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                    }
                    if(lstListaDetalle!=null) {
                        for(int l=1;l<=anio;l++) {
                            int finalI = l;
                            List<RentabilidadManejoForestalDetalleEntity> lstDetalleAnios = lstListaDetalle.stream().filter(a -> a.getAnio() == finalI).collect(Collectors.toList());
                            if(lstDetalleAnios.size()>0){
                                for(RentabilidadManejoForestalDetalleEntity det : lstDetalleAnios){
                                    cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                }
                            }else{
                                cell = new PdfPCell(new Paragraph("", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                        }
                    }

                    contador3++;
                }
            }

        }

        return table;
    }



    public  PdfPTable createRentabilidadEgresos(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("PGMFA");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        int i=0;
        int anio=0;
        if(lstRMF!=null) {
            List<RentabilidadManejoForestalDto> lstIngresos = lstRMF.stream().filter(p -> p.getIdTipoRubro() == 2).collect(Collectors.toList());
            for (RentabilidadManejoForestalDto renta : lstIngresos) {
                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }
                if(lstListaDetalle!=null) {
                    for (RentabilidadManejoForestalDetalleEntity detalle : lstListaDetalle) {
                        if (detalle.getAnio() > anio) {
                            anio = detalle.getAnio();
                        }
                    }
                }

                i++;
            }
        }
        PdfPTable table =null;
        if(anio>0){
            table = new PdfPTable(anio+1);
            PdfPCell cell;
            float[] values = new float[anio+1];
            for (int j = 0; j < values.length; j++) {
                if (j == 0) {
                    values[j] = 80;
                } else {
                    values[j] = 15;
                }
            }
            table.setWidths(values);
            table.setWidthPercentage(100);
            int size = 30;

            cell = new PdfPCell(new Paragraph( "Rubro", cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
            String anioCabecera="";
            for(int z=1;z<=anio;z++){
                anioCabecera="AÑO "+z;
                cell = new PdfPCell(new Paragraph( anioCabecera, cabecera));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }

            int contador3=0;
            if(lstRMF!=null) {
                List<RentabilidadManejoForestalDto> lstIngresos = lstRMF.stream().filter(p -> p.getIdTipoRubro() == 2).collect(Collectors.toList());
                for (RentabilidadManejoForestalDto renta : lstIngresos) {
                    cell = new PdfPCell(new Paragraph(renta.getRubro() != null ? renta.getRubro() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                    if (lstIngresos.size() > 0) {
                        RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(contador3);
                        lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                    }
                    if(lstListaDetalle!=null) {
                        for(int l=1;l<=anio;l++) {
                            int finalI = l;
                            List<RentabilidadManejoForestalDetalleEntity> lstDetalleAnios = lstListaDetalle.stream().filter(a -> a.getAnio() == finalI).collect(Collectors.toList());
                            if(lstDetalleAnios.size()>0){
                                for(RentabilidadManejoForestalDetalleEntity det : lstDetalleAnios){
                                    cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                                    cell.setFixedHeight(size);
                                    table.addCell(cell);
                                }
                            }else{
                                cell = new PdfPCell(new Paragraph("", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                        }
                    }

                    contador3++;
                }
            }

        }

        return table;
    }


        public  PdfPTable createActCronogramaPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPCell cell;
        int size = 20;

        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PGMFA");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();

      int anio=0;
        int j =0;
        if(lstcrono!=null){
            for (CronogramaActividadEntity detalle1 : lstcrono) {
                List<CronogramaActividadDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstcrono.size() > 0) {
                    CronogramaActividadEntity PGMFEVALI = lstcrono.get(j);
                    lstListaDetalle = PGMFEVALI.getDetalle();
                }
                if (lstListaDetalle != null) {
                    for (CronogramaActividadDetalleEntity detalle : lstListaDetalle) {
                        if (detalle.getAnio() > anio) {
                            anio = detalle.getAnio();
                        }
                    }
                }
            }
        }

        PdfPTable table = new PdfPTable(1+anio);
        table.setWidthPercentage(90);

        cell = new PdfPCell(new Paragraph( "Actividad", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        String anioCabecera="";
        for(int i=1;i<=anio;i++){
            anioCabecera="AÑO "+i;
            cell = new PdfPCell(new Paragraph( anioCabecera, cabecera));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }


        if(lstcrono!=null){

            for (CronogramaActividadEntity detalle : lstcrono) {
                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<CronogramaActividadDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstcrono.size() > 0) {
                    CronogramaActividadEntity PGMFEVALI = lstcrono.get(0);
                    lstListaDetalle = PGMFEVALI.getDetalle();
                }

                if (lstListaDetalle != null) {
                    for(int i=1;i<=anio;i++) {
                        int finalI = i;
                        List<CronogramaActividadDetalleEntity> lstDetalleMeses = detalle.getDetalle().stream().filter(a -> a.getAnio() == finalI).collect(Collectors.toList());
                        cell = new PdfPCell(new Paragraph(lstDetalleMeses.size() > 0 ? "X" : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }

        return table;
    }

    public  PdfPTable createAspectosCompPGMFA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 50;

        //15.	ASPECTOS COMPLEMENTARIOS   (47)
        InformacionGeneralDEMAEntity aspectosComRequest = new InformacionGeneralDEMAEntity();
        aspectosComRequest.setCodigoProceso("PGMFA");
        aspectosComRequest.setIdPlanManejo(idPlanManejo);
        ResultEntity<InformacionGeneralDEMAEntity> lstAspectos = infoGeneralDemaRepo.listarInformacionGeneralDema(aspectosComRequest);

        if(lstAspectos.getData()!=null) {
            for (InformacionGeneralDEMAEntity detalle : lstAspectos.getData()) {

                cell = new PdfPCell(new Paragraph(detalle.getDetalle() != null ? detalle.getDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }



}
