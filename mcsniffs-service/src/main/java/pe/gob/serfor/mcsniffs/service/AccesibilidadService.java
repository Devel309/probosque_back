package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoEntity;
import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoMatrizDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoRequisitoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.AccesibilidadManejoMatrizDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface AccesibilidadService {

    ResultClassEntity ListarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception;

    ResultClassEntity RegistrarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception;

    ResultClassEntity ActualizarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception;

    ResultClassEntity ListarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception;
    ResultClassEntity RegistrarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity>  accesibilidadManejoRequisitoEntity) throws Exception;
    ResultClassEntity ActualizarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity> accesibilidadManejoRequisitoEntity) throws Exception;
    ResultClassEntity EliminarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception;

    ResultClassEntity RegistrarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> accesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity ListarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity EliminarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity ActualizarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity EliminarAccesibilidadManejoMatrizDetalle(AccesibilidadManejoMatrizDetalleEntity accesibilidadManejoMatrizDetalleEntity) throws Exception;

}
