package pe.gob.serfor.mcsniffs.service.constant;

/**
 * 
 * @author Rafael Azaña
 */
public final class PoacType {

	private PoacType() {
	}
	public static final String COMUNIDAD_NOMBRE = "1nombre";
	public static final String REPRESENTANTE_NOMBRE = "1representante";
	public static final String DNI = "1dni";
	public static final String FEDERACION = "1federacion";
	public static final String DEPARTAMENTO = "1departamento";
	public static final String PROVINCIA = "1provincia";
	public static final String DISTRITO = "1distrito";
	public static final String CUENCA = "1cuenca";
	public static final String SUB_CUENCA = "1subcuenca";
	public static final String EMPRESA = "1empresa";
	public static final String INGENIERO_NOMBRE = "1IngForestal";
	public static final String CERTIFICADO = "1certificado" ;
	public static final String INSCRIPCION = "1inscripcion";
	public static final String RESOLUCION = "1resolucion";
	public static final String ANIO = "1anio";
	public static final String FECHA = "1fecha";
	public static final String FECHA_INICIO = "1fechaInicio";
	public static final String FECHA_FINAL = "1fechaFinal";
	public static final String AREA_APROVECHAMIENTO = "1areaAprovechamiento";
	public static final String AREA_EFECTIVA = "1areaEfectiva";
	public static final String AREA_PROTECCION = "1areaProtecion";


}
