package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;

public interface MesaPartesService {
    ResultClassEntity<List<MesaPartesDto>> listarMesaPartes(MesaPartesDto dto) throws Exception;
    ResultClassEntity registrarMesaPartes(MesaPartesDto dto) throws Exception;    
    ResultClassEntity eliminarMesaPartes(MesaPartesDto dto) throws Exception;

    ResultClassEntity<List<MesaPartesDto>> listarMesaPartesPMFIDEMA(MesaPartesDto dto) throws Exception;
    ResultClassEntity registrarMesaPartesPMFIDEMA(MesaPartesDto dto) throws Exception;
}
