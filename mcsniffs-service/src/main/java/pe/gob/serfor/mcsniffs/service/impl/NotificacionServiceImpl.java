package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;
import pe.gob.serfor.mcsniffs.repository.NotificacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.NotificacionService;
@Service("NotificacionService")
public class NotificacionServiceImpl implements NotificacionService {

    @Autowired
    private NotificacionRepository notificacionRepository;

    @Override
   
    public ResultClassEntity registrarNotificacion(NotificacionEntity obj) throws Exception{
        return notificacionRepository.registrarNotificacion(obj);
    }

    @Override
    public List<NotificacionEntity> listarNotificacion(NotificacionEntity param) throws Exception {
        return notificacionRepository.listarNotificacion(param);
    }

    @Override
    public ResultClassEntity EliminarNotificacion(NotificacionEntity param) throws Exception {

        return  notificacionRepository.EliminarNotificacion(param);
    }

}