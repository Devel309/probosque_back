package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.PlanGeneralManejoRepository;
import pe.gob.serfor.mcsniffs.service.PlanGeneralManejoService;


/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad   de la base de datos mcsniffs}
 *
 */
@Service("PlanGeneralManejoService")
public class PlanGeneralManejoServiceImpl implements PlanGeneralManejoService {


    private static final Logger log = LogManager.getLogger(PlanGeneralManejoServiceImpl.class);

    @Autowired
    private PlanGeneralManejoRepository planGeneralManejoRepository;
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Registro ResumenEjecutivo}
     * @param: InformacionGeneralEntity
     */
    @Override
    public ResultClassEntity RegistrarResumenEjecutivoEscalaAlta(InformacionGeneralEntity param, MultipartFile file,Boolean accion) throws Exception {
        return planGeneralManejoRepository.RegistrarResumenEjecutivoEscalaAlta(param,file,accion);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Actualizar ResumenEjecutivo}
     * @param: InformacionGeneralEntity
     */
    @Override
    public ResultClassEntity ActualizarResumenEjecutivoEscalaAlta(InformacionGeneralEntity param, MultipartFile file,Boolean accion) throws Exception {




        return planGeneralManejoRepository.ActualizarResumenEjecutivoEscalaAlta(param,file,accion);
    }

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Obtener ResumenEjecutivo}
     * @param: InformacionGeneralEntity
     */
    @Override
    public ResultClassEntity ObtenerResumenEjecutivoEscalaAlta(InformacionGeneralEntity param) throws Exception {
        return planGeneralManejoRepository.ObtenerResumenEjecutivoEscalaAlta(param);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Actualizar duración ResumenEjecutivo}
     * @param: InformacionGeneralEntity
     */
    @Override
    public ResultClassEntity ActualizarDuracionResumenEjecutivo(InformacionGeneralEntity param) throws Exception {
        return planGeneralManejoRepository.ActualizarDuracionResumenEjecutivo(param);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Actualizar el aspecto complementario del ResumenEjecutivo}
     * @param: PlanManejoEntity
     */
    @Override
    public ResultClassEntity ActualizarAspectoComplementarioPlanManejo(PlanManejoEntity param) throws Exception {
        return planGeneralManejoRepository.ActualizarAspectoComplementarioPlanManejo(param);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Obtener plan de manejo}
     * @param: PlanManejoEntity
     */
    @Override
    public ResultClassEntity ObtenerPlanManejo(PlanManejoEntity param)throws Exception{
        return planGeneralManejoRepository.ObtenerPlanManejo(param);
    }
}
