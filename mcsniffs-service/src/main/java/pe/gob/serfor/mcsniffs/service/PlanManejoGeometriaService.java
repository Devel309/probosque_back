package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoGeometriaActualizarCatastroDto;

public interface PlanManejoGeometriaService {
    ResultClassEntity registrarPlanManejoGeometria( List<PlanManejoGeometriaEntity> items)  throws Exception ;
    ResultClassEntity listarPlanManejoGeometria (PlanManejoGeometriaEntity item);
    ResultClassEntity eliminarPlanManejoGeometriaArchivo (Integer idArchivo, Integer idUsuario);
    ResultClassEntity actualizarCatastroPlanManejoGeometria(PlanManejoGeometriaActualizarCatastroDto dto)  throws Exception ;
}
