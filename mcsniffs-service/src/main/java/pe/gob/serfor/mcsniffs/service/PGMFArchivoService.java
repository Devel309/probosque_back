package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface PGMFArchivoService {
    ResultClassEntity registrarArchivo(PGMFArchivoEntity obj) throws Exception;
    ResultEntity<PGMFArchivoEntity> listarArchivosPGMF(PGMFArchivoEntity filtro);
    ResultArchivoEntity generarAnexo2PGMF(PGMFArchivoEntity filtro);
    ResultArchivoEntity generarAnexo3PGMF(PGMFArchivoEntity filtro);
    ResultEntity<PGMFArchivoEntity>listarDetalleArchivo(Integer idPlanManejo, String codigoTipoPGMF) throws Exception;
    ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception;
}
