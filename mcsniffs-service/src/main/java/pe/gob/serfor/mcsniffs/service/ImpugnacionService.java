package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.Dto.Impugnacion.ImpugnacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
public interface ImpugnacionService {

    ResultClassEntity ValidarImpugnacionResolucion(ImpugnacionDto param);

    ResultClassEntity ListarImpugnacion(ImpugnacionDto param);

    ResultClassEntity notificarSolicitudImpugnacion(ImpugnacionDto param);
    
    ResultClassEntity ListarResolucionImpugnada(ImpugnacionDto param);

    ResultClassEntity RegistrarImpugnacion(ImpugnacionDto param) throws Exception;

    ResultClassEntity ActualizarImpugnacion(Integer idImpugnacion,String tipoImpugnacion, Boolean esfundado, Boolean retrotraer, Integer idUsuarioModificacion, MultipartFile fileEvaluacion, MultipartFile fileFirme);
    ResultClassEntity ObtenerImpugnacion(ImpugnacionDto param);

    ResultClassEntity ActualizarImpugnacionArchivo(ImpugnacionDto obj);

    ResultClassEntity EliminarImpugnacionArchivo(ImpugnacionDto obj) throws Exception;

}
