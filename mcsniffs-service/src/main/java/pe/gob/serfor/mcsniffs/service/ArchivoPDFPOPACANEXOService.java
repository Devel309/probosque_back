package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPOPACANEXOService {
    ByteArrayResource consolidadoPOPACANEXO_PDF(Integer idPlanManejo) throws Exception;
}
