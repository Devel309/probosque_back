package pe.gob.serfor.mcsniffs.service.impl;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.repository.CoreCentralRepository;
import pe.gob.serfor.mcsniffs.service.CoreCentralService;

import java.util.List;

/**
* @autor: Ivan Minaya [09-06-2021]
* @modificado:
* @descripción: {Clase de servicio relacionada a las entidades de CoreCentral de la base de datos maestra CoreCentral}
*
*/
@Service("CoreCentralService")
public class CoreCentralServiceImpl implements CoreCentralService {


    private static final Logger log = LogManager.getLogger(CoreCentralServiceImpl.class); 

	@Autowired
	private CoreCentralRepository coreCentralRepository;

	/**
	 * @autor: Miguel F. [09-06-2021]
	 * @descripción: {Lista por filtro Departamento}
	 * @param: DepartamentoEntity
	 */
	@Override
	public List<DepartamentoEntity> ListarPorFiltroDepartamentos(DepartamentoEntity param) throws Exception {
		return coreCentralRepository.ListarPorFiltroDepartamentos(param);
	}
	/**
	 * @autor: Miguel F. [09-06-2021]
	 * @descripción: {Lista por filtro AutoridadForestal}
	 * @param: AutoridadForestalEntity
	 */
	@Override
	public List<AutoridadForestalEntity> ListarPorFiltroAutoridadForestal(AutoridadForestalEntity param) throws Exception {
		return coreCentralRepository.ListarPorFiltroAutoridadForestal(param);
	}

	/**
	 * @autor: Miguel F. [09-06-2021]
	 * @descripción: {Lista por filtro TipoComunidad}
	 * @param: TipoComunidadEntity
	 */
	@Override
	public List<TipoComunidadEntity> ListarPorFiltroTipoComunidad(TipoComunidadEntity param) throws Exception {
		return coreCentralRepository.ListarPorFiltroTipoComunidad(param);
	}

	/**
	 * @autor: Miguel F. [09-06-2021]
	 * @descripción: {Lista por filtro TipoTrasporte}
	 * @param: TipoTrasporteEntity
	 */
	@Override
	public List<TipoTrasporteEntity> ListarPorFiltroTipoTrasporte(TipoTrasporteEntity param) throws Exception {
		return coreCentralRepository.ListarPorFiltroTipoTrasporte(param);
	}

	/**
	 * @autor: Miguel F. [09-06-2021]
	 * @descripción: {Lista por filtro TipoZona}
	 * @param: TipoZonaEntity
	 */
	@Override
	public List<TipoZonaEntity> listarPorFiltroTipoZona(TipoZonaEntity param) throws Exception {
		return coreCentralRepository.listarPorFiltroTipoZona(param);
	}


	//IVAN
	
	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoBosque}
	* @param: TipoBosqueEntity
	*/
	@Transactional
	@Override
	public List<TipoBosqueEntity> listarPorFiltroTipoBosque(TipoBosqueEntity tipoBosqueEntity) throws Exception {

		List<TipoBosqueEntity> response ;
		log.info("CoreCentral - listarPorFiltroTipoBosque",tipoBosqueEntity.toString());
		try {
			response=   coreCentralRepository.listarPorFiltroTipoBosque(tipoBosqueEntity);
		} catch (Exception e) {

			log.error("CoreCentral -ListapeticionCompraDetalle","Ocurrió un error :"+ e.getMessage());
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoConcesion}
	* @param: TipoConcesionEntity
	*/
	@Transactional
	@Override
	public List<TipoConcesionEntity> listarPorFiltroTipoConcesion(TipoConcesionEntity tipoConcesionEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoConcesionEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoConcesion(tipoConcesionEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoContrato}
	* @param: TipoContratoEntity
	*/
	@Transactional
	@Override
	public List<TipoContratoEntity> listarPorFiltroTipoContrato(TipoContratoEntity tipoContratoEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoContratoEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoContrato(tipoContratoEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoDocGestionErgtf}
	* @param: TipoDocGestionErgtfEntity
	*/
	@Transactional
	@Override
	public List<TipoDocGestionErgtfEntity> listarPorFiltroTipoDocGestionErgtf(TipoDocGestionErgtfEntity tipoDocGestionErgtfEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoDocGestionErgtfEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoDocGestionErgtf(tipoDocGestionErgtfEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	@Transactional
	@Override
	public List<TipoDocGestionEntity> listarPorFiltroTipoDocGestion(TipoDocGestionEntity tipoDocGestionEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoDocGestionEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoDocGestion(tipoDocGestionEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

		

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoDocumento}
	* @param: TipoDocumentoEntity
	*/
	@Transactional
	@Override
	public List<TipoDocumentoEntity> listarPorFiltroTipoDocumento(TipoDocumentoEntity tipoDocumentoEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoDocumentoEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoDocumento(tipoDocumentoEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoEscala}
	* @param: TipoEscalaEntity
	*/
	@Transactional
	@Override
	public List<TipoEscalaEntity> listarPorFiltroTipoEscala(TipoEscalaEntity tipoEscalaEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoEscalaEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoEscala(tipoEscalaEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}
			

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoMoneda}
	* @param: TipoMonedaEntity
	*/
	@Transactional
	@Override
	public List<TipoMonedaEntity> listarPorFiltroTipoMoneda(TipoMonedaEntity tipoMonedaEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoMonedaEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoMoneda(tipoMonedaEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}


	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoOficina}
	* @param: TipoOficinaEntity
	*/
	@Transactional
	@Override
	public List<TipoOficinaEntity> listarPorFiltroTipoOficina(TipoOficinaEntity tipoOficinaEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoOficinaEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoOficina(tipoOficinaEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

		
	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoPago}
	* @param: TipoPagoEntity
	*/
	@Transactional
	@Override
	public List<TipoPagoEntity> listarPorFiltroTipoPago(TipoPagoEntity tipoPagoEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoPagoEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoPago(tipoPagoEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoPermiso}
	* @param: TipoPermisoEntity
	*/
	@Transactional
	@Override
	public List<TipoPermisoEntity> listarPorFiltroTipoPermiso(TipoPermisoEntity tipoPermisoEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoPermisoEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoPermiso(tipoPermisoEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

		
	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoProcedimientoConces}
	* @param: TipoProcedimientoConcesEntity
	*/
	@Transactional
	@Override
	public List<TipoProcedimientoConcesEntity> listarPorFiltroTipoProcedimientoConces(TipoProcedimientoConcesEntity tipoProcedimientoConcesEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoProcedimientoConcesEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoProcedimientoConces(tipoProcedimientoConcesEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoProducto}
	* @param: TipoProductoEntity
	*/
	@Transactional
	@Override
	public List<TipoProductoEntity> listarPorFiltroTipoProducto(TipoProductoEntity tipoProductoEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoProductoEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoProducto(tipoProductoEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

	
	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro TipoRegente}
	* @param: TipoRegenteEntity
	*/
	@Transactional
	@Override
	public List<TipoRegenteEntity> listarPorFiltroTipoRegente(TipoRegenteEntity tipoRegenteEntity) throws Exception {
		// TODO Auto-generated method stub
		List<TipoRegenteEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroTipoRegente(tipoRegenteEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

		

	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro UbigeoArffs}
	* @param: UbigeoArffsEntity
	*/
	@Transactional
	@Override
	public List<UbigeoArffsEntity> listarPorFiltroUbigeoArffs(UbigeoArffsEntity ubigeoArffsEntity) throws Exception {
		// TODO Auto-generated method stub
		List<UbigeoArffsEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroUbigeoArffs(ubigeoArffsEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}

		
	/**
	* @autor: IVAN MINAYA [09-06-2021]
	* @descripción: {Lista por filtro UnidadMedida}
	* @param: UnidadMedidaEntity
	*/
	@Transactional
	@Override
	public List<UnidadMedidaEntity> listarPorFiltroUnidadMedida(UnidadMedidaEntity UnidadMedidaEntity) throws Exception {
		// TODO Auto-generated method stub
		List<UnidadMedidaEntity> response ;
		try {
			response=   coreCentralRepository.listarPorFiltroUnidadMedida(UnidadMedidaEntity);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}
		return response;
	}
	public List<RegenteComunidadEntity> listarPorFiltroRegenteComunidad(RegenteComunidadEntity regenteComunidad) throws Exception{

	// TODO Auto-generated method stub
	List<RegenteComunidadEntity> response ;
		try {
		response=   coreCentralRepository.listarPorFiltroRegenteComunidad(regenteComunidad);
	} catch (Exception e) {
		// TODO: handle exception
		log.error(e.getMessage(), e);
		throw new Exception(e.getMessage(), e);
	}
		return response;
    }

	@Override
	public ResultEntity<ProvinciaEntity> listarPorFilroProvincia(ProvinciaEntity provincia) {
		return coreCentralRepository.listarPorFilroProvincia(provincia);
	}


	/**
	 * @autor: JaquelineDB [22-06-2021]
	 * @modificado:
	 * @descripción: {Lista los Distritos}
	 * @param:DistritoEntity
	 */
	@Override
	public ResultEntity<DistritoEntity> listarPorFilroDistrito(DistritoEntity distrito) {
		return coreCentralRepository.listarPorFilroDistrito(distrito);
	}

	/**
	 * @autor: Edwin M.C [22-06-2021]
	 * @modificado:
	 * @descripción: {Lista los Distritos}
	 * @param:EspecieForestalEntity
	 */
	@Override
	public ResultEntity<EspecieForestalEntity> ListaPorFiltroEspecieForestal(EspecieForestalEntity request) {
		// TODO Auto-generated method stub
		return coreCentralRepository.ListaPorFiltroEspecieForestal(request);
	}
	@Override
	public ResultEntity<EspecieFaunaEntity> ListaPorFiltroEspecieFauna(EspecieFaunaEntity request) {
		// TODO Auto-generated method stub
		return coreCentralRepository.ListaPorFiltroEspecieFauna(request);
	}

/*	@Override
	public List<EspecieForestalEntity> ListaEspecieForestalPorFiltro(EspecieForestalEntity param) throws Exception {
		return coreCentralRepository.ListaEspecieForestalPorFiltro(param);
	}*/

	@Override
	public ResultEntity ListaEspecieForestalPorFiltro(EspecieForestalEntity request) {

		return coreCentralRepository.ListaEspecieForestalPorFiltro(request);
	}


	@Override
	public ResultEntity ListaEspecieFaunaPorFiltro(EspecieFaunaEntity request) {

		return coreCentralRepository.ListaEspecieFaunaPorFiltro(request);
	}

	@Override
	public ResultClassEntity listarEspecieForestalSincronizacion(EspecieForestalEntity param) {

		return coreCentralRepository.listarEspecieForestalSincronizacion(param);
	}

	@Override
	public ResultClassEntity listarCuencaSubcuenca(CuencaEntity param) {

		return coreCentralRepository.listarCuencaSubcuenca(param);
	}
}
