package pe.gob.serfor.mcsniffs.service.impl;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ArchivoPDFPMFIService")
public class ArchivoPDFPMFIServiceImpl implements  ArchivoPDFPMFIService{

    @Autowired
    private  SolicitudAccesoServiceImpl acceso;

    @Autowired
    private PlanManejoRepository planManejoRepository;

    @Autowired
    private PlanManejoContratoRepository planManejoContratoRepository;

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private SistemaManejoForestalRepository smfRepo;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;

    @Autowired
    private SistemaManejoForestalRepository sistema;

    @Autowired
    private ImpactoAmbientalService ambiental;

    @Autowired
    CensoForestalRepository censoForestalDetalleRepo;

    @Autowired
    ZonificacionRepository zonificacionRepository;

    @Autowired
    CoreCentralRepository corecentral;
/*
    @Autowired
    SolicitudAccesoRepository acceso;*/

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

    @Autowired
    private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ArchivoRepository archivoRepository;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;


    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private SistemaManejoForestalService sistemaManejoForestalService;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font cabecera= new Font(Font.HELVETICA, 9f, Font.BOLD);
    Font subTituloTabla= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font letraPeque = new Font(Font.HELVETICA, 6f, Font.COURIER);


    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: RAFAEL AZAÑA
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con itext}
     */
    @Override
    public ByteArrayResource consolidadoPMFI_PDF(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPFMI.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("formatoPFMI", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPdfPMFIC(os,idPlanManejo);
        os.flush();
        os.close();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }

    public  void createPdfPMFIC(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);
        try{
            PdfWriter writer = PdfWriter.getInstance(document, os);

            document.open();
            Paragraph titlePara1 = new Paragraph("PLAN DE MANEJO FORESTAL INTERMEDIO PARA EL APROVECHAMIENTO DE PRODUCTOS FORESTALES DIFERENTES A LA MADERA",titulo);
            titlePara1.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph(titlePara1));
            document.add(new Paragraph("1. INFORMACION GENERAL",subTitulo));
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable table1 = createTableInformacionGeneral(writer,idPlanManejo);
            document.add(table1);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("2.\tOBJETIVOS",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table2 = procesarObjetivosPMFI(writer,idPlanManejo);
            document.add(table2);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.\tINFORMACIÓN DEL ÁREA",subTitulo));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.1.\tUbicación\n" +
                    "\n" +
                    "3.1.1.\tUbicación política",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table3 = generarInfoAreaUbiPolitica(writer,idPlanManejo);
            document.add(table3);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.1.2.\tUbicación geográfica del área del predio, comunidad o área concedida por el Estado \n" +
                    "3.1.3.\tUnidad de manejo forestal –CASTAÑA.",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table4 = generarInfoAreAP(writer,idPlanManejo);
            document.add(table4);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.1.4.\tUbicación del área de manejo (PC–01) 266.66 ha –MADERA.",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table5 = generarInfoAreAM(writer,idPlanManejo);
            document.add(table5);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.2.\tAccesibilidad\n" +
                    "\n" +
                    "3.2.1.\tRutas o vías terrestres o fluviales al predio, comunidad o área concedida por el Estado\n",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table6 = generarInfoAreACC1(writer,idPlanManejo);
            document.add(table6);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.2.2.\tRutas o vías de acceso al área de la UMF",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table7 = generarInfoAreACC2(writer,idPlanManejo);
            document.add(table7);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.3.\tAspectos Físicos (Hidrografía y fisiografía)",subTitulo));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("3.3.1.\tCuerpos de agua",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table8 = generarInfoAreRIO(writer,idPlanManejo);
            document.add(table8);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.3.2.\tPrincipales unidades fisiográficas en el área.",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table9 = generarInfoAreUF(writer,idPlanManejo);
            document.add(table9);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.4.\tZonas de vida",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table10 = generarInfoAreZV(writer,idPlanManejo);
            document.add(table10);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.5.\tAspectos Biológicos\n" +
                    "\n" +
                    "3.5.1\tFauna silvestre representativa\n",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table11 = generarInfoAreFAAU(writer,idPlanManejo);
            document.add(table11);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.5.2\tFlora silvestre representativa ",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table12 = generarInfoAreFLO(writer,idPlanManejo);
            document.add(table12);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("3.6.\tAspectos sociales para el aprovechamiento*",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table13 = generarInfoAreOrganizacion(writer,idPlanManejo);
            document.add(table13);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("4.\tORDENAMIENTO INTERNO",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table14 = generarOrdenamientoInterno(writer,idPlanManejo);
            document.add(table14);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("5.\tINFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS\n" +
                    "\n" +
                    "5.1.\tIdentificación de la especie a aprovechar\n" +
                    "\n" +
                    "A) Productos No Maderables\n",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table15 = generarIdentiEspecieNoMaderable(writer,idPlanManejo);
            document.add(table15);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("B)\tProducto Maderable",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table16 = generarIdentiEspecieMaderable(writer,idPlanManejo);
            document.add(table16);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("5.2.\tInventario\n" +
                    "\n" +
                    "5.2.1.\tDescripción del inventario \n" +
                    "\n" +
                    "ARBOLES MADERABLES\n",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table17 = generarRealizacionNO(writer,idPlanManejo);
            document.add(table17);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("a)\tResultados del inventario por parcela o estrada:",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table18 = generarInventarioANO(writer,idPlanManejo);
            document.add(table18);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("b)\tResumen de resultado del inventario y proyección de la cantidad a aprovechar anualmente:",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table19 = generarInventarioBNO(writer,idPlanManejo);
            document.add(table19);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("ARBOLES NO MADERABLES",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table20 = generarRealizacion(writer,idPlanManejo);
            document.add(table20);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("a)\tResultados del inventario por parcela o estrada:",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table21 = generarInventarioA(writer,idPlanManejo);
            document.add(table21);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("b)\tResumen de resultado del inventario y proyección de la cantidad a aprovechar anualmente:",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table22 = generarInventarioB(writer,idPlanManejo);
            document.add(table22);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("6.\tSISTEMA DE MANEJO, APROVECHAMIENTO Y LABORES SILVICULTURALES\n" +
                    "\n" +
                    "6.1.\tEtapas del aprovechamiento\n",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table23 = generarEtapasPMFI(writer,idPlanManejo);
            document.add(table23);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("6.2.\tCiclo de corta y división administrativa (sólo en caso el aprovechamiento implique la muerte del individuo)",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table24 = generarCicloCortaPMFI(writer,idPlanManejo);
            document.add(table24);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("6.3.\tLabores silviculturales",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table25 = generarLaboresSilviculturalesPMFI(writer,idPlanManejo);
            document.add(table25);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("7.\tMEDIDAS DE PROTECCIÓN DE LA UNIDAD DE MANEJO FORESTAL ",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table26 = generarMedidasPMFI(writer,idPlanManejo);
            document.add(table26);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("8.\tIDENTIFICACIÓN DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCIÓN Y MITIGACIÓN",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table27 = generarIdentificacionImpactosPMFI(writer,idPlanManejo);
            document.add(table27);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("9.\tCRONOGRAMA DE ACTIVIDADES PARA EL PERIODO DE EJECUCIÓN",subTitulo));
            document.add(new Paragraph("\n\n"));
            PdfPTable table28 = createCronogramaActividadesAnios(writer,idPlanManejo);
            document.add(table28);
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("10.\tANEXOS ",subTitulo));
            document.add(new Paragraph("\n\n"));
            document.add(new Paragraph("10.1.\tMapa base\n" +
                    "10.2.\tMapa de la Unidad de Manejo\n" +
                    "10.3.\tMapa de dispersión\n" +
                    "10.4.\tDatos de los resultados del inventario\n",subTitulo2));
            document.add(new Paragraph("\n\n"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable createTableInformacionGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size=20;

        //1 - informacion general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("PMFI");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);

        cell = new PdfPCell(new Paragraph( "1.1. N° de Contrato/Resolución (de corresponder) ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getIdContrato() != null ? o.getIdContrato().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.2 Nombre del titular o razón social*:",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getNombreElaboraDema() != null ? o.getNombreElaboraDema().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.3\tNombre del representante \t\t\t\n" +
                "(en caso de comunidad Jefe/Presidente):",subTitulo2));
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getRepresentanteLegal() != null ? o.getRepresentanteLegal().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setColspan(2);
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.4\tNúmero de D.N.I. o RUC*:",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getDniElaboraDema() != null ? o.getDniElaboraDema().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.5\tDomicilio legal del titular:\t\t\t\t\n" +
                "(indicar Calle/Av./Jr./Psje. y el número. \n" +
                "El departamento, provincia y distrito)\n",subTitulo2));
        cell.setFixedHeight(25);
        cell.setColspan(3);
        table.addCell(cell);
        if(o!=null) {
            String ubigeo = o.getDepartamento() != null ? o.getDepartamento()
                    : ""
                    + " - " + o.getProvincia() != null ? o.getProvincia()
                    : ""
                    + " - " + o.getDistrito() != null ? o.getDistrito()
                    : "";
            cell = new PdfPCell(new Paragraph(o.getDireccionLegalTitular()!= null? ubigeo+o.getDireccionLegalTitular(): "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        cell.setColspan(3);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.6\tDomicilio legal del representante legal:\t\t\n" +
                "(indicar Calle/Av./Jr./Psje. y el número. \n" +
                "El departamento, provincia y distrito)\n",subTitulo2));
        cell.setFixedHeight(25);
        cell.setColspan(3);
        table.addCell(cell);
        if(o!=null) {
            String ubigeo = o.getDepartamentoRepresentante() != null
                    ? o.getDepartamentoRepresentante()
                    : ""
                    + "-" + o.getProvinciaRepresentante() != null
                    ? o.getProvinciaRepresentante()
                    : ""
                    + "-" + o.getDistritoRepresentante() != null
                    ? o.getDistritoRepresentante()
                    : "";
            cell = new PdfPCell(new Paragraph(o.getDireccionLegalRepresentante()!= null? ubigeo+o.getDireccionLegalRepresentante(): "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        cell.setColspan(3);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.7\tNúmero de teléfono/celular",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getCelularTitular() != null ? o.getCelularTitular().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.8\tCorreo electrónico",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getCorreoTitular() != null ? o.getCorreoTitular().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.9\tNombre del regente que formulo el PMF",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            if(o.getRegente()!=null) {
                String nombres= o.getRegente().getNombres() != null ? o.getRegente().getNombres()
                        : "" + " " + o.getRegente().getApellidos() != null
                        ? o.getRegente().getApellidos()
                        : "";
                cell = new PdfPCell(new Paragraph(nombres, contenido));
            }else{cell = new PdfPCell(new Paragraph("", contenido));}
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.10\tN° licencia del regente forestal",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            if(o.getRegente()!=null) {
                cell = new PdfPCell(new Paragraph(o.getRegente().getNumeroLicencia() != null ? o.getRegente().getNumeroLicencia().toString() : "", contenido));
            }else{cell = new PdfPCell(new Paragraph("", contenido));}
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.11\tFecha de elaboración del PMFI:\t",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi())  : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("1.12\tVigencia de la PMFI (años)",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getVigencia() != null ? o.getVigencia().toString() : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("•\tFecha de inicio (mes/año):",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("•\tFecha de culminación (mes/años):",subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        if(o!=null) {
            cell = new PdfPCell(new Paragraph(o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "", contenido));
        }else{cell = new PdfPCell(new Paragraph("", contenido));}
        cell.setFixedHeight(25);
        table.addCell(cell);

        return table;
    }

    public PdfPTable procesarObjetivosPMFI(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        //2 - Objetivos
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        objetivo.setCodigoObjetivo("PMFI");
        objetivo.setIdPlanManejo(idPlanManejo);
        ResultEntity<ObjetivoManejoEntity> lstObjetivos= objetivoservice.listarObjetivoManejo(objetivo);

        if (lstObjetivos.getData() !=null) {
                for (ObjetivoManejoEntity detalleEntity : lstObjetivos.getData()) {
                    cell = new PdfPCell(new Paragraph(detalleEntity.getGeneral() != null ? detalleEntity.getGeneral() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                }
        }

        return table;
    }


    public PdfPTable generarInfoAreaUbiPolitica(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaUbiPolitica = informacionBasicaRepository.listarInfBasicaAerea("PMFI", idPlanManejo, "PMFIAM");


        if (lstInfoAreaUbiPolitica !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaUbiPolitica) {
                cell = new PdfPCell(new Paragraph("Departamento", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDepartamento() != null ? detalleEntity.getDepartamento() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Provincia", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getProvincia() != null ? detalleEntity.getProvincia() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Distrito", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDistrito() != null ? detalleEntity.getDistrito() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Sector/ Cuenca", subTitulo2));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getCuenca() != null ? detalleEntity.getCuenca() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInfoAreAP(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Coordenada UTM (Zona __, Datum UTM)", subTitulo2));
        cell.setFixedHeight(25);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vértice", subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este (E)", subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte (N)", subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaAP = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIAP");


        if (lstInfoAreaAP !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaAP) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getPuntoVertice() != null ? detalleEntity.getPuntoVertice() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getCoordenadaEste() != null ? detalleEntity.getCoordenadaEste().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getCoordenadaNorte() != null ? detalleEntity.getCoordenadaNorte().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }



    public PdfPTable generarInfoAreAM(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Coordenada UTM (Zona __, Datum UTM)", subTitulo2));
        cell.setFixedHeight(25);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vértice", subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este (E)", subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte (N)", subTitulo2));
        cell.setFixedHeight(25);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaAM = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIAM");


        if (lstInfoAreaAM !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaAM) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getPuntoVertice() != null ? detalleEntity.getPuntoVertice() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getCoordenadaEste() != null ? detalleEntity.getCoordenadaEste().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getCoordenadaNorte() != null ? detalleEntity.getCoordenadaNorte().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }



    public PdfPTable generarInfoAreACC1(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Punto de\n" +
                " Referencia\n", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distancia \naproximada(Km)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tiempo\n (Horas)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medio de\n transporte", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Época/Periodo", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaACC1 = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFACC1");


        if (lstInfoAreaACC1 !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaACC1) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getReferencia() != null ? detalleEntity.getReferencia() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDistanciaKm() != null ? detalleEntity.getDistanciaKm().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getTiempo() != null ? detalleEntity.getTiempo().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getMedioTransporte() != null ? detalleEntity.getMedioTransporte().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getEpoca() != null ? detalleEntity.getEpoca().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInfoAreACC2(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Punto de\n" +
                " Referencia\n", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distancia \naproximada(Km)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tiempo\n (Horas)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medio de\n transporte", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Época/Periodo", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaACC2 = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFACC2");


        if (lstInfoAreaACC2 !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaACC2) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getReferencia() != null ? detalleEntity.getReferencia() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDistanciaKm() != null ? detalleEntity.getDistanciaKm().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getTiempo() != null ? detalleEntity.getTiempo().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getMedioTransporte() != null ? detalleEntity.getMedioTransporte().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getEpoca() != null ? detalleEntity.getEpoca().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInfoAreRIO(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Ríos (principales\n y secundarios)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Quebradas", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Lagunas (cochas)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaRio = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFRIO");


        if (lstInfoAreaRio !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaRio) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreRio() != null ? detalleEntity.getNombreRio() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreQuebrada() != null ? detalleEntity.getNombreQuebrada().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreLaguna() != null ? detalleEntity.getNombreLaguna().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }

    public PdfPTable generarInfoAreUF(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Unidades fisiográficas", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área (ha)", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaUF = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIUF");


        if (lstInfoAreaUF !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaUF) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getAreaHa() != null ? detalleEntity.getAreaHa().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInfoAreZV(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaZV = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIZV");

        if (lstInfoAreaZV !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaZV) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getZonaVida() != null ? detalleEntity.getZonaVida() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }

    public PdfPTable generarInfoAreFAAU(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Nombre común", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Status", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);


        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaFAU = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIFAU");
        if (lstInfoAreaFAU !=null) {
        List<EspecieFaunaEntity> lstFauna = new ArrayList<>();
        for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lstInfoAreaFAU) {
            EspecieFaunaEntity request = new EspecieFaunaEntity();
            short idEspecie=0;
            if(infBasicaAereaDetalleDto.getIdFauna()!=null) {
                 idEspecie = infBasicaAereaDetalleDto.getIdFauna().shortValue();
            }
            request.setIdEspecie(idEspecie);
            ResultEntity<EspecieFaunaEntity> lstListdoFauna = corecentral.ListaPorFiltroEspecieFauna(request);
            if (lstListdoFauna != null){
                lstFauna.add(lstListdoFauna.getData().get(0));
            }
        }

            if(lstFauna.size()>0) {
                for (EspecieFaunaEntity detalleEntity : lstFauna) {

                    cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getNombreCientifico() != null ? detalleEntity.getNombreCientifico().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getCategoria() != null ? detalleEntity.getCategoria().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public PdfPTable generarInfoAreFLO(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Nombre común", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Status", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);


        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaFLO = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIFLOR");
        if (lstInfoAreaFLO !=null) {
            List<EspecieForestalEntity> lstFlora = new ArrayList<>();
            for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lstInfoAreaFLO) {
                EspecieForestalEntity request = new EspecieForestalEntity();
                short idEspecie=0;
                if(infBasicaAereaDetalleDto.getIdFauna()!=null) {
                    idEspecie =infBasicaAereaDetalleDto.getIdFlora().shortValue();
                }
                request.setIdEspecie(idEspecie);
                ResultEntity<EspecieForestalEntity>lstFloraTotal= corecentral.ListaPorFiltroEspecieForestal(request);
                if(lstFloraTotal!=null)
                    lstFlora.add(lstFloraTotal.getData().get(0));
            }

            if(lstFlora.size()>0) {
                for (EspecieForestalEntity detalleEntity : lstFlora) {

                    cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getNombreCientifico() != null ? detalleEntity.getNombreCientifico().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getFamilia() != null ? detalleEntity.getFamilia().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public PdfPTable generarInfoAreOrganizacion(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Organización para\n el aprovechamiento", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº de familias o personas\n involucradas en la actividad", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especie a \naprovechar", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tipo de producto \na aprovechar", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);

        //3.	INFORMACIÓN DEL ÁREA
        List<InfBasicaAereaDetalleDto> lstInfoAreaOrganiza = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIASP");


        if (lstInfoAreaOrganiza !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaOrganiza) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getPuntoVertice() != null ? detalleEntity.getPuntoVertice().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getZonaVida() != null ? detalleEntity.getZonaVida().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getReferencia() != null ? detalleEntity.getReferencia().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarOrdenamientoInterno(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Denominación o espacios", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nº de familias o personas\n involucradas en la actividad", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //4.	ORDENAMIENTO INTERNO
        List<InfBasicaAereaDetalleDto> lstInfoAreaOrganiza = informacionBasicaRepository.listarInfBasicaAerea("PMFI",idPlanManejo,"PMFIASP");


        if (lstInfoAreaOrganiza !=null) {
            for (InfBasicaAereaDetalleDto detalleEntity : lstInfoAreaOrganiza) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getPuntoVertice() != null ? detalleEntity.getPuntoVertice().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getZonaVida() != null ? detalleEntity.getZonaVida().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getReferencia() != null ? detalleEntity.getReferencia().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarIdentiEspecieNoMaderable(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("N°", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especies", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Productos", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre común", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaRecursoNoMaderable = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"IDEN");
        int i=1;
        if (lstListaRecursoNoMaderable !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaRecursoNoMaderable) {

                cell = new PdfPCell(new Paragraph(String.valueOf(i), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreCientifico() != null ? detalleEntity.getNombreCientifico().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getProducto() != null ? detalleEntity.getProducto().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarIdentiEspecieMaderable(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("N°", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especies", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Productos", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre científico", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre común", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaRecursoMaderable = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"IDEN");
        int i=1;
        if (lstListaRecursoMaderable !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaRecursoMaderable) {

                cell = new PdfPCell(new Paragraph(String.valueOf(i), contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreCientifico() != null ? detalleEntity.getNombreCientifico().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getProducto() != null ? detalleEntity.getProducto().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarRealizacionNO(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Fecha de realización del inventario:", contenido));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaFechaRealizacionNO = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"INVE");

        if (lstListaFechaRealizacionNO !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaFechaRealizacionNO) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getFechaRealizacionInventario() != null ? formatter.format(detalleEntity.getFechaRealizacionInventario()) : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionFechaRealizacion() != null ? detalleEntity.getDescripcionFechaRealizacion().toString() : "", contenido));
                cell.setFixedHeight(25);
                cell.setColspan(2);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInventarioANO(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("\t\tEspecie:", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tCastaña (Bertholletia excelsa)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(4);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° estrada", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\tN° individuos", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Producto \na obtener", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\t\t\tProducción", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Aprov. *", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Sem.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Unidad de medida", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total (Barricas)", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Aprov*.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaInventarioANO = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"INVE");

        if (lstListaInventarioANO !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaInventarioANO) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getNumParcela() != null ? detalleEntity.getNumParcela().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesAprovechables() != null ? detalleEntity.getNumeroArbolesAprovechables().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesSemilleros() != null ? detalleEntity.getNumeroArbolesSemilleros().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getProducto() != null ? detalleEntity.getProducto().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getUniMedida() != null ? detalleEntity.getUniMedida().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesTotal() != null ? detalleEntity.getNumeroArbolesTotal().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getVolumenComercial() != null ? detalleEntity.getVolumenComercial().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInventarioBNO(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Especie:", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° parcelas o estradas:", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\tN° de individuos", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\tEstimación de la producción ", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Aprov.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Sem.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Unidad de medida", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Cantidad", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaInventarioBNO = recursoForestalRepository.listarRecursoForestal(idPlanManejo,1,"RESU");

        if (lstListaInventarioBNO !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaInventarioBNO) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumParcela() != null ? detalleEntity.getNumParcela().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesTotal() != null ? detalleEntity.getNumeroArbolesTotal().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesAprovechables() != null ? detalleEntity.getNumeroArbolesAprovechables().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesSemilleros() != null ? detalleEntity.getNumeroArbolesSemilleros().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getUniMedida() != null ? detalleEntity.getUniMedida().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getVolumenComercial() != null ? detalleEntity.getVolumenComercial().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarRealizacion(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Fecha de realización del inventario:", contenido));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaFechaRealizacion = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"INVE");

        if (lstListaFechaRealizacion !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaFechaRealizacion) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getFechaRealizacionInventario() != null ? formatter.format(detalleEntity.getFechaRealizacionInventario()) : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionFechaRealizacion() != null ? detalleEntity.getDescripcionFechaRealizacion().toString() : "", contenido));
                cell.setFixedHeight(25);
                cell.setColspan(2);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInventarioA(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("\t\tEspecie:", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tCastaña (Bertholletia excelsa)", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(4);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° estrada", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\tN° individuos", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Producto \na obtener", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\t\t\tProducción", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Aprov. *", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Sem.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Unidad de medida", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total (Barricas)", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Aprov*.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaInventarioA = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"INVE");

        if (lstListaInventarioA !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaInventarioA) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getNumParcela() != null ? detalleEntity.getNumParcela().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesAprovechables() != null ? detalleEntity.getNumeroArbolesAprovechables().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesSemilleros() != null ? detalleEntity.getNumeroArbolesSemilleros().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getProducto() != null ? detalleEntity.getProducto().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getUniMedida() != null ? detalleEntity.getUniMedida().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesTotal() != null ? detalleEntity.getNumeroArbolesTotal().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getVolumenComercial() != null ? detalleEntity.getVolumenComercial().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarInventarioB(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Especie:", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° parcelas o estradas:", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\tN° de individuos", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(3);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\tEstimación de la producción ", subTitulo2));
        cell.setFixedHeight(20);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Aprov.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Sem.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Unidad de medida", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Cantidad", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //5.	INFORMACIÓN DE ESPECIES, RECURSOS O SERVICIOS
        List<RecursoForestalDetalleDto> lstListaInventarioB = recursoForestalRepository.listarRecursoForestal(idPlanManejo,2,"RESU");

        if (lstListaInventarioB !=null) {
            for (RecursoForestalDetalleDto detalleEntity : lstListaInventarioB) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getNombreComun() != null ? detalleEntity.getNombreComun().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumParcela() != null ? detalleEntity.getNumParcela().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesTotal() != null ? detalleEntity.getNumeroArbolesTotal().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesAprovechables() != null ? detalleEntity.getNumeroArbolesAprovechables().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getNumeroArbolesSemilleros() != null ? detalleEntity.getNumeroArbolesSemilleros().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getUniMedida() != null ? detalleEntity.getUniMedida().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getVolumenComercial() != null ? detalleEntity.getVolumenComercial().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarEtapasPMFI(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Etapas", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Equipos, herramientas e insumos utilizados.", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //6.	SISTEMA DE MANEJO, APROVECHAMIENTO Y LABORES SILVICULTURALES
        //6.1
        ResultClassEntity<ActividadSilviculturalDto>lstEtapastotal= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"5");

        if (lstEtapastotal !=null && lstEtapastotal.getData().getDetalle()!=null) {
            for (ActividadSilviculturalDetalleEntity detalleEntity : lstEtapastotal.getData().getDetalle()) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getActividad() != null ? detalleEntity.getActividad().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionDetalle() != null ? detalleEntity.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getEquipo() != null ? detalleEntity.getEquipo().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarCicloCortaPMFI(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(90);

        PdfPCell cell;
        int size = 20;

        //6.2
        ResultClassEntity<List<ActividadSilviculturalDto>> lstSivicultural = actividadSilviculturalRepository.ListarActividadSilviculturalCabecera(idPlanManejo);

        if (lstSivicultural !=null && lstSivicultural.getData()!=null) {
            for (ActividadSilviculturalDto detalleEntity : lstSivicultural.getData()) {
                if (detalleEntity.getCodActividad()!=null) {
                    if (detalleEntity.getCodActividad().equals("DIVICAD")) {
                        cell = new PdfPCell(new Paragraph(detalleEntity.getObservacion() != null ? detalleEntity.getObservacion().toString() : "", contenido));
                        cell.setFixedHeight(25);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public PdfPTable generarLaboresSilviculturalesPMFI(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(90);
        float[] values = new float[3];
        values[0] = 100;
        values[1] = 30;
        values[2] = 150;
        table.setWidths(values);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Labores", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Marcar", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //6.3
        ResultClassEntity<ActividadSilviculturalDto>lstLabores= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"6");

        if (lstLabores !=null && lstLabores.getData().getDetalle()!=null) {
            for (ActividadSilviculturalDetalleEntity detalleEntity : lstLabores.getData().getDetalle()) {

                cell = new PdfPCell(new Paragraph(detalleEntity.getActividad() != null ? detalleEntity.getActividad().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getAccion() == true ? "X" : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcionDetalle() != null ? detalleEntity.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }


    public PdfPTable generarMedidasPMFI(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("\t\t\t\tMedidas", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("\t\t\t\t\t\tDescripción", subTitulo2));
        cell.setFixedHeight(20);
        table.addCell(cell);

        //7.	MEDIDAS DE PROTECCIÓN DE LA UNIDAD DE MANEJO FORESTAL
        MedidasPmfiDto medidas = new MedidasPmfiDto();
        medidas.setIdPlanManejo(idPlanManejo);
        medidas.setCodigoImpactoAmbiental("PMFIMP");
        medidas.setCodigoImpactoAmbientalDet("PMFIMPPRO");
        ResultClassEntity response = ambiental.ListarImpactoAmbientalPmfi(medidas);

        if(response!=null) {

            ImpactoAmbientalPmfiDto impactoAmbientalEntityList = (ImpactoAmbientalPmfiDto) response.getData();

            if (impactoAmbientalEntityList != null && impactoAmbientalEntityList.getDetalle() != null ) {
                for (ImpactoAmbientalDetalleEntity detalleEntity : impactoAmbientalEntityList.getDetalle()) {

                    cell = new PdfPCell(new Paragraph(detalleEntity.getMedidasProteccion() != null ? detalleEntity.getMedidasProteccion().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }


    public PdfPTable generarIdentificacionImpactosPMFI(PdfWriter writer, Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 20;

        cell = new PdfPCell(new Paragraph("Actividad que \ngenera impacto", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del\n Impacto", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de\n prevención", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Medidas de\n Mitigación", subTitulo2));
        cell.setFixedHeight(20);
        cell.setRowspan(2);
        table.addCell(cell);

        //8.	IDENTIFICACIÓN DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCIÓN Y MITIGACIÓN
        MedidasPmfiDto impacto = new MedidasPmfiDto();
        impacto.setIdPlanManejo(idPlanManejo);
        impacto.setCodigoImpactoAmbiental( "PMFIIA");
        impacto.setCodigoImpactoAmbientalDet("PMFIIAMIT");
        ResultClassEntity responseImpacto = ambiental.ListarImpactoAmbientalPmfi(impacto);

        if(responseImpacto!=null) {

            ImpactoAmbientalPmfiDto identificacionImpactos =  (ImpactoAmbientalPmfiDto)responseImpacto.getData();

            if (identificacionImpactos != null && identificacionImpactos.getDetalle() != null ) {
                for (ImpactoAmbientalDetalleEntity detalleEntity : identificacionImpactos.getDetalle()) {

                    cell = new PdfPCell(new Paragraph(detalleEntity.getActividad() != null ? detalleEntity.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getDescripcion() != null ? detalleEntity.getDescripcion().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getMedidasProteccion() != null ? detalleEntity.getMedidasProteccion().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalleEntity.getMedidasMitigacion() != null ? detalleEntity.getMedidasMitigacion().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }


    public  PdfPTable createCronogramaActividadesAnios(PdfWriter writer,Integer idPlanManejo) throws Exception {

        PdfPTable table = new PdfPTable(40);
        table.setWidthPercentage(90);
        PdfPCell cell;
        float[] values = new float[40];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] = 80;
            }else{
                values[i] = 15;
            }
        }

        table.setWidths(values);
        int size = 30;



        Paragraph titlePara2 = new Paragraph("Actividad",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(3);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara3 = new Paragraph("Años",cabecera);
        titlePara3.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara3);
        cell.setColspan(39);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara7 = new Paragraph("4",cabecera);
        titlePara7.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara7);
        cell.setFixedHeight(size);
        cell.setRowspan(3);
        table.addCell(cell);
        Paragraph titlePara8 = new Paragraph("5",cabecera);
        titlePara8.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara8);
        cell.setFixedHeight(size);
        cell.setRowspan(3);
        table.addCell(cell);
        Paragraph titlePara9 = new Paragraph("6",cabecera);
        titlePara9.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara9);
        cell.setFixedHeight(size);
        cell.setRowspan(3);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //9.	CRONOGRAMA DE ACTIVIDADES PARA EL PERIODO DE EJECUCIÓN
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("PMFI");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();

        String marca="";
        if(lstcrono!=null) {
            for (CronogramaActividadEntity detalle : lstcrono) {
                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                List<CronogramaActividadDetalleEntity> lstDetalle = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                if (lstDetalle != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                if (lstDetalle2 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);


                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                if (lstDetalle3 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }

                List<CronogramaActividadDetalleEntity> lstDetalle4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 4).collect(Collectors.toList());
                if (lstDetalle4 != null) {
                    cell = new PdfPCell(new Paragraph(lstDetalle4.size() > 0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }

                List<CronogramaActividadDetalleEntity> lstDetalle5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 5).collect(Collectors.toList());
                if (lstDetalle5 != null) {
                    cell = new PdfPCell(new Paragraph(lstDetalle5.size() > 0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }

                List<CronogramaActividadDetalleEntity> lstDetalle6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 6).collect(Collectors.toList());
                if (lstDetalle6 != null) {
                    cell = new PdfPCell(new Paragraph(lstDetalle6.size() > 0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }

            }
        }

        return table;
    }



    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }

}
