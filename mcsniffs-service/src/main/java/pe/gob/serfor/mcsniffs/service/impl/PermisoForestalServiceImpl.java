package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ModeloContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.PermisoForestalService;
import pe.gob.serfor.mcsniffs.service.util.DocUtil;
import pe.gob.serfor.mcsniffs.service.util.DocUtilPermisos;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;

@Service
public class PermisoForestalServiceImpl implements PermisoForestalService {

    @Autowired
    private PermisoForestalRepository forestalRepository;

    @Autowired
    private ArchivoSolicitudRepository archivoSolicitudRepository;

    @Autowired
    private FileServerRepository fileServerRepository;

    @Autowired
    private SolicitudMovimientoFlujoRepository movimientoFlujoRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Autowired
    private TituloHabilitanteRepository tituloHabilitanteRepository;

    @Override
    public ResultClassEntity<Boolean> archivoEliminar(Integer idArchivo) {
        
        return archivoSolicitudRepository.archivoEliminar(idArchivo);
    }

    @Override
    public ResultClassEntity ObtenerPersonaPorUsuario(Integer idUsuario) {
        PersonaEntity personaEntity = new PersonaEntity();
        personaEntity.setIdUsuario(idUsuario);
        return forestalRepository.ObtenerPersonaPorUsuario(personaEntity);
    }

    @Override
    public ResultClassEntity obtenerSolicitudPorAccesoSolicitud(Integer idSolicitud, Integer idSolicitudAcceso) {
        return forestalRepository.obtenerSolicitudPorAccesoSolicitud(idSolicitud, idSolicitudAcceso);
    }

    @Override
    public ResultClassEntity registrarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity) {
        return forestalRepository.registrarSolicitudPorAccesoSolicitud(solicitudRequestEntity);
    }

    @Override
    public ResultClassEntity editarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity) {
        return forestalRepository.editarSolicitudPorAccesoSolicitud(solicitudRequestEntity);
    }

    // ARCHIVOS
    @Override
    public ResultClassEntity<ArchivoEntity> registrarArchivoSolicitud(MultipartFile file, Integer idSolicitud, String tipoArchivo, Boolean flagActualiza) {
        if (flagActualiza.booleanValue()) {
            String nombreArchivoGenerado = fileServerRepository.insertarArchivoSolicitud(file);
            return archivoSolicitudRepository.actualizarArchivoSolicitud(nombreArchivoGenerado, file.getOriginalFilename(), idSolicitud, tipoArchivo);
        } else {
            // SE REGISTRA NUEVO ARCHIVO
            String nombreArchivoGenerado = fileServerRepository.insertarArchivoSolicitud(file);
            return archivoSolicitudRepository.insertarArchivoSolicitud(nombreArchivoGenerado, file.getOriginalFilename(), idSolicitud, tipoArchivo);
        }
    }

    @Override
    public ResultClassEntity<ArchivoEntity> registrarArchivoSolicitudAdjunto(MultipartFile file, Integer idSolicitud, String tipoArchivo, String descripcionArchivo, Boolean flagActualiza) {
        if (flagActualiza.booleanValue()) {
            String nombreArchivoGenerado = fileServerRepository.insertarArchivoSolicitud(file);
            return archivoSolicitudRepository.actualizarArchivoSolicitudAdjunto(nombreArchivoGenerado, file.getOriginalFilename(), idSolicitud, tipoArchivo, descripcionArchivo);
        } else {
            String nombreArchivoGenerado = fileServerRepository.insertarArchivoSolicitud(file);
            return archivoSolicitudRepository.insertarArchivoSolicitudAdjunto(nombreArchivoGenerado, file.getOriginalFilename(), idSolicitud, tipoArchivo, descripcionArchivo);
        }
    }


    @Override
    public List<ArchivoSolicitudEntity> obtenerArchivoSolicitud(Integer idSolicitud) {
        return archivoSolicitudRepository.obtenerArchivoSolicitud(idSolicitud);
    }


    @Override
    public List<ArchivoSolicitudEntity> obtenerArchivoSolicitudAdjunto(Integer idSolicitud) {
        return archivoSolicitudRepository.obtenerArchivoSolicitudAdjunto(idSolicitud);
    }

    @Override
    public ResultArchivoEntity descargarArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo) {
        return fileServerRepository.descargarArchivoSolicitud(idArchivoSolicitud, idSolicitud, tipoArchivo);
    }

    @Override
    public ResultArchivoEntity descargarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAdjunto, Integer idSolicitud, String tipoArchivo) {
        return fileServerRepository.descargarArchivoSolicitudAdjunto(idArchivoSolicitudAdjunto, idSolicitud, tipoArchivo);
    }

    @Override
    public ResultClassEntity eliminarArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo) {
        return archivoSolicitudRepository.eliminarArchivoSolicitud(idArchivoSolicitud, idSolicitud, tipoArchivo);
    }

    @Override
    public ResultClassEntity eliminarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAjunto, Integer idSolicitud, String tipoArchivo) {
        return archivoSolicitudRepository.eliminarArchivoSolicitudAdjunto(idArchivoSolicitudAjunto, idSolicitud, tipoArchivo);
    }


    @Override
    public ResultClassEntity ListarPermisosForestales(SolicitudEntity request) {
        // TODO Auto-generated method stub
        return forestalRepository.ListarPermisosForestales(request);
    }

    @Override
    public ResultEntity RegistrarPermisosForestales(SolicitudEntity request) {
        // TODO Auto-generated method stub
        return forestalRepository.RegistrarPermisosForestales(request);
    }


    @Override
    public ResultEntity ObtenerPermisosForestales(SolicitudEntity request) {
        // TODO Auto-generated method stub
        return forestalRepository.ObtenerPermisosForestales(request);
    }

    @Override
    public ResultClassEntity validarRequisitoSolicitud(ValidacionRequisitoEntity entity) {
        return forestalRepository.validarRequisitoSolicitud(entity);
    }

    @Override
    public ResultClassEntity obtenerSolicitudValidacionPorSeccion(ValidacionRequisitoEntity entity) {
        return forestalRepository.obtenerSolicitudValidacionPorSeccion(entity);
    }

    @Override
    public ResultClassEntity obtenerSolicitudValidacionPorSolicitud(ValidacionRequisitoEntity entity) {
        return forestalRepository.obtenerSolicitudValidacionPorSolicitud(entity);
    }

    @Override
    public ResultClassEntity obtenerSolicitudEvaluacionPorSolicitud(ValidacionRequisitoEntity entity) {
        return forestalRepository.obtenerSolicitudEvaluacionPorSolicitud(entity);
    }

    @Override
    public ResultClassEntity<Boolean> envioSolicitudEvaluacion(ValidacionRequisitoEntity entity) {
        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        SolicitudMovimientoFlujoEntity movimientoFlujoEntity = new SolicitudMovimientoFlujoEntity();
        EmailEntity emailModel = new EmailEntity();
        StringBuilder builder = new StringBuilder();
        builder.append("<p>Estimado <strong>" + entity.getNombrePersonaSolicitud().concat(" ").concat(entity.getApellidoPersonaSolicitud()) + "</strong>, ");
        builder.append("se ha observado la solicitud Nro. ");
        builder.append(entity.getIdSolicitud());
        builder.append(" registrada por Ud.");
        emailModel.setContent(builder.toString());
        String[] mails =new String[0];
        mails[0]=entity.getEmailPersonaSolicitud().trim();
        emailModel.setEmail(mails);
        emailModel.setSubject("Mensaje de Solicitud");
        boolean boolEnvioCorreo = emailService.sendEmail(emailModel);
        if (boolEnvioCorreo) {
            result.setData(boolEnvioCorreo);
            result.setSuccess(true);
            result.setMessage("Se envio el correo.");

            movimientoFlujoEntity.setIdSolicitud(entity.getIdSolicitud());
            movimientoFlujoEntity.setTipoValidacion(entity.getTipovalidacion());
            movimientoFlujoEntity.setFlagObservado(entity.getFlagObservado());
            movimientoFlujoEntity.setIdUsuarioRegistro(entity.getIdUsuarioRegistro());
            movimientoFlujoRepository.registroSolicitudMovimientoFlujo(movimientoFlujoEntity);
        } else {
            result.setData(boolEnvioCorreo);
            result.setSuccess(false);
            result.setMessage("No se envio el correo.");
        }

        return result;
    }

    @Override
    public ResultClassEntity evaluarRequisitoSolicitud(ValidacionRequisitoEntity entity) {
        return forestalRepository.evaluarRequisitoSolicitud(entity);
    }

    @Override
    public Pageable<List<PermisoForestalEntity>> filtrar(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
            String codEstado, String codTipoPersona, String codTipoActor, String codTipoCncc, String numeroDocumento,
            String nombrePersona, String razonSocial, Page page) throws Exception {
        return forestalRepository.filtrar(idPermisoForestal, idTipoProceso, idTipoPermiso, codEstado, codTipoPersona,
                codTipoActor, codTipoCncc, numeroDocumento, nombrePersona, razonSocial, page);
    }

    @Override
    public Pageable<List<PermisoForestalEntity>> filtrarEval(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
                                                             String fechaRegistro, String codigoEstado, String codigoPerfil, Page page) throws Exception {
        return forestalRepository.filtrarEval(idPermisoForestal, idTipoProceso, idTipoPermiso, fechaRegistro, codigoEstado, codigoPerfil, page);
    }

    @Override
    public ResultClassEntity registrarPermisoForestal(PermisoForestalEntity obj) throws Exception {



        ResultClassEntity resultClassEntity = forestalRepository.registrarPermisoForestal(obj);

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_PERMISOFORESTAL,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MVC_PERMISOFORESTAL + resultClassEntity.getCodigo(),
                obj.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    @Override
    public ResultClassEntity actualizarEstadoPermisoForestal(PermisoForestalEntity obj) throws Exception {
        return forestalRepository.actualizarEstadoPermisoForestal(obj);
    }

    @Override
    public ResultClassEntity registrarInformacionGeneral(InformacionGeneralPermisoForestalEntity obj) throws Exception {
        return forestalRepository.registrarInformacionGeneral(obj);
    }

    @Override
    public ResultClassEntity<InformacionGeneralPermisoForestalEntity> listarInformacionGeneral(InformacionGeneralPermisoForestalEntity filtro) throws Exception {
        return forestalRepository.listarInformacionGeneral(filtro);
    }

    @Override
    public ResultClassEntity<CodigoCifradoEntity> codigoCifrado(CodigoCifradoEntity filtro) throws Exception {
        return forestalRepository.codigoCifrado(filtro);
    }

    @Override
    public ResultClassEntity RegistrarArchivo(List<SolicitudSANArchivoEntity> list) throws Exception {
        return forestalRepository.RegistrarArchivo(list);
    }

    @Override
    public List<SolicitudSANArchivoEntity> ListarArchivo(SolicitudSANArchivoEntity param) throws Exception {
        return forestalRepository.ListarArchivo(param);
    }


    @Override
    public ResultClassEntity EliminarArchivo(SolicitudSANArchivoEntity param) throws Exception {
        return  forestalRepository.EliminarArchivo(param);
    }
    
    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Lista Plan Manejo por filtro }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPorFiltroPlanManejo(String nroDocumento, String codTipoPlan) throws Exception {
        return forestalRepository.listarPorFiltroPlanManejo(nroDocumento, codTipoPlan);
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Registrar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarPermisoForestalPlanManejo(List<PermisoForestalPlanManejoEntity> items) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                forestalRepository.registrarPermisoForestalPlanManejo(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se registró correctamente.");
        result.setSuccess(true);
        return result;
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Listar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @Override
    public ResultClassEntity listarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception {
        return forestalRepository.listarPermisoForestalPlanManejo(item);
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Eliminar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @Override
    public ResultClassEntity eliminarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception {
        return forestalRepository.eliminarPermisoForestalPlanManejo(item);
    }
    @Override
    public ResultClassEntity permisoForestalActualizarRemitido(PermisoForestalPlanManejoEntity request)throws Exception{


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_PERMISOFORESTAL,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PERMISOFORESTAL + request.getIdPermisoForestal(),
                request.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);


        return forestalRepository.permisoForestalActualizarRemitido(request);
    }
    @Override
    public ResultArchivoEntity descargarPlantillaFormatoPermiso() throws Exception {

        ResultArchivoEntity result = new ResultArchivoEntity();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/PermisosForestal/PlantillaFormatoPermiso.docx");
        File fileCopi = File.createTempFile("PlantillaFormatoPermiso",".docx");
        FileUtils.copyInputStreamToFile(inputStream, fileCopi);
        byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
        result.setArchivo(fileContent);
        result.setNombeArchivo("PlantillaFormatoPermiso.docx");
        result.setContenTypeArchivo("application/octet-stream");

        return result;
    }

    @Override
    public ResultArchivoEntity descargarPlantillaResolAprobacion() throws Exception {


        ResultArchivoEntity result = new ResultArchivoEntity();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/PermisosForestal/PlantillaResolucionAprobacion.docx");
        File fileCopi = File.createTempFile("PlantillaResolucionAprobacion",".docx");
        FileUtils.copyInputStreamToFile(inputStream, fileCopi);
        byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
        result.setArchivo(fileContent);
        result.setNombeArchivo("PlantillaResolucionAprobacion.docx");
        result.setContenTypeArchivo("application/octet-stream");

        return result;
    }

    @Override
    public ResultArchivoEntity descargarPlantillaResolDesAprobacion() throws Exception {

        ResultArchivoEntity result = new ResultArchivoEntity();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/PermisosForestal/PlantillaResolucionDesaprobacion.docx");
        File fileCopi = File.createTempFile("PlantillaResolucionDesaprobacion",".docx");
        FileUtils.copyInputStreamToFile(inputStream, fileCopi);
        byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
        result.setArchivo(fileContent);
        result.setNombeArchivo("PlantillaResolucionDesaprobacion.docx");
        result.setContenTypeArchivo("application/octet-stream");

        return result;
    }

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }




    @Override
    public ResultArchivoEntity plantillaPermisosForestales(Integer idPermisoForestal) throws Exception {

        XWPFDocument doc = getDoc("formatoPlantillaPermisos.docx");
        List<InformacionGeneralPermisoForestalEntity> listaSol=new ArrayList<>();
        InformacionGeneralPermisoForestalEntity pfcr = new InformacionGeneralPermisoForestalEntity();
        pfcr.setCodTipoInfGeneral("PFCR");
        pfcr.setIdPermisoForestal(idPermisoForestal);
        pfcr.setIdInfGeneral(null);
        ResultClassEntity<InformacionGeneralPermisoForestalEntity>lstInfPermisoForestal= listarInformacionGeneral(pfcr);

        List <InformacionGeneralPermisoForestalEntity> lista = (List<InformacionGeneralPermisoForestalEntity>) lstInfPermisoForestal.getData();

        DocUtilPermisos.setearInfoPermiso(lista, doc);


        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("formatoPlantillaPermisos.docx");
        word.setTipoDocumento("Permiso");
        word.setArchivo(b.toByteArray());

        return word;
    }
    @Override
    public  ResultClassEntity actualizarPermisoForestal(PermisoForestalEntity obj) throws Exception {

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_PERMISOFORESTAL,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PERMISOFORESTAL + obj.getIdPermisoForestal(),
                obj.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return forestalRepository.actualizarPermisoForestal(obj);
    }

}
