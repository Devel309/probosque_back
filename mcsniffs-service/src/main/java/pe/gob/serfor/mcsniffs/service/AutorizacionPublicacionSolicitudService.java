package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.EstatusProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface AutorizacionPublicacionSolicitudService {

    ResultArchivoEntity descargarPlantillaCartaAutorizacion();
    ResultClassEntity guardarAutorizacionPublicacionSolicitud( AutorizacionPublicacionSolicitudEntity request);
    ResultClassEntity enviarAutorizacionPublicacionSolicitud(EstatusProcesoPostulacionEntity request, String token);
}
