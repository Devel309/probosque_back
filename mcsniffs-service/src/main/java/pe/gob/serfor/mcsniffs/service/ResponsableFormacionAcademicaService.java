package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;

import java.util.List;

public interface ResponsableFormacionAcademicaService {

    ResultClassEntity obtenerInformacionFormacionProfesional(ResponsableFormacionAcademicaEntity obj) throws Exception;
    ResultClassEntity registrarInformacionFormacionProfesional(List<ResponsableFormacionAcademicaEntity> items) throws Exception;
    ResultClassEntity actualizarInformacionFormacionProfesional(SolicitudConcesionResponsableDto dto) throws Exception;
    ResultClassEntity eliminarInformacionFormacionProfesional(ResponsableFormacionAcademicaEntity obj) throws Exception;
}
