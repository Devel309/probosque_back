package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;

import java.util.List;

public interface CoreCentralService {

	//MIGUEL
	List<AutoridadForestalEntity> ListarPorFiltroAutoridadForestal(AutoridadForestalEntity param) throws Exception;
	List<TipoComunidadEntity> ListarPorFiltroTipoComunidad (TipoComunidadEntity param) throws Exception;
	List<TipoTrasporteEntity> ListarPorFiltroTipoTrasporte (TipoTrasporteEntity param) throws Exception;
	List<TipoZonaEntity> listarPorFiltroTipoZona(TipoZonaEntity param) throws Exception;
	List<DepartamentoEntity> ListarPorFiltroDepartamentos(DepartamentoEntity param) throws Exception;
	//IVAN
	List<TipoBosqueEntity> listarPorFiltroTipoBosque(TipoBosqueEntity tipoBosqueEntity) throws Exception;
	List<TipoConcesionEntity> listarPorFiltroTipoConcesion(TipoConcesionEntity tipoConcesionEntity) throws Exception;
	List<TipoContratoEntity> listarPorFiltroTipoContrato(TipoContratoEntity tipoContratoEntity) throws Exception;
	List<TipoDocGestionErgtfEntity> listarPorFiltroTipoDocGestionErgtf(TipoDocGestionErgtfEntity tipoDocGestionErgtfEntity) throws Exception;
	List<TipoDocGestionEntity> listarPorFiltroTipoDocGestion(TipoDocGestionEntity tipoDocGestionEntity) throws Exception;
	List<TipoDocumentoEntity> listarPorFiltroTipoDocumento(TipoDocumentoEntity tipoDocumentoEntity) throws Exception;
	List<TipoEscalaEntity> listarPorFiltroTipoEscala(TipoEscalaEntity tipoEscalaEntity) throws Exception;
	List<TipoMonedaEntity> listarPorFiltroTipoMoneda(TipoMonedaEntity tipoMonedaEntity) throws Exception;
	List<TipoOficinaEntity> listarPorFiltroTipoOficina(TipoOficinaEntity tipoOficinaEntity) throws Exception;
	List<TipoPagoEntity> listarPorFiltroTipoPago(TipoPagoEntity tipoPagoEntity) throws Exception;
	List<TipoPermisoEntity> listarPorFiltroTipoPermiso(TipoPermisoEntity tipoPermisoEntity) throws Exception;
	List<TipoProcedimientoConcesEntity> listarPorFiltroTipoProcedimientoConces(TipoProcedimientoConcesEntity tipoProcedimientoConcesEntity) throws Exception;
	List<TipoProductoEntity> listarPorFiltroTipoProducto(TipoProductoEntity tipoProductoEntity) throws Exception;
	List<TipoRegenteEntity> listarPorFiltroTipoRegente(TipoRegenteEntity tipoRegenteEntity) throws Exception;
	List<UbigeoArffsEntity> listarPorFiltroUbigeoArffs(UbigeoArffsEntity ubigeoArffsEntity) throws Exception;
	List<UnidadMedidaEntity> listarPorFiltroUnidadMedida(UnidadMedidaEntity UnidadMedidaEntity) throws Exception;
	List<RegenteComunidadEntity> listarPorFiltroRegenteComunidad(RegenteComunidadEntity regenteComunidad) throws Exception;
	//Jaqueline DB :3
	ResultEntity<ProvinciaEntity> listarPorFilroProvincia(ProvinciaEntity provincia);
	ResultEntity<DistritoEntity> listarPorFilroDistrito(DistritoEntity provincia);

	ResultEntity<EspecieForestalEntity> ListaPorFiltroEspecieForestal(EspecieForestalEntity request);
	ResultEntity<EspecieFaunaEntity> ListaPorFiltroEspecieFauna(EspecieFaunaEntity request);


	//List<EspecieForestalEntity> ListaEspecieForestalPorFiltro(EspecieForestalEntity param) throws Exception;
	ResultEntity ListaEspecieForestalPorFiltro(EspecieForestalEntity request);
	ResultEntity ListaEspecieFaunaPorFiltro(EspecieFaunaEntity request);
	ResultClassEntity listarEspecieForestalSincronizacion(EspecieForestalEntity param);


	ResultClassEntity listarCuencaSubcuenca(CuencaEntity param);

}
