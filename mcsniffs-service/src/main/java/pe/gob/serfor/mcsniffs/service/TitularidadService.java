package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaTitularidadDto;
import pe.gob.serfor.mcsniffs.entity.*;

public interface TitularidadService {

    ResultClassEntity consultarTitularidad(String tipoParticipante,String apellidoPaterno,String apellidoMaterno,String nombres,String razonSocial, String token) throws Exception;


}

