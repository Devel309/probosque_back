package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiltroFiscalizacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Fiscalizacion.FiscalizacionDto;

public interface FiscalizacionService {
    ResultClassEntity registrarFiscalizacion(FiscalizacionDto obj) throws Exception;
    ResultClassEntity AdjuntarArchivosFiscalizacion(FiscalizacionEntity obj);
    ResultClassEntity listarFiscalizacion(FiscalizacionDto obj);
    ResultEntity<FiscalizacionAdjuntosEntity> ObtenerArchivosFiscalizacion(Integer IdFiscalizacion);
    ResultClassEntity ObtenerFiscalizacion(FiscalizacionDto obj);
    ResultClassEntity eliminarFiscalizacionArchivo(FiscalizacionArchivoDto obj) throws Exception;
    ResultArchivoEntity DescagarPlantillaFormatoFiscalizacion();
}
