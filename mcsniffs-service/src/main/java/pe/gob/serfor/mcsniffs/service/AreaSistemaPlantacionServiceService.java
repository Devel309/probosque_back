package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;

public interface AreaSistemaPlantacionServiceService {
    
    ResultClassEntity registrarAreaSistemaPlantacion(List<AreaSistemaPlantacionDto> obj) throws Exception;
    ResultClassEntity listarAreaSistemaPlantacion(AreaSistemaPlantacionDto obj) throws Exception;
    ResultClassEntity eliminarAreaSistemaPlantacion(AreaSistemaPlantacionDto obj) throws Exception;
    ResultClassEntity ListarEspecieForestalSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto obj) throws Exception;
    ResultClassEntity eliminarSistemaPlantacionEspecieLista(List<EspecieForestalSistemaPlantacionEspecieDto> obj) throws Exception;
    ResultClassEntity registrarSistemaPlantacionEspecie(List<EspecieForestalSistemaPlantacionEspecieDto> obj) throws Exception;
}
