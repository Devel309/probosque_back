package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPOACService {

    ByteArrayResource consolidadoPOAC(Integer idPlanManejo) throws Exception;

}
