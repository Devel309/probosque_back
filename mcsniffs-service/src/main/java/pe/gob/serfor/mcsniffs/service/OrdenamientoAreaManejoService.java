package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoAreaManejoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface OrdenamientoAreaManejoService {
    ResultClassEntity RegistrarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param,MultipartFile file)throws Exception;
    ResultClassEntity ListarOrdenamientoAreaManejo(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param)throws Exception;
    List<Dropdown> ComboCategoriaOrdenamiento(ParametroEntity param) throws Exception;
    ResultClassEntity ObtenerOrdenamientoAreaManejoArchivo(OrdenamientoAreaManejoEntity param)throws Exception;
}
