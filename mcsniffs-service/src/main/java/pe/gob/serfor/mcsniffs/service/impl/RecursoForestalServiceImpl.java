package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDto;
import pe.gob.serfor.mcsniffs.repository.RecursoForestalRepository;
import pe.gob.serfor.mcsniffs.service.RecursoForestalService;

import java.util.List;

@Service("RecursoForestalService")
public class RecursoForestalServiceImpl implements RecursoForestalService {

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    public ResultClassEntity RegistrarRecursoForestal(List<RecursoForestalEntity> list) throws Exception {
        return recursoForestalRepository.RegistrarRecursoForestal(list);
    }

    /*
        @Override
        public ResultClassEntity RegistrarRecursoForestal(RecursoForestalEntity param) throws Exception {
            return recursoForestalRepository.RegistrarRecursoForestal(param);
    }
    */



    @Override
    public ResultClassEntity EliminarRecursoForestal(RecursoForestalDetalleDto param) throws Exception {
        return  recursoForestalRepository.EliminarRecursoForestal(param);
    }



    @Override
    public ResultClassEntity<List<RecursoForestalDetalleDto>> listarRecursoForestal(Integer idPlanManejo,Integer idRecursoForestal,String codCabecera)
            throws Exception {

        ResultClassEntity<List<RecursoForestalDetalleDto>> result = new ResultClassEntity<>();

        result.setData(recursoForestalRepository.listarRecursoForestal(idPlanManejo,idRecursoForestal,codCabecera));
        result.setSuccess(true);
        result.setMessage("Lista Recurso Forestal");

        return result;
    }



}
