package pe.gob.serfor.mcsniffs.service;


import java.util.List;

import pe.gob.serfor.mcsniffs.entity.MunicipioPersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface MunicipioPersonaService {

    ResultClassEntity listarMunicipioPersona(MunicipioPersonaEntity obj) throws Exception;


}
