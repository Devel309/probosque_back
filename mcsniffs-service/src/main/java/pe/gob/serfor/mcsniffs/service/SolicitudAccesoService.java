package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudAccesoEntity;

import java.util.List;

public interface SolicitudAccesoService {

    ResultClassEntity RegistrarSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    public SolicitudAccesoEntity ObtenerSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    List<SolicitudAccesoEntity> ListarSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    public ResultClassEntity ActualizarRevisionSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    boolean envioEmail(SolicitudAccesoEntity solicitudAcceso) throws Exception;
    ResultClassEntity listarSolicitudAccesoUsuario (SolicitudAccesoEntity solicitudAcceso);
    boolean enviarEmailRegistroSolicitudAcceso(SolicitudAccesoEntity solicitudAcceso, String token) throws Exception;
}
