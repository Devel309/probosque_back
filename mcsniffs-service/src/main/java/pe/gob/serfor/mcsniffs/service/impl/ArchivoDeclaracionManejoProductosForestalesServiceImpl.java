package pe.gob.serfor.mcsniffs.service.impl;


import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CensoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CensoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SistemaManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Colindancia.ColindanciaPredioDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad.CronogramaActividadDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarDeclaracionManejoProductosForestales.DescargarArchivoDeclaracionManejoProductosForestalesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.CensoForestalListarDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.SistemaManejoForestalDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.AreaBloqueEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolicitudPlantacionForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.repository.ActividadSilviculturalRepository;
import pe.gob.serfor.mcsniffs.repository.AreaSistemaPlantacionRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.ColindanciaPredioRepository;
import pe.gob.serfor.mcsniffs.repository.CronogramaActividesRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalListarRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoProteccionRepository;
import pe.gob.serfor.mcsniffs.repository.SistemaManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralDemaRepository;
import pe.gob.serfor.mcsniffs.service.ArchivoDeclaracionManejoProductosForestalesService;

@Service("ArchivoDeclaracionManejoProductosForestales")
public class ArchivoDeclaracionManejoProductosForestalesServiceImpl implements ArchivoDeclaracionManejoProductosForestalesService {


    @Autowired
    private SolPlantacionForestalRepository repoSolPlanForesRepository;

    @Autowired
    private AreaSistemaPlantacionRepository areaSistemaPlantacionRepository;

    @Autowired
    OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    SistemaManejoForestalRepository sistemaManejoForestalRepository;

    @Autowired
    CronogramaActividesRepository cronogramaActividesRepository;
    
    @Autowired
    CensoForestalDetalleRepository censoForestalDetalleRepository;
    
    @Autowired
    ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    CensoForestalListarRepository censoForestalListarRepository;

    @Autowired
    InformacionGeneralDemaRepository informacionGeneralDemaRepository;

    @Autowired
    ColindanciaPredioRepository ColindanciaPredioRepository;

    @Autowired
    EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 12f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    static Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font subTituloTabla= new Font(Font.HELVETICA, 9f, Font.COURIER);

    Font normal= new Font(Font.HELVETICA, 10f);

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    @Override
    public ByteArrayResource ArchivoDeclaracionManejoProductosForestales(DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception {
        XWPFDocument doc = getDoc("formatoPGMFA.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        /* doc.write(b);
        doc.close(); */

        File archivo = File.createTempFile("SolPlantForestal", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPDFSolPanFor(os, obj);
        os.flush();
        os.close();
        //ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }


    public  void createPDFSolPanFor(FileOutputStream os,DescargarArchivoDeclaracionManejoProductosForestalesDto obj) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);

        try {

            PdfWriter writer = PdfWriter.getInstance(document, os);
            document.open();

            Paragraph preface = new Paragraph();

            //  SUB TITULO
            preface.add(new Paragraph("Anexo 01:  Formato para la Declaración de Manejo para el aprovechamiento de Productos Forestales Diferentes a la Madera", subTitulo));

            //1. INFORMACION GENERAL
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("1.   INFORMACION GENERAL", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            informacionGeneralPredio(document,obj);


            //2. INFORMACION DE LA UNIDAD DE MANEJO FORESTAL
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("2.   INFORMACION DE LA UNIDAD DE MANEJO FORESTAL", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableinfoUniManejoFores = infoUniManejoFores(writer,obj);
            document.add(tableinfoUniManejoFores);

            //3.    ORDENAMIENTO INTERNO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("3.   ORDENAMIENTO INTERNO", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableOrdInterno = ordenmaientoInterno(writer,obj);
            document.add(tableOrdInterno);

            //4.    INFORMACION DE ESPECIES, RECURSOS O SERVIVCIOS
                //4.1
/*             preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 4.	INFORMACION DE ESPECIES, RECURSOS O SERVIVCIOS", subTitulo));
            preface.add(new Paragraph("     4.1  Identificación de las especies a aprovechar", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            preface = new Paragraph();
            PdfPTable tableInfoEspecies = infoEspeciesAprov(writer,idPlanManejo);
            document.add(tableInfoEspecies);
                //4.2
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("     4.2  Inventario", subTituloTabla));
            preface.add(new Paragraph("     4.2.1  Descripción del Inventario", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableDescInventario = descrInventario(writer,idPlanManejo);
            document.add(tableDescInventario);
                //a)
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("     a)	Resultados del inventario por parcela o estrada :", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableResultInvParc = ResultInvParc(writer,idPlanManejo);
            document.add(tableResultInvParc);
                //a)
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("     b)	Resumen de resultado del inventario y proyección de la cantidad a aprovechar anualmente:", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableResultInvAnual= ResultInvAnual(writer,idPlanManejo);
            document.add(tableResultInvAnual); */


            //4 APROVECHAMIENTO DE RECURSOS FORESTABLES MADERABLES
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 4  Aprovechamiento de Recursos Forestales Maderables", subTitulo)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableAprovRFM = tableAprovRFM(writer,obj);
            document.add(tableAprovRFM);

            //5.    APROVECHAMIENTO DE RECURSOS FORESTALES DIFERENTES A LA MADERA
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 5  Aprovechamiento de Recursos Forestales Diferentes a la Madera", subTitulo)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableAprovRFDM = tableAprovRFDM(writer,obj);
            document.add(tableAprovRFDM);

                //5.1
/*                 preface = new Paragraph();
                addEmptyLine(preface, 1);
                preface.add(new Paragraph(" 5.	SISTEMA DE MANEJO, APROVECHAMIENTO Y LABORES SILVICULTURALES", subTitulo));
                preface.add(new Paragraph("     5.1.	Sistema de aprovechamiento", subTituloTabla));
                addEmptyLine(preface, 1);
                document.add(preface);
                preface = new Paragraph();
                PdfPTable tableSisAprov = sistemaAprov(writer,idPlanManejo);
                document.add(tableSisAprov);
                //5.2
                preface = new Paragraph();
                addEmptyLine(preface, 1);
                preface.add(new Paragraph("     5.2.	Labores silviculturales", subTituloTabla));
                addEmptyLine(preface, 1);
                document.add(preface);
                preface = new Paragraph();
                PdfPTable tablelaboresSilviculturales = laboresSilviculturales(writer,idPlanManejo);
                document.add(tablelaboresSilviculturales); */

            //6.    SISTEMA DE MANEJO Y LABORES SILVICULTURALES
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 6.	SISTEMA DE MANEJO Y LABORES SILVICULTURALES", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableManejoLaboresSilvi = manejoLaboresSilvi(writer,obj);
            document.add(tableManejoLaboresSilvi);

            //7.    MEDIDAS DE PROTECCION DE LA UNIDAD DE MANEJO FORESTAL
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 7.	MEDIDAS DE PROTECCION DE LA UNIDAD DE MANEJO FORESTAL", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableMedidasProt = medidasProt(writer,obj);
            document.add(tableMedidasProt);

            //8.    DESCRIPCION DE LAS ACTIVIDADES APROVECHAMIENTO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 8.	DESCRIPCION DE LAS ACTIVIDADES APROVECHAMIENTO", subTitulo));
            preface.add(new Paragraph("     8.1  Aprovechamiento forestal maderable", subTituloTabla)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableAprovMaderable = aprovForestal(writer,obj,"MAD");
            document.add(tableAprovMaderable);
            document.add(new Paragraph("\r\n"));
            preface = new Paragraph();
            preface.add(new Paragraph("     8.2  Aprovechamiento forestal no maderable", subTituloTabla)); 
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableAprovNoMaderable = aprovForestal(writer,obj,"NMAD");
            document.add(tableAprovNoMaderable);

            //9.   IDENTIFICACION DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCION Y MITIGACION
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 9.	IDENTIFICACION DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCION Y MITIGACION", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tableImpactoAmbien = impactosAmbientales(writer,obj);
            document.add(tableImpactoAmbien);

             //10.   CRONOGRAMA DE ACTIVIDADES PARA EL PRIMER PERIODO DE EJECUCION
            Rectangle two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 10.	CRONOGRAMA DE ACTIVIDADES PARA EL PRIMER PERIODO DE EJECUCION", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);
            PdfPTable tablecronogramaActividades = cronogramaActividades(writer,obj);
            document.add(tablecronogramaActividades);


            //9.   ANEXOS DEL FORMATO DE LA DEMA
            //9.1.   DETALLE DEL RESULTADO DEL CENSO FORESTAL MADERABLE
            //11.   IDENTIFICACION DE IMPACTOS AMBIENTALES NEGATIVOS Y ESTABLECIMIENTO DE MEDIDAS DE PREVENCION Y MITIGACION
            Rectangle vert = new Rectangle(595.0F,842.0F);
            document.setPageSize(vert);
            document.setMargins(60, 60, 40, 40);
            document.newPage();
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph(" 11.	ANEXOS DEL FORMATO DE LA DEMA", subTitulo));
            addEmptyLine(preface, 1);
            document.add(preface);

            //11.   ANEXOS DEL FORMATO DE LA DEMA
            //11.1.   DETALLE DEL RESULTADO DEL CENSO FORESTAL MADERABLE
            preface = new Paragraph();
            preface.add(new Paragraph(" 11.1	DETALLE DEL RESULTADO DEL CENSO FORESTAL DE LAS ESPECIES MADERABLES DE INTERÉS", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);
             
            PdfPTable tableDetalleResulMaderable = detalleResultadoMaderable(writer,obj);
            document.add(tableDetalleResulMaderable);
            document.add(new Paragraph("\r\n"));

            //11.2.   DETALLE DE LOS VOLÚMENES COMERCIALES DIFERENTES A LA MADERA
            preface = new Paragraph();
            preface.add(new Paragraph("11.2	DETALLE DE LOS VOLÚMENES COMERCIALES DIFERENTES A LA MADERA", subTituloTabla));
            addEmptyLine(preface, 1);
            document.add(preface);

            PdfPTable tableDetalleVolNoMaderable = detalleVolumenNoMaderable(writer,obj);
            document.add(tableDetalleVolNoMaderable);


            //********************************************************************************************/

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable informacionGeneral(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//
        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {0.20f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(100);
        setearBodyDataSborder("1.1  ",1,table);
        setearBodyDataSborder("N° de título habilitante (en caso corresponda)",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.2  ",1,table);
        setearBodyDataSborder("Nombre del titular o razón social",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.3  ",1,table);
        setearBodyDataSborder("1.3	Nombre del representante (en caso de comunidad Jefe/Presidente):",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.4  ",1,table);
        setearBodyDataSborder("Número de D.N.I. o RUC",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.5  ",1,table);
        setearBodyDataSborder("1.5	Domicilio legal del titular: (indicar Calle/Av./Jr./Psje. y el número. El departamento, provincia y distrito)",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.6  ",1,table);
        setearBodyDataSborder("1.6	Domicilio legal del representante legal: (indicar Calle/Av./Jr./Psje. y el número. El departamento, provincia y distrito)",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.7  ",1,table);
        setearBodyDataSborder("Número de teléfono/celular",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.8  ",1,table);
        setearBodyDataSborder("Correo electrónico",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.9  ",1,table);
        setearBodyDataSborder("Fecha de elaboración de la DEMA",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.10  ",1,table);
        setearBodyDataSborder("Vigencia de la DEMA (años)",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("  ",1,table);
        setearBodyDataSborder("•	Fecha de inicio (mes/año):",1,table);
        setearBodyData("dato 1",1,table);
        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("  ",1,table);
        setearBodyDataSborder("•	Fecha de culminación (mes/años):",1,table);
        setearBodyData("dato 1",1,table);
        
        return table;
    }

    public void informacionGeneralPredio(Document document,DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception {
        //------DATA-------//

        //----------------//
        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {0.20f, 1.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(100);
        setearBodyDataSborder("1.1  ",1,table);
        setearBodyDataSborder("Nombre del propietario del predio",1,table);

        String nombrePropietario = "";
        String departamento = "";
        String provincia = "";
        String distrito = "";
        String cuenca = "";
        InformacionGeneralDEMAEntity c = new InformacionGeneralDEMAEntity();
        c.setIdPlanManejo(obj.getIdPlanManejo());

        ResultEntity<InformacionGeneralDEMAEntity> result = informacionGeneralDemaRepository.listarInformacionGeneralDema(c);
        if(result.getData() != null && result.getData().size() > 0){
            c = result.getData().get(0);
            nombrePropietario = c.getNombreElaboraDema() + " " + c.getApellidoPaternoElaboraDema() + " " + c.getApellidoMaternoElaboraDema();
            departamento = c.getDepartamento();
            provincia = c.getProvincia();
            distrito = c.getDistrito();
            cuenca = c.getCuenca();
        }
        setearBodyData(nombrePropietario,1,table);

        setearBodyDataSborder("",3,table);
        setearBodyDataSborder("1.2  ",1,table);
        setearBodyDataSborder("Ubicación del predio",1,table);
        setearBodyDataSborder("",1,table);
        setearBodyDataSborder("",1,table);
        setearBodyDataSborder("Departamento",1,table);
        setearBodyData(departamento,1,table);
        setearBodyDataSborder("",1,table);
        setearBodyDataSborder("Provincia",1,table);
        setearBodyData(provincia,1,table);
        setearBodyDataSborder("",1,table);
        setearBodyDataSborder("Distrito",1,table);
        setearBodyData(distrito,1,table);
        setearBodyDataSborder("",1,table);
        setearBodyDataSborder("Caserío, centro poblado o sector",1,table);
        setearBodyData(cuenca,1,table);
        setearBodyDataSborder("",3,table);

        document.add(table);

        Paragraph preface = new Paragraph();

        addEmptyLine(preface, 1);
        preface.add(new Paragraph("1.3  Puntos de ubicación en coordenadas UTM del predio (Zona..., Datum .., Horizontal...)", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);


        PdfPTable tableUbicacionUTM = new PdfPTable(4);
        float[] medidaCeldaUbicacionUTM = {0.50f, 0.80f, 0.80f, 0.80f};
        try {
            tableUbicacionUTM.setWidths(medidaCeldaUbicacionUTM);
        } catch (DocumentException e) {}
        tableUbicacionUTM.setWidthPercentage(100);

        setearBodyStatic("Punto",1,tableUbicacionUTM);
        setearBodyStatic("Este (E)",1,tableUbicacionUTM);
        setearBodyStatic("Norte (N)",1,tableUbicacionUTM);
        setearBodyStatic("Referencia",1,tableUbicacionUTM);

        List<InfBasicaAereaDetalleDto> lst = informacionBasicaRepository.listarInfBasicaAerea("DEMAC", obj.getIdPlanManejo(),"DEMAC1_SS3");

        if(lst != null && lst.size() > 0) {
            for (InfBasicaAereaDetalleDto element : lst) {
                setearBodyData(element.getPuntoVertice() == null ? "" : element.getPuntoVertice(), 1, tableUbicacionUTM);
                setearBodyData(element.getCoordenadaEste() == null ? "" : element.getCoordenadaEste().toString(), 1, tableUbicacionUTM);
                setearBodyData(element.getCoordenadaNorte() == null ? "" : element.getCoordenadaNorte().toString(), 1, tableUbicacionUTM);
                setearBodyData(element.getReferencia() == null ? "" : element.getReferencia(), 1, tableUbicacionUTM);
            }
        }

        document.add(tableUbicacionUTM);

        preface = new Paragraph();

        addEmptyLine(preface, 1);
        preface.add(new Paragraph("1.4  Colindancia y límites del predio ", subTituloTabla));
        addEmptyLine(preface, 1);
        document.add(preface);

        PdfPTable tableLimitePredio = new PdfPTable(3);
        float[] medidaCeldaLimitePredio = {0.50f, 1.20f, 1.20f};
        try {
            tableLimitePredio.setWidths(medidaCeldaLimitePredio);
        } catch (DocumentException e) {}
        tableLimitePredio.setWidthPercentage(100);

        setearBodyStatic("",1,tableLimitePredio);
        setearBodyStatic("Colindantes (precisar el nombre de las personas propietarias de predios con las que colinda)",1,tableLimitePredio);
        setearBodyStatic("Limites (precisar el nombre del río, quebrada, camino u otros con los que limita)",1,tableLimitePredio);

        ColindanciaPredioDto dto = new ColindanciaPredioDto();
        dto.setIdPlanManejo(obj.getIdPlanManejo());
        List<ColindanciaPredioDto> lstColindancia = ColindanciaPredioRepository.listarColindanciaPredio(dto);

        if(lstColindancia != null && lstColindancia.size() > 0) {
            for (ColindanciaPredioDto element : lstColindancia) {
                setearBodyData(element.getTipoColindancia() == null ? "" : element.getTipoColindancia(), 1, tableLimitePredio);
                setearBodyData(element.getDescripcionColindante() == null ? "" : element.getDescripcionColindante().toString(), 1, tableLimitePredio);
                setearBodyData(element.getDescripcionLimite() == null ? "" : element.getDescripcionLimite().toString(), 1, tableLimitePredio);
            }
        }

        document.add(tableLimitePredio);
    }
    
    public  PdfPTable ordenmaientoInterno(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        List<OrdenamientoProteccionDetalleEntity> listaOrd = new ArrayList<>();
        OrdenamientoProteccionEntity param = new OrdenamientoProteccionEntity();
        param.setIdPlanManejo(obj.getIdPlanManejo());
        param.setCodTipoOrdenamiento("DEMAC");
        ResultEntity resul = ordenamientoProteccionRepository.ListarOrdenamientoInterno(param);
        if(resul!=null && resul.getIsSuccess()){
            listaOrd = ((OrdenamientoProteccionEntity) resul.getData().get(0)).getListOrdenamientoProteccionDet();
        }
        //----------------//

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(95);
        setearBodyStatic("Denominación o espacios",1,table);
        setearBodyStatic("Superficie aprox. (ha)",1,table);

        for (OrdenamientoProteccionDetalleEntity element : listaOrd) {
            setearBodyData(element.getCategoria()==null?"":element.getCategoria(),1,table);
            setearBodyData(element.getAreaHA()==null?"":element.getAreaHA().toString(),1,table);
        }
        setearBodyStatic("TOTAL",1,table);
        setearBodyData(sumaAreaHa(listaOrd),1,table);
        
        return table;
    }

    public  PdfPTable infoEspeciesAprov(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//
        PdfPTable table = new PdfPTable(4);
        float[] medidaCeldas = {0.25f, 1.25f, 1.25f,1.25f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearRowsColspanStatic("N°",2,1,table);
        setearRowsColspanStatic("Especies",1,2,table);
        setearRowsColspanStatic("Productos",2,1,table);

        setearBodyData("Nombre científico",1,table);
        setearBodyData("Nombre común",1,table);

        setearBodyData(" 1",1,table);
        setearBodyData(" 2",1,table);
        setearBodyData("3 ",1,table);
        setearBodyData(" 4",1,table);
        
        
        return table;
    }

    public  PdfPTable descrInventario(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(95);
        setearBodyStatic("Fecha de realización del inventario:",1,table);
        setearBodyData("(Consignar el mes y año)",1,table);

        setearRowsColspanData("--------------------------", 1, 2, table);
        
        return table;
    }

    public  PdfPTable tableAprovRFM(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Especie",1,2,table);
        setearRowsColspanStatic("N° de árboles censados",2,1,table);
        setearRowsColspanStatic("Árboles aprovechables",1,2,table);
        setearRowsColspanStatic("N° de árboles semilleros",2,1,table);
        setearRowsColspanStatic("Nombre común",1,1,table);
        setearRowsColspanStatic("Nombre científico",1,1,table);
        setearRowsColspanStatic("N° de árboles",1,1,table);
        setearRowsColspanStatic("Volumen comercial m3(r)",1,1,table);
 
  
        CensoForestalDetalleEntity e = new CensoForestalDetalleEntity();
        e.setIdPlanManejo(obj.getIdPlanManejo());
        ResultClassEntity result = censoForestalDetalleRepository.listarRecursosMaderablesCensoForestalDetalle(e);

        if (result!=null && result.getSuccess() && result.getData()!=null) {
            
            CensoForestalEntity data = (CensoForestalEntity) result.getData();
            
            if (data.getListCensoForestalDetalle()!=null && !data.getListCensoForestalDetalle().isEmpty()) {               

                for (CensoForestalDetalleEntity element : data.getListCensoForestalDetalle()) {
                    setearBodyData(element.getNombreComun()==null ? "" : element.getNombreComun(),1,table);
                    setearBodyData(element.getNombreCientifico()==null ? "" : element.getNombreCientifico(),1,table);
                    setearBodyData(element.getNroArbolesCensados()==null ? "" : element.getNroArbolesCensados().toString(),1,table);
                    setearBodyData(element.getNroArboles()==null ? "" : element.getNroArboles().toString(),1,table);
                    setearBodyData(element.getVolumen()==null ? "" : element.getVolumen().toString(),1,table);
                    setearBodyData(element.getNroArbolesSemilleros()==null ? "" : element.getNroArbolesSemilleros().toString(),1,table);
                }

            }
        }
        return table;
    }

    public  PdfPTable tableAprovRFDM(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//

        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Especie",1,2,table);
        setearRowsColspanStatic("N° de individuos",2,1,table);
        setearRowsColspanStatic("Área productiva(m2)",2,1,table);
        setearRowsColspanStatic("Productos a extraer (nombre)",2,1,table);
        setearRowsColspanStatic("Producción aprovechable",1,2,table);

        setearRowsColspanStatic("Nombre comun",1,1,table);
        setearRowsColspanStatic("Nombre científico",1,1,table);
        setearRowsColspanStatic("N° de árboles",1,1,table);
        setearRowsColspanStatic("Volumen comercial m3(r)",1,1,table);
 

        CensoForestalDetalleEntity e = new CensoForestalDetalleEntity();
        e.setIdPlanManejo(obj.getIdPlanManejo());
        ResultClassEntity result = censoForestalDetalleRepository.listarRecursosDiferentesCensoForestalDetalle(e);

        if (result!=null && result.getSuccess() && result.getData()!=null) {
            
            List<CensoForestalDetalleEntity> data = (List<CensoForestalDetalleEntity>) result.getData();
            
            for (CensoForestalDetalleEntity element : data) {
                setearBodyData(element.getNombreComun()==null ? "" : element.getNombreComun(),1,table);
                setearBodyData(element.getNombreCientifico()==null ? "" : element.getNombreCientifico(),1,table);
                setearBodyData(element.getNroIndividuo()==null ? "" : element.getNroIndividuo().toString(),1,table);
                setearBodyData("",1,table);
                setearBodyData(element.getProductoExtraer()==null ? "" : element.getProductoExtraer(),1,table);
                setearBodyData(element.getUnidadMedida()==null ? "" : element.getUnidadMedida(),1,table);
                setearBodyData(element.getCantidadProducto()==null ? "" : element.getCantidadProducto().toString(),1,table);
            }
        }
        return table;
    }
    public  PdfPTable ResultInvParc(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(95);

        setearBodyStatic("Especie:",3,table);
        setearBodyData("(Consignar nombre de la especie)",4,table);

        setearRowsColspanStatic("N° Parcela o estrada",2,1,table);
        setearRowsColspanStatic("N° individuos",1,2,table);
        setearRowsColspanStatic("Producto a obtener",2,1,table);
        setearRowsColspanStatic("Producción",1,3,table);
        setearBodyStatic("Aprov.",1,table);
        setearBodyStatic("Sem.",1,table);
        setearBodyStatic("Unidad de medida",1,table);
        setearBodyStatic("Total.",1,table);
        setearBodyStatic("Aprov.",1,table);

        setearBodyData(" 1",1,table);
        setearBodyData(" 2",1,table);
        setearBodyData("3 ",1,table);
        setearBodyData(" 4",1,table);
        setearBodyData(" 5",1,table);
        setearBodyData(" 6",1,table);
        setearBodyData("7",1,table);

        setearBodyData("TOTAL",6,table);
        setearBodyData("---",1,table);

        return table;
    }

    public  PdfPTable ResultInvAnual(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Especie:",2,1,table);
        setearRowsColspanStatic("N° parcelas o estradas",2,1,table);
        setearRowsColspanStatic("N° de individuos",1,3,table);
        setearRowsColspanStatic("Estimación de la producción",1,2,table);

        setearBodyStatic("Total",1,table);
        setearBodyStatic("Aprov.",1,table);
        setearBodyStatic("Sem",1,table);
        setearBodyStatic("Unidad de medida",1,table);
        setearBodyStatic("Cantidad",1,table);

        setearBodyData(" 1",1,table);
        setearBodyData(" 2",1,table);
        setearBodyData("3 ",1,table);
        setearBodyData(" 4",1,table);
        setearBodyData(" 5",1,table);
        setearBodyData(" 6",1,table);
        setearBodyData("7",1,table);
        
        return table;
    }

    public  PdfPTable sistemaAprov(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(95);
        setearBodyStatic("Aspecto",1,table);
        setearBodyStatic("Breve descripción",1,table);

        setearBodyData("TOTAL",1,table);
        setearBodyData("Dato1",1,table);

        setearBodyData("TOTAL",1,table);
        setearBodyData("Dato2",1,table);
        
        return table;
    }

    public  PdfPTable laboresSilviculturales(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        
        //----------------//

        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {1.20f, 0.40f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);
        setearBodyStatic("Labores",1,table);
        setearBodyStatic("Marcar",1,table);
        setearBodyStatic("Descripción",1,table);

        setearBodyData("Identificación de semilleros",1,table);
        setearBodyData("",1,table);
        setearBodyData("",1,table);

        setearBodyData("Limpieza de sotobosque",1,table);
        setearBodyData("",1,table);
        setearBodyData("",1,table);
        
        return table;
    }

    public  PdfPTable medidasProt(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        ResultClassEntity<SistemaManejoForestalDto> data = sistemaManejoForestalRepository.obtener(obj.getIdPlanManejo(), obj.getCodigoProceso());
        List<SistemaManejoForestalDetalleEntity> listaActividades = new ArrayList<>();

        if(data != null && data.getSuccess()){
            listaActividades = (List<SistemaManejoForestalDetalleEntity>) data.getData().getDetalle();
        }
        //----------------//

        PdfPTable table = new PdfPTable(2);
        float[] medidaCeldas = {0.80f, 1.20f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}

        table.setWidthPercentage(95);
        setearBodyStatic("Medidas",1,table);
        setearBodyStatic("Descripción",1,table);

        for (SistemaManejoForestalDetalleEntity e : listaActividades) {
            setearBodyData(e.getActividades()==null?"":e.getActividades(),1,table);
            setearBodyData(e.getDescripcionSistema()==null?"":e.getDescripcionSistema(),1,table);
        }

        return table;
    }

    public  PdfPTable impactosAmbientales(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        EvaluacionAmbientalAprovechamientoEntity param = new EvaluacionAmbientalAprovechamientoEntity();
        param.setIdPlanManejo(obj.getIdPlanManejo());
        param.setTipoAprovechamiento(obj.getCodigo());
        List<EvaluacionAmbientalAprovechamientoEntity> result = evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(param);
        
        //----------------//

        PdfPTable table = new PdfPTable(3);
        float[] medidaCeldas = {0.80f, 0.80f, 1.40f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearBodyStatic("Actividades que generan impacto",1,table);
        setearBodyStatic("Descripción del impacto",1,table);
        setearBodyStatic("Medidas de prevención y/o mitigación, incluido el manejo de residuos sólidos",1,table);

        if(result!=null && !result.isEmpty()){
            for (EvaluacionAmbientalAprovechamientoEntity e : result) {
                setearBodyData(e.getNombreAprovechamiento()==null?"":e.getNombreAprovechamiento(),1,table);
                setearBodyData(e.getImpacto()==null?"":e.getImpacto(),1,table);

                if(e.getMedidasControl()!=null){
                    if (e.getMedidasControl().contains("-")) {
                        String[] lines = e.getMedidasControl().split("-");
                        Paragraph preface = new Paragraph();
                        for (String line : lines) {
                            if(!line.isEmpty()) preface.add(new Paragraph(("- "+line+"\n"), contenido));
                        }
                        setearBodyDataPreface(preface,1,table);
                    } else setearBodyData(("- "+e.getMedidasControl()),1,table);

                } else setearBodyData(" ",1,table);
            }
        }
        
        return table;
    }

    public  PdfPTable detalleResultadoMaderable(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception {
        //------DATA-------//

        //----------------//

        PdfPTable table = new PdfPTable(8);
        float[] medidaCeldas = {0.80f, 0.40f, 0.40f, 0.40f, 0.40f, 0.80f, 0.50f, 0.50f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Especie",2,1,table);
        setearRowsColspanStatic("Nº Árbol",2,1,table);
        setearRowsColspanStatic("DAP (cm)",2,1,table);
        setearRowsColspanStatic("Altura Comercial (m)",2,1,table);
        setearRowsColspanStatic("Volumen (m3)",2,1,table);
        setearRowsColspanStatic("Tipo de àrbol (A / 5)",2,1,table);
        setearRowsColspanStatic("Ubicación en coordenadas UTM",1,2,table);
        setearBodyStatic("Este",1,table);
        setearBodyStatic("Norte",1,table);

        CensoForestalDetalleEntity e = new CensoForestalDetalleEntity();
        e.setIdPlanManejo(obj.getIdPlanManejo());
        ResultClassEntity result = censoForestalDetalleRepository.listarCensoForestalDetalleAnexo(e);

        if (result!=null && result.getSuccess() && result.getData()!=null) {

            List<CensoForestalDetalleEntity> data = (List<CensoForestalDetalleEntity>) result.getData();
            List<CensoForestalDetalleEntity> det;
            if(data != null && data.size() > 0) {
                List<String> lista = data.stream().map(CensoForestalDetalleEntity::getNombreComun).distinct().collect(Collectors.toList());
                Integer filas = 1;

                for (String especie : lista) {
                    det = data.stream().filter(p->p.getNombreComun().equals(especie)).collect(Collectors.toList());
                    filas = det.size();

                    setearRowsColspanData(especie == null ? "" : especie, filas, 1, table);

                    for (CensoForestalDetalleEntity element : det) {
                        setearBodyData(element.getNroArboles() == null ? "" : element.getNroArboles().toString(), 1, table);
                        setearBodyData(element.getStrDap() == null ? "" : element.getStrDap(), 1, table);
                        setearBodyData(element.getStrAlturaComercial() == null ? "" : element.getStrAlturaComercial(), 1, table);
                        setearBodyData(element.getStrVolumen() == null ? "" : element.getStrVolumen(), 1, table);
                        setearBodyData(element.getTipoArbol() == null ? "" : element.getTipoArbol(), 1, table);
                        setearBodyData(element.getCoorEste() == null ? "" : element.getCoorEste(), 1, table);
                        setearBodyData(element.getCoorNorte() == null ? "" : element.getCoorNorte(), 1, table);
                    }
                }

            }
        }
        return table;
    }

    public  PdfPTable detalleVolumenNoMaderable(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception {
        //------DATA-------//

        //----------------//

        PdfPTable table = new PdfPTable(9);
        float[] medidaCeldas = {0.50f, 0.80f, 0.80f, 0.50f, 0.50f, 0.80f, 0.80f, 0.80f, 0.80f};
        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}
        table.setWidthPercentage(95);

        setearRowsColspanStatic("Nº Individuo\n(Código)",2,1,table);
        setearRowsColspanStatic("Nombre  común",2,1,table);
        setearRowsColspanStatic("Nombre científico",2,1,table);
        setearRowsColspanStatic("Coordenadas UTM",1,2,table);
        setearRowsColspanStatic("Nº de árboles / área \n productiva (m2)",2,1,table);
        setearRowsColspanStatic("Producto",2,1,table);
        setearRowsColspanStatic("Produc. Aprovec,\n(lt./arbl/kg./m2)",2,1,table);
        setearRowsColspanStatic("Observaciones",2,1,table);

        setearBodyStatic("Este",1,table);
        setearBodyStatic("Norte",1,table);


        CensoForestalListarDto e = new CensoForestalListarDto();
        e.setIdPlanManejo(obj.getIdPlanManejo());
        List<CensoForestalListarDto> data = censoForestalListarRepository.censoForestalListar(e);

        for (CensoForestalListarDto element : data) {
            setearBodyData(element.getIdPlanManejo()==null ? "" : element.getIdPlanManejo().toString(),1,table);
            setearBodyData(element.getNombreComun()==null ? "" : element.getNombreComun().toString(),1,table);
            setearBodyData(element.getNombreCientifico()==null ? "" : element.getNombreCientifico(),1,table);
            setearBodyData(element.getEsteUTM()==null ? "" : element.getEsteUTM(),1,table);
            setearBodyData(element.getNorteUTM()==null ? "" : element.getNorteUTM(),1,table);
            setearBodyData(element.getNroArbolesArea()==null ? "" : element.getNroArbolesArea().toString(),1,table);
            setearBodyData(element.getProducto()==null ? "" : element.getProducto(),1,table);
            setearBodyData(element.getProductoAprov()==null ? "" : element.getProductoAprov(),1,table);
            setearBodyData(element.getObservaciones()==null ? "" : element.getObservaciones(),1,table);
        }

        return table;
    }

    public  PdfPTable cronogramaActividades(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        List<CronogramaActividadDto> listaActividades = new ArrayList<>();
        List<CronogramaActividadDto> listaMaderables = new ArrayList<>();
        List<CronogramaActividadDto> listaNoMaderables = new ArrayList<>();

        CronogramaActividadDto param = new CronogramaActividadDto();
        param.setIdPlanManejo(obj.getIdPlanManejo());
        param.setCodigoProceso(obj.getCodigo());  // NO FIJO {codigoProceso}
        ResultClassEntity result = cronogramaActividesRepository.ListarPorFiltroCronogramaActividad(param);
        if(result != null && result.getSuccess() && result.getData() != null){
            listaActividades = (List<CronogramaActividadDto>) result.getData();
            if(!listaActividades.isEmpty()){
                listaMaderables = listaActividades.stream().filter(c -> c.getGrupo().equals("1")).collect(Collectors.toList());
                listaNoMaderables = listaActividades.stream().filter(c -> c.getGrupo().equals("2")).collect(Collectors.toList());
            }
        }

        Integer numAnio = listaActividades.get(0).getCantidadAnio()==null? 3 : listaActividades.get(0).getCantidadAnio();
        numAnio = numAnio < 3 ? 3 : numAnio;
        //----------------//
        Integer meses = 12;
        Integer numColumns = (numAnio * meses)+1;
        PdfPTable table = new PdfPTable(numColumns);
    
        float[] medidaCeldas = new float[numColumns];
        medidaCeldas[0] = 4f;
        for (int i = 1; i < numColumns; i++) {
            medidaCeldas[i] = 0.50f;
        }

        try {
            table.setWidths(medidaCeldas);
        } catch (DocumentException e) {}

        table.setWidthPercentage(100);

        setearRowsColspanStatic("Actividad",3,1,table);
        for (int i = 1; i <= numAnio; i++) {
            setearRowsColspanStatic(("Año "+i),1,meses,table);
        }
        for (int i = 1; i <= numAnio; i++) {
            setearRowsColspanStatic("Meses",1,meses,table);
        }
        Integer cont = 1;
        for (int i = 1; i <= meses*numAnio; i++) {
            if(cont==13) cont = 1;
            setearBodyStaticLetraPequenio((""+cont),1,table);
            cont++;
        }
        
        setearBodyStatic("Aprovechamiento Maderables", numColumns, table);

        for (CronogramaActividadDto e : listaMaderables) {
            setearBodyData(e.getActividad()==null?"":e.getActividad(),1, table);

            for (int j = 1; j <= numAnio; j++) {
                for (int k = 1; k <= meses; k++) {
                    Boolean completado = false;
                    for (CronogramaActividadDetalleEntity det : e.getDetalle()) {
                        if(det.getAnio() == j && det.getMes()==k) {
                            setearBodyData("x", 1, table);
                            completado=true;
                            break;
                        }
                    }
                    if(!completado) setearBodyData("", 1, table);
                }
            }  
        }

        setearBodyStatic("Aprovechamiento de Productos Forestales diferentes a la madera", numColumns, table);
        
        for (CronogramaActividadDto e : listaNoMaderables) {
            setearBodyData(e.getActividad()==null?"":e.getActividad(),1, table);

            for (int j = 1; j <= numAnio; j++) {
                for (int k = 1; k <= meses; k++) {
                    Boolean completado = false;
                    for (CronogramaActividadDetalleEntity det : e.getDetalle()) {
                        if(det.getAnio() == j && det.getMes()==k) {
                            setearBodyData("x", 1, table);
                            completado=true;
                            break;
                        }
                    }
                    if(!completado) setearBodyData("", 1, table);
                }
            }     
        }

        return table;
    }

    public  PdfPTable manejoLaboresSilvi(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        List<ManejoBosqueEntity> lista = manejoBosqueRepository.ListarManejoBosque(obj.getIdPlanManejo(), obj.getCodigo(), obj.getSubCodigoGeneral(), null);
        //----DEMAP - DEMAP6_SS1
        List<ManejoBosqueDetalleEntity> listDet = new ArrayList<>();

        if(lista!=null && lista.get(0).getListManejoBosqueDetalle() != null && !lista.get(0).getListManejoBosqueDetalle().isEmpty()){
            listDet = (List<ManejoBosqueDetalleEntity>) lista.get(0).getListManejoBosqueDetalle();
        }
        //----------------//

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(95);

        setearBodyStatic("Actividades",1,table);
        setearBodyStatic("Descripción",1,table);
        setearBodyStatic("Equipos",1,table);
        setearBodyStatic("Insumos",1,table);
        setearBodyStatic("Personal",1,table);
        setearBodyStatic("Observaciones",1,table);

        for (ManejoBosqueDetalleEntity e : listDet) {
            setearBodyData(e.getTratamientoSilvicultural()==null?"":e.getTratamientoSilvicultural(), 1, table);
            setearBodyData(e.getDescripcionTratamiento()==null?"":e.getDescripcionTratamiento(), 1, table);
            setearBodyData(e.getEquipo()==null?"":e.getEquipo(), 1, table);
            setearBodyData(e.getInsumo()==null?"":e.getInsumo(), 1, table);
            setearBodyData(e.getPersonal()==null?"":e.getPersonal(), 1, table);
            setearBodyData(e.getObservacion()==null?"":e.getObservacion(), 1, table);
        }
        
        return table;
    }

    public  PdfPTable aprovForestal(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj, String cod) throws Exception { 
        //------DATA-------//
        ResultClassEntity<ActividadSilviculturalDto> result = actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(obj.getIdPlanManejo(), obj.getCodigo());
        //----DEMAC
        List<ActividadSilviculturalDetalleEntity> listMaderable = new ArrayList<>();
        List<ActividadSilviculturalDetalleEntity> listNoMaderable = new ArrayList<>();
        List<ActividadSilviculturalDetalleEntity> listAux = new ArrayList<>();

        if(result!=null && result.getSuccess() && result.getData()!=null){
            List<ActividadSilviculturalDetalleEntity> lista = result.getData().getDetalle();
            if(lista!=null && !lista.isEmpty()){
                listMaderable = lista.stream().filter(c -> c.getObservacionDetalle().equals("MAD")).collect(Collectors.toList());
                listNoMaderable = lista.stream().filter(c -> c.getObservacionDetalle().equals("NMAD")).collect(Collectors.toList());
            }
        }
        //----------------//
        if(cod.equals("MAD")){
            listAux = listMaderable;
        }else { listAux = listNoMaderable; }

        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(95);

        setearBodyStatic("Actividad",1,table);
        setearBodyStatic("Descripción del sistema a utilizar",1,table);
        setearBodyStatic("Equipos e insumos a utilizar",1,table);
        setearBodyStatic("Personal requerido",1,table);
        setearBodyStatic("Observaciones",1,table);

        for (ActividadSilviculturalDetalleEntity e : listAux) {
            setearBodyData(e.getActividad()==null?"":e.getActividad(), 1, table);
            setearBodyData(e.getDescripcionDetalle()==null?"":e.getDescripcionDetalle(), 1, table);
            setearBodyData(e.getEquipo()==null?"":e.getEquipo(), 1, table);
            setearBodyData(e.getPersonal()==null?"":e.getPersonal(), 1, table);
            setearBodyData(e.getDetalle()==null?"":e.getDetalle(), 1, table);
        }
        return table;
    }

    public  PdfPTable infoUniManejoFores(PdfWriter writer, DescargarArchivoDeclaracionManejoProductosForestalesDto obj) throws Exception { 
        //------DATA-------//
        List<InfBasicaAereaDetalleDto> lista = informacionBasicaRepository.listarInfBasicaAerea(obj.getCodigo(), obj.getIdPlanManejo(), obj.getCodCabecera());
        //DEMAC - DEMAC2_SS1

        //----------------//
        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(95);

        setearBodyStatic("Punto", 1, table);
        setearBodyStatic("Este (E)", 1, table);
        setearBodyStatic("Norte (N)", 1, table);
        setearBodyStatic("Referencia", 1, table);

        if(lista!=null && !lista.isEmpty()){
            for (InfBasicaAereaDetalleDto e : lista) {
                setearBodyData(e.getPuntoVertice()==null?"":e.getPuntoVertice(),1,table);
                setearBodyData(e.getCoordenadaEste()==null?"":e.getCoordenadaEste().toString(),1,table);
                setearBodyData(e.getCoordenadaNorte()==null?"":e.getCoordenadaNorte().toString(),1,table);
                setearBodyData(e.getReferencia()==null?"":e.getReferencia(),1,table);
            }
        }
        
        return table;
    }


    
//-------------------------------------------------------------------------
    private static void setearCellcabezera(String dato, Integer colum, PdfPTable table){

        Font title = new Font(Font.HELVETICA, 12,Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato,title));
        Color colorHeader = new Color(189 , 214 , 238);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setColspan(colum);
        c.setBackgroundColor(colorHeader);
        c.setPadding(5);
        table.addCell(c);
        table.setHeaderRows(1);
    }
    private static void setearSubTitle(String dato, Integer colum, PdfPTable table){
        Font subTitle = new Font(Font.HELVETICA, 10,Font.BOLD);
        Color colorSubTitle = new Color(242 , 242 , 242);
        PdfPCell c = new PdfPCell(new Phrase(dato,subTitle));
        c.setColspan(colum);
        c.setBackgroundColor(colorSubTitle);
        c.setPadding(5);
        table.addCell(c);
        table.setHeaderRows(1);
    }
    private static void setearBodyStatic(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        table.addCell(c);
    }   
    private static void setearBodyData(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        c.setColspan(colum);
        c.setPadding(5);
        table.addCell(c);
    }
    private static void setearBodyDataPreface(Paragraph preface, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(preface);
        c.setColspan(colum);
        c.setPadding(5);
        table.addCell(c);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void setearRowsColspanStatic(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        table.addCell(c);
    }
    private static void setearRowsColspanData(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        table.addCell(c);
    }
    private static void setearBodyStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        table.addCell(c);
    } 
    private static void setearDataStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        c.setColspan(colum);
        c.setPadding(5);
        table.addCell(c);
    }  
    private static void setearBodyDataSborder(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato,contenido));
        c.setColspan(colum);
        c.setPadding(5);
        c.setBorder(0);
        table.addCell(c);
    }

    private Integer sumaTotalArbolesExistentes (List<SolicitudPlantacionForestalDetalleEntity> listaEsp){
        Integer suma = 0;
        for (SolicitudPlantacionForestalDetalleEntity element : listaEsp) {
            suma += element.getTotalArbolesExistentes();
        }
        return suma;
    }
    public static String sumAreaTotalHa(List<AreaBloqueEntity> data){

        String numero = "";
        BigDecimal suma = new BigDecimal(0);

        for (AreaBloqueEntity e : data) {
            suma = suma.add(e.getArea() == null ? new BigDecimal(0) : e.getArea());
        }
        BigDecimal total = suma.setScale(2, RoundingMode.HALF_UP);

        return numero = total.toString();
    }

    public static String sumaAreaHa(List<OrdenamientoProteccionDetalleEntity> lista){
        String numero="";
        BigDecimal cont = new BigDecimal(0);

        for (OrdenamientoProteccionDetalleEntity e : lista) {
            cont = cont.add(e.getAreaHA() == null ? new BigDecimal(0) : e.getAreaHA());
        }
        BigDecimal total = cont.setScale(2, RoundingMode.HALF_UP);
        return numero = total.toString();
    }


}
