package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudOpinionEntity;

import java.util.List;


public interface SolicitudOpinionService {

    ResultClassEntity ListarSolicitudOpinion(SolicitudOpinionDto param);

    ResultClassEntity RegistrarSolicitudOpinion(SolicitudOpinionDto param) throws Exception;

    ResultClassEntity ActualizarSolicitudOpinion(SolicitudOpinionDto param) throws Exception;

    ResultClassEntity ActualizarSolicitudOpinionArchivo(SolicitudOpinionDto obj) throws Exception;

    ResultClassEntity EliminarSolicitudOpinionArchivo(SolicitudOpinionDto obj)throws Exception;

    ResultClassEntity ObtenerSolicitudOpinion(SolicitudOpinionDto param);

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {CRUD DE LA SOLICITUD OPINION}
     * @param:SolicitudOpinionEntity
     */

    ResultClassEntity RegistrarSolicitudOpinionEvaluacion(List<SolicitudOpinionEntity> list) throws Exception;

    ResultClassEntity<List<SolicitudOpinionEntity>> ListarSolicitudOpinionEvaluacion(Integer idPlanManejo, String codSolicitud, String subCodSolicitud) throws Exception;
    ResultClassEntity EliminarSolicitudOpinionEvaluacion(SolicitudOpinionEntity solicitudOpinionEntity) throws Exception;


}
