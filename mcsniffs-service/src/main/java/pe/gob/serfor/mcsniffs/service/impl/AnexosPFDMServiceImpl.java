package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.AnexosPFDMRepository;
import pe.gob.serfor.mcsniffs.service.AnexosPFDMService;

@Service
public class AnexosPFDMServiceImpl implements AnexosPFDMService{
    /**
     * @autor: JaquelineDB [24-08-2021]
     * @modificado:
     * @descripción: {Repository de los anexos pfdm}
     *
     */
    @Autowired
    AnexosPFDMRepository repo;

    /**
     * @autor: JaquelineDB [24-08-2021]
     * @modificado:
     * @descripción: {Generar el anexo 1}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo1PFDM(AnexosPFDMResquestEntity filtro) {
        // TODO Auto-generated method stub
        return null;
    }
}
