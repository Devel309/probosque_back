package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoNivel1Entity;

public interface TipoProcesoService {
    ResultEntity<TipoProcesoNivel1Entity> listarTipoProceso();
}
