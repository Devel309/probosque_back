package pe.gob.serfor.mcsniffs.service;
import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.InfraccionEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoInfraccionEntity;

public interface EvaluacionCampoService {

    ResultClassEntity registroEvaluacionCampo( EvaluacionCampoEntity request);
    ResultClassEntity obtenerEvaluacionCampo( EvaluacionCampoEntity request) ;
    ResultClassEntity obtenerEvaluacionOcular( EvaluacionCampoEntity request) ;
    ResultClassEntity NotificacionEvaluacionCampo(EvaluacionCampoEntity p);
    ResultClassEntity registroEvaluacionCampoArchivo( EvaluacionCampoArchivoEntity request);
    ResultClassEntity eliminarEvaluacionCampoArchivo( EvaluacionCampoArchivoEntity request);
    ResultClassEntity obtenerEvaluacionCampoArchivo( EvaluacionCampoArchivoEntity request);
    ResultClassEntity listarEvaluacionCampo(EvaluacionCampoDto request) ;
    ResultClassEntity listarPorSeccionEvaluacionCampoArchivo(EvaluacionCampoArchivoEntity p);
    ResultClassEntity listarEvaluacionCampoArchivoInspeccion(EvaluacionCampoArchivoEntity p);
}
