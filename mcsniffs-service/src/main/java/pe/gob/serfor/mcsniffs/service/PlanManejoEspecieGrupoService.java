package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEspecieGrupoEntity;


public interface PlanManejoEspecieGrupoService {
    public ResultClassEntity registrarPlanManejoEspecieGrupo(PlanManejoEspecieGrupoEntity obj) throws Exception;
}
