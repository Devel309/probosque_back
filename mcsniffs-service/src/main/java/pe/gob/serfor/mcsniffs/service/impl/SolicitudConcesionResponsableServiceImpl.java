package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionAreaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionResponsableRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionResponsableService;

@Service
public class SolicitudConcesionResponsableServiceImpl implements SolicitudConcesionResponsableService {

    @Autowired
    private SolicitudConcesionResponsableRepository solicitudConcesionResponsableRepository;

    @Override
    public ResultClassEntity obtenerSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) throws Exception{
        return solicitudConcesionResponsableRepository.obtenerSolicitudConcesionResponsable(obj);
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) throws Exception{
        return solicitudConcesionResponsableRepository.registrarSolicitudConcesionResponsable(obj);
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) throws Exception{
        return solicitudConcesionResponsableRepository.actualizarSolicitudConcesionResponsable(obj);
    }


}
