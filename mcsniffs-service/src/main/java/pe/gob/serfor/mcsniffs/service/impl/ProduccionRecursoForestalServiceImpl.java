package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalDto;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ProduccionRecursoForestalRepository;
import pe.gob.serfor.mcsniffs.service.ProduccionRecursoForestalService;

import java.util.List;

@Service("ProduccionRecursoForestalService")
public class ProduccionRecursoForestalServiceImpl implements ProduccionRecursoForestalService {
    private static final Logger log = LogManager.getLogger(ProduccionRecursoForestalServiceImpl.class);

    @Autowired
    private ProduccionRecursoForestalRepository produccionRecursoForestalRepository;

    @Override
    public ResultClassEntity RegistrarProduccionRecursoForestal(List<ProduccionRecursoForestalDto> list) throws Exception {
        return produccionRecursoForestalRepository.RegistrarProduccionRecursoForestal(list);
    }

    @Override
    public ResultClassEntity ListarProduccionRecursoForestal(ProduccionRecursoForestalEntity param) throws Exception {
        return produccionRecursoForestalRepository.ListarProduccionRecursoForestal(param);
    }

    @Override
    public ResultClassEntity EliminarProduccionRecursoForestal(ProduccionRecursoForestalEntity param) throws Exception {
        return produccionRecursoForestalRepository.EliminarProduccionRecursoForestal(param);
    }

    @Override
    public ResultClassEntity EliminarProduccionRecursoForestalEspecie(ProduccionRecursoForestalEspecieEntity param) throws Exception{
        return produccionRecursoForestalRepository.EliminarProduccionRecursoForestalEspecie(param);
    }
}
