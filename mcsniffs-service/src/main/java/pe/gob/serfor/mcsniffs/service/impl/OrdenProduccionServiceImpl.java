package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.OrdenProduccionEntity;
import java.util.List;

import javax.transaction.Transactional;


import pe.gob.serfor.mcsniffs.entity.ProdTerminadoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.OrdenProduccionRepository;
import pe.gob.serfor.mcsniffs.service.OrdenProduccionService;

@Service("OrdenProduccionService")
public class OrdenProduccionServiceImpl implements OrdenProduccionService {

    private static final Logger log = LogManager.getLogger(OrdenProduccionServiceImpl.class);
    @Autowired
    private OrdenProduccionRepository ordenProduccionRepository;



    @Override
    public ResultClassEntity registrarOrdenProduccion(List<OrdenProduccionEntity> list) throws Exception {
        return ordenProduccionRepository.RegistrarOrdenProduccion(list);
    }

    @Override
    public ResultClassEntity listarOrdenProduccion(OrdenProduccionEntity param) throws Exception {
        return ordenProduccionRepository.ListarOrdenProduccion((param));
    }

    @Override
    @Transactional
    public ResultClassEntity registrarOrdenProduccionProductoTerminado(List<ProdTerminadoEntity> items) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                ordenProduccionRepository.registrarOrdenProduccionProductoTerminado(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se sincronizaron correctamente los registros.");
        result.setSuccess(true);
        return result;

    }

    @Override
    public ResultClassEntity listarOrdenProduccionProductoTerminado(ProdTerminadoEntity param)throws Exception {
        return ordenProduccionRepository.listarOrdenProduccionProductoTerminado(param);
    }

    
}
