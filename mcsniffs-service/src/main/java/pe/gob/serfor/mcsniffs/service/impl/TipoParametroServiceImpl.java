package pe.gob.serfor.mcsniffs.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.TipoParametroDto;
import pe.gob.serfor.mcsniffs.repository.TipoParametroRepository;
import pe.gob.serfor.mcsniffs.service.TipoParametroService;

@Service("TipoParametroService")
public class TipoParametroServiceImpl implements TipoParametroService {


    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

  

    @Autowired
    TipoParametroRepository  tipoParametroRepository;

    @Override

    public ResultClassEntity listarTipoParametro(TipoParametroDto obj) throws Exception {
        return tipoParametroRepository.listarTipoParametro(obj);
    }

    public ResultClassEntity actualizarParametro(TipoParametroDto obj) throws Exception {
        return tipoParametroRepository.actualizarParametro(obj);
    }

    }


