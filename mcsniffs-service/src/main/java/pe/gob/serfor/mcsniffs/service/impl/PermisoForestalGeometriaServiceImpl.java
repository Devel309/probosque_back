package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.serfor.mcsniffs.entity.PermisoForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.PermisoForestalGeometriaRepository;
import pe.gob.serfor.mcsniffs.service.PermisoForestalGeometriaService;

import java.util.List;

@Service("servicePermisoForestalGeometria")
public class PermisoForestalGeometriaServiceImpl implements PermisoForestalGeometriaService {
    @Autowired
    private PermisoForestalGeometriaRepository permisoForestalGeometriaRepository;

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Registrar Geometría Permiso Forestal}
     * @param: PermisoForestalGeometriaEntity
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarPermisoForestalGeometria(List<PermisoForestalGeometriaEntity> items) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                permisoForestalGeometriaRepository.registrarPermisoForestalGeometria(item);
                result.setMessage("Se registró correctamente.");
                result.setSuccess(true);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        return result;
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Obtener Geometría Permiso Forestal}
     * @param: PermisoForestalGeometriaEntity
     */
    @Override
    public ResultClassEntity listarPermisoForestalGeometria(PermisoForestalGeometriaEntity item) throws Exception {
        return permisoForestalGeometriaRepository.listarPermisoForestalGeometria(item);
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Obtener Geometría Permiso Forestal archivo y/o capa}
     * @param: PermisoForestalGeometriaEntity
     */
    @Override
    public ResultClassEntity eliminarPermisoForestalGeometriaArchivo(PermisoForestalGeometriaEntity item) throws Exception {
        return permisoForestalGeometriaRepository.eliminarPermisoForestalGeometriaArchivo(item);
    }
}
