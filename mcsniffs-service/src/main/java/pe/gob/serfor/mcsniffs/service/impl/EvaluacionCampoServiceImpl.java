package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoInfraccionEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.InfraccionEntity;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoRespository;
import pe.gob.serfor.mcsniffs.service.EvaluacionCampoService;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
@Service("EvaluacionCampoService")
public class EvaluacionCampoServiceImpl implements EvaluacionCampoService{
    
@Autowired
EvaluacionCampoRespository serEvaCampo;

@Override
public ResultClassEntity registroEvaluacionCampo(EvaluacionCampoEntity request) {
    // TODO Auto-generated method stub
    return serEvaCampo.registroEvaluacionCampo(request);
}

@Override
public ResultClassEntity obtenerEvaluacionOcular(EvaluacionCampoEntity request) {
    // TODO Auto-generated method stub
    return serEvaCampo.obtenerEvaluacionOcular(request);
}

    @Override
    public ResultClassEntity obtenerEvaluacionCampo(EvaluacionCampoEntity request) {
        // TODO Auto-generated method stub
        return serEvaCampo.obtenerEvaluacionCampo(request);
    }


@Override
public ResultClassEntity registroEvaluacionCampoArchivo(
        EvaluacionCampoArchivoEntity request) {
    // TODO Auto-generated method stub
    return serEvaCampo.registroEvaluacionCampoArchivo(request);
}
    @Override
    public ResultClassEntity eliminarEvaluacionCampoArchivo(
            EvaluacionCampoArchivoEntity request) {
        // TODO Auto-generated method stub
        return serEvaCampo.eliminarEvaluacionCampoArchivo(request);
    }
    @Override
    public ResultClassEntity NotificacionEvaluacionCampo(
            EvaluacionCampoEntity request) {
        // TODO Auto-generated method stub




        return serEvaCampo.NotificacionEvaluacionCampo(request);
    }

    @Override
    public ResultClassEntity obtenerEvaluacionCampoArchivo(EvaluacionCampoArchivoEntity request) {
        // TODO Auto-generated method stub
        return serEvaCampo.obtenerEvaluacionCampoArchivo(request);
    }

    @Override
    public ResultClassEntity listarEvaluacionCampo(
        EvaluacionCampoDto request) {
        // TODO Auto-generated method stub
        return serEvaCampo.listarEvaluacionCampo(request);
    }
    @Override
    public ResultClassEntity listarPorSeccionEvaluacionCampoArchivo(
            EvaluacionCampoArchivoEntity request) {
        // TODO Auto-generated method stub
        return serEvaCampo.listarPorSeccionEvaluacionCampoArchivo(request);
    }
    @Override
    public ResultClassEntity listarEvaluacionCampoArchivoInspeccion(
            EvaluacionCampoArchivoEntity request) {
        // TODO Auto-generated method stub
        return serEvaCampo.listarEvaluacionCampoArchivoInspeccion(request);
    }

}
