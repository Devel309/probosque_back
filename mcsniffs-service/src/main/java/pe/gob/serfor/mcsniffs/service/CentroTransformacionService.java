package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.centroTansformacion.CentroTransformacionDto;

public interface CentroTransformacionService {
    ResultClassEntity ListarCentroTransformacion(CentroTransformacionDto dbo) throws Exception;
}
