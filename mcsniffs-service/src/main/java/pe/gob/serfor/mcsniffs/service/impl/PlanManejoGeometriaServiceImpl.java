package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoGeometriaActualizarCatastroDto;
import pe.gob.serfor.mcsniffs.repository.PlanManejoGeometriaRepository;
import pe.gob.serfor.mcsniffs.service.PlanManejoGeometriaService;

@Service("servicePlanManejoGeometria")
public class PlanManejoGeometriaServiceImpl implements PlanManejoGeometriaService {

    @Autowired
    private PlanManejoGeometriaRepository repository;

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Registrar la geometria}
     * @param:List<PlanManejoGeometriaEntity>
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarPlanManejoGeometria(List<PlanManejoGeometriaEntity> items) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                repository.registrarPlanManejoGeometria(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se registraron correctamente.");
        result.setSuccess(true);
        return result;
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Lista las geometrias activas}
     * @param:PlanManejoGeometriaEntity
     */
    @Override
    public ResultClassEntity listarPlanManejoGeometria(PlanManejoGeometriaEntity item) {
        return repository.listarPlanManejoGeometria(item);
    }

    /**
     * @autor: Abner Valdez [17-12-2021]
     * @modificado:
     * @descripción: {Eliminar PlanManejoGeometria archivo y/o capa}
     * @param:idArchivo, idUsuario
     */
    @Override
    public ResultClassEntity eliminarPlanManejoGeometriaArchivo(Integer idArchivo, Integer idUsuario) {
        return repository.eliminarPlanManejoGeometriaArchivo(idArchivo,idUsuario);
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Registrar la geometria}
     * @param:List<PlanManejoGeometriaEntity>
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarCatastroPlanManejoGeometria(PlanManejoGeometriaActualizarCatastroDto dto) throws Exception {
        return repository.actualizarCatastroPlanManejoGeometria(dto);
    }
}
