package pe.gob.serfor.mcsniffs.service;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;

public interface PGMFAbreviadoService {
    
    ResultClassEntity<PGMFAbreviadoObjetivo2Entity> obtenerObjetivo2 (PGMFAbreviadoObjetivo2Entity filtro);
    ResultClassEntity<String> insertarObjetivo2(PGMFAbreviadoObjetivo2Entity params);
    ResultClassEntity<PGMFAbreviadoObjetivo1Entity> pgmfAbreviadoObj1(PGMFAbreviadoObjetivo1Entity param);

    ResultClassEntity<List< ParticipacionCiudadanaEntity>> participacionCiudadana(ParticipacionCiudadanaEntity param);
    ResultClassEntity<List< CapacitacionPGMFAbreviadoEntity>> Capacitacion(CapacitacionPGMFAbreviadoEntity param);
    
    ResultClassEntity<String> PGMFAbreviadoDetalleRegistrar(PGMFAbreviadoDetalleRegistrarEntity param);
    ResultClassEntity PGMFAbreviadoDetalleEliminar(PGMFAbreviadoDetalleEntity param);

    ResultClassEntity<List< MonitoreoAbreviadoEntity>> Monitoreo(MonitoreoAbreviadoEntity param);
    ResultClassEntity<Boolean> MonitoreoActualizar(List< MonitoreoAbreviadoEntity>  param);
    
    ResultClassEntity<List< OrganizacionManejoEntity>> OrganizacionManejo(OrganizacionManejoEntity param);

    ResultClassEntity<ProgramaInversionEntity> ProgramaInversionObtener(ProgramaInversionEntity param); 
    ResultClassEntity<Boolean> ProgramaInversionRegistrar(ProgramaInversionEntity param); 
    ResultClassEntity<Boolean> ProgramaInversionEliminar(ProgramaInversionEntity param); 
    
    ResultClassEntity<Boolean> cronogramaActividadRegistrar (CronogramaActividadEntity filtro);
    ResultClassEntity<Boolean> cronogramaActividadActualizar (CronogramaActividadEntity filtro);
    ResultClassEntity<Boolean> cronogramaActividadEliminar (CronogramaActividadEntity filtro);

    ResultClassEntity<Boolean> cronogramaActividadDetalleRegistar (CronogramaActividadDetalleEntity filtro);
    ResultClassEntity<List<CronogramaActividadDetalleEntity>> cronogramaActividadDetalle_ListarDetalle (CronogramaActividadDetalleEntity filtro);

    ResultClassEntity<String> protecionVigilanciaArchivo (MultipartFile file, OrdenamientoDetalleEntity params) throws Exception; 
    ResultClassEntity<List<OrdenamientoDetalleEntity>> protecionVigilanciaObtener(OrdenamientoDetalleEntity param); 
}
