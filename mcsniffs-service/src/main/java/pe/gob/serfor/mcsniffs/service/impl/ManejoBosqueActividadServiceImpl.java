package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueActividadRepository;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueActividadService;

import java.util.List;

@Service("ManejoBosqueActividadService")
public class ManejoBosqueActividadServiceImpl implements ManejoBosqueActividadService {

    private static final Logger log = LogManager.getLogger(ManejoBosqueActividadServiceImpl.class);
    @Autowired
    private ManejoBosqueActividadRepository moniManejoBosqueActividadRepository;



    @Override
    public ResultClassEntity RegistrarManejoBosqueActividad(List<ManejoBosqueActividadEntity> list) throws Exception {
        return moniManejoBosqueActividadRepository.RegistrarManejoBosqueActividad(list);
    }

    @Override
    public ResultClassEntity ListarManejoBosqueActividad(PlanManejoEntity param) throws Exception {
        return moniManejoBosqueActividadRepository.ListarManejoBosqueActividad((param));
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueActividad(ManejoBosqueActividadEntity param) throws Exception {
        return moniManejoBosqueActividadRepository.EliminarManejoBosqueActividad(param);
    }
}
