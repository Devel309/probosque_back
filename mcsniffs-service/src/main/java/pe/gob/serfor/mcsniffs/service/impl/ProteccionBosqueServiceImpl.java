package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.repository.ProteccionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.ProteccionBosqueService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
     * @autor: Ivan Minaya [09-06-2021]
     * @modificado:
     * @descripción: {Clase de servicio relacionada a la entidad ProteccionBosqueDemarcacion  de la base de datos mcsniffs}
     *
     */
    @Service("ProteccionBosqueService")
    public class ProteccionBosqueServiceImpl implements ProteccionBosqueService {


        private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.service.impl.ProteccionBosqueServiceImpl.class);

        @Autowired
        private ProteccionBosqueRepository proteccionBosqueRespository;

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Configuracion ProteccionBosqueDemarcacion}
     * @param: ProteccionBosqueDemarcacionEntity
     */

    @Override
    public ResultClassEntity ConfiguracionProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity proteccionBosqueDemarcacion)  throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - ConfiguracionProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        try {
            response=   proteccionBosqueRespository.ConfiguracionProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -ConfiguracionProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Registrar ProteccionBosqueDemarcacion}
     * @param: ProteccionBosqueDemarcacionEntity
     */
    @Override
    public ResultClassEntity RegistrarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity> proteccionBosqueDemarcacion)  throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - RegistrarBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        try {
            response=   proteccionBosqueRespository.RegistrarProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -RegistrarBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Actualizar ProteccionBosqueDemarcacion}
     * @param: ProteccionBosqueDemarcacionEntity
     */
    @Override
    public ResultClassEntity ActualizarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity> proteccionBosqueDemarcacion)  throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - ActualizarProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        try {
            response=   proteccionBosqueRespository.ActualizarProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -ActualizarProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Obtener ProteccionBosqueDemarcacion}
     * @param: ProteccionBosqueDemarcacionEntity
     */
    @Override
    public ResultClassEntity ObtenerProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity proteccionBosqueDemarcacion)  throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - ObtenerProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        try {
            response=   proteccionBosqueRespository.ObtenerProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -ObtenerProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Registrar ProteccionBosqueGestionAmbiental}
     * @param: List<ProteccionBosqueGestionAmbientalEntity>
     */
    @Override
    public  ResultClassEntity RegistrarProteccionBosqueAmbiental(List<ProteccionBosqueGestionAmbientalEntity> list) throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - RegistrarProteccionBosqueAmbiental",list.toString());
        try {
            response=   proteccionBosqueRespository.RegistrarProteccionBosqueAmbiental(list);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -RegistrarProteccionBosqueAmbiental","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Listar por filtro ProteccionBosqueGestionAmbiental}
     * @param: ProteccionBosqueGestionAmbientalEntity
     */
    @Override
    public ResultClassEntity ListarPorFiltroProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param) throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - ListarProFiltroProteccionBosqueAmbiental",param.toString());
        try {
            response=   proteccionBosqueRespository.ListarPorFiltroProteccionBosqueAmbiental(param);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -ListarProFiltroProteccionBosqueAmbiental","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Eliminar ProteccionBosqueGestionAmbiental}
     * @param: ProteccionBosqueGestionAmbientalEntity
     */
    @Override
    public ResultClassEntity EliminarProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param) throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - EliminarProteccionBosqueAmbiental",param.toString());
        try {
            response=   proteccionBosqueRespository.EliminarProteccionBosqueAmbiental(param);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -EliminarProteccionBosqueAmbiental","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Registrar ProteccionBosqueImapctoAmbiental}
     * @param: (List<ProteccionBosqueImapctoAmbientalEntity>
     */
    @Override
    public ResultClassEntity RegistrarProteccionBosqueImpactoAmbiental(List<ProteccionBosqueImpactoAmbientalEntity> list) throws Exception {
        ResultClassEntity response ;
        log.info("ProteccionBosque - RegistrarProteccionBosqueImpactoAmbiental",list.toString());
        try {
            response=   proteccionBosqueRespository.RegistrarProteccionBosqueImpactoAmbiental(list);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("ProteccionBosque -RegistrarProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
        return response;
    }

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Listar Por Filtro ProteccionBosqueImapctoAmbiental}
     * @param: ProteccionBosqueImapctoAmbientalEntity
     */
    @Override
    public ResultClassEntity ListarPorFiltroProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param) throws Exception {
            ResultClassEntity response ;
            log.info("ProteccionBosque - ListarPorFiltroProteccionBosqueImpactoAmbiental",param.toString());
            try {
                response=   proteccionBosqueRespository.ListarPorFiltroProteccionBosqueImpactoAmbiental(param);
            } catch (Exception e) {
                // TODO: handle exception
                log.error("ProteccionBosque -ListarPorFiltroProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
                throw new Exception(e.getMessage(), e);
            }
            return response;
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Eliminar ProteccionBosqueImapctoAmbiental}
     * @param: ProteccionBosqueImapctoAmbientalEntity
     */
    @Override
    public ResultClassEntity EliminarProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param) throws Exception {
            ResultClassEntity response ;
            log.info("ProteccionBosque - EliminarProteccionBosqueImpactoAmbiental",param.toString());
            try {
                response=   proteccionBosqueRespository.EliminarProteccionBosqueImpactoAmbiental(param);
            } catch (Exception e) {
                // TODO: handle exception
                log.error("ProteccionBosque -EliminarProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
                throw new Exception(e.getMessage(), e);
            }
            return response;
    }

    /**
     * @autor:  Rafael Azaña [15-10-2021]
     * @descripción: {Registrar y Actualizar ProteccionBosque}
     * @param: ProteccionBosqueEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @Override
    public ResultClassEntity RegistrarProteccionBosque(List<ProteccionBosqueEntity> list) throws Exception {
        return proteccionBosqueRespository.RegistrarProteccionBosque(list);
    }

    /**
     * @autor:  Rafael Azaña [15-10-2021]
     * @descripción: {Listar ProteccionBosque}
     * @param: ProteccionBosqueEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @Override
    public ResultClassEntity<List<ProteccionBosqueDetalleDto>> listarProteccionBosque(Integer idPlanManejo, String codPlanGeneral, String subCodPlanGeneral)
            throws Exception {

        ResultClassEntity<List<ProteccionBosqueDetalleDto>> result = new ResultClassEntity<>();

        result.setData(proteccionBosqueRespository.listarProteccionBosque(idPlanManejo,codPlanGeneral,subCodPlanGeneral));
        result.setSuccess(true);
        result.setMessage("Lista Proteccion Bosque");

        return result;
    }


    @Override
    public ResultClassEntity EliminarProteccionBosque(ProteccionBosqueDetalleDto param) throws Exception {
        return  proteccionBosqueRespository.EliminarProteccionBosque(param);
    }

    /**
     * @autor:  Danny Nazario [28-12-2021]
     * @modificado:
     * @descripción: {Registra Potencial Producción Recurso Forestal Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarProteccionBosqueExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                            String codPlanGeneral,String subCodPlanGeneral,String codPlanGeneralDet,String subCodPlanGeneralDet,
                                                            int idPlanManejo, int idUsuarioRegistro) {
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            List<ArrayList<String>> listaExcelCabecera = fl.getExcel(file, nombreHoja, 5, numeroColumna);
             //cantidad = fl.getWorkBook(file);
            int cantidad=listaExcelCabecera.size();
            int contadorRow=0;
            String nombre="";
            int contdor=0;
            for(ArrayList objAct : listaExcelCabecera) {
                ArrayList<String> rowAct = objAct;

                for(int j = 0; j < 1; j++) {
                    if(contdor==0){
                        for (int i = 0; i < rowAct.size(); i++) {
                            if (i >= 3) {
                                    nombre += rowAct.get(i) + "/";
                            }
                        }
                    }
                }
                contdor++;
            }

            List<ProteccionBosqueEntity> list = new ArrayList<>();
            List<ProteccionBosqueDetalleEntity> listItem = new ArrayList<>();
            List<ProteccionBosqueDetalleActividadEntity> listDet = new ArrayList<>();
            ProteccionBosqueEntity cab = new ProteccionBosqueEntity();
            Boolean isInicio = true;
            int indiceInfo=0;
            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                listDet = new ArrayList<>();
                ProteccionBosqueDetalleActividadEntity det = new ProteccionBosqueDetalleActividadEntity();
                ProteccionBosqueDetalleEntity item = new ProteccionBosqueDetalleEntity();

                if (!row.get(0).isEmpty()) {
                    if(!isInicio) {
                        cab.setListProteccionBosque(listItem);
                        list.add(cab);
                        proteccionBosqueRespository.RegistrarProteccionBosque(list);
                        listItem = new ArrayList<>();
                        list = new ArrayList<>();
                    }
                    cab = new ProteccionBosqueEntity();
                    cab.setIdProBosque(0);
                    cab.setCodPlanGeneral(codPlanGeneral);
                    cab.setSubCodPlanGeneral(subCodPlanGeneral);
                    //cab.setTipoBosque(row.get(0));
                    cab.setIdPlanManejo(idPlanManejo);
                    cab.setIdUsuarioRegistro(idUsuarioRegistro);
                    isInicio = false;
                }

                item.setIdProBosqueDet(0);
                item.setCodPlanGeneralDet(codPlanGeneralDet);
                item.setSubCodPlanGeneralDet(subCodPlanGeneralDet);
                item.setActividad(row.get(0));
                item.setTipoMarcacion(row.get(1));
                item.setImpacto(row.get(2));

                nombre=nombre.replace("\n","");
                    String[] nombreCabecera = nombre.split("/");
                for(int i=0;i<nombreCabecera.length;i++){
                        det = new ProteccionBosqueDetalleActividadEntity();
                        det.setIdProBosqueActividad(0);
                        det.setDescripcion(nombreCabecera[i]);

                        if(row.get(i+3).trim().equals("X") || row.get(i+3).trim().equals("x")){
                            det.setAccion(true);
                        }else{
                            det.setAccion(false);
                        }
                        listDet.add(det);
                }
                item.setListProteccionBosqueActividad(listDet);
                listItem.add(item);
                indiceInfo++;
                contadorRow++;
            }
            cab.setListProteccionBosque(listItem);
            cab.setListProteccionGestion(null);
            cab.setListProteccionGestion(null);
            list.add(cab);
            proteccionBosqueRespository.RegistrarProteccionBosque(list);
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }


}
