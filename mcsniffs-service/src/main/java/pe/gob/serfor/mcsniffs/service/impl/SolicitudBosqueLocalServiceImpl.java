package pe.gob.serfor.mcsniffs.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.PermisoForestalService;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalService;
@Service("SolicitudBosqueLocalService")
public class SolicitudBosqueLocalServiceImpl implements SolicitudBosqueLocalService {

    @Autowired
    private SolicitudBosqueLocalRepository solicitudBosqueLocalRepository;

    @Autowired
    private EmailService serMail;

    @Autowired
    NotificacionRepository notificacionRepository;

    @Autowired
    PermisoForestalRepository permisoForestalRepository;

    @Autowired
    TituloHabilitanteRepository tituloHabilitanteRepository;

    @Autowired
    SolicitudBosqueLocalFlujEstadoRepository solicitudBosqueLocalFlujEstadoRepository;

    @Override
    public ResultClassEntity listarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        return solicitudBosqueLocalRepository.listarSolicitudBosqueLocal(obj);
    }
    @Override
    public ResultClassEntity registrarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        obj.setEstadoSolicitud("SEBESTBORR");
        return solicitudBosqueLocalRepository.registrarSolicitudBosqueLocal(obj);
    }
    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        return solicitudBosqueLocalRepository.actualizarSolicitudBosqueLocal(obj);
    }
    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        return solicitudBosqueLocalRepository.eliminarSolicitudBosqueLocal(obj);
    }
    @Override
    public ResultClassEntity obtenerSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        return solicitudBosqueLocalRepository.obtenerSolicitudBosqueLocal(obj);
    }
    @Override
    public ResultClassEntity actualizarEstadoSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        return solicitudBosqueLocalRepository.actualizarEstadoSolicitudBosqueLocal(obj);
    }
    @Override
    public ResultClassEntity listarUsuarioSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        return solicitudBosqueLocalRepository.listarUsuarioSolicitudBosqueLocal(obj);
    }

    @Override
    public ResultClassEntity generarTHSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        ResultClassEntity resultClassEntity = solicitudBosqueLocalRepository.generarTHSolicitudBosqueLocal(obj);

        CodigoCifradoEntity cifradoEntity = new CodigoCifradoEntity();
        SolicitudBosqueLocalEntity result = (SolicitudBosqueLocalEntity)resultClassEntity.getData();


        cifradoEntity.setCodigo(result.getCodigoTH());
        cifradoEntity.setProceso("C");
        ResultClassEntity codigoCifrado = permisoForestalRepository.codigoCifrado(cifradoEntity);

        List<CodigoCifradoEntity> data = (List<CodigoCifradoEntity>)codigoCifrado.getData();

        List<TituloHabilitanteEntity> list = new ArrayList<>();
        TituloHabilitanteEntity tituloHabilitanteEntity = new TituloHabilitanteEntity();
        tituloHabilitanteEntity.setIdTituloHabilitante(resultClassEntity.getCodigo());
        tituloHabilitanteEntity.setCodigoResumido(data.get(0).getCodigoCifrado());
        tituloHabilitanteEntity.setIdUsuarioRegistro(obj.getIdUsuarioRegistro());
        list.add(tituloHabilitanteEntity);

        tituloHabilitanteRepository.actualizarTituloHabilitante(list);


        return resultClassEntity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity enviarGobiernoLocal(SolicitudBosqueLocalEntity obj) throws Exception{
        ResultClassEntity result = new ResultClassEntity();

        result=  solicitudBosqueLocalRepository.actualizarSolicitudBosqueLocal(obj);

        NotificacionEntity param = new NotificacionEntity();
        param.setIdNotificacion(0);
        param.setCodigoDocgestion("TDGBOSLOC");
        param.setNumDocgestion(obj.getIdSolBosqueLocal());
        param.setMensaje("El establecimiento de Bosque Local ID Solicitud "+obj.getIdSolBosqueLocal()+" ha sido observada");
        param.setCodigoPerfil("MUNICIPIO");
        param.setFechaInicio(new Timestamp(System.currentTimeMillis()));
        param.setCantidadDias(30);
        param.setUrl("/bosque-local/bandeja-establecimiento-BL");
        param.setIdUsuarioRegistro(obj.getIdUsuarioModificacion());
        ResultClassEntity resultNotificacion = notificacionRepository.registrarNotificacion(param);

      if(result.getSuccess()){
          obj.setPerfilUsuario("MUNICIPIO");
          result=  solicitudBosqueLocalRepository.listarUsuarioSolicitudBosqueLocal(obj);
          List<SolicitudBosqueLocalEntity> lista =   (List<SolicitudBosqueLocalEntity>)  result.getData();

          for (SolicitudBosqueLocalEntity item : lista) {

            serMail.enviarEmailEstandar(
                    item.getEmailUsuario(),
                    item.getNombresUsuario(),
                    "BOSQUE LOCAL Nº "+obj.getIdSolBosqueLocal().toString(),
                    "El  Establecimiento de Bosque Local ID Solicitud: "+obj.getIdSolBosqueLocal().toString()+ " ha sido observada.</span></p>");

          }
          return result;
      }
     else{
         return result;
      }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity notificarCreacionTH(SolicitudBosqueLocalEntity obj) throws Exception{



        ResultClassEntity result = new ResultClassEntity();

        result=  solicitudBosqueLocalRepository.actualizarSolicitudBosqueLocal(obj);



        /*NotificacionEntity param = new NotificacionEntity();
        param.setIdNotificacion(0);
        param.setCodigoDocgestion("TDGBOSLOC");
        param.setNumDocgestion(obj.getIdSolBosqueLocal());
        param.setMensaje("El establecimiento de Bosque Local ID Solicitud "+obj.getIdSolBosqueLocal()+" Creación de TH");
        param.setCodigoPerfil("MUNICIPIO");
        param.setFechaInicio(new Timestamp(System.currentTimeMillis()));
        param.setCantidadDias(30);
        param.setUrl("/bosque-local/bandeja-establecimiento-BL");
        param.setIdUsuarioRegistro(obj.getIdUsuarioModificacion());
        ResultClassEntity resultNotificacion = notificacionRepository.registrarNotificacion(param);
        */
        if(result.getSuccess()){

            SolicitudBosqueLocalFlujEstadoEntity sblfe = new SolicitudBosqueLocalFlujEstadoEntity();
            sblfe.setPerfil("MUNICIPIO,ARFFS");
            sblfe.setIdSolBosqueLocal(obj.getIdSolBosqueLocal());
            result=  solicitudBosqueLocalFlujEstadoRepository.listarSolicitudBosqueLocalFlujEstado(sblfe);
            List<SolicitudBosqueLocalFlujEstadoEntity>  lista = (List<SolicitudBosqueLocalFlujEstadoEntity> )result.getData();

            for (SolicitudBosqueLocalFlujEstadoEntity item : lista) {

                serMail.enviarEmailEstandar(
                        item.getEmail(),
                        item.getPersona(),
                        "BOSQUE LOCAL Nº "+obj.getIdSolBosqueLocal().toString(),
                        "Creación de TH de Bosque Local  ID Solicitud: "+obj.getIdSolBosqueLocal().toString()+ ".</span></p>");

            }
            return result;
        }
        else{
            return result;
        }

    }

}
