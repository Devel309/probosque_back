package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;

import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalArchivoRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalArchivoService;

import java.util.List;

@Service("SolicitudBosqueLocalArchivoService")
public class SolicitudBosqueLocalArchivoServiceImpl implements SolicitudBosqueLocalArchivoService{

    @Autowired
    private SolicitudBosqueLocalArchivoRepository repository;

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj) throws Exception {
        return repository.listarSolicitudBosqueLocalArchivoPaginado(obj);
    }

    @Override
    public ResultClassEntity obtenerArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception {
        return repository.listarSolicitudBosqueLocalArchivo(obj);
    }

    @Override
    public ResultClassEntity registrarArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception {
        return repository.registrarSolicitudBosqueLocalArchivo(obj);
    }

    @Override
    public ResultClassEntity actualizarArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception {
        return repository.actualizarSolicitudBosqueLocalArchivo(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarArchivosSolicitudBosqueLocal(List<SolicitudBosqueLocalArchivoEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (items != null && !items.isEmpty()) {
            for (SolicitudBosqueLocalArchivoEntity item : items) {
                if (item.getIdSolBosqueLocalArchivo() != null && item.getIdSolBosqueLocalArchivo() > 0)
                    result = repository.actualizarSolicitudBosqueLocalArchivo(item);
                else
                    result = repository.registrarSolicitudBosqueLocalArchivo(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen archivos por registrar.");
        }

        return result;
    }

    @Override
    public ResultClassEntity eliminarArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception {
        return repository.eliminarSolicitudBosqueLocalArchivo(obj);

    }

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    @Override
    public ResultArchivoEntity descargarPlantillaIniciativaBosqueLocal() throws Exception {

        XWPFDocument doc = new XWPFDocument();
        ResultArchivoEntity result = new ResultArchivoEntity();

        try {
            doc = getDoc("PlantillaIniciativaBosqueLocal.docx");
            /** ------- END WORKING -------------------- */
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            result.setTipoDocumento("INICIATIVA");
            result.setArchivo(b.toByteArray());
            result.setNombeArchivo("PlantillaIniciativaBosqueLocal.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargó la plantilla de iniciativa de bosque local correctamente.");
            return result;

        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrio un Error. No se pudo decargar la plantilla de iniciativa de bosque local.");
            result.setMessageExeption(e.getMessage());

            return result;
        }

    }
}
