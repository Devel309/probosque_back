package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ProcedimientoAdministrativo.ProcedimientoAdministrativoDto;
import pe.gob.serfor.mcsniffs.repository.ProcedimientoAdministrativoRepository;
import pe.gob.serfor.mcsniffs.service.ProcedimientoAdministrativoService;



@Service("ProcedimientoAdministrativoService")
public class ProcedimientoAdministrativoServiceImpl implements ProcedimientoAdministrativoService {

    private static final Logger log = LogManager.getLogger(ProcedimientoAdministrativoServiceImpl.class);


    
    @Autowired
    private ProcedimientoAdministrativoRepository procedimientoAdministrativoRepository;

 
    @Override
    public ResultClassEntity<List<ProcedimientoAdministrativoDto>> listarPlanManejoEvaluacion(ProcedimientoAdministrativoDto dto) throws Exception {
        return procedimientoAdministrativoRepository.listarProcedimientoAdministrativo(dto);
    }
 
}
