package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDto;

public interface MedidaService {
    ResultClassEntity<List<MedidaDto>> listarMedida(MedidaDto dto) throws Exception;
    ResultClassEntity registrarMedida(MedidaDto dto) throws Exception;
    ResultClassEntity eliminarMedida(MedidaDto dto) throws Exception;
}
