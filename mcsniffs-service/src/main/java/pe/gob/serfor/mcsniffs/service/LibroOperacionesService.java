package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;

public interface LibroOperacionesService {

    ResultClassEntity listarLibroOperaciones(LibroOperacionesDto dto) throws Exception;
    ResultClassEntity registrarLibroOperaciones(LibroOperacionesDto param) throws Exception;
    ResultClassEntity actualizarLibroOperaciones(LibroOperacionesDto param) throws Exception;
    ResultClassEntity eliminarLibroOperaciones(LibroOperacionesDto dto) throws Exception;
    ResultClassEntity validarNroRegistroLibroOperaciones(LibroOperacionesDto param) throws Exception;
    ResultClassEntity listarLibroOperacionesPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;
    ResultClassEntity listarEvaluacionLibroOperaciones(LibroOperacionesDto dto)throws Exception;
}
