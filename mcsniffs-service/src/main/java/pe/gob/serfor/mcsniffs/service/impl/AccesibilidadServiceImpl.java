package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoEntity;
import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoMatrizDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoRequisitoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.AccesibilidadManejoMatrizDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AccesibilidadRepository;
import pe.gob.serfor.mcsniffs.repository.AnexoAdjuntoRepository;
import pe.gob.serfor.mcsniffs.service.AccesibilidadService;

import java.util.List;

@Service("AccesibilidadService")
public class AccesibilidadServiceImpl implements AccesibilidadService {

    @Autowired
    private AccesibilidadRepository accesibilidadRepository;

    @Override
    public ResultClassEntity ListarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception {
        return accesibilidadRepository.ListarAccesibilidadManejo(accesibilidadManejoEntity);
    }

    @Override
    public ResultClassEntity RegistrarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception {
        return accesibilidadRepository.RegistrarAccesibilidadManejo(accesibilidadManejoEntity);
    }

    @Override
    public ResultClassEntity ActualizarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception {
        return accesibilidadRepository.ActualizarAccesibilidadManejo(accesibilidadManejoEntity);
    }

    @Override
    public ResultClassEntity ListarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception {
        return accesibilidadRepository.ListarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
    }

    @Override
    public ResultClassEntity RegistrarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity> accesibilidadManejoRequisitoEntity) throws Exception {
        return accesibilidadRepository.RegistrarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
    }

    @Override
    public ResultClassEntity ActualizarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity>  accesibilidadManejoRequisitoEntity) throws Exception {
        return accesibilidadRepository.ActualizarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
    }

    @Override
    public ResultClassEntity EliminarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception {
        return accesibilidadRepository.EliminarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
    }

    @Override
    public ResultClassEntity RegistrarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> accesibilidadManejoMatrizDto) throws Exception {
        return  accesibilidadRepository.RegistrarAccesibilidadManejoMatriz(accesibilidadManejoMatrizDto);
    }

    @Override
    public ResultClassEntity ListarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception {
        return accesibilidadRepository.ListarAccesibilidadManejoMatriz(accesibilidadManejoMatrizDto);
    }

    @Override
    public ResultClassEntity EliminarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception {
        return accesibilidadRepository.EliminarAccesibilidadManejoMatriz(accesibilidadManejoMatrizDto);
    }

    @Override
    public ResultClassEntity ActualizarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) throws Exception {
        return  accesibilidadRepository.ActualizarAccesibilidadManejoMatriz(listAccesibilidadManejoMatrizDto);
    }

    @Override
    public ResultClassEntity EliminarAccesibilidadManejoMatrizDetalle(AccesibilidadManejoMatrizDetalleEntity accesibilidadManejoMatrizDetalleEntity) throws Exception {
        return accesibilidadRepository.EliminarAccesibilidadManejoMatrizDetalle(accesibilidadManejoMatrizDetalleEntity);
    }

}
