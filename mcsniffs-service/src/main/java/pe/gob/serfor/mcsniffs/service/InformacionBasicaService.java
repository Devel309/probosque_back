package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.Dto.InformacionBasicaDetalle.InformacionBasicaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.FaunaEntity;
import pe.gob.serfor.mcsniffs.entity.HidrografiaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TipoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.PoblacionAledaniaDto;

public interface InformacionBasicaService {
        ResultClassEntity registrarInformacionBasica(List<InformacionBasicaEntity> informacionBasicaEntity) throws Exception;
        //
        ResultClassEntity registrarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception;
        ResultClassEntity ActualizarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception;



        ResultClassEntity EliminarInfBasicaAerea(InfBasicaAereaDetalleDto infBasicaAereaDetalleDto) throws Exception;
        ResultClassEntity<List<InfBasicaAereaDetalleDto>> listarInfBasicaAerea(String idInfBasica, Integer idPlanManejo, String codCabecera) throws Exception;
        ResultClassEntity<List<InfBasicaAereaDetalleDto>> listarInfBasicaAereaTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso) throws Exception;

        //
        ResultClassEntity<List<TipoBosqueEntity>> obtenerTipoBosque(Integer idTipoBosque, String descripcion,
                        String region) throws Exception;

        ResultClassEntity<List<FaunaEntity>> obtenerFauna(Integer idPlanManejo, String tipoFauna, String nombre,
                        String nombreCientifico, String familia, String estatus, String codigoTipo) throws Exception;

        ResultClassEntity registrarSolicitudFauna(FaunaEntity faunaEntity) throws Exception;

        ResultClassEntity<List<HidrografiaEntity>> obtenerHidrografia(Integer idHidrografia,String tipoHidrografia ) throws Exception;

        ResultClassEntity<List<PoblacionAledaniaDto>> obtenerPoblacion() throws Exception;

        ResultClassEntity registrarArchivoInfBasica(InformacionBasicaEntity param,MultipartFile file)throws Exception;
        ResultClassEntity listarInfBasicaArchivo(Integer id)throws Exception;
        ResultClassEntity eliminarInfBasicaArchivo(Integer id,Integer idArchivo,Integer idUsuario)throws Exception;
        ResultClassEntity ListarInformacionBasica(Integer idPlanManejo) throws Exception;
        ResultClassEntity actualizarInformacionBasica(List<InformacionBasicaEntity> listaInformacionBasicaEntity) throws Exception;
        ResultClassEntity ListarInformacionBasicaPorFiltro(Integer idPlanManejo, String codTipoInfBasica) throws Exception;
        //Jaqueline DB
        ResultClassEntity EliminarFauna(InfBasicaAereaDetalleDto infBasicaAereaDetalleDto);

        ResultClassEntity listarInfBasicaAreaDetalle(String codTipo, String codSubTipo, Integer idInfBasica) throws Exception;

        ResultClassEntity listarPorFiltrosInfBasicaAerea(InfBasicaAereaDetalleDto param) throws Exception;

        ResultClassEntity registrarInformacionBasicaUbigeo(InformacionBasicaEntity dto) throws Exception;

        ResultClassEntity<List<InformacionBasicaUbigeoEntity>> listarInfBasicaUbigeo(InformacionBasicaEntity obj) throws Exception;
        ResultClassEntity eliminarInformacionBasicaUbigeo(InformacionBasicaUbigeoEntity dto)throws Exception;

        ResultClassEntity ListarFaunaInformacionBasicaDetalle(InformacionBasicaDetalleDto param)  throws Exception ;

        //Jason Retamozo / 26-01-2022
        ResultClassEntity listarInformacionSocioeconomica(Integer idPlanManejo, String codigoTipoInfBasica,String subCodigoTipoInfBasica) throws Exception;
}
