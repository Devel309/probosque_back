package pe.gob.serfor.mcsniffs.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.repository.ObjetivoManejoRepository;
import pe.gob.serfor.mcsniffs.service.ObjetivoManejoService;

import java.util.List;


/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad   de la base de datos mcsniffs}
 *
 */
@Service("ObjetivoManejoService")
public class ObjetivoManejoServiceImpl implements ObjetivoManejoService {


    private static final Logger log = LogManager.getLogger(ObjetivoManejoServiceImpl.class);

    @Autowired
    private ObjetivoManejoRepository ObjetivoManejoRepository;

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Registro ObjetivoManejo}
     * @param: ObjetivoManejoEntity ,ObjetivoEspecificoManejoEntity
     */
    @Override
    public ResultClassEntity RegistrarObjetivoManejo(ObjetivoManejoEntity ObjetivoManejo) throws Exception {
        return ObjetivoManejoRepository.RegistrarObjetivoManejo(ObjetivoManejo);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Actualizar ObjetivoManejo}
     * @param: ObjetivoManejoEntity ,ObjetivoEspecificoManejoEntity
     */
    @Override
    public ResultClassEntity ActualizarObjetivoManejo(ObjetivoManejoEntity ObjetivoManejo) throws Exception {
        return ObjetivoManejoRepository.ActualizarObjetivoManejo(ObjetivoManejo);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Obtener RegistrarObjetivoManejo}
     * @param: ObjetivoManejoEntity
     */
    @Override
    public ResultClassEntity ObtenerObjetivoManejo(ObjetivoManejoEntity ObjetivoManejo) throws Exception {
        return ObjetivoManejoRepository.ObtenerObjetivoManejo(ObjetivoManejo);
    }

    @Override
    public ResultClassEntity RegistrarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoList) throws Exception {
        return ObjetivoManejoRepository.RegistrarObjetivoEspecificoManejo(ObjetivoEspecificoManejoList);
    }

    @Override
    public ResultClassEntity ActualizarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoList) throws Exception {
        return ObjetivoManejoRepository.ActualizarObjetivoEspecificoManejo(ObjetivoEspecificoManejoList);
    }

    @Override
    public ResultClassEntity ObtenerObjetivoEspecificoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception {
        return ObjetivoManejoRepository.ObtenerObjetivoEspecificoManejo(ObjetivoManejoEntity);
    }
    @Override
    public ResultClassEntity ComboPorFiltroObjetivoEspecifico(ParametroEntity parametro) throws Exception{
        return ObjetivoManejoRepository.ComboPorFiltroObjetivoEspecifico(parametro);
    }

    /**
     * @autor: Jaqueline DB [20-09-2021]
     * @modificado:
     * @descripción: {Elimina los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultClassEntity eliminarObjetivoManejo(ObjetivoManejoEntity param) {
        return ObjetivoManejoRepository.eliminarObjetivoManejo(param);
    }

    /**
     * @autor: Jason Retamozo [20-01-2022]
     * @modificado:
     * @descripción: {listar los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultEntity<ObjetivoDto> listarObjetivoManejoTitular(ObjetivoManejoEntity param) {
        return ObjetivoManejoRepository.listarObjetivoManejoTitular(param);
    }

     /**
     * @autor: Jaqueline DB [21-09-2021]
     * @modificado:
     * @descripción: {listar los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultEntity<ObjetivoManejoEntity> listarObjetivoManejo(ObjetivoManejoEntity param) {
        return ObjetivoManejoRepository.listarObjetivoManejo(param);
    }

    @Override
    public ResultEntity<ObjetivoDto> listarObjetivos(String codigoObjetivo, Integer idPlanManejo) {
        return ObjetivoManejoRepository.listarObjetivos(codigoObjetivo, idPlanManejo);
    }

    @Override
    public ResultClassEntity RegistrarObjetivo(List<ObjetivoManejoEntity> list) throws Exception {
        return ObjetivoManejoRepository.RegistrarObjetivo(list);
    }

}
