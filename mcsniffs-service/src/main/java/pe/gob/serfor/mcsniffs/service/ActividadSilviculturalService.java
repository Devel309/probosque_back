package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetallePgmfDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.SistemaManejoAprovechamientoLaboresSilviculturales.SistemaManejoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;

import java.util.List;

public interface ActividadSilviculturalService {
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetalle(Integer idPlanManejo) throws Exception;
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalFiltroTipo(Integer idPlanManejo,String idTipo) throws Exception;
    ResultClassEntity ConfigurarActividadSilviculturalPgmf(ActividadSilviculturalEntity actividadSilviculturalEntity) throws Exception;
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetallePgmf(Integer idPlanManejo, Integer idTipo) throws Exception;
    ResultClassEntity ActualizarActividadSilviculturalPgmf(List<ActividadSilviculturalDetallePgmfDto> lista) throws Exception;

    ResultClassEntity EliminarActividadSilviculturalPgmf(List<ActividadSilviculturalDetalleEntity> list) throws Exception;

    List<ActividadSilviculturalDetalleDto> ListarActividadSilvicultural(Integer idPlanManejo,String codigoPlan) throws Exception;
    ResultClassEntity RegistrarLaborSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception;
    ResultClassEntity EliminarActividadSilviculturalDema(List<ActividadSilviculturalDetalleEntity> list) throws Exception;

    //region PFDM
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalPFDM(Integer idPlanManejo, String idTipo) throws Exception;
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalPFDMTitular(String tipoDocumento, String nroDocumento,String codigoProcesoTitular,String codigoProceso,Integer idPlanManejo) throws Exception;

    ResultClassEntity EliminarActividadSilviculturalPFDM(List<ActividadSilviculturalDetalleEntity> list) throws Exception;
    ResultClassEntity GuardarSistemaManejoPFDM(SistemaManejoDto param) throws Exception;

    //endregion

    List<ActividadSilviculturalEntity> ListarActSilviCulturalDetalle(ActividadSilviculturalEntity param) throws Exception;

    ResultClassEntity RegistrarActividadSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception;
    ResultClassEntity<List<ActividadSilviculturalDto>> ListarActividadSilviculturalCabecera(Integer idPlanManejo) throws Exception;
}
