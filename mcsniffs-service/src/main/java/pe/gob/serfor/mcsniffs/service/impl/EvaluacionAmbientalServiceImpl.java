package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.EvaluacionAmbientalService;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad de la base de datos
 *               mcsniffs}
 *
 */
@Service("EvaluacionAmbientalService")
public class EvaluacionAmbientalServiceImpl implements EvaluacionAmbientalService {

    private static final Logger log = LogManager.getLogger(EvaluacionAmbientalServiceImpl.class);

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbiental)
            throws Exception {
        return evaluacionAmbientalRepository.RegistrarEvaluacionAmbiental(evaluacionAmbiental);
    }

    @Override
    public ResultClassEntity ActualizarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
            throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity EliminarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
            throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ObtenerEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
            throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbientalDetalle(
            List<EvaluacionAmbientalDetalleEntity> evaluacionAmbientalDetalleList) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ActualizarEvaluacionAmbientalDetalle(
            List<EvaluacionAmbientalDetalleEntity> evaluacionAmbientalDetalleList) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity EliminarEvaluacionAmbientalDetalle(
            EvaluacionAmbientalDetalleEntity evaluacionAmbientalDetalleEntity) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ObtenerEvaluacionAmbientalDetalle(
            EvaluacionAmbientalDetalleEntity evaluacionAmbientalDetalleEntity) throws Exception {
        return null;
    }

    @Override
    public List<EvaluacionAmbientalAprovechamientoEntity> ListarEvaluacionAprovechamientoFiltro(
            EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        return evaluacionAmbientalRepository.ListarEvaluacionAprovechamientoFiltro(param);
    }

    @Override
    public ResultClassEntity RegistrarAprovechamientoEvaluacionAmbiental(
            EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        return evaluacionAmbientalRepository.RegistrarAprovechamientoEvaluacionAmbiental(param);
    }

    @Override
    public ResultClassEntity RegistrarPlanGestionExcel(MultipartFile file, String nombreHoja, int numeroFila,
            int numeroColumna, int idPlanManejo, String codTipoAprovechamiento, String tipoAprovechamiento,
            String tipoNombreAprovechamiento, int idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        ResultClassEntity response;
        EvaluacionAmbientalAprovechamientoEntity evamapr = new EvaluacionAmbientalAprovechamientoEntity();
        EvaluacionAmbientalActividadEntity evamact = new EvaluacionAmbientalActividadEntity();
        EvaluacionAmbientalEntity evam = new EvaluacionAmbientalEntity();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcelActividad = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            for (ArrayList obj : listaExcelActividad) {
                ArrayList<String> row = obj;
                evamapr.setCodTipoAprovechamiento(codTipoAprovechamiento);
                if (row.get(2).equals("Pre Aprovechamiento")) {
                    tipoAprovechamiento = "PREAPR";
                } else if (row.get(2).equals("Aprovechamiento")) {
                    tipoAprovechamiento = "APROVE";
                } else if (row.get(2).equals("Post Aprovechamiento")) {
                    tipoAprovechamiento = "POSTAP";
                }
                evamapr.setTipoAprovechamiento(tipoAprovechamiento);
                evamapr.setTipoNombreAprovechamiento(row.get(2));
                evamapr.setNombreAprovechamiento(row.get(3));
                evamapr.setIdPlanManejo(idPlanManejo);
                evamapr.setIdUsuarioRegistro(idUsuarioRegistro);
                // System.out.println(row.get(1));
                evaluacionAmbientalRepository.RegistrarAprovechamientoEvaluacionAmbiental(evamapr);
            }

            List<ArrayList<String>> listaExcelFactor = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            int idEvalActividad = 0;
            String tipoActividad = "FACTOR";
            String TipoNombreActividad = "Factores Ambientales";
            for (ArrayList obj : listaExcelFactor) {
                ArrayList<String> row = obj;
                evamact.setIdEvalActividad(idEvalActividad);
                evamact.setCodTipoActividad(codTipoAprovechamiento);
                evamact.setTipoActividad(tipoActividad);
                evamact.setTipoNombreActividad(TipoNombreActividad);
                evamact.setNombreActividad(row.get(0));
                evamact.setIdPlanManejo(idPlanManejo);
                evamact.setIdUsuarioRegistro(idUsuarioRegistro);
                evaluacionAmbientalRepository.RegistrarActividadEvaluacionAmbiental(evamact);
            }

            List<ArrayList<String>> listaExcelValorEvaluacion = fl.getExcel(file, nombreHoja, numeroFila,
                    numeroColumna);
            int idEvaluacionAmbiental = 0;
            for (ArrayList obj : listaExcelValorEvaluacion) {
                ArrayList<String> row = obj;
                evam.setIdEvaluacionAmbiental(idEvaluacionAmbiental);
                evam.setNombreAprovechamiento(row.get(3));
                evam.setNombreActividad(row.get(0));
                evam.setCaracterizacionImpacto(row.get(1));
                evam.setMedidasMonitoreo(row.get(5));
                evam.setIdPlanManejo(idPlanManejo);
                evam.setValorEvaluacion(row.get(4));
                evam.setIdUsuarioRegistro(idUsuarioRegistro);
                evam.setAuxiliar(row.get(1));
                evam.setResponsableSecundario(row.get(0));
                // System.out.println();
                evaluacionAmbientalRepository.RegistrarEvaluacionAmbientalDetalleSubXls(evam);
            }
            result.setSuccess(true);
            result.setMessage("Se registraron los Planes de Acción Preventivo-Corrector correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    @Override
    public ResultClassEntity RegistrarPlanAccionExcel(MultipartFile file, String nombreHoja, int numeroFila,
            int numeroColumna, int idPlanManejo, String codTipoAprovechamiento, String tipoAprovechamiento,
            String tipoNombreAprovechamiento, int idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        EvaluacionAmbientalAprovechamientoEntity eval = new EvaluacionAmbientalAprovechamientoEntity();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            for (ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                eval.setCodTipoAprovechamiento(codTipoAprovechamiento);
                eval.setTipoAprovechamiento(tipoAprovechamiento);
                eval.setTipoNombreAprovechamiento(tipoNombreAprovechamiento);
                eval.setNombreAprovechamiento(row.get(0));
                eval.setImpacto(row.get(1));
                eval.setMedidasControl(row.get(2));
                eval.setIdPlanManejo(idPlanManejo);
                eval.setIdUsuarioRegistro(idUsuarioRegistro);
                evaluacionAmbientalRepository.RegistrarAprovechamientoEvaluacionAmbiental(eval);
            }
            result.setSuccess(true);
            result.setMessage("Se registraron los Planes de Acción Preventivo-Corrector correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    @Override
    public ResultClassEntity EliminarEvaluacionAmbientalAprovechamiento(EvaluacionAmbientalAprovechamientoEntity valor)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        try {
            evaluacionAmbientalRepository.EvaluacionAmbientalAprovechamiento_Eliminar(valor);
            result.setSuccess(true);
            result.setMessage("Se elimino el aprovechamiento de evaluación ambiental correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    @Override
    public ResultClassEntity eliminarEvaluacionAmbientalActividad(EvaluacionAmbientalActividadEntity param) throws Exception {
        return  evaluacionAmbientalRepository.eliminarEvaluacionAmbientalActividad(param);
    }

    @Override
    public ResultClassEntity eliminarEvaluacionAmbientalAprovechamientoList(EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        return  evaluacionAmbientalRepository.eliminarEvaluacionAmbientalAprovechamientoList(param);
    }

    @Override
    public ResultClassEntity eliminarEvaluacionAmbiental(EvaluacionAmbientalEntity param) throws Exception {
        return  evaluacionAmbientalRepository.eliminarEvaluacionAmbiental(param);
    }

    @Override
    public ResultClassEntity RegistrarListAprovechamientoEvaluacionAmbiental(
            List<EvaluacionAmbientalAprovechamientoEntity> valor) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try {
            for (EvaluacionAmbientalAprovechamientoEntity obj : valor) {
                evaluacionAmbientalRepository.RegistrarAprovechamientoEvaluacionAmbiental(obj);
            }
            result.setSuccess(true);
            result.setMessage("Se registró el aprovechamiento de evaluación ambiental correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    @Override
    public ResultClassEntity RegistrarPlanVigilanciaExcel(MultipartFile file, String nombreHoja, int numeroFila,
            int numeroColumna, int idPlanManejo, String codTipoAprovechamiento, String tipoAprovechamiento,
            String tipoNombreAprovechamiento, int idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        EvaluacionAmbientalAprovechamientoEntity eval = new EvaluacionAmbientalAprovechamientoEntity();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            for (ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                eval.setCodTipoAprovechamiento(codTipoAprovechamiento);
                eval.setTipoAprovechamiento(tipoAprovechamiento);
                eval.setTipoNombreAprovechamiento(tipoNombreAprovechamiento);
                eval.setImpacto(row.get(0));
                eval.setMedidasControl(row.get(1));
                eval.setMedidasMonitoreo(row.get(2));
                eval.setFrecuencia(row.get(3));
                eval.setResponsable(row.get(4));
                eval.setIdPlanManejo(idPlanManejo);
                eval.setIdUsuarioRegistro(idUsuarioRegistro);
                evaluacionAmbientalRepository.RegistrarAprovechamientoEvaluacionAmbiental(eval);
            }
            result.setSuccess(true);
            result.setMessage("Se registraron los Planes de Vigilancia y Seguimiento correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    @Override
    public ResultClassEntity RegistrarPlanContingenciaExcel(MultipartFile file, String nombreHoja, int numeroFila,
            int numeroColumna, int idPlanManejo, String codTipoAprovechamiento, String tipoAprovechamiento,
            String tipoNombreAprovechamiento, int idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        EvaluacionAmbientalAprovechamientoEntity eval = new EvaluacionAmbientalAprovechamientoEntity();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);
            for (ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                eval.setCodTipoAprovechamiento(codTipoAprovechamiento);
                eval.setTipoAprovechamiento(tipoAprovechamiento);
                eval.setTipoNombreAprovechamiento(tipoNombreAprovechamiento);
                eval.setContingencia(row.get(0));
                eval.setAcciones(row.get(1));
                eval.setResponsableSecundario(row.get(2));
                eval.setIdPlanManejo(idPlanManejo);
                eval.setIdUsuarioRegistro(idUsuarioRegistro);
                evaluacionAmbientalRepository.RegistrarAprovechamientoEvaluacionAmbiental(eval);
            }
            result.setSuccess(true);
            result.setMessage("Se registraron los Planes de Contingencia Ambiental correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
        }
        return result;
    }

    @Override
    public List<EvaluacionAmbientalActividadEntity> ListarEvaluacionActividadFiltro(
            EvaluacionAmbientalActividadEntity param) throws Exception {
        return evaluacionAmbientalRepository.ListarEvaluacionActividadFiltro(param);
    }

    @Override
    public ResultClassEntity RegistrarActividadEvaluacionAmbiental(
            EvaluacionAmbientalActividadEntity param) throws Exception {
        return evaluacionAmbientalRepository.RegistrarActividadEvaluacionAmbiental(param);
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbientalDetalleSub(List<EvaluacionAmbientalEntity> list)
            throws Exception {
        return evaluacionAmbientalRepository.RegistrarEvaluacionAmbientalDetalleSub(list);
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbientalDetalleSubXls(EvaluacionAmbientalEntity item) {
        return evaluacionAmbientalRepository.RegistrarEvaluacionAmbientalDetalleSubXls(item);
    }

    @Override
    public List<EvaluacionAmbientalDto> ListarEvaluacionAmbiental(EvaluacionAmbientalDto list) throws Exception {
        return evaluacionAmbientalRepository.ListarEvaluacionAmbiental(list);
    }
}
