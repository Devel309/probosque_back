package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;

public interface ResponsableExperienciaLaboralService {

        ResultClassEntity obtenerResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj)
                        throws Exception;

        ResultClassEntity listarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj)
                        throws Exception;

        ResultClassEntity registrarResponsableExperienciaLaboral(SolicitudConcesionResponsableDto obj)
                        throws Exception;

        ResultClassEntity actualizarResponsableExperienciaLaboral(SolicitudConcesionResponsableDto obj)
                        throws Exception;

        ResultClassEntity eliminarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj)
                        throws Exception;
}
