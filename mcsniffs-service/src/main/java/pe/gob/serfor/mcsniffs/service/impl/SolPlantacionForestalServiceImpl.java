package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarSolicitudPlantacionForestal.DescargarSolicitudPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
//import pe.gob.serfor.mcsniffs.entity.Dto.comboBloque.BloqueDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.*;
import pe.gob.serfor.mcsniffs.repository.AreaSistemaPlantacionRepository;
import pe.gob.serfor.mcsniffs.repository.CoreCentralRepository;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalGeometriaRepository;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.util.DocUtilPlantacionForestal;

@Service("SolPlantacionForestalService")
public class SolPlantacionForestalServiceImpl implements SolPlantacionForestalService {

    @Autowired
    private SolPlantacionForestalRepository repo;

    @Autowired
    private FileServerConexion fileServ;

    @Autowired
    private ArchivoService ArServ;

    @Value("${smb.file.server.path}")
    private String fileServerPath;

    @Autowired
    private EmailService emailService;

    @Autowired
    SolPlantacionForestalEvaluacionRepository solPlantacionForestalEvaluacionRepository;

    @Autowired
    private ServicioExternoService serExt;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

    @Autowired
    private EmailService serMail;

    @Autowired
    private AreaSistemaPlantacionRepository areaSistemaPlantacionRepository;

    @Autowired
    SolPlantacionForestalGeometriaRepository solPlantacionForestalGeometriaRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Override
    public ResultEntity<SolPlantacionForestalEntity> registroSolicitudPlantacionForestal(
            SolPlantacionForestalEntity request) {
        // TODO Auto-generated method stub
        return repo.registroSolicitudPlantacionForestal(request);
    }
    @Override
    public ResultClassEntity clonarPlantacionForestal(
            SolPlantacionForestalDto request) {
        // TODO Auto-generated method stub
        return repo.clonarPlantacionForestal(request);
    }

    @Override
    public ResultClassEntity validarClonPlantacionForestal(SolPlantacionForestalDto request){
        // TODO Auto-generated method stub
        return repo.validarClonPlantacionForestal(request);
    }


    @Override
    public ResultClassEntity eliminarSolicitudPlantacionForestal(
            SolPlantacionForestalEntity request) throws Exception {
        // TODO Auto-generated method stub
        return repo.eliminarSolicitudPlantacionForestal(request);
    }

    @Override
    public ResultClassEntity listarSolicitudPlantacionForestal(
            SolPlantacionForestalEntity request) {
        return repo.listarSolicitudPlantacionForestal(request);
    }

    @Override
    public ResultClassEntity obtenerSolicitudPlantacionForestal(
        SolPlantacionForestalDto request) {

        ResultClassEntity resul = new ResultClassEntity();
        try {

            ResultClassEntity result =repo.listarBandejaSolicitudPlantacionForestal(request);
            List<SolPlantacionForestalDto> lsResult= (List<SolPlantacionForestalDto>) result.getData();
            if(lsResult.size()>0){
                resul.setData(lsResult.get(0));
                resul.setMessage("Información encontrada");
                resul.setSuccess(true);
            }else{
                resul.setMessage("No se encontró información");
                resul.setSuccess(false);
            }

        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setSuccess(false);
        }
            return resul;

    }
    @Override
    public ResultClassEntity obtenerInformacionTitular(SolPlantacionForestalEntity request) {
        return repo.obtenerSolicitudPlantacionForestal(request);
    }

    @Override
    public ResultClassEntity actualizarInformacionTitular(
            SolPlantacionForestalEntity request) {
        return repo.actualizarSolicitudPlantacionForestal(request);
    }

    @Override
    public ResultEntity<Dropdown> listarTipoComboSistemaPlantacionForestal(SistemaPlantacionEntity request) {
        // TODO Auto-generated method stub
        return repo.listarTipoComboSistemaPlantacionForestal(request);
    }

    @Override
    public ResultClassEntity listarBandejaSolicitudPlantacionForestal(
        SolPlantacionForestalDto request) {
        // TODO Auto-generated method stub
        return repo.listarBandejaSolicitudPlantacionForestal(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalEstadoEntity> RegistrarSolicitudPlantacionForestalEstado(
            SolPlantacionForestalEstadoEntity request) {
        // TODO Auto-generated method stub
        return repo.RegistrarSolicitudPlantacionForestalEstado(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> registroSolicitudPlantacionForestalAreaPlantada(
            List<SolPlantacionForestalAreaPlantadaEntity> request) {
        // TODO Auto-generated method stub
        return repo.registroSolicitudPlantacionForestalAreaPlantada(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> eliminarSolicitudPlantacionForestalAreaPlantada(
            SolPlantacionForestalAreaPlantadaEntity request) {
        // TODO Auto-generated method stub
        return repo.eliminarSolicitudPlantacionForestalAreaPlantada(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> listarSolicitudPlantacionForestalAreaPlantada(
            SolPlantacionForestalAreaPlantadaEntity request) {
        // TODO Auto-generated method stub
        return repo.listarSolicitudPlantacionForestalAreaPlantada(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> obtenerSolicitudPlantacionForestalAreaPlantada(
            SolPlantacionForestalAreaPlantadaEntity request) {
        // TODO Auto-generated method stub
        return repo.obtenerSolicitudPlantacionForestalAreaPlantada(request);
    }

    /** area */
    @Override
    public ResultClassEntity registroSolPlantacionForestalArea(
            SolPlantacionForestalAreaEntity request) {
        // TODO Auto-generated method stub
        return repo.registroSolPlantacionForestalArea(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaEntity> listarSolPlantacionForestalArea(
            SolPlantacionForestalAreaEntity request) {
        // TODO Auto-generated method stub
        return repo.listarSolPlantacionForestalArea(request);
    }

    /***** coordenada */
    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> registroSolicitudPlantacionForestalDetCoord(
            List<SolPlantacionForestalDetCoordenadaEntity> request) {
        // TODO Auto-generated method stub
        return repo.registroSolicitudPlantacionForestalDetCoord(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> eliminarSolicitudPlantacionForestalDetCoord(
            SolPlantacionForestalDetCoordenadaEntity request) {
        // TODO Auto-generated method stub
        return repo.eliminarSolicitudPlantacionForestalDetCoord(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> listarSolicitudPlantacionForestalDetCoord(
            SolPlantacionForestalDetCoordenadaEntity request) {
        // TODO Auto-generated method stub
        return repo.listarSolicitudPlantacionForestalDetCoord(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> obtenerSolicitudPlantacionForestalDetCoord(
            SolPlantacionForestalDetCoordenadaEntity request) {
        // TODO Auto-generated method stub
        return repo.obtenerSolicitudPlantacionForestalDetCoord(request);
    }

    /***** det */
    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> registroSolicitudPlantacionForestalDet(
            List<SolPlantacionForestalDetalleEntity> request) {
        // TODO Auto-generated method stub
        return repo.registroSolicitudPlantacionForestalDet(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> eliminarSolicitudPlantacionForestalDet(
            SolPlantacionForestalDetalleEntity request) {
        // TODO Auto-generated method stub
        return repo.eliminarSolicitudPlantacionForestalDet(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> listarSolicitudPlantacionForestalDet(
            SolPlantacionForestalDetalleEntity request) {
        // TODO Auto-generated method stub
        return repo.listarSolicitudPlantacionForestalDet(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> obtenerSolicitudPlantacionForestalDet(
            SolPlantacionForestalDetalleEntity request) {
        // TODO Auto-generated method stub
        return repo.obtenerSolicitudPlantacionForestalDet(request);
    }

    /*** anexo */
    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> registroSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request, MultipartFile file) {
        // TODO Auto-generated method stub
        ResultEntity res = new ResultEntity();

        String nombreGenerado = fileServ.uploadFile(file);

        ArchivoEntity requestArchivo = new ArchivoEntity();
        SolPlantacionForestalAnexoEntity anexo = new SolPlantacionForestalAnexoEntity();
        requestArchivo.setEstado("A");

        requestArchivo
                .setRuta(fileServerPath + (!nombreGenerado.equals("") ? nombreGenerado : file.getOriginalFilename()));

        requestArchivo.setExtension(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),
                file.getOriginalFilename().length()));
        requestArchivo.setNombre(file.getOriginalFilename());
        requestArchivo.setNombreGenerado((!nombreGenerado.equals("") ? nombreGenerado : file.getOriginalFilename()));
        requestArchivo.setIdArchivo(request.getIdArchivo());
        requestArchivo.setIdUsuarioRegistro(request.getIdUsuarioRegistro());
        requestArchivo.setIdUsuarioModificacion(request.getIdUsuarioModificacion());
        requestArchivo.setTipoDocumento("");
        res = ArServ.registroArchivo(requestArchivo);

        if (res.getIsSuccess()) {
            request.setIdArchivo(res.getCodigo());
            // anexo.setDescripcion(file.getOriginalFilename());
            // anexo.setIdUsuarioRegistro(idUsuario);
            // anexo.setIdUsuarioModificacion(idUsuarioEdicion);
            // anexo.setId_archivo_solplantacionforest(id_archivo_solplantacionforest);
            // anexo.setId_solplantforest(id_solplantforest);
            // anexo.setId_tipodetalleanexo(id_tipodetalleanexo);
            // anexo.setId_tipodetalleseccion(id_tipodetalleseccion);
            res = repo.registroSolPlantacionForestalAnexo(request);
        }

        return res;
    }

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> eliminarSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        // TODO Auto-generated method stub
        return repo.eliminarSolPlantacionForestalAnexo(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> listarSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        // TODO Auto-generated method stub
        return repo.listarSolPlantacionForestalAnexo(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> obtenerSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        // TODO Auto-generated method stub
        return repo.obtenerSolPlantacionForestalAnexo(request);
    }

    /*************** observacion */
    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> registroSolPlantacionForestalObservacion(
            List<SolPlantacionForestalObservacionEntity> request) {
        // TODO Auto-generated method stub
        return repo.registroSolPlantacionForestalObservacion(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> eliminarSolPlantacionForestalObservacion(
            SolPlantacionForestalObservacionEntity request) {
        // TODO Auto-generated method stub
        return repo.eliminarSolPlantacionForestalObservacion(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> listarSolPlantacionForestalObservacion(
            SolPlantacionForestalObservacionEntity request) {
        // TODO Auto-generated method stub
        return repo.listarSolPlantacionForestalObservacion(request);
    }

    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> obtenerSolPlantacionForestalObservacion(
            SolPlantacionForestalObservacionEntity request) {
        // TODO Auto-generated method stub
        return repo.obtenerSolPlantacionForestalObservacion(request);
    }

    @Override
    public ResultClassEntity obtenerArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj)
            throws Exception {
        return repo.listarSolPlantacionForestalArchivo(obj);
    }

    @Override
    public ResultClassEntity registrarArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj)
            throws Exception {
        return repo.registrarSolPlantacionForestalArchivo(obj);
    }

    @Override
    public ResultClassEntity registrarArchivosSolPlantacionForestal(List<SolPlantacionForestalArchivoEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (items != null && !items.isEmpty()) {
            for (SolPlantacionForestalArchivoEntity item : items) {
                if (item.getIdSolPlantForestArchivo() > 0)
                    result = repo.actualizarSolPlantacionForestalArchivo(item);
                else
                    result = repo.registrarSolPlantacionForestalArchivo(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen archivos por registrar.");
        }

        return result;
    }

    @Override
    public ResultClassEntity actualizarArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj)
            throws Exception {
        return repo.actualizarSolPlantacionForestalArchivo(obj);
    }

    @Override
    public ResultClassEntity eliminarArchivoSolPlantacionForestal(SolPlantacionForestalArchivoEntity obj)
            throws Exception {
        return repo.eliminarSolPlantacionForestalArchivo(obj);
    }

    @Override
    public ResultClassEntity registrarSolPlantacacionForestal(SolicitudPlantacionForestalEntity obj) throws Exception {

        ResultClassEntity resultClassEntity = repo.registrarSolPlantacacionForestal(obj);
        SolicitudPlantacionForestalEntity request = (SolicitudPlantacionForestalEntity) resultClassEntity.getData();

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLPLANTACIONFORESTAL,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLPLANTACIONFORESTAL + request.getIdSolPlantForest(),
                obj.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    @Override
    public ResultClassEntity actualizarSolicitudPlantacionForestal(SolPlantacionForestalEntity request) {
        return repo.actualizarSolicitudPlantacionForestal(request);
    }

    @Override
    public ResultClassEntity enviarSolicitud(SolPlantacionForestalDto obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        SolPlantacionForestalEntity params = new SolPlantacionForestalEntity();
        params.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());

        ResultClassEntity rsSolicitud = repo.listarSolicitudPlantacionForestal(params);
        List<SolPlantacionForestalEntity> lsSol=(List<SolPlantacionForestalEntity>) rsSolicitud.getData();
        SolPlantacionForestalEntity regSol = lsSol.get(0);

        String asuntoMail = "";
        String mensajeMail = "";

        SolPlantacionForestalEntity objActualizar = null;
        switch (obj.getTipoEnvio()) {
            case "ENVIARSC": // Presentada

                objActualizar = new SolPlantacionForestalEntity();
                objActualizar.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());
                objActualizar.setEstadoSolicitud("SPFESTPRES");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());

                repo.actualizarSolicitudPlantacionForestal(objActualizar);
                break;
            case "SUBSANAR": // Requisitos Observados

                objActualizar = new SolPlantacionForestalEntity();
                objActualizar.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());
                objActualizar.setEstadoSolicitud("SPFESTREQO");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                //objActualizar.setFechaRequisitoObs(new Date());
                repo.actualizarSolicitudPlantacionForestal(objActualizar);

                //enviando notificación
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSol.getIdSolicitudPlantacionForestal() + " OBSERVADA";
                mensajeMail = "La solicitud N° : " + regSol.getIdSolicitudPlantacionForestal() + " ha sido observada";
                emailService.enviarEmailEstandar(regSol.getSolicitante().getCorreo(), regSol.getSolicitante().getRazonSocialEmpresa(),asuntoMail, mensajeMail );

                break;
            case "EVALUADOR": // En Evaluación

                objActualizar = new SolPlantacionForestalEntity();
                objActualizar.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());
                objActualizar.setEstadoSolicitud("SPFESTEVAL");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                //objActualizar.setFechaEvaluacion(new Date());
                repo.actualizarSolicitudPlantacionForestal(objActualizar);

                SolPlantacionForestalEvaluacionDto objEvaluacion = new SolPlantacionForestalEvaluacionDto();
                objEvaluacion.setIdSolPlantacionForestal(obj.getIdSolicitudPlantacion());

                objEvaluacion.setFechaInicio(new Date());
                objEvaluacion.setIdUsuarioRegistro(obj.getIdUsuarioModificacion());
                solPlantacionForestalEvaluacionRepository.registrarSolicitudPlantacionForestalEvaluacion(objEvaluacion);


                //enviando notificación
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSol.getIdSolicitudPlantacionForestal() + " EN EVALUACIÓN";
                mensajeMail = "La solicitud N° : " + regSol.getIdSolicitudPlantacionForestal() + " esta en evaluación";
                emailService.enviarEmailEstandar(regSol.getSolicitante().getCorreo(), regSol.getSolicitante().getRazonSocialEmpresa(),asuntoMail, mensajeMail );

                break;

            case "EVALUACIONOBS": // solicitud observada

                objActualizar = new SolPlantacionForestalEntity();
                objActualizar.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());
                objActualizar.setEstadoSolicitud("SPFESTSOLO");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                repo.actualizarSolicitudPlantacionForestal(objActualizar);

                //enviando notificación
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSol.getIdSolicitudPlantacionForestal() + " OBSERVADA";
                mensajeMail = "La solicitud N° : " + regSol.getIdSolicitudPlantacionForestal() + " ha sido observada.";
                mensajeMail += "<p>Motivo: " + obj.getObservacion() + "</p>";
                emailService.enviarEmailEstandar(regSol.getSolicitante().getCorreo(), regSol.getSolicitante().getRazonSocialEmpresa(),asuntoMail, mensajeMail );

                break;

            case "APROBARSPF": // solicitud aprobada

                objActualizar = new SolPlantacionForestalEntity();
                objActualizar.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());
                objActualizar.setEstadoSolicitud("SPFESTSOLA");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                repo.actualizarSolicitudPlantacionForestal(objActualizar);

                //enviando notificación
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSol.getIdSolicitudPlantacionForestal() + " APROBADA";
                mensajeMail = "La solicitud N° : " + regSol.getIdSolicitudPlantacionForestal() + " ha sido aprobada.";
                emailService.enviarEmailEstandar(regSol.getSolicitante().getCorreo(), regSol.getSolicitante().getRazonSocialEmpresa(),asuntoMail, mensajeMail );

                break;
            case "DENEGARSPF": // solicitud denegada

                objActualizar = new SolPlantacionForestalEntity();
                objActualizar.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacion());
                objActualizar.setEstadoSolicitud("SPFESTSOLD");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                repo.actualizarSolicitudPlantacionForestal(objActualizar);

                //enviando notificación
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSol.getIdSolicitudPlantacionForestal() + " DENEGADA";
                mensajeMail = "La solicitud N° : " + regSol.getIdSolicitudPlantacionForestal() + " ha sido denegada.";
                emailService.enviarEmailEstandar(regSol.getSolicitante().getCorreo(), regSol.getSolicitante().getRazonSocialEmpresa(),asuntoMail, mensajeMail );

                break;

//            case "FINALIZARSC": // solicitud denegada
//
//                objActualizar = new SolicitudConcesionDto();
//                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
//                objActualizar.setEstadoSolicitud("SCESTFINO");
//                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
//                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);
//
//                regSC.setEstadoSolicitud("SCESTFINO");
//                this.enviarEmail(regSC);
//                break;

        }

        result.setSuccess(true);
        result.setMessage("Se actualizó la solicitud de concesión correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity listarEspeciesSistemaPlantacion(Integer id) throws Exception {
        return repo.listarEspeciesSistemaPlantacion(id);
    }

    @Override
    public ResultClassEntity registrarSolPlantacacionForestalDetalle(List<SolicitudPlantacionForestalDetalleEntity> obj)
            throws Exception {
    
        return repo.registrarSolPlantacionForestalDetalle(obj);

    }

    @Override
    public ResultClassEntity notificarSolicitante(SolPlantacionForestalEntity request) {
        
        ResultClassEntity envio = new ResultClassEntity();
        Gson gson = new Gson();

        UsuarioDto user= new UsuarioDto();
        user.setIdusuario(request.getIdSolicitante());

        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), request.getToken());
        ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
        String correo = null;
        String nombreCompleto = null;
        
        for(Object item: resultApiExt.getData().stream().toArray()) {
            LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
            correo= val.get("correoElectronico").toString();
            nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
        }

        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        if (correo!=null) {
            String[] mails=new String[1];
            mails[0]= correo;
            EmailEntity mail=new EmailEntity();
            mail.setSubject("SERFOR:: Plantaciones Forestales");
            mail.setEmail(mails);

            String body="<html><head>"
                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                    +"</head>"
                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                    +"<div class=WordSection1>"
                    +"<p class=MsoNormal>Estimado(s) <span lang=ES> "+nombreCompleto+"</span></p>"
                    +"<p class=MsoNormal><span lang=ES> Se envia su Certificado de registro de plantaciones, ID Sol: "+ request.getIdSolicitudPlantacionForestal().toString() +".</span></p>"
                    +"<p class=MsoNormal><span lang=ES> (Tendrá 5 días hábiles de plazo) (irá el numero de certificado y el solicitante podrá visualizar en su bandeja el Certificado ya resuelto).</span></p>"
                    +"<br><br><br>"
                    +"<p class=MsoNormal><b><i><span lang=ES> Atentamente,</span></i></b><i></i></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES> Departamento de Sistemas</span></i></b><i></i></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES>MCSniffs</span></i></b><i></i></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES>P.D.: No necesita responder este correo debido a que ha sido generado por un proceso automático.</span></i></b><i></i></p>"
                    +"</div></body></html>";

            mail.setContent(body);
            Boolean smail = emailService.sendEmail(mail);
            if(smail){
                return  repo.actualizarSolicitudPlantacionForestal(request); 
            }else{
                envio.setMessage("No se ha enviado la notificación al solicitante");
            }
        }

        return envio;
    }

    @Override
    public ResultClassEntity notificarPasSolicitante(SolPlantacionForestalEntity request) {
        
        ResultClassEntity envio = new ResultClassEntity();
        Gson gson = new Gson();

        UsuarioDto user= new UsuarioDto();
        user.setIdusuario(request.getIdSolicitante());

        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), request.getToken());
        ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
        String correo = null;
        String nombreCompleto = null;
        
        for(Object item: resultApiExt.getData().stream().toArray()) {

            LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
            correo= val.get("correoElectronico").toString();
            nombreCompleto = val.get("nombres").toString()+" " + val.get("apellidoPaterno").toString()+ " " +val.get("apellidoMaterno").toString();
        }

        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        if (correo!=null) {
            String[] mails=new String[1];
            mails[0]= correo;
            EmailEntity mail=new EmailEntity();
            mail.setSubject("Resgistro el PAS (Procedimiento administrativo sancionador)" + request.getIdSolicitudPlantacionForestal());
            mail.setEmail(mails);

            String body="<html><head>"
                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                    +"</head>"
                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                    +"<div class=WordSection1>"
                    +"<p class=MsoNormal>Estimado(s) <span lang=ES> "+nombreCompleto+"</span></p>";

            mail.setContent(body);
            Boolean smail = emailService.sendEmail(mail);
            if(smail){
                envio.setSuccess(true);
                envio.setMessage("Se ha enviado la notificación al solicitante");
                  repo.actualizarSolicitudPlantacionForestal(request); 
                  return envio;
            }else{
                envio.setSuccess(false);
                envio.setMessage("No se ha enviado la notificación al solicitante");
            }
        }

        return envio;
    }

    @Override
    public ResultClassEntity notificarPassSerfor(SolPlantacionForestalEntity param) {
        
        ResultClassEntity response = new ResultClassEntity();
        Gson gson = new Gson();

        UsuarioDto user = new UsuarioDto();
        user.setPerfil("SERFOR");
        user.setRegion("");


        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/listarPorRegionPerfilUsuario", gson.toJson(user), param.getToken());

        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        if(responseApiExt.getIsSuccess()){

            ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
            String[] mails = new String[resultApiExt.getData().size()];
            int cont = 0;
            for(Object item: resultApiExt.getData().stream().toArray()){
                LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
                mails[cont]=val.get("correoElectronico").toString();
                cont++;
            }
            if(mails.length==0){
                response.setMessage("Resgistro el PAS (Procedimiento administrativo sancionador) correctamente , pero no se envio la notificación");
                return new ResultClassEntity(response);
            }
            EmailEntity mail = new EmailEntity();
            mail.setSubject("Resgistro el PAS (Procedimiento administrativo sancionador) " + param.getIdSolicitudPlantacionForestal().toString());
            mail.setEmail(mails);
            String body="<html><head>"
                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                    +"</head>"
                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                    +"<div class=WordSection1>"
                    +"<p class=MsoNormal><span lang=ES>Estimado(a): </span></p>"
                    +"<p class=MsoNormal><span lang=ES> Resgistro el PAS (Procedimiento administrativo sancionador) : "+ param.getIdSolicitudPlantacionForestal().toString()+" </span></p>"
                    +"<p class=MsoNormal><span lang=ES>  Se encuentra pendiente</span></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                    +"</div></body></html>";
            mail.setContent(body);

            Boolean smail = serMail.sendEmail(mail);

            if(smail){
                response.setSuccess(true);
                response.setMessage("Se ha enviado la notificación al SERFOR");
                repo.actualizarSolicitudPlantacionForestal(param); 
                return response; 



            }else{
                response.setSuccess(false);
                response.setMessage("No se ha enviado la notificación al SERFOR");
            }


        }
        return response;
    }

    @Override
    public ResultClassEntity registrarAreaBloque(AreaBloqueEntity param) {
        return repo.registrarAreaBloque( param );
    }

    @Override
    public ResultClassEntity listarAreaBloque(AreaBloqueEntity param) {
        return repo.listarAreaBloque( param );
    }

    @Autowired
    private CoreCentralRepository coreCentralRepository;

    @Override
    public ResultArchivoEntity descargarSolicitudPlantacionesForestales(DescargarSolicitudPlantacionForestalDto obj) throws Exception {
        
        XWPFDocument doc = getDoc("PlantacionesForestales.docx");

        SolPlantacionForestalEntity params = new SolPlantacionForestalEntity();
        params.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacionForestal());

        //1. INFORMACION DEL SOLICITANTE
            PersonaEntity titular = new PersonaEntity();
            PersonaEntity ReprLegal = new PersonaEntity();
            List<SolPlantacionForestalEntity> infoGeneral = new ArrayList<>();

            ResultClassEntity data = repo.listarSolicitudPlantacionForestal(params);

            if (data != null && data.getSuccess()) {

                infoGeneral = (List<SolPlantacionForestalEntity>) data.getData();

                if (infoGeneral != null && !infoGeneral.isEmpty() && infoGeneral.get(0) != null) {
                    titular = infoGeneral.get(0).getSolicitante();
                    ReprLegal = infoGeneral.get(0).getRepresentanteLegal();
                    
                }
            }

        DocUtilPlantacionForestal.setearInformacionSolicitante_1(titular, ReprLegal, doc, 0);

        //2. INFORMACION DEL AREA
            SolPlantacionForestalAreaEntity predio = new SolPlantacionForestalAreaEntity();
            SolPlantacionForestalAreaEntity param = new SolPlantacionForestalAreaEntity();
            param.setIdSolPlantaForestalArea(obj.getIdSolicitudPlantacionForestal());

            ResultEntity result = repo.listarSolPlantacionForestalArea(param);

            if (result != null && result.getIsSuccess()) {
                List<SolPlantacionForestalAreaEntity> lista = (List<SolPlantacionForestalAreaEntity>) result.getData();
                if (lista != null && !lista.isEmpty() && lista.get(0) != null) {
                    predio = lista.get(0);
                    //altura promedio
                }
            }
            // Dueño del Predio  --> Titular 
            DocUtilPlantacionForestal.setearInformacionDelArea(predio, titular,infoGeneral.get(0), doc, 1);

        //3. INFORMACION GENERAL DEL AREA PLANTADA
            AreaSistemaPlantacionDto paramArea = new AreaSistemaPlantacionDto();
            EspecieForestalSistemaPlantacionEspecieDto esp = new EspecieForestalSistemaPlantacionEspecieDto();
            

            List<AreaSistemaPlantacionDto> listaAreaplantada = new ArrayList();
            paramArea.setIdSolPlantForest(obj.getIdSolicitudPlantacionForestal());

            ResultClassEntity respAreaPlan = areaSistemaPlantacionRepository.listarAreaSistemaPlantacion(paramArea);

            if (result != null && result.getIsSuccess()) {
                listaAreaplantada = (List<AreaSistemaPlantacionDto>) respAreaPlan.getData();

                for (AreaSistemaPlantacionDto element : listaAreaplantada) {
                    esp.setIdAreaSistemaPlantacion(element.getIdAreaSistemaPlantacion());
                    esp.setPageNum(1);
                    esp.setPageSize(1000);
                    ResultClassEntity resultEsp = areaSistemaPlantacionRepository.ListarEspecieForestalSistemaPlantacionEspecie(esp);
                    List<EspecieForestalSistemaPlantacionEspecieDto> objList = (List<EspecieForestalSistemaPlantacionEspecieDto>) resultEsp.getData();
                    List<EspecieForestalSistemaPlantacionEspecieDto> filtroId = objList.stream()
                    .filter(c -> c.getIdAreaSistemaPlantacion().equals(element.getIdAreaSistemaPlantacion()))
                    .collect(Collectors.toList());
                    if(filtroId != null && !filtroId.isEmpty()){
                        element.setEspeciesEstablecidas(true);
                    }
                }

                
            }
            // Area total --> Predio
            DocUtilPlantacionForestal.setearInformacionGeneralDelArea(listaAreaplantada, predio, doc, 2);

        //4. DETALLE DE PLANTACION FORESTAL
            List<SolicitudPlantacionForestalDetalleEntity> listaEsp = new ArrayList<>();
            List<AreaBloqueEntity> listaBloques = new ArrayList<>();
            Integer totalArbolesExistentes = 0;
            Integer id = obj.getIdSolicitudPlantacionForestal();
            ResultClassEntity resultEsp = repo.listarEspeciesSistemaPlantacion(id);

            AreaBloqueEntity request = new AreaBloqueEntity();
            request.setIdSolPlantForest(obj.getIdSolicitudPlantacionForestal());

            //request.setBloque("BLOQUE 1");
            ResultClassEntity resultListAreaBloque = repo.listarListaBloque(request);

            if (resultListAreaBloque != null && resultListAreaBloque.getSuccess()) {
                listaBloques = (List<AreaBloqueEntity>) resultListAreaBloque.getData();
            }

            if(resultEsp != null && resultEsp.getSuccess()){
                listaEsp = (List<SolicitudPlantacionForestalDetalleEntity>) resultEsp.getData();
                totalArbolesExistentes = sumaTotalArbolesExistentes(listaEsp);
            }
            
            DocUtilPlantacionForestal.setearDetallePlantacionForestal(listaEsp,totalArbolesExistentes, doc, 3);
            DocUtilPlantacionForestal.setearAlturaPromedio(predio, doc, 4);
            DocUtilPlantacionForestal.setearBloquesSector(listaBloques,predio, doc, 5);
            


        
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("PlantacionesForestales.docx");
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());
        word.setSuccess(true);
        return word;
    }





    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    private Integer sumaTotalArbolesExistentes (List<SolicitudPlantacionForestalDetalleEntity> listaEsp){
        Integer suma = 0;
        for (SolicitudPlantacionForestalDetalleEntity element : listaEsp) {
            suma += element.getTotalArbolesExistentes();
        }
        return suma;
    }

    @Override
    public ResultClassEntity generarNroRegistro(SolPlantacionForestalEntity obj)  throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        SolPlantacionForestalEntity objReg=new SolPlantacionForestalEntity();

        objReg.setIdSolicitudPlantacionForestal(obj.getIdSolicitudPlantacionForestal());
        objReg.setNroRegistro("15-LIM/REG-PLT-2021-001");
        objReg.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
        repo.actualizarSolicitudPlantacionForestal(objReg);

        result.setSuccess(true);
        result.setMessage("Se generó el nro. de registro correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity registrarSolPlantacionForestalPersona(List<SolPlantacionForestalPersonaEntity> request)
            throws Exception {

        return repo.registrarSolPlantacionForestalPersona(request);

    }


    @Override
    public ResultClassEntity listarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request)
            throws Exception {

        return repo.listarSolPlantacionForestalPersona(request);

    }

    @Override
    public ResultClassEntity eliminarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request)
            throws Exception {

        return repo.eliminarSolPlantacionForestalPersona(request);

    }


}

