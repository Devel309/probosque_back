package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudEntity;

public interface SolicitudAprovechamientoCcnnService {
    ResultClassEntity ObtenerSolicitudAprovechamientoCcnn(SolicitudEntity solicitud)throws Exception;
}
