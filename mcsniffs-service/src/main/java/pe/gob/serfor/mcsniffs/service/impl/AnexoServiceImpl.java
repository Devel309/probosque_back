package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.AdjuntoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.EliminarDocAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.GeneralAnexoEntity;
import pe.gob.serfor.mcsniffs.entity.ListarProcesoPosEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaDto;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UsuarioSolicitanteEntity;
import pe.gob.serfor.mcsniffs.entity.ValidarDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaRequisitosDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ListaInformeDocumentoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ObtenerDocumentoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ValidarAnexoDto;
import pe.gob.serfor.mcsniffs.repository.AnexoRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.service.AnexoService;
import pe.gob.serfor.mcsniffs.service.CondicionMinimaService;
import pe.gob.serfor.mcsniffs.service.util.DocUtil;

@Service
public class AnexoServiceImpl implements AnexoService {
    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los anexos}
     *
     */
    @Autowired
    AnexoRepository repo;

    @Autowired
    private CondicionMinimaService condicionMinimaService;

    
    @Autowired
    private FileServerConexion fileServerConexion;

    @Autowired
    private ProcesoPostulacionRepository procesoPostulacionRepository;

    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {se obtiene la informacion del usuario}
     * @param:Anexo1Entity
     */
    @Override
    public ResultClassEntity<UsuarioSolicitanteEntity> obtenerUsuarioSolicitante(AnexoRequestEntity filtro) {
        return repo.obtenerUsuarioSolicitante(filtro);
    }

    @Override
    public ResultClassEntity<PersonaDto> obtenerUsuario(AnexoRequestEntity obj) {


        ListarProcesoPosEntity proc = new ListarProcesoPosEntity();
        proc.setIdProcesoPostulacion(obj.getIdProcesoPostulacion());
        proc.setPageNum(1);
        proc.setPageSize(1);
        ResultEntity resultProceso = procesoPostulacionRepository.listarProcesoPostulacion(proc);


        ProcesoPostulacionEntity d = new ProcesoPostulacionEntity();

        if (resultProceso!=null && resultProceso.getIsSuccess() && resultProceso.getData()!=null ) {
            List<ProcesoPostulacionEntity> listaPp = (List<ProcesoPostulacionEntity>) resultProceso.getData();

            if (listaPp!=null && !listaPp.isEmpty() && listaPp.get(0)!=null) {
                d = listaPp.get(0);
            }
        }

        PersonaDto o = new PersonaDto();
        o.setIdUsuario(d.getIdUsuarioPostulacion());

        ResultClassEntity result = repo.obtenerUsuario(o);

        if(result!=null && result.getSuccess()){
            PersonaDto data = (PersonaDto) result.getData();
            if(data.getCodTipoDoc().equals("TDOCRUC")) { 
                PersonaDto obj2 = new PersonaDto();
                obj2.setIdRepLegal(data.getIdPersona());
                ResultClassEntity result2 = repo.obtenerUsuario(obj2);
                data.setRepresentanteLegal((PersonaDto) result2.getData());
                ResultClassEntity result3 =  new ResultClassEntity();
                result3.setData(data);
                return result3;
            }
        }

        return result;
    }

    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 1 para que el usuario lo firme}
     * @param:Anexo1Entity
     */

    @Override
    public ResultArchivoEntity generarAnexo1(AnexoRequestEntity filtro) {
        return repo.generarAnexo1(filtro);
    }


    /**
     * @autor: JaquelineDB [19-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 2 para que el usuario lo firme}
     * @param:Anexo1Entity
     */
    @Override
    public ResultArchivoEntity generarAnexo2(AnexoRequestEntity filtro) {
        return repo.generarAnexo2(filtro);
    }

    /**
     * @autor: JaquelineDB [19-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 3 para que el usuario lo firme}
     * @param:Anexo1Entity
     */
    @Override
    public ResultArchivoEntity generarAnexo3(AnexoRequestEntity filtro) {
        return repo.generarAnexo3(filtro);
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Descarga el anexo 4}
     * @param:Anexo1Entity
     */
    @Override
    public ResultArchivoEntity descargarAnexo4() {
        return repo.descargarAnexo4();
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Obtiene parametros del anexo}
     * @param:AnexoRequestEntity
     */
    @Override
    public ResultEntity<GeneralAnexoEntity> ObtenerAnexo(AnexoRequestEntity filtro) {
        return repo.ObtenerAnexo(filtro);
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Se adjunta el anexo firmado}
     * @param:AnexoRequestEntity
     */
    @Override
    public ResultClassEntity AdjuntarAnexo(MultipartFile file, Integer IdProcesoPostulacion, Integer IdUsuarioAdjunta, String CodigoAnexo,String NombreArchivo,Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto) {
        return repo.AdjuntarAnexo(file,IdProcesoPostulacion,IdUsuarioAdjunta,CodigoAnexo,NombreArchivo,IdTipoDocumento,IdPostulacionPFDM, idDocumentoAdjunto);
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Obtiene los documentos adjuntos por el usuario}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<DocumentoAdjuntoEntity> ObtenerAdjuntos(AdjuntoRequestEntity filtro) {
        return repo.ObtenerAdjuntos(filtro);
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Cuando la un anexo esta incorrecto}
     * @param:AnexoRequestEntity
     */
    @Override
    public ResultClassEntity ValidarAnexo(ValidarDocumentoEntity anexo) {
        return repo.ValidarAnexo(anexo);
    }

    /**
     * @autor: JaquelineDB [19-07-2021]
     * @modificado:
     * @descripción: {Inactivar documentos adjuntos por el usuario}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultClassEntity eliminarDocumentoAdjunto(EliminarDocAdjuntoEntity obj) {
        return repo.eliminarDocumentoAdjunto(obj);
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene el estatus y obs del anexo}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultClassEntity insertarEstatusAnexo(AnexoRequestEntity filtro) {
        return repo.insertarEstatusAnexo(filtro);
    }

    /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene registra el estatus de los anexos}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultClassEntity actualizarEstatusAnexo(AnexoRequestEntity filtro) {
        return repo.actualizarEstatusAnexo(filtro);
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene el estatus y obs del anexo}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<ValidarDocumentoEntity> obtenerEstatusAnexo(AnexoRequestEntity filtro) {
        return repo.obtenerEstatusAnexo(filtro);
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene el detalle de las observaciones}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<ValidarDocumentoEntity> obtenerDetalleObservacion(AnexoRequestEntity filtro) {
        return repo.obtenerDetalleObservacion(filtro);
    }

    @Override
    public ResultClassEntity listarDocumentos(AdjuntoRequestEntity filtro) {
        return repo.listarDocumentos(filtro);
    }

    @Override
    public ResultClassEntity guardarObservacion(List<DocumentoAdjuntoEntity> params) {
        return repo.guardarObservacion(params);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity adjuntarListAnexo(List<DocumentoAdjuntoDto> lista) {

        ResultClassEntity result = new ResultClassEntity<>();


        if (lista!=null && !lista.isEmpty()) {
            AdjuntoRequestEntity filtro = new AdjuntoRequestEntity();
            filtro.setIdProcesoPostulacion(lista.get(0).getIdProcesoPostulacion());
            filtro.setIdTipoDocumento(lista.get(0).getIdTipoDocumento());

            ResultClassEntity data = repo.listarDocumentos(filtro);

            if (data!=null && data.getData()!=null) {
                
                List<DocumentoAdjuntoEntity> lstresult = (List<DocumentoAdjuntoEntity>)data.getData();
                if (lstresult !=null && !lstresult.isEmpty() && lstresult.size()>=5) {
                    result = new ResultClassEntity<>();
                    result.setSuccess(true);
                    result.setValidateBusiness(false);
                    result.setMessage("Ha alcanzado el límite máximo de 5 evidencias o documentos a publicar");
                    return result;
                }
            }    
        }


        for (int i = 0; i < lista.size(); i++) {
         //   lista.get(i).setNombreGenerado(lista.get(i).getNombreGenerado());
         //   lista.get(i).setNombreDocumento(lista.get(i).getNombreDocumento());
            result = repo.registrarDocumentoAdjunto(lista.get(i));
        }

        result.setSuccess(true);
        return result;
    }

    // @Override
    // @Transactional(rollbackFor = Exception.class)
    // public ResultClassEntity adjuntarListAnexo(List<MultipartFile> files, List<DocumentoAdjuntoDto> lista) {

    //     ResultClassEntity result = new ResultClassEntity<>();


    //     if (lista!=null && !lista.isEmpty()) {
    //         AdjuntoRequestEntity filtro = new AdjuntoRequestEntity();
    //         filtro.setIdProcesoPostulacion(lista.get(0).getIdProcesoPostulacion());
    //         filtro.setIdTipoDocumento(lista.get(0).getIdTipoDocumento());

    //         ResultClassEntity data = repo.listarDocumentos(filtro);

    //         if (data!=null && data.getData()!=null) {
                
    //             List<DocumentoAdjuntoEntity> lstresult = (List<DocumentoAdjuntoEntity>)data.getData();
    //             if (lstresult !=null && !lstresult.isEmpty() && lstresult.size()>=5) {
    //                 result = new ResultClassEntity<>();
    //                 result.setSuccess(true);
    //                 result.setValidateBusiness(false);
    //                 result.setMessage("Ha alcanzado el límite máximo de 5 evidencias o documentos a publicar");
    //                 return result;
    //             }
    //         }    
    //     }

    //     String nombreGenerado = null;
    //     for (int i = 0; i < files.size(); i++) {
    //         nombreGenerado = fileServerConexion.uploadFile(files.get(i));
    //         nombreGenerado=((!nombreGenerado.equals("")?nombreGenerado:files.get(i).getOriginalFilename()));
            
    //         lista.get(i).setNombreGenerado(nombreGenerado);
    //         lista.get(i).setNombreDocumento(files.get(i).getOriginalFilename());

    //         result = repo.registrarDocumentoAdjunto(lista.get(i));
    //     }

    //     result.setSuccess(true);
    //     return result;
    // }

    @Override    
    public ResultArchivoEntity generarInformeGeneral(ObtenerDocumentoDto dto, String token) throws Exception {

        ConsultaRequisitosDto consulta = new ConsultaRequisitosDto();
        consulta.setApellidoPaterno(dto.getApellidoPaterno());
        consulta.setApellidoMaterno(dto.getApellidoMaterno());
        consulta.setNombres(dto.getNombres());
        consulta.setFechaEvaluacion(dto.getFechaEvaluacion());
        consulta.setNroDocumento(dto.getNroDocumento());
        consulta.setTipoDocumento(dto.getTipoDocumento());

        ResultClassEntity result = condicionMinimaService.validarCondicionesMinimas(consulta, token);

        List<ListaInformeDocumentoDto> listaCondicionesMinimas = new ArrayList<>();
        ListaInformeDocumentoDto d = new ListaInformeDocumentoDto();

        if (result!= null && result.getData()!=null) {
            ConsultaRequisitosDto response = (ConsultaRequisitosDto)result.getData();

            d.setDescripcion("No figura en el Registro Nacional de Infractores");
            d.setResultado(response.getResultadoInfractor());
            listaCondicionesMinimas.add(d);

            d = new ListaInformeDocumentoDto();
            d.setDescripcion("No cuenta con condenas Vigentes (Antecedentes Penales)");
            d.setResultado(response.getResultadoAntecedentePenal());
            listaCondicionesMinimas.add(d);

            d = new ListaInformeDocumentoDto();
            d.setDescripcion("No cuenta con condenas Vigentes (Antecedentes Judiciales)");
            d.setResultado(response.getResultadoAntecedenteJudicial());
            listaCondicionesMinimas.add(d);

            d = new ListaInformeDocumentoDto();
            d.setDescripcion("No cuenta con condenas Vigentes (Antecedentes Policiales PNP)");
            d.setResultado(response.getResultadoAntecedentePolicial());
            listaCondicionesMinimas.add(d);

            d = new ListaInformeDocumentoDto();
            d.setDescripcion("No cuenta con impedimento para contratar con el Estado");
            d.setResultado(response.getResultadoSancionVigenteOsce());
            listaCondicionesMinimas.add(d);    
        }
      

        XWPFDocument doc = getDoc("plantillaEvaluacionSolicitudPermisos.docx");

        
        DocUtil.setearCondicionesMinimas(listaCondicionesMinimas, doc);


        ValidarAnexoDto valid = new ValidarAnexoDto();
        valid.setIdProcesoPostulacion(dto.getIdProcesoPostulacion());
        valid.setDescripcion("anexo2");

        List<ValidarAnexoDto> lista = repo.listarValidarAnexos(valid);
 

        if (lista!=null && !lista.isEmpty()) {
            DocUtil.setearCapacidadJustificacion(lista.get(0).getDevolucion(), lista.get(0).getObservacion(), 1, doc); 
        }else{
            DocUtil.setearCapacidadJustificacion(null, "NO EVALUADO", 3, doc); 
        }

        valid = new ValidarAnexoDto();
        valid.setIdProcesoPostulacion(dto.getIdProcesoPostulacion());
        valid.setDescripcion("anexo3");
        lista = repo.listarValidarAnexos(valid);

        if (lista!=null && !lista.isEmpty()) {
            DocUtil.setearCapacidadJustificacion(lista.get(0).getDevolucion(), lista.get(0).getObservacion(), 2, doc); 
        }else{
            DocUtil.setearCapacidadJustificacion(null, "NO EVALUADO", 4, doc); 
        }

        ByteArrayOutputStream b = new ByteArrayOutputStream();
 
        doc.write(b);
        doc.close();  
        
        
        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("plantillaEvaluacionSolicitudPermisos.docx");
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());
        return word;
    }

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }
}
