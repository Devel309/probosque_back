package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDto;

import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalEvaluacionRepository;
import pe.gob.serfor.mcsniffs.service.SolPlantacionForestalEvaluacionService;

import java.util.List;

@Service("SolicitudPlantacionForestalEvaluacionService")
public class SolPlantacionForestalEvaluacionServiceImpl  implements SolPlantacionForestalEvaluacionService {

    @Autowired
    private SolPlantacionForestalEvaluacionRepository repository;

    @Override
    public ResultClassEntity obtenerEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDto obj) throws Exception {
        return repository.listarSolicitudPlantacionForestalEvaluacion(obj);
    }

    @Override
    public ResultClassEntity obtenerDetalleEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDetalleDto obj) throws Exception {
        return repository.listarSolicitudPlantacionForestalEvaluacionDetalle(obj);
    }

    @Override
    public ResultClassEntity registrarEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDto obj) throws Exception {
        return repository.registrarSolicitudPlantacionForestalEvaluacion(obj);
    }

    @Override
    public ResultClassEntity actualizarEvaluacionSolicitudPlantacionForestal(SolPlantacionForestalEvaluacionDto obj) throws Exception {
        return repository.actualizarSolicitudPlantacionForestalEvaluacion(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarDetalleEvaluacionSolicitudPlantacionForestal(List<SolPlantacionForestalEvaluacionDetalleDto> lst) throws Exception{

        ResultClassEntity result = new ResultClassEntity<>();

        if (lst != null && !lst.isEmpty()) {
            for (SolPlantacionForestalEvaluacionDetalleDto item : lst) {
                if (item.getIdSolPlantacionForestalEvaluacionDetalle() != null && item.getIdSolPlantacionForestalEvaluacionDetalle() > 0)
                    result = repository.actualizarSolicitudPlantacionForestalEvaluacionDetalle(item);
                else
                    result = repository.registrarSolicitudPlantacionForestalEvaluacionDetalle(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen detalles de evaluación por procesar.");
        }

        return result;

    }
}
