package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Colindancia.ColindanciaPredioDto;
import pe.gob.serfor.mcsniffs.repository.ColindanciaPredioRepository;
import pe.gob.serfor.mcsniffs.service.ColindanciaPredioService;

@Service("ColindanciaPredioService")
public class ColindanciaPredioServiceImpl implements ColindanciaPredioService{
    
    private static final Logger log = LogManager.getLogger(EvaluacionServiceImpl.class);

    @Autowired
    private ColindanciaPredioRepository ColindanciaPredioRepository;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarColindanciaPredio(List<ColindanciaPredioDto> dto) throws Exception {

        ResultClassEntity result = null;
        for (ColindanciaPredioDto element : dto) {
            result = ColindanciaPredioRepository.registrarColindanciaPredio(element);
        }

        return  result;
    }

    @Override
    public ResultClassEntity<List<ColindanciaPredioDto>> listarColindanciaPredio(ColindanciaPredioDto dto) throws Exception {        
      
        List<ColindanciaPredioDto> lista = ColindanciaPredioRepository.listarColindanciaPredio(dto);
        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");

        return  result;
    }

}
