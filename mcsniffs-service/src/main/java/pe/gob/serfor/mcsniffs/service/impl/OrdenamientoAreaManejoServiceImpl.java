package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoAreaManejoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoAreaManejoRepository;
import pe.gob.serfor.mcsniffs.service.OrdenamientoAreaManejoService;

import java.util.List;

@Service("OrdenamientoAreaManejoService")
public class OrdenamientoAreaManejoServiceImpl implements OrdenamientoAreaManejoService {
    private static final Logger log = LogManager.getLogger(OrdenamientoAreaManejoServiceImpl.class);

    @Autowired
    private OrdenamientoAreaManejoRepository ordenamientoAreaManejoRepository;

    @Override
    public ResultClassEntity RegistrarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param,MultipartFile file) throws Exception {
        return ordenamientoAreaManejoRepository.RegistrarOrdenamientoAreaManejo(param,file);
    }

    @Override
    public ResultClassEntity ListarOrdenamientoAreaManejo(PlanManejoEntity param) throws Exception {
        return ordenamientoAreaManejoRepository.ListarOrdenamientoAreaManejo(param);
    }

    @Override
    public ResultClassEntity EliminarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param) throws Exception {
        return ordenamientoAreaManejoRepository.EliminarOrdenamientoAreaManejo(param);
    }

    @Override
    public List<Dropdown> ComboCategoriaOrdenamiento(ParametroEntity param) throws Exception {
        return ordenamientoAreaManejoRepository.ComboCategoriaOrdenamiento(param);
    }

    @Override
    public ResultClassEntity ObtenerOrdenamientoAreaManejoArchivo(OrdenamientoAreaManejoEntity param) throws Exception {
        return ordenamientoAreaManejoRepository.ObtenerOrdenamientoAreaManejoArchivo(param);
    }

}
