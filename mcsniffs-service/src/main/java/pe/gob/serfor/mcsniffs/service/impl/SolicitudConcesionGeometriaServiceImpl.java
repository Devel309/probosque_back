package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionGeometriaRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionGeometriaService;

import java.util.List;

@Service("serviceSolicitudConcesionGeometria")
public class SolicitudConcesionGeometriaServiceImpl implements SolicitudConcesionGeometriaService {

    @Autowired
    private SolicitudConcesionGeometriaRepository repository;

    /**
     * @autor: Jordy Zamata [10-01-2022]
     * @modificado:
     * @descripción: {Registrar la geometria}
     * @param:List<SolicitudConcesionGeometriaEntity>
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarSolicitudConcesionGeometria(List<SolicitudConcesionGeometriaEntity> items) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                repository.registrarSolicitudConcesionGeometria(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se registraron correctamente.");
        result.setSuccess(true);
        return result;
    }



    /**
     * @autor: Jordy Zamata [11-01-2021]
     * @modificado:
     * @descripción: {Lista las geometrias activas}
     * @param:SolicitudConcesionGeometriaEntity
     */
    @Override
    public ResultClassEntity listarSolicitudConcesionGeometria(SolicitudConcesionGeometriaEntity item) {
        return repository.listarSolicitudConcesionGeometria(item);
    }

    @Override
    public ResultClassEntity obtenerInformacionAreaGeometriaSolicitada(SolicitudConcesionGeometriaEntity item) {
        return repository.listarSolicitudConcesionGeometria(item);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarInformacionAreaGeometriaSolicitada(List<SolicitudConcesionGeometriaEntity> items) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();

        for (SolicitudConcesionGeometriaEntity element : items) {
            result = repository.registrarSolicitudConcesionGeometria(element);
        }

        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarInformacionAreaGeometriaSolicitada(List<SolicitudConcesionGeometriaEntity> items) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();

        for (SolicitudConcesionGeometriaEntity element : items) {
            result = repository.actualizarSolicitudConcesionGeometria(element);
        }

        return result;
    }

    /**
     * @autor: Jordy Zamata [10-01-2022]
     * @modificado:
     * @descripción: {Eliminar PlanManejoGeometria archivo y/o capa}
     * @param:idArchivo, idUsuario
     */
    @Override
    public ResultClassEntity eliminarSolicitudConcesionGeometriaArchivo(SolicitudConcesionGeometriaEntity item) {
        return repository.eliminarSolicitudConcesionGeometriaArchivo(item);
    }

    @Override
    public ResultClassEntity eliminarInformacionAreaGeometriaSolicitada(SolicitudConcesionGeometriaEntity item) {
        return repository.eliminarSolicitudConcesionGeometriaArchivo(item);
    }

}
