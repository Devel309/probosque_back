package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueActividadService {
    ResultClassEntity RegistrarManejoBosqueActividad(List<ManejoBosqueActividadEntity> list)throws Exception;
    ResultClassEntity ListarManejoBosqueActividad(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarManejoBosqueActividad(ManejoBosqueActividadEntity param)throws Exception;
}
