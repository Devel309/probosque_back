package pe.gob.serfor.mcsniffs.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFinalidadDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalFinalidadRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalFinalidadService;

@Service("SolicitudBosqueLocalFinalidadService")
public class SolicitudBosqueLocalFinalidadServiceImpl implements SolicitudBosqueLocalFinalidadService {

    @Autowired
    private SolicitudBosqueLocalFinalidadRepository solicitudBosqueLocalFinalidadRepository;


    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalFinalidad(List<SolicitudBosqueLocalFinalidadDto> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        for (SolicitudBosqueLocalFinalidadDto e : param) {
            result = solicitudBosqueLocalFinalidadRepository.registrarSolicitudBosqueLocalFinalidad(e);
        }
        
        return result;
    }

    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto param) throws Exception {
        return  solicitudBosqueLocalFinalidadRepository.eliminarSolicitudBosqueLocalFinalidad(param);
    }

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto param) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        result.setData(solicitudBosqueLocalFinalidadRepository.listarSolicitudBosqueLocalFinalidad(param));
        return result;
    }


    
}
