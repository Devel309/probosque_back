package pe.gob.serfor.mcsniffs.service;


import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface SolicitudConcesionResponsableService {
    ResultClassEntity obtenerSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) throws Exception;
    ResultClassEntity registrarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) throws Exception;
    ResultClassEntity actualizarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) throws Exception;

}
