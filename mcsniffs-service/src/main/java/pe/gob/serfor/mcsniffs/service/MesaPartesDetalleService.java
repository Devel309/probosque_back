package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;

public interface MesaPartesDetalleService {
    ResultClassEntity<List<MesaPartesDetalleDto>> listarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception;
    ResultClassEntity registrarMesaPartesDetalle(List<MesaPartesDetalleDto> dto) throws Exception;
    ResultClassEntity eliminarMesaPartesDetalle(List<MesaPartesDetalleDto> dto) throws Exception;
}
