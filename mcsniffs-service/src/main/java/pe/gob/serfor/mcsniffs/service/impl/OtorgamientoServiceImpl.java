package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.OtorgamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.OtorgamientoRepository;
import pe.gob.serfor.mcsniffs.service.OtorgamientoService;

@Service("OtorgamientoService")
public class OtorgamientoServiceImpl implements OtorgamientoService {
    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado:
     * @descripción: {Repository Otorgamiento}
     *
     */
    @Autowired
    OtorgamientoRepository otorgamientoRepository;

    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado:
     * @descripción: {Registrar Otorgamiento}
     *
     */
    @Override
    public ResultClassEntity registrarOtorgamiento(OtorgamientoEntity obj) throws Exception {
        return otorgamientoRepository.registrarOtorgamiento(obj);
    }

    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado:
     * @descripción: {Listar Otorgamiento}
     *
     */
    @Override
    public ResultClassEntity<OtorgamientoEntity> listarOtorgamiento(OtorgamientoEntity filtro) throws Exception {
        return otorgamientoRepository.listarOtorgamiento(filtro);
    }

    /**
     * @autor: Danny Nazario [09-12-2021]
     * @modificado:
     * @descripción: {Eliminar cabecera y detalle de Otorgamiento}
     *
     */
    @Override
    public ResultClassEntity<OtorgamientoEntity> eliminarOtorgamiento(OtorgamientoEntity obj) throws Exception {
        return otorgamientoRepository.eliminarOtorgamiento(obj);
    }

    /**
     * @autor: Danny Nazario [23-12-2021]
     * @modificado: Danny Nazario [21-01-2022]
     * @descripción: {Listar Otorgamiento por filtro}
     * @param: OtorgamientoEntity
     */
    @Override
    public ResultClassEntity listarPorFiltroOtorgamiento(String nroDocumento, String nroRucEmpresa, Integer idPlanManejo) throws Exception {
        return otorgamientoRepository.listarPorFiltroOtorgamiento(nroDocumento, nroRucEmpresa, idPlanManejo);
    }
}
