package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

import java.util.List;
import java.util.Map;

public interface AprovechamientoService {
    ResultClassEntity RegistrarAprovechamiento(AprovechamientoEntity aprovechamientoEntity) throws Exception;

    ResultClassEntity<List<AprovechamientoEntity>> ListarAprovechamiento(Integer idPlanManejo) throws Exception;

}
