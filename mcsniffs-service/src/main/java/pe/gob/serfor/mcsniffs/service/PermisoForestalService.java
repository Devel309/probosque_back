package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;

public interface PermisoForestalService {
    ResultClassEntity<Boolean> archivoEliminar(Integer idArchivo); 

    ResultClassEntity ObtenerPersonaPorUsuario(Integer idUsuario);

    ResultClassEntity obtenerSolicitudPorAccesoSolicitud(Integer idSolicitud, Integer idSolicitudAcceso);

    ResultClassEntity registrarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity);

    ResultClassEntity editarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity);

    ResultArchivoEntity descargarArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivos);

    ResultArchivoEntity descargarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAdjunto, Integer idSolicitud, String tipoArchivos);

    ResultClassEntity eliminarArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivos);

    ResultClassEntity eliminarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAjunto, Integer idSolicitud, String tipoArchivo);

    List<ArchivoSolicitudEntity> obtenerArchivoSolicitud(Integer idSolicitud);

    List<ArchivoSolicitudEntity> obtenerArchivoSolicitudAdjunto(Integer idSolicitud);

    ResultClassEntity<ArchivoEntity> registrarArchivoSolicitud(MultipartFile file, Integer idSolicitud, String tipoArchivo, Boolean flagActualiza);

    ResultClassEntity<ArchivoEntity> registrarArchivoSolicitudAdjunto(MultipartFile file, Integer idSolicitud, String tipoArchivo, String descripcionArchivo,Boolean flagActualiza);

    ResultClassEntity ListarPermisosForestales(SolicitudEntity request);

    ResultEntity RegistrarPermisosForestales(SolicitudEntity request);


    ResultEntity ObtenerPermisosForestales(SolicitudEntity request);

    ResultClassEntity validarRequisitoSolicitud(ValidacionRequisitoEntity entity);

    ResultClassEntity<ValidacionRequisitoEntity> evaluarRequisitoSolicitud(ValidacionRequisitoEntity entity);

    ResultClassEntity obtenerSolicitudValidacionPorSeccion(ValidacionRequisitoEntity entity);

    ResultClassEntity obtenerSolicitudValidacionPorSolicitud(ValidacionRequisitoEntity entity);

    ResultClassEntity obtenerSolicitudEvaluacionPorSolicitud(ValidacionRequisitoEntity entity);

    ResultClassEntity<Boolean> envioSolicitudEvaluacion(ValidacionRequisitoEntity entity);

    Pageable<List<PermisoForestalEntity>> filtrar(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso, String codEstado,
                                                  String codTipoPersona, String codTipoActor, String codTipoCncc, String numeroDocumento,
                                                  String nombrePersona, String razonSocial, Page page) throws Exception;

    Pageable<List<PermisoForestalEntity>> filtrarEval(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
                                                      String fechaRegistro, String codigoEstado, String codigoPerfil, Page page) throws Exception;

    ResultClassEntity registrarPermisoForestal(PermisoForestalEntity obj) throws Exception;

    ResultClassEntity actualizarEstadoPermisoForestal(PermisoForestalEntity obj) throws Exception;

    ResultClassEntity registrarInformacionGeneral(InformacionGeneralPermisoForestalEntity obj) throws Exception;

    ResultClassEntity<InformacionGeneralPermisoForestalEntity> listarInformacionGeneral(InformacionGeneralPermisoForestalEntity obj) throws Exception;
    ResultClassEntity<CodigoCifradoEntity> codigoCifrado(CodigoCifradoEntity obj) throws Exception;


    ResultClassEntity RegistrarArchivo(List<SolicitudSANArchivoEntity> list) throws Exception;
    List<SolicitudSANArchivoEntity> ListarArchivo(SolicitudSANArchivoEntity param) throws Exception;
    ResultClassEntity EliminarArchivo(SolicitudSANArchivoEntity solicitudSANArchivoEntity) throws Exception;

    ResultClassEntity listarPorFiltroPlanManejo(String nroDocumento, String codTipoPlan) throws Exception;
    ResultClassEntity registrarPermisoForestalPlanManejo(List<PermisoForestalPlanManejoEntity> items) throws Exception;
    ResultClassEntity listarPermisoForestalPlanManejo (PermisoForestalPlanManejoEntity item) throws Exception;
    ResultClassEntity eliminarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception;

    ResultArchivoEntity descargarPlantillaFormatoPermiso() throws Exception;
    ResultArchivoEntity descargarPlantillaResolAprobacion() throws Exception;
    ResultArchivoEntity descargarPlantillaResolDesAprobacion() throws Exception;
    ResultClassEntity permisoForestalActualizarRemitido(PermisoForestalPlanManejoEntity request)throws Exception;
    ResultArchivoEntity plantillaPermisosForestales(Integer idPermisoForestal) throws Exception;
    ResultClassEntity actualizarPermisoForestal(PermisoForestalEntity obj) throws Exception;
}



