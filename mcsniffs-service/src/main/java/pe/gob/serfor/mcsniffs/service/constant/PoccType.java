package pe.gob.serfor.mcsniffs.service.constant;

/**
 * 
 * @author Rafael Azaña
 */
public final class PoccType {

	private PoccType() {
	}
	public static final String TITULAR_NOMBRE = "1titular";
	public static final String REPRESENTANTE_NOMBRE = "1representante";
	public static final String NRO_CONTRATO = "1numerocontrato";
	public static final String REPRESENTANTE_DNI = "1nrodni";
	public static final String REPRESENTANTE_RUC = "1nroruc";
	public static final String DISTRITO = "1distrito";
	public static final String DEPARTAMENTO = "1departamento";
	public static final String PROVINCIA = "1provincia";
	public static final String OBJETIVO = "1objetivo";
	public static final String NRO_PO = "1nroPO";
	public static final String FECHA_PRESENTACION = "1fechaPO";
	public static final String DURACION_PO = "1duracionPO";
	public static final String FECHA_INICIO = "1FechaIni";
	public static final String FECHA_FIN = "1FechaFin";
	public static final String NRO_PC = "1nroPC";
	public static final String AREA_TOTAL = "1areaTotal";
	public static final String AREA_BOSQUE = "1areaBosque";
	public static final String AREA_PROTECCION = "1areaProtec";
	public static final String VOLUMEN_TOTAL = "1Volumentotal";
	public static final String VOLUMEN_CANTIDAD = "1volumenCantidad";
	public static final String NOMBRE_REGENTE = "1regente";
	public static final String DOMICILIO_LEGAL = "1domicilio";
	public static final String CIP = "1nroCIP";
	public static final String CONTRATO_TITULAR = "1contratoTitular";
	public static final String NRO_INSCRIPCION = "1nroInscripcion";

	//2 RESUMEN DE ACTIVIDAD
	public static final String NRO_RESOLUCION2 = "21nroresolucion";
	public static final String NRO_PC2 = "21nroPC";
	public static final String NRO_AREA2 = "21area";
}
