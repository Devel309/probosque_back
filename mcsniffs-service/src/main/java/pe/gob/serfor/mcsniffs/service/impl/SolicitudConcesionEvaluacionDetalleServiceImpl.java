package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionEvaluacionDetalleService;

@Service
public class SolicitudConcesionEvaluacionDetalleServiceImpl implements SolicitudConcesionEvaluacionDetalleService {

    @Autowired
    private SolicitudConcesionEvaluacionDetalleRepository solicitudConcesionEvaluacionDetalleRepository;


    @Override
    public ResultClassEntity registrarSolicitudConcesionEvaluacionDetalle(List<SolicitudConcesionEvaluacionDetalleDto> lsobj) throws Exception{
        return solicitudConcesionEvaluacionDetalleRepository.registrarSolicitudConcesionEvaluacionDetalle(lsobj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarSolicitudConcesionEvaluacionDetalle(List<SolicitudConcesionEvaluacionDetalleDto> obj) throws Exception{

        ResultClassEntity result = new ResultClassEntity<>();

        for (SolicitudConcesionEvaluacionDetalleDto element : obj) {

            result = solicitudConcesionEvaluacionDetalleRepository.actualizarSolicitudConcesionEvaluacionDetalle(element);
        }

        return result;

    }

    
}
