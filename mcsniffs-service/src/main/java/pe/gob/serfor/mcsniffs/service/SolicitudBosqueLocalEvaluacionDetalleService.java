package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;

import java.util.List;

public interface SolicitudBosqueLocalEvaluacionDetalleService {

    ResultClassEntity registrarEvaluacionDetallesSolicitudBosqueLocal(List<SolicitudBosqueLocalEvaluacionDetalleDto> items) throws Exception;
    ResultClassEntity listarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj)throws Exception;
}
