package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ClonarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarResAdminFavDesfavDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ParametroRepository;
import pe.gob.serfor.mcsniffs.repository.ResponsableExperienciaLaboralRepository;
import pe.gob.serfor.mcsniffs.repository.ResponsableFormacionAcademicaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionActividadRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionAreaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionCriterioAreaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionIngresoCuentaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionResponsableRepository;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;
import pe.gob.serfor.mcsniffs.service.util.DocUtilSolicitudConcesion;

@Service("SolicitudConcesionService")
public class SolicitudConcesionServiceImpl implements SolicitudConcesionService {

    @Autowired
    private SolicitudConcesionRepository solicitudConcesionRepository;

    @Autowired
    private SolicitudConcesionAreaRepository solicitudConcesionAreaRepository;

    @Autowired
    private SolicitudConcesionEvaluacionRepository solicitudConcesionEvaluacionRepository;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private EmailService emailService;
    @Autowired
    private SolicitudConcesionActividadRepository solicitudConcesionActividadRepository;

    @Autowired
    private SolicitudConcesionResponsableRepository solicitudConcesionResponsableRepository;

    @Autowired
    private ResponsableFormacionAcademicaRepository responsableFormacionAcademicaRepository;

    @Autowired
    private ResponsableExperienciaLaboralRepository responsableExperienciaLaboralRepository;

    @Autowired
    SolicitudConcesionIngresoCuentaRepository solicitudConcesionIngresoCuentaRepository;

    @Autowired
    ParametroRepository parametroRepository;

    @Autowired
    SolicitudConcesionCriterioAreaRepository solicitudConcesionCriterioAreaRepository;

    @Autowired
    ArchivoRepository archivoRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;


    @Override
    public ResultClassEntity obtenerInformacionSolicitanteSolicitado(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.listarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity listarSolicitudConcesionPorFiltro(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.listarSolicitudConcesionPorFiltro(obj);

    }

    @Override
    public ResultClassEntity listarSolicitudConcesionCalificacionPorFiltro(SolicitudConcesionCalificacionDto obj)
            throws Exception {
        return solicitudConcesionRepository.listarSolicitudConcesionCalificacionPorFiltro(obj);

    }

    @Override
    public ResultClassEntity listarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.listarSolicitudConcesion(obj);

    }

    @Override
    public ResultClassEntity registrarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.registrarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity registrarInformacionSolicitante(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.registrarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity registrarInformacionSobreSolicitado(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.registrarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.actualizarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity actualizarInformacionSolicitante(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.actualizarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity actualizarInformacionSobreSolicitado(SolicitudConcesionDto obj) throws Exception {
        return solicitudConcesionRepository.actualizarSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception {

        ResultClassEntity resultClassEntity = solicitudConcesionRepository.eliminarSolicitudConcesion(obj);


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUDCONCESION,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MVC_SOLICITUDCONCESION + obj.getIdSolicitudConcesion(),
                obj.getIdUsuarioElimina());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    @Override
    public ResultClassEntity obtenerArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception {
        return solicitudConcesionRepository.listarSolicitudConcesionArchivo(obj);
    }

    @Override
    public ResultClassEntity registrarArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception {
        return solicitudConcesionRepository.registrarSolicitudConcesionArchivo(obj);
    }

    @Override
    public ResultClassEntity actualizarArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception {
        return solicitudConcesionRepository.actualizarSolicitudConcesionArchivo(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarArchivosSolicitudConcesion(List<SolicitudConcesionArchivoEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (items != null && !items.isEmpty()) {
            for (SolicitudConcesionArchivoEntity item : items) {
                if (item.getIdSolicitudConcesionArchivo() > 0)
                    result = solicitudConcesionRepository.actualizarSolicitudConcesionArchivo(item);
                else
                    result = solicitudConcesionRepository.registrarSolicitudConcesionArchivo(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen archivos por registrar.");
        }

        return result;
    }

    @Override
    public ResultClassEntity eliminarArchivoSolicitudConcesion(SolicitudConcesionArchivoEntity obj) throws Exception {
        return solicitudConcesionRepository.eliminarSolicitudConcesionArchivo(obj);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarSolicitudConcesionAnexo(List<SolicitudConcesionAnexoDto> obj) throws Exception {

        ResultClassEntity resultado = null;

        for (SolicitudConcesionAnexoDto anexos : obj) {

            resultado = solicitudConcesionRepository.registrarSolicitudConcesionAnexo(anexos);

            if (anexos.getConforme() && anexos.getSolicitudConcesionArchivo() != null) {
                if (anexos.getSolicitudConcesionArchivo().getIdSolicitudConcesionArchivo() == null) {
                    solicitudConcesionRepository
                            .registrarSolicitudConcesionArchivo(anexos.getSolicitudConcesionArchivo());
                } else {
                    solicitudConcesionRepository
                            .actualizarSolicitudConcesionArchivo(anexos.getSolicitudConcesionArchivo());
                }
            }
        }
        return resultado;
    }

    @Override
    public ResultClassEntity listarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto) throws Exception {

        ResultClassEntity resultado = new ResultClassEntity<>();

        List<SolicitudConcesionAnexoDto> lista = solicitudConcesionRepository.listarSolicitudConcesionAnexo(dto);

        for (SolicitudConcesionAnexoDto e : lista) {

            SolicitudConcesionArchivoEntity a = new SolicitudConcesionArchivoEntity();
            a.setIdSolicitudConcesion(e.getIdSolicitudConcesion());
            // a.setCodigoSeccion(e.getCodigoAnexo());
            a.setCodigoSubSeccion(dto.getSolicitudConcesionArchivo().getCodigoSubSeccion());
            a.setTipoArchivo(e.getCodigoAnexo());

            ResultClassEntity result = solicitudConcesionRepository.listarSolicitudConcesionArchivo(a);

            if (result != null && result.getSuccess()) {

                List<SolicitudConcesionArchivoEntity> lista2 = (List<SolicitudConcesionArchivoEntity>) result.getData();

                if (lista2 != null && !lista2.isEmpty() && lista2.get(0) != null) {

                    e.setSolicitudConcesionArchivo(lista2.get(0));
                }
            }
        }
        resultado.setData(lista);
        resultado.setSuccess(true);

        return resultado;
    }

    @Override
    public ResultArchivoEntity descargarSolicitudConcesionAnexo(DescargarSolicitudConcesionDto obj) throws Exception {

        XWPFDocument doc = new XWPFDocument();
        ResultArchivoEntity word = new ResultArchivoEntity();

        try {

            if (obj.getTipoAnexo() == 1) {

                doc = getDoc("PlantillaSolicitudConcesionAnexo1.docx");

                SolicitudConcesionDto listaObj = new SolicitudConcesionDto();
                SolicitudConcesionAreaEntity objAreaS = new SolicitudConcesionAreaEntity();
                SolicitudConcesionDto id = new SolicitudConcesionDto();
                id.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionAreaEntity idArea = new SolicitudConcesionAreaEntity();
                idArea.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionActividadEntity idAct = new SolicitudConcesionActividadEntity();
                idAct.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                ResultClassEntity infoSolicitante = solicitudConcesionRepository.listarSolicitudConcesion(id);

                if (infoSolicitante != null && infoSolicitante.getSuccess()) {
                    List<SolicitudConcesionDto> objInfoSolicitante = (ArrayList<SolicitudConcesionDto>) infoSolicitante
                            .getData();
                    if (objInfoSolicitante != null && !objInfoSolicitante.isEmpty()
                            && objInfoSolicitante.get(0) != null) {
                        listaObj = objInfoSolicitante.get(0);
                    }
                }

                ResultClassEntity infoAreaSolicitada = solicitudConcesionAreaRepository
                        .listarSolicitudConcesionArea(idArea);

                if (infoAreaSolicitada != null && infoAreaSolicitada.getSuccess()) {
                    List<SolicitudConcesionAreaEntity> objAreaSolicitada = (ArrayList<SolicitudConcesionAreaEntity>) infoAreaSolicitada
                            .getData();
                    if (objAreaSolicitada != null && !objAreaSolicitada.isEmpty() && objAreaSolicitada.get(0) != null) {
                        objAreaS = objAreaSolicitada.get(0);
                    }
                }
                DocUtilSolicitudConcesion.setearInformacionGeneralAnexo1(listaObj, objAreaS, doc, 0);

                List<SolicitudConcesionActividadEntity> listAct = new ArrayList<>();
                List<SolicitudConcesionActividadEntity> listActCom = new ArrayList<>();

                ResultClassEntity infoActividades = solicitudConcesionActividadRepository
                        .listarActividadSolicitudConcesion(idAct);

                if (infoActividades != null && infoActividades.getSuccess()) {
                    List<SolicitudConcesionActividadEntity> ListInfoActividades = (ArrayList<SolicitudConcesionActividadEntity>) infoActividades
                            .getData();
                    if (ListInfoActividades != null && !ListInfoActividades.isEmpty()) {
                        listAct = ListInfoActividades.stream()
                                .filter(c -> c.getComplementario() == false)
                                .collect(Collectors.toList());
                        listActCom = ListInfoActividades.stream()
                                .filter(c -> c.getComplementario() == true)
                                .collect(Collectors.toList());
                    }
                }

                DocUtilSolicitudConcesion.setearInformacionGeneralAnexo1_2(listaObj, listAct, listActCom, obj, objAreaS,doc, 1);
                word.setNombeArchivo("PlantillaSolicitudConcesionAnexo1.docx");
            }

            // ANEXO 2
            if (obj.getTipoAnexo() == 2) {

                doc = getDoc("PlantillaSolicitudConcesionAnexo2.docx");

                List<SolicitudConcesionDto> listaSolicitudConcesion = new ArrayList<>();
                List<ResponsableFormacionAcademicaEntity> listaFormacion = new ArrayList<>();
                List<ResponsableExperienciaLaboralDto> listaExperiencia = new ArrayList<ResponsableExperienciaLaboralDto>();

                SolicitudConcesionResponsableDto solConceRespon = new SolicitudConcesionResponsableDto();
                solConceRespon.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                ResultClassEntity resultConcesionResponsable = solicitudConcesionResponsableRepository
                        .obtenerSolicitudConcesionResponsable(solConceRespon);

                if (resultConcesionResponsable != null && resultConcesionResponsable.getSuccess()) {

                    solConceRespon = (SolicitudConcesionResponsableDto) resultConcesionResponsable.getData();

                    SolicitudConcesionDto solConce = new SolicitudConcesionDto();
                    solConce.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                    ResultClassEntity resultSolicitudConcesion = solicitudConcesionRepository
                            .listarSolicitudConcesion(solConce);

                    ResponsableFormacionAcademicaEntity responsable = new ResponsableFormacionAcademicaEntity();
                    responsable.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                    ResultClassEntity resultFormacion = responsableFormacionAcademicaRepository
                            .listarResponsableFormacionAcademica(responsable);

                    ResponsableExperienciaLaboralDto responExperiencia = new ResponsableExperienciaLaboralDto();
                    responExperiencia
                            .setIdSolicitudConcesionResponsable(solConceRespon.getIdSolicitudConcesionResponsable());
                    ResultClassEntity resultExperiencia = new ResultClassEntity<>();
                    resultExperiencia = null;

                    if (responExperiencia.getIdSolicitudConcesionResponsable() != null) {
                        resultExperiencia = responsableExperienciaLaboralRepository
                                .obtenerResponsableExperienciaLaboral(responExperiencia);
                    }

                    if (resultSolicitudConcesion != null && resultSolicitudConcesion.getSuccess()) {
                        listaSolicitudConcesion = (List<SolicitudConcesionDto>) resultSolicitudConcesion.getData();
                    }

                    if (resultFormacion != null && resultFormacion.getSuccess()) {
                        listaFormacion = (List<ResponsableFormacionAcademicaEntity>) resultFormacion.getData();
                    }

                    if (resultExperiencia != null && resultExperiencia.getSuccess()) {
                        listaExperiencia = (List<ResponsableExperienciaLaboralDto>) resultExperiencia.getData();
                    }
                }
                DocUtilSolicitudConcesion.setearSolcitudConcesionAnexo2(solConceRespon, listaSolicitudConcesion,
                        listaFormacion, listaExperiencia, obj, doc);
                word.setNombeArchivo("PlantillaSolicitudConcesionAnexo2.docx");
            }

            // ANEXO 3
            if (obj.getTipoAnexo() == 3) {
                doc = getDoc("PlantillaSolicitudConcesionAnexo3.docx");

                SolicitudConcesionDto id = new SolicitudConcesionDto();
                id.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionAreaEntity idArea = new SolicitudConcesionAreaEntity();
                idArea.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionIngresoCuentaDto idCuenta = new SolicitudConcesionIngresoCuentaDto();
                idCuenta.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                SolicitudConcesionDto listaObj = new SolicitudConcesionDto();
                SolicitudConcesionAreaEntity objArea = new SolicitudConcesionAreaEntity();
                List<SolicitudConcesionIngresoCuentaDto> listIngrCuent = new ArrayList<SolicitudConcesionIngresoCuentaDto>();
                // listarSolicitudConcesion
                ResultClassEntity infoSolicitante = solicitudConcesionRepository.listarSolicitudConcesion(id);

                if (infoSolicitante != null && infoSolicitante.getSuccess()) {

                    ResultClassEntity infoAreaSolicitada = solicitudConcesionAreaRepository
                            .listarSolicitudConcesionArea(idArea);
                    ResultClassEntity lisIngresoCuenta = solicitudConcesionIngresoCuentaRepository
                            .listarSolicitudConcesionIngresoCuenta(idCuenta);

                    List<SolicitudConcesionDto> objInfoSolicitante = (ArrayList<SolicitudConcesionDto>) infoSolicitante
                            .getData();
                    if (objInfoSolicitante != null && !objInfoSolicitante.isEmpty()
                            && objInfoSolicitante.get(0) != null) {
                        listaObj = objInfoSolicitante.get(0);
                    }

                    if (infoAreaSolicitada != null && infoAreaSolicitada.getSuccess()) {
                        List<SolicitudConcesionAreaEntity> objAreaSolicitada = (ArrayList<SolicitudConcesionAreaEntity>) infoAreaSolicitada
                                .getData();
                        if (objAreaSolicitada != null && !objAreaSolicitada.isEmpty()
                                && objAreaSolicitada.get(0) != null) {
                            objArea = objAreaSolicitada.get(0);
                        }
                    }
                    if (lisIngresoCuenta != null && lisIngresoCuenta.getSuccess()) {
                        listIngrCuent = (ArrayList<SolicitudConcesionIngresoCuentaDto>) lisIngresoCuenta.getData();
                    }

                }

                DocUtilSolicitudConcesion.setearInformacionGeneralAnexo3(listaObj, objArea, listIngrCuent, obj, doc, 0);
                word.setNombeArchivo("PlantillaSolicitudConcesionAnexo3.docx");
            }

            // ANEXO 4
            if (obj.getTipoAnexo() == 4) {
                doc = getDoc("PlantillaSolicitudConcesionAnexo4.docx");

                SolicitudConcesionDto id = new SolicitudConcesionDto();
                id.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionAreaEntity idArea = new SolicitudConcesionAreaEntity();
                idArea.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionCriterioAreaEntity idCriterio = new SolicitudConcesionCriterioAreaEntity();
                idCriterio.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                SolicitudConcesionDto listaObj = new SolicitudConcesionDto();
                SolicitudConcesionAreaEntity objArea = new SolicitudConcesionAreaEntity();
                List<SolicitudConcesionCriterioAreaEntity> list2 = new ArrayList<SolicitudConcesionCriterioAreaEntity>();
                List<SolicitudConcesionArchivoEntity> panelList = new ArrayList<SolicitudConcesionArchivoEntity>();
                SolicitudConcesionArchivoEntity panelFoto = new SolicitudConcesionArchivoEntity();

                SolicitudConcesionArchivoEntity param = new SolicitudConcesionArchivoEntity();
                param.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                param.setCodigoSeccion("PFDMA4");
                param.setCodigoSubSeccion("PFDMA4_SS3");

                ResultClassEntity panelFotoR = solicitudConcesionRepository.listarSolicitudConcesionArchivo(param);

                if (panelFotoR != null && panelFotoR.getSuccess()) {
                    panelList = (ArrayList<SolicitudConcesionArchivoEntity>) panelFotoR.getData();
                    if (panelList != null && !panelList.isEmpty() && panelList.get(0) != null) {
                        panelFoto = panelList.get(0);
                    }

                }

                ResultClassEntity infoSolicitante = solicitudConcesionRepository.listarSolicitudConcesion(id);

                if (infoSolicitante != null && infoSolicitante.getSuccess()) {
                    List<SolicitudConcesionDto> objInfoSolicitante = (ArrayList<SolicitudConcesionDto>) infoSolicitante
                            .getData();

                    ResultClassEntity infoAreaSolicitada = solicitudConcesionAreaRepository
                            .listarSolicitudConcesionArea(idArea);
                    ResultClassEntity infoCriterio = solicitudConcesionCriterioAreaRepository
                            .listarSolicitudConcesionCriterioArea(idCriterio);

                    listaObj = objInfoSolicitante.get(0);
                    if (infoAreaSolicitada != null && infoAreaSolicitada.getSuccess()) {
                        List<SolicitudConcesionAreaEntity> objAreaSolicitada = (ArrayList<SolicitudConcesionAreaEntity>) infoAreaSolicitada
                                .getData();
                        if (objAreaSolicitada != null && !objAreaSolicitada.isEmpty()
                                && objAreaSolicitada.get(0) != null) {
                            objArea = objAreaSolicitada.get(0);
                        }
                    }
                    if (infoCriterio != null && infoCriterio.getSuccess()) {
                        list2 = (ArrayList<SolicitudConcesionCriterioAreaEntity>) infoCriterio.getData();
                    }
                }
                DocUtilSolicitudConcesion.setearInformacionGeneralAnexo4(listaObj, objArea, list2, panelFoto, obj, doc,
                        0);
                word.setNombeArchivo("PlantillaSolicitudConcesionAnexo4.docx");
            }

            // ANEXO 5
            if (obj.getTipoAnexo() == 5) {
                doc = getDoc("PlantillaSolicitudConcesionAnexo5.docx");
                SolicitudConcesionDto id = new SolicitudConcesionDto();
                id.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionAreaEntity idArea = new SolicitudConcesionAreaEntity();
                idArea.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionDto objSoli = new SolicitudConcesionDto();
                SolicitudConcesionAreaEntity objArea = new SolicitudConcesionAreaEntity();

                ResultClassEntity infoSolicitante = solicitudConcesionRepository.listarSolicitudConcesion(id);

                if (infoSolicitante != null && infoSolicitante.getSuccess()) {

                    List<SolicitudConcesionDto> objInfoSolicitante = (ArrayList<SolicitudConcesionDto>) infoSolicitante
                            .getData();
                    if (objInfoSolicitante != null && !objInfoSolicitante.isEmpty()
                            && objInfoSolicitante.get(0) != null) {
                        objSoli = objInfoSolicitante.get(0);
                    }

                    ResultClassEntity infoAreaSolicitada = solicitudConcesionAreaRepository
                            .listarSolicitudConcesionArea(idArea);

                    if (infoAreaSolicitada != null && infoAreaSolicitada.getSuccess()) {
                        List<SolicitudConcesionAreaEntity> objAreaSolicitada = (ArrayList<SolicitudConcesionAreaEntity>) infoAreaSolicitada
                                .getData();
                        if (objAreaSolicitada != null && !objAreaSolicitada.isEmpty()
                                && objAreaSolicitada.get(0) != null) {
                            objArea = objAreaSolicitada.get(0);
                        }

                    }

                }

                DocUtilSolicitudConcesion.setearInformacionGeneralAnexo5(objSoli, objArea, obj, doc, 0);
                word.setNombeArchivo("PlantillaSolicitudConcesionAnexo5.docx");
            }

            // ANEXO 6
            if (obj.getTipoAnexo() == 6) {
                doc = getDoc("PlantillaSolicitudConcesionAnexo6.docx");
                // archivoRepository

                SolicitudConcesionDto id = new SolicitudConcesionDto();
                id.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionAreaEntity idArea = new SolicitudConcesionAreaEntity();
                idArea.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionActividadEntity idAct = new SolicitudConcesionActividadEntity();
                idAct.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                List<SolicitudConcesionActividadEntity> listActCom = new ArrayList<>();

                SolicitudConcesionArchivoEntity financiero = new SolicitudConcesionArchivoEntity();
                SolicitudConcesionArchivoEntity presupuesto = new SolicitudConcesionArchivoEntity();
                SolicitudConcesionArchivoEntity experiencia = new SolicitudConcesionArchivoEntity();
                SolicitudConcesionArchivoEntity mapaUbicacion = new SolicitudConcesionArchivoEntity();
                SolicitudConcesionArchivoEntity mapaBase = new SolicitudConcesionArchivoEntity();

                SolicitudConcesionDto objSoli = new SolicitudConcesionDto();
                SolicitudConcesionAreaEntity objArea = new SolicitudConcesionAreaEntity();
                SolicitudConcesionArchivoEntity param = new SolicitudConcesionArchivoEntity();
                SolicitudConcesionArchivoEntity param2 = new SolicitudConcesionArchivoEntity();
                SolicitudConcesionArchivoEntity param3 = new SolicitudConcesionArchivoEntity();
                param.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                param.setCodigoSeccion("PFDMA6");
                param.setCodigoSubSeccion("PFDMA6_SS6");
                param2.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                param2.setCodigoSeccion("PFDMA6");
                param2.setCodigoSubSeccion("PFDMA6_SS7");
                param3.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                param3.setCodigoSeccion("PFDMA6");
                param3.setCodigoSubSeccion("PFDMA6_SS8");

                ResultClassEntity sustentoFinancieroR = solicitudConcesionRepository
                        .listarSolicitudConcesionArchivo(param);
                ResultClassEntity experienciaR = solicitudConcesionRepository.listarSolicitudConcesionArchivo(param2);
                ResultClassEntity anexosR = solicitudConcesionRepository.listarSolicitudConcesionArchivo(param3);

                ResultClassEntity infoSolicitante = solicitudConcesionRepository.listarSolicitudConcesion(id);
                ResultClassEntity infoAreaSolicitada = solicitudConcesionAreaRepository
                        .listarSolicitudConcesionArea(idArea);
                ResultClassEntity infoActividades = solicitudConcesionActividadRepository
                        .listarActividadSolicitudConcesion(idAct);

                if (infoSolicitante != null && infoSolicitante.getSuccess()) {
                    List<SolicitudConcesionDto> objInfoSolicitante = (ArrayList<SolicitudConcesionDto>) infoSolicitante
                            .getData();
                    if (objInfoSolicitante != null && !objInfoSolicitante.isEmpty()
                            && objInfoSolicitante.get(0) != null) {
                        objSoli = objInfoSolicitante.get(0);
                    }
                }
                if (infoAreaSolicitada != null && infoAreaSolicitada.getSuccess()) {
                    List<SolicitudConcesionAreaEntity> objAreaSolicitada = (ArrayList<SolicitudConcesionAreaEntity>) infoAreaSolicitada
                            .getData();
                    if (objAreaSolicitada != null && !objAreaSolicitada.isEmpty() && objAreaSolicitada != null) {
                        objArea = objAreaSolicitada.get(0);
                    }
                }

                if (infoActividades != null && infoActividades.getSuccess()) {
                    List<SolicitudConcesionActividadEntity> ListInfoActividades = (ArrayList<SolicitudConcesionActividadEntity>) infoActividades
                            .getData();
                    if (ListInfoActividades != null && !ListInfoActividades.isEmpty()) {
                        listActCom = ListInfoActividades.stream()
                                .filter(c -> c.getComplementario() == true)
                                .collect(Collectors.toList());
                    }
                }

                if (sustentoFinancieroR != null && sustentoFinancieroR.getSuccess()) {
                    List<SolicitudConcesionArchivoEntity> sustentoFinanciero = (ArrayList<SolicitudConcesionArchivoEntity>) sustentoFinancieroR
                            .getData();
                    List<SolicitudConcesionArchivoEntity> financieroList = sustentoFinanciero.stream()
                            .filter(c -> c.getTipoArchivo().equals("Financiero"))
                            .collect(Collectors.toList());
                    if (financieroList != null && !financieroList.isEmpty() && financieroList.get(0) != null) {
                        financiero = financieroList.get(0);
                    }
                    List<SolicitudConcesionArchivoEntity> presupuestoList = sustentoFinanciero.stream()
                            .filter(c -> c.getTipoArchivo().equals("Presupuesto"))
                            .collect(Collectors.toList());
                    if (presupuestoList != null && !presupuestoList.isEmpty() && presupuestoList.get(0) != null) {
                        presupuesto = presupuestoList.get(0);
                    }
                }

                if (experienciaR != null && experienciaR.getSuccess()) {
                    List<SolicitudConcesionArchivoEntity> experienciaList = (ArrayList<SolicitudConcesionArchivoEntity>) experienciaR
                            .getData();
                    if (experienciaList != null && !experienciaList.isEmpty() && experienciaList.get(0) != null) {
                        experiencia = experienciaList.get(0);
                    }
                }
                if (anexosR != null && anexosR.getSuccess()) {
                    List<SolicitudConcesionArchivoEntity> anexos = (ArrayList<SolicitudConcesionArchivoEntity>) anexosR
                            .getData();
                    List<SolicitudConcesionArchivoEntity> mapaUbicacionLis = anexos.stream()
                            .filter(c -> c.getTipoArchivo().equals("PFDMA6_SS8_1"))
                            .collect(Collectors.toList());
                    List<SolicitudConcesionArchivoEntity> mapaBaseLis = anexos.stream()
                            .filter(c -> c.getTipoArchivo().equals("PFDMA6_SS8_2"))
                            .collect(Collectors.toList());

                    if (mapaUbicacionLis != null && !mapaUbicacionLis.isEmpty() && mapaUbicacionLis.get(0) != null) {
                        mapaUbicacion = mapaUbicacionLis.get(0);
                    }
                    if (mapaBaseLis != null && !mapaBaseLis.isEmpty() && mapaBaseLis.get(0) != null) {
                        mapaBase = mapaBaseLis.get(0);
                    }
                }

                DocUtilSolicitudConcesion.setearInformacionGeneralAnexo6(objSoli, objArea, listActCom, doc, 0, 1,
                        financiero, presupuesto, experiencia,
                        mapaUbicacion, mapaBase, obj);
                word.setNombeArchivo("PlantillaSolicitudConcesionAnexo6.docx");
            }

            /** ------- END WORKING -------------------- */

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();

            word.setTipoDocumento("EVALUACION");
            word.setArchivo(b.toByteArray());

            word.setSuccess(true);
            return word;

        } catch (Exception e) {
            word.setMessage("Ocurrio un Error.");
            word.setSuccess(false);
            return word;
        }

    }

    
    @Override
    public ResultArchivoEntity descargarPlantillaResolucionEvaluacionSolictud(DescargarResAdminFavDesfavDto obj) throws Exception {

    XWPFDocument doc = new XWPFDocument();
    ResultArchivoEntity word = new ResultArchivoEntity();

    try {

        if (obj.getCodRadio().equals("TRESEVALFAV")) doc = getDoc("PlantillaResolucionEvaluacionSolicitudF.docx");
        
        if (obj.getCodRadio().equals("TRESEVALDFA")) doc = getDoc("PlantillaResolucionEvaluacionSolicitudDesF.docx");

                SolicitudConcesionDto infoSoli = new SolicitudConcesionDto();
                SolicitudConcesionAreaEntity objArea = new SolicitudConcesionAreaEntity();
                SolicitudConcesionDto id = new SolicitudConcesionDto();
                id.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionAreaEntity idArea = new SolicitudConcesionAreaEntity();
                idArea.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                SolicitudConcesionActividadEntity idAct = new SolicitudConcesionActividadEntity();
                idAct.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                ResultClassEntity infoSolicitante = solicitudConcesionRepository.listarSolicitudConcesion(id);

                if (infoSolicitante != null && infoSolicitante.getSuccess()) {
                    List<SolicitudConcesionDto> objInfoSolicitante = (ArrayList<SolicitudConcesionDto>) infoSolicitante.getData();
                    if (objInfoSolicitante != null && !objInfoSolicitante.isEmpty()
                            && objInfoSolicitante.get(0) != null) {
                        infoSoli = objInfoSolicitante.get(0);
                    }
                }
                ResultClassEntity infoAreaSolicitada = solicitudConcesionAreaRepository.listarSolicitudConcesionArea(idArea);

                if (infoAreaSolicitada != null && infoAreaSolicitada.getSuccess()) {
                    List<SolicitudConcesionAreaEntity> objAreaSolicitada = (ArrayList<SolicitudConcesionAreaEntity>) infoAreaSolicitada.getData();
                    if (objAreaSolicitada != null && !objAreaSolicitada.isEmpty() && objAreaSolicitada.get(0) != null) {
                        objArea = objAreaSolicitada.get(0);
                    }
                }

                DocUtilSolicitudConcesion.SetearResolucionEvaluacionSolictud(infoSoli,objArea,obj,doc);

        /** -------- END WORKING -------------------- */

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());
        word.setContenTypeArchivo("application/octet-stream");
        word.setSuccess(true);
        word.setMessage("Se descargo plantilla de informe de evaluacion de solicitud.");
        word.setNombeArchivo("PlantillaResolucionEvaluacionSolicitud.docx");
        return word;

        } catch (Exception e) {
            word.setMessage("Ocurrio un Error.");
            word.setSuccess(false);
            return word;
        }

    }
    
    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    @Override
    public ResultArchivoEntity descargarPlantillaCriteriosCalificacionPropuesta(DescargarSolicitudConcesionDto obj)
            throws Exception {

        XWPFDocument doc = new XWPFDocument();
        ResultArchivoEntity word = new ResultArchivoEntity();

        try {
            doc = getDoc("PlantillaInformeEvaluacionPropuestas.docx");
            /** ------- END WORKING -------------------- */
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            word.setTipoDocumento("EVALUACION");
            word.setArchivo(b.toByteArray());
            word.setNombeArchivo("PlantillaInformeEvaluacionPropuestas.docx");
            word.setContenTypeArchivo("application/octet-stream");
            word.setSuccess(true);
            word.setMessage("Se descargo plantilla de informe de evaluacion de propuestas.");
            return word;

        } catch (Exception e) {
            word.setMessage("Ocurrio un Error.");
            word.setSuccess(false);
            return word;
        }

    }


    @Override
    public ResultClassEntity enviarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        ResultClassEntity rsSolConcesion = solicitudConcesionRepository.listarSolicitudConcesion(obj);
        List<SolicitudConcesionDto> lsResSC = (List<SolicitudConcesionDto>) rsSolConcesion.getData();
        SolicitudConcesionDto regSC = lsResSC.get(0);

        SolicitudConcesionDto objActualizar = null;
        switch (obj.getTipoEnvio()) {
            case "ENVIARSC": // Presentada

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                objActualizar.setEstadoSolicitud("SCESTPRESN");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);
                break;
            case "SUBSANAR": // Requisitos Observados

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                objActualizar.setEstadoSolicitud("SCESTREQO");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                objActualizar.setFechaRequisitoObs(new Date());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);

                regSC.setEstadoSolicitud("SCESTREQO");
                this.enviarEmail(regSC);

                break;
            case "EVALUADOR": // En Evaluación

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                objActualizar.setEstadoSolicitud("SCESTEEVAL");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                objActualizar.setFechaEvaluacion(new Date());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);

                SolicitudConcesionEvaluacionDto objEvaluacion = new SolicitudConcesionEvaluacionDto();
                objEvaluacion.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                objEvaluacion.setFechaInicio(new Date());
                objEvaluacion.setIdUsuarioRegistro(obj.getIdUsuarioModificacion());
                solicitudConcesionEvaluacionRepository.registrarSolicitudConcesionEvaluacion(objEvaluacion);

                regSC.setEstadoSolicitud("SCESTEEVAL");
                this.enviarEmail(regSC);
                break;

            case "EVALUACIONOBS": // solicitud observada

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                objActualizar.setEstadoSolicitud("SCESTSOLO");
                objActualizar.setFechaObservacion(new Date());
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);

                regSC.setEstadoSolicitud("SCESTSOLO");
                this.enviarEmail(regSC);
                break;

            case "APROBARSC": // solicitud publicada

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());

                if (regSC.getIdSolicitudConcesionPadre() == null) {
                    objActualizar.setEstadoSolicitud("SCESTPUB");

                } else {
                    objActualizar.setEstadoSolicitud("SCESTCALP");
                }

                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);
                break;
            case "DENEGARSC": // solicitud denegada

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                objActualizar.setEstadoSolicitud("SCESTSOLD");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);

                regSC.setEstadoSolicitud("SCESTSOLD");
                this.enviarEmail(regSC);
                break;

            case "FINALIZARSC": // solicitud denegada

                objActualizar = new SolicitudConcesionDto();
                objActualizar.setIdSolicitudConcesion(obj.getIdSolicitudConcesion());
                objActualizar.setEstadoSolicitud("SCESTFINO");
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                solicitudConcesionRepository.actualizarSolicitudConcesion(objActualizar);

                regSC.setEstadoSolicitud("SCESTFINO");
                this.enviarEmail(regSC);
                break;

        }

        result.setSuccess(true);
        result.setMessage("Se actualizó la solicitud de concesión correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity enviarComunicado(SolicitudConcesionDto obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        ResultClassEntity rsSolConcesion = solicitudConcesionRepository.listarSolicitudConcesion(obj);

        List<SolicitudConcesionDto> lsResSC = (List<SolicitudConcesionDto>) rsSolConcesion.getData();

        SolicitudConcesionDto regSC = lsResSC.get(0);
        if (regSC != null && regSC.getEstadoSolicitud().equals("SCESTPUB")) {

            Boolean estadoEnvio = this.enviarEmail(regSC);

            if (estadoEnvio) {
                SolicitudConcesionEvaluacionDto objListar = new SolicitudConcesionEvaluacionDto();
                objListar.setIdSolicitudConcesion(regSC.getIdSolicitudConcesion());

                ResultClassEntity rsSolConcesionEval = solicitudConcesionEvaluacionRepository
                        .listarSolicitudConcesionEvaluacion(objListar);
                List<SolicitudConcesionEvaluacionDto> lsResSCEval = (List<SolicitudConcesionEvaluacionDto>) rsSolConcesionEval
                        .getData();
                SolicitudConcesionEvaluacionDto regSCEval = lsResSCEval.get(0);

                SolicitudConcesionEvaluacionDto objActualizar = new SolicitudConcesionEvaluacionDto();
                objActualizar.setIdSolicitudConcesionEvaluacion(regSCEval.getIdSolicitudConcesionEvaluacion());
                objActualizar.setComunicadoEnviado(true);
                objActualizar.setIdUsuarioModificacion(obj.getIdUsuarioModificacion());
                solicitudConcesionEvaluacionRepository.actualizarSolicitudConcesionEvaluacion(objActualizar);

                result.setSuccess(true);
                result.setMessage("Se envió el comunicado correctamente.");
            } else {
                result.setSuccess(false);
                result.setMessage("Problemas en el envío");
            }

        } else {
            result.setSuccess(false);
            result.setMessage("No se encontró la solicitud");
        }

        return result;
    }

    public Boolean enviarEmail(SolicitudConcesionDto regSC) throws Exception {

        String correoElectronico = regSC.getCorreoElectronicoNotif();
        String nombreCompleto = regSC.getNombreSolicitanteUnico();
        Boolean smail = null;

        String asuntoMail = "";
        String mensajeMail = "";

        switch (regSC.getEstadoSolicitud()) {
            case "SCESTPUB":
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSC.getIdSolicitudConcesion() + " PUBLICADA";
                mensajeMail = "La solicitud N° : "
                        + regSC.getIdSolicitudConcesion() + " se autoriza su publicación";
                break;
            case "SCESTREQO":
            case "SCESTSOLO":
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSC.getIdSolicitudConcesion() + " OBSERVADA";
                mensajeMail = "La solicitud N° : " + regSC.getIdSolicitudConcesion()
                        + " ha sido observada";
                break;

            case "SCESTEEVAL":
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSC.getIdSolicitudConcesion() + " EN EVALUACIÓN";
                mensajeMail = "La solicitud N° : " + regSC.getIdSolicitudConcesion()
                        + " esta en evaluación";
                break;

            case "SCESTSOLD":
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSC.getIdSolicitudConcesion() + " DENEGADA";
                mensajeMail = "La solicitud N° : " + regSC.getIdSolicitudConcesion()
                        + " ha sido denegada";
                break;
            case "SCESTFINO":
                asuntoMail = "SERFOR::SOLICITUD Nº " + regSC.getIdSolicitudConcesion() + " FINALIZADA";
                mensajeMail = "La solicitud N° : " + regSC.getIdSolicitudConcesion()
                        + " ha sido finalizada por oposición";
                break;

        }

        if (correoElectronico != null && nombreCompleto != null) {
            String[] mails = new String[1];
            mails[0] = correoElectronico;
            EmailEntity mail = new EmailEntity();
            mail.setSubject(asuntoMail);
            mail.setEmail(mails);
            String body = Constantes.BODY_HTML_EMAIL;

            body = body.replace("[NOMBRES_USUARIO_ACTUAL]", nombreCompleto.toUpperCase());
            body = body.replace("[MENSAJE_DEL_PROCESO]", mensajeMail);
            body = body.replace("[NOTA_COMPLEMENTARIA]",
                    "Tenga presente que usted está recibiendo este correo porque ha realizado la creación de una solicitud PFDM en el aplicativo MCSNIFFS");

            mail.setContent(body);
            smail = emailService.sendEmail(mail);

        }

        return smail;
    }

    @Override
    public ResultArchivoEntity descargarPlantillaManejo() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/PLANTILLA_ACTIVIDADES_DE_MANEJO.xlsx");
            File fileCopi = File.createTempFile("PLANTILLA_ACTIVIDADES_DE_MANEJO", ".xlsx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("PLANTILLA_ACTIVIDADES_DE_MANEJO.xlsx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la plantilla.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {

            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultArchivoEntity descargarPlantillaPresupuesto() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/PLANTILLA_FLUJO_DE_CAJA.xlsx");
            File fileCopi = File.createTempFile("PLANTILLA_FLUJO_DE_CAJA", ".xlsx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("PLANTILLA_FLUJO_DE_CAJA.xlsx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la plantilla.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {

            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity clonarSolicitudConcesion(ClonarSolicitudConcesionDto obj) throws Exception {

        return solicitudConcesionRepository.clonarSolicitudConcesion(obj);
    }
}
