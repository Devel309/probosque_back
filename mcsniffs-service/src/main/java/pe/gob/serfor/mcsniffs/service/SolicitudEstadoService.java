package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.EstadoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface SolicitudEstadoService {
    ResultEntity ListaComboSolicitudEstado(EstadoSolicitudEntity request) ;
}
