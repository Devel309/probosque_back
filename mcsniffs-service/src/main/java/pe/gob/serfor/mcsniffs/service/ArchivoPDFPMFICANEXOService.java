package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPMFICANEXOService {
    ByteArrayResource consolidadoPMFICANEXO_PDF(Integer idPlanManejo) throws Exception;
}
