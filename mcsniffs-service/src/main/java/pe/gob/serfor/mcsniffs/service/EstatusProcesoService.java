package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.EstatusProceso.ListarEstadoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface EstatusProcesoService {

    ResultClassEntity<List<ListarEstadoDto>> listarEstadoEstatusProceso(ListarEstadoDto request);

}
