package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralDemaRepository;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralDemaService;

@Service
public class InformacionGeneralDemaServiceImpl implements InformacionGeneralDemaService{
    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {Repository  de la informacion general relacionada con el proceso DEMA}
     *
     */
    @Autowired
    InformacionGeneralDemaRepository repository;


    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {Registrar informacion general dema}
     *
     */
    @Override
    public ResultClassEntity registrarInformacionGeneralDema(InformacionGeneralDEMAEntity obj) {
        return repository.registrarInformacionGeneralDema(obj);
    }

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {actualizar informacion general dema}
     *
     */
    @Override
    public ResultClassEntity actualizarInformacionGeneralDema(InformacionGeneralDEMAEntity obj) {
        return repository.actualizarInformacionGeneralDema(obj);
    }

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {listar informacion general dema}
     *
     */
    @Override
    public ResultEntity<InformacionGeneralDEMAEntity> listarInformacionGeneralDema(
            InformacionGeneralDEMAEntity filtro) {
        return repository.listarInformacionGeneralDema(filtro);
    }

    /**
     * @autor: Danny Nazario [19-01-2022]
     * @modificado: Danny Nazario [04-02-2022]
     * @descripción: {Listar Información General por filtro}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPorFiltroInformacionGeneral(String nroDocumento, String nroRucEmpresa, String codTipoPlan, Integer idPlanManejo) throws Exception {
        return repository.listarPorFiltroInformacionGeneral(nroDocumento, nroRucEmpresa, codTipoPlan, idPlanManejo);
    }

}
