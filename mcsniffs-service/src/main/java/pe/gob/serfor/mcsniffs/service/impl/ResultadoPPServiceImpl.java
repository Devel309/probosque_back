package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.EvaluacionRequestEntity;
import pe.gob.serfor.mcsniffs.entity.OposicionEntity;
import pe.gob.serfor.mcsniffs.entity.OposicionRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoNotalListEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPNotasEntity;
import pe.gob.serfor.mcsniffs.repository.OposicionRepository;
import pe.gob.serfor.mcsniffs.repository.ResultadoPPRepository;
import pe.gob.serfor.mcsniffs.service.ResultadoPPService;

@Service
public class ResultadoPPServiceImpl implements ResultadoPPService {
    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Repository de los resultados de los procesos de postulacion}
     *
     */
    @Autowired
    ResultadoPPRepository respository;

    @Autowired
    OposicionRepository oposicionRepository;

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Lista los resultados de las evaluaciones}
     * @param:ResultadoPPEntity
     */
    @Override
    public ResultEntity<ResultadoPPEntity> ListarResultadosEvaluaciones(ResultadoPPEntity filtro) {
        return respository.ListarResultadosEvaluaciones(filtro);
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Registra y envia la resulusion a los participantes}
     * @param:ResultadoPPEntity
     */
    @Override
    public ResultClassEntity RegistrarResultados(ResultadoPPEntity obj) {
        return respository.RegistrarResultados(obj);
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Obtiene los resultados}
     * @param:IdResultadoPP
     */
    @Override
    public ResultClassEntity<ResultadoPPAdjuntoEntity> ObtenerResultados(Integer IdResultadoPP) {
        return respository.ObtenerResultados(IdResultadoPP);
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Registra las notas del proceso de postulacion}
     * @param:ResultadoPPNotasEntity
     */
    @Override
    public ResultClassEntity RegistrarNotas(ResultadoPPNotasEntity notas) {
        return respository.RegistrarNotas(notas);
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {obtiene las notas del proceso de postulacion}
     * @param:IdResultadoPP
     */
    @Override
    public ResultEntity<ResultadoNotalListEntity> ObtenerNotas(Integer IdResultadoPP) {
        return respository.ObtenerNotas(IdResultadoPP);
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {obtiene el primer proceso postulacion
     * @param:IdProcesoOferta
     */
    @Override
    public ResultClassEntity<ProcesoPostulacionEntity> ObtenerPrimerPP(Integer IdProcesoOferta) {
        return respository.ObtenerPrimerPP(IdProcesoOferta);
    }

    /**
     * @autor: JaquelineDB [13-07-2021]
     * @modificado:
     * @descripción: {Listar los procesos de postulacion que estan listos para ser evaluados
     * @param:EvaluacionRequestEntity
     */
    @Override
    public ResultClassEntity  ListarPPEvaluacion(EvaluacionRequestEntity filtro) {
        return respository.ListarPPEvaluacion(filtro);
    }
    @Override
    public ResultClassEntity  listarPorFiltroOtrogamientoProcesoPostulacion(EvaluacionRequestEntity filtro) {
        return respository.listarPorFiltroOtrogamientoProcesoPostulacion(filtro);
    }
    
    /**
     * @autor: JaquelineDB [14-07-2021]
     * @modificado:
     * @descripción: {Listar los procesos en oferta con potulaciones evaluadas
     * @param:
     */
    @Override
    public ResultClassEntity<ProcesoOfertaEntity> ListarProcesosOferta() {
        return respository.ListarProcesosOferta();
    }

    @Override
    public ResultClassEntity obtenerResultadoPPPorIdPP(Integer idProcesoPostulacion) {
        ResultClassEntity  result = new ResultClassEntity<>();

        OposicionRequestEntity req = new OposicionRequestEntity();
        req.setIdProcesoPostulacion(idProcesoPostulacion);
        ResultEntity<OposicionEntity> d = oposicionRepository.listarOposicion(req);

        if (d!=null && d.getData()!=null) {
            List<OposicionEntity> list = d.getData();
            if (list!=null && !list.isEmpty()) {
                
                result.setSuccess(true);
                result.setValidateBusiness(false);
                result.setMessage("El Id N° "+idProcesoPostulacion+ " Ya se encuentra registrado en oposición ");
                return result;
            }
        }

        result = respository.obtenerResultadoPPPorIdPP(idProcesoPostulacion);

        if (result.getData()!=null) {
            
            List<ResultadoPPEntity> list = (List<ResultadoPPEntity>)result.getData();
            if (list== null || list.isEmpty()) {
                result.setSuccess(true);
                result.setValidateBusiness(false);
                result.setMessage("El Id N° "+ idProcesoPostulacion +" ingresado no se encuentra registrado.");
                return result;
            }            
        }
        return result;
    }

}
