package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

import pe.gob.serfor.mcsniffs.entity.Dto.DecargarPDFSolBosqueLocalBeneficiario.DescargarPDFSolBeneficiario;

public interface ArchivoPDFSolicitudBosqueLocalBeneficiarioService{

    ByteArrayResource ArchivoPDFSolicitudBosqueLocalBeneficiario(DescargarPDFSolBeneficiario obj) throws Exception;
}
