package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.AspectoHidrograficoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface AspectoHidrograficoService {
    ResultClassEntity RegistrarAspectoHidrografico(List<AspectoHidrograficoEntity> lista) throws Exception;
    ResultClassEntity ActualizarAspectoHidrografico(List<AspectoHidrograficoEntity> lista) throws Exception;
    ResultClassEntity<AspectoHidrograficoEntity> ListarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception;
    ResultClassEntity<AspectoHidrograficoEntity> EliminarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception;

}
