package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAreaDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;

import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionAreaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionAreaService;

import java.util.List;

@Service("SolicitudConcesionAreaService")
public class SolicitudConcesionAreaServiceImpl implements SolicitudConcesionAreaService {

    @Autowired
    private SolicitudConcesionAreaRepository repository;

    @Autowired
    private SolicitudConcesionRepository repositorySolicitudConcesion;

    @Override
    public ResultClassEntity listarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) throws Exception {
        return repository.listarSolicitudConcesionArea(obj);
    }

    @Override
    public ResultClassEntity obtenerInformacionAreaSolicitada(SolicitudConcesionAreaEntity obj) throws Exception {
        return repository.listarSolicitudConcesionArea(obj);
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) throws Exception {
        return repository.registrarSolicitudConcesionArea(obj);
    }

    @Override
    public ResultClassEntity registrarInformacionAreaSolicitada(SolicitudConcesionAreaEntity obj) throws Exception {
        return repository.registrarSolicitudConcesionArea(obj);
    }

    @Override
    public ResultClassEntity actualizarInformacionAreaSolicitada(SolicitudConcesionAreaEntity obj) throws Exception {
        return repository.actualizarSolicitudConcesionArea(obj);
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) throws Exception {
        return repository.eliminarSolicitudConcesionArea(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarInformacionAreaSolicitudConcesion(SolicitudConcesionAreaDto dto)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();

        SolicitudConcesionAreaEntity obj = new SolicitudConcesionAreaEntity();
        obj.setIdSolicitudConcesionArea(dto.getIdSolicitudConcesionArea());
        obj.setIdSolicitudConcesion(dto.getIdSolicitudConcesion());
        obj.setFuenteBibliografica(dto.getFuenteBibliografica());

        obj.setCaractFisica(dto.getCaractFisica());
        obj.setCaractBiologica(dto.getCaractBiologica());
        obj.setCaractSocioeconomica(dto.getCaractSocioeconomica());
        obj.setFactorAmbientalBiologico(dto.getFactorAmbientalBiologico());
        obj.setFactorSocioEconocultural(dto.getFactorSocioEconocultural());
        obj.setManejoConcesion(dto.getManejoConcesion());

        obj.setIdUsuarioModificacion(dto.getIdUsuarioModificacion());

        result = repository.actualizarSolicitudConcesionArea(obj);

        List<SolicitudConcesionArchivoEntity> adjuntos = dto.getLstArchivo();

        if (adjuntos != null && !adjuntos.isEmpty()) {
            for (SolicitudConcesionArchivoEntity archivo : adjuntos) {
                if (archivo.getIdSolicitudConcesionArchivo() > 0)
                    result = repositorySolicitudConcesion.actualizarSolicitudConcesionArchivo(archivo);
                else
                    result = repositorySolicitudConcesion.registrarSolicitudConcesionArchivo(archivo);
            }
        }
        return result;
    }
}
