package pe.gob.serfor.mcsniffs.service.impl;


import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.PersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarSolicitudPlantacionForestal.DescargarSolicitudPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.AreaBloqueDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.AreaBloqueEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolicitudPlantacionForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.repository.ActividadSilviculturalRepository;
import pe.gob.serfor.mcsniffs.repository.AreaSistemaPlantacionRepository;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalRepository;
import pe.gob.serfor.mcsniffs.service.ArchivoPDFSolPlantacionForestalService;

@Service("ArchivoPDFSolPlantacionForestalService")
public class ArchivoPDFSolPlantacionForestalServiceImpl implements ArchivoPDFSolPlantacionForestalService {

    

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private SolPlantacionForestalRepository repoSolPlanForesRepository;

    @Autowired
    private AreaSistemaPlantacionRepository areaSistemaPlantacionRepository;



    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 12f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    static Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font subTituloTabla= new Font(Font.HELVETICA, 10f, Font.COURIER);

    Font normal= new Font(Font.HELVETICA, 10f);

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    @Override
    public ByteArrayResource consolidadoSolPlantForestal_PDF(DescargarSolicitudPlantacionForestalDto obj) throws Exception {
        XWPFDocument doc = getDoc("formatoPGMFA.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        /* doc.write(b);
        doc.close(); */

        File archivo = File.createTempFile("SolPlantForestal", ".pdf");

        Integer idPlanManejo = obj.getIdSolicitudPlantacionForestal();
        FileOutputStream os = new FileOutputStream(archivo);

        createPDFSolPanFor(os,idPlanManejo);
        os.flush();
        os.close();
        //ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }


    public  void createPDFSolPanFor(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);

        try {

            PdfWriter writer = PdfWriter.getInstance(document, os);
            document.open();

            //*********************************  GENERANDO EL PDF  ***************************************/
            //TITULO
            Paragraph preface = new Paragraph();

            preface.add(new Paragraph("ANEXO N° 01", titulo));
            preface.setAlignment(Element.ALIGN_CENTER);
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("FORMATO PARA LA INSCRIPCION EN EL REGISTRO NACIONAL DE PLANTACIONES FORESTALES", subTitulo));
            document.add(preface);
            addEmptyLine(preface, 1);

            //1. INFORMACION DEL SOLICITANTE
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable tableInfoSoli = informacionDelSolicitante(writer,idPlanManejo);
            document.add(tableInfoSoli);
            
            //2. INFORMACION DEL AREA
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable tableInfoArea = informacionDelArea(writer,idPlanManejo);
            document.add(tableInfoArea);

            //3. INFORMACION GENERAL DEL AREA PLANTEADA
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable tableInfoGenAreaPlan = informacionGeneralDelAreaPlanteada(writer,idPlanManejo);
            document.add(tableInfoGenAreaPlan);

            //4. INFORMACION DETALLADA DE PLANTACION FORESTAL
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable tableInfoDetPlanForest = infoDetallePlantForestal(writer,idPlanManejo);
            document.add(tableInfoDetPlanForest);

            // ALTURA PROMEDIO - PREDIO
            preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("Altura promedio en metros (sólo en caso del bambú o especies cuyas unidades de medidas sean cañas o unidades)", subTituloTabla));
            document.add(preface);
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable alturaProm = infoAlturaPromedio(writer,idPlanManejo);
            document.add(alturaProm);

            // BLOQUES/SECTOR
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable tableBloquesSector = infoBloquesSector(writer,idPlanManejo);
            document.add(tableBloquesSector);

            // DECLARACION || LUGAR - FECHA
            document.add(new Paragraph("\r\n\r\n"));
            preface = new Paragraph();
            preface.add(new Paragraph("1.   Declaro bajo juramento que toda la información antes consignada en la presente solicitud, es veraz y ha sido debidamente verificada. En caso que se compruebe fraude o falsedad en la declaración, información o documentación presentada, me someto a las consecuencias y responsabilidades administrativas y penales que correspondan, conforme a lo previsto en el artículo 32° de la Ley N° 27444, Ley del Procedimiento Administrativo General, y el Código Penal respecto a los delitos contra la fe pública.  Asimismo, declaro que no existe otro derecho de propiedad, registrado o no, sobre el área objeto de registro.", contenido));
            preface.add(new Paragraph("2.   Me comprometo a permitir a la autoridad encargada del registro o quien esta designe, a que en el ejercicio de sus facultades de seguimiento y control, pueda realizar visitas inspectoras con el objeto de verificar la información señalada en la presente solicitud.",contenido));
            preface.add(new Paragraph("3.   Me comprometo a actualizar la información contenida en el presente formato, previo a los trabajos de aprovechamiento forestal y brindar las facilidades del caso a la Autoridad Forestal competente para que verifique los volúmenes existentes en campo, salvo las excepciones establecidas en la legislación.",contenido));
            document.add(preface);
            document.add(new Paragraph("\r\n\r\n"));

            preface = new Paragraph();
            preface.add(new Paragraph("| Lugar y Fecha:                                 (         /          /         )", normal));
            document.add(preface);
            document.add(new Paragraph("\r\n\r\n"));

            preface = new Paragraph();
            preface.add(new Paragraph("Firma del Solicitante", normal));
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("DNI N° _____________________", subTitulo));
            document.add(preface);
            document.add(new Paragraph("\r\n\r\n"));

            //ANEXO 5
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable tableAnexo5 = infoAnexo5(writer);
            document.add(tableAnexo5);

            //********************************************************************************************/

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable informacionDelSolicitante(PdfWriter writer, Integer idPlanManejo) throws Exception { //throws Exception
        //------DATA-----//
        SolPlantacionForestalEntity params = new SolPlantacionForestalEntity();
        params.setIdSolicitudPlantacionForestal(idPlanManejo);

        PersonaEntity titular = new PersonaEntity();
        PersonaEntity ReprLegal = new PersonaEntity();
        List<SolPlantacionForestalEntity> infoGeneral = new ArrayList<>();

        ResultClassEntity data = repoSolPlanForesRepository.listarSolicitudPlantacionForestal(params);
            if (data != null && data.getSuccess()) {

                infoGeneral = (List<SolPlantacionForestalEntity>) data.getData();

                if (infoGeneral != null && !infoGeneral.isEmpty() && infoGeneral.get(0) != null) {
                    titular = infoGeneral.get(0).getSolicitante();
                    ReprLegal = infoGeneral.get(0).getRepresentanteLegal();
                }
            }

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);
        //--------------- 1°
        setearCellcabezera("1.	INFORMACION DEL SOLICITANTE", 6,table);
        //---------------  2° ROW
        setearSubTitle("1.1 Del Titular de la Plantación",6,table);
        //---------------  3° ROW
        setearBodyStatic("Nombre y/o razón social",2,table);
        setearBodyData(titular.getRazonSocialEmpresa()==null ? " ":titular.getRazonSocialEmpresa(),4,table);
        // --------------- 4° ROW
        setearBodyData("DNI",1,table);
        setearBodyStatic(titular.getNumeroDocumento()==null ? " ":titular.getNumeroDocumento() ,2,table);
        setearBodyData("*RUC",1,table);
        setearBodyStatic(titular.getRuc()==null ? " ":titular.getRuc(),2,table);
        //---------------  5° ROW
        setearBodyStatic("Dirección",1,table);
        setearBodyData(titular.getDireccion()==null ? " ":titular.getDireccion(),2,table);
        setearBodyStatic("N°",1,table);
        setearBodyData(titular.getDireccionNumero()==null ? " ":titular.getDireccionNumero(),1,table);
        setearBodyData(titular.getSector()==null ? " ":titular.getSector(),1,table);
        //---------------  6° ROW
        setearBodyStatic("Dst.",1,table);
        setearBodyData(titular.getnombreDistrito()==null ? " ":titular.getnombreDistrito(),1,table);
        setearBodyStatic("Prov.",1,table);
        setearBodyData(titular.getnombreProvincia()==null ? " ":titular.getnombreProvincia(),1,table);
        setearBodyStatic("Dpto.",1,table);
        setearBodyData(titular.getnombreDepartamento()==null ? " ":titular.getnombreDepartamento(),1,table);
        //---------------  7° ROW
        setearBodyStatic("*Telf. Fijo",1,table);
        setearBodyData(titular.getTelefono()==null ? " ":titular.getTelefono(),1,table);
        setearBodyStatic("*Telf. Celular",1,table);
        setearBodyData(titular.getCelular()==null ? " ":titular.getCelular(),1,table);
        setearBodyStatic("*Correo electrónico.",1,table);
        setearBodyData(titular.getCorreo()==null ? " ":titular.getCorreo(),1,table);
        //---------------  8° ROW
        setearSubTitle("1.2 Del Representante Legal(De ser el caso)",6,table);
        //---------------  9° ROW
        setearBodyStatic("Nombre",1,table);
        setearBodyData(ReprLegal.getNombres()==null ? " ":ReprLegal.getNombres(),2,table);
        setearBodyStatic("DNI",1,table);
        setearBodyData(ReprLegal.getNumeroDocumento()==null ? " ":ReprLegal.getNumeroDocumento(),2,table);
        // --------------- 10° ROW
        setearBodyStatic("Dirección",1,table);
        setearBodyData(ReprLegal.getDireccion()==null ? " ":ReprLegal.getDireccion(),2,table);
        setearBodyStatic("N°",1,table);
        setearBodyData(ReprLegal.getDireccionNumero()==null ? " ":ReprLegal.getDireccionNumero(),1,table);
        setearBodyData(ReprLegal.getSector()==null ? " ":ReprLegal.getSector(),1,table);
        //---------------  11° ROW
        setearBodyStatic("Dst.",1,table);
        setearBodyData(ReprLegal.getnombreDistrito()==null ? " ":ReprLegal.getnombreDistrito(),1,table);
        setearBodyStatic("Prov.",1,table);
        setearBodyData(ReprLegal.getnombreProvincia()==null ? " ":ReprLegal.getnombreProvincia(),1,table);
        setearBodyStatic("Dpto.",1,table);
        setearBodyData(ReprLegal.getnombreDepartamento()==null ? " ":ReprLegal.getnombreDepartamento(),1,table);
        // --------------- 12° ROW
        setearBodyStatic("*Telf. Fijo",1,table);
        setearBodyData(ReprLegal.getTelefono()==null ? " ":ReprLegal.getTelefono(),1,table);
        setearBodyStatic("*Telf. Celular",1,table);
        setearBodyData(ReprLegal.getCelular()==null ? " ":ReprLegal.getCelular(),1,table);
        setearBodyStatic("*Correo electrónico.",1,table);
        setearBodyData(ReprLegal.getCorreo()==null ? " ":ReprLegal.getCorreo(),1,table);
        
        return table;
    }

    public  PdfPTable informacionDelArea(PdfWriter writer,Integer idPlanManejo) throws Exception {
        //----DATA----
        SolPlantacionForestalEntity params = new SolPlantacionForestalEntity();
        params.setIdSolicitudPlantacionForestal(idPlanManejo); 

            PersonaEntity titular = new PersonaEntity();
            List<SolPlantacionForestalEntity> infoGeneral = new ArrayList<>();

            ResultClassEntity data = repoSolPlanForesRepository.listarSolicitudPlantacionForestal(params);

            if (data != null && data.getSuccess()) {

                infoGeneral = (List<SolPlantacionForestalEntity>) data.getData();

                if (infoGeneral != null && !infoGeneral.isEmpty() && infoGeneral.get(0) != null) {
                    titular = infoGeneral.get(0).getSolicitante();
                    
                }
            }

            SolPlantacionForestalAreaEntity predio = new SolPlantacionForestalAreaEntity();
            SolPlantacionForestalAreaEntity param = new SolPlantacionForestalAreaEntity();
            param.setIdSolPlantaForestalArea(idPlanManejo);

            ResultEntity result = repoSolPlanForesRepository.listarSolPlantacionForestalArea(param);

            if (result != null && result.getIsSuccess()) {
                List<SolPlantacionForestalAreaEntity> lista = (List<SolPlantacionForestalAreaEntity>) result.getData();
                if (lista != null && !lista.isEmpty() && lista.get(0) != null) {
                    predio = lista.get(0);
                    //altura promedio
                }
            }

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);

        //--------------- 1° ROW
        setearCellcabezera("2. INFORMACION DEL AREA",6,table);
        //---------------  2° ROW
        setearSubTitle("2.1 Del Pedido",6,table);
        //---------------  3° ROW
        setearBodyStatic("Nombre del Pedido",2,table);
        setearBodyData(predio.getPredio()==null?" ":predio.getPredio(),2,table);
        setearBodyStatic("Área (ha)",1,table);
        setearBodyData(predio.getArea()==null?" ":predio.getArea().toString(),1,table);
        //----------------4 ° ROW
        setearBodyStatic("Nombre del Propietario del Pedido",3,table);
        setearBodyData(titular.getRazonSocialEmpresa()==null?" ":titular.getRazonSocialEmpresa(),3,table);
        // --------------- 5° ROW
        setearBodyStatic("DNI",1,table);
        setearBodyData(titular.getNumeroDocumento()==null?" ":titular.getNumeroDocumento(),2,table);
        setearBodyStatic("*RUC",1,table);
        setearBodyData(titular.getRuc()==null?" ":titular.getRuc(),2,table);
        //---------------  6° ROW
        setearBodyStatic("Ubicación Geográfica",3,table);
        setearBodyData(predio.getZona()==null?" ":predio.getZona(),3,table);
        //---------------  7° ROW
        setearBodyStatic("Caserio",1,table);
        setearBodyData(predio.getCaserioComunidad()==null?" ":predio.getCaserioComunidad(),2,table);
        setearBodyStatic("Dist.",1,table);
        setearBodyData(predio.getNombreDistrito()==null?" ":predio.getNombreDistrito(),2,table);
        //---------------  8° ROW
        setearBodyStatic("Prov.",1,table);
        setearBodyData(predio.getNombreProvincia()==null?" ":predio.getNombreProvincia(),2,table);
        setearBodyStatic("Dpto.",1,table);
        setearBodyData(predio.getNombreDepartamento()==null?" ":predio.getNombreDepartamento(),2,table);
        //---------------  9° ROW
        setearRowsColspanStatic("Condición",5,1,table); //row (5)
        setearBodyStatic("Propietario",1,table);

        if(predio.getPropietario()!=null)
            if(predio.getPropietario()) setearBodyData("X",1,table); else setearBodyData(" ",1,table);
        else setearBodyData(" ",1,table);

        setearBodyStaticLetraPequenio("Tipo/Número de documento que acredita:",1,table);

        if(predio.getPropietario()!=null && predio.getCodigoTipoDocPropietario()!=null && predio.getPropietario()==true)
            if(predio.getCodigoTipoDocPropietario().equals("TDOCDNI")) setearBodyData("DNI" + ": " + predio.getNumeroDocPropietario(),2,table);
            else setearBodyData("RUC" + ": " + predio.getNumeroDocPropietario(),2,table);
         else setearBodyData(" " + ": " + " ",2,table);

        //----------------- 10° ROW
        setearBodyStatic("Inversionista",1,table);

        if(predio.getInversionista()!=null)
            if(predio.getInversionista()) setearBodyData("X",1,table); else setearBodyData(" ",1,table);
        else setearBodyData(" ",1,table);

        setearBodyStaticLetraPequenio("Tipo/Número de documento que acredita:",1,table);

        if(predio.getInversionista()!=null && predio.getCodigoTipodocInversionista()!=null && predio.getInversionista()==true)
            if(predio.getCodigoTipodocInversionista().equals("TDOCDNI")) setearBodyData("DNI" + ": " + predio.getNumerDocInversionista(),2,table);
            else setearBodyData("RUC" + ": " + predio.getNumerDocInversionista(),2,table);
         else setearBodyData(" " + ": " + " ",2,table);

        // --------------- 11° ROW
        setearBodyStaticLetraPequenio("En el caso de contratos donde haya un acuerdo de beneficios mutuos entre el propietario y el inversionista, éste deberá especificar a nombre de quien se registrará la plantación.",5,table);
        //---------------  12° ROW
        setearBodyData("Especificar el documento que autorice el uso del área para el establecimiento de la plantación.",5,table);
        //---------------------------------
        setearBodyData(" ",5,table);
        //---------------  13° ROW
        setearSubTitle("2.2 Del Título Habilitante - Contrato (De corresponder)",6,table);
        // --------------- 14° ROW
        setearRowsColspanStatic("Tipo",2,1,table); //row vert (2)
        setearBodyData("Cesión en uso para sistemas agroforestales",2,table);
        setearBodyData(" ",1,table);
        setearBodyStatic("Numero",1,table);
        setearBodyData(" ",1,table);
        //---------------- 15° ROW
        setearBodyData("Cesión en uso para sistemas agroforestales",2,table);
        setearBodyData(" ",1,table);
        setearBodyStatic("Numero",1,table);
        setearBodyData(" ",1,table);

        return table;
    }

    public  PdfPTable informacionGeneralDelAreaPlanteada(PdfWriter writer,Integer idPlanManejo) throws Exception {
        //--- DATA
            AreaSistemaPlantacionDto paramArea = new AreaSistemaPlantacionDto();
            EspecieForestalSistemaPlantacionEspecieDto esp = new EspecieForestalSistemaPlantacionEspecieDto();
            SolPlantacionForestalAreaEntity param = new SolPlantacionForestalAreaEntity();
            param.setIdSolPlantaForestalArea(idPlanManejo);

            List<AreaSistemaPlantacionDto> listaAreaplantada = new ArrayList();
            SolPlantacionForestalAreaEntity predio = new SolPlantacionForestalAreaEntity();
            paramArea.setIdSolPlantForest(idPlanManejo);

            ResultClassEntity respAreaPlan = areaSistemaPlantacionRepository.listarAreaSistemaPlantacion(paramArea);
            ResultEntity result = repoSolPlanForesRepository.listarSolPlantacionForestalArea(param);

            if (result != null && result.getIsSuccess()) {
                List<SolPlantacionForestalAreaEntity> lista = (List<SolPlantacionForestalAreaEntity>) result.getData();
                if (lista != null && !lista.isEmpty() && lista.get(0) != null) {
                    predio = lista.get(0);
                }
                listaAreaplantada = (List<AreaSistemaPlantacionDto>) respAreaPlan.getData();
                for (AreaSistemaPlantacionDto element : listaAreaplantada) {
                    esp.setIdAreaSistemaPlantacion(element.getIdAreaSistemaPlantacion());
                    esp.setPageNum(1);
                    esp.setPageSize(1000);
                    ResultClassEntity resultEsp = areaSistemaPlantacionRepository.ListarEspecieForestalSistemaPlantacionEspecie(esp);
                    List<EspecieForestalSistemaPlantacionEspecieDto> objList = (List<EspecieForestalSistemaPlantacionEspecieDto>) resultEsp.getData();
                    List<EspecieForestalSistemaPlantacionEspecieDto> filtroId = objList.stream()
                    .filter(c -> c.getIdAreaSistemaPlantacion().equals(element.getIdAreaSistemaPlantacion()))
                    .collect(Collectors.toList());
                    if(filtroId != null && !filtroId.isEmpty()){
                        element.setEspeciesEstablecidas(true);
                    }
                } 
            }

        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100);

        //--------------- 1°
        setearCellcabezera("3. INFORMACION GENERAL DEL AREA PLANTADA",5,table);
        //---------------  2° ROW
        setearBodyStatic("Área total de la plantación (ha):",1,table);
        setearBodyData(predio.getAreaTotalPlantacion()==null?" ":predio.getAreaTotalPlantacion().toString(),1,table);
        setearBodyStatic("Mes y año de establecimiento de la plantación:",2,table);
        String fecha = " ";
        if(predio.getMesAnhoPlantacion()!=null){
            Date myDate = predio.getMesAnhoPlantacion();
            fecha = new SimpleDateFormat("MM-yyyy").format(myDate);
        }
        setearBodyData(fecha,1,table);
        //----------------3 ° ROW
        setearRowsColspanStatic("Sistema de plantación",2,1,table); //row (2)
        setearBodyStatic("Superficie",2,table);
        setearRowsColspanStatic("Fines",2,1,table); //row (2)
        setearRowsColspanStatic("Especies establecidas",2,1,table); //row (2)
        // --------------- 4° ROW
        setearBodyStatic("Unid. Medida (ha/m2/otros)",1,table);
        setearBodyStatic("Cantidad",1,table);
        //---------------  5° ROW
        for (AreaSistemaPlantacionDto element : listaAreaplantada) {
        setearBodyData(element.getNombre()==null?" ":element.getNombre(),1,table);
        setearBodyData(element.getCodigoUm()==null?" ":element.getCodigoUm(),1,table);
        setearBodyData(element.getCantidad()==null?" ":element.getCantidad().toString(),1,table);
        setearBodyData(element.getFines()==null?" ":element.getFines(),1,table);
        if(element.getEspeciesEstablecidas()!=null)
            if(element.getEspeciesEstablecidas()) setearBodyData("SI",1,table); else setearBodyData("NO",1,table);
        else setearBodyData(" ",1,table);
        
        }

        return table;
    }

    public  PdfPTable infoDetallePlantForestal(PdfWriter writer,Integer idPlanManejo) throws Exception {
        // DATA
        List<SolicitudPlantacionForestalDetalleEntity> listaEsp = new ArrayList<>();
            List<AreaBloqueEntity> listaBloques = new ArrayList<>();
            Integer totalArbolesExistentes = 0;
            ResultClassEntity resultEsp = repoSolPlanForesRepository.listarEspeciesSistemaPlantacion(idPlanManejo);

            AreaBloqueEntity request = new AreaBloqueEntity();
            request.setIdSolPlantForest(idPlanManejo);

            //request.setBloque("BLOQUE 1");
            ResultClassEntity resultListAreaBloque = repoSolPlanForesRepository.listarListaBloque(request);

            if (resultListAreaBloque != null && resultListAreaBloque.getSuccess()) {
                listaBloques = (List<AreaBloqueEntity>) resultListAreaBloque.getData();
            }

            if(resultEsp != null && resultEsp.getSuccess()){
                listaEsp = (List<SolicitudPlantacionForestalDetalleEntity>) resultEsp.getData();
                totalArbolesExistentes = sumaTotalArbolesExistentes(listaEsp);
            }

        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(100);
        //--------------- 1°
        setearCellcabezera("4. DETALLE DE LA PLANTACION FORESTAL AASSSADDD",7,table);
        //---------------  2° ROW
        setearRowsColspanStatic("Especie",1,3,table);
        setearRowsColspanStatic("Total, de árboles/matas/cepas existentes",2,1,table);
        setearRowsColspanStatic("Producción estimada(m3, kg, L, cañas, unidades, otros)",2,1,table);
        setearRowsColspanStatic("Coordenadas UTM2 referencial del macizo, lindero, etc",1,2,table);
        //----------------3 ° ROW
        setearRowsColspanStatic("Nombre común",1,2,table);
        setearRowsColspanStatic("Nombre científico ",1,1,table);
        setearBodyStatic("Este",1,table);
        setearBodyStatic("Norte",1,table);
        // --------------- 4° ROW
        for (SolicitudPlantacionForestalDetalleEntity element : listaEsp) {
        setearBodyData(element.getEspecieNombreComun()==null?" ":element.getEspecieNombreComun(),2,table);
        setearBodyData(element.getEspecieNombreCientifico()==null?" ":element.getEspecieNombreCientifico(),1,table);
        setearBodyData(element.getTotalArbolesExistentes()==null?" ":element.getTotalArbolesExistentes().toString(),1,table);
        setearBodyData(element.getProduccionEstimada()==null?" ":element.getProduccionEstimada().toString(),1,table);
        setearBodyData(element.getCoordenadaEste()==null?" ":element.getCoordenadaEste(),1,table);
        setearBodyData(element.getCoordenadaNorte()==null?" ":element.getCoordenadaNorte(),1,table);
        }
        // ------------ 5° ROW
        setearBodyStatic("TOTAL",3,table);
        setearBodyData(totalArbolesExistentes.toString(),1,table);
        setearBodyStatic(" ",3,table);

        return table;
    }
    
    public  PdfPTable infoAlturaPromedio(PdfWriter writer,Integer idPlanManejo) throws Exception {
        // DATA
        SolPlantacionForestalAreaEntity param = new SolPlantacionForestalAreaEntity();
            param.setIdSolPlantaForestalArea(idPlanManejo);
        SolPlantacionForestalAreaEntity predio = new SolPlantacionForestalAreaEntity();

        ResultEntity result = repoSolPlanForesRepository.listarSolPlantacionForestalArea(param);
        if (result != null && result.getIsSuccess()) {
            List<SolPlantacionForestalAreaEntity> lista = (List<SolPlantacionForestalAreaEntity>) result.getData();
            if (lista != null && !lista.isEmpty() && lista.get(0) != null) {
                predio = lista.get(0);
            }
        }

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        setearBodyData(predio.getAlturaPromedio()==null?" ":predio.getAlturaPromedio().toString(),1,table);

        return table;
    }
    
    public  PdfPTable infoBloquesSector(PdfWriter writer,Integer idPlanManejo) throws Exception {
        // DATA
        AreaBloqueEntity request = new AreaBloqueEntity();
        request.setIdSolPlantForest(idPlanManejo);
        ResultClassEntity resultListAreaBloque = repoSolPlanForesRepository.listarListaBloque(request);
        List<AreaBloqueEntity> listaBloques = new ArrayList<>();

            if (resultListAreaBloque != null && resultListAreaBloque.getSuccess()) {
                listaBloques = (List<AreaBloqueEntity>) resultListAreaBloque.getData();
            }

        SolPlantacionForestalAreaEntity param = new SolPlantacionForestalAreaEntity();
            param.setIdSolPlantaForestalArea(idPlanManejo);
        SolPlantacionForestalAreaEntity predio = new SolPlantacionForestalAreaEntity();

        ResultEntity result = repoSolPlanForesRepository.listarSolPlantacionForestalArea(param);
        if (result != null && result.getIsSuccess()) {
            List<SolPlantacionForestalAreaEntity> lista = (List<SolPlantacionForestalAreaEntity>) result.getData();
            if (lista != null && !lista.isEmpty() && lista.get(0) != null) {
                predio = lista.get(0);
            }
        }

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);

        //--------------- 1°
        setearBodyStaticLetraPequenio("Coordenadas UTM de los vértices de los Bloques (Sólo si la plantación se encuentra dispersa) Datum:  WGS 84 Zona: " + (predio.getZona()==null?" ":predio.getZona()),4,table);
        setearRowsColspanStatic("Observaciones",2,1,table);
        setearRowsColspanStatic("Área Bloque (ha)",2,1,table);
        //---------------  2° ROW
        setearBodyStatic("Bloque/Sector", 1, table);
        setearBodyStatic("Vértice", 1, table);
        setearBodyStatic("Este", 1, table);
        setearBodyStatic("Norte", 1, table);
        //---------------- 3 ° ROW
        if(listaBloques!=null){
            for (AreaBloqueEntity element : listaBloques) {
                if(element.getAreaBloqueDetalle()!=null && element.getAreaBloqueDetalle().size()!=0) {

                    Integer i = element.getAreaBloqueDetalle().size();
                    setearRowsColspanStatic(element.getBloque()==null?" ":element.getBloque(),i,1,table);
                    Boolean primero = true;

                    for (AreaBloqueDetalleEntity det : element.getAreaBloqueDetalle()) {
                        setearBodyData(det.getNroVertice()==null?" ":det.getNroVertice().toString(),1,table);
                        setearBodyData(det.getCoordenadaEste()==null?" ":det.getCoordenadaEste().toString(),1,table);
                        setearBodyData(det.getCoordenadaNorte()==null?" ":det.getCoordenadaNorte().toString(),1,table);
                        setearBodyData(det.getObservacion()==null?" ":det.getObservacion(),1,table);
                        if(primero) setearRowsColspanStatic(element.getArea()==null?" ":element.getArea().toString(),i,1,table);
                        primero = false;
                    }
                }

            }
            String total = sumAreaTotalHa(listaBloques);
            setearRowsColspanStatic("TOTAL",1,5,table);
            setearBodyData(total,1,table);
        }
        
        return table;
    }
    
    public  PdfPTable infoAnexo5(PdfWriter writer) throws Exception {
        
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);
        //--------------- 1°
        setearCellcabezera("5. ANEXOS",6,table);
        //---------------  2° ROW
        Paragraph preface = new Paragraph();
        PdfPCell c = new PdfPCell();

        preface.add(new Phrase("ANEXO 1:",subTitulo));
        preface.add(new Phrase(" Mapa/croquis de la ubicación",normal));
        addEmptyLine(preface, 2);
        preface.add(new Phrase("ANEXO 2:",subTitulo));
        preface.add(new Phrase(" Copia simple del título de propiedad u otro documento que acredite el derecho de propiedad, según lo señalado en el numeral 6.4 de los Lineamientos para el Registro Nacional de Plantaciones Forestales.",normal));
        addEmptyLine(preface, 2);
        preface.add(new Phrase("ANEXO 3:",subTitulo));
        preface.add(new Phrase(" Carta poder a favor de la persona autorizada a realizar el trámite",normal));
        addEmptyLine(preface, 2);

        c.addElement(preface);
        c.setColspan(6);
        c.setPadding(5);
        table.addCell(c);

        return table;
    }
    
    
    private static void setearCellcabezera(String dato, Integer colum, PdfPTable table){

        Font title = new Font(Font.HELVETICA, 12,Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato,title));
        Color colorHeader = new Color(189 , 214 , 238);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setColspan(colum);
        c.setBackgroundColor(colorHeader);
        c.setPadding(5);
        table.addCell(c);
        table.setHeaderRows(1);
    }
    private static void setearSubTitle(String dato, Integer colum, PdfPTable table){
        Font subTitle = new Font(Font.HELVETICA, 10,Font.BOLD);
        Color colorSubTitle = new Color(242 , 242 , 242);
        PdfPCell c = new PdfPCell(new Phrase(dato,subTitle));
        c.setColspan(colum);
        c.setBackgroundColor(colorSubTitle);
        c.setPadding(5);
        table.addCell(c);
        table.setHeaderRows(1);
    }
    private static void setearBodyStatic(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        table.addCell(c);
    }   
    private static void setearBodyData(String dato, Integer colum, PdfPTable table){
        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        c.setColspan(colum);
        c.setPadding(5);
        table.addCell(c);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void setearRowsColspanStatic(String dato, Integer row,Integer colum, PdfPTable table){

        PdfPCell c = new PdfPCell(new Phrase(dato, contenido));
        Color colorBody = new Color(242 , 242 , 242);
        
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        c.setHorizontalAlignment(Element.ALIGN_CENTER);
        c.setVerticalAlignment(Element.ALIGN_CENTER);
        c.setRowspan(row);
        c.setColspan(colum);
        table.addCell(c);
    }
    private static void setearBodyStaticLetraPequenio(String dato, Integer colum, PdfPTable table){
        Font pequenio = new Font(Font.HELVETICA, 6,
            Font.BOLD);
        PdfPCell c = new PdfPCell(new Phrase(dato, pequenio));
        Color colorBody = new Color(242 , 242 , 242);
        c.setColspan(colum);
        c.setBackgroundColor(colorBody);
        c.setPadding(5);
        table.addCell(c);
    }  

    private Integer sumaTotalArbolesExistentes (List<SolicitudPlantacionForestalDetalleEntity> listaEsp){
        Integer suma = 0;
        for (SolicitudPlantacionForestalDetalleEntity element : listaEsp) {
            suma += element.getTotalArbolesExistentes();
        }
        return suma;
    }
    public static String sumAreaTotalHa(List<AreaBloqueEntity> data){

        String numero = "";
        BigDecimal suma = new BigDecimal(0);

        for (AreaBloqueEntity e : data) {
            suma = suma.add(e.getArea());
        }
        BigDecimal total = suma.setScale(2, RoundingMode.HALF_UP);

        return numero = total.toString();
    }


}
