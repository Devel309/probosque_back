package pe.gob.serfor.mcsniffs.service.util;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionResumidoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionResumidoDto;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDetalle;
import pe.gob.serfor.mcsniffs.entity.ObjetivoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Anexo2Dto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ListaEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosAnexo6;
import pe.gob.serfor.mcsniffs.entity.SolicitudOpinionEntity;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class DocUtilInformeEvaluacion {
    private DocUtilInformeEvaluacion(){
    }

    static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * @autor: Jason Retamozo - 22-03-2022
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static XWPFDocument processFile(InformacionGeneralDEMAEntity data,
                                           List<ObjetivoManejoEntity>  listaObjetivos,
                                           XWPFDocument doc) throws IOException {
        System.out.println("processFile listaObjetivos "+listaObjetivos.size());
        try {
            //Se lista los objetivos
            String objetivo="";
            int contador=1;
            for(ObjetivoManejoEntity obj : listaObjetivos){
                objetivo+="\n\t"+contador+"\t"+obj.getGeneral()+"\n\n";
                contador++;
            }

            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                          //  System.out.println("processFile text "+text);
                            if (text.contains("txtNombreCompletoElaborador")) {
                                text = text.replace("txtNombreCompletoElaborador",data.getNombreElaboraDema()!= null?
                                        data.getNombreElaboraDema()+" "+data.getApellidoPaternoElaboraDema()+" "+data.getApellidoMaternoElaboraDema(): "");
                                r.setText(text, 0);
                            }else if (text.contains("txtNombreDirigido")) {
                                text = text.replace("txtNombreDirigido",data.getNombreRepresentante() != null ? data.getNombreRepresentante() + "  "+data.getApellidoPaternoRepresentante()+" , "+data.getApellidoMaternoRepresentante(): "");
                                r.setText(text, 0);
                            }else if (text.contains("txtTipoConcesion")) {
                                 text = text.replace("txtTipoConcesion",data.getCodigoProceso() != null ? data.getCodigoProceso() : "");
                                r.setText(text, 0);
                            }else if (text.contains("txtNumExp")) {
                                 text = text.replace("txtNumExp",data.getNroTituloPropiedadComunidad() != null ? data.getNroTituloPropiedadComunidad() : "");
                                r.setText(text, 0);
                            }else if (text.contains("txtfechaInicio")) {
                                text = text.replace("txtfechaInicio",data.getFechaElaboracionPmfi() != null ? ""+data.getFechaElaboracionPmfi() : "");
                                r.setText(text, 0);
                            }else if (text.contains("txtTituloHabilitante")) {
                                text = text.replace("txtTituloHabilitante",data.getNombreTituloHabilitante() != null ? data.getNombreTituloHabilitante(): "");
                                r.setText(text, 0);
                            }else if (text.contains("txt2objetivos")) {
                                text = text.replace("txt2objetivos",objetivo != null ? objetivo: "");
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }


            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    /*
    public static void procesarSolicitudOpinionTabla0(List<SolicitudOpinionEntity> lstLista, XWPFDocument doc){
        try {
            System.out.println("procesarSolicitudOpinion ");
            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(1);
            System.out.println(lstLista.size());
            for (SolicitudOpinionEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarSolicitudOpinion text5  "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C0documento")) {
                                        text5 = text5.replace("C0documento",
                                                detalleEntity.getNumDocumento() != null
                                                        ? detalleEntity.getNumDocumento().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C0asunto")) {
                                        text5 = text5.replace("C0asunto",
                                                detalleEntity.getAsunto() != null
                                                        ? detalleEntity.getAsunto().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C0entidad")) {
                                        text5 = text5.replace("C0entidad",
                                                detalleEntity.getNombreEntidad() != null
                                                        ? detalleEntity.getNombreEntidad().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C0descripcion")) {
                                        text5 = text5.replace("C0descripcion",
                                                detalleEntity.getDescripcion() != null
                                                        ? detalleEntity.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C0fecha")) {
                                        text5 = text5.replace("C0fecha",
                                                detalleEntity.getFechaDocumento() != null
                                                        ? detalleEntity.getFechaDocumento().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public static void procesarResultadoEvaluacion1(int numeroTablaWord,List<SolicitudOpinionEntity> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarResultadoEvaluacion1   "+lista.size());
            for (SolicitudOpinionEntity detalle : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarResultadoEvaluacion1  text5 "+text5);
                                if (text5 != null) {
                                    if (text5.contains("OPINION1")) {
                                        text5 = text5.replace("OPINION1",
                                                detalle.getIdPlanManejo() != null
                                                        ?  detalle.getIdPlanManejo().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OPINION2")) {
                                        text5 = text5.replace("OPINION2",
                                                detalle.getEntidad() != null
                                                        ? ""+detalle.getEntidad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OPINION3")) {
                                        text5 = text5.replace("OPINION3",
                                                detalle.getTipoDocumento()!= null
                                                        ? ""+detalle.getTipoDocumento()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OPINION4")) {
                                        text5 = text5.replace("OPINION4",
                                                detalle.getNumDocumento()!= null
                                                        ? ""+detalle.getNumDocumento()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OPINION5")) {
                                        text5 = text5.replace("OPINION5",detalle.getFechaDocumento()!=null?formatter.format(detalle.getFechaDocumento()):"");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OPINION6")) {
                                        text5 = text5.replace("OPINION6",
                                                detalle.getAsunto()!= null
                                                        ? ""+detalle.getAsunto()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarResultadoEvaluacion2(int numeroTablaWord, List<EvaluacionCampoDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarResultadoEvaluacion2   "+lista.size());
            for (EvaluacionCampoDto detalle : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarResultadoEvaluacion2  text5 "+text5);
                                if (text5 != null) {
                                    if (text5.contains("OCULAR1")) {
                                        text5 = text5.replace("OCULAR1",
                                                detalle.getTipoDocumentoGestionTexto() != null
                                                        ?  detalle.getTipoDocumentoGestionTexto().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OCULAR2")) {
                                        text5 = text5.replace("OCULAR2",
                                                detalle.getDocumentoGestion() != null
                                                        ? ""+detalle.getDocumentoGestion()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OCULAR3")) {
                                        text5 = text5.replace("OCULAR3",
                                                detalle.getEvaluacionCampoEstado()!= null
                                                        ? ""+detalle.getEvaluacionCampoEstado()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OCULAR4")) {
                                        text5 = text5.replace("OCULAR4",
                                                detalle.getIdEvaluacionCampo()!= null
                                                        ? ""+detalle.getIdEvaluacionCampo().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void procesarResultadoEvaluacion3(int numeroTablaWord, List<EvaluacionResumidoDetalleDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarResultadoEvaluacion3   "+lista.size());
            for (EvaluacionResumidoDetalleDto detalle : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarResultadoEvaluacion3  text5 "+text5);
                                if (text5 != null) {
                                    if (text5.contains("OBS1")) {
                                        text5 = text5.replace("OBS1",
                                                detalle.getCodigoEvaluacionDetSub() != null
                                                        ?  detalle.getCodigoEvaluacionDetSub().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OBS2")) {
                                        text5 = text5.replace("OBS2",
                                                detalle.getConforme() != null
                                                        ? ""+detalle.getConforme()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("OBS3")) {
                                        text5 = text5.replace("OBS3",
                                                detalle.getCodigoEvaluacionDetPost()!= null
                                                        ? ""+detalle.getCodigoEvaluacionDetPost()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarCuadro1(int numeroTablaWord,List<InfBasicaAereaDetalleDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarCuadro1   "+lista.size());
            for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarCuadro1  text5 "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C1Vertice")) {
                                        text5 = text5.replace("C1Vertice",
                                                infBasicaAereaDetalleDto.getReferencia() != null
                                                        ?  infBasicaAereaDetalleDto.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C1X")) {
                                        text5 = text5.replace("C1X",
                                                infBasicaAereaDetalleDto.getCoordenadaEste() != null
                                                        ? ""+infBasicaAereaDetalleDto.getCoordenadaEste()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C1Y")) {
                                        text5 = text5.replace("C1Y",
                                                infBasicaAereaDetalleDto.getCoordenadaNorte()!= null
                                                        ? ""+infBasicaAereaDetalleDto.getCoordenadaNorte()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarCuadro2(int numeroTablaWord,BigDecimal areaTotal, List<Anexo2Dto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarCuadro2   "+lista.size()+"\nareaTotal "+areaTotal);
            for (Anexo2Dto anexo2Dto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0).trim();
                                System.out.println("procesarCuadro2  text5  "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C2NC")) {
                                        text5 = text5.replace("C2NC",
                                                anexo2Dto.getNombreComun() != null
                                                        ?  ""+anexo2Dto.getNombreComun()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C2NCi")) {
                                        text5 = text5.replace("C2NCi",
                                                anexo2Dto.getNombreCientifico()  != null
                                                        ? ""+anexo2Dto.getNombreCientifico()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C2NACen")) {
                                        System.out.println("procesarCuadro2  C2NumArbCensados  "+anexo2Dto.getnTotalArboles());
                                        text5 = text5.replace("C2NACen",
                                                anexo2Dto.getnTotalArboles()  != null
                                                        ? ""+anexo2Dto.getnTotalArboles()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C2NArb")) {
                                        System.out.println("procesarCuadro2  C2NumArbArprov  "+anexo2Dto.getnArbolesAprovechables());
                                        text5 = text5.replace("C2NArb",
                                                anexo2Dto.getnArbolesAprovechables()  != null
                                                        ? ""+anexo2Dto.getnArbolesAprovechables()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C2Volha")) {
                                        text5 = text5.replace("C2Volha",
                                                anexo2Dto.getVolumen() != null
                                                        ? ""+ BigDecimal.valueOf( Double.parseDouble(anexo2Dto.getVolumen()) ).divide(areaTotal,3, RoundingMode.HALF_UP)
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C2NASem")) {
                                        text5 = text5.replace("C2NASem",
                                                anexo2Dto.getnArbolesSemilleros() != null
                                                        ? ""+anexo2Dto.getnArbolesSemilleros()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarAnexo5( int numeroTablaWord,List<Anexo2Dto> lista, XWPFDocument doc) {
        try {
        XWPFTable table = doc.getTables().get(numeroTablaWord);
        XWPFTableRow row = table.getRow(1);
        System.out.println("procesarAnexo5   "+lista.size());
        for (Anexo2Dto anexo2Dto : lista) {
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0).trim();
                            System.out.println("procesarAnexo5  text5  "+text5);
                            if (text5 != null) {
                                if (text5.contains("7N5Faja")) {
                                    text5 = text5.replace("7N5Faja",
                                            anexo2Dto.getNumeroFaja() != null
                                                    ?  ""+anexo2Dto.getNumeroFaja()
                                                    : "");
                                    r.setText(text5, 0);
                                } else if (text5.contains("7N5Arb")) {
                                    text5 = text5.replace("7N5Arb",
                                            anexo2Dto.getnArbol()  != null
                                                    ? ""+anexo2Dto.getnArbol()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Esp")) {
                                    //System.out.println("procesarCuadro2  C2NumArbCensados  "+anexo2Dto.getnTotalArboles());
                                    text5 = text5.replace("7N5Esp",
                                            anexo2Dto.getNombreCientifico()  != null
                                                    ? ""+anexo2Dto.getNombreCientifico()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Dap")) {
                                    //System.out.println("procesarCuadro2  C2NumArbArprov  "+anexo2Dto.getnArbolesAprovechables());
                                    text5 = text5.replace("7N5Dap",
                                            anexo2Dto.getDap()  != null
                                                    ? ""+anexo2Dto.getDap()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Altura")) {
                                    text5 = text5.replace("7N5Altura",
                                            anexo2Dto.getAlturaComercial() != null
                                                    ? ""+  anexo2Dto.getAlturaComercial()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Calfus")) {
                                    text5 = text5.replace("7N5Calfus",
                                            anexo2Dto.getAlturaComercial() != null
                                                    ? ""+  anexo2Dto.getAlturaComercial()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Este")) {
                                    text5 = text5.replace("7N5Este",
                                            anexo2Dto.getEste() != null
                                                    ? ""+anexo2Dto.getEste()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Norte")) {
                                    text5 = text5.replace("7N5Norte",
                                            anexo2Dto.getNorte() != null
                                                    ? ""+anexo2Dto.getNorte()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5Vol")) {
                                    text5 = text5.replace("7N5Vol",
                                            anexo2Dto.getVolumen() != null
                                                    ? ""+anexo2Dto.getVolumen()
                                                    : "");
                                    r.setText(text5, 0);
                                }else if (text5.contains("7N5ApSem")) {
                                    text5 = text5.replace("7N5ApSem",
                                            anexo2Dto.getnArbolesSemilleros() != null
                                                    ? ""+anexo2Dto.getCategoria()
                                                    : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRow);
        }
        table.removeRow(1);
    } catch (Exception e) {
        e.printStackTrace();
    }
    }

    public static void procesarAnexo7(int numeroTablaWord,List<Anexo2Dto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarAnexo7   "+lista.size());
            for (Anexo2Dto anexo2Dto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0).trim();
                                System.out.println("procesarAnexo7  text5  "+text5);
                                if (text5 != null) {
                                    if (text5.contains("7N7Faja")) {
                                        text5 = text5.replace("7N7Faja",
                                                anexo2Dto.getNumeroFaja() != null
                                                        ?  ""+anexo2Dto.getNumeroFaja()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("7N7NArb")) {
                                        text5 = text5.replace("7N7NArb",
                                                anexo2Dto.getnArbol()  != null
                                                        ? ""+anexo2Dto.getnArbol()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("7N7Especies")) {
                                        text5 = text5.replace("7N7Especies",
                                                anexo2Dto.getNombreEspecies()  != null
                                                        ? ""+anexo2Dto.getNombreEspecies()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("7N7Este")) {
                                        text5 = text5.replace("7N7Este",
                                                anexo2Dto.getEste()  != null
                                                        ? ""+anexo2Dto.getEste()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("7N7Norte")) {
                                        text5 = text5.replace("7N7Norte",
                                                anexo2Dto.getNorte()  != null
                                                        ? ""+anexo2Dto.getNorte()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("7N7Unidad")) {
                                        text5 = text5.replace("7N7Unidad",
                                                anexo2Dto.getC()  != null
                                                        ? ""+anexo2Dto.getC()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("7N7ApSem")) {
                                        text5 = text5.replace("7N7ApSem",
                                                anexo2Dto.getCategoria()  != null
                                                        ? ""+anexo2Dto.getCategoria()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarAnexo8(int numeroTablaWord,List<ListaEspecieDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(numeroTablaWord);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarAnexo8   "+lista.size());
            for (ListaEspecieDto listaEspecieDto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0).trim();
                                System.out.println("procesarAnexo8  text5  "+text5);
                                if (text5 != null) {
                                    if (text5.contains("7N8NCO")) {
                                        text5 = text5.replace("7N8NCO",
                                                listaEspecieDto.getTextNombreComun() != null
                                                        ?  ""+listaEspecieDto.getTextNombreComun()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("7N8NA")) {
                                        text5 = text5.replace("7N8NA",
                                                listaEspecieDto.getNombreNativo()  != null
                                                        ? ""+listaEspecieDto.getNombreNativo()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("7N8NCI")) {
                                        text5 = text5.replace("7N8NCI",
                                                listaEspecieDto.getTextNombreCientifico()  != null
                                                        ? ""+listaEspecieDto.getTextNombreCientifico()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


