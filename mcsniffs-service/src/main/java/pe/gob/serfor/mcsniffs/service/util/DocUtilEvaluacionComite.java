package pe.gob.serfor.mcsniffs.service.util;

import java.math.BigInteger;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;

import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;

public class DocUtilEvaluacionComite {

    private DocUtilEvaluacionComite() {
    }

    public static XWPFDocument setearResultadoObtenidoGabinete(SolicitudBosqueLocalEvaluacionDetalleDto obj,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (int i = 2; i <5; i++) {
                
            
                XWPFTableRow row = table.getRow(i);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("111")) {
                                    text = text.replace("111", obj.getValidado() == null || obj.getValidado() == false ? "" : "X");
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("222")) {
                                    text = text.replace("222", obj.getValidado() == null || obj.getValidado() == true ? "" : "X");
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("333")) {
                                    if(obj.getValidado()!=null && obj.getValidado() == false){
                                    text = text.replace("333", obj.getObservacion() == null ? "" : obj.getObservacion());
                                    r.setText(text, 0);
                                    } else{
                                        text = text.replace("n3", " ");
                                        r.setText(" ", 0);
                                    }

                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            for (int i = 2; i <5; i++) {
            table.removeRow(2);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearResultadoObtenidoCampo(List<SolicitudBosqueLocalArchivoEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);
            Integer i = 1;

            for (SolicitudBosqueLocalArchivoEntity element : lista) {
                
                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("i")) {
                                    text = text.replace("i", i.toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("data")) {
                                    text = text.replace("data", element.getNombreArchivo()==null?"":element.getNombreArchivo() );
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
                i++;
            }
            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


}