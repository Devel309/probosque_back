package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaRequisitosDto;

public interface CondicionMinimaService {

    ResultClassEntity validarCondicionesMinimas(ConsultaRequisitosDto consultaRequisitosDto, String token) throws Exception;
    ResultClassEntity registrarCondicionMinima(CondicionMinimaDto request);
    ResultClassEntity obtenerCondicionMinima(Integer idCondicionMinima) throws Exception;

}
