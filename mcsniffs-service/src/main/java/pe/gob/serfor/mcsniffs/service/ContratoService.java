package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ContratoAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoAdjuntosRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoValidacionesEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.GenerarCodContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.DescargarContratoPersonaJuridicaDto;

public interface ContratoService {
    ResultClassEntity registrarContrato(ContratoEntity obj) throws Exception;
    ResultClassEntity AdjuntarArchivosContrato(ContratoAdjuntosRequestEntity obj);
    ResultClassEntity ListarContrato(ContratoEntity obj);
    ResultEntity<ContratoAdjuntosEntity> ObtenerArchivosContrato(Integer IdContrato);
    ResultClassEntity ObtenerContrato(ContratoEntity IdContrato);
    //ResultArchivoEntity DescargarContrato(Integer IdContrato);
    ResultClassEntity RegistrarValidaciones(ContratoValidacionesEntity obj);
    ResultEntity<ContratoValidacionesEntity> ListarValidaciones(Integer IdContrato);
    ResultClassEntity actualizarContrato(ContratoEntity obj) throws Exception;
    ResultClassEntity ActualizarStatusContrato(ContratoEntity obj);
    ResultClassEntity obtenerContratoGeneral(ContratoEntity obj)  throws Exception;
    ResultClassEntity eliminarContratoArchivo(ContratoArchivoDto obj) throws Exception;
    ResultClassEntity generarCodigoContrato(GenerarCodContratoDto obj) throws Exception;
    ResultClassEntity listarContratoArchivo(ContratoArchivoDto dto) throws Exception;
    ResultClassEntity obtenerContratoArchivo(ContratoArchivoDto dto) throws Exception;
    ResultClassEntity actualizarEstadoContrato(ContratoEntity dto) throws Exception;
    ResultClassEntity listarComboContrato(ContratoEntity obj);
    ResultArchivoEntity descargarContratoPersonaJuridica(DescargarContratoPersonaJuridicaDto obj) throws Exception;
    ResultClassEntity listarContratoGeometria (String idsContrato);
    ResultClassEntity registrarResolucion(ResolucionDto obj) throws Exception;
    ResultClassEntity notificarResolucion(ResolucionDto param) throws Exception;
    ResultClassEntity obtenerResolucion(Integer nroDocumentoGestion) throws Exception;
    ResultClassEntity actualizarGeometria(ContratoEntity dto) throws Exception;
    ResultClassEntity actualizarEstadoRemitido(ContratoEntity obj) throws Exception;
    ResultArchivoEntity descargarModeloConcesionesMaderables(Integer idPlanManejo) throws Exception;
    ResultClassEntity listarGeneracionContrato(ContratoDto obj) throws Exception;
    ResultClassEntity obtenerPostulacion(ContratoDto obj);
    
}
