package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.AspectoFisicoFisiografiaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface AspectoFisicoFisiografiaService {

    ResultClassEntity ListarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception;
    // el registrar internamente, actualiza o registra a nivel de sp, si se le manda el id
    ResultClassEntity RegistrarAspectoFisicoFisiografia(List<AspectoFisicoFisiografiaEntity> listAspectoFisicoFisiografiaEntity) throws Exception;
    ResultClassEntity EliminarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception;
    ResultClassEntity ConfigurarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception;
}
