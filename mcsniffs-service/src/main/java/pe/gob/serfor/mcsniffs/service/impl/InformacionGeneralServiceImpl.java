package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralService;

import java.util.List;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad de la base de datos
 *               mcsniffs}
 *
 */
@Service("InformacionGeneralService")
public class InformacionGeneralServiceImpl implements InformacionGeneralService {
	private static final Logger log = LogManager.getLogger(InformacionGeneralServiceImpl.class);

	@Autowired
	private InformacionGeneralRepository infoGeneralRepo;

	@Override
	public ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity)
			throws Exception {
		return infoGeneralRepo.RegistrarInformacionGeneral(informacionGeneralEntity);
	}

	@Override
	public ResultClassEntity ActualizarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity)
			throws Exception {
		return infoGeneralRepo.ActualizarInformacionGeneral(informacionGeneralEntity);
	}

	@Override
	public ResultClassEntity EliminarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity)
			throws Exception {
		return null;
	}

	@Override
	public ResultClassEntity<InformacionGeneralDto> obtener(Integer idPlanManejo) throws Exception {
		return infoGeneralRepo.obtener(idPlanManejo);
	}

	@Override
	public ResultClassEntity<InformacionGeneralDto> guardar(InformacionGeneralDto dto) throws Exception {
		return infoGeneralRepo.guardar(dto);
	}


	@Override
	public ResultEntity obtenerInformacionPlanOperativo(InformacionGeneralDEMAEntity request) {

		return infoGeneralRepo.obtenerInformacionPlanOperativo(request);
	}

	@Override
	public ResultClassEntity actualizarInfGeneralResumido(List<InfGeneralEntity> list) throws Exception {

		return infoGeneralRepo.actualizarInfGeneralResumido(list);
	}

	@Override
	public List<InfGeneralEntity> listarInfGeneralResumido(InfGeneralEntity param) throws Exception {
		return infoGeneralRepo.listarInfGeneralResumido(param);
	}

}
