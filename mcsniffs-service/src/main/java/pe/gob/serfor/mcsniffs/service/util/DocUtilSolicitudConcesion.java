package pe.gob.serfor.mcsniffs.service.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;

import pe.gob.serfor.mcsniffs.entity.CronogramaActividadDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalActividadEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.FaunaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDetalle;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.MonitoreoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.MonitoreoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoEspecificoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.OrganizacionManejoEntity;
import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalDetEntity;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalEntity;
import pe.gob.serfor.mcsniffs.entity.RentabilidadManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionCriterioAreaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha.ProyeccionCosechaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarResAdminFavDesfavDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionVerticesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.AprovechamientoRFNMDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosAprovechamientoRFNMDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosEspeciesMuestreoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.TablaEspeciesMuestreo;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;

public class DocUtilSolicitudConcesion {

    private DocUtilSolicitudConcesion() {
    }

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static void processFile(Map<String, String> keys, XWPFDocument doc,
            List<InformacionGeneralDetalle> lstResult) throws IOException {
        List<XWPFParagraph> xwpfParagraphs = doc.getParagraphs();
        replaceTextInParagraphs(keys, xwpfParagraphs);
        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable xwpfTable : tables) {
            List<XWPFTableRow> tableRows = xwpfTable.getRows();
            for (XWPFTableRow xwpfTableRow : tableRows) {
                List<XWPFTableCell> tableCells = xwpfTableRow.getTableCells();
                for (XWPFTableCell xwpfTableCell : tableCells) {
                    xwpfParagraphs = xwpfTableCell.getParagraphs();
                    replaceTextInParagraphs(keys, xwpfParagraphs);
                }
            }
        }
    }

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Procesa un archivo word, reemplazando valores marcados en el
     *               documento}
     */
    public static void replaceTextInParagraphs(Map<String, String> keys, List<XWPFParagraph> xwpfParagraphs) {
        for (XWPFParagraph xwpfParagraph : xwpfParagraphs) {
            List<XWPFRun> xwpfRuns = xwpfParagraph.getRuns();
            for (XWPFRun xwpfRun : xwpfRuns) {
                String xwpfRunText = xwpfRun.getText(xwpfRun.getTextPosition());
                for (Map.Entry<String, String> entry : keys.entrySet()) {
                    if (xwpfRunText != null && xwpfRunText.contains(entry.getKey())) {
                        xwpfRunText = xwpfRunText.replaceAll(entry.getKey(), MapBuilder.toString(entry.getValue()));
                    }
                }
                xwpfRun.setText(xwpfRunText, 0);
            }
        }
    }

    public static String setearNull(String valor) {
        String value = "";
        if (valor != null && valor != "" && valor != "null") {
            value = valor;
        }
        return value;
    }



    public static XWPFDocument setearInformacionGeneral(InformacionGeneralDto data, XWPFDocument doc) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(0);
            Integer re = table.getRows().size() + 1;

            for (int j = 1; j < re; j++) {// 19 lineas de la tabla

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("txtNombre")) {
                                        text = text.replace("txtNombre", data.getRazonSocialTitularTH() == null ? ""
                                                : data.getRazonSocialTitularTH());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtRl")) {
                                        text = text.replace("txtRl",
                                                data.getNombreRepresentanteLegal() + " "
                                                        + data.getApellidoPaternoRepLegal() + " " +
                                                        data.getApellidoMaternoRepLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDni")) {
                                        text = text.replace("txtDni", data.getNumeroDocumentoRepLegal() == null ? ""
                                                : data.getNumeroDocumentoRepLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtRuc")) {
                                        text = text.replace("txtRuc",
                                                data.getNombreTitularTH() == null ? "" : data.getNombreTitularTH());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDom")) {
                                        text = text.replace("txtDom",
                                                data.getDomicilioLegal() == null ? "" : data.getDomicilioLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtCc")) {
                                        text = text.replace("txtCc", data.getNroContratoConcesion() == null ? ""
                                                : data.getNroContratoConcesion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDep")) {
                                        text = text.replace("txtDep", data.getNombreDepartamentoContrato() == null ? ""
                                                : data.getNombreDepartamentoContrato());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtProv")) {
                                        text = text.replace("txtProv", data.getNombreProvinciaContrato() == null ? ""
                                                : data.getNombreProvinciaContrato());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtFecha")) {
                                        text = text.replace("txtFecha",
                                                data.getFechaPresentacion() == null ? ""
                                                        : FechaUtil.obtenerFechaString(data.getFechaPresentacion(),
                                                                "dd/MM/yyyy"));
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtD")) {
                                        text = text.replace("txtD",
                                                data.getDuracion() == null ? "" : data.getDuracion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtFechI")) {
                                        text = text.replace("txtFechI", data.getFechaFin() == null ? ""
                                                : FechaUtil.obtenerFechaString(data.getFechaInicio(), "dd/MM/yyyy"));
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtFechF")) {
                                        text = text.replace("txtFechF", data.getFechaFin() == null ? ""
                                                : FechaUtil.obtenerFechaString(data.getFechaFin(), "dd/MM/yyyy"));
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtAtc")) {
                                        text = text.replace("txtAtc", data.getAreaTotalConcesion() == null ? ""
                                                : data.getAreaTotalConcesion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtAb")) {
                                        text = text.replace("txtAb", data.getAreaBosqueProduccionForestal() == null ? ""
                                                : data.getAreaBosqueProduccionForestal().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtAp")) {
                                        text = text.replace("txtAp", data.getAreaProteccion() == null ? ""
                                                : data.getAreaProteccion().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtBq")) {
                                        text = text.replace("txtBq", data.getNumeroBloquesQuinquenales() == null ? ""
                                                : data.getNumeroBloquesQuinquenales());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtPm")) {
                                        text = text.replace("txtPm", data.getPotencialMaderable() == null ? ""
                                                : data.getPotencialMaderable().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtVcap")) {
                                        text = text.replace("txtVcap",
                                                data.getVolumenCortaAnualPermisible() == null ? ""
                                                        : data.getVolumenCortaAnualPermisible().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtNomR")) {
                                        text = text.replace("txtNomR", data.getNombreRegenteForestal() == null ? ""
                                                : data.getNombreRegenteForestal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtDomR")) {
                                        text = text.replace("txtDomR", data.getDomicilioLegalRegente() == null ? ""
                                                : data.getDomicilioLegalRegente());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtCon")) {
                                        text = text.replace("txtCon",
                                                data.getContratoSuscritoTitularTituloHabilitante() == null ? ""
                                                        : data.getContratoSuscritoTitularTituloHabilitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtChprf")) {
                                        text = text.replace("txtChprf",
                                                data.getCertificadoHabilitacionProfesionalRegenteForestal() == null ? ""
                                                        : data.getCertificadoHabilitacionProfesionalRegenteForestal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtIrr")) {
                                        text = text.replace("txtIrr",
                                                data.getNumeroInscripcionRegistroRegente() == null ? ""
                                                        : data.getNumeroInscripcionRegistroRegente());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 1; j < re; j++) {
                table.removeRow(1);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearObjetivoGeneral(ObjetivoManejoEntity data, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("txtObjG")) {
                                text = text.replace("txtObjG", data.getGeneral() == null ? "" : data.getGeneral());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearObjetivoEspecifico(List<ObjetivoEspecificoManejoEntity> lista, XWPFDocument doc) {
        try {

            for (int i = 2; i < 11; i++) {// tablas de 3 a 11

                XWPFTable table = doc.getTables().get(i);
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);

                                if (text != null && text.trim().contains("txt")) {
                                    text = text.replace("txt", buscarObjetivoEspecifico(lista, i));
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.removeRow(0);
                table.addRow(copiedRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearUbicacionPolitica(List<InformacionBasicaUbigeoEntity> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(11);

            for (InformacionBasicaUbigeoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtDep")) {
                                    text = text.replace("txtDep", element.getNombreDepartamento() == null ? ""
                                            : element.getNombreDepartamento());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtPro")) {
                                    text = text.replace("txtPro",
                                            element.getNombreProvincia() == null ? "" : element.getNombreProvincia());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtDis")) {
                                    text = text.replace("txtDis",
                                            element.getNombreDistrito() == null ? "" : element.getNombreDistrito());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtCue")) {
                                    text = text.replace("txtCue",
                                            element.getCuenca() == null ? "" : element.getCuenca().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearCoordenadasUTM(List<InfBasicaAereaDetalleDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(12);

            for (InfBasicaAereaDetalleDto element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtPun")) {
                                    text = text.replace("txtPun",
                                            element.getPuntoVertice() == null ? "" : element.getPuntoVertice());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txEs")) {
                                    text = text.replace("txEs", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txNor")) {
                                    text = text.replace("txNor", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txRef")) {
                                    text = text.replace("txRef",
                                            element.getReferencia() == null ? "" : element.getReferencia());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInfoBasicDet(List<InfBasicaAereaDetalleDto> lista, XWPFDocument doc,
            Integer intTable, Integer initRow) {
        try {
            XWPFTable table = doc.getTables().get(intTable);

            for (InfBasicaAereaDetalleDto element : lista) {

                XWPFTableRow row = table.getRow(initRow);

                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("txtPr")) {
                                    text = text.replace("txtPr",
                                            element.getReferencia() == null ? "" : element.getReferencia());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtEi")) {
                                    text = text.replace("txtEi", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtNi")) {
                                    text = text.replace("txtNi", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtEf")) {
                                    text = text.replace("txtEf", element.getCoordenadaEsteFin() == null ? ""
                                            : element.getCoordenadaEsteFin().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtNf")) {
                                    text = text.replace("txtNf", element.getCoordenadaNorteFin() == null ? ""
                                            : element.getCoordenadaNorteFin().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtDis")) {
                                    text = text.replace("txtDis", element.getDistanciaKm() == null ? ""
                                            : element.getDistanciaKm().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtTi")) {
                                    text = text.replace("txtTi",
                                            element.getTiempo() == null ? "" : element.getTiempo().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtMed")) {
                                    text = text.replace("txtMed",
                                            element.getMedioTransporte() == null ? "" : element.getMedioTransporte());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txt")) {
                                    text = text.replace("txt",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtR")) {
                                    text = text.replace("txtR",
                                            element.getNombreRio() == null ? "" : element.getNombreRio());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtQ")) {
                                    text = text.replace("txtQ",
                                            element.getNombreQuebrada() == null ? "" : element.getNombreQuebrada());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("txtL")) {
                                    text = text.replace("txtL",
                                            element.getNombreLaguna() == null ? "" : element.getNombreLaguna());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(initRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearAspectosFisicosTab2(List<UnidadFisiograficaUMFEntity> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(16);

            for (UnidadFisiograficaUMFEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("1u")) {
                                    text = text.replace("1u",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("2u")) {
                                    text = text.replace("2u",
                                            element.getAccion() == null ? "" : element.getAccion() ? "X" : "");
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("3u")) {
                                    text = text.replace("3u",
                                            element.getArea() == null ? "" : element.getArea().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("4u")) {
                                    text = text.replace("4u",
                                            element.getPorcentaje() == null ? "" : element.getPorcentaje().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    private static String buscarObjetivoEspecifico(List<ObjetivoEspecificoManejoEntity> lista, Integer cuadro) {

        String marcar = "";

        for (ObjetivoEspecificoManejoEntity objetivoEspecificoManejoEntity : lista) {
            if (objetivoEspecificoManejoEntity.getIdTipoObjEspecifico() != null
                    && objetivoEspecificoManejoEntity.getIdTipoObjEspecifico().equals(String.valueOf(cuadro))) {
                marcar = "X";
                break;
            }
        }

        return marcar;
    }

    public static XWPFDocument setearInfoAnexo4(List<PGMFArchivoEntity> lista, XWPFDocument doc) {
        try {
            Boolean check = lista == null ? false : lista.isEmpty() ? false : true;

            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.trim().contains("txtA4")) {
                            text = text.replace("txtA4", check ? "X" : "");
                            r.setText(text, 0);
                        } else if (text != null && text.trim().contains("txtAn4")) {
                            text = text.replace("txtAn4", check ? "" : "X");
                            r.setText(text, 0);
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearFaunaSilvestre(List<FaunaEntity> listaFauna, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(19);

            for (FaunaEntity element : listaFauna) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {
                                    text = text.replace("n1", element.getNombre() == null ? "" : element.getNombre());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n2")) {
                                    text = text.replace("n2",
                                            element.getNombreCientifico() == null ? "" : element.getNombreCientifico());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("st")) {
                                    text = text.replace("st", element.getEstatus() == null ? "" : element.getEstatus());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearAspectosSocioEconomicos(List<InfBasicaAereaDetalleDto> listaAspecto,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(21);

            for (InfBasicaAereaDetalleDto element : listaAspecto) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("1")) {

                                    text = text.replace("1",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("2")) {

                                    text = text.replace("2", element.getNombre() == null ? "" : element.getNombre());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("3")) {

                                    text = text.replace("3", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("4")) {

                                    text = text.replace("4", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("5")) {

                                    text = text.replace("5",
                                            element.getAreaHa() == null ? "" : element.getAreaHa().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("6")) {

                                    text = text.replace("6", element.getNumeroFamilia() == null ? ""
                                            : element.getNumeroFamilia().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("7")) {

                                    text = text.replace("7", element.getSubsistencia() == null ? "( )"
                                            : !element.getSubsistencia() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n8")) {

                                    text = text.replace("n8", element.getPerenne() == null ? "( )"
                                            : !element.getPerenne() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n9")) {

                                    text = text.replace("n9", element.getGanaderia() == null ? "( )"
                                            : !element.getGanaderia() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n10")) {

                                    text = text.replace("n10",
                                            element.getCaza() == null ? "( )" : !element.getCaza() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n11")) {

                                    text = text.replace("n11",
                                            element.getPesca() == null ? "( )" : !element.getPesca() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n12")) {

                                    text = text.replace("n12",
                                            element.getMadera() == null ? "( )" : !element.getMadera() ? "( )" : "(X)");
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n13")) {

                                    text = text.replace("n13", element.getOtroProducto() == null ? "( )"
                                            : !element.getOtroProducto() ? "( )" : "(X)");
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearAspectosSocioEconomicos2(List<InfBasicaAereaDetalleDto> listaAspecto,
            XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(22);

            for (InfBasicaAereaDetalleDto element : listaAspecto) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("1")) {

                                    text = text.replace("1",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("2")) {

                                    text = text.replace("2", element.getNombre() == null ? "" : element.getNombre());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("3")) {

                                    text = text.replace("3", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("4")) {

                                    text = text.replace("4", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("5")) {

                                    text = text.replace("5",
                                            element.getAcceso() == null ? "" : element.getAcceso().toString());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaAntecedentes1(List<InfBasicaAereaDetalleDto> listaAnt, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(23);

            for (InfBasicaAereaDetalleDto element : listaAnt) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2",
                                            element.getMarcar() == null ? "" : element.getMarcar().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3",
                                            element.getEspecieExtraida() == null ? "" : element.getEspecieExtraida());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4",
                                            element.getObservaciones() == null ? "" : element.getObservaciones());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaAntecedentes2(List<InfBasicaAereaDetalleDto> listaAnt2, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(24);

            for (InfBasicaAereaDetalleDto element : listaAnt2) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {

                                    text = text.replace("s1",
                                            element.getConflicto() == null ? "" : element.getConflicto());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("s2")) {

                                    text = text.replace("s2",
                                            element.getSolucion() == null ? "" : element.getSolucion());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearDescripcionSistemaMonitoreo(String data, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("descr")) {
                                text = text.replace("descr", data == null ? "" : data);
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearDescripcionAccionMonitoreo(List<MonitoreoEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (MonitoreoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    primero: for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("mn")) {

                                    text = text.replace("mn",
                                            element.getMonitoreo() == null ? "" : element.getMonitoreo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("oper")) {
                                    XWPFParagraph lnewPara = cell.addParagraph();

                                    String dato = "";
                                    for (MonitoreoDetalleEntity i : element.getLstDetalle()) {
                                        dato = dato + i.getOperacion() + "\n";
                                    }
                                    text = text.replace("oper", "");
                                    r.setText(text, 0);

                                    XWPFRun lnewRun = lnewPara.createRun();
                                    if (dato.contains("\n")) {
                                        String[] lines = dato.split("\n");
                                        lnewRun.setText(lines[0], 0);
                                        for (int i = 1; i < lines.length; i++) {
                                            lnewRun.addBreak();
                                            lnewRun.setText(lines[i]);
                                        }
                                    } else {
                                        lnewRun.setText(dato, 0);
                                    }
                                    break primero;

                                } else if (text != null && text.trim().contains("descr")) {

                                    text = text.replace("descr",
                                            element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument addColumnFlujoCaja(List<Integer> lastColumn, XWPFDocument doc, Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            XWPFTableRow row2 = table.createRow();
            row2.getCell(0).setText("rub");
            int i = 1;
            for (Integer e : lastColumn) {
                table = doc.getTables().get(numTable);
                XWPFTableRow row = table.getRow(0);
                row.addNewTableCell().setText("Año" + e);

                row2.createCell();
                row2.getCell(i).setText(e + "");

                i++;
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearFlujoCaja(List<RentabilidadManejoForestalDto> lista, XWPFDocument doc,
            Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            for (RentabilidadManejoForestalDto element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("rub")) {
                                    text = text.replace("rub", element.getRubro());
                                    r.setText(text, 0);
                                } else if (text != null && !text.trim().contains("rub")) {
                                    String replace = "";

                                    loop: for (RentabilidadManejoForestalDetalleEntity e : element
                                            .getListRentabilidadManejoForestalDetalle()) {
                                        if (text.trim().equals(e.getAnio().toString())) {
                                            replace = e.getMonto().toString();
                                            break loop;
                                        }
                                    }
                                    r.setText(replace, 0);
                                }

                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListarNecesidades(List<RentabilidadManejoForestalDto> lista, XWPFDocument doc,
            Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                primero: for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("n1")) {
                                String dato = "";
                                XWPFParagraph lnewPara = cell.addParagraph();

                                for (RentabilidadManejoForestalDto element : lista) {
                                    dato = dato + element.getDescripcion() + "\n";
                                }
                                text = text.replace("n1", "");
                                r.setText(text, 0);

                                XWPFRun lnewRun = lnewPara.createRun();
                                if (dato.contains("\n")) {
                                    String[] lines = dato.split("\n");
                                    lnewRun.setText(lines[0], 0);
                                    for (int i = 1; i < lines.length; i++) {
                                        lnewRun.addBreak();
                                        lnewRun.setText(lines[i]);
                                    }
                                } else {
                                    lnewRun.setText(dato, 0);
                                }
                                break primero;
                            }
                        }
                    }
                }

            }
            table.addRow(copiedRow);
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearFomulacionPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtFoPMF")) {
                                    text = text.replace("txtFoPMF",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearImplementacionPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtImPMF")) {
                                    text = text.replace("txtImPMF",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearComitesPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtGestBosq")) {
                                    text = text.replace("txtGestBosq",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearReconicimientoPMF(List<ParticipacionComunalEntity> lista, XWPFDocument doc, Integer numTabla) {
        try {
            List<ParticipacionComunalDetEntity> lstListaDetalle = new ArrayList<ParticipacionComunalDetEntity>();
            if (lista.size() > 0) {
                ParticipacionComunalEntity PGMFEVALI = lista.get(0);
                lstListaDetalle = PGMFEVALI.getLstDetalle();
            }

            XWPFTable table = doc.getTables().get(numTabla);

            for (ParticipacionComunalDetEntity element : lstListaDetalle) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("txtMecPar")) {
                                    text = text.replace("txtMecPar",
                                            element.getActividad() == null ? "" : element.getActividad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtMetod")) {
                                    text = text.replace("txtMetod",
                                            element.getMetodologia() == null ? "" : element.getMetodologia());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("txtLug")) {

                                    String res = element.getLugar() == null ? "" : element.getLugar() + " ";
                                    String res2 = element.getFecha() == null ? "" : element.getFecha().toString();
                                    r.setText(res + res2, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearAnexoMapa(ResultEntity<PGMFArchivoEntity> archivo, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("imagenMapa")) {
                                text = text.replace("imagenMapa", "");
                                r.setText(text, 0);
                                if (archivo.getData().size() > 0) {
                                    PGMFArchivoEntity imagen = archivo.getData().get(0);
                                    if (imagen.getDocumento() != null) {

                                        InputStream is = new ByteArrayInputStream(imagen.getDocumento());
                                        r.addBreak();
                                        r.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, "Anexo Mapa", Units.toEMU(300),
                                                Units.toEMU(300));
                                        is.close();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearCronogramaAct(List<CronogramaActividadEntity> lista, XWPFDocument doc,
            Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            for (CronogramaActividadEntity element : lista) {

                XWPFTableRow row = table.getRow(6);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("act")) {
                                    text = text.replace("act", element.getActividad());
                                    r.setText(text, 0);
                                } else if (text != null && !text.trim().contains("act")) {
                                    text = buscarCronogramaAct(element.getDetalle(), text);
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(6);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private static String buscarCronogramaAct(List<CronogramaActividadDetalleEntity> detalle, String codigo) {

        String data = "";

        for (CronogramaActividadDetalleEntity e : detalle) {
            if (codigo.equals("1") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(1)) {
                data = "X";
                break;
            } else if (codigo.equals("2") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(2)) {
                data = "X";
                break;
            } else if (codigo.equals("3") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(3)) {
                data = "X";
                break;
            } else if (codigo.equals("4") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(4)) {
                data = "X";
                break;
            } else if (codigo.equals("5") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(5)) {
                data = "X";
                break;
            } else if (codigo.equals("6") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(6)) {
                data = "X";
                break;
            } else if (codigo.equals("7") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(7)) {
                data = "X";
                break;
            } else if (codigo.equals("8") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(8)) {
                data = "X";
                break;
            } else if (codigo.equals("9") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(9)) {
                data = "X";
                break;
            } else if (codigo.equals("10") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(10)) {
                data = "X";
                break;
            } else if (codigo.equals("11") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(11)) {
                data = "X";
                break;
            } else if (codigo.equals("12") && e.getAnio().equals(1) && e.getMes() != null && e.getMes().equals(12)) {
                data = "X";
                break;
            } else if (codigo.equals("a") && e.getAnio() != null && e.getAnio().equals(2)) {
                data = "X";
                break;
            } else if (codigo.equals("b") && e.getAnio() != null && e.getAnio().equals(3)) {
                data = "X";
                break;
            } else if (codigo.equals("c") && e.getAnio() != null && e.getAnio().equals(4)) {
                data = "X";
                break;
            } else if (codigo.equals("d") && e.getAnio() != null && e.getAnio().equals(5)) {
                data = "X";
                break;
            } else if (codigo.equals("e") && e.getAnio() != null && e.getAnio().equals(6)) {
                data = "X";
                break;
            } else if (codigo.equals("f") && e.getAnio() != null && e.getAnio().equals(7)) {
                data = "X";
                break;
            } else if (codigo.equals("g") && e.getAnio() != null && e.getAnio().equals(8)) {
                data = "X";
                break;
            } else if (codigo.equals("h") && e.getAnio() != null && e.getAnio().equals(9)) {
                data = "X";
                break;
            } else if (codigo.equals("i") && e.getAnio() != null && e.getAnio().equals(10)) {
                data = "X";
                break;
            } else if (codigo.equals("j") && e.getAnio() != null && e.getAnio().equals(11)) {
                data = "X";
                break;
            } else if (codigo.equals("k") && e.getAnio() != null && e.getAnio().equals(12)) {
                data = "X";
                break;
            } else if (codigo.equals("l") && e.getAnio() != null && e.getAnio().equals(13)) {
                data = "X";
                break;
            } else if (codigo.equals("m") && e.getAnio() != null && e.getAnio().equals(14)) {
                data = "X";
                break;
            } else if (codigo.equals("n") && e.getAnio() != null && e.getAnio().equals(15)) {
                data = "X";
                break;
            } else if (codigo.equals("o") && e.getAnio() != null && e.getAnio().equals(16)) {
                data = "X";
                break;
            } else if (codigo.equals("p") && e.getAnio() != null && e.getAnio().equals(17)) {
                data = "X";
                break;
            } else if (codigo.equals("q") && e.getAnio() != null && e.getAnio().equals(18)) {
                data = "X";
                break;
            } else if (codigo.equals("r") && e.getAnio() != null && e.getAnio().equals(19)) {
                data = "X";
                break;
            } else if (codigo.equals("s") && e.getAnio() != null && e.getAnio().equals(20)) {
                data = "X";
                break;
            }

        }

        return data;

    }

    public static XWPFDocument setearObjetivosCapacitacionTab10(List<CapacitacionDto> lista, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (CapacitacionDto element : lista) {

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("obj")) {
                                    text = text.replace("obj", element.getTema() == null ? "" : element.getTema());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearActividadesPlanUMFTab10(List<CapacitacionDto> lista, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (CapacitacionDto element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("act1")) {
                                    text = text.replace("act1", element.getTema() == null ? "" : element.getTema());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("act2")) {
                                    text = text.replace("act2", element.getPersonaCapacitar() == null ? ""
                                            : element.getPersonaCapacitar().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("act3")) {
                                    text = text.replace("act3", element.getIdTipoModalidad() == null ? ""
                                            : element.getIdTipoModalidad().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("act4")) {
                                    text = text.replace("act4",
                                            element.getLugar() == null ? "" : element.getLugar().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaFuncionesTab1(List<OrganizacionManejoEntity> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrganizacionManejoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    primero: for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a",
                                            element.getNombreSubTipo() == null ? "" : element.getNombreSubTipo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {
                                    XWPFParagraph lnewPara = cell.addParagraph();

                                    String dato = "";
                                    for (OrganizacionManejoEntity i : element.getListaDetalle()) {
                                        dato = dato + i.getFuncion() + "\n";
                                    }
                                    text = text.replace("b", "");
                                    r.setText(text, 0);

                                    XWPFRun lnewRun = lnewPara.createRun();
                                    if (dato.contains("\n")) {
                                        String[] lines = dato.split("\n");
                                        lnewRun.setText(lines[0], 0);
                                        for (int i = 1; i < lines.length; i++) {
                                            lnewRun.addBreak();
                                            lnewRun.setText(lines[i]);
                                        }
                                    } else {
                                        lnewRun.setText(dato, 0);
                                    }
                                    break primero;

                                } else if (text != null && text.trim().contains("c")) {
                                    String descripcion = "";
                                    if (element.getListaDetalle() != null && !element.getListaDetalle().isEmpty()) {
                                        descripcion = element.getListaDetalle().get(0).getDescripcion();
                                    }

                                    text = text.replace("c", descripcion);
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("d")) {
                                    String numero = "";
                                    if (element.getListaDetalle() != null && !element.getListaDetalle().isEmpty()) {
                                        numero = element.getListaDetalle().get(0).getNumero().toString();
                                    }

                                    text = text.replace("d", numero);
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaFuncionesTab2(List<OrganizacionManejoEntity> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrganizacionManejoEntity element : lista) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("e")) {
                                    text = text.replace("e",
                                            element.getTipoActividad() == null ? "" : element.getTipoActividad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("f")) {
                                    text = text.replace("f",
                                            element.getFuncion() == null ? "" : element.getFuncion().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("g")) {
                                    text = text.replace("g",
                                            element.getNumero() == null ? "" : element.getNumero().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaFuncionesTab3(List<OrganizacionManejoEntity> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrganizacionManejoEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("h")) {
                                    text = text.replace("h",
                                            element.getTipoActividad() == null ? "" : element.getTipoActividad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("i")) {
                                    text = text.replace("i", element.getMaquinaEquipo() == null ? ""
                                            : element.getMaquinaEquipo().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("j")) {
                                    text = text.replace("j",
                                            element.getNumero() == null ? "" : element.getNumero().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarResumenActividadPOAC(List<ResumenActividadPoEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            System.out.println(lstLista.size());
            for (ResumenActividadPoEntity detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (detalleEntity.getCodigoSubResumen().equals("POACRA")) {
                                        if (text5 != null) {
                                            if (text5.contains("2NroResolucion")) {
                                                text5 = text5.replace("2NroResolucion", "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("2NroPc")) {
                                                text5 = text5.replace("2NroPc",
                                                        detalleEntity.getNroPcs() != null
                                                                ? detalleEntity.getNroPcs().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            } else if (text5.contains("2Area")) {
                                                text5 = text5.replace("2Area",
                                                        detalleEntity.getAreaHa() != null
                                                                ? detalleEntity.getAreaHa().toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */
    public static XWPFDocument generarResumenActividadFrenteCortaPOAC(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {

        try {

            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(1);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("2actividad")) {
                                            text = text.replace("2actividad",
                                                    detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("2indicador")) {
                                            text = text.replace("2indicador",
                                                    detalleEntity.getIndicador() != null ? detalleEntity.getIndicador()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("2programado")) {
                                            text = text.replace("2programado",
                                                    detalleEntity.getProgramado() != null
                                                            ? detalleEntity.getProgramado()
                                                            : "");
                                            r.setText(text, 0);
                                        } else if (text.contains("2realizado")) {
                                            text = text.replace("2realizado",
                                                    detalleEntity.getRealizado() != null ? detalleEntity.getRealizado()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOACPos(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getTipoResumen() == 1).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(3);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("22positivos")) {
                                            text = text.replace("22positivos",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOACNeg(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getTipoResumen() == 2).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(4);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("22negativos")) {
                                            text = text.replace("22negativos",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarResumenActividadResultadosPOACRec(List<ResumenActividadPoEntity> lstLista,
            XWPFDocument doc) {
        try {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstLista.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                    .filter(eval -> eval.getTipoResumen() == 3).collect(Collectors.toList());

            XWPFTable table = doc.getTables().get(5);
            XWPFTableRow row = table.getRow(0);
            for (ResumenActividadPoDetalleEntity detalleEntity : lstListaDetalle2) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text != null) {
                                        if (text.contains("22recomendaciones")) {
                                            text = text.replace("22recomendaciones",
                                                    detalleEntity.getResumen() != null ? detalleEntity.getResumen()
                                                            : "");
                                            r.setText(text, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void procesarObjetivosPOAC(List<ObjetivoDto> lstLista, XWPFDocument doc) {
        try {

            // objetivos especificos
            XWPFTable table = doc.getTables().get(6);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            List<ObjetivoDto> maderables = lstLista.stream().filter(m -> m.getDetalle().equals("Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> noMaderables = lstLista.stream().filter(n -> n.getDetalle().equals("No Maderable"))
                    .collect(Collectors.toList());
            List<ObjetivoDto> lstOrdenada = new ArrayList<>();
            lstOrdenada.addAll(maderables);
            lstOrdenada.addAll(noMaderables);
            int i = 0;
            for (ObjetivoDto detalleEntity : lstOrdenada) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("31tipo")) {
                                        String detalle = "";
                                        if (i == 0)
                                            detalle = "Maderable";
                                        if (i == maderables.size())
                                            detalle = "No Maderable";
                                        text5 = text5.replace("31tipo", detalle);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("31E")) {
                                        String check;
                                        if (detalleEntity.getActivo().toString().equals("A")) {
                                            check = "(X)";
                                        } else {
                                            check = "( )";
                                        }
                                        text5 = text5.replace("31E", check);
                                        r.setText(text5, 0);
                                    } else if (text5.contains("31producto")) {
                                        text5 = text5.replace("31producto", detalleEntity.getDescripcionDetalle() + "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */
    public static XWPFDocument generarLaboresSilviculturalesPOAC(ActividadSilviculturalDto lstLista, XWPFDocument doc)
            throws IOException {
        try {
            XWPFTable table = doc.getTables().get(23);
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("laboresx")) {
                                        text = text.replace("laboresx",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("marcax")) {
                                        if (lstLista.getDetalle().get(i).getAccion()) {
                                            text = text.replace("marcax", "X");
                                        } else {
                                            text = text.replace("marcax", "");
                                        }
                                        r.setText(text, 0);
                                    } else if (text.contains("descripcionx")) {
                                        text = text.replace("descripcionx",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument generarActividadSilviculturalAplicacionPOAC(ActividadSilviculturalDto lstLista,
            XWPFDocument doc) throws IOException {
        try {

            XWPFTable table = doc.getTables().get(23);//

            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("6tratamiento")) {
                                        text = text.replace("6tratamiento",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("6esta")) {
                                        if (lstLista.getDetalle().get(i).getAccion()) {
                                            text = text.replace("6esta", "X");
                                        } else {
                                            text = text.replace("6esta", "");
                                        }
                                        r.setText(text, 0);
                                    } else if (text.contains("6descripcion")) {
                                        text = text.replace("6descripcion",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarActividadSilviculturalReforestacionPOAC(ActividadSilviculturalDto lstLista,
            XWPFDocument doc) throws IOException {
        try {

            XWPFTable table = doc.getTables().get(24);//

            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("6reforestacion")) {
                                        text = text.replace("6reforestacion",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("6descripcionB")) {
                                        text = text.replace("6descripcionB",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarProteccionBosquePOAC(List<ProteccionBosqueDetalleDto> lstLista, XWPFDocument doc) {
        try {
            // 7.1 Demarcación y Mantenimiento de Linderos
            XWPFTable table = doc.getTables().get(25);
            XWPFTableRow row = table.getRow(1);
            int i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACDEMA")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("71sistema")) {
                                            text5 = text5.replace("71sistema", detalleEntity.getTipoMarcacion());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71accion")) {
                                            text5 = text5.replace("71accion",
                                                    detalleEntity.getNuAccion() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71modo")) {
                                            text5 = text5.replace("71modo", detalleEntity.getImplementacion());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);

            // 7.2.1. Análisis de Impacto Ambiental
            table = doc.getTables().get(26);
            row = table.getRow(2);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACANIM")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {

                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("71factor")) {
                                            text5 = text5.replace("71factor", detalleEntity.getFactorAmbiental());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71impacto")) {
                                            text5 = text5.replace("71impacto", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71c")) {
                                            text5 = text5.replace("71c",
                                                    detalleEntity.getNuCenso() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71dema")) {
                                            text5 = text5.replace("71dema",
                                                    detalleEntity.getNuDemarcacionLineal() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71constcamp")) {
                                            text5 = text5.replace("71constcamp",
                                                    detalleEntity.getNuConstruccionCampamento() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71constcam")) {
                                            text5 = text5.replace("71constcam",
                                                    detalleEntity.getNuConstruccionCamino() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71t")) {
                                            text5 = text5.replace("71t",
                                                    detalleEntity.getNuTala() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71a")) {
                                            text5 = text5.replace("71a",
                                                    detalleEntity.getNuArrastre() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        } else if (text5.contains("71o")) {
                                            text5 = text5.replace("71o",
                                                    detalleEntity.getNuOtra() == true ? "X" : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 2 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            // 7.2.2. Plan de gestión ambiental
            // Programa preventivo - corrector
            table = doc.getTables().get(27);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("POACPRCO")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("722actividad")) {
                                            text5 = text5.replace("722actividad", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722descripcion")) {
                                            text5 = text5.replace("722descripcion", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722medidas")) {
                                            text5 = text5.replace("722medidas", detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            //  Programa de Vigilancia y Seguimiento
            table = doc.getTables().get(28);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("POACVISE")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("722actividad2")) {
                                            text5 = text5.replace("722actividad2", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722descripcion2")) {
                                            text5 = text5.replace("722descripcion2", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722medidasAmbiental")) {
                                            text5 = text5.replace("722medidasAmbiental",
                                                    detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);

            //  Programa de contingencia ambiental:
            table = doc.getTables().get(29);
            row = table.getRow(1);
            i = 0;
            for (ProteccionBosqueDetalleDto detalleEntity : lstLista) {
                if (detalleEntity.getCodPlanGeneralDet().equals("POACPGA") &&
                        detalleEntity.getSubCodPlanGeneralDet().equals("POACCOAM")) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("722contin")) {
                                            text5 = text5.replace("722contin", detalleEntity.getActividad());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722descripcion3")) {
                                            text5 = text5.replace("722descripcion3", detalleEntity.getImpacto());
                                            r.setText(text5, 0);
                                        } else if (text5.contains("722medidas3")) {
                                            text5 = text5.replace("722medidas3",
                                                    detalleEntity.getMitigacionAmbiental());
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    table.addRow(copiedRow, 1 + i);
                    i++;
                }
            }
            table.removeRow(table.getRows().size() - 1);
            doc.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 17-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarMonitoreoPOAC(List<MonitoreoDetalleEntity> lstLista, XWPFDocument doc) {
        try {

            XWPFTable table = doc.getTables().get(30);
            XWPFTableRow row = table.getRow(1);
            int i = 0;
            for (MonitoreoDetalleEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("8actividades")) {
                                        text5 = text5.replace("8actividades", detalleEntity.getActividad());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("8descripcion")) {
                                        text5 = text5.replace("8descripcion", detalleEntity.getDescripcion());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("8responsable")) {
                                        text5 = text5.replace("8responsable", detalleEntity.getResponsable());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(table.getRows().size() - 1);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void generarParticipacionComunalPOAC(List<ParticipacionComunalEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(32);
            System.out.println(lstLista.size());
            XWPFTableRow row2 = table.getRow(1);
            int i = 0;
            for (ParticipacionComunalEntity detalleEntity : lstLista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {

                                    if (text5.contains("91actividades")) {
                                        text5 = text5.replace("91actividades",
                                                detalleEntity.getActividad() != null ? detalleEntity.getActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("91responsable")) {
                                        text5 = text5.replace("91responsable",
                                                detalleEntity.getResponsable() != null ? detalleEntity.getResponsable()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("91seguimiento")) {
                                        text5 = text5.replace("91seguimiento",
                                                detalleEntity.getSeguimientoActividad() != null
                                                        ? detalleEntity.getSeguimientoActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstLista.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static XWPFDocument generarActividadSilviculturalOrganizPOAC(ActividadSilviculturalDto lstLista,
            XWPFDocument doc) throws IOException {
        try {
            XWPFTable table = doc.getTables().get(34);//
            XWPFTableRow row = table.getRow(1);
            for (int i = 0; i < lstLista.getDetalle().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("11actividad")) {
                                        text = text.replace("11actividad",
                                                lstLista.getDetalle().get(i).getActividad() != null
                                                        ? lstLista.getDetalle().get(i).getActividad()
                                                        : "");
                                        r.setText(text, 0);
                                    } else if (text.contains("11forma")) {
                                        text = text.replace("11forma",
                                                lstLista.getDetalle().get(i).getDescripcionDetalle() != null
                                                        ? lstLista.getDetalle().get(i).getDescripcionDetalle()
                                                        : "");
                                        r.setText(text, 0);
                                    }

                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarTotalesProgramaInversionPOAC(Double[] totales, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(35);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            XWPFTableRow row3 = table.getRow(3);

            // total ingresos
            XWPFTableRow copiedRowTotalIngr = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            CTTrPr trPrIngr = copiedRowTotalIngr.getCtRow().addNewTrPr();
            CTHeight htIngr = trPrIngr.addNewTrHeight();
            htIngr.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalIngr.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12INI")) {
                                text5 = text5.replace("12INI", totales[0] != null ? totales[0].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalIngr, 1);
            table.removeRow(2);
            // total egresos
            XWPFTableRow copiedRowTotalEgre = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPrEgre = copiedRowTotalEgre.getCtRow().addNewTrPr();
            CTHeight htEgre = trPrEgre.addNewTrHeight();
            htEgre.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalEgre.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12INE")) {
                                text5 = text5.replace("12INE", totales[1] != null ? totales[1].toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalEgre, 2);
            table.removeRow(3);
            // total neto
            XWPFTableRow copiedRowTotalNeto = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table);
            CTTrPr trPrNeto = copiedRowTotalNeto.getCtRow().addNewTrPr();
            CTHeight htNeto = trPrNeto.addNewTrHeight();
            htNeto.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotalNeto.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5.contains("12BN")) {
                                Double totalNeto = (totales[0] != null ? totales[0] : 0.00)
                                        - (totales[1] != null ? totales[1] : 0.00);
                                text5 = text5.replace("12BN", totalNeto != null ? totalNeto.toString() : "");
                                r.setText(text5, 0);
                            }
                        }
                    }
                }
            }
            table.addRow(copiedRowTotalNeto, 3);
            table.removeRow(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */
    public static void generarIngresoProgramaInversionPOAC(List<RentabilidadManejoForestalDto> lstIngresos,
            XWPFDocument doc, Double[] total) {
        try {
            total[0] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstIngresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(36);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("121INGRMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("121INGRTOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr1 = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb1 = tcPr1.addNewTcBorders();
                tb1.addNewRight().setVal(STBorder.SINGLE);
                tb1.addNewTop().setVal(STBorder.SINGLE);
                tb1.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto ingreso : lstIngresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("121INGRRUB")) {
                                        text5 = text5.replace("121INGRRUB",
                                                ingreso.getRubro() != null ? ingreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("INGR121DESC")) {
                                        text5 = text5.replace("INGR121DESC",
                                                ingreso.getDescripcion() != null ? ingreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121INGRMONTOT")) {
                                        text5 = text5.replace("121INGRMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[0] += totalFila;
                                    } else if (text5.contains("121INGRMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : ingreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("121INGRMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("121INGRMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("121INGRMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstIngresos.size());

            // totales de ingresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("121INGRTOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("121INGRTOTALA" + integer.toString())) {
                                                text5 = text5.replace("121INGRTOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("121INGRTOTALT")) {
                                    text5 = text5.replace("121INGRTOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstIngresos.size());
            table.removeRow(2 + lstIngresos.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarEgresoProgramaInversionPOAC(List<RentabilidadManejoForestalDto> lstEgresos,
            XWPFDocument doc, Double[] total) {
        try {
            total[1] = 0.00;
            List<Integer> lstAnios = new ArrayList<>();
            lstEgresos.stream().forEach((a) -> {
                a.getListRentabilidadManejoForestalDetalle().forEach((b) -> {
                    lstAnios.add(b.getAnio());
                });
            });
            List<Integer> lstAniosFinal = lstAnios.stream().sorted(Comparator.naturalOrder()).distinct()
                    .collect(Collectors.toList());
            XWPFTable table = doc.getTables().get(37);
            XWPFTableRow row0 = table.getRow(0);
            XWPFTableRow row1 = table.getRow(1);
            XWPFTableRow row2 = table.getRow(2);
            // var para totales por año
            Map<Integer, Double> totales_anios = new HashMap<>();
            // backup de la ultima columna
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
            XWPFTableCell copiedCell3 = copiedRow0.getCell(3);
            XWPFTableRow copiedRow1 = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
            XWPFTableCell copiedCellData3 = copiedRow1.getCell(3);
            XWPFTableRow copiedRow2 = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            XWPFTableCell copiedCellTotal3 = copiedRow2.getCell(2);
            // eliminar las celdas de la columna
            {
                row0.removeCell(3);
                row1.removeCell(3);
                row2.removeCell(2);
            }
            // insertar columnas por años
            for (Integer integer : lstAniosFinal) {
                if (!totales_anios.containsKey(integer))
                    totales_anios.put(integer, 0.00);
                if (!integer.equals(1)) {
                    // row0
                    XWPFTableCell cell0 = row0.createCell();
                    XWPFParagraph p0 = cell0.addParagraph();
                    cell0.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                    cell0.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr = cell0.getCTTc().addNewTcPr();
                    CTTblWidth tcW = tcPr.addNewTcW();
                    tcW.setW(BigInteger.valueOf(1008));
                    CTTcBorders tb = tcPr.addNewTcBorders();
                    tb.addNewRight().setVal(STBorder.SINGLE);
                    tb.addNewTop().setVal(STBorder.SINGLE);
                    tb.addNewBottom().setVal(STBorder.SINGLE);

                    XWPFRun r0 = p0.createRun();
                    r0.setText("Año " + integer, 0);
                    r0.setFontFamily("Arial");
                    r0.setFontSize(9);
                    r0.setBold(true);
                    p0.setAlignment(ParagraphAlignment.CENTER);
                    // row1
                    XWPFTableCell cell1 = row1.createCell();
                    XWPFParagraph p1 = cell1.addParagraph();
                    cell1.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr1 = cell1.getCTTc().addNewTcPr();
                    CTTcBorders tb1 = tcPr1.addNewTcBorders();
                    tb1.addNewRight().setVal(STBorder.SINGLE);
                    tb1.addNewTop().setVal(STBorder.SINGLE);
                    tb1.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r1 = p1.createRun();
                    r1.setText("121EGREMONTOA" + integer, 0);
                    r1.setFontFamily("Arial");
                    r1.setFontSize(8);
                    r1.setBold(true);
                    p1.setAlignment(ParagraphAlignment.CENTER);
                    // row2
                    XWPFTableCell cell2 = row2.createCell();
                    XWPFParagraph p2 = cell2.addParagraph();
                    cell2.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                    CTTcPr tcPr2 = cell2.getCTTc().addNewTcPr();
                    CTTcBorders tb2 = tcPr2.addNewTcBorders();
                    tb2.addNewRight().setVal(STBorder.SINGLE);
                    tb2.addNewTop().setVal(STBorder.SINGLE);
                    tb2.addNewBottom().setVal(STBorder.SINGLE);
                    XWPFRun r2 = p2.createRun();
                    r2.setText("121EGRETOTALA" + integer, 0);
                    r2.setFontFamily("Arial");
                    r2.setFontSize(9);
                    r2.setBold(true);
                    p2.setAlignment(ParagraphAlignment.CENTER);
                }
            }
            // recuperar ultima columna
            // row0
            {
                XWPFTableCell newCell = row0.createCell();
                XWPFParagraph pCab = newCell.addParagraph();
                newCell.getCTTc().addNewTcPr().addNewShd().setFill("A6A6A6");
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCell.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rCab = pCab.createRun();
                rCab.setText(copiedCell3.getText(), 0);
                rCab.setFontFamily("Arial");
                rCab.setFontSize(9);
                rCab.setBold(true);
                pCab.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 1
            {
                XWPFTableCell newCellData = row1.createCell();
                XWPFParagraph pData = newCellData.addParagraph();
                newCellData.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellData.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rData = pData.createRun();
                rData.setText(copiedCellData3.getText(), 0);
                rData.setFontFamily("Arial");
                rData.setFontSize(8);
                rData.setBold(true);
                pData.setAlignment(ParagraphAlignment.CENTER);
            }
            // row 2
            {
                XWPFTableCell newCellTotal = row2.createCell();
                XWPFParagraph pTotal = newCellTotal.addParagraph();
                newCellTotal.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTTcPr tcPr = newCellTotal.getCTTc().addNewTcPr();
                CTTcBorders tb = tcPr.addNewTcBorders();
                tb.addNewRight().setVal(STBorder.SINGLE);
                tb.addNewTop().setVal(STBorder.SINGLE);
                tb.addNewBottom().setVal(STBorder.SINGLE);
                XWPFRun rTotal = pTotal.createRun();
                rTotal.setText(copiedCellTotal3.getText(), 0);
                rTotal.setFontFamily("Arial");
                rTotal.setFontSize(9);
                rTotal.setBold(true);
                pTotal.setAlignment(ParagraphAlignment.CENTER);
            }
            // llenando data
            int i = 0;
            for (RentabilidadManejoForestalDto egreso : lstEgresos) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                Double totalFila = 0.00;

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("121EGRERUB")) {
                                        text5 = text5.replace("121EGRERUB",
                                                egreso.getRubro() != null ? egreso.getRubro().toString() : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121EGREDESC")) {
                                        text5 = text5.replace("121EGREDESC",
                                                egreso.getDescripcion() != null ? egreso.getDescripcion().toString()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("121EGREMONTOT")) {
                                        text5 = text5.replace("121EGREMONTOT",
                                                totalFila != null ? totalFila.toString() : "");
                                        r.setText(text5, 0);
                                        total[1] += totalFila;
                                    } else if (text5.contains("121EGREMONTOA")) {
                                        for (Integer integer : lstAniosFinal) {
                                            for (RentabilidadManejoForestalDetalleEntity detalleEntity : egreso
                                                    .getListRentabilidadManejoForestalDetalle()) {
                                                if (text5.equals("121EGREMONTOA" + integer.toString())
                                                        && integer == detalleEntity.getAnio()) {
                                                    text5 = text5.replace("121EGREMONTOA" + integer.toString(),
                                                            detalleEntity.getMonto() != null
                                                                    ? detalleEntity.getMonto().toString()
                                                                    : "");
                                                    r.setText(text5, 0);
                                                    Double s = detalleEntity.getMonto() != null
                                                            ? detalleEntity.getMonto()
                                                            : 0.00;
                                                    totales_anios.replace(integer, totales_anios.get(integer) + s);
                                                    totalFila += s;
                                                    break;
                                                }
                                            }
                                        }
                                        if (text5.contains("121EGREMONTOA")) {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 1 + i);
                i++;
            }
            table.removeRow(1 + lstEgresos.size());

            // totales de egresos
            XWPFTableRow copiedRowTotal = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
            CTTrPr trPr = copiedRowTotal.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));
            for (XWPFTableCell cell : copiedRowTotal.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("121EGRETOTALA")) {
                                    for (Integer integer : lstAniosFinal) {
                                        if (totales_anios.containsKey(integer)) {
                                            if (text5.trim().equals("121EGRETOTALA" + integer.toString())) {
                                                text5 = text5.replace("121EGRETOTALA" + integer.toString(),
                                                        totales_anios.get(integer) != null
                                                                ? totales_anios.get(integer).toString()
                                                                : "");
                                                r.setText(text5, 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                } else if (text5.contains("121EGRETOTALT")) {
                                    text5 = text5.replace("121EGRETOTALT", total[0] != null ? total[0].toString() : "");
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }

            table.addRow(copiedRowTotal, 1 + lstEgresos.size());
            table.removeRow(2 + lstEgresos.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarCronogramaActividadesPOAC(List<CronogramaActividadEntity> lstCrono, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(38);
            XWPFTableRow row1 = table.getRow(4);

            int i = 0;
            for (CronogramaActividadEntity actividad : lstCrono) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row1.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("13actividad")) {
                                        text5 = text5.replace("13actividad",
                                                actividad.getActividad() != null ? actividad.getActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("M")) {
                                        List<CronogramaActividadDetalleEntity> lstDetalle = actividad.getDetalle()
                                                .stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                                        if (lstDetalle != null) {
                                            boolean breaker = false;
                                            for (CronogramaActividadDetalleEntity detalle : lstDetalle) {
                                                for (int mes = 1; mes <= 12; mes++) {
                                                    if (detalle.getMes() == mes && text5.trim().equals("M" + mes)) {
                                                        text5 = text5.replace("M" + mes, "X");
                                                        r.setText(text5, 0);
                                                        breaker = true;
                                                        break;
                                                    }
                                                }
                                                if (breaker)
                                                    break;
                                            }
                                            if (!breaker) {
                                                r.setText("", 0);
                                            }
                                        } else {
                                            r.setText("", 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow, 4 + i);
                i++;
            }
            table.removeRow(4 + lstCrono.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @autor: Rafael Azaña 18-01-2022
     *         * @modificado:
     * @descripción: {Procesa un archivo word POAC, reemplazando valores marcados
     *               en el
     */

    public static void generarAspectosCompPOAC(List<InformacionGeneralDEMAEntity> lstLista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(39);
            System.out.println(lstLista.size());
            for (InformacionGeneralDEMAEntity detalleEntity : lstLista) {
                for (XWPFTableRow copiedRow : table.getRows()) {
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text5 = r.getText(0);
                                    if (text5 != null) {
                                        if (text5.contains("14ASPECTOS")) {
                                            text5 = text5.replace("14ASPECTOS",
                                                    detalleEntity.getDetalle() != null ? detalleEntity.getDetalle()
                                                            : "");
                                            r.setText(text5, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static XWPFDocument setearListaActividadesTab2(
            List<EvaluacionAmbientalAprovechamientoEntity> listaActividades, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalAprovechamientoEntity element : listaActividades) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getNombreAprovechamiento() == null ? ""
                                            : element.getNombreAprovechamiento());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b", element.getImpacto() == null ? "" : element.getImpacto());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c",
                                            element.getMedidasControl() == null ? "" : element.getMedidasControl());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaActividadesTab3(
            List<EvaluacionAmbientalAprovechamientoEntity> listaActividades, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalAprovechamientoEntity element : listaActividades) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getImpacto() == null ? "" : element.getImpacto());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b",
                                            element.getMedidasControl() == null ? "" : element.getMedidasControl());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c",
                                            element.getMedidasMonitoreo() == null ? "" : element.getMedidasMonitoreo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("d")) {

                                    text = text.replace("d",
                                            element.getFrecuencia() == null ? "" : element.getFrecuencia());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("e")) {

                                    text = text.replace("e",
                                            element.getResponsable() == null ? "" : element.getResponsable());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaActividadesTab4(
            List<EvaluacionAmbientalAprovechamientoEntity> listaActividades, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalAprovechamientoEntity element : listaActividades) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s")) {

                                    text = text.replace("s",
                                            element.getContingencia() == null ? "" : element.getContingencia());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("t")) {

                                    text = text.replace("t",
                                            element.getAcciones() == null ? "" : element.getAcciones());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("u")) {

                                    text = text.replace("u",
                                            element.getResponsable() == null ? "" : element.getResponsable());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaProduccionTab1(PotencialProduccionForestalEntity data, XWPFDocument doc,
            Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);
            Integer re = table.getRows().size() + 1;

            for (int j = 0; j < re; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().equals("a")) {
                                        text = text.replace("a", data.getDisenio() == null ? ""
                                                : data.getDisenio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("b")) {
                                        text = text.replace("b", data.getDiametroMinimoInventariadaCM() == null ? ""
                                                : data.getDiametroMinimoInventariadaCM().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("c")) {
                                        text = text.replace("c", data.getIntensidadMuestreoPorcentaje() == null ? ""
                                                : data.getIntensidadMuestreoPorcentaje().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("d")) {
                                        text = text.replace("d", data.getTamanioParcela() == null ? ""
                                                : data.getTamanioParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("e")) {
                                        text = text.replace("e", data.getDistanciaParcela() == null ? ""
                                                : data.getDistanciaParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("f")) {
                                        text = text.replace("f", data.getNroParcela() == null ? ""
                                                : data.getNroParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("g")) {
                                        text = text.replace("g", data.getTotalAreaInventariada() == null ? ""
                                                : data.getTotalAreaInventariada().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("h")) {
                                        text = text.replace("h", data.getMetodoMuestreo() == null ? ""
                                                : data.getMetodoMuestreo());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 1; j < re; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaProduccionTab2(PotencialProduccionForestalEntity data, XWPFDocument doc,
            Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);
            Integer re = table.getRows().size() + 1;

            for (int j = 0; j < re; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().equals("a")) {
                                        text = text.replace("a", data.getRangoDiametroCM() == null ? ""
                                                : data.getRangoDiametroCM().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("b")) {
                                        text = text.replace("b", data.getIntensidadMuestreoPorcentaje() == null ? ""
                                                : data.getIntensidadMuestreoPorcentaje().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("c")) {
                                        text = text.replace("c", data.getTamanioParcela() == null ? ""
                                                : data.getTamanioParcela().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("d")) {
                                        text = text.replace("d", data.getAreaMuestreada() == null ? ""
                                                : data.getAreaMuestreada().toString());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 1; j < re; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearCabeceraImpactoAmb(
            List<EvaluacionAmbientalAprovechamientoEntity> listaPreApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaPosApro,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            XWPFTableRow row = table.getRow(3);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {

                            String text = r.getText(0);
                            if (text != null && text.trim().contains("a1")) {
                                text = text.replace("a1", getColumn(listaPreApro, 0));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a2")) {
                                text = text.replace("a2", getColumn(listaPreApro, 1));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a3")) {
                                text = text.replace("a3", getColumn(listaPreApro, 2));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a4")) {
                                text = text.replace("a4", getColumn(listaPreApro, 3));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a5")) {
                                text = text.replace("a5", getColumn(listaPreApro, 4));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("a6")) {
                                text = text.replace("a6", getColumn(listaPreApro, 5));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b1")) {
                                text = text.replace("b1", getColumn(listaApro, 0));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b2")) {
                                text = text.replace("b2", getColumn(listaApro, 1));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b3")) {
                                text = text.replace("b3", getColumn(listaApro, 2));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b4")) {
                                text = text.replace("b4", getColumn(listaApro, 3));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b5")) {
                                text = text.replace("b5", getColumn(listaApro, 4));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("b6")) {
                                text = text.replace("b6", getColumn(listaApro, 5));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c1")) {
                                text = text.replace("c1", getColumn(listaPosApro, 0));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c2")) {
                                text = text.replace("c2", getColumn(listaPosApro, 1));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c3")) {
                                text = text.replace("c3", getColumn(listaPosApro, 2));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c4")) {
                                text = text.replace("c4", getColumn(listaPosApro, 3));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c5")) {
                                text = text.replace("c5", getColumn(listaPosApro, 4));
                                r.setText(text, 0);
                            } else if (text != null && text.trim().contains("c6")) {
                                text = text.replace("c6", getColumn(listaPosApro, 5));
                                r.setText(text, 0);
                            }
                        }
                    }
                }

            }

            table.removeRow(3);
            table.addRow(copiedRow, 3);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearDataImpactoAmbiental(
            List<EvaluacionAmbientalActividadEntity> lista,
            List<EvaluacionAmbientalAprovechamientoEntity> listaPreApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaApro,
            List<EvaluacionAmbientalAprovechamientoEntity> listaPosApro,
            List<EvaluacionAmbientalDto> listaRespuesta,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (EvaluacionAmbientalActividadEntity e : lista) {

                XWPFTableRow row = table.getRow(4);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String col = "";
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("a")) {
                                    text = text.replace("a",
                                            e.getNombreActividad() == null ? "" : e.getNombreActividad());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("b")) {
                                    text = text.replace("b",
                                            e.getMedidasControl() == null ? "" : e.getMedidasControl());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("n")) {
                                    text = text.replace("n",
                                            e.getMedidasMonitoreo() == null ? "" : e.getMedidasMonitoreo());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().equals("a1")) {
                                    col = getColumn(listaPreApro, 0);
                                    text = text.replace("a1", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a2")) {
                                    col = getColumn(listaPreApro, 1);
                                    text = text.replace("a2", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a3")) {
                                    col = getColumn(listaPreApro, 2);
                                    text = text.replace("a3", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a4")) {
                                    col = getColumn(listaPreApro, 3);
                                    text = text.replace("a4", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a5")) {
                                    col = getColumn(listaPreApro, 4);
                                    text = text.replace("a5", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("a6")) {
                                    col = getColumn(listaPreApro, 5);
                                    text = text.replace("a6", getRespuestaImpactAmb(e, col, listaRespuesta, "PREAPR"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b1")) {
                                    col = getColumn(listaApro, 0);
                                    text = text.replace("b1", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b2")) {
                                    col = getColumn(listaApro, 1);
                                    text = text.replace("b2", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b3")) {
                                    col = getColumn(listaApro, 2);
                                    text = text.replace("b3", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b4")) {
                                    col = getColumn(listaApro, 3);
                                    text = text.replace("b4", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b5")) {
                                    col = getColumn(listaApro, 4);
                                    text = text.replace("b5", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b6")) {
                                    col = getColumn(listaApro, 5);
                                    text = text.replace("b6", getRespuestaImpactAmb(e, col, listaRespuesta, "APROVE"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c1")) {
                                    col = getColumn(listaPosApro, 0);
                                    text = text.replace("c1", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c2")) {
                                    col = getColumn(listaPosApro, 1);
                                    text = text.replace("c2", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c3")) {
                                    col = getColumn(listaPosApro, 2);
                                    text = text.replace("c3", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c4")) {
                                    col = getColumn(listaPosApro, 3);
                                    text = text.replace("c4", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c5")) {
                                    col = getColumn(listaPosApro, 4);
                                    text = text.replace("c5", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c6")) {
                                    col = getColumn(listaPosApro, 5);
                                    text = text.replace("c6", getRespuestaImpactAmb(e, col, listaRespuesta, "POSTAP"));
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);

            }

            table.removeRow(4);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private static String getColumn(List<EvaluacionAmbientalAprovechamientoEntity> lista, int column) {
        String data = "";
        try {

            if (lista != null && !lista.isEmpty() && lista.get(column) != null) {

                data = lista.get(column).getNombreAprovechamiento();
            }
        } catch (Exception e) {
            data = "";
        }
        return data;
    }

    private static String getRespuestaImpactAmb(EvaluacionAmbientalActividadEntity eval, String nombreColumna,
            List<EvaluacionAmbientalDto> listaRespuesta, String tipoAprovechamiento) {
        String data = "";
        try {

            if (nombreColumna.equals("")) {
                return data;
            }

            for (EvaluacionAmbientalDto resp : listaRespuesta) {

                if (resp.getNombreActividad().equals(eval.getNombreActividad()) &&
                        resp.getTipoAprovechamiento().equals(tipoAprovechamiento) &&
                        resp.getMedidasControl().equals(eval.getMedidasControl()) &&
                        resp.getNombreAprovechamiento().equals(nombreColumna) &&
                        resp.getValorEvaluacion() != null && resp.getValorEvaluacion().equals("S")) {
                    data = "X";
                    break;
                }

            }

        } catch (Exception e) {
            data = "";
        }
        return data;
    }

    public static XWPFDocument setearListaProduccionTab3(PotencialProduccionForestalEntity data, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("n1")) {

                                text = text.replace("n1",
                                        data.getErrorMuestreo() == null ? "" : data.getErrorMuestreo().toString());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    // *************************** WORKING */
    /*
     * public static XWPFDocument
     * setearListaProduccionForestal2(List<ResultadosEspeciesMuestreoDto> data,
     * XWPFDocument doc, Integer numTabla) {
     * try {
     * 
     * List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
     * List<TablaEspeciesMuestreo> lista = new ArrayList<>();
     * 
     * XWPFTable table = doc.getTables().get(numTabla);
     * 
     * for (ResultadosEspeciesMuestreoDto element : data) {
     * 
     * lista = element.getListTablaEspeciesMuestreo();
     * 
     * for (int j = 1; j <= 3; j++) { // 3 lineas de la tabla
     * 
     * XWPFTableRow row = table.getRow(j);
     * 
     * if (row != null) {
     * XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(),
     * table);
     * 
     * CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
     * CTHeight ht = trPr.addNewTrHeight();
     * ht.setVal(BigInteger.valueOf(240));
     * 
     * for (XWPFTableCell cell : copiedRow.getTableCells()) {
     * for (XWPFParagraph p : cell.getParagraphs()) {
     * for (XWPFRun r : p.getRuns()) {
     * if (r != null) {
     * String text = r.getText(0);
     * if (text != null && text.trim().contains("n1")) {
     * text = text.replace("n1", element.getNombreBosque() == null ? ""
     * : element.getNombreBosque().toString());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n2")) {
     * text = text.replace("n2",
     * lista.get(0) == null ? ""
     * : lista.get(0).getEspecies() == null ? ""
     * : lista.get(0).getEspecies());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n3")) {
     * text = text.replace("n3",
     * lista.get(0) == null ? ""
     * : lista.get(0).getNumeroArbolesTotalBosque() == null ? ""
     * : lista.get(0).getNumeroArbolesTotalBosque());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n4")) {
     * text = text.replace("n4",
     * lista.get(0) == null ? ""
     * : lista.get(0).getAreaBasalTotalBosque() == null ? ""
     * : lista.get(0).getAreaBasalTotalBosque());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n5")) {
     * text = text.replace("n5",
     * lista.get(0) == null ? ""
     * : lista.get(0).getVolumenTotalBosque() == null ? ""
     * : lista.get(0).getVolumenTotalBosque());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n6")) {
     * text = text.replace("n6",
     * lista.get(0) == null ? ""
     * : lista.get(0).getNumeroArbolesTotalHa() == null ? ""
     * : lista.get(0).getNumeroArbolesTotalHa());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n7")) {
     * text = text.replace("n7",
     * lista.get(0) == null ? ""
     * : lista.get(0).getAreaBasalTotalHa() == null ? ""
     * : lista.get(0).getAreaBasalTotalHa());
     * r.setText(text, 0);
     * } else if (text != null && text.trim().contains("n8")) {
     * text = text.replace("n8",
     * lista.get(0) == null ? ""
     * : lista.get(0).getVolumenTotalHa() == null ? ""
     * : lista.get(0).getVolumenTotalHa());
     * r.setText(text, 0);
     * }
     * }
     * }
     * }
     * }
     * listaTablaContenido.add(copiedRow);
     * }
     * }
     * }
     * 
     * for (int j = 1; j <= 3; j++) {
     * table.removeRow(1);
     * }
     * 
     * for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
     * table.addRow(xwpfTableRow);
     * }
     * return doc;
     * } catch (Exception e) {
     * System.out.println(e.getMessage());
     * return null;
     * }
     * 
     * }
     */

    public static XWPFDocument setearListaPotencialProduccion(List<AprovechamientoRFNMDto> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            List<ResultadosAprovechamientoRFNMDto> lista = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);

            for (AprovechamientoRFNMDto element : listaPotencial) {

                lista = element.getResultadosAprovechamientoRFNMDtos();

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getEspecie() == null ? "" : element.getEspecie());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b", element.getProductoAprovechar() == null ? ""
                                            : element.getProductoAprovechar());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c", lista.isEmpty() ? ""
                                            : lista.get(0) == null ? ""
                                                    : lista.get(0).getSector() == null ? "" : lista.get(0).getSector());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("d")) {

                                    text = text.replace("d", lista.isEmpty() ? ""
                                            : lista.get(0) == null ? ""
                                                    : lista.get(0).getArea() == null ? "" : lista.get(0).getArea());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("e")) {

                                    text = text.replace("e",
                                            lista.isEmpty() ? ""
                                                    : lista.get(0) == null ? ""
                                                            : lista.get(0).getnTotalIndividuos() == null ? ""
                                                                    : lista.get(0).getnTotalIndividuos());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("f")) {

                                    text = text.replace("f",
                                            lista.isEmpty() ? ""
                                                    : lista.get(0) == null ? ""
                                                            : lista.get(0).getIndividuosHa() == null ? ""
                                                                    : lista.get(0).getIndividuosHa());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("g")) {

                                    text = text.replace("g",
                                            lista.isEmpty() ? ""
                                                    : lista.get(0) == null ? ""
                                                            : lista.get(0).getVolumen() == null ? ""
                                                                    : lista.get(0).getVolumen());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearUsoPotencialPorCategoria(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {

                                    text = text.replace("s1", listaPotencial.get(0).getCatOrdenamiento() == null ? "" : listaPotencial.get(0).getCatOrdenamiento());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("s2")) {

                                    text = text.replace("s2", listaPotencial.get(0).getDescripcionOrd() == null ? ""
                                            : listaPotencial.get(0).getDescripcionOrd());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s3")) {

                                    text = text.replace("s3", listaPotencial.get(0).getActividad() == null ? ""
                                            : listaPotencial.get(0).getActividad());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearSistemaManejoEspecificarYCicloDeCorta(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("1")) {

                                    text = text.replace("1", listaPotencial.get(0).getDescripcionOrd() == null ? "" : listaPotencial.get(0).getDescripcionOrd());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            table.removeRow(0);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesManejarDiametros(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : listaPotencial) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getEspecie() == null ? ""
                                            : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getLineaProduccion() == null ? ""
                                            : element.getLineaProduccion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getNuDiametro() == null ? ""
                                            : element.getNuDiametro().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n5")) {

                                    text = text.replace("n5", element.getNuCaminoAcceso() == null ? ""
                                            : element.getNuCaminoAcceso().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesDeFloraProteger(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : listaPotencial) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getEspecie() == null ? ""
                                            : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getDescripcionOtros() == null ? ""
                                            : element.getDescripcionOtros());
                                    r.setText(text, 0);
                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearHeaderCortaAnual(List<VolumenComercialPromedioCabeceraDto> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (VolumenComercialPromedioCabeceraDto element : lista) {
                
            
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b", element.getBloquequinquenal() == null ? "" : "Tipo de bloque: " + element.getBloquequinquenal());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a", element.getToTalAreaHa() == null ? "" : "Área (ha): " + element.getToTalAreaHa().toString());
                                    r.setText(text, 0);
                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearBodyCortaAnual(List<VolumenComercialPromedioEspecieDto> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (VolumenComercialPromedioEspecieDto element : lista) {
                
            
                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getNombreComun() == null ? "" : element.getNombreComun());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getNombreCientifico() == null ? "" : element.getNombreCientifico());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getDMC() == null ? "" : element.getDMC().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getCantidadArb() == null ? "" : element.getCantidadArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n5")) {

                                    text = text.replace("n5", element.getTotalArb() == null ? "" : element.getTotalArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n6")) {

                                    text = text.replace("n6", element.getVolumenComercialPromedio() == null ? "" : element.getVolumenComercialPromedio().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n7")) {

                                    text = text.replace("n7", element.getVolumenComercialPromedioTotal() == null ? "" : element.getVolumenComercialPromedioTotal().toString());
                                    r.setText(text, 0);
                                } 
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


    public static XWPFDocument setearCortaAnualTab2(List<VolumenComercialPromedioEspecieDto> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (VolumenComercialPromedioEspecieDto element : lista) {
                
            
                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getNombreComun() == null ? "" : element.getNombreComun());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getCantidadArb() == null ? ""
                                            : element.getCantidadArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getPorcentajeArb() == null ? ""
                                            : element.getPorcentajeArb().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getVolumenComercialPromedio() == null ? ""
                                            : element.getVolumenComercialPromedio().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n5")) {

                                    text = text.replace("n5", element.getVolumenComercialPromedioPonderado() == null ? ""
                                            : element.getVolumenComercialPromedioPonderado().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n6")) {

                                    text = text.replace("n6", element.getVolumenCortaAnualPermisible() == null ? ""
                                            : element.getVolumenCortaAnualPermisible().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument addColumnTableProyCos(List<ProyeccionCosechaDetalleDto> lastColumn, XWPFDocument doc, Integer numTable) {
        try {
            XWPFTable table = doc.getTables().get(numTable);

            XWPFTableRow row2 = table.createRow();
            //row2.getCell(0).setText("rub");


            XWPFTableRow row1  = table.getRow(0);
            row2 = table.getRow(1);
            row2.removeCell(2);
            Integer i = 1;
            for (ProyeccionCosechaDetalleDto e : lastColumn) {

                
                row2.addNewTableCell().setText("Año " + e.getAnio().toString());

                if(lastColumn.size()!=i){ row1.addNewTableCell().setText(""); }
                i++;
            }

            

            mergeCellVertically(table, 0, 0, 1);
            mergeCellVertically(table, 1, 0, 1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspecificacionesSistAprov(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getMetodoConstruccion() == null ? "" : element.getMetodoConstruccion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getManoObra() == null ? "" : element.getManoObra());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n4")) {

                                    text = text.replace("n4", element.getMaquina() == null ? "" : element.getMaquina());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesImportantes(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);
            Integer i = 1;

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", i.toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getEspecie() == null ? "" : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
                i++;
            }
        table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearEspeciesPosibles(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);
            Integer i = 1;

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", i.toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n3")) {

                                    text = text.replace("n3", element.getAccion() == false ? "" : "X");
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
                i++;
            }
        table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearManejoAreasDegradadas(List<ManejoBosqueDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

            for (ManejoBosqueDetalleEntity element : lista) {
                
            
                XWPFTableRow row = table.getRow(0);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", element.getTratamientoSilvicultural() == null ? "" : element.getTratamientoSilvicultural());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("n2")) {

                                    text = text.replace("n2", element.getDescripcionOtro() == null ? "" : element.getDescripcionOtro());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }
        table.removeRow(0);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearSistemaManejoEspecificarasd(List<ManejoBosqueDetalleEntity> listaPotencial,
            XWPFDocument doc, Integer numTabla) {
        try {

            XWPFTable table = doc.getTables().get(numTabla);

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {

                                    text = text.replace("n1", listaPotencial.get(0).getDescripcionOrd() == null ? "" : listaPotencial.get(0).getDescripcionOrd());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            table.removeRow(1);
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearListaProduccionForestal2(List<ResultadosEspeciesMuestreoDto> lista,
            XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            int addRowEspecie = 0;

            for (ResultadosEspeciesMuestreoDto e : lista) {

                int rowIni = addRowEspecie;

                for (TablaEspeciesMuestreo j : e.getListTablaEspeciesMuestreo()) {

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row = table.getRow(addRowEspecie);
                    row.getCell(0).setText(e.getNombreBosque());

                    row.getCell(1).setText(j.getEspecies());
                    row.getCell(2).setText("N");
                    row.getCell(3).setText(j.getNumeroArbolesTotalBosque());
                    row.getCell(4).setText(j.getNumeroArbolesTotalHa());

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row2 = table.getRow(addRowEspecie);

                    row2.getCell(2).setText("AB m2");
                    row2.getCell(3).setText(j.getAreaBasalTotalBosque());
                    row2.getCell(4).setText(j.getAreaBasalTotalHa());

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row3 = table.getRow(addRowEspecie);

                    row3.getCell(2).setText("Vp m3");
                    row3.getCell(3).setText(j.getVolumenTotalBosque());
                    row3.getCell(4).setText(j.getVolumenTotalHa());

                    mergeCellVertically(table, 1, addRowEspecie - 2, addRowEspecie);
                }

                mergeCellVertically(table, 0, rowIni + 1, addRowEspecie);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListaProduccionTab4(List<ResultadosEspeciesMuestreoDto> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            List<TablaEspeciesMuestreo> data = new ArrayList<>();

            for (ResultadosEspeciesMuestreoDto element : lista) {

                data = element.getListTablaEspeciesMuestreo();

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("n1")) {
                                    text = text.replace("n1", data.get(0) == null ? ""
                                            : data.get(0).getEspecies() == null ? "" : data.get(0).getEspecies());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n2")) {
                                    text = text.replace("n2",
                                            data.get(0) == null ? ""
                                                    : data.get(0).getNumeroArbolesTotalHa() == null ? ""
                                                            : data.get(0).getNumeroArbolesTotalHa().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearResultadosFustales(List<ResultadosEspeciesMuestreoDto> lista, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            int addRowEspecie = 1;

            for (ResultadosEspeciesMuestreoDto e : lista) {

                int rowIni = addRowEspecie;

                for (TablaEspeciesMuestreo j : e.getListTablaEspeciesMuestreo()) {

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row = table.getRow(addRowEspecie);
                    row.createCell();
                    row.getCell(0).setText(e.getNombreBosque());

                    row.getCell(1).setText(j.getEspecies());
                    row.getCell(2).setText("N");
                    row.getCell(3).setText(j.getNumeroArbolesDap10a14());
                    row.getCell(4).setText(j.getNumeroArbolesDap15a19());
                    row.getCell(5).setText(j.getNumeroArbolesTotalHa());
                    row.getCell(6).setText(j.getNumeroArbolesTotalBosque());

                    addRowEspecie++;
                    table.createRow();
                    XWPFTableRow row2 = table.getRow(addRowEspecie);
                    row2.createCell();

                    row2.getCell(2).setText("AB m2");
                    row2.getCell(3).setText(j.getAreaBasalDap10a14());
                    row2.getCell(4).setText(j.getAreaBasalDap15a19());
                    row2.getCell(5).setText(j.getAreaBasalTotalHa());
                    row2.getCell(6).setText(j.getAreaBasalTotalBosque());

                    mergeCellVertically(table, 1, addRowEspecie - 1, addRowEspecie);
                }

                mergeCellVertically(table, 0, rowIni + 1, addRowEspecie);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearResultadosFustalesE(List<PotencialProduccionForestalEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            List<PotencialProduccionForestalDto> data = new ArrayList<>();

            if (lista.get(0) != null) {
                data = lista.get(0).getListPotencialProduccion();
            }

            for (PotencialProduccionForestalDto element : data) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("a")) {
                                    text = text.replace("a", element.getEspecie() == null ? "" : element.getEspecie());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b")) {
                                    text = text.replace("b",
                                            element.getGrupoComercial() == null ? "" : element.getGrupoComercial());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c")) {
                                    text = text.replace("c",
                                            element.getTotalHa() == null ? "" : element.getTotalHa().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("d")) {
                                    text = text.replace("d",
                                            element.getPorcentaje() == null ? "" : element.getPorcentaje().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearCategoriasOrdenamiento(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("a")) {

                                    text = text.replace("a",
                                            element.getCategoria() == null ? "" : element.getCategoria());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("b")) {

                                    text = text.replace("b",
                                            element.getAreaHA() == null ? "" : element.getAreaHA().toString());
                                    r.setText(text, 0);

                                } else if (text != null && text.trim().contains("c")) {

                                    text = text.replace("c",
                                            element.getAreaHAPorcentaje() == null ? ""
                                                    : element.getAreaHAPorcentaje().toString());
                                    r.setText(text, 0);

                                }
                            }
                        }
                    }

                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearNBloques(List<OrdenamientoProteccionDetalleEntity> data, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("n1")) {

                                text = text.replace("n1", data.get(0).getDescripcion() == null ? ""
                                        : data.get(0).getDescripcion().toString());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearPeriodo(List<OrdenamientoProteccionDetalleEntity> data, XWPFDocument doc,
            Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            XWPFTableRow row = table.getRow(0);
            XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

            CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(240));

            for (XWPFTableCell cell : copiedRow.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text = r.getText(0);
                            if (text != null && text.trim().contains("p1")) {
                                text = text.replace("p1", data.get(0).getObservacion() == null ? ""
                                        : data.get(0).getObservacion().toString());
                                r.setText(text, 0);
                            }
                        }
                    }
                }
            }
            table.removeRow(0);
            table.addRow(copiedRow);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    // ***** WORKING 222 */
    public static XWPFDocument setearListaParcelaCorta(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {
                                    text = text.replace("s1", element.getParcelaCorta() == null ? ""
                                            : element.getParcelaCorta().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s2")) {
                                    text = text.replace("s2",
                                            element.getTipoBosque() == null ? "" : element.getTipoBosque().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s3")) {
                                    text = text.replace("s3",
                                            element.getAreaHA() == null ? "" : element.getAreaHA().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s4")) {
                                    text = text.replace("s4", element.getAreaHAPorcentaje() == null ? ""
                                            : element.getAreaHAPorcentaje().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(2);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListUbicacionYSenalizacion(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("1")) {
                                    text = text.replace("1", element.getVerticeBloque() == null ? ""
                                            : element.getVerticeBloque().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("2")) {
                                    text = text.replace("2", element.getCoordenadaEste() == null ? ""
                                            : element.getCoordenadaEste().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("3")) {
                                    text = text.replace("3", element.getCoordenadaNorte() == null ? ""
                                            : element.getCoordenadaNorte().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("4")) {
                                    text = text.replace("4",
                                            element.getZonaUTM() == null ? "" : element.getZonaUTM().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("5")) {
                                    text = text.replace("5", element.getDescripcion() == null ? ""
                                            : element.getDescripcion().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("6")) {
                                    text = text.replace("6", element.getObservacion() == null ? ""
                                            : element.getObservacion().toString());
                                    r.setText(text, 0);
                                }

                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearListDemarcacionYVigilancia(List<OrdenamientoProteccionDetalleEntity> lista,
            XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            for (OrdenamientoProteccionDetalleEntity element : lista) {

                XWPFTableRow row = table.getRow(1);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("s1")) {
                                    text = text.replace("s1", element.getActividad() == null ? ""
                                            : element.getActividad().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s2")) {
                                    text = text.replace("s2", element.getDescripcion() == null ? ""
                                            : element.getDescripcion().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("s3")) {
                                    text = text.replace("s3", element.getObservacion() == null ? ""
                                            : element.getObservacion().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            table.removeRow(1);

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**---------------------------------- SOLICITUD CONCESION ANEXOS------------------------------ */
    public static XWPFDocument setearInformacionGeneralAnexo1(SolicitudConcesionDto data,SolicitudConcesionAreaEntity data2,
    XWPFDocument doc, Integer numTabla) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(numTabla);


            Integer anexo1 = table.getRows().size() + 1; //Numero de filas 
            //Integer anexo1 = 13;
            
            for (int j = 0; j < anexo1; j++) {// 19 lineas de la tabla

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("name")) { // I. INFORMACION DEL SOLICITANTE
                                        text = text.replace("name", data.getNombreSolicitanteUnico() == null ? ""
                                                : data.getNombreSolicitanteUnico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dnii")) {
                                        text = text.replace("dnii", data.getTipoDocumentoSolicitante().equals("TDOCDNI") ? 
                                        data.getNumeroDocumentoSolicitanteUnico()
                                                : "---");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("rcc")) {
                                        text = text.replace("rcc", data.getTipoDocumentoSolicitante().equals("TDOCRUC") ?
                                        data.getNumeroDocumentoSolicitanteUnico()
                                                : "" );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("prg")) {
                                        text = text.replace("prg",
                                                data.getNroPartidaRegistral() == null ? "" : data.getNroPartidaRegistral());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dirc")) {
                                        text = text.replace("dirc",
                                                data.getDireccionSolicitante() == null ? "" : data.getDireccionSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("ndir")) {
                                        text = text.replace("ndir",
                                                data.getNumeroDireccion() == null ? "" : data.getNumeroDireccion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("secc")) {
                                        text = text.replace("secc",
                                                data.getSectorCaserio() == null ? "" : data.getSectorCaserio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dtro")) {
                                        text = text.replace("dtro",
                                                data.getNombreDistritoSolicitante() == null ? "" : data.getNombreDistritoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("prov")) {
                                        text = text.replace("prov",
                                                data.getNombreProvinciaSolicitante() == null ? "" : data.getNombreProvinciaSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dept")) {
                                        text = text.replace("dept",
                                                data.getNombreDepartamentoSolicitante() == null ? "" : data.getNombreDepartamentoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("tlf")) {
                                        text = text.replace("tlf",
                                                data.getTelefonoFijo() == null ? "" : data.getTelefonoFijo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("cell")) {
                                        text = text.replace("cell",
                                                data.getTelefonoCelular() == null ? "" : data.getTelefonoCelular());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("corr")) {
                                        text = text.replace("corr",
                                                data.getCorreoElectronico() == null ? "" : data.getCorreoElectronico());
                                        r.setText(text, 0);

                                    } else if (text != null && text.trim().equals("111")) { // II. INFORMACION SOBRE LO SOLICITADO
                                        text = text.replace("111", data.getCodigoModalidad() != null &&
                                                data.getCodigoModalidad().equals("SCMODPFDM") ? "X" : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("222")) { 
                                        text = text.replace("222", data.getCodigoModalidad() != null &&
                                                data.getCodigoModalidad().equals("SCMODECOT") ? "X" : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("333")) {
                                        text = text.replace("333", data.getCodigoModalidad() != null &&
                                                data.getCodigoModalidad().equals("SCMODCONS") ? "X" : "");
                                        r.setText(text, 0);
                                    } 
                                    else if (text != null && text.trim().contains("vanio")) {
                                        text = text.replace("vanio",
                                                data.getVigenciaAnio() == null ? "" : data.getVigenciaAnio().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("objt")) {
                                        text = text.replace("objt",
                                                data.getObjetivo() == null ? "" : data.getObjetivo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("recs")) {
                                        text = text.replace("recs",
                                                data.getRecurso() == null ? "" : data.getRecurso());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("impt")) {
                                        text = text.replace("impt",
                                                data.getImportancia() == null ? "" : data.getImportancia());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("brdescr")) {
                                        text = text.replace("brdescr",
                                                data.getDescripcionProyecto() == null ? "" : data.getDescripcionProyecto());
                                        r.setText(text, 0);

                                    } else if (text != null && text.trim().contains("spha")) { //III. INFO DEL AREA SOLICITADA // metodobig
                                        text = text.replace("spha", bigdecimalTranfor(data2.getSuperficieHa()) );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dstro")) {
                                        text = text.replace("dstro",
                                                data2.getNombreDistritoArea() == null ? "" : data2.getNombreDistritoArea());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("lugTxt")) {
                                        text = text.replace("lugTxt",
                                                data2.getNombreProvinciaArea() == null ? "" : data2.getNombreProvinciaArea());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("noDtxt")) {
                                        text = text.replace("noDtxt",
                                                data2.getNombreDepartamentoArea() == null ? "" : data2.getNombreDepartamentoArea());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("obsrv")) {
                                        text = text.replace("obsrv",
                                                data2.getObservacion() == null ? "" : data2.getObservacion().toString());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 0; j < anexo1; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInformacionGeneralAnexo1_2(SolicitudConcesionDto data, List<SolicitudConcesionActividadEntity> lista, 
        List<SolicitudConcesionActividadEntity> lista2, DescargarSolicitudConcesionDto obj,SolicitudConcesionAreaEntity area, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            List<DescargarSolicitudConcesionVerticesDto> vertices =  obj.getVertices();

            //Agregando las 2 primeras filas de la tabla a listaTablaContenido
            for (int j = 0; j <= 1; j++) {
                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240)); 
                    listaTablaContenido.add(copiedRow);
                }
            }

            //PRIMERA PARTE
            for (SolicitudConcesionActividadEntity element : lista) {

                XWPFTableRow row = table.getRow(2);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("-----")) {
                                    text = text.replace("-----", element.getNombre() == null ? ""
                                            : element.getNombre());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("----")) {
                                    text = text.replace("----", element.getDescripcion() == null ? ""
                                            : element.getDescripcion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("---")) {
                                    text = text.replace("---", element.getPeriodo() == null ? ""
                                            : element.getPeriodo().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("--")) {
                                    text = text.replace("--", bigdecimalTranfor(element.getPresupuesto()));
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                listaTablaContenido.add(copiedRow);
            }
            
            //Agregando la tercera fila
                XWPFTableRow row_3 = table.getRow(3);
                if (row_3 != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row_3.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240)); 
                    listaTablaContenido.add(copiedRow);
                }

            // SEGUNDA PARTE

            for (SolicitudConcesionActividadEntity element : lista2) {

                XWPFTableRow row = table.getRow(4);

                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("actcom")) {
                                    text = text.replace("actcom", element.getNombre() == null ? ""
                                            : element.getNombre());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("at")) {
                                    text = text.replace("at", element.getDescripcion().equals("") ? ""
                                            : "X");
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n1")) {
                                    text = text.replace("n1", element.getDescripcion() == null ? ""
                                            : element.getDescripcion());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                listaTablaContenido.add(copiedRow);
            }

            for (int j = 5; j <= 16; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().equals("111")) { // I. INFORMACION DEL SOLICITANTE
                                        text = text.replace("111", data.getCodigoFuenteFinanciamiento() != null &&
                                         data.getCodigoFuenteFinanciamiento().equals("SCFFRP") ? "X" 
                                                : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("222")) {
                                        text = text.replace("222", data.getCodigoFuenteFinanciamiento() != null &&
                                         data.getCodigoFuenteFinanciamiento().equals("SCFFEB") ? "X" 
                                                : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("333")) {
                                        text = text.replace("333", data.getCodigoFuenteFinanciamiento() != null &&
                                         data.getCodigoFuenteFinanciamiento().equals("SCFFCI") ? "X" 
                                                : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("444")) {
                                        text = text.replace("444", data.getCodigoFuenteFinanciamiento() != null &&
                                         data.getCodigoFuenteFinanciamiento().equals("SCFFOT") ? "X" 
                                                : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dsOtros")) {
                                        text = text.replace("dsOtros", data.getDescripcionOtros().equals("") ? ""
                                                : data.getDescripcionOtros());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("corrf")) {
                                        text = text.replace("corrf",
                                                data.getCorreoElectronicoNotif() == null ? "" : data.getCorreoElectronicoNotif());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("lugTxt")) {
                                        text = text.replace("lugTxt",
                                            obj.getLugar() == null ? "" : obj.getLugar());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("fTxt")) {
                                        text = text.replace("fTxt",
                                            obj.getFecha() == null ? "" : obj.getFecha());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

            for (int j = 0; j <= 16; j++) {
                table.removeRow(0);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }

            /******************************************************************************************************************************************************** */

            //TABLA 3

            XWPFTable table3 = doc.getTables().get(2);

            List<XWPFTableRow> listaTablaContenido3 = new ArrayList<>();

            for (int j = 0; j < 4; j++) {

                 XWPFTableRow row = table3.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table3);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().equals("444")) {
                                        text = text.replace("444",
                                                area.getZonaUTM() == null ? "" : area.getZonaUTM().toString());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido3.add(copiedRow);
                    
                }
            }

            for (DescargarSolicitudConcesionVerticesDto element : vertices) {

                
                XWPFTableRow rowf = table3.getRow(4);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) rowf.getCtRow().copy(), table3);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {
                                    text = text.replace("n1", element.getVertice() == null ? ""
                                            : element.getVertice());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n2")) {
                                    text = text.replace("n2", element.getEste() == null ? ""
                                            : element.getEste());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {
                                    text = text.replace("n3", element.getNorte() == null ? ""
                                            : element.getNorte());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                listaTablaContenido3.add(copiedRow);
            }


                /******************************************* */
                
            for (XWPFTableRow xwpfTableRow : listaTablaContenido3) {
                table3.addRow(xwpfTableRow);
                }

            for (int j = 0; j < 5; j++) {
                table3.removeRow(0);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }


    public static XWPFDocument setearSolcitudConcesionAnexo2(SolicitudConcesionResponsableDto solConceRespon, List<SolicitudConcesionDto> listaSolicitudConcesion, 
                                                             List<ResponsableFormacionAcademicaEntity> listaFormacion, 
                                                             List<ResponsableExperienciaLaboralDto> listaExperiencia,DescargarSolicitudConcesionDto obj,  XWPFDocument doc) {
        try {

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTable table = doc.getTables().get(0);
            Integer re = table.getRows().size();

            for (int j = 1; j <= 11; j++) {

                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);

                                    if (text != null && text.trim().equals("a1")) {
                                        text = text.replace("a1", nombreCompletoResponsableSolicitudConcesion(solConceRespon));                                         
                                        r.setText(text, 0);

                                    } else if (text != null && text.trim().equals("b1")) {
                                        text = text.replace("b1", solConceRespon.getNumeroDocumento() == null ? "" : solConceRespon.getNumeroDocumento());
                                        r.setText(text, 0);

                                    } else if (text != null && text.trim().equals("c1")) {
                                        text = text.replace("c1", solConceRespon.getDireccion() == null ? "" : solConceRespon.getDireccion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("d1")) {
                                        text = text.replace("d1", solConceRespon.getSector() == null ? "" : solConceRespon.getSector());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("e1")) {
                                        text = text.replace("e1", solConceRespon.getNombreDistrito() == null ? "" : solConceRespon.getNombreDistrito());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("f1")) {
                                        text = text.replace("f1", solConceRespon.getNombreProvincia() == null ? "" : solConceRespon.getNombreProvincia());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("g1")) {
                                        text = text.replace("g1", solConceRespon.getNombreDepartamento() == null ? "" : solConceRespon.getNombreDepartamento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("h1")) {
                                        text = text.replace("h1", solConceRespon.getTelefonoFijo() == null ? "" : solConceRespon.getTelefonoFijo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("i1")) {
                                        text = text.replace("i1", solConceRespon.getTelefonoCelular() == null ? "" : solConceRespon.getTelefonoCelular());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("j1")) {
                                        text = text.replace("j1", solConceRespon.getEmail() == null ? "" : solConceRespon.getEmail());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("k1")) {
                                        text = text.replace("k1", listaSolicitudConcesion.isEmpty() ? "" : listaSolicitudConcesion.get(0).getNombreSolicitanteUnico());//falta
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("l1")) {
                                        text = text.replace("l1", solConceRespon.getProfesion()== null ? "" : solConceRespon.getProfesion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("m1")) {
                                        text = text.replace("m1", solConceRespon.getNumeroColegiatura() == null ? "" : solConceRespon.getNumeroColegiatura());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
            }

           

            for (ResponsableFormacionAcademicaEntity element : listaFormacion) {

                XWPFTableRow row = table.getRow(12);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("a")) {
                                    text = text.replace("a",  element.getDescripcionGrado() == null ? "" : element.getDescripcionGrado());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("b")) {
                                    text = text.replace("b",  element.getDescripcionEspecialidad() == null ? "" : element.getDescripcionEspecialidad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("c")) {
                                    text = text.replace("c",  element.getNombreUniversidad() == null ? "" : element.getNombreUniversidad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("d")) {
                                    text = text.replace("d",  element.getAnioTitulo() == null ? "" : element.getAnioTitulo().toString());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                listaTablaContenido.add(copiedRow);            
            }


            for (int j = 13; j <= 14; j++) {
                XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240)); 
                    listaTablaContenido.add(copiedRow);
                }
            }
            
            Integer i = 0;
            
            for (ResponsableExperienciaLaboralDto element : listaExperiencia) {
                i++;
                XWPFTableRow row = table.getRow(15);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().equals("e")) {
                                    text = text.replace("e", i.toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("f")) {
                                    text = text.replace("f",  element.getEntidad() == null ? "" : element.getEntidad());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("g")) {
                                    text = text.replace("g",  element.getCargo() == null ? "" : element.getCargo());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("h")) {
                                    text = text.replace("h",  element.getTiempo() == null ? "" : element.getTiempo().toString());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("i")) {
                                    text = text.replace("i",  element.getDescripcion() == null ? "" : element.getDescripcion());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().equals("j")) {
                                    text = text.replace("j",  element.getTelefonoContacto() == null ? "" : element.getTelefonoContacto());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                listaTablaContenido.add(copiedRow);            
            }            
            
            for (int j = 16; j <= 17; j++) {

                XWPFTableRow row = table.getRow(j);

               if (row != null) {
                   XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                   CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                   CTHeight ht = trPr.addNewTrHeight();
                   ht.setVal(BigInteger.valueOf(240));

                   for (XWPFTableCell cell : copiedRow.getTableCells()) {
                       for (XWPFParagraph p : cell.getParagraphs()) {
                           for (XWPFRun r : p.getRuns()) {
                               if (r != null) {
                                   String text = r.getText(0);
                                   if (text != null && text.trim().equals("dirTxt")) {
                                       text = text.replace("dirTxt",
                                               obj.getLugar() == null ? "" : obj.getLugar().toString());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().equals("fecTxt")) {
                                    text = text.replace("fecTxt",
                                        obj.getFecha() == null ? "" : obj.getFecha().toString());
                                    r.setText(text, 0);
                                }
                               }
                           }
                       }
                   }
                   listaTablaContenido.add(copiedRow);
                   
               }
           }

            for (int j = 1; j < re; j++) {
                table.removeRow(1);
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
            }
            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    static String nombreCompletoResponsableSolicitudConcesion(SolicitudConcesionResponsableDto sol){
        String data = "";
        data += sol.getApellidoPaterno() == null ? "": sol.getApellidoPaterno() + " ";
        data += sol.getApellidoMaterno() == null ? "": sol.getApellidoMaterno() + " " ;
        data += sol.getNombres() == null ? "": sol.getNombres();

        return data;
    }   

    public static XWPFDocument setearInformacionGeneralAnexo3(SolicitudConcesionDto data, SolicitudConcesionAreaEntity data2,
    List<SolicitudConcesionIngresoCuentaDto> data3,DescargarSolicitudConcesionDto obj, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

                // PARTE 1°

                XWPFTableRow row = table.getRow(0);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("----------")) {
                                        text = text.replace("----------", data.getNombreSolicitanteUnico() == null ? "----------"
                                                : data.getNombreSolicitanteUnico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("---------")) {
                                        text = text.replace("---------", data.getNumeroDocumentoSolicitanteUnico() == null ? "----------"
                                                : data.getNumeroDocumentoSolicitanteUnico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("--------")) {
                                        text = text.replace("--------", data.getDireccionSolicitante() == null ? "----------"
                                                : data.getDireccionSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("-------")) {
                                        text = text.replace("-------", data.getDescripcionModalidad() == null ? "----------"
                                                : data.getDescripcionModalidad());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("------")) {
                                        text = text.replace("------", bigdecimalTranfor(data2.getSuperficieHa()) );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("-----")) {
                                        text = text.replace("-----", data.getSectorCaserio() == null ? "----------"
                                                : data.getSectorCaserio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("----")) {
                                        text = text.replace("----", data2.getNombreDistritoArea() == null ? "----------"
                                                : data2.getNombreDistritoArea());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("---")) {
                                        text = text.replace("---", data2.getNombreProvinciaArea() == null ? "----------"
                                                : data2.getNombreProvinciaArea());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("--")) {
                                        text = text.replace("--", data2.getNombreDepartamentoArea() == null ? "----------"
                                                : data2.getNombreDepartamentoArea());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                    
                }
                // PARTE 2°
                for (int j = 1; j <= 2; j++) {
                    XWPFTableRow row2 = table.getRow(j);
    
                    if (row2 != null) {
                        XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                        CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                        CTHeight ht = trPr.addNewTrHeight();
                        ht.setVal(BigInteger.valueOf(240)); 
                        listaTablaContenido.add(copiedRow);
                    }
                }

                for (SolicitudConcesionIngresoCuentaDto element : data3) {

                
                    XWPFTableRow rowf = table.getRow(3);
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) rowf.getCtRow().copy(), table);
    
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
    
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", element.getCodigoTipoDocumento() == null ? ""
                                                : element.getCodigoTipoDocumento());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n2")) {
                                        text = text.replace("n2", element.getDescripcion() == null ? ""
                                                : element.getDescripcion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n3")) {
                                        text = text.replace("n3", element.getDescripcion() == null ? ""
                                                : element.getDescripcion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n4")) {
                                        text = text.replace("n4", bigdecimalTranfor(element.getMonto()) );
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
                // PARTE 3
                for (int j = 4; j <= 6; j++) {

                    XWPFTableRow row3 = table.getRow(j);
   
                   if (row3 != null) {
                       XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row3.getCtRow().copy(), table);
   
                       CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                       CTHeight ht = trPr.addNewTrHeight();
                       ht.setVal(BigInteger.valueOf(240));
   
                       for (XWPFTableCell cell : copiedRow.getTableCells()) {
                           for (XWPFParagraph p : cell.getParagraphs()) {
                               for (XWPFRun r : p.getRuns()) {
                                   if (r != null) {
                                       String text = r.getText(0);
                                       if (text != null && text.trim().contains("lugTxt")) {
                                           text = text.replace("lugTxt", obj.getLugar() == null ? ""
                                                   : obj.getLugar());
                                           r.setText(text, 0);
                                       } else if (text != null && text.trim().contains("fecTxt")) {
                                           text = text.replace("fecTxt", obj.getFecha() == null ? ""
                                                   : obj.getFecha());
                                           r.setText(text, 0);
                                       } 
                                   }
                               }
                           }
                       }
                       listaTablaContenido.add(copiedRow);
                       
                   }
               }
                
            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                    table.addRow(xwpfTableRow);
                }

            for (int j = 0; j <= 6; j++) {
                table.removeRow(0);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInformacionGeneralAnexo4(SolicitudConcesionDto data, SolicitudConcesionAreaEntity data2,
    List<SolicitudConcesionCriterioAreaEntity> data3,SolicitudConcesionArchivoEntity data4,DescargarSolicitudConcesionDto obj,
     XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            XWPFTableRow row0 = table.getRow(0); // Agregando la fila 0
            XWPFTableRow copiedRow0 = new XWPFTableRow((CTRow) row0.getCtRow().copy(), table);
                        CTTrPr trPr0 = copiedRow0.getCtRow().addNewTrPr();
                        CTHeight ht0 = trPr0.addNewTrHeight();
                        ht0.setVal(BigInteger.valueOf(240)); 
                        listaTablaContenido.add(copiedRow0);

                // PARTE 1°
            for (int j = 1; j <= 4; j++) {

                 XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("nombreTx")) {
                                        text = text.replace("nombreTx", data.getNombreSolicitanteUnico() == null ? ""
                                                : data.getNombreSolicitanteUnico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("suprfHa")) {
                                        text = text.replace("suprfHa", bigdecimalTranfor(data2.getSuperficieHa()) );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("plazA")) {
                                        text = text.replace("plazA", data.getVigenciaAnio() == null ? ""
                                                : data.getVigenciaAnio().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("sct")) {
                                        text = text.replace("sct", data.getSectorCaserio() == null ? ""
                                                : data.getSectorCaserio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dstro")) {
                                        text = text.replace("dstro", data.getNombreDistritoSolicitante()== null ? ""
                                                : data.getNombreDistritoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("textPro")) {
                                        text = text.replace("textPro", data.getNombreProvinciaSolicitante() == null ? ""
                                                : data.getNombreProvinciaSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dpto")) {
                                        text = text.replace("dpto", data.getNombreDepartamentoSolicitante() == null ? ""
                                                : data.getNombreDepartamentoSolicitante());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                    
                }
            }
                // PARTE 2°
                for (int j = 5; j <= 6; j++) {
                    XWPFTableRow row2 = table.getRow(j);
    
                    if (row2 != null) {
                        XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                        CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                        CTHeight ht = trPr.addNewTrHeight();
                        ht.setVal(BigInteger.valueOf(240)); 
                        listaTablaContenido.add(copiedRow);
                    }
                }

                for (SolicitudConcesionCriterioAreaEntity element : data3) {

                
                    XWPFTableRow rowf = table.getRow(7);
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) rowf.getCtRow().copy(), table);
    
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
    
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", element.getNombreCriterio() == null ? ""
                                                : element.getNombreCriterio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("otrosDecr")) {
                                        text = text.replace("otrosDecr", element.getCodigoCriterio().equals("SCCTOT") ? element.getDescripcionOtroCriterio()
                                                : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("111")) {
                                        text = text.replace("111", element.getEstadoCriterio() == true ? "X"
                                                : "");
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }
               // PARTE 3°
               for (int j = 8; j <= 19; j++) {

                XWPFTableRow row = table.getRow(j);

               if (row != null) {
                   XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                   CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                   CTHeight ht = trPr.addNewTrHeight();
                   ht.setVal(BigInteger.valueOf(240));

                   for (XWPFTableCell cell : copiedRow.getTableCells()) {
                       for (XWPFParagraph p : cell.getParagraphs()) {
                           for (XWPFRun r : p.getRuns()) {
                               if (r != null) {
                                   String text = r.getText(0);
                                   if (text != null && text.trim().contains("justCrit")) {
                                       text = text.replace("justCrit", data2.getJustificacion() == null ? ""
                                               : data2.getJustificacion());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("mapTem")) {
                                       text = text.replace("mapTem", data2.getAnalisisMapa() == null ? ""
                                               : data2.getAnalisisMapa());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("deltCon")) {
                                       text = text.replace("deltCon", data2.getDelimitacion() == null ? ""
                                               : data2.getDelimitacion());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("obsvTx")) {
                                       text = text.replace("obsvTx", data2.getObservacionTamanio() == null ? ""
                                               : data2.getObservacionTamanio());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("fntCit")) {
                                       text = text.replace("fntCit", data2.getFuenteBibliografica() == null ? ""
                                               : data2.getFuenteBibliografica());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("panFot")) {
                                    text = text.replace("panFot", data4.getNombreArchivo() == null ? ""
                                            : data4.getNombreArchivo());
                                    r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dirTxt")) {
                                        text = text.replace("dirTxt", obj.getLugar() == null ? ""
                                                : obj.getLugar());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("fecTxt")) {
                                        text = text.replace("fecTxt", obj.getFecha() == null ? ""
                                                : obj.getFecha());
                                        r.setText(text, 0);
                                    } 
                               }
                           }
                       }
                   }
                   listaTablaContenido.add(copiedRow);
                   
               }
           }

                /******************************************* */
                
            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                    table.addRow(xwpfTableRow);
                }

            for (int j = 0; j <= 19; j++) {
                table.removeRow(0);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static XWPFDocument setearInformacionGeneralAnexo5(SolicitudConcesionDto data, SolicitudConcesionAreaEntity data2, 
    DescargarSolicitudConcesionDto obj, XWPFDocument doc, Integer numTabla) {
        try {
            XWPFTable table = doc.getTables().get(numTabla);
            Integer rowsTable = table.getRows().size();

            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

            for (int j = 0; j < rowsTable; j++) {

                 XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("tipTxt")) {
                                        text = text.replace("tipTxt", data.getDescripcionModalidad() == null ? ""
                                                : data.getDescripcionModalidad());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("nameSoli")) {
                                        text = text.replace("nameSoli", data.getNombreSolicitanteUnico() == null ? ""
                                                : data.getNombreSolicitanteUnico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("supHa")) {
                                        text = text.replace("supHa", bigdecimalTranfor(data2.getSuperficieHa()) );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("vigAnio")) {
                                        text = text.replace("vigAnio", data.getVigenciaAnio() == null ? ""
                                                : data.getVigenciaAnio().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dept")) {
                                        text = text.replace("dept", data.getNombreDepartamentoSolicitante() == null ? ""
                                                : data.getNombreDepartamentoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("proText")) {
                                        text = text.replace("proText", data.getNombreProvinciaSolicitante() == null ? ""
                                                : data.getNombreProvinciaSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("disto")) {
                                        text = text.replace("disto", data.getNombreDistritoSolicitante() == null ? ""
                                                : data.getNombreDistritoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("secX")) {
                                        text = text.replace("secX", data.getSectorCaserio() == null ? ""
                                                : data.getSectorCaserio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", data.getObjetivo() == null ? ""
                                                : data.getObjetivo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("n2")) {
                                        text = text.replace("n2", data.getDescripcionProyecto() == null ? ""
                                                : data.getDescripcionProyecto());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dirTxt")) {
                                        text = text.replace("dirTxt", obj.getLugar() == null ? ""
                                                : obj.getLugar());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("fecTxt")) {
                                        text = text.replace("fecTxt", obj.getFecha() == null ? ""
                                                : obj.getFecha());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                    
                }
            }
                /******************************************* */
                
            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                    table.addRow(xwpfTableRow);
                }

            for (int j = 0; j < rowsTable; j++) {
                table.removeRow(0);
            }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static XWPFDocument setearInformacionGeneralAnexo6(SolicitudConcesionDto data, SolicitudConcesionAreaEntity data2,
    List<SolicitudConcesionActividadEntity> data3, XWPFDocument doc, Integer numTabla1, Integer numTabla2, SolicitudConcesionArchivoEntity financiero,
    SolicitudConcesionArchivoEntity presupuesto, SolicitudConcesionArchivoEntity experiencia, SolicitudConcesionArchivoEntity mapaUbicacion
    , SolicitudConcesionArchivoEntity mapaBase, DescargarSolicitudConcesionDto obj) {
        try {
            XWPFTable table = doc.getTables().get(numTabla1);
            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();

                // PARTE 1°  0 - 18
            for (int j = 0; j <= 18; j++) {

                 XWPFTableRow row = table.getRow(j);

                if (row != null) {
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));

                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("nameTxt")) {
                                        text = text.replace("nameTxt", data.getNombreSolicitanteUnico() == null ? ""
                                                : data.getNombreSolicitanteUnico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dniTxt")) {
                                        text = text.replace("dniTxt", data.getTipoDocumentoSolicitante().equals("TDOCDNI") ? 
                                        data.getNumeroDocumentoSolicitanteUnico()
                                                : "---");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("rucTxt")) {
                                        text = text.replace("rucTxt", data.getTipoDocumentoSolicitante().equals("TDOCRUC") ?
                                        data.getNumeroDocumentoSolicitanteUnico()
                                                : "" );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("domcTxt")) {
                                        text = text.replace("domcTxt", data.getDireccionSolicitante() == null ? ""
                                                : data.getDireccionSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("tell")) {
                                        text = text.replace("tell", data.getTelefonoCelular()== null ? ""
                                                : data.getTelefonoCelular());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("gmail")) {
                                        text = text.replace("gmail", data.getCorreoElectronico() == null ? ""
                                                : data.getCorreoElectronico());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("reprLegal")) {
                                        text = text.replace("reprLegal", data.getNombreReprLegal() == null ? ""
                                                : data.getNombreReprLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("domcRep")) {
                                        text = text.replace("domcRep", data.getDireccionReprLegal() == null ? ""
                                                : data.getDireccionReprLegal());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("111")) {
                                        text = text.replace("111", data.getCodigoModalidad() != null && data.getCodigoModalidad().equals("SCMODPFDM") ? "X"
                                                : "" );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("222")) {
                                        text = text.replace("222", data.getCodigoModalidad() != null && data.getCodigoModalidad().equals("SCMODECOT") ? "X"
                                                : "" );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("333")) {
                                        text = text.replace("333", data.getCodigoModalidad() != null && data.getCodigoModalidad().equals("SCMODCONS") ? "X"
                                                : "");
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("vgnAnio")) {
                                        text = text.replace("vgnAnio", data.getVigenciaAnio() == null ? ""
                                                : data.getVigenciaAnio().toString());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("supHa")) {
                                        text = text.replace("supHa", bigdecimalTranfor(data2.getSuperficieHa()) );
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("secTxt")) {
                                        text = text.replace("secTxt", data.getSectorCaserio() == null ? ""
                                                : data.getSectorCaserio());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("distTxt")) {
                                        text = text.replace("distTxt", data.getNombreDistritoSolicitante() == null ? ""
                                                : data.getNombreDistritoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("txtProv")) {
                                        text = text.replace("txtProv", data.getNombreProvinciaSolicitante() == null ? ""
                                                : data.getNombreProvinciaSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("Dpto")) {
                                        text = text.replace("Dpto", data.getNombreDepartamentoSolicitante() == null ? ""
                                                : data.getNombreDepartamentoSolicitante());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("recApr")) {
                                        text = text.replace("recApr", data.getRecurso() == null ? ""
                                                : data.getRecurso());
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                    
                }
            }
                // PARTE 2°

                for (SolicitudConcesionActividadEntity element : data3) {

                
                    XWPFTableRow rowf = table.getRow(19);
                    XWPFTableRow copiedRow = new XWPFTableRow((CTRow) rowf.getCtRow().copy(), table);
    
                    CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                    CTHeight ht = trPr.addNewTrHeight();
                    ht.setVal(BigInteger.valueOf(240));
    
                    for (XWPFTableCell cell : copiedRow.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if (r != null) {
                                    String text = r.getText(0);
                                    if (text != null && text.trim().contains("n1")) {
                                        text = text.replace("n1", element.getNombre() == null ? ""
                                                : element.getNombre());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("descrTxt")) {
                                        text = text.replace("descrTxt", element.getDescripcion() == null ? ""
                                                : element.getDescripcion());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().equals("555")) {
                                        text = text.replace("555", element.getDescripcion().equals("") ? ""
                                                : "X");
                                        r.setText(text, 0);
                                    } 
                                }
                            }
                        }
                    }
                    listaTablaContenido.add(copiedRow);
                }

               // PARTE 3°
            for (int j = 20; j <= 26; j++) {

                XWPFTableRow row = table.getRow(j);

               if (row != null) {
                   XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                   CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                   CTHeight ht = trPr.addNewTrHeight();
                   ht.setVal(BigInteger.valueOf(240));

                   for (XWPFTableCell cell : copiedRow.getTableCells()) {
                       for (XWPFParagraph p : cell.getParagraphs()) {
                           for (XWPFRun r : p.getRuns()) {
                               if (r != null) {
                                   String text = r.getText(0);
                                   if (text != null && text.trim().contains("objTxt")) {
                                       text = text.replace("objTxt", data.getObjetivo() == null ? ""
                                               : data.getObjetivo());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("metTxt")) {
                                       text = text.replace("metTxt", data.getMetas() == null ? ""
                                               : data.getMetas());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("fisTxt")) {
                                       text = text.replace("fisTxt", data2.getCaractFisica() == null ? ""
                                               : data2.getCaractFisica());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("bioTxt")) {
                                       text = text.replace("bioTxt", data2.getCaractBiologica() == null ? ""
                                               : data2.getCaractBiologica());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("socioTxt")) {
                                       text = text.replace("socioTxt", data2.getCaractSocioeconomica()== null ? ""
                                               : data2.getCaractSocioeconomica());
                                       r.setText(text, 0);
                                   }
                               }
                           }
                       }
                   }
                   listaTablaContenido.add(copiedRow);
                   
                }
            }

                /******************************************* */
                
            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                    table.addRow(xwpfTableRow);
                }

            for (int j = 0; j <= 26; j++) {
                table.removeRow(0);
            }

            

              // TABLA N° 2

            XWPFTable table2 = doc.getTables().get(numTabla2);
            List<XWPFTableRow> listaTablaContenido2 = new ArrayList<>();
            Integer sizeRow = table2.getRows().size();

              for (int j = 0; j < sizeRow; j++) {

                XWPFTableRow row = table2.getRow(j);

               if (row != null) {
                   XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table2);

                   CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                   CTHeight ht = trPr.addNewTrHeight();
                   ht.setVal(BigInteger.valueOf(240));

                   for (XWPFTableCell cell : copiedRow.getTableCells()) {
                       for (XWPFParagraph p : cell.getParagraphs()) {
                           for (XWPFRun r : p.getRuns()) {
                               if (r != null) {
                                   String text = r.getText(0);
                                   if (text != null && text.trim().contains("ambTxt")) {
                                       text = text.replace("ambTxt", data2.getFactorAmbientalBiologico() == null ? ""
                                               : data2.getFactorAmbientalBiologico());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("soecoculTxt")) {
                                       text = text.replace("soecoculTxt", data2.getFactorSocioEconocultural() == null ? ""
                                               : data2.getFactorSocioEconocultural());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("mnconTxt")) {
                                       text = text.replace("mnconTxt", data2.getManejoConcesion() == null ? ""
                                               : data2.getManejoConcesion());
                                       r.setText(text, 0);
                                   } else if (text != null && text.trim().contains("prsTxt")) {
                                    text = text.replace("prsTxt", financiero.getNombreArchivo() == null ? ""
                                            : financiero.getNombreArchivo());
                                    r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("finanTxt")) {
                                        text = text.replace("finanTxt", presupuesto.getNombreArchivo() == null ? ""
                                                : presupuesto.getNombreArchivo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("expTxt")) {
                                        text = text.replace("expTxt", experiencia.getNombreArchivo() == null ? ""
                                                : experiencia.getNombreArchivo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("mapaubTxtt")) {
                                        text = text.replace("mapaubTxtt", mapaUbicacion.getNombreArchivo() == null ? ""
                                                : mapaUbicacion.getNombreArchivo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("mapabaseTxt")) {
                                        text = text.replace("mapabaseTxt", mapaBase.getNombreArchivo() == null ? ""
                                                : mapaBase.getNombreArchivo());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("dirTxt")) {
                                        text = text.replace("dirTxt", obj.getLugar() == null ? ""
                                                : obj.getLugar());
                                        r.setText(text, 0);
                                    } else if (text != null && text.trim().contains("fecTxt")) {
                                        text = text.replace("fecTxt", obj.getFecha() == null ? ""
                                                : obj.getFecha());
                                        r.setText(text, 0);
                                    }
                               }
                           }
                       }
                   }
                   listaTablaContenido2.add(copiedRow);
                   
                }
            }

            for (XWPFTableRow xwpfTableRow : listaTablaContenido2) {
                table2.addRow(xwpfTableRow);
            }

            for (int j = 0; j < sizeRow; j++) {
            table2.removeRow(0);
        }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**---------------------------------- FIN SOLICITUD CONCESION ANEXOS------------------------------ */

    public static XWPFDocument SetearResolucionEvaluacionSolictud(SolicitudConcesionDto data, SolicitudConcesionAreaEntity area,
    DescargarResAdminFavDesfavDto obj, XWPFDocument doc) {
        try {
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {

                            System.out.println(text);

                            if (text.contains("modalidadTxt")){
                                if(data.getCodigoModalidad()!= null){
                                    if(data.getCodigoModalidad().equals("SCMODPFDM")) text = text.replace("modalidadTxt", "PFDM");
                                    if(data.getCodigoModalidad().equals("SCMODECOT")) text = text.replace("modalidadTxt", "ECOTURISMO");
                                    if(data.getCodigoModalidad().equals("SCMODCONS")) text = text.replace("modalidadTxt", "CONSERVACION");
                                }else {text = text.replace("modalidadTxt", " ");}
                                r.setText(text, 0);
                            } else if (text.contains("nameTxt")) {
                                if(data.getTipoDocumentoSolicitante()!= null){
                                    if(data.getTipoDocumentoSolicitante().equals("TDOCDNI")) text = text.replace("nameTxt", data.getNombreSolicitanteUnico()==null?" ":data.getNombreSolicitanteUnico());
                                    if(data.getTipoDocumentoSolicitante().equals("TDOCRUC")) text = text.replace("nameTxt", data.getNombreReprLegal()==null?" ":data.getNombreReprLegal());
                                }else {text = text.replace("nameTxt", " ");}
                                r.setText(text, 0);
                            } else if (text.contains("documentTxt")) {
                                if(data.getTipoDocumentoSolicitante()!= null){
                                    if(data.getTipoDocumentoSolicitante().equals("TDOCDNI")) text = text.replace("documentTxt", data.getNumeroDocumentoSolicitanteUnico()==null?" ":data.getNumeroDocumentoSolicitanteUnico());
                                    if(data.getTipoDocumentoSolicitante().equals("TDOCRUC")) text = text.replace("documentTxt", data.getNumeroDocumentoReprLegal()==null?" ":data.getNumeroDocumentoReprLegal());
                                }else {text = text.replace("documentTxt", " ");}
                                r.setText(text, 0);
                            } else if (text.contains("dataTxt")) {
                                if(data.getTipoDocumentoSolicitante()!= null){
                                    if(data.getTipoDocumentoSolicitante().equals("TDOCDNI")) text = text.replace("dataTxt", "");
                                    if(data.getTipoDocumentoSolicitante().equals("TDOCRUC")) text = text.replace("dataTxt", " en representación de "+(data.getNombreSolicitanteUnico()==null?"":data.getNombreSolicitanteUnico())+", con RUC "+(data.getNumeroDocumentoSolicitanteUnico()==null?"":data.getNumeroDocumentoSolicitanteUnico())+",");
                                }else {text = text.replace("dataTxt", "");}
                                r.setText(text, 0);
                            } else if (text.contains("distTxt")) {
                                text = text.replace("distTxt", data.getNombreDistritoSolicitante()==null?" ":data.getNombreDistritoSolicitante());
                                r.setText(text, 0);
                            } else if (text.contains("provTxt")) {
                                text = text.replace("provTxt", data.getNombreProvinciaSolicitante()==null?" ":data.getNombreProvinciaSolicitante());
                                r.setText(text, 0);
                            } else if (text.contains("deptoTxt")) {
                                text = text.replace("deptoTxt", data.getNombreDepartamentoSolicitante()==null?" ":data.getNombreDepartamentoSolicitante());
                                r.setText(text, 0);
                            /********************************************************* */
                            } else if (text.contains("haTxt")) {
                                text = text.replace("haTxt", area.getSuperficieHa()==null?" ":area.getSuperficieHa().toString());
                                r.setText(text, 0);
                            } else if (text.contains("zonTxt")) {
                                text = text.replace("zonTxt", area.getZonaUTM()==null?" ":area.getZonaUTM().toString());
                                r.setText(text, 0);
                            } else if (text.contains("areaDist")) {
                                text = text.replace("areaDist", area.getNombreDistritoArea()==null?" ":area.getNombreDistritoArea());
                                r.setText(text, 0);
                            } else if (text.contains("provArea")) {
                                text = text.replace("provArea", area.getNombreProvinciaArea()==null?" ":area.getNombreProvinciaArea());
                                r.setText(text, 0);
                            } else if (text.contains("deptoArea")) {
                                text = text.replace("deptoArea", area.getNombreDepartamentoArea()==null?" ":area.getNombreDepartamentoArea());
                                r.setText(text, 0);
                            } 
                        }
                    }
                }
            }

            //  TABLA DE VERTICES
            XWPFTable table = doc.getTables().get(0);
            List<XWPFTableRow> listaTablaContenido = new ArrayList<>();
            List<DescargarSolicitudConcesionVerticesDto> vertices =  obj.getVertices();

            for (int j = 0; j < 4; j++) {

                XWPFTableRow row = table.getRow(j);

               if (row != null) {
                   XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                   CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                   CTHeight ht = trPr.addNewTrHeight();
                   ht.setVal(BigInteger.valueOf(240));

                   for (XWPFTableCell cell : copiedRow.getTableCells()) {
                       for (XWPFParagraph p : cell.getParagraphs()) {
                           for (XWPFRun r : p.getRuns()) {
                               if (r != null) {
                                   String text = r.getText(0);
                                   if (text != null && text.trim().equals("444")) {
                                       text = text.replace("444",
                                               area.getZonaUTM() == null ? "" : area.getZonaUTM().toString());
                                       r.setText(text, 0);
                                   }
                               }
                           }
                       }
                   }
                   listaTablaContenido.add(copiedRow);
                   
               }
           }

           Integer breakRow = 0;
            for (DescargarSolicitudConcesionVerticesDto element : vertices) {

                
                XWPFTableRow rowf = table.getRow(4);
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) rowf.getCtRow().copy(), table);

                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null && text.trim().contains("n1")) {
                                    text = text.replace("n1", element.getVertice() == null ? ""
                                            : element.getVertice());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n2")) {
                                    text = text.replace("n2", element.getEste() == null ? ""
                                            : element.getEste());
                                    r.setText(text, 0);
                                } else if (text != null && text.trim().contains("n3")) {
                                    text = text.replace("n3", element.getNorte() == null ? ""
                                            : element.getNorte());
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
                listaTablaContenido.add(copiedRow);
                breakRow++;
                if(breakRow==100) break;
            }
            for (XWPFTableRow xwpfTableRow : listaTablaContenido) {
                table.addRow(xwpfTableRow);
                }
            for (int j = 0; j < 5; j++) {
                    table.removeRow(0);
              }

            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    static void mergeCellVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            CTVMerge vmerge = (CTVMerge) CTVMerge.Factory.newInstance();
            if (rowIndex == fromRow) {
                // The first merged cell is set with RESTART merge value
                vmerge.setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                vmerge.setVal(STMerge.CONTINUE);
                // and the content should be removed
                for (int i = cell.getParagraphs().size(); i > 0; i--) {
                    cell.removeParagraph(0);
                }
                cell.addParagraph();
            }
            // Try getting the TcPr. Not simply setting an new one every time.
            CTTcPr tcPr = cell.getCTTc().getTcPr();
            if (tcPr == null)
                tcPr = cell.getCTTc().addNewTcPr();
            tcPr.setVMerge(vmerge);
        }
    }

    static void mergeCellHorizontally(XWPFTable table, int row, int fromCol, int toCol) {
        XWPFTableCell cell = table.getRow(row).getCell(fromCol);
        // Try getting the TcPr. Not simply setting an new one every time.
        CTTcPr tcPr = cell.getCTTc().getTcPr();
        if (tcPr == null) tcPr = cell.getCTTc().addNewTcPr();
        // The first merged cell has grid span property set
        if (tcPr.isSetGridSpan()) {
         tcPr.getGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        } else {
         tcPr.addNewGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        }
        // Cells which join (merge) the first one, must be removed
        for(int colIndex = toCol; colIndex > fromCol; colIndex--) {
         table.getRow(row).getCtRow().removeTc(colIndex);
         table.getRow(row).removeCell(colIndex);
        }
    }

    public static String bigdecimalTranfor(Double num){
        String numero = "";

        if(num!=null){
            BigDecimal num1 = new BigDecimal(num);
            BigDecimal num2 = num1.setScale(2, RoundingMode.HALF_UP);
            return numero = num2.toString();
        }else{
            return numero;
        }

    }

}