package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.repository.UnidadFisiograficaRepository;
import pe.gob.serfor.mcsniffs.service.UnidadFisiograficaService;

@Service("UnidadFisiograficaService")
public class UnidadFisiograficaServiceImpl implements UnidadFisiograficaService {
    @Autowired
    private UnidadFisiograficaRepository repository;

    @Override
    public ResultClassEntity listarUnidadFisiografica(Integer idInfBasica) throws Exception {
        return repository.listarUnidadFisiografica(idInfBasica);
    }
    @Override
    public ResultClassEntity registrarUnidadFisiograficaArchivo(UnidadFisiograficaUMFEntity param,MultipartFile file) throws Exception {
        return repository.registrarUnidadFisiograficaArchivo(param,file);
    }
    @Override
    public ResultClassEntity eliminarUnidadFisiograficaArchivo(Integer idInfBasica,Integer idUnidadFisiografica,Integer idUsuario) throws Exception {
        return repository.eliminarUnidadFisiograficaArchivo(idInfBasica,idUnidadFisiografica,idUsuario);
    }
}
