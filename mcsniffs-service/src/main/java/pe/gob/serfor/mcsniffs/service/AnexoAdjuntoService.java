package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.AnexoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface AnexoAdjuntoService {

    ResultClassEntity MarcarParaAmpliacionAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception;

    List<AnexoAdjuntoEntity> ListarAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception;

}
