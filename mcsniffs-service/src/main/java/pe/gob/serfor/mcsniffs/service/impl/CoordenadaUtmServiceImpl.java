package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.CoordenadaUtmEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.CoordenadaUtmRepository;
import pe.gob.serfor.mcsniffs.service.CoordenadaUtmService;

import javax.transaction.Transactional;
import java.util.List;

@Service("CoordenadaUtmService")
public class CoordenadaUtmServiceImpl implements CoordenadaUtmService {
    @Autowired
    private CoordenadaUtmRepository coordenadaUtmRepository;


    @Override
    @Transactional
    public ResultClassEntity RegistrarCoordenadaUtm(List<CoordenadaUtmEntity> lista) throws Exception {
        ResultClassEntity result = null;
        for (CoordenadaUtmEntity element : lista) {
            result = coordenadaUtmRepository.RegistrarCoordenadaUtm(element);
        }
        return result;
    }

    @Override
    @Transactional
    public ResultClassEntity ActualizarCoordenadaUtm(List<CoordenadaUtmEntity> lista) throws Exception {
        ResultClassEntity result = null;
        for (CoordenadaUtmEntity element : lista) {
            result = coordenadaUtmRepository.ActualizarCoordenadaUtm(element);
        }
        return result;
    }

    @Override
    public ResultClassEntity<CoordenadaUtmEntity> ListarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception {
        return coordenadaUtmRepository.ListarCoordenadaUtm(coordenadaUtmEntity);
    }

    @Override
    public ResultClassEntity<CoordenadaUtmEntity> EliminarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception {
        return coordenadaUtmRepository.EliminarCoordenadaUtm(coordenadaUtmEntity);
    }

}
