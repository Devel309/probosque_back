package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.DescargarRespuestaEvaluacionComiteDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDto;

public interface SolicitudBosqueLocalEvaluacionService {

    ResultClassEntity registrarSolicitudBosqueLocalEvaluacion(List<SolicitudBosqueLocalEvaluacionDto> param) throws Exception;
    ResultClassEntity eliminarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto param) throws Exception;
    ResultClassEntity listarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto param) throws Exception;
    ResultClassEntity actualizarSolicitudBosqueLocalEvaluacionDetalle(List<SolicitudBosqueLocalEvaluacionDetalleDto> listObj) throws Exception;
    ResultClassEntity registrarSolicitudBosqueLocalEvaluacionDetalle(List<SolicitudBosqueLocalEvaluacionDetalleDto> items) throws Exception;
    ResultClassEntity listarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj) throws Exception;
    ResultArchivoEntity descargarRespuestaEvaluacionComite(DescargarRespuestaEvaluacionComiteDto obj) throws Exception;

}
