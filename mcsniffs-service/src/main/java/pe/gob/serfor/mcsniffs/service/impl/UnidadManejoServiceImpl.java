package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoEntity;
import pe.gob.serfor.mcsniffs.repository.UnidadManejoRepository;
import pe.gob.serfor.mcsniffs.service.UnidadManejoService;

@Service("UnidadManejoService")
public class UnidadManejoServiceImpl implements UnidadManejoService {

    @Autowired
    private UnidadManejoRepository unidadManejoRepository;

    @Override
    public ResultClassEntity RegistrarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception {
        return unidadManejoRepository.RegistrarUnidadManejo(unidadManejoEntity);
    }

    @Override
    public ResultClassEntity ActualizarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception {
        return unidadManejoRepository.ActualizarUnidadManejo(unidadManejoEntity);
    }

    @Override
    public ResultClassEntity ListarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception {
        return unidadManejoRepository.ListarUnidadManejo(unidadManejoEntity);
    }

}
