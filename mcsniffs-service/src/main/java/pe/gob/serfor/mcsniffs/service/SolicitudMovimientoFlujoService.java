package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudMovimientoFlujoEntity;

public interface SolicitudMovimientoFlujoService {
    ResultClassEntity registroSolicitudMovimientoFlujo(SolicitudMovimientoFlujoEntity entity);
}
