package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueEspecieService {
    ResultClassEntity RegistrarManejoBosqueEspecie(List<ManejoBosqueEspecieEntity> manejoBosqueEspecie) throws Exception;
    ResultClassEntity EliminarManejoBosqueEspecie(ManejoBosqueEspecieEntity manejoBosqueEspecie) throws Exception;
    ResultClassEntity ListarManejoBosqueEspecie(ManejoBosqEntity manejoBosqueEspecie) throws Exception;



}
