package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;

import java.util.List;

public interface MonitoreoService {
    ResultClassEntity RegistrarMonitoreoPgmfea(List<MonitoreoPgmfeaEntity> monitoreoPgmfea) throws Exception;
    ResultClassEntity EliminarMonitoreoPgmfea(MonitoreoPgmfeaEntity monitoreoPgmfea) throws Exception;
    ResultClassEntity ListarMonitoreoPgmfea(MonitoreoEntity monitoreo) throws Exception;
    ResultClassEntity ObtenerMonitoreoPgmfea(MonitoreoPgmfeaEntity param) throws Exception;
    //Jaqueline DB
    ResultClassEntity RegistrarMonitoreo(MonitoreoEntity monitoreo);
    ResultClassEntity ActualizarMonitoreo(MonitoreoEntity monitoreo);
    ResultClassEntity<MonitoreoEntity> listarMonitoreo(MonitoreoEntity monitoreo);
    ResultClassEntity<List<MonitoreoEntity>> listarMonitoreoPOCC(Integer idPlanManejo, String codigoMonitoreo ) throws Exception;
    ResultClassEntity RegistrarMonitoreoPOCC(List<MonitoreoEntity> list) throws Exception;
    ResultClassEntity ActualizarMonitoreoPOCC(List<MonitoreoEntity> list) throws Exception;
    ResultClassEntity EliminarDetalleMonitoreo(MonitoreoDetalleEntity detalle);
    ResultClassEntity EliminarMonitoreoCabera(MonitoreoEntity monitoreo);
}
