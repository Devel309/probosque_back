package pe.gob.serfor.mcsniffs.service;


import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

import java.util.List;

public interface SolicitudConcesionIngresoCuentaService {
    ResultClassEntity listarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) throws Exception;
    ResultClassEntity registrarSolicitudConcesionIngresoCuenta(List<SolicitudConcesionIngresoCuentaDto> lsObj) throws Exception;
    ResultClassEntity eliminarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) throws Exception;

}
