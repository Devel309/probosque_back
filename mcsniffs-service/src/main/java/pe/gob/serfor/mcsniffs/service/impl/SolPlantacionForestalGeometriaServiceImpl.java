package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalGeometriaRepository;
import pe.gob.serfor.mcsniffs.service.SolPlantacionForestalGeometriaService;

@Service("serviceSolPlantacionForestalGeometria")
public class SolPlantacionForestalGeometriaServiceImpl implements SolPlantacionForestalGeometriaService {

    @Autowired
    private SolPlantacionForestalGeometriaRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarSolPlantacionForestalGeometria(List<SolPlantacionForestalGeometriaEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        items.forEach((item) -> {
            try {
                repository.registrarSolPlantacionForestalGeometria(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se registraron correctamente.");
        result.setSuccess(true);
        return result;

    }

    @Override
    public ResultClassEntity listarSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item) {
        return repository.listarSolPlantacionForestalGeometria(item);
    }

    @Override
    public ResultClassEntity obtenerSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item) throws Exception{
        return repository.obtenerSolPlantacionForestalGeometria(item);
    }

    @Override
    public ResultClassEntity eliminarSolPlantacionForestalGeometriaArchivo(SolPlantacionForestalGeometriaEntity item) {
        return repository.eliminarSolPlantacionForestalGeometriaArchivo(item);
    }

    @Override
    public ResultClassEntity actualizarSolPlantacionForestalGeometria(List<SolPlantacionForestalGeometriaEntity> items)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        for (SolPlantacionForestalGeometriaEntity element : items) {
            result = repository.actualizarSolPlantacionForestalGeometria(element);
        }

        return result;
    }
    
}
