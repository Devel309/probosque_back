package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieProtegerEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueEspecieProtegerRepository;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueEspecieProtegerService;

import java.util.List;
@Service("ManejoBosqueEspecieProtegerService")
public class ManejoBosqueEspecieProtegerServiceImpl implements ManejoBosqueEspecieProtegerService {


    private static final Logger log = LogManager.getLogger(ManejoBosqueEspecieProtegerServiceImpl.class);

    @Autowired
    private ManejoBosqueEspecieProtegerRepository manejoBosqueEspecieProtegerepository;


    @Override
    public ResultClassEntity RegistrarManejoBosqueEspecieProteger(List<ManejoBosqueEspecieProtegerEntity> list) throws Exception {
        return manejoBosqueEspecieProtegerepository.RegistrarManejoBosqueEspecieProteger(list);
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueEspecieProteger(ManejoBosqueEspecieProtegerEntity manejoBosqueEspecieProteger) throws Exception {
        return manejoBosqueEspecieProtegerepository.EliminarManejoBosqueEspecieProteger(manejoBosqueEspecieProteger);
    }

    @Override
    public ResultClassEntity ListarManejoBosqueEspecieProteger(PlanManejoEntity planManejo) throws Exception {
        return manejoBosqueEspecieProtegerepository.ListarManejoBosqueEspecieProteger(planManejo);
    }
}
