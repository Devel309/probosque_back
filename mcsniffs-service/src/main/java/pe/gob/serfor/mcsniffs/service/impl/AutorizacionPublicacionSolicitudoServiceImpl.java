package pe.gob.serfor.mcsniffs.service.impl;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.EstatusProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.AutorizacionPublicacionSolicitudRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.service.AutorizacionPublicacionSolicitudService;
import pe.gob.serfor.mcsniffs.service.EmailService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.gob.serfor.mcsniffs.service.ServicioExternoService;

@Service("AutorizacionPublicacionSolicitudService")
public class AutorizacionPublicacionSolicitudoServiceImpl implements AutorizacionPublicacionSolicitudService{

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private EmailService servMail;

    @Autowired
    AutorizacionPublicacionSolicitudRepository autorizaPublicacionRepository;

    @Autowired
    ProcesoPostulacionRepository postulacionRepository;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

    @Override
    public ResultArchivoEntity descargarPlantillaCartaAutorizacion(){
        return autorizaPublicacionRepository.descargarPlantillaCartaAutorizacion();
    }

    @Override
    public ResultClassEntity guardarAutorizacionPublicacionSolicitud(AutorizacionPublicacionSolicitudEntity request) {
        // TODO Auto-generated method stub
        return autorizaPublicacionRepository.guardarAutorizacionPublicacionSolicitud(request);
    }

    @Override
    public ResultClassEntity enviarAutorizacionPublicacionSolicitud(EstatusProcesoPostulacionEntity request, String token) {

        ResultClassEntity result = new ResultClassEntity();

        result = postulacionRepository.actualizarEstadoProcesoPostulacion(request.getIdProcesoPostulacion(), request.getIdStatus(), request.getIdUsuarioModificacion(), new Date());
        Gson gson = new Gson();
        //Se le envia un correo al postulante que fue autorizada la publicación de su solicitud
        if(result.getSuccess()) {
            UsuarioDto user= new UsuarioDto();
            user.setIdusuario(request.getIdPostulante());
            ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad +"usuario/ObtenerUsuarioID", gson.toJson(user), token);
            ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);

            if(resultApiExt != null && !resultApiExt.getData().isEmpty()) {
                Integer cont = 0;

                String nombres = "";
                String apePaterno = "";
                String apeMaterno = "";

                String[] mails = new String[1];
                for (Object item : resultApiExt.getData().stream().toArray()) {
                    if (cont == 0) {
                        LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
                        mails[cont] = val.get("correoElectronico").toString();
                        nombres = val.get("nombres").toString();
                        apePaterno = val.get("apellidoPaterno").toString();
                        apeMaterno = val.get("apellidoMaterno").toString();
                    }
                    cont++;
                }

                EmailEntity mail = new EmailEntity();
                if (mails.length > 0 && mails[0] != null && !mails[0].equals("")) {
                    mail.setEmail(mails);
                    String body = "<html><head>"
                            + "<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                            + "<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                            + "</head>"
                            + "<body lang=ES-PE style='word-wrap:break-word'>"
                            + "<div class=WordSection1>"
                            + "<p class=MsoNormal><span lang=ES>Sr(a). " + nombres + " " + apePaterno + " " + apeMaterno + ":</span></p>"
                            + "<p class=MsoNormal><span lang=ES>Se le informa que es necesaria su autorización para publicar el resumen de la solicitud de postulación Nro: " + (request.getIdProcesoPostulacion()).toString() + ".</span></p>"
                            + "<p class=MsoNormal><span lang=ES>Agradecemos realizar la aprobación en el sistema MCSNIFFS.</span></p>"
                            + "<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> No responder este correo automático.</i></p>"
                            + "</div></body></html>";
                    mail.setContent(body);
                    mail.setSubject("Autorización de publicación del resumen de la solicitud");
                    Boolean enviarmail = servMail.sendEmail(mail);
                    result.setInformacion(enviarmail ? "Se envió el correo correctamente" : "Ocurrió un error el enviar el correo");
                }
            }
        }

        return result;

    }

}
