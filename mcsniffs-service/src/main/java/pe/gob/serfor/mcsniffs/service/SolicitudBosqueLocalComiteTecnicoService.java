package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalComiteTecnicoEntity;

import java.util.List;

public interface SolicitudBosqueLocalComiteTecnicoService {
    ResultClassEntity listarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj) throws Exception;
    ResultClassEntity registrarComiteTecnicoSolicitudBosqueLocal(List<SolicitudBosqueLocalComiteTecnicoEntity> items) throws Exception;
    ResultClassEntity eliminarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj)throws Exception;

}
