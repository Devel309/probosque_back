package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionArchivoDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionArchivoRepository;
import pe.gob.serfor.mcsniffs.service.EvaluacionArchivoService;



@Service("EvaluacionArchivoService")
public class EvaluacionArchivoServiceImpl implements EvaluacionArchivoService {

    private static final Logger log = LogManager.getLogger(EvaluacionServiceImpl.class);


    
    @Autowired
    private EvaluacionArchivoRepository evaluacionArchivoRepository;
 

    @Override
    public ResultClassEntity<List<EvaluacionArchivoDto>> listarEvaluacionArchivo(EvaluacionArchivoDto dto) throws Exception {        
      
        List<EvaluacionArchivoDto> lista = evaluacionArchivoRepository.listarEvaluacionArchivo(dto);
        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");

        return  result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarEvaluacionArchivo(List<EvaluacionArchivoDto> dto) throws Exception {

        ResultClassEntity result = null;
        for (EvaluacionArchivoDto element : dto) {
            result = evaluacionArchivoRepository.registrarEvaluacionArchivo(element);
        }

        return  result;
    }


    @Override
    public ResultClassEntity eliminarEvaluacionArchivo(List<EvaluacionArchivoDto> dto) throws Exception{
        ResultClassEntity result = null;
        for (EvaluacionArchivoDto element : dto) {
            result = evaluacionArchivoRepository.eliminarEvaluacionArchivo(element);
        }
        return  result;
    }
    
}
