package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.PermisoForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface PermisoForestalGeometriaService {
    ResultClassEntity registrarPermisoForestalGeometria(List<PermisoForestalGeometriaEntity> items) throws Exception;
    ResultClassEntity listarPermisoForestalGeometria(PermisoForestalGeometriaEntity item) throws Exception;
    ResultClassEntity eliminarPermisoForestalGeometriaArchivo(PermisoForestalGeometriaEntity item) throws Exception;
}
