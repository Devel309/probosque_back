package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface SincronizacionService {
    ResultClassEntity sincronizar(MultipartFile file) throws Exception;
}
