package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoNivel1Entity;
import pe.gob.serfor.mcsniffs.repository.TipoProcesoRepository;
import pe.gob.serfor.mcsniffs.service.TipoProcesoService;

@Service
public class TipoProcesoServiceImpl  implements TipoProcesoService {
    /**
     * @autor: JaquelineDB [09-06-2021]
     * @modificado:
     * @descripción: {Servicios de tipo de proceso}
     *
     */
    @Autowired
    TipoProcesoRepository tipoproceso;

    @Override
    public ResultEntity<TipoProcesoNivel1Entity> listarTipoProceso() {
        /**
         * @autor: JaquelineDB [09-06-2021]
         * @modificado:
         * @descripción: {Listar tipo de proceso}
         * @param:
         */
        return tipoproceso.listarTipoProceso();
    }
}
