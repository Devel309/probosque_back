package pe.gob.serfor.mcsniffs.service.impl;


import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.constant.PoacType;
import pe.gob.serfor.mcsniffs.service.util.DocUtil;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("ArchivoPDFPOACService")
public class ArchivoPDFPOACServiceImpl implements ArchivoPDFPOACService {

    @Autowired
    private  SolicitudAccesoServiceImpl acceso;

    @Autowired
    private PlanManejoRepository planManejoRepository;

    @Autowired
    private PlanManejoContratoRepository planManejoContratoRepository;

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private SistemaManejoForestalRepository smfRepo;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;

    @Autowired
    private SistemaManejoForestalRepository sistema;

    @Autowired
    private ImpactoAmbientalService ambiental;

    @Autowired
    CensoForestalRepository censoForestalDetalleRepo;

    @Autowired
    ZonificacionRepository zonificacionRepository;

    @Autowired
    CoreCentralRepository corecentral;
/*
    @Autowired
    SolicitudAccesoRepository acceso;*/

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

    @Autowired
    private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ArchivoRepository archivoRepository;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;


    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private SistemaManejoForestalService sistemaManejoForestalService;


    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: Rafael Azaña [19-02-2022]
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con Apache POI}
     */

    @Override
    public ByteArrayResource consolidadoPOAC(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoPOAC.docx");
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("consolidadoPOAC", ".pdf");
        FileOutputStream os = new FileOutputStream(archivo);
        createPdfPOAC(os,idPlanManejo);
        os.flush();
        os.close();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }


    public  void createPdfPOAC(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);
        try {

            Font titulo = new Font(Font.HELVETICA, 12f, Font.BOLD);
            Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
            Font subTitulo2= new Font(Font.HELVETICA, 9f, Font.BOLD);
            Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

            PdfWriter writer = PdfWriter.getInstance(document, os);
            document.open();
            Paragraph titlePara2 = new Paragraph("PLAN OPERATIVO ANUAL (POA) EN BOSQUES DE COMUNIDADES NATIVAS Y/O CAMPESINAS CON FINES DE COMERCIALIZACION A ALTA ESCALA",titulo);
            titlePara2.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph(titlePara2));
            document.add(new Paragraph("\r\n\r\n"));


            document.add(new Paragraph("\t\t1. INFORMACIÓN GENERAL",subTitulo));
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable table = createTableInformacionGeneral(writer,idPlanManejo);
            document.add(table);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t2. RESUMEN DE ACTIVIDADES Y RECOMENDACIONES DEL PO ANTERIOR ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t2.1 Resumen Actividades",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table2 = createResumenActividades(writer,idPlanManejo);
            document.add(table2);
            document.add(new Paragraph("\r\n"));
            PdfPTable table3 = createResumenActividadesR(writer,idPlanManejo);
            document.add(table3);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t2.2 Balance las activiades pasadas",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tAspectos Positivos",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table4 = createResumenActividadesPositivos(writer,idPlanManejo);
            document.add(table4);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tAspectos Negativos",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table5 = createResumenActividadesNegativos(writer,idPlanManejo);
            document.add(table5);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tRecomendaciones",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table6 = createResumenActividadesRecomendaciones(writer,idPlanManejo);
            document.add(table6);


            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t3. OBJETIVOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t3.1 Objetivos especificos",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table7 = createObjetivoEspecifico(writer,idPlanManejo);
            document.add(table7);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t4. INFORMACIÓN BÁSICA DEL ÁREA DE APROVECHAMIENTO ANUAL",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t4.1 Ubicación y extensión del área de aprovechamiento anual ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table8 = createInfBasicaUbicacion(writer,idPlanManejo);
            document.add(table8);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t4.2 Coordenadas UTM del área de aprovechamiento anual (Mapa 01)",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table9 = createInfBasicaCoor(writer,idPlanManejo);
            document.add(table9);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t4.3 Tipos de Bosque",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table10 = createInfBasicaTipoBosque(writer,idPlanManejo);
            document.add(table10);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t4.4 Accesibilidad",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\ta) Rutas o vías de acceso y otra infraestructura en el área de aprovechamiento anual\n" +
                    "(Mapa 01) ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table11 = createInfBasicaACCA(writer,idPlanManejo);
            document.add(table11);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph(" \t\tb) Sistema de transporte de madera a utilizar   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t Fluvial (   )\t       Terrestre (   )\t                     Mixto (fluvial y terrestre)   (X)    ",contenido));
            document.add(new Paragraph("\r\n\r\n"));

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph(" \t\tc) Medios de transporte a utilizar entre la PC y el destino de la madera   ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph(" \t\tDel área de aprovechamiento al centro de acopio y/o transformación ",contenido));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t Flotación (X)           Chatas   (X)          Camión   (X)        Otros (X)  (Arrastre tractor forestal) ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table12 = createInfBasicaACCC(writer,idPlanManejo);
            document.add(table12);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t5. ACTIVIDADES DE APROVECHAMIENTO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.1.\tDelimitación del área de aprovechamiento anual ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tIndicar en el cuadro que se muestra a continuación el sistema de demarcación utilizado. ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table13 = createActAprovDeli(writer,idPlanManejo);
            document.add(table13);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tDiferencia entre fajas de orientación (m) ",contenido));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.2. Censo comercial ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.2.1. Metodología ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.3 Planificación y construcción de infraestructura de aprovechamiento ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tSe desarrollarán en esta sección las actividades y metas de construcción y mantenimiento de\n" +
                    "infraestructura de caminos, drenaje y patios de acopio.  ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table14 = createActAprovPlani(writer,idPlanManejo);
            document.add(table14);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t5.4. Operaciones de corta",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table15 = createActAprovPOACOperaCorta(writer,idPlanManejo);
            document.add(table15);
            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t5.5.\tOperaciones de arrastre y transporte",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tArrastre:   Manual (  )   Mecanizado (X)   Mixto (  )\n" +
                    "                  Maquinaria / equipo: propio (  )    alquilado y/o asociación (X)   \n",contenido));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.5.1.\tOperaciones de arrastre",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table16 = createActAprovPOACArras(writer,idPlanManejo);
            document.add(table16);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.5.2.\tOperaciones de Transporte",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table17 = createActAprovPOACTrans(writer,idPlanManejo);
            document.add(table17);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t5.6.\tProcesamiento Local",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table18 = createActAprovPOACProce(writer,idPlanManejo);
            document.add(table18);

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t6. ACTIVIDADES SILVICULTURALES",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\ta) Tratamiento",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tSi se decide la aplicación de tratamientos silviculturales, se deberá presentar el análisis y fuente\n" +
                    "de información que determine la necesidad de la aplicación de dichos tratamientos (Ej: inventarios,\n" +
                    "censos, muestreo diagnostico, etc) ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table19 = createActividadSilviculturalAplicacionPOAC(writer,idPlanManejo);
            document.add(table19);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tb) Tratamiento",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tEn caso se prevea realizar la reforestación en la PCA, se deberá presentar la información como se\n" +
                    "muestra en el cuadro a continuación ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table20 = createActividadSilviculturalReforestacionPOAC(writer,idPlanManejo);
            document.add(table20);

            document.add(new Paragraph("\r\n\r\n"));
            document.add(new Paragraph("\t\t7. PROTECCIÓN DEL BOSQUE",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tLas actividades de protección del bosque son responsabilidad fundamental de la comunidad, en caso se\n" +
                    "esté aprovechando el bosque mediante contrato con terceros, deben realizarse acciones compartidas.\n" +
                    "Es necesaria en esta fase un seguimiento activo de miembros de la comunidad como de otros miembros\n" +
                    "que pueden contribuir a disminuir conflictos por ingresos no autorizados, tala ilegal, invasiones, etc.\n" +
                    "realizando las actividades de señalización, vigilancia, y verificando que las contrapartes del contrato\n" +
                    "cumplan con sus tareas de acuerdo a lo previsto.\n" +
                    "Se deben presentar las acciones de acuerdo a los ítems siguientes: ",contenido));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t7.1 Marcación y mantenimiento de linderos",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table21 = createProteccionBosquePOAC1(writer,idPlanManejo);
            document.add(table21);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t7.2.1.\tAnálisis de Impacto Ambiental",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tEs necesario presentar las interacciones causa-efecto de las actividades del Plan General\n" +
                    "de Manejo, que se espera tendrán un mayor impacto ambiental. Para este fin se puede\n" +
                    "recurrir a matrices que identifiquen las causas y efectos, de las actividades siguientes, u otras\n" +
                    "que se estimen podrían tener un impacto significativo:",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table22 = createProteccionBosquePOAC2(writer,idPlanManejo);
            document.add(table22);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t7.2.2.\tPlan de gestión ambiental",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tEn función a los impactos identificados en la matriz y considerando los impactos negativos de\n" +
                    "mayor intensidad, se debe indicar las medidas que se tomarán para mitigar cada impacto,\n" +
                    "programa de vigilancia y programa de contingencias ambientales, usando como ejemplo el cuadro\n" +
                    "mostrado a continuación",contenido));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t* Programa preventivo - corrector",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table23 = createProteccionBosquePOAC3(writer,idPlanManejo);
            document.add(table23);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t* Programa preventivo - corrector",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table24 = createProteccionBosquePOAC4(writer,idPlanManejo);
            document.add(table24);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t* Programa preventivo - corrector",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table25 = createProteccionBosquePOAC5(writer,idPlanManejo);
            document.add(table25);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t8. MONITOREO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tPara el monitoreo dentro de la comunidad es recomendable la designación de un grupo, el cual\n" +
                    "deberá registrar y presentar la información de las actividades que se realizarán durante el año\n" +
                    "operativo correspondiente ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table26 = createMonitoreoPOAC(writer,idPlanManejo);
            document.add(table26);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t9. PARTICIPACIÓN COMUNAL",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tDebe destacarse las diferentes formas en que la comunidad participará en la elaboración, ejecución\n" +
                    "del Plan Operativo Anual, así como actividades de control, seguimiento, negociaciones con\n" +
                    "terceros, y otras actividades que se quiera destacar en cuanto a las tareas de responsabilidad de la\n" +
                    "comunidad.",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table27 = createParticipacionComunalPOAC(writer,idPlanManejo);
            document.add(table27);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t10. CAPACITACIÓN",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tEn este acápite del plan, se deberán identificar y desarrollar las actividades de capacitación a\n" +
                    "desarrollar durante el año operativo ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table28 = createCapacitacionPOAC(writer,idPlanManejo);
            document.add(table28);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t11. ORGANIZACIÓN PARA EL DESARROLLO DE LA ACTIVIDAD",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tIndicar la forma de organización que tiene la comunidad para el desarrollo de la actividad forestal\n" +
                    "durante el año operativo, indicando los participantes, las modalidades de financiamiento, los\n" +
                    "compromisos económicos, entre otros aspectos. \n",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table29 = createActividadSilviculturalOrganizPOAC(writer,idPlanManejo);
            document.add(table29);
            Rectangle two = new Rectangle(842.0F,595.0F);
            document.setPageSize(two);
            document.setMargins(40, 40, 40, 40);
            document.newPage();
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t12. RENTABILIDAD DEL PLAN DE MANEJO",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tPresentar el flujo de caja o balance mensual de ingresos y costos de las actividades de manejo\n" +
                    "forestal, previstas en el POA. ",contenido));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tINGRESOS",subTitulo2));
            document.add(new Paragraph("\r\n\r\n"));
            PdfPTable table30 = createRentabilidadIngresos(writer,idPlanManejo);
            document.add(table30);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tEGRESOS",subTitulo2));
            document.add(new Paragraph("\r\n"));
            PdfPTable table31 = createRentabilidadEgresos(writer,idPlanManejo);
            document.add(table31);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t13. CRONOGRAMA DE ACTIVIDADES",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tSe deberá presentar un cronograma de actividades a desarrollar durante el año operativo, de acuerdo al\n" +
                    "modelo presentado en el cuadro siguiente: ",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table32 = createCronogramaActividadesPOACAnios(writer,idPlanManejo);
            document.add(table32);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t14. ASPECTOS COMPLEMENTARIOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tIndicar cualquier otro aspecto importante dentro de este punto",contenido));
            document.add(new Paragraph("\r\n"));
            PdfPTable table33 = createAspectosCompPOAC(writer,idPlanManejo);
            document.add(table33);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\t15. ANEXOS",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tAnexo 01: Mapas ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tMapa 01: Ubicación de Parcela de corta anual, tipos de bosque, hidrografía y accesibilidad.\n" +
                    "Mapa 02: Dispersión de especies aprovechables, con caminos secundarios y vías o caminos de\n" +
                    "arrastre, patios de acopio, áreas a excluir de aprovechamiento y semilleros.  ",subTitulo2));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tNota: El mapa en pdf se puede descargar desde la web en el apartado Anexo 1 ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tAnexos 2: Censo Comercial",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table34 = createAnexos2(writer,idPlanManejo);
            document.add(table34);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tAnexo 04: Aprovechamiento de recursos no maderables ",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("\t\tConsiderar el presente anexo si durante el año operativo se prevé realizar el aprovechamiento de\n" +
                    "recursos no maderables.\n" +
                    "Realizar el censo comercial de los recursos no maderables a aprovechar.\n" +
                    "Incluir la metodología empleada para el aprovechamiento de los recursos no maderables,\n" +
                    "tomando en cuenta las consideraciones necesarias que aseguren la sostenibilidad del recurso, los\n" +
                    "aspectos silviculturales y aspectos ambientales, entre los más importantes. ",subTitulo2));
            document.add(new Paragraph("\r\n"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable createTableInformacionGeneral(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //1 - Información general
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso("POAC");
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0);
        SolicitudAccesoEntity param = new SolicitudAccesoEntity();
        param.setNumeroDocumento(o.getDniJefecomunidad());
        List<SolicitudAccesoEntity> lstacceso = acceso.ListarSolicitudAcceso(param);
        if (lstacceso.size() > 0) {
            //o.setDepartamento(lstacceso.get(0).getNombreDepartamento());
            //o.setProvincia(lstacceso.get(0).getNombreProvincia());
            //o.setDistrito(lstacceso.get(0).getNombreDistrito());
            o.setNombreComunidad(lstacceso.get(0).getRazonSocialEmpresa());
            o.setNombresJefeComunidad(lstacceso.get(0).getNombres());
            o.setApellidoPaternoJefeComunidad(lstacceso.get(0).getApellidoPaterno());
            o.setApellidoMaternoJefeComunidad(lstacceso.get(0).getApellidoMaterno());
            //o.setNroRucComunidad(lstacceso.get(0).getNumeroRucEmpresa());
        }

        cell = new PdfPCell(new Paragraph("1. Del Titular del Permiso",subTitulo));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre de la comunidad: ",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(o.getNombreElaboraDema()!=null? o.getNombreElaboraDema():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre del Jefe  o representante legal: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(toString(o.getNombreRepresentante())+" "+toString(o.getApellidoPaternoRepresentante())+" "+toString(o.getApellidoMaternoRepresentante())));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("2. Del aprovechamiento",subTitulo));
        cell.setColspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Nombre de la empresa o extractor: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getObservacion()!=null? o.getObservacion():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Departamento: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDepartamento()!=null? o.getDepartamento():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Provincia: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getProvincia()!=null? o.getProvincia():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Distrito: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getDistrito()!=null? o.getDistrito():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Ing. Forestal que elaboró el POA: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!=null?toString(o.getRegente().getNombres())+" "+toString(o.getRegente().getApellidos()):"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Certificado de habilitación del Ing Forestal: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("N° de licencia o inscripción en el registro de regentes que conduce el SERFOR ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getRegente()!=null?o.getRegente().getNumeroLicencia():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Área de aprovechamiento anual: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getAreaTotal()!=null?o.getAreaTotal().toString():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Año del POA: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getVigencia()!=null?o.getVigencia().toString():"",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("Fecha presentación del POA: ",subTitulo2));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph(o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "",contenido));
        cell.setFixedHeight(size);
        table.addCell(cell);

        return table;
    }

    public  PdfPTable createResumenActividades(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.HELVETICA);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);
        int contador=0;
        if(lstResumen.size()>0) {
            for (ResumenActividadPoEntity resumen : lstResumen) {
                if (contador == 0) {
                    cell = new PdfPCell(new Paragraph("Nro PC", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(resumen.getNroPcs() != null ? resumen.getNroPcs().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph("Área (ha)", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(resumen.getAreaHa() != null ? resumen.getAreaHa().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
                contador++;
            }
        }
        return table;
    }

    public  PdfPTable createResumenActividadesR(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(4);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.HELVETICA);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);

        cell = new PdfPCell(new Paragraph( "Actividades",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Indicadores",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Programado",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "Realizado",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstResumen.size()>0) {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }

            if(lstListaDetalle.size()>0) {
                for (ResumenActividadPoDetalleEntity resumen : lstListaDetalle) {

                    cell = new PdfPCell(new Paragraph(resumen.getActividad() != null ? resumen.getActividad() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph(resumen.getIndicador() != null ? resumen.getIndicador() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph(resumen.getProgramado() != null ? resumen.getProgramado() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                    cell = new PdfPCell(new Paragraph(resumen.getRealizado() != null ? resumen.getRealizado() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                }
            }
        }
        return table;
    }

    public  PdfPTable createResumenActividadesPositivos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.HELVETICA);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        cell = new PdfPCell(new Paragraph( "Aspectos Positivos",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);
        if(lstResumen!=null){
            List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream().filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();

            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }
            if(lstListaDetalle!=null) {
                List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                        .filter(eval -> eval.getTipoResumen() == 1).collect(Collectors.toList());
                if(lstListaDetalle2!=null) {
                    for(ResumenActividadPoDetalleEntity resumen : lstListaDetalle2 ){
                        cell = new PdfPCell(new Paragraph( resumen.getResumen() != null? resumen.getResumen().toString(): "",contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }

            }

        }


        return table;
    }

    public  PdfPTable createResumenActividadesNegativos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.HELVETICA);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        cell = new PdfPCell(new Paragraph( "Aspectos Negativos",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);

        if(lstResumen!=null){
            List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream().filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }
            if(lstListaDetalle!=null){
                List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream().filter(eval -> eval.getTipoResumen() == 2).collect(Collectors.toList());

                if(lstListaDetalle2!=null){
                    for(ResumenActividadPoDetalleEntity resumen : lstListaDetalle2 ){
                        cell = new PdfPCell(new Paragraph( resumen.getResumen() != null? resumen.getResumen().toString(): "",contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }

            }

        }

        return table;
    }

    public  PdfPTable createResumenActividadesRecomendaciones(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.HELVETICA);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        cell = new PdfPCell(new Paragraph( "Recomendaciones",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //2 - Resumen de actividades 2
        ResumenActividadPoEntity resumenRequest = new ResumenActividadPoEntity();
        resumenRequest.setCodTipoResumen("POAC");
        resumenRequest.setIdPlanManejo(idPlanManejo);
        List<ResumenActividadPoEntity> lstResumen = resumenActividadPoRepository.ListarResumenActividad_Detalle(resumenRequest);

        if(lstResumen!=null) {
            List<ResumenActividadPoEntity> listPGMFEVALI = lstResumen.stream()
                    .filter(eval -> eval.getCodigoSubResumen().equals("POACRPA")).collect(Collectors.toList());
            List<ResumenActividadPoDetalleEntity> lstListaDetalle = new ArrayList<>();
            if (listPGMFEVALI.size() > 0) {
                ResumenActividadPoEntity PGMFEVALI = listPGMFEVALI.get(0);
                lstListaDetalle = PGMFEVALI.getListResumenActividadDetalle();
            }
            if(lstListaDetalle!=null) {
                List<ResumenActividadPoDetalleEntity> lstListaDetalle2 = lstListaDetalle.stream()
                        .filter(eval -> eval.getTipoResumen() == 3).collect(Collectors.toList());
                if(lstListaDetalle2!=null) {
                    for (ResumenActividadPoDetalleEntity resumen : lstListaDetalle2) {
                        cell = new PdfPCell(new Paragraph(resumen.getResumen() != null ? resumen.getResumen().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                }
            }
        }
        return table;
    }


    public  PdfPTable createObjetivoEspecifico(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //3 - Objetivos  -- 6
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        ResultEntity<ObjetivoDto> lstObjetivos= objetivoservice.listarObjetivos("POAC",idPlanManejo);

        if(lstObjetivos.getData()!=null){
            List<ObjetivoDto> maderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("Maderable"))
                    .collect(Collectors.toList());


            cell = new PdfPCell(new Paragraph("Productos a obtener del manejo ",titulo));
            cell.setColspan(3);
            cell.setFixedHeight(20);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Maderable",subTitulo));
            cell.setRowspan(maderables.size());
            cell.setFixedHeight(size);
            table.addCell(cell);
            String marcaM="";
            for(ObjetivoDto obM : maderables){
                if(obM.getActivo().equals("A")){
                    marcaM="(X)";
                }else{marcaM="";}
                cell = new PdfPCell(new Paragraph(marcaM,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(obM.getDescripcionDetalle(),contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }


            List<ObjetivoDto> NMaderables = lstObjetivos.getData().stream().filter(m -> m.getDetalle().equals("No Maderable"))
                    .collect(Collectors.toList());

            cell = new PdfPCell(new Paragraph("No Maderable",subTitulo));
            cell.setRowspan(NMaderables.size());
            cell.setFixedHeight(size);
            table.addCell(cell);
            String marcaNM="";
            for(ObjetivoDto obNM : NMaderables){
                if(obNM.getActivo().equals("I")){
                    marcaNM="(X)";
                }else{marcaNM="";}
                cell = new PdfPCell(new Paragraph(marcaNM,contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(obNM.getDescripcionDetalle(),contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }

        }

        return table;
    }



    public  PdfPTable createInfBasicaUbicacion(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //4 - INFORMACIÓN BASICA DE LA PARCELA DE CORTA (PC)
        //4.1  -- 7
        List<InfBasicaAereaDetalleDto> lstInfoBasicaUEPC = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMATC");
       if(lstInfoBasicaUEPC!=null){
           String departamento="",provincia="",distrito="";
           for (InfBasicaAereaDetalleDto detalleEntityUbigeo : lstInfoBasicaUEPC) {
               List<InfBasicaAereaDetalleDto> lstInfoAreaUbigeo = informacionBasicaRepository.listarInfBasicaAereaUbigeo(detalleEntityUbigeo.getDepartamento(), detalleEntityUbigeo.getProvincia(), detalleEntityUbigeo.getDistrito());
               for (InfBasicaAereaDetalleDto detalleEntityUbigeo2 : lstInfoAreaUbigeo) {
                   departamento=detalleEntityUbigeo2.getDepartamento();
                   provincia=detalleEntityUbigeo2.getProvincia();
                   distrito=detalleEntityUbigeo2.getDistrito();
               }
           }

           String nroPc="",areatotal="",cuenca="";

           for(InfBasicaAereaDetalleDto detalle: lstInfoBasicaUEPC){
               if(detalle.getDescripcion()==null || detalle.getDescripcion().equals("")){
                   nroPc="";
               }else{
                   nroPc="Número PC:"+detalle.getDescripcion();
               }

               if(detalle.getAreaHa()==null || detalle.getAreaHa().equals("")){
                   areatotal="";
               }else{
                   areatotal="Área total (ha) "+detalle.getAreaHa().toString();
               }

               if(detalle.getCuenca()==null || detalle.getCuenca().equals("")){

                   cuenca="";
               }else{
                   cuenca=detalle.getCuenca().toString();
               }
               cell = new PdfPCell(new Paragraph(nroPc,contenido));
               cell.setFixedHeight(size);
               table.addCell(cell);
               cell = new PdfPCell(new Paragraph(areatotal,contenido));
               cell.setFixedHeight(size);
               table.addCell(cell);
               cell = new PdfPCell(new Paragraph(cuenca,contenido));
               cell.setFixedHeight(size);
               table.addCell(cell);
               cell = new PdfPCell(new Paragraph(departamento,contenido));
               cell.setFixedHeight(size);
               table.addCell(cell);
               cell = new PdfPCell(new Paragraph(provincia,contenido));
               cell.setFixedHeight(size);
               table.addCell(cell);
               cell = new PdfPCell(new Paragraph(distrito,contenido));
               cell.setFixedHeight(size);
               table.addCell(cell);
           }
       }


        return table;
    }

    public  PdfPTable createInfBasicaCoor(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(5);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font titulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //4.2  --8
        List<InfBasicaAereaDetalleDto> lstInfoBasicaCOOUTM = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMCUMD");

        cell = new PdfPCell(new Paragraph("Anexo o sector",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Punto",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Este",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Norte",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Referencia",titulo));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoBasicaCOOUTM!=null){
            for(InfBasicaAereaDetalleDto detalle: lstInfoBasicaCOOUTM){

                cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getPuntoVertice() != null? detalle.getPuntoVertice().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaEste() != null? detalle.getCoordenadaEste().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCoordenadaNorte() != null? detalle.getCoordenadaNorte().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getReferencia() != null? detalle.getReferencia().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }


        return table;
    }

    public  PdfPTable createInfBasicaTipoBosque(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //4.3
        List<InfBasicaAereaDetalleDto> lstInfoBasicaTB = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMABTB");

        cell = new PdfPCell(new Paragraph("Tipo de Bosque",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Área",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("%",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstInfoBasicaTB!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaTB) {

                cell = new PdfPCell(new Paragraph(detalle.getNombre() != null ? detalle.getNombre().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHa() != null ? detalle.getAreaHa().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAreaHaPorcentaje() != null ? detalle.getAreaHaPorcentaje().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createInfBasicaACCA(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //4.4
        List<InfBasicaAereaDetalleDto> lstInfoBasicaAcc = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMA");
        if(lstInfoBasicaAcc!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaAcc) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createInfBasicaACCC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //4.4
        List<InfBasicaAereaDetalleDto> lstInfoBasicaAcc = informacionBasicaRepository.listarInfBasicaAerea("POAC", idPlanManejo, "POACIBAMA");
        if(lstInfoBasicaAcc!=null) {
            for (InfBasicaAereaDetalleDto detalle : lstInfoBasicaAcc) {

                cell = new PdfPCell(new Paragraph(detalle.getJustificacion() != null ? detalle.getJustificacion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createActAprovDeli(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        // 5.1
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);

        cell = new PdfPCell(new Paragraph("Sistema de Demarcación",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modo de Implementación",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lstAprovechamiento!=null) {
            List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream().filter(eval -> eval.getCodSubActvAprove().equals("POACAADAAA")).collect(Collectors.toList());

            List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
            if (lstListaFil.size() > 0) {
                ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
                lstListaDetalle = PGMFEVALI.getLstactividadDet();
            }
            if(lstListaDetalle!=null) {
                List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream().filter(eval -> eval.getCodActvAproveDet().equals("POACAADAAA")).collect(Collectors.toList());
                String estado = "";

                if(lstListaDet!=null) {
                    for (ActividadAprovechamientoDetEntity detalle : lstListaDet) {
                        if (detalle.getDetalle().equals("S")) {
                            estado = "X";
                        } else {
                            estado = "";
                        }
                        cell = new PdfPCell(new Paragraph(detalle.getOperaciones() != null ? detalle.getOperaciones().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(estado, contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);

                    }
                }
            }

        }

        return table;
    }

    public  PdfPTable createActAprovPlani(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(7);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 8f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        // 5.1
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);

        cell = new PdfPCell(new Paragraph("Nro",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Camino",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Construcción (km.)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Mantenimiento (km)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Alcantarillas (N°)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Puentes (N°)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Cunetas (n°)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream().filter(eval -> eval.getCodSubActvAprove().equals("POACAAPCIA")).collect(Collectors.toList());

        List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
        if (lstListaFil.size() > 0) {
            ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
            lstListaDetalle = PGMFEVALI.getLstactividadDet();
        }
        int contador=0;
        for(ActividadAprovechamientoDetEntity detalle: lstListaDetalle){
            contador++;
            cell = new PdfPCell(new Paragraph(contador+"",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getDetalle() != null? detalle.getDetalle().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getFamilia() != null? detalle.getFamilia().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getObservacion() != null? detalle.getObservacion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getOperaciones() != null? detalle.getOperaciones().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getPersonal() != null? detalle.getPersonal().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

        }

        return table;
    }

    public  PdfPTable createActAprovPOACOperaCorta(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        // 5.1
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);

        cell = new PdfPCell(new Paragraph("Método",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Equipo (tipo y número)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                .filter(eval -> eval.getCodSubActvAprove().equals("POACAAOC")).collect(Collectors.toList());

        List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
        if (lstListaFil.size() > 0) {
            ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
            lstListaDetalle = PGMFEVALI.getLstactividadDet();
        }


        for(ActividadAprovechamientoDetEntity detalle: lstListaDetalle){

            cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getPersonal() != null? detalle.getPersonal().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getMaquinariaMaterial() != null? detalle.getMaquinariaMaterial().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

        }

        return table;
    }

    public  PdfPTable createActAprovPOACArras(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        // 5.1
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);

        cell = new PdfPCell(new Paragraph("Método",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal (función y número)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                .filter(eval -> eval.getCodSubActvAprove().equals("POACAAOAT")).collect(Collectors.toList());

        List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
        if (lstListaFil.size() > 0) {
            ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
            lstListaDetalle = PGMFEVALI.getLstactividadDet();
        }

        List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                .filter(eval -> eval.getCodSubActvAproveDet().equals("A")).collect(Collectors.toList());


        for(ActividadAprovechamientoDetEntity detalle: lstListaDet){

            cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getPersonal() != null? detalle.getPersonal().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

        }

        return table;
    }

    public  PdfPTable createActAprovPOACTrans(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 8f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        // 5.1
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);

        cell = new PdfPCell(new Paragraph("Transporte",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Personal (función y número)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Maquinaria / equupo (tipo y número)",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                .filter(eval -> eval.getCodSubActvAprove().equals("POACAAOAT")).collect(Collectors.toList());

        List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
        if (lstListaFil.size() > 0) {
            ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
            lstListaDetalle = PGMFEVALI.getLstactividadDet();
        }

        List<ActividadAprovechamientoDetEntity> lstListaDet = lstListaDetalle.stream()
                .filter(eval -> eval.getCodSubActvAproveDet().equals("T")).collect(Collectors.toList());


        for(ActividadAprovechamientoDetEntity detalle: lstListaDet){

            cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null? detalle.getDescripcion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getPersonal() != null? detalle.getPersonal().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph(detalle.getMaquinariaMaterial() != null? detalle.getMaquinariaMaterial().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

        }

        return table;
    }

    public  PdfPTable createActAprovPOACProce(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        // 5.1
        ActividadAprovechamientoParam actAprov = new ActividadAprovechamientoParam();
        actAprov.setIdPlanManejo(idPlanManejo);
        actAprov.setCodActvAprove("POAC");
        ResultEntity<ActividadAprovechamientoEntity> lstAprovechamiento = actividadAprovechamientoRepository.ListarActvidadAprovechamiento(actAprov);

        List<ActividadAprovechamientoEntity> lstListaFil = lstAprovechamiento.getData().stream()
                .filter(eval -> eval.getCodSubActvAprove().equals("POACAAPL")).collect(Collectors.toList());

        List<ActividadAprovechamientoDetEntity> lstListaDetalle = new ArrayList<>();
        if (lstListaFil.size() > 0) {
            ActividadAprovechamientoEntity PGMFEVALI = lstListaFil.get(0);
            lstListaDetalle = PGMFEVALI.getLstactividadDet();
        }


        for(ActividadAprovechamientoDetEntity detalle: lstListaDetalle){

            cell = new PdfPCell(new Paragraph(detalle.getObservacion() != null? detalle.getObservacion().toString(): "",contenido));
            cell.setFixedHeight(50);
            table.addCell(cell);
        }

        return table;
    }

    public  PdfPTable createActividadSilviculturalAplicacionPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        float[] values = new float[3];
        values[0] = 110;
        values[1] = 30;
        values[2] = 130;
        table.setWidths(values);
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        ///6 A
        ResultClassEntity<ActividadSilviculturalDto>lstActSilvicultural= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC1");

        cell = new PdfPCell(new Paragraph("Tratamiento Silviculturales",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción del tratamiento",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstActSilvicultural.getData()!=null){
            for(ActividadSilviculturalDetalleEntity detalle: lstActSilvicultural.getData().getDetalle()){

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null? detalle.getActividad().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                if (detalle.getAccion()) {
                    cell = new PdfPCell(new Paragraph("(X)",contenido));
                } else {
                    cell = new PdfPCell(new Paragraph( "",contenido));
                }
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null? detalle.getDescripcionDetalle().toString(): "",contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createActividadSilviculturalReforestacionPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        ///6 B
        ResultClassEntity<ActividadSilviculturalDto>lstActSilvicultural= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC2");

        cell = new PdfPCell(new Paragraph("Reforestación",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstActSilvicultural.getData()!=null) {
            for (ActividadSilviculturalDetalleEntity detalle : lstActSilvicultural.getData().getDetalle()) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }
        return table;
    }

    public  PdfPTable createProteccionBosquePOAC1(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //7. Protección del Bosque
        List<ProteccionBosqueDetalleDto> lstProteccionBosqueBDML = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBDML");

        cell = new PdfPCell(new Paragraph("Sistema de Marcación",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Acción",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modo de implementación",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        if(lstProteccionBosqueBDML!=null){
        for (ProteccionBosqueDetalleDto detalle : lstProteccionBosqueBDML) {

            cell = new PdfPCell(new Paragraph(detalle.getTipoMarcacion() != null? detalle.getTipoMarcacion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(detalle.getNuAccion() == true ? "X" : "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(detalle.getImplementacion() != null? detalle.getImplementacion().toString(): "",contenido));
            cell.setFixedHeight(size);
            table.addCell(cell);
        }
        }

        return table;
    }

    public  PdfPTable createProteccionBosquePOAC2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(10);// Genera una tabla de dos columnas
        table.setWidthPercentage(90);
        PdfPCell cell;
        int size = 45;
        Font cabecera= new Font(Font.HELVETICA, 7f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBAIA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBAIA");

        cell = new PdfPCell(new Paragraph("Medio",cabecera));
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Factores \nAmbientales",cabecera));
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Impactos \nIdentificados",cabecera));
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modo de Implementación",cabecera));
        cell.setColspan(7);
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Censo",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Demarcación \nde Linderos",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Construcción \nde campamentos",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Construcción \nde caminos",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Tala",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Arrastre",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Otras",cabecera));
        cell.setFixedHeight(40);
        table.addCell(cell);
int i=0;
        if(lstProteccionBosquePBAIA!=null) {
            for (ProteccionBosqueDetalleDto detalle2 : lstProteccionBosquePBAIA) {

                cell = new PdfPCell(new Paragraph(detalle2.getActividad() != null ? detalle2.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle2.getTipoMarcacion() != null ? detalle2.getTipoMarcacion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle2.getImpacto() != null ? detalle2.getImpacto().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<ProteccionBosqueDetalleActividadEntity> lstListaDetalle = new ArrayList<>();
                if (lstProteccionBosquePBAIA.size() > 0) {
                    ProteccionBosqueDetalleDto PGMFEVALI = lstProteccionBosquePBAIA.get(i);
                    lstListaDetalle = PGMFEVALI.getListProteccionBosqueActividad();
                }

                if(lstListaDetalle!=null){
                    for (ProteccionBosqueDetalleActividadEntity detalle : lstListaDetalle) {
                        if (detalle.getDescripcion() == null ) {
                            cell = new PdfPCell(new Paragraph("", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        } else {
                            if(detalle.getDescripcion().equals("Censo")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else if(detalle.getDescripcion().equals("Demarcación de Linderos")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else if(detalle.getDescripcion().equals("Construcción de Campamentos")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else if(detalle.getDescripcion().equals("Construcción de Caminos")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else if(detalle.getDescripcion().equals("Tala")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else if(detalle.getDescripcion().equals("Arrastre")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else if(detalle.getDescripcion().equals("Otras")){
                                cell = new PdfPCell(new Paragraph(detalle.isAccion() == true ? "X" : "", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                            else {
                                cell = new PdfPCell(new Paragraph("", contenido));
                                cell.setFixedHeight(size);
                                table.addCell(cell);
                            }
                        }
                    }
                }else{
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }
        }

        return table;
    }

    public  PdfPTable createProteccionBosquePOAC3(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 50;
        Font cabecera= new Font(Font.HELVETICA, 8f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBPGA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBPGA");

        cell = new PdfPCell(new Paragraph("ACTIVIDAD",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DESCRIPCIÓN DE IMPACTO ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("MEDIDAS DE MITIGACIÓN AMBIENTAL",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstProteccionBosquePBPGA!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosquePBPGA) {
                if (detalle.getCodPlanGeneralDet().equals("POACPBPGA") &&
                        detalle.getSubCodPlanGeneralDet().equals("POACPBPGAPPC")) {

                    cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMitigacionAmbiental() != null ? detalle.getMitigacionAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public  PdfPTable createProteccionBosquePOAC4(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 8f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBPGA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBPGA");

        cell = new PdfPCell(new Paragraph("ACTIVIDADES",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DESCRIPCIÓN DE IMPACTO ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("MEDIDAS DE MITIGACIÓN AMBIENTAL",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstProteccionBosquePBPGA!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosquePBPGA) {
                if (detalle.getCodPlanGeneralDet().equals("POACPBPGA") &&
                        detalle.getSubCodPlanGeneralDet().equals("POACPBPGAPVS")) {

                    cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMitigacionAmbiental() != null ? detalle.getMitigacionAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }


    public  PdfPTable createProteccionBosquePOAC5(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 60;
        Font cabecera= new Font(Font.HELVETICA, 8f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        List<ProteccionBosqueDetalleDto> lstProteccionBosquePBPGA = proteccionBosqueRespository.listarProteccionBosque(idPlanManejo, "POAC", "POACPBPGA");

        cell = new PdfPCell(new Paragraph("CONTINGENCIA",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DESCRIPCIÓN DEL IMPACTO ",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("MEDIDAS DE MITIGACIÓN AMBIENTAL",cabecera));
        cell.setFixedHeight(20);
        table.addCell(cell);
        if(lstProteccionBosquePBPGA!=null) {
            for (ProteccionBosqueDetalleDto detalle : lstProteccionBosquePBPGA) {
                if (detalle.getCodPlanGeneralDet().equals("POACPBPGA") &&
                        detalle.getSubCodPlanGeneralDet().equals("POACPBPGAPCA")) {

                    cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getImpacto() != null ? detalle.getImpacto().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getMitigacionAmbiental() != null ? detalle.getMitigacionAmbiental().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public  PdfPTable createMonitoreoPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 8f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        //8  Monitoreo -- 30
        MonitoreoEntity monitoreo = new MonitoreoEntity();
        monitoreo.setIdPlanManejo(idPlanManejo);
        monitoreo.setCodigoMonitoreo("POAC");
        List<MonitoreoDetalleEntity> lstMonitoreo = monitoreoRepository.listarMonitoreo(monitoreo).getData().getLstDetalle();

        cell = new PdfPCell(new Paragraph("Actividades previstas",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Descripción de monitoreo ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsable",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstMonitoreo!=null) {
            for (MonitoreoDetalleEntity detalle : lstMonitoreo) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcion() != null ? detalle.getDescripcion().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createParticipacionComunalPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(3);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //9. PARTICIPACION COMUNAL 31
        ParticipacionComunalEntity participacionRequest = new ParticipacionComunalEntity();
        participacionRequest.setCodTipoPartComunal("POAC");
        participacionRequest.setIdPlanManejo(idPlanManejo);
        ResultClassEntity<List<ParticipacionComunalEntity>>lstParticipacion= participacionComunalRepository.ListarPorFiltroParticipacionComunal(participacionRequest);


        cell = new PdfPCell(new Paragraph("Actividades",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Responsables ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Seguimiento, actividades ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstParticipacion.getData()!=null) {
            for (ParticipacionComunalEntity detalle : lstParticipacion.getData()) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getResponsable() != null ? detalle.getResponsable().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getSeguimientoActividad() != null ? detalle.getSeguimientoActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createCapacitacionPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(4);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 9f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);

        //10. Capacitacion 32
        CapacitacionDto obj = new CapacitacionDto();
        obj.setIdPlanManejo(idPlanManejo);
        obj.setCodTipoCapacitacion("POAC");
        List<CapacitacionDto> lstCapacitacion = capacitacionRepository.ListarCapacitacion(obj);

        cell = new PdfPCell(new Paragraph("Personal a capacitar", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modalidad de capacitación  ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Lugar de Capacitación ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Temas ",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        String temas="";
        if(lstCapacitacion!=null) {
            for (CapacitacionDto detalle : lstCapacitacion) {

                cell = new PdfPCell(new Paragraph(detalle.getPersonaCapacitar() != null ? detalle.getPersonaCapacitar().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getIdTipoModalidad() != null ? detalle.getIdTipoModalidad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getLugar() != null ? detalle.getLugar().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                for (CapacitacionDetalleEntity t : detalle.getListCapacitacionDetalle()) {
                    temas += t.getActividad() != null ? t.getActividad() + ",\n" : "";
                }

                cell = new PdfPCell(new Paragraph(temas, contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createActividadSilviculturalOrganizPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(2);// Genera una tabla de dos columnas
        PdfPCell cell;
        int size = 15;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //11. Organizacion para el desarrollo 32
        ResultClassEntity<ActividadSilviculturalDto>lstActSilviculturalOrg= actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo,"POAC");


        cell = new PdfPCell(new Paragraph("Personal a capacitar", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Modalidad de capacitación  ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        if(lstActSilviculturalOrg.getData()!=null ) {
            for (ActividadSilviculturalDetalleEntity detalle : lstActSilviculturalOrg.getData().getDetalle()) {

                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDescripcionDetalle() != null ? detalle.getDescripcionDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }

    public  PdfPTable createAspectosCompPOAC(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(1);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 50;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        //14.	ASPECTOS COMPLEMENTARIOS   (37)
        InformacionGeneralDEMAEntity aspectosComRequest = new InformacionGeneralDEMAEntity();
        aspectosComRequest.setCodigoProceso("POAC");
        aspectosComRequest.setIdPlanManejo(idPlanManejo);
        ResultEntity<InformacionGeneralDEMAEntity> lstAspectos= infoGeneralDemaRepo.listarInformacionGeneralDema(aspectosComRequest);

        if(lstAspectos.getData()!=null) {
            for (InformacionGeneralDEMAEntity detalle : lstAspectos.getData()) {

                cell = new PdfPCell(new Paragraph(detalle.getDetalle() != null ? detalle.getDetalle().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createAnexos2(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(10);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 25;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        List<Anexo2Dto> lista= censoForestalRepository.ResultadosPOConcesionesAnexos2(idPlanManejo,"POAC",null,null);

        cell = new PdfPCell(new Paragraph("N° Faja", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° árbol", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DAP \n(cm)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Altura comercial \n (m)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Calidad de fuste", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas M", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vc (m3)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Arbol semillero", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("X", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Y", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(lista!=null) {
            for (Anexo2Dto detalle : lista) {

                cell = new PdfPCell(new Paragraph(detalle.getNumeroFaja() != null ? detalle.getNumeroFaja().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getnArbol() != null ? detalle.getnArbol().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNombreEspecies() != null ? detalle.getNombreEspecies().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getDap() != null ? detalle.getDap().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getAlturaComercial() != null ? detalle.getAlturaComercial().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCalidadFuste() != null ? detalle.getCalidadFuste().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getEste() != null ? detalle.getEste().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNorte() != null ? detalle.getNorte().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getVolumen() != null ? detalle.getVolumen().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }


    public  PdfPTable createCronogramaActividadesPOACAnios(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);
        table.setWidthPercentage(100);
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] = 80;
            }else{
                values[i] = 15;
            }
        }

        table.setWidths(values);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);

        Paragraph titlePara1 = new Paragraph("PLAN OPERATIVO ANUAL (POA) EN BOSQUES DE COMUNIDADES NATIVAS Y/O CAMPESINAS CON FINES DE COMERCIALIZACION A ALTA ESCALA",cabecera);
        titlePara1.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara1);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(37);
        cell.setFixedHeight(30);
        table.addCell(cell);

        Paragraph titlePara2 = new Paragraph("Actividad",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(3);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara3 = new Paragraph("Años",cabecera);
        titlePara3.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara3);
        cell.setColspan(36);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        //13. Cronograma de Actividades 36
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setCodigoProceso("POAC");
        request.setIdPlanManejo(idPlanManejo);
        request.setIdCronogramaActividad(null);
        List<CronogramaActividadEntity> lstcrono = cronograma.ListarCronogramaActividad(request).getData();


        String marca="";
        if(lstcrono!=null) {
            for (CronogramaActividadEntity detalle : lstcrono) {
                cell = new PdfPCell(new Paragraph(detalle.getActividad() != null ? detalle.getActividad().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                List<CronogramaActividadDetalleEntity> lstDetalle = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1).collect(Collectors.toList());
                if (lstDetalle != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 1 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2).collect(Collectors.toList());
                if (lstDetalle2 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 2 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);


                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<CronogramaActividadDetalleEntity> lstDetalle3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3).collect(Collectors.toList());
                if (lstDetalle3 != null) {
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses1 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==1).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses1.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses2 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==2).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses2.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeseS3 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==3).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeseS3.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses4 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==4).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses4.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses5 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==5).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses5.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses6 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==6).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses6.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses7 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==7).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses7.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses8 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==8).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses8.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses9 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==9).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses9.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses10 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==10).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses10.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses11 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==11).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses11.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    List<CronogramaActividadDetalleEntity> lstDetalleMeses12 = detalle.getDetalle().stream().filter(a -> a.getAnio() == 3 && a.getMes()==12).collect(Collectors.toList());
                    cell = new PdfPCell(new Paragraph(lstDetalleMeses12.size()>0 ? "X" : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);

                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
            }
        }

        return table;
    }

    public  PdfPTable createRentabilidadIngresos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);// Genera una tabla de dos columnas
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] =80;
            }
            else{
                values[i] =15;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);


        Paragraph titlePara2 = new Paragraph("RUBRO",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.isUseAscender() ;
        cell.setRowspan(2);
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("Año 1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("Año 2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("Año 3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setColspan(12);
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;
        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POAC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();
        if(lstRMF!=null){

            List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream().filter(p-> p.getIdTipoRubro()==1).collect(Collectors.toList());


            for(RentabilidadManejoForestalDto renta : lstIngresos) {
                cell = new PdfPCell(new Paragraph(renta.getRubro() != null ? renta.getRubro() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);

                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }

                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle = lstListaDetalle.stream().filter(p -> p.getAnio() == 1).collect(Collectors.toList());

                if (lstIngresosDetalle != null) {
                   List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                           cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                           cell.setFixedHeight(size);
                           table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }


                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2).collect(Collectors.toList());

                if (lstIngresosDetalle2 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3).collect(Collectors.toList());

                if (lstIngresosDetalle3 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }
            }


        return table;
    }


    public  PdfPTable createRentabilidadEgresos(PdfWriter writer,Integer idPlanManejo) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        PdfPTable table = new PdfPTable(37);// Genera una tabla de dos columnas
        PdfPCell cell;
        float[] values = new float[37];
        for(int i=0;i<values.length;i++){
            if(i==0){
                values[i] =80;
            }
            else{
                values[i] =15;
            }
        }
        table.setWidths(values);
        table.setWidthPercentage(100);
        int size = 30;
        Font cabecera= new Font(Font.HELVETICA, 10f, Font.BOLD);
        Font subTitulo= new Font(Font.HELVETICA, 10f, Font.COURIER);
        Font contenido= new Font(Font.HELVETICA, 9f, Font.COURIER);


        Paragraph titlePara2 = new Paragraph("RUBRO",cabecera);
        titlePara2.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara2);
        cell.setRowspan(2);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        Paragraph titlePara4 = new Paragraph("Año 1",cabecera);
        titlePara4.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara4);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara5 = new Paragraph("Año 2",cabecera);
        titlePara5.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara5);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);
        Paragraph titlePara6 = new Paragraph("Año 3",cabecera);
        titlePara6.setAlignment(Element.ALIGN_CENTER);
        cell = new PdfPCell(titlePara6);
        cell.setColspan(12);
        cell.setVerticalAlignment(3);
        cell.isUseAscender() ;
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);


        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph( "1",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "2",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "3",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "4",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "5",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "6",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "7",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "8",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "9",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "10",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "11",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph( "12",cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        int i=0;

        PlanManejoEntity pme = new PlanManejoEntity();
        pme.setIdPlanManejo(idPlanManejo);
        pme.setCodigoParametro("POAC");
        List<RentabilidadManejoForestalDto> lstRMF = (List<RentabilidadManejoForestalDto>) rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(pme).getData();

        if(lstRMF!=null){

            List<RentabilidadManejoForestalDto> lstIngresos= lstRMF.stream().filter(p-> p.getIdTipoRubro()==2).collect(Collectors.toList());

            for(RentabilidadManejoForestalDto renta : lstIngresos) {
                cell = new PdfPCell(new Paragraph(renta.getRubro()!=null?renta.getRubro():"", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);


                List<RentabilidadManejoForestalDetalleEntity> lstListaDetalle = new ArrayList<>();
                if (lstIngresos.size() > 0) {
                    RentabilidadManejoForestalDto PGMFEVALI = lstIngresos.get(i);
                    lstListaDetalle = PGMFEVALI.getListRentabilidadManejoForestalDetalle();
                }



                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle= lstListaDetalle.stream().filter(p-> p.getAnio()==1).collect(Collectors.toList());

                if (lstIngresosDetalle != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 1 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle2= lstListaDetalle.stream().filter(p-> p.getAnio()==2).collect(Collectors.toList());

                if (lstIngresosDetalle2 != null) {
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }

                List<RentabilidadManejoForestalDetalleEntity> lstIngresosDetalle3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3).collect(Collectors.toList());

                if (lstIngresosDetalle3 != null) {

                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes1 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 1).collect(Collectors.toList());
                    if(lstIngresosMes1.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes1){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes2 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 2).collect(Collectors.toList());
                    if(lstIngresosMes2.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes2){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes3 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 3).collect(Collectors.toList());
                    if(lstIngresosMes3.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes3){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes4 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 4).collect(Collectors.toList());
                    if(lstIngresosMes4.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes4){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes5 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 5).collect(Collectors.toList());
                    if(lstIngresosMes5.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes5){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes6 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 6).collect(Collectors.toList());
                    if(lstIngresosMes6.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes6){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes7 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 7).collect(Collectors.toList());
                    if(lstIngresosMes7.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes7){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes8 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 8).collect(Collectors.toList());
                    if(lstIngresosMes8.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes8){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes9 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 9).collect(Collectors.toList());
                    if(lstIngresosMes9.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes9){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes10 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 10).collect(Collectors.toList());
                    if(lstIngresosMes10.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes10){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes11 = lstListaDetalle.stream().filter(p -> p.getAnio() == 2 && p.getMes() == 11).collect(Collectors.toList());
                    if(lstIngresosMes11.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes11){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                    List<RentabilidadManejoForestalDetalleEntity> lstIngresosMes12 = lstListaDetalle.stream().filter(p -> p.getAnio() == 3 && p.getMes() == 12).collect(Collectors.toList());
                    if(lstIngresosMes12.size()>0){
                        for(RentabilidadManejoForestalDetalleEntity det : lstIngresosMes12){
                            cell = new PdfPCell(new Paragraph(det.getMonto() != null ? det.getMonto().toString() : "", contenido));
                            cell.setFixedHeight(size);
                            table.addCell(cell);
                        }
                    }else{
                        cell = new PdfPCell(new Paragraph("", contenido));
                        cell.setFixedHeight(size);
                        table.addCell(cell);
                    }
                } else {
                    cell = new PdfPCell(new Paragraph("", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                }
                i++;
            }


        }


        return table;
    }




    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }

}
