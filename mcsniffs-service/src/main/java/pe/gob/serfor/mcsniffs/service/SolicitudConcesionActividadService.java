package pe.gob.serfor.mcsniffs.service;


import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;

public interface SolicitudConcesionActividadService {

    ResultClassEntity listarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj) throws Exception;
    ResultClassEntity obtenerInformacionActividadFuenteFinanciamiento(SolicitudConcesionActividadEntity obj) throws Exception;
    ResultClassEntity registrarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj) throws Exception;
    ResultClassEntity registrarInformacionActividadFuenteFinanciamiento(List<SolicitudConcesionActividadEntity> obj) throws Exception;
    ResultClassEntity actualizarInformacionActividadFuenteFinanciamiento(SolicitudConcesionActividadEntity obj) throws Exception;
}
