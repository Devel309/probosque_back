package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDto;

import java.util.List;

public interface RecursoForestalService {
   // ResultClassEntity RegistrarRecursoForestal(RecursoForestalEntity recursoForestalEntity) throws Exception;
   ResultClassEntity RegistrarRecursoForestal(List<RecursoForestalEntity> list) throws Exception;


    ResultClassEntity EliminarRecursoForestal(RecursoForestalDetalleDto recursoForestalDetalleDto) throws Exception;
    ResultClassEntity<List<RecursoForestalDetalleDto>> listarRecursoForestal(Integer idPlanManejo,Integer idRecursoForestal,String codCabecera) throws Exception;

}
