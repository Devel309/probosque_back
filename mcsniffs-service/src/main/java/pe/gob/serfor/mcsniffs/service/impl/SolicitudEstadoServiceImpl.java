package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.EstadoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudEstadoRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudEstadoService;

@Repository
public class SolicitudEstadoServiceImpl implements SolicitudEstadoService {


    private static final Logger log = LogManager.getLogger(SolicitudEstadoServiceImpl.class);

    @Autowired
    private SolicitudEstadoRepository repo;

    
    
    @Override
    public ResultEntity ListaComboSolicitudEstado(EstadoSolicitudEntity request) {
        // TODO Auto-generated method stub
        return repo.ListaComboSolicitudEstado(request);
    }
    
}
