package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;

import java.util.List;

public interface SolicitudBosqueLocalArchivoService {

    ResultClassEntity listarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj) throws Exception;
    ResultClassEntity obtenerArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception;
    ResultClassEntity registrarArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception;
    ResultClassEntity actualizarArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception;
    ResultClassEntity registrarArchivosSolicitudBosqueLocal(List<SolicitudBosqueLocalArchivoEntity> items) throws Exception;
    ResultClassEntity eliminarArchivoSolicitudBosqueLocal(SolicitudBosqueLocalArchivoEntity obj) throws Exception;
    ResultArchivoEntity descargarPlantillaIniciativaBosqueLocal() throws Exception;
}
