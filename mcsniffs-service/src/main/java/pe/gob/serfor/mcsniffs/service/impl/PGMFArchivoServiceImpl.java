package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.PGMFArchivoRepository;
import pe.gob.serfor.mcsniffs.service.PGMFArchivoService;

@Service
public class PGMFArchivoServiceImpl implements PGMFArchivoService{
    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {Repository de los archivos relaciones con el plan de manejo}
     *
     */
    @Autowired
    PGMFArchivoRepository repository;

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {registrar archivos}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultClassEntity registrarArchivo(PGMFArchivoEntity obj) throws Exception {
        return repository.registrarArchivo(obj);
    }

    /**
     * @autor: JaquelineDB [1-09-2021]
     * @modificado:
     * @descripción: {listar los archivos del proceso}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultEntity<PGMFArchivoEntity> listarArchivosPGMF(PGMFArchivoEntity filtro) {
        return repository.listarArchivosPGMF(filtro);
    }

    /**
     * @autor: JaquelineDB [2-09-2021]
     * @modificado:
     * @descripción: {se genera el anexo2pgmf}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo2PGMF(PGMFArchivoEntity filtro) {
        return repository.generarAnexo2PGMF(filtro);
    }

    /**
     * @autor: JaquelineDB [2-09-2021]
     * @modificado:
     * @descripción: {se genera el anexo3pgmf}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo3PGMF(PGMFArchivoEntity filtro) {
        return repository.generarAnexo3PGMF(filtro);
    }

    @Override
    public ResultEntity<PGMFArchivoEntity> listarDetalleArchivo(Integer idPlanManejo, String codigoTipoPGMF) {
        return repository.listarDetalleArchivo(idPlanManejo,codigoTipoPGMF);
    }

    @Override
    public ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception {
        return repository.eliminarDetalleArchivo(idArchivo, idUsuario);
    }

}
