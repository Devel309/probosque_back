package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDetalleDto;

public interface EvaluacionDetalleService {
    ResultClassEntity<List<EvaluacionDetalleDto>> listarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception;
    ResultClassEntity registrarEvaluacionDetalle(List<EvaluacionDetalleDto> dto) throws Exception;
    ResultClassEntity eliminarEvaluacionDetalle(List<EvaluacionDetalleDto> dto) throws Exception;
}
