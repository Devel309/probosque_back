package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaInfractorDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultarRegenteDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;
import pe.gob.serfor.mcsniffs.service.client.FeignPideApi;
import pe.gob.serfor.mcsniffs.service.util.*;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;


@Service("EvaluacionService")
public class EvaluacionServiceImpl implements EvaluacionService {

    private static final Logger log = LogManager.getLogger(EvaluacionServiceImpl.class);
    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private CronogramaActividesRepository CronogramaActividesRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;
    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;

    @Autowired
    private GenericoService genericoService;

    @Autowired
    private TipoDocumentoRepository tipoDocumentoRepository;

    @Autowired
    InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;




    @Autowired
    private EvaluacionRepository eval;

    @Autowired
    private PlanManejoRepository plan;

    @Autowired 
    private FeignPideApi feignPideApi;

    @Autowired 
    private DemaAnexosService  demaAnexos;

    @Autowired 
    private MesaPartesService  mesa;
    @Autowired 
    private SolicitudOpinionRepository opi;

    @Override
    public ResultClassEntity<List<EvaluacionDto>> listarEvaluacion(ListarEvaluacionDto dto) throws Exception {


        EvaluacionDto d = new EvaluacionDto();
        d.setIdPlanManejo(dto.getIdPlanManejo());
        d.setCodigoEvaluacion(dto.getCodigoEvaluacion());

        List<EvaluacionDto> lista = evaluacionRepository.listarEvaluacion(d);
        EvaluacionDetalleDto m = null;
        for (EvaluacionDto element : lista) {
            m = new EvaluacionDetalleDto();
            m.setIdEvaluacion(element.getIdEvaluacion());
            m.setCodigoEvaluacionDet(dto.getCodigoEvaluacionDet());
            m.setCodigoEvaluacionDetSub(dto.getCodigoEvaluacionDetSub());
            m.setCodigoEvaluacionDetPost(dto.getCodigoEvaluacionDetPost());            
            element.setListarEvaluacionDetalle(evaluacionDetalleRepository.listarEvaluacionDetalle(m));
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }

    @Override
    public ResultClassEntity<List<EvaluacionResumidoDto>> listarEvaluacionResumido(EvaluacionResumidoDto dto) throws Exception {


        EvaluacionResumidoDto d = new EvaluacionResumidoDto();
        d.setIdPlanManejo(dto.getIdPlanManejo());

        List<EvaluacionResumidoDto> lista = evaluacionRepository.listarEvaluacionResumido(d);
        EvaluacionResumidoDto m = null;
        for (EvaluacionResumidoDto element : lista) {
            m = new EvaluacionResumidoDto();
            m.setIdEvaluacion(element.getIdEvaluacion());
            element.setListarEvaluacionDetalle(evaluacionDetalleRepository.listarEvaluacionDetalleResumido(m));
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
        return result;
    }

    @Override
    public ResultClassEntity<List<EvaluacionPermisoForestalDto>> listarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception {


        EvaluacionPermisoForestalDto d = new EvaluacionPermisoForestalDto();
        d.setIdPermisoForestal(dto.getIdPermisoForestal());
        d.setCodigoEvaluacion(dto.getCodigoEvaluacion());
        d.setTipoEvaluacion(dto.getTipoEvaluacion());

        List<EvaluacionPermisoForestalDto> lista = evaluacionRepository.listarEvaluacionPermisoForestal(d);
        EvaluacionPermisoForestalDetalleDto m = null;

        for (EvaluacionPermisoForestalDto element : lista) {
            m = new EvaluacionPermisoForestalDetalleDto();

            m.setIdEvaluacionPermiso(element.getIdEvaluacionPermiso());
            m.setCodigoEvaluacionDet(dto.getCodigoEvaluacionDet());
            m.setCodigoEvaluacionDetSub(dto.getCodigoEvaluacionDetSub());
            m.setCodigoEvaluacionDetPost(dto.getCodigoEvaluacionDetPost());
            element.setListarEvaluacionPermisoDetalle(evaluacionDetalleRepository.listarEvaluacionPermisoForestaDetalle(m));
        }

        ResultClassEntity result = new ResultClassEntity();
        result.setData(lista);
        result.setSuccess(true);
        result.setMessage(lista.size()>0?"Información encontrada de la ":"No se encontró información");
        return result;
    }


    @Override
    public Pageable<List<PlanManejoEntity>> filtrarEvaluacionPlan(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado, Page page) throws Exception {
        return plan.filtrarEvaluacionPlan(idPlanManejo, idTipoProceso, idTipoPlan, idContrato, dniElaborador,
                rucComunidad,nombreTitular, codigoEstado, page);
    }

    @Override
    public Pageable<List<PlanManejoEntity>> filrarEval(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado,String codigoUnico,String modalidadTH, Page page) throws Exception {

        return plan.filtrarEval(idPlanManejo, idTipoProceso, idTipoPlan, idContrato, dniElaborador,
                rucComunidad,nombreTitular, codigoEstado, codigoUnico, modalidadTH, page);
    }



    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity eliminarEvaluacion(EvaluacionDto dto) throws Exception{
        ResultClassEntity result = null;
        result = evaluacionRepository.eliminarEvaluacion(dto);

        if (dto.getListarEvaluacionDetalle()!=null) {
            for (EvaluacionDetalleDto element : dto.getListarEvaluacionDetalle()) {            
                element.setIdEvaluacion(dto.getIdEvaluacion());
                element.setIdUsuarioElimina(dto.getIdUsuarioElimina());
                result = evaluacionDetalleRepository.eliminarEvaluacionDetalle(element);
            }
        }
        return  result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity eliminarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception{
        ResultClassEntity result = null;
        result = evaluacionRepository.eliminarEvaluacionPermisoForestal(dto);

        if (dto.getListarEvaluacionPermisoDetalle()!=null) {
            for (EvaluacionPermisoForestalDetalleDto element : dto.getListarEvaluacionPermisoDetalle()) {
                element.setIdEvaluacionPermiso(dto.getIdEvaluacionPermiso());
                element.setIdUsuarioElimina(dto.getIdUsuarioElimina());
                result = evaluacionDetalleRepository.eliminarEvaluacionPermisoForestalDetalle(element);
            }
        }
        return  result;
    }

    @Override
    public ResultClassEntity<InspeccionDocumentoDto> listarInspeccionTipoDocumento(TipoDocumentoEntity dto) throws Exception{
        
        ParametroEntity p = new ParametroEntity();
        p.setIdTipoParametro(99);
        List<ParametroEntity> listaParametro = genericoService.ListarPorFiltroParametro(p);
        List<TipoDocumentoEntity> listaDocumento = tipoDocumentoRepository.listaTipoDocumentoFiltro(dto);

        InspeccionDocumentoDto data = new InspeccionDocumentoDto();
        data.setListaParametro(listaParametro);
        data.setListaDocumento(listaDocumento);

        ResultClassEntity<InspeccionDocumentoDto> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
 
        return  result;
    }

    /**
     * @autor:  Rafael Azaña [31-10-2021]
     * @descripción: {Listado de tabs para la evaluacion}
     * @param: EvaluacionDto
     * @return: ResponseEntity<ResponseVO>
     */


    @Override
    public ResultClassEntity<List<EvaluacionDetalleDto>> listarEvaluacionTab(Integer idPlanManejo, String codigo,String codigoEvaluacionDet, String conforme, String codigoEvaluacionDetSub) throws Exception {

        ResultClassEntity<List<EvaluacionDetalleDto>> result = new ResultClassEntity<>();

        result.setData(evaluacionRepository.listarEvaluacionTab(idPlanManejo,codigo,codigoEvaluacionDet,conforme, codigoEvaluacionDetSub));
        result.setSuccess(true);
        result.setMessage("Lista de la Evaluación");

        return result;
    }

    @Override
    public ResultEntity listarEvaluacionGeneral(EvaluacionGeneralDto request) {

        return evaluacionRepository.listarEvaluacionGeneral(request);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarEvaluacion(EvaluacionDto dto) throws Exception {
        ResultClassEntity result = null;
        result = new ResultClassEntity<>();

        ResultClassEntity r = evaluacionRepository.obtenerUltimaEvaluacion(dto.getIdPlanManejo());
       
        dto.setIdEvaluacion(r.getCodigo());
        result = evaluacionRepository.registrarEvaluacion(dto);
        
        if (dto.getListarEvaluacionDetalle()!=null) {
            for (EvaluacionDetalleDto element : dto.getListarEvaluacionDetalle()) {
                element.setIdEvaluacion(dto.getIdEvaluacion());
                element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
                result = evaluacionDetalleRepository.registrarEvaluacionDetalle(element);
            }
        }
        result.setData(dto);
        return  result;
    }

    //PermisoForestal
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception {
        ResultClassEntity result = null;
        result = new ResultClassEntity<>();

        ResultClassEntity r = evaluacionRepository.obtenerUltimaEvaluacionPermisoForestal(dto.getIdPermisoForestal());

        dto.setIdEvaluacionPermiso(r.getCodigo());
            String tipo="";
        result = evaluacionRepository.registrarEvaluacionPermisoForestal(dto);
        tipo=result.getMessage();
        if (dto.getListarEvaluacionPermisoDetalle()!=null) {
            for (EvaluacionPermisoForestalDetalleDto element : dto.getListarEvaluacionPermisoDetalle()) {
                element.setIdEvaluacionPermiso(dto.getIdEvaluacionPermiso());
                element.setIdUsuarioRegistro(dto.getIdUsuarioRegistro());
                result = evaluacionDetalleRepository.registrarEvaluacionPermisoForestalDetalle(element);
            }
        }
        if(tipo.equals("EEVPF")){tipo="Evaluación";}else if(tipo.equals("EVPF")){tipo="Validación";}
        result.setMessage("Se registró la "+tipo + " correctamente.");
        result.setData(dto);
        return  result;
    }

    @Override
    public ResultEntity<PlanManejoEvaluacionDetalleDto> listarEvaluacionDetalle(Integer idPlanManejoEval) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @autor: Alexis Salgado - 15-09-2021
     * @modificado:
     * @descripción: {Obtiene un archivo word}
     */
    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    @Override
    public ResultArchivoEntity generarInformacionEvaluacion(CompiladoPgmfDto compiladoPgmfDto) throws IOException {
        XWPFDocument doc = getDoc("plantillaEvaluacionPOAC.docx");
        XWPFDocument doc2 = getDoc("plantillaEvaluacionPGMFA.docx");
        ResultEntity<FichaEvaluacionEntitty> listarFichaCabera=listarFichaCabera(compiladoPgmfDto.getIdPlanManejo(), compiladoPgmfDto.getCodigoProceso());
        if(compiladoPgmfDto.getCodigoProceso().equals("POAC")){
            DocUtil.setearTitularRegente(listarFichaCabera.getData(),doc,compiladoPgmfDto.getCodigoProceso()); 
            DocUtil.setearTitularRegentePOAC(listarFichaCabera.getData(), doc);
            MesaPartesDto dto = new MesaPartesDto();
            dto.setIdPlanManejo(dto.getIdPlanManejo());
            ResultClassEntity<List<MesaPartesDto>> lstArchivos = null;
            try {
                lstArchivos = mesa.listarMesaPartes(dto);
                DocUtil.setearMesaPartes(lstArchivos.getData(), doc);
                List<SolicitudOpinionEntity> lstOpinion= opi.ListarSolicitudOpinionEvaluacion(compiladoPgmfDto.getIdPlanManejo(), compiladoPgmfDto.getCodigoProceso(),null);
                DocUtil.setearOpiniones(lstOpinion, doc);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(compiladoPgmfDto.getCodigoProceso().equals("PGMFA")){
            DocUtil.setearTitularRegente(listarFichaCabera.getData(),doc2,compiladoPgmfDto.getCodigoProceso()); 
            DocUtil.setearTitularRegentePGFMA(listarFichaCabera.getData(),doc2);
            MesaPartesDto dto = new MesaPartesDto();
            dto.setIdPlanManejo(dto.getIdPlanManejo());
            ResultClassEntity<List<MesaPartesDto>> lstArchivos = null;
            try {
                lstArchivos = mesa.listarMesaPartes(dto);
                DocUtil.setearMesaPartes(lstArchivos.getData(), doc2);
                List<SolicitudOpinionEntity> lstOpinion= opi.ListarSolicitudOpinionEvaluacion(compiladoPgmfDto.getIdPlanManejo(), compiladoPgmfDto.getCodigoProceso(),null);
                DocUtil.setearOpiniones(lstOpinion, doc2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        if(compiladoPgmfDto.getCodigoProceso().equals("POAC")){
            doc.write(b);
            doc.close();
        }else if(compiladoPgmfDto.getCodigoProceso().equals("PGMFA")){
            doc2.write(b);
            doc2.close();
        }
        
        
        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("InformeEvaluacion.docx");
        word.setTipoDocumento("COMPILADO");
        word.setArchivo(b.toByteArray());
        return word;
    }

    public ResultClassEntity obtenerRegente(String numeroDocumento, String token) throws Exception {
       
        ConsultaInfractorDto dto = new ConsultaInfractorDto();
        ConsultarRegenteDto consulregente = feignPideApi.consultarRegente(token, dto);
        ConsultarRegenteDto.Regente regente = new ConsultarRegenteDto.Regente();

        if (consulregente!=null && consulregente.getDataService() != null){
            for (ConsultarRegenteDto.Regente i : consulregente.getDataService()) {
                if (i.getNumeroDocumento()!=null && i.getNumeroDocumento().equals(numeroDocumento)){
                    regente.setId(i.getId());
                    regente.setApellidos(i.getApellidos());
                    regente.setPeriodo(i.getPeriodo());
                    regente.setNumeroLicencia(i.getNumeroLicencia());
                    regente.setEstado(i.getEstado());
                    regente.setNumeroDocumento(i.getNumeroDocumento());
                    break;
                }
            }
        }
        
        ResultClassEntity result = new ResultClassEntity();
        result.setSuccess(true);
        result.setData(regente);

        return result;
    }




    @Override
    public ResultEntity<FichaEvaluacionEntitty> listarFichaCabera(Integer idPlanManejo, String codigoProceso) {
        return evaluacionRepository.listarFichaCabera(idPlanManejo,codigoProceso);
    }

    

    @Override
    public ResultClassEntity diasHabiles(DiasHabilesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        result = evaluacionRepository.diasHabiles(dto);
        result.setSuccess(true);
        result.setMessage("Días Hábiles");
        result.setData(dto);

        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity evaluacionActualizarPlan(EvaluacionActualizarPlan dto) throws Exception{
        ResultClassEntity result = null;
        result = evaluacionRepository.evaluacionActualizarPlan(dto);

       
        return  result;
    }

    @Override
    public ResultClassEntity evaluacionActualizarPlanManejo(PlanManejoDto dto) throws Exception{
        ResultClassEntity result = null;

        result = plan.actualizarPlanManejoEstado(dto);
   
        return  result;
    }


    @Override
    public ResultArchivoEntity plantillaResolucion(Integer idPlanManejo,String tipoPlan) throws Exception {

        XWPFDocument doc = getDoc("Plantilla_de_resolucion.docx");
        ListarEvaluacionDto dto= new ListarEvaluacionDto();
        dto.setIdPlanManejo(idPlanManejo);
        System.out.println("tipoPlan "+tipoPlan);
        //InformacionGeneral
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o = infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData()!=null?
                infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().size()>0?infoGeneralDemaRepo.listarInformacionGeneralDema(o).getData().get(0):null:null;
        DocUtilResolucion.setearInfoResolucion(o,doc);

        //CUADRO 1
        List<InfBasicaAereaDetalleDto>  listaInfBasicaAereaDetalleDto=informacionBasicaRepository.listarInfBasicaAerea(tipoPlan,idPlanManejo,"PMFICINFBASUUMF");
       // http://10.6.1.162/mcsniffs-rest/api/informacionBasica/listarInfBasicaAerea?idInfBasica=PMFIC&idPlanManejo=1441&codCabecera=PMFICINFBASUUMF;
        if(listaInfBasicaAereaDetalleDto!=null && listaInfBasicaAereaDetalleDto.size()>0) DocUtilResolucion.procesarCuadro1(listaInfBasicaAereaDetalleDto,doc);
        //CUADRO 2
        List<Anexo2Dto>  listaAnexo2Dto=censoForestalRepository.ResultadosPOConcesionesAnexos2( idPlanManejo,  tipoPlan, 3,null);
        if(listaAnexo2Dto!=null && listaAnexo2Dto.size()>0) DocUtilResolucion.procesarCuadro2(o.getAreaTotal(),listaAnexo2Dto,doc);
        //CUADRO 3
        List<InfBasicaAereaDetalleDto>  listaInfBasicaAerea=informacionBasicaRepository.listarInfBasicaAerea(tipoPlan,idPlanManejo,"PMFICINFBASUUMF");
        if(listaInfBasicaAerea!=null && listaInfBasicaAerea.size()>0) DocUtilResolucion.procesarCuadro3(listaInfBasicaAerea,doc);
        //CUADRO 4
        if(listaAnexo2Dto!=null && listaAnexo2Dto.size()>0) DocUtilResolucion.procesarCuadro4(o.getAreaTotal(),listaAnexo2Dto,doc);
        //CUADRO 5
        CronogramaActividadEntity request=new CronogramaActividadEntity();
        request.setCodigoProceso( tipoPlan);
        request.setIdPlanManejo(idPlanManejo);
        ResultEntity<CronogramaActividadEntity>  listaCronogramaActividadEntity=CronogramaActividesRepository.ListarCronogramaActividad( request);
        if(listaCronogramaActividadEntity.getData()!=null && listaCronogramaActividadEntity.getData().size()>0) DocUtilResolucion.procesarCuadro5(listaCronogramaActividadEntity.getData(),doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("Plantilla_de_resolucion.docx");
        word.setTipoDocumento("Resolucion");
        word.setArchivo(b.toByteArray());

        return word;
    }


    /**
     * @autor: Jason Retamozo 22-03-2022
     * @modificado:
     * @descripción: {Obtener Archivo Plantilla Informe}
     * @param: Integer idPlanManejo
     */
    @Override
    public ByteArrayResource plantillaInformeEvaluacion(Integer idPlanManejo,String tipoProceso) throws Exception{
        XWPFDocument doc = getDoc("plantillaInformeEvaluacion.docx");

        //titulo
        InformacionGeneralDEMAEntity o = new InformacionGeneralDEMAEntity();
        o.setIdPlanManejo(idPlanManejo);
        o.setCodigoProceso(tipoProceso);
        ResultEntity<InformacionGeneralDEMAEntity> info=infoGeneralDemaRepo.listarInformacionGeneralDema(o);
        //2. objetivos
        ObjetivoManejoEntity objetivo = new ObjetivoManejoEntity();
        objetivo.setCodigoObjetivo(tipoProceso);
        objetivo.setIdPlanManejo(idPlanManejo);
        ResultEntity<ObjetivoManejoEntity> lstObjetivos= objetivoservice.listarObjetivoManejo(objetivo);
        if(info.getData()!=null && info.getData().size()>0){
            System.out.println("titulo  "+info.getData().size());
            o=info.getData().get(0);
            DocUtilInformeEvaluacion.processFile(o,lstObjetivos.getData(),doc);
        }
        //4. resultados obtenidos
        // Resultados de solicitud opinion
        List<SolicitudOpinionEntity> ListarSolicitudOpinion =  solicitudOpinionRepository.ListarSolicitudOpinionEvaluacion(idPlanManejo, tipoProceso,  null);
        if(ListarSolicitudOpinion!=null && ListarSolicitudOpinion.size()>0) DocUtilInformeEvaluacion.procesarResultadoEvaluacion1(0,ListarSolicitudOpinion,doc);

        // Resultados de Inspeccion ocular
        EvaluacionCampoDto p = new EvaluacionCampoDto();
        p.setApellidoTitular(""); //
        p.setDocumentoGestion(idPlanManejo); //
        p.setIdEvaluacionCampoEstado(0); //
        p.setNumThActoAdmin(""); //
        p.setPageNum(1);//
        p.setPageSize(10);//
        p.setTipoDocumentoGestion("");//
        p.setTitular(""); //
        ResultClassEntity listarEvaluacionCampo =evaluacionCampoRespository.listarEvaluacionCampo(p);
        List<EvaluacionCampoDto> listaOcular = new ArrayList<EvaluacionCampoDto>();
        if(listarEvaluacionCampo.getData()!=null) {
            listaOcular=(List<EvaluacionCampoDto>) listarEvaluacionCampo.getData();
            if(listaOcular!=null && listaOcular.size()>0) DocUtilInformeEvaluacion.procesarResultadoEvaluacion2(1,listaOcular,doc);
        }

        // Resultados de Observacion
            EvaluacionResumidoDto d = new EvaluacionResumidoDto();
            d.setIdPlanManejo(idPlanManejo);

            List<EvaluacionResumidoDto> lista = evaluacionRepository.listarEvaluacionResumido(d);
            List<EvaluacionResumidoDetalleDto> listaDetalle = new ArrayList<EvaluacionResumidoDetalleDto>();
            EvaluacionResumidoDto m = null;
            for (EvaluacionResumidoDto element : lista) {
                m = new EvaluacionResumidoDto();
                m.setIdEvaluacion(element.getIdEvaluacion());
                element.setListarEvaluacionDetalle(evaluacionDetalleRepository.listarEvaluacionDetalleResumido(m));
                listaDetalle=element.getListarEvaluacionDetalle();
            }
            if(listaDetalle!=null && listaDetalle.size()>0) DocUtilInformeEvaluacion.procesarResultadoEvaluacion3(2,listaDetalle,doc);



        //CUADRO 1 Coordenadas UTM
        // tabla indice word 0
        List<InfBasicaAereaDetalleDto>  listaInfBasicaAereaDetalleDto=informacionBasicaRepository.listarInfBasicaAerea(tipoProceso,idPlanManejo,"PMFICINFBASUUMF");
        if(listaInfBasicaAereaDetalleDto!=null && listaInfBasicaAereaDetalleDto.size()>0) DocUtilInformeEvaluacion.procesarCuadro1(3,listaInfBasicaAereaDetalleDto,doc);
        //CUADRO 2  :Especies de la DEMA
        // tabla indice word 1
        List<Anexo2Dto>  listaAnexo2Dto=censoForestalRepository.ResultadosPOConcesionesAnexos2( idPlanManejo,  tipoProceso, 3,null);
        if(listaAnexo2Dto!=null && listaAnexo2Dto.size()>0) DocUtilInformeEvaluacion.procesarCuadro2(4,o.getAreaTotal(),listaAnexo2Dto,doc);
        //Cuadro 3 : Desplazamientos de los vertices del area de manejo del Plan de Manejo Forestal  Intermedio.Intermedio.
        // tabla indice word 2
        //Cuadro 4:Desplazamiento de los individuos  aprovechables del PMFI.
        // tabla indice word 3
        //Cuadro 5:Desplazamiento de los individuos semilleros del PMFI de la comunidad nativa
        // tabla indice word 4
        //Anexo 5
        // tabla indice word 5
        http://10.6.1.162/mcsniffs-rest/api/censoForestal/POConcesiones/Anexos/Anexo2?idPlanDeManejo=1441&tipoPlan=PMFIC
         listaAnexo2Dto=censoForestalRepository.ResultadosPOConcesionesAnexos2( idPlanManejo,  tipoProceso, 1,null);
        if(listaAnexo2Dto!=null && listaAnexo2Dto.size()>0) DocUtilInformeEvaluacion.procesarAnexo5(8,listaAnexo2Dto,doc);
        //Anexo 7
        // tabla indice word 6
        http://10.6.1.162/mcsniffs-rest/api/censoForestal/POConcesiones/Anexos/Anexo2?idPlanDeManejo=1441&tipoPlan=PMFIC
        listaAnexo2Dto=censoForestalRepository.ResultadosAnexo7NoMaderable( idPlanManejo,  tipoProceso);
        if(listaAnexo2Dto!=null && listaAnexo2Dto.size()>0) DocUtilInformeEvaluacion.procesarAnexo7(9,listaAnexo2Dto,doc);
        //Anexo 8
        // tabla indice word 7
        List<ListaEspecieDto>  listaEspecieDto=censoForestalRepository.ListaEspeciesInventariadasMaderables( idPlanManejo,  tipoProceso);
        if(listaEspecieDto!=null && listaEspecieDto.size()>0) DocUtilInformeEvaluacion.procesarAnexo8(10,listaEspecieDto,doc);

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();
        return new ByteArrayResource(b.toByteArray());
    }



}
