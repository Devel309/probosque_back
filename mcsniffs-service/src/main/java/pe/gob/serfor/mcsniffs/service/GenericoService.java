package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Generico.ParametroDto;

public interface GenericoService {
    List<ParametroEntity> ListarPorFiltroParametro(ParametroEntity param);
    ResultEntity<PersonaEntity> listarPorFilroPersona(PersonaEntity persona) throws  Exception;    
    ResultClassEntity<String> descargarPdf() throws Exception;
    ResultClassEntity<List<ParametroEntity>> listarParametroPorPrefijo(ParametroDto dto) throws Exception;
    ResultClassEntity<List<ParametroNivel1Entity>> listarParametroLineamiento(ParametroNivel1Entity param) throws Exception;
}
