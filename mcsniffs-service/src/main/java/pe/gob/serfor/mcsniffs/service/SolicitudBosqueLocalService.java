package pe.gob.serfor.mcsniffs.service;


import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalFlujEstadoEntity;

public interface SolicitudBosqueLocalService {

    ResultClassEntity listarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity registrarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity actualizarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity eliminarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity obtenerSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity actualizarEstadoSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity listarUsuarioSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;
    ResultClassEntity generarTHSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) throws Exception;


    ResultClassEntity enviarGobiernoLocal(SolicitudBosqueLocalEntity obj) throws Exception;

    ResultClassEntity notificarCreacionTH(SolicitudBosqueLocalEntity obj) throws Exception;

}
