package pe.gob.serfor.mcsniffs.service;

import org.springframework.security.core.userdetails.UserDetails;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;

public interface SeguridadService {

	SeguridadEntity LoginUser(SeguridadEntity obj_seguridad);
	UserDetails  UserByUsername(String username);
}
