package pe.gob.serfor.mcsniffs.service;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.ByteArrayResource;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.FichaEvaluacionEntitty;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;

public interface EvaluacionService {
    ResultClassEntity<List<EvaluacionDto>> listarEvaluacion(ListarEvaluacionDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacion(EvaluacionDto dto) throws Exception;
    ResultClassEntity registrarEvaluacion(EvaluacionDto dto) throws Exception;
    ResultClassEntity<InspeccionDocumentoDto> listarInspeccionTipoDocumento(TipoDocumentoEntity dto) throws Exception;
    ResultClassEntity<List<EvaluacionResumidoDto>> listarEvaluacionResumido(EvaluacionResumidoDto dto) throws Exception;

    /**
     * @autor: Rafael Azaña [31/10-2021]
     * @modificado:
     * @descripción: {Evaluacion}
     * @param:EvaluacionDto
     */
    ResultClassEntity<List<EvaluacionDetalleDto>> listarEvaluacionTab(Integer idPlanManejo, String codigo,String codigoEvaluacionDet, String conforme, String codigoEvaluacionDetSub) throws Exception;
    ResultEntity listarEvaluacionGeneral(EvaluacionGeneralDto request);

     // Permisos forestales
    ResultClassEntity registrarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception;
    ResultClassEntity<List<EvaluacionPermisoForestalDto>> listarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception;


     //JaquelineDB
     ResultEntity<PlanManejoEvaluacionDetalleDto> listarEvaluacionDetalle(Integer idPlanManejoEval);
     ResultEntity<FichaEvaluacionEntitty> listarFichaCabera(Integer idPlanManejo, String codigoProceso);
    ResultArchivoEntity generarInformacionEvaluacion(CompiladoPgmfDto compiladoPgmfDto)  throws IOException;
    ResultClassEntity obtenerRegente(String numeroDocumento, String token) throws Exception;

    Pageable<List<PlanManejoEntity>> filrarEval(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado,String codigoUnico,String modalidadTH, Page page) throws Exception;

    ResultClassEntity diasHabiles(DiasHabilesDto dto) throws Exception;
    ResultClassEntity evaluacionActualizarPlan(EvaluacionActualizarPlan dto) throws Exception;
    ResultClassEntity evaluacionActualizarPlanManejo(PlanManejoDto dto) throws Exception;

    Pageable<List<PlanManejoEntity>> filtrarEvaluacionPlan(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
                                                       Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado, Page page) throws Exception;

    ByteArrayResource plantillaInformeEvaluacion(Integer idPlanManejo, String tipoProceso) throws Exception;

    ResultArchivoEntity plantillaResolucion(Integer idPlanManejo,String tipoPlan) throws Exception;

}

