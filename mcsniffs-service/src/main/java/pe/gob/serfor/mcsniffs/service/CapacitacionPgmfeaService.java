package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface CapacitacionPgmfeaService {
    ResultClassEntity RegistrarCapacitacionPgmfea(List<CapacitacionDto> list)throws Exception;
    ResultClassEntity ListarCapacitacionPgmfea(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarCapacitacionPgmfea(CapacitacionEntity param)throws Exception;
}
