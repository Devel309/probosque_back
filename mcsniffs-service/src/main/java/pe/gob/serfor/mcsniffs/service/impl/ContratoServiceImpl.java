package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoPersonaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.GenerarCodContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ModeloContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.DescargarContratoPersonaJuridicaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.AprovechamientoRFNMDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosEspeciesMuestreoDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.AnexoRepository;
import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.ContratoArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ContratoPersonaRepository;
import pe.gob.serfor.mcsniffs.repository.ContratoRepository;
import pe.gob.serfor.mcsniffs.repository.CronogramaActividesRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaUbigeoRepository;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.ObjetivoManejoRepository;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoProteccionRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFAbreviadoRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ParticipacionComunalRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.PotencialProduccionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.RentabilidadManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.ResolucionArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ResolucionRepository;
import pe.gob.serfor.mcsniffs.repository.UnidadFisiograficaRepository;
import pe.gob.serfor.mcsniffs.repository.impl.ResolucionRepositoryImpl;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.ContratoService;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;
import pe.gob.serfor.mcsniffs.service.util.DocUtil;

@Service
public class ContratoServiceImpl implements ContratoService {

    private static final Logger log = LogManager.getLogger(ResolucionRepositoryImpl.class);

    @Autowired
    private ContratoRepository contratoRepository;

    @Autowired
    private ContratoPersonaRepository contratoPersonaRepository;

    @Autowired
    private ContratoArchivoRepository contratoArchivoRepository;

    @Autowired
    private FileServerConexion fileCn;

    @Autowired
    private ResolucionRepository resolucionRepository;

    @Autowired
    private ResolucionArchivoRepository resolucionArchivoRepository;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private AnexoRepository repositoryAnexo;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private EmailService emailService;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

    @Override
    public ResultClassEntity AdjuntarArchivosContrato(ContratoAdjuntosRequestEntity obj) {
        return contratoRepository.AdjuntarArchivosContrato(obj);
    }

    @Override
    public ResultClassEntity ListarContrato(ContratoEntity obj) {
        return contratoRepository.listarContrato(obj);
    }


    @Override
    public ResultEntity<ContratoAdjuntosEntity> ObtenerArchivosContrato(Integer IdContrato) {
        return contratoRepository.ObtenerArchivosContrato(IdContrato);
    }

    @Override
    public ResultClassEntity ObtenerContrato(ContratoEntity obj) {
        return contratoRepository.ObtenerContrato(obj);
    }

    // @Override
    // public ResultArchivoEntity DescargarContrato(Integer IdContrato) {
    //     return contratoRepository.DescargarContrato(IdContrato);
    // }

    @Override
    public ResultClassEntity RegistrarValidaciones(ContratoValidacionesEntity obj) {
        return contratoRepository.RegistrarValidaciones(obj);
    }

    @Override
    public ResultEntity<ContratoValidacionesEntity> ListarValidaciones(Integer IdContrato) {
        return contratoRepository.ListarValidaciones(IdContrato);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity actualizarContrato(ContratoEntity obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        try{
            if (obj.getCodigoPartidaRegistral() == null) {
                result.setSuccess(true);
                result.setValidateBusiness(false);
                result.setMessage("Debe ingresar el código de partida registral.");
                return result;
            }

            result = contratoRepository.registrarContrato(obj);


            ContratoEntity request = (ContratoEntity) result.getData();

            LogAuditoriaEntity logAuditoriaEntityContrato = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MAD_CONTRATO,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MAD_CONTRATO + request.getIdContrato(),
                    obj.getIdUsuarioModificacion());

            logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntityContrato);


            if(obj.getListaContratoArchivo() !=null ||  obj.getListaContratoArchivo().size()>0){
                for (ContratoArchivoDto element : obj.getListaContratoArchivo()) {
                    element.setIdContrato(obj.getIdContrato());
                    element.setIdUsuarioRegistro(obj.getIdUsuarioRegistro());

                    contratoArchivoRepository.registrarContratoArchivo(element);
                }
            }

            result.setSuccess(true);

        }catch (Exception e){
            result.setMessage(e.getMessage());
            result.setSuccess(false);
        }

        return result;

    }

    @Override
    public ResultClassEntity ActualizarStatusContrato(ContratoEntity obj) {
        return contratoRepository.ActualizarStatusContrato(obj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarContrato(ContratoEntity obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (obj.getCodigoPartidaRegistral() == null) {
            result.setSuccess(true);
            result.setValidateBusiness(false);
            result.setMessage("Debe ingresar el código de partida registral.");
            return result;
        }

        if (obj.getListaContratoArchivo() == null || obj.getListaContratoArchivo().isEmpty()) {
            result.setSuccess(true);
            result.setValidateBusiness(false);
            result.setMessage("Debe agregar un archivo.");
            return result;
        }

        for (ContratoArchivoDto element : obj.getListaContratoArchivo()) {
            if (element.getIdArchivo() == null) {
                result.setSuccess(true);
                result.setValidateBusiness(false);
                result.setMessage("Debe adjuntar un archivo");
                return result;
            }
        }

        result = contratoRepository.registrarContrato(obj);

        ContratoEntity request = (ContratoEntity) result.getData();
        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_CONTRATO,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MAD_CONTRATO + request.getIdContrato(),
                obj.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);


        if (result.getSuccess()) {
            for (ContratoArchivoDto element : obj.getListaContratoArchivo()) {
                element.setIdContrato(obj.getIdContrato());
                element.setIdUsuarioRegistro(obj.getIdUsuarioRegistro());

                contratoArchivoRepository.registrarContratoArchivo(element);
            }
        }

        return result;
    }

    @Override
    public ResultClassEntity obtenerContratoGeneral(ContratoEntity obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        List<ContratoEntity> listaContrato = contratoRepository.listarContratoGeneral(obj);
        ContratoPersonaDto cp = null;
        ContratoArchivoDto ca = null;

        for (ContratoEntity contratoEntity : listaContrato) {
            cp = new ContratoPersonaDto();
            cp.setIdContrato(contratoEntity.getIdContrato());
            contratoEntity.setListaContratoPersona(contratoPersonaRepository.listarContratoPersona(cp));

            ca = new ContratoArchivoDto();
            ca.setIdContrato(contratoEntity.getIdContrato());
            contratoEntity.setListaContratoArchivo(contratoArchivoRepository.listarContratoArchivo(ca));
        }

        result.setSuccess(true);
        result.setData(listaContrato);

        return result;
    }

    @Override
    public ResultClassEntity eliminarContratoArchivo(ContratoArchivoDto obj) throws Exception {
        return contratoArchivoRepository.eliminarContratoArchivo(obj);
    }

    @Override
    public ResultClassEntity generarCodigoContrato(GenerarCodContratoDto obj) throws Exception {
        return contratoRepository.generarCodigoContrato(obj);
    }

    @Override
    public ResultClassEntity listarContratoArchivo(ContratoArchivoDto dto) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        List<ContratoArchivoDto> listaContratoArchivo = contratoArchivoRepository.listarContratoArchivo(dto);
        result.setSuccess(true);
        result.setData(listaContratoArchivo);

        return result;
    }

    @Override
    public ResultClassEntity obtenerContratoArchivo(ContratoArchivoDto dto) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        byte[] byteFile = fileCn.loadFileAsResource(dto.getNombreGenerado());
        result.setSuccess(true);
        result.setData(byteFile);

        return result;
    }

    @Override
    public ResultClassEntity actualizarEstadoContrato(ContratoEntity dto) throws Exception {

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_CONTRATO,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MAD_CONTRATO + dto.getIdContrato(),
                dto.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);


        return contratoRepository.actualizarEstadoContrato(dto);
    }

    @Override
    public ResultClassEntity listarComboContrato(ContratoEntity obj) {

        return contratoRepository.listarComboContrato(obj);
    }

    @Override
    public ResultArchivoEntity descargarContratoPersonaJuridica(DescargarContratoPersonaJuridicaDto obj) throws Exception {

        ResultArchivoEntity word = new ResultArchivoEntity();
        XWPFDocument doc = new XWPFDocument();
        Integer idContrato = obj.getIdContrato();
        

        if(obj.getTipoDocumentoGestion().equals("TPMTHPA")){

            doc = getDoc("ModelContratoPersonaJuridica.docx");
            ModeloContratoDto modeloDto = new ModeloContratoDto();
            
            ModeloContratoDto resp = contratoRepository.obtenerModeloContrato(idContrato);

            if(resp != null){
                modeloDto = resp;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(modeloDto.getFechaIniContrato());
                modeloDto.setDiaLetra(FechaUtil.convertirNumeroALetras(calendar.get(Calendar.DAY_OF_MONTH)));
                modeloDto.setMesLetra(FechaUtil.getMesletras(calendar.get(Calendar.MONTH)));
                modeloDto.setAnioLetra(FechaUtil.convertirNumeroALetras(calendar.get(Calendar.YEAR)));
            }
            
            DocUtil.setearInfoContrato(modeloDto, doc);
            word.setNombeArchivo("ModelContratoPersonaJuridica.docx");
        }
        if(obj.getTipoDocumentoGestion().equals("TPMPFDM")){

            doc = getDoc("ContratoPDFM.docx");

            ModeloContratoDto modeloDto = new ModeloContratoDto();
            
            ModeloContratoDto resp = contratoRepository.obtenerModeloContrato(idContrato);

            if(resp != null){
                modeloDto = resp;

                if (modeloDto.getFechaIniContrato()!=null){
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(modeloDto.getFechaIniContrato());
                    modeloDto.setDiaLetra(FechaUtil.convertirNumeroALetras(calendar.get(Calendar.DAY_OF_MONTH)));
                    modeloDto.setMesLetra(FechaUtil.getMesletras(calendar.get(Calendar.MONTH)));
                    modeloDto.setAnioLetra(FechaUtil.convertirNumeroALetras(calendar.get(Calendar.YEAR)));
                }
            }
             

            DocUtil.setearInfoContrato2(modeloDto, doc);
            word.setNombeArchivo("ContratoPDFM.docx");
        }

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());

        return word;
    }

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Lista las geometrias activas}
     * @param:PlanManejoGeometriaEntity
     */
    @Override
    public ResultClassEntity listarContratoGeometria(String idsContrato) {
        return contratoRepository.listarContratoGeometria(idsContrato);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarResolucion(ResolucionDto obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();
        obj.setFechaResolucion(new Date());
        obj.setComentarioResolucion("REGISTRO RESOLUCION PERMISO FORESTAL" + obj.getNroDocumentoGestion());

        result = resolucionRepository.registrarResolucion(obj);

        for (ResolucionArchivoDto element : obj.getListaResolucionArchivo()) {
            element.setIdResolucion(obj.getIdResolucion());
            element.setIdUsuarioRegistro(obj.getIdUsuarioRegistro());
            resolucionArchivoRepository.registrarResolucionArchivo(element);
        }

        return result;
    }

    @Override
    public ResultClassEntity notificarResolucion(ResolucionDto param) throws Exception {
        ResultClassEntity envio = new ResultClassEntity();
        Gson gson = new Gson();


        UsuarioDto user= new UsuarioDto();
        try {
            user.setIdusuario(param.getIdUsuarioRegistro());

            ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad + "usuario/ObtenerUsuarioID", gson.toJson(user), param.getToken());
            ResultEntity resultApiExt = gson.fromJson(responseApiExt.getMessage(), ResultEntity.class);
            String correo = null;
            String nombreCompleto = null;
            for (Object item : resultApiExt.getData().stream().toArray()) {
                LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
                correo = val.get("correoElectronico").toString();
                nombreCompleto = val.get("nombres").toString() + " " + val.get("apellidoPaterno").toString() + " " + val.get("apellidoMaterno").toString();
            }

            String fecha = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

            if (correo != null) {
                String[] mails = new String[1];
                mails[0] = correo;
                EmailEntity mail = new EmailEntity();
                mail.setSubject("SERFOR:: Plan General de Manejo Forestal");
                mail.setEmail(mails);

                String body = "<html><head>"
                        + "<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                        + "<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                        + "</head>"
                        + "<body lang=ES-PE style='word-wrap:break-word'>"
                        + "<div class=WordSection1>"
                        + "<p class=MsoNormal><span lang=ES>Estimado(a): </span></p>"
                        + "<p class=MsoNormal><span lang=ES>Se remitió la Resolución de la solicitud con el ID: " + param.getIdSolicitud().toString() + " </span></p>"
                        + "<p class=MsoNormal><span lang=ES>Tiene un plazo de 15 días hábiles para que se pueda presentar algún recurso de impugnación</span></p>"
                        + "<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                        + "</div></body></html>";

                mail.setContent(body);
                Boolean smail = emailService.sendEmail(mail);
                if (smail) {
                    envio.setMessage("Correo Enviado Correctamente.");
                    envio.setSuccess(true);
                }
            }
            return envio;
        }catch(Exception e){
            log.error(e.getMessage(), e);
            envio.setSuccess(false);
            envio.setMessage("Ocurrió un error.");
            return  envio;
        }



    }




    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity obtenerResolucion(Integer nroDocumentoGestion) throws Exception {

        ResolucionDto resolucion = new ResolucionDto();
        resolucion.setNroDocumentoGestion(nroDocumentoGestion);
        ResultClassEntity result = resolucionRepository.ListarResolucion(resolucion);
        List<ResolucionDto> lista = new ArrayList<>();

        if (result != null && result.getData() != null) {
            lista = (List<ResolucionDto>) result.getData();

            ResolucionArchivoDto resolucionArchivoDto = null;
            for (ResolucionDto element : lista) {

                resolucionArchivoDto = new ResolucionArchivoDto();
                resolucionArchivoDto.setIdResolucion(element.getIdResolucion());
                element.setListaResolucionArchivo(
                        resolucionArchivoRepository.listarResolucionArchivo(resolucionArchivoDto));
            }

        }

        result.setData(lista);
        result.setSuccess(true);

        return result;
    }

    @Override
    public ResultClassEntity actualizarGeometria(ContratoEntity dto) throws Exception {


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_CONTRATO,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MAD_CONTRATO + dto.getIdContrato(),
                dto.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return contratoRepository.actualizarGeometria(dto);
    }

    @Override
    public ResultClassEntity actualizarEstadoRemitido(ContratoEntity obj) throws Exception {


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAD_CONTRATO,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MAD_CONTRATO + obj.getIdContrato(),
                obj.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return contratoRepository.actualizarEstadoRemitido(obj);
    }

    // ------------------------------------------------------------------------------------
    @Override
    public ResultArchivoEntity descargarModeloConcesionesMaderables(Integer idPlanManejo) throws Exception {

        XWPFDocument doc = getDoc("ModeloConcesionesMaderables.docx");

        /* INFORMACION GENERAL 1 */
        InformacionGeneralDto data = infoGeneralRepo.obtener(idPlanManejo).getData();
        DocUtil.setearInformacionGeneral(data, doc);

        ObjetivoManejoEntity ome = new ObjetivoManejoEntity();
        ome.setPlanManejo(new PlanManejoEntity());
        ome.getPlanManejo().setIdPlanManejo(idPlanManejo);

        /* OBJETIVOS DE MANEJO 2 */
        ResultClassEntity resObjManejo = objetivoManejoRepository.ObtenerObjetivoManejo(ome);

        if (resObjManejo != null && resObjManejo.getSuccess()) {
            ObjetivoManejoEntity dataObjetivo = (ObjetivoManejoEntity) objetivoManejoRepository
                    .ObtenerObjetivoManejo(ome).getData();
            DocUtil.setearObjetivoGeneral(dataObjetivo, doc);

            ResultClassEntity resultEspecifco = objetivoManejoRepository.ObtenerObjetivoEspecificoManejo(dataObjetivo);

            if (resultEspecifco != null && resultEspecifco.getSuccess()) {

                List<ObjetivoEspecificoManejoEntity> list = (ArrayList<ObjetivoEspecificoManejoEntity>) resultEspecifco
                        .getData();
                DocUtil.setearObjetivoEspecifico(list, doc);
            }
        }

        /* INFORMACION BASICA 3 */
        InformacionBasicaEntity ibe = new InformacionBasicaEntity();
        ibe.setIdPlanManejo(idPlanManejo);
        ibe.setCodInfBasica("PGMF");
        ibe.setCodSubInfBasica("PGMFIBAM");

        /* INFORMACION BASICA 3.1 */
        List<InformacionBasicaUbigeoEntity> listaUbicacion = informacionBasicaUbigeoRepository
                .ListarInformacionBasicaDistrito(ibe);
        if (listaUbicacion != null && !listaUbicacion.isEmpty()) {
            DocUtil.setearUbicacionPolitica(listaUbicacion, doc);
        }

        List<InfBasicaAereaDetalleDto> listaCoordenadas = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFCFFMIBUMFUECCC");
        if (listaCoordenadas != null && !listaCoordenadas.isEmpty()) {
            DocUtil.setearCoordenadasUTM(listaCoordenadas, doc);
        }

        /* INFORMACION BASICA 3.2 */

        List<InfBasicaAereaDetalleDto> listaAccesibilidad = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFIBAMA");
        if (listaAccesibilidad != null && !listaAccesibilidad.isEmpty()) {

            List<InfBasicaAereaDetalleDto> lista = listaAccesibilidad.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            List<InfBasicaAereaDetalleDto> lista2 = listaAccesibilidad.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());

            DocUtil.setearInfoBasicDet(lista, doc, 13, 2);
            DocUtil.setearInfoBasicDet(lista2, doc, 14, 0);
        }

 /*        ResultEntity<PGMFArchivoEntity> resultDetalleArch = pgmfArchivoRepository.listarDetalleArchivo(idPlanManejo,
                "PGMFIBAMA2");

        if (resultDetalleArch != null && resultDetalleArch.getIsSuccess()) {
            List<PGMFArchivoEntity> listaInfoAnexo = (ArrayList<PGMFArchivoEntity>) resultDetalleArch.getData();
            DocUtil.setearInfoAnexo4(listaInfoAnexo, doc);
        } */

        /* INFORMACION BASICA 3.3 */

        List<InfBasicaAereaDetalleDto> listaAspectosFisicos = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFIBAMAF");
        ResultClassEntity resultUnidadF = unidadFisiograficaRepository.listarUnidadFisiografica(1);// falta arreglar
        // front

        if (resultUnidadF != null && resultUnidadF.getSuccess()) {

            List<UnidadFisiograficaUMFEntity> listaUnidadFisiografica = (ArrayList<UnidadFisiograficaUMFEntity>) resultUnidadF
                    .getData();
            DocUtil.setearAspectosFisicosTab2(listaUnidadFisiografica, doc);
        }

        if (listaAspectosFisicos != null && !listaAspectosFisicos.isEmpty()) {

            List<InfBasicaAereaDetalleDto> listaAspectosFisicosTab1 = listaAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            List<InfBasicaAereaDetalleDto> listaAspectosFisicosTab3 = listaAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_3"))
                    .collect(Collectors.toList());

            List<InfBasicaAereaDetalleDto> listaAspectosFisicosTab4 = listaAspectosFisicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_4"))
                    .collect(Collectors.toList());

            DocUtil.setearInfoBasicDet(listaAspectosFisicosTab1, doc, 15, 1);
            DocUtil.setearInfoBasicDet(listaAspectosFisicosTab3, doc, 17, 0);
            DocUtil.setearInfoBasicDet(listaAspectosFisicosTab4, doc, 18, 0);

        }

        /* INFORMACION BASICA 3.4 */
        ResultClassEntity fauna = informacionBasicaRepository.obtenerFauna(idPlanManejo, "", "", "", "", "", "PGMF");

        if (fauna != null && fauna.getSuccess()) {

            List<FaunaEntity> listaFauna = (ArrayList<FaunaEntity>) fauna.getData();
            DocUtil.setearFaunaSilvestre(listaFauna, doc);
        }

        /* INFORMACION BASICA 3.5 */

        List<InfBasicaAereaDetalleDto> listaAspectosSocioEconomicos = informacionBasicaRepository
                .listarInfBasicaAerea("PGMF", idPlanManejo, "PGMFIBAMAS");
        if (listaAspectosSocioEconomicos != null &&
                !listaAspectosSocioEconomicos.isEmpty()) {

            List<InfBasicaAereaDetalleDto> listaTab1 = listaAspectosSocioEconomicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            List<InfBasicaAereaDetalleDto> listaTab2 = listaAspectosSocioEconomicos.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());

            DocUtil.setearAspectosSocioEconomicos(listaTab1, doc);
            DocUtil.setearAspectosSocioEconomicos2(listaTab2, doc);
        }

        /* INFORMACION BASICA 3.6 */

        List<InfBasicaAereaDetalleDto> listaAntecedentes = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFAUIC");
        if (listaAntecedentes != null && !listaAntecedentes.isEmpty()) {

            List<InfBasicaAereaDetalleDto> listaAntecedentesTab1 = listaAntecedentes.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_1"))
                    .collect(Collectors.toList());

            DocUtil.setearListaAntecedentes1(listaAntecedentesTab1, doc);
        }

        List<InfBasicaAereaDetalleDto> listaAntecedentes2 = informacionBasicaRepository.listarInfBasicaAerea("PGMF",
                idPlanManejo, "PGMFIBAMAIC");
        if (listaAntecedentes2 != null && !listaAntecedentes2.isEmpty()) {

            List<InfBasicaAereaDetalleDto> listaAntecedentes2tab1 = listaAntecedentes2.stream()
                    .filter(c -> c.getCodSubInfBasicaDet().equals("TAB_2"))
                    .collect(Collectors.toList());

            DocUtil.setearListaAntecedentes2(listaAntecedentes2tab1, doc);
        }

        // 4 ORDENAMIENTO Y PROTECCIÓN DE LA UMF

        // 4.1 CATEGORIAS DE ORDENAMIENTO

        OrdenamientoProteccionEntity op = new OrdenamientoProteccionEntity();
        op.setIdPlanManejo(idPlanManejo);
        op.setCodTipoOrdenamiento("PGMF");
        ResultEntity categorias = ordenamientoProteccionRepository.ListarOrdenamientoInternoDetalle(op);

        if (categorias != null && categorias.getIsSuccess()) {

            List<OrdenamientoProteccionEntity> listaOrdenamiento = categorias.getData();

            List<OrdenamientoProteccionDetalleEntity> list = listaOrdenamiento.get(0).getListOrdenamientoProteccionDet()
                    .stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFCO"))
                    .collect(Collectors.toList());
            DocUtil.setearCategoriasOrdenamiento(list, doc, 25);

            // 4.2.1 DIVISION ADMINISTRATIVA
            List<OrdenamientoProteccionDetalleEntity> list2 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABBQ")
                            && c.getCategoria() != null && c.getCategoria().equals("A"))
                    .collect(Collectors.toList());
            DocUtil.setearNBloques(list2, doc, 26);
            DocUtil.setearPeriodo(list2, doc, 27);

            List<OrdenamientoProteccionDetalleEntity> listBloquesQuincenales = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABBQ")
                            && c.getCategoria() != null && c.getCategoria().equals("B"))
                    .collect(Collectors.toList());
            DocUtil.setearListaParcelaCorta(listBloquesQuincenales, doc, 28);

            // 4.2.2 PARCELAS DE CORTA
            List<OrdenamientoProteccionDetalleEntity> listaParcelaCorta1 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABPC") &&
                            c.getDescripcion().equals("OPCION1"))
                    .collect(Collectors.toList());
            listaParcelaCorta1 = calcularAreaTotalParcela(listaParcelaCorta1);

            DocUtil.setearListaParcelaCorta(listaParcelaCorta1, doc, 29);

            List<OrdenamientoProteccionDetalleEntity> listaParcelaCorta2 = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFDABPC") &&
                            c.getDescripcion().equals("OPCION2"))
                    .collect(Collectors.toList());
            listaParcelaCorta2 = calcularAreaTotalParcela(listaParcelaCorta2);
            DocUtil.setearListaParcelaCorta(listaParcelaCorta2, doc, 30);

            // 4.3 PROTECCION Y VIGILANCIA
            // 4.3.1
            List<OrdenamientoProteccionDetalleEntity> listUbicacionMarcador = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVUMV"))
                    .collect(Collectors.toList());
            DocUtil.setearListUbicacionYSenalizacion(listUbicacionMarcador, doc, 31);

            // 4.3.2
            List<OrdenamientoProteccionDetalleEntity> listSenalizacion = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVS"))
                    .collect(Collectors.toList());
            DocUtil.setearListUbicacionYSenalizacion(listSenalizacion, doc, 32);

            // 4.3.3
            List<OrdenamientoProteccionDetalleEntity> listDemarcacion = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVDML"))
                    .collect(Collectors.toList());
            DocUtil.setearListDemarcacionYVigilancia(listDemarcacion, doc, 33);

            // 4.3.4
            List<OrdenamientoProteccionDetalleEntity> listVigilancia = listaOrdenamiento.get(0)
                    .getListOrdenamientoProteccionDet().stream()
                    .filter(c -> c.getCodigoTipoOrdenamientoDet().equals("PGMFCFFMOPUMFPVVUMF"))
                    .collect(Collectors.toList());
            System.out.println(listVigilancia);

            DocUtil.setearListDemarcacionYVigilancia(listVigilancia, doc, 34);

        }

        // 5.1 INVENTARIO FORESTAL
        List<PotencialProduccionForestalEntity> listaProduccionForestal = potencialProduccionForestalRepository
                .ListarPotencialProducForestal(idPlanManejo, "PGMF");

        List<PotencialProduccionForestalEntity> listaProduccionTab1 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMCIF"))
                .collect(Collectors.toList());

        List<PotencialProduccionForestalEntity> listaProduccionTab2 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRPM"))
                .collect(Collectors.toList());

        if (listaProduccionTab1 != null && !listaProduccionTab1.isEmpty()) {
            DocUtil.setearListaProduccionTab1(listaProduccionTab1.get(0), doc, 35);
        }
        if (listaProduccionTab2 != null && !listaProduccionTab2.isEmpty()) {
            DocUtil.setearListaProduccionTab2(listaProduccionTab2.get(0), doc, 36);
        }

        // 5.2 INVENTARIO FORESTAL
        // 5.2.1 ERROR MUESTREO
        List<ResultadosEspeciesMuestreoDto> censoForestalEspecies = censoForestalRepository
                .ResultadosEspecieMuestreo(idPlanManejo, "PGMF");

        List<PotencialProduccionForestalEntity> listaProduccionTab3 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRPMB"))
                .collect(Collectors.toList());

        if (listaProduccionTab3 != null && !listaProduccionTab3.isEmpty()) {
            DocUtil.setearListaProduccionTab3(listaProduccionTab3.get(0), doc, 37);
        }
        // 5.2.2 RESUMEN VALORES
        List<ResultadosEspeciesMuestreoDto> listaProduccionForestal2 = censoForestalRepository
                .ResultadosEspecieMuestreo(idPlanManejo, "PGMF");
        if (listaProduccionForestal2 != null && !listaProduccionForestal2.isEmpty()) {
            List<ResultadosEspeciesMuestreoDto> listaProdFor = listaProduccionForestal2.stream()
                    .filter(c -> c.getListTablaEspeciesMuestreo() != null
                            && !c.getListTablaEspeciesMuestreo().isEmpty())
                    .collect(Collectors.toList());

            DocUtil.setearListaProduccionForestal2(listaProdFor, doc, 38);
        }

        // 5.2.3 ESPECIES ABUNDANTES
        DocUtil.setearListaProduccionTab4(censoForestalEspecies, doc, 39);
        // 5.3.1 RESULTADOS FUSTALES
        ResultClassEntity resFustales = censoForestalRepository.listarResultadosFustales(idPlanManejo, "PGMF");
        if (resFustales != null && resFustales.getSuccess()) {
            List<ResultadosEspeciesMuestreoDto> objList = (ArrayList<ResultadosEspeciesMuestreoDto>) resFustales
                    .getData();
            System.out.println(objList);

            List<ResultadosEspeciesMuestreoDto> lista53 = objList.stream()
                    .filter(c -> c.getListTablaEspeciesMuestreo() != null
                            && !c.getListTablaEspeciesMuestreo().isEmpty())
                    .collect(Collectors.toList());

            DocUtil.setearResultadosFustales(lista53, doc, 40);
        }
        // 5.3.2 RESULTADOS FUSTALES

        List<PotencialProduccionForestalEntity> listaProduccionTab532 = listaProduccionForestal.stream()
                .filter(c -> c.getCodigoSubTipoPotencialProdForestal().equals("PGMFCFFMPPRFMRFD"))
                .collect(Collectors.toList());

        if (listaProduccionTab532 != null && !listaProduccionTab532.isEmpty()) {
            DocUtil.setearResultadosFustalesE(listaProduccionTab532, doc, 41);
        }

        // 5.4 POTENCIAL DE PRODUCCIÓN

        List<AprovechamientoRFNMDto> listaPotencialProduccion = censoForestalRepository
                .ResultadosAprovechamientoRFNM(idPlanManejo, "PGMF");
        DocUtil.setearListaPotencialProduccion(listaPotencialProduccion, doc, 42);

        /** 6. MANEJO FORESTAL */

        // 6.1.
        List<ManejoBosqueEntity> listaManejoBosqueTab1 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFUPCO", null);
        if(listaManejoBosqueTab1!=null && !listaManejoBosqueTab1.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaBosqueTab1 =listaManejoBosqueTab1.get(0).getListManejoBosqueDetalle();
            DocUtil.setearUsoPotencialPorCategoria(listaBosqueTab1, doc, 43);
        }
        // 6.2.
        List<ManejoBosqueEntity> listaManejoBosqueTab2 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFST", null);
        if(listaManejoBosqueTab2!=null && !listaManejoBosqueTab2.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaBosqueTab2 =listaManejoBosqueTab2.get(0).getListManejoBosqueDetalle();
            DocUtil.setearSistemaManejoEspecificarYCicloDeCorta(listaBosqueTab2, doc, 44);
        }
        // 6.3.
        List<ManejoBosqueEntity> listaManejoBosqueTab3 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFCC", null);
        if(listaManejoBosqueTab3!=null && !listaManejoBosqueTab3.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaBosqueTab3 =listaManejoBosqueTab3.get(0).getListManejoBosqueDetalle();
            DocUtil.setearSistemaManejoEspecificarYCicloDeCorta(listaBosqueTab3, doc, 45);
        }
        // 6.4.
        List<ManejoBosqueEntity> listaManejoBosqueTab4 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEMDMC", null);
        if(listaManejoBosqueTab4!=null && !listaManejoBosqueTab4.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaBosqueTab4 =listaManejoBosqueTab4.get(0).getListManejoBosqueDetalle();
            DocUtil.setearEspeciesManejarDiametros(listaBosqueTab4, doc, 46);
        }
        // 6.5.
        List<ManejoBosqueEntity> listaManejoBosqueTab5 = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEFP", null);
        if(listaManejoBosqueTab5!=null && !listaManejoBosqueTab5.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaBosqueTab5 =listaManejoBosqueTab5.get(0).getListManejoBosqueDetalle();
            DocUtil.setearEspeciesDeFloraProteger(listaBosqueTab5, doc, 47);
        }
        //Se usa hasta la tabla N° 57

        // 6.6.1
        ResultClassEntity listVolumenComerProm = censoForestalRepository.listarVolumenComercialPromedio(idPlanManejo, "PGMF");
        ResultClassEntity listEspVolumenComerProm = censoForestalRepository.listarEspecieVolumenComercialPromedio(idPlanManejo, "PGMF");

        if (listVolumenComerProm != null && listVolumenComerProm.getSuccess() && listEspVolumenComerProm != null && listEspVolumenComerProm.getSuccess()) {
            List<VolumenComercialPromedioCabeceraDto> header = (ArrayList<VolumenComercialPromedioCabeceraDto>) listVolumenComerProm.getData();
            List<VolumenComercialPromedioEspecieDto> body = (ArrayList<VolumenComercialPromedioEspecieDto>) listEspVolumenComerProm.getData();
            

            DocUtil.setearHeaderCortaAnual(header, doc, 50);
            DocUtil.setearBodyCortaAnual(body, doc, 51);
        }

        ResultClassEntity listCortaAnualPerm = censoForestalRepository.listarEspecieVolumenCortaAnualPermisible(idPlanManejo, "PGMF");
        // 6.6.2
        if (listCortaAnualPerm != null && listCortaAnualPerm.getSuccess()) {
            List<VolumenComercialPromedioEspecieDto> objListCortAnPer = (ArrayList<VolumenComercialPromedioEspecieDto>) listCortaAnualPerm
                    .getData();

            objListCortAnPer = addCortaAnualTotales(objListCortAnPer);

            DocUtil.setearCortaAnualTab2(objListCortAnPer, doc, 52);
        }

        //6.7

        /* ResultClassEntity proyectoCosecha = censoForestalRepository.ListarProyeccionCosecha(idPlanManejo, "PGMF");
        if (proyectoCosecha != null && proyectoCosecha.getSuccess()) {
            List<ProyeccionCosechaCabeceraDto> listProyCos = (ArrayList<ProyeccionCosechaCabeceraDto>) proyectoCosecha
                        .getData();
            List<ProyeccionCosechaDetalleDto> cabezeraList = listProyCos.get(0).getDetalle();

            DocUtil.addColumnTableProyCos(cabezeraList, doc, 53);
        } */

        //6.8

        List<ManejoBosqueEntity> listaEspecSistAprov = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFESA", null);
        if(listaEspecSistAprov!=null && !listaEspecSistAprov.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaObjSisAprov =listaEspecSistAprov.get(0).getListManejoBosqueDetalle();
            DocUtil.setearEspecificacionesSistAprov(listaObjSisAprov, doc, 54);

            Integer asd = 2;
            Integer asdasd = 2;
        }
        //6.9
        //6.9.2 2 tablas
        List<ManejoBosqueEntity> listaEspecImport = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEPSTSA", null);
        if(listaEspecImport!=null && !listaEspecImport.isEmpty()){
            List<ManejoBosqueDetalleEntity> listaObjEspecImpor =listaEspecImport.get(0).getListManejoBosqueDetalle();

            List<ManejoBosqueDetalleEntity> ObjSubCodTipManA = listaObjEspecImpor.stream()
            .filter(c -> c.getSubCodtipoManejoDet() != null && !c.getSubCodtipoManejoDet().equals("B"))
            .collect(Collectors.toList());

            List<ManejoBosqueDetalleEntity> ObjSubCodTipManB = listaObjEspecImpor.stream()
            .filter(c -> c.getSubCodtipoManejoDet() != null && c.getSubCodtipoManejoDet().equals("B"))
            .collect(Collectors.toList());

            DocUtil.setearEspeciesImportantes(ObjSubCodTipManA, doc, 55);
            DocUtil.setearEspeciesPosibles(ObjSubCodTipManB, doc, 56);

            
        }
        //6.9.3 1 tabla
        List<ManejoBosqueEntity> manejoAreasDegradadas = manejoBosqueRepository.ListarManejoBosque(idPlanManejo, "PGMF", "PGMFCFFMMFEPSMEAD", null);
        if(manejoAreasDegradadas!=null && !manejoAreasDegradadas.isEmpty()){
            List<ManejoBosqueDetalleEntity> objAreasDeg =manejoAreasDegradadas.get(0).getListManejoBosqueDetalle();

            DocUtil.setearManejoAreasDegradadas(objAreasDeg, doc, 57);


        }
        


        /* EVALUACION IMPACTO 7 */
        EvaluacionAmbientalActividadEntity ev = new EvaluacionAmbientalActividadEntity();
        ev.setIdPlanManejo(idPlanManejo);
        ev.setTipoActividad("FACTOR");
        List<EvaluacionAmbientalActividadEntity> listaFactor = evaluacionAmbientalRepository
                .ListarEvaluacionActividadFiltro(ev);

        EvaluacionAmbientalAprovechamientoEntity eval = new EvaluacionAmbientalAprovechamientoEntity();
        eval.setIdPlanManejo(idPlanManejo);
        eval.setTipoAprovechamiento("");
        List<EvaluacionAmbientalAprovechamientoEntity> listaActividades = evaluacionAmbientalRepository
                .ListarEvaluacionAprovechamientoFiltro(eval);

        List<EvaluacionAmbientalAprovechamientoEntity> listaPreAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("PREAPR"))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("APROVE"))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaPostAprovechamiento = listaActividades.stream()
                .filter(c -> c.getTipoAprovechamiento() != null && c.getTipoAprovechamiento().equals("POSTAP"))
                .collect(Collectors.toList());

        EvaluacionAmbientalDto evalDto = new EvaluacionAmbientalDto();
        evalDto.setIdPlanManejo(idPlanManejo);
        List<EvaluacionAmbientalDto> listaRespuesta = evaluacionAmbientalRepository.ListarEvaluacionAmbiental(evalDto);

        DocUtil.setearCabeceraImpactoAmb(listaPreAprovechamiento, listaAprovechamiento, listaPostAprovechamiento, doc,
                58);
        DocUtil.setearDataImpactoAmbiental(listaFactor, listaPreAprovechamiento, listaAprovechamiento,
                listaPostAprovechamiento, listaRespuesta, doc, 58);

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab2 = listaActividades.stream()
                .filter(c -> c.getNombreAprovechamiento() != null && !c.getNombreAprovechamiento().equals(""))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab3 = listaActividades.stream()
                .filter(c -> c.getImpacto() != null && !c.getImpacto().equals(""))
                .collect(Collectors.toList());

        List<EvaluacionAmbientalAprovechamientoEntity> listaActividadesTab4 = listaActividades.stream()
                .filter(c -> c.getContingencia() != null && !c.getContingencia().equals(""))
                .collect(Collectors.toList());

        DocUtil.setearListaActividadesTab2(listaActividadesTab2, doc, 59);
        DocUtil.setearListaActividadesTab3(listaActividadesTab3, doc, 60);
        DocUtil.setearListaActividadesTab4(listaActividadesTab4, doc, 61);

        /* MONITOREO 8 */
        List<MonitoreoEntity> listaMonitoreo = monitoreoRepository.listarMonitoreoPOCC(idPlanManejo, "PGMF");
        if (listaMonitoreo != null && !listaMonitoreo.isEmpty()) {

            List<MonitoreoEntity> listaMonitoreoTab1 = listaMonitoreo.stream()
                    .filter(c -> c.getMonitoreo().equals("TXTCabecera"))
                    .collect(Collectors.toList());

            List<MonitoreoEntity> listaMonitoreoTab2 = listaMonitoreo.stream()
                    .filter(c -> !c.getMonitoreo().equals("TXTCabecera"))
                    .collect(Collectors.toList());

            if (listaMonitoreoTab1 != null && !listaMonitoreoTab1.isEmpty()) {

                String descripcionMonitoreo = listaMonitoreoTab1.get(0).getDescripcion();
                DocUtil.setearDescripcionSistemaMonitoreo(descripcionMonitoreo, doc, 62);//62
            }
            if (listaMonitoreoTab2 != null && !listaMonitoreoTab2.isEmpty()) {

                DocUtil.setearDescripcionAccionMonitoreo(listaMonitoreoTab2, doc, 63);//63
            }

            // ResultEntity<DocumentoAdjuntoEntity> archivo =
            // null;//ObtenerAdjuntos(filtro2);
            // DocUtil.setearAnexoMapa(archivo, doc);
        }

        /* PARTICIPACION CIUDADANA 9 */
        ParticipacionComunalParamEntity pcp = new ParticipacionComunalParamEntity();
        pcp.setIdPlanManejo(idPlanManejo);
        pcp.setCodTipoPartComunal("PGMFFO");
        pcp.setIdPartComunal(null);
        ResultEntity formulacion = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp);
        if (formulacion != null && formulacion.getIsSuccess()) {
            List<ParticipacionComunalEntity> listaFormulacion = (ArrayList<ParticipacionComunalEntity>) formulacion
                    .getData();
            DocUtil.setearFomulacionPMF(listaFormulacion, doc, 64);//64
        }

        ParticipacionComunalParamEntity pcp2 = new ParticipacionComunalParamEntity();
        pcp2.setIdPlanManejo(idPlanManejo);
        pcp2.setCodTipoPartComunal("PGMFIM");
        pcp2.setIdPartComunal(null);
        ResultEntity implementacion = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp2);
        if (implementacion != null && implementacion.getIsSuccess()) {
            List<ParticipacionComunalEntity> listaFormulacion = (ArrayList<ParticipacionComunalEntity>) implementacion
                    .getData();
            DocUtil.setearImplementacionPMF(listaFormulacion, doc, 65);//65
        }

        ParticipacionComunalParamEntity pcp3 = new ParticipacionComunalParamEntity();
        pcp3.setIdPlanManejo(idPlanManejo);
        pcp3.setCodTipoPartComunal("PGMFCO");
        pcp3.setIdPartComunal(null);
        ResultEntity comites = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp3);
        if (comites != null && comites.getIsSuccess()) {
            List<ParticipacionComunalEntity> listaFormulacion = (ArrayList<ParticipacionComunalEntity>) comites
                    .getData();
            DocUtil.setearComitesPMF(listaFormulacion, doc, 66);//66
        }

        ParticipacionComunalParamEntity pcp4 = new ParticipacionComunalParamEntity();
        pcp4.setIdPlanManejo(idPlanManejo);
        pcp4.setCodTipoPartComunal("PGMFRE");
        pcp4.setIdPartComunal(null);
        ResultEntity reconocimiento = participacionComunalRepository.listarParticipacionComunalCaberaDetalle(pcp4);
        if (reconocimiento != null && reconocimiento.getIsSuccess()) {
            List<ParticipacionComunalEntity> listaFormulacion = (ArrayList<ParticipacionComunalEntity>) reconocimiento
                    .getData();
            DocUtil.setearReconicimientoPMF(listaFormulacion, doc, 67);//67
        }

        /* CAPACITACION 10 */
        CapacitacionDto capacitacionDto = new CapacitacionDto();
        capacitacionDto.setIdPlanManejo(idPlanManejo);
        capacitacionDto.setCodTipoCapacitacion("PGMFOBJ");

        List<CapacitacionDto> listaCapacitacion = capacitacionRepository.ListarCapacitacion(capacitacionDto);

        if (listaCapacitacion != null && !listaCapacitacion.isEmpty()) {
            DocUtil.setearObjetivosCapacitacionTab10(listaCapacitacion, doc, 68);//68
        }

        CapacitacionDto actividadPlanDto = new CapacitacionDto();
        capacitacionDto.setIdPlanManejo(idPlanManejo);
        capacitacionDto.setCodTipoCapacitacion("PGMF");

        List<CapacitacionDto> listaActividadPlan = capacitacionRepository.ListarCapacitacion(capacitacionDto);

        if (listaActividadPlan != null && !listaActividadPlan.isEmpty()) {
            DocUtil.setearActividadesPlanUMFTab10(listaActividadPlan, doc, 69);//69
        }

        // 11.1
        OrganizacionManejoEntity organizacionManejoEntity = new OrganizacionManejoEntity();
        organizacionManejoEntity.setIdPGMF(idPlanManejo);
        ResultClassEntity<List<OrganizacionManejoEntity>> result = pgmfaBreviadoRepository
                .OrganizacionManejo(organizacionManejoEntity);

        if (result != null && result.getSuccess()) {

            List<OrganizacionManejoEntity> listaFunciones = result.getData();

            List<OrganizacionManejoEntity> listaFuncionesTab1 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("FUNRESP"))
                    .collect(Collectors.toList());

            List<OrganizacionManejoEntity> listaFuncionesTab1Cab = listaFuncionesTab1.stream()
                    .filter(c -> c.getEsCabecera())
                    .collect(Collectors.toList());

            for (OrganizacionManejoEntity e : listaFuncionesTab1Cab) {

                List<OrganizacionManejoEntity> listaDetalle = listaFuncionesTab1.stream()
                        .filter(c -> !c.getEsCabecera() && c.getSubTipo().equals(e.getSubTipo()))
                        .collect(Collectors.toList());

                e.setListaDetalle(listaDetalle);
            }

            List<OrganizacionManejoEntity> listaFuncionesTab2 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("REQPERS"))
                    .collect(Collectors.toList());

            List<OrganizacionManejoEntity> listaFuncionesTab3 = listaFunciones.stream()
                    .filter(c -> c.getTipo().equals("REQMAQU"))
                    .collect(Collectors.toList());

            DocUtil.setearListaFuncionesTab1(listaFuncionesTab1Cab, doc, 70);
            DocUtil.setearListaFuncionesTab2(listaFuncionesTab2, doc, 71);
            DocUtil.setearListaFuncionesTab3(listaFuncionesTab3, doc, 72);
        }

        /* PROGRAMA DE INVERSIONES 12 */
        PlanManejoEntity p = new PlanManejoEntity();
        p.setIdPlanManejo(idPlanManejo);
        p.setCodigoParametro("PGMF");
        ResultClassEntity resultRenta = rentabilidadManejoForestalRepository.ListarRentabilidadManejoForestal(p);

        if (resultRenta != null && resultRenta.getSuccess()) {

            List<RentabilidadManejoForestalDto> listaProgramaInversiones = (ArrayList<RentabilidadManejoForestalDto>) resultRenta
                    .getData();

            // FIN NECESIDADES DE FINANCIAMIENTO
            // FLUJO CAJA INGRESO
            List<RentabilidadManejoForestalDto> listaFlujoCajaIngreso = listaProgramaInversiones.stream()
                    .filter(c -> c.getIdTipoRubro().equals(1))
                    .collect(Collectors.toList());

            List<Integer> listaAnioIngreso = new ArrayList<>();

            for (RentabilidadManejoForestalDto e : listaFlujoCajaIngreso) {
                for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                    listaAnioIngreso.add(el.getAnio());
                }
            }

            listaAnioIngreso = listaAnioIngreso.stream().distinct().collect(Collectors.toList());
            Collections.sort(listaAnioIngreso);

            RentabilidadManejoForestalDto rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
            rentabilidadManejoForestalDto.setRubro("TOTAL");
            RentabilidadManejoForestalDetalleEntity rentabilidadManejoForestalDetalleEntity = null;
            List<RentabilidadManejoForestalDetalleEntity> listRentabilidadManejoForestalDetalle = new ArrayList<>();
            for (Integer e : listaAnioIngreso) {
                rentabilidadManejoForestalDetalleEntity = new RentabilidadManejoForestalDetalleEntity();
                rentabilidadManejoForestalDetalleEntity.setAnio(e);
                rentabilidadManejoForestalDetalleEntity.setMonto(
                        sumarColumna(listaFlujoCajaIngreso, e));
                listRentabilidadManejoForestalDetalle.add(rentabilidadManejoForestalDetalleEntity);
            }

            rentabilidadManejoForestalDto
                    .setListRentabilidadManejoForestalDetalle(listRentabilidadManejoForestalDetalle);
            listaFlujoCajaIngreso.add(rentabilidadManejoForestalDto);

            DocUtil.addColumnFlujoCaja(listaAnioIngreso, doc, 73);
            DocUtil.setearFlujoCaja(listaFlujoCajaIngreso, doc, 73);

            // FIN FLUJO CAJA INGRESO

            // FLUJO CAJA EGRESO
            List<RentabilidadManejoForestalDto> listaFlujoCajaEgreso = listaProgramaInversiones.stream()
                    .filter(c -> c.getIdTipoRubro().equals(2))
                    .collect(Collectors.toList());

            List<Integer> listaAnioEgreso = new ArrayList<>();

            for (RentabilidadManejoForestalDto e : listaFlujoCajaEgreso) {
                for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                    listaAnioEgreso.add(el.getAnio());
                }
            }

            listaAnioEgreso = listaAnioEgreso.stream().distinct().collect(Collectors.toList());
            Collections.sort(listaAnioEgreso);

            rentabilidadManejoForestalDto = new RentabilidadManejoForestalDto();
            rentabilidadManejoForestalDto.setRubro("TOTAL");
            rentabilidadManejoForestalDetalleEntity = null;
            listRentabilidadManejoForestalDetalle = new ArrayList<>();
            for (Integer e : listaAnioEgreso) {
                rentabilidadManejoForestalDetalleEntity = new RentabilidadManejoForestalDetalleEntity();
                rentabilidadManejoForestalDetalleEntity.setAnio(e);
                rentabilidadManejoForestalDetalleEntity.setMonto(
                        sumarColumna(listaFlujoCajaEgreso, e));
                listRentabilidadManejoForestalDetalle.add(rentabilidadManejoForestalDetalleEntity);
            }

            rentabilidadManejoForestalDto
                    .setListRentabilidadManejoForestalDetalle(listRentabilidadManejoForestalDetalle);
            listaFlujoCajaEgreso.add(rentabilidadManejoForestalDto);

            DocUtil.addColumnFlujoCaja(listaAnioEgreso, doc, 74);
            DocUtil.setearFlujoCaja(listaFlujoCajaEgreso, doc, 74);
            // FIN FLUJO CAJA EGRESO

            // NECESIDADES DE FINANCIAMIENTO

            if (listaProgramaInversiones != null && !listaProgramaInversiones.isEmpty()) {

                List<RentabilidadManejoForestalDto> listaNecesidadesFinanciamiento = listaProgramaInversiones.stream()
                        .filter(c -> c.getIdTipoRubro().equals(3))
                        .collect(Collectors.toList());

                DocUtil.setearListarNecesidades(listaNecesidadesFinanciamiento, doc, 75);

            }

        }

        /* CRONOGRAMA 13 */
        CronogramaActividadEntity request = new CronogramaActividadEntity();
        request.setIdPlanManejo(idPlanManejo);
        request.setCodigoProceso("PGMF");
        ResultEntity<CronogramaActividadEntity> resultCronograma = repCronAct.ListarCronogramaActividad(request);

        if (resultCronograma != null && resultCronograma.getIsSuccess()) {

            List<CronogramaActividadEntity> listaCronograma = resultCronograma.getData();

            System.out.println(listaCronograma);

            List<CronogramaActividadEntity> listaCronogramaPlan = listaCronograma.stream()
                    .filter(c -> c.getCodigoActividad().equals("PLANIFICACION"))
                    .collect(Collectors.toList());

            List<CronogramaActividadEntity> listaCronogramaAprov = listaCronograma.stream()
                    .filter(c -> c.getCodigoActividad().equals("APROVECHAMIENTO"))
                    .collect(Collectors.toList());

            DocUtil.setearCronogramaAct(listaCronogramaPlan, doc, 76);
            DocUtil.setearCronogramaAct(listaCronogramaAprov, doc, 77);

        }

 /*        AdjuntoRequestEntity filtro2 = new AdjuntoRequestEntity();
        filtro2.setIdProcesoPostulacion(idPlanManejo);
        filtro2.setCodigoAnexo("Anexo");
        ResultEntity<PGMFArchivoEntity> archivo = this.repositoryAnexo.ObtenerArchivoAnexoMapa(filtro2);
        if (archivo != null) {

            DocUtil.setearAnexoMapa(archivo, doc);//79
        } */

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("ModeloConcesionesMaderables.docx");
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());

        return word;
    }

    private Double sumarColumna(List<RentabilidadManejoForestalDto> listaFlujoCaja, int columna) {
        BigDecimal bd = BigDecimal.ZERO;
        for (RentabilidadManejoForestalDto e : listaFlujoCaja) {
            for (RentabilidadManejoForestalDetalleEntity el : e.getListRentabilidadManejoForestalDetalle()) {
                if (el.getAnio().equals(columna)) {
                    BigDecimal b = new BigDecimal(el.getMonto(), MathContext.DECIMAL64);
                    bd = bd.add(b);
                }
            }
        }

        return bd.doubleValue();
    }

    private List<OrdenamientoProteccionDetalleEntity> calcularAreaTotalParcela(
            List<OrdenamientoProteccionDetalleEntity> lista) {

        BigDecimal bdSumaArea = BigDecimal.ZERO;

        for (OrdenamientoProteccionDetalleEntity e : lista) {
            bdSumaArea = bdSumaArea.add(e.getAreaHA());
        }

        for (OrdenamientoProteccionDetalleEntity e : lista) {

            if (e.getAreaHA() != null) {
                e.setAreaHAPorcentaje(
                        e.getAreaHA().multiply(new BigDecimal(100)).divide(bdSumaArea, 2, RoundingMode.HALF_UP));
            }
        }

        return lista;

    }

    private List<VolumenComercialPromedioEspecieDto> addCortaAnualTotales(
            List<VolumenComercialPromedioEspecieDto> lista) {
        
        VolumenComercialPromedioEspecieDto e = new VolumenComercialPromedioEspecieDto();

        Integer sumaHa = 0;
        Double sumaPorc = 0.0;
        BigDecimal sumaVcp = BigDecimal.ZERO;
        BigDecimal sumaVcpPond = BigDecimal.ZERO;
        BigDecimal sumaVolAnual = BigDecimal.ZERO;

        for (VolumenComercialPromedioEspecieDto i : lista) {
            sumaHa += i.getCantidadArb();
            sumaPorc += i.getPorcentajeArb();
            sumaVcp = sumaVcp.add(i.getVolumenComercialPromedio());
            sumaVcpPond = sumaVcpPond.add(i.getVolumenComercialPromedioPonderado());
            sumaVolAnual = sumaVolAnual.add(i.getVolumenCortaAnualPermisible());
        }

        e.setNombreComun("Total UMF");
        e.setCantidadArb(sumaHa);
        e.setPorcentajeArb(sumaPorc);
        e.setVolumenComercialPromedio(sumaVcp);
        e.setVolumenComercialPromedioPonderado(sumaVcpPond);
        e.setVolumenCortaAnualPermisible(sumaVolAnual);

        lista.add(e);
        
        return lista;
    }

    @Override
    public ResultClassEntity listarGeneracionContrato(ContratoDto obj) throws Exception {
        return contratoRepository.listarGeneracionContrato(obj);   
    }
    @Override
    public ResultClassEntity obtenerPostulacion(ContratoDto obj)  {
        return contratoRepository.obtenerPostulacion(obj);   
    }


}
