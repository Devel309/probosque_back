package pe.gob.serfor.mcsniffs.service.impl;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.*;
import pe.gob.serfor.mcsniffs.service.*;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service("ArchivoPDFPOPACANEXOService")
public class ArchivoPDFPOPACANEXOSServiceImpl implements  ArchivoPDFPOPACANEXOService{

    @Autowired
    private  SolicitudAccesoServiceImpl acceso;

    @Autowired
    private PlanManejoRepository planManejoRepository;

    @Autowired
    private PlanManejoContratoRepository planManejoContratoRepository;

    @Autowired
    private InformacionGeneralPlanificacionBosqueRepository infoGeneralPlanRepo;

    @Autowired
    private PGMFArchivoRepository pgmfArchivoRepo;

    @Autowired
    private InformacionGeneralDemaRepository infoGeneralDemaRepo;

    @Autowired
    private SistemaManejoForestalRepository smfRepo;

    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Autowired
    private CronogramaActividesRepository cronograma;

    @Autowired
    private SistemaManejoForestalRepository sistema;

    @Autowired
    private ImpactoAmbientalService ambiental;

    @Autowired
    CensoForestalRepository censoForestalDetalleRepo;

    @Autowired
    ZonificacionRepository zonificacionRepository;

    @Autowired
    CoreCentralRepository corecentral;
/*
    @Autowired
    SolicitudAccesoRepository acceso;*/

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ObjetivoManejoService objetivoservice;

    @Autowired
    private InformacionBasicaRepository informacionBasicaRepository;

    @Autowired
    private RecursoForestalRepository recursoForestalRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoRepo;

    @Autowired
    private AprovechamientoRepository aprovechamientoRepository;

    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoRepository;

    @Autowired
    private PlanManejoEvaluacionIterRepository planManejoEvaluacionIterRepository;

    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @Autowired
    private InformacionGeneralRepository infoGeneralRepo;

    @Autowired
    private PlanManejoEvaluacionRepository planManejoEvaluacionRepository;

    @Autowired
    private PlanManejoEvaluacionDetalleRepository planManejoEvaluacionDetalleRepository;

    @Autowired
    private ArchivoRepository archivoRepository;

    @Autowired
    private SolicitudOpinionRepository solicitudOpinionRepository;

    @Autowired
    private EvaluacionCampoRespository evaluacionCampoRespository;

    @Autowired
    private ProteccionBosqueRepository proteccionBosqueRespository;

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    @Autowired
    private PotencialProduccionForestalRepository potencialProduccionForestalRepository;

    @Autowired
    private ParticipacionComunalRepository participacionComunalRepository;

    @Autowired
    private CapacitacionRepository capacitacionRepository;

    @Autowired
    private RentabilidadManejoForestalRepository rentabilidadManejoForestalRepository;

    @Autowired
    private ManejoBosqueRepository manejoBosqueRepository;

    @Autowired
    private ResumenActividadPoRepository resumenActividadPoRepository;

    @Autowired
    private EvaluacionAmbientalRepository evaluacionAmbientalRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Autowired
    private ObjetivoManejoRepository objetivoManejoRepository;


    @Autowired
    private InformacionBasicaUbigeoRepository informacionBasicaUbigeoRepository;

    @Autowired
    private UnidadFisiograficaRepository unidadFisiograficaRepository;

    @Autowired
    private OrdenamientoProteccionRepository ordenamientoProteccionRepository;

    @Autowired
    private CensoForestalRepository censoForestalRepository;

    @Autowired
    private PGMFAbreviadoRepository pgmfaBreviadoRepository;

    @Autowired
    private CronogramaActividesRepository repCronAct;

    @Autowired
    private SistemaManejoForestalService sistemaManejoForestalService;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Font titulo= new Font(Font.HELVETICA, 11f, Font.BOLD);
    Font subTitulo= new Font(Font.HELVETICA, 10f, Font.BOLD);
    Font contenido= new Font(Font.HELVETICA, 8f, Font.COURIER);
    Font cabecera= new Font(Font.HELVETICA, 9f, Font.BOLD);
    Font subTituloTabla= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font subTitulo2= new Font(Font.HELVETICA, 10f, Font.COURIER);
    Font letraPeque = new Font(Font.HELVETICA, 6f, Font.COURIER);


    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

    /**
     * @autor: RAFAEL AZAÑA
     * @modificado:
     * @descripción: {Ejemplo de uso PDF con itext}
     */
    @Override
    public ByteArrayResource consolidadoPOPACANEXO_PDF(Integer idPlanManejo) throws Exception {
        XWPFDocument doc = getDoc("formatoAnexoPOPAC.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        /* ***********************************   USO DE LIBRERIA PDF       *************************************/
        InputStream myInputStream = new ByteArrayInputStream(b.toByteArray());
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(myInputStream);
        File archivo = File.createTempFile("consolidadoPOPAC", ".pdf");

        FileOutputStream os = new FileOutputStream(archivo);

        createPdfPOPAC(os,idPlanManejo);
        os.flush();
        os.close();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] fileContent = Files.readAllBytes(archivo.toPath());
        return new ByteArrayResource(fileContent);

    }

    public  void createPdfPOPAC(FileOutputStream os,Integer idPlanManejo) {
        Document document = new Document(PageSize.A4,40,40,40,40);
        document.setMargins(60, 60, 40, 40);
        try{
            PdfWriter writer = PdfWriter.getInstance(document, os);

            document.open();
            Paragraph titlePara1 = new Paragraph("PLAN OPERATIVO PMFI EN COMUNIDADES CAMPESINAS Y NATIVAS\n" +
                    "ANEXOS\n",titulo);
            titlePara1.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph(titlePara1));
            document.add(new Paragraph("\r\n"));
            PdfPTable table0 = createCodigoPlanManejo(writer,idPlanManejo);
            document.add(table0);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 1: Mapa base",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 2: Mapa de ordenamiento y división administrativa",subTitulo));
            document.add(new Paragraph("\r\n"));

            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo5: Resultados del censo forestal comercial con fines maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table3 = createAnexos5(writer,idPlanManejo);
            document.add(table3);


            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo7: Resultados del censo forestal comercial con fines no maderables",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table6=createAnexos7(writer,idPlanManejo);
            document.add(table6);


            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 8: Lista de especies mencionadas",subTitulo));
            document.add(new Paragraph("\r\n"));
            PdfPTable table7=createAnexos8(writer,idPlanManejo);
            document.add(table7);
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Anexo 9: Acta de asamblea comunal",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("Acta de asamblea comunal",subTitulo));
            document.add(new Paragraph("\r\n"));
            document.add(new Paragraph("( En el que conste el acuerdo para el ordenamiento interno, la zonificación interna comunal, la decisión del aprovechamiento del recurso y el compromiso de implementar el PMFI )",subTitulo2));
            document.add(new Paragraph("\r\n"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }

    public  PdfPTable createCodigoPlanManejo(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        float[] values = new float[2];
        values[0] = 40;
        values[1] = 150;
        table.setWidths(values);
        PdfPCell cell;
        int size=20;

        cell = new PdfPCell(new Paragraph("Plan Manejo: ",subTitulo));
        cell.setFixedHeight(size);
        cell.setBorder(0);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(idPlanManejo.toString(),subTitulo2));
        cell.setFixedHeight(size);
        cell.setBorder(0);
        table.addCell(cell);
        return table;
    }

    public  PdfPTable createAnexos4(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(6);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 25;

        //Anexo 4
        List <Anexo3PGMFDto> lstLista=censoForestalRepository.ResultadosFormatoPGMFAnexo3(idPlanManejo,"POPAC","2","");


        cell = new PdfPCell(new Paragraph("Nombre Común", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Var/ha", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DAP(cm)", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total por tipo de bosque", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total\nÁrea censada", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("10-20", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("20-30", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



        if(lstLista!=null) {
            List<Anexo3PGMFDto> lista = new ArrayList<Anexo3PGMFDto>();

            for(Anexo3PGMFDto detalle : lstLista){
                Anexo3PGMFDto data = new Anexo3PGMFDto();
                data.setNombreComun(detalle.getNombreComun());
                data.setBloque(1);
                data.setArbHaDap10a19(detalle.getArbHaDap10a19());
                data.setArbHaDap20a29(detalle.getArbHaDap20a29());
                data.setArbHaTotalTipoBosque(detalle.getArbHaTotalTipoBosque());
                data.setArbHaTotalPorArea(detalle.getArbHaTotalPorArea());
                lista.add(data);
                Anexo3PGMFDto data2 = new Anexo3PGMFDto();
                data2.setNombreComun(detalle.getNombreComun());
                data2.setBloque(2);
                data2.setArbHaDap10a19(detalle.getAbHaDap10a19());
                data2.setArbHaDap20a29(detalle.getAbHaDap20a29());
                data2.setArbHaTotalTipoBosque(detalle.getAbHaTotalTipoBosque());
                data2.setArbHaTotalPorArea(detalle.getAbHaTotalPorArea());
                lista.add(data2);
            }
            String variable="";
            if(lista!=null)
            for (Anexo3PGMFDto detalle : lista) {

                cell = new PdfPCell(new Paragraph(detalle.getNombreComun() != null ? detalle.getNombreComun().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                if(detalle.getBloque()==1){variable="N";}
                else if(detalle.getBloque()==2){variable="AB m2";}
                cell = new PdfPCell(new Paragraph(detalle.getBloque() != null ? variable : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getArbHaDap10a19() != null ? detalle.getArbHaDap10a19().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getArbHaDap20a29() != null ? detalle.getArbHaDap20a29().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getArbHaTotalTipoBosque() != null ? detalle.getArbHaTotalTipoBosque().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getArbHaTotalPorArea() != null ? detalle.getArbHaTotalPorArea().toString() : "", contenido));
                cell.setFixedHeight(size);
                table.addCell(cell);
            }
        }

        return table;
    }



    public  PdfPTable createAnexos5(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(10);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 5
        List <Anexo2Dto> lstLista=censoForestalRepository.ResultadosPOConcesionesAnexos2(idPlanManejo,"POPAC",null,null);


        cell = new PdfPCell(new Paragraph("N° \nFaja", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° Árbol\n(código)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("DAP \n (cm)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Altura\n comercial \n(m)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Calidad \n fuste", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Vc(m3) ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Árbol Aprovechable /Semillero ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("E", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



            if(lstLista!=null){
                for (Anexo2Dto detalle : lstLista) {

                    cell = new PdfPCell(new Paragraph(detalle.getNumeroFaja() != null ? detalle.getNumeroFaja().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getnArbol() != null ? detalle.getnArbol() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreEspecies() != null ? detalle.getNombreEspecies().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getDap() != null ? detalle.getDap().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getAlturaComercial() != null ? detalle.getAlturaComercial().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCalidadFuste() != null ? detalle.getCalidadFuste().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getEste() != null ? detalle.getEste().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNorte() != null ? detalle.getNorte().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getVolumen() != null ? detalle.getVolumen().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                    cell.setFixedHeight(size);
                    table.addCell(cell);
                }
        }

        return table;
    }



    public  PdfPTable createAnexos6(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(5);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 6
        List <ResultadosAnexo6> listaAnexo6=censoForestalRepository.ResultadosAnexo6(idPlanManejo,"POPAC");



        cell = new PdfPCell(new Paragraph("Tipo de Bosque", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre Comun", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Var / ha", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total por\n tipo de bosque", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Total por ha", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



        if(listaAnexo6!=null){
            List <TablaAnexo6Dto> listaDetalle=new ArrayList<TablaAnexo6Dto>();
            if (listaAnexo6.size() > 0) {
                ResultadosAnexo6 POPACLista = listaAnexo6.get(0);
                listaDetalle = POPACLista.getListaTablaAnexo6Dto();
            }
            if(listaDetalle!=null) {

                List<TablaAnexo6Dto> listaDetalle2 = new ArrayList<TablaAnexo6Dto>();
                for (TablaAnexo6Dto detalle : listaDetalle) {
                    TablaAnexo6Dto data = new TablaAnexo6Dto();
                    data.setNombreComun(detalle.getNombreComun());
                    data.setVariable("N");
                    data.setnTotalPorTipoBosque(detalle.getnTotalPorTipoBosque());
                    data.setnTotalPorHa(detalle.getnTotalPorHa());
                    listaDetalle2.add(data);
                    TablaAnexo6Dto data2 = new TablaAnexo6Dto();
                    data2.setNombreComun(detalle.getNombreComun());
                    data2.setVariable("C**");
                    data2.setnTotalPorTipoBosque(detalle.getcTotalPorTipoBosque());
                    data2.setnTotalPorHa(detalle.getcTotalPorHa());
                    listaDetalle2.add(data2);
                }

                for (TablaAnexo6Dto detalle : listaDetalle2) {

                    cell = new PdfPCell(new Paragraph( "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreComun() != null ? detalle.getNombreComun() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getVariable() != null ? detalle.getVariable().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getnTotalPorTipoBosque() != null ? detalle.getnTotalPorTipoBosque().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getnTotalPorHa() != null ? detalle.getnTotalPorHa().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);

                }
            }
        }

        return table;
    }


    public  PdfPTable createAnexos7(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(6);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 7
        List <Anexo2Dto> listaAnexo7=censoForestalRepository.ResultadosAnexo7NoMaderable(idPlanManejo,"POPAC");

        cell = new PdfPCell(new Paragraph("N° \nFaja", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N° Árbol\n(código)", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Especie", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Coordenadas UTM ", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("C* ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Árbol o individuo Semillero ", cabecera));
        cell.setFixedHeight(size);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("E", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("N", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);



        if(listaAnexo7!=null){

                for (Anexo2Dto detalle : listaAnexo7) {

                    cell = new PdfPCell(new Paragraph(detalle.getNumeroFaja() != null ? detalle.getNumeroFaja() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getnArbol() != null ? detalle.getnArbol().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getNombreEspecies() != null ? detalle.getNombreEspecies().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getEste() != null ? detalle.getEste().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getC() != null ? detalle.getC().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);
                    cell = new PdfPCell(new Paragraph(detalle.getCategoria() != null ? detalle.getCategoria().toString() : "", contenido));
                    cell.setFixedHeight(25);
                    table.addCell(cell);

                }
        }

        return table;
    }


    public  PdfPTable createAnexos8(PdfWriter writer,Integer idPlanManejo) throws Exception {
        PdfPTable table = new PdfPTable(6);// Genera una tabla de dos columnas
        PdfPCell cell;
        table.setWidthPercentage(100);
        int size = 40;

        //Anexo 8
        List <ListaEspecieDto> listaAnexo8=censoForestalRepository.ListaEspeciesInventariadasMaderables(idPlanManejo,"POPAC");

        cell = new PdfPCell(new Paragraph("Nombre común", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre nativo", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);
        cell = new PdfPCell(new Paragraph("Nombre Científico", cabecera));
        cell.setFixedHeight(size);
        table.addCell(cell);

        if(listaAnexo8!=null){

            for (ListaEspecieDto detalle : listaAnexo8) {

                cell = new PdfPCell(new Paragraph(detalle.getTextNombreComun() != null ? detalle.getTextNombreComun() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getNombreNativo() != null ? detalle.getNombreNativo().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph(detalle.getTextNombreCientifico() != null ? detalle.getTextNombreCientifico().toString() : "", contenido));
                cell.setFixedHeight(25);
                table.addCell(cell);
            }
        }

        return table;
    }



    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }

}
