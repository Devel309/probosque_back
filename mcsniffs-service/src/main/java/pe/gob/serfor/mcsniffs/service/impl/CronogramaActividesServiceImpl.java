package pe.gob.serfor.mcsniffs.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad.CronogramaActividadDto;
import pe.gob.serfor.mcsniffs.repository.CronogramaActividesRepository;
// import pe.gob.serfor.mcsniffs.repository.CronogramaActividesSDReposittory;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.CronogramaActividesService;
@Service
public class CronogramaActividesServiceImpl implements CronogramaActividesService{


    @Autowired
    private CronogramaActividesRepository repCronAct;

//@Autowired
//CronogramaActividesSDReposittory repCronActSD;

    @Override
    public ResultEntity<CronogramaActividesEntity> ListarCronogramaActividades(CronogramaActividesEntity request) {
        // TODO Auto-generated method stub
        return repCronAct.ListarCronogramaActividades(request);
    }

    @Override
    public ResultEntity<CronogramaActividesEntity> RegistrarCronogramaActividades(
            List<CronogramaActividesEntity> request) {
        // TODO Auto-generated method stub
        return repCronAct.RegistrarCronogramaActividades(request);
    }

    @Override
    public ResultEntity<CronogramaActividesEntity> EliminarCronogramaActividades(CronogramaActividesEntity request) {
        // TODO Auto-generated method stub
        return repCronAct.EliminarCronogramaActividades(request);
    }

    @Override
    public List<Object[]> ListarCronogramaActividadesPivot(CronogramaActividesEntity request) {
        // TODO Auto-generated method stub
        return null;//repCronActSD.ListarCronogramaActividadesPivot(request.getID_PLAN_MANEJO());
    }

    /***Métodos para el manejo de Cronograma Actividades usando tabla detalle*******/
    @Override
    public ResultEntity<CronogramaActividadEntity> RegistrarCronogramaActividad(CronogramaActividadEntity request){
        // TODO Auto-generated method stub
        int anio = 0;//pru: "1-1"
        int mes = 0;//pru: "2-13"

        //registramos la cabecera de cronograma
        ResultEntity<CronogramaActividadEntity> responseActividad  = repCronAct.RegistrarCronogramaActividad(request);

        //Enviamos el listado de selección
        if(request.getListaMarca()!=null){
            if (request.getListaMarca().length!=0){
                String[] arrayMarca = request.getListaMarca();
                for (int i=0;i<arrayMarca.length;i++) {
                    String[] prua = arrayMarca[i].split("-");
                    anio = Integer.parseInt(prua[0]);
                    mes = Integer.parseInt(prua[1]);
                    request.setIdCronogramaActividad(responseActividad.getCodigo());
                    request.setAnio(anio);
                    request.setMes(mes);
                    repCronAct.RegistrarMarcaCronogramaActividadDetalle(request);
                }
            }
        }
        return responseActividad;
    }
    @Override
    public ResultEntity<CronogramaActividadEntity> EliminarCronogramaActividad(CronogramaActividadEntity request){
        // TODO Auto-generated method stub
        return repCronAct.EliminarCronogramaActividad(request);
    }
    @Override
    public ResultEntity<CronogramaActividadEntity> ListarCronogramaActividad(CronogramaActividadEntity request){
        // TODO Auto-generated method stub
        return repCronAct.ListarCronogramaActividad(request);
    }

    @Override
    public ResultEntity<CronogramaActividadEntity> ListarCronogramaActividadTitular(CronogramaActividadEntity request){
        // TODO Auto-generated method stub
        return repCronAct.ListarCronogramaActividadTitular(request);
    }


    @Override
    public ResultEntity<CronogramaActividadEntity> RegistrarMarcaCronogramaActividadDetalle(CronogramaActividadEntity request){
        // TODO Auto-generated method stub
        return repCronAct.RegistrarMarcaCronogramaActividadDetalle(request);
    }

    @Override
    public ResultEntity<CronogramaActividesEntity> ListarCronogramaActividadDetalle(CronogramaActividesEntity request) {
        // TODO Auto-generated method stub
        return null; //return repCronAct.ListarCronogramaActividadDetalle(request);
    }

     /**
     * @autor: JaquelineDB [07-09-2021]
     * @modificado:
     * @descripción: {Elimina el detalle del cronograma}
     * @param:
     */
    @Override
    public ResultClassEntity EliminarCronogramaActividadDetalle(CronogramaActividadEntity filtro) {
        return repCronAct.EliminarCronogramaActividadDetalle(filtro);
    }
    @Override
    public ResultClassEntity RegistrarConfiguracionCronogramaActividad(CronogramaActividadDto request) {
        return repCronAct.RegistrarConfiguracionCronogramaActividad(request);
    }
    @Override
    public ResultClassEntity ListarPorFiltroCronogramaActividad(CronogramaActividadDto request) {
        return repCronAct.ListarPorFiltroCronogramaActividad(request);
    }

    /**
     * @autor: Danny Nazario [29-07-2021]
     * @modificado:
     * @descripción: {Registra Cronograma Actividades Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarCronogramaActividadExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna, String codigoProceso, String codigoActividad, int idPlanManejo, int idUsuarioRegistro) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);

            CronogramaActividadEntity cab = new CronogramaActividadEntity();
            CronogramaActividadEntity det = new CronogramaActividadEntity();
            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;

                cab.setCodigoProceso(codigoProceso);
                cab.setActividad(codigoActividad);
                cab.setActividad(row.get(0));
                cab.setIdPlanManejo(idPlanManejo);
                cab.setIdUsuarioRegistro(idUsuarioRegistro);
                ResultEntity<CronogramaActividadEntity> resAct = repCronAct.RegistrarCronogramaActividad(cab);

                det.setIdCronogramaActividad(resAct.getCodigo());
                det.setIdCronogramaActividadDetalle(0);
                det.setIdUsuarioRegistro(idUsuarioRegistro);
                String mesesAnios = "";
                if (codigoProceso.equals("PGMFA")) {
                    for (int i = 1; i < 21; i++) {
                        if (!row.get(i).isEmpty()) {
                            if (mesesAnios.equals("")) {
                                mesesAnios = i + "-0";
                            } else {
                                mesesAnios += "," + i + "-0";
                            }
                        }
                    }
                }
                if (codigoProceso.equals("POCC")) {
                    for (int i = 1; i < 15; i++) {
                        if (i < 13) {
                            if (!row.get(i).isEmpty()) {
                                if (mesesAnios.equals("")) {
                                    mesesAnios = "1-" + i;
                                } else {
                                    mesesAnios += ",1-" + i;
                                }
                            }
                        } else {
                            if (!row.get(i).isEmpty()) {
                                if (mesesAnios.equals("")) {
                                    mesesAnios = (i - 11) + "-0";
                                } else {
                                    mesesAnios += "," + (i - 11) + "-0";
                                }
                            }
                        }
                    }
                }
                if (codigoProceso.equals("POAC")) {
                    for (int i = 1; i < 13; i++) {
                        if (!row.get(i).isEmpty()) {
                            if (mesesAnios.equals("")) {
                                mesesAnios = "1-" + i;
                            } else {
                                mesesAnios += ",1-" + i;
                            }
                        }
                    }
                }
                det.setMesesAnios(mesesAnios);
                if (!mesesAnios.equals("")) {
                    repCronAct.RegistrarMarcaCronogramaActividadDetalle(det);
                }
            }
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

}
