package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueOperacionEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueOperacionService {
    ResultClassEntity RegistrarManejoBosqueOperacion(List<ManejoBosqueOperacionEntity> list) throws Exception;
    ResultClassEntity EliminarManejoBosqueOperacion(ManejoBosqueOperacionEntity manejoBosqueOperacion) throws Exception;
    ResultClassEntity ListarManejoBosqueOperacion(PlanManejoEntity planManejo) throws Exception;
}
