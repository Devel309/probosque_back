package pe.gob.serfor.mcsniffs.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ActividadAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.ActividadAprovechamientoService;

@Service
public class ActividadAprovechamientoServiceImpl implements ActividadAprovechamientoService{
    /**
     * @autor: Jaqueline DiazB [26-11-2021]
     * @modificado:
     * @descripción: {Repository actividades aprovechamiento}
     */
    @Autowired
    private ActividadAprovechamientoRepository actividadAprovechamientoService;

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {Lista Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultEntity<ActividadAprovechamientoEntity> ListarActvidadAprovechamiento(
            ActividadAprovechamientoParam request) throws Exception {
        return actividadAprovechamientoService.ListarActvidadAprovechamiento(request);
    }

    /**
     * @autor: Jason Retamozo [15-02-2022]
     * @creado:
     * @descripción: {Obtener total de Aprovechamiento no maderable}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity<CensoAprovechamientoNoMaderables> ObtenerTotalAprovechamientoNoMaderable(
            ActividadAprovechamientoParam request) throws Exception{
        ResultClassEntity<CensoAprovechamientoNoMaderables> result = new ResultClassEntity<>();
        CensoAprovechamientoNoMaderables vce = new CensoAprovechamientoNoMaderables();
        try{
            vce.setIdPlanManejo(request.getIdPlanManejo());
            vce.setCodActvAprove(request.getCodActvAprove());
            List<ActividadAprovechamientoEntity> aae= actividadAprovechamientoService.ListarActvidadAprovechamiento(request).getData();
            if(aae!=null){
                if(aae.size()>0){
                    vce.setIdActvAprove(aae.get(0).getIdActvAprove());
                    vce.setCodSubActvAprove(aae.get(0).getCodSubActvAprove());
                    vce.setTotal(BigDecimal.valueOf(0.00));
                    for(ActividadAprovechamientoDetEntity det:aae.get(0).getLstactividadDet()){
                        if(det.getCodActvAproveDet().equals("POCCAAPROCCAR")) {
                            vce.setTotal(vce.getTotal().add(BigDecimal.valueOf(Double.parseDouble(det.getVolumen().trim().equals("") ? "0.00" : det.getVolumen()))));
                        }
                    }
                    result.setMessage("Se listo la actividad aprovechamiento correctamente.");
                }else{
                    result.setMessage("No existen datos sincronizados ara el Plan de Manejo "+vce.getIdPlanManejo());
                }
            }else{
                result.setMessage("No existen datos sincronizados ara el Plan de Manejo "+vce.getIdPlanManejo());
            }
            result.setData(vce);
            result.setSuccess(true);
            return result;
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jason Retamozo [08-02-2022]
     * @creado:
     * @descripción: {Obtener total de volumen de corta en Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity<VolumenCortaEntity> ObtenerTotalVolumenCorta(
            ActividadAprovechamientoParam request) throws Exception {
        ResultClassEntity<VolumenCortaEntity> result = new ResultClassEntity<>();
        VolumenCortaEntity vce = new VolumenCortaEntity();
        try{
            vce.setIdPlanManejo(request.getIdPlanManejo());
            vce.setCodActvAprove(request.getCodActvAprove());
            List<ActividadAprovechamientoEntity> aae= actividadAprovechamientoService.ListarActvidadAprovechamiento(request).getData();
            if(aae!=null){
                if(aae.size()>1){
                    vce.setIdActvAprove(aae.get(1).getIdActvAprove());
                    vce.setCodSubActvAprove(aae.get(1).getCodSubActvAprove());
                    vce.setTotal(BigDecimal.valueOf(0.00));
                    for(ActividadAprovechamientoDetEntity det:aae.get(1).getLstactividadDet()){
                        vce.setTotal(vce.getTotal().add(BigDecimal.valueOf(Double.parseDouble(det.getVolumen().trim().equals("")?"0.00":det.getVolumen()))));
                    }
                    result.setMessage("Se listo la actividad aprovechamiento correctamente.");
                }else{
                    result.setMessage("No existen datos sincronizados ara el Plan de Manejo "+vce.getIdPlanManejo());
                }
            }else{
                result.setMessage("No existen datos sincronizados ara el Plan de Manejo "+vce.getIdPlanManejo());
            }
            result.setData(vce);
            result.setSuccess(true);
            return result;
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }

    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {registra Aprovechamiento Actividad}
     * @param: List<ActividadAprovechamientoEntity>
     */
    @Override
    public ResultClassEntity RegistrarActvidadAprovechamiento(List<ActividadAprovechamientoEntity> request)
            throws Exception {
        return actividadAprovechamientoService.RegistrarActvidadAprovechamiento(request);
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity EliminarActvidadAprovechamiento(ActividadAprovechamientoParam request) throws Exception {
        return actividadAprovechamientoService.EliminarActvidadAprovechamiento(request);
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad det}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity EliminarActvidadAprovechamientoDet(ActividadAprovechamientoParam request)
            throws Exception {
        return actividadAprovechamientoService.EliminarActvidadAprovechamientoDet(request);
    }

     /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad det sub}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity EliminarActvidadAprovechamientoDetSub(ActividadAprovechamientoParam request)
            throws Exception {
        return actividadAprovechamientoService.EliminarActvidadAprovechamientoDetSub(request);
    }

    /**
     * @autor:  Danny Nazario [08-01-2022]
     * @modificado:
     * @descripción: { Registra Censo Comercial Especies Excel }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity registrarCensoComercialEspecieExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                                 String codActvAprove, String codSubActvAprove, String codActvAproveDet,
                                                                 int idActvAprove, int idPlanManejo, int idUsuarioRegistro) {
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            File_Util fl = new File_Util();
            List<ArrayList<String>> listaExcel = fl.getExcel(file, nombreHoja, numeroFila, numeroColumna);

            List<ActividadAprovechamientoEntity> list = new ArrayList<>();
            ActividadAprovechamientoEntity cab = new ActividadAprovechamientoEntity();

            cab.setCodActvAprove(codActvAprove);
            cab.setCodSubActvAprove(codSubActvAprove);
            cab.setIdPlanManejo(idPlanManejo);
            cab.setIdUsuarioRegistro(idUsuarioRegistro);
            cab.setIdActvAprove(idActvAprove);

            List<ActividadAprovechamientoDetEntity> listDet = new ArrayList<>();
            ActividadAprovechamientoDetEntity det = new ActividadAprovechamientoDetEntity();
            for(ArrayList obj : listaExcel) {
                ArrayList<String> row = obj;
                det = new ActividadAprovechamientoDetEntity();
                det.setCodActvAproveDet(codActvAproveDet);
                det.setIdActvAprove(idActvAprove);
                det.setIdActvAproveDet(0);
                det.setNombreComun(row.get(0));
                det.setNombreCientifica(row.get(1));
                det.setFamilia(row.get(2));
                det.setLineaProduccion(row.get(3));
                det.setDcm(BigDecimal.valueOf(Double.parseDouble(row.get(4))));
                det.setIdUsuarioRegistro(idUsuarioRegistro);
                listDet.add(det);
            }
            cab.setLstactividadDet(listDet);
            list.add(cab);
            actividadAprovechamientoService.RegistrarActvidadAprovechamiento(list);
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error");
            result.setMessageExeption(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

}
