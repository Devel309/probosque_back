package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.OrganizacionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.util.List;

public interface OrganizacionActividadService {
    ResultClassEntity RegistrarOrganizacionActividad(List<OrganizacionActividadEntity> list)throws Exception;
    ResultClassEntity ListarPorFiltroOrganizacionActividad(OrganizacionActividadEntity param)throws Exception;
    ResultClassEntity EliminarOrganizacionActividad(OrganizacionActividadEntity param)throws Exception;
    ResultClassEntity RegistrarOrganizacionActividadArchivo(MultipartFile file, PlanManejoEntity planManejo);
    ResultClassEntity ListarOrganizacionActividadArchivo(PlanManejoEntity planManejo);

    ResultEntity ListarPorFiltroOrganizacionActividadArchivo(PlanManejoEntity request) ;
}
