package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.Anexo1PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo2PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo3PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo4PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo5PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo6PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.PostulacionPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.PostulacionPFDMRepository;
import pe.gob.serfor.mcsniffs.service.PostulacionPFDMService;

@Service
public class PostulacionPFDMServiceImpl implements PostulacionPFDMService{
     /**
     * @autor: JaquelineDB [06-08-2021]
     * @modificado:
     * @descripción: {Repository de las postulacion pfdme}
     *
     */
    @Autowired
    PostulacionPFDMRepository repository;

     /**
     * @autor: JaquelineDB [06-08-2021]
     * @modificado:
     * @descripción: {Guardar postulacion}
     * @param:PostulacionPFDMEResquestEntity
     */
    @Override
    public ResultClassEntity guardaPostulacion(PostulacionPFDMResquestEntity postulacion) {
        return repository.guardaPostulacion(postulacion);
    }

     /**
     * @autor: JaquelineDB [09-08-2021]
     * @modificado:
     * @descripción: {Obtener anexo 1 pfdm}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo1PFDMEntity> obtenerAnexo1PFDM(AnexosPFDMResquestEntity filtro) {
       return repository.obtenerAnexo1PFDM(filtro);
    }

    /**
     * @autor: JaquelineDB [10-08-2021]
     * @modificado:
     * @descripción: {se adjuntan los archivos relacionados con la postulacion pfdm}
     * @param:ContratoAdjuntosRequestEntity
     */
    @Override
    public ResultClassEntity AdjuntarArchivosPostulacionPFDM(AnexosPFDMAdjuntosEntity obj) {
        return repository.AdjuntarArchivosPostulacionPFDM(obj);
    }

    /**
     * @autor: JaquelineDB [12-08-2021]
     * @modificado:
     * @descripción: {se registran los datos del anexo 2}
     * @param:Anexo2PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo2(Anexo2PFDMEntity anexo) {
        return repository.guardarAnexo2(anexo);
    }

    /**
     * @autor: JaquelineDB [13-08-2021]
     * @modificado:
     * @descripción: {se registran los datos del anexo 3}
     * @param:Anexo3PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo3(Anexo3PFDMEntity anexo) {
        return repository.guardarAnexo3(anexo);
    }

    /**
     * @autor: JaquelineDB [16-08-2021]
     * @modificado:
     * @descripción: {se obtienen los datos del anexo 2}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo2PFDMEntity> obtenerAnexo2PFDM(AnexosPFDMResquestEntity filtro) {
        return repository.obtenerAnexo2PFDM(filtro);
    }

     /**
     * @autor: JaquelineDB [16-08-2021]
     * @modificado:
     * @descripción: {se obtienen los datos del anexo 3}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo3PFDMEntity> obtenerAnexo3PFDM(AnexosPFDMResquestEntity filtro) {
        return repository.obtenerAnexo3PFDM(filtro);
    }

    /**
     * @autor: JaquelineDB [17-08-2021]
     * @modificado:
     * @descripción: {guardar anexo 4}
     * @param:Anexo4PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo4(Anexo4PFDMEntity anexo) {
        return repository.guardarAnexo4(anexo);
    }

    /**
     * @autor: JaquelineDB [17-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 4}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo4PFDMEntity> obtenerAnexo4PFDM(AnexosPFDMResquestEntity filtro) {
         return repository.obtenerAnexo4PFDM(filtro);
    }

    /**
     * @autor: JaquelineDB [18-08-2021]
     * @modificado:
     * @descripción: {guardas los datos del anexo 5}
     * @param:Anexo5PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo5(Anexo5PFDMEntity anexo) {
        return repository.guardarAnexo5(anexo);
    }

    /**
     * @autor: JaquelineDB [18-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 5}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo5PFDMEntity> obtenerAnexo5PFDM(AnexosPFDMResquestEntity filtro) {
        return repository.obtenerAnexo5PFDM(filtro);
    }

    /**
     * @autor: JaquelineDB [19-08-2021]
     * @modificado:
     * @descripción: {guardar datos del anexo 6}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity guardarAnexo6(Anexo6PFDMEntity anexo) {
        return repository.guardarAnexo6(anexo);
    }

     /**
     * @autor: JaquelineDB [19-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 6}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo6PFDMEntity> obtenerAnexo6PFDM(AnexosPFDMResquestEntity filtro) {
        return repository.obtenerAnexo6PFDM(filtro);
    }

}
