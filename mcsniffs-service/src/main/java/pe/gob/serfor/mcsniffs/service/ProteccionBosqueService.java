package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;

import java.util.List;

public interface ProteccionBosqueService {
    ResultClassEntity ConfiguracionProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity param) throws Exception ;
    ResultClassEntity RegistrarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity> param) throws Exception ;
    ResultClassEntity ActualizarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity>  param) throws Exception ;
    ResultClassEntity ObtenerProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity param) throws Exception ;
    ResultClassEntity RegistrarProteccionBosqueAmbiental(List<ProteccionBosqueGestionAmbientalEntity> list) throws Exception ;
    ResultClassEntity ListarPorFiltroProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param)throws Exception;
    ResultClassEntity EliminarProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param)throws Exception;
    ResultClassEntity RegistrarProteccionBosqueImpactoAmbiental(List<ProteccionBosqueImpactoAmbientalEntity> list)throws Exception;
    ResultClassEntity ListarPorFiltroProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param)throws Exception;
    ResultClassEntity EliminarProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param)throws Exception;

    /**
     * @autor:  Rafael Azaña [15-10-2021]
     * @descripción: {CRUD ProteccionBosque}
     * @param: ProteccionBosqueEntity
     * @return: ResponseEntity<ResponseVO>
     */

    ResultClassEntity RegistrarProteccionBosque(List<ProteccionBosqueEntity> list) throws Exception;

    ResultClassEntity<List<ProteccionBosqueDetalleDto>> listarProteccionBosque(Integer idPlanManejo, String codPlanGeneral, String subCodPlanGeneral) throws Exception;
    ResultClassEntity EliminarProteccionBosque(ProteccionBosqueDetalleDto proteccionBosqueDetalleDto) throws Exception;

    ResultClassEntity registrarProteccionBosqueExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna,
                                                        String codPlanGeneral,String subCodPlanGeneral,String codPlanGeneralDet,String subCodPlanGeneralDet,
                                                        int idPlanManejo, int idUsuarioRegistro);

}
