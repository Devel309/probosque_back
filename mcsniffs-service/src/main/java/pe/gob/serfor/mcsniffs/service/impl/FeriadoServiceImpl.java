package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.FeriadoRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.service.FeriadoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service("FeriadoService")
public class FeriadoServiceImpl implements FeriadoService {

    @Autowired
    private FeriadoRepository feriadoRepository;

    @Override
    public ResultClassEntity<List<FeriadoEntity>> ListarFeriados(String codUbigeo)
            throws Exception {

        ResultClassEntity<List<FeriadoEntity>> result = new ResultClassEntity<>();

        result.setData(feriadoRepository.ListarFeriados(codUbigeo));
        result.setSuccess(true);
        result.setMessage("Lista Censo Forestal Detalle");

        return result;
    }

    @Override
    public ResultClassEntity RegistrarFeriados(FeriadoEntity param) throws Exception {
        return feriadoRepository.RegistrarFeriados(param);

    }

}
