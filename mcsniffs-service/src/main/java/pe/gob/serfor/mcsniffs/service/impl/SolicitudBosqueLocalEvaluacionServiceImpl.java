package pe.gob.serfor.mcsniffs.service.impl;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.DescargarRespuestaEvaluacionComiteDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEvaluacionRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalEvaluacionService;
import pe.gob.serfor.mcsniffs.service.util.DocUtilEvaluacionComite;

@Service("SolicitudBosqueLocalEvaluacionService")
public class SolicitudBosqueLocalEvaluacionServiceImpl implements SolicitudBosqueLocalEvaluacionService {

    @Autowired
    private SolicitudBosqueLocalEvaluacionRepository solicitudBosqueLocalEvaluacionRepository;

    @Autowired
    private SolicitudBosqueLocalEvaluacionDetalleRepository solicitudBosqueLocalEvaluacionDetalleRepository;

    @Autowired
    private SolicitudBosqueLocalArchivoRepository solicitudBosqueLocalArchivoRepository;


    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalEvaluacion(List<SolicitudBosqueLocalEvaluacionDto> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        for (SolicitudBosqueLocalEvaluacionDto e : param) {
            result = solicitudBosqueLocalEvaluacionRepository.registrarSolicitudBosqueLocalEvaluacion(e);
            
        }
        
        return result;
    }

    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto param) throws Exception {
        return  solicitudBosqueLocalEvaluacionRepository.eliminarSolicitudBosqueLocalEvaluacion(param);
    }

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        result.setData(solicitudBosqueLocalEvaluacionRepository.listarSolicitudBosqueLocalEvaluacion(param));

        return result;
    }

    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocalEvaluacionDetalle(List<SolicitudBosqueLocalEvaluacionDetalleDto> listObj) throws Exception {
        return  solicitudBosqueLocalEvaluacionDetalleRepository.actualizarSolicitudBosqueLocalEvaluacionDetalle(listObj);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarSolicitudBosqueLocalEvaluacionDetalle(List<SolicitudBosqueLocalEvaluacionDetalleDto> items) throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        if (items != null && !items.isEmpty()) {
            for (SolicitudBosqueLocalEvaluacionDetalleDto item : items) {
                result = solicitudBosqueLocalEvaluacionDetalleRepository.registrarSolicitudBosqueLocalEvaluacionDetalle(item);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("No existen detalles de la evaluación por registrar.");
        }

        return result;
    }

  
    @Override
    public ResultClassEntity listarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj) throws Exception {
        return  solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(obj);
    }

    @Override
    public ResultArchivoEntity descargarRespuestaEvaluacionComite(DescargarRespuestaEvaluacionComiteDto obj) throws Exception {

        XWPFDocument doc = new XWPFDocument();

        SolicitudBosqueLocalEvaluacionDetalleDto param = new SolicitudBosqueLocalEvaluacionDetalleDto();
        param.setIdSolBosqueLocal(obj.getIdSolBosqueLocal());
        param.setCodigoSeccion("SOLBL3");
        
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_1 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_2 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_3 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_4 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_5 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_6 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_7 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_8 = new ArrayList<>();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> tab_9 = new ArrayList<>();

        // DATA GABINETE
        param.setCodigoSubSeccion("SOLBL3_SS1");
        ResultClassEntity result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_1 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS2");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_2 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS3");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_3 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS4");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_4 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS5");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_5 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS6");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_6 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS7");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_7 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS8");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_8 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();
        param.setCodigoSubSeccion("SOLBL3_SS9");
        result = solicitudBosqueLocalEvaluacionDetalleRepository.listarSolicitudBosqueLocalEvaluacionDetalle(param);
        if(result!=null) tab_9 = (List<SolicitudBosqueLocalEvaluacionDetalleDto>) result.getData();

        // DATA CAMPO
        SolicitudBosqueLocalArchivoEntity parametro = new SolicitudBosqueLocalArchivoEntity();
        parametro.setIdSolBosqueLocal(obj.getIdSolBosqueLocal());
        parametro.setCodigoSeccion("SOLBL4");
        parametro.setCodigoSubSeccion("SOLBL4_SS1");
        ResultClassEntity result2 = solicitudBosqueLocalArchivoRepository.listarSolicitudBosqueLocalArchivo(parametro);
        List<SolicitudBosqueLocalArchivoEntity> lista = new ArrayList<>();
        if(result2!=null && result2.getData()!=null) lista = (List<SolicitudBosqueLocalArchivoEntity>) result2.getData() ;
        
        if(obj.getEvalGabineteObservado() && obj.getEvalCampoObservado()){
            doc = getDoc("PropuestaDeInforme.docx");
            // EVA. GABINETE
            SolicitudBosqueLocalEvaluacionDetalleDto aux = new SolicitudBosqueLocalEvaluacionDetalleDto();
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_1.size()==0?aux:tab_1.get(0),doc,0);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_2.size()==0?aux:tab_2.get(0),doc,1);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_3.size()==0?aux:tab_3.get(0),doc,2);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_4.size()==0?aux:tab_4.get(0),doc,3);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_5.size()==0?aux:tab_5.get(0),doc,4);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_6.size()==0?aux:tab_6.get(0),doc,5);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_7.size()==0?aux:tab_7.get(0),doc,6);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_8.size()==0?aux:tab_8.get(0),doc,7);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_9.size()==0?aux:tab_9.get(0),doc,8);
            // EVA. CAMPO
            DocUtilEvaluacionComite.setearResultadoObtenidoCampo(lista,doc,9);
        }
        if(obj.getEvalGabineteObservado() && !obj.getEvalCampoObservado()){
            doc = getDoc("PropuestaDeInforme_1.docx");
            // EVA. GABINETE
            SolicitudBosqueLocalEvaluacionDetalleDto aux = new SolicitudBosqueLocalEvaluacionDetalleDto();
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_1.size()==0?aux:tab_1.get(0),doc,0);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_2.size()==0?aux:tab_2.get(0),doc,1);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_3.size()==0?aux:tab_3.get(0),doc,2);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_4.size()==0?aux:tab_4.get(0),doc,3);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_5.size()==0?aux:tab_5.get(0),doc,4);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_6.size()==0?aux:tab_6.get(0),doc,5);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_7.size()==0?aux:tab_7.get(0),doc,6);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_8.size()==0?aux:tab_8.get(0),doc,7);
            DocUtilEvaluacionComite.setearResultadoObtenidoGabinete(tab_9.size()==0?aux:tab_9.get(0),doc,8);
        }

        if(!obj.getEvalGabineteObservado() && obj.getEvalCampoObservado()){
            doc = getDoc("PropuestaDeInforme_2.docx");
            // EVA. CAMPO
            DocUtilEvaluacionComite.setearResultadoObtenidoCampo(lista,doc,0);
        }
        
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("PropuestaDeInforme.docx");
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());
        word.setSuccess(true);

        return word;
    }

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }
    
}
