package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.centroTansformacion.CentroTransformacionDto;
import pe.gob.serfor.mcsniffs.repository.CentroTransformacionRepository;
import pe.gob.serfor.mcsniffs.service.CentroTransformacionService;

@Service
public class CentroTransformacionServiceImpl implements CentroTransformacionService{
   
    @Autowired 
    CentroTransformacionRepository repo;

    @Override
    public ResultClassEntity ListarCentroTransformacion(CentroTransformacionDto obj) throws Exception {
        return repo.ListarCentroTransformacion(obj);
    }
}
