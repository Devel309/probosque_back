package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEstudioTecnicoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEstudioTecnicoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalEstudioTecnicoService;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalService;

@Service("SolicitudBosqueLocalEstudioTecnicoService")
public class SolicitudBosqueLocalEstudioTecnicoServiceImpl implements SolicitudBosqueLocalEstudioTecnicoService {

    @Autowired
    private SolicitudBosqueLocalEstudioTecnicoRepository solicitudBosqueLocalEstudioTecnicoRepository;

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj) throws Exception{
        return solicitudBosqueLocalEstudioTecnicoRepository.listarSolicitudBosqueLocalEstudioTecnico(obj);
    }

    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj) throws Exception{
        return solicitudBosqueLocalEstudioTecnicoRepository.actualizarSolicitudBosqueLocalEstudioTecnico(obj);
    }


}
