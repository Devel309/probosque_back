package pe.gob.serfor.mcsniffs.service;


import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.TipoParametroDto;


public interface TipoParametroService {
        ResultClassEntity listarTipoParametro(TipoParametroDto obj) throws Exception;

        ResultClassEntity actualizarParametro(TipoParametroDto obj) throws Exception;




}
