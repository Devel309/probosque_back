package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueOperacionEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueOperacionRepository;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueOperacionService;

import java.util.List;

/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad ManejoBosqueOperacion  de la base de datos mcsniffs}
 *
 */
@Service("ManejoBosqueOperacionService")
public class ManejoBosqueOperacionServiceImpl implements ManejoBosqueOperacionService {


    private static final Logger log = LogManager.getLogger(ManejoBosqueOperacionServiceImpl.class);

    @Autowired
    private ManejoBosqueOperacionRepository manejoBosqueOperacionRepository;

    @Override
    public ResultClassEntity RegistrarManejoBosqueOperacion(List<ManejoBosqueOperacionEntity> list) throws Exception {
        return manejoBosqueOperacionRepository.RegistrarManejoBosqueOperacion(list);
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueOperacion(ManejoBosqueOperacionEntity manejoBosqueOperacion) throws Exception {
        return manejoBosqueOperacionRepository.EliminarManejoBosqueOperacion(manejoBosqueOperacion);
    }

    @Override
    public ResultClassEntity ListarManejoBosqueOperacion(PlanManejoEntity planManejo) throws Exception {
        return manejoBosqueOperacionRepository.ListarManejoBosqueOperacion(planManejo);
    }
}

