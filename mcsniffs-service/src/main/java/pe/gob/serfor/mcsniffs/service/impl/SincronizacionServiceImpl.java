package pe.gob.serfor.mcsniffs.service.impl;

import java.io.Reader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.SincronizacionRepository;
import pe.gob.serfor.mcsniffs.repository.TalaRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.service.SincronizacionService;

@Service("serviceSincronizacion")
public class SincronizacionServiceImpl implements SincronizacionService {

    @Autowired
    ArchivoRepository repositoryArchivo;

    @Autowired
    TalaRepository repositoryTala;
 
    @Autowired
    private FileServerConexion fileConexion;

    @Autowired
    private SincronizacionRepository repositorySincronizacion;

     /**
     * @autor: Abner Valdez [25-04-2022]
     * @modificado:
     * @descripción: {Sincronizar}
     * @param: file
     */
    @Override
    public ResultClassEntity sincronizar(MultipartFile file) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        String nombreGenerado = fileConexion.uploadFileDBSqlite(file);
        String path = fileConexion.loadFileAsResourceDbSqlite(nombreGenerado);//"C://Gerardo//Consultorias//valtx//mcsniffsDesktop//SqliteJavaDB.db";

        List<TalaEntity> items = this.repositorySincronizacion.listarTala(path+".db");
        items.forEach((item) -> {
            try {
                this.repositoryTala.registrarTala(item);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setMessage("Ocurrió un error");
                result.setMessageExeption(e.getMessage());
                e.printStackTrace();
            }
        });
        result.setMessage("Se sincronizaron correctamente los registros.");
        result.setSuccess(true);
        return result;

    }
}
