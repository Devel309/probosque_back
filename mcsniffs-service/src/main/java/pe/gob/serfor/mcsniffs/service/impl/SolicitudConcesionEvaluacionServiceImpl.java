package pe.gob.serfor.mcsniffs.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionAreaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionEvaluacionService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;

@Service("SolicitudConcesionEvaluacionService")
public class SolicitudConcesionEvaluacionServiceImpl implements SolicitudConcesionEvaluacionService {

    @Autowired
    private SolicitudConcesionEvaluacionRepository solicitudConcesionEvaluacionRepository;

    @Override
    public ResultClassEntity listarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj)
            throws Exception {
        return solicitudConcesionEvaluacionRepository.listarSolicitudConcesionEvaluacion(obj);
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj)
            throws Exception {
        return solicitudConcesionEvaluacionRepository.actualizarSolicitudConcesionEvaluacion(obj);
    }

    @Override
    public ResultClassEntity listarSolicitudConcesionEvaluacionDetalle(SolicitudConcesionEvaluacionDto obj)
            throws Exception {
        return solicitudConcesionEvaluacionRepository.listarSolicitudConcesionEvaluacionDetalle(obj);
    }

    @Override
    public ResultArchivoEntity descargarSolicitudConcesionEvaluacion(DescargarSolicitudConcesionEvaluacionDto obj)
            throws Exception {

        XWPFDocument doc = getDoc("PlantillaResultadoEvaluacion.doc.docx");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        doc.write(b);
        doc.close();

        ResultArchivoEntity word = new ResultArchivoEntity();
        word.setNombeArchivo("PlantillaResultadoEvaluacion.doc.docx");
        word.setTipoDocumento("EVALUACION");
        word.setArchivo(b.toByteArray());

        return word;
    }

    private XWPFDocument getDoc(String nameFile) throws NullPointerException, IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream(nameFile);
        return new XWPFDocument(file);
    }

}
