package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;
import pe.gob.serfor.mcsniffs.repository.FuenteRepository;
import pe.gob.serfor.mcsniffs.repository.SeguridadRepository;
import pe.gob.serfor.mcsniffs.service.SeguridadService;

import java.util.ArrayList;

@Service
public class SeguridadServiceImpl implements SeguridadService,UserDetailsService {

	@Autowired
	private SeguridadRepository seguridadDao;

	@Override
	public SeguridadEntity LoginUser(SeguridadEntity obj_seguridad) {
		return seguridadDao.LoginUser(obj_seguridad);
	}

	@Override
	public UserDetails UserByUsername(String username) throws UsernameNotFoundException {
		SeguridadEntity usuariologueado = seguridadDao.UserByUsername(username);
		return new User(usuariologueado.getUsername(), usuariologueado.getPassword(), new ArrayList<>());
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SeguridadEntity usuariologueado = seguridadDao.UserByUsername(username);
        return new User(usuariologueado.getUsername(),usuariologueado.getPassword(),new ArrayList<>());
	}

}
