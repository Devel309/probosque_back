package pe.gob.serfor.mcsniffs.service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import com.lowagie.text.DocumentException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContrato;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContratoDetalle;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface PlanManejoService {

    ResultClassEntity<PlanManejoEntity> ListarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    ResultClassEntity RegistrarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    ResultClassEntity ActualizarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    ResultClassEntity EliminarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    Pageable<List<PlanManejoEntity>> filtrar(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad, String nombreTitular, String codigoEstado,
            Page page) throws Exception;

    // Jaqueline DB
    ResultClassEntity registrarEstadoPlanManejo(List<PlanManejoEstadoEntity> obj);

    ResultClassEntity actualizarEstadoPlanManejo(PlanManejoEstadoEntity obj);

    ResultEntity<PlanManejoEstadoEntity> listarEstadoPlanManejo(PlanManejoEstadoEntity filtro);

    ByteArrayResource consolidado(Integer idPlanManejo) throws Exception;

    ResultClassEntity ListarPorFiltroPlanManejoArchivo(PlanManejoArchivoEntity request);

    ResultClassEntity regitrarArchivosPlanManejo(Integer idUsuarioRegistro, Integer idPlanManejo,
            String idTipoDocumento, MultipartFile[] attachments) throws Exception;

    ResultClassEntity registrarArchivoPlanManejo(Integer idPlanManejoArchivo, Integer idUsuarioRegistro,
            Integer idPlanManejo, String codTipo, String codTipoDocumento, String subCodTipoDocumento,
            MultipartFile file) throws Exception;

    ByteArrayResource consolidadoPMFI(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoPGMF(Integer idPGMF) throws Exception;

    ByteArrayResource consolidadoPGMFA(Integer idPlanManejo, String extension) throws Exception;

    ByteArrayResource consolidadoPOCC(Integer idPlanManejo, String extension) throws Exception;

    ByteArrayResource consolidadoPOAC(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoPOPAC(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoPFCR(Integer idPermisoForestal) throws Exception;

    ByteArrayResource generarEvaluacionCompiladaPGMF(Integer idPlanManejo, Integer idPlanManejoEvaluacion,
            String nombreUsuarioArffs, String tipoDocumento) throws Exception;

    ResultClassEntity obtenerArchivosCompiladosRelacionados(CompiladoPgmfDto compiladoPgmfDto) throws Exception;

    ResultClassEntity eliminarPlanManejoArchivo(PlanManejoArchivoDto request) throws Exception;

    ResultClassEntity obtenerPlanManejoArchivo(PlanManejoArchivoDto request) throws Exception;

    ResultClassEntity actualizarPlanManejoArchivo(PlanManejoArchivoDto request) throws Exception;

    ResultEntity<PlanManejoEvaluacionIterDto> listarPlanManejoEvaluacionIter(
            PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto);

    ResultClassEntity registrarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto)
            throws Exception;

    ResultClassEntity actualizarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto)
            throws Exception;

    ResultClassEntity eliminarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto)
            throws Exception;

    ResultClassEntity cambiarEstadoPlanManejo(Integer idPlanManejo, Integer idUsuario) throws Exception;

    ResultClassEntity<PlanManejoObtenerContrato> listarPlanManejoContrato(Integer idPlanManejo) throws Exception;

    ResultClassEntity actualizarConformePlanManejoArchivo(Integer idPlanManejo, Integer idUsuarioModificacion)
            throws Exception;

    ResultClassEntity guardarArchivoPlanManejoArchivo(PlanManejoArchivoDto dto, MultipartFile file) throws Exception;

    ResultClassEntity ObtenerPlanManejo(PlanManejoDto plan) throws Exception;

    ResultClassEntity actualizarPlanManejoEstado(PlanManejoDto request) throws Exception;

    ResultClassEntity<List<PlanManejoObtenerContratoDetalle>> obtenerPlanManejoDetalleContrato(
            PlanManejoArchivoDto filtro);

    ResultClassEntity<List<PlanManejoArchivoDto>> listarPlanManejoListar(Integer idPlanManejo, Integer idArchivo,
            String tipoDocumento, String codigoProceso, String extension) throws Exception;

    ResultClassEntity<List<PlanManejoArchivoDto>> PlanManejoArchivoEntidad(Integer idPlanManejoArchivo)
            throws Exception;

    ResultClassEntity<PlanManejoArchivoEntity> listarPlanManejoAnexoArchivo(PlanManejoArchivoEntity filtro)
            throws Exception;

    ResultClassEntity registrarPlanManejoContrato(PlanManejoContratoEntity obj) throws Exception;

    ResultClassEntity obtenerPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception;

    ResultClassEntity listarPorFiltroPlanManejoContrato(String nroDocumento, String codTipoPlan) throws Exception;

    ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo, String tipoPlan, String idTipoBosque);

    ResultClassEntity<PlanManejoDto> DatosGeneralesPlan(Integer idPlanManejo) throws Exception;

    ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo, String tipoPlan);

    ByteArrayResource consolidadoPMFIC(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoAnexosPMFIC(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoAnexosPOPAC(Integer idPlanManejo) throws Exception;

    List<RegistroTabEntity> listarTabsRegistrados(RegistroTabEntity param) throws Exception;

    ByteArrayResource descargarFormatoPlantacionesForestales() throws Exception;

    ByteArrayResource descargarPlantillaFormatoFiscalizacion2() throws Exception;

    ByteArrayResource consolidadoPOAC_Resumido(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoPMFIC_Resumido(Integer idPlanManejo) throws Exception;

    ByteArrayResource consolidadoPOPAC_Resumido(Integer idPlanManejo) throws Exception;

    ResultEntity<PlanManejoInformacionEntity> obtenerInformacionPlan(Integer idPlanManejo, String tipoPlan)
            throws Exception;

    ResultArchivoEntity plantillaInformeGeneral(Integer idPlanManejo) throws Exception;

    List<DivisionAdministrativaEntity> listarDivisionAdministrativa(DivisionAdministrativaEntity param)
            throws Exception;

    ResultEntity<PlanManejoEntity> obtenerPlanesManejoTitular(String nroDocumento, String tipoDocumento)
            throws Exception;

}
