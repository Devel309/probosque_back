package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Colindancia.ColindanciaPredioDto;

public interface ColindanciaPredioService {
    
    ResultClassEntity registrarColindanciaPredio(List<ColindanciaPredioDto> dto) throws Exception;
    ResultClassEntity<List<ColindanciaPredioDto>> listarColindanciaPredio(ColindanciaPredioDto dto) throws Exception;
}
