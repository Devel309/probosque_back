package pe.gob.serfor.mcsniffs.service.constant;

/**
 *
 * @author Jason Retamozo / 22-03-2022
 */
public final class InformeEvaluacionType {
    private InformeEvaluacionType(){}

    public static final String TITULO = "1titulo";

    public static final String DIRIGIDOA = "1dirigidoa";
    public static final String CARGODIR = "1cargodir";
    public static final String ELABORADOPOR = "1elaboradopor";
    public static final String CARGOELA = "1cargoela";
    public static final String TIPO_CONCESION = "[tipoConcesion]";
    public static final String REFERENCIA = "1referencia";
    public static final String LUGAR_FECHA = "1lugarFecha ";

    public static final String NOMBRES_EVALUADOR = "[nombresEval]";
    public static final String CARGO_EVALUADOR = "[cargoEval]";
    public static final String NOMBRES_ARFFS = "[nombreArffs]";

}
