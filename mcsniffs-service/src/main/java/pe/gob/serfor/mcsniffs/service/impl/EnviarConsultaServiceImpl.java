package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.EnviarConsultaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.EnviarConsultaRepository;
import pe.gob.serfor.mcsniffs.service.EnviarConsultaService;

@Service
public class EnviarConsultaServiceImpl implements EnviarConsultaService {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {repositorio de la tabla Enviar consulta}
     *
     */
    @Autowired
    EnviarConsultaRepository repository;

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Envia la consulta a una entidad}
     * @param:
     */

    @Override
    public ResultClassEntity EnviarConsulta(EnviarConsultaEntity obj) {
        return repository.EnviarConsulta(obj);
    }
}
