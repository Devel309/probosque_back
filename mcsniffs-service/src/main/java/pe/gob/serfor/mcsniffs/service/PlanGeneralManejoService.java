package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface PlanGeneralManejoService {
     ResultClassEntity RegistrarResumenEjecutivoEscalaAlta(InformacionGeneralEntity param, MultipartFile file,Boolean accion) throws Exception;
     ResultClassEntity ActualizarResumenEjecutivoEscalaAlta(InformacionGeneralEntity param, MultipartFile file,Boolean accion) throws Exception;
     ResultClassEntity ObtenerResumenEjecutivoEscalaAlta(InformacionGeneralEntity param) throws Exception;
     ResultClassEntity ActualizarDuracionResumenEjecutivo(InformacionGeneralEntity param) throws Exception;
     ResultClassEntity ActualizarAspectoComplementarioPlanManejo(PlanManejoEntity param)throws Exception;
     ResultClassEntity ObtenerPlanManejo(PlanManejoEntity param)throws Exception;
}
