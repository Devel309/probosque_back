package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.EvaluacionRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoNotalListEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPNotasEntity;

public interface ResultadoPPService {
    ResultEntity<ResultadoPPEntity> ListarResultadosEvaluaciones(ResultadoPPEntity filtro);
    ResultClassEntity RegistrarResultados(ResultadoPPEntity obj);
    ResultClassEntity<ResultadoPPAdjuntoEntity> ObtenerResultados(Integer IdResultadoPP);
    ResultClassEntity RegistrarNotas(ResultadoPPNotasEntity notas);
    ResultEntity<ResultadoNotalListEntity> ObtenerNotas(Integer IdResultadoPP);
    ResultClassEntity<ProcesoPostulacionEntity> ObtenerPrimerPP(Integer IdProcesoOferta);
    ResultClassEntity ListarPPEvaluacion(EvaluacionRequestEntity filtro);
    ResultClassEntity<ProcesoOfertaEntity> ListarProcesosOferta();
    ResultClassEntity obtenerResultadoPPPorIdPP (Integer idProcesoPostulacion);
    ResultClassEntity listarPorFiltroOtrogamientoProcesoPostulacion(EvaluacionRequestEntity filtro);
}
