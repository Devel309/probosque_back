package pe.gob.serfor.mcsniffs.service.impl;

import com.google.gson.Gson;

import com.google.gson.internal.LinkedTreeMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudAccesoRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.SolicitudAccesoService;

import java.util.List;

import pe.gob.serfor.mcsniffs.service.ServicioExternoService;

@Service("SolicitudAccesoService")
public class SolicitudAccesoServiceImpl implements SolicitudAccesoService {
    private static final Logger log = LogManager.getLogger(SolicitudAccesoServiceImpl.class);

    @Autowired
    private SolicitudAccesoRepository solicitudAccesoRepository;

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private EmailService emailService;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para consultar Solicitudes de Acceso}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: List<SolicitudAccesoEntity>
     */
    @Override
    public List<SolicitudAccesoEntity> ListarSolicitudAcceso(SolicitudAccesoEntity param) throws Exception {
        return solicitudAccesoRepository.ListarSolicitudAcceso(param);
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para obtener datos de una Solicitud de Acceso específica}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: SolicitudAccesoEntity
     */
    @Override
    public SolicitudAccesoEntity ObtenerSolicitudAcceso(SolicitudAccesoEntity param) throws Exception {
        return solicitudAccesoRepository.ObtenerSolicitudAcceso(param);
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para registrar Solicitudes de Acceso}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: ResultClassEntity
     */
    @Override
    public ResultClassEntity RegistrarSolicitudAcceso(SolicitudAccesoEntity param) throws Exception {
        return solicitudAccesoRepository.RegistrarSolicitudAcceso(param);
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para actualizar Solicitudes de Acceso cambiando su estado y datos de sus revisión.}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: ResultClassEntity
     */
    @Override
    public ResultClassEntity ActualizarRevisionSolicitudAcceso(SolicitudAccesoEntity param) throws Exception {

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUDACCESO,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUDACCESO + param.getIdSolicitudAcceso(),
                param.getIdUsuarioRevision());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);



        return solicitudAccesoRepository.ActualizarRevisionSolicitudAcceso(param);
    }

    @Override
    public boolean envioEmail(SolicitudAccesoEntity entity) {
        boolean EnvioCorreo;
        EmailEntity emailModel = new EmailEntity();
        StringBuilder builder = new StringBuilder();
        builder.append("<p>Estimado <strong>"+entity.getNombres().concat(" ").concat(entity.getApellidoPaterno()).concat(" ").concat(entity.getApellidoMaterno())+"</strong>, su ");
        builder.append("solicitud de acceso fue aprobada puede acceder al");
        builder.append("sistema con las siguientes credenciales:<br />");
        builder.append("<strong>Usuario</strong>: "+entity.getEmail()+"<br />");
        builder.append("<strong>Contrase&ntilde;a</strong>: "+entity.getNumeroDocumento()+"</p>");
        emailModel.setContent(builder.toString());
        String[] mails = new String[1];
        mails[0]=entity.getEmail().trim();
        emailModel.setEmail(mails);
        emailModel.setSubject("Mensaje de Aprobación");
        EnvioCorreo  = emailService.sendEmail(emailModel);
        return EnvioCorreo;
    }

    @Override
    public ResultClassEntity listarSolicitudAccesoUsuario(SolicitudAccesoEntity solicitudAcceso) {
        return solicitudAccesoRepository.listarSolicitudAccesoUsuario(solicitudAcceso);
    }

    @Override
    public boolean enviarEmailRegistroSolicitudAcceso(SolicitudAccesoEntity solicitudAcceso, String token) {
        boolean EnvioCorreo = false;
        Gson gson = new Gson();

        UsuarioDto user= new UsuarioDto();
        user.setIdPerfil(solicitudAcceso.getIdPerfil());
        user.setCodigoAplicacion(solicitudAcceso.getCodigoAplicacion());

        ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad +"usuario/listarUsuarioPorPerfil", gson.toJson(user), token);
        ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
        Integer cont=0;

        if(resultApiExt.getData() != null && !resultApiExt.getData().isEmpty()){
            String[] mails = new String[resultApiExt.getData().stream().toArray().length];
            for (Object item : resultApiExt.getData().stream().toArray()) {
                LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
                mails[cont] = val.get("correoElectronico").toString();
                cont++;
            }

            EmailEntity emailModel = new EmailEntity();

            String body="<html><head>"
                    +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                    +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                    +"</head>"
                    +"<body lang=ES-PE style='word-wrap:break-word'>"
                    +"<div class=WordSection1>"
                    +"<p class=MsoNormal><span lang=ES>Estimado Administrador:</span></p>"
                    +"<p class=MsoNormal><span lang=ES>Se le informa que es necesaria su aprobación de la siguiente solicitud de acceso:</span></p>"
                    +"<p class=MsoNormal><span lang=ES>Código solicitud acceso: </span><span lang=ES><strong>" + solicitudAcceso.getIdSolicitudAcceso().toString() + "</strong></span></p>"
                    +"<p class=MsoNormal><span lang=ES>Solicitante: </span><span lang=ES><strong>" + solicitudAcceso.getNombres().concat(" ").concat(solicitudAcceso.getApellidoPaterno()).concat(" ").concat(solicitudAcceso.getApellidoMaterno()) + "</strong></span></p>"
                    +"<p class=MsoNormal><span lang=ES>Número documento: </span><span lang=ES><strong>" + solicitudAcceso.getNumeroDocumento() + "</strong></span></p>"
                    +"<p class=MsoNormal><span lang=ES>Agradecemos realizar la aprobación en el sistema MCSNIFFS.</span></p>"
                    +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> No responder este correo automático.</i></p>"
                    +"</div></body></html>";

            emailModel.setContent(body);
            emailModel.setEmail(mails);
            emailModel.setSubject("Solicitud de Acceso pendiente por aprobar");
            EnvioCorreo  = emailService.sendEmail(emailModel);

        }

        return EnvioCorreo;

    }
}
