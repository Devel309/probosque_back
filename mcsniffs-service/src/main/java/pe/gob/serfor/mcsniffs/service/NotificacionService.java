package pe.gob.serfor.mcsniffs.service;


import java.util.List;

import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;

public interface NotificacionService {


    ResultClassEntity registrarNotificacion(NotificacionEntity ObjetivoManejo) throws Exception;
    List<NotificacionEntity> listarNotificacion(NotificacionEntity param) throws Exception;
    ResultClassEntity EliminarNotificacion(NotificacionEntity param) throws Exception;


}
