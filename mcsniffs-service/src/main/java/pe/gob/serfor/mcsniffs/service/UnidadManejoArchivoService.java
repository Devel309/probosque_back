package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoArchivoEntity;

import java.util.List;

public interface UnidadManejoArchivoService {

    ResultClassEntity RegistrarUnidadManejo(UnidadManejoArchivoEntity unidadManejoArchivoEntity) throws Exception;
    ResultClassEntity EliminarUnidadManejoArchivo(UnidadManejoArchivoEntity unidadManejoEntity) throws Exception;
    ResultClassEntity ListarUnidadManejoArchivo(UnidadManejoArchivoEntity request);
}
