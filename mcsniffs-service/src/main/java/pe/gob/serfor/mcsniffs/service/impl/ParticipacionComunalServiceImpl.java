package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalEntity;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalParamEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.ParticipacionComunalRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaDetalleRepository;
import pe.gob.serfor.mcsniffs.service.ParticipacionComunalService;

import java.util.List;

/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad ParticipacionComunal  de la base de datos mcsniffs}
 *
 */
@Service("ParticipacionComunalService")
public class ParticipacionComunalServiceImpl implements ParticipacionComunalService {

    @Autowired
    ParticipacionComunalRepository participacionComunalRepository;
    /**
     * @autor: Ivan Minaya. [08-07-2021]
     * @descripción: {Registrar ParticipacionComunal}
     * @param: List<ParticipacionComunalEntity>
     */
    @Override
    public ResultClassEntity RegistrarParticipacionComunal(List<ParticipacionComunalEntity> list) {
        return participacionComunalRepository.RegistrarParticipacionComunal(list);
    }
    /**
     * @autor: Ivan Minaya. [08-07-2021]
     * @descripción: {Actualizar ParticipacionComunal}
     * @param: List<ParticipacionComunalEntity>
     */
    @Override
    public ResultClassEntity ActualizarParticipacionComunal(List<ParticipacionComunalEntity> list) {
        return participacionComunalRepository.ActualizarParticipacionComunal(list);
    }
    /**
     * @autor: Ivan Minaya. [08-07-2021]
     * @descripción: {Eliminar ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity EliminarParticipacionComunal(ParticipacionComunalEntity participacionCom) {
        return participacionComunalRepository.EliminarParticipacionComunal(participacionCom);
    }
    /**
     * @autor: Ivan Minaya. [08-07-2021]
     * @descripción: {Obtener ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity ObtenerParticipacionComunal(ParticipacionComunalEntity participacionComunal) {
        return participacionComunalRepository.ObtenerParticipacionComunal(participacionComunal);
    }
    /**
     * @autor: Ivan Minaya. [08-07-2021]
     * @descripción: {Listar por filtro ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity ListarPorFiltroParticipacionComunal(ParticipacionComunalEntity participacionComunal) {
        return participacionComunalRepository.ListarPorFiltroParticipacionComunal(participacionComunal);
    }

    /**
     * @autor: Harry Coa. [23-07-2021]
     * @descripción: {registrar ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity RegistrarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception {
        return participacionComunalRepository.RegistrarParticipacionCiudadana(participacionComunalEntity);
    }

    /**
     * @autor: Harry Coa. [23-07-2021]
     * @descripción: {actualizar ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity ActualizarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception {
        return participacionComunalRepository.ActualizarParticipacionCiudadana(participacionComunalEntity);
    }

    /**
     * @autor: Harry Coa. [23-07-2021]
     * @descripción: {eliminar ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity EliminarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception {
        return participacionComunalRepository.EliminarParticipacionCiudadana(participacionComunalEntity);
    }

    /**
     * @autor: Harry Coa. [23-07-2021]
     * @descripción: {listar ParticipacionComunal}
     * @param: ParticipacionComunalEntity
     */
    @Override
    public List<ParticipacionComunalEntity> ListarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception {
        return participacionComunalRepository.ListarParticipacionCiudadana(participacionComunalEntity);
    }

    @Override
    public ResultClassEntity RegistrarParticipacionCiudadanaList(List<ParticipacionComunalEntity> list) throws Exception {
        return participacionComunalRepository.RegistrarParticipacionCiudadanaList(list);
    }

    
    @Override
    public ResultClassEntity registrarParticipacionComunalCaberaDetalle(
            List<ParticipacionComunalEntity> participacionComunal) {
        return participacionComunalRepository.registrarParticipacionComunalCaberaDetalle(participacionComunal);
    }


    @Override
    public ResultEntity<ParticipacionComunalEntity> listarParticipacionComunalCaberaDetalle(
            ParticipacionComunalParamEntity participacionComunal) {
        return participacionComunalRepository.listarParticipacionComunalCaberaDetalle(participacionComunal);
    }
    @Override
    public ResultClassEntity eliminarParticipacionComunalCaberaDetalle(
            ParticipacionComunalParamEntity participacionComunal) {
        return participacionComunalRepository.eliminarParticipacionComunalCaberaDetalle(participacionComunal);
    }
}
