package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface InformacionGeneralDemaService {
    ResultClassEntity registrarInformacionGeneralDema(InformacionGeneralDEMAEntity obj);
    ResultClassEntity actualizarInformacionGeneralDema(InformacionGeneralDEMAEntity obj);
    ResultEntity<InformacionGeneralDEMAEntity>listarInformacionGeneralDema(InformacionGeneralDEMAEntity filtro);
    ResultClassEntity listarPorFiltroInformacionGeneral(String nroDocumento, String nroRucEmpresa, String codTipoPlan, Integer idPlanManejo) throws Exception;
    
}
