package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.RegenteEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.RegenteRepository;
import pe.gob.serfor.mcsniffs.service.RegenteService;


import java.util.List;

/**
 * @autor: Harry Coa [29-07-2021]
 * @modificado: Service Regente
 * @descripción: {}
 *
 */
@Service("RegenteService")
public class RegenteServiceImpl implements RegenteService {

    @Autowired
    private RegenteRepository regenteRepository;

    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @Override
    public ResultEntity<RegenteEntity> ListarRegente(RegenteEntity param) throws Exception {
        return regenteRepository.ListarRegente(param);
    }

    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @Override
    public ResultClassEntity RegistrarRegente(RegenteEntity param) throws Exception {
        return regenteRepository.RegistrarRegente(param);
    }

    /**
     * @autor: Rafael Azaña [25-03-2022]
     * @modificado:
     * @descripción: {Listar información del regente y sus archivos}
     * @param:RegenteEntity
     */
    @Override
    public ResultEntity<RegenteEntity> ListarRegenteArchivos(RegenteEntity param) throws Exception {
        return regenteRepository.ListarRegenteArchivos(param);
    }


}
