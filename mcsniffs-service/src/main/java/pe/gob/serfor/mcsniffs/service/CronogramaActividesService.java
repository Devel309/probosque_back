package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividesEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad.CronogramaActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface CronogramaActividesService {
    
    ResultEntity<CronogramaActividesEntity> ListarCronogramaActividades(CronogramaActividesEntity request);
    ResultEntity<CronogramaActividesEntity> RegistrarCronogramaActividades(List<CronogramaActividesEntity> request);
    ResultEntity<CronogramaActividesEntity> EliminarCronogramaActividades(CronogramaActividesEntity request);

    List<Object[]> ListarCronogramaActividadesPivot(CronogramaActividesEntity request);

    /***Métodos para el manejo de Cronograma Actividades usando tabla detalle*******/
    ResultEntity<CronogramaActividadEntity> RegistrarCronogramaActividad(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> EliminarCronogramaActividad(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> ListarCronogramaActividad(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> ListarCronogramaActividadTitular(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> RegistrarMarcaCronogramaActividadDetalle(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividesEntity> ListarCronogramaActividadDetalle(CronogramaActividesEntity request);
//JaquelineDB
    ResultClassEntity EliminarCronogramaActividadDetalle(CronogramaActividadEntity filtro);

    ResultClassEntity registrarCronogramaActividadExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna, String codigoProceso, String codigoActividad, int idPlanManejo, int idUsuarioRegistro) throws Exception;
    ResultClassEntity RegistrarConfiguracionCronogramaActividad(CronogramaActividadDto request);
    ResultClassEntity ListarPorFiltroCronogramaActividad(CronogramaActividadDto request);
}
