package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;

public interface SolicitudConcesionEvaluacionCalificacionService {

        ResultClassEntity listarSolicitudConcesionEvaluacionCalificacion(
                        SolicitudConcesionEvaluacionCalificacionDto obj) throws Exception;

        ResultClassEntity registrarSolicitudConcesionEvaluacionCalificacion(
                        SolicitudConcesionEvaluacionDto obj)
                        throws Exception;

        ResultClassEntity actualizarSolicitudConcesionEvaluacionCalificacion(
                        SolicitudConcesionEvaluacionDto obj)
                        throws Exception;
}
