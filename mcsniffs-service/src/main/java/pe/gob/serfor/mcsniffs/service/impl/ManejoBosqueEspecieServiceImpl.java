package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueEspecieRepository;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueEspecieService;

import java.util.List;

@Service("ManejoBosqueEspecieService")
public class ManejoBosqueEspecieServiceImpl implements ManejoBosqueEspecieService {


    private static final Logger log = LogManager.getLogger(ManejoBosqueEspecieServiceImpl.class);

    @Autowired
    private ManejoBosqueEspecieRepository manejoBosqueEspecieRepository;

    @Override
    public ResultClassEntity RegistrarManejoBosqueEspecie(List<ManejoBosqueEspecieEntity> manejoBosqueEspecie) throws Exception {
        return manejoBosqueEspecieRepository.RegistrarManejoBosqueEspecie(manejoBosqueEspecie);
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueEspecie(ManejoBosqueEspecieEntity manejoBosqueEspecie) throws Exception {
        return manejoBosqueEspecieRepository.EliminarManejoBosqueEspecie(manejoBosqueEspecie);
    }

    @Override
    public ResultClassEntity ListarManejoBosqueEspecie(ManejoBosqEntity manejoBosque) throws Exception {
        return manejoBosqueEspecieRepository.ListarManejoBosqueEspecie(manejoBosque);
    }
}
