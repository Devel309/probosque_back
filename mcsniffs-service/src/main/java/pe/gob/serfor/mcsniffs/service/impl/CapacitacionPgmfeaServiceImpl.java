package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.CapacitacionPgmfeaRepository;
import pe.gob.serfor.mcsniffs.service.CapacitacionPgmfeaService;

import java.util.List;

@Service("CapacitacionPgmfeaService")
public class CapacitacionPgmfeaServiceImpl implements CapacitacionPgmfeaService {

    @Autowired
    private CapacitacionPgmfeaRepository capacitacionPgmfeaRepository;

    /**
     * @autor: IVAN MINAYA [01-07-2021]
     * @modificado: Registrar capacitacion PGMFEA
     * @descripción: {}
     *
     */
    @Override
    public ResultClassEntity RegistrarCapacitacionPgmfea(List<CapacitacionDto> list) throws Exception {
        return capacitacionPgmfeaRepository.RegistrarCapacitacionPgmfea(list);
    }

    @Override
    public ResultClassEntity ListarCapacitacionPgmfea(PlanManejoEntity param) throws Exception {
        return capacitacionPgmfeaRepository.ListarCapacitacionPgmfea(param);
    }

    @Override
    public ResultClassEntity EliminarCapacitacionPgmfea(CapacitacionEntity param) throws Exception {
        return capacitacionPgmfeaRepository.EliminarCapacitacionPgmfea(param);
    }
}
