package pe.gob.serfor.mcsniffs.service;


import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalBeneficiarioEntity;

public interface SolicitudBosqueLocalBeneficiarioService {

    ResultClassEntity listarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) throws Exception;

    //ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) throws Exception;

    ResultClassEntity eliminarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) throws Exception;

    ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(List<SolicitudBosqueLocalBeneficiarioEntity> obj) throws Exception;

}
