package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionCalificacionRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionEvaluacionCalificacionService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;

@Service("SolicitudConcesionEvaluacionCalificacionService")
public class SolicitudConcesionEvaluacionCalificacionServiceImpl
        implements SolicitudConcesionEvaluacionCalificacionService {

    @Autowired
    private SolicitudConcesionEvaluacionCalificacionRepository solicitudConcesionEvaluacionCalificacionRepository;

    @Autowired
    private SolicitudConcesionEvaluacionRepository solicitudConcesionEvaluacionRepository;

    @Autowired
    private SolicitudConcesionService servicioSolicitudConcesion;

    @Override
    public ResultClassEntity listarSolicitudConcesionEvaluacionCalificacion(
            SolicitudConcesionEvaluacionCalificacionDto obj) throws Exception {
        return solicitudConcesionEvaluacionCalificacionRepository.listarSolicitudConcesionEvaluacionCalificacion(obj);
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionEvaluacionCalificacion(
            SolicitudConcesionEvaluacionDto obj)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity<>();

        result = solicitudConcesionEvaluacionRepository.actualizarSolicitudConcesionEvaluacion(obj);

        List<SolicitudConcesionEvaluacionCalificacionDto> calificacion = obj
                .getLstSolicitudConcesionEvaluacionCalificacion();
        List<SolicitudConcesionArchivoEntity> adjuntos = obj.getLstSolicitudConcesionEvaluacionCalificacionArchivo();

        if (calificacion != null && !calificacion.isEmpty()) {
            for (SolicitudConcesionEvaluacionCalificacionDto element : calificacion) {
                result = solicitudConcesionEvaluacionCalificacionRepository
                        .registrarSolicitudConcesionEvaluacionCalificacion(element);
            }
        }

        if (adjuntos != null && !adjuntos.isEmpty()) {
            for (SolicitudConcesionArchivoEntity archivo : adjuntos) {
                if (archivo.getIdSolicitudConcesionArchivo() > 0)
                    result = servicioSolicitudConcesion.actualizarArchivoSolicitudConcesion(archivo);
                else
                    result = servicioSolicitudConcesion.registrarArchivoSolicitudConcesion(archivo);
            }
        }

        return result;
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionEvaluacionCalificacion(
            SolicitudConcesionEvaluacionDto obj)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();

        result = solicitudConcesionEvaluacionRepository.actualizarSolicitudConcesionEvaluacion(obj);

        List<SolicitudConcesionEvaluacionCalificacionDto> calificacion = obj
                .getLstSolicitudConcesionEvaluacionCalificacion();
        List<SolicitudConcesionArchivoEntity> adjuntos = obj.getLstSolicitudConcesionEvaluacionCalificacionArchivo();

        if (calificacion != null && !calificacion.isEmpty()) {
            for (SolicitudConcesionEvaluacionCalificacionDto element : calificacion) {
                result = solicitudConcesionEvaluacionCalificacionRepository
                        .actualizarSolicitudConcesionEvaluacionCalificacion(element);
            }
        }

        if (adjuntos != null && !adjuntos.isEmpty()) {
            for (SolicitudConcesionArchivoEntity archivo : adjuntos) {
                if (archivo.getIdSolicitudConcesionArchivo() > 0)
                    result = servicioSolicitudConcesion.actualizarArchivoSolicitudConcesion(archivo);
                else
                    result = servicioSolicitudConcesion.registrarArchivoSolicitudConcesion(archivo);
            }
        }

        return result;
    }
}
