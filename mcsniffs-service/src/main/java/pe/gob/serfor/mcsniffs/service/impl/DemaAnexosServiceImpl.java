package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.DemaAnexosRepository;
import pe.gob.serfor.mcsniffs.service.DemaAnexosService;

@Service
public class DemaAnexosServiceImpl implements DemaAnexosService{
    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Repository en donde se procesos los anexos referentes a dema}
     * @param:
     */
    @Autowired
    DemaAnexosRepository repo;
    
    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se relacion los archivos cargados con dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultClassEntity<PGMFArchivoEntity> registrarArchivo(PGMFArchivoEntity archivo) throws Exception{
        return repo.registrarArchivo(archivo);
    }

     /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se listan los archivos cargados con dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultEntity<PlanManejoArchivoEntity> listarArchivoDema(PlanManejoArchivoEntity filtro) {
        return repo.listarArchivoDema(filtro);
    }

     /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se genera el anexo2 de dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo2Dema(PlanManejoArchivoEntity filtro) {
        return repo.generarAnexo2Dema(filtro);
    }

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se genera el anexo3 de dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo3Dema(PlanManejoArchivoEntity filtro) {
        return repo.generarAnexo3Dema(filtro);
    }
                                 
}
