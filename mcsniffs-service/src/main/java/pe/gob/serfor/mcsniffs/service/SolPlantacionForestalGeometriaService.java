package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalGeometriaEntity;

public interface SolPlantacionForestalGeometriaService {

    ResultClassEntity registrarSolPlantacionForestalGeometria( List<SolPlantacionForestalGeometriaEntity> items)  throws Exception;
    ResultClassEntity actualizarSolPlantacionForestalGeometria( List<SolPlantacionForestalGeometriaEntity> items)  throws Exception;
    ResultClassEntity listarSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item);
    ResultClassEntity obtenerSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item) throws Exception;
    ResultClassEntity eliminarSolPlantacionForestalGeometriaArchivo (SolPlantacionForestalGeometriaEntity item);


}
