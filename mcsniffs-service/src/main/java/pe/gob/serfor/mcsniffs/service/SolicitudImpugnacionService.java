package pe.gob.serfor.mcsniffs.service;

import java.util.List;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudImpugnacionEntity;


public interface SolicitudImpugnacionService {
    ResultClassEntity<List<Solicitud2Entity>> listarSolicitudesVencidas(); 
    
    ResultClassEntity<Boolean> SolicitudImpugnacionActualizar(SolicitudImpugnacionEntity params);

    ResultClassEntity<List<SolicitudImpugnacionEntity>> ListaSolicitudesVencidasObtener() throws Exception;
    
    ResultClassEntity<Boolean> SolicitudImpugnacionEnviarCorreo(SolicitudImpugnacionEntity params) throws Exception;

    ResultClassEntity<Boolean> SolicitudImpugnacionRegistrarActualizar(SolicitudImpugnacionEntity params) ;    

    ResultClassEntity SolicitudImpugnacionObtener(SolicitudImpugnacionEntity solicitudImpugnacionEntity) throws Exception;

    ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> listarArchivoSolicitudImpugnacion(SolicitudImpugnacionArchivo2Entity param); 

    ResultClassEntity<List<SolicitudImpugnacionEntity>> solicitudImpugnacionListar(SolicitudImpugnacionEntity param); 
}
