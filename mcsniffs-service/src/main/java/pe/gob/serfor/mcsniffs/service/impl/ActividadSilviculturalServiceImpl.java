package pe.gob.serfor.mcsniffs.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetallePgmfDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.SistemaManejoAprovechamientoLaboresSilviculturales.SistemaManejoDto;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;
import pe.gob.serfor.mcsniffs.repository.ActividadSilviculturalRepository;
import pe.gob.serfor.mcsniffs.service.ActividadSilviculturalService;

@Service("ActividadSilviculturalService")
public class ActividadSilviculturalServiceImpl implements ActividadSilviculturalService {
    @Autowired
    private ActividadSilviculturalRepository actividadSilviculturalRepository;

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetalle(Integer idPlanManejo)
            throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilviculturalDetalle(idPlanManejo);
    }

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalFiltroTipo(Integer idPlanManejo,
            String idTipo) throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo, idTipo);
    }

    @Override
    public ResultClassEntity ConfigurarActividadSilviculturalPgmf(
            ActividadSilviculturalEntity actividadSilviculturalEntity) throws Exception {
        return actividadSilviculturalRepository.ConfigurarActividadSilviculturalPgmf(actividadSilviculturalEntity);
    }

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetallePgmf(Integer idPlanManejo,
            Integer idTipo) throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilviculturalDetallePgmf(idPlanManejo, idTipo);
    }

    @Override
    @Transactional
    public ResultClassEntity ActualizarActividadSilviculturalPgmf(List<ActividadSilviculturalDetallePgmfDto> lista)
            throws Exception {
        ResultClassEntity result = null;
        for (ActividadSilviculturalDetallePgmfDto element : lista) {
            result = actividadSilviculturalRepository.ActualizarActividadSilviculturalPgmf(element);
        }
        return result;
    }

    public ResultClassEntity RegistrarActividadSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception {
        return actividadSilviculturalRepository.RegistrarActividadSilvicultural(list);
    }
    @Override
    public List<ActividadSilviculturalEntity> ListarActSilviCulturalDetalle(ActividadSilviculturalEntity param) throws Exception {
        return actividadSilviculturalRepository.ListarActSilviCulturalDetalle(param);
    }

    @Override
    public List<ActividadSilviculturalDetalleDto> ListarActividadSilvicultural(Integer idPlanManejo,String codigoPlan) throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilvicultural(idPlanManejo,codigoPlan);
    }

    @Override
    public ResultClassEntity RegistrarLaborSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception {
        return actividadSilviculturalRepository.RegistrarLaborSilvicultural(list);
    }

    @Override
    @Transactional
    public ResultClassEntity EliminarActividadSilviculturalPgmf(List<ActividadSilviculturalDetalleEntity> list)
            throws Exception {
        ResultClassEntity result = null;
        for (ActividadSilviculturalDetalleEntity element : list) {
            result = actividadSilviculturalRepository.EliminarActividadSilviculturalPgmf(element);
        }
        return result;
    }

    @Override
    @Transactional
    public ResultClassEntity EliminarActividadSilviculturalDema(List<ActividadSilviculturalDetalleEntity> list)
            throws Exception {
        ResultClassEntity result = null;
        for (ActividadSilviculturalDetalleEntity element : list) {
            result = actividadSilviculturalRepository.EliminarActividadSilviculturalDema(element);
        }
        return result;
    }

    // region PDFM
    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalPFDM(Integer idPlanManejo,
            String idTipo) throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipo(idPlanManejo, idTipo);
    }


    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalPFDMTitular(String tipoDocumento, String nroDocumento,String codigoProcesoTitular,String codigoProceso,Integer idPlanManejo) throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilviculturalFiltroTipoTitular(tipoDocumento, nroDocumento,codigoProcesoTitular,codigoProceso,idPlanManejo);
    }

    @Override
    @Transactional
    public ResultClassEntity EliminarActividadSilviculturalPFDM(List<ActividadSilviculturalDetalleEntity> list)
            throws Exception {
        ResultClassEntity result = null;
        for (ActividadSilviculturalDetalleEntity element : list) {
            result = actividadSilviculturalRepository.EliminarActividadSilviculturalDema(element);
        }
        return result;
    }

    @Override
    @Transactional
    public ResultClassEntity GuardarSistemaManejoPFDM(SistemaManejoDto param) throws Exception {
        ResultClassEntity result = null;
        List<ActividadSilviculturalEntity> listActividad = new ArrayList<ActividadSilviculturalEntity>();

        // Guardar etapa aprovechamiento
        if (param.getAprovechamiento() != null) {
            listActividad.add(param.getAprovechamiento());
        }

        // Guardar 5.2
        if (param.getDivisionAdministrativa() != null) {
            listActividad.add(param.getDivisionAdministrativa());
        }

        // Guardar Labores silviculturales
        if (param.getLaboresSilviculturales() != null) {
            listActividad.add(param.getLaboresSilviculturales());
        }

        if (listActividad.size() > 0) {
            result = actividadSilviculturalRepository.RegistrarLaborSilvicultural(listActividad);
        }
        return result;
    }

    @Override
    public ResultClassEntity<List<ActividadSilviculturalDto>> ListarActividadSilviculturalCabecera(Integer idPlanManejo)
            throws Exception {
        return actividadSilviculturalRepository.ListarActividadSilviculturalCabecera(idPlanManejo);
    }
    // endregion PDFM

}
