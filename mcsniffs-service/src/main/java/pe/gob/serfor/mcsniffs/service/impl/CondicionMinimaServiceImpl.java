package pe.gob.serfor.mcsniffs.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.*;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.CondicionMinimaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.CondicionMinimaRepository;
import pe.gob.serfor.mcsniffs.service.CondicionMinimaService;
import pe.gob.serfor.mcsniffs.service.client.FeignPideApi;

@Service("CondicionMinimaService")
public class CondicionMinimaServiceImpl implements CondicionMinimaService {


    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.service.impl.GenericoServiceImpl.class);

    @Autowired
    private CondicionMinimaRepository condicionMinimaRepository;

    @Autowired
    private CondicionMinimaDetalleRepository condicionMinimaDetalleRepository;

    @Autowired
    private FeignPideApi feignPideApi;

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Lista las Provincias}
     * @param:ProvinciaEntity
     */


    @Override
    public ResultClassEntity validarCondicionesMinimas(ConsultaRequisitosDto consultaRequisitosDto, String token) throws Exception{
        ResultClassEntity result =  new ResultClassEntity();
        ConsultaRequisitosDto response = new ConsultaRequisitosDto();

        try {
            ConsultaAntecedenteJudicialDto consultaJudicial = new ConsultaAntecedenteJudicialDto();
            consultaJudicial.setApellidoMaterno(consultaRequisitosDto.getApellidoMaterno());
            consultaJudicial.setApellidoPaterno(consultaRequisitosDto.getApellidoPaterno());
            consultaJudicial.setNombres(consultaRequisitosDto.getNombres());

            ConsultaAntecedentePenalDto consultaAntecedentePenal = new ConsultaAntecedentePenalDto();
            consultaAntecedentePenal.setxApellidoMaterno(consultaRequisitosDto.getApellidoMaterno());
            consultaAntecedentePenal.setxApellidoPaterno(consultaRequisitosDto.getApellidoPaterno());
            consultaAntecedentePenal.setxAudDireccionMAC("90:B1:1C:90:DA:AD");
            consultaAntecedentePenal.setxAudIP("191.98.137.60");
            consultaAntecedentePenal.setxAudNombrePC("SERFORPC");
            consultaAntecedentePenal.setxAudNombreUsuario("ZZ");
            consultaAntecedentePenal.setxDni(consultaRequisitosDto.getNroDocumento());
            consultaAntecedentePenal.setxDniPersonaConsultante(consultaRequisitosDto.getNroDocumento());
            consultaAntecedentePenal.setxIpPublica("191.98.137.60");
            consultaAntecedentePenal.setxMotivoConsulta("CONSULTA DE ANTECEDENTES PENALES");
            String[] nom = consultaRequisitosDto.getNombres().split(" ");
            consultaAntecedentePenal.setxNombre1(nom.length>=1 && nom[0]!=null?nom[0]:"");
            consultaAntecedentePenal.setxNombre2(nom.length>=2 && nom[1]!=null?nom[1]:"");
            consultaAntecedentePenal.setxNombre3(nom.length>=3 && nom[2]!=null?nom[2]:"");
            consultaAntecedentePenal.setxProcesoEntidadConsultante("SERFOR");
            consultaAntecedentePenal.setxRucEntidadConsultante("20562836927");

            ConsultaAntecedentePolicialNumDocDto consultaAntecedentePolicialNumDoc= new ConsultaAntecedentePolicialNumDocDto();
            consultaAntecedentePolicialNumDoc.setClienteClave("FL4G4NT");
            consultaAntecedentePolicialNumDoc.setClienteIp("192.168.1.154");
            consultaAntecedentePolicialNumDoc.setClienteMac("AA:BB:CC:DD:EE:FF");
            consultaAntecedentePolicialNumDoc.setClienteSistema("SISTEMA ENTIDAD");
            consultaAntecedentePolicialNumDoc.setClienteUsuario("SERFOR");
            consultaAntecedentePolicialNumDoc.setNumeroDocUserClieFin(consultaRequisitosDto.getNroDocumento());
            consultaAntecedentePolicialNumDoc.setNumeroDocumento(consultaRequisitosDto.getNroDocumento());
            consultaAntecedentePolicialNumDoc.setServicioCodigo("WS_PIDE_ANTECEDENTES_FLAG");
            consultaAntecedentePolicialNumDoc.setTipoDocUserClieFin("2");

            ConsultarSancionVigenteOsceDto consultarSancionVigente = new ConsultarSancionVigenteOsceDto();
            consultarSancionVigente.setRuc(consultaRequisitosDto.getNroDocumento());

            ConsultaInfractorDto consultaInfractor= new ConsultaInfractorDto();
            consultaInfractor.setTipoDocumento(consultaRequisitosDto.getTipoDocumento());
            consultaInfractor.setNumDocumento(consultaRequisitosDto.getNroDocumento());

            AntecedenteJudicialDto data = feignPideApi.consultarAntecedenteJudicial(token, consultaJudicial);
            AntecedentePenalDto data2 = feignPideApi.consultarAntecedentePenal(token, consultaAntecedentePenal);
            AntecedentePolicialDto data3 = feignPideApi.consultarAPbyNumDoc(token, consultaAntecedentePolicialNumDoc);
            SancionVigenteOsceDto data4 = feignPideApi.consultarSancionVigente(token, consultarSancionVigente);
            InfractorDto data5 = feignPideApi.consultarInfractor(token, consultaInfractor);


            response.setTipoDocumento(consultaRequisitosDto.getTipoDocumento());
            response.setNroDocumento(consultaRequisitosDto.getNroDocumento());
            response.setNombres(consultaRequisitosDto.getNombres());
            response.setApellidoPaterno(consultaRequisitosDto.getApellidoPaterno());
            response.setApellidoMaterno(consultaRequisitosDto.getApellidoMaterno());

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            response.setFechaEvaluacion(dateFormat.format(date));
            response.setResultadoGeneral("NO CONFORME");
            response.setResultadoAntecedenteJudicial("----");
            response.setResultadoAntecedentePenal("----");
            response.setResultadoAntecedentePolicial("----");
            response.setResultadoSancionVigenteOsce("----");
            response.setResultadoInfractor("----");

            if(consultaRequisitosDto.getTipoDocumento().equals("DNI")) {
                if (data.getDataService() != null) {

                    if (data.getDataService().equals("No registra antecedentes judiciales")) {
                        response.setResultadoAntecedenteJudicial("CONFORME");
                    } else {
                        response.setResultadoAntecedenteJudicial("NO CONFORME");
                    }
                }

                if (data2.getDataService() != null && data2.getDataService().getXcodigoRespuesta() != null) {
                    if (data2.getDataService().getXcodigoRespuesta().equals("0000")) {
                        response.setResultadoAntecedentePenal("CONFORME");
                    } else {
                        response.setResultadoAntecedentePenal("NO CONFORME");
                    }
                }

                if (data3.getDataService() != null && data3.getDataService().get(0) != null) {
                    if (data3.getDataService().get(0).getCodigoMensaje() != null && data3.getDataService().get(0).getCodigoMensaje().equals("16")) {
                        response.setResultadoAntecedentePolicial("CONFORME");
                    } else {
                        response.setResultadoAntecedentePolicial("NO CONFORME");
                    }
                }
            }else {
                response.setResultadoAntecedenteJudicial("CONFORME");
                response.setResultadoAntecedentePenal("CONFORME");
                response.setResultadoAntecedentePolicial("CONFORME");
            }

            if (data4.getDataService()!=null && data4.getDataService().getOutput()!= null) {
                if (data4.getDataService().getOutput().getResultado().equals("2")) {
                    response.setResultadoSancionVigenteOsce("CONFORME");
                }else{
                    response.setResultadoSancionVigenteOsce("NO CONFORME");
                }
            }
            if (data5.getDataService()!=null && data5.getDataService().getMessage()!=null){
                if(data5.getDataService().getMessage().equals("No se encontró infracciones")) {
                    response.setResultadoInfractor("CONFORME");
                }else{
                    response.setResultadoInfractor("NO CONFORME");
                }
            }

            if (response.getResultadoAntecedenteJudicial().equals("CONFORME") &&
                response.getResultadoAntecedentePenal().equals("CONFORME") &&
                response.getResultadoAntecedentePolicial().equals("CONFORME") &&
                response.getResultadoSancionVigenteOsce().equals("CONFORME") &&
                response.getResultadoInfractor().equals("CONFORME")) {
                response.setResultadoGeneral("CONFORME");
            }

            result.setSuccess(true);
            result.setData(response);

        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }


        return result;
    }

    @Override
    public ResultClassEntity registrarCondicionMinima(CondicionMinimaDto request) {

        return condicionMinimaRepository.registrarCondicionMinima(request);

    }

    @Override
    public ResultClassEntity obtenerCondicionMinima(Integer idCondicionMinima) throws Exception{
 
        List<ListarCondicionMinimaDto> lista = condicionMinimaRepository.obtenerCondicionMinima(idCondicionMinima);
        
        ListarCondicionMinimaDto dto = new ListarCondicionMinimaDto();

        if (lista!=null && !lista.isEmpty()) {
            dto = lista.get(0);            
        }
        
        dto.setListaCondicionMinimaDetalle(condicionMinimaDetalleRepository.listarCondicionMinimaDetalle(dto.getIdCondicionMinima()));

        ResultClassEntity result = new ResultClassEntity();        
        result.setData(dto);
        result.setSuccess(true);
 
        return result;

    }

}
