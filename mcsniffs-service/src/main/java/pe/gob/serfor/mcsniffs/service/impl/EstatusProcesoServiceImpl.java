package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.EstatusProceso.ListarEstadoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.EstatusProcesoRepository;
import pe.gob.serfor.mcsniffs.service.EstatusProcesoService;

import java.util.List;

@Service("EstatusProcesoService")
public class EstatusProcesoServiceImpl implements EstatusProcesoService {


    //private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.service.impl.GenericoServiceImpl.class);

    @Autowired
    private EstatusProcesoRepository estatusProcesoRepository;

    @Override
    public ResultClassEntity<List<ListarEstadoDto>> listarEstadoEstatusProceso(ListarEstadoDto request) {
        return estatusProcesoRepository.listarEstadoEstatusProceso(request);
    }

}
