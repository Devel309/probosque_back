package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalBeneficiarioEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalBeneficarioRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalBeneficiarioService;
@Service("SolicitudBosqueLocalBeneficiarioService")
public class SolicitudBosqueLocalBeneficiarioServiceImpl implements SolicitudBosqueLocalBeneficiarioService {

    @Autowired
    private SolicitudBosqueLocalBeneficarioRepository solicitudBosqueLocalBeneficarioRepository;

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) throws Exception{
        return solicitudBosqueLocalBeneficarioRepository.listarSolicitudBosqueLocalBeneficiario(obj);
    }

    /*public ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) throws Exception{
        return solicitudBosqueLocalBeneficarioRepository.registrarSolicitudBosqueLocalBeneficiario(obj);
    }
    */
    public ResultClassEntity eliminarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) throws Exception{
        return solicitudBosqueLocalBeneficarioRepository.eliminarSolicitudBosqueLocalBeneficiario(obj);
    }
   
    public ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(List<SolicitudBosqueLocalBeneficiarioEntity> obj) throws Exception{
        return solicitudBosqueLocalBeneficarioRepository.registrarSolicitudBosqueLocalBeneficiario(obj);
    }
}