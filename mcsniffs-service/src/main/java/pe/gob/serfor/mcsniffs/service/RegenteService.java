package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.RegenteEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.util.List;

public interface RegenteService {
    ResultEntity<RegenteEntity> ListarRegente(RegenteEntity param) throws Exception;
    ResultClassEntity RegistrarRegente(RegenteEntity param) throws Exception;
    ResultEntity<RegenteEntity> ListarRegenteArchivos(RegenteEntity param) throws Exception;

}
