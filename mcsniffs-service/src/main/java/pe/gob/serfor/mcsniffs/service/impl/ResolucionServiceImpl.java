package pe.gob.serfor.mcsniffs.service.impl;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionResultadoDto;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.ResolucionArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ResolucionRepository;
import pe.gob.serfor.mcsniffs.repository.ResultadoPPRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ResolucionService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;
import org.springframework.beans.factory.annotation.Value;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Service("resolucionServiceImpl")
public class ResolucionServiceImpl implements ResolucionService {

    private static final Logger log = LogManager.getLogger(SolicitudOpinionServiceImpl.class);

    @Autowired
    private ResolucionRepository resolucionRepository;
    @Autowired
    private ResolucionArchivoRepository resolucionArchivoRepository;
    @Autowired
    private ResultadoPPRepository resultadoPPRepository;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    @Autowired
    private ServicioExternoService serExt;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;
    @Autowired
    private EmailService emailService;
    @Override
    public ResultClassEntity ListarResolucion(ResolucionDto param) {
        return resolucionRepository.ListarResolucion(param);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity ObtenerResolucion(ResolucionDto param) throws Exception {

 
        ResultClassEntity result = resolucionRepository.ListarResolucion(param);
        List<ResolucionDto> lista = new ArrayList<>();

        if (result != null && result.getData() != null) {
            lista = (List<ResolucionDto>) result.getData();

            ResolucionArchivoDto resolucionArchivoDto = null;
            for (ResolucionDto element : lista) {

                resolucionArchivoDto = new ResolucionArchivoDto();
                resolucionArchivoDto.setIdResolucion(element.getIdResolucion());
                element.setListaResolucionArchivo(
                        resolucionArchivoRepository.listarResolucionArchivo(resolucionArchivoDto));
            }
            result.setData(lista);
            result.setSuccess(true);
        }



        return result;
    }



    @Override
    public ResultClassEntity RegistrarResolucion(ResolucionDto param) {
        ResultClassEntity envio = new ResultClassEntity();

            Gson gson = new Gson();


            UsuarioDto user= new UsuarioDto();
            user.setIdusuario(param.getIdResolucion());

            ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), param.getToken());
            ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
            String correo = null;
            String nombreCompleto = null;
            for(Object item: resultApiExt.getData().stream().toArray()) {
                LinkedTreeMap<Object, Object> val = (LinkedTreeMap) item;
                correo= val.get("correoElectronico").toString();
                nombreCompleto = val.get("nombres").toString()+" "+val.get("apellidoPaterno").toString()+" "+val.get("apellidoMaterno").toString();
            }

            String fecha = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

            if (correo!=null) {
                String[] mails=new String[1];
                mails[0]= correo;
                EmailEntity mail=new EmailEntity();
                mail.setSubject("SERFOR:: Plan General de Manejo Forestal");
                mail.setEmail(mails);

                String body="<html><head>"
                        +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                        +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                        +"</head>"
                        +"<body lang=ES-PE style='word-wrap:break-word'>"
                        +"<div class=WordSection1>"
                        +"<p class=MsoNormal>Estimado(s) <span lang=ES> "+nombreCompleto+"</span></p>"
                        +"<p class=MsoNormal><span lang=ES>Se comunica lo siguiente:</span></p>"
                        +"<p class=MsoNormal><span lang=ES>La solicitud N° : 52 ha sido Observada.:</span></p>"
                        +"<p class=MsoNormal><span lang=ES>Nota:</span></p>"
                        +"<p class=MsoNormal><span lang=ES>Tenga presente que usted está recibiendo este correo porque ha realizado la creación de una resolución solicitud de PFDM en el aplicativo MCSNIFFS.</span></p>"
                        +"<br><br><br>"
                        +"<p class=MsoNormal><b><i><span lang=ES> Atentamente,</span></i></b><i></i></p>"
                        +"<p class=MsoNormal><b><i><span lang=ES> Departamento de Sistemas</span></i></b><i></i></p>"
                        +"<p class=MsoNormal><b><i><span lang=ES>MCSniffs</span></i></b><i></i></p>"
                        +"<p class=MsoNormal><b><i><span lang=ES>P.D.: No necesita responder este correo debido a que ha sido generado por un proceso automático.</span></i></b><i></i></p>"
                        +"</div></body></html>";

                mail.setContent(body);
                Boolean smail = emailService.sendEmail(mail);
                if(smail){
                    envio.setMessage("Correo Enviado Correctamente.");
                }
            }


        envio= resolucionRepository.registrarResolucion(param);

        for (ResolucionArchivoDto element : param.getListaResolucionArchivo()) {
            element.setIdResolucion(envio.getCodigo());
            element.setIdUsuarioRegistro(param.getIdUsuarioRegistro());
            resolucionArchivoRepository.registrarResolucionArchivo(element);
        }

        return envio;
    }
    @Override
    public ResultClassEntity registrarResolucionBase(ResolucionDto param) {
        ResultClassEntity envio = new ResultClassEntity();

        envio= resolucionRepository.registrarResolucion(param);

        if(param.getListaResolucionArchivo()!=null && param.getListaResolucionArchivo().size()>0 ){
            for (ResolucionArchivoDto element : param.getListaResolucionArchivo()) {
                element.setIdResolucion(envio.getCodigo());
                element.setIdUsuarioRegistro(param.getIdUsuarioRegistro());
                resolucionArchivoRepository.registrarResolucionArchivo(element);
            }
        }
        return envio;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultClassEntity registrarResolucionResultado(ResolucionResultadoDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity<>();
        result = resolucionRepository.registrarResolucion(param.getResolucion());
        if(result.getSuccess()){
               result = resultadoPPRepository.registrarResultadoPp(param.getResultadoPp());
            if(result.getSuccess()){
                return   result ;
            }
            else{
                return result;
            }
        }
        else{
            return result;
        }


    }

    @Override
    public ResultClassEntity ActualizarResolucion(ResolucionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        result= resolucionRepository.ActualizarResolucion(param);

        for (ResolucionArchivoDto element : param.getListaResolucionArchivo()) {
            element.setIdResolucion(param.getIdResolucion());
            element.setIdUsuarioRegistro(param.getIdUsuarioRegistro());
            resolucionArchivoRepository.registrarResolucionArchivo(element);
        }

        return result;



    }

    @Override
    public ResultClassEntity ActualizarResolucionEstado(ResolucionDto param) throws Exception{


        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_RESOLUCION,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_RESOLUCION + param.getIdResolucion(),
                param.getIdUsuarioModificacion());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resolucionRepository.ActualizarResolucionEstado(param);
    }

    @Override
    public ResultClassEntity resolucionValidar(Integer idProcesoOferta) {

        ResultClassEntity result = new  ResultClassEntity<>();
        String  msg = resolucionRepository.resolucionValidar(idProcesoOferta);


        result.setSuccess(true);
        result.setValidateBusiness(true);

        if(!msg.equals("OK")){
            result.setValidateBusiness(false);
            result.setMessage(msg);            
        }
        return result;
    }

}
