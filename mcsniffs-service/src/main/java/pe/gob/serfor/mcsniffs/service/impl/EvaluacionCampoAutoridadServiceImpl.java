package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad.EvaluacionCampoAutoridadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoAutoridadRepository;
import pe.gob.serfor.mcsniffs.service.EvaluacionCampoAutoridadService;

import java.util.List;


@Service("EvaluacionCampoAutoridadService")
public class EvaluacionCampoAutoridadServiceImpl implements EvaluacionCampoAutoridadService {


    private static final Logger log = LogManager.getLogger(EvaluacionCampoAutoridadServiceImpl.class);

    @Autowired
    private EvaluacionCampoAutoridadRepository evaluacionCampoAutoridadRepository;
    @Override
    public ResultClassEntity ComboPorFiltroAutoridad(EvaluacionCampoAutoridadDto request) throws Exception  {
        return evaluacionCampoAutoridadRepository.ComboPorFiltroAutoridad(request);
    }
    @Override
    public ResultClassEntity RegistrarEvaluacionCampoAutoridad(List<EvaluacionCampoAutoridadDto> list){
        return evaluacionCampoAutoridadRepository.RegistrarEvaluacionCampoAutoridad(list);
    }

}
