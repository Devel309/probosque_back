package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.UnidadAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.UnidadAprovechamientoService;

@Service
public class UnidadAprovechamientoServiceImpl implements UnidadAprovechamientoService {
    /**
     * @autor: JaquelineDB [12-06-2021]
     * @modificado:
     * @descripción: {Servicio de Unidad aprovechamiento, en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    UnidadAprovechamientoRepository repository;

    @Autowired
    LogAuditoriaService logAuditoriaService;

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {inserta y actualiza la unidad aprovechamiento}
     * @param:obj
     */
    @Override
    public ResultClassEntity guardarUnidadAprovechamiento(UnidadAprovechamientoEntity obj) throws Exception{

        ResultClassEntity resultClassEntity = repository.guardarUnidadAprovechamiento(obj);

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAC_UNIDAD_APROVECHAMIENTO,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MAC_UNIDAD_APROVECHAMIENTO + resultClassEntity.getCodigo(),
                obj.getIdUsuarioRegistro());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return resultClassEntity;
    }

    @Override
    public ResultEntity<PropertiesEntity> listar() {
        /**
         * @autor: Abner Valdez [07-07-2021]
         * @modificado:
         * @descripción: {Listar Unidad de Aprovechamiento}
         *
         */
        return repository.listar();
    }
    @Override
    public ResultEntity<PropertiesEntity> buscarByIdProcesoOferta(Integer id) {
        /**
         * @autor: Abner Valdez [19-07-2021]
         * @modificado:
         * @descripción: {Buscar Unidad de Aprovechamiento por Proceso de Oferta}
         *
         */
        return repository.buscarByIdProcesoOferta(id);
    }

    @Override
    public ResultEntity<PropertiesEntity> buscarByIdPlanManejo(Integer id) {
        /**
         * @autor: Abner Valdez [04-08-2021]
         * @modificado:
         * @descripción: {Buscar Unidad de Aprovechamiento por Id Plan de Manejo}
         *
         */
        return repository.buscarByIdPlanManejo(id);
    }
}
