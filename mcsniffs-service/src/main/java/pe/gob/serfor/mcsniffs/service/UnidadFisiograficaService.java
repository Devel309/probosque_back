package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import org.springframework.web.multipart.MultipartFile;

public interface UnidadFisiograficaService {
    ResultClassEntity listarUnidadFisiografica(Integer idInfBasica)throws Exception;
    ResultClassEntity registrarUnidadFisiograficaArchivo(UnidadFisiograficaUMFEntity param,MultipartFile file)throws Exception;
    ResultClassEntity eliminarUnidadFisiograficaArchivo(Integer idInfBasica,Integer idUnidadFisiografica,Integer idUsuario)throws Exception;
}
