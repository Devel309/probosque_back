package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.service.MonitoreoService;

import java.util.List;

/**
 * @autor: Ivan Minaya [09-06-2021]
 * @modificado:
 * @descripción: {Clase de servicio relacionada a la entidad Monitoreo  de la base de datos mcsniffs}
 *
 */
@Service("MonitoreoService")
public class MonitoreoServiceImpl implements MonitoreoService {


    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.service.impl.ProteccionBosqueServiceImpl.class);

    @Autowired
    private MonitoreoRepository monitoreoRepository;

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Registrar monitoreo PGMFEA}
     * @param: List<MonitoreoPgmfeaEntity>
     */
    @Override
    public ResultClassEntity RegistrarMonitoreoPgmfea(List<MonitoreoPgmfeaEntity>  monitoreo) throws Exception {
        return monitoreoRepository.RegistrarMonitoreoPgmfea(monitoreo);
    }

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Elimina  monitoreo PGMFEA}
     * @param: MonitoreoPgmfeaEntity
     */

    @Override
    public ResultClassEntity EliminarMonitoreoPgmfea(MonitoreoPgmfeaEntity monitoreo) throws Exception {
        return monitoreoRepository.EliminarMonitoreoPgmfea(monitoreo);
    }

    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Lista por filtro  monitoreo PGMFEA}
     * @param: MonitoreoEntity
     */

    @Override
    public ResultClassEntity ListarMonitoreoPgmfea(MonitoreoEntity monitoreo) throws Exception {
        return monitoreoRepository.ListarMonitoreoPgmfea(monitoreo);
    }
    /**
     * @autor: Ivan Minaya. [09-06-2021]
     * @descripción: {Obtener monitoreo PGMFEA}
     * @param: MonitoreoPgmfeaEntity
     */

    @Override
    public ResultClassEntity ObtenerMonitoreoPgmfea(MonitoreoPgmfeaEntity monitoreo) throws Exception {
        return monitoreoRepository.ObtenerMonitoreoPgmfea(monitoreo);
    }

    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Registra la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity RegistrarMonitoreo(MonitoreoEntity monitoreo) {
        return monitoreoRepository.RegistrarMonitoreo(monitoreo);
    }


    /**
     * @autor:  Rafael Azaña [23-11-2021]
     * @descripción: {Registrar monitoreo}
     * @param: MonitoreoEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @Override
    public ResultClassEntity RegistrarMonitoreoPOCC(List<MonitoreoEntity> list) throws Exception {
        return monitoreoRepository.RegistrarMonitoreoPOCC(list);
    }


    /**
     * @autor:  Rafael Azaña [23-11-2021]
     * @descripción: {Actualizar monitoreo}
     * @param: MonitoreoEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @Override
    public ResultClassEntity ActualizarMonitoreoPOCC(List<MonitoreoEntity> list) throws Exception {
        return monitoreoRepository.ActualizarMonitoreoPOCC(list);
    }


    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Actualiza la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity ActualizarMonitoreo(MonitoreoEntity monitoreo) {
        return monitoreoRepository.ActualizarMonitoreo(monitoreo);
    }

    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Lista la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity<MonitoreoEntity> listarMonitoreo(MonitoreoEntity monitoreo) {
        return monitoreoRepository.listarMonitoreo(monitoreo);
    }

    /**
     * @autor: Rafael Azaña DB [22-11-2021]
     * @modificado:
     * @descripción: {Lista la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity<List<MonitoreoEntity>> listarMonitoreoPOCC(Integer idPlanManejo, String codigoMonitoreo)
            throws Exception {

        ResultClassEntity<List<MonitoreoEntity>> result = new ResultClassEntity<>();

        result.setData(monitoreoRepository.listarMonitoreoPOCC(idPlanManejo,codigoMonitoreo));
        result.setSuccess(true);
        result.setMessage("Lista Monitoreo");

        return result;
    }


    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Eliminar el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity EliminarDetalleMonitoreo(MonitoreoDetalleEntity detalle) {
        return monitoreoRepository.EliminarDetalleMonitoreo(detalle);
    }

    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Eliminar el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity EliminarMonitoreoCabera(MonitoreoEntity monitoreo) {
        return monitoreoRepository.EliminarMonitoreoCabera(monitoreo);
    }
}

