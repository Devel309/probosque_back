package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.PlanGeneralManejoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.UbigeoArffsEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.PGMFEnvioCorreo;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoForestalContratoDto;

import javax.servlet.http.HttpServletRequest;

public interface PlanGeneralManejoForestalService {

    ResultClassEntity registrarPlanManejoForestal(List<PlanGeneralManejoForestalEntity> list) throws Exception;

   // ResultClassEntity<PlanManejoForestalContratoDto> obtenerContrato(Integer idContrato) throws Exception;
    ResultClassEntity obtenerContrato(ContratoEntity obj) throws Exception;

    ResultClassEntity<List<PlanManejoForestalContratoDto>> listarFiltroContrato() throws Exception;

    ResultEntity<PlanManejoContratoEntity>listarContratosVigentes(Integer idContrato);
    ResultClassEntity actualizarPlanManejoForestal(PlanGeneralManejoForestalEntity obj);
    ResultClassEntity enviarEvaluacion(PGMFEnvioCorreo obj, HttpServletRequest request1) throws Exception;
    ResultEntity<UbigeoArffsEntity>listarUbigeoContratos(Integer idContrato);
}
