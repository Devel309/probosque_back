package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.PropertiesEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadAprovechamientoEntity;

public interface UnidadAprovechamientoService {
    ResultClassEntity guardarUnidadAprovechamiento(UnidadAprovechamientoEntity obj) throws Exception;
    ResultEntity<PropertiesEntity> listar();
    ResultEntity<PropertiesEntity> buscarByIdProcesoOferta(Integer id);
    ResultEntity<PropertiesEntity> buscarByIdPlanManejo(Integer id);
}
