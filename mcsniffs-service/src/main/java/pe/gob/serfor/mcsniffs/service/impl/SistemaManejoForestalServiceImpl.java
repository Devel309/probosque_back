package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SistemaManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.SistemaManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.SistemaManejoForestalRepository;
import pe.gob.serfor.mcsniffs.service.SistemaManejoForestalService;

@Service("SistemaManejoForestalService")
public class SistemaManejoForestalServiceImpl implements SistemaManejoForestalService {

	@Autowired
	private SistemaManejoForestalRepository smfRepo;

	@Override
	public ResultClassEntity<SistemaManejoForestalDto> obtener(Integer idPlanManejo, String codigoProceso)
			throws Exception {
		return smfRepo.obtener(idPlanManejo, codigoProceso);
	}

	@Override
	public ResultClassEntity<List<SistemaManejoForestalDto>> listar(Integer idSistemaManejoForestal,
			Integer idPlanManejo, String descripcionFinMaderable, String descripcionCicloCorta,
			String descripcionFinNoMaderable, String descripcionCicloAprovechamiento) throws Exception {
		return smfRepo.listar(idSistemaManejoForestal, idPlanManejo, descripcionFinMaderable, descripcionCicloCorta,
				descripcionFinNoMaderable, descripcionCicloAprovechamiento);
	}

	@Override
	public ResultClassEntity<SistemaManejoForestalDto> guardar(SistemaManejoForestalDto request) throws Exception {
		return smfRepo.guardar(request);
	}

	@Override
	public ResultClassEntity<SistemaManejoForestalDetalleEntity> guardarDetalle(
			SistemaManejoForestalDetalleEntity request) throws Exception {
		return smfRepo.guardarDetalle(request);
	}

	@Override
	public ResultClassEntity<Integer> eliminarDetalle(Integer idDetalle, Integer idUsuario) throws Exception {
		return smfRepo.eliminarDetalle(idDetalle, idUsuario);
	}

}
