package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

import pe.gob.serfor.mcsniffs.entity.Dto.DescargarSolicitudPlantacionForestal.DescargarSolicitudPlantacionForestalDto;

public interface ArchivoPDFSolPlantacionForestalService{

    ByteArrayResource consolidadoSolPlantForestal_PDF(DescargarSolicitudPlantacionForestalDto obj) throws Exception;

}
