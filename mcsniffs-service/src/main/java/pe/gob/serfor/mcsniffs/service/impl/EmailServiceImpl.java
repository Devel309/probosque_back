package pe.gob.serfor.mcsniffs.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.EvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.service.EmailService;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionDetalleRepository evaluacionDetalleRepository;


    @Override
    public boolean sendEmail(EmailEntity emailModel) {
        LOGGER.info("EmailBody: {}", emailModel.toString());
        return sendEmailTool(emailModel.getContent(),emailModel.getEmail(), emailModel.getSubject(),emailModel.getEmailCc());
    }



    @Override
    public boolean sendCorreo(EvaluacionPermisoForestalDto dto) throws Exception {
        EvaluacionPermisoForestalDto d = new EvaluacionPermisoForestalDto();
        d.setIdPermisoForestal(dto.getIdPermisoForestal());
        d.setCodigoEvaluacion(dto.getCodigoEvaluacion());

        List<EvaluacionPermisoForestalDto> lista = evaluacionRepository.listarEvaluacionPermisoForestal(d);
        EvaluacionPermisoForestalDetalleDto m = null;
        int contador=0;

        for (EvaluacionPermisoForestalDto element : lista) {
            m = new EvaluacionPermisoForestalDetalleDto();

            m.setIdEvaluacionPermiso(element.getIdEvaluacionPermiso());
            m.setCodigoEvaluacionDet(dto.getCodigoEvaluacionDet());
            m.setCodigoEvaluacionDetSub(dto.getCodigoEvaluacionDetSub());
            m.setCodigoEvaluacionDetPost(dto.getCodigoEvaluacionDetPost());
            element.setListarEvaluacionPermisoDetalle(evaluacionDetalleRepository.listarEvaluacionPermisoForestaDetalle(m));
            for (EvaluacionPermisoForestalDetalleDto elementDetalle: element.getListarEvaluacionPermisoDetalle()) {
                if(elementDetalle.getConforme().equals("EPSFVALOB")){
                    contador++;
                }
            }
        }
        EmailEntity emailModel= new EmailEntity();
        if(contador>0){
            String[] email = new String[1];
            EvaluacionPermisoForestalDto d2 = new EvaluacionPermisoForestalDto();
            d2.setIdPermisoForestal(dto.getIdPermisoForestal());
            List<EvaluacionPermisoForestalDto> listaUsuario = evaluacionRepository.listarEvaluacionPermisoForestalUsuario(d2);
            for (EvaluacionPermisoForestalDto elementUsuario: listaUsuario) {
                email[0] = elementUsuario.getResponsable();
            }

            emailModel.setContent("Su solicitud de permiso forestal ha sido observada");
            emailModel.setEmail(email);
            emailModel.setSubject("OBSERVADO");
            emailModel.setEmailCc(email);
        }

        //return sendEmailTool(emailModel.getContent(),emailModel.getEmail(), emailModel.getSubject(),emailModel.getEmailCc());
        return sendEmailTool(emailModel.getContent(),emailModel.getEmail(), emailModel.getSubject(),emailModel.getEmailCc());
    }



    @Override
    public boolean sendEmailWithFile(EmailEntity emailModel) {
        boolean send = false;
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            message.setFrom(new InternetAddress(from));
            // message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailModel.getEmail()));
            helper.setTo(emailModel.getEmail());
            message.setSubject(emailModel.getSubject());

            //3) create MimeBodyPart object and set your message text
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText(emailModel.getContent());

            //4) create new MimeBodyPart object and set DataHandler object to this object
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();

            DataSource source = new FileDataSource(emailModel.getAdjunto().getAbsolutePath());
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(emailModel.getAdjunto().getName());

            //5) create Multipart object and add MimeBodyPart objects to this object
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);
            multipart.addBodyPart(messageBodyPart2);

            //6) set the multiplart object to the message object
            message.setContent(multipart);

            //7) send message
            sender.send(message);

            System.out.println("message sent....");
            send = true;
            LOGGER.info("Mail enviado!");
        } catch (MessagingException e) {
            LOGGER.error("Hubo un error al enviar el mail: {}", e);
        }
        return send;
    }

    private boolean sendEmailTool(String textMessage, String[] email,String subject,String[] emailCc) {
        boolean send = false;
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setTo(email);
            message.setFrom(new InternetAddress(from));

            helper.setText(textMessage, true);
            helper.setSubject(subject);
            if(emailCc!=null){
                helper.setCc(emailCc);
            }
            sender.send(message);
            send = true;
            LOGGER.info("Mail enviado!");
        } catch (MessagingException e) {
            LOGGER.error("Hubo un error al enviar el mail: {}", e);
        }
        return send;
    }

    
	/*private boolean sendEmailTool(String textMessage, String email,String subject, String Adjunto, String nombreAdjunto, String[] documentos) {
		boolean send = false;
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			message.setFrom(new InternetAddress(from));
			helper.setTo(email);
			helper.setText(textMessage, true);
			helper.setSubject(subject);
			if( Adjunto.equals("no") == false || Adjunto == null){
				Adjunto = Adjunto.substring(51);
				byte[] doc = Base64.getDecoder().decode(Adjunto);
				ByteArrayResource file  = new ByteArrayResource(doc);
				helper.addAttachment(nombreAdjunto, file);
			}

			//adjuntar documentos
			//genLogger.loggInfo("adjunto: ", documentos.length);	
			if(documentos != null ){
				
				for (String id : documentos) {
					Integer idd =  Integer.parseInt(id);
					LOGGER.loggInfo("adjunto: ", idd.toString());	
					AdjuntoModel result = adjuntoServiceImpl.findEntity(idd);
					byte[] fileByte;
					try {
						fileByte = blobstoreimpl.getFile(result.getNombreInterno());
						ByteArrayResource file  = new ByteArrayResource(fileByte);
						helper.addAttachment(result.getNombre(), file);
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
			}

			sender.send(message);
			send = true;
			LOGGER.info("Mail enviado!");
		} catch (MessagingException e1) {
			LOGGER.error("Hubo un error al enviar el mail: {}", e1.getMessage());
			e1.printStackTrace();
		}		
		
		return send;
	}*/

    public boolean enviarEmailEstandar(String email, String nombres, String asuntoMail,String mensajeMail) throws Exception {

        boolean smail = false;

        if (email != null && nombres != null) {
            String[] mails = new String[1];
            mails[0] = email;
            EmailEntity mail = new EmailEntity();
            mail.setSubject(asuntoMail);
            mail.setEmail(mails);
            String body = Constantes.BODY_HTML_EMAIL;

            body = body.replace("[NOMBRES_USUARIO_ACTUAL]", nombres.toUpperCase());
            body = body.replace("[MENSAJE_DEL_PROCESO]", mensajeMail);
            body = body.replace("[NOTA_COMPLEMENTARIA]",
                    "Tenga presente que usted está recibiendo este correo porque ha realizado la creación de una solicitud en el aplicativo MCSNIFFS");

            mail.setContent(body);
            smail = this.sendEmail(mail);

        }

        return smail;
    }

}
