package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionActividadRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionActividadService;

@Service("SolicitudConcesionActividadService")
public class SolicitudConcesionActividadServiceImpl implements SolicitudConcesionActividadService {

    @Autowired
    private SolicitudConcesionActividadRepository solicitudConcesionActividadRepository;

    @Override
    public ResultClassEntity listarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj) throws Exception{
        return solicitudConcesionActividadRepository.listarActividadSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity obtenerInformacionActividadFuenteFinanciamiento(SolicitudConcesionActividadEntity obj) throws Exception{
        return solicitudConcesionActividadRepository.listarActividadSolicitudConcesion(obj);
    }
    @Override
    public ResultClassEntity registrarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj) throws Exception{
        return solicitudConcesionActividadRepository.registrarActividadSolicitudConcesion(obj);
    }

    @Override
    public ResultClassEntity registrarInformacionActividadFuenteFinanciamiento(List<SolicitudConcesionActividadEntity> obj) throws Exception{

        ResultClassEntity result = new ResultClassEntity<>();
        for (SolicitudConcesionActividadEntity solicitudConcesionActividadEntity : obj) {

            if (solicitudConcesionActividadEntity.getIdSolicitudConcesionActividad()==null) {
                result = solicitudConcesionActividadRepository.registrarActividadSolicitudConcesion(solicitudConcesionActividadEntity);
            }else{
                result = solicitudConcesionActividadRepository.actualizarActividadSolicitudConcesion(solicitudConcesionActividadEntity);
            }                        
        }

        return result;   
    }

    @Override
    public ResultClassEntity actualizarInformacionActividadFuenteFinanciamiento(SolicitudConcesionActividadEntity obj) throws Exception{
        return solicitudConcesionActividadRepository.actualizarActividadSolicitudConcesion(obj);
    }
}
