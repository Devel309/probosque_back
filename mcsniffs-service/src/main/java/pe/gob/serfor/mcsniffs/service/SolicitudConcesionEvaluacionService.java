package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface SolicitudConcesionEvaluacionService {

    ResultClassEntity listarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj)
            throws Exception;

    ResultClassEntity actualizarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj)
            throws Exception;

    ResultClassEntity listarSolicitudConcesionEvaluacionDetalle(SolicitudConcesionEvaluacionDto obj)
            throws Exception;

    ResultArchivoEntity descargarSolicitudConcesionEvaluacion(DescargarSolicitudConcesionEvaluacionDto obj)
            throws Exception;
}
