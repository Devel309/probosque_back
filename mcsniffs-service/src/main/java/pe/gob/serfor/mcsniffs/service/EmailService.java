package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;

public interface EmailService {
    boolean sendEmail(EmailEntity emailModel);
    boolean sendEmailWithFile(EmailEntity emailModel);
    boolean sendCorreo(EvaluacionPermisoForestalDto evaluacionPermisoForestalDto) throws Exception;
    boolean enviarEmailEstandar(String email, String nombres, String asuntoMail,String mensajeMail) throws Exception;
}
