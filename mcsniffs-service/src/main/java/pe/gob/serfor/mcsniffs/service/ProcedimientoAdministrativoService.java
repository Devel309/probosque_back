package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ProcedimientoAdministrativo.ProcedimientoAdministrativoDto;



public interface ProcedimientoAdministrativoService {
    ResultClassEntity<List<ProcedimientoAdministrativoDto>> listarPlanManejoEvaluacion(ProcedimientoAdministrativoDto dto) throws Exception;
}
