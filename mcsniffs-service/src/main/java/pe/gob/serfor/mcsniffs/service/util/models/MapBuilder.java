package pe.gob.serfor.mcsniffs.service.util.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDetalle;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.service.constant.*;

public class MapBuilder {

    private MapBuilder() {
    }

    public static Map<String, String> keysInfoGeneral(InformacionGeneralDEMAEntity o) {
        Map<String, String> keys = new HashMap<>();
        keys.put(DemaType.COMUNIDAD_NOMBRE, o.getNombreComunidad());
        String nombres = toString(o.getNombresJefeComunidad()) + " " + toString(o.getApellidoPaternoJefeComunidad())
                + " " + toString(o.getApellidoMaternoJefeComunidad());
        keys.put(DemaType.JEFE_NOMBRE, nombres);
        keys.put(DemaType.JEFE_DNI, o.getDniJefecomunidad());
        keys.put(DemaType.FEDERACION, o.getFederacionComunidad());
        keys.put(DemaType.DEPARTAMENTO, toString(o.getDepartamento()));
        keys.put(DemaType.PROVINCIA, toString(o.getProvincia()));
        keys.put(DemaType.DISTRITO, toString(o.getDistrito()));
        keys.put(DemaType.CUENCA, o.getCuenca());
        keys.put(DemaType.SUB_CUENCA, o.getSubCuenca());
        keys.put(DemaType.NRO_ANEXOS, toString(o.getNroAnexosComunidad()));
        keys.put(DemaType.NRO_RESOLUCION, o.getNroResolucionComunidad());
        keys.put(DemaType.NRO_TITULO, o.getNroTituloPropiedadComunidad());
        keys.put(DemaType.NRO_FAMILIAS, toString(o.getNroTotalFamiliasComunidad()));
        keys.put(DemaType.NRO_RUC, o.getNroRucComunidad());
        keys.put(DemaType.FECHA_INICIO, toString(o.getFechaInicioDema()));
        keys.put(DemaType.FECHA_FIN, toString(o.getFechaFinDema()));
        String elaborador = toString(o.getNombreElaboraDema()) + " " + toString(o.getApellidoPaternoElaboraDema()) + " "
                + toString(o.getApellidoMaternoElaboraDema());
        keys.put(DemaType.ELABORADOR_NOMBRE, elaborador);
        keys.put(DemaType.ELABORADOR_DNI, o.getDniElaboraDema());
        keys.put(DemaType.NRO_FAMILIAS_DEMA, toString(o.getNroTotalFamiliasComunidad()));
        keys.put(DemaType.MADERABLE_TOTAL, toString(o.getNroArbolesMaderables()));
        keys.put(DemaType.MADERABLE_SEMILLERO, toString(o.getNroArbolesMaderablesSemilleros()));
        keys.put(DemaType.MADERABLE_SUPERFICIE, o.getSuperficieHaMaderables());
        keys.put(DemaType.MADERABLE_VOLUMEN, o.getVolumenM3rMaderables());
        keys.put(DemaType.NO_MADERABLE_TOTAL, toString(o.getNroArbolesNoMaderables()));
        keys.put(DemaType.NO_MADERABLE_SEMILLEROS, toString(o.getNroArbolesNoMaderablesSemilleros()));
        keys.put(DemaType.NO_MADERABLE_SUPERFICIE, o.getSuperficieHaNoMaderables());
        keys.put(DemaType.NO_MADERABLE_VOLUMEN, o.getVolumenM3rNoMaderables());
        keys.put(DemaType.TOTAL_INGRESOS, toString(o.getTotalIngresosEstimado()));
        keys.put(DemaType.TOTAL_COSTOS, toString(o.getTotalCostoEstimado()));
        keys.put(DemaType.TOTAL_UTILIDADES, toString(o.getTotalUtilidadesEstimado()));
        return keys;
    }

    /**
     *
     * @author Rafael Azaña
     */
    public static Map<String, String> keysInfoGeneralPGMFA(InformacionGeneralDEMAEntity o) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Map<String, String> keys = new HashMap<>();
        keys.put(PgmfaType.COMUNIDAD_NOMBRE, toString(o.getNombreElaboraDema()));
        String nombres = toString(o.getNombresJefeComunidad()) + " " + toString(o.getApellidoPaternoJefeComunidad())
                + " " + toString(o.getApellidoMaternoJefeComunidad());
        keys.put(PgmfaType.JEFE_NOMBRE, toString(o.getRepresentanteLegal()));
        keys.put(PgmfaType.JEFE_DNI, toString(o.getDescripcion()));
        keys.put(PgmfaType.JEFE_RUC, toString(o.getNroRucComunidad()));
        keys.put(PgmfaType.FEDERACION, toString(o.getFederacionComunidad()));
        keys.put(PgmfaType.REGENTE, toString(o.getRegente()!= null ? o.getRegente().getNombres() + " " + o.getRegente().getApellidos():""));
        keys.put(PgmfaType.NRO_CERTIFICADO, "");
        keys.put(PgmfaType.NRO_INSCRIPCION, toString(o.getNroResolucionComunidad()));
        keys.put(PgmfaType.FECHA_PRESTACION, o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "");
        keys.put(PgmfaType.FECHA_INICIO, o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "");
        keys.put(PgmfaType.FECHA_FIN, o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "");

        // keys.put(PgmfaType.FECHA_PRESTACION, toString(formatter.format(o.getFechaElaboracionPmfi())));
       // keys.put(PgmfaType.FECHA_INICIO, toString(formatter.format(o.getFechaInicioDema())));
       // keys.put(PgmfaType.FECHA_FIN, toString(formatter.format(o.getFechaFinDema())));

        keys.put(PgmfaType.OBJETIVOS_ESPECIFICOS, "");
        keys.put(PgmfaType.POTENCIAL_MADERABLE, toString(o.getAreaTotal()));
        keys.put(PgmfaType.DEPARTAMENTO, toString(o.getDepartamento()));
        keys.put(PgmfaType.PROVINCIA, toString(o.getProvincia()));
        keys.put(PgmfaType.DISTRITO, toString(o.getDistrito()));
        keys.put(PgmfaType.NRO_SECTORES, toString(o.getNroAnexosComunidad()));
        keys.put(PgmfaType.SUPERFICIE_COMUNIDAD, toString(o.getSuperficieHaMaderables()));
        keys.put(PgmfaType.AREA_MANEJO, toString(o.getAreaTotal()));
        return keys;
    }


    /**
     *
     * @author Rafael Azaña
     */
    public static Map<String, String> keysInfoGeneralPOCC(InformacionGeneralDEMAEntity o,String vigencia) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Map<String, String> keys = new HashMap<>();
        keys.put(PoccType.TITULAR_NOMBRE, toString(o.getNombreElaboraDema()));
        String nombres = toString(o.getNombresJefeComunidad()) + " " + toString(o.getApellidoPaternoJefeComunidad())
                + " " + toString(o.getApellidoMaternoJefeComunidad());
        keys.put(PoccType.REPRESENTANTE_NOMBRE, toString(o.getRepresentanteLegal()));
        keys.put(PoccType.NRO_CONTRATO, toString(o.getNroResolucionComunidad()));
        keys.put(PoccType.REPRESENTANTE_DNI, toString(o.getDniElaboraDema()));
        keys.put(PoccType.REPRESENTANTE_RUC, toString(o.getNroRucComunidad()));
        keys.put(PoccType.DISTRITO, toString(o.getDireccionLegalTitular()));
        keys.put(PoccType.DEPARTAMENTO, toString(o.getDepartamentoRepresentante()));
        keys.put(PoccType.PROVINCIA, toString(o.getProvinciaRepresentante()));
        keys.put(PoccType.OBJETIVO, toString(o.getDetalle()));
        keys.put(PoccType.NRO_PO, toString(o.getObservacion()));
        keys.put(PoccType.FECHA_PRESENTACION, o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "");
        keys.put(PoccType.DURACION_PO, toString(vigencia));
        keys.put(PoccType.FECHA_INICIO, o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "");
        keys.put(PoccType.FECHA_FIN, o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "");
        keys.put(PoccType.NRO_PC, toString(o.getNroAnexosComunidad()));
        keys.put(PoccType.AREA_TOTAL, toString(o.getSuperficieHaMaderables()));
        keys.put(PoccType.AREA_BOSQUE, toString(o.getSuperficieHaNoMaderables()));
        keys.put(PoccType.AREA_PROTECCION, toString(o.getVolumenM3rMaderables()));
        keys.put(PoccType.VOLUMEN_TOTAL, toString(o.getVolumenM3rNoMaderables()));
        keys.put(PoccType.VOLUMEN_CANTIDAD, toString(o.getTotalCostoEstimado()));
        keys.put(PoccType.NOMBRE_REGENTE, toString(o.getRegente().getNombres()));
        keys.put(PoccType.DOMICILIO_LEGAL, toString(o.getDireccionLegalRepresentante()));
        keys.put(PoccType.CIP, toString(o.getRegente().getNumeroLicencia()));
        keys.put(PoccType.CONTRATO_TITULAR, toString(o.getRegente().getContratoSuscrito()));
        keys.put(PoccType.NRO_INSCRIPCION, toString(o.getRegente().getNumeroInscripcion()));

        return keys;
    }


    /**
     *
     * @author Rafael Azaña
     */
    public static Map<String, String> keysInfoGeneralPOAC(InformacionGeneralDEMAEntity o) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Map<String, String> keys = new HashMap<>();
        keys.put(PoacType.COMUNIDAD_NOMBRE, toString(o.getNombreElaboraDema()));
        keys.put(PoacType.REPRESENTANTE_NOMBRE, toString(o.getNombreRepresentante())+" "+toString(o.getApellidoPaternoRepresentante())+" "+toString(o.getApellidoMaternoRepresentante()) );
        keys.put(PoacType.EMPRESA, toString(o.getObservacion()));
        keys.put(PoacType.DEPARTAMENTO, toString(o.getDepartamento()));
        keys.put(PoacType.PROVINCIA, toString(o.getProvincia()));
        keys.put(PoacType.DISTRITO, toString(o.getDistrito()));
        keys.put(PoacType.INGENIERO_NOMBRE, toString(o.getRegente().getNombres())+" "+toString(o.getRegente().getApellidos()));
        keys.put(PoacType.CERTIFICADO, toString(""));
        keys.put(PoacType.INSCRIPCION, toString(o.getRegente().getNumeroLicencia()));
        keys.put(PoacType.AREA_APROVECHAMIENTO, toString(o.getAreaTotal()));
        keys.put(PoacType.ANIO, toString(o.getVigencia()));
        keys.put(PoacType.FECHA, o.getFechaElaboracionPmfi() != null ? formatter.format(o.getFechaElaboracionPmfi()) : "");

        return keys;
    }

    /**
     *
     * @author Jason Retamozo
     */
    public static Map<String, String> keysInfoGeneralPMFIC(InformacionGeneralDEMAEntity o) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Map<String, String> keys = new HashMap<>();
        //1.1
        keys.put(PmficType.PERMISO, toString(o.getIdPermisoForestal()));
        keys.put(PmficType.COMUNIDAD_NOMBRE, toString(o.getNombreElaboraDema()));
        keys.put(PmficType.REPRESENTANTE_NOMBRE, toString(o.getNombreRepresentante()+" "+o.getApellidoPaternoRepresentante()+" "+o.getApellidoMaternoRepresentante()));
        keys.put(PmficType.DNI, toString(o.getDocumentoRepresentante()));
        keys.put(PmficType.FEDERACION, toString(o.getFederacionComunidad()));
        keys.put(PmficType.DEPARTAMENTO, toString(o.getDepartamento()));
        keys.put(PmficType.PROVINCIA, toString(o.getProvincia()));
        keys.put(PmficType.DISTRITO, toString(o.getDistrito()));
        keys.put(PmficType.CUENCA, "");
        keys.put(PmficType.SUB_CUENCA, "");
        keys.put(PmficType.AREA, toString(o.getAreaTotal()));
        keys.put(PmficType.NRO_ANEXOS, toString(o.getNroAnexosComunidad()));
        keys.put(PmficType.NRO_RESOLUCION, toString(o.getNroResolucionComunidad()));
        keys.put(PmficType.TITULOS, toString(o.getNroTituloPropiedadComunidad()));
        keys.put(PmficType.RUC, toString(o.getNroRucComunidad()));
        keys.put(PmficType.FAMILIAS, toString(o.getNroTotalFamiliasComunidad()));
        //1.2
        keys.put(PmficType.VIGENCIA, toString(o.getVigencia()));
        keys.put(PmficType.FECHA_INICIO, o.getFechaInicioDema() != null ? formatter.format(o.getFechaInicioDema()) : "");
        keys.put(PmficType.FECHA_FINAL, o.getFechaFinDema() != null ? formatter.format(o.getFechaFinDema()) : "");
        keys.put(PmficType.AREA_TOTAL_UMF, toString(o.getAreaBosqueProduccionForestal()));
        //1.3
        keys.put(PmficType.REGENTE, toString(o.getRegente().getNombres()+" "+o.getRegente().getApellidos()));
        keys.put(PmficType.DNI_REGENTE, toString(o.getRegente().getNumeroDocumento()));
        keys.put(PmficType.DOMICILIO, toString(o.getRegente().getDomicilioLegal()));
        keys.put(PmficType.CERTIFICADO, toString(""));
        keys.put(PmficType.INSCRIPCION, toString(o.getRegente().getNumeroLicencia()));
        //1.4
        keys.put(PmficType.NRO_FAMILIAS_APROV, toString(o.getNroPersonasInvolucradas()));
        keys.put(PmficType.NRO_ARBOLES_SEMILLEROS, toString(o.getNroArbolesMaderablesSemilleros()));
        keys.put(PmficType.NRO_ARBOLES_MADERABLES, toString(o.getNroArbolesMaderables()));
        keys.put(PmficType.SUPERFICIE, toString(o.getSuperficieHaMaderables()));
        keys.put(PmficType.VOLUMEN_TOTAL, toString(o.getVolumenM3rMaderables()));
        //1.5
        keys.put(PmficType.NRO_ARBOLES_SEMILLEROS_NOM, toString(o.getNroArbolesNoMaderablesSemilleros()));
        keys.put(PmficType.NRO_ARBOLES_NO_MADERABLES, toString(o.getNroArbolesNoMaderables()));
        keys.put(PmficType.SUPERFICIE_NOM, toString(o.getSuperficieHaNoMaderables()));
        keys.put(PmficType.VOLUMEN_TOTAL_NOM, toString(o.getVolumenM3rNoMaderables()));
        //1.6
        int i=1;
        for(InformacionGeneralDetalle det: o.getLstUmf()){
            keys.put("1si"+i,det.getDetalle().equals("SI")?"X":"");
            keys.put("1no"+i,det.getDetalle().equals("NO")?"X":"");
            keys.put("1noap"+i,det.getDetalle().equals("No Aplica")?"X":"");
            i++;
        }
        //1.7
        keys.put(PmficType.EMPRESA, toString(o.getNombreElaboraDema()));
        keys.put(PmficType.DOMICILIO_FISCAL, toString(o.getDireccionLegalTitular()));
        keys.put(PmficType.REPRESENTANTE_EMPRESA, toString(o.getNombreRepresentante()+" "+o.getApellidoPaternoRepresentante()+" "+o.getApellidoMaternoRepresentante()));
        keys.put(PmficType.DNI_REPRESENTANTE, toString(o.getDocumentoRepresentante()));
        keys.put(PmficType.DOMICILIO_REPRESENTANTE, toString(o.getDireccionLegalRepresentante()));
        keys.put(PmficType.RUC_EMPRESA, toString(o.getDniElaboraDema()));
        keys.put(PmficType.COPIA_CONTRATO, "");

        return keys;
    }


    /**
     *
     * @author Jason Retamozo / 22-03-2022
     */
    public static Map<String, String> keysGeneralInformeEvaluacion(Object o) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Map<String, String> keys = new HashMap<>();
        //TITULO
        keys.put(InformeEvaluacionType.TITULO, toString(""));


        return keys;
    }


    public static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }
}
