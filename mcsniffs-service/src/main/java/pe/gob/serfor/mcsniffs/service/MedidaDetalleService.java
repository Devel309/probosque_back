package pe.gob.serfor.mcsniffs.service;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDetalleDto;

public interface MedidaDetalleService {
    ResultClassEntity<List<MedidaDetalleDto>> listarMedidaDetalle(MedidaDetalleDto dto) throws Exception;
    ResultClassEntity registrarMedidaDetalle(List<MedidaDetalleDto> dto) throws Exception;
    ResultClassEntity eliminarMedidaDetalle(List<MedidaDetalleDto> dto) throws Exception;
}
