package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.MunicipioPersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.MunicipioPersonaRepository;
import pe.gob.serfor.mcsniffs.service.MunicipioPersonaService;
@Service("MunicipioPersonaService")
public class MunicipioPersonaServiceImpl implements MunicipioPersonaService {

    @Autowired
    private MunicipioPersonaRepository municipioPersonaRepository;

    @Override
    public ResultClassEntity listarMunicipioPersona(MunicipioPersonaEntity obj) throws Exception{
        return municipioPersonaRepository.listarMunicipioPersona(obj);
    }

    
}