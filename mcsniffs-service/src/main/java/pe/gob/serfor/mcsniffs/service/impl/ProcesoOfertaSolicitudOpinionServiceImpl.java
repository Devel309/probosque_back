package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoOfertaSolicitudOpinion.ProcesoOfertaSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaSolicitudOpinionRepository;
import pe.gob.serfor.mcsniffs.service.ProcesoOfertaSolicitudOpinionService;


@Service("ProcesoOfertaSolicitudOpinionService")
public class ProcesoOfertaSolicitudOpinionServiceImpl implements ProcesoOfertaSolicitudOpinionService {


    private static final Logger log = LogManager.getLogger(ProcesoOfertaSolicitudOpinionServiceImpl.class);

    @Autowired
    private ProcesoOfertaSolicitudOpinionRepository ProcesoOfertaSolicitudOpinionRepository;

    @Override
    public ResultClassEntity ListarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        return ProcesoOfertaSolicitudOpinionRepository.ListarProcesoOfertaSolicitudOpinion(param);
    }

    @Override
    public ResultClassEntity RegistrarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param){
        return ProcesoOfertaSolicitudOpinionRepository.RegistrarProcesoOfertaSolicitudOpinion(param);
    }

    @Override
    public ResultClassEntity ActualizarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param){
        return ProcesoOfertaSolicitudOpinionRepository.ActualizarProcesoOfertaSolicitudOpinion(param);
    }

    @Override
    public ResultClassEntity EliminarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        return ProcesoOfertaSolicitudOpinionRepository.EliminarProcesoOfertaSolicitudOpinion(param);
    }

    @Override
    public ResultClassEntity ObtenerProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        return ProcesoOfertaSolicitudOpinionRepository.ObtenerProcesoOfertaSolicitudOpinion(param);
    }
}
