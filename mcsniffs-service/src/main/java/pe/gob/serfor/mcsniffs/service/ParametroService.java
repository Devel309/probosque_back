package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;

import java.util.List;

public interface ParametroService {

    List<ParametroValorEntity> ListarPorCodigoParametroValor(ParametroValorEntity param) throws Exception;

}
