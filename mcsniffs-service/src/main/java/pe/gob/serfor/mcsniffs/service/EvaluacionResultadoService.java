package pe.gob.serfor.mcsniffs.service;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.EvaluacionResultadoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionResultadoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;


public interface EvaluacionResultadoService {

    /**
     * @autor: Rafael Azaña [16/10-2021]
     * @modificado:
     * @descripción: {CRUD DEL RESULTADO DE LA EVALUACION}
     * @param:EvaluacionResultadoDetalleDto
     */

    ResultClassEntity<List<EvaluacionResultadoDetalleDto>> ListarEvaluacionResultado(Integer idPlanManejo, String codResultado ,String subCodResultado ) throws Exception;
    ResultClassEntity RegistrarEvaluacionResultado(List<EvaluacionResultadoEntity> list) throws Exception;
    ResultClassEntity EliminarEvaluacionResultado(EvaluacionResultadoDetalleDto evaluacionResultadoDetalleDto) throws Exception;


}
