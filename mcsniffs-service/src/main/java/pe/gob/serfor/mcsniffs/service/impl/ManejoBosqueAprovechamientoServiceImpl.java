package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueAprovechamientoService;
@Service("ManejoBosqueAprovechamientoService")
public class ManejoBosqueAprovechamientoServiceImpl implements ManejoBosqueAprovechamientoService {


    private static final Logger log = LogManager.getLogger(ManejoBosqueAprovechamientoServiceImpl.class);

    @Autowired
    private ManejoBosqueAprovechamientoRepository manejoBosqueAprovechamientoRepository;

    @Override
    public ResultClassEntity RegistrarManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception {
        return manejoBosqueAprovechamientoRepository.RegistrarManejoBosqueAprovechamiento(manejoBosqueAprovechamiento);
    }

    @Override
    public ResultClassEntity ObtenerManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception {
        return manejoBosqueAprovechamientoRepository.ObtenerManejoBosqueAprovechamiento(manejoBosqueAprovechamiento);
    }
}
