package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.DerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.ListarDerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.repository.DerechoAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.service.DerechoAprovechamientoService;



@Service("DerechoAprovechamientoService")
public class DerechoAprovechamientoServiceImpl implements DerechoAprovechamientoService {

    @Autowired
    DerechoAprovechamientoRepository derechoAprovechamientoRepository;

    @Override
    public Pageable<List<DerechoAprovechamientoDto>> listarDerechoAprovechamiento(ListarDerechoAprovechamientoDto dto) throws Exception {

        
        return derechoAprovechamientoRepository.listarDerechoAprovechamiento(dto);
    }

    
}
