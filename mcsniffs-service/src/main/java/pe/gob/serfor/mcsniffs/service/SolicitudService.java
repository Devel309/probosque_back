package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;

import java.util.List;

public interface SolicitudService {

	ResponseEntity ListarPorFiltroSolicitud() throws Exception;

	ResultClassEntity RegistrarSolicitudAmpliacion(SolicitudDto obj);
	ResultClassEntity RegistrarDocumentoAdjunto(SolicitudEntity obj);
	ResultEntity<SolicitudAdjuntosEntity> ObtenerArchivosSolicitudes(Integer IdSolicitud);
	ResultClassEntity AprobarRechazarSolicitud(SolicitudDto obj);
	ResultClassEntity listarSolicitudesAmpliacion(SolicitudDto param) ;
	ResultClassEntity<SolicitudDto> ObtenerSolicitudAmpliacion(SolicitudDto request);
	ResultEntity<TipoSolicitudEntity>ListarTipoSolicitud();
	ResultEntity<TipoDocumentoEntity>ListarTipoDocumento();
	ResultArchivoEntity DescagarDeclaracionJurada();
	ResultArchivoEntity DescargarPropuestaExploracion();
	ResultArchivoEntity DescargarFormatoAprobacion();
	ResultClassEntity<ImpugnacionNuevoGanadorEntity> ObtenerNuevoUsuarioGanador(FiltroSolicitudEntity filtro);
	ResultClassEntity ActualizarAprobadoSolicitud(SolicitudDto obj);
	ResultClassEntity RegistrarObservacionDocumentoAdjunto(List<DocumentoAdjuntoDto> obj);
	ResultArchivoEntity DescagarPlantillaResolucionResumen();
	ResultClassEntity ActualizarSolicitudAmpliacion(SolicitudDto obj);
}
