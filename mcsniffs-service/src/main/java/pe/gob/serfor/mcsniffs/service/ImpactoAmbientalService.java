package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ImpactoAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;


public interface ImpactoAmbientalService {
    ResultClassEntity ListarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) throws Exception;
    ResultClassEntity RegistrarImpactoAmbiental(List<ImpactoAmbientalDto> impactoAmbientalDtoList) throws Exception;
    ResultClassEntity EliminarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity);
    ResultClassEntity EliminarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity);
    ResultClassEntity ListarImpactoAmbientalPmfi(MedidasPmfiDto medidasPmfiDto) throws Exception;

    ResultClassEntity RegistrarImpactoAmbientalPmfi(ImpactoAmbientalPmfiDto impactoAmbientalPmfiDto) throws Exception;
}
