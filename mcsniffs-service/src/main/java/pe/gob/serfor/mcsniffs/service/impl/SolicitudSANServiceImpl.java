package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ProteccionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudSANRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;
import pe.gob.serfor.mcsniffs.service.ProteccionBosqueService;
import pe.gob.serfor.mcsniffs.service.SolicitudSANService;

import java.util.List;


    @Service("SolicitudSANService")
    public class SolicitudSANServiceImpl implements SolicitudSANService {


        private static final Logger log = LogManager.getLogger(SolicitudSANServiceImpl.class);

        @Autowired
        private SolicitudSANRepository solicitudSANRespository;

        @Autowired
        LogAuditoriaService logAuditoriaService;

    @Override
    public ResultClassEntity RegistrarSolicitudSAN(List<SolicitudSANEntity> list) throws Exception {



        return solicitudSANRespository.RegistrarSolicitudSAN(list);
    }


    @Override
    public List<SolicitudSANEntity> ListarSolicitudSAN(SolicitudSANEntity param) throws Exception {
        return solicitudSANRespository.ListarSolicitudSAN(param);
    }


    @Override
    public ResultClassEntity EliminarSolicitudSAN(SolicitudSANEntity param) throws Exception {

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_SOLICITUDSAN,
                LogAuditoria.ELIMINAR,
                LogAuditoria.DescripcionAccion.Eliminar.T_MVC_SOLICITUDSAN + param.getIdSolicitudSAN(),
                param.getIdUsuarioElimina());

        logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);

        return  solicitudSANRespository.EliminarSolicitudSAN(param);
    }

}
