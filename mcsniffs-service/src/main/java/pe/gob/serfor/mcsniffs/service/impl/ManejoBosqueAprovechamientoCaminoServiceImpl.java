package pe.gob.serfor.mcsniffs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoCaminoEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueAprovechamientoCaminoRepository;
import pe.gob.serfor.mcsniffs.service.ManejoBosqueAprovechamientoCaminoService;

import java.util.List;

@Service("ManejoBosqueAprovechamientoCaminoService")
public class ManejoBosqueAprovechamientoCaminoServiceImpl implements ManejoBosqueAprovechamientoCaminoService {


    private static final Logger log = LogManager.getLogger(ManejoBosqueAprovechamientoCaminoServiceImpl.class);

    @Autowired
    private ManejoBosqueAprovechamientoCaminoRepository manejoBosqueAprovechamientoCaminoRepository;

    @Override
    public ResultClassEntity RegistrarManejoBosqueAprovechamientoCamino(List<ManejoBosqueAprovechamientoCaminoEntity> list) throws Exception {
        return manejoBosqueAprovechamientoCaminoRepository.RegistrarManejoBosqueAprovechamientoCamino(list);
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoCaminoEntity manejoBosqueAprovechamientoCamino) throws Exception {
        return manejoBosqueAprovechamientoCaminoRepository.EliminarManejoBosqueAprovechamientoCamino(manejoBosqueAprovechamientoCamino);
    }

    @Override
    public ResultClassEntity ListarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamientoCamino) throws Exception {
        return manejoBosqueAprovechamientoCaminoRepository.ListarManejoBosqueAprovechamientoCamino(manejoBosqueAprovechamientoCamino);
    }
}
