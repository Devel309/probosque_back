package pe.gob.serfor.mcsniffs.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.EvaluacionResultadoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionResultadoDetalleDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionResultadoRepository;
import pe.gob.serfor.mcsniffs.service.EvaluacionResultadoService;

@Service("EvaluacionResultadoService")
public class EvaluacionResultadoServiceImpl implements EvaluacionResultadoService {

    @Autowired
    private EvaluacionResultadoRepository repository;


    /**
     * @autor: Rafael Azaña [16/10-2021]
     * @modificado:
     * @descripción: {CRUD DEL RESULTADO DE LA EVALUACIÓN}
     * @param:PotencialProduccionForestalDto
     */


    @Override
    public ResultClassEntity<List<EvaluacionResultadoDetalleDto>> ListarEvaluacionResultado( Integer idPlanManejo, String codResultado, String subCodResultado)
            throws Exception {

        ResultClassEntity<List<EvaluacionResultadoDetalleDto>> result = new ResultClassEntity<>();

        result.setData(repository.ListarEvaluacionResultado(idPlanManejo,codResultado,subCodResultado));
        result.setSuccess(true);
        result.setMessage("Lista Resultado de la Evaluación");

        return result;
    }


    @Override
    public ResultClassEntity RegistrarEvaluacionResultado(List<EvaluacionResultadoEntity> list) throws Exception {
        return repository.RegistrarEvaluacionResultado(list);
    }



    @Override
    public ResultClassEntity EliminarEvaluacionResultado(EvaluacionResultadoDetalleDto param) throws Exception {
        return  repository.EliminarEvaluacionResultado(param);
    }

}
