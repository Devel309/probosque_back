package pe.gob.serfor.mcsniffs.service.util;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Anexo2Dto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ListaEspecieDto;
import pe.gob.serfor.mcsniffs.service.util.models.MapBuilder;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import java.util.List;

public class DocUtilResolucion {

    private DocUtilResolucion() {
    }


    public static XWPFDocument setearInfoResolucion(InformacionGeneralDEMAEntity data, XWPFDocument doc) {
        try {
                for (XWPFParagraph p : doc.getParagraphs()) {
                    List<XWPFRun> runs = p.getRuns();
                    if (runs != null) {
                        for (XWPFRun r : runs) {
                            String text = r.getText(0);
                            if (text != null) {

                                if (text.contains("txComunidad")) {
                                    text = text.replace("txComunidad",data.getNombreElaboraDema()!= null? data.getNombreElaboraDema(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txNombre")) {
                                    text = text.replace("txNombre",data.getNombreRepresentante() != null ?
                                            data.getNombreRepresentante() + " "+data.getApellidoPaternoRepresentante()+", "+data.getApellidoMaternoRepresentante(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txNumero")) {
                                    text = text.replace("txNumero",data.getDocumentoRepresentante() != null ?data.getDocumentoRepresentante(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txRUC")) {
                                    text = text.replace("txRUC",data.getDniElaboraDema() != null ?data.getDniElaboraDema(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDstrito")) {
                                    text = text.replace("txDstrito",data.getDistrito() != null ?data.getDistrito(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txProvincia")) {
                                    text = text.replace("txProvincia",data.getProvincia() != null ?data.getProvincia(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDepartamento")) {
                                    text = text.replace("txDepartamento",data.getDepartamento() != null ?data.getDepartamento(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txResolucion")) {
                                    text = text.replace("txResolucion",data.getNroResolucionComunidad() != null ?data.getNroResolucionComunidad(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txSuperficie")) {
                                    text = text.replace("txSuperficie",data.getAreaTotal() != null ?data.getAreaTotal().toString(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txZona")) {
                                    text = text.replace("txZona",data.getZona() != null ?data.getZona(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDatum")) {
                                    text = text.replace("txDatum",data.getDatum() != null ?data.getDatum(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txDomicilio")) {
                                    text = text.replace("txDomicilio",data.getDireccionLegalTitular() != null ?data.getDireccionLegalTitular(): "");
                                    r.setText(text, 0);
                                }else if (text.contains("txVigencia")) {
                                    text = text.replace("txVigencia",data.getVigencia() != null ?data.getVigencia().toString(): "");
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }


            return doc;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public static void procesarCuadro1(List<InfBasicaAereaDetalleDto>  lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarCuadro1   "+lista.size());
            for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarCuadro1  text5 "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C1Vertice")) {
                                        text5 = text5.replace("C1Vertice",
                                                infBasicaAereaDetalleDto.getReferencia() != null
                                                        ?  infBasicaAereaDetalleDto.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C1X")) {
                                        text5 = text5.replace("C1X",
                                                infBasicaAereaDetalleDto.getCoordenadaEste() != null
                                                        ? ""+infBasicaAereaDetalleDto.getCoordenadaEste()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C1Y")) {
                                        text5 = text5.replace("C1Y",
                                                infBasicaAereaDetalleDto.getCoordenadaNorte()!= null
                                                        ? ""+infBasicaAereaDetalleDto.getCoordenadaNorte()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarCuadro2(BigDecimal areaTotal, List<Anexo2Dto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(1);
            XWPFTableRow row = table.getRow(1);
            System.out.println("procesarCuadro2   "+lista.size()+"\nareaTotal "+areaTotal);
            for (Anexo2Dto anexo2Dto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarCuadro2   text5 "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C2NombreComun")) {
                                        text5 = text5.replace("C2NombreComun",
                                                anexo2Dto.getNombreComun() != null
                                                        ?  ""+anexo2Dto.getNombreComun()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C2NombreCientifico")) {
                                        text5 = text5.replace("C2NombreCientifico",
                                                anexo2Dto.getNombreCientifico()  != null
                                                        ? ""+anexo2Dto.getNombreCientifico()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C2DMC")) {
                                        text5 = text5.replace("C2DMC",
                                                anexo2Dto.getDmc()!= null
                                                        ? ""+anexo2Dto.getDmc()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("C2Individuos")) {
                                        text5 = text5.replace("C2Individuos",
                                                anexo2Dto.getnTotalArboles()!= null
                                                        ? ""+anexo2Dto.getnTotalArboles()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("C2TotalPorHa")) {
                                        text5 = text5.replace("C2TotalPorHa",
                                                areaTotal!= null
                                                        ? ""+ BigDecimal.valueOf( anexo2Dto.getnTotalArboles() ).divide(areaTotal,3, RoundingMode.HALF_UP)
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarCuadro3(List<InfBasicaAereaDetalleDto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(2);
            XWPFTableRow row = table.getRow(1);
             System.out.println("procesarCuadro3   "+lista.size());
            for (InfBasicaAereaDetalleDto infBasicaAereaDetalleDto : lista) {
                // System.out.println("procesarCuadro3 ref)  "+infBasicaAereaDetalleDto.getReferencia());
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    System.out.println("procesarCuadro3    text5 "+text5);
                                    if (text5.contains("C3Vertice")) {
                                        text5 = text5.replace("C3Vertice",
                                                infBasicaAereaDetalleDto.getReferencia() != null
                                                        ?  infBasicaAereaDetalleDto.getReferencia()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C3X")) {
                                        text5 = text5.replace("C3X",
                                                infBasicaAereaDetalleDto.getCoordenadaEste() != null
                                                        ? ""+infBasicaAereaDetalleDto.getCoordenadaEste()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C3Y")) {
                                        text5 = text5.replace("C3Y",
                                                infBasicaAereaDetalleDto.getCoordenadaNorte()!= null
                                                        ? ""+infBasicaAereaDetalleDto.getCoordenadaNorte()
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void procesarCuadro5(List<CronogramaActividadEntity> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(4);
            XWPFTableRow row = table.getRow(3);
            System.out.println("CronogramaActividadEntity  "+lista.size());
            for (CronogramaActividadEntity cronogramaActividadEntity : lista) {
             //   System.out.println("cronogramaActividadEntity.getActividad() "+cronogramaActividadEntity.getActividad());
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarCuadro5 text5  "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C5Act")) {
                                        text5 = text5.replace("C5Act",
                                                cronogramaActividadEntity.getActividad() != null
                                                        ?  ""+cronogramaActividadEntity.getActividad()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C5Mes1")) {
                                        text5 = text5.replace("C5Mes1",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 1
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5Mes2")) {
                                        text5 = text5.replace("C5Mes2",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 2
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5Mes3")) {
                                        text5 = text5.replace("C5Mes3",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 3
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("C5Mes4")) {
                                        text5 = text5.replace("C5Mes4",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 4
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C5Mes5")) {
                                        text5 = text5.replace("C5Mes5",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 5
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                    else if (text5.contains("C5Mes6")) {
                                        text5 = text5.replace("C5Mes6",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 6
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5Mes7")) {
                                        text5 = text5.replace("C5Mes7",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 7
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5Mes8")) {
                                        text5 = text5.replace("C5Mes8",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 8
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5Mes9")) {
                                        text5 = text5.replace("C5Mes9",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 9
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5MesDiez")) {
                                        text5 = text5.replace("C5MesDiez",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 10
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5MesOnce")) {
                                        text5 = text5.replace("C5MesOnce",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 11
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C5MesDoce")) {
                                        text5 = text5.replace("C5MesDoce",
                                                ( cronogramaActividadEntity.getDetalle()  != null &&
                                                        cronogramaActividadEntity.getDetalle().size()>0l &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  != null &&
                                                        cronogramaActividadEntity.getDetalle().get(0).getMes()  == 12
                                                )
                                                        ? "X"
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void procesarCuadro4(BigDecimal areaTotal, List<Anexo2Dto> lista, XWPFDocument doc) {
        try {
            XWPFTable table = doc.getTables().get(3);
            XWPFTableRow row = table.getRow(2);
            System.out.println("procesarCuadro4   "+lista.size()+"\nareaTotal "+areaTotal);
            for (Anexo2Dto anexo2Dto : lista) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);
                CTTrPr trPr = copiedRow.getCtRow().addNewTrPr();
                CTHeight ht = trPr.addNewTrHeight();
                ht.setVal(BigInteger.valueOf(240));
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                System.out.println("procesarCuadro4 text5  "+text5);
                                if (text5 != null) {
                                    if (text5.contains("C4NombreComun")) {
                                        text5 = text5.replace("C4NombreComun",
                                                anexo2Dto.getNombreComun() != null
                                                        ?  ""+anexo2Dto.getNombreComun()
                                                        : "");
                                        r.setText(text5, 0);
                                    } else if (text5.contains("C4NombreCientifico")) {
                                        text5 = text5.replace("C4NombreCientifico",
                                                anexo2Dto.getNombreCientifico()  != null
                                                        ? ""+anexo2Dto.getNombreCientifico()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C4DMC")) {
                                        text5 = text5.replace("C4DMC",
                                                anexo2Dto.getDmc()  != null
                                                        ? ""+anexo2Dto.getDmc()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C4ArbolesExtraer")) {
                                        text5 = text5.replace("C4ArbolesExtraer",
                                                anexo2Dto.getnTotalArboles()  != null
                                                        ? ""+anexo2Dto.getnTotalArboles()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C4AreaPlanManejo")) {
                                        text5 = text5.replace("C4AreaPlanManejo",
                                                areaTotal != null
                                                        ? ""+areaTotal
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C4CantidadMaderable")) {
                                        text5 = text5.replace("C4CantidadMaderable",
                                                anexo2Dto.getVolumen() != null
                                                        ? ""+anexo2Dto.getVolumen()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C4Unit")) {
                                        text5 = text5.replace("C4Unit",
                                                anexo2Dto.getCantidadProducto() != null
                                                        ? ""+anexo2Dto.getCantidadProducto()
                                                        : "");
                                        r.setText(text5, 0);
                                    }else if (text5.contains("C4Total")) {
                                        text5 = text5.replace("C4Total",
                                                anexo2Dto.getCantidadProducto() != null
                                                        ? ""+Double.parseDouble(anexo2Dto.getCantidadProducto())*Double.parseDouble(anexo2Dto.getVolumen())
                                                        : "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}