package pe.gob.serfor.mcsniffs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionCriterioAreaEntity;

import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionCriterioAreaRepository;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionCriterioAreaService;

import java.util.List;

@Service("SolicitudConcesionCriterioAreaService")
public class SolicitudConcesionCriterioAreaServiceImpl implements SolicitudConcesionCriterioAreaService{

    @Autowired
    private SolicitudConcesionCriterioAreaRepository repository;

    @Override
    public ResultClassEntity listarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception{
        return repository.listarSolicitudConcesionCriterioArea(obj);
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception{
        return repository.registrarSolicitudConcesionCriterioArea(obj);
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception{
        return repository.actualizarSolicitudConcesionCriterioArea(obj);
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj) throws Exception{
        return repository.eliminarSolicitudConcesionCriterioArea(obj);
    }
}
