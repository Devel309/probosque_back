package pe.gob.serfor.mcsniffs.service.impl;

import java.io.IOException;
import java.nio.charset.Charset;

import com.google.gson.Gson;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;

@Service
public class ServicioExternoServiceImpl implements ServicioExternoService{
    private static final Logger log = LogManager.getLogger(ServicioExternoServiceImpl.class);

    
    public ResultEntity ejecutarServicio(String url, String json, String token){
        Gson gson = new Gson();       
		ResultEntity result = new ResultEntity();	
			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");			
			RequestBody body = RequestBody.create(mediaType,  json);			
			Request request = new Request.Builder()
			.url(url)
			.method("POST", body)
            //post(body)
            .addHeader("Content-Type", "application/json")
			//.addHeader("cache-control", "no-cache")      
            .addHeader("Authorization", token)   
			.build();
			try {
        	    final Response response = client.newCall(request).execute();               
                final ResponseBody authBody =response.body();
                BufferedSource source = authBody.source();
                source.request(Long.MAX_VALUE); // request the entire body.
                Buffer buffer = source.buffer();
                // clone buffer before reading from it
                String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));
                //Log.d("TAG", responseBodyString);
                authBody.close();
                if(response.code()==200){
                   // result.setReponseBody(authBody);
                    result.setMessage(responseBodyString);
                    result.setIsSuccess(true);
                }else{
                    //result.setReponseBody(authBody);
                    result.setMessage( responseBodyString);
                    result.setIsSuccess(false);
                }  
			} catch (IOException e) {
				e.printStackTrace();
				result.setMessage(e.getMessage());
				result.setIsSuccess(false);
				log.error("Servicio Externo ejecutarServicio", e.getMessage());
			}
		return result;
	}


}

class ResulResponse {
    Boolean  IsSuccess ;
    String  Message ;
  
      public Boolean getIsSuccess() {
          return IsSuccess;
      }
      public void setIsSuccess(Boolean isSuccess) {
          IsSuccess = isSuccess;
      }
      public String getMessage() {
          return Message;
      }
      public void setMessage(String message) {
          Message = message;
      }      
  
  }