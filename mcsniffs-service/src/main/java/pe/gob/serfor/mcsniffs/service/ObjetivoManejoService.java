package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.ObjetivoEspecificoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.util.List;

public interface ObjetivoManejoService {
     ResultClassEntity RegistrarObjetivoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
     ResultClassEntity ActualizarObjetivoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
     ResultClassEntity ObtenerObjetivoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;

     ResultClassEntity RegistrarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoEntityList) throws Exception;
     ResultClassEntity ActualizarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoEntityList) throws Exception;
     ResultClassEntity ObtenerObjetivoEspecificoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
     ResultClassEntity ComboPorFiltroObjetivoEspecifico(ParametroEntity param) throws Exception;
//Jaqueline DB
     ResultClassEntity eliminarObjetivoManejo(ObjetivoManejoEntity param);
     ResultEntity<ObjetivoManejoEntity> listarObjetivoManejo(ObjetivoManejoEntity param);
     ResultEntity<ObjetivoDto> listarObjetivos(String codigoObjetivo, Integer idPlanManejo);
     ResultClassEntity RegistrarObjetivo(List<ObjetivoManejoEntity> list) throws Exception;
     //Jason Retamozo
     ResultEntity<ObjetivoDto> listarObjetivoManejoTitular(ObjetivoManejoEntity param);
}
