package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface LogAuditoriaService {

    ResultClassEntity RegistrarLogAuditoria(LogAuditoriaEntity request) throws Exception;


}
