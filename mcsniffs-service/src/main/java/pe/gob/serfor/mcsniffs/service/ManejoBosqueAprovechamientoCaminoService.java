package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoCaminoEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueAprovechamientoCaminoService {
    ResultClassEntity RegistrarManejoBosqueAprovechamientoCamino(List<ManejoBosqueAprovechamientoCaminoEntity> list) throws Exception;
    ResultClassEntity EliminarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoCaminoEntity manejoBosqueAprovechamientoCamino) throws Exception;
    ResultClassEntity ListarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamientoCamino) throws Exception;
}
