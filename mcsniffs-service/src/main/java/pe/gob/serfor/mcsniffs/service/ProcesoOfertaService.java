package pe.gob.serfor.mcsniffs.service;

import pe.gob.serfor.mcsniffs.entity.*;

import java.util.List;

public interface ProcesoOfertaService {
    ResultClassEntity guardarProcesoOferta(ProcesoOfertaEntity obj) throws Exception;
    ResultClassEntity listarProcesoOferta(ProcesoOfertaRequestEntity obj);
    ResultClassEntity actualizarEsttatusProcesoOferta(Integer IdProcesoOferta, Integer IdStatus, Integer IdUsuarioModificacion);
    ResultClassEntity eliminarProcesoOferta(ProcesoOfertaRequestEntity obj)throws Exception;

}
