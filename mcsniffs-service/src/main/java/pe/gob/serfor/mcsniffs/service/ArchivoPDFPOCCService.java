package pe.gob.serfor.mcsniffs.service;

import org.springframework.core.io.ByteArrayResource;

public interface ArchivoPDFPOCCService {

    ByteArrayResource consolidadoPOCC(Integer idPlanManejo) throws Exception;

}
