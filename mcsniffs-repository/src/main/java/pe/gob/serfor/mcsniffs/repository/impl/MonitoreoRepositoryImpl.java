package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MonitoreoRepositoryImpl extends JdbcDaoSupport implements MonitoreoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(MonitoreoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    @Override
    public ResultClassEntity RegistrarMonitoreoPgmfea(List<MonitoreoPgmfeaEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            MonitoreoPgmfeaEntity monitoreo = new MonitoreoPgmfeaEntity();
            monitoreo=list.get(0);
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_Registrar");
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored_.setParameter("idPlanManejo", monitoreo.getMonitoreo().getPlanManejo().getIdPlanManejo());
            processStored_.setParameter("idMonitoreo", monitoreo.getMonitoreo().getIdMonitoreo());
            processStored_.setParameter("estado", monitoreo.getEstado());
            processStored_.setParameter("idUsuarioRegistro", monitoreo.getIdUsuarioRegistro());
            processStored_.execute();
            MonitoreoEntity obj_ = new MonitoreoEntity() ;
            List<Object[]> spResult_ =processStored_.getResultList();
            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    obj_.setIdMonitoreo((Integer) row[0]);
                }
            }

            List<MonitoreoPgmfeaEntity> list_ = new ArrayList<MonitoreoPgmfeaEntity>() ;
            for(MonitoreoPgmfeaEntity p:list){

                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoPgmfea_Registrar");
                processStored.registerStoredProcedureParameter("idMonitoreoPgmfea", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                processStored.setParameter("idMonitoreoPgmfea", p.getIdMonitoreoPgmfea());
                processStored.setParameter("idMonitoreo", obj_.getIdMonitoreo());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("descripcion", p.getDescripcion());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        MonitoreoPgmfeaEntity obj = new MonitoreoPgmfeaEntity() ;
                        MonitoreoEntity m= new MonitoreoEntity();
                        obj.setIdMonitoreoPgmfea((Integer) row[0]);
                        m.setIdMonitoreo((Integer) row[1]);
                        obj.setMonitoreo(m);
                        obj.setActividad((String) row[2]);
                        obj.setDescripcion((String) row[3]);
                        obj.setResponsable((String) row[4]);
                        list_.add(obj);
                    }
                }
            }

            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el Monitoreo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity EliminarMonitoreoPgmfea(MonitoreoPgmfeaEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoPgmfea_Eliminar");
            processStored.registerStoredProcedureParameter("idMonitoreoPgmfea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored.setParameter("idMonitoreoPgmfea", param.getIdMonitoreoPgmfea());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity ListarMonitoreoPgmfea(MonitoreoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<MonitoreoPgmfeaEntity> list = new ArrayList<MonitoreoPgmfeaEntity>() ;
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoPgmfea_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    MonitoreoPgmfeaEntity obj = new MonitoreoPgmfeaEntity() ;
                    MonitoreoEntity m= new MonitoreoEntity();
                    obj.setIdMonitoreoPgmfea((Integer) row[0]);
                    m.setIdMonitoreo((Integer) row[1]);
                    obj.setMonitoreo(m);
                    obj.setActividad((String) row[2]);
                    obj.setDescripcion((String) row[3]);
                    obj.setResponsable((String) row[4]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se actualizó monitoreo.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    
    @Override
    public ResultClassEntity ObtenerMonitoreoPgmfea(MonitoreoPgmfeaEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoPgmfea_Obtener");
            processStored.registerStoredProcedureParameter("idMonitoreoPgmfea", Integer.class, ParameterMode.IN);
            processStored.setParameter("idMonitoreoPgmfea", param.getIdMonitoreoPgmfea());
            processStored.execute();
            MonitoreoPgmfeaEntity obj = new MonitoreoPgmfeaEntity() ;
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    MonitoreoEntity m= new MonitoreoEntity();
                    obj.setIdMonitoreoPgmfea((Integer) row[0]);
                    m.setIdMonitoreo((Integer) row[1]);
                    obj.setMonitoreo(m);
                    obj.setActividad((String) row[2]);
                    obj.setDescripcion((String) row[3]);
                    obj.setResponsable((String) row[4]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el  monitoreo.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity RegistrarMonitoreoPOCC(List<MonitoreoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<MonitoreoEntity> list_ = new ArrayList<>();
        try {
            for (MonitoreoEntity p : list) {
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_RegistrarCabecera");
                processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoMonitoreo", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("monitoreo", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("indicador", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idMonitoreo", p.getIdMonitoreo());
                processStored.setParameter("descripcion", p.getDescripcion());
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("codigoMonitoreo", p.getCodigoMonitoreo());
                processStored.setParameter("observacion", p.getObservacion());
                processStored.setParameter("monitoreo", p.getMonitoreo());
                processStored.setParameter("indicador", p.getIndicador());
                processStored.setParameter("frecuencia", p.getFrecuencia());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult_ = processStored.getResultList();
                MonitoreoEntity obj_=null;
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        obj_= new MonitoreoEntity();
                        obj_.setIdMonitoreo((Integer) row_[0]);

                            List<MonitoreoDetalleEntity> list_Detalle = new ArrayList<>();
                            for (MonitoreoDetalleEntity param : p.getLstDetalle()) {
                                StoredProcedureQuery processStored2 = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoDetalle_RegistrarDetalle");
                                processStored2.registerStoredProcedureParameter("idMonitoreoDetalle", Integer.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("codigoMonitoreoDet", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("operacion", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("subOperacion", String.class, ParameterMode.IN);
                                processStored2.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                                SpUtil.enableNullParams(processStored2);
                                processStored2.setParameter("idMonitoreoDetalle", param.getIdMonitoreoDetalle());
                                processStored2.setParameter("descripcion", param.getDescripcion());
                                processStored2.setParameter("responsable", param.getResponsable());
                                processStored2.setParameter("idMonitoreo", obj_.getIdMonitoreo());
                                processStored2.setParameter("actividad", param.getActividad());
                                processStored2.setParameter("observacion", param.getObservacion());
                                processStored2.setParameter("codigoMonitoreoDet", param.getCodigoMonotoreoDet());
                                processStored2.setParameter("operacion", param.getOperacion());
                                processStored2.setParameter("subOperacion", param.getSubOperacion());
                                processStored2.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                                processStored2.execute();
                                List<Object[]> spResult2 =processStored2.getResultList();
                                if (!spResult2.isEmpty()) {
                                    MonitoreoDetalleEntity obj = null;
                                    for (Object[] row : spResult2) {
                                        //obj.setIdMonitoreoDetalle((Integer) row[0]);
                                    }
                                    list_Detalle.add(obj);
                                }
                            }
                          obj_.setLstDetalle(list_Detalle);
                        }

                        list_.add(obj_);
                    }
                }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el monitoreo POCC.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public ResultClassEntity ActualizarMonitoreoPOCC(List<MonitoreoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<MonitoreoEntity> list_ = new ArrayList<>();
        try {
            for (MonitoreoEntity p : list) {
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_ActualizarCabecera");
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("monitoreo", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("indicador", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("descripcion", p.getDescripcion());
                processStored.setParameter("idMonitoreo", p.getIdMonitoreo());
                processStored.setParameter("observacion", p.getObservacion());
                processStored.setParameter("monitoreo", p.getMonitoreo());
                processStored.setParameter("indicador", p.getIndicador());
                processStored.setParameter("frecuencia", p.getFrecuencia());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("idUsuarioModificacion", p.getIdUsuarioModificacion());
                processStored.execute();
                List<Object[]> spResult_ = processStored.getResultList();
                MonitoreoEntity obj_=null;
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        obj_= new MonitoreoEntity();
                        obj_.setIdMonitoreo((Integer) row_[0]);

                        List<MonitoreoDetalleEntity> list_Detalle = new ArrayList<>();
                        for (MonitoreoDetalleEntity param : p.getLstDetalle()) {
                            StoredProcedureQuery processStored2 = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoDetalle_ActualizarDetalle");
                            processStored2.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("idMonitoreoDetalle", Integer.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("operacion", String.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("subOperacion", String.class, ParameterMode.IN);
                            processStored2.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(processStored2);
                            processStored2.setParameter("descripcion", param.getDescripcion());
                            processStored2.setParameter("responsable", param.getResponsable());
                            processStored2.setParameter("idMonitoreoDetalle", param.getIdMonitoreoDetalle());
                            processStored2.setParameter("actividad", param.getActividad());
                            processStored2.setParameter("observacion", param.getObservacion());
                            processStored2.setParameter("operacion", param.getOperacion());
                            processStored2.setParameter("subOperacion", param.getSubOperacion());
                            processStored2.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
                            processStored2.execute();
                            List<Object[]> spResult2 =processStored2.getResultList();
                            if (!spResult2.isEmpty()) {
                                MonitoreoDetalleEntity obj = null;
                                for (Object[] row : spResult2) {
                                    //obj.setIdMonitoreoDetalle((Integer) row[0]);
                                }
                                list_Detalle.add(obj);
                            }
                        }
                        obj_.setLstDetalle(list_Detalle);
                    }

                    list_.add(obj_);
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el monitoreo POCC.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }



    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Registra la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity RegistrarMonitoreo(MonitoreoEntity monitoreoPgmfea) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_RegistrarCabecera");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMonitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("monitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("indicador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("descripcion", monitoreoPgmfea.getDescripcion());
            processStored.setParameter("idPlanManejo", monitoreoPgmfea.getIdPlanManejo());
            processStored.setParameter("codigoMonitoreo", monitoreoPgmfea.getCodigoMonitoreo());
            processStored.setParameter("observacion", monitoreoPgmfea.getObservacion());
            processStored.setParameter("monitoreo", monitoreoPgmfea.getMonitoreo());
            processStored.setParameter("indicador", monitoreoPgmfea.getIndicador());
            processStored.setParameter("frecuencia", monitoreoPgmfea.getFrecuencia());
            processStored.setParameter("responsable", monitoreoPgmfea.getResponsable());
            processStored.setParameter("idUsuarioRegistro", monitoreoPgmfea.getIdUsuarioRegistro());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    monitoreoPgmfea.setIdMonitoreo((Integer) row[0]);
                }
            }
            if(monitoreoPgmfea.getLstDetalle()!=null){
                if(monitoreoPgmfea.getLstDetalle().size()>0){
                    for (MonitoreoDetalleEntity objects : monitoreoPgmfea.getLstDetalle()) {
                        objects.setIdMonitoreo(monitoreoPgmfea.getIdMonitoreo());
                        ResultClassEntity detalle = RegistrarMonitoreoDetalle(objects);
                        result.setInformacion(detalle.getMessage());
                    }
                }
            }
            result.setData(monitoreoPgmfea);
            result.setSuccess(true);
            result.setMessage("Se registró el monitoreo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    public ResultClassEntity RegistrarMonitoreoDetalle(MonitoreoDetalleEntity monitoreoPgmfea) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoDetalle_RegistrarDetalle");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMonitoreoDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("operacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subOperacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("descripcion", monitoreoPgmfea.getDescripcion());
            processStored.setParameter("responsable", monitoreoPgmfea.getResponsable());
            processStored.setParameter("idMonitoreo", monitoreoPgmfea.getIdMonitoreo());
            processStored.setParameter("actividad", monitoreoPgmfea.getActividad());
            processStored.setParameter("observacion", monitoreoPgmfea.getObservacion());
            processStored.setParameter("codigoMonitoreoDet", monitoreoPgmfea.getCodigoMonotoreoDet());
            processStored.setParameter("operacion", monitoreoPgmfea.getOperacion());
            processStored.setParameter("subOperacion", monitoreoPgmfea.getSubOperacion());
            processStored.setParameter("idUsuarioRegistro", monitoreoPgmfea.getIdUsuarioRegistro());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    monitoreoPgmfea.setIdMonitoreoDetalle((Integer) row[0]);
                }
            }
            result.setData(monitoreoPgmfea);
            result.setSuccess(true);
            result.setMessage("Se registró monitoreo detalle correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

     /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Actualiza la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity ActualizarMonitoreo(MonitoreoEntity monitoreoPgmfea) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_ActualizarCabecera");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("monitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("indicador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("descripcion", monitoreoPgmfea.getDescripcion());
            processStored.setParameter("idMonitoreo", monitoreoPgmfea.getIdMonitoreo());
            processStored.setParameter("observacion", monitoreoPgmfea.getObservacion());
            processStored.setParameter("monitoreo", monitoreoPgmfea.getMonitoreo());
            processStored.setParameter("indicador", monitoreoPgmfea.getIndicador());
            processStored.setParameter("frecuencia", monitoreoPgmfea.getFrecuencia());
            processStored.setParameter("responsable", monitoreoPgmfea.getResponsable());
            processStored.setParameter("idUsuarioModificacion", monitoreoPgmfea.getIdUsuarioModificacion());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    monitoreoPgmfea.setIdMonitoreo((Integer) row[0]);
                }
            }
            if(monitoreoPgmfea.getLstDetalle()!=null){
                if(monitoreoPgmfea.getLstDetalle().size()>0){
                    for (MonitoreoDetalleEntity objects : monitoreoPgmfea.getLstDetalle()) {
                        if(objects.getIdMonitoreoDetalle()!=null){
                            if(objects.getIdMonitoreoDetalle().equals(0)){
                                objects.setIdMonitoreo(monitoreoPgmfea.getIdMonitoreo());
                                ResultClassEntity detalle = RegistrarMonitoreoDetalle(objects);
                                result.setInformacion(detalle.getMessage());
                            }else{
                                ResultClassEntity detalle = ActualizarMonitoreoDetalle(objects);
                                result.setInformacion(detalle.getMessage());
                            }
                        }
                    }
                }
            }
            result.setData(monitoreoPgmfea);
            result.setSuccess(true);
            result.setMessage("Se actualizó monitoreo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    public ResultClassEntity ActualizarMonitoreoDetalle(MonitoreoDetalleEntity monitoreoPgmfea) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoDetalle_ActualizarDetalle");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMonitoreoDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("operacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subOperacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("descripcion", monitoreoPgmfea.getDescripcion());
            processStored.setParameter("responsable", monitoreoPgmfea.getResponsable());
            processStored.setParameter("idMonitoreoDetalle", monitoreoPgmfea.getIdMonitoreoDetalle());
            processStored.setParameter("actividad", monitoreoPgmfea.getActividad());
            processStored.setParameter("observacion", monitoreoPgmfea.getObservacion());
            processStored.setParameter("operacion", monitoreoPgmfea.getOperacion());
            processStored.setParameter("subOperacion", monitoreoPgmfea.getSubOperacion());
            processStored.setParameter("idUsuarioModificacion", monitoreoPgmfea.getIdUsuarioModificacion());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    monitoreoPgmfea.setIdMonitoreoDetalle((Integer) row[0]);
                }
            }
            result.setData(monitoreoPgmfea);
            result.setSuccess(true);
            result.setMessage("Se actualizó monitoreo detalle correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

     /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Lista la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity<MonitoreoEntity> listarMonitoreo(MonitoreoEntity monitoreo) {
        ResultClassEntity<MonitoreoEntity> result = new ResultClassEntity<MonitoreoEntity>();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_ListarDetalle");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMonitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMonitoreoDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMonitoreoDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", monitoreo.getIdPlanManejo());
            processStored.setParameter("codigoMonitoreo", monitoreo.getCodigoMonitoreo());
            processStored.setParameter("codigoMonitoreoDet", monitoreo.getCodigoMonitoreoDet());
            processStored.setParameter("idMonitoreoDet", monitoreo.getIdMonitoreoDet());
            processStored.setParameter("idMonitoreo", monitoreo.getIdMonitoreo());
            processStored.execute();
            MonitoreoEntity obj = new MonitoreoEntity() ;
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                Object[] cabecera = spResult.get(0);
                obj.setIdMonitoreo((Integer) cabecera[0]);
                obj.setDescripcion((String) cabecera[1]);
                obj.setIdPlanManejo((Integer) cabecera[2]);
                obj.setCodigoMonitoreo((String) cabecera[3]);
                obj.setObservacion((String) cabecera[4]);
                obj.setIndicador((String) cabecera[5]);
                obj.setFrecuencia((String) cabecera[6]);
                obj.setMonitoreo((String) cabecera[7]);
                List<MonitoreoDetalleEntity> lstDetalle = new ArrayList<>();
                for (Object[] row : spResult) {
                    MonitoreoDetalleEntity detalle = new MonitoreoDetalleEntity();
                    detalle.setIdMonitoreoDetalle((Integer) row[8]);
                    detalle.setDescripcion((String) row[9]);
                    detalle.setResponsable((String) row[10]);
                    detalle.setActividad((String) row[11]);
                    detalle.setObservacion((String) row[12]);
                    detalle.setCodigoMonotoreoDet((String) row[13]);
                    detalle.setOperacion((String) cabecera[14]);
                    lstDetalle.add(detalle);
                }
                obj.setLstDetalle(lstDetalle);
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se listo el  monitoreo.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Rafael Azaña [22-11-2021]
     * @modificado:
     * @descripción: {Lista la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    public List<MonitoreoEntity>  listarMonitoreoPOCC(Integer idPlanManejo, String codigoMonitoreo) throws Exception {
        List<MonitoreoEntity> lista = new ArrayList<MonitoreoEntity>();
        try {
            int idDetalle=0;
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoPOCC_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMonitoreo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigoMonitoreo", codigoMonitoreo);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        MonitoreoEntity temp = new MonitoreoEntity();

                        temp.setIdMonitoreo((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setCodigoMonitoreo((String) row[2]);
                        temp.setIndicador((String) row[3]);
                        temp.setFrecuencia((String) row[4]);
                        temp.setMonitoreo((String) row[5]);
                        temp.setResponsable((String) row[6]);
                        temp.setDescripcion((String) row[7]);
                        temp.setObservacion((String) row[8]);
                        idDetalle=(Integer) row[0];

                        /********************************** OPERACION **********************************************/
                        List<MonitoreoDetalleEntity> listDetActividad = new ArrayList<MonitoreoDetalleEntity>();
                        StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoDetallePOCC_Listar");
                        pa.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
                        SpUtil.enableNullParams(pa);
                        pa.setParameter("idMonitoreo", idDetalle);
                        pa.execute();
                        List<Object[]> spResultAct = pa.getResultList();
                        if(spResultAct!=null) {
                            if (spResultAct.size() >= 1) {
                                for (Object[] rowAct : spResultAct) {
                                    MonitoreoDetalleEntity temp2 = new MonitoreoDetalleEntity();
                                    temp2.setIdMonitoreoDetalle((Integer) rowAct[0]);
                                    temp2.setResponsable((String) rowAct[1]);
                                    temp2.setCodigoMonotoreoDet((String) rowAct[2]);
                                    temp2.setOperacion((String) rowAct[3]);
                                    temp2.setSubOperacion((String) rowAct[4]);
                                    temp2.setActividad((String) rowAct[5]);
                                    temp2.setDescripcion((String) rowAct[6]);
                                    listDetActividad.add(temp2);
                                }
                            }
                        }
                        /********************************************************************************/
                        temp.setLstDetalle(listDetActividad);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }


    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Eliminar el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity EliminarDetalleMonitoreo(MonitoreoDetalleEntity detalle) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MonitoreoDetalle_EliminarDetalle");
            processStored.registerStoredProcedureParameter("idMonitoreoDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idMonitoreoDetalle", detalle.getIdMonitoreoDetalle());
            processStored.setParameter("idUsuarioElimina", detalle.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el monitoreo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

      /**
     * @autor: Jaqueline DB [26-11-2021]
     * @modificado:
     * @descripción: {elimina el monitoreo}
     * @param:MonitoreoEntity
     */
    @Override
    public ResultClassEntity EliminarMonitoreoCabera(MonitoreoEntity monitoreo) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Monitoreo_EliminarCabera");
            processStored.registerStoredProcedureParameter("idMonitoreo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored.setParameter("idMonitoreo", monitoreo.getIdMonitoreo());
            processStored.setParameter("idUsuarioElimina", monitoreo.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return  result;
        }
    }
}
