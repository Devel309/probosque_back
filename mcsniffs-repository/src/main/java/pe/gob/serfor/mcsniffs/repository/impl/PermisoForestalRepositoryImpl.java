package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.PermisoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class PermisoForestalRepositoryImpl extends JdbcDaoSupport implements PermisoForestalRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(PermisoForestalRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Autowired
    private ArchivoRepository fileUp;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @Override
    public ResultClassEntity ObtenerPersonaPorUsuario(PersonaEntity entity) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Persona_ListarPorUsuario");
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.setParameter("idUsuario", entity.getIdUsuario());
            processStored.execute();
            PersonaEntity personaEntity = new PersonaEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    personaEntity.setIdPersona((Integer) row[0]);
                    personaEntity.setNombres((String) row[1]);
                    personaEntity.setApellidoPaterno((String) row[2]);
                    personaEntity.setApellidoMaterno((String) row[3]);
                    personaEntity.setCodInrena((String) row[4]);
                    personaEntity.setIdUsuario((Integer) row[5]);
                    personaEntity.setIdSolicitudAcceso((Integer) row[6]);
                    break;
                }
            }

            result.setData(personaEntity);
            result.setSuccess(true);
            result.setMessage("Se obtuvo información de la persona.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener información de la persona.");
            return result;
        }
    }

    @Override
    public ResultClassEntity obtenerSolicitudPorAccesoSolicitud(Integer idSolicitud, Integer idSolicitudAcceso) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_ObtenerPorCodigo");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudAcceso", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", idSolicitud.intValue());
            processStored.setParameter("idSolicitudAcceso", idSolicitudAcceso.intValue());
            processStored.execute();
            SolicitudEntity solicitudEntity = new SolicitudEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    solicitudEntity.setIdSolicitud((Integer) row[0]);
                    solicitudEntity.setNroPropiedad((String) row[1]);
                    solicitudEntity.setNroPartidaRegistral((String) row[2]);
                    solicitudEntity.setNroContratoRegente((String) row[3]);
                    solicitudEntity.setNroActaAsamblea((String) row[4]);
                    solicitudEntity.setRegenteForestal((String) row[5]);
                    solicitudEntity.setNombreRegenteForestal((String) row[6]);
                    solicitudEntity.setEscalaManejo((String) row[7]);
                    solicitudEntity.setNroComprobantePago((String) row[8]);
                    solicitudEntity.setNroActaAsambleaAcuerdo((String) row[9]);
                    solicitudEntity.setNroRucEmpresa((String) row[10]);
                    solicitudEntity.setTipoAprovechamiento((String) row[11]);
                    solicitudEntity.setNroContratoTercero((String) row[12]);
                    solicitudEntity.setNroActaAprovechamiento((String) row[13]);
                    solicitudEntity.setIdSolicitudAcceso((Integer) row[14]);
                    solicitudEntity.setIdPersonaRepresentante((Integer) row[15]);
                    solicitudEntity.setDescSolicitudReconocimiento((String) row[16]);
                    solicitudEntity.setDescSolicitudAmpliacion((String) row[17]);
                    solicitudEntity.setDescSolicitudTitulacion((String) row[18]);
                    solicitudEntity.setDescDocumentoResidencia((String) row[19]);
                    solicitudEntity.setEstado((String) row[19]);
                    break;
                }
            }

            result.setData(solicitudEntity);
            result.setSuccess(true);
            result.setMessage("Se obtuvo información de la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener información de la solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_Registrar");
            processStored.registerStoredProcedureParameter("nroPropiedad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPartidaRegistral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroActaAsamblea", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("regenteForestal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreRegenteForestal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroContratoRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("escalaManejo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroComprobantePago", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroActaAsambleaAcuerdo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroRucEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroContratoTercero", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroActaAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descSolicitudReconocimiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descSolicitudAmpliacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descSolicitudTitulacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descDocumentoResidencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudAcceso", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersonaRepresentante", int.class, ParameterMode.IN);

            processStored.setParameter("nroPropiedad", solicitudRequestEntity.getNroPropiedad());
            processStored.setParameter("nroPartidaRegistral", solicitudRequestEntity.getNroPartidaRegistral());
            processStored.setParameter("nroActaAsamblea", solicitudRequestEntity.getNroActaAsamblea());
            processStored.setParameter("regenteForestal", solicitudRequestEntity.getRegenteForestal());
            processStored.setParameter("nroContratoRegente", solicitudRequestEntity.getNroContratoRegente());
            processStored.setParameter("nombreRegenteForestal", solicitudRequestEntity.getNombreRegenteForestal());
            processStored.setParameter("escalaManejo", solicitudRequestEntity.getEscalaManejo());
            processStored.setParameter("nroComprobantePago", solicitudRequestEntity.getNroComprobantePago());
            processStored.setParameter("nroActaAsambleaAcuerdo", solicitudRequestEntity.getNroActaAsambleaAcuerdo());
            processStored.setParameter("nroRucEmpresa", solicitudRequestEntity.getNroRucEmpresa());
            processStored.setParameter("codTipoAprovechamiento", solicitudRequestEntity.getTipoAprovechamiento());
            processStored.setParameter("nroContratoTercero", solicitudRequestEntity.getNroContratoTercero());
            processStored.setParameter("nroActaAprovechamiento", solicitudRequestEntity.getNroActaAprovechamiento());

            processStored.setParameter("descSolicitudReconocimiento", solicitudRequestEntity.getDescSolicitudReconocimiento());
            processStored.setParameter("descSolicitudAmpliacion", solicitudRequestEntity.getDescSolicitudAmpliacion());
            processStored.setParameter("descSolicitudTitulacion", solicitudRequestEntity.getDescSolicitudTitulacion());
            processStored.setParameter("descDocumentoResidencia", solicitudRequestEntity.getDescDocumentoResidencia());

            processStored.setParameter("idSolicitudAcceso", solicitudRequestEntity.getIdSolicitudAcceso());
            processStored.setParameter("idPersonaRepresentante", solicitudRequestEntity.getIdPersonaRepresentante());

            processStored.execute();
            SolicitudEntity solicitudEntity = new SolicitudEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    solicitudEntity.setIdSolicitud((Integer) row[0]);
                    solicitudEntity.setNroPropiedad((String) row[1]);
                    solicitudEntity.setNroPartidaRegistral((String) row[2]);
                    solicitudEntity.setNroContratoRegente((String) row[3]);
                    solicitudEntity.setNroActaAsamblea((String) row[4]);
                    solicitudEntity.setRegenteForestal((String) row[5]);
                    solicitudEntity.setNombreRegenteForestal((String) row[6]);
                    solicitudEntity.setEscalaManejo((String) row[7]);
                    solicitudEntity.setNroComprobantePago((String) row[8]);
                    solicitudEntity.setNroActaAsambleaAcuerdo((String) row[9]);
                    solicitudEntity.setNroRucEmpresa((String) row[10]);
                    solicitudEntity.setTipoAprovechamiento((String) row[11]);
                    solicitudEntity.setNroContratoTercero((String) row[12]);
                    solicitudEntity.setNroActaAprovechamiento((String) row[13]);
                    solicitudEntity.setIdSolicitudAcceso((Integer) row[14]);
                    solicitudEntity.setIdPersonaRepresentante((Integer) row[15]);
                    solicitudEntity.setDescSolicitudReconocimiento((String) row[16]);
                    solicitudEntity.setDescSolicitudAmpliacion((String) row[17]);
                    solicitudEntity.setDescSolicitudTitulacion((String) row[18]);
                    solicitudEntity.setDescDocumentoResidencia((String) row[19]);
                    break;
                }
            }

            result.setData(solicitudEntity);
            result.setSuccess(true);
            result.setMessage("Se registró la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al registrar solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity editarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_Editar");
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPropiedad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPartidaRegistral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroActaAsamblea", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("regenteForestal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreRegenteForestal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroContratoRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("escalaManejo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroComprobantePago", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroActaAsambleaAcuerdo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroRucEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroContratoTercero", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroActaAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descSolicitudReconocimiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descSolicitudAmpliacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descSolicitudTitulacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descDocumentoResidencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudAcceso", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersonaRepresentante", int.class, ParameterMode.IN);

            processStored.setParameter("idSolicitud", solicitudRequestEntity.getIdSolicitud());
            processStored.setParameter("nroPropiedad", solicitudRequestEntity.getNroPropiedad());
            processStored.setParameter("nroPartidaRegistral", solicitudRequestEntity.getNroPartidaRegistral());
            processStored.setParameter("nroActaAsamblea", solicitudRequestEntity.getNroActaAsamblea());
            processStored.setParameter("regenteForestal", solicitudRequestEntity.getRegenteForestal());
            processStored.setParameter("nroContratoRegente", solicitudRequestEntity.getNroContratoRegente());
            processStored.setParameter("nombreRegenteForestal", solicitudRequestEntity.getNombreRegenteForestal());
            processStored.setParameter("escalaManejo", solicitudRequestEntity.getEscalaManejo());
            processStored.setParameter("nroComprobantePago", solicitudRequestEntity.getNroComprobantePago());
            processStored.setParameter("nroActaAsambleaAcuerdo", solicitudRequestEntity.getNroActaAsambleaAcuerdo());
            processStored.setParameter("nroRucEmpresa", solicitudRequestEntity.getNroRucEmpresa());
            processStored.setParameter("codTipoAprovechamiento", solicitudRequestEntity.getTipoAprovechamiento());
            processStored.setParameter("nroContratoTercero", solicitudRequestEntity.getNroContratoTercero());
            processStored.setParameter("nroActaAprovechamiento", solicitudRequestEntity.getNroActaAprovechamiento());

            processStored.setParameter("descSolicitudReconocimiento", solicitudRequestEntity.getDescSolicitudReconocimiento());
            processStored.setParameter("descSolicitudAmpliacion", solicitudRequestEntity.getDescSolicitudAmpliacion());
            processStored.setParameter("descSolicitudTitulacion", solicitudRequestEntity.getDescSolicitudTitulacion());
            processStored.setParameter("descDocumentoResidencia", solicitudRequestEntity.getDescDocumentoResidencia());

            processStored.setParameter("idSolicitudAcceso", solicitudRequestEntity.getIdSolicitudAcceso());
            processStored.setParameter("idPersonaRepresentante", solicitudRequestEntity.getIdPersonaRepresentante());

            processStored.execute();
            SolicitudEntity solicitudEntity = new SolicitudEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    solicitudEntity.setIdSolicitud((Integer) row[0]);
                    solicitudEntity.setNroPropiedad((String) row[1]);
                    solicitudEntity.setNroPartidaRegistral((String) row[2]);
                    solicitudEntity.setNroContratoRegente((String) row[3]);
                    solicitudEntity.setNroActaAsamblea((String) row[4]);
                    solicitudEntity.setRegenteForestal((String) row[5]);
                    solicitudEntity.setNombreRegenteForestal((String) row[6]);
                    solicitudEntity.setEscalaManejo((String) row[7]);
                    solicitudEntity.setNroComprobantePago((String) row[8]);
                    solicitudEntity.setNroActaAsambleaAcuerdo((String) row[9]);
                    solicitudEntity.setNroRucEmpresa((String) row[10]);
                    solicitudEntity.setTipoAprovechamiento((String) row[11]);
                    solicitudEntity.setNroContratoTercero((String) row[12]);
                    solicitudEntity.setNroActaAprovechamiento((String) row[13]);
                    solicitudEntity.setIdSolicitudAcceso((Integer) row[14]);
                    solicitudEntity.setIdPersonaRepresentante((Integer) row[15]);
                    solicitudEntity.setDescSolicitudReconocimiento((String) row[16]);
                    solicitudEntity.setDescSolicitudAmpliacion((String) row[17]);
                    solicitudEntity.setDescSolicitudTitulacion((String) row[18]);
                    solicitudEntity.setDescDocumentoResidencia((String) row[19]);
                    break;
                }
            }

            result.setData(solicitudEntity);
            result.setSuccess(true);
            result.setMessage("Se edito la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al editar solicitud.");
            return result;
        }
    }


    @Override
    public ResultEntity ObtenerPermisosForestales(SolicitudEntity request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultClassEntity validarRequisitoSolicitud(ValidacionRequisitoEntity entity) {
        ResultClassEntity<ValidacionRequisitoEntity> result = new ResultClassEntity();
        try {
            ResultClassEntity<ValidacionRequisitoEntity> resultData;
            if (entity.getIdSolicitudValidacion() != 0) {
                resultData = this.actualizarSolicitudValidacion(entity);
            } else {
                resultData = this.registrarSolicitudValidacion(entity);
            }

            result.setData(resultData.getData());
            result.setSuccess(true);
            result.setMessage("Se valida la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al validar solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<ValidacionRequisitoEntity> registrarSolicitudValidacion(ValidacionRequisitoEntity entity) {
        ResultClassEntity<ValidacionRequisitoEntity> result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_solicitud_validacion_registrar");
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoValidacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tieneObservaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);

            processStored.setParameter("idSolicitud", entity.getIdSolicitud());
            processStored.setParameter("tipoSeccion", entity.getTipoSeccion());
            processStored.setParameter("tipoValidacion", entity.getTipovalidacion());
            processStored.setParameter("tieneObservaciones", entity.getTieneObservaciones());
            processStored.setParameter("observaciones", entity.getObservacion());
            processStored.setParameter("estadoSolicitud", entity.getEstadoSolicitud());
            processStored.execute();
            ValidacionRequisitoEntity requisitoEntity = new ValidacionRequisitoEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    requisitoEntity.setIdSolicitudValidacion((Integer) row[0]);
                    requisitoEntity.setIdSolicitud((Integer) row[1]);
                    requisitoEntity.setTipoSeccion((String) row[2]);
                    requisitoEntity.setTipovalidacion((String) row[3]);
                    requisitoEntity.setTieneObservaciones((String) row[4]);
                    requisitoEntity.setObservacion((String) row[5]);
                    requisitoEntity.setEstadoSolicitud((String) row[6]);
                    break;
                }
            }

            result.setData(requisitoEntity);
            result.setSuccess(true);
            result.setMessage("Se registró la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al registrar solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<ValidacionRequisitoEntity> actualizarSolicitudValidacion(ValidacionRequisitoEntity entity) {
        ResultClassEntity<ValidacionRequisitoEntity> result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_solicitud_validacion_actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudValidacion", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoValidacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tieneObservaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);

            processStored.setParameter("idSolicitudValidacion", entity.getIdSolicitudValidacion());
            processStored.setParameter("idSolicitud", entity.getIdSolicitud());
            processStored.setParameter("tipoSeccion", entity.getTipoSeccion());
            processStored.setParameter("tipoValidacion", entity.getTipovalidacion());
            processStored.setParameter("tieneObservaciones", entity.getTieneObservaciones());
            processStored.setParameter("observaciones", entity.getObservacion());
            processStored.setParameter("estadoSolicitud", entity.getEstadoSolicitud());
            processStored.execute();
            ValidacionRequisitoEntity requisitoEntity = new ValidacionRequisitoEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    requisitoEntity.setIdSolicitudValidacion((Integer) row[0]);
                    requisitoEntity.setIdSolicitud((Integer) row[1]);
                    requisitoEntity.setTipoSeccion((String) row[2]);
                    requisitoEntity.setTipovalidacion((String) row[3]);
                    requisitoEntity.setTieneObservaciones((String) row[4]);
                    requisitoEntity.setObservacion((String) row[5]);
                    requisitoEntity.setEstadoSolicitud((String) row[6]);
                    break;
                }
            }

            result.setData(requisitoEntity);
            result.setSuccess(true);
            result.setMessage("Se registró la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al registrar solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity obtenerSolicitudValidacionPorSeccion(ValidacionRequisitoEntity entity) {
        ResultClassEntity<ValidacionRequisitoEntity> result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_solicitud_validacion_obtenerPorSeccion");
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoSeccion", String.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", entity.getIdSolicitud());
            processStored.setParameter("tipoSeccion", entity.getTipoSeccion());
            processStored.execute();
            ValidacionRequisitoEntity requisitoEntity = new ValidacionRequisitoEntity();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    requisitoEntity.setIdSolicitudValidacion((Integer) row[0]);
                    requisitoEntity.setIdSolicitud((Integer) row[1]);
                    requisitoEntity.setTipoSeccion((String) row[2]);
                    requisitoEntity.setTipovalidacion((String) row[3]);
                    requisitoEntity.setTieneObservaciones((String) row[4]);
                    requisitoEntity.setObservacion((String) row[5]);
                    requisitoEntity.setEstadoSolicitud((String) row[6]);
                    break;
                }
            }

            result.setData(requisitoEntity);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity obtenerSolicitudValidacionPorSolicitud(ValidacionRequisitoEntity entity) {
        ResultClassEntity<List<ValidacionRequisitoEntity>> result = new ResultClassEntity();
        List<ValidacionRequisitoEntity> validacionRequisitoEntityList = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_solicitud_validacion_obtenerPorSolicitud");
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", entity.getIdSolicitud());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ValidacionRequisitoEntity requisitoEntity = new ValidacionRequisitoEntity();
                    requisitoEntity.setIdSolicitudValidacion((Integer) row[0]);
                    requisitoEntity.setIdSolicitud((Integer) row[1]);
                    requisitoEntity.setTipoSeccion((String) row[2]);
                    requisitoEntity.setTipovalidacion((String) row[3]);
                    requisitoEntity.setTieneObservaciones((String) row[4]);
                    requisitoEntity.setObservacion((String) row[5]);
                    requisitoEntity.setEstadoSolicitud((String) row[6]);
                    validacionRequisitoEntityList.add(requisitoEntity);
                }
            }

            result.setData(validacionRequisitoEntityList);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity obtenerSolicitudEvaluacionPorSolicitud(ValidacionRequisitoEntity entity) {
        ResultClassEntity<List<ValidacionRequisitoEntity>> result = new ResultClassEntity();
        List<ValidacionRequisitoEntity> validacionRequisitoEntityList = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_solicitud_evaluacion_obtenerPorSolicitud");
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", entity.getIdSolicitud());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ValidacionRequisitoEntity requisitoEntity = new ValidacionRequisitoEntity();
                    requisitoEntity.setIdSolicitudValidacion((Integer) row[0]);
                    requisitoEntity.setIdSolicitud((Integer) row[1]);
                    requisitoEntity.setTipoSeccion((String) row[2]);
                    requisitoEntity.setTipovalidacion((String) row[3]);
                    requisitoEntity.setTieneObservaciones((String) row[4]);
                    requisitoEntity.setObservacion((String) row[5]);
                    requisitoEntity.setEstadoSolicitud((String) row[6]);
                    validacionRequisitoEntityList.add(requisitoEntity);
                }
            }

            result.setData(validacionRequisitoEntityList);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity evaluarRequisitoSolicitud(ValidacionRequisitoEntity entity) {
        ResultClassEntity<ValidacionRequisitoEntity> result = new ResultClassEntity();
        try {
            ResultClassEntity<ValidacionRequisitoEntity> resultData;
            if (entity.getIdSolicitudValidacion() != 0) {
                resultData = this.actualizarSolicitudValidacion(entity);
            } else {
                resultData = this.registrarSolicitudValidacion(entity);
            }

            result.setData(resultData.getData());
            result.setSuccess(true);
            result.setMessage("Se valida la solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al validar solicitud.");
            return result;
        }
    }     

    @Override
    public ResultEntity RegistrarPermisosForestales(SolicitudEntity request) {
        return null;
        /*
        ResultEntity resul = new ResultEntity();
        CallableStatement pstm = null;
        Connection con = null;
        try {
            String res = "";
            con = dataSource.getConnection();

            pstm = con.prepareCall("{call pa_RegistrarPersmisoForestales (?,?,?,?,?,?,?,?,?,?)}");
            pstm.setObject(1, request.getIdSolicitud(), Types.INTEGER);
            pstm.setObject(2, request.getIdProcesoPostulacion(), Types.INTEGER);
            pstm.setObject(3, request.getIdTipoSolicitud(), Types.VARCHAR);
            pstm.setObject(4, request.getInicioSolicitud(), Types.DATE);
            pstm.setObject(5, request.getFinSolicitud(), Types.DATE);

            //pstm.setObject (7, request.getAprobado(),Types.BIT);
            pstm.setObject(7, request.getAprobado(), Types.BIT);
            //pstm.setObject (7, request.getAprobado(),Types.BIT);
            pstm.setObject(8, request.getMotivoPostulacion(), Types.VARCHAR);
            pstm.setObject(9, request.getDistrito().getIdDepartamento(), Types.INTEGER);
            pstm.setObject(7, request.getSuperficieComunidad(), Types.BIT);
            pstm.setObject(7, request.getAreaManejoForestal(), Types.BIT);
            pstm.setObject(7, request.getNroSectorAnexo(), Types.VARCHAR);
            pstm.setObject(7, request.getIdUsuarioRegistro(), Types.INTEGER);
            pstm.setObject(7, request.getIdUsuarioModificacion(), Types.INTEGER);
            pstm.setObject(7, request.getEstado(), Types.VARCHAR);
            //pstm.setObject (7, request.getPersona().getCodInrena() ,Types.BIT);
            pstm.setObject(7, request.getNroPartidaRegistral(), Types.VARCHAR);
            pstm.setObject(7, request.getCodigoEvalEmpresa(), Types.VARCHAR);
            pstm.setObject(7, request.getTieneAreaComunidadCatastro(), Types.VARCHAR);
            pstm.setObject(7, request.getIdPersonaRepresentante(), Types.VARCHAR);
            pstm.setObject(7, request.getCodigoNivelPlanif(), Types.VARCHAR);
            pstm.setObject(7, request.getNroVoucherPago(), Types.VARCHAR);
            pstm.setObject(7, request.getRequiereEvalEntidadBancaria(), Types.VARCHAR);

            pstm.setObject(7, request.getCodigoRptaEntidadBancaria(), Types.VARCHAR);
            pstm.setObject(7, request.getNroActaAsamblea(), Types.VARCHAR);
            pstm.setObject(7, request.getCodigoTipoAprovechamiento(), Types.VARCHAR);
            pstm.setObject(7, request.getNroContratoTercero(), Types.VARCHAR);
            pstm.setObject(7, request.getNroActaAprovechamiento(), Types.VARCHAR);
            pstm.setObject(7, request.getEnvioSolicitudRealizado(), Types.VARCHAR);
            pstm.setObject(7, request.getNroSolicitudReconocimiento(), Types.VARCHAR);
            pstm.setObject(7, request.getNroSolicitudAmpliacion(), Types.VARCHAR);
            pstm.setObject(7, request.getRegenteForestal(), Types.VARCHAR);
            pstm.setObject(7, request.getEscalaManejo(), Types.VARCHAR);

            pstm.setObject(7, request.getIdSolicitudAcceso(), Types.VARCHAR);
            pstm.setObject(7, request.getIdPersonaRepresentante(), Types.VARCHAR);
            pstm.setObject(7, request.getIdEstadoSolicitud(), Types.VARCHAR);

            pstm.registerOutParameter(10, Types.INTEGER);
            pstm.execute();
            resul.setCodigo(pstm.getInt(10));
            //res+= ( pstm.getInt(10)>0?"":"Item:" + obj.getID_PLAN_MANEJO().toString()  +" => "+obj.getACTIVIDAD()  +",") ;
            resul.setInformacion(res);

            resul.setMessage((resul.getCodigo() > 0 ? "Información Actualizada" : "Informacion no Actualizada"));
            resul.setIsSuccess((resul.getCodigo() > 0 ? true : false));

        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setCodigo(0);
            resul.setIsSuccess(false);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (Exception e) {
                resul.setMessage(e.getMessage());
                resul.setCodigo(0);
                resul.setIsSuccess(false);
            }
        }
        return resul;
        */
    }


    @Override
    public ResultClassEntity ListarPermisosForestales(SolicitudEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ListarPermisosForestales");
            sp.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstadoSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoPersona", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idSolicitud", param.getIdSolicitud());
            sp.setParameter("idEstadoSolicitud", param.getIdEstadoSolicitud());
            sp.setParameter("perfil", param.getPerfil());
            sp.setParameter("tipoPersona", param.getTipoPersona()); 
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.setParameter("P_PAGENUM", param.getPageNum());
            sp.setParameter("P_PAGESIZE", param.getPageSize());
            sp.execute();
            List<SolicitudEntity> list = new ArrayList<SolicitudEntity>();
            SolicitudEntity obj = null;
            List<Object[]> spResult =sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    obj = new SolicitudEntity();
                    obj.setIdSolicitud(row[0] == null ?null :(Integer) row[0]);
                    obj.setEstadoSolicitud(row[1] == null ?null :(String) row[1]);
                    obj.setFechaRegistro(row[2] == null ?null :(Date) row[2]);
                    obj.setNombreRepresentante(row[3] == null ?null :(String) row[3]);
                    obj.setApellidoRepresentante(row[4] == null ?null :(String) row[4]);
                    obj.setTipoPersona(row[5] == null ?null :(String) row[5]);
                    obj.setDescripcionActor(row[6] == null ?null :(String) row[6]);
                    obj.setDescripcionCncc(row[7] == null ?null :(String) row[7]);
                    result.setTotalRecord(row[8] == null ?null :(Integer) row[8]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public Pageable<List<PermisoForestalEntity>> filtrar(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
        String codEstado, String codTipoPersona, String codTipoActor, String codTipoCncc, String numeroDocumento,
        String nombrePersona, String razonSocial, Page p) throws Exception {
        StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("pa_PermisoForestal_ListarPorFiltro");
        sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoPermiso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codEstado", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codTipoPersona", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codTipoActor", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codTipoCncc", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("nombrePersona", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("razonSocial", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("offset", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("pageSize", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortField", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortType", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPermisoForestal", idPermisoForestal);
        sp.setParameter("idTipoProceso", idTipoProceso);
        sp.setParameter("idTipoPermiso", idTipoPermiso);
        sp.setParameter("codEstado", codEstado);
        sp.setParameter("codTipoPersona", codTipoPersona);
        sp.setParameter("codTipoActor", codTipoActor);
        sp.setParameter("codTipoCncc", codTipoCncc);
        sp.setParameter("numeroDocumento", numeroDocumento);
        sp.setParameter("nombrePersona", nombrePersona);
        sp.setParameter("razonSocial", razonSocial);
        sp.setParameter("offset", p.getOffset());
        sp.setParameter("pageSize", p.getPageSize());
        sp.setParameter("sortField", p.getSortField());
        sp.setParameter("sortType", p.getSortType());
        sp.execute();
        return setResultDataFiltrar(p, sp.getResultList());
    }

    @Override
    public Pageable<List<PermisoForestalEntity>> filtrarEval(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
                                                             String fechaRegistro, String codigoEstado, String codigoPerfil, Page p) throws Exception {
        StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("pa_PermisoForestal_ListarPorFiltroEval");
        sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoPermiso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("fechaRegistro", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoPerfil", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("offset", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("pageSize", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortField", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortType", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPermisoForestal", idPermisoForestal);
        sp.setParameter("idTipoProceso", idTipoProceso);
        sp.setParameter("idTipoPermiso", idTipoPermiso);
        sp.setParameter("fechaRegistro", fechaRegistro);
        sp.setParameter("codigoEstado", codigoEstado);
        sp.setParameter("codigoPerfil", codigoPerfil);
        sp.setParameter("offset", p.getOffset());
        sp.setParameter("pageSize", p.getPageSize());
        sp.setParameter("sortField", p.getSortField());
        sp.setParameter("sortType", p.getSortType());
        sp.execute();
        return setResultDataFiltrar(p, sp.getResultList());
    }

    private Pageable<List<PermisoForestalEntity>> setResultDataFiltrar(Page page, List<Object[]> dataDb) throws Exception {
        Pageable<List<PermisoForestalEntity>> pageable = new Pageable<>(page);
        List<PermisoForestalEntity> items = new ArrayList<>();
        for (Object[] row : dataDb) {
            PermisoForestalEntity item = new PermisoForestalEntity();
            InformacionGeneralPermisoForestalEntity info = new InformacionGeneralPermisoForestalEntity();
            item.setIdPermisoForestal((Integer) row[0]);
            item.setDescripcion((String) row[1]);
            item.setIdSolicitud((Integer) row[2]);
            item.setIdTipoProceso((Integer) row[3]);
            item.setIdTipoPermiso((Integer) row[4]);
            item.setIdTipoEscala((short) row[5]);
            item.setFechaRegistro((Date)(row[6]));
            Calendar c = Calendar.getInstance();
            c.setTime(item.getFechaRegistro());
            c.add(Calendar.DATE, 90);//se agregó 90 días
            item.setFechaSolicitudFinal(c.getTime());
            int diasRestante = (int) ChronoUnit.DAYS.between(Instant.now(), item.getFechaSolicitudFinal().toInstant());
            if(diasRestante<0)diasRestante = 0;
            item.setDiasRestantes(diasRestante);
            item.setCodigoEstado((String)row[7]);
            item.setEstado((String)row[8]);
            info.setDocumentoElaborador((String) row[9]);
            info.setRucComunidad((String) row[10]);
            info.setNombreElaborador((String) row[11]);
            info.setApellidosElaborador((String) row[12]);
            info.setFederacionComunidad((String) row[13]);
            info.setCodTipoPersona((String) row[14]);
            info.setTipoPersona((String) row[15]);
            info.setCodTipoActor((String) row[16]);
            info.setTipoActor((String) row[17]);
            info.setCodTipoCncc((String) row[18]);
            info.setTipoCncc((String) row[19]);
            item.setRemitido((Boolean) row[21]);
            item.setCodigoUnico(row[22] == null ?null :(String) row[22]);
            item.setIdUsuarioRegistro((Integer) row[23]);
            item.setIdGeometria((Integer) row[24]);
            item.setInformacionGeneral(info);
            items.add(item);
            pageable.setTotalRecords(SpUtil.toLong(row[20]));
        }
        pageable.setData(items);
        return pageable;
    }

    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado:
     * @descripción: {Registrar Permiso Forestal}
     *
     */
    @Override
    public ResultClassEntity registrarPermisoForestal(PermisoForestalEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestal_Registrar");
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.INOUT);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoPermiso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoEscala", Short.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            sp.setParameter("descripcion", obj.getDescripcion());
            sp.setParameter("idSolicitud", obj.getIdSolicitud());
            sp.setParameter("idTipoProceso", obj.getIdTipoProceso());
            sp.setParameter("idTipoPermiso", obj.getIdTipoPermiso());
            sp.setParameter("tipoEscala", obj.getIdTipoEscala());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.execute();
            Integer id = (Integer) sp.getOutputParameterValue("idPermisoForestal");
            result.setCodigo(id);
            result.setSuccess(true);
            result.setMessage("Se registró permiso forestal correctamente.");
            return  result;
        } catch (Exception e) {
            log.error("registrarPermisoForestal - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarPermisoForestal(PermisoForestalEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestal_Actualizar");
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idGeometria", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            sp.setParameter("idGeometria", obj.getIdGeometria());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizo permiso forestal correctamente.");
            return  result;
        } catch (Exception e) {
            log.error("actualizarPermisoForestal - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [17-12-2021]
     * @modificado:
     * @descripción: {Actualizar estados de Permiso Forestal}
     *
     */

    @Override
    public ResultClassEntity actualizarEstadoPermisoForestal(PermisoForestalEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestal_ActualizarEstado");
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            sp.setParameter("estado", obj.getCodigoEstado());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.setParameter("observacion", obj.getObservacion());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó el estado del permiso forestal correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_PERMISOFORESTAL,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PERMISOFORESTAL + obj.getIdPermisoForestal(),
                    obj.getIdUsuarioRegistro());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return  result;
        } catch (Exception e) {
            log.error("registrarPermisoForestal - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado:
     * @descripción: {Registrar Información General de Permiso Forestal}
     *
     */
    @Override
    public ResultClassEntity registrarInformacionGeneral(InformacionGeneralPermisoForestalEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneralPermisoForestal_Registrar");
            sp.registerStoredProcedureParameter("idInfGeneral", Integer.class, ParameterMode.INOUT);
            sp.registerStoredProcedureParameter("codTipoInfGeneral", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreElaborador", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("apellidoPaternoElaborador", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("apellidoMaternoElaborador", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocumentoElaborador", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("documentoElaborador", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("rucComunidad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("federacionComunidad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("representanteLegal", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("domicilioLegal", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("domicilioLegalRegente", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoPersona", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoActor", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoCncc", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("esReprLegal", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ubigeoTitular", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("correo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("correoEmpresa", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idInfGeneral", obj.getIdInfGeneral());
            sp.setParameter("codTipoInfGeneral",  obj.getCodTipoInfGeneral());
            sp.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            sp.setParameter("fechaPresentacion", obj.getFechaPresentacion());
            sp.setParameter("nombreElaborador", obj.getNombreElaborador());
            sp.setParameter("apellidoPaternoElaborador", obj.getApellidoPaternoElaborador());
            sp.setParameter("apellidoMaternoElaborador", obj.getApellidoMaternoElaborador());
            sp.setParameter("tipoDocumentoElaborador", obj.getTipoDocumentoElaborador());
            sp.setParameter("documentoElaborador", obj.getDocumentoElaborador());
            sp.setParameter("rucComunidad", obj.getRucComunidad());
            sp.setParameter("federacionComunidad", obj.getFederacionComunidad());
            sp.setParameter("representanteLegal", obj.getRepresentanteLegal());
            sp.setParameter("domicilioLegal", obj.getDomicilioLegal());
            sp.setParameter("domicilioLegalRegente", obj.getDomicilioLegalRegente());
            sp.setParameter("codTipoDocumento", obj.getCodTipoDocumento());
            sp.setParameter("codTipoPersona", obj.getCodTipoPersona());
            sp.setParameter("codTipoActor", obj.getCodTipoActor());
            sp.setParameter("codTipoCncc", obj.getCodTipoCncc());
            sp.setParameter("esReprLegal", obj.getEsReprLegal());
            sp.setParameter("ubigeoTitular", obj.getUbigeoTitular());
            sp.setParameter("ubigeo", obj.getUbigeo());
            sp.setParameter("correo", obj.getCorreo());
            sp.setParameter("correoEmpresa", obj.getCorreoEmpresa());
            sp.setParameter("descripcion", obj.getDescripcion());
            sp.setParameter("detalle", obj.getDetalle());
            sp.setParameter("observacion", obj.getObservacion());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.execute();
            Integer id = (Integer) sp.getOutputParameterValue("idInfGeneral");
            result.setCodigo(id);
            result.setSuccess(true);
            result.setMessage("Se registró información general correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_PERMISOFORESTAL,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PERMISOFORESTAL + obj.getIdPermisoForestal(),
                    obj.getIdUsuarioRegistro());
            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return  result;
        } catch (Exception e) {
            log.error("registrarInformacionGeneral - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Danny Nazario [09-12-2021]
     * @modificado:
     * @descripción: {Listar Información General de Permiso Forestal}
     *
     */
    @Override
    public ResultClassEntity listarInformacionGeneral(InformacionGeneralPermisoForestalEntity filtro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneralPermisoForestal_Listar");
            sp.registerStoredProcedureParameter("idInfGeneral", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoInfGeneral", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idInfGeneral", filtro.getIdInfGeneral());
            sp.setParameter("codTipoInfGeneral", filtro.getCodTipoInfGeneral());
            sp.setParameter("idPermisoForestal", filtro.getIdPermisoForestal());
            sp.execute();

            List<Object[]> spResult = sp.getResultList();
            List<InformacionGeneralPermisoForestalEntity> list = new ArrayList<>();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    InformacionGeneralPermisoForestalEntity obj = new InformacionGeneralPermisoForestalEntity();
                    obj.setIdInfGeneral(row[0] == null ? null : (Integer) row[0]);
                    obj.setCodTipoInfGeneral(row[1] == null ? null : (String) row[1]);
                    obj.setIdPermisoForestal(row[2] == null ? null : (Integer) row[2]);
                    obj.setFechaPresentacion(row[3] == null ? null : (Date) row[3]);
                    obj.setNombreElaborador(row[4] == null ? null : (String) row[4]);
                    obj.setApellidoPaternoElaborador(row[5] == null ? null : (String) row[5]);
                    obj.setApellidoMaternoElaborador(row[6] == null ? null : (String) row[6]);
                    obj.setTipoDocumentoElaborador(row[7] == null ? null : (String) row[7]);
                    obj.setDocumentoElaborador(row[8] == null ? null : (String) row[8]);
                    obj.setRucComunidad(row[9] == null ? null : (String) row[9]);
                    obj.setFederacionComunidad(row[10] == null ? null : (String) row[10]);
                    obj.setRepresentanteLegal(row[11] == null ? null : (String) row[11]);
                    obj.setDomicilioLegal(row[12] == null ? null : (String) row[12]);
                    obj.setDomicilioLegalRegente(row[13] == null ? null : (String) row[13]);
                    obj.setCodTipoDocumento(row[14] == null ? null : (String) row[14]);
                    obj.setTipoDocumento(row[15] == null ? null : (String) row[15]);
                    obj.setCodTipoPersona(row[16] == null ? null : (String) row[16]);
                    obj.setTipoPersona(row[17] == null ? null : (String) row[17]);
                    obj.setCodTipoActor(row[18] == null ? null : (String) row[18]);
                    obj.setTipoActor(row[19] == null ? null : (String) row[19]);
                    obj.setCodTipoCncc(row[20] == null ? null : (String) row[20]);
                    obj.setTipoCncc(row[21] == null ? null : (String) row[21]);
                    obj.setEsReprLegal(row[22] == null ? null : String.valueOf((Character) row[22]));
                    obj.setUbigeoTitular(row[23] == null ? null : (String) row[23]);
                    obj.setUbigeo(row[24] == null ? null : (String) row[24]);
                    obj.setCorreo(row[25] == null ? null : (String) row[25]);
                    obj.setCorreoEmpresa(row[26] == null ? null : (String) row[26]);
                    obj.setDescripcion(row[27] == null ? null : (String) row[27]);
                    obj.setDetalle(row[28] == null ? null : (String) row[28]);
                    obj.setObservacion(row[29] == null ? null : (String) row[29]);
                    obj.setDistritoTitular(row[30] == null ? null : (String) row[30]);
                    obj.setProvinciaTitular(row[31] == null ? null : (String) row[31]);
                    obj.setDepartamentoTitular(row[32] == null ? null : (String) row[32]);
                    obj.setDistrito(row[33] == null ? null : (String) row[33]);
                    obj.setProvincia(row[34] == null ? null : (String) row[34]);
                    obj.setDepartamento(row[35] == null ? null : (String) row[35]);
                    obj.setIdDistrito(row[36] == null ? null : (Integer) row[36]);
                    obj.setIdProvincia(row[37] == null ? null : (Integer) row[37]);
                    obj.setIdDepartamento(row[38] == null ? null : (Integer) row[38]);
                    obj.setCodDistrito(row[39] == null ? null : (String) row[39]);
                    obj.setCodProvincia(row[40] == null ? null : (String) row[40]);
                    obj.setCodDepartamento(row[41] == null ? null : (String) row[41]);
                    obj.setCodEstadoPF(row[42] == null ? null : (String) row[42]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó información general.");
            return result;
        } catch (Exception e) {
            log.error("listarInformacionGeneral - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity RegistrarArchivo(List<SolicitudSANArchivoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudSANArchivoEntity> list_ = new ArrayList<>();
        try {
            for (SolicitudSANArchivoEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalArchivo_Insertar");
                sp.registerStoredProcedureParameter("idPermisoForestalArchivo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoSubTipo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(sp);
                sp.setParameter("idPermisoForestalArchivo", p.getIdPermisoForestalArchivo());
                sp.setParameter("codigoTipo", p.getCodigoArchivo());
                sp.setParameter("codigoSubTipo", p.getCodigoSubArchivo());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("idPermisoForestal", p.getIdPermisoForestal());
                sp.setParameter("idArchivo", p.getIdArchivo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        SolicitudSANArchivoEntity obj_ = new SolicitudSANArchivoEntity();
                        obj_.setIdPermisoForestalArchivo((Integer) row_[0]);
                        obj_.setCodigoArchivo((String) row_[1]);
                        obj_.setCodigoSubArchivo((String) row_[2]);
                        obj_.setDescripcion((String) row_[3]);
                        obj_.setObservacion((String) row_[4]);
                        obj_.setDetalle((String) row_[5]);
                        obj_.setIdPermisoForestal((Integer) row_[6]);
                        obj_.setIdArchivo((Integer) row_[7]);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el archivo de la Solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public List<SolicitudSANArchivoEntity> ListarArchivo(SolicitudSANArchivoEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalArchivo_Listar");
            processStored.registerStoredProcedureParameter("idPermisoForestalArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodigoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestalArchivo", param.getIdPermisoForestalArchivo());
            processStored.setParameter("idPermisoForestal", param.getIdPermisoForestal());
            processStored.setParameter("codigoTipoDocumento", param.getCodigoTipoDocumento());
            processStored.setParameter("codigoArchivo", param.getCodigoArchivo());
            processStored.setParameter("subCodigoArchivo", param.getCodigoSubArchivo());
            processStored.setParameter("idArchivo", param.getIdArchivo());

            processStored.execute();
            List<SolicitudSANArchivoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                SolicitudSANArchivoEntity temp = null;
                for (Object[] row: spResult){
                    temp = new SolicitudSANArchivoEntity();
                    temp.setIdPermisoForestalArchivo((Integer) row[0]);
                    temp.setCodigoArchivo((String) row[1]);
                    temp.setCodigoSubArchivo((String) row[2]);
                    temp.setDescripcion((String) row[3]);
                    temp.setObservacion((String) row[4]);
                    temp.setDetalle((String) row[5]);
                    temp.setIdPermisoForestal((Integer) row[6]);
                    temp.setIdArchivo((Integer) row[7]);
                    temp.setNombreArchivo((String) row[8]);
                    temp.setExtensionArchivo((String) row[9]);
                    temp.setCodigoTipoDocumento((String) row[10]);
                    if (!temp.getIdArchivo().equals(null)) {
                        ResultClassEntity<ArchivoEntity> file = fileUp.obtenerArchivo(temp.getIdArchivo());
                        temp.setDocumento(file.getData() == null ? null : file.getData().getFile());
                    }
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity EliminarArchivo(SolicitudSANArchivoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_PermisoForestalArhivo_Eliminar");
        processStored.registerStoredProcedureParameter("idPermisoForestalArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPermisoForestalArchivo", param.getIdPermisoForestalArchivo());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Lista Plan Manejo por filtro }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPorFiltroPlanManejo(String nroDocumento, String codTipoPlan) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalPlanManejo_ListarPorFiltro");
            sp.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("nroDocumento", nroDocumento);
            sp.setParameter("codTipoPlan", codTipoPlan);
            sp.execute();

            List<Object[]> spResult = sp.getResultList();
            List<PlanManejoEntity> objList = new ArrayList<>();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    PlanManejoEntity obj = new PlanManejoEntity();
                    obj.setIdPlanManejo(row[0] == null ? null : (Integer) row[0]);
                    obj.setDescripcion(row[1] == null ? null : (String) row[1]);
                    obj.setCodigoEstado(row[2] == null ? null : (String) row[2]);
                    obj.setEstado(row[3] == null ? null : (String) row[3]);
                    objList.add(obj);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("PermisoForestal - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Registrar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @Override
    public ResultClassEntity registrarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalPlanManejo_Registrar");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestal", item.getIdPermisoForestal());
            processStored.setParameter("idPlanManejo", item.getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            PermisoForestalPlanManejoEntity obj = new PermisoForestalPlanManejoEntity();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    obj.setIdPermisoForestal((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                }
            }
            result.setData(obj);
            result.setMessage("Se registró correctamente.");
            result.setSuccess(true);
        } catch (Exception e) {
            log.error("PermisoForestal - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }

        return result;
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado: Danny Nazario [07-01-2022]
     * @descripción: { Listar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @Override
    public ResultClassEntity listarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PermisoForestalPlanManejoEntity> objList = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalPlanManejo_Listar");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestal", item.getIdPermisoForestal());
            processStored.setParameter("codTipoPlan", item.getCodTipoPlan());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PermisoForestalPlanManejoEntity item2 = new PermisoForestalPlanManejoEntity();
                    item2.setIdPermisoForestal(row[0] == null ? null : (Integer) row[0]);
                    item2.setIdPlanManejo(row[1] == null ? null : (Integer) row[1]);
                    item2.setDescripcion(row[2] == null ? null : (String) row[2]);
                    item2.setCodigoEstado(row[3] == null ? null : (String) row[3]);
                    item2.setEstado(row[4] == null ? null : (String) row[4]);
                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Eliminar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @Override
    public ResultClassEntity eliminarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalPlanManejo_Eliminar");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestal", item.getIdPermisoForestal());
            processStored.setParameter("idPlanManejo", item.getIdPlanManejo());
            processStored.setParameter("idUsuarioElimina", item.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }
    @Override
    public ResultClassEntity permisoForestalActualizarRemitido(PermisoForestalPlanManejoEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestal_ActualizarRemitido");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("remitido", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPermisoForestal", request.getIdPermisoForestal());
            processStored.setParameter("remitido", request.getRemitido());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setData(request);
            result.setMessage("Se remitió el Permiso Forestal correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo remitir.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Rafael AZAÑA[01-02-2022]
     * @modificado:
     * @descripción: {Cifrado de codigo}
     *
     */
    @Override
    public ResultClassEntity codigoCifrado(CodigoCifradoEntity filtro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            String mensajeCifrado = "",mensajeDescifrado="";
  /* System.out.println("Rotando abcdefghijklmnopqrstuvwxyz ==>" + rot13("abcdefghijklmnopqrstuvwxyz"));
            System.out.println("Rotando ABCDEFGHIJKLMNOPQRSTUVWXYZ ==>" + rot13("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
            //String mensajeOriginal = "Hola, mundo! hacen algo hoy?";
            String mensajeCifrado = rot13(filtro.getCodigo());
            //String mensajeDescifrado = rot13(mensajeCifrado);
           // System.out.println("Original: " + mensajeOriginal);
           // System.out.println("Cifrado: " + mensajeCifrado);
            //System.out.println("Descifrado: " + mensajeDescifrado);
*/
            List<CodigoCifradoEntity> list = new ArrayList<>();

            CodigoCifradoEntity obj = new CodigoCifradoEntity();
            obj.setCodigo(filtro.getCodigo());
            if (filtro.getProceso().equals("C")){
                //mensajeCifrado = rot13(filtro.getCodigo());
                mensajeCifrado=cifrar(filtro.getCodigo(),SGD_SECRET_KEY_PROPERTIES);
                //mensajeCifrado=cifradoCesar(filtro.getCodigo(),4);
                obj.setCodigoCifrado(mensajeCifrado);
            }else if (filtro.getProceso().equals("D")){
                mensajeDescifrado = descifrar(filtro.getCodigo(),SGD_SECRET_KEY_PROPERTIES);
                //mensajeDescifrado=descifradoCesar(filtro.getCodigo(),4);
                obj.setCodigoDesCifrado(mensajeDescifrado);
            }
            list.add(obj);

            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se genero el cifrado correctamente.");
            return result;
        } catch (Exception e) {
            log.error("cifrado - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    public static final String SGD_SECRET_KEY_PROPERTIES = "SgDPropertiesSecretPasswor";
    public static final String SGD_SECRET_KEY_PASSWORD = "SgDPasswordSecretPasswor";

    /*************************************************************************************************************************/

    public static String cifrar(String sinCifrar, String key) throws Exception {
        final byte[] bytes = sinCifrar.getBytes("UTF-8");
        final Cipher aes = obtieneCipher(true,key);
        final byte[] cifrado = aes.doFinal(bytes);
        return DatatypeConverter.printHexBinary(cifrado);
    }
    private static Cipher obtieneCipher(boolean paraCifrar, String keyString) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA");
        digest.update(keyString.getBytes("UTF-8"));
        final SecretKeySpec key = new SecretKeySpec(digest.digest(), 0, 16, "AES");

        final Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
        if (paraCifrar) {
            aes.init(Cipher.ENCRYPT_MODE, key);
        } else {
            aes.init(Cipher.DECRYPT_MODE, key);
        }

        return aes;
    }

    public String descifrar(String cifrado,String key) throws Exception {
        byte[] bcadena = hexStrToByteArray(cifrado);
        final Cipher aes = obtieneCipher(false,key);
        final byte[] bytes = aes.doFinal(bcadena);
        final String sinCifrar = new String(bytes, "UTF-8");
        return sinCifrar;
    }
    public byte[] hexStrToByteArray(String hex) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(hex.length() / 2);
        for (int i = 0; i < hex.length(); i += 2) {
            String output = hex.substring(i, i + 2);
            int decimal = Integer.parseInt(output, 16);
            baos.write(decimal);
        }
        return baos.toByteArray();
    }



    public static String rotar(String cadenaOriginal, int rotaciones) {
        // En ASCII, la a es 97, b 98, A 65, B 66, etcétera
        final int LONGITUD_ALFABETO = 26, INICIO_MINUSCULAS = 97, INICIO_MAYUSCULAS = 65;
        String cadenaRotada = ""; // La cadena nueva, la que estará rotada
        for (int x = 0; x < cadenaOriginal.length(); x++) {
            char caracterActual = cadenaOriginal.charAt(x);
            // Si no es una letra del alfabeto entonces ponemos el char tal y como está
            // y pasamos a la siguiente iteración
            if (!Character.isLetter(caracterActual)) {
                cadenaRotada += caracterActual;
                continue;
            }

            int codigoAsciiDeCaracterActual = (int) caracterActual;
            boolean esMayuscula = Character.isUpperCase(caracterActual);

            // La posición (1 a 26) que ocupará la letra después de ser rotada
            // El % LONGITUD_ALFABETO se utiliza por si se pasa de 26. Por ejemplo,
            // la "z", al ser rotada una vez da el valor de 27, pero en realidad debería
            // regresar a la letra "a", y con mod hacemos eso ya que 27 % 26 == 1,
            // 28 % 26 == 2, etcétera ;)
            int nuevaPosicionEnAlfabeto = ((codigoAsciiDeCaracterActual
                    - (esMayuscula ? INICIO_MAYUSCULAS : INICIO_MINUSCULAS)) + rotaciones) % LONGITUD_ALFABETO;
            // Arreglar rotaciones negativas
            if (nuevaPosicionEnAlfabeto < 0)
                nuevaPosicionEnAlfabeto += LONGITUD_ALFABETO;
            int nuevaPosicionAscii = (esMayuscula ? INICIO_MAYUSCULAS : INICIO_MINUSCULAS) + nuevaPosicionEnAlfabeto;
            // Convertir el código ASCII numérico a su representación como símbolo o letra y
            // concatenar
            cadenaRotada += Character.toString((char) nuevaPosicionAscii);
        }
        return cadenaRotada;
    }

    public static String rot13(String cadenaOriginal) {
        return rotar(cadenaOriginal, 13);
    }

    public static String cifradoCesar(String texto, int codigo) {
        StringBuilder cifrado = new StringBuilder();
        codigo = codigo % 26;
        for (int i = 0; i < texto.length(); i++) {
            if (texto.charAt(i) >= 'a' && texto.charAt(i) <= 'z') {
                if ((texto.charAt(i) + codigo) > 'z') {
                    cifrado.append((char) (texto.charAt(i) + codigo - 26));
                } else {
                    cifrado.append((char) (texto.charAt(i) + codigo));
                }
            } else if (texto.charAt(i) >= 'A' && texto.charAt(i) <= 'Z') {
                if ((texto.charAt(i) + codigo) > 'Z') {
                    cifrado.append((char) (texto.charAt(i) + codigo - 26));
                } else {
                    cifrado.append((char) (texto.charAt(i) + codigo));
                }
            }
        }
        return cifrado.toString();
    }

    public static String descifradoCesar(String texto, int codigo) {
        StringBuilder cifrado = new StringBuilder();
        codigo = codigo % 26;
        for (int i = 0; i < texto.length(); i++) {
            if (texto.charAt(i) >= 'a' && texto.charAt(i) <= 'z') {
                if ((texto.charAt(i) - codigo) < 'a') {
                    cifrado.append((char) (texto.charAt(i) - codigo + 26));
                } else {
                    cifrado.append((char) (texto.charAt(i) - codigo));
                }
            } else if (texto.charAt(i) >= 'A' && texto.charAt(i) <= 'Z') {
                if ((texto.charAt(i) - codigo) < 'A') {
                    cifrado.append((char) (texto.charAt(i) - codigo + 26));
                } else {
                    cifrado.append((char) (texto.charAt(i) - codigo));
                }
            }
        }
        return cifrado.toString();
    }

}
