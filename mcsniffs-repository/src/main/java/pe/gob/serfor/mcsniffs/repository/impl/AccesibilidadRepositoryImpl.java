package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.AccesibilidadManejoMatrizDto;
import pe.gob.serfor.mcsniffs.repository.AccesibilidadRepository;
import pe.gob.serfor.mcsniffs.repository.AnexoAdjuntoRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


@Repository
public class AccesibilidadRepositoryImpl extends JdbcDaoSupport implements AccesibilidadRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.AccesibilidadRepositoryImpl.class);


    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", accesibilidadManejoEntity.getIdPlanManejo());
            processStored.execute();

            //crear el listado
            List<AccesibilidadManejoEntity> listAccesibilidadManejoEntity = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    AccesibilidadManejoEntity temp = new AccesibilidadManejoEntity();
                    temp.setIdAcceManejo((Integer) row[0]);
                    temp.setTipoAccesibilidad((Boolean) row[1]);
                    temp.setIdPlanManejo((Integer) row[2]);
                    listAccesibilidadManejoEntity.add(temp);
                }
            }

            result.setData(listAccesibilidadManejoEntity);
            result.setSuccess(true);
            result.setMessage("Se listó la Accesibilidad de Manejo.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }


    }

    @Override
    public ResultClassEntity RegistrarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejo_Registrar");
            processStored.registerStoredProcedureParameter("tipoAccesibilidad", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("tipoAccesibilidad", accesibilidadManejoEntity.getTipoAccesibilidad());
            processStored.setParameter("idPlanManejo", accesibilidadManejoEntity.getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", accesibilidadManejoEntity.getIdUsuarioRegistro());
            processStored.execute();
            result.setData(accesibilidadManejoEntity);
            result.setSuccess(true);
            result.setMessage("Se registró la Accesibilidad de Manejo correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejo_Actualizar");

            processStored.registerStoredProcedureParameter("idAcceManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoAccesibilidad", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejo", accesibilidadManejoEntity.getIdAcceManejo());
            processStored.setParameter("tipoAccesibilidad", accesibilidadManejoEntity.getTipoAccesibilidad());
            processStored.setParameter("idUsuarioModificacion", accesibilidadManejoEntity.getIdUsuarioModificacion());
            processStored.execute();

            result.setData(accesibilidadManejoEntity);
            result.setSuccess(true);
            result.setMessage("Se actualizó la Accesibilidad Manejo correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }


    }

    @Override
    public ResultClassEntity ListarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoRequisito_Listar");
            processStored.registerStoredProcedureParameter("idAcceManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejo", accesibilidadManejoRequisitoEntity.getIdAcceManejo());
            processStored.execute();

            //crear el listado
            List<AccesibilidadManejoRequisitoEntity> listAccesibilidadManejoRequisitoEntity = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    AccesibilidadManejoRequisitoEntity temp = new AccesibilidadManejoRequisitoEntity();
                    temp.setIdAcceManejoreq((Integer) row[0]);
                    temp.setRequisito((String) row[1]);
                    temp.setIdAcceManejo((Integer) row[2]);
                    listAccesibilidadManejoRequisitoEntity.add(temp);
                }
            }

            result.setData(listAccesibilidadManejoRequisitoEntity);
            result.setSuccess(true);
            result.setMessage("Se listó la accesibilidad manejo requisito.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            for(AccesibilidadManejoRequisitoEntity p:param){
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoRequisito_Registrar");
            processStored.registerStoredProcedureParameter("idAcceManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("requisito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejo", p.getIdAcceManejo());
            processStored.setParameter("requisito", p.getRequisito());
            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored.execute();


            }
            result.setSuccess(true);
            result.setMessage("Se registró la Accesibilidad de Manejo correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity> param) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        
            try {
                for(AccesibilidadManejoRequisitoEntity p:param){
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoRequisito_Actualizar");

            processStored.registerStoredProcedureParameter("idAcceManejoreq", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("requisito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejoreq", p.getIdAcceManejoreq());
            processStored.setParameter("requisito", p.getRequisito());
            processStored.setParameter("idUsuarioModificacion", p.getIdUsuarioModificacion());
            processStored.execute();
                }
                result.setSuccess(true);
            result.setMessage("Se actualizó la Accesibilidad de Manejo Requisito correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoRequisito_Eliminar");

            processStored.registerStoredProcedureParameter("idAcceManejoreq", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejoreq", accesibilidadManejoRequisitoEntity.getIdAcceManejoreq());
            processStored.setParameter("idUsuarioElimina", accesibilidadManejoRequisitoEntity.getIdUsuarioElimina());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        try{
            AccesibilidadManejoMatrizDto obj = new AccesibilidadManejoMatrizDto();
                for(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto:listAccesibilidadManejoMatrizDto) {
                    StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatriz_Registrar");
                    processStored_.registerStoredProcedureParameter("idAcceManejo", Integer.class, ParameterMode.IN);
                    processStored_.registerStoredProcedureParameter("idAcceManejomatriz", Integer.class, ParameterMode.IN);
                    processStored_.registerStoredProcedureParameter("anexosector", String.class, ParameterMode.IN);
                    processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored_.setParameter("idAcceManejo", accesibilidadManejoMatrizDto.getIdAcceManejo());
                    processStored_.setParameter("idAcceManejomatriz", accesibilidadManejoMatrizDto.getIdAcceManejomatriz());
                    processStored_.setParameter("anexosector", accesibilidadManejoMatrizDto.getAnexosector());
                    processStored_.setParameter("idUsuarioRegistro", accesibilidadManejoMatrizDto.getIdUsuarioRegistro());
                    processStored_.execute();
                     if(accesibilidadManejoMatrizDto.getIdAcceManejomatriz()==0){
                         Object retorno= processStored_.getSingleResult();
                         BigDecimal retornoIdentitie = (BigDecimal) retorno;
                         accesibilidadManejoMatrizDto.setIdAcceManejomatriz(retornoIdentitie.intValue());
                     }
                    obj.setIdAcceManejo(accesibilidadManejoMatrizDto.getIdAcceManejo());


                    for (AccesibilidadManejoMatrizDetalleEntity matrizDetalleEntity : accesibilidadManejoMatrizDto.getListAccesibilidadManejoMatrizDetalle()) {
                        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatrizDetalle_Registrar");
                        processStored.registerStoredProcedureParameter("idAcceManejomatriz", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idAcceManejomatrizdetalle", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("subparcela", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("via", String.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("distancia", Double.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("tiempo", Double.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("tipovehiculo", String.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                        processStored.setParameter("idAcceManejomatriz", accesibilidadManejoMatrizDto.getIdAcceManejomatriz());
                        processStored.setParameter("idAcceManejomatrizdetalle", matrizDetalleEntity.getIdAcceManejomatrizdetalle());
                        processStored.setParameter("subparcela", matrizDetalleEntity.getSubparcela());
                        processStored.setParameter("referencia", matrizDetalleEntity.getReferencia());
                        processStored.setParameter("via", matrizDetalleEntity.getVia());
                        processStored.setParameter("distancia", matrizDetalleEntity.getDistancia());
                        processStored.setParameter("tiempo", matrizDetalleEntity.getTiempo());
                        processStored.setParameter("tipovehiculo", matrizDetalleEntity.getTipovehiculo());
                        processStored.setParameter("idUsuarioRegistro", accesibilidadManejoMatrizDto.getIdUsuarioRegistro());
                        processStored.execute();
                    }

                }

            ListarAccesibilidadManejoMatriz(obj);

            result.setData(ListarAccesibilidadManejoMatriz(obj).getData());
            result.setSuccess(true);
            result.setMessage("Se registró la lista de Accesibilidad de Manejo  Matriz correctamente.");
            return  result;
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity ListarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try{
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatriz_Listar");
            processStored_.registerStoredProcedureParameter("idAcceManejo", Integer.class, ParameterMode.IN);
            processStored_.setParameter("idAcceManejo", accesibilidadManejoMatrizDto.getIdAcceManejo());
            processStored_.execute();
            List<AccesibilidadManejoMatrizDto> list = new ArrayList<>();
            List<Object[]> spResult = processStored_.getResultList();

            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    AccesibilidadManejoMatrizDto temp = new AccesibilidadManejoMatrizDto();
                    temp.setIdAcceManejomatriz((Integer) row[0]);
                    temp.setAnexosector((String) row[1]);
                    temp.setIdAcceManejo((Integer) row[2]);

                    List<AccesibilidadManejoMatrizDetalleEntity> listDetalle = new ArrayList<>();
                    StoredProcedureQuery processStored_det = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatrizDetalle_Listar");
                    processStored_det.registerStoredProcedureParameter("idAcceManejomatriz", Integer.class, ParameterMode.IN);
                    processStored_det.setParameter("idAcceManejomatriz", temp.getIdAcceManejomatriz());
                    processStored_det.execute();

                    List<Object[]> spResultDet = processStored_det.getResultList();
                    if (spResultDet.size() >= 1){
                        for (Object[] rowD: spResultDet){
                            AccesibilidadManejoMatrizDetalleEntity ammde = new AccesibilidadManejoMatrizDetalleEntity();
                            ammde.setIdAcceManejomatrizdetalle((Integer)rowD[0]);
                            ammde.setSubparcela((Integer) rowD[1]);
                            ammde.setReferencia((String) rowD[2]);
                            ammde.setVia((String) rowD[3]);
                            ammde.setDistancia(((BigDecimal) rowD[4]).doubleValue());
                            ammde.setTiempo(((BigDecimal) rowD[5]).doubleValue());
                            ammde.setTipovehiculo((String) rowD[6]);
                            listDetalle.add(ammde);
                        }
                    }
                    temp.setListAccesibilidadManejoMatrizDetalle(listDetalle);
                    list.add(temp);
                }
            }

            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó la Accesibilidad de Manejo  Matriz correctamente.");
            return  result;

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }

    }

    @Override
    public ResultClassEntity EliminarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatriz_Eliminar");
            processStored.registerStoredProcedureParameter("idAcceManejomatriz", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejomatriz", accesibilidadManejoMatrizDto.getIdAcceManejomatriz());
            processStored.setParameter("idUsuarioElimina", accesibilidadManejoMatrizDto.getIdUsuarioElimina());
            processStored.execute();
            result.setData(accesibilidadManejoMatrizDto);
            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try{
            for(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto:listAccesibilidadManejoMatrizDto) {
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatriz_Actualizar");
                processStored_.registerStoredProcedureParameter("idAcceManejomatriz", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("anexosector", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                processStored_.setParameter("idAcceManejomatriz", accesibilidadManejoMatrizDto.getIdAcceManejomatriz());
                processStored_.setParameter("anexosector", accesibilidadManejoMatrizDto.getAnexosector());
                processStored_.setParameter("idUsuarioModificacion", accesibilidadManejoMatrizDto.getIdUsuarioModificacion());
                processStored_.execute();

                for (AccesibilidadManejoMatrizDetalleEntity matrizDetalleEntity : accesibilidadManejoMatrizDto.getListAccesibilidadManejoMatrizDetalle()) {
                    StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatrizDetalle_Actualizar");
                    processStored.registerStoredProcedureParameter("idAcceManejomatrizdetalle", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("subparcela", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("via", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("distancia", Double.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tiempo", Double.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tipovehiculo", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idAcceManejomatrizdetalle", matrizDetalleEntity.getIdAcceManejomatrizdetalle());
                    processStored.setParameter("subparcela", matrizDetalleEntity.getSubparcela());
                    processStored.setParameter("referencia", matrizDetalleEntity.getReferencia());
                    processStored.setParameter("via", matrizDetalleEntity.getVia());
                    processStored.setParameter("distancia", matrizDetalleEntity.getDistancia());
                    processStored.setParameter("tiempo", matrizDetalleEntity.getTiempo());
                    processStored.setParameter("tipovehiculo", matrizDetalleEntity.getTipovehiculo());
                    processStored.setParameter("idUsuarioModificacion", accesibilidadManejoMatrizDto.getIdUsuarioModificacion());
                    processStored.execute();
                }

            }
            result.setData(listAccesibilidadManejoMatrizDto);
            result.setSuccess(true);
            result.setMessage("Se actualizó la Accesibilidad de Manejo  Matriz.");
            return  result;
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarAccesibilidadManejoMatrizDetalle(AccesibilidadManejoMatrizDetalleEntity accesibilidadManejoMatrizDetalleEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AccesibilidadManejoMatrizDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idAcceManejomatrizdetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAcceManejomatrizdetalle", accesibilidadManejoMatrizDetalleEntity.getIdAcceManejomatrizdetalle());
            processStored.setParameter("idUsuarioElimina", accesibilidadManejoMatrizDetalleEntity.getIdUsuarioElimina());
            processStored.execute();
            result.setData(accesibilidadManejoMatrizDetalleEntity);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

}
