package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoOfertaSolicitudOpinion.ProcesoOfertaSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ProcesoOfertaSolicitudOpinionRepository {

    ResultClassEntity ListarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param);
    ResultClassEntity RegistrarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param);
    ResultClassEntity ActualizarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param);
    ResultClassEntity EliminarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param);
    ResultClassEntity ObtenerProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param);

}
