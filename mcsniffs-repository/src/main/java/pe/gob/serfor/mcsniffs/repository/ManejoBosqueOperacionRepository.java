package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;

import java.util.List;

public interface ManejoBosqueOperacionRepository {
    ResultClassEntity RegistrarManejoBosqueOperacion(List<ManejoBosqueOperacionEntity> list) throws Exception;
    ResultClassEntity EliminarManejoBosqueOperacion(ManejoBosqueOperacionEntity manejoBosqueOperacion) throws Exception;
    ResultClassEntity ListarManejoBosqueOperacion(PlanManejoEntity planManejo) throws Exception;
}
