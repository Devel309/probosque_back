package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueRepository {
    ResultClassEntity RegistrarManejoBosque(ManejoBosqEntity manejoBosque) throws Exception;
    ResultClassEntity ObtenerManejoBosque(ManejoBosqEntity manejoBosque) throws Exception;

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {CRUD DE MANEJO DEL BOSQUE}
     * @param:PotencialProduccionForestalDto
     */

    List<ManejoBosqueEntity> ListarManejoBosque(Integer idPlanManejo, String codigo,String subCodigoGeneral,String subCodigoGeneralDet ) throws Exception;
    List<ManejoBosqueEntity> ListarManejoBosqueTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso) throws Exception;

    ResultClassEntity RegistrarManejoBosque(List<ManejoBosqueEntity> list) throws Exception;
    ResultClassEntity EliminarManejoBosque(ManejoBosqueDto manejoBosqueDto) throws Exception;


}
