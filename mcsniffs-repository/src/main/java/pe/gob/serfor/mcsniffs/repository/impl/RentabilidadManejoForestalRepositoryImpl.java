package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.RentabilidadManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RentabilidadManejoForestalRepositoryImpl extends JdbcDaoSupport implements RentabilidadManejoForestalRepository {

        @Autowired
        @Qualifier("dataSourceBDMCSNIFFS")
        DataSource dataSource;

        private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.RentabilidadManejoForestalRepositoryImpl.class);

        @PersistenceContext
        private EntityManager entityManager;


        @PostConstruct
        private void initialize(){
            setDataSource(dataSource);
        }



    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por filtro Objetivo Especifico de Parametos}
     * @param:ObjetivoEspecificoEntity
     */
    @Override
    public List<Dropdown> ComboRubroRentabilidadForestal(ParametroEntity param) throws Exception  {
        return null;
        /*
        try {
            List<ParametroEntity> listPar = new ArrayList<>();

            List<Dropdown> result = new ArrayList<Dropdown>();
            List<ParametroEntity> detalle = new ArrayList<ParametroEntity>();
            List<ParametroEntity> cabecera = new ArrayList<ParametroEntity>();

            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Parametro_ListarPorFiltro (?,?,?,?,?)}");
            pstm.setObject (1, param.getCodigo(), Types.VARCHAR);
            pstm.setObject(2, param.getValorPrimario(),Types.VARCHAR);
            pstm.setObject(3, param.getValorSecundario(),Types.VARCHAR);
            pstm.setObject (4, param.getIdTipoParametro(), Types.INTEGER);
            pstm.setObject(5, param.getIdParametroPadre(),Types.INTEGER);
            ResultSet rs = pstm.executeQuery();

            while(rs.next()){
                ParametroEntity temp = new ParametroEntity();
                temp.setCodigo(rs.getString(1));
                temp.setValorPrimario(rs.getString(2));
                temp.setValorSecundario(rs.getString(3));
                temp.setIdTipoParametro(rs.getInt(4));
                temp.setIdParametroPadre(rs.getInt(5));
                if(rs.getString(3)!="17"){
                    if(rs.getInt(5)==0){
                        cabecera.add(temp);
                    }
                    else{
                        detalle.add(temp);
                    }
                }

            }
            for (ParametroEntity customer : cabecera) {
                Dropdown obj = new Dropdown();
                obj.setLabel(customer.getValorPrimario());
                obj.setValue(customer.getValorSecundario());
                obj.setValor(Integer.parseInt(customer.getValorSecundario()));
                List<Dropdown.item> list = new ArrayList<Dropdown.item>();
                for (ParametroEntity customer_ : detalle) {
                    if (obj.getValor().equals(customer_.getIdParametroPadre())) {
                        Dropdown.item item = new Dropdown.item();
                        item.setLabel(customer_.getValorPrimario());
                        item.setValue(customer_.getValorSecundario());
                        item.setValor(Integer.parseInt(customer_.getValorSecundario()));
                        list.add(item);
                    }
                }
                obj.setItems(list);
                result.add(obj);
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e); log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        */
    }


    @Override
    public ResultClassEntity RegistrarRentabilidadManejoForestal(List<RentabilidadManejoForestalDto> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<RentabilidadManejoForestalDto> list = new ArrayList<RentabilidadManejoForestalDto>();
        try{
            for(RentabilidadManejoForestalDto p:param){
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestal_Registrar");
                processStored_.registerStoredProcedureParameter("idRentManejoForestal", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idTipoRubro", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("rubro", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codigoRentabilidad", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored_);
                processStored_.setParameter("idRentManejoForestal", p.getIdRentManejoForestal());
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.setParameter("idTipoRubro", p.getIdTipoRubro());
                processStored_.setParameter("rubro", p.getRubro());
                processStored_.setParameter("descripcion", p.getDescripcion());
                processStored_.setParameter("estado", p.getEstado());
                processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored_.setParameter("observacion", p.getObservacion());
                processStored_.setParameter("codigoRentabilidad", p.getCodigoRentabilidad());
                processStored_.execute();

                List<Object[]> spResult_ =processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        RentabilidadManejoForestalDto obj_ = new RentabilidadManejoForestalDto();
                        obj_.setIdRentManejoForestal((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setIdTipoRubro((Integer) row_[2]);
                        obj_.setRubro((String) row_[3]);
                        obj_.setDescripcion((String) row_[4]);
                        obj_.setCodigoRentabilidad((String) row_[5]);
                        obj_.setObservacion((String) row_[6]);
                        List<RentabilidadManejoForestalDetalleEntity> listDet = new ArrayList<RentabilidadManejoForestalDetalleEntity>();
                        for(RentabilidadManejoForestalDetalleEntity x:p.getListRentabilidadManejoForestalDetalle()){

                            StoredProcedureQuery processStored= entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestalDetalle_Registrar");
                            processStored.registerStoredProcedureParameter("idRentManejoForestal", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idRentManejoForestalDet", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("ano", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("monto", Double.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(processStored);
                            processStored.setParameter("idRentManejoForestal", obj_.getIdRentManejoForestal());
                            processStored.setParameter("idRentManejoForestalDet", x.getIdRentManejoForestalDet());
                            processStored.setParameter("ano", x.getAnio());
                            processStored.setParameter("monto", x.getMonto());
                            processStored.setParameter("estado", p.getEstado());
                            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                            processStored.setParameter("mes", x.getMes());
                            processStored.execute();

                            List<Object[]> spResult =processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    RentabilidadManejoForestalDetalleEntity obj = new RentabilidadManejoForestalDetalleEntity();
                                    obj.setIdRentManejoForestalDet((Integer) row[0]);
                                    obj.setAnio((Integer) row[1]);
                                    BigDecimal valor =(BigDecimal) row[2];
                                    obj.setMonto((Double) valor.doubleValue());
                                    obj.setMes(row[3] == null ? null : (Integer) row[3]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListRentabilidadManejoForestalDetalle(listDet);
                        list.add(obj_);
                    }
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró la rentabilidad de manejo forestal.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Jason Retamozo [21-01-2022]
     * @modificado:
     * @descripción: {Lista Rentabilidad en relacion a su Titular}
     * @param:ObjetivoEspecificoEntity
     */
    @Override
    public ResultClassEntity ListarRentabilidadManejoForestalTitular(PlanManejoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<RentabilidadManejoForestalDto> list = new ArrayList<RentabilidadManejoForestalDto>();
        try{
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestal_Listar_Titular");
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("codigoRentabilidad", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("tipoDoc", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("nroDoc", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("tipoPlanPadre", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored_);
            processStored_.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored_.setParameter("codigoRentabilidad", param.getCodigoParametro());
            processStored_.setParameter("tipoDoc", param.getDescripcion());
            processStored_.setParameter("nroDoc", param.getDescripcionContrato());
            processStored_.setParameter("tipoPlanPadre", param.getAspectoComplementario());
            processStored_.execute();

            List<Object[]> spResult_ =processStored_.getResultList();
            if (spResult_.size() >= 1) {
                for (Object[] row_ : spResult_) {
                    RentabilidadManejoForestalDto obj_ = new RentabilidadManejoForestalDto();
                    obj_.setIdRentManejoForestal((Integer) row_[0]);
                    obj_.setIdPlanManejo((Integer) row_[1]);
                    obj_.setIdTipoRubro((Integer) row_[2]);
                    obj_.setRubro((String) row_[3]);
                    obj_.setDescripcion((String) row_[4]);
                    obj_.setCodigoRentabilidad((String) row_[5]);
                    obj_.setObservacion((String) row_[6]);
                    if (param.getIdPlanManejo()!=null) {
                        if(!param.getIdPlanManejo().equals(0)){
                            List<RentabilidadManejoForestalDetalleEntity> listDet = new ArrayList<RentabilidadManejoForestalDetalleEntity>();
                            StoredProcedureQuery processStored= entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestalDetalle_Listar");
                            processStored.registerStoredProcedureParameter("idRentManejoForestal", Integer.class, ParameterMode.IN);
                            processStored.setParameter("idRentManejoForestal", obj_.getIdRentManejoForestal());

                            processStored.execute();

                            List<Object[]> spResult =processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    RentabilidadManejoForestalDetalleEntity obj = new RentabilidadManejoForestalDetalleEntity();
                                    obj.setIdRentManejoForestalDet((Integer) row[0]);
                                    obj.setAnio((Integer) row[1]);
                                    BigDecimal bd=(BigDecimal)row[2]; // the value you get
                                    double d = bd.doubleValue();
                                    obj.setMonto((Double) d);
                                    obj.setMes(row[3] == null ? null : (Integer) row[3]);
                                    listDet.add(obj);
                                }
                            }
                            obj_.setListRentabilidadManejoForestalDetalle(listDet);
                        }else{
                            obj_.setListRentabilidadManejoForestalDetalle(null);
                        }
                    }else{
                        obj_.setListRentabilidadManejoForestalDetalle(null);
                    }
                    list.add(obj_);
                }

            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se obtuvo rentabilidad de manejo forestal.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarRentabilidadManejoForestal(PlanManejoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<RentabilidadManejoForestalDto> list = new ArrayList<RentabilidadManejoForestalDto>();
        try{

                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestal_Listar");
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codigoRentabilidad", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored_);
                processStored_.setParameter("idPlanManejo", param.getIdPlanManejo());
                processStored_.setParameter("codigoRentabilidad", param.getCodigoParametro());
                processStored_.execute();

                List<Object[]> spResult_ =processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        RentabilidadManejoForestalDto obj_ = new RentabilidadManejoForestalDto();
                        obj_.setIdRentManejoForestal((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setIdTipoRubro((Integer) row_[2]);
                        obj_.setRubro((String) row_[3]);
                        obj_.setDescripcion((String) row_[4]);
                        obj_.setCodigoRentabilidad((String) row_[5]);
                        obj_.setObservacion((String) row_[6]);
                            if (param.getIdPlanManejo()!=null) {
                                if(!param.getIdPlanManejo().equals(0)){
                                    List<RentabilidadManejoForestalDetalleEntity> listDet = new ArrayList<RentabilidadManejoForestalDetalleEntity>();
                                    StoredProcedureQuery processStored= entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestalDetalle_Listar");
                                    processStored.registerStoredProcedureParameter("idRentManejoForestal", Integer.class, ParameterMode.IN);
                                    processStored.setParameter("idRentManejoForestal", obj_.getIdRentManejoForestal());
        
                                    processStored.execute();
        
                                    List<Object[]> spResult =processStored.getResultList();
                                    if (spResult.size() >= 1) {
                                        for (Object[] row : spResult) {
                                            RentabilidadManejoForestalDetalleEntity obj = new RentabilidadManejoForestalDetalleEntity();
                                            obj.setIdRentManejoForestalDet((Integer) row[0]);
                                            obj.setAnio((Integer) row[1]);
                                            BigDecimal bd=(BigDecimal)row[2]; // the value you get
                                            double d = bd.doubleValue();
                                            obj.setMonto((Double) d);
                                            obj.setMes(row[3] == null ? null : (Integer) row[3]);
                                            listDet.add(obj);
                                        }
                                    }
                                    obj_.setListRentabilidadManejoForestalDetalle(listDet);
                                }else{
                                    obj_.setListRentabilidadManejoForestalDetalle(null);
                                }
                            }else{
                                obj_.setListRentabilidadManejoForestalDetalle(null);
                            }
                        list.add(obj_);
                    }

            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se obtuvo rentabilidad de manejo forestal.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarRentabilidadManejoForestal(RentabilidadManejoForestalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RentabilidadManejoForestal_Eliminar");
            processStored.registerStoredProcedureParameter("idRentManejoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idRentManejoForestal", param.getIdRentManejoForestal());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");


            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultArchivoEntity descargarFormato() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/formatoProgramaInversiones.xlsx");
            File fileCopi = File.createTempFile("formatoProgramaInversiones",".xlsx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("formatoProgramaInversiones.xlsx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el formato.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

}
