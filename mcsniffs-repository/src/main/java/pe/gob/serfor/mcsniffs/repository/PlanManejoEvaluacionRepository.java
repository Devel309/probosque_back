package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.AprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvalSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacion2Dto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface PlanManejoEvaluacionRepository {
    List<PlanManejoEvaluacionDto> listarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;

    ResultClassEntity registrarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;
    ResultClassEntity actualizarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;
    ResultClassEntity eliminarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception;
    ResultClassEntity ActualizarPlanManejoEvaluacionEstado(PlanManejoEvaluacionDto request) throws Exception;

    ResultClassEntity ActualizarEvaluacionCampoPlanManejoEvaluacion(PlanManejoEvaluacionDto param);
    ResultClassEntity ListarPlanManejoEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param);
    ResultClassEntity obtenerUltimaEvaluacion(Integer idPlanManejo) throws Exception;
    ResultClassEntity actualizarEstadoPlanManejoEvaluacion(PlanManejoEvaluacionDto dto) throws Exception;
    ResultClassEntity obtenerEvaluacionPlanManejo(Integer idPlanManejo) throws Exception;
    ResultClassEntity actualizarIdSolicitudOpcionPlanManejoEvaluacion(PlanManejoEvalSolicitudOpinionDto planManejoEvalSolicitudOpinionDto) throws Exception;
    ResultClassEntity ActualizarNotificacionPlanManejoEvaluacion(PlanManejoEvaluacionDto param);
    ResultClassEntity validarLineamiento(PlanManejoEvaluacionDto param);
    ResultClassEntity retrotraerPlanManejoEvaluacion(PlanManejoEvaluacionDto param) throws Exception;
}
