package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface DemaAnexosRepository {
   ResultClassEntity<PGMFArchivoEntity> registrarArchivo(PGMFArchivoEntity archivo) throws Exception;
   ResultEntity<PlanManejoArchivoEntity> listarArchivoDema(PlanManejoArchivoEntity filtro);
   ResultArchivoEntity generarAnexo2Dema(PlanManejoArchivoEntity filtro);
   ResultArchivoEntity generarAnexo3Dema(PlanManejoArchivoEntity filtro);
}
