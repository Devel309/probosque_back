package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.Impugnacion.ImpugnacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ImpugnacionRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ImpugnacionRepositoryImpl extends JdbcDaoSupport implements ImpugnacionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(ImpugnacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarImpugnacion(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Impugnacion_Listar");
            sp.registerStoredProcedureParameter("nroResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaImpugnacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocGestion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            String datde=null;
            if(param.getFechaImpugnacion()!=null){
                datde= FechaUtil.obtenerFechaString(param.getFechaImpugnacion(),"yyyy-MM-dd");
            }
            sp.setParameter("nroResolucion", param.getNroResolucion());
            sp.setParameter("fechaImpugnacion", datde );
            sp.setParameter("estadoSolicitud", param.getEstadoSolicitud());
            sp.setParameter("nroDocGestion", param.getNroDocGestion());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.setParameter("perfil", param.getPerfil());
            sp.setParameter("tipoDocGestion", param.getTipoDocGestion());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());
            sp.execute();
            List<ImpugnacionDto> list = new ArrayList<ImpugnacionDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ImpugnacionDto obj = new ImpugnacionDto();
                    obj.setIdResolucion((Integer) row[0]);
                    obj.setIdImpugnacion((Integer) row[1]);
                    obj.setEstadoImpugnacion((String) row[2]);
                    obj.setAsunto((String) row[3]);
                    obj.setDescripcion((String) row[4]);
                    obj.setFechaImpugnacion((Date) row[5]);
                    obj.setIdArchivoRecurso((Integer) row[6]);
                    obj.setIdArchivoSustento((Integer) row[7]);
                    obj.setTipoImpugnacion((String) row[8]);
                    obj.setIdArchivoEvaluacion((Integer) row[9]);
                    obj.setEsfundado((Boolean) row[10]);
                    obj.setMensaje((String) row[11]);
                    obj.setDesEstadoImpugnacion((String) row[12]);
                    obj.setDocumentoGestion((String) row[13]);
                    obj.setNroDocGestion((Integer) row[14]);
                    obj.setIdUsuarioRegistro((Integer) row[15]);
                    result.setTotalRecord((Integer) row[16]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity notificarSolicitudImpugnacion(ImpugnacionDto param){
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_NotificacionSolicitudImpugnacion_Listar");
            sp.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("perfil", param.getPerfil());
            sp.execute();

            List<ImpugnacionDto> list = new ArrayList<ImpugnacionDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ImpugnacionDto obj = new ImpugnacionDto();
                    obj.setIdResolucion((Integer) row[0]);
                    obj.setIdImpugnacion((Integer) row[1]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }



    @Override
    public ResultClassEntity ListarResolucionImpugnada(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ImpugnacionResolucion_Listar");
            sp.registerStoredProcedureParameter("nroResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaImpugnacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocGestion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            String datde=null;
            if(param.getFechaImpugnacion()!=null){
                datde= FechaUtil.obtenerFechaString(param.getFechaImpugnacion(),"yyyy-MM-dd");
            }
            sp.setParameter("nroResolucion", param.getNroResolucion());
            sp.setParameter("fechaImpugnacion", datde );
            sp.setParameter("estadoSolicitud", param.getEstadoSolicitud());
            sp.setParameter("nroDocGestion", param.getNroDocGestion());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.setParameter("perfil", param.getPerfil());
            sp.setParameter("tipoDocGestion", param.getTipoDocGestion());
            sp.execute();
            List<ImpugnacionDto> list = new ArrayList<ImpugnacionDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ImpugnacionDto obj = new ImpugnacionDto();
                    obj.setIdResolucion((Integer) row[0]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }
    @Override
    public ResultClassEntity RegistrarImpugnacion(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Impugnacion_Registrar");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaImpugnacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoRecurso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoSustento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("esfundado", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("mensaje", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idImpugnacion", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("asunto", param.getAsunto());
            sp.setParameter("descripcion", param.getDescripcion());
            sp.setParameter("fechaImpugnacion", param.getFechaImpugnacion());
            sp.setParameter("idArchivoRecurso", param.getIdArchivoRecurso());
            sp.setParameter("idArchivoSustento", param.getIdArchivoSustento());
            sp.setParameter("esfundado", param.getEsfundado());
            sp.setParameter("mensaje", param.getMensaje());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.execute();

            Integer idImpugnacion = (Integer) sp.getOutputParameterValue("idImpugnacion");

            result.setCodigo(idImpugnacion);
            result.setSuccess(true);
            result.setMessage("Mensaje enviado correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ActualizarImpugnacion(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Impugnacion_Actualizar");
            sp.registerStoredProcedureParameter("idImpugnacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoImpugnacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaImpugnacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoRecurso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoSustento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoImpugnacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoEvaluacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoFirme", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("esfundado", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("retrotraer", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("mensaje", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idImpugnacion", param.getIdImpugnacion());
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("estadoImpugnacion", param.getEstadoImpugnacion());
            sp.setParameter("asunto", param.getAsunto());
            sp.setParameter("descripcion", param.getDescripcion());
            sp.setParameter("fechaImpugnacion", param.getFechaImpugnacion());
            sp.setParameter("idArchivoRecurso", param.getIdArchivoRecurso());
            sp.setParameter("idArchivoSustento", param.getIdArchivoSustento());
            sp.setParameter("tipoImpugnacion", param.getTipoImpugnacion());
            sp.setParameter("idArchivoEvaluacion", param.getIdArchivoEvaluacion());
            sp.setParameter("idArchivoFirme", param.getIdArchivoFirme());
            sp.setParameter("esfundado", param.getEsfundado());
            sp.setParameter("retrotraer", param.getRetrotraer());
            sp.setParameter("mensaje", param.getMensaje());
            sp.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se Actualizo la Impugnación correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_IMPUGNACION,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_IMPUGNACION + param.getIdImpugnacion(),
                    param.getIdUsuarioModificacion());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ObtenerImpugnacion(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Impugnacion_Obtener");
            sp.registerStoredProcedureParameter("idImpugnacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idImpugnacion", param.getIdImpugnacion());
            sp.execute();
            ImpugnacionDto obj = new ImpugnacionDto();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    obj.setIdImpugnacion((Integer) row[0]);
                    obj.setIdResolucion((Integer) row[1]);
                    obj.setEstadoImpugnacion((String) row[2]);
                    obj.setAsunto((String) row[3]);
                    obj.setDescripcion((String) row[4]);
                    obj.setFechaImpugnacion((Date) row[5]);
                    obj.setIdArchivoRecurso((Integer) row[6]);
                    obj.setIdArchivoSustento((Integer) row[7]);
                    obj.setTipoImpugnacion((String) row[8]);
                    obj.setIdArchivoEvaluacion((Integer) row[9]);
                    obj.setEsfundado((Boolean) row[10]);
                    obj.setMensaje((String) row[11]);
                    obj.setNombreArchivoRecurso((String) row[12]);
                    obj.setIdArchivoResolucion((Integer) row[13]);
                    obj.setEstadoResolucion((String) row[14]);
                    obj.setNombreArchivoResolucion((String) row[15]);
                    obj.setNombreArchivoSustento((String) row[16]);
                    obj.setNombreArchivoEvaluacion((String) row[17]);
                    obj.setRetrotraer((Boolean) row[18]);
                    obj.setNroDocGestion((Integer) row[19]);
                    obj.setTipoDocGestion((String) row[20]);
                    obj.setNombreArchivoFirme((String) row[21]);
                    obj.setIdArchivoFirme((Integer) row[22]);
                    obj.setFechaRegistroFirme((Date) row[23]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Información encontrada");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ActualizarImpugnacionArchivo(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ImpugnacionArchivo_Actualizar");
            processStored.registerStoredProcedureParameter("idImpugnacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoRecurso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSustento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoFirme", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idImpugnacion", param.getIdImpugnacion());
            processStored.setParameter("idArchivoRecurso", param.getIdArchivoRecurso());
            processStored.setParameter("idArchivoSustento", param.getIdArchivoSustento());
            processStored.setParameter("idArchivoFirme", param.getIdArchivoFirme());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Impugnación Archivo correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_IMPUGNACION,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_IMPUGNACION + param.getIdImpugnacion(),
                    param.getIdUsuarioModificacion());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessage(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarImpugnacionArchivo(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ImpugnacionArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idImpugnacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idImpugnacion", param.getIdImpugnacion());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó Solicitud Opinión Archivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ValidarImpugnacionResolucion(ImpugnacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ImpugnacionResolucion_Validar");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());

            sp.execute();

            List<String> spResult = sp.getResultList();
            String mensaje = "";
            if (spResult!= null && !spResult.isEmpty()){
                mensaje = spResult.get(0);

            }

            result.setSuccess(true);
            result.setMessage(mensaje);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo validar el plazo máximo para impugnar la resolución.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }


}
