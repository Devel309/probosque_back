package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.repository.ObjetivoManejoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ObjetivoManejoRepositoryImpl extends JdbcDaoSupport implements ObjetivoManejoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.ObjetivoManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarObjetivoManejo(ObjetivoManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoManejo_Registrar");
            processStored.registerStoredProcedureParameter("general", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoObjetivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("general", param.getGeneral());
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("codigoObjetivo", param.getCodigoObjetivo());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    param.setIdObjManejo((Integer) row[0]);
                }
            }
            if(param.getCodigoObjetivo()!=null &&  param.getListDetalle()!=null){
                if(param.getCodigoObjetivo().equals("PGMFA") && param.getListDetalle().size()>0){
                    for (ObjetivoEspecificoManejoEntity objects : param.getListDetalle()) {
                        ObjetivoManejoEntity objM = new ObjetivoManejoEntity();
                        objM.setIdObjManejo(param.getIdObjManejo());
                        objects.setObjetivoManejo(objM);
                    }
                    ResultClassEntity registrarDetalle = RegistrarObjetivoEspecificoManejo(param.getListDetalle());
                    result.setInformacion(registrarDetalle.getMessage());
                }
            }
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró el Objetivo de Manejo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity ActualizarObjetivoManejo(ObjetivoManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoManejo_Actualizar");
            processStored.registerStoredProcedureParameter("idObjManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("general", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idObjManejo", param.getIdObjManejo());
            processStored.setParameter("general", param.getGeneral());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ObjetivoManejoEntity objM = new ObjetivoManejoEntity();
                    objM.setIdObjManejo((Integer) row[0]);
                }
            }
            if(param.getCodigoObjetivo()!=null &&  param.getListDetalle()!=null){
                if(param.getCodigoObjetivo().equals("PGMFA") && param.getListDetalle().size()>0){
                    ResultClassEntity registrarDetalle =  ActualizarObjetivoEspecificoManejoMarcado(param.getListDetalle());
                    result.setInformacion(registrarDetalle.getMessage());
                }
            }
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se actualizó el Objetivo de Manejo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity ObtenerObjetivoManejo(ObjetivoManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoManejo_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());

            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity pla = new PlanManejoEntity();
                    param.setIdObjManejo((Integer) row[0]);
                    pla.setIdPlanManejo((Integer) row[1]);
                    param.setPlanManejo(pla);
                    param.setGeneral((String) row[2]);
                }
            }


            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se obtuvo Objetivos de Manejo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity RegistrarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> list) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ObjetivoEspecificoManejoEntity> list_ = new ArrayList<ObjetivoEspecificoManejoEntity>() ;
            for(ObjetivoEspecificoManejoEntity p:list){

                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoEspecificoManejo_Registrar");
                processStored.registerStoredProcedureParameter("idObjManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("desObjEspecifico", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("detalleOtro", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoObjEspecifico", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idObjManejo", p.getObjetivoManejo().getIdObjManejo());
                processStored.setParameter("desObjEspecifico", p.getDesObjEspecifico());
                processStored.setParameter("detalleOtro", p.getDetalleOtro());
                processStored.setParameter("idTipoObjEspecifico", p.getIdTipoObjEspecifico());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();

                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ObjetivoEspecificoManejoEntity objE = new ObjetivoEspecificoManejoEntity();
                        ObjetivoManejoEntity objM = new ObjetivoManejoEntity();
                        ObjetivoEspecificoEntity objEs= new ObjetivoEspecificoEntity();
                        objM.setIdObjManejo((Integer) row[0]);
                        objE.setObjetivoManejo(objM);
                        objE.setIdObjEspecificoManejo((Integer) row[1]);
                        objE.setDesObjEspecifico((String) row[2]);
                        objE.setDetalleOtro((String) row[3]);
                        objE.setIdTipoObjEspecifico((String) row[4]);
                        list_.add(objE);
                    }
                }

            }

            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró Objetivo Especifico correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    public ResultClassEntity ActualizarObjetivoEspecificoManejoMarcado(List<ObjetivoEspecificoManejoEntity> list) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ObjetivoEspecificoManejoEntity> list_ = new ArrayList<ObjetivoEspecificoManejoEntity>() ;
            for(ObjetivoEspecificoManejoEntity p:list){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoEspecificoManejo_ActualizarMarcado");
                processStored.registerStoredProcedureParameter("idObjManejoEspecifico", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("detalleOtro", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descEspecifico", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idObjManejoEspecifico", p.getIdObjEspecificoManejo());
                processStored.setParameter("detalleOtro", p.getDetalleOtro());
                processStored.setParameter("idUsuarioModificacion", p.getIdUsuarioModificacion());
                processStored.setParameter("descEspecifico", p.getDesObjEspecifico());
                processStored.execute();
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se actualizó Objetivo Especifico correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity ActualizarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> list) {
        ResultClassEntity result = new ResultClassEntity();
        try{


            ObjetivoEspecificoManejoEntity obj =list.get(0);;
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoEspecificoManejo_Inactivar");

            processStored_.registerStoredProcedureParameter("idObjManejo", Integer.class, ParameterMode.IN);
            processStored_.setParameter("idObjManejo", obj.getObjetivoManejo().getIdObjManejo());
            processStored_.execute();

            List<ObjetivoEspecificoManejoEntity> list_ = new ArrayList<ObjetivoEspecificoManejoEntity>() ;
            for(ObjetivoEspecificoManejoEntity p:list){

                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoEspecificoManejo_Actualizar");
                processStored.registerStoredProcedureParameter("idObjManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("desObjEspecifico", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("detalleOtro", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoObjEspecifico", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idObjManejo", p.getObjetivoManejo().getIdObjManejo());
                processStored.setParameter("desObjEspecifico", p.getDesObjEspecifico());
                processStored.setParameter("detalleOtro", p.getDetalleOtro());
                processStored.setParameter("idTipoObjEspecifico", p.getIdTipoObjEspecifico());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioModificacion", p.getIdUsuarioModificacion());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ObjetivoEspecificoManejoEntity objE = new ObjetivoEspecificoManejoEntity();
                        ObjetivoManejoEntity objM = new ObjetivoManejoEntity();
                        ObjetivoEspecificoEntity objEs= new ObjetivoEspecificoEntity();
                        objM.setIdObjManejo((Integer) row[0]);
                        objE.setObjetivoManejo(objM);
                        objE.setIdObjEspecificoManejo((Integer) row[1]);
                        objE.setDesObjEspecifico((String) row[2]);
                        objE.setDetalleOtro((String) row[3]);
                        objE.setIdTipoObjEspecifico((String) row[4]);

                        list_.add(objE);
                    }
                }
            }



            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se actualizó Objetivo Especifico correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }


    @Override
    public ResultClassEntity ObtenerObjetivoEspecificoManejo(ObjetivoManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoEspecificoManejo_Obtener");
            processStored.registerStoredProcedureParameter("idObjManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idObjManejo", param.getIdObjManejo());
            List<ObjetivoEspecificoManejoEntity> list = new ArrayList<ObjetivoEspecificoManejoEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ObjetivoEspecificoManejoEntity objE = new ObjetivoEspecificoManejoEntity();
                    ObjetivoManejoEntity objM = new ObjetivoManejoEntity();
                    ObjetivoEspecificoEntity objEs= new ObjetivoEspecificoEntity();
                    objM.setIdObjManejo((Integer) row[0]);
                    objE.setObjetivoManejo(objM);
                    objE.setIdObjEspecificoManejo((Integer) row[1]);
                    objE.setDesObjEspecifico((String) row[2]);
                    objE.setDetalleOtro((String) row[3]);
                    objE.setIdTipoObjEspecifico((String) row[4]);
                    list.add(objE);
                }
            }


            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se obtuvo Objetivo Especifico.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }



    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por filtro Objetivo Especifico de Parametos}
     * @param:ObjetivoEspecificoEntity
     */
    @Override
    public ResultClassEntity ComboPorFiltroObjetivoEspecifico(ParametroEntity param) throws Exception  {
        ResultClassEntity result = new ResultClassEntity();
        try {
            List<Dropdown> listDropdown = new ArrayList<Dropdown>();
            List<ParametroEntity> detalle = new ArrayList<ParametroEntity>();
            List<ParametroEntity> cabecera = new ArrayList<ParametroEntity>();
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_ListarPorFiltro");
            sp.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("valorPrimario", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("valorSecundario", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoParametro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idParametroPadre", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("codigo", param.getCodigo());
            sp.setParameter("valorPrimario", param.getValorPrimario());
            sp.setParameter("valorSecundario", param.getValorSecundario());
            sp.setParameter("idTipoParametro", param.getIdTipoParametro());
            sp.setParameter("idParametroPadre", param.getIdParametroPadre());
            sp.setParameter("prefijo", param.getPrefijo());
            sp.execute();
            List<Object[]> spResult = sp.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ParametroEntity temp = new ParametroEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValorPrimario((String) row[1]);
                    temp.setValorSecundario((String) row[2]);
                    temp.setIdTipoParametro((Integer) row[3]);
                    temp.setIdParametroPadre((Integer) row[4]);
                    if((Integer) row[4] == null) {
                        cabecera.add(temp);
                    }else {
                        if((Integer) row[4] == 0) {
                            cabecera.add(temp);
                        }else {
                            detalle.add(temp);
                        }
                    }
                }
            }
            for (ParametroEntity customer : cabecera) {
                Dropdown obj = new Dropdown();
                obj.setLabel(customer.getValorPrimario());
                obj.setValue(customer.getValorSecundario());
                obj.setValor(Integer.parseInt(customer.getValorSecundario()));
                List<Dropdown.item> list = new ArrayList<Dropdown.item>();
                for (ParametroEntity customer_ : detalle) {
                    if (obj.getValor().equals(customer_.getIdParametroPadre())) {
                        Dropdown.item item = new Dropdown.item();
                        item.setLabel(customer_.getValorPrimario());
                        item.setValue(customer_.getValorSecundario());
                        item.setValor(Integer.parseInt(customer_.getValorSecundario()));
                        list.add(item);
                    }
                }
                obj.setItems(list);
                listDropdown.add(obj);
            }
            result.setData(listDropdown);
            result.setSuccess(true);
            result.setMessage("Información encontrada.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Jaqueline DB [20-09-2021]
     * @modificado:
     * @descripción: {Elimina los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultClassEntity eliminarObjetivoManejo(ObjetivoManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoManejo_Eliminar");
            processStored.registerStoredProcedureParameter("idObjManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioEliminacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idObjManejo", param.getIdObjManejo());
            processStored.setParameter("idUsuarioEliminacion", param.getIdUsuarioElimina());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminÓ el Objetivo de Manejo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error al ejecutar el Procedimiento pa_ObjetivoManejo_Eliminar.");
            return  result;
        }
    }

    /**
     * @autor: Jason Retamozo [20-01-2022]
     * @modificado:
     * @descripción: {listar los objetivos relacionados con Titular}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultEntity<ObjetivoDto> listarObjetivoManejoTitular(ObjetivoManejoEntity param) {
        ResultEntity<ObjetivoDto> result = new ResultEntity<ObjetivoDto>();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoManejo_ListarporFiltro_Titular");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoObjetivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDoc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDoc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlanPadre", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoObjetivo", param.getCodigoObjetivo());
            processStored.setParameter("tipoDoc", param.getDescripcion());
            processStored.setParameter("nroDoc", param.getDetalle());
            processStored.setParameter("tipoPlanPadre", param.getObservacion());
            List<ObjetivoDto> lstResult = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (!spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ObjetivoDto obj = new ObjetivoDto();
                    obj.setIdObjManejo(row[0]==null?null:(Integer) row[0]);
                    obj.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                    obj.setCodigoObjetivo(row[2]==null?null:(String) row[2]);
                    obj.setDescripcion(row[3]==null?null:(String) row[3]);
                    obj.setIdObjetivoDet(row[4]==null?null:(Integer) row[4]);
                    obj.setDescripcionDetalle(row[5]==null?null:(String) row[5]);
                    obj.setDetalle(row[6]==null?null:(String) row[6]);
                    obj.setActivo(row[7]==null?null:String.valueOf((Character) row[7]));
                    obj.setObservacionCabera(row[8]==null?null:(String) row[8]);
                    obj.setDetalleCabera(row[9]==null?null:(String) row[9]);
                    obj.setDescripcionCabera(row[10]==null?null:(String) row[10]);
                    lstResult.add(obj);
                }
            }
            result.setData(lstResult);
            result.setIsSuccess(true);
            result.setMessage("Se obtuvo los Objetivos correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }
    }



    /**
     * @autor: Jaqueline DB [21-09-2021]
     * @modificado:
     * @descripción: {listar los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultEntity<ObjetivoManejoEntity> listarObjetivoManejo(ObjetivoManejoEntity param) {
        ResultEntity<ObjetivoManejoEntity> result = new ResultEntity<ObjetivoManejoEntity>();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoManejo_ListarporFiltro");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoObjetivo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoObjetivo", param.getCodigoObjetivo());
            List<ObjetivoManejoEntity> lstResult = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ObjetivoManejoEntity obj = new ObjetivoManejoEntity();
                    obj.setIdObjManejo((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setGeneral((String) row[2]);
                    if(param.getCodigoObjetivo()!=null){
                        if(param.getCodigoObjetivo().equals("PGMFA")){
                            ResultClassEntity lstEspecificoC = ObtenerObjetivoEspecificoManejo(obj);
                            List<ObjetivoEspecificoManejoEntity> lstEspeci = (List<ObjetivoEspecificoManejoEntity>) lstEspecificoC.getData();
                            if(lstEspeci.size()>0){
                                obj.setListDetalle(lstEspeci);
                            }else{
                                obj.setListDetalle(null);
                            }
                        }
                    }
                    lstResult.add(obj);
                }
            }
            result.setData(lstResult);
            result.setIsSuccess(true);
            result.setMessage("Se obtuvo Objetivos de Manejo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }
    }


    /**
     * @autor: Harry coa [14-10-2021]
     * @modificado:
     * @descripción: {listar los objetivos por defecto y por idPlan y codigo}
     * @param:ObjetivoManejoEntity
     */
    @Override
    public ResultEntity<ObjetivoDto> listarObjetivos(String codigoObjetivo, Integer idPlanManejo) {
        ResultEntity<ObjetivoDto> result = new ResultEntity<>();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Objetivo_Listar");
            processStored.registerStoredProcedureParameter("codigoObjetivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoObjetivo", codigoObjetivo);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            List<ObjetivoDto> lstResult = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (!spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ObjetivoDto obj = new ObjetivoDto();
                    obj.setIdObjManejo(row[0]==null?null:(Integer) row[0]);
                    obj.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                    obj.setCodigoObjetivo(row[2]==null?null:(String) row[2]);
                    obj.setDescripcion(row[3]==null?null:(String) row[3]);
                    obj.setIdObjetivoDet(row[4]==null?null:(Integer) row[4]);
                    obj.setDescripcionDetalle(row[5]==null?null:(String) row[5]);
                    obj.setDetalle(row[6]==null?null:(String) row[6]);
                    obj.setActivo(row[7]==null?null:String.valueOf((Character) row[7]));
                    obj.setObservacionCabera(row[8]==null?null:(String) row[8]);
                    obj.setDetalleCabera(row[9]==null?null:(String) row[9]);
                    obj.setDescripcionCabera(row[10]==null?null:(String) row[10]);
                    lstResult.add(obj);
                }
            }
            result.setData(lstResult);
            result.setIsSuccess(true);
            result.setMessage("Se obtuvo los Objetivos correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarObjetivo(List<ObjetivoManejoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ObjetivoManejoEntity> list_ = new ArrayList<>();
        try {
            for (ObjetivoManejoEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Objetivo_Registrar");
                sp.registerStoredProcedureParameter("idObjetivo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoObjetivo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("general", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idObjetivo", p.getIdObjManejo());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codigoObjetivo", p.getCodigoObjetivo());
                sp.setParameter("general", p.getGeneral());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_){
                        ObjetivoManejoEntity obj_ = new ObjetivoManejoEntity();
                        obj_.setIdObjManejo(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodigoObjetivo(row_[1]==null?null:(String) row_[1]);
                        obj_.setGeneral(row_[2]==null?null:(String) row_[2]);
                        obj_.setIdPlanManejo(row_[3]==null?null:(Integer) row_[3]);
                        obj_.setDescripcion(p.getDescripcion());
                        obj_.setDetalle(p.getDetalle());
                        obj_.setObservacion(p.getObservacion());
                        List<ObjetivoEspecificoManejoEntity> listDet = new ArrayList<>();
                        for (ObjetivoEspecificoManejoEntity param: p.getListDetalle()){
                            StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_ObjetivoDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idObjetivoDet", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codTipo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoObjetivo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("activo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idObjetivo", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(pa);
                            pa.setParameter("idObjetivoDet", param.getIdObjEspecificoManejo());
                            pa.setParameter("codTipo", param.getCodTipo());
                            pa.setParameter("tipoObjetivo", param.getTipoObjetivo());
                            pa.setParameter("descripcion", param.getDescripcion());
                            pa.setParameter("detalle", param.getDetalle());
                            pa.setParameter("observacion", param.getObservacion());
                            pa.setParameter("activo", param.getActivo());
                            pa.setParameter("idObjetivo", obj_.getIdObjManejo());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()){
                                ObjetivoEspecificoManejoEntity obj = null;
                                for (Object[] row :spResult) {
                                    obj = new ObjetivoEspecificoManejoEntity();
                                    obj.setIdObjEspecificoManejo(row[0]==null?null:(Integer) row[0]);
                                    obj.setCodTipo(row[1]==null?null:(String) row[1]);
                                    obj.setTipoObjetivo(row[2]==null?null:(String) row[2]);
                                    obj.setDescripcion(row[3]==null?null:(String) row[3]);
                                    obj.setDetalle(row[4]==null?null:(String) row[4]);
                                    obj.setObservacion(row[5]==null?null:(String) row[5]);
                                    obj.setActivo(row[6]==null?null: String.valueOf((Character) row[6]));
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListDetalle(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró los objetivos correctamente.");
            return result;

        }catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }
}
