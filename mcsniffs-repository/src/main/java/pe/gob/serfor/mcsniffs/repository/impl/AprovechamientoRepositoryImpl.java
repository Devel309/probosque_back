package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.AprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class AprovechamientoRepositoryImpl extends JdbcDaoSupport implements AprovechamientoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(CensoForestalDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }



    @Override
    public ResultClassEntity RegistrarAprovechamiento(AprovechamientoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_Aprovechamiento_Registrar");
        processStored.registerStoredProcedureParameter("idOrganizacion", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codOrganizacion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("organizacion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoProducto", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);


        processStored.setParameter("idOrganizacion", param.getIdOrganizacion());
        processStored.setParameter("codOrganizacion", param.getCodOrganizacion());
        processStored.setParameter("organizacion", param.getOrganizacion());
        processStored.setParameter("familia", param.getFamilia());
        processStored.setParameter("especie", param.getEspecie());
        processStored.setParameter("tipoProducto", param.getTipoProducto());
        processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
        processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se registró el Aprovechamiento con éxito.");
        return result;
    }


    @Override
    public List<AprovechamientoEntity> ListarAprovechamiento(Integer idPlanManejo) throws Exception {
        List<AprovechamientoEntity> lista = new ArrayList<AprovechamientoEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Aprovechamiento_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    AprovechamientoEntity temp = new AprovechamientoEntity();
                    temp.setIdOrganizacion((Integer) row[0]);
                    temp.setCodOrganizacion((String) row[1]);
                    temp.setOrganizacion((String) row[2]);
                    temp.setFamilia((String) row[3]);
                    temp.setEspecie((String) row[4]);
                    temp.setTipoProducto(((String) row[5]));
                    temp.setIdPlanManejo(((Integer) row[6]));

                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("AprovechamientoRepositoryImpl - ListaT Aprovechamiento", e.getMessage());
            throw e;
        }

    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

}
