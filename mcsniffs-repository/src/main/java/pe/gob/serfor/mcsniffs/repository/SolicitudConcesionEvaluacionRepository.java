package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;

public interface SolicitudConcesionEvaluacionRepository {

    ResultClassEntity listarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj);
    ResultClassEntity actualizarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj);

    ResultClassEntity listarSolicitudConcesionEvaluacionDetalle(SolicitudConcesionEvaluacionDto obj) throws Exception;
    ResultClassEntity registrarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj) ;
}
