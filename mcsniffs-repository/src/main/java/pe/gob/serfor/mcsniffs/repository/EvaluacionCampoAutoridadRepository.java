package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad.EvaluacionCampoAutoridadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface EvaluacionCampoAutoridadRepository {
    ResultClassEntity ComboPorFiltroAutoridad(EvaluacionCampoAutoridadDto request) throws Exception;
    ResultClassEntity RegistrarEvaluacionCampoAutoridad(List<EvaluacionCampoAutoridadDto> list);
}
