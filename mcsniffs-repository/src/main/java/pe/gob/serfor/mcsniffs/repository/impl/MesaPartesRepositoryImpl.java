package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.MesaPartesRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class MesaPartesRepositoryImpl extends JdbcDaoSupport implements MesaPartesRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(MesaPartesRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<MesaPartesDto> listarMesaPartes(MesaPartesDto dto) throws Exception {
      
        List<MesaPartesDto> lista = new ArrayList<MesaPartesDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartes_Listar");
            processStored.registerStoredProcedureParameter("idMesaPartes", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idMesaPartes",dto.getIdMesaPartes());
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
            processStored.setParameter("codigo",dto.getCodigo());
            processStored.setParameter("P_PAGENUM", dto.getPageNum());
            processStored.setParameter("P_PAGESIZE", dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                MesaPartesDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new MesaPartesDto();
                        temp.setIdMesaPartes(row[0]==null?null:(Integer) row[0]);
                        temp.setCodigo(row[1]==null?null:(String) row[1]);
                        temp.setConforme(row[2]==null?null:(String) row[2]);
                        temp.setComentario(row[3]==null?null:(String) row[3]);
                        temp.setDetalle(row[4]==null?null:(String) row[4]);
                        temp.setDescripcion(row[5]==null?null:(String) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setEstado(row[7]==null?null:(String) row[7]);

                        temp.setIdPlanManejo(row[8]==null?null:(Integer) row[8]);
                        temp.setFechaTramite(row[9]==null?null:(Timestamp) row[9]);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarMesaPartes", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity registrarMesaPartes(MesaPartesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartes_Registrar");
            processStored.registerStoredProcedureParameter("idMesaPartes", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("comentario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("fechaTramite", Timestamp.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMesaPartes", dto.getIdMesaPartes());
            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("codigo", dto.getCodigo());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("comentario", dto.getComentario());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.setParameter("fechaTramite", dto.getFechaTramite());
            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idMesaPartes");
            dto.setIdMesaPartes(id);

            result.setSuccess(true);
            result.setMessage("Se registró mesa partes correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_PLANMANEJO,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PLANMANEJO + dto.getIdPlanManejo(),
                    dto.getIdUsuarioRegistro());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity eliminarMesaPartes(MesaPartesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartes_Eliminar");
            processStored.registerStoredProcedureParameter("idMesaPartes", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMesaPartes", dto.getIdMesaPartes());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó mesa partes correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public List<MesaPartesDto> listarMesaPartesSendEmail(Integer idPlanManejo) throws Exception {
      
        List<MesaPartesDto> lista = new ArrayList<MesaPartesDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartes_SendEmail");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                MesaPartesDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new MesaPartesDto();
                        temp.setIdPlanManejo(row[0]==null?null:(Integer) row[0]);
                        temp.setCorreo(row[1]==null?null:(String) row[1]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarMesaPartesSendEmail", e.getMessage());
            throw e;
        }
    }
 
}
