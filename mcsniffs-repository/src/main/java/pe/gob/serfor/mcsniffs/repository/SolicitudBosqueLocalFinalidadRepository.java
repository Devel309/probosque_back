package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFinalidadDto;

public interface SolicitudBosqueLocalFinalidadRepository {
    List<SolicitudBosqueLocalFinalidadDto> listarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto dto) throws Exception;
    ResultClassEntity eliminarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto dto) throws Exception;
    ResultClassEntity registrarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto dto) throws Exception;
    
}
