package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoNivel1Entity;

public interface TipoProcesoRepository {
    ResultEntity<TipoProcesoNivel1Entity> listarTipoProceso();
}
