package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDto;


public interface SolPlantacionForestalEvaluacionRepository {

    ResultClassEntity listarSolicitudPlantacionForestalEvaluacion(SolPlantacionForestalEvaluacionDto dto);
    ResultClassEntity listarSolicitudPlantacionForestalEvaluacionDetalle(SolPlantacionForestalEvaluacionDetalleDto detDto);
    ResultClassEntity registrarSolicitudPlantacionForestalEvaluacion(SolPlantacionForestalEvaluacionDto dto);
    ResultClassEntity registrarSolicitudPlantacionForestalEvaluacionDetalle(SolPlantacionForestalEvaluacionDetalleDto dto);
    ResultClassEntity actualizarSolicitudPlantacionForestalEvaluacion(SolPlantacionForestalEvaluacionDto dto);
    ResultClassEntity actualizarSolicitudPlantacionForestalEvaluacionDetalle(SolPlantacionForestalEvaluacionDetalleDto dto);
}
