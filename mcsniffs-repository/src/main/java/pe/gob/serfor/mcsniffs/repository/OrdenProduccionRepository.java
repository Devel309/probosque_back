package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.OrdenProduccionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;
   
import pe.gob.serfor.mcsniffs.entity.ProdTerminadoEntity;

public interface OrdenProduccionRepository {
    ResultClassEntity registrarOrdenProduccionProductoTerminado(ProdTerminadoEntity item) throws Exception;
    ResultClassEntity listarOrdenProduccionProductoTerminado(ProdTerminadoEntity param);
    ResultClassEntity ListarOrdenProduccion(OrdenProduccionEntity ordenProduccion) throws Exception;
    ResultClassEntity RegistrarOrdenProduccion(List<OrdenProduccionEntity> param) throws Exception;
}
