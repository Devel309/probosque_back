package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.TipoParametroDto;

public interface TipoParametroRepository {

    ResultClassEntity listarTipoParametro(TipoParametroDto obj);
    ResultClassEntity actualizarParametro(TipoParametroDto obj);

}
