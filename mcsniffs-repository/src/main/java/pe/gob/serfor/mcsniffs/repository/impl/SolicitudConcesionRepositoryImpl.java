package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ClonarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudConcesionRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @Autowired
    private ArchivoRepository repositoryArchivo;




    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudConcesionPorFiltro(SolicitudConcesionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionDto> lista = new ArrayList<SolicitudConcesionDto>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_ListarPorFiltro");

            processStored.registerStoredProcedureParameter("idConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoPerfil", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoModalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumentoSolicitante", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pPageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pPageSize", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("evidenciaRemitida", Boolean.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idConcesion", obj.getIdConcesion());
            processStored.setParameter("codigoPerfil", obj.getCodigoPerfil());
            processStored.setParameter("idSolicitante", obj.getIdSolicitante());
            processStored.setParameter("codigoModalidad", obj.getCodigoModalidad());
            processStored.setParameter("nombreSolicitante", obj.getNombreSolicitanteUnico());
            processStored.setParameter("numeroDocumentoSolicitante", obj.getNumeroDocumentoSolicitanteUnico());
            processStored.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            processStored.setParameter("pPageNum", obj.getPageNum());
            processStored.setParameter("pPageSize", obj.getPageSize());
            processStored.setParameter("evidenciaRemitida", obj.getEvidenciaRemitida());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolicitudConcesionDto c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionDto();
                        c.setIdConcesion(row[0] == null ? null : (Integer) row[0]);
                        c.setCodigoModalidad(row[1] == null ? null : (String) row[1]);
                        c.setDescripcionModalidad(row[2] == null ? null : (String) row[2]);
                        c.setNumeroDocumentoSolicitante(row[3] == null ? null : (String) row[3]);
                        c.setNombreSolicitanteNatural(row[4] == null ? null : (String) row[4]);
                        c.setNombreSolicitanteJuridico(row[5] == null ? null : (String) row[5]);
                        c.setNombreSolicitanteUnico(row[6] == null ? null : (String) row[6]);
                        c.setFecha(row[7] == null ? null : (String) row[7]);
                        c.setEstadoSolicitud(row[8] == null ? null : (String) row[8]);
                        c.setEstadoSolicitudTexto(row[9] == null ? null : (String) row[9]);
                        result.setTotalRecord(row[10] == null ? 0 : (Integer) row[10]);
                        c.setDiaEvaluacion(row[11] == null ? null : (Integer) row[11]);
                        c.setNroOpinion(row[12] == null ? null : (Integer) row[12]);
                        c.setFechaCambioEstado((Date) row[13]);
                        c.setDiaVigencia(row[14] == null ? null : (Integer) row[14]);
                        c.setFlagReutilizado(row[15] == null ? false : (Boolean) row[15]);
                        c.setIdSolicitudConcesionPadre(row[16] == null ? null : (Integer) row[16]);
                        c.setCantidadHijos(row[17] == null ? null : (Integer) row[17]);
                        c.setIdArchivoEvidencia(row[18] == null ? null : (Integer) row[18]);
                        c.setNombreEvidencia(row[19] == null ? null : (String) row[19]);
                        c.setDescripcionEvidencia(row[20] == null ? null : (String) row[20]);
                        c.setNombreArchivo(row[21] == null ? null : (String) row[21]);
                        c.setCodigoPerfil(row[22] == null ? null : (String) row[22]);
                        c.setIdContrato(row[23] == null ? null : (Integer) row[23]);
                        c.setFechaInicioContrato(row[24] == null ? null : (Date) row[24]);
                        c.setFechaFinContrato(row[25] == null ? null : (Date) row[25]);
                        c.setRemitido(row[26] == null ? false : (Boolean) row[26]);
                        c.setRemiteInformacionAnexo6(row[27] == null ? false : (Boolean) row[27]);
                        c.setIdResolucion(row[28] == null ? null : (Integer) row[28]);
                        c.setGanador(row[29] == null ? null : (BigInteger) row[29]);
                        c.setIdSolicitante(row[30] == null ? null : (Integer) row[30]);
                        c.setVerificadoAcreditaEvidencia(row[31] == null ? false : (Boolean) row[31]);
                        c.setObservacionAcreditaEvidencia(row[32] == null ? null : (String) row[32]);
                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar  Solicitud Concesion correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarActividadSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Solicitud Concesion.");
            return result;
        }
    }

    @Override
    public ResultClassEntity listarSolicitudConcesion(SolicitudConcesionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionDto> lista = new ArrayList<SolicitudConcesionDto>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_Listar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolicitudConcesionDto c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionDto();
                        c.setIdSolicitudConcesion(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitante(row[1] == null ? null : (Integer) row[1]);
                        c.setTipoDocumentoSolicitante(row[2] == null ? null : (String) row[2]);
                        c.setNumeroDocumentoSolicitanteUnico(row[3] == null ? null : (String) row[3]);
                        c.setNombreSolicitanteUnico(row[4] == null ? null : (String) row[4]);
                        c.setNroPartidaRegistral(row[5] == null ? null : (String) row[5]);
                        c.setDireccionSolicitante(row[6] == null ? null : (String) row[6]);
                        c.setNumeroDireccion(row[7] == null ? null : (String) row[7]);
                        c.setSectorCaserio(row[8] == null ? null : (String) row[8]);
                        c.setIdDepartamentosolicitante(row[9] == null ? null : (Integer) row[9]);
                        c.setNombreDepartamentoSolicitante(row[10] == null ? null : (String) row[10]);
                        c.setIdProvinciaSolicitante(row[11] == null ? null : (Integer) row[11]);
                        c.setNombreProvinciaSolicitante(row[12] == null ? null : (String) row[12]);
                        c.setIdDistritoSolicitante(row[13] == null ? null : (Integer) row[13]);
                        c.setNombreDistritoSolicitante(row[14] == null ? null : (String) row[14]);
                        c.setTelefonoFijo(row[15] == null ? null : (String) row[15]);
                        c.setTelefonoCelular(row[16] == null ? null : (String) row[16]);
                        c.setCodigoModalidad(row[17] == null ? null : (String) row[17]);
                        c.setDescripcionModalidad(row[18] == null ? null : (String) row[18]);
                        c.setVigenciaAnio(row[19] == null ? null : (Integer) row[19]);
                        c.setObjetivo(row[20] == null ? null : (String) row[20]);
                        c.setRecurso(row[21] == null ? null : (String) row[21]);
                        c.setImportancia(row[22] == null ? null : (String) row[22]);
                        c.setDescripcionProyecto(row[23] == null ? null : (String) row[23]);
                        c.setCorreoElectronico(row[24] == null ? null : (String) row[24]);
                        c.setEstadoSolicitud(row[25] == null ? null : (String) row[25]);
                        c.setCorreoElectronicoNotif(row[26] == null ? null : (String) row[26]);
                        c.setCodigoFuenteFinanciamiento(row[27] == null ? null : (String) row[27]);
                        c.setDescripcionFuenteFinanciamiento(row[28] == null ? null : (String) row[28]);
                        c.setNombreReprLegal(row[29] == null ? null : (String) row[29]);
                        c.setDireccionReprLegal(row[30] == null ? null : (String) row[30]);
                        c.setDescripcionOtros(row[31] == null ? null : (String) row[31]);
                        c.setNroTramite(row[32] == null ? null : (String) row[32]);
                        c.setFechaTramite(row[33] == null ? null : (Date) row[33]);
                        c.setMetas(row[34] == null ? null : (String) row[34]);
                        c.setAnexoCompletado(row[35] == null ? null : (Boolean) row[35]);
                        c.setIdSolicitudConcesionPadre(row[36] == null ? null : (Integer) row[36]);
                        c.setEvidenciaRemitida(row[37] == null ? null : (Boolean) row[37]);
                        c.setRemiteInformacionAnexo6(row[38] == null ? null : (Boolean) row[38]);
                        c.setNumeroDocumentoReprLegal(row[39] == null ? null : (String) row[39]);
                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Actividad Solicitud Concesion correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarActividadSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Actividad Solicitud Concesion.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception{
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_Registrar");

            processStored.registerStoredProcedureParameter("idSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPartidaRegistral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDireccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sectorCaserio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoFijo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoCelular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoModalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaAnio", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("objetivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("recurso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("importancia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionProyecto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correoElectronico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoFuenteFinanciamiento", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correoElectronicoNotif", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionPadre", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.INOUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitante", obj.getIdSolicitante());
            processStored.setParameter("nroPartidaRegistral", obj.getNroPartidaRegistral());
            processStored.setParameter("numeroDireccion", obj.getNumeroDireccion());
            processStored.setParameter("sectorCaserio", obj.getSectorCaserio());
            processStored.setParameter("telefonoFijo", obj.getTelefonoFijo());
            processStored.setParameter("telefonoCelular", obj.getTelefonoCelular());
            processStored.setParameter("codigoModalidad", obj.getCodigoModalidad());
            processStored.setParameter("vigenciaAnio", obj.getVigenciaAnio());
            processStored.setParameter("objetivo", obj.getObjetivo());
            processStored.setParameter("recurso", obj.getRecurso());
            processStored.setParameter("importancia", obj.getImportancia());
            processStored.setParameter("descripcionProyecto", obj.getDescripcionProyecto());
            processStored.setParameter("correoElectronico", obj.getCorreoElectronico());
            processStored.setParameter("direccion", obj.getDireccionSolicitante());
            processStored.setParameter("codigoFuenteFinanciamiento", obj.getCodigoFuenteFinanciamiento());
            processStored.setParameter("correoElectronicoNotif", obj.getCorreoElectronicoNotif());
            processStored.setParameter("ubigeo", obj.getUbigeo());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            processStored.setParameter("idSolicitudConcesionPadre", obj.getIdSolicitudConcesionPadre());

            processStored.execute();

            Integer idSolicitudConcesion = (Integer) processStored.getOutputParameterValue("idSolicitudConcesion");
            obj.setIdSolicitudConcesion(idSolicitudConcesion);
            result.setCodigo(idSolicitudConcesion);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la solicitud de concesión correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_SOLICITUDCONCESION,
                    LogAuditoria.REGISTRAR,
                    LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLICITUDCONCESION + idSolicitudConcesion,
                    obj.getIdUsuarioRegistro());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesion(SolicitudConcesionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPartidaRegistral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDireccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sectorCaserio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoFijo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoCelular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoModalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaAnio", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("objetivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("recurso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("importancia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionProyecto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correoElectronico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoFuenteFinanciamiento", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correoElectronicoNotif", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionOtros", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("metas", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTramite", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaTramite", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anexoCompletado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaObservacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaRequisitoObs", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionPadre", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoEvidencia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreEvidencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionEvidencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("evidenciaRemitida", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("remiteInformacionAnexo6", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("verificadoAcreditaEvidencia", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacionAcreditaEvidencia", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("idSolicitante", obj.getIdSolicitante());
            processStored.setParameter("nroPartidaRegistral", obj.getNroPartidaRegistral());
            processStored.setParameter("numeroDireccion", obj.getNumeroDireccion());
            processStored.setParameter("sectorCaserio", obj.getSectorCaserio());
            processStored.setParameter("telefonoFijo", obj.getTelefonoFijo());
            processStored.setParameter("telefonoCelular", obj.getTelefonoCelular());
            processStored.setParameter("codigoModalidad", obj.getCodigoModalidad());
            processStored.setParameter("vigenciaAnio", obj.getVigenciaAnio());
            processStored.setParameter("objetivo", obj.getObjetivo());
            processStored.setParameter("recurso", obj.getRecurso());
            processStored.setParameter("importancia", obj.getImportancia());
            processStored.setParameter("descripcionProyecto", obj.getDescripcionProyecto());
            processStored.setParameter("correoElectronico", obj.getCorreoElectronico());
            processStored.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            processStored.setParameter("direccion", obj.getDireccionSolicitante());
            processStored.setParameter("codigoFuenteFinanciamiento", obj.getCodigoFuenteFinanciamiento());
            processStored.setParameter("correoElectronicoNotif", obj.getCorreoElectronicoNotif());
            processStored.setParameter("ubigeo", obj.getIdDistritoSolicitante());
            processStored.setParameter("descripcionOtros", obj.getDescripcionOtros());
            processStored.setParameter("metas", obj.getMetas());
            processStored.setParameter("nroTramite", obj.getNroTramite());
            processStored.setParameter("fechaTramite", obj.getFechaTramite());
            processStored.setParameter("anexoCompletado", obj.getAnexoCompletado());
            processStored.setParameter("fechaEvaluacion", obj.getFechaEvaluacion());
            processStored.setParameter("fechaObservacion", obj.getFechaObservacion());
            processStored.setParameter("fechaRequisitoObs", obj.getFechaRequisitoObs());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            processStored.setParameter("idSolicitudConcesionPadre", obj.getIdSolicitudConcesionPadre());
            processStored.setParameter("idArchivoEvidencia", obj.getIdArchivoEvidencia());
            processStored.setParameter("nombreEvidencia", obj.getNombreEvidencia());
            processStored.setParameter("descripcionEvidencia", obj.getDescripcionEvidencia());
            processStored.setParameter("evidenciaRemitida", obj.getEvidenciaRemitida());
            processStored.setParameter("remiteInformacionAnexo6", obj.getRemiteInformacionAnexo6());
            processStored.setParameter("verificadoAcreditaEvidencia", obj.getVerificadoAcreditaEvidencia());
            processStored.setParameter("observacionAcreditaEvidencia", obj.getObservacionAcreditaEvidencia());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Solicitud Concesión.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_SOLICITUDCONCESION,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUDCONCESION + obj.getIdSolicitudConcesion(),
                    obj.getIdUsuarioModificacion());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesion(SolicitudConcesionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_Eliminar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("idUsuarioElimina", obj.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó la solicitud concesión.");
            return result;
        } catch (Exception e) {
            log.error("SolicitudConcesionRepositoryImpl -eliminarSolicitudEliminar ocurrio un error: \n"
                    + e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity listarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionArchivoEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArchivo_Listar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArchivo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionArchivo", obj.getIdSolicitudConcesionArchivo());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                SolicitudConcesionArchivoEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolicitudConcesionArchivoEntity();
                        c.setIdSolicitudConcesionArchivo(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitudConcesion(row[1] == null ? null : (Integer) row[1]);
                        c.setIdArchivo(row[2] == null ? null : (Integer) row[2]);
                        c.setCodigoSeccion(row[3] == null ? null : (String) row[3]);
                        c.setDescripcionSeccion(row[4] == null ? null : (String) row[4]);
                        c.setCodigoSubSeccion(row[5] == null ? null : (String) row[5]);
                        c.setNombreArchivo(row[6] == null ? null : (String) row[6]);
                        c.setNombreGeneradoArchivo(row[7] == null ? null : (String) row[7]);
                        c.setExtensionArchivo(row[8] == null ? null : (String) row[8]);
                        c.setRutaArchivo(row[9] == null ? null : (String) row[9]);
                        c.setTipoDocumento(row[10] == null ? null : (String) row[10]);
                        c.setConforme(row[11] == null ? null : (Boolean) row[11]);
                        c.setObservacion(row[12] == null ? null : (String) row[12]);
                        c.setTipoArchivo(row[13] == null ? null : (String) row[13]);
                        /*
                         * if (c.getIdArchivo() != null) {
                         * ResultClassEntity<ArchivoEntity> file =
                         * repositoryArchivo.obtenerArchivo(c.getIdArchivo());
                         * c.setDocumento(file.getData() == null ? null : file.getData().getFile());
                         * }
                         */
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de archivos de la solicitud de concesión correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolicitudConcesionArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo obtener la lista de archivos de la solicitud de concesión.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArchivo_Registrar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionArchivo", Integer.class,
                    ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idArchivoSolicitudConcesion = (Integer) processStored
                    .getOutputParameterValue("idSolicitudConcesionArchivo");
            obj.setIdSolicitudConcesionArchivo(idArchivoSolicitudConcesion);
            result.setCodigo(idArchivoSolicitudConcesion);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el archivo de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el archivo de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArchivo_Actualizar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionArchivo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionArchivo", obj.getIdSolicitudConcesionArchivo());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("conforme", obj.getConforme());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("idUsuario", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó el archivo de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el archivo de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArchivo_Eliminar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionArchivo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionArchivo", obj.getIdSolicitudConcesionArchivo());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se eliminó el archivo de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo eliminar el archivo de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionAnexo_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionAnexo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoAnexo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFirma", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstadoAnexo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionAnexo", dto.getIdSolicitudConcesionAnexo());
            processStored.setParameter("idSolicitudConcesion", dto.getIdSolicitudConcesion());
            processStored.setParameter("codigoAnexo", dto.getCodigoAnexo());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("fechaFirma", dto.getFechaFirma());
            processStored.setParameter("codigoEstadoAnexo", dto.getCodigoEstadoAnexo());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.setParameter("lugar", dto.getLugar());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró Solicitud Concesión anexo correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<SolicitudConcesionAnexoDto> listarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto) {

        List<SolicitudConcesionAnexoDto> lista = new ArrayList<>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionAnexo_Listar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionAnexo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoAnexo", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionAnexo", dto.getIdSolicitudConcesionAnexo());
            processStored.setParameter("idSolicitudConcesion", dto.getIdSolicitudConcesion());
            processStored.setParameter("codigoAnexo", dto.getCodigoAnexo());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolicitudConcesionAnexoDto temp = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        temp = new SolicitudConcesionAnexoDto();
                        temp.setIdSolicitudConcesionAnexo(row[0] == null ? null : (Integer) row[0]);
                        temp.setIdSolicitudConcesion(row[1] == null ? null : (Integer) row[1]);
                        temp.setCodigoAnexo(row[2] == null ? null : (String) row[2]);
                        temp.setConforme(row[3] == null ? null : (Boolean) row[3]);
                        temp.setFechaFirma(row[4] == null ? null : (Date) row[4]);
                        temp.setCodigoEstadoAnexo(row[5] == null ? null : (String) row[5]);
                        temp.setDescripcionCodigoEstado(row[6] == null ? null : (String) row[6]);
                        temp.setEstado(String.valueOf((Character) row[7]));
                        temp.setLugar(row[8] == null ? null : (String) row[8]);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarSolicitudConcesionAnexo", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionAnexo_Eliminar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionAnexo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionAnexo", dto.getIdSolicitudConcesionAnexo());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó Solicitud Concesión anexo correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity clonarSolicitudConcesion(ClonarSolicitudConcesionDto dto) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_ClonarSolicitud");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionNuevo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoClonacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesion", dto.getIdSolicitudConcesion());
            processStored.setParameter("idSolicitudConcesionNuevo", dto.getIdSolicitudConcesionNuevo());
            processStored.setParameter("tipoClonacion", dto.getTipoClonacion());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se Clonó Solicitud Concesión correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity listarSolicitudConcesionCalificacionPorFiltro(SolicitudConcesionCalificacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();

        List<SolicitudConcesionCalificacionDto> lista = new ArrayList<SolicitudConcesionCalificacionDto>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionCalificacion_ListarPorFiltro");

            processStored.registerStoredProcedureParameter("idConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoModalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumentoSolicitante", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionPadre", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pPageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pPageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idConcesion", obj.getIdConcesion());
            processStored.setParameter("idSolicitante", obj.getIdSolicitante());
            processStored.setParameter("codigoModalidad", obj.getCodigoModalidad());
            processStored.setParameter("nombreSolicitante", obj.getNombreSolicitanteUnico());
            processStored.setParameter("numeroDocumentoSolicitante", obj.getNumeroDocumentoSolicitanteUnico());
            processStored.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("idSolicitudConcesionPadre", obj.getIdSolicitudConcesionPadre());
            processStored.setParameter("pPageNum", obj.getPageNum());
            processStored.setParameter("pPageSize", obj.getPageSize());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolicitudConcesionCalificacionDto c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionCalificacionDto();
                        c.setIdConcesion(row[0] == null ? null : (Integer) row[0]);
                        c.setCodigoModalidad(row[1] == null ? null : (String) row[1]);
                        c.setDescripcionModalidad(row[2] == null ? null : (String) row[2]);
                        c.setNumeroDocumentoSolicitante(row[3] == null ? null : (String) row[3]);
                        c.setNombreSolicitanteNatural(row[4] == null ? null : (String) row[4]);
                        c.setNombreSolicitanteJuridico(row[5] == null ? null : (String) row[5]);
                        c.setNombreSolicitanteUnico(row[6] == null ? null : (String) row[6]);
                        c.setFecha(row[7] == null ? null : (String) row[7]);
                        c.setEstadoSolicitud(row[8] == null ? null : (String) row[8]);
                        c.setEstadoSolicitudTexto(row[9] == null ? null : (String) row[9]);
                        c.setIdSolicitudConcesionPadre(row[10] == null ? null : (Integer) row[10]);
                        result.setTotalRecord(row[11] == null ? 0 : (Integer) row[11]);
                        c.setDiaEvaluacion(row[12] == null ? null : (Integer) row[12]);
                        c.setFlagReutilizado(row[13] == null ? false : (Boolean) row[13]);
                        c.setPuntajeObtenido(row[14] == null ? null : ((BigDecimal) row[14]).doubleValue());
                        c.setOrdenPuntaje(row[15] == null ? null : (String) row[15]);
                        c.setIdSolicitudConcesionEvaluacion(row[16] == null ? null : (Integer) row[16]);
                        c.setActividadComplementaria(row[17] == null ? null : (Integer) row[17]);
                        c.setCapacidadFinanciera(row[18] == null ? null : ((BigDecimal) row[18]).doubleValue());
                        c.setPuntajeTotal(row[19] == null ? null : ((BigDecimal) row[19]).doubleValue());
                        c.setCalificacionFinalizada(row[20] == null ? false : (Boolean) row[20]);
                        c.setIdArchivoInformeFinal(row[21] == null ? null : (Integer) row[21]);
                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar  Solicitud Concesion Calificación correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudConcesionCalificacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo realizar la acción Listar Solicitud Concesion Calificación.");
            return result;
        }
    }
}
