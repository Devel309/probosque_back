package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenProduccionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueEspecieRepository;
import pe.gob.serfor.mcsniffs.repository.OrdenProduccionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import pe.gob.serfor.mcsniffs.entity.ProdTerminadoEntity;
@Repository("repositoryOrdenProduccion")
public class OrdenProduccionRepositoryImpl extends JdbcDaoSupport implements OrdenProduccionRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(OrdenProduccionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarOrdenProduccion(List<OrdenProduccionEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenProduccionEntity> list = new ArrayList<OrdenProduccionEntity>();
        try{
            for(OrdenProduccionEntity p:param){

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Sincronizacion.pa_OrdenProduccion_Registrar");

            processStored.registerStoredProcedureParameter("idOrdenDesktop", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaConsumo", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaElimina", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idOrdenDesktop", p.getIdOrdenDesktop());
            processStored.setParameter("fechaConsumo", p.getFechaConsumo());
            processStored.setParameter("idResolucion", p.getIdResolucion());
            processStored.setParameter("estado", p.getEstado());
            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored.setParameter("fechaRegistro", p.getFechaRegistro());
            processStored.setParameter("idUsuarioModificacion", p.getIdUsuarioModificacion());
            processStored.setParameter("fechaModificacion", p.getFechaModificacion());
            processStored.setParameter("idUsuarioElimina", p.getIdUsuarioElimina());
            processStored.setParameter("fechaElimina", p.getFechaElimina());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();
            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    OrdenProduccionEntity obj = new OrdenProduccionEntity() ;
                    obj.setIdOrdenProduccion((Integer) row[0]);
                    obj.setIdOrdenDesktop((Integer) row[1]);
                    obj.setFechaConsumo((Date) row[2]);
                    obj.setIdResolucion((Integer) row[3]);
                    obj.setEstado((String) row[4]);
                    obj.setIdUsuarioRegistro((Integer) row[5]);
                    obj.setFechaRegistro((Date) row[6]);
                    obj.setIdUsuarioModificacion((Integer) row[7]);
                    obj.setFechaModificacion((Date) row[8]);
                    obj.setIdUsuarioElimina((Integer) row[9]);
                    obj.setFechaElimina((Date) row[10]);

                    list.add(obj);
                   
                  }
               }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró Orden Produccion correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }



    @Override
    public ResultClassEntity ListarOrdenProduccion(OrdenProduccionEntity ordenProduccion) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<OrdenProduccionEntity> list = new ArrayList<OrdenProduccionEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Sincronizacion.pa_OrdenProduccion_Listar");
            processStored.registerStoredProcedureParameter("idOrdenProduccion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idOrdenProduccion", ordenProduccion.getIdOrdenProduccion());
            processStored.execute();
            
            List<Object[]> spResult_ =processStored.getResultList();

            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    OrdenProduccionEntity obj = new OrdenProduccionEntity() ;
                    obj.setIdOrdenProduccion((Integer) row[0]);
                    obj.setIdOrdenDesktop((Integer) row[1]);
                    obj.setFechaConsumo((Date) row[2]);
                    obj.setIdResolucion((Integer) row[3]);
                    obj.setEstado((String) row[4]);
                    obj.setIdUsuarioRegistro((Integer) row[5]);
                    obj.setFechaRegistro((Date) row[6]);
                    obj.setIdUsuarioModificacion((Integer) row[7]);
                    obj.setFechaModificacion((Date) row[8]);
                    obj.setIdUsuarioElimina((Integer) row[9]);
                    obj.setFechaElimina((Date) row[10]);
                    list.add(obj);
                }
            }

            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró la Orden Produccion correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la Orden Produccion");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override               
    public ResultClassEntity registrarOrdenProduccionProductoTerminado(ProdTerminadoEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Aprovechamiento.pa_OrdenProduccionProductoTerminado_Registrar");
            processStored.registerStoredProcedureParameter("idProdTerminado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idOrdProduccion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("trozas", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumen", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cantidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumentransformado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoProducto", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("espesor", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ancho", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("longitud", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaProdTerminado", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProdTerminado", item.getIdProdTerminado());
            processStored.setParameter("idOrdProduccion", item.getIdOrdProduccion());
            processStored.setParameter("trozas", item.getTrozas());
            processStored.setParameter("volumen", item.getVolumen());
            processStored.setParameter("cantidad", item.getCantidad());
            processStored.setParameter("volumentransformado", item.getVolumentransformado());
            processStored.setParameter("tipoProducto", item.getTipoProducto());
            processStored.setParameter("espesor", item.getEspesor());
            processStored.setParameter("ancho", item.getAncho());
            processStored.setParameter("longitud", item.getLongitud());
            processStored.setParameter("fechaProdTerminado", item.getFechaProdTerminado());
            processStored.setParameter("idUsuario", item.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la Orden Produccion Producto Terminado - Rep.Impl.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity listarOrdenProduccionProductoTerminado(ProdTerminadoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ProdTerminadoEntity> list  = new ArrayList<ProdTerminadoEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Aprovechamiento.pa_OrdenProduccionProductoTerminado_Listar");
            processStored.registerStoredProcedureParameter("idProdTerminado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idOrdProduccion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProdTerminado", param.getIdProdTerminado());
            processStored.setParameter("idOrdProduccion", param.getIdOrdProduccion());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ProdTerminadoEntity obj = new ProdTerminadoEntity();

                    obj.setIdProdTerminado((Integer) row[0]);
                    obj.setIdOrdProduccion((Integer) row[1]);
                    obj.setTrozas((Integer) row[2]);
                    obj.setVolumen((BigDecimal) row[3]);
                    obj.setCantidad((Integer) row[4]);
                    obj.setVolumentransformado((BigDecimal) row[5]);
                    obj.setTipoProducto((Integer) row[6]);
                    obj.setEspesor((BigDecimal) row[7]);
                    obj.setAncho((BigDecimal) row[8]);
                    obj.setLongitud((BigDecimal) row[9]);
                    obj.setFechaProdTerminado((Date) row[10]);
                    list.add(obj);
                    Date ts = obj.getFechaProdTerminado();
                    System.out.println("fecha--> "+ts);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se Listo Correctamente");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    
}
