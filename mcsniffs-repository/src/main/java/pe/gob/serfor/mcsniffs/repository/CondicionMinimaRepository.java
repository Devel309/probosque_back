package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ListarCondicionMinimaDto;

public interface CondicionMinimaRepository {
    ResultClassEntity registrarCondicionMinima(CondicionMinimaDto request);
    List<ListarCondicionMinimaDto> obtenerCondicionMinima(Integer idCondicionMinima) throws Exception;
}
