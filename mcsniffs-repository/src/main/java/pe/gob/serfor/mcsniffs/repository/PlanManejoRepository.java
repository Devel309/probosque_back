package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;

public interface PlanManejoRepository {

    ResultClassEntity RegistrarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    ResultClassEntity ListarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    ResultClassEntity ActualizarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    ResultClassEntity EliminarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;

    Pageable<List<PlanManejoEntity>> filtrar(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad, String nombreTitular, String codigoEstado,
            Page page) throws Exception;

    Pageable<List<PlanManejoEntity>> filtrarEval(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad, String nombreTitular, String codigoEstado,String codigoUnico,String modalidadTH,
            Page p) throws Exception;

    // Jaqueline DB
    ResultClassEntity registrarEstadoPlanManejo(PlanManejoEstadoEntity obj);

    ResultClassEntity actualizarEstadoPlanManejo(PlanManejoEstadoEntity obj);

    ResultEntity<PlanManejoEstadoEntity> listarEstadoPlanManejo(PlanManejoEstadoEntity filtro);

    ResultClassEntity ListarPorFiltroPlanManejoArchivo(PlanManejoArchivoEntity request);

    public ResultClassEntity eliminarPlanManejoArchivo(PlanManejoArchivoDto request);

    public ResultClassEntity obtenerPlanManejoArchivo(PlanManejoArchivoDto request);

    public ResultClassEntity actualizarPlanManejoArchivo(PlanManejoArchivoDto request);

    ResultClassEntity actualizarPlanManejoEstado(PlanManejoDto request);

    public ResultClassEntity obtenerPlanManejoTitular(Integer idPlanManejo);

    ResultClassEntity listarPlanManejoContrato(Integer idPlanManejo) throws Exception;

    ResultClassEntity obtenerArchivoVerificacionCampo(Integer idPlanManejo) throws Exception;

    ResultClassEntity obtenerArchivoSolicitudOpinion(Integer idPlanManejoEvaluacion) throws Exception;

    ResultClassEntity actualizarConformePlanManejoArchivo(PlanManejoArchivoDto request);

    ResultClassEntity actualizarArchivoPlanManejoArchivo(PlanManejoArchivoDto request);

    ResultClassEntity ObtenerPlanManejo(PlanManejoDto plan) throws Exception;

    ResultClassEntity obtenerPlanManejoDetalleContrato(PlanManejoArchivoDto request);

    List<PlanManejoArchivoDto> listarPlanManejoListar(Integer idPlanManejo, Integer idArchivo, String tipoDocumento,
            String codigoProceso,String extension) throws Exception;

    List<PlanManejoArchivoDto> PlanManejoArchivoEntidad(Integer idPlanManejoArchivo) throws Exception;

    ResultClassEntity listarPlanManejoAnexoArchivo(PlanManejoArchivoEntity filtro) throws Exception;

    ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo, String tipoPlan, String idTipoBosque);

    ResultClassEntity<PlanManejoDto> DatosGeneralesPlan(Integer idPlanManejo) throws Exception;

    ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo, String tipoPlan);

    List<RegistroTabEntity> listarTabsRegistrados(RegistroTabEntity param) throws Exception;

    Pageable<List<PlanManejoEntity>> filtrarEvaluacionPlan(Integer idPlanManejo, Integer idTipoProceso,
            Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad, String nombreTitular, String codigoEstado,
            Page p) throws Exception;

    ResultEntity<PlanManejoInformacionEntity> obtenerInformacionPlan(Integer idPlanManejo, String tipoPlan)
            throws Exception;

    List<DivisionAdministrativaEntity> listarDivisionAdministrativa(DivisionAdministrativaEntity param)
            throws Exception;

    ResultEntity<PlanManejoEntity> obtenerPlanesManejoTitular(String nroDocumento, String tipoDocumento)
            throws Exception;

}