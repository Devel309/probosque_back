package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudBosqueLocalArchivoRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalArchivoRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalArchivoEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Archivo_ListarFiltro");
            processStored.registerStoredProcedureParameter("idSolBosqueLocalArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocalArchivo", obj.getIdSolBosqueLocalArchivo());
            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                SolicitudBosqueLocalArchivoEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolicitudBosqueLocalArchivoEntity();
                        c.setIdSolBosqueLocalArchivo(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolBosqueLocal(row[1] == null ? null : (Integer) row[1]);
                        c.setIdArchivo(row[2] == null ? null : (Integer) row[2]);
                        c.setCodigoSeccion(row[3] == null ? null : (String) row[3]);
                        c.setCodigoSubSeccion(row[4] == null ? null : (String) row[4]);
                        c.setConforme(row[5] == null ? null : (Boolean) row[5]);
                        c.setObservacion(row[6] == null ? null : (String) row[6]);
                        c.setTipoArchivo(row[7] == null ? null : (String) row[7]);
                        c.setNombreArchivo(row[8] == null ? null : (String) row[8]);
                        c.setTipoDocumento(row[9] == null ? null : (String) row[9]);
                        c.setCodigoUbigeoMunicipio(row[10] == null ? null : (String) row[10]);
                        c.setAsunto(row[11] == null ? null : (String) row[11]);
                        c.setNumeroDocumento(row[12] == null ? null : (String) row[12]);
                        c.setFechaDocumento(row[13] == null ? null : (Date) row[13]);
                        c.setIdDepartamentoMunicipio(row[14] == null ? null : (Integer) row[14]);
                        c.setIdProvinciaMunicipio(row[15] == null ? null : (Integer) row[15]);
                        c.setIdDistritoMunicipio(row[16] == null ? null : (Integer) row[16]);
                        c.setDepartamentoMunicipio(row[17] == null ? null : (String) row[17]);
                        c.setProvinciaMunicipio(row[18] == null ? null : (String) row[18]);
                        c.setDistritoMunicipio(row[19] == null ? null : (String) row[19]);
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de archivos de la solicitud de bosque local correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocalArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de archivos de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalArchivoPaginado(SolicitudBosqueLocalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();

        List<SolicitudBosqueLocalArchivoEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Archivo_Listar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("tipoDocumento", obj.getTipoDocumento());
            processStored.setParameter("pageNum", obj.getPageNum());
            processStored.setParameter("pageSize", obj.getPageSize());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                SolicitudBosqueLocalArchivoEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolicitudBosqueLocalArchivoEntity();
                        c.setIdSolBosqueLocalArchivo(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolBosqueLocal(row[1] == null ? null : (Integer) row[1]);
                        c.setIdArchivo(row[2] == null ? null : (Integer) row[2]);
                        c.setCodigoSeccion(row[3] == null ? null : (String) row[3]);
                        c.setCodigoSubSeccion(row[4] == null ? null : (String) row[4]);
                        c.setConforme(row[5] == null ? null : (Boolean) row[5]);
                        c.setObservacion(row[6] == null ? null : (String) row[6]);
                        c.setTipoArchivo(row[7] == null ? null : (String) row[7]);
                        c.setNombreArchivo(row[8] == null ? null : (String) row[8]);
                        c.setTipoDocumento(row[9] == null ? null : (String) row[9]);
                        c.setCodigoUbigeoMunicipio(row[10] == null ? null : (String) row[10]);
                        c.setAsunto(row[11] == null ? null : (String) row[11]);
                        c.setNumeroDocumento(row[12] == null ? null : (String) row[12]);
                        c.setFechaDocumento(row[13] == null ? null : (Date) row[13]);
                        c.setSeccion(row[14] == null ? null : (String) row[14]);
                        c.setIdDepartamentoMunicipio(row[15] == null ? null : (Integer) row[15]);
                        c.setIdProvinciaMunicipio(row[16] == null ? null : (Integer) row[16]);
                        c.setIdDistritoMunicipio(row[17] == null ? null : (Integer) row[17]);
                        c.setDepartamentoMunicipio(row[18] == null ? null : (String) row[18]);
                        c.setProvinciaMunicipio(row[19] == null ? null : (String) row[19]);
                        c.setDistritoMunicipio(row[20] == null ? null : (String) row[20]);
                        lista.add(c);
                        result.setTotalRecord((Integer) row[21]);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de archivos de la solicitud de bosque local correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocalArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de archivos de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Archivo_Registrar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocalArchivo", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeoMunicipio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaDocumento", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("conforme", obj.getConforme());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("nombreDocumento", obj.getNombreArchivo());
            processStored.setParameter("tipoDocumento", obj.getTipoDocumento());
            processStored.setParameter("ubigeoMunicipio", obj.getCodigoUbigeoMunicipio());
            processStored.setParameter("asunto", obj.getAsunto());
            processStored.setParameter("numeroDocumento", obj.getNumeroDocumento());
            processStored.setParameter("fechaDocumento", obj.getFechaDocumento());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idArchivoSolicitudBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocalArchivo");
            obj.setIdSolBosqueLocalArchivo(idArchivoSolicitudBosqueLocal);
            result.setCodigo(idArchivoSolicitudBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el archivo de la solicitud de bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el archivo de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Archivo_Actualizar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocalArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeoMunicipio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaDocumento", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocalArchivo", obj.getIdSolBosqueLocalArchivo());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("conforme", obj.getConforme());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("tipoDocumento", obj.getTipoDocumento());
            processStored.setParameter("ubigeoMunicipio", obj.getCodigoUbigeoMunicipio());
            processStored.setParameter("asunto", obj.getAsunto());
            processStored.setParameter("numeroDocumento", obj.getNumeroDocumento());
            processStored.setParameter("fechaDocumento", obj.getFechaDocumento());
            processStored.setParameter("idUsuario", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó el archivo de la solicitud de bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el archivo de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Archivo_Eliminar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocalArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocalArchivo", obj.getIdSolBosqueLocalArchivo());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el archivo de la solicitud de bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo eliminar el archivo de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }
}
