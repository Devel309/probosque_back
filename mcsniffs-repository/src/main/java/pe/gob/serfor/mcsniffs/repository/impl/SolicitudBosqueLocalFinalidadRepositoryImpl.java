package pe.gob.serfor.mcsniffs.repository.impl;

 

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFinalidadDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalFinalidadRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudBosqueLocalFinalidadRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalFinalidadRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(SolicitudBosqueLocalFinalidadRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public List<SolicitudBosqueLocalFinalidadDto> listarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto dto) throws Exception {
         
        List<SolicitudBosqueLocalFinalidadDto> lista = new ArrayList<SolicitudBosqueLocalFinalidadDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Finalidad_Listar");            
            processStored.registerStoredProcedureParameter("idBosqueLocalFinalidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idBosqueLocalFinalidad",dto.getIdBosqueLocalFinalidad());
            processStored.setParameter("idSolBosqueLocal",dto.getIdSolBosqueLocal());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalFinalidadDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new SolicitudBosqueLocalFinalidadDto();

                        temp.setIdBosqueLocalFinalidad(row[0]==null?null:(Integer) row[0]);
                        temp.setIdSolBosqueLocal(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoFinalidad(row[2]==null?null:(String) row[2]);
                        temp.setSeleccionado(row[3]==null?null:(Boolean) row[3]);
                        temp.setDescripcion(row[4]==null?null:(String) row[4]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocalFinalidad", e.getMessage());
            throw e;
        }
    }


    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Finalidad_Eliminar");
            processStored.registerStoredProcedureParameter("idBosqueLocalFinalidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idBosqueLocalFinalidad", dto.getIdBosqueLocalFinalidad());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }



    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalFinalidad(SolicitudBosqueLocalFinalidadDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Finalidad_Registrar");
            processStored.registerStoredProcedureParameter("idBosqueLocalFinalidad", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoFinalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("seleccionado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idBosqueLocalFinalidad", dto.getIdBosqueLocalFinalidad());
            processStored.setParameter("idSolBosqueLocal", dto.getIdSolBosqueLocal());
            processStored.setParameter("codigoFinalidad", dto.getCodigoFinalidad());
            processStored.setParameter("seleccionado", dto.getSeleccionado());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se Ejecuto el Procedimiento correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    
}
