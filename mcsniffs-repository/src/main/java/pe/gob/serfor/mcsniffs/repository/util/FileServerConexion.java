package pe.gob.serfor.mcsniffs.repository.util;

import jcifs.smb.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * @autor: Victor Matos [14-07-2021]
 * @modificado:
 * @descripción: {Repositorio generico cargar y descargar archivo del file
 *               server}
 *
 */

@Repository
public class FileServerConexion {

    @Value("${smb.file.server.username}")
    private String fileServerUserName;

    @Value("${smb.file.server.password}")
    private String fileServerPassword;

    @Value("${smb.file.server.host}")
    private String fileServerHost;

    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    private static final String FORMATO_FECHA_HORA = "_dd-MM-yyyy_HHmmss";
    private static final String STRING_VACIO = "";

    private static final Logger log = LogManager.getLogger(FileServerConexion.class);

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Transfiere el archivo al file server}
     * @param: Un archivo tipo MultipartFile que contiene el archivo cargado.
     */
    public String uploadFile(MultipartFile multipartFile) {
        log.info("FileServer - uploadFile : \n" + multipartFile.getOriginalFilename());
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            String nombreNuevo = getFileName(multipartFile.getOriginalFilename());
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(fileServerHost, fileServerUserName,
                    fileServerPassword);
            SmbFile smbfile = new SmbFile(fileServerPath, nombreNuevo, auth);
            inputStream = new BufferedInputStream(multipartFile.getInputStream());
            outputStream = new BufferedOutputStream(new SmbFileOutputStream(smbfile));
            FileCopyUtils.copy(inputStream, outputStream);
            log.info("FileServer - insertarArchivo \n" + "Proceso realizado correctamente");
            return nombreNuevo;
        } catch (SmbException e) {
            log.error("FileServer - insertarArchivo \n" + "Ocurrió un error de conexion en: " + e.getMessage());
        } catch (MalformedURLException e) {
            log.error("FileServer - insertarArchivo \n" + "Ocurrió un error de ruta del archivo en: " + e.getMessage());
        } catch (UnknownHostException e) {
            log.error("FileServer - insertarArchivo \n" + "Ocurrió un error al conectarse al servidor en: "
                    + e.getMessage());
        } catch (IOException e) {
            log.error(
                    "FileServer - insertarArchivo \n" + "Ocurrió un error al transferir archivo en: " + e.getMessage());
        } finally {
            closeStreanm(inputStream, outputStream);
        }
        return STRING_VACIO;
    }

    /**
     * @autor: Abner Valdez [25-04-2022]
     * @modificado:
     * @descripción: {Transfiere el archivo al file server para un file Sqlite}
     * @param: Un archivo tipo MultipartFile que contiene el archivo cargado .
     */
    public String uploadFileDBSqlite(MultipartFile multipartFile) {
        log.info("FileServer - uploadFile : \n" + multipartFile.getOriginalFilename());
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            String nombreNuevo = "ArchivoSqlite";
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(fileServerHost, fileServerUserName,
                    fileServerPassword);
            SmbFile smbfile = new SmbFile(fileServerPath, nombreNuevo, auth);
            inputStream = new BufferedInputStream(multipartFile.getInputStream());
            outputStream = new BufferedOutputStream(new SmbFileOutputStream(smbfile));
            FileCopyUtils.copy(inputStream, outputStream);
            log.info("FileServer - insertarArchivo \n" + "Proceso realizado correctamente");
            return nombreNuevo;
        } catch (SmbException e) {
            log.error("FileServer - insertarArchivo \n" + "Ocurrió un error de conexion en: " + e.getMessage());
        } catch (MalformedURLException e) {
            log.error("FileServer - insertarArchivo \n" + "Ocurrió un error de ruta del archivo en: " + e.getMessage());
        } catch (UnknownHostException e) {
            log.error("FileServer - insertarArchivo \n" + "Ocurrió un error al conectarse al servidor en: "
                    + e.getMessage());
        } catch (IOException e) {
            log.error(
                    "FileServer - insertarArchivo \n" + "Ocurrió un error al transferir archivo en: " + e.getMessage());
        } finally {
            closeStreanm(inputStream, outputStream);
        }
        return STRING_VACIO;
    }

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Descarga el archivo del file server a la aplicacion}
     * @param: Nombre de archivo con su extension.
     * @return
     */
    public byte[] loadFileAsResource(String fileName) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bytes = null;
        try {
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(fileServerHost, fileServerUserName,
                    fileServerPassword);
            SmbFile smbfile = new SmbFile(fileServerPath, fileName, auth);
            inputStream = new BufferedInputStream(new SmbFileInputStream(smbfile));
            outputStream = new BufferedOutputStream(byteArrayOutputStream);
            bytes = IOUtils.toByteArray(inputStream);
            return bytes;
        } catch (SmbException e) {
            log.error("FileServer - loadFileAsResource \n" + "Ocurrió un error de conexion en: ", e.getMessage());
        } catch (MalformedURLException e) {
            log.error("FileServer - loadFileAsResource \n" + "Ocurrió un error de ruta del archivo en: ",
                    e.getMessage());
        } catch (UnknownHostException e) {
            log.error("FileServer - loadFileAsResource \n" + "Ocurrió un error al conectarse al servidor en: ",
                    e.getMessage());
        } catch (IOException e) {
            log.error("FileServer - loadFileAsResource \n" + "Ocurrió un error al transferir archivo en: ",
                    e.getMessage());
        } finally {
            closeStreanm(inputStream, outputStream);
        }
        return bytes;
    }

    /**
     * @autor: Abner Valdez [25-04-2022]
     * @modificado:
     * @descripción: {Descarga el archivo del file server para leer}
     * @param: Nombre de archivo con su extension.
     * @return
     */
    public String loadFileAsResourceDbSqlite(String fileName) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        String path = null;
        try {
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(fileServerHost, fileServerUserName,
                    fileServerPassword);
            SmbFile smbfile = new SmbFile(fileServerPath, fileName, auth);
            path = smbfile.getPath();
            return path;
        } catch (Exception e) {
            log.error("FileServer - loadFileAsResource \n" + "Ocurrió un error al transferir archivo en: ",
                    e.getMessage());
        } finally {
            closeStreanm(inputStream, outputStream);
        }
        return path;
    }

    private String getFileName(String fullFileName) {
        SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_HORA);
        Date date = new Date();
        if (Objects.nonNull(fullFileName)) {
            String newFileName = FilenameUtils.getBaseName(fullFileName)
                    .concat(formatter.format(date))
                    .concat(SEPARADOR_ARCHIVO)
                    .concat(FilenameUtils.getExtension(fullFileName));
            return newFileName;
        }
        return STRING_VACIO;
    }

    private void closeStreanm(InputStream in, OutputStream out) {
        try {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("FileServer - closeStreanm \n" + "Ocurrió un error: ", e.getMessage());
        }
    }
}
