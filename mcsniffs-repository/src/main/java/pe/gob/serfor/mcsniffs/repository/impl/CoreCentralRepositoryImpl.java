package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import net.bytebuddy.TypeCache.Sort;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.*;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.DerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.repository.CoreCentralRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
// 

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;



@Repository
public class CoreCentralRepositoryImpl extends JdbcDaoSupport implements CoreCentralRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(CoreCentralRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: JaquelineDB [24-07-2021]
     * @modificado:
     * @descripción: {Lista la autoridad forestal}
     * @param:DistritoEntity
     */
    @Override
    public List<AutoridadForestalEntity> ListarPorFiltroAutoridadForestal(AutoridadForestalEntity param) {

        List<AutoridadForestalEntity> lstresult = new ArrayList<>();

        try{
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_AutoridadForestal_ListarPorFiltros");
            sp.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("IdAutoridadForestal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("nombre", param.getNombre());
            sp.setParameter("estado", param.getEstado());
            sp.setParameter("IdAutoridadForestal", param.getIdAutoridadForestal());

            sp.execute();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    AutoridadForestalEntity c = new AutoridadForestalEntity();
                    c.setIdAutoridadForestal((Integer) row[0]);
                    c.setNombre((String) row[1]);
                    c.setEstado((String) row[2]);
                    c.setIdUsuarioRegistro((Integer) row[3]);
                    c.setFechaRegistro((Date) row[4]);

                    lstresult.add(c);
                }
            }
            return lstresult;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }



        /*
        Connection con = null;
        PreparedStatement pstm  = null;
        List<AutoridadForestalEntity> lstresult = new ArrayList<>();
        try {
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_AutoridadForestal_ListarPorFiltros (?,?,?)}");
            pstm.setObject (1, param.getNombre(), Types.VARCHAR);
            pstm.setObject(2, param.getEstado(), Types.VARCHAR);
            pstm.setObject(3, param.getIdAutoridadForestal(),Types.INTEGER);
            ResultSet rs = pstm.executeQuery();
            AutoridadForestalEntity c = null;
            while(rs.next()){
                c = new AutoridadForestalEntity();
                c.setIdAutoridadForestal(rs.getInt("NU_ID_AUTORIDAD_FORESTAL"));
                c.setNombre(rs.getString("TX_NOMBRE"));
                lstresult.add(c);
            }
            return lstresult;
        } catch (Exception e) {
            return lstresult;
        }finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("CoreCentral - ListarPorFiltroAutoridadForestal", e2.getMessage());
                
                return lstresult;
            }
		}
        */
    }

    @Override
    public List<DepartamentoEntity> ListarPorFiltroDepartamentos(DepartamentoEntity param) {
        List<DepartamentoEntity> lstdeparta = new ArrayList<>();
        try{
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Departamento_ListarPorFiltros");
            sp.registerStoredProcedureParameter("idDepartamento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreDepartamento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDepartamento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDepartamentoInei", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDepartamentoReniec", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDepartamentoSunat", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idDepartamento", param.getIdDepartamento());
            sp.setParameter("nombreDepartamento", param.getNombreDepartamento());
            sp.setParameter("codDepartamento", param.getCodDepartamento());
            sp.setParameter("codDepartamentoInei", param.getCodDepartamentoInei());
            sp.setParameter("codDepartamentoReniec", param.getCodDepartamentoReniec());
            sp.setParameter("codDepartamentoSunat", param.getCodDepartamentoSunat());
            sp.execute();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    DepartamentoEntity temp = new DepartamentoEntity();
                    temp.setIdDepartamento((Integer) row[0]);
                    temp.setNombreDepartamento((String) row[1]);
                    temp.setCodDepartamento((String) row[2]);
                    temp.setCodDepartamentoInei(row[3].toString());
                    temp.setCodDepartamentoReniec(row[4].toString());
                    temp.setCodDepartamentoSunat(row[5].toString());
                    temp.setEstado(row[6].toString());
                    temp.setIdUsuarioRegistro((Integer) row[7]);
                    temp.setFechaRegistro((Date) row[8]);
                    lstdeparta.add(temp);
                }
            }
            return lstdeparta;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public List<TipoComunidadEntity> ListarPorFiltroTipoComunidad(TipoComunidadEntity param) {

        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoComunidad_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("Descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Estado", String.class, ParameterMode.IN);
            processStored.setParameter("Descripcion", param.getDescripcion());
            processStored.setParameter("Estado", param.getEstado());
            processStored.execute();
            List<TipoComunidadEntity> result = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoComunidadEntity temp = new TipoComunidadEntity();
                    temp.setIdTipoComunidad((short) row[0]);
                    temp.setDescripcion((String) row[1]);
                    temp.setEstado( row[2].toString());
                    result.add(temp);
                }
            }
            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public List<TipoTrasporteEntity> ListarPorFiltroTipoTrasporte(TipoTrasporteEntity param) {

        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_TipoTrasporte_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("descripcion", param.getDescripcion());
            processStored.execute();
            List<TipoTrasporteEntity> result = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoTrasporteEntity temp=new TipoTrasporteEntity();
                    temp.setIdTipoTrasporte((short) row[0]);
                    temp.setDescripcion((String) row[1]);
                    result.add(temp);
                }
            }
            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public List<TipoZonaEntity> listarPorFiltroTipoZona(TipoZonaEntity param) throws Exception {
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_TipoZona_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("descripcion", param.getDescripcion());
            processStored.execute();
            List<TipoZonaEntity> result = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoZonaEntity temp=new TipoZonaEntity();
                    temp.setIdTipoZona((short) row[0]);
                    temp.setDescripcion((String) row[1]);
                    result.add(temp);
                }
            }
            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    //IVAN

    @Override
    public List<TipoBosqueEntity> listarPorFiltroTipoBosque(TipoBosqueEntity tipoBosqueEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoBosque_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTipoBosque", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("region", String.class, ParameterMode.IN);
            processStored.setParameter("idTipoBosque", tipoBosqueEntity.getIdTipoBosque()==null?0:tipoBosqueEntity.getIdTipoBosque());
            processStored.setParameter("descripcion",tipoBosqueEntity.getDescripcion()==null?"":tipoBosqueEntity.getDescripcion());
            processStored.setParameter("region", tipoBosqueEntity.getRegion()==null?"":tipoBosqueEntity.getRegion());
            processStored.execute();
            List<TipoBosqueEntity> result = new ArrayList<TipoBosqueEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoBosqueEntity tipoBosque=new TipoBosqueEntity();
                    tipoBosque.setIdTipoBosque((short) row[0]);
                    tipoBosque.setDescripcion((String) row[1]);
                    tipoBosque.setRegion((String) row[2]);
                    result.add(tipoBosque);
                }
            } else {
                return null;
            }
            return  result;


        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoConcesionEntity> listarPorFiltroTipoConcesion(TipoConcesionEntity tipoConcesionEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoConcesion_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("tipoConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("tipoConcesion", tipoConcesionEntity.getTipoConcesion());
            processStored.setParameter("descripcion", tipoConcesionEntity.getDescripcion());
            processStored.execute();

            List<TipoConcesionEntity> result = new ArrayList<TipoConcesionEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoConcesionEntity tipoConcesion=new TipoConcesionEntity();
                    tipoConcesion.setTipoConcesion((Integer) row[0]);
                    tipoConcesion.setDescripcion((String) row[1]);
                    result.add(tipoConcesion);
                }
            } else {
                return null;
            }
            return  result;

        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoContratoEntity> listarPorFiltroTipoContrato(TipoContratoEntity tipoContratoEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoContrato_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("tipoContrato", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("tipoContrato", tipoContratoEntity.getTipoContrato());
            processStored.setParameter("descripcion", tipoContratoEntity.getDescripcion());
            processStored.execute();

            List<TipoContratoEntity> result = new ArrayList<TipoContratoEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoContratoEntity tipoContrato=new TipoContratoEntity();
                    tipoContrato.setTipoContrato((Short) row[0]);
                    tipoContrato.setDescripcion((String) row[1]);
                    result.add(tipoContrato);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoDocGestionErgtfEntity> listarPorFiltroTipoDocGestionErgtf(TipoDocGestionErgtfEntity tipoDocGestionErgtfEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoDocGestionErgtf_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idDocumentoGestion", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionCorta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocGestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.setParameter("idDocumentoGestion", tipoDocGestionErgtfEntity.getIdDocumentoGestion());
            processStored.setParameter("descripcion", tipoDocGestionErgtfEntity.getDescripcion());
            processStored.setParameter("descripcionCorta", tipoDocGestionErgtfEntity.getDescripcionCorta());
            processStored.setParameter("tipoDocGestion", tipoDocGestionErgtfEntity.getTipoDocGestion());
            processStored.setParameter("estado", tipoDocGestionErgtfEntity.getEstado());
            processStored.execute();

            List<TipoDocGestionErgtfEntity> result = new ArrayList<TipoDocGestionErgtfEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoDocGestionErgtfEntity tipoDocGestionErgtf=new TipoDocGestionErgtfEntity();
                    tipoDocGestionErgtf.setIdDocumentoGestion((Short) row[0]);
                    tipoDocGestionErgtf.setDescripcionCorta((String) row[1]);
                    tipoDocGestionErgtf.setDescripcion((String) row[2]);
                    tipoDocGestionErgtf.setTipoDocGestion((String) row[3]);
                    tipoDocGestionErgtf.setEstado((String) row[4]);
                    result.add(tipoDocGestionErgtf);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoDocGestionEntity> listarPorFiltroTipoDocGestion(TipoDocGestionEntity tipoDocGestionEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoDocGestion_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTipoDocGestion", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sigla", String.class, ParameterMode.IN);
            processStored.setParameter("idTipoDocGestion", tipoDocGestionEntity.getIdTipoDocGestion());
            processStored.setParameter("descripcion", tipoDocGestionEntity.getDescripcion());
            processStored.setParameter("sigla", tipoDocGestionEntity.getSigla());

            processStored.execute();

            List<TipoDocGestionEntity> result = new ArrayList<TipoDocGestionEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoDocGestionEntity tipoDocGestion=new TipoDocGestionEntity();
                    tipoDocGestion.setIdTipoDocGestion((Short) row[0]);
                    tipoDocGestion.setDescripcion((String) row[1]);
                    tipoDocGestion.setSigla((String) row[2]);
                    result.add(tipoDocGestion);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoDocumentoEntity> listarPorFiltroTipoDocumento(TipoDocumentoEntity tipoDocumentoEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoDocumento_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idDocumentoTipo", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionCorta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("idDocumentoTipo", tipoDocumentoEntity.getIdDocumentoGestion());
            processStored.setParameter("descripcionCorta", tipoDocumentoEntity.getDescripcionCorta());
            processStored.setParameter("descripcion", tipoDocumentoEntity.getDescripcion());
            processStored.execute();

            List<TipoDocumentoEntity> result = new ArrayList<TipoDocumentoEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoDocumentoEntity tipoDocumento=new TipoDocumentoEntity();
                    tipoDocumento.setIdDocumentoGestion((Short) row[0]);
                    tipoDocumento.setDescripcionCorta((String) row[1]);
                    tipoDocumento.setDescripcion((String) row[3]);
                    result.add(tipoDocumento);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoEscalaEntity> listarPorFiltroTipoEscala(TipoEscalaEntity tipoEscalaEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoEscala_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("tipoEscala", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("tipoEscala", tipoEscalaEntity.getTipoEscala());
            processStored.setParameter("descripcion", tipoEscalaEntity.getDescripcion());
            processStored.execute();

            List<TipoEscalaEntity> result = new ArrayList<TipoEscalaEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoEscalaEntity tipoEscala=new TipoEscalaEntity();
                    tipoEscala.setTipoEscala((short) row[0]);
                    tipoEscala.setDescripcion((String) row[1]);
                    result.add(tipoEscala);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoMonedaEntity> listarPorFiltroTipoMoneda(TipoMonedaEntity tipoMonedaEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoMoneda_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTipoMoneda", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.setParameter("idTipoMoneda", tipoMonedaEntity.getIdTipoMoneda());
            processStored.setParameter("descripcion", tipoMonedaEntity.getDescripcion());
            processStored.setParameter("codigo", tipoMonedaEntity.getCodigo());
            processStored.execute();

            List<TipoMonedaEntity> result = new ArrayList<TipoMonedaEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoMonedaEntity tipoMoneda=new TipoMonedaEntity();
                    tipoMoneda.setIdTipoMoneda((Short) row[0]);
                    tipoMoneda.setDescripcion((String) row[1]);
                    tipoMoneda.setCodigo((String) row[2]);
                    result.add(tipoMoneda);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoOficinaEntity> listarPorFiltroTipoOficina(TipoOficinaEntity tipoOficinaEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoOficina_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTipoOficina", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("idTipoOficina", tipoOficinaEntity.getIdTipoOficina());
            processStored.setParameter("descripcion", tipoOficinaEntity.getDescripcion());
            processStored.execute();

            List<TipoOficinaEntity> result = new ArrayList<TipoOficinaEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoOficinaEntity tipoOficina=new TipoOficinaEntity();
                    tipoOficina.setIdTipoOficina((Integer) row[0]);
                    tipoOficina.setDescripcion((String) row[1]);
                    result.add(tipoOficina);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoPagoEntity> listarPorFiltroTipoPago(TipoPagoEntity tipoPagoEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoPago_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("IdTipoPago", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.setParameter("IdTipoPago", tipoPagoEntity.getIdTipoPago());
            processStored.setParameter("descripcion", tipoPagoEntity.getDescripcion());
            processStored.setParameter("estado", tipoPagoEntity.getEstado());
            processStored.execute();

            List<TipoPagoEntity> result = new ArrayList<TipoPagoEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoPagoEntity tipoPago=new TipoPagoEntity();
                    tipoPago.setIdTipoPago((Short) row[0]);
                    tipoPago.setDescripcion((String) row[1]);
                    tipoPago.setEstado((String) row[1]);
                    result.add(tipoPago);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoPermisoEntity> listarPorFiltroTipoPermiso(TipoPermisoEntity tipoPermisoEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoPermiso_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("tipoPermiso", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("tipoPermiso", tipoPermisoEntity.getTipoPermiso());
            processStored.setParameter("descripcion", tipoPermisoEntity.getDescripcion());
            processStored.execute();

            List<TipoPermisoEntity> result = new ArrayList<TipoPermisoEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoPermisoEntity tipoPermiso=new TipoPermisoEntity();
                    tipoPermiso.setTipoPermiso((Short) row[0]);
                    tipoPermiso.setDescripcion((String) row[1]);
                    result.add(tipoPermiso);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoProcedimientoConcesEntity> listarPorFiltroTipoProcedimientoConces(TipoProcedimientoConcesEntity tipoProcedimientoConcesEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoProcedimientoConces_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("tipoProcedimientoConces", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("tipoProcedimientoConces", tipoProcedimientoConcesEntity.getTipoProcedimientoConces());
            processStored.setParameter("descripcion", tipoProcedimientoConcesEntity.getDescripcion());
            processStored.execute();

            List<TipoProcedimientoConcesEntity> result = new ArrayList<TipoProcedimientoConcesEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoProcedimientoConcesEntity tipoProcedimientoConces=new TipoProcedimientoConcesEntity();
                    tipoProcedimientoConces.setTipoProcedimientoConces((Short) row[0]);
                    tipoProcedimientoConces.setDescripcion((String) row[1]);
                    result.add(tipoProcedimientoConces);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoProductoEntity> listarPorFiltroTipoProducto(TipoProductoEntity tipoProductoEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoProducto_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTipoProducto", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoProducto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUnidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idRecurso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idGrupoRecurso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.setParameter("idTipoProducto", tipoProductoEntity.getIdTipoProducto());
            processStored.setParameter("tipoProducto", tipoProductoEntity.getTipoProducto());
            processStored.setParameter("idUnidad", tipoProductoEntity.getIdUnidad());
            processStored.setParameter("idRecurso", tipoProductoEntity.getIdRecurso());
            processStored.setParameter("idGrupoRecurso", tipoProductoEntity.getIdGrupoRecurso());
            processStored.setParameter("estado", tipoProductoEntity.getEstado());
            processStored.setParameter("fechaInicio", tipoProductoEntity.getFechaInicio()==null?"":tipoProductoEntity.getFechaInicio());
            processStored.setParameter("fechaFin", tipoProductoEntity.getFechaFin());
            processStored.execute();

            List<TipoProductoEntity> result = new ArrayList<TipoProductoEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoProductoEntity tipoProducto=new TipoProductoEntity();
                    tipoProducto.setIdTipoProducto((Short) row[0]);
                    tipoProducto.setTipoProducto((String) row[1]);
                    tipoProducto.setIdUnidad((Integer) row[2]);
                    tipoProducto.setIdRecurso((Integer) row[3]);
                    tipoProducto.setIdGrupoRecurso((String) row[4]);
                    tipoProducto.setEstado((String) row[5]);
                    tipoProducto.setFechaInicio((Date) row[6]);
                    tipoProducto.setFechaFin((Date) row[7]);
                    result.add(tipoProducto);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<TipoRegenteEntity> listarPorFiltroTipoRegente(TipoRegenteEntity tipoRegenteEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoRegente_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTipoRegente", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.setParameter("idTipoRegente", tipoRegenteEntity.getIdTipoRegente());
            processStored.setParameter("descripcion", tipoRegenteEntity.getIdTipoRegente());
            processStored.execute();

            List<TipoRegenteEntity> result = new ArrayList<TipoRegenteEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TipoRegenteEntity tipoRegente=new TipoRegenteEntity();
                    tipoRegente.setIdTipoRegente((Short) row[0]);
                    tipoRegente.setDescripcion((String) row[1]);
                    result.add(tipoRegente);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<UbigeoArffsEntity> listarPorFiltroUbigeoArffs(UbigeoArffsEntity uigeoArffsEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_UbigeoArffs_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("id", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codDepartamento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreDepartamento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codProvincia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreProvincia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codDistrito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreDistrito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("region", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("latitud", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("longitud", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idAutoridadForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreAutoridadForestal", String.class, ParameterMode.IN);
            processStored.setParameter("id", uigeoArffsEntity.getId());
            processStored.setParameter("ubigeo", uigeoArffsEntity.getUbigeo());
            processStored.setParameter("codDepartamento", uigeoArffsEntity.getCodDepartamento());
            processStored.setParameter("nombreDepartamento", uigeoArffsEntity.getNombreDepartamento());
            processStored.setParameter("codProvincia", uigeoArffsEntity.getCodProvincia());
            processStored.setParameter("nombreProvincia", uigeoArffsEntity.getNombreProvincia());
            processStored.setParameter("codDistrito", uigeoArffsEntity.getCodDistrito());
            processStored.setParameter("nombreDistrito", uigeoArffsEntity.getNombreDistrito());
            processStored.setParameter("region", uigeoArffsEntity.getRegion());
            processStored.setParameter("latitud", uigeoArffsEntity.getLatitud());
            processStored.setParameter("longitud", uigeoArffsEntity.getLongitud());
            processStored.setParameter("idAutoridadForestal", uigeoArffsEntity.getIdAutoridadForestal());
            processStored.setParameter("nombreAutoridadForestal", uigeoArffsEntity.getNombreAutoridadForestal());
            processStored.execute();

            List<UbigeoArffsEntity> result = new ArrayList<UbigeoArffsEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    UbigeoArffsEntity ubigeoArffs=new UbigeoArffsEntity();
                    ubigeoArffs.setId((Short) row[0]);
                    ubigeoArffs.setCodDepartamento((String) row[1]);
                    ubigeoArffs.setNombreDepartamento((String) row[2]);
                    ubigeoArffs.setCodProvincia((String) row[3]);
                    ubigeoArffs.setNombreProvincia((String) row[4]);
                    ubigeoArffs.setCodDistrito((String) row[5]);
                    ubigeoArffs.setNombreDistrito((String) row[6]);
                    ubigeoArffs.setLatitud((Double) row[7]);
                    ubigeoArffs.setLongitud((Double) row[8]);
                    ubigeoArffs.setIdAutoridadForestal((Integer) row[8]);
                    ubigeoArffs.setNombreAutoridadForestal((String) row[9]);
                    result.add(ubigeoArffs);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    @Override
    public List<UnidadMedidaEntity> listarPorFiltroUnidadMedida(UnidadMedidaEntity unidadMedidaEntity) throws Exception {
        // TODO Auto-generated method stub
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_UnidadMedida_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idUnidadMedida", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("simbolo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSunat", short.class, ParameterMode.IN);
            processStored.setParameter("idUnidadMedida", unidadMedidaEntity.getIdUnidadMedida());
            processStored.setParameter("descripcion", unidadMedidaEntity.getDescripcion());
            processStored.setParameter("simbolo", unidadMedidaEntity.getSimbolo());
            processStored.setParameter("codigoSunat", unidadMedidaEntity.getCodigoSunat());
            processStored.execute();
            List<UnidadMedidaEntity> result = new ArrayList<UnidadMedidaEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    UnidadMedidaEntity UnidadMedida=new UnidadMedidaEntity();
                    UnidadMedida.setIdUnidadMedida((Short) row[0]);
                    UnidadMedida.setDescripcion((String) row[1]);
                    UnidadMedida.setSimbolo((String) row[2]);
                    UnidadMedida.setCodigoSunat((short) row[3]);
                    result.add(UnidadMedida);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por filtro Regente Comunidad}
     * @param:RegenteComunidadEntity
     */
    @Override
    public List<RegenteComunidadEntity> listarPorFiltroRegenteComunidad(RegenteComunidadEntity regenteComunidad) throws Exception  {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RegenteComunidad_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idRegComunidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codInrena", String.class, ParameterMode.IN);
            processStored.setParameter("idRegComunidad", regenteComunidad.getIdRegenteComunidad());
            processStored.setParameter("idPersona", regenteComunidad.getPersona().getIdPersona());
            processStored.setParameter("nombres", regenteComunidad.getPersona().getNombres());
            processStored.setParameter("codInrena", regenteComunidad.getCodInrena());
            processStored.execute();

            List<RegenteComunidadEntity> result = new ArrayList<RegenteComunidadEntity>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    RegenteComunidadEntity obj=new RegenteComunidadEntity();
                    PersonaEntity objPersona= new PersonaEntity();
                    obj.setIdRegenteComunidad((Integer) row[0]);
                    objPersona.setIdPersona((Integer) row[1]);
                    objPersona.setNombres((String) row[2]);
                    obj.setCodInrena((String) row[3]);
                    obj.setPersona(objPersona);
                    result.add(obj);
                }
            } else {
                return null;
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e); log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }
    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Lista las Provincias}
     * @param:ProvinciaEntity
     */
    @Override
    public ResultEntity<ProvinciaEntity> listarPorFilroProvincia(ProvinciaEntity provincia) {
        ResultEntity<ProvinciaEntity> lst_result =  new ResultEntity<>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Provincia_ListarPorFiltro");
            sp.registerStoredProcedureParameter("idProvincia", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreProvincia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codProvincia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDepartamento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codProvinciaInei", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codProvinciaReniec", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codProvinciaSunat", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDepartamento", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProvincia", provincia.getIdProvincia());
            sp.setParameter("nombreProvincia", provincia.getNombreProvincia());
            sp.setParameter("codProvincia", provincia.getCodProvincia());
            sp.setParameter("codDepartamento", provincia.getCodDepartamento());
            sp.setParameter("codProvinciaInei", provincia.getCodProvinciaInei());
            sp.setParameter("codProvinciaReniec", provincia.getCodProvinciaReniec());
            sp.setParameter("codProvinciaSunat", provincia.getCodProvinciaSunat());
            sp.setParameter("idDepartamento", provincia.getIdDepartamento());
            sp.execute();
            List<ProvinciaEntity> lstprovi = new ArrayList<ProvinciaEntity>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ProvinciaEntity provix = new ProvinciaEntity();
                    provix.setIdProvincia((Integer) row[0]);
                    provix.setNombreProvincia((String) row[1]);
                    provix.setCodProvincia((String) row[2]);
                    provix.setCodDepartamento(row[3].toString());
                    provix.setCodProvinciaInei(row[4].toString());
                    /*provix.setCodProvinciaReniec(row[5].toString());
                    provix.setCodProvinciaSunat(row[6].toString());*/
                    provix.setIdDepartamento((Integer) row[7]);
                    provix.setIdUsuarioRegistro((Integer) row[8]);
                    provix.setEstado(row[9].toString());
                    provix.setFechaRegistro((Date) row[10]);
                    lstprovi.add(provix);
                }
            }
            if (lstprovi.size() == 0){
                lst_result.setIsSuccess(true);
                lst_result.setMessage("No se encontraron provincias.");
                lst_result.setData(lstprovi);
                return lst_result;
            }
            lst_result.setIsSuccess(true);
            lst_result.setMessage("Se listaron las provincias correctamente.");
            lst_result.setData(lstprovi);
            return  lst_result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            lst_result.setIsSuccess(false);
            lst_result.setMessage("Ocurrió un error.");
            lst_result.setMessageExeption(e.getMessage());
            return  lst_result;
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Lista los Distritos}
     * @param:DistritoEntity
     */
    @Override
    public ResultEntity<DistritoEntity> listarPorFilroDistrito(DistritoEntity distrito) {
        ResultEntity<DistritoEntity> lst_result =  new ResultEntity<>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Distrito_ListarPorFiltro");
            sp.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreDistrito", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDistrito", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codProvincia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDistritoInei", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDistritoReniec", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codDistritoSunat", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProvincia", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDepartamento", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idDistrito", distrito.getIdDistrito());
            sp.setParameter("nombreDistrito", distrito.getNombreDistrito());
            sp.setParameter("codDistrito", distrito.getCodDistrito());
            sp.setParameter("codProvincia", distrito.getCodProvincia());
            sp.setParameter("codDistritoInei", distrito.getCodDistritoInei());
            sp.setParameter("codDistritoReniec", distrito.getCodDistritoReniec());
            sp.setParameter("codDistritoSunat", distrito.getCodDistritoSunat());
            sp.setParameter("idProvincia", distrito.getIdProvincia());
            sp.setParameter("idDepartamento", distrito.getIdDepartamento());
            sp.execute();
            List<DistritoEntity> result = new ArrayList<DistritoEntity>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    DistritoEntity distri=new DistritoEntity();
                    distri.setIdDistrito((Integer) row[0]);
                    distri.setNombreDistrito((String) row[1]);
                    distri.setCodDistrito((String) row[2]);
                    distri.setCodProvincia(row[3].toString());
                    distri.setCodDistritoInei(row[4].toString());
                    distri.setCodDistritoReniec(row[5].toString());
                    distri.setCodDistritoSunat(row[6].toString());
                    distri.setIdProvincia((Integer) row[7]);
                    distri.setIdDepartamento((Integer) row[8]);
                    distri.setIdUsuarioRegistro((Integer) row[9]);
                    distri.setEstado(String.valueOf((Character) row[10]));
                    distri.setFechaRegistro((Date) row[11]);
                    result.add(distri);
                }
            }
            if (result.size() == 0){
                lst_result.setIsSuccess(true);
                lst_result.setMessage("No se encontraron provincias.");
                lst_result.setData(result);
                return lst_result;
            }
            lst_result.setIsSuccess(true);
            lst_result.setMessage("Se listaron los distritos correctamente.");
            lst_result.setData(result);
            return  lst_result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            lst_result.setIsSuccess(false);
            lst_result.setInnerException(e.getMessage());
            lst_result.setMessage("Ocurrió un error.");
            lst_result.setData(null);
            return  lst_result;
        }
    }

    @Override
    public ResultEntity<EspecieForestalEntity> ListaPorFiltroEspecieForestal(EspecieForestalEntity request) {
        return null;
        /*
        ResultEntity resul=new ResultEntity();
        PreparedStatement pstm  = null;
        Connection con = null;
        try
        {

            List<EspecieForestalEntity> objList=new ArrayList<EspecieForestalEntity>();

            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_EspecieForestal_ListarPorFiltro (?,?,?,?,?)}");
            pstm.setObject (1, request.getIdEspecie(), Types.SMALLINT);
            .03P'9+
            pstm.setObject(2, request.getNombreComun(),Types.VARCHAR);
            pstm.setObject(3, request.getNombreCientifico(),Types.VARCHAR);
            pstm.setObject (4, request.getAutor(), Types.VARCHAR);
            pstm.setObject(5, request.getFamilia(),Types.VARCHAR);
            ResultSet rs = pstm.executeQuery();

            while(rs.next()){
                EspecieForestalEntity per=new EspecieForestalEntity();
                per.setIdEspecie(rs.getShort(1));
                per.setNombreComun(rs.getString(2));
                per.setNombreCientifico(rs.getString(3));
                per.setAutor(rs.getString(4));
                per.setFamilia(rs.getString(5));
                objList.add(per);
            }

            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
        */
    }

    @Override
    public ResultEntity<EspecieFaunaEntity> ListaPorFiltroEspecieFauna(EspecieFaunaEntity request) {
        return null;
        /*
        ResultEntity resul=new ResultEntity();
        PreparedStatement pstm  = null;
        Connection con = null;
        try
        {

            List<EspecieFaunaEntity> objList=new ArrayList<EspecieFaunaEntity>();

            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_EspecieFauna_ListarPorFiltro (?,?,?,?,?)}");
            pstm.setObject (1, request.getIdEspecie(), Types.SMALLINT);
            pstm.setObject(2, request.getNombreComun(),Types.VARCHAR);
            pstm.setObject(3, request.getNombreCientifico(),Types.VARCHAR);
            pstm.setObject (4, request.getAutor(), Types.VARCHAR);
            pstm.setObject(5, request.getFamilia(),Types.VARCHAR);
            ResultSet rs = pstm.executeQuery();

            while(rs.next()){
                EspecieFaunaEntity per=new EspecieFaunaEntity();
                per.setIdEspecie(rs.getShort(1));
                per.setNombreComun(rs.getString(2));
                per.setNombreCientifico(rs.getString(3));
                per.setAutor(rs.getString(4));
                per.setFamilia(rs.getString(5));
                per.setCategoria(rs.getString(6));
                objList.add(per);
            }
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
        */
    }
    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }


    @Override
    public ResultEntity ListaEspecieForestalPorFiltro(EspecieForestalEntity param) {
        ResultEntity resul=new ResultEntity();
        try
        {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EspecieForestal_listar");
            processStored.registerStoredProcedureParameter("P_NU_IDESPECIE", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESC_COMUN", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESC_CIENTIFICO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("p_AUTOR", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("p_FAMILIA", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            if(param.getIdEspecie()==0){processStored.setParameter("P_NU_IDESPECIE", null); }
            else{processStored.setParameter("P_NU_IDESPECIE", param.getIdEspecie());}
            processStored.setParameter("P_DESC_COMUN", param.getNombreComun());
            processStored.setParameter("P_DESC_CIENTIFICO", param.getNombreCientifico());
            processStored.setParameter("p_AUTOR", param.getAutor());
            processStored.setParameter("p_FAMILIA", param.getFamilia());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());
            processStored.execute();
            List<EspecieForestalEntity> objList=new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                EspecieForestalEntity objOrdPro = null;
                List<EspecieForestalEntity> objListDet=new ArrayList<>();
                for (Object[] row: spResult){
                    objOrdPro = new EspecieForestalEntity();
                    objOrdPro.setIdEspecie((Short) row[0]);
                    objOrdPro.setNombreComun((String) row[1]);
                    objOrdPro.setNombreCientifico((String) row[2]);
                    objOrdPro.setAutor(row[3]==null?null:(String) row[3]);
                    objOrdPro.setFamilia(row[4]==null?null:(String) row[4]);
                    //objOrdPro.setCantidad(row[5]==null?null:(Integer) row[5]);
                    resul.setTotalrecord((Integer) row[5]);
                    objList.add(objOrdPro);
                }
            }

            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }


    @Override
    public ResultEntity ListaEspecieFaunaPorFiltro(EspecieFaunaEntity param) {
        ResultEntity resul=new ResultEntity();
        try
        {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EspecieFauna_Listar");
            processStored.registerStoredProcedureParameter("P_NU_IDESPECIE", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESC_COMUN", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESC_CIENTIFICO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("p_AUTOR", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("p_FAMILIA", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            if(param.getIdEspecie()==0){processStored.setParameter("P_NU_IDESPECIE", null); }
            else{processStored.setParameter("P_NU_IDESPECIE", param.getIdEspecie());}
            processStored.setParameter("P_DESC_COMUN", param.getNombreComun());
            processStored.setParameter("P_DESC_CIENTIFICO", param.getNombreCientifico());
            processStored.setParameter("p_AUTOR", param.getAutor());
            processStored.setParameter("p_FAMILIA", param.getFamilia());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());

            processStored.execute();
            List<EspecieFaunaEntity> objList=new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                EspecieFaunaEntity objOrdPro = null;
                List<EspecieFaunaEntity> objListDet=new ArrayList<>();
                for (Object[] row: spResult){
                    objOrdPro = new EspecieFaunaEntity();
                    objOrdPro.setIdEspecie((Short) row[0]);
                    objOrdPro.setNombreComun((String) row[1]);
                    objOrdPro.setNombreCientifico((String) row[2]);
                    objOrdPro.setAutor(row[3]==null?null:(String) row[3]);
                    objOrdPro.setFamilia(row[4]==null?null:(String) row[4]);
                    objOrdPro.setAmenaza(row[5]==null?null:(String) row[5]);
                    objOrdPro.setCities(row[6]==null?null:(String) row[6]);
                    resul.setTotalrecord((Integer) row[7]);
                    objList.add(objOrdPro);
                }

            }

            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess(true);
           // resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }
/*
    @Override
    public List<EspecieForestalEntity> ListaEspecieForestalPorFiltro(EspecieForestalEntity param) throws Exception {
        Long pageNum=new Long(param.getPageNum());
        Long pageSize=new Long(param.getPageSize());
        Page p = new Page(pageNum, pageSize, null, null);

        Pageable<List<EspecieForestalEntity>> pageable=new Pageable<>(p);
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EspecieForestal_listar");
            processStored.registerStoredProcedureParameter("P_NU_IDESPECIE", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESC_COMUN", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESC_CIENTIFICO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("p_AUTOR", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("p_FAMILIA", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            if(param.getIdEspecie()==0){processStored.setParameter("P_NU_IDESPECIE", null); }
            else{processStored.setParameter("P_NU_IDESPECIE", param.getIdEspecie());}
            processStored.setParameter("P_DESC_COMUN", param.getNombreComun());
            processStored.setParameter("P_DESC_CIENTIFICO", param.getNombreCientifico());
            processStored.setParameter("p_AUTOR", param.getAutor());
            processStored.setParameter("p_FAMILIA", param.getFamilia());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());

            processStored.execute();
            List<EspecieForestalEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                EspecieForestalEntity temp = null;
                for (Object[] row: spResult){
                    temp = new EspecieForestalEntity();
                    temp.setIdEspecie((Short) row[0]);
                    temp.setNombreComun((String) row[1]);
                    temp.setNombreCientifico((String) row[2]);
                    temp.setAutor(row[3]==null?null:(String) row[3]);
                    temp.setFamilia(row[4]==null?null:(String) row[4]);
                    temp.setCantidad(row[5]==null?null:(Integer) row[5]);
                    result.add(temp);
                    pageable.setTotalRecords(SpUtil.toLong(row[5]));
                }
            }
          //  result.
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }*/

    @Override
    public ResultClassEntity listarEspecieForestalSincronizacion(EspecieForestalEntity param) {
        ResultClassEntity resul=new ResultClassEntity();
        try
        {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Sincronizacion.pa_EspecieForestal_listar");

            processStored.execute();
            List<EspecieForestalEntity> objList=new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                EspecieForestalEntity objOrdPro = null;

                for (Object[] row: spResult){
                    objOrdPro = new EspecieForestalEntity();
                    objOrdPro.setNombreCientifico(row[0]==null?null:(String) row[0]);
                    objOrdPro.setNombreComun(row[1]==null?null:(String) row[1]);
                    objOrdPro.setIdFamilia(row[2]==null?null:(Short) row[2]);
                    objOrdPro.setFamilia(row[3]==null?null:(String) row[3]);
                    objOrdPro.setIdEspecie(row[4]==null?null:(Short) row[4]);

                    objList.add(objOrdPro);
                }

            }

            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setSuccess((objList.size()!=0?true:false));
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setSuccess(false);
        }
        return resul;
    }


    @Override
    public ResultClassEntity listarCuencaSubcuenca(CuencaEntity param) {
        ResultClassEntity resul=new ResultClassEntity();
        try
        {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CuencaSub_Listar");
            processStored.registerStoredProcedureParameter("idCuenca", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idCuenca",param.getIdCuenca());
            processStored.execute();
            List<CuencaEntity> objList=new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                CuencaEntity objOrdPro = null;

                for (Object[] row: spResult){
                    objOrdPro = new CuencaEntity();
                    objOrdPro.setIdCuenca(row[0]== null? 0 :  ((Short) row[0]).intValue());
                    objOrdPro.setCodCuenca(row[1]==null?null:(String) row[1]);
                    objOrdPro.setCuenca(row[2]==null?null:(String) row[2]);
                    objOrdPro.setIdSubCuenca(row[3]== null? 0 : (short) row[3]);
                    objOrdPro.setCodSubCuenca(row[4]==null?null:(String) row[4]);
                    objOrdPro.setSubCuenca(row[5]==null?null:(String) row[5]);

                    objList.add(objOrdPro);
                }

            }

            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setSuccess((objList.size()!=0?true:false));
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setSuccess(false);
        }
        return resul;
    }


}
