package pe.gob.serfor.mcsniffs.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;
import pe.gob.serfor.mcsniffs.repository.FuenteRepository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Repository
public class FuenteRepositoryImpl extends JdbcDaoSupport implements FuenteRepository {

    @Autowired
    @Qualifier("DataSourceBDCORECENTRAL")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public String test() {

        SeguridadEntity usuario = null;
        String sql = " select * from Maestras.T_MAP_FUENTE ";

        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

        System.out.println("///////////////////////////////////////////////");
        System.out.println(rows);

            for (Map<String, Object> row : rows) {
                System.out.println(row.get("TX_DESCRIPCION"));
            }

        return "hola";
    }
}
