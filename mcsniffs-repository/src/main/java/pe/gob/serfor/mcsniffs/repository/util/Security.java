package pe.gob.serfor.mcsniffs.repository.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
@Repository
public class Security {

    @Value("${security.KEY}")
    private String KEY_SECURITY;

    public  SecretKeySpec createKey() {
        String key=KEY_SECURITY;
        try{
            byte [] cadena = key.getBytes(StandardCharsets.UTF_8);
            MessageDigest md = MessageDigest.getInstance("MD5");
            cadena = md.digest(cadena);
            cadena= Arrays.copyOf(cadena,16);
            SecretKeySpec secretKeySpec = new SecretKeySpec(cadena,"AES");
            return secretKeySpec;

        }
        catch(Exception e){
            return null;
        }


    }

    public  String aesEncrypt(String encrypt) {
        try {
            SecretKeySpec secretKeySpec = createKey();
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipher.ENCRYPT_MODE,secretKeySpec);
            byte [] cadena = encrypt.getBytes(StandardCharsets.UTF_8);
            byte [] encriptada= cipher.doFinal(cadena);
            String cade_encriptada= Base64.getEncoder().encodeToString(encriptada);
            return cade_encriptada;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public  String aesDecrypt(String decrypt) {
        try {
            SecretKeySpec secretKeySpec = createKey();
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipher.DECRYPT_MODE,secretKeySpec);

            byte [] cadena = Base64.getDecoder().decode(decrypt);
            byte [] decriptada= cipher.doFinal(cadena);
            String cadena_decriptada=  new String(decriptada);
            return cadena_decriptada;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
