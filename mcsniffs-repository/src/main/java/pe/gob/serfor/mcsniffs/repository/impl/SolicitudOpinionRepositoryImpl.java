package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueDetalleActividadEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudOpinionEntity;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudOpinionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SolicitudOpinionRepositoryImpl extends JdbcDaoSupport implements SolicitudOpinionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(SolicitudOpinionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager em;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarSolicitudOpinion(SolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_SolicitudOpinion_Listar");
            sp.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("siglaEntidad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocGestion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estSolicitudOpinion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaDocGestion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            sp.setParameter("siglaEntidad", param.getSiglaEntidad());
            sp.setParameter("tipoDocGestion", param.getTipoDocGestion());
            sp.setParameter("tipoDocumento", param.getTipoDocumento());
            sp.setParameter("estSolicitudOpinion", param.getEstSolicitudOpinion());
            sp.setParameter("nroDocGestion", param.getNroDocGestion());
            sp.setParameter("fechaDocGestion", param.getFechaDocGestion());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());
            sp.execute();
            List<SolicitudOpinionDto> list = new ArrayList<SolicitudOpinionDto>();
            List<Object[]> spResult =sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    SolicitudOpinionDto obj = new SolicitudOpinionDto();
                    obj.setIdSolicitudOpinion((Integer) row[0]);
                    obj.setSiglaEntidad((String) row[1]);
                    obj.setTipoDocGestion((String) row[2]);
                    obj.setTipoDocGestionTexto((String) row[3]);
                    obj.setNroDocGestion((Integer) row[4]);
                    obj.setFechaDocGestion((Date) row[5]);
                    obj.setEstadoSolicitudOpinion((String) row[6]);
                    obj.setEstadoSolicitudOpinionTexto((String) row[7]);
                    obj.setAsunto((String) row[8]);
                    obj.setTipoDocumento((String) row[9]);
                    obj.setIdPlanManejo((Integer) row[10]);
                    result.setTotalRecord((Integer) row[11]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error("SolicitudOpinionRepositoryImpl -ListarSolicitudOpinion ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity RegistrarSolicitudOpinion(SolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_SolicitudOpinion_Registrar");
            processStored.registerStoredProcedureParameter("siglaEntidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocGestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaDocGestion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estSolicitudOpinion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolOpinion", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("siglaEntidad", param.getSiglaEntidad());
            processStored.setParameter("tipoDocGestion", param.getTipoDocGestion());
            processStored.setParameter("nroDocGestion", param.getNroDocGestion());
            processStored.setParameter("fechaDocGestion", param.getFechaDocGestion());
            processStored.setParameter("estSolicitudOpinion", param.getEstSolicitudOpinion());
            processStored.setParameter("documentoGestion", param.getDocumentoGestion());
            processStored.setParameter("tipoDocumento", param.getTipoDocumento());
            processStored.setParameter("numeroDocumento", param.getNumeroDocumento());
            processStored.setParameter("asunto", param.getAsunto());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();

            Integer idSolOpinion = (Integer) processStored.getOutputParameterValue("idSolOpinion");

            param.setIdSolicitudOpinion(idSolOpinion);

            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró Solicitud Opinión correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error("SolicitudOpinionRepositoryImpl -RegistrarSolicitudOpinion ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarSolicitudOpinion(SolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_SolicitudOpinion_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estSolicitudOpinion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("esFavorable", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDoc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEmision", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            processStored.setParameter("estSolicitudOpinion", param.getEstSolicitudOpinion());
            processStored.setParameter("esFavorable", param.getEsFavorable());
            processStored.setParameter("descripcion", param.getDescripcion());
            processStored.setParameter("tipoDoc", param.getTipoDoc());
            processStored.setParameter("tipoDocumento", param.getTipoDocumento());
            processStored.setParameter("fechaEmision", param.getFechaEmision());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Solicitud Opinión correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarSolicitudOpinionArchivo(SolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_SolicitudOpinionArchivo_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoOficio", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoRespuesta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            processStored.setParameter("documentoOficio", param.getDocumentoOficio());
            processStored.setParameter("documentoRespuesta", param.getDocumentoRespuesta());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró Archivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error("SolicitudOpinionRepositoryImpl -ActualizarSolicitudOpinionArchivo ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessage(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarSolicitudOpinionArchivo(SolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_SolicitudOpinionArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            processStored.setParameter("idArchivo", param.getIdArchivo());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó Solicitud Opinión Archivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error("SolicitudOpinionRepositoryImpl -EliminarSolicitudOpinionArchivo ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ObtenerSolicitudOpinion(SolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            log.info("ObtenerSolicitudOpinion - ObtenerSolicitudOpinion \n"+param.toString());
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_SolicitudOpinion_Obtener");
            sp.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            sp.execute();
            SolicitudOpinionDto obj = new SolicitudOpinionDto();
            List<Object[]> spResult =sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {

                    obj.setIdSolicitudOpinion((Integer) row[0]);
                    obj.setSiglaEntidad((String) row[1]);
                    obj.setTipoDocGestion((String) row[2]);
                    obj.setTipoDocGestionTexto((String) row[3]);
                    obj.setNroDocGestion((Integer) row[4]);
                    obj.setFechaDocGestion((Date) row[5]);
                    obj.setEstadoSolicitudOpinion((String) row[6]);
                    obj.setEstadoSolicitudOpinionTexto((String) row[7]);
                    obj.setAsunto((String) row[8]);
                    obj.setFechaEmision((Date) row[9]);
                    obj.setTipoDoc((String) row[10]);
                    obj.setDescripcion((String) row[11]);
                    obj.setEsFavorable((Boolean) row[12]);
                    obj.setDocumentoGestion((Integer) row[13]);
                    obj.setDocumentoOficio((Integer) row[14]);
                    obj.setDocumentoRespuesta((Integer) row[15]);
                    obj.setEstSolicitudOpinion((String) row[16]);
                    obj.setNombreDocumentoOficio((String) row[17]);
                    obj.setNombreDocumentoRespuesta((String) row[18]);
                    obj.setTipoDocumento((String) row[19]);
                    obj.setNombreDocumentoArchivo((String) row[20]);
                    obj.setIdArchivo((Integer) row[21]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);
            return  result;
        } catch (Exception e) {
            log.error("SolicitudOpinionRepositoryImpl -ObtenerSolicitudOpinion ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado: Danny Nazario [30/12/2021]
     * @descripción: {Registrar y Actualizar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */
    @PersistenceContext
    private EntityManager entityManager;



    @Override
    public ResultClassEntity RegistrarSolicitudOpinionEvaluacion(List<SolicitudOpinionEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudOpinionEntity> list_ = new ArrayList<>();
        LogAuditoriaEntity logAuditoriaEntity;
        Integer idSolOpinionOut = 0;

        try {
            for (SolicitudOpinionEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudOpinionEvaluacion_Registrar");
                sp.registerStoredProcedureParameter("idSolOpinion", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codSolicitud", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subCodSolicitud", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("numDocumento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaDocumento", Timestamp.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("entidad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idSolOpinion", p.getIdSolOpinion());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codSolicitud", p.getCodSolicitud());
                sp.setParameter("subCodSolicitud", p.getSubCodSolicitud());
                sp.setParameter("tipoDocumento", p.getTipoDocumento());
                sp.setParameter("numDocumento", p.getNumDocumento());
                sp.setParameter("fechaDocumento", p.getFechaDocumento());
                sp.setParameter("asunto", p.getAsunto());
                sp.setParameter("entidad", p.getEntidad());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("idArchivo", p.getIdArchivo());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        SolicitudOpinionEntity obj_ = new SolicitudOpinionEntity();
                        obj_.setIdSolOpinion((Integer) row_[0]);

                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setCodSolicitud((String) row_[2]);
                        obj_.setSubCodSolicitud((String) row_[3]);
                        obj_.setTipoDocumento((String) row_[4]);
                        obj_.setNumDocumento((String) row_[5]);
                        obj_.setFechaDocumento((Timestamp) row_[6]);
                        obj_.setAsunto((String) row_[7]);
                        obj_.setEntidad((String) row_[8]);
                        obj_.setDescripcion((String) row_[9]);
                        obj_.setIdArchivo((Integer) row_[10]);
                        list_.add(obj_);
                        idSolOpinionOut= ((Integer) row_[0]);

                    }
                }
                if(p.getIdSolOpinion() == 0 || p.getIdSolOpinion()==null) {
                    logAuditoriaEntity = new LogAuditoriaEntity(
                            LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                            LogAuditoria.REGISTRAR,
                            LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLICITUD_OPINION + idSolOpinionOut,
                            p.getIdUsuarioRegistro());

                }else{

                    logAuditoriaEntity = new LogAuditoriaEntity(
                            LogAuditoria.Table.T_MVC_SOLICITUD_OPINION,
                            LogAuditoria.ACTUALIZAR,
                            LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUD_OPINION + p.getIdSolOpinion(),
                            p.getIdUsuarioRegistro());

                }
                logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la Solicitud de opinión correctamente.");






            return result;
        } catch (Exception e) {
            log.error("SolicitudOpinionRepositoryImpl -RegistrarSolicitudOpinionEvaluacion ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Listar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */

    @Override
    public List<SolicitudOpinionEntity> ListarSolicitudOpinionEvaluacion(Integer idPlanManejo, String codSolicitud, String subCodSolicitud) throws Exception {
        List<SolicitudOpinionEntity> lista = new ArrayList<SolicitudOpinionEntity>();
        try {
            int idDetalle=0;
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudOpinionEvaluacion_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodSolicitud", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codSolicitud", codSolicitud);
            processStored.setParameter("subCodSolicitud", subCodSolicitud);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        SolicitudOpinionEntity temp = new SolicitudOpinionEntity();
                        temp.setIdSolOpinion((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setCodSolicitud((String) row[2]);
                        temp.setSubCodSolicitud((String) row[3]);
                        temp.setTipoDocumento((String) row[4]);
                        temp.setNumDocumento((String) row[5]);
                        temp.setFechaDocumento((Timestamp) row[6]);
                        temp.setAsunto((String) row[7]);
                        temp.setEntidad((String) row[8]);
                        temp.setNombreEntidad((String) row[9]);
                        temp.setDescripcion((String) row[10]);
                        temp.setIdArchivo((Integer) row[11]);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }


    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Eliminar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */

    @Override
    public ResultClassEntity EliminarSolicitudOpinionEvaluacion(SolicitudOpinionEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_SolicitudOpinionEvaluacion_Eliminar]");
        processStored.registerStoredProcedureParameter("idSolOpinion", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

        processStored.setParameter("idSolOpinion", param.getIdSolOpinion());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

}
