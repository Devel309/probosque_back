package pe.gob.serfor.mcsniffs.repository.impl;

import lombok.var;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudMovimientoFlujoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudMovimientoFlujoRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudMovimientoFlujoRepositoryImpl extends JdbcDaoSupport implements SolicitudMovimientoFlujoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    @PersistenceContext
    private EntityManager entityManager;
    private static final Logger log = LogManager.getLogger(SolicitudMovimientoFlujoRepositoryImpl.class);

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registroSolicitudMovimientoFlujo(SolicitudMovimientoFlujoEntity entity) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudMovimientoFlujo_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("flagObservado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistra", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", entity.getIdSolicitud());
            processStored.setParameter("tipoEvaluacion", entity.getTipoValidacion());
            processStored.setParameter("flagObservado", entity.getFlagObservado());
            processStored.setParameter("idUsuarioRegistra", entity.getIdUsuarioRegistro());
            boolean boolResultado = processStored.execute();
            result.setData(boolResultado);
            result.setSuccess(true);
            result.setMessage("Se registró solicitud correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error al registrar.");
            return result;
        }

    }

}
