package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ArchivoSolicitudAccesoRepository;
import pe.gob.serfor.mcsniffs.repository.ArchivoSolicitudRepository;
import pe.gob.serfor.mcsniffs.repository.FileServerRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

import java.io.File;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @autor: Victor Matos [14-07-2021]
 * @modificado:
 * @descripción: {Repositorio cargar y descargar archivo del file server}
 *
 */

@Repository
public class FileServerRepositoryImpl implements FileServerRepository {

    private static final Logger log = LogManager.getLogger(FileServerRepositoryImpl.class);

    @Autowired
    private FileServerConexion fileServerConexion;

    @Autowired
    private ArchivoSolicitudAccesoRepository accesoRepository;

    @Autowired
    private ArchivoSolicitudRepository  solicitudRepository;

    private static final String STRING_VACIO = "";
    private static final String SEPARADOR_ARCHIVO = ".";

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Transfiere el archivo al file server}
     * @param: Un archivo tipo MultipartFile que contiene el archivo cargado.
     */
    @Override
    public String insertarArchivoSolicitudAcceso(MultipartFile file) {
        log.info("FileServer - insertarArchivoSolicitud", "Inicio insertarArchivoSolicitud");
        try {
            log.info("FileServer - insertarArchivoSolicitud", "Proceso realizado correctamente");
            return fileServerConexion.uploadFile(file);
        } catch (Exception e) {
            log.error("FileServer - insertarArchivoSolicitud", e.getMessage());
        }
        return STRING_VACIO;
    }

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Descarga el archivo del file server al cliente}
     * @param: Entidad de tipo ArchivoEntity .
     */
    @Override
    public ResultArchivoEntity descargarArchivoSolicitudAcceso(Integer idSolicitudAcceso) {
        log.info("FileServer - descargarArchivoSolicitud", "Inicio descargarArchivoSolicitud");
        ResultClassEntity<ArchivoEntity> resultEntity = accesoRepository.obtenerArchivoSolicitudAcceso(idSolicitudAcceso);
        String nombreArchivoGenerado = resultEntity.getData().getNombreGenerado().
                concat(SEPARADOR_ARCHIVO).
                concat(resultEntity.getData().getExtension());
        String nombreArchivo = resultEntity.getData().getNombre().
                concat(SEPARADOR_ARCHIVO).
                concat(resultEntity.getData().getExtension());
        ResultArchivoEntity resultArchivoEntity = new ResultArchivoEntity();
        try {
            byte[] byteFile =fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
            resultArchivoEntity.setArchivo(byteFile);
            resultArchivoEntity.setNombeArchivo(nombreArchivo);
            resultArchivoEntity.setContenTypeArchivo("application/octet-stream");
            resultArchivoEntity.setSuccess(true);
            resultArchivoEntity.setMessage("Se descargo el archivo del file server  con éxito.");
            log.info("FileServer - descargarArchivoSolicitud", "Proceso realizado correctamente");
        } catch (Exception e) {
            resultArchivoEntity.setSuccess(false);
            resultArchivoEntity.setMessage("Se produjo un error al descargar el archivo de Azure Container");
            resultArchivoEntity.setMessageExeption(e.getMessage());
            log.error("FileServer - descargarArchivoSolicitud", e.getMessage());
        }
        return resultArchivoEntity;
    }

    @Override
    public String insertarArchivoSolicitud(MultipartFile file) {
        log.info("FileServer - insertarArchivoSolicitud", "Inicio insertarArchivoSolicitud");
        try {
            log.info("FileServer - insertarArchivoSolicitud", "Proceso realizado correctamente");
            return fileServerConexion.uploadFile(file);
        } catch (Exception e) {
            log.error("FileServer - insertarArchivoSolicitud", e.getMessage());
        }
        return STRING_VACIO;
    }

    @Override
    public ResultArchivoEntity descargarArchivoSolicitud(Integer idArchivoSolicitud,Integer idSolicitud,String tipoArchivo) {
        log.info("FileServer - descargarArchivoSolicitud", "Inicio descargarArchivoSolicitud");
        ResultClassEntity<ArchivoSolicitudEntity> resultEntity = solicitudRepository.obtenerArchivoSolicitud(idArchivoSolicitud,idSolicitud,tipoArchivo);
        String nombreArchivoGenerado = resultEntity.getData().getNombreGenerado().
                concat(SEPARADOR_ARCHIVO).
                concat(resultEntity.getData().getExtensionArchivo());
        String nombreArchivo = resultEntity.getData().getNombreArchivo().
                concat(SEPARADOR_ARCHIVO).
                concat(resultEntity.getData().getExtensionArchivo());
        ResultArchivoEntity resultArchivoEntity = new ResultArchivoEntity();
        try {
            byte[] byteFile =fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
            resultArchivoEntity.setArchivo(byteFile);
            resultArchivoEntity.setNombeArchivo(nombreArchivo);
            resultArchivoEntity.setContenTypeArchivo("application/octet-stream");
            resultArchivoEntity.setSuccess(true);
            resultArchivoEntity.setMessage("Se descargo el archivo del file server  con éxito.");
            log.info("FileServer - descargarArchivoSolicitud", "Proceso realizado correctamente");
        } catch (Exception e) {
            resultArchivoEntity.setSuccess(false);
            resultArchivoEntity.setMessage("Se produjo un error al descargar el archivo de Azure Container");
            resultArchivoEntity.setMessageExeption(e.getMessage());
            log.error("FileServer - descargarArchivoSolicitud", e.getMessage());
        }
        return resultArchivoEntity;
    }


    @Override
    public ResultArchivoEntity descargarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAdjunto,Integer idSolicitud,String tipoArchivo) {
        log.info("FileServer - descargarArchivoSolicitudAdjunto", "Inicio descargarArchivoSolicitudAdjunto");
        ResultClassEntity<ArchivoSolicitudEntity> resultEntity = solicitudRepository.obtenerArchivoSolicitudAdjunto(idArchivoSolicitudAdjunto,idSolicitud,tipoArchivo);
        String nombreArchivoGenerado = resultEntity.getData().getNombreGenerado().
                concat(SEPARADOR_ARCHIVO).
                concat(resultEntity.getData().getExtensionArchivo());
        String nombreArchivo = resultEntity.getData().getNombreArchivo().
                concat(SEPARADOR_ARCHIVO).
                concat(resultEntity.getData().getExtensionArchivo());
        ResultArchivoEntity resultArchivoEntity = new ResultArchivoEntity();
        try {
            byte[] byteFile =fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
            resultArchivoEntity.setArchivo(byteFile);
            resultArchivoEntity.setNombeArchivo(nombreArchivo);
            resultArchivoEntity.setContenTypeArchivo("application/octet-stream");
            resultArchivoEntity.setSuccess(true);
            resultArchivoEntity.setMessage("Se descargo el archivo del file server  con éxito.");
            log.info("FileServer - descargarArchivoSolicitudAdjunto", "Proceso realizado correctamente");
        } catch (Exception e) {
            resultArchivoEntity.setSuccess(false);
            resultArchivoEntity.setMessage("Se produjo un error al descargar el archivo de Azure Container");
            resultArchivoEntity.setMessageExeption(e.getMessage());
            log.error("FileServer - descargarArchivoSolicitudAdjunto", e.getMessage());
        }
        return resultArchivoEntity;
    }
}