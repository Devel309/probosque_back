package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;

public interface AreaSistemaPlantacionRepository {
    ResultClassEntity registrarAreaSistemaPlantacion(AreaSistemaPlantacionDto obj);
    ResultClassEntity listarAreaSistemaPlantacion(AreaSistemaPlantacionDto filtro);
    ResultClassEntity eliminarAreaSistemaPlantacion(AreaSistemaPlantacionDto p);
    ResultClassEntity ListarEspecieForestalSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto param)  throws Exception;
    ResultClassEntity eliminarSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto param) throws Exception;
    ResultClassEntity registrarSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto param) throws Exception;
}
