package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudConcesionEvaluacionRepositoryImpl extends JdbcDaoSupport
        implements SolicitudConcesionEvaluacionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionEvaluacionDto> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionEvaluacion_ListarFiltro");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionEvaluacion", obj.getIdSolicitudConcesionEvaluacion());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            if (spResult != null && !spResult.isEmpty()) {
                SolicitudConcesionEvaluacionDto c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolicitudConcesionEvaluacionDto();
                        c.setIdSolicitudConcesionEvaluacion(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitudConcesion(row[1] == null ? null : (Integer) row[1]);
                        c.setStrFechaInicio(row[2] == null ? null : (String) row[2]);
                        c.setStrFechaFin(row[3] == null ? null : (String) row[3]);
                        c.setDiasEvaluacion(row[4] == null ? null : (Integer) row[4]);
                        c.setCodigoCatZonificacion(row[5] == null ? null : (String) row[5]);
                        c.setSinCategoria(row[6] == null ? null : (Boolean) row[6]);
                        c.setIdArchivoSutento(row[7] == null ? null : (Integer) row[7]);
                        c.setSolicitaOpinionSernamp(row[8] == null ? null : (Boolean) row[8]);
                        c.setSolicitaOpinionAna(row[9] == null ? null : (Boolean) row[9]);
                        c.setCodigoResultadoEvaluacion(row[10] == null ? null : (String) row[10]);
                        c.setObservacion(row[11] == null ? null : (String) row[11]);
                        c.setPuntajeObtenido(row[12] == null ? null : ((BigDecimal) row[12]).doubleValue());
                        c.setPuntajeExtra(row[13] == null ? null : ((BigDecimal) row[13]).doubleValue());
                        c.setPuntajeTotal(row[14] == null ? null : ((BigDecimal) row[14]).doubleValue());
                        c.setComunicadoEnviado(row[15] == null ? null : (Boolean) row[15]);
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de evaluaciones de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo obtener la lista de evaluaciones de la solicitud de concesión");
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionEvaluacion_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ndiasEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoCatZonificacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sinCategoria", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSutento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitaOpinionSernamp", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitaOpinionAna", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoResultadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("puntajeObtenido", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("puntajeExtra", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("puntajeTotal", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("comunicadoEnviado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("calificacionFinalizada", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoInformeFinal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionEvaluacion", obj.getIdSolicitudConcesionEvaluacion());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("fechaInicio", obj.getFechaInicio());
            processStored.setParameter("fechaFin", obj.getFechaFin());
            processStored.setParameter("ndiasEvaluacion", obj.getDiasEvaluacion());
            processStored.setParameter("codigoCatZonificacion", obj.getCodigoCatZonificacion());
            processStored.setParameter("sinCategoria", obj.getSinCategoria());
            processStored.setParameter("idArchivoSutento", obj.getIdArchivoSutento());
            processStored.setParameter("solicitaOpinionSernamp", obj.getSolicitaOpinionSernamp());
            processStored.setParameter("solicitaOpinionAna", obj.getSolicitaOpinionAna());
            processStored.setParameter("codigoResultadoEvaluacion", obj.getCodigoResultadoEvaluacion());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("puntajeObtenido", obj.getPuntajeObtenido());
            processStored.setParameter("puntajeExtra", obj.getPuntajeExtra());
            processStored.setParameter("puntajeTotal", obj.getPuntajeTotal());
            processStored.setParameter("comunicadoEnviado", obj.getComunicadoEnviado());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            processStored.setParameter("calificacionFinalizada", obj.getCalificacionFinalizada());
            processStored.setParameter("idArchivoInformeFinal", obj.getIdArchivoInformeFinal());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Solicitud Concesión Evaluación.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Jordy Zamata [31-01-2021]
     * @modificado:
     * @descripción: {Listar Detalle de la Solicitud de Concesión Evaluación}
     * @param: listarSolicitudConcesionEvaluacionDetalle
     */
    @Override
    public ResultClassEntity listarSolicitudConcesionEvaluacionDetalle(SolicitudConcesionEvaluacionDto obj)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionEvaluacionDto> lista = new ArrayList<SolicitudConcesionEvaluacionDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesion_Evaluacion_Detalle_Listar");
            processStored.registerStoredProcedureParameter("idSolConEvaluacionDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMotivDenegacion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolConEvaluacionDet", obj.getIdSolicitudConcesionEvaluacion());
            processStored.setParameter("codSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("codigoMotivDenegacion", obj.getCodigoMotivDenegacion());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    SolicitudConcesionEvaluacionDto temp = new SolicitudConcesionEvaluacionDto();
                    temp.setIdSolicitudConcesionEvaluacionDetalle(row[0] == null ? null : (Integer) row[0]);
                    temp.setIdSolicitudConcesionEvaluacion(row[1] == null ? null : (Integer) row[1]);
                    temp.setCodigoSeccion(row[2] == null ? null : (String) row[2]);
                    temp.setCodigoSubSeccion(row[3] == null ? null : (String) row[3]);
                    temp.setLineamiento(row[4] == null ? null : (Boolean) row[4]);
                    temp.setCodigoLineamiento(row[5] == null ? null : (String) row[5]);
                    temp.setConforme(row[6] == null ? null : (Boolean) row[6]);
                    temp.setObservacion(row[7] == null ? null : (String) row[7]);
                    temp.setCodigoMotivDenegacion(row[8] == null ? null : (String) row[8]);
                    temp.setEstado(row[9] == null ? null : row[9].toString());
                    temp.setIdUsuarioRegistro(row[10] == null ? null : (Integer) row[10]);
                    temp.setFechaRegistro(row[11] == null ? null : (Date) row[11]);
                    temp.setIdUsuarioModificacion(row[12] == null ? null : (Integer) row[12]);
                    temp.setFechaModificacion(row[13] == null ? null : (Date) row[13]);
                    temp.setIdUsuarioElimina(row[14] == null ? null : (Integer) row[14]);
                    temp.setFechaElimina(row[15] == null ? null : (Date) row[15]);

                    lista.add(temp);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionEvaluacion(SolicitudConcesionEvaluacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionEvaluacion_Registrar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diasEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoCatZonificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sinCategoria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSutento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitaOpinionSernamp", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitaOpinionAna", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoResultadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            //processStored.registerStoredProcedureParameter("codigoMotivDenegacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                    ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("fechaInicio", obj.getFechaInicio());
            processStored.setParameter("fechaFin", obj.getFechaFin());
            processStored.setParameter("diasEvaluacion", obj.getDiasEvaluacion());
            processStored.setParameter("codigoCatZonificacion", obj.getCodigoCatZonificacion());
            processStored.setParameter("sinCategoria", obj.getSinCategoria());
            processStored.setParameter("idArchivoSutento", obj.getIdArchivoSutento());
            processStored.setParameter("solicitaOpinionSernamp", obj.getSolicitaOpinionSernamp());
            processStored.setParameter("solicitaOpinionAna", obj.getSolicitaOpinionAna());
            processStored.setParameter("codigoResultadoEvaluacion", obj.getCodigoResultadoEvaluacion());
            processStored.setParameter("observacion", obj.getObservacion());
            //processStored.setParameter("codigoMotivDenegacion", obj.getCodigoMotivDenegacion());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolicitudConcesionEvaluacion = (Integer) processStored
                    .getOutputParameterValue("idSolicitudConcesionEvaluacion");
            obj.setIdSolicitudConcesionEvaluacion(idSolicitudConcesionEvaluacion);
            result.setCodigo(idSolicitudConcesionEvaluacion);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la evaluación de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

}
