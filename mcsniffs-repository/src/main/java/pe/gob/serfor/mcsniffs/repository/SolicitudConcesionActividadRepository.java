package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface SolicitudConcesionActividadRepository {

    ResultClassEntity listarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj);
    ResultClassEntity registrarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj);
    ResultClassEntity actualizarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj);
}
