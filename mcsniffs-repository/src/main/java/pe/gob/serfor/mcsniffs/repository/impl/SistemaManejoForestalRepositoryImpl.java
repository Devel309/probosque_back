package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;

import pe.gob.serfor.mcsniffs.entity.Parametro.SistemaManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.SistemaManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Julio Meza [26-07-2021]
 * @modificado: Alexis Salgado 17-08-2021 / 06-09-2021
 * @descripción: {Repositorio registrar un Sistema Manejo Forestal}
 *
 */

@Repository
public class SistemaManejoForestalRepositoryImpl extends JdbcDaoSupport implements SistemaManejoForestalRepository {
	@Autowired
	@Qualifier("dataSourceBDMCSNIFFS")
	DataSource dataSource;

	private static final Logger log = LogManager
			.getLogger(pe.gob.serfor.mcsniffs.repository.impl.SistemaManejoForestalRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public ResultClassEntity<SistemaManejoForestalDto> obtener(Integer idPlanManejo, String codigoProceso)
			throws Exception {

		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_SistemaManejoForestal_Obtener");
		sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
		sp.setParameter("idPlanManejo", idPlanManejo);
		sp.setParameter("codigoProceso", codigoProceso);
		sp.execute();

		SistemaManejoForestalDto data = setResultDataObtener(sp.getResultList());

		ResultClassEntity<SistemaManejoForestalDto> result = new ResultClassEntity<>();
		result.setData(data);
		result.setSuccess(true);
		return result;
	}

	private SistemaManejoForestalDto setResultDataObtener(List<Object[]> dataDb) throws Exception {
		SistemaManejoForestalDto data = new SistemaManejoForestalDto();
		if (!dataDb.isEmpty()) {
			Object[] item = dataDb.get(0);
			data.setIdPlanManejo(item[0]==null?null:(Integer) item[0]);
			data.setIdSistemaManejoForestal(item[1]==null?null:(Integer) item[1]);
			data.setDescripcionFinMaderable(item[2]==null?null:(String) item[2]);
			data.setDescripcionCicloCorta(item[3]==null?null:(String) item[3]);
			data.setDescripcionFinNoMaderable(item[4]==null?null:(String) item[4]);
			data.setDescripcionCicloAprovechamiento(item[5]==null?null:(String) item[5]);
			data.setCodigoProceso(item[21]==null?null:(String) item[21]);
			data.setSeccion(item[22]==null?null:(String) item[22]);
			data.setSubSeccion(item[23]==null?null:(String) item[23]);

			for (Object[] row : dataDb) {
				SistemaManejoForestalDetalleEntity detalle = new SistemaManejoForestalDetalleEntity();
				detalle.setIdSistemaManejoForestal(row[1]==null?null:(Integer) item[1]);
				detalle.setIdSistemaManejoForestalDetalle(row[6]==null?null:(Integer) row[6]);
				detalle.setCodigoTipoDetalle(row[7]==null?null:(String) row[7]);
				detalle.setCategoriaZona(row[8]==null?null: (String) row[8]);
				detalle.setUsoPotencial(row[9]==null?null: (String) row[9]);
				detalle.setUsoPotencialNumerico(row[10]==null?null:  ((BigDecimal) row[10]).doubleValue());
				detalle.setActividadRealizar(row[11]==null?null:(String) row[11]);
				detalle.setActividades(row[12]==null?null:(String) row[12]);
				detalle.setDescripcionSistema(row[13]==null?null:(String) row[13]);
				detalle.setMaquinariasInsumos(row[14]==null?null:(String) row[14]);
				detalle.setPersonalRequerido(row[15]==null?null:(String) row[15]);
				detalle.setObservacion(row[16]==null?null:(String) row[16]);
				detalle.setCodigoTipoSilvicultural(row[17]==null?null:(String) row[17]);
				detalle.setNombreTipoSilvicultural(row[18]==null?null:(String) row[18]);
				detalle.setDescripcionTipoSilvicultural(row[19]==null?null:(String) row[19]);
				detalle.setEditable(row[20]==null?null:(Boolean) row[20]);
				detalle.setCodigoProceso(row[21]==null?null:(String) row[21]);
				data.getDetalle().add(detalle);
			}
		}
		return data;
	}

	@Override
	public ResultClassEntity<List<SistemaManejoForestalDto>> listar(Integer idSistemaManejoForestal,
			Integer idPlanManejo, String descripcionFinMaderable, String descripcionCicloCorta,
			String descripcionFinNoMaderable, String descripcionCicloAprovechamiento) throws Exception {

		ArrayList<SistemaManejoForestalDto> result = new ArrayList<>();
		ResultClassEntity<List<SistemaManejoForestalDto>> response = new ResultClassEntity<>();
		response.setData(result);
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResultClassEntity<SistemaManejoForestalDto> guardar(SistemaManejoForestalDto request) throws Exception {

		request.setIdSistemaManejoForestal(execGuardar(request));

		for (SistemaManejoForestalDetalleEntity item : request.getDetalle()) {
			Integer itemId = execGuardarDetalle(request.getIdSistemaManejoForestal(), item);
			item.setIdSistemaManejoForestal(request.getIdSistemaManejoForestal());
			item.setIdSistemaManejoForestalDetalle(itemId);
		}

		ResultClassEntity<SistemaManejoForestalDto> result = new ResultClassEntity<>();
		result.setData(request);
		result.setSuccess(true);
		return result;
	}

	@Override
	public ResultClassEntity<SistemaManejoForestalDetalleEntity> guardarDetalle(
			SistemaManejoForestalDetalleEntity request) throws Exception {

		Integer itemId = execGuardarDetalle(request.getIdSistemaManejoForestal(), request);

		request.setIdSistemaManejoForestal(request.getIdSistemaManejoForestal());
		request.setIdSistemaManejoForestalDetalle(itemId);

		ResultClassEntity<SistemaManejoForestalDetalleEntity> result = new ResultClassEntity<>();
		result.setData(request);
		result.setSuccess(true);
		return result;
	}

	@Override
	public ResultClassEntity<Integer> eliminarDetalle(Integer idDetalle, Integer idUsuario) throws Exception {

		execEliminarDetalle(idDetalle, idUsuario);

		ResultClassEntity<Integer> result = new ResultClassEntity<>();
		result.setData(idDetalle);
		result.setSuccess(true);
		return result;
	}

	private Integer execGuardar(SistemaManejoForestalDto item) throws Exception {
		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_SistemaManejoForestal_Guardar");
		sp.registerStoredProcedureParameter("idSistemaManejoForestal", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionFinMaderable", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionCicloCorta", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionFinNoMaderable", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionCicloAprovechamiento", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("seccion", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("subSeccion", String.class, ParameterMode.IN);
		SpUtil.enableNullParams(sp);
		sp.setParameter("idSistemaManejoForestal", item.getIdSistemaManejoForestal());
		sp.setParameter("idPlanManejo", item.getIdPlanManejo());
		sp.setParameter("codigoProceso", item.getCodigoProceso());
		sp.setParameter("descripcionFinMaderable", item.getDescripcionFinMaderable());
		sp.setParameter("descripcionCicloCorta", item.getDescripcionCicloCorta());
		sp.setParameter("descripcionFinNoMaderable", item.getDescripcionFinNoMaderable());
		sp.setParameter("descripcionCicloAprovechamiento", item.getDescripcionCicloAprovechamiento());
		sp.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
		sp.setParameter("seccion", item.getSeccion());
		sp.setParameter("subSeccion", item.getSubSeccion());
		sp.execute();
		return Integer.parseInt(sp.getSingleResult().toString());
	}

	private Integer execGuardarDetalle(Integer idSistemaManejoForestal, SistemaManejoForestalDetalleEntity item)
			throws Exception {
		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_SistemaManejoForestalDetalle_Guardar");

		sp.registerStoredProcedureParameter("idSistemaManejoForestalDetalle", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idSistemaManejoForestal", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoTipoDetalle", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("categoriaZona", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("usoPotencial", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("usoPotencialNumerico", Double.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("actividadRealizar", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("actividades", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionSistema", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("maquinariasInsumos", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("personalRequerido", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoTipoSilvicultural", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("nombreTipoSilvicultural", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionTipoSilvicultural", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("editable", Boolean.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
		SpUtil.enableNullParams(sp);
		sp.setParameter("idSistemaManejoForestalDetalle", item.getIdSistemaManejoForestalDetalle());
		sp.setParameter("idSistemaManejoForestal", idSistemaManejoForestal);
		sp.setParameter("codigoTipoDetalle", item.getCodigoTipoDetalle());
		sp.setParameter("categoriaZona", item.getCategoriaZona());
		sp.setParameter("usoPotencial", item.getUsoPotencial());
		sp.setParameter("usoPotencialNumerico", item.getUsoPotencialNumerico());
		sp.setParameter("actividadRealizar", item.getActividadRealizar());
		sp.setParameter("actividades", item.getActividades());
		sp.setParameter("descripcionSistema", item.getDescripcionSistema());
		sp.setParameter("maquinariasInsumos", item.getMaquinariasInsumos());
		sp.setParameter("personalRequerido", item.getPersonalRequerido());
		sp.setParameter("observacion", item.getObservacion());
		sp.setParameter("codigoTipoSilvicultural", item.getCodigoTipoSilvicultural());
		sp.setParameter("nombreTipoSilvicultural", item.getNombreTipoSilvicultural());
		sp.setParameter("descripcionTipoSilvicultural", item.getDescripcionTipoSilvicultural());
		sp.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
		sp.setParameter("editable", item.getEditable());
		sp.setParameter("codigoProceso", item.getCodigoProceso());
		sp.execute();
		return Integer.parseInt(sp.getSingleResult().toString());
	}

	private void execEliminarDetalle(Integer idDetalle, Integer idUsuario) throws Exception {
		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_SistemaManejoForestalDetalle_Eliminar");

		sp.registerStoredProcedureParameter("idSistemaManejoForestalDetalle", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
		sp.setParameter("idSistemaManejoForestalDetalle", idDetalle);
		sp.setParameter("idUsuarioElimina", idUsuario);
		sp.execute();
	}

}
