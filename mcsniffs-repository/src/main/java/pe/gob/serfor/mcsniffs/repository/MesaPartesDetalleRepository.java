package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;

public interface MesaPartesDetalleRepository {
    List<MesaPartesDetalleDto> listarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception;
    ResultClassEntity registrarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception;
    ResultClassEntity eliminarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception;
}
