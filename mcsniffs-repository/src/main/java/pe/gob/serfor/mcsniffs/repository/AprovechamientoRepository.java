package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

import java.util.List;

public interface AprovechamientoRepository {
    ResultClassEntity RegistrarAprovechamiento(AprovechamientoEntity request) throws Exception;

    List<AprovechamientoEntity> ListarAprovechamiento(Integer idPlanManejo) throws Exception;



}
