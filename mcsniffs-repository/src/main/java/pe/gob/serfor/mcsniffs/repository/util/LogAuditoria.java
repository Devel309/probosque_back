package pe.gob.serfor.mcsniffs.repository.util;

public class LogAuditoria {


    public static final String REGISTRAR  = "TAREGI";
    public static final String ACTUALIZAR  = "TAACTU";
    public static final String ELIMINAR  = "TAELIM";

    public static class Table {
        public static final String T_MVC_PERSONA  = "T_MVC_PERSONA";
        public static final String T_MAD_PROCESO_POSTULACION = "T_MAD_PROCESO_POSTULACION";
        public static final String T_MAC_UNIDAD_APROVECHAMIENTO  = "T_MAC_UNIDAD_APROVECHAMIENTO";
        public static final String T_MAD_CONTRATO = "T_MAD_CONTRATO";
        public static final String T_MAD_PROCESO_OFERTA  = "T_MAD_PROCESO_OFERTA";
        public static final String T_MVC_SOLICITUDCONCESION  = "T_MVC_SOLICITUDCONCESION";
        public static final String T_MVD_FISCALIZACION_ARCHIVO  = "T_MVD_FISCALIZACION_ARCHIVO";
        public static final String T_MVC_SOLICITUD_OPINION  = "T_MVC_SOLICITUD_OPINION";
        public static final String T_MVC_SOLICITUDACCESO  = "T_MVC_SOLICITUDACCESO";
        public static final String T_MVC_RESOLUCION  = "T_MVC_RESOLUCION";
        public static final String T_MVC_IMPUGNACION  = "T_MVC_IMPUGNACION";
        public static final String T_MAC_OPOSICION  = "T_MAC_OPOSICION";
        public static final String T_MVC_EVALUACION_CAMPO  = "T_MVC_EVALUACION_CAMPO";
        public static final String T_MVC_PERMISOFORESTAL  = "T_MVC_PERMISOFORESTAL";
        public static final String T_MVC_SOLICITUDSAN  = "T_MVC_SOLICITUDSAN";
        public static final String T_MAD_CENSOFORESTAL_DETALLE  = "T_MAD_CENSOFORESTAL_DETALLE";
        public static final String T_MVC_PLANMANEJO  = "T_MVC_PLANMANEJO";
        public static final String T_MAC_CENSOFORESTAL  = "T_MAC_CENSOFORESTAL";
        public static final String T_MVC_INFORMACIONGENERAL  = "T_MVC_INFORMACIONGENERAL";
        public static final String T_MVC_SOLPLANTACIONFORESTAL  = "T_MVC_SOLPLANTACIONFORESTAL";


    }



    public static class DescripcionAccion {

        public static class Registrar{
            public static final String T_MVC_PERSONA                = "Se Registró la información en la tabla "+Table.T_MVC_PERSONA +" id: ";
            public static final String T_MAD_PROCESO_POSTULACION    = "Se Registró la información en la tabla "+Table.T_MAD_PROCESO_POSTULACION +" id: ";
            public static final String T_MAC_UNIDAD_APROVECHAMIENTO  = "Se Registró la información en la tabla "+Table.T_MAC_UNIDAD_APROVECHAMIENTO +" id: ";
            public static final String T_MAD_CONTRATO               = "Se Registró la información en la tabla "+Table.T_MAD_CONTRATO +" id: ";
            public static final String T_MAD_PROCESO_OFERTA         = "Se Registró la información en la tabla "+Table.T_MAD_PROCESO_OFERTA +" id: ";
            public static final String T_MVC_SOLICITUDCONCESION     = "Se Registró la información en la tabla "+Table.T_MVC_SOLICITUDCONCESION +" id: ";
            public static final String T_MVD_FISCALIZACION_ARCHIVO  = "Se Registró la información en la tabla "+Table.T_MVD_FISCALIZACION_ARCHIVO +" id: ";
            public static final String T_MVC_SOLICITUD_OPINION      = "Se Registró la información en la tabla "+Table.T_MVC_SOLICITUD_OPINION +" id: ";
            public static final String T_MVC_SOLICITUDACCESO        = "Se Registró la información en la tabla "+Table.T_MVC_SOLICITUDACCESO +" id: ";
            public static final String T_MVC_RESOLUCION             = "Se Registró la información en la tabla "+Table.T_MVC_RESOLUCION +" id: ";
            public static final String T_MVC_IMPUGNACION            = "Se Registró la información en la tabla "+Table.T_MVC_IMPUGNACION +" id: ";
            public static final String T_MAC_OPOSICION              = "Se Registró la información en la tabla "+Table.T_MAC_OPOSICION +" id: ";
            public static final String T_MVC_EVALUACION_CAMPO       = "Se Registró la información en la tabla "+Table.T_MVC_EVALUACION_CAMPO +" id: ";
            public static final String T_MVC_PERMISOFORESTAL        = "Se Registró la información en la tabla "+Table.T_MVC_PERMISOFORESTAL +" id: ";
            public static final String T_MVC_SOLICITUDSAN           = "Se Registró la información en la tabla "+Table.T_MVC_SOLICITUDSAN +" id: ";
            public static final String T_MAD_CENSOFORESTAL_DETALLE  = "Se Registró la información en la tabla "+Table.T_MAD_CENSOFORESTAL_DETALLE +" id: ";
            public static final String T_MVC_PLANMANEJO             = "Se Registró la información en la tabla "+Table.T_MVC_PLANMANEJO +" id: ";
            public static final String T_MAC_CENSOFORESTAL          = "Se Registró la información en la tabla "+Table.T_MAC_CENSOFORESTAL +" id: ";
            public static final String T_MVC_INFORMACIONGENERAL     = "Se Registró la información en la tabla "+Table.T_MVC_INFORMACIONGENERAL +" id: ";
            public static final String T_MVC_SOLPLANTACIONFORESTAL  = "Se Registró la información en la tabla "+Table.T_MVC_SOLPLANTACIONFORESTAL +" id: ";
        }

        public static class Actualizar{
            public static final String T_MVC_PERSONA = "Se Actualizó la información en la tabla "+Table.T_MVC_PERSONA +" del id: ";
            public static final String T_MAD_PROCESO_POSTULACION    = "Se Actualizó la información en la tabla "+Table.T_MAD_PROCESO_POSTULACION +" del id: ";
            public static final String T_MAC_UNIDAD_APROVECHAMIENTO  = "Se Actualizó la información en la tabla "+Table.T_MAC_UNIDAD_APROVECHAMIENTO +" del id: ";
            public static final String T_MAD_CONTRATO               = "Se Actualizó la información en la tabla "+Table.T_MAD_CONTRATO +" del id: ";
            public static final String T_MAD_PROCESO_OFERTA         = "Se Actualizó la información en la tabla "+Table.T_MAD_PROCESO_OFERTA +" del id: ";
            public static final String T_MVC_SOLICITUDCONCESION     = "Se Actualizó la información en la tabla "+Table.T_MVC_SOLICITUDCONCESION +" del id: ";
            public static final String T_MVD_FISCALIZACION_ARCHIVO  = "Se Actualizó la información en la tabla "+Table.T_MVD_FISCALIZACION_ARCHIVO +" del id: ";
            public static final String T_MVC_SOLICITUD_OPINION      = "Se Actualizó la información en la tabla "+Table.T_MVC_SOLICITUD_OPINION +" del id: ";
            public static final String T_MVC_SOLICITUDACCESO        = "Se Actualizó la información en la tabla "+Table.T_MVC_SOLICITUDACCESO +" del id: ";
            public static final String T_MVC_RESOLUCION             = "Se Actualizó la información en la tabla "+Table.T_MVC_RESOLUCION +" del id: ";
            public static final String T_MVC_IMPUGNACION            = "Se Actualizó la información en la tabla "+Table.T_MVC_IMPUGNACION +" del id: ";
            public static final String T_MAC_OPOSICION              = "Se Actualizó la información en la tabla "+Table.T_MAC_OPOSICION +" del id: ";
            public static final String T_MVC_EVALUACION_CAMPO       = "Se Actualizó la información en la tabla "+Table.T_MVC_EVALUACION_CAMPO +" del id: ";
            public static final String T_MVC_PERMISOFORESTAL        = "Se Actualizó la información en la tabla "+Table.T_MVC_PERMISOFORESTAL +" del id: ";
            public static final String T_MVC_SOLICITUDSAN           = "Se Actualizó la información en la tabla "+Table.T_MVC_SOLICITUDSAN +" del id: ";
            public static final String T_MAD_CENSOFORESTAL_DETALLE  = "Se Actualizó la información en la tabla "+Table.T_MAD_CENSOFORESTAL_DETALLE +" del id: ";
            public static final String T_MVC_PLANMANEJO             = "Se Actualizó la información en la tabla "+Table.T_MVC_PLANMANEJO +" del id: ";
            public static final String T_MAC_CENSOFORESTAL          = "Se Actualizó la información en la tabla "+Table.T_MAC_CENSOFORESTAL +" del id: ";
            public static final String T_MVC_INFORMACIONGENERAL     = "Se Actualizó la información en la tabla "+Table.T_MVC_INFORMACIONGENERAL +" del id: ";
            public static final String T_MVC_SOLPLANTACIONFORESTAL  = "Se Actualizó la información en la tabla "+Table.T_MVC_SOLPLANTACIONFORESTAL +" del id: ";
        }

        public static class Eliminar{
            public static final String T_MVC_PERSONA                = "Se Eliminó la información en la tabla "+Table.T_MVC_PERSONA +" del id: ";
            public static final String T_MAD_PROCESO_POSTULACION    = "Se Eliminó la información en la tabla "+Table.T_MAD_PROCESO_POSTULACION +" del id: ";
            public static final String T_MAC_UNIDAD_APROVECHAMIENTO  = "Se Eliminó la información en la tabla "+Table.T_MAC_UNIDAD_APROVECHAMIENTO +" del idProcesoOferta : ";
            public static final String T_MAD_CONTRATO               = "Se Eliminó la información en la tabla "+Table.T_MAD_CONTRATO +" del id: ";
            public static final String T_MAD_PROCESO_OFERTA         = "Se Eliminó la información en la tabla "+Table.T_MAD_PROCESO_OFERTA +" del id: ";
            public static final String T_MVC_SOLICITUDCONCESION     = "Se Eliminó la información en la tabla "+Table.T_MVC_SOLICITUDCONCESION +" del id: ";
            public static final String T_MVD_FISCALIZACION_ARCHIVO  = "Se Eliminó la información en la tabla "+Table.T_MVD_FISCALIZACION_ARCHIVO +" del id: ";
            public static final String T_MVC_SOLICITUD_OPINION      = "Se Eliminó la información en la tabla "+Table.T_MVC_SOLICITUD_OPINION +" del id: ";
            public static final String T_MVC_SOLICITUDACCESO        = "Se Eliminó la información en la tabla "+Table.T_MVC_SOLICITUDACCESO +" del id: ";
            public static final String T_MVC_RESOLUCION             = "Se Eliminó la información en la tabla "+Table.T_MVC_RESOLUCION +" del id: ";
            public static final String T_MVC_IMPUGNACION            = "Se Eliminó la información en la tabla "+Table.T_MVC_IMPUGNACION +" del id: ";
            public static final String T_MAC_OPOSICION              = "Se Eliminó la información en la tabla "+Table.T_MAC_OPOSICION +" del id: ";
            public static final String T_MVC_EVALUACION_CAMPO       = "Se Eliminó la información en la tabla "+Table.T_MVC_EVALUACION_CAMPO +" del id: ";
            public static final String T_MVC_PERMISOFORESTAL        = "Se Eliminó la información en la tabla "+Table.T_MVC_PERMISOFORESTAL +" del id: ";
            public static final String T_MVC_SOLICITUDSAN           = "Se Eliminó la información en la tabla "+Table.T_MVC_SOLICITUDSAN +" del id: ";
            public static final String T_MAD_CENSOFORESTAL_DETALLE  = "Se Eliminó la información en la tabla "+Table.T_MAD_CENSOFORESTAL_DETALLE +" del id: ";
            public static final String T_MVC_PLANMANEJO             = "Se Eliminó la información en la tabla "+Table.T_MVC_PLANMANEJO +" del id: ";
            public static final String T_MAC_CENSOFORESTAL          = "Se Eliminó la información en la tabla "+Table.T_MAC_CENSOFORESTAL +" del id: ";
            public static final String T_MVC_INFORMACIONGENERAL     = "Se Eliminó la información en la tabla "+Table.T_MVC_INFORMACIONGENERAL +" del id: ";
            public static final String T_MVC_SOLPLANTACIONFORESTAL  = "Se Eliminó la información en la tabla "+Table.T_MVC_SOLPLANTACIONFORESTAL +" del id: ";

        }

    }


}
