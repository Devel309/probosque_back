package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.repository.ProteccionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

    @Repository
    public class ProteccionBosqueRepositoryImpl extends JdbcDaoSupport implements ProteccionBosqueRepository {

        @Autowired
        @Qualifier("dataSourceBDMCSNIFFS")
        DataSource dataSource;

        private static final Logger log = LogManager.getLogger(ProteccionBosqueRepositoryImpl.class);

        @PersistenceContext
        private EntityManager entityManager;


        @PostConstruct
        private void initialize(){
            setDataSource(dataSource);
        }
        /**
         * @autor: Ivan Minaya [22-06-2021]
         * @modificado:
         * @descripción: {Configuracion de la protección de bosque demarcación}
         * @param:ProteccionBosqueDemarcacionEntity
         */
        @Override
        public ResultClassEntity ConfiguracionProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity param) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueDemarcacion_Configurar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
                processStored.execute();

                result.setData(param);
                result.setSuccess(true);
                result.setMessage("Se registró configuración proteccion bosque demarcación.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }

        }
        /**
         * @autor: Ivan Minaya [22-06-2021]
         * @modificado:
         * @descripción: {Registrar de la protección de bosque demarcación}
         * @param:List<ProteccionBosqueDemarcacionEntity>
         */
        @Override
        public ResultClassEntity RegistrarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity> list) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                for(ProteccionBosqueDemarcacionEntity p:list){

                    StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueDemarcacion_Configurar");
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idTipoDem", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("implementacion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                    processStored.setParameter("idTipoDem", p.getParametro().getValorSecundario());
                    processStored.setParameter("accion", p.getAccion());
                    processStored.setParameter("descripcion", p.getDescripcion());
                    processStored.setParameter("implementacion", p.getDescripcion());
                    processStored.setParameter("estado", p.getEstado());
                    processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                    processStored.execute();

                    result.setData(p);
                }

                result.setSuccess(true);
                result.setMessage("Se registró Configuración de Protección del bosque correctamente.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }
        /**
         * @autor: Ivan Minaya [22-06-2021]
         * @modificado:
         * @descripción: {Actualizar de la protección de bosque demarcación}
         * @param:List<ProteccionBosqueDemarcacionEntity>
         */
        @Override
        public ResultClassEntity ActualizarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity>  list) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                List<ProteccionBosqueDemarcacionEntity> list_ = new ArrayList<ProteccionBosqueDemarcacionEntity>();
                for(ProteccionBosqueDemarcacionEntity p:list){

                    StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueDemarcacion_Actualizar");
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idProBosqueDem", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idTipoDem", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("implementacion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                    processStored.setParameter("idProBosqueDem", p.getIdProBosqueDem());
                    processStored.setParameter("idTipoDem", Integer.parseInt(p.getParametro().getValorSecundario()));
                    processStored.setParameter("accion", p.getAccion());
                    processStored.setParameter("descripcion", p.getDescripcion());
                    processStored.setParameter("implementacion", p.getImplementacion());
                    processStored.setParameter("estado", p.getEstado());
                    processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                    processStored.execute();


                    List<Object[]> spResult =processStored.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] row : spResult) {
                            ProteccionBosqueDemarcacionEntity obj = new ProteccionBosqueDemarcacionEntity();
                            ParametroEntity pa = new ParametroEntity();
                            PlanManejoEntity pla = new PlanManejoEntity();
                            pla.setIdPlanManejo((Integer) row[0]);
                            obj.setPlanManejo(pla);
                            obj.setIdProBosqueDem((Integer) row[1]);
                            pa.setValorSecundario((java.lang.String) row[2].toString());
                            obj.setAccion((Boolean) row[3]);
                            obj.setDescripcion((java.lang.String) row[4]);
                            obj.setImplementacion((java.lang.String) row[5]);
                            pa.setValorPrimario((java.lang.String) row[6]);
                            obj.setParametro(pa);
                            list_.add(obj);
                        }
                    }

                }

                result.setData(list_);
                result.setSuccess(true);
                result.setMessage("Se actualizó Configuración de Protección del bosque correctamente.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }
        /**
         * @autor: Ivan Minaya [22-06-2021]
         * @modificado:
         * @descripción: {Obtener de la protección de bosque demarcación}
         * @param:ProteccionBosqueDemarcacionEntity
         */
        @Override
        public ResultClassEntity ObtenerProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity param) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueDemarcacion_Obtener");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
                processStored.execute();

                List<ProteccionBosqueDemarcacionEntity> list = new ArrayList<ProteccionBosqueDemarcacionEntity>();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ProteccionBosqueDemarcacionEntity obj = new ProteccionBosqueDemarcacionEntity();
                        ParametroEntity pa = new ParametroEntity();

                        PlanManejoEntity pla = new PlanManejoEntity();
                        pla.setIdPlanManejo((Integer) row[0]);
                        obj.setPlanManejo(pla);
                        obj.setIdProBosqueDem((Integer) row[1]);
                        pa.setValorSecundario((java.lang.String) row[2].toString());
                        obj.setAccion((Boolean) row[3]);
                        obj.setDescripcion((java.lang.String) row[4]);
                        obj.setImplementacion((java.lang.String) row[5]);
                        pa.setValorPrimario((java.lang.String) row[6]);
                        obj.setParametro(pa);
                        list.add(obj);
                    }
                }
                result.setData(list);
                result.setSuccess(true);
                result.setMessage("Se obtuvo proteccion bosque demarcación.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }
        /**
         * @autor: Ivan Minaya [22-06-2021]
         * @modificado:
         * @descripción: {Registrar de la protección de bosque ambiental}
         * @param:ProteccionBosqueDemarcacionEntity
         */
        @Override
        public ResultClassEntity RegistrarProteccionBosqueAmbiental(List<ProteccionBosqueGestionAmbientalEntity> param) {
            ResultClassEntity result = new ResultClassEntity();
            List<ProteccionBosqueGestionAmbientalEntity> list = new ArrayList<ProteccionBosqueGestionAmbientalEntity>();
            try{
                for(ProteccionBosqueGestionAmbientalEntity p:param){
                    StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueGestionAmbiental_Registrar");
                    processStored.registerStoredProcedureParameter("idProBosqueGesAmb", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idTipoPrograma", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("impacto", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("mitigacionAmbiental", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idProBosqueGesAmb", p.getIdProBosqueGesAmb());
                    processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                    processStored.setParameter("idTipoPrograma", p.getIdTipoPrograma());
                    processStored.setParameter("actividad", p.getActividad());
                    processStored.setParameter("impacto", p.getImpacto());
                    processStored.setParameter("mitigacionAmbiental", p.getMitigacionAmbiental());
                    processStored.setParameter("estado", p.getEstado());
                    processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                    processStored.execute();

                    List<Object[]> spResult =processStored.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] row : spResult) {
                            ProteccionBosqueGestionAmbientalEntity obj = new ProteccionBosqueGestionAmbientalEntity();
                            PlanManejoEntity pla = new PlanManejoEntity();
                            obj.setIdProBosqueGesAmb((Integer) row[0]);
                            pla.setIdPlanManejo((Integer) row[1]);
                            obj.setPlanManejo(pla);
                            obj.setIdTipoPrograma((Integer) row[2]);
                            obj.setActividad((String) row[3]);
                            obj.setImpacto((String) row[4]);
                            obj.setMitigacionAmbiental((String) row[5]);
                            list.add(obj);
                        }
                    }
                }
                result.setData(list);
                result.setSuccess(true);
                result.setMessage("Se registró el Plan de Gestión Ambiental correctamente.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }


        @Override
        public ResultClassEntity ListarPorFiltroProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                 List<ProteccionBosqueGestionAmbientalEntity> list  = new ArrayList<ProteccionBosqueGestionAmbientalEntity>();
                 StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueGestionAmbiental_ListarPorFiltro");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoPrograma", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
                processStored.setParameter("idTipoPrograma", param.getIdTipoPrograma());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ProteccionBosqueGestionAmbientalEntity obj = new ProteccionBosqueGestionAmbientalEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();
                        obj.setIdProBosqueGesAmb((Integer) row[0]);
                        pla.setIdPlanManejo((Integer) row[1]);
                        obj.setPlanManejo(pla);
                        obj.setIdTipoPrograma((Integer) row[2]);
                        obj.setActividad((String) row[3]);
                        obj.setImpacto((String) row[4]);
                        obj.setMitigacionAmbiental((String) row[5]);
                        list.add(obj);
                    }
                }
                result.setData(list);
                result.setSuccess(true);
                result.setMessage("Se obtuvieron la proteccion de bosque ambiental.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }


        @Override
        public ResultClassEntity EliminarProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueGestionAmbiental_Eliminar");
                processStored.registerStoredProcedureParameter("idProBosqueGesAmb", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
                processStored.setParameter("idProBosqueGesAmb", param.getIdProBosqueGesAmb());
                processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
                processStored.execute();

                result.setSuccess(true);
                result.setMessage("Se eliminó el registro correctamente.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }

        @Override
        public ResultClassEntity RegistrarProteccionBosqueImpactoAmbiental(List<ProteccionBosqueImpactoAmbientalEntity> param) {
            ResultClassEntity result = new ResultClassEntity();
            List<ProteccionBosqueImpactoAmbientalEntity> list = new ArrayList<ProteccionBosqueImpactoAmbientalEntity>();
            try{
                for(ProteccionBosqueImpactoAmbientalEntity p:param){
                    StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueImpactoAmbiental_Registrar");
                    processStored.registerStoredProcedureParameter("idProBosqueImpacAmb", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("factorAmbiental", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("impacto", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("censo", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("demarcacionLineal", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("construccionCampamento", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("construccionCamino", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tala", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("arrastre", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("otra", Boolean.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("descripccionOtra", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idProBosqueImpacAmb", p.getIdProBosqueImpacAmb());
                    processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                    processStored.setParameter("factorAmbiental", p.getFactorAmbiental());
                    processStored.setParameter("impacto", p.getImpacto());
                    processStored.setParameter("censo", p.getCenso());
                    processStored.setParameter("demarcacionLineal", p.getDemarcacionLineal());
                    processStored.setParameter("construccionCampamento", p.getConstruccionCampamento());
                    processStored.setParameter("construccionCamino", p.getConstruccionCamino());
                    processStored.setParameter("tala", p.getTala());
                    processStored.setParameter("arrastre", p.getArrastre());
                    processStored.setParameter("otra", p.getOtra());
                    processStored.setParameter("descripccionOtra", p.getDescripccionOtra());
                    processStored.setParameter("estado", p.getEstado());
                    processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                    processStored.execute();

                    List<Object[]> spResult =processStored.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] row : spResult) {
                            ProteccionBosqueImpactoAmbientalEntity obj = new ProteccionBosqueImpactoAmbientalEntity();
                            PlanManejoEntity pla = new PlanManejoEntity();

                            obj.setIdProBosqueImpacAmb((Integer) row[0]);
                            pla.setIdPlanManejo((Integer) row[1]);
                            obj.setPlanManejo(pla);
                            obj.setFactorAmbiental((String) row[2]);
                            obj.setImpacto((String) row[3]);
                            obj.setCenso((Boolean) row[4]);
                            obj.setDemarcacionLineal((Boolean) row[5]);
                            obj.setConstruccionCampamento((Boolean) row[6]);
                            obj.setConstruccionCamino((Boolean) row[7]);
                            obj.setTala((Boolean) row[8]);
                            obj.setArrastre((Boolean) row[9]);
                            obj.setOtra((Boolean) row[10]);
                            obj.setDescripccionOtra((String) row[11]);
                            list.add(obj);
                        }
                    }
                }
                result.setData(list);
                result.setSuccess(true);
                result.setMessage("Se registró el Análisis de Impacto Ambiental correctamente.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }

        @Override
        public ResultClassEntity ListarPorFiltroProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                List<ProteccionBosqueImpactoAmbientalEntity> list  = new ArrayList<ProteccionBosqueImpactoAmbientalEntity>();
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueImpactoAmbiental_ListarPorFiltro");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ProteccionBosqueImpactoAmbientalEntity obj = new ProteccionBosqueImpactoAmbientalEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();

                        obj.setIdProBosqueImpacAmb((Integer) row[0]);
                        pla.setIdPlanManejo((Integer) row[1]);
                        obj.setPlanManejo(pla);
                        obj.setFactorAmbiental((String) row[2]);
                        obj.setImpacto((String) row[3]);
                        obj.setCenso((Boolean) row[4]);
                        obj.setDemarcacionLineal((Boolean) row[5]);
                        obj.setConstruccionCampamento((Boolean) row[6]);
                        obj.setConstruccionCamino((Boolean) row[7]);
                        obj.setTala((Boolean) row[8]);
                        obj.setArrastre((Boolean) row[9]);
                        obj.setOtra((Boolean) row[10]);
                        obj.setDescripccionOtra((String) row[11]);
                        list.add(obj);
                    }
                }
                result.setData(list);
                result.setSuccess(true);
                result.setMessage("Se obtuvieron la proteccion de bosque ambiental.");
                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }

        @Override
        public ResultClassEntity EliminarProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param) {
            ResultClassEntity result = new ResultClassEntity();
            try{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueImpactoAmbiental_Eliminar");
                processStored.registerStoredProcedureParameter("idProBosqueImpacAmb", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
                processStored.setParameter("idProBosqueImpacAmb", param.getIdProBosqueImpacAmb());
                processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
                processStored.execute();

                result.setSuccess(true);
                result.setMessage("Se eliminó el registro correctamente.");


                return  result;
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return  result;
            }
        }


        /**
         * @autor:  Rafael Azaña [15-10-2021]
         * @descripción: {Registrar y Actualizar ProteccionBosque}
         * @param: ProteccionBosqueEntity
         * @return: ResponseEntity<ResponseVO>
         */


        @Override
        public ResultClassEntity RegistrarProteccionBosque(List<ProteccionBosqueEntity> list) throws Exception {
            ResultClassEntity result = new ResultClassEntity();
            List<ProteccionBosqueEntity> list_ = new ArrayList<>();
            try {
                for (ProteccionBosqueEntity p : list) {
                    StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosque_Registrar");
                    sp.registerStoredProcedureParameter("idProBosque", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("codPlanGeneral", String.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("subCodPlanGeneral", String.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                    setStoreProcedureEnableNullParameters(sp);
                    sp.setParameter("idProBosque", p.getIdProBosque());
                    sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                    sp.setParameter("codPlanGeneral", p.getCodPlanGeneral());
                    sp.setParameter("subCodPlanGeneral", p.getSubCodPlanGeneral());
                    sp.setParameter("descripcion", p.getDescripcion());
                    sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                    sp.execute();
                    List<Object[]> spResult_ = sp.getResultList();

                    if (!spResult_.isEmpty()) {
                        for (Object[] row_ : spResult_) {
                            ProteccionBosqueEntity obj_ = new ProteccionBosqueEntity();
                            obj_.setIdProBosque((Integer) row_[0]);
                            obj_.setIdPlanManejo((Integer) row_[1]);
                            obj_.setCodPlanGeneral((String) row_[2]);
                            obj_.setSubCodPlanGeneral((String) row_[3]);
                            obj_.setDescripcion((String) row_[4]);
                        /***************************************** DEMARCACION ********************************************************/
                            List<ProteccionBosqueDetalleEntity> listDet = new ArrayList<>();
                            for (ProteccionBosqueDetalleEntity param : p.getListProteccionBosque()) {
                                StoredProcedureQuery pa = entityManager
                                        .createStoredProcedureQuery("dbo.pa_ProteccionBosqueDetalle_Registrar");
                                pa.registerStoredProcedureParameter("idProBosqueDet", Integer.class,ParameterMode.IN);
                                pa.registerStoredProcedureParameter("idProBosque", Integer.class,ParameterMode.IN);
                                pa.registerStoredProcedureParameter("codPlanGeneralDet", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("subCodPlanGeneralDet", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("tipoMarcacion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuAccion", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("implementacion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("factorAmbiental", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("impacto", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuCenso", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuDemarcacionLineal", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuConstruccionCampamento", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuConstruccionCamino", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuTala", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuArrastre", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuOtra", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("descOtra", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("tipoPrograma", Integer.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("mitigacionAmbiental", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("impactoGestion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                                setStoreProcedureEnableNullParameters(pa);
                                pa.setParameter("idProBosqueDet", param.getIdProBosqueDet());
                                pa.setParameter("idProBosque", obj_.getIdProBosque());
                                pa.setParameter("codPlanGeneralDet", param.getCodPlanGeneralDet());
                                pa.setParameter("subCodPlanGeneralDet", param.getSubCodPlanGeneralDet());
                                pa.setParameter("tipoMarcacion", param.getTipoMarcacion());
                                pa.setParameter("nuAccion", param.getNuAccion());
                                pa.setParameter("implementacion", param.getImplementacion());
                                pa.setParameter("factorAmbiental", param.getFactorAmbiental());
                                pa.setParameter("impacto", param.getImpacto());
                                pa.setParameter("nuCenso", param.getNuCenso());
                                pa.setParameter("nuDemarcacionLineal", param.getNuDemarcacionLineal());
                                pa.setParameter("nuConstruccionCampamento", param.getNuConstruccionCampamento());
                                pa.setParameter("nuConstruccionCamino", param.getNuConstruccionCamino());
                                pa.setParameter("nuTala", param.getNuTala());
                                pa.setParameter("nuArrastre", param.getNuArrastre());
                                pa.setParameter("nuOtra", param.getNuOtra());
                                pa.setParameter("descOtra", param.getDescOtra());
                                pa.setParameter("tipoPrograma", param.getTipoPrograma());
                                pa.setParameter("actividad", param.getActividad());
                                pa.setParameter("mitigacionAmbiental", param.getMitigacionAmbiental());
                                pa.setParameter("impactoGestion", param.getImpactoGestion());
                                pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                                pa.execute();
                                List<Object[]> spResult = pa.getResultList();
                                if (!spResult.isEmpty()) {
                                    ProteccionBosqueDetalleEntity obj = null;
                                    for (Object[] row : spResult) {
                                        obj = new ProteccionBosqueDetalleEntity();
                                        obj.setIdProBosqueDet((Integer) row[0]);
                                        obj.setCodPlanGeneralDet((String) row[1]);
                                        obj.setSubCodPlanGeneralDet((String) row[2]);
                                        obj.setTipoMarcacion((String) row[3]);
                                        obj.setNuAccion((Boolean) row[4]);
                                        obj.setFactorAmbiental((String) row[5]);
                                        obj.setImpacto((String) row[6]);
                                        obj.setNuCenso((Boolean) row[7]);
                                        obj.setNuDemarcacionLineal((Boolean) row[8]);
                                        obj.setNuConstruccionCampamento((Boolean) row[9]);
                                        obj.setNuConstruccionCamino((Boolean) row[10]);
                                        obj.setNuTala((Boolean) row[11]);
                                        obj.setNuArrastre((Boolean) row[12]);
                                        obj.setNuOtra((Boolean) row[13]);
                                        obj.setDescOtra((String) row[14]);
                                        obj.setTipoPrograma((Integer) row[15]);
                                        obj.setActividad((String) row[16]);
                                        obj.setMitigacionAmbiental((String) row[17]);
                                        obj.setImpactoGestion((String) row[18]);
                                        /********************************** ACTIVIDAD **********************************************/
                                        List<ProteccionBosqueDetalleActividadEntity> listDetAct = new ArrayList<>();
                                        for (ProteccionBosqueDetalleActividadEntity paramAc : param.getListProteccionBosqueActividad()) {
                                            StoredProcedureQuery pa3 = entityManager
                                                    .createStoredProcedureQuery("dbo.pa_ProteccionBosqueActividad_Registrar");
                                            pa3.registerStoredProcedureParameter("idProBosqueActividad", Integer.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("idProBosqueDet", Integer.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("codigo", String.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("descripcion", String.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("accion", Boolean.class,ParameterMode.IN);

                                            setStoreProcedureEnableNullParameters(pa3);
                                            pa3.setParameter("idProBosqueActividad", paramAc.getIdProBosqueActividad());
                                            pa3.setParameter("idProBosqueDet", obj.getIdProBosqueDet());
                                            pa3.setParameter("codigo", paramAc.getCodigo());
                                            pa3.setParameter("descripcion", paramAc.getDescripcion());
                                            pa3.setParameter("accion", paramAc.isAccion());
                                            pa3.execute();
                                            List<Object[]> spResult3 = pa3.getResultList();
                                            if (!spResult3.isEmpty()) {
                                                ProteccionBosqueDetalleActividadEntity obj3 = null;
                                                for (Object[] row3 : spResult3) {
                                                    obj3 = new ProteccionBosqueDetalleActividadEntity();
                                                    obj3.setIdProBosqueActividad((Integer) row3[0]);
                                                    obj3.setIdProBosqueDet((Integer) row3[1]);
                                                    obj3.setCodigo((String) row3[2]);
                                                    obj3.setDescripcion((String) row3[3]);
                                                    obj3.setAccion((Boolean) row3[4]);
                                                    listDetAct.add(obj3);
                                                }
                                            }
                                        }
                                        /********************************************************************************/
                                        obj.setListProteccionBosqueActividad(listDetAct);
                                        listDet.add(obj);
                                    }
                                }
                            }
                            obj_.setListProteccionBosque(listDet);
                            /***************************************** ANALISIS ********************************************************/
                            List<ProteccionBosqueDetalleEntity> listDetAnalisis = new ArrayList<>();
                            for (ProteccionBosqueDetalleEntity param : p.getListProteccionAnalisis()) {
                                StoredProcedureQuery pa = entityManager
                                        .createStoredProcedureQuery("dbo.pa_ProteccionBosqueDetalle_Registrar");
                                pa.registerStoredProcedureParameter("idProBosqueDet", Integer.class,ParameterMode.IN);
                                pa.registerStoredProcedureParameter("idProBosque", Integer.class,ParameterMode.IN);
                                pa.registerStoredProcedureParameter("codPlanGeneralDet", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("subCodPlanGeneralDet", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("tipoMarcacion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuAccion", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("implementacion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("factorAmbiental", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("impacto", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuCenso", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuDemarcacionLineal", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuConstruccionCampamento", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuConstruccionCamino", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuTala", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuArrastre", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuOtra", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("descOtra", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("tipoPrograma", Integer.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("mitigacionAmbiental", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("impactoGestion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                                setStoreProcedureEnableNullParameters(pa);
                                pa.setParameter("idProBosqueDet", param.getIdProBosqueDet());
                                pa.setParameter("idProBosque", obj_.getIdProBosque());
                                pa.setParameter("codPlanGeneralDet", param.getCodPlanGeneralDet());
                                pa.setParameter("subCodPlanGeneralDet", param.getSubCodPlanGeneralDet());
                                pa.setParameter("tipoMarcacion", param.getTipoMarcacion());
                                pa.setParameter("nuAccion", param.getNuAccion());
                                pa.setParameter("implementacion", param.getImplementacion());
                                pa.setParameter("factorAmbiental", param.getFactorAmbiental());
                                pa.setParameter("impacto", param.getImpacto());
                                pa.setParameter("nuCenso", param.getNuCenso());
                                pa.setParameter("nuDemarcacionLineal", param.getNuDemarcacionLineal());
                                pa.setParameter("nuConstruccionCampamento", param.getNuConstruccionCampamento());
                                pa.setParameter("nuConstruccionCamino", param.getNuConstruccionCamino());
                                pa.setParameter("nuTala", param.getNuTala());
                                pa.setParameter("nuArrastre", param.getNuArrastre());
                                pa.setParameter("nuOtra", param.getNuOtra());
                                pa.setParameter("descOtra", param.getDescOtra());
                                pa.setParameter("tipoPrograma", param.getTipoPrograma());
                                pa.setParameter("actividad", param.getActividad());
                                pa.setParameter("mitigacionAmbiental", param.getMitigacionAmbiental());
                                pa.setParameter("impactoGestion", param.getImpactoGestion());
                                pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                                pa.execute();
                                List<Object[]> spResult = pa.getResultList();
                                if (!spResult.isEmpty()) {
                                    ProteccionBosqueDetalleEntity obj = null;
                                    for (Object[] row : spResult) {
                                        obj = new ProteccionBosqueDetalleEntity();
                                        obj.setIdProBosqueDet((Integer) row[0]);
                                        obj.setCodPlanGeneralDet((String) row[1]);
                                        obj.setSubCodPlanGeneralDet((String) row[2]);
                                        obj.setTipoMarcacion((String) row[3]);
                                        obj.setNuAccion((Boolean) row[4]);
                                        obj.setFactorAmbiental((String) row[5]);
                                        obj.setImpacto((String) row[6]);
                                        obj.setNuCenso((Boolean) row[7]);
                                        obj.setNuDemarcacionLineal((Boolean) row[8]);
                                        obj.setNuConstruccionCampamento((Boolean) row[9]);
                                        obj.setNuConstruccionCamino((Boolean) row[10]);
                                        obj.setNuTala((Boolean) row[11]);
                                        obj.setNuArrastre((Boolean) row[12]);
                                        obj.setNuOtra((Boolean) row[13]);
                                        obj.setDescOtra((String) row[14]);
                                        obj.setTipoPrograma((Integer) row[15]);
                                        obj.setActividad((String) row[16]);
                                        obj.setMitigacionAmbiental((String) row[17]);
                                        obj.setImpactoGestion((String) row[18]);
                                        /********************************** ACTIVIDAD **********************************************/
                                        List<ProteccionBosqueDetalleActividadEntity> listDetActAnali = new ArrayList<>();
                                        for (ProteccionBosqueDetalleActividadEntity paramAc : param.getListProteccionBosqueActividad()) {
                                            StoredProcedureQuery pa3 = entityManager
                                                    .createStoredProcedureQuery("dbo.pa_ProteccionBosqueActividad_Registrar");
                                            pa3.registerStoredProcedureParameter("idProBosqueActividad", Integer.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("idProBosqueDet", Integer.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("codigo", String.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("descripcion", String.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("accion", Boolean.class,ParameterMode.IN);

                                            setStoreProcedureEnableNullParameters(pa3);
                                            pa3.setParameter("idProBosqueActividad", paramAc.getIdProBosqueDet());
                                            pa3.setParameter("idProBosqueDet", obj.getIdProBosqueDet());
                                            pa3.setParameter("codigo", paramAc.getCodigo());
                                            pa3.setParameter("descripcion", paramAc.getDescripcion());
                                            pa3.setParameter("accion", paramAc.isAccion());
                                            pa3.execute();
                                            List<Object[]> spResult3 = pa3.getResultList();
                                            if (!spResult3.isEmpty()) {
                                                ProteccionBosqueDetalleActividadEntity obj3 = null;
                                                for (Object[] row3 : spResult3) {
                                                    obj3 = new ProteccionBosqueDetalleActividadEntity();
                                                    obj3.setIdProBosqueActividad((Integer) row3[0]);
                                                    obj3.setIdProBosqueDet((Integer) row3[1]);
                                                    obj3.setCodigo((String) row3[2]);
                                                    obj3.setDescripcion((String) row3[3]);
                                                    obj3.setAccion((Boolean) row3[4]);
                                                    listDetActAnali.add(obj3);
                                                }
                                            }
                                        }
                                        /********************************************************************************/
                                        obj.setListProteccionBosqueActividad(listDetActAnali);
                                        listDetAnalisis.add(obj);
                                    }
                                }
                            }
                            obj_.setListProteccionAnalisis(listDetAnalisis);
                            /***************************************** GESTION ********************************************************/
                            List<ProteccionBosqueDetalleEntity> listDetGestion = new ArrayList<>();
                            for (ProteccionBosqueDetalleEntity param : p.getListProteccionGestion()) {
                                StoredProcedureQuery pa = entityManager
                                        .createStoredProcedureQuery("dbo.pa_ProteccionBosqueDetalle_Registrar");
                                pa.registerStoredProcedureParameter("idProBosqueDet", Integer.class,ParameterMode.IN);
                                pa.registerStoredProcedureParameter("idProBosque", Integer.class,ParameterMode.IN);
                                pa.registerStoredProcedureParameter("codPlanGeneralDet", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("subCodPlanGeneralDet", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("tipoMarcacion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuAccion", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("implementacion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("factorAmbiental", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("impacto", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuCenso", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuDemarcacionLineal", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuConstruccionCampamento", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuConstruccionCamino", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuTala", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuArrastre", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("nuOtra", Boolean.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("descOtra", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("tipoPrograma", Integer.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("mitigacionAmbiental", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("impactoGestion", String.class, ParameterMode.IN);
                                pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                                setStoreProcedureEnableNullParameters(pa);
                                pa.setParameter("idProBosqueDet", param.getIdProBosqueDet());
                                pa.setParameter("idProBosque", obj_.getIdProBosque());
                                pa.setParameter("codPlanGeneralDet", param.getCodPlanGeneralDet());
                                pa.setParameter("subCodPlanGeneralDet", param.getSubCodPlanGeneralDet());
                                pa.setParameter("tipoMarcacion", param.getTipoMarcacion());
                                pa.setParameter("nuAccion", param.getNuAccion());
                                pa.setParameter("implementacion", param.getImplementacion());
                                pa.setParameter("factorAmbiental", param.getFactorAmbiental());
                                pa.setParameter("impacto", param.getImpacto());
                                pa.setParameter("nuCenso", param.getNuCenso());
                                pa.setParameter("nuDemarcacionLineal", param.getNuDemarcacionLineal());
                                pa.setParameter("nuConstruccionCampamento", param.getNuConstruccionCampamento());
                                pa.setParameter("nuConstruccionCamino", param.getNuConstruccionCamino());
                                pa.setParameter("nuTala", param.getNuTala());
                                pa.setParameter("nuArrastre", param.getNuArrastre());
                                pa.setParameter("nuOtra", param.getNuOtra());
                                pa.setParameter("descOtra", param.getDescOtra());
                                pa.setParameter("tipoPrograma", param.getTipoPrograma());
                                pa.setParameter("actividad", param.getActividad());
                                pa.setParameter("mitigacionAmbiental", param.getMitigacionAmbiental());
                                pa.setParameter("impactoGestion", param.getImpactoGestion());
                                pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                                pa.execute();
                                List<Object[]> spResult = pa.getResultList();
                                if (!spResult.isEmpty()) {
                                    ProteccionBosqueDetalleEntity obj = null;
                                    for (Object[] row : spResult) {
                                        obj = new ProteccionBosqueDetalleEntity();
                                        obj.setIdProBosqueDet((Integer) row[0]);
                                        obj.setCodPlanGeneralDet((String) row[1]);
                                        obj.setSubCodPlanGeneralDet((String) row[2]);
                                        obj.setTipoMarcacion((String) row[3]);
                                        obj.setNuAccion((Boolean) row[4]);
                                        obj.setFactorAmbiental((String) row[5]);
                                        obj.setImpacto((String) row[6]);
                                        obj.setNuCenso((Boolean) row[7]);
                                        obj.setNuDemarcacionLineal((Boolean) row[8]);
                                        obj.setNuConstruccionCampamento((Boolean) row[9]);
                                        obj.setNuConstruccionCamino((Boolean) row[10]);
                                        obj.setNuTala((Boolean) row[11]);
                                        obj.setNuArrastre((Boolean) row[12]);
                                        obj.setNuOtra((Boolean) row[13]);
                                        obj.setDescOtra((String) row[14]);
                                        obj.setTipoPrograma((Integer) row[15]);
                                        obj.setActividad((String) row[16]);
                                        obj.setMitigacionAmbiental((String) row[17]);
                                        obj.setImpactoGestion((String) row[18]);
                                        /********************************** ACTIVIDAD **********************************************/
                                        List<ProteccionBosqueDetalleActividadEntity> listDetActGestion = new ArrayList<>();
                                        for (ProteccionBosqueDetalleActividadEntity paramAc : param.getListProteccionBosqueActividad()) {
                                            StoredProcedureQuery pa3 = entityManager
                                                    .createStoredProcedureQuery("dbo.pa_ProteccionBosqueActividad_Registrar");
                                            pa3.registerStoredProcedureParameter("idProBosqueActividad", Integer.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("idProBosqueDet", Integer.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("codigo", String.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("descripcion", String.class,ParameterMode.IN);
                                            pa3.registerStoredProcedureParameter("accion", Boolean.class,ParameterMode.IN);

                                            setStoreProcedureEnableNullParameters(pa3);
                                            pa3.setParameter("idProBosqueActividad", paramAc.getIdProBosqueDet());
                                            pa3.setParameter("idProBosqueDet", obj.getIdProBosqueDet());
                                            pa3.setParameter("codigo", paramAc.getCodigo());
                                            pa3.setParameter("descripcion", paramAc.getDescripcion());
                                            pa3.setParameter("accion", paramAc.isAccion());
                                            pa3.execute();
                                            List<Object[]> spResult3 = pa3.getResultList();
                                            if (!spResult3.isEmpty()) {
                                                ProteccionBosqueDetalleActividadEntity obj3 = null;
                                                for (Object[] row3 : spResult3) {
                                                    obj3 = new ProteccionBosqueDetalleActividadEntity();
                                                    obj3.setIdProBosqueActividad((Integer) row3[0]);
                                                    obj3.setIdProBosqueDet((Integer) row3[1]);
                                                    obj3.setCodigo((String) row3[2]);
                                                    obj3.setDescripcion((String) row3[3]);
                                                    obj3.setAccion((Boolean) row3[4]);
                                                    listDetActGestion.add(obj3);
                                                }
                                            }
                                        }
                                        /********************************************************************************/
                                        obj.setListProteccionBosqueActividad(listDetActGestion);
                                        listDetGestion.add(obj);
                                    }
                                }
                            }
                            obj_.setListProteccionGestion(listDet);
                            list_.add(obj_);
                        }
                    }
                }
                result.setData(list_);
                result.setSuccess(true);
                result.setMessage("Se registró la proteccion del bosque.");
                return result;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return result;
            }
        }


        /**
         * @autor:  Rafael Azaña [15-10-2021]
         * @descripción: {Listar ProteccionBosque}
         * @param: ProteccionBosqueEntity
         * @return: ResponseEntity<ResponseVO>
         */

        @Override
        public List<ProteccionBosqueDetalleDto> listarProteccionBosque(Integer idPlanManejo, String codPlanGeneral, String subCodPlanGeneral) throws Exception {
            List<ProteccionBosqueDetalleDto> lista = new ArrayList<ProteccionBosqueDetalleDto>();
            try {
                int idDetalle=0;
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueDetalle_Listar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codPlanGeneral", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("subCodPlanGeneral", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPlanManejo", idPlanManejo);
                processStored.setParameter("codPlanGeneral", codPlanGeneral);
                processStored.setParameter("subCodPlanGeneral", subCodPlanGeneral);
                processStored.execute();

                List<Object[]> spResult = processStored.getResultList();
                if(spResult!=null){

                    if (spResult.size() >= 1){
                        for (Object[] row : spResult) {

                            ProteccionBosqueDetalleDto temp = new ProteccionBosqueDetalleDto();
                            temp.setIdProBosque((Integer) row[0]);
                            temp.setIdPlanManejo((Integer) row[1]);
                            temp.setCodPlanGeneral((String) row[2]);
                            temp.setSubCodPlanGeneral((String) row[3]);
                            temp.setDescripcion((String) row[4]);
                            temp.setIdProBosqueDet((Integer) row[5]);
                            idDetalle=(Integer) row[5];
                            temp.setCodPlanGeneralDet((String) row[6]);
                            temp.setSubCodPlanGeneralDet((String) row[7]);
                            temp.setTipoMarcacion((String) row[8]);
                            temp.setNuAccion((Boolean) row[9]);
                            temp.setFactorAmbiental((String) row[10]);
                            temp.setImpacto((String) row[11]);
                            temp.setNuCenso((Boolean) row[12]);
                            temp.setNuDemarcacionLineal((Boolean) row[13]);
                            temp.setNuConstruccionCampamento((Boolean) row[14]);
                            temp.setNuConstruccionCamino((Boolean) row[15]);
                            temp.setNuTala((Boolean) row[16]);
                            temp.setNuArrastre((Boolean) row[17]);
                            temp.setNuOtra((Boolean) row[18]);
                            temp.setDescOtra((String) row[19]);
                            temp.setTipoPrograma((Integer) row[20]);
                            temp.setActividad((String) row[21]);
                            temp.setMitigacionAmbiental((String) row[22]);
                            temp.setImpactoGestion((String) row[23]);
                            temp.setImplementacion((String) row[24]);
                            /********************************** ACTIVIDAD **********************************************/
                            List<ProteccionBosqueDetalleActividadEntity> listDetActividad = new ArrayList<ProteccionBosqueDetalleActividadEntity>();
                            StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_ProteccionBosqueActividad_Listar");
                            pa.registerStoredProcedureParameter("idProBosqueDet", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(pa);
                            pa.setParameter("idProBosqueDet", idDetalle);
                            pa.execute();
                            List<Object[]> spResultAct = pa.getResultList();
                            if(spResultAct!=null) {
                                if (spResultAct.size() >= 1) {
                                    for (Object[] rowAct : spResultAct) {
                                        ProteccionBosqueDetalleActividadEntity temp2 = new ProteccionBosqueDetalleActividadEntity();
                                        temp2.setIdProBosqueActividad((Integer) rowAct[0]);
                                        temp2.setIdProBosqueDet((Integer) rowAct[1]);
                                        temp2.setCodigo((String) rowAct[2]);
                                        temp2.setDescripcion((String) rowAct[3]);
                                        temp2.setAccion((Boolean) rowAct[4]);
                                        listDetActividad.add(temp2);
                                    }
                                }
                            }
                            /********************************************************************************/
                            temp.setListProteccionBosqueActividad(listDetActividad);
                            lista.add(temp);
                        }
                    }
                }
                return lista;
            } catch (Exception e) {
                log.error("listarProteccionBosque", e.getMessage());
                throw e;
            }
        }



        @Override
        public ResultClassEntity EliminarProteccionBosque(ProteccionBosqueDetalleDto param) throws Exception {
            ResultClassEntity result = new ResultClassEntity();
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("[dbo].[pa_ProteccionBosque_Eliminar]");
            processStored.registerStoredProcedureParameter("idProBosque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idProBosqueDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);


            processStored.setParameter("idProBosque", param.getIdProBosque());
            processStored.setParameter("idProBosqueDet", param.getIdProBosqueDet());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

            processStored.execute();

            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return result;
        }


        public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
            if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
                return;

            for (Parameter parameter : storedProcedureQuery.getParameters()) {
                ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
            }

        }

    }
