package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;
import pe.gob.serfor.mcsniffs.repository.MesaPartesDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class MesaPartesDetalleRepositoryImpl extends JdbcDaoSupport implements MesaPartesDetalleRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(MesaPartesRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<MesaPartesDetalleDto> listarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception {
        
        List<MesaPartesDetalleDto> lista = new ArrayList<MesaPartesDetalleDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartesDetalle_Listar");
            processStored.registerStoredProcedureParameter("idMesaPartesDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMesaPartes", Integer.class, ParameterMode.IN); 
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaTramite", Timestamp.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTramite", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idMesaPartesDet",dto.getIdMesaPartesDet());
            processStored.setParameter("idMesaPartes",dto.getIdMesaPartes()); 
            processStored.setParameter("codigo",dto.getCodigo());
            processStored.setParameter("fechaTramite",dto.getFecha());
            processStored.setParameter("nroTramite",dto.getDetalle());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                MesaPartesDetalleDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new MesaPartesDetalleDto();
                        temp.setIdMesaPartesDet(row[0]==null?null:(Integer) row[0]);
                        temp.setCodigo(row[1]==null?null:(String) row[1]);
                        temp.setConforme(row[2]==null?null:(String) row[2]);
                        temp.setIdArchivo(row[3]==null?null:(Integer) row[3]);                        
                        temp.setRequisito(row[4]==null?null:(String) row[4]);
                        temp.setDetalle(row[5]==null?null:(String) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setDescripcion(row[7]==null?null:(String) row[7]);
                        temp.setIdMesaPartes(row[8]==null?null:(Integer) row[8]);
                        temp.setEstado(row[9]==null?null:(String) row[9]);

                        temp.setFecha(row[10]==null?null:(Timestamp) row[10]);
                        temp.setTipoRequisito(row[11]==null?null:(String) row[11]);

                        temp.setArchivo(row[12]==null?null:(String) row[12]);

                        temp.setJustificacion(row[14]==null?null:(String) row[14]);
                        temp.setComentario(row[13]==null?null:(String) row[13]);

                        temp.setFechaDocumento(row[15]==null?null:(Timestamp) row[15]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarProcedimientoAdministrativo", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity registrarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartesDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idMesaPartesDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("requisito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMesaPartes", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("fecha", Timestamp.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoRequisito", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("archivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("comentario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaDocumento", Timestamp.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMesaPartesDet", dto.getIdMesaPartesDet());
            processStored.setParameter("codigo", dto.getCodigo());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("requisito", dto.getRequisito());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("idMesaPartes", dto.getIdMesaPartes());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.setParameter("fecha", dto.getFecha());
            processStored.setParameter("tipoRequisito", dto.getTipoRequisito());

            processStored.setParameter("archivo", dto.getArchivo());
            processStored.setParameter("justificacion", dto.getJustificacion());
            processStored.setParameter("comentario", dto.getComentario());
            processStored.setParameter("fechaDocumento", dto.getFechaDocumento());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Mesa de partes validó los requisitos TUPA correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarMesaPartesDetalle(MesaPartesDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MesaPartesDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idMesaPartesDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMesaPartesDet", dto.getIdMesaPartesDet());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó mesa partes detalle correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }    
 
}
