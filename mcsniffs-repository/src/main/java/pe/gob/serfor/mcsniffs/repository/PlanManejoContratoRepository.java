package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PlanManejoContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface PlanManejoContratoRepository {
    ResultClassEntity obtenerPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception;
    ResultClassEntity RegistrarPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception;
    ResultClassEntity EliminarPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception;
    ResultClassEntity listarPorFiltroPlanManejoContrato(String nroDocumento, String codTipoPlan) throws Exception;
}
