package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.MunicipioPersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.MunicipioPersonaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class MunicipioPersonaRepositoryImpl extends JdbcDaoSupport implements MunicipioPersonaRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(MunicipioPersonaRepositoryImpl.class);

    @Override
    public ResultClassEntity listarMunicipioPersona(MunicipioPersonaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<MunicipioPersonaEntity> lista = new ArrayList<MunicipioPersonaEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Municipio_Listar");


            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idDistrito",obj.getIdDistrito());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                MunicipioPersonaEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new MunicipioPersonaEntity();
                        c.setIdMunicipio(row[0]==null?null:(Integer) row[0]);
                        c.setIdDistrito(row[1]==null?null:(Integer) row[1]);
                        c.setIdPersona(row[2]==null?null:(Integer) row[2]);
                        c.setNumeroDocumento(row[3]==null?null:(String) row[3]);
                        c.setNombrePersona(row[4]==null?null:(String) row[4]);

                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar municipio correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar municipio");
            return  result;
        }
    }

   

}