package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.PagoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudSANRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class PagoRepositoryImpl extends JdbcDaoSupport implements PagoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(PagoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ArchivoRepository fileUp;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarPago(List<PagoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PagoEntity> list_ = new ArrayList<>();
        try {
            for (PagoEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Pago_Registrar");
                sp.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codTipoPago", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTH", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("modalidadTH", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("vigencia", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoPersona", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nombreTitular", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("paternoTitular", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("maternoTitular", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoDocumentoRepresentante", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroDocumentoRepresentante", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nombreRepresentante", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("paternoRepresentante", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("maternoRepresentante", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("mecanismoPago", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("areHA", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("montoPagoHA", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoMoneda", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("montoPagoAnual", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("montoPagoAnualCalculado", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nrDocumentoGestion", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("conforme", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idPago", p.getIdPago());
                sp.setParameter("codTipoPago", p.getCodTipoPago());
                sp.setParameter("codigoTH", p.getCodigoTH());
                sp.setParameter("modalidadTH", p.getModalidadTH());
                sp.setParameter("fechaInicio", p.getFechaInicio());
                sp.setParameter("fechaFin", p.getFechaFin());
                sp.setParameter("vigencia", p.getVigencia());
                sp.setParameter("tipoPersona", p.getTipoPersona());
                sp.setParameter("tipoDocumento", p.getTipoDocumento());
                sp.setParameter("nroDocumento", p.getNroDocumento());
                sp.setParameter("nombreTitular", p.getNombreTitular());
                sp.setParameter("paternoTitular", p.getPaternoTitular());
                sp.setParameter("maternoTitular", p.getMaternoTitular());
                sp.setParameter("tipoDocumentoRepresentante", p.getTipoDocumentoRepresentante());
                sp.setParameter("nroDocumentoRepresentante", p.getNroDocumentoRepresentante());
                sp.setParameter("nombreRepresentante", p.getNombreRepresentante());
                sp.setParameter("paternoRepresentante", p.getPaternoRepresentante());
                sp.setParameter("maternoRepresentante", p.getMaternoRepresentante());
                sp.setParameter("ubigeo", p.getUbigeo());
                sp.setParameter("mecanismoPago", p.getMecanismoPago());
                sp.setParameter("areHA", p.getAreHA());
                sp.setParameter("montoPagoHA", p.getMontoPagoHA());
                sp.setParameter("tipoMoneda", p.getTipoMoneda());
                sp.setParameter("montoPagoAnual", p.getMontoPagoAnual());
                sp.setParameter("montoPagoAnualCalculado", p.getMontoPagoAnualCalculado());
                sp.setParameter("accion", p.getAccion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("nrDocumentoGestion", p.getNrDocumentoGestion());
                sp.setParameter("tipoDocumentoGestion", p.getTipoDocumentoGestion());
                sp.setParameter("codigoEstado", p.getCodigoEstado());
                sp.setParameter("conforme", p.getConforme());
                sp.setParameter("perfil", p.getPerfil());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        PagoEntity obj_ = new PagoEntity();
                        obj_.setIdPago(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodTipoPago(row_[1]==null?null:(String) row_[1]);
                        obj_.setCodigoTH(row_[2]==null?null:(String) row_[2]);
                        obj_.setModalidadTH(row_[3]==null?null:(String) row_[3]);
                        obj_.setFechaInicio(row_[4]==null?null:(Date) row_[4]);
                        obj_.setFechaFin(row_[5]==null?null:(Date) row_[5]);
                        obj_.setVigencia(row_[6]==null?null:(Integer) row_[6]);
                        obj_.setTipoDocumento(row_[7]==null?null:(String) row_[7]);
                        obj_.setNroDocumento(row_[8]==null?null:(String) row_[8]);
                        obj_.setNombreTitular(row_[9]==null?null:(String) row_[9]);
                        obj_.setPaternoTitular(row_[10]==null?null:(String) row_[10]);
                        obj_.setMaternoTitular(row_[11]==null?null:(String) row_[11]);
                        obj_.setUbigeo(row_[12]==null?null:(String) row_[12]);
                        obj_.setMecanismoPago(row_[13]==null?null:(String) row_[13]);
                        obj_.setAreHA(row_[14]==null?null:(BigDecimal) row_[14]);
                        obj_.setMontoPagoHA(row_[15]==null?null:(BigDecimal) row_[15]);
                        obj_.setTipoMoneda(row_[16]==null?null:(String) row_[16]);
                        obj_.setMontoPagoAnual(row_[17]==null?null:(BigDecimal) row_[17]);
                        obj_.setMontoPagoAnualCalculado(row_[18]==null?null:(BigDecimal) row_[18]);
                        obj_.setAccion(row_[19]==null?null:(Boolean) row_[19]);
                        obj_.setObservacion(row_[20]==null?null:(String) row_[20]);
                        obj_.setDetalle(row_[21]==null?null:(String) row_[21]);
                        obj_.setDescripcion(row_[22]==null?null:(String) row_[22]);
                        obj_.setNrDocumentoGestion(row_[23]==null?null:(Integer) row_[23]);
                        obj_.setTipoDocumentoGestion(row_[24]==null?null:(String) row_[24]);
                        obj_.setCodigoEstado(row_[25]==null?null:(String) row_[25]);
                        obj_.setTipoPersona(row_[26]==null?null:(String) row_[26]);
                        obj_.setTipoDocumentoRepresentante(row_[27]==null?null:(String) row_[27]);
                        obj_.setNroDocumentoRepresentante(row_[28]==null?null:(String) row_[28]);
                        obj_.setNombreRepresentante(row_[29]==null?null:(String) row_[29]);
                        obj_.setPaternoRepresentante(row_[30]==null?null:(String) row_[30]);
                        obj_.setMaternoRepresentante(row_[31]==null?null:(String) row_[31]);
                        obj_.setConforme(row_[32]==null?null:(String) row_[32]);
                        obj_.setPerfil(row_[33]==null?null:(String) row_[33]);

                        /********************************** Cronograma **********************************************/
                        List<PagoCronogramaEntity> listDet = new ArrayList<>();
                        for (PagoCronogramaEntity param : p.getListaCronograma()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_PagoCronograma_Registrar");
                            pa.registerStoredProcedureParameter("idPagoCronograma", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codTipoPagoCronograma", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(pa);
                            pa.setParameter("idPagoCronograma", param.getIdPagoCronograma());
                            pa.setParameter("codTipoPagoCronograma", param.getCodTipoPagoCronograma());
                            pa.setParameter("fechaInicio", param.getFechaInicio());
                            pa.setParameter("fechaFin", param.getFechaFin());
                            pa.setParameter("anio", param.getAnio());
                            pa.setParameter("perfil", param.getPerfil());
                            pa.setParameter("observacion", param.getObservacion());
                            pa.setParameter("detalle", param.getDetalle());
                            pa.setParameter("descripcion", param.getDescripcion());
                            pa.setParameter("idPago", obj_.getIdPago());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                PagoCronogramaEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new PagoCronogramaEntity();
                                    obj.setIdPagoCronograma(row[0]==null?null:(Integer) row[0]);
                                    obj.setCodTipoPagoCronograma(row[1]==null?null:(String) row[1]);
                                    obj.setFechaInicio(row[2]==null?null:(Date) row[2]);
                                    obj.setFechaFin(row[3]==null?null:(Date) row[3]);
                                    obj.setAnio(row[4]==null?null:(Integer) row[4]);
                                    obj.setObservacion(row[5]==null?null:(String) row[5]);
                                    obj.setDetalle(row[6]==null?null:(String) row[6]);
                                    obj.setDescripcion(row[7]==null?null:(String) row[7]);
                                    obj.setPerfil(row[8]==null?null:(String) row[8]);

                                    /********************************** PERIODO **********************************************/
                                    List<PagoPeriodoEntity> listDetAct = new ArrayList<>();
                                    for (PagoPeriodoEntity paramAc : param.getListaPeriodo()) {
                                        StoredProcedureQuery pa3 = entityManager.createStoredProcedureQuery("dbo.pa_PagoPeriodo_Registrar");
                                        pa3.registerStoredProcedureParameter("idPagoPeriodo", Integer.class,ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("codTipoPagoPeriodo", String.class,ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("fechaLimite", Date.class,ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("monto", BigDecimal.class,ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("montoFinal", BigDecimal.class,ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("balancePago", BigDecimal.class,ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("fechaComprobante", Date.class, ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("idPagoCronograma", Integer.class, ParameterMode.IN);
                                        pa3.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                                        SpUtil.enableNullParams(pa3);
                                        pa3.setParameter("idPagoPeriodo", paramAc.getIdPagoPeriodo());
                                        pa3.setParameter("codTipoPagoPeriodo", paramAc.getCodTipoPagoPeriodo());
                                        pa3.setParameter("fechaLimite", paramAc.getFechaLimite());
                                        pa3.setParameter("monto", paramAc.getMonto());
                                        pa3.setParameter("montoFinal", paramAc.getMontoFinal());
                                        pa3.setParameter("balancePago", paramAc.getBalancePago());
                                        pa3.setParameter("perfil", paramAc.getPerfil());
                                        pa3.setParameter("observacion", paramAc.getObservacion());
                                        pa3.setParameter("detalle", paramAc.getDetalle());
                                        pa3.setParameter("descripcion", paramAc.getDescripcion());
                                        pa3.setParameter("fechaComprobante", paramAc.getFechaComprobante());
                                        pa3.setParameter("idPagoCronograma", obj.getIdPagoCronograma());
                                        pa3.setParameter("idUsuarioRegistro", paramAc.getIdUsuarioRegistro());
                                        pa3.execute();
                                        List<Object[]> spResult3 = pa3.getResultList();
                                        if (!spResult3.isEmpty()) {
                                            PagoPeriodoEntity obj3 = null;
                                            for (Object[] row3 : spResult3) {
                                                obj3 = new PagoPeriodoEntity();
                                                obj3.setIdPagoPeriodo(row3[0]==null?null:(Integer) row3[0]);
                                                obj3.setCodTipoPagoPeriodo(row3[1]==null?null:(String) row3[1]);
                                                obj3.setFechaLimite(row3[2]==null?null:(Date) row3[2]);
                                                obj3.setMonto(row3[3]==null?null:(BigDecimal) row3[3]);
                                                obj3.setMontoFinal(row3[4]==null?null:(BigDecimal) row3[4]);
                                                obj3.setBalancePago(row3[5]==null?null:(BigDecimal) row3[5]);
                                                obj3.setObservacion(row3[6]==null?null:(String) row3[6]);
                                                obj3.setDetalle(row3[7]==null?null:(String) row3[7]);
                                                obj3.setDescripcion(row3[8]==null?null:(String) row3[8]);
                                                obj3.setFechaComprobante(row3[9]==null?null:(Date) row3[9]);
                                                obj3.setPerfil(row3[10]==null?null:(String) row3[10]);
                                                listDetAct.add(obj3);

                                            }
                                        }
                                    }
                                    /********************************************************************************/
                                    obj.setListaPeriodo(listDetAct);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListaCronograma(listDet);



                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la Solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }






    @Override
    public ResultEntity ListarPago(PagoEntity param)  {
        ResultEntity resulFinal=new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Pago_Listar");
            processStored.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTH", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("mecanismoPago", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroGestion", param.getNrDocumentoGestion());
            processStored.setParameter("idPago", param.getIdPago());
            processStored.setParameter("codigoTH", param.getCodigoTH());
            processStored.setParameter("mecanismoPago", param.getMecanismoPago());
            processStored.setParameter("nroDocumento", param.getNroDocumento());
            processStored.setParameter("codigoEstado", param.getCodigoEstado());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());
            String estadoPlan="";
            int idPago=0;
            int idPagoCronograma=0;
            processStored.execute();
            List<PagoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                PagoEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new PagoEntity();
                    idPago=row_[0]==null?0:(Integer) row_[0];
                    obj_.setIdPago(row_[0]==null?null:(Integer) row_[0]);
                    obj_.setCodTipoPago(row_[1]==null?null:(String) row_[1]);
                    obj_.setCodigoTH(row_[2]==null?null:(String) row_[2]);
                    obj_.setModalidadTH(row_[3]==null?null:(String) row_[3]);
                    obj_.setFechaInicio(row_[4]==null?null:(Date) row_[4]);
                    obj_.setFechaFin(row_[5]==null?null:(Date) row_[5]);
                    obj_.setVigencia(row_[6]==null?null:(Integer) row_[6]);
                    obj_.setTipoDocumento(row_[7]==null?null:(String) row_[7]);
                    obj_.setNroDocumento(row_[8]==null?null:(String) row_[8]);
                    obj_.setNombreTitular(row_[9]==null?null:(String) row_[9]);
                    obj_.setPaternoTitular(row_[10]==null?null:(String) row_[10]);
                    obj_.setMaternoTitular(row_[11]==null?null:(String) row_[11]);
                    obj_.setUbigeo(row_[12]==null?null:(String) row_[12]);
                    obj_.setMecanismoPago(row_[13]==null?null:(String) row_[13]);
                    obj_.setAreHA(row_[14]==null?null:(BigDecimal) row_[14]);
                    obj_.setMontoPagoHA(row_[15]==null?null:(BigDecimal) row_[15]);
                    obj_.setTipoMoneda(row_[16]==null?null:(String) row_[16]);
                    obj_.setMontoPagoAnual(row_[17]==null?null:(BigDecimal) row_[17]);
                    obj_.setMontoPagoAnualCalculado(row_[18]==null?null:(BigDecimal) row_[18]);
                    obj_.setAccion(row_[19]==null?null:(Boolean) row_[19]);
                    obj_.setObservacion(row_[20]==null?null:(String) row_[20]);
                    obj_.setDetalle(row_[21]==null?null:(String) row_[21]);
                    obj_.setDescripcion(row_[22]==null?null:(String) row_[22]);
                    obj_.setNrDocumentoGestion(row_[23]==null?null:(Integer) row_[23]);
                    obj_.setTipoDocumentoGestion(row_[24]==null?null:(String) row_[24]);
                    obj_.setCodigoEstado(row_[25]==null?null:(String) row_[25]);
                    obj_.setCodigoEstadoText(row_[26]==null?null:(String) row_[26]);
                    obj_.setTipoPersona(row_[27]==null?null:(String) row_[27]);
                    obj_.setTipoDocumentoRepresentante(row_[28]==null?null:(String) row_[28]);
                    obj_.setNroDocumentoRepresentante(row_[29]==null?null:(String) row_[29]);
                    obj_.setNombreRepresentante(row_[30]==null?null:(String) row_[30]);
                    obj_.setPaternoRepresentante(row_[31]==null?null:(String) row_[31]);
                    obj_.setMaternoRepresentante(row_[32]==null?null:(String) row_[32]);
                    resulFinal.setTotalrecord(row_[33]==null?0:(Integer) row_[33]);
                    obj_.setFechaRegistro(row_[34]==null?null:(Date) row_[34]);
                    obj_.setConforme(row_[35]==null?null:(String) row_[35]);
                    obj_.setPerfil(row_[36]==null?null:(String) row_[36]);
                    obj_.setIdUsuarioRegistro(row_[37]==null?null:(Integer) row_[37]);
                    obj_.setIdUsuarioModificacion(row_[38]==null?null:(Integer) row_[38]);
                    obj_.setIdUsuarioElimina(row_[39]==null?null:(Integer) row_[39]);

                    /********************************** CRONGORAMA **********************************************/
                    List<PagoCronogramaEntity> listDetCronograma = new ArrayList<PagoCronogramaEntity>();
                    StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_PagoCronograma_Listar");
                    pa.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(pa);
                    if( idPago==0){pa.setParameter("idPago", null); }
                    else{pa.setParameter("idPago", idPago);}
                    pa.execute();
                    List<Object[]> spResultAct = pa.getResultList();
                    if(spResultAct!=null) {
                        if (spResultAct.size() >= 1) {
                            for (Object[] row : spResultAct) {
                                PagoCronogramaEntity obj = new PagoCronogramaEntity();
                                idPagoCronograma=row[0]==null?0:(Integer) row[0];
                                obj.setIdPagoCronograma(row[0]==null?null:(Integer) row[0]);
                                obj.setIdPago(row[1]==null?null:(Integer) row[1]);
                                obj.setCodTipoPagoCronograma(row[2]==null?null:(String) row[2]);
                                obj.setFechaInicio(row[3]==null?null:(Date) row[3]);
                                obj.setFechaFin(row[4]==null?null:(Date) row[4]);
                                obj.setAnio(row[5]==null?null:(Integer) row[5]);
                                obj.setObservacion(row[6]==null?null:(String) row[6]);
                                obj.setDetalle(row[7]==null?null:(String) row[7]);
                                obj.setDescripcion(row[8]==null?null:(String) row[8]);
                                obj.setIdUsuarioRegistro(row[9]==null?null:(Integer) row[9]);
                                obj.setIdUsuarioModificacion(row[10]==null?null:(Integer) row[10]);
                                obj.setIdUsuarioElimina(row[11]==null?null:(Integer) row[11]);
                                obj.setPerfil(row[12]==null?null:(String) row[12]);
                                listDetCronograma.add(obj);
                                /********************************** Periodo **********************************************/
                                List<PagoPeriodoEntity> listDetPeriodo = new ArrayList<PagoPeriodoEntity>();
                                StoredProcedureQuery pa3 = entityManager.createStoredProcedureQuery("dbo.pa_PagoPeriodo_Listar");
                                pa3.registerStoredProcedureParameter("idPagoCronograma", Integer.class, ParameterMode.IN);
                                SpUtil.enableNullParams(pa3);
                                if( idPagoCronograma==0){pa3.setParameter("idPagoCronograma", null); }
                                else{pa3.setParameter("idPagoCronograma", idPagoCronograma);}
                                pa3.execute();
                                List<Object[]> spResultPeriodo = pa3.getResultList();
                                if(spResultPeriodo!=null) {
                                    if (spResultPeriodo.size() >= 1) {
                                        for (Object[] row3 : spResultPeriodo) {
                                            PagoPeriodoEntity obj3 = new PagoPeriodoEntity();
                                            obj3.setIdPagoPeriodo(row3[0]==null?null:(Integer) row3[0]);
                                            obj3.setIdPagoCronograma(row3[1]==null?null:(Integer) row3[1]);
                                            obj3.setCodTipoPagoPeriodo(row3[2]==null?null:(String) row3[2]);
                                            obj3.setFechaLimite(row3[3]==null?null:(Date) row3[3]);
                                            obj3.setMonto(row3[4]==null?null:(BigDecimal) row3[4]);
                                            obj3.setMontoFinal(row3[5]==null?null:(BigDecimal) row3[5]);
                                            obj3.setBalancePago(row3[6]==null?null:(BigDecimal) row3[6]);
                                            obj3.setObservacion(row3[7]==null?null:(String) row3[7]);
                                            obj3.setDetalle(row3[8]==null?null:(String) row3[8]);
                                            obj3.setDescripcion(row3[9]==null?null:(String) row3[9]);
                                            obj3.setFechaComprobante(row3[10]==null?null:(Date) row3[10]);
                                            obj3.setIdUsuarioRegistro(row3[11]==null?null:(Integer) row3[11]);
                                            obj3.setIdUsuarioModificacion(row3[12]==null?null:(Integer) row3[12]);
                                            obj3.setIdUsuarioElimina(row3[13]==null?null:(Integer) row3[13]);
                                            obj3.setPerfil(row3[14]==null?null:(String) row3[14]);
                                            listDetPeriodo.add(obj3);

                                        }
                                    }
                                }
                                /********************************************************************************/
                                obj.setListaPeriodo(listDetPeriodo);
                            }
                        }
                    }
                    /********************************************************************************/
                    obj_.setListaCronograma(listDetCronograma);


                    result.add(obj_);
                }
            }
            resulFinal.setData(result);
            resulFinal.setMessage((result.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resulFinal.setIsSuccess(true);
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resulFinal.setMessage(e.getMessage());
            resulFinal.setIsSuccess(false);
        }
        return resulFinal;
    }

    @Override
    public ResultClassEntity EliminarPago(PagoPeriodoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Pago_Eliminar");
        processStored.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPagoCronograma", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPagoPeriodo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPago", param.getIdPago());
        processStored.setParameter("idPagoCronograma", param.getIdPagoCronograma());
        processStored.setParameter("idPagoPeriodo", param.getIdPagoPeriodo());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }


    @Override
    public ResultClassEntity RegistrarDescuento(List<DescuentoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<DescuentoEntity> list_ = new ArrayList<>();
        try {
            for (DescuentoEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PagoDescuento_Registrar");
                sp.registerStoredProcedureParameter("idDescuento", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoDescuento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descDescuento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("porcentaje", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idDescuento", p.getIdDescuento());
                sp.setParameter("codigoDescuento", p.getCodigoDescuento());
                sp.setParameter("descDescuento", p.getDescDescuento());
                sp.setParameter("porcentaje", p.getPorcentaje());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        DescuentoEntity obj_ = new DescuentoEntity();
                        obj_.setIdDescuento(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodigoDescuento(row_[1]==null?null:(String) row_[1]);
                        obj_.setDescDescuento(row_[2]==null?null:(String) row_[2]);
                        obj_.setPorcentaje(row_[3]==null?null:(BigDecimal) row_[3]);
                        obj_.setObservacion(row_[4]==null?null:(String) row_[4]);
                        obj_.setDetalle(row_[5]==null?null:(String) row_[5]);
                        obj_.setDescripcion(row_[6]==null?null:(String) row_[6]);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el descuento.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }



    @Override
    public List<DescuentoEntity> ListarDescuento(DescuentoEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoDescuento_Listar");
            processStored.registerStoredProcedureParameter("idDescuento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idDescuento", param.getIdDescuento());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());
            processStored.execute();
            List<DescuentoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                DescuentoEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new DescuentoEntity();
                    obj_.setIdDescuento(row_[0]==null?null:(Integer) row_[0]);
                    obj_.setCodigoDescuento(row_[1]==null?null:(String) row_[1]);
                    obj_.setDescDescuento(row_[2]==null?null:(String) row_[2]);
                    obj_.setPorcentaje(row_[3]==null?null:(BigDecimal) row_[3]);
                    obj_.setObservacion(row_[4]==null?null:(String) row_[4]);
                    obj_.setDetalle(row_[5]==null?null:(String) row_[5]);
                    obj_.setDescripcion(row_[6]==null?null:(String) row_[6]);
                    result.add(obj_);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity EliminarDescuento(DescuentoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoDescuento_Eliminar");
        processStored.registerStoredProcedureParameter("idDescuento", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idDescuento", param.getIdDescuento());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }


    @Override
    public ResultEntity listarTH(THEntity param)  {
        ResultEntity resulFinal=new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TH_Listar");
            processStored.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTH", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroGestion", param.getNroGestion());
            processStored.setParameter("codigoTH", param.getCodigoTH());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());
            processStored.execute();
            List<THEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                THEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new THEntity();
                    obj_.setCodigoTH(row_[0]==null?null:(String) row_[0]);
                    obj_.setNombreTH(row_[1]==null?null:(String) row_[1]);
                    obj_.setPaternoTH(row_[2]==null?null:(String) row_[2]);
                    obj_.setMaternoTH(row_[3]==null?null:(String) row_[3]);
                    obj_.setTipoTH(row_[4]==null?null:(String) row_[4]);
                    obj_.setFechaInicio(row_[5]==null?null:(Date) row_[5]);
                    obj_.setUbigeo(row_[6]==null?null:(String) row_[6]);
                    obj_.setNroGestion(row_[7]==null?null:(Integer) row_[7]);
                    obj_.setTipoDocumentoRepresentante(row_[8]==null?null:(String) row_[8]);
                    obj_.setNroDocumentoRepresentante(row_[9]==null?null:(String) row_[9]);
                    obj_.setNombreRepresentante(row_[10]==null?null:(String) row_[10]);
                    obj_.setPaternoRepresentante(row_[11]==null?null:(String) row_[11]);
                    obj_.setMaternoRepresentante(row_[12]==null?null:(String) row_[12]);
                    resulFinal.setTotalrecord(row_[13]==null?0:(Integer) row_[13]);
                    obj_.setNumeroTH(row_[14]==null?null:(String) row_[14]);
                    obj_.setAreaTotal(row_[15]==null?null:(BigDecimal) row_[15]);
                    obj_.setFechaFin(row_[16]==null?null:(Date) row_[16]);
                    obj_.setVigencia(row_[17]==null?null:(Integer) row_[17]);
                    obj_.setModalidadTH(row_[18]==null?null:(String) row_[18]);
                    obj_.setMecanismo(row_[19]==null?null:(String) row_[19]);
                    obj_.setMecanismoTexto(row_[20]==null?null:(String) row_[20]);
                    obj_.setNombrePlan(row_[21]==null?null:(String) row_[21]);
                    obj_.setTipoPlan(row_[22]==null?null:(String) row_[22]);
                    result.add(obj_);
                }
            }
            resulFinal.setData(result);
            resulFinal.setMessage((result.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resulFinal.setIsSuccess(true);
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resulFinal.setMessage(e.getMessage());
            resulFinal.setIsSuccess(false);
        }
        return resulFinal;
    }



    @Override
    public ResultClassEntity RegistrarArchivo(List<PagoArchivoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PagoArchivoEntity> list_ = new ArrayList<>();
        try {
            for (PagoArchivoEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PagoArchivo_Insertar");
                sp.registerStoredProcedureParameter("idPagoArchivo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoSubTipo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(sp);
                sp.setParameter("idPagoArchivo", p.getIdPagoArchivo());
                sp.setParameter("codigoTipo", p.getCodigoArchivo());
                sp.setParameter("codigoSubTipo", p.getCodigoSubArchivo());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("idPago", p.getIdPago());
                sp.setParameter("idArchivo", p.getIdArchivo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        PagoArchivoEntity obj_ = new PagoArchivoEntity();
                        obj_.setIdPagoArchivo(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodigoArchivo(row_[1]==null?null:(String) row_[1]);
                        obj_.setCodigoSubArchivo(row_[2]==null?null:(String) row_[2]);
                        obj_.setDescripcion(row_[3]==null?null:(String) row_[3]);
                        obj_.setObservacion(row_[4]==null?null:(String) row_[4]);
                        obj_.setDetalle(row_[5]==null?null:(String) row_[5]);
                        obj_.setIdPago(row_[6]==null?null:(Integer) row_[6]);
                        obj_.setIdArchivo(row_[7]==null?null:(Integer) row_[7]);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el archivo del pago .");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public List<PagoArchivoEntity> ListarArchivo(PagoArchivoEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoArchivo_Listar");
            processStored.registerStoredProcedureParameter("idPagoArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodigoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPagoArchivo", param.getIdPagoArchivo());
            processStored.setParameter("idPago", param.getIdPago());
            processStored.setParameter("codigoTipoDocumento", param.getCodigoTipoDocumento());
            processStored.setParameter("codigoArchivo", param.getCodigoArchivo());
            processStored.setParameter("subCodigoArchivo", param.getCodigoSubArchivo());
            processStored.setParameter("idArchivo", param.getIdArchivo());

            processStored.execute();
            List<PagoArchivoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                PagoArchivoEntity temp = null;
                for (Object[] row: spResult){
                    temp = new PagoArchivoEntity();
                    temp.setIdPagoArchivo(row[0]== null ? null : (Integer) row[0]);
                    temp.setCodigoArchivo(row[1]== null ? null : (String) row[1]);
                    temp.setCodigoSubArchivo(row[2]== null ? null : (String) row[2]);
                    temp.setDescripcion(row[3]== null ? null : (String) row[3]);
                    temp.setObservacion(row[4]== null ? null : (String) row[4]);
                    temp.setDetalle(row[5]== null ? null : (String) row[5]);
                    temp.setIdPago(row[6]== null ? null : (Integer) row[6]);
                    temp.setIdArchivo(row[7]== null ? null : (Integer) row[7]);
                    temp.setNombreArchivo(row[8]== null ? null : (String) row[8]);
                    temp.setExtensionArchivo(row[9]== null ? null : (String) row[9]);
                    temp.setCodigoTipoDocumento(row[10]== null ? null : (String) row[10]);
                    if (!temp.getIdArchivo().equals(null)) {
                        ResultClassEntity<ArchivoEntity> file = fileUp.obtenerArchivo(temp.getIdArchivo());
                        temp.setDocumento(file.getData() == null ? null : file.getData().getFile());
                    }
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity EliminarArchivo(PagoArchivoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_PagoArhivo_Eliminar");
        processStored.registerStoredProcedureParameter("idPagoArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPagoArchivo", param.getIdPagoArchivo());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }


    @Override
    public ResultEntity listarPagoCronogramaAnios(PagoCronogramaEntity param)  {
        ResultEntity resulFinal=new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoCronograma_ListarAnios");
            processStored.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroGestion", param.getNrDocumentoGestion());
            processStored.execute();
            List<PagoCronogramaEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                PagoCronogramaEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new PagoCronogramaEntity();
                    obj_.setFecha(row_[0]==null?null:(Date) row_[0]);
                    resulFinal.setTotalrecord(row_[1]==null?0:(Integer) row_[1]);
                    result.add(obj_);
                }
            }
            resulFinal.setData(result);
            resulFinal.setMessage((result.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resulFinal.setIsSuccess(true);
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resulFinal.setMessage(e.getMessage());
            resulFinal.setIsSuccess(false);
        }
        return resulFinal;
    }


    @Override
    public ResultClassEntity RegistrarGuiaPago(List<PagoGuiaEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PagoGuiaEntity> list_ = new ArrayList<>();
        try {
            for (PagoGuiaEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PagoGuia_Registrar");
                sp.registerStoredProcedureParameter("idPagoGuia", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codPagoGuia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nrDocumentoGestion", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idPagoGuia", p.getIdPagoGuia());
                sp.setParameter("codPagoGuia", p.getCodPagoGuia());
                sp.setParameter("nrDocumentoGestion", p.getNrDocumentoGestion());
                sp.setParameter("tipoDocumentoGestion", p.getTipoDocumentoGestion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("idPago", p.getIdPago());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        PagoGuiaEntity obj_ = new PagoGuiaEntity();
                        obj_.setIdPagoGuia(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodPagoGuia(row_[1]==null?null:(String) row_[1]);
                        obj_.setObservacion(row_[2]==null?null:(String) row_[2]);
                        obj_.setDetalle(row_[3]==null?null:(String) row_[3]);
                        obj_.setDescripcion(row_[4]==null?null:(String) row_[4]);
                        obj_.setIdPago(row_[5]==null?null:(Integer) row_[5]);
                        obj_.setCodPagoGuia(row_[6]==null?null:(String) row_[6]);
                        obj_.setCodPagoGuia(row_[7]==null?null:(String) row_[7]);
                        /********************************** ESPECIES **********************************************/
                        List<PagoGuiaEspecieEntity> listDetAct = new ArrayList<>();
                        for (PagoGuiaEspecieEntity paramAc : p.getListaGuiaEspecie()) {
                            StoredProcedureQuery pa3 = entityManager.createStoredProcedureQuery("dbo.pa_PagoGuiaEspecie_Registrar");
                            pa3.registerStoredProcedureParameter("idPagoGuiaEspecie", Integer.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("codPagoGuiaEspecie", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("idEspecie", Integer.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("volumen", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("valorEstadoNatural", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("monto", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("descuento", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("montoFinal", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("descuentoTotal", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("montoTotal", BigDecimal.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("fechaComprobante", Date.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("comentarios", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("idPagoGuia", Integer.class, ParameterMode.IN);
                            pa3.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(pa3);
                            pa3.setParameter("idPagoGuiaEspecie", paramAc.getIdPagoGuiaEspecie());
                            pa3.setParameter("codPagoGuiaEspecie", paramAc.getCodPagoGuiaEspecie());
                            pa3.setParameter("idEspecie", paramAc.getIdEspecie());
                            pa3.setParameter("nombreCientifico", paramAc.getNombreCientifico());
                            pa3.setParameter("nombreComun", paramAc.getNombreComun());
                            pa3.setParameter("volumen", paramAc.getVolumen());
                            pa3.setParameter("valorEstadoNatural", paramAc.getValorEstadoNatural());
                            pa3.setParameter("monto", paramAc.getMonto());
                            pa3.setParameter("descuento", paramAc.getDescuento());
                            pa3.setParameter("montoFinal", paramAc.getMontoFinal());
                            pa3.setParameter("descuentoTotal", paramAc.getDescuentoTotal());
                            pa3.setParameter("montoTotal", paramAc.getMontoTotal());
                            pa3.setParameter("fechaComprobante", paramAc.getFechaComprobante());
                            pa3.setParameter("comentarios", paramAc.getComentarios());
                            pa3.setParameter("perfil", paramAc.getPerfil());
                            pa3.setParameter("categoria", paramAc.getCategoria());
                            pa3.setParameter("observacion", paramAc.getObservacion());
                            pa3.setParameter("detalle", paramAc.getDetalle());
                            pa3.setParameter("descripcion", paramAc.getDescripcion());
                            pa3.setParameter("idPagoGuia", obj_.getIdPagoGuia());
                            pa3.setParameter("idUsuarioRegistro", paramAc.getIdUsuarioRegistro());
                            pa3.execute();
                            List<Object[]> spResult3 = pa3.getResultList();
                            if (!spResult3.isEmpty()) {
                                PagoGuiaEspecieEntity obj3 = null;
                                for (Object[] row3 : spResult3) {
                                     obj3 = new PagoGuiaEspecieEntity();
                                    obj3.setIdPagoGuiaEspecie(row_[0]==null?null:(Integer) row_[0]);
                                    obj3.setCodPagoGuiaEspecie(row_[1]==null?null:(String) row_[1]);
                                    obj3.setIdEspecie(row_[2]==null?null:(Integer) row_[2]);
                                    obj3.setVolumen(row_[3]==null?null:(BigDecimal) row_[3]);
                                    obj3.setValorEstadoNatural(row_[4]==null?null:(BigDecimal) row_[4]);
                                    obj3.setMonto(row_[5]==null?null:(BigDecimal) row_[5]);
                                    obj3.setDescuento(row_[6]==null?null:(BigDecimal) row_[6]);
                                    obj3.setMontoFinal(row_[7]==null?null:(BigDecimal) row_[7]);
                                    obj3.setDescuentoTotal(row_[8]==null?null:(BigDecimal) row_[8]);
                                    obj3.setMontoTotal(row_[9]==null?null:(BigDecimal) row_[9]);
                                    obj3.setFechaComprobante(row_[10]==null?null:(Date) row_[10]);
                                    obj3.setComentarios(row_[11]==null?null:(String) row_[11]);
                                    obj3.setObservacion(row_[12]==null?null:(String) row_[12]);
                                    obj3.setDetalle(row_[13]==null?null:(String) row_[13]);
                                    obj3.setDescripcion(row_[14]==null?null:(String) row_[14]);
                                    obj3.setIdPagoGuia(row_[15]==null?null:(Integer) row_[15]);
                                    obj3.setNombreCientifico(row_[16]==null?null:(String) row_[16]);
                                    obj3.setNombreComun(row_[17]==null?null:(String) row_[17]);
                                    obj3.setPerfil(row_[18]==null?null:(String) row_[18]);
                                    obj3.setCategoria(row_[19]==null?null:(String) row_[19]);
                                    listDetAct.add(obj3);

                                }
                            }
                        }
                        /********************************************************************************/
                        obj_.setListaGuiaEspecie(listDetAct);

                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la guia de pago.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public ResultEntity ListarGuiaPago(PagoGuiaEntity param)  {
        ResultEntity resulFinal=new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoGuia_Listar");
            processStored.registerStoredProcedureParameter("idPago", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPagoGuia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPago", param.getIdPago());
            processStored.setParameter("idPagoGuia", param.getIdPagoGuia());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());
            processStored.execute();
            List<PagoGuiaEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                PagoGuiaEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new PagoGuiaEntity();
                    obj_.setIdPagoGuia(row_[0]==null?null:(Integer) row_[0]);
                    obj_.setCodPagoGuia(row_[1]==null?null:(String) row_[1]);
                    obj_.setObservacion(row_[2]==null?null:(String) row_[2]);
                    obj_.setDetalle(row_[3]==null?null:(String) row_[3]);
                    obj_.setDescripcion(row_[4]==null?null:(String) row_[4]);
                    obj_.setIdPago(row_[5]==null?null:(Integer) row_[5]);
                    obj_.setNrDocumentoGestion(row_[6]==null?null:(Integer) row_[6]);
                    obj_.setTipoDocumentoGestion(row_[7]==null?null:(String) row_[7]);

                    /********************************** Especies **********************************************/
                    List<PagoGuiaEspecieEntity> listDetEspecies = new ArrayList<PagoGuiaEspecieEntity>();
                    StoredProcedureQuery pa3 = entityManager.createStoredProcedureQuery("dbo.pa_PagoGuiaEspecie_Listar");
                    pa3.registerStoredProcedureParameter("idPagoGuia", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(pa3);
                    processStored.setParameter("idPagoGuia", param.getIdPagoGuia());
                    pa3.execute();
                    List<Object[]> spResultPeriodo = pa3.getResultList();
                    if(spResultPeriodo!=null) {
                        if (spResultPeriodo.size() >= 1) {
                            for (Object[] row3 : spResultPeriodo) {
                                PagoGuiaEspecieEntity obj_2 = new PagoGuiaEspecieEntity();
                                obj_2.setIdPagoGuiaEspecie(row3[0]==null?null:(Integer) row3[0]);
                                obj_2.setCodPagoGuiaEspecie(row3[1]==null?null:(String) row3[1]);
                                obj_2.setIdEspecie(row3[2]==null?null:(Integer) row3[2]);
                                obj_2.setVolumen(row3[3]==null?null:(BigDecimal) row3[3]);
                                obj_2.setValorEstadoNatural(row3[4]==null?null:(BigDecimal) row3[4]);
                                obj_2.setMonto(row3[5]==null?null:(BigDecimal) row3[5]);
                                obj_2.setDescuento(row3[6]==null?null:(BigDecimal) row3[6]);
                                obj_2.setMontoFinal(row3[7]==null?null:(BigDecimal) row3[7]);
                                obj_2.setDescuentoTotal(row3[8]==null?null:(BigDecimal) row3[8]);
                                obj_2.setMontoTotal(row3[9]==null?null:(BigDecimal) row3[9]);
                                obj_2.setFechaComprobante(row3[10]==null?null:(Date) row3[10]);
                                obj_2.setComentarios(row3[11]==null?null:(String) row3[11]);
                                obj_2.setObservacion(row3[12]==null?null:(String) row3[12]);
                                obj_2.setDetalle(row3[13]==null?null:(String) row3[13]);
                                obj_2.setDescripcion(row3[14]==null?null:(String) row3[14]);
                                obj_2.setIdPagoGuia(row3[15]==null?null:(Integer) row3[15]);
                                obj_2.setNombreCientifico(row3[16]==null?null:(String) row3[16]);
                                obj_2.setNombreComun(row3[17]==null?null:(String) row3[17]);
                                obj_2.setPerfil(row3[18]==null?null:(String) row3[18]);
                                obj_2.setCategoria(row3[19]==null?null:(String) row3[19]);
                                listDetEspecies.add(obj_2);
                            }
                        }
                    }
                    /********************************************************************************/
                    obj_.setListaGuiaEspecie(listDetEspecies);


                    resulFinal.setTotalrecord(row_[8]==null?0:(Integer) row_[8]);
                    result.add(obj_);
                }
            }
            resulFinal.setData(result);
            resulFinal.setMessage((result.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resulFinal.setIsSuccess(true);
            //resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resulFinal.setMessage(e.getMessage());
            resulFinal.setIsSuccess(false);
        }
        return resulFinal;
    }


    @Override
    public List<PagoEntity> listarModalidad(PagoEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoModalidad_Listar");
            processStored.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroGestion", param.getNrDocumentoGestion());
            processStored.execute();
            List<PagoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                PagoEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new PagoEntity();
                    obj_.setModalidadTH(row_[0]==null?null:(String) row_[0]);
                    result.add(obj_);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("Modalidad", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<THEntity> listarOperativosPorPGM(THEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PagoOperativosPorPGM_Listar");
            processStored.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroGestion", param.getNroGestion());
            processStored.execute();
            List<THEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                THEntity obj_ = null;
                for (Object[] row_: spResult){
                    obj_ = new THEntity();
                    obj_.setNroGestion(row_[0]==null?null:(Integer) row_[0]);
                    obj_.setTipoPlan(row_[1]==null?null:(String) row_[1]);
                    obj_.setNombrePlan(row_[2]==null?null:(String) row_[2]);
                    result.add(obj_);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("Modalidad", e.getMessage());
            throw e;
        }
    }



}
