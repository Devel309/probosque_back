package pe.gob.serfor.mcsniffs.repository.impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.CensoForestalDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

@Repository
public class CensoForestalDetalleRepositoryImpl extends JdbcDaoSupport implements CensoForestalDetalleRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(CensoForestalDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;



    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity RegistrarCensoForestalDetalle(CensoForestalDetalleEntity param) throws SQLException {
          ResultClassEntity result = new ResultClassEntity();
          StoredProcedureQuery processStored = entityManager
                  .createStoredProcedureQuery("[dbo].[pa_CensoForestalDetalle_Registrar]");
          processStored.registerStoredProcedureParameter("idTipoRecurso", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("idTipoArbol", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("codigoArbolCalidad", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("codigoUnidadMedida", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("productoTipo", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("parcelaCorte", String.class, ParameterMode.IN); // de Integer a String
          processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);  // de Integer a String
          processStored.registerStoredProcedureParameter("faja", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("alturaTotal", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("dap", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("alturaComercial", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("este", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("zona", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("norte", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("cantidad", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("superficieComercial", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("contrato", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("codigoTipoCensoForestal", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("codigoUnico", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("idCodigoEspecie", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("nombreNativo", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("volumen", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("factorForma", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("categoriaDiametrica", Double.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("descripcionOtros", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("nombreComercial", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("nombreAlterno", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("mortanda", Boolean.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("estadoArbol", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("condicionArbol", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("numeroCorrelativoArbol",Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("bosqueSecundario", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("idPlanManManejo", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("idCensoForestalCabecera", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("unidadTrabajo", Integer.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("codigoDeArbol", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("imagen", String.class, ParameterMode.IN);
          processStored.registerStoredProcedureParameter("audio", String.class, ParameterMode.IN);
         //////////////////////////////////////////////////////////////////////////////////////////
          processStored.setParameter("idTipoRecurso", param.getIdTipoRecurso());
          processStored.setParameter("idTipoBosque", param.getIdTipoBosque());
          processStored.setParameter("idTipoArbol", param.getIdTipoArbol());
          processStored.setParameter("codigoArbolCalidad", param.getCodigoArbolCalidad());
          processStored.setParameter("codigoUnidadMedida", param.getCodigoUnidadMedida());
          processStored.setParameter("productoTipo", param.getProductoTipo());
          processStored.setParameter("parcelaCorte", param.getParcelaCorta()); // de Integer a String
          processStored.setParameter("bloque", param.getBloqueQuinquenal());   // de Integer a String
          processStored.setParameter("faja", param.getFaja());
          processStored.setParameter("alturaTotal",param.getAlturaTotal() );
          processStored.setParameter("dap", param.getDap());
          processStored.setParameter("alturaComercial", param.getAlturaComercial());
          processStored.setParameter("este",param.getEste());
          processStored.setParameter("zona", param.getZona());
          processStored.setParameter("norte",param.getNorte());
          processStored.setParameter("cantidad", param.getCantidad());
          processStored.setParameter("superficieComercial", param.getSuperficieComercial());
          processStored.setParameter("contrato", param.getContrato());
          processStored.setParameter("codigoTipoCensoForestal", param.getCodigoTipoCensoForestal());
          processStored.setParameter("codigoUnico", param.getCodigoUnico());
          processStored.setParameter("idCodigoEspecie", param.getIdCodigoEspecie());
          processStored.setParameter("nombreNativo", param.getNombreNativo());
          processStored.setParameter("volumen", param.getVolumen());
          processStored.setParameter("factorForma", param.getFactorForma());
          processStored.setParameter("categoriaDiametrica", param.getCategoriaDiametrica());
          processStored.setParameter("descripcionOtros", param.getDescripcionOtros());
          processStored.setParameter("nombreComercial", param.getNombreComercial());
          processStored.setParameter("nombreAlterno", param.getNombreAlterno());
          processStored.setParameter("familia", param.getFamilia());
          processStored.setParameter("mortanda", param.getMortanda());
          processStored.setParameter("estadoArbol", param.getEstadoArbol());
          processStored.setParameter("nombreComun", param.getNombreComun());
          processStored.setParameter("nombreCientifico", param.getNombreCientifico());
          processStored.setParameter("condicionArbol", param.getCondicionArbol());
          processStored.setParameter("numeroCorrelativoArbol",param.getNumeroCorrelativoArbol());
          processStored.setParameter("bosqueSecundario", param.getBosqueSecundario());
          processStored.setParameter("idPlanManManejo", param.getIdPlanManManejo());
          processStored.setParameter("idCensoForestalCabecera", param.getIdCensoForestalCabecera());
          processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioRegistro());
          processStored.setParameter("unidadTrabajo", param.getUnidadTrabajo());
          processStored.setParameter("codigoDeArbol", param.getCodigoDeArbol());
          processStored.setParameter("imagen", param.getImagen());
          processStored.setParameter("audio", param.getAudio());
          processStored.execute();
          result.setData(param);
          result.setSuccess(true);
          result.setMessage("Se registró el detalle del Censo Forestal con éxito.");

          /*if(param.getIdCensoForestalCabecera()!=0) {
                LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MAD_CENSOFORESTAL_DETALLE,
                        LogAuditoria.REGISTRAR,
                        LogAuditoria.DescripcionAccion.Registrar.T_MAD_CENSOFORESTAL_DETALLE + request.getIdSolPlantForest(),
                        obj.getIdUsuarioRegistro());
          }
          logAuditoriaService.RegistrarLogAuditoria(logAuditoriaEntity);*/



          return result;
    }

      @Override
      public ResultClassEntity actualizarCensoForestalDetalle(CensoForestalDetalleEntity param,Integer idCensoForestalDetalle) throws Exception {
            ResultClassEntity result = new ResultClassEntity();
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("[dbo].[pa_CensoForestalDetalle_Actualizar]");
            processStored.registerStoredProcedureParameter("idCensoForestalDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoRecurso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoArbol", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoArbolCalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUnidadMedida", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("productoTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("parcelaCorte", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("faja", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("alturaTotal", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("dap", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("alturaComercial", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("este", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("zona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("norte", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cantidad", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficieComercial", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("contrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoCensoForestal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUnico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCodigoEspecie", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreNativo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumen", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("factorForma", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("categoriaDiametrica", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionOtros", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreComercial", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreAlterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("mortanda", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoArbol", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("condicionArbol", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroCorrelativoArbol",Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bosqueSecundario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCensoForestalCabecera", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("unidadTrabajo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoDeArbol", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("imagen", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("audio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resultado", Integer.class, ParameterMode.OUT);
            //////////////////////////////////////////////////////////////////////////////////////////
            processStored.setParameter("idCensoForestalDetalle", idCensoForestalDetalle);
            processStored.setParameter("idTipoRecurso", param.getIdTipoRecurso());
            processStored.setParameter("idTipoBosque", param.getIdTipoBosque());
            processStored.setParameter("idTipoArbol", param.getIdTipoArbol());
            processStored.setParameter("codigoArbolCalidad", param.getCodigoArbolCalidad());
            processStored.setParameter("codigoUnidadMedida", param.getCodigoUnidadMedida());
            processStored.setParameter("productoTipo", param.getProductoTipo());
            processStored.setParameter("parcelaCorte", param.getParcelaCorta());
            processStored.setParameter("bloque", param.getBloqueQuinquenal());
            processStored.setParameter("faja", param.getFaja());
            processStored.setParameter("alturaTotal",param.getAlturaTotal() );
            processStored.setParameter("dap", param.getDap());
            processStored.setParameter("alturaComercial", param.getAlturaComercial());
            processStored.setParameter("este",param.getEste());
            processStored.setParameter("zona", param.getZona());
            processStored.setParameter("norte",param.getNorte());
            processStored.setParameter("cantidad", param.getCantidad());
            processStored.setParameter("superficieComercial", param.getSuperficieComercial());
            processStored.setParameter("contrato", param.getContrato());
            processStored.setParameter("codigoTipoCensoForestal", param.getCodigoTipoCensoForestal());
            processStored.setParameter("codigoUnico", param.getCodigoUnico());
            processStored.setParameter("idCodigoEspecie", param.getIdCodigoEspecie());
            processStored.setParameter("nombreNativo", param.getNombreNativo());
            processStored.setParameter("volumen", param.getVolumen());
            processStored.setParameter("factorForma", param.getFactorForma());
            processStored.setParameter("categoriaDiametrica", param.getCategoriaDiametrica());
            processStored.setParameter("descripcionOtros", param.getDescripcionOtros());
            processStored.setParameter("nombreComercial", param.getNombreComercial());
            processStored.setParameter("nombreAlterno", param.getNombreAlterno());
            processStored.setParameter("familia", param.getFamilia());
            processStored.setParameter("mortanda", param.getMortanda());
            processStored.setParameter("estadoArbol", param.getEstadoArbol());
            processStored.setParameter("nombreComun", param.getNombreComun());
            processStored.setParameter("nombreCientifico", param.getNombreCientifico());
            processStored.setParameter("condicionArbol", param.getCondicionArbol());
            processStored.setParameter("numeroCorrelativoArbol",param.getNumeroCorrelativoArbol());
            processStored.setParameter("bosqueSecundario", param.getBosqueSecundario());
            processStored.setParameter("idPlanManManejo", param.getIdPlanManManejo());
            processStored.setParameter("idCensoForestalCabecera", param.getIdCensoForestalCabecera());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioRegistro());
            processStored.setParameter("unidadTrabajo", param.getUnidadTrabajo());
            processStored.setParameter("codigoDeArbol", param.getCodigoDeArbol());
            processStored.setParameter("imagen", param.getImagen());
            processStored.setParameter("audio", param.getAudio());
            processStored.execute();
            Integer resultado=Integer.parseInt(""+processStored.getOutputParameterValue("resultado"));
            if(resultado>0){
                  result.setData(param);
                  result.setSuccess(true);
                  result.setMessage("Se actualizó el detalle del Censo Forestal con éxito.");

                  LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                          LogAuditoria.Table.T_MAD_CENSOFORESTAL_DETALLE,
                          LogAuditoria.ACTUALIZAR,
                          LogAuditoria.DescripcionAccion.Actualizar.T_MAD_CENSOFORESTAL_DETALLE + idCensoForestalDetalle,
                          param.getIdUsuarioRegistro());

                  logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            }
            else{
                  result.setSuccess(false);
            }
            return result;
      }

      @Override
      public ResultClassEntity eliminarCensoForestalDetalle(Integer idUsuarioElimina,Integer idCensoForestalDetalle) throws Exception {
            ResultClassEntity result = new ResultClassEntity();
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("[dbo].[pa_CensoForestalDetalle_Eliminar]");
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCensoForestalDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resultado", Integer.class, ParameterMode.OUT);
            processStored.setParameter("idUsuarioElimina",idUsuarioElimina );
            processStored.setParameter("idCensoForestalDetalle", idCensoForestalDetalle);
            processStored.execute();
           Integer resultado=Integer.parseInt(""+processStored.getOutputParameterValue("resultado"));
           if(resultado>0){
                 result.setSuccess(true);
                 result.setMessage("Se borro detalle del Censo Forestal con éxito.");

                 LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                         LogAuditoria.Table.T_MAD_CENSOFORESTAL_DETALLE,
                         LogAuditoria.ELIMINAR,
                         LogAuditoria.DescripcionAccion.Eliminar.T_MAD_CENSOFORESTAL_DETALLE + idCensoForestalDetalle,
                         idUsuarioElimina);

                 logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

           }
          else{
                 result.setSuccess(false);
           }
            return result;
      }


      @Override
      public ResultClassEntity listarRecursosMaderablesCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception {
            ResultClassEntity result = new ResultClassEntity();
            CensoForestalEntity lista = new CensoForestalEntity();
            List<CensoForestalDetalleEntity> listaDetalle = new ArrayList<>();
            try {

                  StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestal_Listar");
                  processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                  SpUtil.enableNullParams(processStored);
                  processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
                  processStored.execute();
                  List<Object[]> spResult = processStored.getResultList();
                  if(spResult!=null){
                        if (spResult.size() >= 1) {
                              for (Object[] row1 : spResult) {

                                    lista.setStrFechaInicio((String) row1[3]);
                                    lista.setStrFechaFin((String) row1[4]);
                              }
                        }

                  }

                  /////////////////////////////////////////////////////////////////////////////////////////77
                  /////////////////////////////////////////////////////////////////////////////////////////77
                  /////////////////////////////////////////////////////////////////////////////////////////77
                  StoredProcedureQuery processStoredDetalle = entityManager
                          .createStoredProcedureQuery("dbo.pa_CensoForestalDetalle_ListarRecursosMaderables");

                  processStoredDetalle.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

                  SpUtil.enableNullParams(processStoredDetalle);
                  processStoredDetalle.setParameter("idPlanManejo", request.getIdPlanManejo());

                  processStoredDetalle.execute();

                  List<Object[]> spResult2 = processStoredDetalle.getResultList();
                  if (spResult2 != null) {
                        CensoForestalDetalleEntity c = null;
                        if (spResult2.size() >= 1) {
                              for (Object[] row : spResult2) {
                                    c = new CensoForestalDetalleEntity();

                                    c.setIdCensoForestalDetalle(row[0] == null ? null : (Integer) row[0]);
                                    c.setIdCensoForestal(row[1] == null ? null : (Integer) row[1]);
                                    c.setNombreComun(row[2] == null ? null : (String) row[2]);
                                    c.setNombreCientifico(row[3] == null ? null : (String) row[3]);
                                    c.setNroArbolesCensados(row[4] == null ? null : (Integer) row[4]);
                                    c.setNroArboles(row[5] == null ? null : (Integer) row[5]);
                                    c.setVolumen(row[6] == null ? null : Double.parseDouble(row[6].toString()));
                                    c.setNroArbolesSemilleros(row[7] == null ? null : (Integer)(row[7]));
                                    listaDetalle.add(c);
                              }
                        }
                  }
                  lista.setListCensoForestalDetalle(listaDetalle);
                  result.setData(lista);
                  result.setSuccess(true);
                  result.setMessage("Se realizó la acción Listar Solicitud Plantación Forestal correctamente.");

            } catch (Exception e) {
                  log.error("listarSolicitudPlantacionForestal", e.getMessage());
                  result.setSuccess(false);
                  result.setMessage("Ocurrió un error. No se pudo obtener el listado de solicitudes de plantación forestal.");

            }
            return result;
      }

      @Override
      public ResultClassEntity listarRecursosDiferentesCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception {
            ResultClassEntity result = new ResultClassEntity();
            List<CensoForestalDetalleEntity> lista = new ArrayList<>();
            try {
                  StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestalDetalle_ListarRecursosDiferentes");

                  processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

                  SpUtil.enableNullParams(processStored);
                  processStored.setParameter("idPlanManejo", request.getIdPlanManejo());

                  processStored.execute();

                  List<Object[]> spResult = processStored.getResultList();
                  if (spResult != null) {
                        CensoForestalDetalleEntity c = null;
                        if (spResult.size() >= 1) {
                              for (Object[] row : spResult) {
                                    c = new CensoForestalDetalleEntity();
                                    c.setIdCensoForestalDetalle(row[0] == null ? null : (Integer) row[0]);
                                    c.setIdCensoForestal(row[1] == null ? null : (Integer) row[1]);
                                    c.setNombreComun(row[2] == null ? null : (String) row[2]);
                                    c.setNombreCientifico(row[3] == null ? null : (String) row[3]);
                                    c.setNroIndividuo(row[4] == null ? null : (Integer) row[4]);
                                    c.setAreaProductiva(row[5] == null ? null : ((BigDecimal)row[5]).doubleValue());
                                    c.setProductoExtraer(row[6] == null ? null : (String) row[6]);
                                    c.setUnidadMedida(row[7] == null ? null : (String) row[7]);
                                    c.setCantidadProducto(row[8] == null ? null : ((BigDecimal)row[8]).doubleValue());
                                    lista.add(c);
                              }
                        }
                  }

                  result.setData(lista);
                  result.setSuccess(true);
                  result.setMessage("Se realizó la acción Listar Solicitud Plantación Forestal correctamente.");

            } catch (Exception e) {
                  log.error("listarSolicitudPlantacionForestal", e.getMessage());
                  result.setSuccess(false);
                  result.setMessage("Ocurrió un error. No se pudo obtener el listado de solicitudes de plantación forestal.");

            }
            return result;
      }

      @Override
      public ResultClassEntity listarCensoForestalDetalleAnexo(CensoForestalDetalleEntity request) throws Exception {
            ResultClassEntity result = new ResultClassEntity();

            List<CensoForestalDetalleEntity> listaDetalle = new ArrayList<>();
            try {
                  DecimalFormat df = new DecimalFormat("0.000000");
                  StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestalDetalle_AnexoB");
                  processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

                  SpUtil.enableNullParams(processStored);
                  processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
                  processStored.execute();

                  List<Object[]> spResult = processStored.getResultList();
                  if(spResult!=null && !spResult.isEmpty()){
                        CensoForestalDetalleEntity c = null;
                        for (Object[] row : spResult) {
                              c = new  CensoForestalDetalleEntity();
                              c.setNombreCientifico(row[0] == null ? null : (String) row[0]);
                              c.setNombreComun(row[1] == null ? null : (String) row[1]);
                              c.setNroArboles(row[2] == null ? null : (Integer) row[2]);
                              c.setDap(row[3] == null ? null : ((BigDecimal) row[3]).doubleValue());
                              c.setStrDap(df.format(c.getDap()));
                              c.setAlturaComercial(row[4] == null ? null : ((BigDecimal) row[4]).doubleValue());
                              c.setStrAlturaComercial(df.format(c.getAlturaComercial()));
                              c.setVolumen(row[5] == null ? null : ((BigDecimal) row[5]).doubleValue());
                              c.setStrVolumen(df.format(c.getVolumen()));
                              c.setCodigoTipoArbol(row[6] == null ? null : (String) row[6]);
                              c.setTipoArbol(row[7] == null ? null : (String) row[7]);
                              c.setCoorEste(row[8] == null ? null : (String) row[8]);
                              c.setCoorNorte(row[9] == null ? null : (String) row[9]);
                              listaDetalle.add(c);
                        }

                  }

                  result.setData(listaDetalle);
                  result.setSuccess(true);
                  result.setMessage("Se obtuvo la lista de anexos del detalle de censo forestal correctamente.");

            } catch (Exception e) {
                  log.error("listarSolicitudPlantacionForestal", e.getMessage());
                  result.setSuccess(false);
                  result.setMessage("Ocurrió un error. No se pudo obtener la lista de anexos del detalle de censo forestal.");
                  result.setInnerException(e.getMessage());
            }
            return result;
      }

      @Override
      public ResultClassEntity listarSincronizacionCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception {
            ResultClassEntity result = new ResultClassEntity();

            List<CensoForestalDetalleEntity> listaDetalle = new ArrayList<>();
            try {
                  DecimalFormat df = new DecimalFormat("0.000000");
                  StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Sincronizacion.pa_informacionCenso_Listar");
                  processStored.registerStoredProcedureParameter("idPlanManejo", String.class, ParameterMode.IN);
                  processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
                  processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
                  processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

                  SpUtil.enableNullParams(processStored);
                  processStored.setParameter("idPlanManejo", request.getStrIdPlanManejo());
                  processStored.setParameter("idUsuario", request.getIdUsuarioRegistro());
                  processStored.setParameter("pageNum", request.getPageNum());
                  processStored.setParameter("pageSize", request.getPageSize());
                  processStored.execute();

                  List<Object[]> spResult = processStored.getResultList();
                  if(spResult!=null && !spResult.isEmpty()){
                        CensoForestalDetalleEntity c = null;
                        for (Object[] row : spResult) {
                              c = new  CensoForestalDetalleEntity();
                              c.setIdCensoForestalDetalle(row[0] == null ? null : (Integer) row[0]);
                              c.setIdCensoForestal(row[1] == null ? null : (Integer) row[1]);
                              c.setIdPlanManejo(row[2] == null ? null : (Integer) row[2]);
                              c.setParcelaCorta(row[3] == null ? null : (String) row[3]);
                              c.setBloqueQuinquenal(row[4] == null ? null : (String) row[4]);
                              c.setFaja(row[5] == null ? null : (Integer) row[5]);
                              c.setIdCodigoEspecie(row[6] == null ? null : (Integer) row[6]);
                              c.setNombreComun(row[7] == null ? null : (String) row[7]);
                              c.setNombreCientifico(row[8] == null ? null : (String) row[8]);
                              c.setDap(row[9] == null ? null : ((BigDecimal) row[9]).doubleValue());
                              c.setAlturaComercial(row[10] == null ? null : ((BigDecimal) row[10]).doubleValue());
                              c.setAlturaTotal(row[11] == null ? null : ((BigDecimal) row[11]).doubleValue());
                              c.setVolumen(row[12] == null ? null : ((BigDecimal) row[12]).doubleValue());
                              c.setFactorForma(row[13] == null ? null : ((BigDecimal) row[13]).doubleValue());
                              c.setCategoriaDiametrica(row[14] == null ? null : ((BigDecimal) row[14]).doubleValue());
                              c.setCoorEste(row[15] == null ? null : (String) row[15]);
                              c.setCoorNorte(row[16] == null ? null : (String) row[16]);
                              c.setCantidad(row[17] == null ? null : ((BigDecimal) row[17]).doubleValue());
                              c.setSuperficieComercial(row[18] == null ? null : ((BigDecimal) row[18]).doubleValue());
                              c.setCodigoCondicionArbol(row[19] == null ? null : (String) row[19]);
                              c.setCondicionArbol(row[20] == null ? null : (String) row[20]);
                              c.setCodigoTipoArbol(row[21] == null ? null : (String) row[21]);
                              c.setTipoArbol(row[22] == null ? null : (String) row[22]);
                              c.setCodigoArbolCalidad(row[23] == null ? null : (String) row[23]);
                              c.setCodigoArbol(row[24] == null ? null : (String) row[24]);
                              c.setFamilia(row[25] == null ? null : (String) row[25]);
                              c.setCodigoEstadoArbol(row[26] == null ? null : (String) row[26]);
                              c.setEstadoArbol(row[27] == null ? null : (String) row[27]);
                              listaDetalle.add(c);

                              result.setTotalRecord((Integer) row[28]);
                        }
                  }
                  result.setData(listaDetalle);
                  result.setSuccess(true);
                  result.setMessage("Se obtuvo la información del censo forestal correctamente.");
            } catch (Exception e) {
                  log.error("listarSolicitudPlantacionForestal", e.getMessage());
                  result.setSuccess(false);
                  result.setMessage("Ocurrió un error. No se pudo obtener la información del censo forestal.");
                  result.setInnerException(e.getMessage());
            }
            return result;
      }

}
