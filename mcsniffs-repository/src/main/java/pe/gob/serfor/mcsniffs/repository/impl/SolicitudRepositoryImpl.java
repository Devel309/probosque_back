package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.FiltroSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ImpugnacionNuevoGanadorEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.TipoDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.TipoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.ResultadoPPRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;


@Repository
public class SolicitudRepositoryImpl extends JdbcDaoSupport implements SolicitudRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(SolicitudRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ProcesoPostulacionRepository pp;

    @Autowired
    private ResultadoPPRepository rpp;

    @Autowired
    private FileServerConexion fileCn;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResponseEntity ListarPorFiltroSolicitud() {
        ResponseEntity result = new ResponseEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Lista_Solicitud");

            //processStored.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.INOUT);
            //processStored.setParameter("fechaRegistro", param.getFechaRegistro() );

            processStored.execute();
            List<SolicitudEntity> listResult = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    SolicitudEntity temp = new SolicitudEntity();
                    temp.setMotivoPostulacion((String) row[0]);
                    temp.setEstado((String) row[1]);
                    temp.setAprobado( (Boolean) (row[2]) );
                    temp.setInicioSolicitud ((Date) row[3]);
                    //temp.setFinsolicitud((Date) row[4]);
                    //temp.setDescripcion((String) row[5]);
                    listResult.add(temp);
                }
            }

            result.setSuccess(true);
            result.setMessage("ok");
            result.setData(listResult);
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
            result.setData(null);
        }
        return  result;
    }

    /**
     * @autor: JaquelineDB [02-07-2021]
     * @modificado:
     * @descripción: {Registrar solicitud de ampliacion de plazo de entrega de pruebas de publicacion}
     * @param:file
     */
    @Override
    public ResultClassEntity RegistrarSolicitudAmpliacion(SolicitudDto param) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_RegistrarAmpliacionPlazo");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicioSolicitud", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFinSolicitud", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("aprobado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("motivoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("flagAutorizacion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diaAmpliacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitud", param.getIdSolicitud());
            processStored.setParameter("idProcesoPostulacion", param.getIdProcesoPostulacion());
            processStored.setParameter("idTipoSolicitud", param.getIdTipoSolicitud());
            processStored.setParameter("fechaInicioSolicitud", param.getFechaInicioSolicitud());
            processStored.setParameter("fechaFinSolicitud", param.getFechaFinSolicitud());
            processStored.setParameter("aprobado", null);
            processStored.setParameter("asunto", param.getAsunto());
            processStored.setParameter("motivoSolicitud", param.getMotivoSolicitud());
            processStored.setParameter("idUsuarioSolicitante", param.getIdUsuarioSolicitante());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.setParameter("flagAutorizacion", param.getFlagAutorizacion());
            processStored.setParameter("diaAmpliacion", param.getDiaAmpliacion());
            processStored.execute();
            Integer id = (Integer) processStored.getOutputParameterValue("idSolicitud");

            if(param.getIdTipoSolicitud().equals(5)){

                Integer IdEstatus = 15;
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(param.getIdProcesoPostulacion(),IdEstatus,param.getIdUsuarioModificacion(),null);
                result.setData(resultup);
            }
            result.setCodigo(id);
            result.setSuccess(true);
            result.setMessage("Se envio su solicitud de ampliacion.");
            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarDocumentoAdjunto(SolicitudEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudAdjuntos_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudAdjunto", Integer.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitud", param.getIdSolicitud());
            processStored.setParameter("idDocumentoAdjunto", param.getIdDocumentoAdjunto());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.setParameter("idSolicitudAdjunto", param.getIdSolicitudAdjunto());

            processStored.execute();

            Integer idSolicitudAdjunto = (Integer) processStored.getOutputParameterValue("idSolicitudAdjunto");
            result.setCodigo(idSolicitudAdjunto);
            result.setSuccess(true);
            result.setMessage("Se registró Documento adjunto Correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {Obtener archivos de las solicitudes}
     * @param:IdSolicitud
     */
    @Override
    public ResultEntity<SolicitudAdjuntosEntity> ObtenerArchivosSolicitudes(Integer IdSolicitud) {
        ResultEntity<SolicitudAdjuntosEntity> result = new ResultEntity<SolicitudAdjuntosEntity>();
        List<SolicitudAdjuntosEntity>lstFiles = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudAdjunto_Listar");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", IdSolicitud);
            processStored.execute();

            SolicitudAdjuntosEntity temp = null;
            List<Object[]> spResult = processStored.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    temp = new SolicitudAdjuntosEntity();

                    temp.setIdSolicitudAdjunto((Integer) row[0]);
                    temp.setIdSolicitud((Integer) row[1]);
                    temp.setIdDocumentoAdjunto((Integer) row[2]);
                    temp.setNombreGenerado((String) row[3]);
                    temp.setIdTipoDocumento((Integer) row[4]);
                    temp.setNombre((String) row[5]);
                    temp.setConforme((Boolean) row[6]);
                    temp.setObservacion((String) row[7]);
                    if(temp.getNombreGenerado() != null){
                        byte[] byteFile = fileCn.loadFileAsResource(temp.getNombreGenerado());

                        temp.setFile(byteFile);
                    }
                    lstFiles.add(temp);
                }
            }
            result.setData(lstFiles);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los documentos adjuntos.");
            return result;
        } catch (Exception e) {
            log.error("SolicitudRepositoryImpl - ObtenerArchivosSolicitudes", e.getMessage());
            result.setIsSuccess(false);
            result.setData(lstFiles);
            result.setMessage("Ocurrió un error al obtener el archivo.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
    @Override
    public ResultClassEntity ActualizarAprobadoSolicitud(SolicitudDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_ActualizarAprobado");
            sp.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("aprobado", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idSolicitud", obj.getIdSolicitud());
            sp.setParameter("aprobado", obj.getAprobado());
            sp.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            sp.execute();
            if(obj.getAprobado() && obj.getIdTipoSolicitud().equals(4)){
                Integer IdEstatus = 34;
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(obj.getIdProcesoPostulacion(),IdEstatus,obj.getIdUsuarioModificacion(),null);
                result.setData(resultup);
            }

            if(!obj.getAprobado() && obj.getIdTipoSolicitud().equals(4)){
                Integer IdEstatus = 4;
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(obj.getIdProcesoPostulacion(),IdEstatus,obj.getIdUsuarioModificacion(),null);
                result.setData(resultup);
            }


            result.setSuccess(true);
            result.setMessage("Se revisó la solicitud.");
            return result;
        } catch (Exception e) {
            log.error("Solicitud - AprobarRechazarSolicitud", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
    @Override
    public ResultClassEntity RegistrarObservacionDocumentoAdjunto(List<DocumentoAdjuntoDto> obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            for (DocumentoAdjuntoDto item :obj) {

                StoredProcedureQuery sp1 = entityManager.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_RegistrarObservacion");
                sp1.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp1);
                sp1.setParameter("idDocumentoAdjunto", item.getIdDocumentoAdjunto());
                sp1.setParameter("observacion", item.getObservacion());
                sp1.setParameter("conforme", item.getConforme());
                sp1.setParameter("idUsuarioModificacion", item.getIdUsuarioModificacion());
                sp1.execute();
            }

            result.setSuccess(true);
            result.setMessage("Se registró la observación correctamente.");
            return result;
        } catch (Exception e) {
            log.error("Solicitud - AprobarRechazarSolicitud", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [03-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Aprobar o rechazar las solicitudes de ampliacion de plazos}
     * @param:file
     */
    @Override
    public ResultClassEntity AprobarRechazarSolicitud(SolicitudDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_ActualizarAmpliacionPlazo");
            sp.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaFinSolicitud", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaInicioSolicitud", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("aprobado", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("FechaRecepcionDocs", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolic", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idSolicitud", obj.getIdSolicitud());
            sp.setParameter("fechaFinSolicitud", obj.getFechaFinSolicitud());
            sp.setParameter("fechaInicioSolicitud", obj.getFechaInicioSolicitud());
            sp.setParameter("aprobado", obj.getAprobado());
            sp.setParameter("FechaRecepcionDocs", obj.getFechaRecepcionDocsFisicos());
            sp.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            sp.setParameter("fechaModificacion", new java.sql.Timestamp(date.getTime()));
            sp.execute();
            Integer idSolic = (Integer) sp.getOutputParameterValue("idSolic");

            if(!(obj.getObservacion()==null)){
                StoredProcedureQuery sp1 = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudObservacion_Insertar");
                sp1.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("fechaCreacion", Date.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp1);
                sp1.setParameter("idSolicitud", obj.getIdSolicitud());
                sp1.setParameter("observacion", obj.getObservacion());
                sp1.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
                sp1.setParameter("fechaCreacion", new java.sql.Timestamp(date.getTime()));
            }
            ResultClassEntity<SolicitudDto> solicitud = ObtenerSolicitudAmpliacion(obj);
            SolicitudDto objsoli = solicitud.getData();
            if(obj.getAprobado() && objsoli.getIdTipoSolicitud().equals(4)){
                Integer IdEstatus = 10;
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(objsoli.getIdProcesoPostulacion(),IdEstatus,obj.getIdUsuarioModificacion(),null);
                result.setData(resultup);
            }else if(obj.getAprobado() && objsoli.getIdTipoSolicitud().equals(5)){
                Integer IdEstatus = 9;
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(objsoli.getIdProcesoPostulacion(),IdEstatus,obj.getIdUsuarioModificacion(),null);
                if(resultup.getSuccess()){
                    ResultClassEntity ganador = rpp.ActualizarGanador(objsoli.getIdProcesoPostulacion(), obj.getIdProcesoPostulacionGanador(), obj.getIdUsuarioModificacion());
                    result.setData(ganador);
                }
            }else if((!obj.getAprobado()) && objsoli.getIdTipoSolicitud().equals(5)){
                Integer IdEstatus = 11;
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(objsoli.getIdProcesoPostulacion(),IdEstatus,obj.getIdUsuarioModificacion(),null);
                result.setData(resultup);
            }
            result.setCodigo(idSolic);
            result.setSuccess(true);
            result.setMessage("Se revisó la solicitud.");
            return result;
        } catch (Exception e) {
            log.error("Solicitud - AprobarRechazarSolicitud", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [03-07-2021]
     * @modificado:
     * @descripción: {Listar las solicitudes de ampliacion}
     * @param:file
     */
    @Override
    public ResultClassEntity  listarSolicitudesAmpliacion(SolicitudDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_ListarSolicitudAmpliacion");
            sp.registerStoredProcedureParameter("idTipoSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstado", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idTipoSolicitud", param.getIdTipoSolicitud());
            sp.setParameter("idProcesoPostulacion", param.getIdProcesoPostulacion());
            sp.setParameter("idEstado", param.getIdEstado());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());
            sp.execute();
            List<SolicitudDto> list = new ArrayList<SolicitudDto>();
            List<Object[]> spResult =sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    SolicitudDto temp = new SolicitudDto();
                    temp.setIdSolicitud((Integer) row[0]);
                    temp.setIdProcesoPostulacion((Integer) row[1]);
                    temp.setIdTipoSolicitud((Integer) row[2]);
                    temp.setTipoSolicitudDesc((String) row[3]);
                    temp.setFechaInicioSolicitud((Date) row[4]);
                    temp.setFechaFinSolicitud((Date) row[5]);
                    temp.setAprobado((Boolean) row[6]);
                    temp.setMotivoSolicitud((String) row[7]);
                    temp.setObservacion((String) row[8]);
                    temp.setIdProcesoOferta((Integer) row[9]);
                    temp.setIdUsuarioPostulacion((Integer) row[10]);
                    result.setTotalRecord((Integer) row[11]);
                    temp.setVigencia((Boolean) row[12]);
                    list.add(temp);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [03-07-2021]
     * @modificado:
     * @descripción: {Obtener una solicitud de ampliacion}
     * @param:file
     */
    @Override
    public ResultClassEntity<SolicitudDto> ObtenerSolicitudAmpliacion(SolicitudDto param) {

        ResultClassEntity<SolicitudDto> result = new ResultClassEntity<SolicitudDto>();

        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_ObtenerSolicitudAmpliacion");
            sp.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoSolicitud", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idSolicitud", param.getIdSolicitud());
            sp.setParameter("idProcesoPostulacion", param.getIdProcesoPostulacion());
            sp.setParameter("idTipoSolicitud", param.getIdTipoSolicitud());
            sp.execute();
            SolicitudDto temp = new SolicitudDto();
            List<Object[]> spResult =sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    temp.setIdSolicitud((Integer) row[0]);
                    temp.setIdProcesoPostulacion((Integer) row[1]);
                    temp.setIdTipoSolicitud((Integer) row[2]);
                    temp.setTipoSolicitudDesc((String) row[3]);
                    temp.setFechaInicioSolicitud((Date) row[4]);
                    temp.setFechaFinSolicitud((Date) row[5]);
                    temp.setAprobado((Boolean) row[6]);
                    temp.setMotivoSolicitud((String) row[7]);
                    temp.setObservacion((String) row[8]);
                    temp.setFechaRecepcionDocsFisicos((Date) row[9]);
                    temp.setAsunto((String) row[10]);
                    temp.setFlagAutorizacion((Boolean) row[11]);
                    temp.setDiaAmpliacion(row[12]==null? null : (Integer) row[12]);
                }
            }
            result.setData(temp);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la solicitud.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }


    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Listar tipo solicitud}
     * @param:file
     */
    @Override
    public ResultEntity<TipoSolicitudEntity> ListarTipoSolicitud() {
        ResultEntity<TipoSolicitudEntity> result = new ResultEntity<TipoSolicitudEntity>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_TipoSolicitud_Listar");
            sp.execute();
            TipoSolicitudEntity c = null;
            List<TipoSolicitudEntity>lstsolicitud= new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new TipoSolicitudEntity();
                    c.setIdTipoSolicitud((Integer) row[0]);
                    c.setDescripcion((String) row[1]);
                    lstsolicitud.add(c);
                }
            }
            result.setData(lstsolicitud);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los tipos de solicitud.");
            return result;
        } catch (Exception e) {
            log.error("SolicitudRepositoryImpl - ListarTipoSolicitud", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Listar tipo documento}
     * @param:file
     */
    @Override
    public ResultEntity<TipoDocumentoEntity> ListarTipoDocumento() {
        return null;
        /*
        ResultEntity<TipoDocumentoEntity> result = new ResultEntity<TipoDocumentoEntity>();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_TipoDocumento_Listar()}");
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            ResultSet rs = pstm.executeQuery();
            TipoDocumentoEntity c = null;
            List<TipoDocumentoEntity>lstsolicitud= new ArrayList<>();
            while(rs.next()){
                c = new TipoDocumentoEntity();
                c.setIdTipoDocumento(rs.getInt("NU_ID_TIPO_DOCUMENTO"));
                c.setDescripcion(rs.getString("TX_DESCRIPCION"));
                lstsolicitud.add(c);
            }
            result.setData(lstsolicitud);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los tipos de documento.");
            return result;
        } catch (Exception e) {
            log.error("SolicitudRepositoryImpl - ListarTipoDocumento", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("SolicitudRepositoryImpl - ListarTipoDocumento", e2.getMessage());
                result.setIsSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {descargar declaracion jurada}
     * @param:file
     */
    @Override
    public ResultArchivoEntity DescagarDeclaracionJurada() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/Solicitud/DeclaracionJurada.docx");
            File fileCopi = File.createTempFile("DeclaracionJurada",".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("DeclaracionJurada.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la declaracion jurada.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Solicitud - DescagarDeclaracionJurada", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Descargar propuesta exploracion}
     * @param:file
     */
    @Override
    public ResultArchivoEntity DescargarPropuestaExploracion() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/Solicitud/FormatoExploracion.docx");
            File fileCopi = File.createTempFile("FormatoExploracion",".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("FormatoExploracion.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la formato de exploracion.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Solicitud - DescargarPropuestaExploracion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Descargar formato de aprobacion}
     * @param:file
     */
    @Override
    public ResultArchivoEntity DescargarFormatoAprobacion() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/AutorizacionExploracion.docx");
            File fileCopi = File.createTempFile("AutorizacionExploracion",".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("AutorizacionExploracion.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la autorizacion de exploracion.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Solicitud - DescargarFormatoAprobacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Se obtiene al nuevo usuario ganador, luego de ser aprobada una inpugnacion al anterior ganador}
     * @param:FiltroSolicitudEntity
     */
    @Override
    public ResultClassEntity<ImpugnacionNuevoGanadorEntity> ObtenerNuevoUsuarioGanador(FiltroSolicitudEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Solicitud_ObtenerNuevoUsuarioGanador");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolicitud", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOferta", param.getIdProcesoOferta());
            sp.setParameter("idSolicitud", param.getIdSolicitud());
            sp.execute();
            List<ImpugnacionNuevoGanadorEntity> list = new ArrayList<ImpugnacionNuevoGanadorEntity>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    ImpugnacionNuevoGanadorEntity obj = new ImpugnacionNuevoGanadorEntity();
                    obj.setIdProcesoOferta((Integer) row[0]);
                    obj.setIdProcesoPostulacion((Integer) row[1]);
                    obj.setIdResultadoPP((Integer) row[2]);
                    obj.setNotaTotal((Integer) row[3]);
                    obj.setGanador((Boolean) row[4]);
                    obj.setIdStatusPostulacion((Integer) row[5]);
                    obj.setDescripcionStatusProceso((String) row[6]);
                    obj.setIdSolicitud((Integer) row[7]);
                    obj.setNombreUsuarioPostulante((String) row[8]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultArchivoEntity DescagarPlantillaResolucionResumen() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/Solicitud/PlantillaResolucionResumen.docx");
            File fileCopi = File.createTempFile("PlantillaResolucionResumen",".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("PlantillaResolucionResumen.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la Plantilla de Resolución y Resumen.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Solicitud - DescagarDeclaracionJurada", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
}
