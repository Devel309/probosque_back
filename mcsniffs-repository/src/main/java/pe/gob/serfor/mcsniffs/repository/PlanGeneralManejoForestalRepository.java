package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoForestalContratoDto;
import pe.gob.serfor.mcsniffs.entity.PlanGeneralManejoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.UbigeoArffsEntity;

import java.util.List;

public interface PlanGeneralManejoForestalRepository {
    ResultClassEntity registrarPlanManejoForestal(List<PlanGeneralManejoForestalEntity> list) throws Exception;


    //ResultClassEntity<PlanManejoForestalContratoDto> obtenerContrato(Contrat idContrato) throws Exception;
    ResultClassEntity obtenerContrato(ContratoEntity obj) throws Exception;

    ResultClassEntity<List<PlanManejoForestalContratoDto>> listarFiltroContrato() throws Exception;

    ResultEntity<PlanManejoContratoEntity>listarContratosVigentes(Integer idContrato);
    ResultClassEntity actualizarPlanManejoForestal(PlanGeneralManejoForestalEntity obj);

    ResultEntity<UbigeoArffsEntity>listarUbigeoContratos(Integer idContrato);
}
