package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralPlanificacionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface InformacionGeneralPlanificacionBosqueRepository {
    ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralPlanificacionBosqueEntity param) throws Exception;
}
