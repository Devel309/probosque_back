package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface PlanGeneralManejoRepository {
     ResultClassEntity RegistrarResumenEjecutivoEscalaAlta(InformacionGeneralEntity informacionGeneral, MultipartFile file,Boolean accion) throws Exception;
     ResultClassEntity ActualizarResumenEjecutivoEscalaAlta(InformacionGeneralEntity informacionGeneral, MultipartFile file,Boolean accion) throws Exception;
     ResultClassEntity ObtenerResumenEjecutivoEscalaAlta(InformacionGeneralEntity informacionGeneral) throws Exception;
     ResultClassEntity ActualizarDuracionResumenEjecutivo(InformacionGeneralEntity informacionGeneral) throws Exception;
     ResultClassEntity ActualizarAspectoComplementarioPlanManejo(PlanManejoEntity param) throws Exception;
     ResultClassEntity ObtenerPlanManejo(PlanManejoEntity param)throws Exception;
}
