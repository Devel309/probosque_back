package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;

public interface ArchivoRepository {
    ResultEntity<ArchivoEntity> registroArchivo(ArchivoEntity request);
    ResultEntity<ArchivoEntity> listarArchivo(ArchivoEntity request);
    ResultClassEntity<ArchivoEntity> obtenerArchivo(Integer IdArchivo);

    ResultClassEntity<ArchivoEntity> ObtenerArchivoGeneral(Integer idArchivo) throws Exception;

    ResultClassEntity<Integer> EliminarArchivoGeneral(Integer idArchivo, Integer idUsuario) throws Exception;
    ResultClassEntity<Integer> RegistrarArchivoGeneral(ArchivoEntity request) throws Exception;
    ResultClassEntity DescargarArchivoGeneral(ArchivoEntity param) ;
    ResultClassEntity downloadFile(String name) ;
    ResultClassEntity actualizarArchivo(ArchivoEntity request);

    ResultClassEntity eliminarArchivoGeometria(ArchivoEntity param) throws Exception;

}
