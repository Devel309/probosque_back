package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Colindancia.ColindanciaPredioDto;

public interface ColindanciaPredioRepository {
    
    ResultClassEntity registrarColindanciaPredio(ColindanciaPredioDto dto) throws Exception;
    List<ColindanciaPredioDto> listarColindanciaPredio(ColindanciaPredioDto dto) throws Exception;
}
