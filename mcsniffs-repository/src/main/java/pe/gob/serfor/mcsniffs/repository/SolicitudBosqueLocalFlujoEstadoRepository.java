package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFlujoEstadoDto;

public interface SolicitudBosqueLocalFlujoEstadoRepository {

    ResultClassEntity registrarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalFlujoEstadoDto dto) throws Exception;
    
}
