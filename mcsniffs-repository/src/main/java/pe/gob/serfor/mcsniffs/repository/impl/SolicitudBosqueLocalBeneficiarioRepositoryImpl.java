package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalBeneficiarioEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalBeneficarioRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudBosqueLocalBeneficiarioRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalBeneficarioRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalBeneficiarioRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalBeneficiarioEntity> lista = new ArrayList<SolicitudBosqueLocalBeneficiarioEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Beneficiario_Listar");


            processStored.registerStoredProcedureParameter("idBosqueLocalBeneficiario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idBosqueLocalBeneficiario",obj.getIdBosqueLocalBeneficiario());
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());
            processStored.setParameter("pageNum",obj.getPageNum());
            processStored.setParameter("pageSize",obj.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalBeneficiarioEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalBeneficiarioEntity();
                        c.setIdBosqueLocalBeneficiario(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolBosqueLocal(row[1]==null?null:(Integer) row[1]);
                        c.setTipoDocumento(row[2]==null?null:(String) row[2]);
                        c.setNumeroDocumento(row[3]==null?null:(String) row[3]);
                        c.setNombreCompleto(row[4]==null?null:(String) row[4]);
                        c.setResidencia(row[5]==null?null:(String) row[5]);
                        c.setCondicion(row[6]==null?null:(String) row[6]);
                        c.setTelefono(row[7]==null?null:(String) row[7]);
                        c.setEmail(row[8]==null?null:(String) row[8]);
                        c.setCargaFamiliar(row[9]==null?null:(String) row[9]);
                        c.setIdCertificado(row[10]==null?null:(Integer) row[10]);
                        c.setDeclaracionJurada(row[11]==null?null:(Integer) row[11]);
                        c.setEstado(row[12]==null?null:(String) row[12]);
                        c.setFlagActualizarResidencia(row[14]==null?null:(Boolean) row[14]);
                        c.setFlagListaFinal(row[15]==null?null:(Boolean) row[15]);
                        c.setObservaciones(row[16]==null?null:(String) row[16]);
                        c.setUbigeoTexto(row[17]==null?null:(String) row[17]);
                        c.setTieneOposicion(row[18]==null?null:(Boolean) row[18]);
                        c.setIdAdjuntoOposicion(row[19]==null?null:(Integer) row[19]);
                        c.setFlagTieneOtrasSolicitudes(row[20]==null?null:(Integer) row[20]);
                        c.setCondicionDescripcion(row[21]==null?null:(String) row[21]);
                        lista.add(c);
                        result.setTotalRecord((Integer) row[13]);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar solicitud bosque local beneficiario correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar solicitud bosque local beneficiario");
            return  result;
        }
    }

   /*@Override
    public ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Beneficiario_Registrar");
            processStored.registerStoredProcedureParameter("idBosqueLocalBeneficiario", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCompleto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("residencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("condicion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefono", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("email", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cargaFamiliar", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCertificado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("declaracionJurada", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idBosqueLocalBeneficiario", obj.getIdBosqueLocalBeneficiario());
            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("tipoDocumento", obj.getTipoDocumento());
            processStored.setParameter("numeroDocumento", obj.getNumeroDocumento());
            processStored.setParameter("nombreCompleto", obj.getNombreCompleto());
            processStored.setParameter("residencia", obj.getResidencia());
            processStored.setParameter("condicion", obj.getCondicion());
            processStored.setParameter("telefono", obj.getTelefono());
            processStored.setParameter("email", obj.getEmail());
            processStored.setParameter("cargaFamiliar", obj.getCargaFamiliar());
            processStored.setParameter("idCertificado", obj.getIdCertificado());
            processStored.setParameter("declaracionJurada", obj.getDeclaracionJurada());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idBosqueLocalBeneficiario = (Integer) processStored.getOutputParameterValue("idBosqueLocalBeneficiario");
            obj.setIdSolBosqueLocal(idBosqueLocalBeneficiario);
            result.setCodigo(idBosqueLocalBeneficiario);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la actividad de la solicitud bosque Local Beneficiario correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la actividad de la solicitud Bosque Local Beneficiario");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }
    */
  
    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(List<SolicitudBosqueLocalBeneficiarioEntity> obj) throws Exception{
   // public ResultClassEntity perfilOpcionAccionRegistrar(PerfilOpcionAccionEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            for(SolicitudBosqueLocalBeneficiarioEntity p: obj){
                
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Beneficiario_Registrar");
                processStored.registerStoredProcedureParameter("idBosqueLocalBeneficiario", Integer.class, ParameterMode.INOUT);
                processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("nombreCompleto", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("residencia", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("condicion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("telefono", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("email", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("cargaFamiliar", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idCertificado", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("declaracionJurada", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("flagActualizarResidencia", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("flagListaFinal", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("ubigeoTexto", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tieneOposicion", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idAdjuntoOposicion", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idBosqueLocalBeneficiario", p.getIdBosqueLocalBeneficiario());
                processStored.setParameter("idSolBosqueLocal", p.getIdSolBosqueLocal());
                processStored.setParameter("tipoDocumento", p.getTipoDocumento());
                processStored.setParameter("numeroDocumento", p.getNumeroDocumento());
                processStored.setParameter("nombreCompleto", p.getNombreCompleto());
                processStored.setParameter("residencia", p.getResidencia());
                processStored.setParameter("condicion", p.getCondicion());
                processStored.setParameter("telefono", p.getTelefono());
                processStored.setParameter("email", p.getEmail());
                processStored.setParameter("cargaFamiliar", p.getCargaFamiliar());
                processStored.setParameter("idCertificado", p.getIdCertificado());
                processStored.setParameter("declaracionJurada", p.getDeclaracionJurada());
                processStored.setParameter("flagActualizarResidencia", p.getFlagActualizarResidencia());
                processStored.setParameter("idUsuario", p.getIdUsuarioRegistro());
                processStored.setParameter("flagListaFinal", p.getFlagListaFinal());
                processStored.setParameter("observaciones", p.getObservaciones());
                processStored.setParameter("ubigeoTexto", p.getUbigeoTexto());
                processStored.setParameter("tieneOposicion", p.getTieneOposicion());
                processStored.setParameter("idAdjuntoOposicion", p.getIdAdjuntoOposicion());

                processStored.execute();
                result.setSuccess(true);
                result.setData(p);
            }            
            result.setMessage("Se registró la actividad de la solicitud bosque Local Beneficiario correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la actividad de la solicitud Bosque Local Beneficiario");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }


    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Beneficiario_Eliminar");
            processStored.registerStoredProcedureParameter("idBosqueLocalBeneficiario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idBosqueLocalBeneficiario", obj.getIdBosqueLocalBeneficiario());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se elimino la actividad de la solicitud bosque Local Beneficiario correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo aliminar la actividad de la solicitud Bosque Local Beneficario");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

}