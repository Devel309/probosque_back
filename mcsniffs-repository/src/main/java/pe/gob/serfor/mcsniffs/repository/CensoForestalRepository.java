package pe.gob.serfor.mcsniffs.repository;

import java.util.List;
import java.util.stream.Stream;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CensoForestal.CensoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

public interface CensoForestalRepository {
    Integer RegistrarCensoForestal(int idUsuario, String Correlativo,String tipoCenso) throws Exception;
    
    List<ParametroValorEntity> ListarTipoBosque() throws Exception;
    
    List<ParametroValorEntity> ListarEspecieNombreComun() throws Exception;
    
    List<ParametroValorEntity> ListarEspecieNombreCientifico() throws Exception;
    
    CensoForestalDto CensoForestalObtener() throws Exception;
    
    List<ContratoTHDto> ListaTHContratosObtener(ContratoTHDto data) throws Exception;
    
    List<PlanManejoAppEntity> ListaPlanManejoPorContratoObtener(Integer idContrato,String tipoPlanManejo,String tipoCensoForestal) throws Exception;

    DatosTHDto ContratoObtener(Integer idContrato) throws Exception;

    List<ListaEspecieDto> ListaTipoRecursoForestal(String tipoEspecie,String tipoCenso) throws Exception;

    List<ListaEspecieDto> ListaTipoRecursoForestalManejo(Integer idPlanManejo,String tipoCenso,String tipoEspecie) throws Exception;

    List<ListaEspecieDto> ListaResumenEspecies(Integer idPlanManejo,String tipoCenso,String tipoEspecie) throws Exception;

    List<ListaEspecieDto> ListaResumenEspeciesInventario(Integer idPlanManejo,String tipoRecurso,String tipoInventario) throws Exception;


    List<CensoForestalDetalleEntity> ListarCensoForestalDetalle(Integer idPlanManejo,String tipoCenso,String tipoRecurso) throws Exception;

    List<CensoForestalEntity> SincronizacionCenso (Integer idPlanManejo,String tipoCenso,String tipoRecurso) throws Exception;


    List<Anexo2CensoEntity> ListarCensoForestalAnexo2(Integer idPlanManejo, String tipoEspecie) throws Exception;

    ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo2VolumenTotal(Integer idPlanManejo, String tipoEspecie);

    ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo3(Integer idPlanManejo, String tipoEspecie);

    List<PoligonosDto> ContratolistaPoligono(Integer idPlanDeManejo) throws Exception;

    ResultEntity<CensoForestalDetalleDto> listarEspecieXCenso(CensoForestalDto censoForestalDto) throws Exception;

    List<ListaEspecieDto> ListaEspecieDto(Integer idPlanDeManejo, String tipoPlan) throws Exception;

    ResultadosRecursosForestalesMaderablesDto    ObtenerDatosCensoComercial(Integer idPlanDeManejo, String tipoPlan) throws Exception;

    List<ResultadosRFMTabla> ResultadosRecursosForestalesMaderablesDto(Integer idPlanDeManejo, String tipoPlan,String  areaPC) throws Exception;

    VolumenCortaDto    ObtenerDatosVolumenCorta(Integer idPlanDeManejo,String tipoPlan) throws Exception;

    List<ResultadosVolumenDeCortaTabla>  ResultadosVolumenDeCorta(Integer idPlanDeManejo, String tipoPlan,Integer numeroPc) throws Exception;

    List<Anexo2Dto>  ResultadosPOConcesionesAnexos2(Integer idPlanManejo, String tipoPlan,Integer tipoProceso,String unidadMedida) throws Exception;

    List<Anexo2PGMFDto>  ResultadosFormatoPGMFAnexo2(Integer idPlanManejo, String tipoPlan,String idTipoBosque,String  nombreBosque) throws Exception;

    List<Anexo3PGMFDto>  ResultadosFormatoPGMFAnexo3(Integer idPlanManejo, String tipoPlan,String idTipoBosque,String nombreBosque) throws Exception;

    List<Anexo3PGMFDto>  PGMFInventarioExploracionFustales(Integer idPlanManejo, String tipoPlan,String idTipoBosque) throws Exception;

    List<ResultadosEspeciesMuestreoDto>  ResultadosEspecieMuestreo(Integer idPlanManejo, String tipoPlan) throws Exception;

    List<ResultadosEspeciesMuestreoDto>  ResultadosEspecieFrutales(Integer idPlanManejo, String tipoPlan) throws Exception;

    List<AprovechamientoRFNMDto> ResultadosAprovechamientoRFNM(Integer idPlanManejo, String tipoPlan) throws Exception;

    ResultClassEntity listarResultadosFustales(Integer idPlanManejo, String tipoPlan) throws Exception;

    Integer obtenerCensoForestalCabecera(Integer idPlanDeManejo)  throws Exception;

    List<ListaEspecieDto>  ListaEspeciesInventariadasMaderables(Integer idPlanDeManejo, String tipoPlan) throws Exception;

    List<Anexo2PGMFDto>  PGMFInventarioExploracionVolumenP(Integer idPlanManejo, String tipoPlan,String idTipoBosque) throws Exception;

    List<TablaAnexo3PGMF>   ObtenerTablaAnexo3PGMFVolumenPotencial(Integer idPlanManejo, String tipoPlan) throws Exception;

    List<ResultadosAnexo3PGMF>  ResultadosAnexo3PGMFVolumenPotencial(Integer idPlanManejo, String tipoPlan,String  bloque,Integer idTipoBosque,Double areaTipoBosque) throws Exception;

    List<TablaAnexo3PGMF>   ObtenerTablaAnexo3PGMFFustales(Integer idPlanManejo, String tipoPlan) throws Exception;
    List<Anexo2Dto>  ResultadosAnexo7NoMaderable(Integer idPlanManejo, String tipoPlan) throws Exception;
    ResultadosArbolesAprovechablesMaderables ResultadosArbolesAprovechables(Integer idPlanManejo, String tipoPlan) throws Exception;
    List<ResultadosAnexo3PGMF>  ResultadosAnexo3PGMFFustales(Integer idPlanManejo, String tipoPlan,String  bloque,Integer idTipoBosque,Double areaTipoBosque) throws Exception;
    List<ResultadosAnexo6> ResultadosAnexo6(Integer idPlanManejo, String tipoPlan) throws Exception;
    ResultadosArbolesAprovechablesMaderables ResultadosArbolesAprovechablesFustales(Integer idPlanManejo, String tipoPlan) throws Exception;
    List<OrdenamientoPoligonoDto>  ListaOrdenamientoPoligonos(Integer idPlanDeManejo,Integer idArchivo) throws Exception;
    ResultadosArbolesAprovechablesMaderables ResultadosAprovechableForestalNoMaderable(Integer idPlanManejo, String tipoPlan) throws Exception;
    List<ResultadosRFMTabla>  ResultadosCensoComercialAprovechamientoNoMaderable(Integer idPlanDeManejo, String tipoPlan,String  areaPC) throws Exception;
    List<ContratoTHDto> ListarContratoTHMovilObtener(ContratoTHDto data) throws Exception;
    ResultClassEntity ListarProyeccionCosecha(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity listarVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity listarEspecieVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan);

    ResultClassEntity listarEspecieVolumenCortaAnualPermisible(Integer idPlanManejo, String tipoPlan);

    List<Anexo2Dto> ResultadosBuscarAnexo7NoMaderable(Integer idPlanDeManejo, String tipoPlan, Anexo2Dto data)throws Exception;

    List<List<Anexo2PGMFDto>> ResultadosPMFICAnexo3(Integer idPlanDeManejo, String tipoPlan) throws Exception;

    List<List<Anexo3PGMFDto>> ResultadosPMFICAnexo4(Integer idPlanDeManejo, String tipoPlan)throws Exception;

    ResultClassEntity actualizarCensoForestal(CensoForestalEntity request) throws Exception;
    ResultClassEntity  AprovechamientoForestalNMResumen(Integer idPlanManejo, String tipoPlan,Integer tipoProceso) throws Exception;
    ResultClassEntity  ResultadoInventarioPorParcela(Integer idPlanManejo, String tipoPlan,Integer tipoProceso) throws Exception;
    List<Anexo2Dto>  ResultadoInventarioParcelaAgrupadoPorEspecie(Integer idPlanManejo, String tipoPlan,Integer tipoProceso,String nombreEspecie) throws Exception;
    //Stream<InfBasicaAereaDetalleDto> stream();
}
