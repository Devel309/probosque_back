package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.*;

public interface SolPlantacionForestalRepository {// } extends JpaRepository<SolicitudPlantacionForestalEntity, Integer>
                                                  // {

        ResultEntity<SolPlantacionForestalEntity> registroSolicitudPlantacionForestal(
                        SolPlantacionForestalEntity request);

        ResultClassEntity clonarPlantacionForestal (SolPlantacionForestalDto request);

        ResultClassEntity validarClonPlantacionForestal(SolPlantacionForestalDto request);

        ResultClassEntity eliminarSolicitudPlantacionForestal(SolPlantacionForestalEntity request) throws Exception;

        ResultClassEntity listarSolicitudPlantacionForestal(SolPlantacionForestalEntity request);


        ResultClassEntity actualizarSolicitudPlantacionForestal(SolPlantacionForestalEntity request);

        ResultEntity<Dropdown> listarTipoComboSistemaPlantacionForestal(SistemaPlantacionEntity request);

        ResultClassEntity listarBandejaSolicitudPlantacionForestal(
                SolPlantacionForestalDto request);

        ResultEntity<SolPlantacionForestalEstadoEntity> RegistrarSolicitudPlantacionForestalEstado(
                        SolPlantacionForestalEstadoEntity request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> registroSolicitudPlantacionForestalAreaPlantada(
                        List<SolPlantacionForestalAreaPlantadaEntity> request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> eliminarSolicitudPlantacionForestalAreaPlantada(
                        SolPlantacionForestalAreaPlantadaEntity request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> listarSolicitudPlantacionForestalAreaPlantada(
                        SolPlantacionForestalAreaPlantadaEntity request);

        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> obtenerSolicitudPlantacionForestalAreaPlantada(
                        SolPlantacionForestalAreaPlantadaEntity request);

        ResultClassEntity registroSolPlantacionForestalArea(
                        SolPlantacionForestalAreaEntity request);

        ResultEntity<SolPlantacionForestalAreaEntity> listarSolPlantacionForestalArea(
                        SolPlantacionForestalAreaEntity request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> registroSolicitudPlantacionForestalDetCoord(
                        List<SolPlantacionForestalDetCoordenadaEntity> request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> eliminarSolicitudPlantacionForestalDetCoord(
                        SolPlantacionForestalDetCoordenadaEntity request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> listarSolicitudPlantacionForestalDetCoord(
                        SolPlantacionForestalDetCoordenadaEntity request);

        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> obtenerSolicitudPlantacionForestalDetCoord(
                        SolPlantacionForestalDetCoordenadaEntity request);

        ResultEntity<SolPlantacionForestalDetalleEntity> registroSolicitudPlantacionForestalDet(
                        List<SolPlantacionForestalDetalleEntity> request);

        ResultEntity<SolPlantacionForestalDetalleEntity> eliminarSolicitudPlantacionForestalDet(
                        SolPlantacionForestalDetalleEntity request);

        ResultEntity<SolPlantacionForestalDetalleEntity> listarSolicitudPlantacionForestalDet(
                        SolPlantacionForestalDetalleEntity request);

        ResultEntity<SolPlantacionForestalDetalleEntity> obtenerSolicitudPlantacionForestalDet(
                        SolPlantacionForestalDetalleEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> registroSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> eliminarSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> listarSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalAnexoEntity> obtenerSolPlantacionForestalAnexo(
                        SolPlantacionForestalAnexoEntity request);

        ResultEntity<SolPlantacionForestalObservacionEntity> registroSolPlantacionForestalObservacion(
                        List<SolPlantacionForestalObservacionEntity> request);

        ResultEntity<SolPlantacionForestalObservacionEntity> eliminarSolPlantacionForestalObservacion(
                        SolPlantacionForestalObservacionEntity request);

        ResultEntity<SolPlantacionForestalObservacionEntity> listarSolPlantacionForestalObservacion(
                        SolPlantacionForestalObservacionEntity request);

        ResultEntity<SolPlantacionForestalObservacionEntity> obtenerSolPlantacionForestalObservacion(
                        SolPlantacionForestalObservacionEntity request);

        ResultClassEntity listarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj);

        ResultClassEntity registrarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj);

        ResultClassEntity actualizarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj);

        ResultClassEntity eliminarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj);

        ResultClassEntity registrarSolPlantacacionForestal(SolicitudPlantacionForestalEntity request);

        ResultClassEntity listarEspeciesSistemaPlantacion(Integer id);

        ResultClassEntity registrarSolPlantacionForestalDetalle(List<SolicitudPlantacionForestalDetalleEntity> request);

        ResultClassEntity registrarAreaBloque(AreaBloqueEntity request);

        ResultClassEntity listarAreaBloque(AreaBloqueEntity request);

        ResultClassEntity listarListaBloque(AreaBloqueEntity request);

        ResultClassEntity registrarSolPlantacionForestalPersona(List<SolPlantacionForestalPersonaEntity> request);

        ResultClassEntity listarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request);

        ResultClassEntity eliminarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request);

        ResultClassEntity obtenerSolicitudPlantacionForestal(SolPlantacionForestalEntity request);
}
