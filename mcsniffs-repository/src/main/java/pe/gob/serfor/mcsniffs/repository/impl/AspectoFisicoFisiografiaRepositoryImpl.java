package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.AspectoFisicoFisiografiaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AspectoFisicoFisiografiaRepositoryImpl extends JdbcDaoSupport implements AspectoFisicoFisiografiaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(AspectoFisicoFisiografiaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AspectoFisicoFisiografia_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", aspectoFisicoFisiografiaEntity.getIdPlanManejo());
            processStored.execute();

            //crear el listado
            List<AspectoFisicoFisiografiaEntity> listAspectoFisicoFisiografiaEntity = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    AspectoFisicoFisiografiaEntity aspecto = new AspectoFisicoFisiografiaEntity();
                    aspecto.setIdApectofisicofisiografia((Integer) row[0]);
                    aspecto.setIdPlanManejo((Integer) row[1]);
                    aspecto.setIdTipounidadfisiografica((String) row[2]);
                    aspecto.setTipounidadfisiografica((String) row[3]);
                    aspecto.setEspecificacion((String) row[4]);
                    aspecto.setArea( (BigDecimal) row[5] == null ? null: ((BigDecimal) row[5]).doubleValue());
                    aspecto.setAccion((Boolean) row[6]);
                    aspecto.setDescripcion((String) row[7]);
                    listAspectoFisicoFisiografiaEntity.add(aspecto);
                }
            }

            result.setData(listAspectoFisicoFisiografiaEntity);
            result.setSuccess(true);
            result.setMessage("Se listó el aspecto físico fisiografía con éxito.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarAspectoFisicoFisiografia(List<AspectoFisicoFisiografiaEntity> listAspectoFisicoFisiografiaEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            for(AspectoFisicoFisiografiaEntity p:listAspectoFisicoFisiografiaEntity){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AspectoFisicoFisiografia_Registro");
                processStored.registerStoredProcedureParameter("idAspectofisicofisiografia", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipounidadfisiografica", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipounidadfisiografica", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("especificacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("area", Double.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idAspectofisicofisiografia", p.getIdApectofisicofisiografia()==null ? 0:p.getIdApectofisicofisiografia());
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("idTipounidadfisiografica", p.getIdTipounidadfisiografica() == null? "":p.getIdTipounidadfisiografica());
                processStored.setParameter("tipounidadfisiografica", p.getTipounidadfisiografica()== null? "":p.getTipounidadfisiografica());
                processStored.setParameter("especificacion", p.getEspecificacion());
                processStored.setParameter("area", p.getArea());
                processStored.setParameter("accion", p.getAccion());
                processStored.setParameter("descripcion", p.getDescripcion());
                processStored.setParameter("idUsuario", p.getIdUsuarioRegistro());

                processStored.execute();
            }

            result.setData(listAspectoFisicoFisiografiaEntity);
            result.setSuccess(true);
            result.setMessage("Se registró el aspecto Físico Fisiografía correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity EliminarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AspectoFisicoFisiografia_Eliminar");
            processStored.registerStoredProcedureParameter("idAspectofisicofisiografia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAspectofisicofisiografia", aspectoFisicoFisiografiaEntity.getIdApectofisicofisiografia());
            processStored.setParameter("idUsuarioElimina", aspectoFisicoFisiografiaEntity.getIdUsuarioElimina());
            processStored.execute();
            result.setData(aspectoFisicoFisiografiaEntity);
            result.setSuccess(true);
            result.setMessage("Se eliminó el aspecto Físico Fisiografía Correctamente");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity ConfigurarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AspectoFisicoFisiografia_Configurar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoParametro", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", aspectoFisicoFisiografiaEntity.getIdPlanManejo());
            processStored.setParameter("codigoParametro", "TUFAF");
            processStored.execute();
            result.setData(aspectoFisicoFisiografiaEntity);
            result.setSuccess(true);
            result.setMessage("Se configuró el aspecto Físico Fisiografía Correctamente");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

}
