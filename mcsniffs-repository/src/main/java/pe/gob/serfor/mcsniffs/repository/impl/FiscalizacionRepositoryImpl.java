package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiltroFiscalizacionDto;
import pe.gob.serfor.mcsniffs.entity.Fiscalizacion.FiscalizacionDto;
import pe.gob.serfor.mcsniffs.repository.FiscalizacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class FiscalizacionRepositoryImpl extends JdbcDaoSupport implements FiscalizacionRepository{
    /**
     * @autor: JaquelineDB [28-07-2021]
     * @modificado:
     * @descripción: {Servicio donde generan las fiscalizaciones a los contratos firmados}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(FiscalizacionRepositoryImpl.class);


    
    @Override
    public ResultClassEntity registrarFiscalizacion(FiscalizacionDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Fiscalizacion_Registrar");
            processStored.registerStoredProcedureParameter("idFiscalizacion", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resultado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("comentario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroInforme", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInforme", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("emite", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idFiscalizacion", dto.getIdFiscalizacion());
            processStored.setParameter("idContrato", dto.getIdContrato());
            processStored.setParameter("asunto", dto.getAsunto());
            processStored.setParameter("resultado", dto.getResultado());
            processStored.setParameter("comentario", dto.getComentario());
            processStored.setParameter("numeroInforme", dto.getNumeroInforme());
            processStored.setParameter("fechaInforme", dto.getFechaInforme());
            processStored.setParameter("emite", dto.getEmite());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();
            Integer id = (Integer) processStored.getOutputParameterValue("idFiscalizacion");
            dto.setIdFiscalizacion(id);
            result.setSuccess(true);
            result.setData(dto);
            result.setMessage("Se registró la Fiscalización correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity AdjuntarArchivosFiscalizacion(FiscalizacionEntity obj) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            //Adjuntar Documentos
            pstm = con.prepareCall("{call pa_FiscalizacionAdjuntos_Registrar(?,?,?,?)}");
            pstm.setObject (1, obj.getIdFiscalizacion(), Types.INTEGER);
            pstm.setObject(2, obj.getIdDocumentoAdjunto(),Types.INTEGER);
            pstm.setObject(3, obj.getIdUsuarioRegistro(),Types.INTEGER);
            pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida2 = pstm.executeUpdate();
            ResultSet rsa2 = pstm.getGeneratedKeys();
            if(rsa2.next())
            {
                salida2 = rsa2.getInt(1);
            }
            result.setCodigo(salida2);
            result.setSuccess(true);
            result.setMessage("Se registró el adjunto del contrato.");
            return result;
        } catch (Exception e) {
            log.error("FiscalizacionRepositoryImpl - RegistrarFizacalizacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("FiscalizacionRepositoryImpl - RegistrarFizacalizacion", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

 
    @Override
    public ResultClassEntity listarFiscalizacion(FiscalizacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<FiscalizacionDto> list = new ArrayList<FiscalizacionDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Fiscalizacion_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTituloH", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idFiscalizacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstadoContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pagSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idProcesoPostulacion", obj.getIdProcesoPostulacion());
            processStored.setParameter("codigoTituloH",  obj.getCodigoTituloH());
            processStored.setParameter("idFiscalizacion", obj.getIdFiscalizacion());
            processStored.setParameter("codigoEstadoContrato", obj.getCodigoEstadoContrato());
            processStored.setParameter("pageNum", obj.getPageNum());
            processStored.setParameter("pagSize", obj.getPageSize());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    FiscalizacionDto temp = new FiscalizacionDto();
                    temp.setIdProcesoOferta((Integer) row[0]);
                    temp.setIdProcesoPostulacion((Integer) row[1]);
                    temp.setIdUsuarioPostulacion((Integer) row[2]);
                    temp.setIdResultadoPP((Integer) row[3]);
                    temp.setCodigoTituloH((String) row[4]);
                    temp.setIdContrato((Integer) row[5]);
                    temp.setFechaInicioContrato((Date) row[6]);
                    temp.setFechaFinContrato((Date) row[7]);
                    temp.setRegente((String) row[8]);
                    temp.setIdFiscalizacion((Integer) row[9]);
                    temp.setComentario((String) row[10]);
                    temp.setResultado((Boolean) row[11]);
                    temp.setFechaInforme((Date) row[12]);
                    temp.setCodigoEstadoContrato((String) row[13]);
                    temp.setEstadoContrato((String) row[14]);
                  
                    temp.setTipoPersonaTitular(row[15]==null?null:(String) row[15]);
                    temp.setNombreTitular(row[16]==null?null:(String) row[16]);
                    result.setTotalRecord((Integer) row[17]);
                    list.add(temp);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("FiscalizacionRepositoryImpl - Listar Fiscalizacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return result;
        }  
    }


    @Override
    public ResultEntity<FiscalizacionAdjuntosEntity> ObtenerArchivosFiscalizacion(Integer IdFiscalizacion) {
        return null;
        /*
        ResultEntity<FiscalizacionAdjuntosEntity> result = new ResultEntity<FiscalizacionAdjuntosEntity>();
        List<FiscalizacionAdjuntosEntity>lstFiles = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            ResultClassEntity<FiscalizacionResponseEntity> lstoposicion = ObtenerFiscalizacion(IdFiscalizacion);
            FiscalizacionResponseEntity fiscalizacion = lstoposicion.getData();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_FiscalizacionAdjuntos_ObtenerRutaAdjunto(?)}");
            pstm.setObject (1, IdFiscalizacion,Types.INTEGER);
            ResultSet rs = pstm.executeQuery();
            FiscalizacionAdjuntosEntity resulfile =null;
            while(rs.next()){
                resulfile = new FiscalizacionAdjuntosEntity();
                resulfile.setFizcalizacion(fiscalizacion);
                String ruta = null;
                String nombredoc=null;
                Integer idTipoDocumento =0;
                ruta = rs.getString("TX_RUTA");
                nombredoc = rs.getString("TX_NOMBRE");
                idTipoDocumento = rs.getInt("NU_ID_TIPO_DOCUMENTO");
                String descr = rs.getString("TX_DESCRIPCION");
                if(ruta!=null){
                    resulfile.setNombredocumento(nombredoc);
                    resulfile.setIdTipoDocumento(idTipoDocumento);
                    resulfile.setDescripcionDoc(descr);
                    File file = new File(ruta);
                    byte[] fileContent = Files.readAllBytes(file.toPath());
                    resulfile.setDocumento(fileContent);
                    lstFiles.add(resulfile);
                }
            }

            result.setData(lstFiles);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los adjuntos para la generacion del contrato.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - ObtenerArchivosContrato", e.getMessage());
            result.setIsSuccess(false);
            result.setData(lstFiles);
            result.setMessage("Ocurrió un error al obtener el archivo.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - ObtenerArchivosContrato", e2.getMessage());
                result.setIsSuccess(false);
                result.setMessage("Ocurrió un error al obtener el archivo.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }
    @Override
    public FiscalizacionDto ObtenerFiscalizacion(FiscalizacionDto obj) {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Fiscalizacion_ListarPorId");
            processStored.registerStoredProcedureParameter("idFiscalizacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idFiscalizacion", obj.getIdFiscalizacion());
            processStored.setParameter("idContrato", obj.getIdContrato()); 

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            FiscalizacionDto temp = new FiscalizacionDto();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    temp.setIdFiscalizacion(row[0]==null?null:(Integer) row[0]);
                    temp.setIdContrato(row[1]==null?null:(Integer) row[1]);
                    temp.setAsunto(row[2]==null?null:(String) row[2]);
                    temp.setResultado(row[3]==null?null:(Boolean) row[3]);
                    temp.setComentario(row[4]==null?null:(String) row[4]);
                    temp.setNumeroInforme(row[5]==null?null:(String) row[5]);
                    temp.setFechaInforme(row[6]==null?null:(Date) row[6]);
                    temp.setEmite(row[7]==null?null:(Boolean) row[7]);
                    temp.setCodigoEstadoContrato(row[8]==null?null:(String) row[8]);
                    temp.setNumeroExpediente(row[9]==null?null:(String) row[9]);
                    temp.setFechaPresentacion(row[10]==null?null:(Date) row[10]);
                    temp.setEstado(row[11]==null?null:(String) row[11]);                

                }
            }
            
            return temp;
        } catch (Exception e) {
            log.error("listarFiscalizacionPorId", e.getMessage());
            throw e;
        }
    
    
    }
    @Override
    public ResultArchivoEntity DescagarPlantillaFormatoFiscalizacion() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("Fiscalizacion/Plantilla_Fiscalizacion.docx");
            File fileCopi = File.createTempFile("Plantilla_Fiscalizacion",".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("Plantilla_Fiscalizacion.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la Plantilla de Formato Fiscalización.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Solicitud - DescagarDeclaracionJurada", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

}