package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ArchivoSolicitudRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Victor Matos [14-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar y obtener una solicitud de archivo}
 */

@Repository
public class ArchivoSolicitudRepositoryImpl extends JdbcDaoSupport implements ArchivoSolicitudRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ArchivoSolicitudRepositoryImpl.class);

    @Value("${smb.file.server.path}")
    private String fileServerPath;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /**
     * @autor: Miguel F. [24-08-2021]
     * @modificado:
     * @descripción: {insertar en participacion ciudadana y capacitación}
     *
     */
    @Override
    public ResultClassEntity<Boolean> archivoEliminar(Integer idArchivo) {
        
        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivo_eliminar");
            processStored.registerStoredProcedureParameter("idArchivo", int.class, ParameterMode.IN);
            processStored.setParameter("idArchivo", idArchivo);

            processStored.execute();
            result.setData(true);
            result.setSuccess(true);
            result.setMessage("eliminado correctamente - PGMFAbreviadoDetalleEliminado");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - PGMFAbreviadoDetalleEliminado", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;
    }

    @Override
    public ResultClassEntity<ArchivoEntity> insertarArchivoSolicitud(String archivoGenerado, String archivoArchivo, Integer idSolicitud, String tipoArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoEntity entity = new ArchivoEntity();
        try {
            ArchivoEntity entityArchivo = new ArchivoEntity();
            entityArchivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
            entityArchivo.setNombre(FilenameUtils.getBaseName(archivoArchivo));
            entityArchivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
            entityArchivo.setRuta(fileServerPath.concat(archivoGenerado));

            SolicitudEntity solicitudEntity = new SolicitudEntity();
            solicitudEntity.setIdSolicitud(idSolicitud);

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitud_Registrar");
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.setParameter("ruta", entityArchivo.getRuta());
            processStored.setParameter("nombre", entityArchivo.getNombre());
            processStored.setParameter("nombreGenerado", entityArchivo.getNombreGenerado());
            processStored.setParameter("extension", entityArchivo.getExtension());
            processStored.setParameter("tipoDocumento", tipoArchivo);
            processStored.setParameter("idArchivoSolicitud", solicitudEntity.getIdSolicitud());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setRuta((String) row[1]);
                    entity.setNombre((String) row[2]);
                    entity.setNombreGenerado((String) row[3]);
                    entity.setExtension((String) row[4]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se insertar archivo solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al insertar archivo solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<ArchivoEntity> actualizarArchivoSolicitud(String archivoGenerado, String archivoArchivo, Integer idSolicitud, String tipoArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoEntity entity = new ArchivoEntity();
        try {
            ArchivoEntity entityArchivo = new ArchivoEntity();
            entityArchivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
            entityArchivo.setNombre(FilenameUtils.getBaseName(archivoArchivo));
            entityArchivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
            entityArchivo.setRuta(fileServerPath.concat(archivoGenerado));

            SolicitudEntity solicitudEntity = new SolicitudEntity();
            solicitudEntity.setIdSolicitud(idSolicitud);

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitud_actualizar");
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.setParameter("ruta", entityArchivo.getRuta());
            processStored.setParameter("nombre", entityArchivo.getNombre());
            processStored.setParameter("nombreGenerado", entityArchivo.getNombreGenerado());
            processStored.setParameter("extension", entityArchivo.getExtension());
            processStored.setParameter("tipoDocumento", tipoArchivo);
            processStored.setParameter("idArchivoSolicitud", solicitudEntity.getIdSolicitud());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setRuta((String) row[1]);
                    entity.setNombre((String) row[2]);
                    entity.setNombreGenerado((String) row[3]);
                    entity.setExtension((String) row[4]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se insertar archivo solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al insertar archivo solicitud.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<ArchivoEntity> actualizarArchivoSolicitudAdjunto(String archivoGenerado, String archivoArchivo, Integer idSolicitud, String tipoArchivo, String descripcionArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoEntity entity = new ArchivoEntity();
        try {
            ArchivoEntity entityArchivo = new ArchivoEntity();
            entityArchivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
            entityArchivo.setNombre(FilenameUtils.getBaseName(archivoArchivo));
            entityArchivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
            entityArchivo.setRuta(fileServerPath.concat(archivoGenerado));

            SolicitudEntity solicitudEntity = new SolicitudEntity();
            solicitudEntity.setIdSolicitud(idSolicitud);

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitudAdjunto_actualizar");
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.setParameter("ruta", entityArchivo.getRuta());
            processStored.setParameter("nombre", entityArchivo.getNombre());
            processStored.setParameter("nombreGenerado", entityArchivo.getNombreGenerado());
            processStored.setParameter("descripcionArchivo", descripcionArchivo);
            processStored.setParameter("extension", entityArchivo.getExtension());
            processStored.setParameter("tipoDocumento", tipoArchivo);
            processStored.setParameter("idArchivoSolicitud", solicitudEntity.getIdSolicitud());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setRuta((String) row[1]);
                    entity.setNombre((String) row[2]);
                    entity.setNombreGenerado((String) row[3]);
                    entity.setDescripcionArchivo((String) row[4]);
                    entity.setExtension((String) row[5]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se insertar archivo solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al insertar archivo solicitud.");
            return result;
        }
    }


    @Override
    public ResultClassEntity<ArchivoEntity> insertarArchivoSolicitudAdjunto(String archivoGenerado, String archivoArchivo, Integer idSolicitud, String tipoArchivo, String descripcionArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoEntity entity = new ArchivoEntity();
        try {
            ArchivoEntity entityArchivo = new ArchivoEntity();
            entityArchivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
            entityArchivo.setNombre(FilenameUtils.getBaseName(archivoArchivo));
            entityArchivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
            entityArchivo.setRuta(fileServerPath.concat(archivoGenerado));

            SolicitudEntity solicitudEntity = new SolicitudEntity();
            solicitudEntity.setIdSolicitud(idSolicitud);

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitudAdjunto_Registrar");
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.setParameter("ruta", entityArchivo.getRuta());
            processStored.setParameter("nombre", entityArchivo.getNombre());
            processStored.setParameter("nombreGenerado", entityArchivo.getNombreGenerado());
            processStored.setParameter("descripcionArchivo", descripcionArchivo);
            processStored.setParameter("extension", entityArchivo.getExtension());
            processStored.setParameter("tipoDocumento", tipoArchivo);
            processStored.setParameter("idArchivoSolicitud", solicitudEntity.getIdSolicitud());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setRuta((String) row[1]);
                    entity.setNombre((String) row[2]);
                    entity.setNombreGenerado((String) row[3]);
                    entity.setDescripcionArchivo((String) row[4]);
                    entity.setExtension((String) row[5]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se insertar archivo solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al insertar archivo solicitud.");
            return result;
        }
    }

    @Override
    public List<ArchivoSolicitudEntity> obtenerArchivoSolicitud(Integer idSolicitud) {
        ResultClassEntity result = new ResultClassEntity();

        List<ArchivoSolicitudEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitud_ObtenerPorCodigo");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", idSolicitud);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setIdSolicitud((Integer) row[1]);
                    entity.setIdArchivoSolicitud((Integer) row[2]);
                    entity.setTipoDocumento((String) row[3]);
                    entity.setNombreArchivo((String) row[4]);
                    entity.setNombreGenerado((String) row[5]);
                    entity.setRutaArchivo((String) row[6]);
                    entity.setExtensionArchivo((String) row[7]);
                    lista.add(entity);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return lista;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return lista;
        }
    }


    @Override
    public List<ArchivoSolicitudEntity> obtenerArchivoSolicitudAdjunto(Integer idSolicitud) {
        ResultClassEntity result = new ResultClassEntity();

        List<ArchivoSolicitudEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitudAdjunto_ObtenerPorCodigo");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", idSolicitud);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setIdSolicitud((Integer) row[1]);
                    entity.setIdArchivoSolicitud((Integer) row[2]);
                    entity.setTipoDocumento((String) row[3]);
                    entity.setNombreArchivo((String) row[4]);
                    entity.setNombreGenerado((String) row[5]);
                    entity.setDescripcionArchivo((String) row[6]);
                    entity.setRutaArchivo((String) row[7]);
                    entity.setExtensionArchivo((String) row[8]);
                    lista.add(entity);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return lista;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return lista;
        }
    }

    @Override
    public ResultClassEntity<ArchivoSolicitudEntity> obtenerArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivoSolicitud_ObtenerPorCodigoTipo");
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.setParameter("idArchivoSolicitud", idArchivoSolicitud);
            processStored.setParameter("idSolicitud", idSolicitud);
            processStored.setParameter("tipoArchivo", tipoArchivo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setIdSolicitud((Integer) row[1]);
                    entity.setIdArchivoSolicitud((Integer) row[2]);
                    entity.setTipoDocumento((String) row[3]);
                    entity.setNombreArchivo((String) row[4]);
                    entity.setNombreGenerado((String) row[5]);
                    entity.setRutaArchivo((String) row[6]);
                    entity.setExtensionArchivo((String) row[7]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<ArchivoSolicitudEntity> obtenerArchivoSolicitudAdjunto(Integer idArchivoSolicitudAdjunto, Integer idSolicitud, String tipoArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivoSolicitudAdjunto_ObtenerPorCodigoTipo");
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.setParameter("idArchivoSolicitud", idArchivoSolicitudAdjunto);
            processStored.setParameter("idSolicitud", idSolicitud);
            processStored.setParameter("tipoArchivo", tipoArchivo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setIdSolicitud((Integer) row[1]);
                    entity.setIdArchivoSolicitud((Integer) row[2]);
                    entity.setTipoDocumento((String) row[3]);
                    entity.setNombreArchivo((String) row[4]);
                    entity.setNombreGenerado((String) row[5]);
                    entity.setRutaArchivo((String) row[6]);
                    entity.setExtensionArchivo((String) row[7]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivoSolicitud_EliminarPorCodigoTipo");
            processStored.registerStoredProcedureParameter("idArchivoSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.setParameter("idArchivoSolicitud", idArchivoSolicitud);
            processStored.setParameter("idSolicitud", idSolicitud);
            processStored.setParameter("tipoArchivo", tipoArchivo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setIdSolicitud((Integer) row[1]);
                    entity.setIdArchivoSolicitud((Integer) row[2]);
                    entity.setTipoDocumento((String) row[3]);
                    entity.setNombreArchivo((String) row[4]);
                    entity.setNombreGenerado((String) row[5]);
                    entity.setRutaArchivo((String) row[6]);
                    entity.setExtensionArchivo((String) row[7]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return result;
        }
    }


    @Override
    public ResultClassEntity eliminarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAjunto, Integer idSolicitud, String tipoArchivo) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_archivoSolicitudAdjunto_EliminarPorCodigoTipo");
            processStored.registerStoredProcedureParameter("idArchivoSolicitudAdjunto", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.setParameter("idArchivoSolicitudAdjunto", idArchivoSolicitudAjunto);
            processStored.setParameter("idSolicitud", idSolicitud);
            processStored.setParameter("tipoArchivo", tipoArchivo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {

                    entity.setIdArchivo((Integer) row[0]);
                    entity.setIdSolicitud((Integer) row[1]);
                    entity.setIdArchivoSolicitud((Integer) row[2]);
                    entity.setTipoDocumento((String) row[3]);
                    entity.setNombreArchivo((String) row[4]);
                    entity.setNombreGenerado((String) row[5]);
                    entity.setDescripcionArchivo((String) row[6]);
                    entity.setRutaArchivo((String) row[7]);
                    entity.setExtensionArchivo((String) row[8]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return result;
        }
    }

}