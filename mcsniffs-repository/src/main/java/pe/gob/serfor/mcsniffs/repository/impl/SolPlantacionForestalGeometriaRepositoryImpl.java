package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalGeometriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository("repositorySolPlantacionForestalGeometria")
public class SolPlantacionForestalGeometriaRepositoryImpl extends JdbcDaoSupport implements SolPlantacionForestalGeometriaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registrarSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalGeometria_Registrar");
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("colorCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("geometriaWKT", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("srid", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("properties", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForestGeometria", Integer.class,
                    ParameterMode.OUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForest", item.getIdSolPlantForest());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());
            processStored.setParameter("nombreCapa", item.getNombreCapa());
            processStored.setParameter("colorCapa", item.getColorCapa());
            processStored.setParameter("geometriaWKT", item.getGeometry_wkt());
            processStored.setParameter("srid", item.getSrid());
            processStored.setParameter("properties", item.getProperties());
            processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.execute();

            Integer idSolPlantForestGeometria = (Integer) processStored
                    .getOutputParameterValue("idSolPlantForestGeometria");
            item.setIdSolPlantForestGeometria(idSolPlantForestGeometria);
            result.setCodigo(idSolPlantForestGeometria);
            result.setSuccess(true);
            result.setData(item);
            result.setMessage("Se registró el polígono/geometría de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar el polígono/geometría de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity listarSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item) {

        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalGeometriaEntity> objList = new ArrayList<SolPlantacionForestalGeometriaEntity>();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalGeometria_Listar");
            processStored.registerStoredProcedureParameter("idSolPlantForestGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForestGeometria", item.getIdSolPlantForestGeometria());
            processStored.setParameter("idSolPlantForest", item.getIdSolPlantForest());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {

                    SolPlantacionForestalGeometriaEntity item2 = new SolPlantacionForestalGeometriaEntity();

                    item2.setIdSolPlantForestGeometria(((Integer) row[0]));
                    item2.setIdSolPlantForest((Integer) row[1]);
                    item2.setIdArchivo((Integer) row[2]);
                    item2.setTipoGeometria((String) row[3]);
                    item2.setCodigoGeometria((String) row[4]);
                    item2.setCodigoSeccion((String) row[5]);
                    item2.setCodigoSubSeccion((String) row[6]);
                    item2.setNombreCapa((String) row[7]);
                    item2.setColorCapa((String) row[8]);
                    item2.setGeometry_wkt((String) row[9]);
                    item2.setProperties((String) row[10]);

                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de polígonos de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de polígonos de la solicitud de concesión.");
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalGeometria_Actualizar");
            processStored.registerStoredProcedureParameter("idSolPlantForestGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("colorCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("geometriaWKT", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("srid", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForestGeometria", item.getIdSolPlantForestGeometria());
            processStored.setParameter("idSolPlantForest", item.getIdSolPlantForest());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());
            processStored.setParameter("nombreCapa", item.getNombreCapa());
            processStored.setParameter("colorCapa", item.getColorCapa());
            processStored.setParameter("geometriaWKT", item.getGeometry_wkt());
            processStored.setParameter("srid", item.getSrid());
            processStored.setParameter("idUsuarioModificacion", item.getIdUsuarioModificacion());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el polígono/geometría de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el polígono/geometría de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }

    }

    @Override
    public ResultClassEntity eliminarSolPlantacionForestalGeometriaArchivo(SolPlantacionForestalGeometriaEntity item) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalGeometriaArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idSolPlantForestGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantForestGeometria", item.getIdSolPlantForestGeometria());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("idUsuario", item.getIdUsuarioElimina());

            processStored.execute();
            result.setData(item.getIdArchivo());
            result.setSuccess(true);
            result.setMessage("Se eliminó el polígono y/o archivo de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo eliminar el polígono y/o archivo de la solicitud de concesión.");
            return result;
        }

    }

    @Override
    public ResultClassEntity obtenerSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalGeometriaEntity> objList = new ArrayList<SolPlantacionForestalGeometriaEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalGeometria_Obtener");
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForest", item.getIdSolPlantForest());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();


            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {

                    SolPlantacionForestalGeometriaEntity item2 = new SolPlantacionForestalGeometriaEntity();

                    item2.setIdSolPlantForestGeometria(((Integer) row[0]));
                    item2.setIdSolPlantForest((Integer) row[1]);
                    item2.setIdArchivo((Integer) row[2]);
                    item2.setTipoGeometria((String) row[3]);
                    item2.setCodigoGeometria((String) row[4]);
                    item2.setCodigoSeccion((String) row[5]);
                    item2.setCodigoSubSeccion((String) row[6]);
                    item2.setNombreCapa((String) row[7]);
                    item2.setColorCapa((String) row[8]);

                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el polígono de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener el polígono de la solicitud de concesión.");
            return result;
        }


    }
}
