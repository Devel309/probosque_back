package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalComiteTecnicoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalComiteTecnicoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SolicitudBosqueLocalComiteTecnicoRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalComiteTecnicoRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);
    @Override
    public ResultClassEntity listarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalComiteTecnicoEntity> lista = new ArrayList<SolicitudBosqueLocalComiteTecnicoEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_BosqueLocal_ComiteTecnicoPersona_Listar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocalComiteTecnico", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resultadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocalComiteTecnico",obj.getIdSolBosqueLocalComiteTecnico());
            processStored.setParameter("fechaPresentacion",obj.getFechaPresentacion());
            processStored.setParameter("numDocumento",obj.getNumDocumento());
            processStored.setParameter("resultadoSolicitud",obj.getResultadoSolicitud());
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalComiteTecnicoEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalComiteTecnicoEntity();

                        c.setIdSolBosqueLocalComiteTecnico(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolBosqueLocal(row[1]==null?null:(Integer) row[1]);
                        c.setIdDocAcreditacion(row[2]==null?null:(Integer) row[2]);
                        c.setEstado(row[3]==null?null:(String) row[3]);
                        c.setIdPersona(row[4]==null?null:(Integer) row[4]);
                        c.setNumDocumento(row[5]==null?null:(String) row[5]);
                        c.setPersona(row[6]==null?null:(String) row[6]);
                        c.setTelefono(row[7]==null?null:(String) row[7]);
                        c.setIdDepartamento(row[8]==null?null:(Integer) row[8]);
                        c.setDepartamento(row[9]==null?null:(String) row[9]);
                        c.setIdProvincia(row[10]==null?null:(Integer) row[10]);
                        c.setProvincia(row[11]==null?null:(String) row[11]);
                        c.setIdDistrito(row[12]==null?null:(Integer) row[12]);
                        c.setDistrito(row[13]==null?null:(String) row[13]);
                        c.setFechaPresentacion(row[14]==null?null:(Date) row[14]);
                        c.setEstadoSolicitud(row[15]==null?null:(String) row[15]);
                        c.setResultadoSolicitud(row[16]==null?null:(String) row[16]);
                        c.setPerfil(row[17]==null?null:(String) row[17]);
                        c.setEmail(row[18]==null?null:(String) row[18]);


                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar solicitud bosque local de comite técnico correctamente.");
            return result;
        } catch (Exception e) {
            log.error("ListarSolicitudBosqueLocalComiteTecnico", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar solicitud bosque de comite técnico local");
            return  result;
        }
    }
    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_ComiteTecnico_Registrar");


            processStored.registerStoredProcedureParameter("idSolBosqueLocalComiteTecnico", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDocumentoAcreditacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idSolBosqueLocalComiteTecnico", obj.getIdSolBosqueLocalComiteTecnico());
            processStored.setParameter("idPersona", obj.getIdPersona());
            processStored.setParameter("idDocumentoAcreditacion", obj.getIdDocumentoAcreditacion());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idComiteTecnicoSolicitudBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocalComiteTecnico");
            obj.setIdSolBosqueLocalComiteTecnico(idComiteTecnicoSolicitudBosqueLocal);
            result.setCodigo(idComiteTecnicoSolicitudBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el comité técnico de la solicitud de bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el comité técnico de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }


    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_BosqueLocal_ComiteTecnicoPersona_Eliminar");
            processStored.registerStoredProcedureParameter("idSolBosqueLocalComiteTecnico", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocalComiteTecnico", obj.getIdSolBosqueLocalComiteTecnico());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se elimino el registro la solicitud bosque Local comité técnico correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo aliminar la solicitud Bosque Local comité técnico");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }





}
