package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;


import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.repository.ResponsableFormacionAcademicaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ResponsableFormacionAcademicaRepositoryImpl extends JdbcDaoSupport implements ResponsableFormacionAcademicaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<ResponsableFormacionAcademicaEntity> lista = new ArrayList<>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_ResponsableFormacionAcademica_ListarFiltro");

            processStored.registerStoredProcedureParameter("idResponsableFormacionAcademica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionGrado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionEspecialidad", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idResponsableFormacionAcademica",obj.getIdResponsableFormacionAcademica());
            processStored.setParameter("idSolicitudConcesionResponsable",obj.getIdSolicitudConcesionResponsable());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("descripcionGrado", obj.getDescripcionGrado());
            processStored.setParameter("descripcionEspecialidad", obj.getDescripcionEspecialidad());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ResponsableFormacionAcademicaEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new ResponsableFormacionAcademicaEntity();
                        c.setIdResponsableFormacionAcademica(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolicitudConcesionResponsable(row[1]==null?null:(Integer) row[1]);
                        c.setIdSolicitudConcesion(row[2]==null?null:(Integer) row[2]);
                        c.setCodigoGrado(row[3]==null?null:(String) row[3]);
                        c.setDescripcionGrado(row[4]==null?null:(String) row[4]);
                        c.setCodigoEspecialidad(row[5]==null?null:(String) row[5]);
                        c.setDescripcionEspecialidad(row[6]==null?null:(String) row[6]);
                        c.setNombreUniversidad(row[7]==null?null:(String) row[7]);
                        c.setAnioTitulo(row[8]==null?null:(Integer) row[8]);

                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de formación académica del responsable de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarResponsableFormacionAcademica", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de formación académica del responsable de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_ResponsableFormacionAcademica_Registrar");
            processStored.registerStoredProcedureParameter("idResponsableFormacionAcademica", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGrado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionGrado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEspecialidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionEspecialidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreUniversidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anioTitulo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionResponsable", obj.getIdSolicitudConcesionResponsable());
            processStored.setParameter("codigoGrado", obj.getCodigoGrado());
            processStored.setParameter("descripcionGrado", obj.getDescripcionGrado());
            processStored.setParameter("codigoEspecialidad", obj.getCodigoEspecialidad());
            processStored.setParameter("descripcionEspecialidad", obj.getDescripcionEspecialidad());
            processStored.setParameter("nombreUniversidad", obj.getNombreUniversidad());
            processStored.setParameter("anioTitulo", obj.getAnioTitulo());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idResponsableFormacionAcademica = (Integer) processStored.getOutputParameterValue("idResponsableFormacionAcademica");
            obj.setIdResponsableFormacionAcademica(idResponsableFormacionAcademica);
            result.setCodigo(idResponsableFormacionAcademica);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la formación académica del responsable de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la formación académica del responsable de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_ResponsableFormacionAcademica_Actualizar");
            processStored.registerStoredProcedureParameter("idResponsableFormacionAcademica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGrado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionGrado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEspecialidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionEspecialidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreUniversidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anioTitulo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idResponsableFormacionAcademica", obj.getIdResponsableFormacionAcademica());
            processStored.setParameter("idSolicitudConcesionResponsable", obj.getIdSolicitudConcesionResponsable());
            processStored.setParameter("codigoGrado", obj.getCodigoGrado());
            processStored.setParameter("descripcionGrado", obj.getDescripcionGrado());
            processStored.setParameter("codigoEspecialidad", obj.getCodigoEspecialidad());
            processStored.setParameter("descripcionEspecialidad", obj.getDescripcionEspecialidad());
            processStored.setParameter("nombreUniversidad", obj.getNombreUniversidad());
            processStored.setParameter("anioTitulo", obj.getAnioTitulo());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó la formación académica del responsable de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar la formación académica del responsable de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionReponsable_Eliminar");
            processStored.registerStoredProcedureParameter("idResponsableFormacionAcademica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElmina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idResponsableFormacionAcademica", obj.getIdResponsableFormacionAcademica());
            processStored.setParameter("idUsuarioElmina", obj.getIdUsuarioElimina());

            processStored.execute();

            // result.setData(item);
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

}
