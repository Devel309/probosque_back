package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDto;

public interface SolicitudBosqueLocalEvaluacionRepository {

    List<SolicitudBosqueLocalEvaluacionDto> listarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto dto) throws Exception;
    ResultClassEntity eliminarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto dto) throws Exception;
    ResultClassEntity registrarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto dto) throws Exception;
}
