package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;

public interface SolicitudBosqueLocalRepository {
    ResultClassEntity listarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity registrarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity actualizarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity eliminarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity obtenerSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity actualizarEstadoSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity listarUsuarioSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);

    ResultClassEntity generarTHSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj);
}
