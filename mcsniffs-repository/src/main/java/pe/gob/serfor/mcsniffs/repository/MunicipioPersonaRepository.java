package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.MunicipioPersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;


public interface MunicipioPersonaRepository {

    ResultClassEntity listarMunicipioPersona(MunicipioPersonaEntity obj);

    
}
