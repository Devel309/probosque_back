package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionArchivoDto;
import pe.gob.serfor.mcsniffs.repository.ResolucionArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ResolucionArchivoRepositoryImpl extends JdbcDaoSupport implements ResolucionArchivoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(ResolucionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    
    @Override
    public ResultClassEntity registrarResolucionArchivo(ResolucionArchivoDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ResolucionArchivo_Registrar");
            sp.registerStoredProcedureParameter("idResolucionArchivo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);

            sp.setParameter("idResolucionArchivo", param.getIdResolucionArchivo());
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("idArchivo", param.getIdArchivo());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());                        
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se registró Resolución archivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public List<ResolucionArchivoDto> listarResolucionArchivo(ResolucionArchivoDto dto) throws Exception {
         
        List<ResolucionArchivoDto> lista = new ArrayList<ResolucionArchivoDto>();

        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ResolucionArchivo_Listar");            
            processStored.registerStoredProcedureParameter("idResolucionArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idResolucionArchivo",dto.getIdResolucionArchivo());
            processStored.setParameter("idResolucion",dto.getIdResolucion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ResolucionArchivoDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new ResolucionArchivoDto();

                        temp.setIdResolucionArchivo(row[0]==null?null:(Integer) row[0]);
                        temp.setIdResolucion(row[1]==null?null:(Integer) row[1]);
                        temp.setIdArchivo(row[2]==null?null:(Integer) row[2]);
                        temp.setTipoDocumento(row[3]==null?null:(String) row[3]);
                        temp.setNombre(row[4]==null?null:(String) row[4]);      
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacion", e.getMessage());
            throw e;
        }
    }
}
