package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.AutoridadForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudAccesoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface SolicitudAccesoRepository {


    public List<SolicitudAccesoEntity> ListarSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    public SolicitudAccesoEntity ObtenerSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    public ResultClassEntity RegistrarSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    public ResultClassEntity ActualizarRevisionSolicitudAcceso(SolicitudAccesoEntity param) throws Exception;
    ResultClassEntity listarSolicitudAccesoUsuario(SolicitudAccesoEntity solicitudAcceso);
}
