package pe.gob.serfor.mcsniffs.repository.impl;


import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContrato;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContratoDetalle;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

/**
 * @autor: Carlos Tang [20-08-2021]
 * @modificado: Harry Coa [31-08-2021]
 * @descripción: {Repository para Plan Manejo}
 */
@Repository
public class PlanManejoRepositoryImpl extends JdbcDaoSupport implements PlanManejoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(PlanManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Autowired
    private ArchivoRepository fileUp;


    @Override
    public ResultClassEntity RegistrarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try {
        StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejo_registrar");
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.INOUT);
        processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoEscala", Short.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("aspectoComplementario", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPlanManejoPadre", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPlanManejo", planManejoEntity.getIdPlanManejo());
        processStored.setParameter("descripcion", planManejoEntity.getDescripcion());
        processStored.setParameter("idTipoProceso", planManejoEntity.getIdTipoProceso());
        processStored.setParameter("idTipoPlan", planManejoEntity.getIdTipoPlan());
        processStored.setParameter("tipoEscala", planManejoEntity.getIdTipoEscala());
        processStored.setParameter("idSolicitud", planManejoEntity.getIdSolicitud());
        processStored.setParameter("aspectoComplementario", planManejoEntity.getAspectoComplementario());
        processStored.setParameter("idPlanManejoPadre", planManejoEntity.getIdPlanManejoPadre());
        processStored.setParameter("idUsuarioRegistro", planManejoEntity.getIdUsuarioRegistro());
        processStored.execute();
        Integer id = (Integer) processStored.getOutputParameterValue("idPlanManejo");
        planManejoEntity.setIdPlanManejo(id);

        result.setSuccess(true);
        result.setMessage("Se registró plan manejo correctamente.");

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MAC_CENSOFORESTAL,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MAC_CENSOFORESTAL + id,
                planManejoEntity.getIdUsuarioRegistro());

        logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

        LogAuditoriaEntity logAuditoriaEntityPlanManejo = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_PLANMANEJO,
                LogAuditoria.REGISTRAR,
                LogAuditoria.DescripcionAccion.Registrar.T_MVC_PLANMANEJO + id,
                planManejoEntity.getIdUsuarioRegistro());

        logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntityPlanManejo);


        return  result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error("PlanManejoRepository-RegistrarPlanManejo Ocurrió un error."+ e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }



    @Override
    public ResultClassEntity ListarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejo_Listar");
            sp.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            sp.setParameter("idContrato", planManejoEntity.getIdContrato());
            sp.setParameter("idTipoPlan", planManejoEntity.getIdTipoPlan());
            sp.setParameter("idTipoProceso", planManejoEntity.getIdTipoProceso());
            sp.execute();
            List<Object[]> spResultDet = sp.getResultList();

            List<PlanManejoEntity> resultDet = null;            
            if (spResultDet!= null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                PlanManejoEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new PlanManejoEntity();
                    temp.setIdPlanManejo(item[0]==null?null:(Integer) item[0]);
                    temp.setIdContrato(item[1]==null?null:(Integer) item[1]);
                    temp.setDescripcion(item[2]==null?null:(String) item[2]);
                    temp.setIdSolicitud(item[3]==null?null:(Integer) item[3]);
                    temp.setIdTipoProceso(item[4]==null?null:(Integer) item[4]);
                    temp.setIdTipoPlan(item[5]==null?null:(Integer) item[5]);
                    temp.setIdTipoEscala(item[6]==null?null:(Short) item[6]);
                    temp.setAspectoComplementario(item[7]==null?null:(String) item[7]);
                    temp.setIdPlanManejoPadre(item[8]==null?null:(Integer) item[8]);
                    temp.setDescripcionContrato(item[9]==null?null:(String) item[9]);
                    resultDet.add(temp);
                }
            }

            result.setData(resultDet);
            result.setSuccess(true);
            result.setMessage("Se listo Plan de Manejo correctamente.");
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity ActualizarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejo_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEscala", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("aspectoComplementario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoPadre", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", planManejoEntity.getIdPlanManejo());
            processStored.setParameter("descripcion", planManejoEntity.getDescripcion());
            processStored.setParameter("idSolicitud", planManejoEntity.getIdSolicitud());
            processStored.setParameter("idTipoProceso", planManejoEntity.getIdTipoProceso());
            processStored.setParameter("idTipoPlan", planManejoEntity.getIdTipoPlan());
            processStored.setParameter("tipoEscala", planManejoEntity.getTipoEscala());
            processStored.setParameter("aspectoComplementario", planManejoEntity.getAspectoComplementario());
            processStored.setParameter("idPlanManejoPadre", planManejoEntity.getIdPlanManejoPadre());
            processStored.setParameter("idUsuarioModificacion", planManejoEntity.getIdUsuarioModificacion());
            processStored.setParameter("estado", planManejoEntity.getEstado());

            processStored.execute();

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_PLANMANEJO,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PLANMANEJO + planManejoEntity.getIdPlanManejo(),
                    planManejoEntity.getIdUsuarioModificacion());

            if(planManejoEntity.getIdPlanManejo() != 0){
                //cambiar la descripción de log auditoria
            }

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);



        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró plan manejo.");
        return  result;
    }

    @Override
    public ResultClassEntity EliminarPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejo_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", planManejoEntity.getIdPlanManejo());
            processStored.setParameter("idUsuarioElimina", planManejoEntity.getIdUsuarioElimina());
            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correcamente.");
        return  result;
    }

    @Override
    public Pageable<List<PlanManejoEntity>> filtrar(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado, Page p) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PlanManejo_ListarPorFiltro");
        sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("dniElaborador", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("rucComunidad", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("nombreTitular", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("offset", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("pageSize", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortField", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortType", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPlanManejo", idPlanManejo);
        sp.setParameter("idTipoProceso", idTipoProceso); 
        sp.setParameter("idTipoPlan", idTipoPlan);
        sp.setParameter("idContrato", idContrato);
        sp.setParameter("dniElaborador", dniElaborador);
        sp.setParameter("rucComunidad", rucComunidad);
        sp.setParameter("nombreTitular", nombreTitular);
        sp.setParameter("codigoEstado", codigoEstado);
        sp.setParameter("offset", p.getOffset());
        sp.setParameter("pageSize", p.getPageSize());
        sp.setParameter("sortField", p.getSortField());
        sp.setParameter("sortType", p.getSortType());
        sp.execute();
        return setResultDataFiltrarPlanManejo(p, sp.getResultList());
    }


    private Pageable<List<PlanManejoEntity>> setResultDataFiltrarPlanManejo(Page page, List<Object[]> dataDb) throws Exception {
        Pageable<List<PlanManejoEntity>> pageable=new Pageable<>(page);
        List<PlanManejoEntity> items = new ArrayList<>();
        for (Object[] row : dataDb) {
            PlanManejoEntity item = new PlanManejoEntity();
            InformacionGeneralPlanificacionBosqueEntity info = new InformacionGeneralPlanificacionBosqueEntity();
            item.setIdPlanManejo((Integer) row[0]);
            item.setIdContrato((Integer) row[1]);
            item.setDescripcion((String) row[2]);
            item.setIdSolicitud((Integer) row[3]);
            item.setIdTipoProceso((Integer) row[4]);
            item.setIdTipoPlan((Integer) row[5]);
            item.setIdTipoEscala((short) row[6]);
            item.setAspectoComplementario((String) row[7]);
            item.setIdPlanManejoPadre((Integer) row[8]);
            item.setDescripcionContrato((String) row[9]);
            info.setDniElaborador((String) row[10]);
            info.setNroRucComunidad((String) row[11]);
            info.setNombreElaborador((String) row[12]);
            info.setApellidosElaborador((String) row[13]);
            item.setFechaRegistro((Date)(row[15]));
            item.setCodigoEstado((String)row[16]);
            item.setCodEstado((String)row[17]);
            item.setIdPermisoForestal(row[18] == null ? null : (Integer) row[18]);
            item.setCodigoUnico(row[19] == null ? null : (String) row[19]);
            item.setInfoGeneral(info);
            item.setFecha((String)(row[21]));
            items.add(item);
            pageable.setTotalRecords(SpUtil.toLong(row[20]));

        }
        pageable.setData(items);
        pageable.setSuccess(true);
        if(items.size()>0){
            pageable.setMessage("Se obtuvo data.");
        }else{
            pageable.setMessage("No se encontró data.");
        }

        return pageable;
    }

    @Override
    public Pageable<List<PlanManejoEntity>> filtrarEvaluacionPlan(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
                                                        Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado, Page p) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PlanManejo_ListarPorFiltroEvaluacion");
        sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("dniElaborador", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("rucComunidad", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("nombreTitular", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("offset", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("pageSize", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortField", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortType", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPlanManejo", idPlanManejo);
        sp.setParameter("idTipoProceso", idTipoProceso);
        sp.setParameter("idTipoPlan", idTipoPlan);
        sp.setParameter("idContrato", idContrato);
        sp.setParameter("dniElaborador", dniElaborador);
        sp.setParameter("rucComunidad", rucComunidad);
        sp.setParameter("nombreTitular", nombreTitular);
        sp.setParameter("codigoEstado", codigoEstado);
        sp.setParameter("offset", p.getOffset());
        sp.setParameter("pageSize", p.getPageSize());
        sp.setParameter("sortField", p.getSortField());
        sp.setParameter("sortType", p.getSortType());
        sp.execute();
        return setResultDataFiltrar(p, sp.getResultList());
    }

    @Override
    public Pageable<List<PlanManejoEntity>> filtrarEval(Integer idPlanManejo, Integer idTipoProceso, Integer idTipoPlan,
            Integer idContrato, String dniElaborador, String rucComunidad,String nombreTitular, String codigoEstado,String codigoUnico,String modalidadTH, Page p) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PlanManejo_ListarPorFiltroEval");
        sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("dniElaborador", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("rucComunidad", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("nombreTitular", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoUnico", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("modalidadTH", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("offset", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("pageSize", Long.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortField", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("sortType", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPlanManejo", idPlanManejo);
        sp.setParameter("idTipoProceso", idTipoProceso); 
        sp.setParameter("idTipoPlan", idTipoPlan);
        sp.setParameter("idContrato", idContrato);
        sp.setParameter("dniElaborador", dniElaborador);
        sp.setParameter("rucComunidad", rucComunidad);
        sp.setParameter("nombreTitular", nombreTitular);
        sp.setParameter("codigoEstado", codigoEstado);
        sp.setParameter("codigoUnico", codigoUnico);
        sp.setParameter("modalidadTH", modalidadTH);
        sp.setParameter("offset", p.getOffset());
        sp.setParameter("pageSize", p.getPageSize());
        sp.setParameter("sortField", p.getSortField());
        sp.setParameter("sortType", p.getSortType());
        sp.execute();
        return setResultDataFiltrar(p, sp.getResultList());
    }
    @Override
    public ResultClassEntity ObtenerPlanManejo(PlanManejoDto plan) throws Exception {
        ResultClassEntity result= new ResultClassEntity();
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PlanManejo_Obtener");
        sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPlanManejo", plan.getIdPlanManejo());
        sp.execute();

        PlanManejoEntity item = new PlanManejoEntity();
        List<Object[]> spResultDet = sp.getResultList();
        List<Object[]> spResult = sp.getResultList();
        if (spResult != null && !spResult.isEmpty()) {
            for (Object[] row : spResult) {
                InformacionGeneralPlanificacionBosqueEntity info = new InformacionGeneralPlanificacionBosqueEntity();
                item.setIdPlanManejo((Integer) row[0]);
                item.setAspectoComplementario((String) row[1]);
                item.setIdContrato((Integer) row[2]);
                item.setDescripcion((String) row[3]);
                item.setIdSolicitud((Integer) row[4]);
                item.setIdTipoProceso((Integer) row[5]);
                item.setIdTipoPlan((Integer) row[6]);
                item.setIdTipoEscala((short) row[7]);
                item.setIdPlanManejoPadre((Integer) row[8]);
                item.setDescripcionContrato((String) row[9]);
                info.setDniElaborador((String) row[10]);
                info.setNroRucComunidad((String) row[11]);
                info.setNombreElaborador((String) row[12]);
                info.setApellidosElaborador((String) row[13]);
                item.setFechaRegistro((Date)(row[15]));
                item.setCodigoEstado((String)row[16]);
                item.setInfoGeneral(info);
            }
        }
        result.setData(item);
        result.setSuccess(true);
        result.setMessage("Se obtuvieron el Plan Manejo correctamente.");
        return result;

    }
    private Pageable<List<PlanManejoEntity>> setResultDataFiltrar(Page page, List<Object[]> dataDb) throws Exception {
        Pageable<List<PlanManejoEntity>> pageable=new Pageable<>(page);
        List<PlanManejoEntity> items = new ArrayList<>();
        for (Object[] row : dataDb) {
            PlanManejoEntity item = new PlanManejoEntity();
            InformacionGeneralPlanificacionBosqueEntity info = new InformacionGeneralPlanificacionBosqueEntity();
            item.setIdPlanManejo((Integer) row[0]);
            item.setIdContrato((Integer) row[1]);
            item.setDescripcion((String) row[2]);
            item.setIdSolicitud((Integer) row[3]);
            item.setIdTipoProceso((Integer) row[4]);
            item.setIdTipoPlan((Integer) row[5]);
            item.setIdTipoEscala((short) row[6]);
            item.setAspectoComplementario((String) row[7]);
            item.setIdPlanManejoPadre((Integer) row[8]);
            item.setDescripcionContrato((String) row[9]);
            info.setDniElaborador((String) row[10]);
            info.setNroRucComunidad((String) row[11]);
            info.setNombreElaborador((String) row[12]);
            info.setApellidosElaborador((String) row[13]);
            item.setFechaRegistro((Date)(row[15]));
            item.setCodigoEstado((String)row[16]);
            item.setCodEstado((String)row[17]);
            item.setIdPermisoForestal(row[18] == null ? null : (Integer) row[18]);
            item.setCodigoUnico(row[19] == null ? null : (String) row[19]);
            item.setRemitido(row[20] == null ? null : (Boolean) row[20]);
            item.setInfoGeneral(info);
            items.add(item);
            pageable.setTotalRecords(SpUtil.toLong(row[21]));
            item.setVigencia(row[22] == null ? null : (Integer) row[22]);
            item.setModalidad(row[23] == null ? null : (String) row[23]);
            item.setModalidadTexto(row[24] == null ? null : (String) row[24]);

        }
        pageable.setData(items);
        pageable.setSuccess(true);
        if(items.size()>0){
            pageable.setMessage("Se obtuvo data.");
        }else{
            pageable.setMessage("No se encontró data.");
        }

        return pageable;
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {Guardar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @Override
    public ResultClassEntity registrarEstadoPlanManejo(PlanManejoEstadoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoEstado_Registrar");
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEstado", Integer.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoProceso", obj.getCodigoProceso());
            processStored.setParameter("codigo", obj.getCodigo());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("codigoEstado", obj.getCodigoEstado());
            processStored.setParameter("idPlanManejo", obj.getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            processStored.setParameter("idPlanManejoEstado", obj.getIdPlanManejoEstado());
            processStored.execute();  
            Integer id = (Integer) processStored.getOutputParameterValue("idPlanManejoEstado");             
            result.setCodigo(id);  
            result.setSuccess(true);
            result.setMessage("Se registró el estado del plan de manejo.");
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. " + e.getMessage());
            return  result;
        }
           
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {actualizar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @Override
    public ResultClassEntity actualizarEstadoPlanManejo(PlanManejoEstadoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoEstado_actualizar");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEstado", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("codigoEstado", obj.getCodigoEstado());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            processStored.setParameter("idPlanManejoEstado", obj.getIdPlanManejoEstado());
            processStored.execute();              
            result.setCodigo(obj.getIdPlanManejoEstado());  
            result.setSuccess(true);
            result.setMessage("Se actualizó el estado del plan de manejo.");
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.  "+ e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Jason Retamozo [30-03-2022]
     * @modificado:
     * @descripción: {listar informacion del plan de manejo de un titular ingresando nro y tipo de documento}
     * @param:PlanManejoEntity
     */
    @Override
    public ResultEntity<PlanManejoEntity> obtenerPlanesManejoTitular(String nroDocumento, String tipoDocumento) throws Exception{
        ResultEntity<PlanManejoEntity> result = new ResultEntity<>();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoTitular_Obtener");
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroDocumento", nroDocumento);
            processStored.setParameter("tipoDocumento", tipoDocumento);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity obj = new PlanManejoEntity();
                    obj.setIdPlanManejo((Integer) row[0]);
                    obj.setDescripcion((String) row[1]);
                    obj.setIdTipoProceso((Integer) row[2]);
                    obj.setIdTipoPlan((Integer) row[3]);
                    obj.setCodigoEstado((String) row[4]);
                    obj.setIdPlanManejoPadre((Integer) row[5]);
                    obj.setEstado((String) row[6]);

                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los planes de manejo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jason Retamozo [17-03-2022]
     * @modificado:
     * @descripción: {listar informacion del plan de manejo}
     * @param:PlanManejoInformacionEntity
     */

    @Override
    public ResultEntity<PlanManejoInformacionEntity> obtenerInformacionPlan(Integer idPlanManejo,String tipoPlan) throws Exception{
        ResultEntity<PlanManejoInformacionEntity> result = new ResultEntity<>();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoInformacion_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoInformacionEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoInformacionEntity obj = new PlanManejoInformacionEntity();
                    obj.setIdPlanManejo((Integer) row[0]);
                    obj.setTipoPlan((String) row[1]);
                    obj.setNombreTitular((String) row[2]);
                    obj.setFechaRegistro((Timestamp) row[3]);
                    obj.setDepartamento((String) row[4]);
                    obj.setProvincia((String) row[5]);
                    obj.setDistrito((String) row[6]);
                    obj.setAreaConcesion((BigDecimal) row[7]);
                    obj.setAreaAprovechada((BigDecimal) row[8]);
                    obj.setCodigoTH((String) row[9]);
                    obj.setSuperficie(Double.valueOf(row[10].toString()));
                    obj.setNumeroResolucion(row[11]==null?null:(String)row[11]);
                    obj.setNumeroParcela((Integer) row[12]);
                    obj.setNumeroDocumento((String) row[13]);

                    obj.setTituloHabilitante((String) row[14]);
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los documentos vigentes.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {listar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @Override
    public ResultEntity<PlanManejoEstadoEntity> listarEstadoPlanManejo(PlanManejoEstadoEntity filtro) {
        ResultEntity<PlanManejoEstadoEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoEstado_listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEstado", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", filtro.getIdPlanManejo());
            processStored.setParameter("codigoProceso", filtro.getCodigoProceso());
            processStored.setParameter("idPlanManejoEstado", filtro.getIdPlanManejoEstado());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoEstadoEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEstadoEntity obj = new PlanManejoEstadoEntity();
                    obj.setIdPlanManejoEstado((Integer) row[0]);
                    obj.setCodigoProceso((String) row[1]);
                    obj.setCodigo((String) row[2]);
                    obj.setDescripcion((String) row[3]);
                    obj.setObservacion((String) row[4]);
                    obj.setCodigoEstado((String) row[5]);
                    obj.setIdPlanManejo((Integer) row[6]);
                    obj.setDescripcionCodigoEstado((String) row[7]);
                    obj.setDescripcionCodigo((String) row[8]);
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los documentos vigentes.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return result;
        }
    }


    /**
     * @autor: Ivan Minaya [22-09-2021]
     * @modificado:
     * @descripción: {Lista por archivo por IdPlanManejo}
     * @param:PlanManejoEntity
     */
    @Override
    public ResultClassEntity ListarPorFiltroPlanManejoArchivo(PlanManejoArchivoEntity request) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoArchivoGeneral_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("idArchivo",  request.getIdArchivo());
            processStored.setParameter("idTipoDocumento", request.getIdTipoDocumento());
            processStored.setParameter("codigoProceso", request.getCodigoProceso());
            processStored.setParameter("subCodigoProceso", request.getSubCodigoProceso());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoArchivoEntity> lstdocs = new ArrayList<PlanManejoArchivoEntity>();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoArchivoEntity obj = new PlanManejoArchivoEntity();
                    obj.setIdPlanManejoArchivo((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setIdArchivo((Integer) row[2]);
                    obj.setCodigoTipo((String) row[3]);
                    obj.setCodigoSubTipo((String) row[4]);
                    obj.setNombre((String) row[5]);
                    obj.setExtension((String) row[6]);
                    obj.setIdTipoDocumento((String) row[7]);
                    obj.setTipoDocumento((String) row[8]);
                    obj.setDescripcion((String) row[9]);
                    obj.setObservacion((String) row[10]);

                    if (row[11]==null) {
                        obj.setConforme(null);
                    }else{
                        obj.setConforme((Boolean) row[11]);
                    }                   
                    lstdocs.add(obj);
                }
            }
            result.setData(lstdocs);
            result.setSuccess(true);
            result.setMessage("Se obtuvo archivos.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity eliminarPlanManejoArchivo(PlanManejoArchivoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejoArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoArchivo", request.getIdPlanManejoArchivo());
            processStored.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el Plan de Manejo Archivo");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity obtenerPlanManejoArchivo(PlanManejoArchivoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoArchivoGeneral_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("idArchivo",  request.getIdArchivo());

            processStored.execute();
            Object[] row = (Object[]) processStored.getSingleResult();

            PlanManejoArchivoDto obj = new PlanManejoArchivoDto();
            if (row!=null) {


                    obj.setIdPlanManejoArchivo((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setIdArchivo((Integer) row[2]);
                    obj.setCodigoTipoPgmf((String) row[3]);
                    obj.setCodigoSubTipoPgmf((String) row[4]);
                    obj.setNombre((String) row[5]);
                    obj.setExtension((String) row[6]);
                    obj.setIdTipoDocumento((String) row[7]);
                    obj.setTipoDocumento((String) row[8]);
                    obj.setDescripcion((String) row[9]);
                    obj.setObservacion((String) row[10]);
                    
                    if (row[11]==null) {
                        obj.setConforme(null);
                    }else{
                        obj.setConforme((Boolean) row[11]);
                    }
            }

            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se obtuvo archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity actualizarPlanManejoArchivo(PlanManejoArchivoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoArchivo_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejoArchivo", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("codigoTipoPGMF", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubTipoPGMF", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejoArchivo", request.getIdPlanManejoArchivo());
            processStored.setParameter("codigoTipoPGMF", request.getCodigoTipoPgmf());
            processStored.setParameter("codigoSubTipoPGMF", request.getCodigoSubTipoPgmf());
            processStored.setParameter("descripcion", request.getDescripcion());
            processStored.setParameter("observacion", request.getObservacion());
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("idArchivo", request.getIdArchivo());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());
            processStored.setParameter("estado", request.getEstado());
            processStored.setParameter("conforme", request.getConforme());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el Plan de Manejo Archivo");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }


    @Override
    public ResultClassEntity actualizarPlanManejoEstado(PlanManejoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejo_ActualizarEstado");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoPadre", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDivisionAdministrativa", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("remitido", Boolean.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("codigoEstado", request.getCodigoEstado());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());
            processStored.setParameter("idPlanManejoPadre", request.getIdPlanManejoPadre());
            processStored.setParameter("idDivisionAdministrativa", request.getIdDivisionAdministrativa());
            processStored.setParameter("remitido", request.getRemitido());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el Plan de Manejo");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity obtenerPlanManejoTitular(Integer idPlanManejo) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejo_ObtenerTitular");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);


            processStored.execute();
            Object[] row = (Object[]) processStored.getSingleResult();

            PlanManejoDto obj = new PlanManejoDto();
            if (row!=null) {
                obj.setIdPlanManejo((Integer) row[0]);
                obj.setCodigoEstado((String) row[1]);
                obj.setIdTitular((Integer) row[2]);
            }




            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se obtuvo plan.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity listarPlanManejoContrato(Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejo_ObtenerContrato");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.execute();
            List<Object[]> spResultDet = sp.getResultList();

            List<PlanManejoObtenerContrato> resultDet = null;            
            if (spResultDet!= null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                PlanManejoObtenerContrato temp = null;
                for (Object[] item: spResultDet) {
                    temp = new PlanManejoObtenerContrato();
                    temp.setIdContrato(item[0]==null?null:(Integer) item[0]);
                    temp.setCodigoTitulo(item[1]==null?null:(String) item[1]);
                    resultDet.add(temp);
                }
            }

            result.setData(resultDet);
            result.setSuccess(true);
            result.setMessage("Se listó Contrato correctamente.");
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity obtenerArchivoVerificacionCampo(Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejo_EvaluacionCampo_ObtenerArchivo");
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejoEvaluacion", idPlanManejo);
            sp.execute();
            Object spResultDet = sp.getSingleResult();
            Integer idArchivoVerCampo = (Integer)spResultDet;
            result.setData(idArchivoVerCampo);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el idArchivo Evaluación Campo");
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity obtenerArchivoSolicitudOpinion(Integer idPlanManejoEvaluacion) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_SolicitudOpinion_ObtenerArchivos");
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejoEvaluacion", idPlanManejoEvaluacion);
            sp.execute();
            Object[] spResultDet = (Object[])sp.getSingleResult();
            Integer[] numers= {0,0,0};
            numers[0] = (Integer)spResultDet[0];
            numers[1] = (Integer)spResultDet[1];
            numers[2] = (Integer)spResultDet[2];

            result.setData(numers);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el idArchivo Evaluación Campo");
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity actualizarConformePlanManejoArchivo(PlanManejoArchivoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoArchivoGeneral_ActualizarConforme");
            processStored.registerStoredProcedureParameter("idPlanManejoArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejoArchivo", request.getIdPlanManejoArchivo());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());


            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el Plan de Manejo Archivo");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity actualizarArchivoPlanManejoArchivo(PlanManejoArchivoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejoArchivo_ActualizarArchivo");
            processStored.registerStoredProcedureParameter("idPlanManejoArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);


            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejoArchivo", request.getIdPlanManejoArchivo());           
            processStored.setParameter("idArchivo", request.getIdArchivo());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el Plan de Manejo Archivo");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public ResultClassEntity obtenerPlanManejoDetalleContrato(PlanManejoArchivoDto request) {

        ResultClassEntity result = new ResultClassEntity();
        List<PlanManejoObtenerContratoDetalle> lista = new ArrayList<PlanManejoObtenerContratoDetalle>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_PlanManejo_ObtenerPlanContrato");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("codigo",  request.getDescripcion());

            processStored.execute();
 
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                PlanManejoObtenerContratoDetalle temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new PlanManejoObtenerContratoDetalle();
                        temp.setIdPlanManejo(row[0]==null?null:(Integer) row[0]);
                        temp.setDescripcion(row[1]==null?null:(String) row[1]);
                        temp.setIdSolicitud(row[2]==null?null:(Integer) row[2]);
                        temp.setIdTipoProceso(row[3]==null?null:(Integer) row[3]);
                        temp.setIdTipoPlan(row[4]==null?null:(Integer) row[4]);
                        temp.setIdTipoEscala(row[5]==null?null: ((Short) row[5]).intValue());
                        temp.setAspectoComplementario(row[6]==null?null:(String) row[6]);
                        temp.setIdPlanManejoContrato(row[7]==null?null:(Integer) row[7]);
                        temp.setIdContrato(row[8]==null?null:(Integer) row[8]);
                        temp.setResProcesoPostulacion(row[9]==null?null:(Integer) row[9]);
                        temp.setCoditoTituloh(row[10]==null?null:(String) row[10]);
                        temp.setContratoDescripcion(row[11]==null?null:(String) row[11]);
                        temp.setDniRepresentante(row[12]==null?null:(String) row[12]);
                        temp.setNombreRepresentante(row[13]==null?null:(String) row[13]);
                        temp.setCodigoPartidaRegistral(row[14]==null?null:(String) row[14]);
                        temp.setNroContratoConcesion(row[15]==null?null:(String) row[15]);
                        temp.setFirmado(row[16]==null?null:(Boolean) row[16]);
                        temp.setDatosValidados(row[17]==null?null:(Integer) row[17]);
                        temp.setStatusProceso(row[18]==null?null:(Integer) row[18]);
                        temp.setFechaInicioContrato(row[19]==null?null:(Timestamp) row[19]);
                        temp.setFechaFinContrato(row[20]==null?null:(Timestamp) row[20]);
                        temp.setIdContratoDet(row[21]==null?null:(Integer) row[21]);
                        temp.setCodigoTipo(row[22]==null?null:(String) row[22]);
                        temp.setContratoDetalle(row[23]==null?null:(String) row[23]);
                        temp.setFechaRegistro(row[24]==null?null:(Timestamp) row[24]);
          
                        lista.add(temp);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage(lista.size()>0?"Información encontrada":"No se encontró información");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

    @Override
    public List<PlanManejoArchivoDto> listarPlanManejoListar(Integer idPlanManejo, Integer idArchivo, String tipoDocumento,String codigoProceso,String extension) throws Exception {
        List<PlanManejoArchivoDto> lista = new ArrayList<PlanManejoArchivoDto>();
        try {

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PMamejoArchivo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("idArchivo", idArchivo);
            processStored.setParameter("tipoDocumento", tipoDocumento);
            processStored.setParameter("codigoProceso", codigoProceso);
            processStored.setParameter("extension", extension);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        PlanManejoArchivoDto temp = new PlanManejoArchivoDto();
                        temp.setIdPlanManejoArchivo(row[0]!=null?(Integer) row[0]:null);
                        temp.setIdPlanManejo(row[1]!=null?(Integer) row[1]:null);
                        temp.setIdArchivo(row[2]!=null?(Integer) row[2]:null);
                        temp.setCodigoTipoPgmf(row[3]!=null?(String) row[3]:null);
                        temp.setCodigoSubTipoPgmf(row[4]!=null?(String) row[4]:null);
                        temp.setNombre(row[5]!=null?(String) row[5]:null);
                        temp.setExtension(row[6]!=null?(String) row[6]:null);
                        temp.setTipoDocumento(row[7]!=null?(String) row[7]:null);
                        temp.setDescripcion(row[8]!=null?(String) row[8]:null);
                        temp.setDescripcionDocumento(row[9]!=null?(String) row[9]:null);
                        temp.setObservacion(row[10]!=null?(String) row[10]:null);
                        if (!temp.getIdArchivo().equals(null)) {
                            ResultClassEntity<ArchivoEntity> file = fileUp.obtenerArchivo(temp.getIdArchivo());
                            temp.setDocumento(file.getData().getFile());
                        }
                        temp.setFechaPlan(row[11]!=null?(Date) row[11]:null);
                        temp.setCodigoUnico(row[12]!=null?(String) row[12]:null);
                        temp.setFechaRegistro(row[13] != null ? (Date) row[13] : null);

                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("Listar plan manejo archivo", e.getMessage());
            throw e;
        }
    }



    @Override
    public List<PlanManejoArchivoDto> PlanManejoArchivoEntidad(Integer idPlanManejoArchivo) throws Exception {
        List<PlanManejoArchivoDto> lista = new ArrayList<PlanManejoArchivoDto>();
        try {

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PMamejoArchivo_Entidad");
            processStored.registerStoredProcedureParameter("idPlanManejoArchivo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoArchivo", idPlanManejoArchivo);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        PlanManejoArchivoDto temp = new PlanManejoArchivoDto();
                        temp.setIdPlanManejoArchivo((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setIdArchivo((Integer) row[2]);
                        temp.setCodigoTipoPgmf((String) row[3]);
                        temp.setCodigoSubTipoPgmf((String) row[4]);
                        temp.setNombre((String) row[5]);
                        temp.setExtension((String) row[6]);
                        temp.setTipoDocumento((String) row[7]);
                        temp.setDescripcion((String) row[8]);
                        temp.setDescripcionDocumento((String) row[9]);
                        temp.setObservacion((String) row[10]);
                        if (!temp.getIdArchivo().equals(null)) {
                            ResultClassEntity<ArchivoEntity> file = fileUp.obtenerArchivo(temp.getIdArchivo());
                            temp.setDocumento(file.getData().getFile());
                        }
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("Listar plan manejo archivo", e.getMessage());
            throw e;
        }
    }

    /**
     * @autor: Danny Nazario [17-12-2021]
     * @modificado:
     * @descripción: {Listar Plan Manejo Anexo Archivo}
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPlanManejoAnexoArchivo(PlanManejoArchivoEntity filtro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejoAnexoArchivo_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", filtro.getIdPlanManejo());
            sp.setParameter("codigoTipo", filtro.getCodigoTipo());
            sp.setParameter("tipoDocumento", filtro.getTipoDocumento());
            sp.execute();

            List<Object[]> spResult = sp.getResultList();
            List<PlanManejoArchivoEntity> list = new ArrayList<>();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    PlanManejoArchivoEntity obj = new PlanManejoArchivoEntity();
                    obj.setIdPlanManejoArchivo(row[0] == null ? null : (Integer) row[0]);
                    obj.setIdPlanManejo(row[1] == null ? null : (Integer) row[1]);
                    obj.setIdArchivo(row[2] == null ? null : (Integer) row[2]);
                    obj.setCodigoTipo(row[3] == null ? null : (String) row[3]);
                    obj.setCodigoSubTipo(row[4] == null ? null : (String) row[4]);
                    obj.setDescripcion(row[5] == null ? null : (String) row[5]);
                    obj.setAsunto(row[6] == null ? null : (String) row[6]);
                    obj.setAcapite(row[7] == null ? null : (String) row[7]);
                    obj.setNombreArchivo(row[8] == null ? null : (String) row[8]);
                    obj.setExtensionArchivo(row[9] == null ? null : (String) row[9]);
                    obj.setIdTipoDocumento(row[10] == null ? null : (String) row[10]);
                    obj.setItem(row[11] == null ? null : (String) row[11]);
                    if (!obj.getIdArchivo().equals(null)) {
                        ResultClassEntity<ArchivoEntity> file = fileUp.obtenerArchivo(obj.getIdArchivo());
                        obj.setDocumento(file.getData() == null ? null : file.getData().getFile());
                    }
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó plan manejo anexo archivo.");
            return result;
        } catch (Exception e) {
            log.error("PlanManejo - listarPlanManejoAnexoArchivo - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo,String tipoPlan) {
        ResultClassEntity result = new ResultClassEntity();

        List<ParametroValorEntity> lista = new ArrayList<ParametroValorEntity>();
        try {

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_TipoBosque_ListarPorPlanManejo");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ParametroValorEntity temp = new ParametroValorEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValor1((String) row[1]);
                    temp.setPrefijo((String) row[2]);
                    lista.add(temp);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("PlanManejo - ListarPorPlanManejoTipoBosque Ocurrio un error:", e.getMessage());
            result.setMessage("Ocurrio un error.");
            result.setSuccess(false);
            return result;
        }
    }


    @Override
    public ResultClassEntity<PlanManejoDto> DatosGeneralesPlan(Integer idPlanManejo) throws Exception {
        StoredProcedureQuery store = em.createStoredProcedureQuery("pa_Datos_Generales_Plan");
        store.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        store.setParameter("idPlanManejo", idPlanManejo);

        store.execute();
        PlanManejoDto data = setResultData(store.getResultList());
        ResultClassEntity<PlanManejoDto> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    private PlanManejoDto setResultData(List<Object[]> dataDb) throws Exception {
        PlanManejoDto data = null;
        if (dataDb != null && !dataDb.isEmpty()) {
            data = new PlanManejoDto();
            Object[] item = dataDb.get(0);
            data.setTipoDocumento(item[0]==null?null:(String) item[0]);
            data.setNroDocumento(item[1]==null?null:(String) item[1]);
            data.setArea(item[2]==null?null:(String) item[2]);
            data.setCodigo(item[3]==null?null:(String) item[3]);
            data.setEstado(item[4]==null?null:(String) item[4]);
            data.setNombrePlan(item[5]==null?null:(String) item[5]);


            }
        return data;
    }


    @Override
    public ResultClassEntity listarPorPlanManejoTipoBosque(Integer idPlanDeManejo,String tipoPlan,String idTipoBosque) {
        ResultClassEntity result = new ResultClassEntity();

        List<ParametroValorEntity> lista = new ArrayList<ParametroValorEntity>();
        try {

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_TipoBosque_ListarPorPlanManejo");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ParametroValorEntity temp = new ParametroValorEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValor1((String) row[1]);
                    temp.setPrefijo((String) row[2]);
                    lista.add(temp);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("PlanManejo - ListarPorPlanManejoTipoBosque Ocurrio un error:", e.getMessage());
            result.setMessage("Ocurrio un error.");
            result.setSuccess(false);
            return result;
        }
    }



    @Override
    public List<RegistroTabEntity> listarTabsRegistrados(RegistroTabEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejo_ListarConsolidado");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoProceso", param.getCodigoProceso());

            processStored.execute();
            List<RegistroTabEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                RegistroTabEntity temp = null;
                for (Object[] row_: spResult){
                    temp = new RegistroTabEntity();
                    temp.setTab(row_[0]==null?null:(String) row_[0]);
                    temp.setCodigoTab(row_[1]==null?null:(String) row_[1]);
                    temp.setConformidad(row_[2]==null?null:(String) row_[2]);

                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<DivisionAdministrativaEntity> listarDivisionAdministrativa(DivisionAdministrativaEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_DivisionAdministrativa_listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            String estadoPlan="";
            processStored.execute();
            List<DivisionAdministrativaEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                DivisionAdministrativaEntity temp = null;
                for (Object[] row: spResult){
                    temp = new DivisionAdministrativaEntity();
                    temp.setFuente(row[0]==null?null:(String) row[0]);
                    temp.setDocreg(row[1]==null?null:(String) row[1]);
                    temp.setFecreg(row[2]==null?null:(Date) row[2]);
                    temp.setObserv(row[3]==null?null:(String) row[3]);
                    temp.setZonutm(row[4]==null?null:(String) row[4]);
                    temp.setOrigen(row[5]==null?null:(String) row[5]);
                    temp.setNumpc(row[6]==null?null:(String) row[6]);
                    temp.setTipoth(row[7]==null?null:(String) row[7]);
                    temp.setNrotth(row[8]==null?null:(String) row[8]);
                    temp.setNumblo(row[9]==null?null:(String) row[9]);
                    temp.setDocleg(row[10]==null?null:(String) row[10]);
                    temp.setFecleg(row[11]==null?null:(Date) row[11]);
                    temp.setSupafp(row[12]==null?null:(String) row[12]);
                    temp.setNomrgt(row[13]==null?null:(String) row[13]);
                    temp.setEstado(row[14]==null?null:(String) row[14]);
                    temp.setTiempo(row[15]==null?null:(String) row[15]);
                    temp.setFecest(row[16]==null?null:(Date) row[16]);
                    temp.setNumrgt(row[17]==null?null:(String) row[17]);

                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }


}

