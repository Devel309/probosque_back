package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEstudioTecnicoEntity;

public interface SolicitudBosqueLocalEstudioTecnicoRepository {

    ResultClassEntity listarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj);

    ResultClassEntity actualizarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj);


    
}
