package pe.gob.serfor.mcsniffs.repository.impl;

 

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.DerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.ListarDerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.repository.DerechoAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class DerechoAprovechamientoRepositoryImpl extends JdbcDaoSupport implements DerechoAprovechamientoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(DerechoAprovechamientoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public Pageable<List<DerechoAprovechamientoDto>> listarDerechoAprovechamiento(ListarDerechoAprovechamientoDto dto) throws Exception {
      
        Page p = new Page(dto.getPageNum(), dto.getPageSize(), null, null);

        Pageable<List<DerechoAprovechamientoDto>> pageable=new Pageable<>(p);
        List<DerechoAprovechamientoDto> lista = new ArrayList<DerechoAprovechamientoDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_DerechoAprovechamiento_ListarPorFiltro");            
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("mecanismoPago", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("dniTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTituloTh", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContrato",dto.getIdContrato());
            processStored.setParameter("mecanismoPago",dto.getMecanismoPago());
            processStored.setParameter("nombreTitular",dto.getNombreTitular());
            processStored.setParameter("dniTitular",dto.getDniTitular());
            processStored.setParameter("codigoTituloTh",dto.getCodigoTituloTh());
            processStored.setParameter("P_PAGENUM",dto.getPageNum());
            processStored.setParameter("P_PAGESIZE",dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                DerechoAprovechamientoDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new DerechoAprovechamientoDto();

                        temp.setIdDerechoAprovechamiento(row[0]==null?null:(Integer) row[0]);
                        temp.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                        temp.setIdContrato(row[2]==null?null:(Integer) row[2]);
                        temp.setMecanismoPago(row[3]==null?null:(String) row[3]);
                        temp.setCodigoEstado(row[4]==null?null:(String) row[4]);
                        temp.setCodigoTituloH(row[5]==null?null:(String) row[5]);
                        temp.setFechaInicioContrato(row[6]==null?null:(Timestamp) row[6]);
                        temp.setFechaFinContrato(row[7]==null?null:(Timestamp) row[7]);
                        temp.setDescMecanismoPago(row[8]==null?null:(String) row[8]);
                        temp.setDniTitular(row[9]==null?null:(String) row[9]);
                        temp.setNombreTitular(row[10]==null?null:(String) row[10]);

                        lista.add(temp);
                        pageable.setTotalRecords(SpUtil.toLong(row[11]));
                    }
                }
            }

            pageable.setData(lista);
            return pageable;
        } catch (Exception e) {
            log.error("listarDerechoAprovechamiento", e.getMessage());
            throw e;
        }
    } 


    @Override
    public ResultClassEntity registrarDerecho(DerechoAprovechamientoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_Registrar");
            processStored.registerStoredProcedureParameter("idDerechoAprovechamiento", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("mecanismoPago", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idDerechoAprovechamiento", dto.getIdDerechoAprovechamiento());
            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("idContrato", dto.getIdContrato());
            processStored.setParameter("mecanismoPago", dto.getMecanismoPago());
            processStored.setParameter("codigoEstado", dto.getCodigoEstado());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idDerechoAprovechamiento");
            dto.setIdDerechoAprovechamiento(id);
            result.setSuccess(true);
            result.setMessage("Se registró la Evaluacion correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    
}
