package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Generico.ParametroDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.GenericoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class GenericoRepositoryImpl extends JdbcDaoSupport implements GenericoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(GenericoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }



    @Override
    public List<ParametroEntity> ListarPorFiltroParametro(ParametroEntity param) {

        List<ParametroEntity> lista = new ArrayList<ParametroEntity>();
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_ListarPorFiltro");
        processStored.registerStoredProcedureParameter("codigo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("valorPrimario", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("valorSecundario", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idTipoParametro", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idParametroPadre", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("codigo",param.getCodigo());
        processStored.setParameter("valorPrimario",param.getValorPrimario());
        processStored.setParameter("valorSecundario",param.getValorSecundario());
        processStored.setParameter("idTipoParametro",param.getIdTipoParametro());
        processStored.setParameter("idParametroPadre",param.getIdParametroPadre());
        processStored.setParameter("prefijo",param.getPrefijo());
        processStored.execute();

        List<Object[]> spResult = processStored.getResultList();
        if(spResult!=null){
            ParametroEntity temp = null;
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    temp = new ParametroEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValorPrimario((String) row[1]);
                    temp.setValorSecundario((String) row[2]);
                    temp.setIdTipoParametro((Integer) row[3]);
                    temp.setIdParametroPadre((Integer) row[4]);
                    temp.setValorTerciario((String) row[5]);
                    lista.add(temp);
                }
            }
        }

        return  lista;
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Listar por filtro Persona}
     * @param:PersonaEntity
     */
    @Override
    public ResultEntity<PersonaEntity> listarPorFilroPersona(PersonaEntity persona) {

        ResultEntity<PersonaEntity> result =  new ResultEntity<>();

        try {
            Integer IdTipoComunidad = null;
            if(persona.getComunidad()!=null){
                IdTipoComunidad=persona.getComunidad().getIdComunidad();
            }


            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Persona_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idComunidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefono", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ruc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codInrena", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPersona", persona.getIdPersona());
            processStored.setParameter("idComunidad", IdTipoComunidad);
            processStored.setParameter("idTipoPersona", persona.getIdTipoPersona());
            processStored.setParameter("nombres", persona.getNombres());
            processStored.setParameter("correo", persona.getCorreo());
            processStored.setParameter("telefono", persona.getTelefono());
            processStored.setParameter("ruc", persona.getRuc());
            processStored.setParameter("codInrena", persona.getCodInrena());
            processStored.setParameter("idUsuario", persona.getIdUsuario());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            List<PersonaEntity> lsPersona = new ArrayList<PersonaEntity>();
            if(spResult!=null){
                ParametroEntity temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {
                        PersonaEntity per=new PersonaEntity();
                        per.setIdPersona((Integer) row[0]);
                        per.setNombres((String) row[1]);
                        per.setApellidoPaterno((String) row[2]);
                        per.setApellidoMaterno((String) row[3]);
                        per.setCodInrena((String) row[4]);

                        per.setCorreo((String) row[5]);
                        per.setTelefono((String) row[6]);
                        per.setCodigo_tipo_persona((String) row[7]);
                        per.setCodigo_tipo_documento((String) row[8]);
                        per.setId_tipo_documento((Integer) row[9]);
                        per.setIdTipoPersona(Integer.parseInt ((String) row[10]));
                        per.setIdDistrito((Integer) row[11]);
                        per.setIdProvincia((Integer) row[12]);
                        per.setIdDepartamento((Integer) row[13]);

                        per.setNumeroDocumento((String) row[14]);
                        per.setEs_repr_legal((String) row[15]);
                        per.setId_persona_empresa_repr_legal((Integer) row[16]);
                        per.setRuc((String) row[17]);
                        per.setCodigo_eval_empresa((String) row[18]);
                        per.setRazonSocialEmpresa((String) row[19]);
                        per.setDireccion_empresa((String) row[20]);
                        per.setTelefono_empresa((String) row[21]);

                        per.setEmail_empresa((String) row[22]);

                        lsPersona.add(per);
                    }
                }
            }

            result.setData(lsPersona);
            result.setIsSuccess(true);
            result.setMessage("Se realizó la acción correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarPorFilroPersona", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public List<PersonaEntity> listarPersonaPorFiltro(PersonaEntity persona) {
         
        List<PersonaEntity> lista = new ArrayList<PersonaEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Persona_ListarPorFiltro");            
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idComunidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefono", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ruc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codInrena", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPersona",persona.getIdPersona());
            processStored.setParameter("idComunidad",persona.getComunidad().getIdComunidad());
            processStored.setParameter("idTipoPersona",persona.getIdTipoPersona());
            processStored.setParameter("nombres",persona.getNombres());
            processStored.setParameter("correo",persona.getCorreo());
            processStored.setParameter("telefono",persona.getTelefono());
            processStored.setParameter("ruc",persona.getRuc());
            processStored.setParameter("codInrena",persona.getCodInrena());
            processStored.setParameter("idUsuario",persona.getIdUsuario());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                PersonaEntity temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new PersonaEntity();

                        temp.setIdPersona(row[0]==null?null:(Integer) row[0]);
                        temp.setNombres(row[1]==null?null:(String) row[1]);
                        temp.setApellidoPaterno(row[2]==null?null:(String) row[2]);
                        temp.setApellidoMaterno(row[3]==null?null:(String) row[3]);
                        temp.setCodInrena(row[4]==null?null:(String) row[4]);
                        temp.setCorreo(row[5]==null?null:(String) row[5]);
                        temp.setTelefono(row[6]==null?null:(String) row[6]);
                        temp.setCodigo_tipo_persona(row[7]==null?null:(String) row[7]);
                        temp.setCodigo_tipo_documento(row[8]==null?null:(String) row[8]);
                        temp.setId_tipo_documento(row[9]==null?null:(Integer) row[9]);
                        temp.setTipoPersona(row[10]==null?null:(String) row[10]);
                        temp.setIdDistrito(row[11]==null?null:(Integer) row[11]);
                        temp.setIdProvincia(row[12]==null?null:(Integer) row[12]);
                        temp.setIdDepartamento(row[13]==null?null:(Integer) row[13]);
                        temp.setNumeroDocumento(row[14]==null?null:(String) row[14]);
                        temp.setEs_repr_legal(row[15]==null?null:String.valueOf((Character) row[15]));
                        temp.setId_persona_empresa_repr_legal(row[16]==null?null:(Integer) row[16]);
                        temp.setRuc(row[17]==null?null:(String) row[17]);
                        temp.setCodigo_eval_empresa(row[18]==null?null:(String) row[18]);
                        temp.setRazonSocialEmpresa(row[19]==null?null:(String) row[19]);
                        temp.setDireccion_empresa(row[20]==null?null:(String) row[20]);
                        temp.setTelefono_empresa(row[21]==null?null:(String) row[21]);
                        temp.setEmail_empresa(row[22]==null?null:(String) row[22]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarPersonaPorFiltro", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ParametroEntity> listarParametroPorPrefijo(ParametroDto dto) throws Exception {
         
        List<ParametroEntity> lista = new ArrayList<ParametroEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_ListarPorPrefijo");
            processStored.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("prefijo",dto.getPrefijo());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        ParametroEntity temp = new ParametroEntity();

                        temp.setIdParametro(row[0]==null?null:(Integer) row[0]);
                        temp.setCodigo(row[1]==null?null:(String) row[1]);
                        temp.setValorPrimario(row[2]==null?null:(String) row[2]);
                        temp.setValorSecundario(row[3]==null?null:(String) row[3]);
                        temp.setValorTerciario(row[4]==null?null:(String) row[4]);
                        temp.setIdTipoParametro(row[5]==null?null:(Integer) row[5]);
                        temp.setIdParametroPadre(row[6]==null?null:(Integer) row[6]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarProcedimientoAdministrativo", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<ParametroNivel1Entity> listarParametroLineamiento(ParametroNivel1Entity dto) throws Exception {

        List<ParametroNivel1Entity> lista = new ArrayList<ParametroNivel1Entity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_Lineamiento");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
            processStored.setParameter("prefijo",dto.getPrefijo());
            processStored.execute();
            int nivel1=0;
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        ParametroNivel1Entity temp = new ParametroNivel1Entity();

                        temp.setIdParametro(row[0]==null?null:(Integer) row[0]);
                        temp.setCodigo(row[1]==null?null:(String) row[1]);
                        temp.setValorPrimario(row[2]==null?null:(String) row[2]);
                        temp.setValorSecundario(row[3]==null?null:(String) row[3]);
                        temp.setValorTerciario(row[4]==null?null:(String) row[4]);
                        temp.setCodigoSniffs(row[5]==null?null:(String) row[5]);
                        temp.setCodigoSniffsDescripcion(row[6]==null?null:(String) row[6]);
                        temp.setObservacion(row[7]==null?null:(String) row[7]);
                        temp.setIdEvaluacionDet(row[8]==null?null:(Integer) row[8]);
                        /********************************** NIVEL 2 **********************************************/
                        List<ParametroNivel2Entity> listNivel2 = new ArrayList<ParametroNivel2Entity>();
                        StoredProcedureQuery processStored2 = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_Lineamiento");
                        processStored2.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                        processStored2.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
                        SpUtil.enableNullParams(processStored2);
                        processStored2.setParameter("idPlanManejo",dto.getIdPlanManejo());
                        processStored2.setParameter("prefijo",temp.getCodigo());
                        processStored2.execute();
                        List<Object[]> spResultNivel2 = processStored2.getResultList();
                        if(spResultNivel2!=null) {
                            if (spResultNivel2.size() >= 1) {
                                for (Object[] rowAct : spResultNivel2) {
                                    ParametroNivel2Entity temp2 = new ParametroNivel2Entity();

                                    temp2.setIdParametro(rowAct[0]==null?null:(Integer) rowAct[0]);
                                    temp2.setCodigo(rowAct[1]==null?null:(String) rowAct[1]);
                                    temp2.setValorPrimario(rowAct[2]==null?null:(String) rowAct[2]);
                                    temp2.setValorSecundario(rowAct[3]==null?null:(String) rowAct[3]);
                                    temp2.setValorTerciario(rowAct[4]==null?null:(String) rowAct[4]);
                                    temp2.setCodigoSniffs(rowAct[5]==null?null:(String) rowAct[5]);
                                    temp2.setCodigoSniffsDescripcion(rowAct[6]==null?null:(String) rowAct[6]);
                                    temp2.setObservacion(rowAct[7]==null?null:(String) rowAct[7]);
                                    temp2.setIdEvaluacionDet(rowAct[8]==null?null:(Integer) rowAct[8]);
                                    /********************************** NIVEL 3 **********************************************/
                                    List<ParametroNivel3Entity> listNivel3 = new ArrayList<ParametroNivel3Entity>();
                                    StoredProcedureQuery processStored3 = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_Lineamiento");
                                    processStored3.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                                    processStored3.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
                                    SpUtil.enableNullParams(processStored3);
                                    processStored3.setParameter("idPlanManejo",dto.getIdPlanManejo());
                                    processStored3.setParameter("prefijo",temp2.getCodigo());
                                    processStored3.execute();
                                    List<Object[]> spResultNivel3 = processStored3.getResultList();
                                    if(spResultNivel3!=null) {
                                        if (spResultNivel3.size() >= 1) {
                                            for (Object[] rowAct3 : spResultNivel3) {
                                                ParametroNivel3Entity temp3 = new ParametroNivel3Entity();

                                                temp3.setIdParametro(rowAct3[0]==null?null:(Integer) rowAct3[0]);
                                                temp3.setCodigo(rowAct3[1]==null?null:(String) rowAct3[1]);
                                                temp3.setValorPrimario(rowAct3[2]==null?null:(String) rowAct3[2]);
                                                temp3.setValorSecundario(rowAct3[3]==null?null:(String) rowAct3[3]);
                                                temp3.setValorTerciario(rowAct3[4]==null?null:(String) rowAct3[4]);
                                                temp3.setCodigoSniffs(rowAct3[5]==null?null:(String) rowAct3[5]);
                                                temp3.setCodigoSniffsDescripcion(rowAct3[6]==null?null:(String) rowAct3[6]);
                                                temp3.setObservacion(rowAct3[7]==null?null:(String) rowAct3[7]);
                                                temp3.setIdEvaluacionDet(rowAct3[8]==null?null:(Integer) rowAct3[8]);
                                                /********************************** NIVEL 4 **********************************************/
                                                List<ParametroNivel4Entity> listNivel4 = new ArrayList<ParametroNivel4Entity>();
                                                StoredProcedureQuery processStored4 = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_Lineamiento");
                                                processStored4.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                                                processStored4.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
                                                SpUtil.enableNullParams(processStored4);
                                                processStored4.setParameter("idPlanManejo",dto.getIdPlanManejo());
                                                processStored4.setParameter("prefijo",temp3.getCodigo());
                                                processStored4.execute();
                                                List<Object[]> spResultNivel4 = processStored4.getResultList();
                                                if(spResultNivel4!=null) {
                                                    if (spResultNivel4.size() >= 1) {
                                                        for (Object[] rowAct4 : spResultNivel4) {
                                                            ParametroNivel4Entity temp4 = new ParametroNivel4Entity();

                                                            temp4.setIdParametro(rowAct4[0]==null?null:(Integer) rowAct4[0]);
                                                            temp4.setCodigo(rowAct4[1]==null?null:(String) rowAct4[1]);
                                                            temp4.setValorPrimario(rowAct4[2]==null?null:(String) rowAct4[2]);
                                                            temp4.setValorSecundario(rowAct4[3]==null?null:(String) rowAct4[3]);
                                                            temp4.setValorTerciario(rowAct4[4]==null?null:(String) rowAct4[4]);
                                                            temp4.setCodigoSniffs(rowAct4[5]==null?null:(String) rowAct4[5]);
                                                            temp4.setCodigoSniffsDescripcion(rowAct4[6]==null?null:(String) rowAct4[6]);
                                                            temp4.setObservacion(rowAct4[7]==null?null:(String) rowAct4[7]);
                                                            temp4.setIdEvaluacionDet(rowAct4[8]==null?null:(Integer) rowAct4[8]);
                                                            /********************************** NIVEL 5 **********************************************/
                                                            List<ParametroNivel5Entity> listNivel5 = new ArrayList<ParametroNivel5Entity>();
                                                            StoredProcedureQuery processStored5 = entityManager.createStoredProcedureQuery("dbo.pa_Parametro_Lineamiento");
                                                            processStored5.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                                                            processStored5.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
                                                            SpUtil.enableNullParams(processStored5);
                                                            processStored5.setParameter("idPlanManejo",dto.getIdPlanManejo());
                                                            processStored5.setParameter("prefijo",temp4.getCodigo());
                                                            processStored5.execute();
                                                            List<Object[]> spResultNivel5 = processStored5.getResultList();
                                                            if(spResultNivel5!=null) {
                                                                if (spResultNivel5.size() >= 1) {
                                                                    for (Object[] rowAct5 : spResultNivel5) {
                                                                        ParametroNivel5Entity temp5 = new ParametroNivel5Entity();

                                                                        temp5.setIdParametro(rowAct5[0]==null?null:(Integer) rowAct5[0]);
                                                                        temp5.setCodigo(rowAct5[1]==null?null:(String) rowAct5[1]);
                                                                        temp5.setValorPrimario(rowAct5[2]==null?null:(String) rowAct5[2]);
                                                                        temp5.setValorSecundario(rowAct5[3]==null?null:(String) rowAct5[3]);
                                                                        temp5.setValorTerciario(rowAct5[4]==null?null:(String) rowAct5[4]);
                                                                        temp5.setCodigoSniffs(rowAct5[5]==null?null:(String) rowAct5[5]);
                                                                        temp5.setCodigoSniffsDescripcion(rowAct5[6]==null?null:(String) rowAct5[6]);
                                                                        temp5.setObservacion(rowAct5[7]==null?null:(String) rowAct5[7]);
                                                                        temp5.setIdEvaluacionDet(rowAct5[8]==null?null:(Integer) rowAct5[8]);
                                                                        listNivel5.add(temp5);
                                                                    }
                                                                }
                                                            }
                                                            /********************************************************************************/
                                                            temp4.setListNivel5(listNivel5);
                                                            listNivel4.add(temp4);
                                                        }
                                                    }
                                                }
                                                /********************************************************************************/
                                                temp3.setListNivel4(listNivel4);
                                                listNivel3.add(temp3);
                                            }
                                        }
                                    }
                                    /********************************************************************************/
                                    temp2.setListNivel3(listNivel3);

                                    listNivel2.add(temp2);
                                }
                            }
                        }
                        /********************************************************************************/
                        temp.setListNivel2(listNivel2);


                        lista.add(temp);

                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarProcedimientoAdministrativo", e.getMessage());
            throw e;
        }
    }
}