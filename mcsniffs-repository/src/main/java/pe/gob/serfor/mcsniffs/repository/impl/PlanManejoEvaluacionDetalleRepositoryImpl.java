package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.NotificacionDTO;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionSubDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class PlanManejoEvaluacionDetalleRepositoryImpl extends JdbcDaoSupport implements PlanManejoEvaluacionDetalleRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanManejoEvaluacionDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }   

    @Override
    public List<PlanManejoEvaluacionDetalleDto> listarPlanManejoEvaluacionDet(Integer idPlanManejoEval)  {

        List<PlanManejoEvaluacionDetalleDto> lstresult = new ArrayList<>();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_PlanManejoEvaluacionDetalle_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", idPlanManejoEval);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEvaluacionDetalleDto obj = new PlanManejoEvaluacionDetalleDto();
                    obj.setIdPlanManejoEvalDet((Integer) row[0]);
                    obj.setIdPlanManejoEvaluacion((Integer) row[1]);
                    obj.setCodigoTipo((String) row[2]);
                    obj.setTituloHabilitante((String) row[3]);
                    obj.setConforme((Boolean) row[4]);
                    obj.setObservacion((String) row[5]);

                    obj.setCodigoTramite((String) row[7]);
                    obj.setFechaRecepcion((Date) row[8]);
                    obj.setIdArchivo((Integer) row[10]);
                    obj.setDescArchivo((String) row[11]);
                    obj.setEstado((String) row[12]);


                    lstresult.add(obj);
                }
            }
          
            return  lstresult;
        } catch (Exception e) {
            log.error("PlanManejoevaluacionDetalleRepositoryImpl - listarPlanManejoEvaluacionDet", e.getMessage());
           
            return  lstresult;
        }

    }

    @Override
    public ResultClassEntity ListarPlanManejoEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionLineamiento_Listar");
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoParametro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());
            sp.setParameter("idTipoParametro", param.getIdTipoParametro());
            sp.execute();
            List<PlanManejoEvaluacionDetalleDto> list = new ArrayList<PlanManejoEvaluacionDetalleDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    PlanManejoEvaluacionDetalleDto obj = new PlanManejoEvaluacionDetalleDto();
                    obj.setIdPlanManejoEvalDet((Integer) row[0]);
                    obj.setCodigoTipo((String) row[1]);
                    obj.setDescripcion((String) row[2]);
                    obj.setConforme((Boolean) row[3]);
                    obj.setObservacion((String) row[4]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarPlanManejoEvaluacionLineamiento(List<PlanManejoEvaluacionDetalleDto> param) {
        ResultClassEntity result = new ResultClassEntity();
        List<PlanManejoEvaluacionDetalleDto> list = new ArrayList<PlanManejoEvaluacionDetalleDto>();
        try{
            for(PlanManejoEvaluacionDetalleDto p:param) {
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionLineamiento_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("conforme", boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPlanManejoEvaluacion", p.getIdPlanManejoEvaluacion());
                processStored.setParameter("codigoTipo", p.getCodigoTipo());
                processStored.setParameter("conforme", p.getConforme());
                processStored.setParameter("descripcion", p.getDescripcion());
                processStored.setParameter("observacion", p.getObservacion());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
            }
            result.setSuccess(true);
            result.setMessage("Se registró Plan Manejo Evaluación Lineamiento correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarPlanManejoEvaluacionDet(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionDetalle_Registrar");

            processStored.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tituloHabilitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoValidacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTramite", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaRecepcion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("codigoTipo", planManejoEvaluacionDetalleDto.getCodigoTipo());
            processStored.setParameter("tituloHabilitante", planManejoEvaluacionDetalleDto.getTituloHabilitante());
            processStored.setParameter("conforme", planManejoEvaluacionDetalleDto.getConforme());
            processStored.setParameter("observacion", planManejoEvaluacionDetalleDto.getObservacion());
            processStored.setParameter("estadoValidacion", planManejoEvaluacionDetalleDto.getEstadoValidacion());
            processStored.setParameter("codigoTramite", planManejoEvaluacionDetalleDto.getCodigoTramite());
            processStored.setParameter("fechaRecepcion", planManejoEvaluacionDetalleDto.getFechaRecepcion());
            processStored.setParameter("descripcion", planManejoEvaluacionDetalleDto.getDescripcion());
            processStored.setParameter("idArchivo", planManejoEvaluacionDetalleDto.getIdArchivo());
            processStored.setParameter("idPlanManejoEvaluacion", planManejoEvaluacionDetalleDto.getIdPlanManejoEvaluacion());
            processStored.setParameter("idUsuarioRegistro", planManejoEvaluacionDetalleDto.getIdUsuarioRegistro());


            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idPlanManejoEvalDet");
            planManejoEvaluacionDetalleDto.setIdPlanManejoEvalDet(id);


 
        }catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró plan manejo evaluacion detalle.");
        return  result;
       
    }

    @Override
    public ResultClassEntity actualizarPlanManejoEvaluacionDet(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionDetalle_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tituloHabilitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoValidacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTramite", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaRecepcion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);


            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvalDet", planManejoEvaluacionDetalleDto.getIdPlanManejoEvalDet());
            processStored.setParameter("idPlanManejoEvaluacion", planManejoEvaluacionDetalleDto.getIdPlanManejoEvaluacion());
            processStored.setParameter("codigoTipo", planManejoEvaluacionDetalleDto.getCodigoTipo());
            processStored.setParameter("tituloHabilitante", planManejoEvaluacionDetalleDto.getTituloHabilitante());
            processStored.setParameter("conforme", planManejoEvaluacionDetalleDto.getConforme());
            processStored.setParameter("observacion", planManejoEvaluacionDetalleDto.getObservacion());

            processStored.setParameter("estadoValidacion", planManejoEvaluacionDetalleDto.getEstadoValidacion());
            processStored.setParameter("codigoTramite", planManejoEvaluacionDetalleDto.getCodigoTramite());
            processStored.setParameter("fechaRecepcion", planManejoEvaluacionDetalleDto.getFechaRecepcion());
            processStored.setParameter("descripcion", planManejoEvaluacionDetalleDto.getDescripcion());
            processStored.setParameter("idArchivo", planManejoEvaluacionDetalleDto.getIdArchivo());
            processStored.setParameter("idUsuarioModificacion", planManejoEvaluacionDetalleDto.getIdUsuarioModificacion());

            processStored.execute();
 
        }catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se actualizó plan manejo evaluacion detalle.");
        return  result;
       
    }


    @Override
    public ResultClassEntity eliminarPlanManejoEvaluacionDet(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvalDet", planManejoEvaluacionDetalleDto.getIdPlanManejoEvalDet());
            processStored.setParameter("idUsuarioElimina", planManejoEvaluacionDetalleDto.getIdUsuarioElimina());

            processStored.execute();
 
        }catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se actualizó plan manejo evaluacion detalle.");
        return  result;
       
    }

    @Override
    public ResultClassEntity obtenerEvaluacionPlanManejo(Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Validar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();

            List<NotificacionDTO> notificacionDTOS = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    NotificacionDTO notificacionDTO = new NotificacionDTO();
                    notificacionDTO.setIdPlanManejo((Integer)row[0]);
                    notificacionDTO.setIdTitularRegente((Integer) row[1]);
                    notificacionDTO.setNombreTitularRegente((String) row[2]);
                    notificacionDTO.setCorreoTitularRegente((String) row[3]);
                    notificacionDTO.setNroObservaciones((Integer) row[4]);
                    notificacionDTO.setIdMincul((Integer) row[5]);
                    notificacionDTO.setIdAna((Integer) row[6]);
                    notificacionDTO.setIdSernap((Integer) row[7]);
                    notificacionDTO.setIdEvaluacionCampo((Integer) row[8]);
                    notificacionDTOS.add(notificacionDTO);
                }
            }

            result.setData(notificacionDTOS);
            result.setSuccess(true);
            result.setMessage("Se listó los datos de la evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
