package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ResponsableExperienciaLaboralRepository {

    ResultClassEntity obtenerResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj);

    ResultClassEntity listarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj);

    ResultClassEntity registrarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj);

    ResultClassEntity actualizarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj);

    ResultClassEntity eliminarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj);
}
