package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PermisoForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface PermisoForestalGeometriaRepository {
    Integer registrarPermisoForestalGeometria(PermisoForestalGeometriaEntity item) throws Exception;
    ResultClassEntity listarPermisoForestalGeometria (PermisoForestalGeometriaEntity item) throws Exception;
    ResultClassEntity eliminarPermisoForestalGeometriaArchivo(PermisoForestalGeometriaEntity item) throws Exception;
}
