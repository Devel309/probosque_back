package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.bind.annotation.PathVariable;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;

import java.util.List;

public interface PermisoForestalRepository {
    ResultClassEntity ObtenerPersonaPorUsuario(PersonaEntity entity);
    ResultClassEntity obtenerSolicitudPorAccesoSolicitud( Integer idSolicitud, Integer idSolicitudAcceso);
    ResultClassEntity registrarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity);
    ResultClassEntity editarSolicitudPorAccesoSolicitud(SolicitudRequestEntity solicitudRequestEntity);

    ResultClassEntity ListarPermisosForestales(SolicitudEntity request);
    ResultEntity RegistrarPermisosForestales(SolicitudEntity request);
    ResultEntity ObtenerPermisosForestales(SolicitudEntity request);

    ResultClassEntity validarRequisitoSolicitud(ValidacionRequisitoEntity entity);
    ResultClassEntity registrarSolicitudValidacion(ValidacionRequisitoEntity entity);
    ResultClassEntity actualizarSolicitudValidacion(ValidacionRequisitoEntity entity);
    ResultClassEntity obtenerSolicitudValidacionPorSeccion(ValidacionRequisitoEntity entity);
    ResultClassEntity obtenerSolicitudValidacionPorSolicitud(ValidacionRequisitoEntity entity);

    ResultClassEntity obtenerSolicitudEvaluacionPorSolicitud(ValidacionRequisitoEntity entity);
    ResultClassEntity evaluarRequisitoSolicitud(ValidacionRequisitoEntity entity);

    Pageable<List<PermisoForestalEntity>> filtrar(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
            String codEstado, String codTipoPersona, String codTipoActor, String codTipoCncc, String numeroDocumento,
            String nombrePersona, String razonSocial, Page page) throws Exception;

    Pageable<List<PermisoForestalEntity>> filtrarEval(Integer idPermisoForestal, Integer idTipoProceso, Integer idTipoPermiso,
                                                      String fechaRegistro, String codigoEstado, String codigoPerfil, Page p) throws Exception;

    ResultClassEntity registrarPermisoForestal(PermisoForestalEntity obj) throws Exception;

    ResultClassEntity actualizarEstadoPermisoForestal(PermisoForestalEntity obj) throws Exception;

    ResultClassEntity registrarInformacionGeneral(InformacionGeneralPermisoForestalEntity obj) throws Exception;


    ResultClassEntity<InformacionGeneralPermisoForestalEntity> listarInformacionGeneral(InformacionGeneralPermisoForestalEntity obj) throws Exception;
    ResultClassEntity<CodigoCifradoEntity> codigoCifrado(CodigoCifradoEntity obj) throws Exception;


    ResultClassEntity RegistrarArchivo(List<SolicitudSANArchivoEntity> list) throws Exception;
    List<SolicitudSANArchivoEntity>ListarArchivo(SolicitudSANArchivoEntity param) throws Exception;
    ResultClassEntity EliminarArchivo(SolicitudSANArchivoEntity solicitudSANArchivoEntity) throws Exception;

    ResultClassEntity listarPorFiltroPlanManejo(String nroDocumento, String codTipoPlan) throws Exception;
    ResultClassEntity registrarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception;
    ResultClassEntity listarPermisoForestalPlanManejo (PermisoForestalPlanManejoEntity item) throws Exception;
    ResultClassEntity eliminarPermisoForestalPlanManejo(PermisoForestalPlanManejoEntity item) throws Exception;
    ResultClassEntity permisoForestalActualizarRemitido(PermisoForestalPlanManejoEntity request);

    ResultClassEntity actualizarPermisoForestal(PermisoForestalEntity obj) throws Exception ;
}
