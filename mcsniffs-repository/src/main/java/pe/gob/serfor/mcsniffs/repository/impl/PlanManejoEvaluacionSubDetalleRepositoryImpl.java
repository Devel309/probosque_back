package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionSubDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionSubDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class PlanManejoEvaluacionSubDetalleRepositoryImpl extends JdbcDaoSupport implements PlanManejoEvaluacionSubDetalleRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanManejoEvaluacionSubDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity listarplanManejoEvaluacionEspecie(PlanManejoEvaluacionDto request) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_PlanManejoEvaluacionEspecie_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", request.getIdPlanManejoEval());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoEvaluacionSubDetalleDto> lstdocs = new ArrayList<PlanManejoEvaluacionSubDetalleDto>();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEvaluacionSubDetalleDto obj = new PlanManejoEvaluacionSubDetalleDto();
                    obj.setIdPlanManejoEvalSubDet((Integer) row[0]);
                    obj.setIdPlanManejoEvalDet((Integer) row[1]);
                    obj.setIdEspecie((Integer) row[2]);
                    obj.setNombreCientifico((String) row[3]);
                    obj.setNombreComun((String) row[4]);

                    lstdocs.add(obj);
                }
            }
            result.setData(lstdocs);
            result.setSuccess(true);
            result.setMessage("Se obtuvo archivos.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }
    @Override
    public List<PlanManejoEvaluacionSubDetalleDto> listarPlanManejoEvaluacionSubDetalle(Integer idPlanManejoEvalDet)  {
        List<PlanManejoEvaluacionSubDetalleDto> lstresult = new ArrayList<>();

        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_PlanManejoEvaluacionSubDetalle_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvalDet", idPlanManejoEvalDet);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoEvaluacionSubDetalleDto> lstdocs = new ArrayList<PlanManejoEvaluacionSubDetalleDto>();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEvaluacionSubDetalleDto obj = new PlanManejoEvaluacionSubDetalleDto();
                    obj.setIdPlanManejoEvalSubDet((Integer) row[0]);
                    obj.setIdPlanManejoEvalDet((Integer) row[1]);
                    obj.setIdEspecie((Integer) row[2]);
                    obj.setNombreCientifico((String) row[3]);
                    obj.setNombreComun((String) row[4]);
                    obj.setConforme((Boolean) row[5]);
                    obj.setObservacion((String) row[6]);
                    obj.setConsideracion((String) row[7]);
                    obj.setConsideracionDescripcion((String) row[8]);


                    lstresult.add(obj);
                }
            }

            
            return  lstresult;
        } catch (Exception e) {
            log.error("PlanManejoevaluacionSubDetalleRepositoryImpl - listarPlanManejoEvaluacionSubDetalle", e.getMessage());
            
            return  lstresult;
        }
    }

    @Override
    public ResultClassEntity registrarPlanManejoEvaluacionSubDetalle(PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalleDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionSubDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idEspecie", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("consideracion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEspecie", planManejoEvaluacionSubDetalleDto.getIdEspecie());
            processStored.setParameter("nombreCientifico", planManejoEvaluacionSubDetalleDto.getNombreCientifico());
            processStored.setParameter("nombreComun", planManejoEvaluacionSubDetalleDto.getNombreComun());
            processStored.setParameter("conforme", planManejoEvaluacionSubDetalleDto.getConforme());
            processStored.setParameter("observacion", planManejoEvaluacionSubDetalleDto.getObservacion());
            processStored.setParameter("consideracion", planManejoEvaluacionSubDetalleDto.getConsideracion());
            processStored.setParameter("idPlanManejoEvalDet", planManejoEvaluacionSubDetalleDto.getIdPlanManejoEvalDet());
            processStored.setParameter("idUsuarioRegistro", planManejoEvaluacionSubDetalleDto.getIdUsuarioRegistro());

            processStored.execute();
 
        }catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró plan manejo evaluacion sub detalle.");
        return  result;
       
    }

    @Override
    public ResultClassEntity actualizarPlanManejoEvaluacionSubDetalle(PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalleDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionSubDetalle_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvalSubDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEspecie", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("consideracion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvalSubDet", planManejoEvaluacionSubDetalleDto.getIdPlanManejoEvalSubDet());
            processStored.setParameter("idEspecie", planManejoEvaluacionSubDetalleDto.getIdEspecie());
            processStored.setParameter("nombreCientifico", planManejoEvaluacionSubDetalleDto.getNombreCientifico());
            processStored.setParameter("nombreComun", planManejoEvaluacionSubDetalleDto.getNombreComun());
            processStored.setParameter("conforme", planManejoEvaluacionSubDetalleDto.getConforme());
            processStored.setParameter("observacion", planManejoEvaluacionSubDetalleDto.getObservacion());
            processStored.setParameter("consideracion", planManejoEvaluacionSubDetalleDto.getConsideracion());
            processStored.setParameter("idPlanManejoEvalDet", planManejoEvaluacionSubDetalleDto.getIdPlanManejoEvalDet());
            processStored.setParameter("idUsuarioModificacion", planManejoEvaluacionSubDetalleDto.getIdUsuarioModificacion());

            processStored.execute();
 
        }catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se actualizó plan manejo evaluacion sub detalle.");
        return  result;
       
    }

    @Override
    public ResultClassEntity eliminarPlanManejoEvaluacionSubDetalle(PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalleDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionSubDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvalSubDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEvalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvalDet", planManejoEvaluacionSubDetalleDto.getIdPlanManejoEvalSubDet());
            processStored.setParameter("idPlanManejoEval", planManejoEvaluacionSubDetalleDto.getIdPlanManejoEvalDet());
            processStored.setParameter("idUsuarioElimina", planManejoEvaluacionSubDetalleDto.getIdUsuarioElimina());

            processStored.execute();
 
        }catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó plan manejo evaluacion sub detalle.");
        return  result;
       
    }


}
