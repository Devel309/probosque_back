package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Date;

@Repository
public class ProcesoOfertaDetalleRepositoryImpl extends JdbcDaoSupport implements ProcesoOfertaDetalleRepository {
    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Servicio de Proceso oferta en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProcesoOfertaDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * @autor: Abner Valdez [28-06-2021]
     * @modificado:
     * @descripción: {Guarda e proceso en oferta detalle}
     * @param:consulta
     */
    @Override
    public ResultClassEntity guardarProcesoOfertaDetalle(ProcesoOfertaDetalleEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcoferDetalle_insertar");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUnidadAprovechamiento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProOfertaDetalle", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOferta", obj.getIdProcesoOferta());
            sp.setParameter("idUnidadAprovechamiento", obj.getIdUnidadAprovechamiento());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.execute();
            Integer idProOfertaDetalle = (Integer) sp.getOutputParameterValue("idProOfertaDetalle");
            result.setCodigo(idProOfertaDetalle);
            result.setSuccess(true);
            result.setMessage("Se inició el proceso.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoOfertaDetalleRepositoryImpl - guardarProcesoOfertaDetalle", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

}
