package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesPlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.LibroOperacionesPlanManejoRepository;
import pe.gob.serfor.mcsniffs.repository.LibroOperacionesRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LibroOperacionesPlanManejoRepositoryImpl extends JdbcDaoSupport implements LibroOperacionesPlanManejoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(LibroOperacionesPlanManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    

    @Override
    public ResultClassEntity registrarLibroOperacionesPlanManejo(List<LibroOperacionesPlanManejoDto> request) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<LibroOperacionesPlanManejoDto> lista = new ArrayList<>();
        try {
            for(LibroOperacionesPlanManejoDto dto : request) {
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_PlanManejo_Registrar");
                processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idLibroOperacionesPlanManejo", Integer.class, ParameterMode.OUT);

                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idLibroOperaciones", dto.getIdLibroOperaciones());
                processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
                processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
                processStored.execute();

                Integer idLibroOperacionesPlanManejo = (Integer) processStored.getOutputParameterValue("idLibroOperacionesPlanManejo");
                dto.setIdLibroOperacionesPlanManejo(idLibroOperacionesPlanManejo);
                lista.add(dto);
            }
            result.setSuccess(true);
            result.setData(lista);
            result.setMessage("Se registró Libro Operaciones Plan Manejo correctamente.");

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarLibroOperacionesPlanManejo(LibroOperacionesPlanManejoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_PlanManejo_Eliminar");
            processStored.registerStoredProcedureParameter("idLibroOperacionesPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idLibroOperacionesPlanManejo", dto.getIdLibroOperacionesPlanManejo());
            processStored.setParameter("idLibroOperaciones", dto.getIdLibroOperaciones());
            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
 
            result.setSuccess(true);

            result.setMessage("Se eliminó el Libro Operaciones Plan Manejo correctamente.");

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

}
