package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEspecieGrupoEntity;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEspecieGrupoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PlanManejoEspecieGrupoRepositoryImpl extends JdbcDaoSupport implements PlanManejoEspecieGrupoRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity registrarPlanManejoEspecieGrupo(PlanManejoEspecieGrupoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEspecieGrupo_Registrar");
            processStored.registerStoredProcedureParameter("idPlanManejoEspecieGrupo", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCodigoEspecie", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idGrupoComercial", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("arbolesArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("producto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("productoAprovec", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejoEspecieGrupo", obj.getIdPlanManejoEspecieGrupo());
            processStored.setParameter("idPlanManejo", obj.getIdPlanManejo());
            processStored.setParameter("idCodigoEspecie", obj.getIdCodigoEspecie());
            processStored.setParameter("idGrupoComercial", obj.getIdGrupoComercial());
            processStored.setParameter("arbolesArea", obj.getArbolesArea());
            processStored.setParameter("producto", obj.getProducto());
            processStored.setParameter("productoAprovec", obj.getProductoAprovechamiento());
            processStored.setParameter("observaciones", obj.getObservaciones());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            if(obj.getIdPlanManejoEspecieGrupo() == null || obj.getIdPlanManejoEspecieGrupo() <= 0) {
                Integer idGrupoEspecie = (Integer) processStored.getOutputParameterValue("idPlanManejoEspecieGrupo");
                obj.setIdPlanManejoEspecieGrupo(idGrupoEspecie);
            }

            result.setCodigo(obj.getIdPlanManejoEspecieGrupo());
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el grupo de especie del plan de manejo correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el grupo de especie del plan de manejo.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }
}
