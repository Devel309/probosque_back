package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Parametro.ParticipacionComunalDto;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalEntity;
import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalParamEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.util.List;

public interface ParticipacionComunalRepository {
    ResultClassEntity RegistrarParticipacionComunal(List<ParticipacionComunalEntity> participacionComunal) ;
    ResultClassEntity ActualizarParticipacionComunal(List<ParticipacionComunalEntity> participacionComunal) ;
    ResultClassEntity EliminarParticipacionComunal(ParticipacionComunalEntity participacionComunal) ;
    ResultClassEntity ObtenerParticipacionComunal(ParticipacionComunalEntity participacionComunal) ;
    ResultClassEntity ListarPorFiltroParticipacionComunal(ParticipacionComunalEntity participacionComunal) ;


    ResultClassEntity RegistrarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception;
    ResultClassEntity ActualizarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception;
    ResultClassEntity EliminarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception;
    List<ParticipacionComunalEntity> ListarParticipacionCiudadana(ParticipacionComunalEntity participacionComunalEntity) throws Exception;

    ResultClassEntity RegistrarParticipacionCiudadanaList(List<ParticipacionComunalEntity> participacionComunal) throws Exception;

    //Jaqueline Diaz
    ResultClassEntity registrarParticipacionComunalCaberaDetalle(List<ParticipacionComunalEntity> participacionComunal);
    ResultEntity<ParticipacionComunalEntity> listarParticipacionComunalCaberaDetalle(ParticipacionComunalParamEntity participacionComunal);
    ResultClassEntity eliminarParticipacionComunalCaberaDetalle(ParticipacionComunalParamEntity participacionComunal);
}
