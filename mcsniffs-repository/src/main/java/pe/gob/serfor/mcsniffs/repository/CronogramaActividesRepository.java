package pe.gob.serfor.mcsniffs.repository;

import java.util.List;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.CronogramaActividesEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad.CronogramaActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface CronogramaActividesRepository {

    ResultEntity<CronogramaActividesEntity> ListarCronogramaActividades(CronogramaActividesEntity request);
    ResultEntity<CronogramaActividesEntity> RegistrarCronogramaActividades(List<CronogramaActividesEntity> request);
    ResultEntity<CronogramaActividesEntity> EliminarCronogramaActividades(CronogramaActividesEntity request);

    /***Métodos para el manejo de Cronograma Actividades usando tabla detalle*******/
    ResultEntity<CronogramaActividadEntity> RegistrarCronogramaActividad(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> EliminarCronogramaActividad(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> ListarCronogramaActividad(CronogramaActividadEntity request);
    ResultEntity<CronogramaActividadEntity> ListarCronogramaActividadTitular(CronogramaActividadEntity request);
    
    ResultEntity<CronogramaActividadEntity> RegistrarMarcaCronogramaActividadDetalle(CronogramaActividadEntity request);
    /** 
    ResultEntity<CronogramaActividesEntity> ListarCronogramaActividadDetalle(CronogramaActividesEntity request);
    **/
    //JaquelineDB
    ResultClassEntity EliminarCronogramaActividadDetalle(CronogramaActividadEntity filtro);
    ResultClassEntity RegistrarConfiguracionCronogramaActividad(CronogramaActividadDto request);
    ResultClassEntity ListarPorFiltroCronogramaActividad(CronogramaActividadDto request);

}
