package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;

import java.util.List;

public interface ProteccionBosqueRepository {
    ResultClassEntity ConfiguracionProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity param);
    ResultClassEntity RegistrarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity> param);
    ResultClassEntity ActualizarProteccionBosqueDemarcacion(List<ProteccionBosqueDemarcacionEntity>  param);
    ResultClassEntity ObtenerProteccionBosqueDemarcacion(ProteccionBosqueDemarcacionEntity param);
    ResultClassEntity RegistrarProteccionBosqueAmbiental(List<ProteccionBosqueGestionAmbientalEntity> list);
    ResultClassEntity ListarPorFiltroProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param);
    ResultClassEntity EliminarProteccionBosqueAmbiental(ProteccionBosqueGestionAmbientalEntity param);
    ResultClassEntity RegistrarProteccionBosqueImpactoAmbiental(List<ProteccionBosqueImpactoAmbientalEntity> list);
    ResultClassEntity ListarPorFiltroProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param);
    ResultClassEntity EliminarProteccionBosqueImpactoAmbiental(ProteccionBosqueImpactoAmbientalEntity param);

    /**
     * @autor: Rafael Azaña [15/10-2021]
     * @modificado:
     * @descripción: {CRUD DE PROTECCIÓN DEL BOSQUE}
     * @param:ProteccionBosqueEntity
     */

    ResultClassEntity RegistrarProteccionBosque(List<ProteccionBosqueEntity> list) throws Exception;
    List<ProteccionBosqueDetalleDto> listarProteccionBosque(Integer idPlanManejo, String codPlanGeneral, String subCodPlanGeneral) throws Exception;
    ResultClassEntity EliminarProteccionBosque(ProteccionBosqueDetalleDto proteccionBosqueDetalleDto) throws Exception;


}
