package pe.gob.serfor.mcsniffs.repository.util;

public class Urls {

	public static final String ROOT = "/";
	public static final String API = "/api";

	public static class codigo {
		public static final String BASE = API + "/clave";
	}

	public static class marcacion {
		public static final String BASE = API + "/marcacion";
	}

	public static class turno {
		public static final String BASE = API + "/turno";
	}

	public static class area {
		public static final String BASE = API + "/area";
	}

	public static class autenticacion {
		public static final String BASE = API + "/autenticacion";
	}

	public static class consultaApi {
		public static final String BASE = API + "/consultaApi";
	}

	public static class tipoProceso {
		public static final String BASE = API + "/tipoProceso";
	}

	public static class unidadAprovechamiento {
		public static final String BASE = API + "/unidadAprovechamiento";
	}

	public static class enviarConsulta {
		public static final String BASE = API + "/enviarConsulta";
	}

	public static class procesoOferta {
		public static final String BASE = API + "/procesoOferta";
	}

	public static class procesoPostulacion {
		public static final String BASE = API + "/procesoPostulacion";
	}

	public static class postulacionPFDM {
		public static final String BASE = API + "/postulacionPFDM";
	}

	public static class pgmfArchivo {
		public static final String BASE = API + "/pgmfArchivo";
	}

	public static class requisitoEvaluacion {
		public static final String BASE = API + "/requisitoEvaluacion";
	}

	public static class planGeneralManejoForestal {
		public static final String BASE = API + "/planGeneralManejoForestal";
	}

	public static class email {
		public static final String BASE = API + "/email";
	}

	public static class archivo {
		public static final String BASE = API + "/archivo";
	}

	public static class anexo {
		public static final String BASE = API + "/anexo";
	}

	public static class postulante {
		public static final String BASE = API + "/postulante";
	}

	public static class anexopfdm {
		public static final String BASE = API + "/anexopfdm";
	}

	public static class contrato {
		public static final String BASE = API + "/contrato";
	}

	public static class AreaSistemaPlantacion {
		public static final String BASE = API + "/areaSistemaPlantacion";
	}


	public static class fiscalizacion {
		public static final String BASE = API + "/fiscalizacion";
	}

	public static class demaAnexos {
		public static final String BASE = API + "/demaAnexos";
	}

	public static class informacionGeneralDema {
		public static final String BASE = API + "/informacionGeneralDema";
	}

	public static class consultaEntidad {
		public static final String BASE = API + "/consultaEntidad";
	}

	public static class mantenimiento {
		public static final String BASE = API + "/mantenimiento";
	}

	public static class coreCentral {
		public static final String BASE = API + "/coreCentral";
	}

	public static class solicitudEstado {
		public static final String BASE = API + "/solicitudestado";
	}

	public static class solicitud {
		public static final String BASE = API + "/solicitud";
	}

	public static class oposicion {
		public static final String BASE = API + "/oposicion";
	}

	public static class resultadoPP {
		public static final String BASE = API + "/resultadoPP";
	}

	public static class planGeneralManejo {
		public static final String BASE = API + "/planGeneralManejo";
	}

	public static class objetivoManejo {
		public static final String BASE = API + "/objetivoManejo";
	}

	public static class solicitudAcceso {
		public static final String BASE = API + "/solicitudAcceso";
	}

	public static class parametro {
		public static final String BASE = API + "/parametro";
	}

	public static class procesoOfertaDetalle {
		public static final String BASE = API + "/procesoOfertaDetalle";
	}

	public static class obtenerSolicitudAprovechamientoCcnn {
		public static final String BASE = API + "/obtenerSolicitudAprovechamientoCcnn";
	}

	public static class PGMFAbreviado {
		public static final String BASE = API + "/PGMFAbreviado";
	}

	public static class generico {
		public static final String BASE = API + "/generico";
	}

	public static class proteccionBosque {
		public static final String BASE = API + "/proteccionBosque";
	}

	public static class monitoreo {
		public static final String BASE = API + "/monitoreo";
	}
	public static class tituloHabilitante {
		public static final String BASE = API + "/tituloHabilitante";
	}

	public static class tituloHabilitanteRegente {
		public static final String BASE = API + "/tituloHabilitanteRegente";
	}

	public static class participacionComunal {
		public static final String BASE = API + "/participacionComunal";
	}

	public static class CronogramaActividades {
		public static final String BASE = API + "/CronogramaActividades";
	}

	public static class archivoSolicitud {
		public static final String BASE = API + "/archivoSolicitud";
	}

	public static class organizacionActividad {
		public static final String BASE = API + "/organizacionActividad";
	}

	public static class rentabilidadManejoForestal {
		public static final String BASE = API + "/rentabilidadManejoForestal";
	}

	public static class capacitacionPgmfea {
		public static final String BASE = API + "/capacitacionPgmfea";
	}

	public static class capacitacion {
		public static final String BASE = API + "/capacitacion";
	}

	
  
	public static class SolicitudBosqueLocalEvaluacionDetalle {
		public static final String BASE = API + "/SolicitudBosqueLocalEvaluacionDetalle";
	}
	public static class ordenamientoAreaManejo {
		public static final String BASE = API + "/ordenamientoAreaManejo";
	}

	public static class evaluacionAmbiental {
		public static final String BASE = API + "/evaluacionAmbiental";
	}

	public static class informacionGeneral {
		public static final String BASE = API + "/informacionGeneral";
	}

	public static class logAuditoria {
		public static final String BASE = API + "/logAuditoria";
	}

	public static class ordenamientoProteccion {
		public static final String BASE = API + "/ordenamientoProteccion";
	}

	public static class resumenActividad {
		public static final String BASE = API + "/resumenActividad";
	}

	public static class permisoForestal {
		public static final String BASE = API + "/permisoForestal";
	}

	public static class produccionRecursoForestal {
		public static final String BASE = API + "/produccionRecursoForestal";
	}

	public static class evaluacionResultado {
		public static final String BASE = API + "/evaluacionResultado";
	}

	public static class manejoBosque {
		public static final String BASE = API + "/manejoBosque";
	}
	public static class ordenProduccion {
		public static final String BASE = API + "/ordenProduccion";
	}

	public static class resumenActividadPo {
		public static final String BASE = API + "/resumenActividad";
	}

	public static class regente {
		public static final String BASE = API + "/regente";
	}

	public static class informacionGeneralPlanificacion {
		public static final String BASE = API + "/informacionGeneralPlanificacion";
	}

	public static class informacionBasica {
		public static final String BASE = API + "/informacionBasica";
	}

	public static class actividadSilvicultural {
		public static final String BASE = API + "/actividadSilvicultural";
	}

	public static class ActividadAprovechamiento {
		public static final String BASE = API + "/actividadAprovechamiento";
	}

	public static class recursoForestal {
		public static final String BASE = API + "/recursoForestal";
	}

	/********* Sol PlantacionForestal */
	public static class solPlantacionForestal {
		public static final String BASE = API + "/solPlantacionForestal";
	}

	public static class solPlantacionForestalGeometria {
		public static final String BASE = API + "/solPlantacionForestalGeometria";
	}

	public static class solPlantacionForestalEvaluacion {
		public static final String BASE = API + "/solPlantacionForestalEvaluacion";
	}

	public static class censoForestal {
		public static final String BASE = API + "/censoForestal";
	}

	public static class centroTransformacion {
		public static final String BASE = API + "/centroTransformacion";
	}

	public static class aprovechamiento {
		public static final String BASE = API + "/aprovechamiento";
	}

	public static class medioTransporte {
		public static final String BASE = API + "/medioTransporte";
	}

	public static class manejoBosqueAprovechamientoCamino {
		public static final String BASE = API + "/manejoBosqueAprovechamientoCamino";
	}

	public static class solicitudImpugnacion {
		public static final String BASE = API + "/solicitudImpugnacion";
	}

	public static class anexoAdjunto {
		public static final String BASE = API + "/anexoAdjunto";
	}
	public static class libroOperaciones {
		public static final String BASE = API + "/libroOperaciones";
	}
	public static class libroOperacionesPlanManejo {
		public static final String BASE = API + "/libroOperacionesPlanManejo";
	}

	public static class SistemaManejoForestal {
		private SistemaManejoForestal() {
		}

		public static final String BASE = API + "/sistemaManejoForestal";
	}

	public static class informacionAreaManejo {
		public static final String BASE = API + "/informacionAreaManejo";
	}

	public static class unidadFisiografica {
		public static final String BASE = API + "/unidadFisiografica";
	}

	public static class aspectoFisicoFisiografia {
		public static final String BASE = API + "/aspectoFisicoBiologico";
	}

	public static class bandejaPlanOperativo {
		public static final String BASE = API + "/bandejaPlanOperativo";
	}

	public static class potencialProduccionForestal {
		public static final String BASE = API + "/potencialProduccionForestal";

	}

	public static class planManejo {
		public static final String BASE = API + "/planManejo";
	}
	public static class consolidadoDEV {
		public static final String BASE = API + "/consolidadoDEV";
	}

	public static class impugnacion {
		public static final String BASE = API + "/impugnacion";
	}

	public static class resolucion {
		public static final String BASE = API + "/resolucion";
	}

	public static class zonificacionDema {
		public static final String BASE = API + "/zonificacionDema";
	}

	public static class impactoAmbiental {
		public static final String BASE = API + "/impactoAmbiental";
	}

	public static class solicitudBosqueLocalEvaluacion {
		public static final String BASE = API + "/solicitudBosqueLocalEvaluacion";
	}

	public static class solicitudBosqueLocalFinalidad {
		public static final String BASE = API + "/solicitudBosqueLocalFinalidad";
	}

	public static class solicitudBosqueLocalGeometria {
		public static final String BASE = API + "/solicitudBosqueLocalGeometria";
	}

	public static class evaluacionCampo {
		public static final String BASE = API + "/evaluacionCampo";
	}

	public static class evaluacionCampoAutoridad {
		public static final String BASE = API + "/evaluacionCampoAutoridad";
	}

	public static class condicionMinima {
		public static final String BASE = API + "/condicionMinima";
	}

	public static class estatusProceso {
		public static final String BASE = API + "/estatusProceso";
	}

	public static class planManejoEvaluacion {
		public static final String BASE = API + "/planManejoEvaluacion";
	}

	public static class evaluacionCampoInfraccion {
		public static final String BASE = API + "/evaluacionCampoInfraccion";
	}

	public static class solicitudOpinion {
		public static final String BASE = API + "/solicitudOpinion";
	}

	public static class procedimientoAdministrativo {
		public static final String BASE = API + "/procedimientoAdministrativo";
	}

	public static class medidaEvaluacion {
		public static final String BASE = API + "/medidaEvaluacion";
	}

	public static class mesaPartes {
		public static final String BASE = API + "/mesaPartes";
	}

	public static class evaluacion {
		public static final String BASE = API + "/evaluacion";
	}

	public static class derecho {
		public static final String BASE = API + "/derechoAprovechamiento";
	}

	public static class ProcesoOfertaSolicitudOpinion {
		public static final String BASE = API + "/procesoofertasolicitudopinion";
	}

	public static class otorgamiento {
		public static final String BASE = API + "/otorgamiento";
	}

	public static class solicitudSAN {
		public static final String BASE = API + "/solicitudSAN";
	}

	public static class autorizacionPublicacion {
		public static final String BASE = API + "/autorizacionPublicacion";
	}

	public static class planManejoGeometria {
		public static final String BASE = API + "/planManejoGeometria";
	}

	public static class SolicitudConcesionGeometria {
		public static final String BASE = API + "/SolicitudConcesionGeometria";
	}

	public static class permisoForestalGeometria {
		public static final String BASE = API + "/permisoForestalGeometria";
	}

	public static class titularidad {
		public static final String BASE = API + "/titularidad";
	}

	public static class solicitudConcesion {
		public static final String BASE = API + "/solicitudConcesion";
	}
    public static class tipoParametro {
		public static final String BASE = API + "/tipoParametro";
	}
	public static class solicitudConcesionEvaluacion {
		public static final String BASE = API + "/solicitudConcesionEvaluacion";
	}

	public static class responsableExperienciaLaboral {
		public static final String BASE = API + "/responsableExperienciaLaboral";
	}

	public static class responsableSolicitudConcesion {
		public static final String BASE = API + "/responsableSolicitudConcesion";
	}

	public static class ingresoCuentaSolicitudConcesion {
		public static final String BASE = API + "/ingresoCuentaSolicitudConcesion";
	}

	public static class solicitudConcesionEvaluacionCalificacion {
		public static final String BASE = API + "/solicitudConcesionEvaluacionCalificacion";
	}

	public static class censoForestalListar {
		public static final String BASE = API + "/censoForestalListar";
	}

	public static class solicitudBosqueLocal {
		public static final String BASE = API + "/solicitudBosqueLocal";
	}

	public static class solicitudBosqueLocalEstudioTecnico {
		public static final String BASE = API + "/solicitudBosqueLocalEstudioTecnico";
	}

	public static class solicitudBosqueLocalComiteTecnico {
		public static final String BASE = API + "/solicitudBosqueLocalComiteTecnico";
	}

	public static class municipioPersona {
		public static final String BASE = API + "/municipioPersona";
	}
	public static class notificacion {
		public static final String BASE = API + "/notificacion";
	}
	public static class pago {
		public static final String BASE = API + "/pago";
	}
	public static class tala {
		public static final String BASE = API + "/tala";
	}
	public static class sincronizacion {
		public static final String BASE = API + "/sincronizacion";
	}


	
}
