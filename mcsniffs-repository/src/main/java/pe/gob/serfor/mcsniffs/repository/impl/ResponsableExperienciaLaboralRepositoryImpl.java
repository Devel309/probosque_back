package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.repository.ResponsableExperienciaLaboralRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ResponsableExperienciaLaboralRepositoryImpl extends JdbcDaoSupport
        implements ResponsableExperienciaLaboralRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ResponsableExperienciaLaboralRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity obtenerResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj) {

        ResultClassEntity result = new ResultClassEntity();

        List<ResponsableExperienciaLaboralDto> lista = new ArrayList<ResponsableExperienciaLaboralDto>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_ResponsableExperienciaLaboral_Listar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class,
                    ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionResponsable", obj.getIdSolicitudConcesionResponsable());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                ResponsableExperienciaLaboralDto c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new ResponsableExperienciaLaboralDto();
                        c.setIdResponsableExperienciaLaboral(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitudConcesionResponsable(row[1] == null ? null : (Integer) row[1]);
                        c.setEntidad(row[2] == null ? null : (String) row[2]);
                        c.setCargo(row[3] == null ? null : (String) row[3]);
                        c.setTiempo(row[4] == null ? null : (Integer) row[4]);
                        c.setDescripcion(row[5] == null ? null : (String) row[5]);
                        c.setTelefonoContacto(row[6] == null ? null : (String) row[6]);
                        c.setEstadoResponsableExperienciaLaboral(
                                row[7] == null ? null : String.valueOf((Character) row[7]));

                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Responsable Experiencia Laboral correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarResponsableExperienciaLaboral", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo realizar la acción Listar Responsable Experiencia Laboral Concesion.");
            return result;
        }

    }

    @Override
    public ResultClassEntity listarResponsableExperienciaLaboral(
            ResponsableExperienciaLaboralDto obj) {

        ResultClassEntity result = new ResultClassEntity();
        List<ResponsableExperienciaLaboralDto> lista = new ArrayList<ResponsableExperienciaLaboralDto>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_ResponsableExperienciaLaboral_Listar");

            SpUtil.enableNullParams(processStored);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                ResponsableExperienciaLaboralDto c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new ResponsableExperienciaLaboralDto();
                        c.setIdResponsableExperienciaLaboral(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitudConcesionResponsable(row[1] == null ? null : (Integer) row[1]);
                        c.setEntidad(row[2] == null ? null : (String) row[2]);
                        c.setCargo(row[3] == null ? null : (String) row[3]);
                        c.setTiempo(row[4] == null ? null : (Integer) row[4]);
                        c.setDescripcion(row[5] == null ? null : (String) row[5]);
                        c.setTelefonoContacto(row[6] == null ? null : (String) row[6]);
                        c.setEstadoResponsableExperienciaLaboral(
                                row[7] == null ? null : String.valueOf((Character) row[7]));

                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Responsable Experiencia Laboral correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarResponsableExperienciaLaboral", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo realizar la acción Listar Responsable Experiencia Laboral Concesion.");
            return result;
        }

    }

    @Override
    public ResultClassEntity registrarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_ResponsableExperienciaLaboral_Registrar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("entidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cargo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tiempo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoContacto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idResponsableExperienciaLaboral", Integer.class,
                    ParameterMode.INOUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionResponsable", obj.getIdSolicitudConcesionResponsable());
            processStored.setParameter("entidad", obj.getEntidad());
            processStored.setParameter("cargo", obj.getCargo());
            processStored.setParameter("tiempo", obj.getTiempo());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("telefonoContacto", obj.getTelefonoContacto());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idResponsableExperienciaLaboral = (Integer) processStored
                    .getOutputParameterValue("idResponsableExperienciaLaboral");
            obj.setIdResponsableExperienciaLaboral(idResponsableExperienciaLaboral);
            result.setCodigo(idResponsableExperienciaLaboral);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la experiencia laboral del responsable correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la experiencia laboral del responsable.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_ResponsableExperienciaLaboral_Actualizar");
            processStored.registerStoredProcedureParameter("idResponsableExperienciaLaboral", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("entidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cargo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tiempo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoContacto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idResponsableExperienciaLaboral", obj.getIdResponsableExperienciaLaboral());
            processStored.setParameter("idSolicitudConcesionResponsable", obj.getIdSolicitudConcesionResponsable());
            processStored.setParameter("entidad", obj.getEntidad());
            processStored.setParameter("cargo", obj.getCargo());
            processStored.setParameter("tiempo", obj.getTiempo());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("telefonoContacto", obj.getTelefonoContacto());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó la experiencia laboral del responsable.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarResponsableExperienciaLaboral(ResponsableExperienciaLaboralDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_ResponsableExperienciaLaboral_Eliminar");
            processStored.registerStoredProcedureParameter("idResponsableExperienciaLaboral", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idResponsableExperienciaLaboral", obj.getIdResponsableExperienciaLaboral());
            processStored.setParameter("idUsuarioElimina", obj.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó la experiencia laboral del responsable.");
            return result;
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboralRepositoryImpl -eliminarSolicitudEliminar ocurrio un error: \n"
                    + e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

}
