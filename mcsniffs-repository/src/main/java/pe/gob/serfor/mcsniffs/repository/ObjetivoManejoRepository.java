package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.entity.ObjetivoEspecificoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.util.List;

public interface ObjetivoManejoRepository {
    public ResultClassEntity RegistrarObjetivoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
    public ResultClassEntity ActualizarObjetivoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
    public ResultClassEntity ObtenerObjetivoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
    public ResultClassEntity RegistrarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoEntityList) throws Exception;
    public ResultClassEntity ActualizarObjetivoEspecificoManejo(List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoEntityList) throws Exception;
    public ResultClassEntity ObtenerObjetivoEspecificoManejo(ObjetivoManejoEntity ObjetivoManejoEntity) throws Exception;
    ResultClassEntity ComboPorFiltroObjetivoEspecifico(ParametroEntity param) throws Exception;
    //JaquelineDB
    ResultClassEntity eliminarObjetivoManejo(ObjetivoManejoEntity param);
    ResultEntity< ObjetivoManejoEntity> listarObjetivoManejo(ObjetivoManejoEntity param);

    ResultEntity<ObjetivoDto> listarObjetivos(String codigoObjetivo, Integer idPlanManejo);

    ResultClassEntity RegistrarObjetivo(List<ObjetivoManejoEntity> list) throws Exception;

    ResultEntity< ObjetivoDto> listarObjetivoManejoTitular(ObjetivoManejoEntity param);


}

