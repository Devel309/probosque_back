package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.TalaEntity;
import pe.gob.serfor.mcsniffs.repository.SincronizacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.DBHelper;

@Repository("repositorySincronizacion")
public class SincronizacionRepositoryImpl implements SincronizacionRepository {

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);
    
    @Override
    public List<TalaEntity> listarTala(String ruta) throws Exception {
        List<TalaEntity> lista = new ArrayList();
        lista.clear();
        Connection conexion = DBHelper.openDatabase(ruta);
        try {
            String sqlConsulta = "SELECT * FROM T_MAC_TALA";
            System.out.println(sqlConsulta);
            PreparedStatement preparedStatement = conexion.prepareStatement(sqlConsulta);
            try (ResultSet resulset = preparedStatement.executeQuery()) {

                while (resulset.next()) {
                    System.out.println(resulset);
                    lista.add(filaTala(resulset));
                }
            }
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Exception SincronizacionRepository   " + e);
            conexion.close();
        }
        return lista;
    }
    private TalaEntity filaTala(ResultSet resulset) {
        TalaEntity tala = new TalaEntity();
        try {
            tala.setIdTala(resulset.getInt(1));
            tala.setIdCensoForestalDetalle(resulset.getInt(2));
            tala.setFechaTala(resulset.getDate(3));
            tala.setCantidadRama(resulset.getInt(4));
            tala.setObservaciones(resulset.getString(2));
            tala.setEstadoTala(resulset.getString(2));
            tala.setNumeroTrozado(resulset.getInt(2));
            tala.setFechaTrozado(resulset.getString(2));
            tala.setObservacionTrozado(resulset.getString(2));
            tala.setEstado(resulset.getString(2));
            tala.setIdUsuarioRegistro(resulset.getInt(2));
            tala.setFechaRegistro(resulset.getDate(2));
            tala.setIdUsuarioModificacion(resulset.getInt(2));
            tala.setFechaModificacion(resulset.getDate(2));
            tala.setIdUsuarioElimina(resulset.getInt(2));
            tala.setFechaElimina(resulset.getDate(2));
            System.out.println(tala);

        } catch (SQLException ex) {
            System.out.println("Exception fila   " + ex);
        }
        return tala;
    }
}
