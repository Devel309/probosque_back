package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.DerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.ListarDerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;

public interface DerechoAprovechamientoRepository {

    Pageable<List<DerechoAprovechamientoDto>> listarDerechoAprovechamiento(ListarDerechoAprovechamientoDto dto) throws Exception;
    ResultClassEntity registrarDerecho(DerechoAprovechamientoDto dto) throws Exception;
    
}
