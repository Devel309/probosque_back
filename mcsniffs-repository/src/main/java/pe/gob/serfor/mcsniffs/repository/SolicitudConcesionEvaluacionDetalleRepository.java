package pe.gob.serfor.mcsniffs.repository;


import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;

public interface SolicitudConcesionEvaluacionDetalleRepository {
    ResultClassEntity registrarSolicitudConcesionEvaluacionDetalle(List<SolicitudConcesionEvaluacionDetalleDto> obj);
    ResultClassEntity actualizarSolicitudConcesionEvaluacionDetalle(SolicitudConcesionEvaluacionDetalleDto obj);
}
