package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetallePgmfDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDto;

import java.util.List;

public interface RecursoForestalRepository {
   // List<RecursoForestalEntity> listarRecursoForestal(Integer idPlanManejo) throws Exception;
    ResultClassEntity RegistrarRecursoForestal(List<RecursoForestalEntity> list) throws Exception;
    List<RecursoForestalDetalleDto> listarRecursoForestal(Integer idPlanManejo,Integer idRecursoForestal,String codCabecera) throws Exception;

    ResultClassEntity EliminarRecursoForestal(RecursoForestalDetalleDto recursoForestalDetalleDto) throws Exception;

}
