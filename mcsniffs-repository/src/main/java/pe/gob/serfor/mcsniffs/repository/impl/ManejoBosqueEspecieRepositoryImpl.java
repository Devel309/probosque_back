package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueEspecieRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManejoBosqueEspecieRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueEspecieRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueEspecieRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosqueEspecie(List<ManejoBosqueEspecieEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueEspecieEntity> list = new ArrayList<ManejoBosqueEspecieEntity>();
            for(ManejoBosqueEspecieEntity p:param){

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueEsp_Registrar");
            processStored.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idManejoBosqueEsp", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEspecie", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lineaProduccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diametro", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManejoBosque", p.getManejoBosque().getIdManejoBosque());
            processStored.setParameter("idManejoBosqueEsp", p.getIdManejoBosqueEsp());
            processStored.setParameter("idEspecie", p.getIdEspecie());
            processStored.setParameter("especie", p.getEspecie());
            processStored.setParameter("lineaProduccion", p.getLineaProduccion());
            processStored.setParameter("diametro", p.getDiametro());
            processStored.setParameter("estado", p.getEstado());
            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();
            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    ManejoBosqueEspecieEntity obj = new ManejoBosqueEspecieEntity() ;
                    ManejoBosqEntity ma = new ManejoBosqEntity() ;
                    ma.setIdManejoBosque((Integer) row[0]);
                    obj.setManejoBosque(ma);
                    obj.setIdManejoBosqueEsp((Integer) row[1]);
                    obj.setIdEspecie((short) row[2]);
                    obj.setEspecie((String) row[3]);
                    BigDecimal bd=(BigDecimal) row[4];
                    obj.setDiametro((Double) bd.doubleValue());
                    obj.setLineaProduccion((String) row[5]);

                    list.add(obj);
                  }
               }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró Especie de Aprovechamiento correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueEspecie(ManejoBosqueEspecieEntity manejoBosqueEspecie) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueEsp_Eliminar");
            processStored.registerStoredProcedureParameter("idManejoBosqueEsp", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManejoBosqueEsp", manejoBosqueEspecie.getIdManejoBosqueEsp());
            processStored.setParameter("idUsuarioElimina", manejoBosqueEspecie.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarManejoBosqueEspecie(ManejoBosqEntity manejoBosque) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueEspecieEntity> list = new ArrayList<ManejoBosqueEspecieEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueEsp_Listar");
            processStored.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManejoBosque", manejoBosque.getIdManejoBosque());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();

            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    ManejoBosqueEspecieEntity obj = new ManejoBosqueEspecieEntity() ;
                    ManejoBosqEntity ma = new ManejoBosqEntity() ;
                    ma.setIdManejoBosque((Integer) row[0]);
                    obj.setManejoBosque(ma);
                    obj.setIdManejoBosqueEsp((Integer) row[1]);
                    obj.setIdEspecie((short) row[2]);
                    obj.setEspecie((String) row[3]);
                    BigDecimal bd=(BigDecimal) row[4];
                    obj.setDiametro((Double) bd.doubleValue());
                    obj.setLineaProduccion((String) row[5]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se obtuvo Especie de Aprovechamiento correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
