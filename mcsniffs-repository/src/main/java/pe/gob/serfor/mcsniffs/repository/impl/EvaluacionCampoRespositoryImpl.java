package pe.gob.serfor.mcsniffs.repository.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad.EvaluacionCampoAutoridadDto;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoInfraccionEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.InfraccionEntity;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoRespository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class EvaluacionCampoRespositoryImpl extends JdbcDaoSupport implements EvaluacionCampoRespository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(EvaluacionCampoRespositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registroEvaluacionCampo(EvaluacionCampoEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        try{


        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampo_Registrar");
        processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idEvaluacionCampoEstado", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("cites", Boolean.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaInicioEvaluacion", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaFinEvaluacion", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaInicioImpedimentos", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaFinImpedimentos", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("comunidadNativaCampesina", Boolean.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("resultadoFavorable", Boolean.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("notificacion", Boolean.class, ParameterMode.IN);  
        processStored.registerStoredProcedureParameter("usuarioRegistro", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idEval", Integer.class, ParameterMode.OUT);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
        processStored.setParameter("idEvaluacionCampoEstado", p.getIdEvaluacionCampoEstado());
        processStored.setParameter("cites", p.getCites());
        processStored.setParameter("fechaInicioEvaluacion", p.getFechaInicioEvaluacion());
        processStored.setParameter("fechaFinEvaluacion", p.getFechaFinEvaluacion());
        processStored.setParameter("fechaInicioImpedimentos", p.getFechaInicioImpedimentos());
        processStored.setParameter("fechaFinImpedimentos", p.getFechaFinImpedimentos());
        processStored.setParameter("comunidadNativaCampesina", p.getComunidadNatidadCampesina());
        processStored.setParameter("resultadoFavorable", p.getResultadoFavorable());
        processStored.setParameter("notificacion", p.getNotificacion());
        processStored.setParameter("usuarioRegistro", p.getIdUsuarioRegistro());
        processStored.setParameter("documentoGestion", p.getDocumentoGestion());
        processStored.setParameter("tipoDocumentoGestion", p.getTipoDocumentoGestion());
        processStored.execute();
        Integer idEvaluacionCampo = (Integer) processStored.getOutputParameterValue("idEval");
        result.setCodigo(idEvaluacionCampo);
        result.setMessage("registró la Evaluación de Campo correctamente.");
        result.setSuccess(true);

        LogAuditoriaEntity logAuditoriaEntity;

        if(p.getIdEvaluacionCampo() == 0 || p.getIdEvaluacionCampo() == null){

            logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_EVALUACION_CAMPO,
                    LogAuditoria.REGISTRAR,
                    LogAuditoria.DescripcionAccion.Registrar.T_MVC_EVALUACION_CAMPO + idEvaluacionCampo,
                    p.getIdUsuarioRegistro());

        }else {

            logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_EVALUACION_CAMPO,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_EVALUACION_CAMPO + p.getIdEvaluacionCampo(),
                    p.getIdUsuarioRegistro());
        }
        logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);
        return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity NotificacionEvaluacionCampo(EvaluacionCampoEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampo_Notificacion");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificacion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
            processStored.setParameter("notificacion", p.getNotificacion());
            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se envio la notificación correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_EVALUACION_CAMPO,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_EVALUACION_CAMPO + p.getIdEvaluacionCampo(),
                    p.getIdUsuarioRegistro());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity obtenerEvaluacionCampo(EvaluacionCampoEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampo_Obtener");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            EvaluacionCampoEntity obj = new EvaluacionCampoEntity();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    obj.setIdEvaluacionCampo((Integer)row[0]) ;
                    obj.setIdEvaluacionCampoEstado((Integer)row[1]) ;
                    obj.setDocumentoGestion((Integer)row[2]); ;
                    obj.setCites((Boolean)row[3]) ;
                    obj.setFechaInicioEvaluacion((Date)row[4]) ;
                    obj.setFechaFinEvaluacion((Date)row[5]) ;
                    obj.setFechaInicioImpedimentos((Date)row[6]); ;
                    obj.setFechaFinImpedimentos((Date)row[7]) ;
                    obj.setComunidadNatidadCampesina((Boolean)row[8]) ;
                    obj.setResultadoFavorable((Boolean) row[9]) ;
                    obj.setNotificacion((Boolean) row[10]) ;
                    obj.setIdEvaluacionCampoVersion((Integer) row[11]) ;
                    obj.setTipoDocumentoGestion((String)row[12]) ;
                    obj.setEvaluacionCampoEstado((String)row[13]) ;
                    obj.setTitular((String) row[14]);
                    obj.setNumThActoAdmin((String) row[15]);
                    obj.setIdUsuarioRegistro((Integer) row[16]);
                    obj.setTipoDocumentoGestionTexto((String) row[17]);
                }
            }


            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se envio la notificación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity obtenerEvaluacionOcular(EvaluacionCampoEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionOcular_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", p.getIdPlanManejo());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            EvaluacionCampoEntity obj = new EvaluacionCampoEntity();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    obj.setIdEvaluacionCampo((Integer)row[0]) ;
                    obj.setIdEvaluacionCampoEstado((Integer)row[1]) ;
                    obj.setDocumentoGestion((Integer)row[2]); ;
                    obj.setCites((Boolean)row[3]) ;
                    obj.setFechaInicioEvaluacion((Date)row[4]) ;
                    obj.setFechaFinEvaluacion((Date)row[5]) ;
                    obj.setFechaInicioImpedimentos((Date)row[6]); ;
                    obj.setFechaFinImpedimentos((Date)row[7]) ;
                    obj.setComunidadNatidadCampesina((Boolean)row[8]) ;
                    obj.setResultadoFavorable((Boolean) row[9]) ;
                    obj.setNotificacion((Boolean) row[10]) ;
                    obj.setIdEvaluacionCampoVersion((Integer) row[11]) ;
                    obj.setTipoDocumentoGestion((String)row[12]) ;
                    obj.setEvaluacionCampoEstado((String)row[13]) ;
                    obj.setTitular((String) row[14]);
                    obj.setNumThActoAdmin((String) row[15]);
                    obj.setIdUsuarioRegistro((Integer) row[16]);
                    obj.setTipoDocumentoGestionTexto((String) row[17]);
                }
            }


            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se envio la notificación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity registroEvaluacionCampoArchivo(EvaluacionCampoArchivoEntity p) {

        ResultClassEntity result = new ResultClassEntity();

        try{

        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoArchivo_Registrar");
        processStored.registerStoredProcedureParameter("idEvaluacionCampoArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoSeccionArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idEvaluacionCampoArchivo", p.getIdEvaluacionCampoArchivo());
        processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
        processStored.setParameter("idArchivo", p.getIdArchivo());
        processStored.setParameter("idTipoDocumento", p.getIdTipoDocumento());
        processStored.setParameter("idTipoSeccionArchivo", p.getIdTipoSeccionArchivo());
        processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
        processStored.execute();
        List<Object[]> spResult =processStored.getResultList();
            EvaluacionCampoArchivoEntity temp = new EvaluacionCampoArchivoEntity();
        if (spResult.size() >= 1) {
            for (Object[] row : spResult) {
                temp.setIdEvaluacionCampoArchivo((Integer) row[0]);
                temp.setIdEvaluacionCampo((Integer) row[1]);
                temp.setIdArchivo((Integer) row[2]);
                temp.setIdTipoDocumento((String) row[3]);
                temp.setIdTipoSeccionArchivo((Integer) row[4]);
                temp.setNombre((String) row[5]);
                temp.setTipoDocumento((String) row[6]);
            }
        }
        result.setData(temp);
        result.setSuccess(true);
        result.setMessage("Se registró el archivo correctamente.");
        return  result;
    } catch (Exception e){
        log.error(e.getMessage(), e);
        result.setSuccess(false);
        result.setMessage("Ocurrió un error.");
        return  result;
     }
    }
    @Override
    public ResultClassEntity eliminarEvaluacionCampoArchivo(EvaluacionCampoArchivoEntity p) {

        ResultClassEntity result = new ResultClassEntity();

        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idEvaluacionCampoArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampoArchivo", p.getIdEvaluacionCampoArchivo());
            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarPorSeccionEvaluacionCampoArchivo(EvaluacionCampoArchivoEntity p) {

        ResultClassEntity result = new ResultClassEntity();
        List<EvaluacionCampoArchivoEntity> list = new ArrayList<EvaluacionCampoArchivoEntity>();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoArchivo_ListarPorSeccion");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoSeccionArchivo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
            processStored.setParameter("idTipoSeccionArchivo", p.getIdTipoSeccionArchivo());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    EvaluacionCampoArchivoEntity temp = new EvaluacionCampoArchivoEntity();
                    temp.setIdEvaluacionCampoArchivo((Integer) row[0]);
                    temp.setIdEvaluacionCampo((Integer) row[1]);
                    temp.setIdTipoSeccionArchivo((Integer) row[2]);
                    temp.setIdArchivo((Integer) row[3]);
                    temp.setIdTipoDocumento((String) row[4]);
                    temp.setNombre((String) row[5]);
                    temp.setTipoDocumento((String) row[6]);
                    list.add(temp);

                }
            }
            result.setData(list);
            result.setSuccess(true);
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity listarEvaluacionCampoArchivoInspeccion(EvaluacionCampoArchivoEntity p) {

        ResultClassEntity result = new ResultClassEntity();
        List<EvaluacionCampoArchivoEntity> list = new ArrayList<EvaluacionCampoArchivoEntity>();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoArchivoInspeccion_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    EvaluacionCampoArchivoEntity temp = new EvaluacionCampoArchivoEntity();
                    temp.setIdEvaluacionCampoArchivo((Integer) row[0]);
                    temp.setIdEvaluacionCampo((Integer) row[1]);
                    temp.setIdTipoSeccionArchivo((Integer) row[2]);
                    temp.setIdArchivo((Integer) row[3]);
                    temp.setIdTipoDocumento((String) row[4]);
                    temp.setNombre((String) row[5]);
                    temp.setTipoDocumento((String) row[6]);
                    list.add(temp);

                }
            }
            result.setData(list);
            result.setSuccess(true);
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity obtenerEvaluacionCampoArchivo(
            EvaluacionCampoArchivoEntity p) {

        ResultClassEntity result = new ResultClassEntity();
        List<EvaluacionCampoArchivoEntity> objList=new ArrayList<EvaluacionCampoArchivoEntity>();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoArchivo_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    EvaluacionCampoArchivoEntity obj = new EvaluacionCampoArchivoEntity();
                    obj.setIdEvaluacionCampoArchivo((Integer)row[0]);
                    obj.setIdEvaluacionCampo((Integer)row[1]) ;
                    obj.setIdArchivo((Integer)row[2]);
                    obj.setIdTipoSeccionArchivo((Integer)row[3]);
                    obj.setIdTipoDocumento((String)row[4]);
                    obj.setNombre((String)row[5]);
                    obj.setTipoDocumento((String)row[6]);
                    objList.add(obj);

                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarEvaluacionCampo(EvaluacionCampoDto p) {
        ResultClassEntity result = new ResultClassEntity();
        List<EvaluacionCampoDto> objList=new ArrayList<EvaluacionCampoDto>();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampo_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacionCampoEstado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("titular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numTHActoAdmin", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacionVersion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pagSize", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
            processStored.setParameter("idEvaluacionCampoEstado", p.getIdEvaluacionCampoEstado());
            processStored.setParameter("titular", p.getTitular());
            processStored.setParameter("apellidoTitular", p.getApellidoTitular());
            processStored.setParameter("numTHActoAdmin", p.getNumThActoAdmin());
            processStored.setParameter("idEvaluacionVersion", p.getIdEvaluacionCampoVersion());
            processStored.setParameter("pageNum", p.getPageNum());
            processStored.setParameter("pagSize", p.getPageSize());
            processStored.setParameter("documentoGestion", p.getDocumentoGestion());
            processStored.setParameter("tipoDocumentoGestion", p.getTipoDocumentoGestion());

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    EvaluacionCampoDto obj=new EvaluacionCampoDto();
                    obj.setIdEvaluacionCampo((Integer)row[0]) ;
                    obj.setIdEvaluacionCampoEstado((Integer)row[1]) ;
                    obj.setDocumentoGestion((Integer)row[2]); ;
                    obj.setCites((Boolean)row[3]) ;
                    obj.setFechaInicioEvaluacion((Date)row[4]) ;
                    obj.setFechaFinEvaluacion((Date)row[5]) ;
                    obj.setFechaInicioImpedimentos((Date)row[6]); ;
                    obj.setFechaFinImpedimentos((Date)row[7]) ;
                    obj.setComunidadNatidadCampesina((Boolean)row[8]) ;
                    obj.setResultadoFavorable((Boolean) row[9]) ;
                    obj.setNotificacion((Boolean) row[10]) ;
                    obj.setIdEvaluacionCampoVersion((Integer) row[11]) ;
                    obj.setTipoDocumentoGestion((String)row[12]) ;
                    obj.setEvaluacionCampoEstado((String)row[13]) ;
                    obj.setTitular((String) row[14]);
                    obj.setNumThActoAdmin((String) row[15]);
                    obj.setIdUsuarioRegistro((Integer) row[16]);
                    obj.setTipoDocumentoGestionTexto((String) row[17]);
                    result.setTotalRecord((Integer) row[18]);

                    objList.add(obj);

                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }


    }


    
}
