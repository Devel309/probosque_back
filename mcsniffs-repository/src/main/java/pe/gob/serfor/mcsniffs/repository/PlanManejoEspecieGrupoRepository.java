package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEspecieGrupoEntity;

public interface PlanManejoEspecieGrupoRepository {

    ResultClassEntity registrarPlanManejoEspecieGrupo(PlanManejoEspecieGrupoEntity obj);
}
