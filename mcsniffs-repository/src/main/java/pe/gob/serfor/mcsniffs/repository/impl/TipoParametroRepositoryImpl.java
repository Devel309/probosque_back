package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.TipoParametroDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.TipoParametroRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class TipoParametroRepositoryImpl extends JdbcDaoSupport implements TipoParametroRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(TipoParametroRepositoryImpl.class);

    @Override
    public ResultClassEntity listarTipoParametro(TipoParametroDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<TipoParametroDto> lista = new ArrayList<TipoParametroDto>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoParametro_ListarPorFiltro");

            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ValorPrimario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionValorPrimario", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigo", obj.getCodigo());
            processStored.setParameter("ValorPrimario", obj.getValorPrimario());
            processStored.setParameter("descripcionValorPrimario", obj.getDescripcionValorPrimario());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                TipoParametroDto c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new TipoParametroDto();
                        c.setIdParametro(row[0] == null ? null : (Integer) row[0]);
                        c.setIdTipoParametro(row[1] == null ? null : (Integer) row[1]);
                        c.setCodigo(row[2] == null ? null : (String) row[2]);
                        c.setValorPrimario(row[3] == null ? null : (String) row[3]);
                        c.setDescripcionValorPrimario(row[4] == null ? null : (String) row[4]);
                        c.setValorTerciario(row[5] == null ? null : (String) row[5]);
                        c.setNombre(row[6] == null ? null : (String) row[6]);
                        c.setDescripcion(row[7] == null ? null : (String) row[7]);
                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar tipo parametro.");
            return result;
        } catch (Exception e) {
            log.error("listarActividadSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar tipo parametro");
            return result;
        }
    }

    
   @Override
    public ResultClassEntity actualizarParametro(TipoParametroDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Parametro_Actualizar");

            processStored.registerStoredProcedureParameter("idParametro", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("valorPrimario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idParametro", obj.getIdParametro());
            processStored.setParameter("valorPrimario", obj.getValorPrimario());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó correctamente el parametro");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el parametro");
            result.setInnerException(e.getMessage());
            return result;
        }
    }


}
