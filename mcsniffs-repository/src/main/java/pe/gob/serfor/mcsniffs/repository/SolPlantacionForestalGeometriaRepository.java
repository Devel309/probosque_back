package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalGeometriaEntity;


public interface SolPlantacionForestalGeometriaRepository {
    ResultClassEntity registrarSolPlantacionForestalGeometria(SolPlantacionForestalGeometriaEntity item) throws Exception;
    ResultClassEntity listarSolPlantacionForestalGeometria (SolPlantacionForestalGeometriaEntity item);
    ResultClassEntity actualizarSolPlantacionForestalGeometria (SolPlantacionForestalGeometriaEntity item) throws Exception;
    ResultClassEntity eliminarSolPlantacionForestalGeometriaArchivo(SolPlantacionForestalGeometriaEntity item);
    ResultClassEntity obtenerSolPlantacionForestalGeometria (SolPlantacionForestalGeometriaEntity item) throws Exception;
}
