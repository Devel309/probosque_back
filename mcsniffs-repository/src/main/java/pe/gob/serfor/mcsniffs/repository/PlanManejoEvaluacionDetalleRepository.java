package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface PlanManejoEvaluacionDetalleRepository {

    List<PlanManejoEvaluacionDetalleDto> listarPlanManejoEvaluacionDet(Integer idPlanManejoEval);
    ResultClassEntity ListarPlanManejoEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param);

    ResultClassEntity RegistrarPlanManejoEvaluacionLineamiento(List<PlanManejoEvaluacionDetalleDto> param);

    ResultClassEntity registrarPlanManejoEvaluacionDet(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto) throws Exception;
    ResultClassEntity actualizarPlanManejoEvaluacionDet(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto) throws Exception;
    ResultClassEntity eliminarPlanManejoEvaluacionDet(PlanManejoEvaluacionDetalleDto planManejoEvaluacionDetalleDto) throws Exception;

    ResultClassEntity obtenerEvaluacionPlanManejo(Integer idPlanManejo) throws Exception;
    
}
