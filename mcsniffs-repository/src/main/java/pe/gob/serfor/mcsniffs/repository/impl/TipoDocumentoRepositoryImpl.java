package pe.gob.serfor.mcsniffs.repository.impl;

 

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.TipoDocumentoEntity;
import pe.gob.serfor.mcsniffs.repository.TipoDocumentoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class TipoDocumentoRepositoryImpl extends JdbcDaoSupport implements TipoDocumentoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(TipoDocumentoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public List<TipoDocumentoEntity> listaTipoDocumentoFiltro(TipoDocumentoEntity dto) throws Exception {
         
        List<TipoDocumentoEntity> lista = new ArrayList<TipoDocumentoEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoDocumento_ListarFiltro");
            processStored.registerStoredProcedureParameter("codigoDoc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoDocSub", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoDoc",dto.getCodigoDoc());
            processStored.setParameter("codigoDocSub",dto.getCodigoDocSub());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                TipoDocumentoEntity temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new TipoDocumentoEntity();
                        temp.setIdTipoDocumento(row[0]==null?null:(Integer) row[0]);
                        temp.setDescripcion(row[1]==null?null:(String) row[1]);
                        temp.setCodigoDoc(row[2]==null?null:(String) row[2]);
                        temp.setCodigoDocSub(row[3]==null?null:(String) row[3]);     
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listaTipoDocumentoFiltro", e.getMessage());
            throw e;
        }
    } 

     
}
