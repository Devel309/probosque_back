package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiltroFiscalizacionDto;
import pe.gob.serfor.mcsniffs.entity.Fiscalizacion.FiscalizacionDto;

public interface FiscalizacionRepository {
    ResultClassEntity registrarFiscalizacion(FiscalizacionDto dto) throws Exception;
    ResultClassEntity AdjuntarArchivosFiscalizacion(FiscalizacionEntity obj);
    ResultClassEntity listarFiscalizacion(FiscalizacionDto obj);
    ResultEntity<FiscalizacionAdjuntosEntity> ObtenerArchivosFiscalizacion(Integer IdFiscalizacion);
    FiscalizacionDto ObtenerFiscalizacion(FiscalizacionDto obj);
    ResultArchivoEntity DescagarPlantillaFormatoFiscalizacion();
  
}
