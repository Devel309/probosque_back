package pe.gob.serfor.mcsniffs.repository;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface PlanManejoEvaluacionIterRepository {

    ResultEntity<PlanManejoEvaluacionIterDto> listarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto);

    ResultClassEntity registrarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception;

    ResultClassEntity actualizarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception;



    ResultClassEntity eliminarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception;

}
