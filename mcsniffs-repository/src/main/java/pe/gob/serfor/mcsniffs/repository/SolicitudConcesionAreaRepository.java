package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;

public interface SolicitudConcesionAreaRepository {
    ResultClassEntity listarSolicitudConcesionArea (SolicitudConcesionAreaEntity obj);
    ResultClassEntity registrarSolicitudConcesionArea (SolicitudConcesionAreaEntity obj);
    ResultClassEntity actualizarSolicitudConcesionArea (SolicitudConcesionAreaEntity obj);
    ResultClassEntity eliminarSolicitudConcesionArea (SolicitudConcesionAreaEntity obj);
}
