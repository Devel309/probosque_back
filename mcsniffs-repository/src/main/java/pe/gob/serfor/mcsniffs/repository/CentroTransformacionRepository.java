package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.centroTansformacion.CentroTransformacionDto;

public interface CentroTransformacionRepository {
    ResultClassEntity ListarCentroTransformacion(CentroTransformacionDto obj) throws Exception;
}
