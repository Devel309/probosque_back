package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

public interface SolicitudConcesionIngresoCuentaRepository {
    ResultClassEntity listarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj);
    ResultClassEntity registrarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj);
    ResultClassEntity eliminarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj);

}
