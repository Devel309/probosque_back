package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHRegenteDto;

import java.util.List;

public interface TituloHabilitanteRegenteRepository {

    ResultClassEntity listarInformacionTHRegente(InformacionTHRegenteDto param) throws Exception;
}
