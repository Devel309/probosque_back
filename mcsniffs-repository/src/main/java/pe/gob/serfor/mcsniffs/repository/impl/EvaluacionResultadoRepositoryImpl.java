package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionResultadoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.EvaluacionResultadoRepository;
import pe.gob.serfor.mcsniffs.repository.PotencialProduccionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class EvaluacionResultadoRepositoryImpl extends JdbcDaoSupport
        implements EvaluacionResultadoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(EvaluacionResultadoRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";

    @Autowired
    private FileServerConexion fileServerConexion;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<EvaluacionResultadoDetalleDto> ListarEvaluacionResultado(Integer idPlanManejo, String codResultado, String subCodResultado) throws Exception {
        List<EvaluacionResultadoDetalleDto> lista = new ArrayList<EvaluacionResultadoDetalleDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionResultadoDetalle_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codResultado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodResultado", String.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codResultado", codResultado);
            processStored.setParameter("subCodResultado", subCodResultado);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        EvaluacionResultadoDetalleDto temp = new EvaluacionResultadoDetalleDto();
                        temp.setIdEvalResultado((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setCodResultado((String) row[2]);
                        temp.setSubCodResultado((String) row[3]);
                        temp.setDescripcionCab((String) row[4]);
                        temp.setCodInforme((String) row[5]);
                        //DETALLE
                        temp.setIdEvalResultadoDet((Integer) row[6]);
                        temp.setCodResultadoDet((String) row[7]);
                        temp.setTipoArchivo((String) row[8]);
                        temp.setTipoDocumento((String) row[9]);
                        temp.setNuDocumento((Integer) row[10]);
                        temp.setNombreFirmante((String) row[11]);
                        temp.setPaternoFirmante((String) row[12]);
                        temp.setMaternoFirmante((String) row[13]);
                        temp.setCargoFirmante((String) row[14]);
                        temp.setFechaNotificacion((Timestamp) row[15]);
                        temp.setAsunto((String) row[16]);
                        temp.setPerfil((String) row[17]);
                        temp.setObservacion((String) row[18]);
                        temp.setDetalle((String) row[19]);
                        temp.setDescripcion((String) row[20]);
                        temp.setFechaResolucion((Timestamp) row[21]);
                        temp.setNuDocumentoFirmante((Integer) row[22]);
                        temp.setFechaConsentimiento(row[23]!=null?(Date) row[23]:null);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }


    @Override
    public ResultClassEntity RegistrarEvaluacionResultado(List<EvaluacionResultadoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<EvaluacionResultadoEntity> list_ = new ArrayList<>();
        try {
            for (EvaluacionResultadoEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionResultado_Registrar");
                sp.registerStoredProcedureParameter("idEvalResultado", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codResultado", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subCodResultado", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcionCab", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codInforme", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaConsentimiento", Date.class, ParameterMode.IN);

                SpUtil.enableNullParams(sp);
                sp.setParameter("idEvalResultado", p.getIdEvalResultado());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codResultado", p.getCodResultado());
                sp.setParameter("subCodResultado", p.getSubCodResultado());
                sp.setParameter("descripcionCab", p.getDescripcionCab());
                sp.setParameter("codInforme", p.getCodInforme());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("fechaConsentimiento", p.getFechaConsentimiento());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        EvaluacionResultadoEntity obj_ = new EvaluacionResultadoEntity();
                        obj_.setIdEvalResultado((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setCodResultado((String) row_[2]);
                        obj_.setSubCodResultado((String) row_[3]);
                        obj_.setDescripcionCab((String) row_[4]);
                        obj_.setCodInforme((String) row_[5]);

                        List<EvaluacionResultadoDetalleEntity> listDet = new ArrayList<>();
                        for (EvaluacionResultadoDetalleEntity param : p.getListEvaluacionResultado()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_EvaluacionResultadoDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idEvalResultadoDet", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idEvalResultado", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codResultadoDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoArchivo", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuDocumento", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuDocumentoFirmante", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreFirmante", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("paternoFirmante", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("maternoFirmante", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("cargoFirmante", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("fechaNotificacion", Timestamp.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("fechaResolucion", Timestamp.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                            SpUtil.enableNullParams(pa);
                            pa.setParameter("idEvalResultadoDet", param.getIdEvalResultadoDet());
                            pa.setParameter("idEvalResultado", obj_.getIdEvalResultado());
                            pa.setParameter("codResultadoDet", param.getCodResultadoDet());
                            pa.setParameter("tipoArchivo", param.getTipoArchivo());
                            pa.setParameter("tipoDocumento", param.getTipoDocumento());
                            pa.setParameter("nuDocumento", param.getNuDocumento());
                            pa.setParameter("nuDocumentoFirmante", param.getNuDocumentoFirmante());
                            pa.setParameter("nombreFirmante", param.getNombreFirmante());
                            pa.setParameter("paternoFirmante", param.getPaternoFirmante());
                            pa.setParameter("maternoFirmante", param.getMaternoFirmante());
                            pa.setParameter("cargoFirmante", param.getCargoFirmante());
                            pa.setParameter("fechaNotificacion", param.getFechaNotificacion());
                            pa.setParameter("fechaResolucion", param.getFechaResolucion());
                            pa.setParameter("asunto", param.getAsunto());
                            pa.setParameter("perfil", param.getPerfil());
                            pa.setParameter("observacion", param.getObservacion());
                            pa.setParameter("detalle", param.getDetalle());
                            pa.setParameter("descripcion", param.getDescripcion());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                EvaluacionResultadoDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new EvaluacionResultadoDetalleEntity();
                                    obj.setIdEvalResultadoDet((Integer) row[0]);
                                    obj.setCodResultadoDet((String) row[1]);
                                    obj.setTipoArchivo((String) row[2]);
                                    obj.setTipoDocumento((String) row[3]);
                                    obj.setNuDocumento((Integer) row[4]);
                                    obj.setNombreFirmante((String) row[5]);
                                    obj.setPaternoFirmante((String) row[6]);
                                    obj.setMaternoFirmante((String) row[7]);
                                    obj.setCargoFirmante((String) row[8]);
                                    obj.setFechaNotificacion((Timestamp) row[9]);
                                    obj.setAsunto((String) row[10]);
                                    obj.setPerfil((String) row[11]);
                                    obj.setObservacion((String) row[12]);
                                    obj.setDetalle((String) row[13]);
                                    obj.setDescripcion((String) row[14]);
                                    obj.setFechaResolucion((Timestamp) row[15]);
                                    obj.setNuDocumentoFirmante((Integer) row[16]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListEvaluacionResultado(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se envió información del resultado de la evaluación.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public ResultClassEntity EliminarEvaluacionResultado(EvaluacionResultadoDetalleDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_EvaluacionResultado_Eliminar]");
        processStored.registerStoredProcedureParameter("idEvalResultado", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idEvalResultadoDet", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);


        processStored.setParameter("idEvalResultado", param.getIdEvalResultado());
        processStored.setParameter("idEvalResultadoDet", param.getIdEvalResultadoDet());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

}
