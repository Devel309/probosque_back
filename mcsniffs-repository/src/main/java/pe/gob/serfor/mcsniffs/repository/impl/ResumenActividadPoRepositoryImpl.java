package pe.gob.serfor.mcsniffs.repository.impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.MonitoreoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResumenActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;
import pe.gob.serfor.mcsniffs.repository.ResumenActividadPoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * @autor: Harry Coa [29-07-2021]
 * @modificado:
 * @descripción: {Repositorio Resumen Evaluacion}
 *
 */

@Repository
public class ResumenActividadPoRepositoryImpl extends JdbcDaoSupport implements ResumenActividadPoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ResumenActividadPoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Harry Coa [29-07-2021]
     * @modificado:
     * @descripción: {Registrar Resumen}
     * @param:Capacitacion
     */

    @Override
    public ResultClassEntity RegistrarResumenActividad(List<ResumenActividadPoEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ResumenActividadPoEntity> list_ = new ArrayList<>();
        try{
            for(ResumenActividadPoEntity p:list){
                //p.setCodTipoResumen("PORAPO");
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadPo_Registrar");
                processStored_.registerStoredProcedureParameter("idResumenAct", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codTipoResumen", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codigoResumen", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codigoSubResumen", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("nroResolucion", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("nroPcs", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("areaHa", Double.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                setStoreProcedureEnableNullParameters(processStored_);

                processStored_.setParameter("idResumenAct",p.getIdResumenAct());
                processStored_.setParameter("codTipoResumen", p.getCodTipoResumen());
                processStored_.setParameter("codigoResumen", p.getCodigoResumen());
                processStored_.setParameter("codigoSubResumen", p.getCodigoSubResumen());
                processStored_.setParameter("nroResolucion",p.getNroResolucion());
                processStored_.setParameter("nroPcs", p.getNroPcs());
                processStored_.setParameter("areaHa", p.getAreaHa());
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored_.execute();
                List<Object[]> spResult_ =processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        ResumenActividadPoEntity obj_ = new ResumenActividadPoEntity();
                        obj_.setIdResumenAct((Integer) row_[0]);
                        obj_.setCodTipoResumen((String) row_[1]);
                        obj_.setNroResolucion((Integer) row_[2]);
                        obj_.setNroPcs((Integer) row_[3]);
                        Double area = row_[4] == null ? null : ((BigDecimal) row_[4]).doubleValue();
                        obj_.setAreaHa((Double) area);
                        obj_.setIdPlanManejo((Integer) row_[5]);
                        obj_.setCodigoResumen((String) row_[6]);
                        obj_.setCodigoSubResumen((String) row_[7]);
                        List<ResumenActividadPoDetalleEntity> listDet = new ArrayList<>();
                        for(ResumenActividadPoDetalleEntity param:p.getListResumenEvaluacion()){
                            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadPoDetalle_Registrar");
                            processStored.registerStoredProcedureParameter("idResumenActDet", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("tipoResumen", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("indicador", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("programado", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("realizado", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("resumen", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idResumenAct", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("aspectoRecomendacion", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("aspectoPositivo", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("aspectoNegativo", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("estadoVar", String.class, ParameterMode.IN);

                            setStoreProcedureEnableNullParameters(processStored);

                            processStored.setParameter("idResumenActDet", param.getIdResumenActDet());
                            processStored.setParameter("tipoResumen", param.getTipoResumen());
                            processStored.setParameter("actividad", param.getActividad());
                            processStored.setParameter("indicador", param.getIndicador());
                            processStored.setParameter("programado", param.getProgramado());
                            processStored.setParameter("realizado", param.getRealizado());
                            processStored.setParameter("resumen", param.getResumen());
                            processStored.setParameter("idResumenAct", obj_.getIdResumenAct());
                            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            processStored.setParameter("aspectoRecomendacion", param.getAspectoRecomendacion());
                            processStored.setParameter("aspectoPositivo", param.getAspectoPositivo());
                            processStored.setParameter("aspectoNegativo", param.getAspectoNegativo());
                            processStored.setParameter("estadoVar", param.getEstado());

                            processStored.execute();
                            List<Object[]> spResult =processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    ResumenActividadPoDetalleEntity obj = new ResumenActividadPoDetalleEntity();
                                    obj.setIdResumenActDet((Integer) row[0]);
                                    obj.setTipoResumen((Integer) row[1]);
                                    obj.setActividad((String) row[2]);
                                    obj.setIndicador((String) row[3]);
                                    obj.setProgramado((String) row[4]);
                                    obj.setRealizado((String) row[5]);
                                    obj.setResumen((String) row[6]);
                                    obj.setAspectoRecomendacion((String) row[7]);
                                    obj.setIdResumenAct((Integer) row[8]);
                                    obj.setAspectoPositivo((String) row[9]);
                                    obj.setAspectoNegativo((String) row[10]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListResumenEvaluacion(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el resumen correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public List<ResumenActividadDto> ListarResumenActividad(ResumenActividadDto param) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadPoDetalle_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeneral", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoGeneral", param.getCodTipoResumen());

            processStored.execute();
            List<ResumenActividadDto> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                ResumenActividadDto temp = null;
                for (Object[] row: spResult){
                    temp = new ResumenActividadDto();
                    temp.setIdResumenAct((Integer) row[0]);
                    temp.setIdPlanManejo((Integer) row[1]);
                    temp.setCodTipoResumen(row[2]==null?null:(String) row[2]);
                    temp.setNroResolucion(row[3]==null?null:(Integer) row[3]);
                    temp.setNroPcs(row[4]==null?null:(Integer) row[4]);              
                    temp.setAreaHa(row[5]==null?null:((BigDecimal) row[5]).doubleValue());
                    temp.setIdResumenActDet(row[6]==null?null:(Integer) row[6]);
                    temp.setTipoResumen(row[7]==null?null:(Integer) row[7]);
                    temp.setActividad(row[8]==null?null:(String) row[8]);
                    temp.setIndicador(row[9]==null?null:(String) row[9]);
                    temp.setProgramado(row[10]==null?null:(String) row[10]);
                    temp.setRealizado(row[11]==null?null:(String) row[11]);
                    temp.setResumen(row[12]==null?null:(String) row[12]);
                    temp.setAspectoRecomendacion(row[13]==null?null:(String) row[13]);
                    temp.setAspectoPositivo(row[14]==null?null:(String) row[14]);
                    temp.setAspectoNegativo(row[15]==null?null:(String) row[15]);
                    temp.setCodigoResumen(row[16]==null?null:(String) row[16]);
                    temp.setCodigoSubResumen(row[17]==null?null:(String) row[17]);
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public List<ResumenActividadPoEntity> ListarResumenActividad_Detalle(ResumenActividadPoEntity param) throws Exception {

        try {
            int idDetalle=0;
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividad_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeneral", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoGeneral", param.getCodTipoResumen());

            processStored.execute();
            List<ResumenActividadPoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null) {
                if (spResult.size() >= 1) {
                    ResumenActividadPoEntity temp = null;
                    for (Object[] row : spResult) {
                        temp = new ResumenActividadPoEntity();
                        temp.setIdResumenAct((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setCodTipoResumen((String) row[2]);
                        temp.setCodigoResumen(row[3] == null ? null : (String) row[3]);
                        temp.setCodigoSubResumen(row[4] == null ? null : (String) row[4]);
                        temp.setNroResolucion(row[5] == null ? null : (Integer) row[5]);
                        temp.setNroPcs(row[6] == null ? null : (Integer) row[6]);
                        temp.setAreaHa(row[7] == null ? null : ((BigDecimal) row[7]).doubleValue());

                        if (row[0] == null) {
                            idDetalle = 0;
                        } else {
                            idDetalle = (Integer) row[0];
                        }

                        //idDetalle=(Integer) row[0];
                        /********************************** OPERACION **********************************************/
                        List<ResumenActividadPoDetalleEntity> listDetActividad = new ArrayList<ResumenActividadPoDetalleEntity>();
                        StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadDetalle_Listar");
                        pa.registerStoredProcedureParameter("idResumenActividad", Integer.class, ParameterMode.IN);
                        pa.registerStoredProcedureParameter("codigoGeneral", String.class, ParameterMode.IN);
                        SpUtil.enableNullParams(pa);
                        if (idDetalle == 0) {
                            pa.setParameter("idResumenActividad", null);
                        } else {
                            pa.setParameter("idResumenActividad", idDetalle);
                        }
                        pa.setParameter("codigoGeneral", param.getCodTipoResumen());
                        pa.execute();
                        List<Object[]> spResultAct = pa.getResultList();
                        if (spResultAct != null) {
                            if (spResultAct.size() >= 1) {
                                for (Object[] rowAct : spResultAct) {
                                    ResumenActividadPoDetalleEntity temp2 = new ResumenActividadPoDetalleEntity();
                                    temp2 = new ResumenActividadPoDetalleEntity();
                                    temp2.setIdResumenActDet(rowAct[0] == null ? null : (Integer) rowAct[0]);
                                    temp2.setTipoResumen(rowAct[1] == null ? null : (Integer) rowAct[1]);
                                    temp2.setActividad(rowAct[2] == null ? null : (String) rowAct[2]);
                                    temp2.setIndicador(rowAct[3] == null ? null : (String) rowAct[3]);
                                    temp2.setProgramado(rowAct[4] == null ? null : (String) rowAct[4]);
                                    temp2.setRealizado(rowAct[5] == null ? null : (String) rowAct[5]);
                                    temp2.setResumen(rowAct[6] == null ? null : (String) rowAct[6]);
                                    temp2.setAspectoRecomendacion(rowAct[7] == null ? null : (String) rowAct[7]);
                                    temp2.setAspectoPositivo(rowAct[8] == null ? null : (String) rowAct[8]);
                                    temp2.setAspectoNegativo(rowAct[9] == null ? null : (String) rowAct[9]);
                                    listDetActividad.add(temp2);
                                }
                            }
                        }
                        /********************************************************************************/
                        temp.setListResumenActividadDetalle(listDetActividad);


                        result.add(temp);
                    }
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity EliminarResumenActividad(ResumenActividadPoDetalleEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadPoDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idResumenActDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idResumenActDet", param.getIdResumenActDet());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó el Resumen de Actividad con Éxito.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public List<ResumenActividadDto> ListarResumenActividadPO(ResumenActividadDto param) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadPo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idResumenActividad", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idResumenActividad", param.getIdResumenAct());
            processStored.execute();
            List<ResumenActividadDto> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ResumenActividadDto temp = new ResumenActividadDto();
                    temp.setIdResumenAct((Integer) row[0]);
                    temp.setIdPlanManejo((Integer) row[1]);
                    temp.setCodTipoResumen((String) row[2]);
                    temp.setNroResolucion((Integer) row[3]);
                    temp.setNroPcs((Integer) row[4]);
                    double area =((BigDecimal) row[5]).doubleValue();
                    temp.setAreaHa((Double) area);


                    StoredProcedureQuery processStoredDet = entityManager.createStoredProcedureQuery("dbo.pa_ResumenActividadPoDetalle_Listar");
                    processStoredDet.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStoredDet.registerStoredProcedureParameter("idResumenActividad", Integer.class, ParameterMode.IN);
                    processStoredDet.setParameter("idPlanManejo", param.getIdPlanManejo());
                    processStoredDet.setParameter("idResumenActividad", param.getIdResumenAct());
                    processStoredDet.execute();

                    List<ResumenActividadPoDetalleEntity> listaDetalle = new ArrayList();
                    List<Object[]> spResultDet = processStoredDet.getResultList();
                    if (spResultDet.size() >= 1) {
                        for (Object[] rowDet : spResultDet) {
                            ResumenActividadPoDetalleEntity detalleTemp = new ResumenActividadPoDetalleEntity();
                            detalleTemp.setIdResumenActDet((Integer) rowDet[6]);
                            detalleTemp.setTipoResumen((Integer) rowDet[7]);
                            detalleTemp.setActividad((String) rowDet[8]);
                            detalleTemp.setIndicador((String) rowDet[9]);
                            detalleTemp.setProgramado((String) rowDet[10]);
                            detalleTemp.setRealizado((String) rowDet[11]);
                            detalleTemp.setResumen((String) rowDet[12]);
                            detalleTemp.setAspectoRecomendacion((String) rowDet[13]);

                            listaDetalle.add(detalleTemp);
                        }
                    }



                    temp.setListResumenActividadDetalle(listaDetalle);
                    result.add(temp);
                }




            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }


    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }
    }

}
