package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.TalaEntity;

public interface SincronizacionRepository {
    List<TalaEntity> listarTala(String ruta) throws Exception;
}
