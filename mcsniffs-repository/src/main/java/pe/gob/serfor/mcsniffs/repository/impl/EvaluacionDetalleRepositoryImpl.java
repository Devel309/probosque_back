package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionResumidoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionResumidoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class EvaluacionDetalleRepositoryImpl extends JdbcDaoSupport implements EvaluacionDetalleRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(EvaluacionDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }




    @Override
    public List<EvaluacionPermisoForestalDetalleDto> listarEvaluacionPermisoForestaDetalle(EvaluacionPermisoForestalDetalleDto dto) throws Exception {

        List<EvaluacionPermisoForestalDetalleDto> lista = new ArrayList<EvaluacionPermisoForestalDetalleDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestalDetalle_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacionDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetPost", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionDetalle",dto.getIdEvaluacionPermisoDet());
            processStored.setParameter("idEvaluacion",dto.getIdEvaluacionPermiso());
            processStored.setParameter("codigoEvaluacionDet",dto.getCodigoEvaluacionDet());
            processStored.setParameter("codigoEvaluacionDetSub",dto.getCodigoEvaluacionDetSub());
            processStored.setParameter("codigoEvaluacionDetPost",dto.getCodigoEvaluacionDetPost());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                EvaluacionPermisoForestalDetalleDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new EvaluacionPermisoForestalDetalleDto();
                        temp.setIdEvaluacionPermisoDet(row[0]==null?null:(Integer) row[0]);
                        temp.setIdEvaluacionPermiso(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoEvaluacionDet(row[2]==null?null:(String) row[2]);
                        temp.setCodigoEvaluacionDetSub(row[3]==null?null:(String) row[3]);
                        temp.setCodigoEvaluacionDetPost(row[4]==null?null:(String) row[4]);
                        temp.setConforme(row[5]==null?null:(String) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setDetalle(row[7]==null?null:(String) row[7]);
                        temp.setDescripcion(row[8]==null?null:(String) row[8]);
                        temp.setEstadoEvaluacionDet(row[9]==null?null:(String) row[9]);
                        temp.setIdArchivo(row[10]==null?null:(Integer) row[10]);
                        temp.setFechaEvaluacionDetInicial(row[11]==null?null:(Timestamp) row[11]);
                        temp.setFechaEvaluacionDetFinal(row[12]==null?null:(Timestamp) row[12]);
                        temp.setEstado(row[13]==null?null:(String) row[13]);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacionDetalle", e.getMessage());
            throw e;
        }
    }



    @Override
    public List<EvaluacionDetalleDto> listarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception {
         
        List<EvaluacionDetalleDto> lista = new ArrayList<EvaluacionDetalleDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionDetalle_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacionDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetPost", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionDetalle",dto.getIdEvaluacionDet());
            processStored.setParameter("idEvaluacion",dto.getIdEvaluacion());
            processStored.setParameter("codigoEvaluacionDet",dto.getCodigoEvaluacionDet());
            processStored.setParameter("codigoEvaluacionDetSub",dto.getCodigoEvaluacionDetSub());
            processStored.setParameter("codigoEvaluacionDetPost",dto.getCodigoEvaluacionDetPost());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                EvaluacionDetalleDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new EvaluacionDetalleDto();
                        temp.setIdEvaluacionDet(row[0]==null?null:(Integer) row[0]);
                        temp.setIdEvaluacion(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoEvaluacionDet(row[2]==null?null:(String) row[2]);
                        temp.setCodigoEvaluacionDetSub(row[3]==null?null:(String) row[3]);
                        temp.setCodigoEvaluacionDetPost(row[4]==null?null:(String) row[4]);
                        temp.setConforme(row[5]==null?null:(String) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setDetalle(row[7]==null?null:(String) row[7]);
                        temp.setDescripcion(row[8]==null?null:(String) row[8]);
                        temp.setEstadoEvaluacionDet(row[9]==null?null:(String) row[9]);
                        temp.setIdArchivo(row[10]==null?null:(Integer) row[10]);
                        temp.setFechaEvaluacionDetInicial(row[11]==null?null:(Timestamp) row[11]);
                        temp.setFechaEvaluacionDetFinal(row[12]==null?null:(Timestamp) row[12]);
                        temp.setEstado(row[13]==null?null:(String) row[13]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacionDetalle", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<EvaluacionResumidoDetalleDto> listarEvaluacionDetalleResumido(EvaluacionResumidoDto dto) throws Exception {

        List<EvaluacionResumidoDetalleDto> lista = new ArrayList<EvaluacionResumidoDetalleDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionDetalleResumido_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacion",dto.getIdEvaluacion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                EvaluacionResumidoDetalleDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new EvaluacionResumidoDetalleDto();
                        temp.setIdEvaluacionDet(row[0]==null?null:(Integer) row[0]);
                        temp.setIdEvaluacion(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoEvaluacionDet(row[2]==null?null:(String) row[2]);
                        temp.setCodigoEvaluacionDetSub(row[3]==null?null:(String) row[3]);
                        temp.setCodigoEvaluacionDetPost(row[4]==null?null:(String) row[4]);
                        temp.setConforme(row[5]==null?null:(String) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setTab(row[7]==null?null:(String) row[7]);
                        temp.setAcordeon(row[8]==null?null:(String) row[8]);
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacionDetalle", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity registrarEvaluacionPermisoForestalDetalle(EvaluacionPermisoForestalDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestalDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idEvaluacionDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetPost", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionDetIni", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionDetFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEvaluacionDet", dto.getIdEvaluacionPermisoDet());
            processStored.setParameter("idEvaluacion", dto.getIdEvaluacionPermiso());
            processStored.setParameter("codigoEvaluacionDet", dto.getCodigoEvaluacionDet());
            processStored.setParameter("codigoEvaluacionDetSub", dto.getCodigoEvaluacionDetSub());
            processStored.setParameter("codigoEvaluacionDetPost", dto.getCodigoEvaluacionDetPost());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("estadoEvaluacionDet", dto.getEstadoEvaluacionDet());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("fechaEvaluacionDetIni", dto.getFechaEvaluacionDetInicial());
            processStored.setParameter("fechaEvaluacionDetFin", dto.getFechaEvaluacionDetFinal());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró evaluación de permiso forestal correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idEvaluacionDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetPost", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionDetIni", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionDetFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
 
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEvaluacionDet", dto.getIdEvaluacionDet());
            processStored.setParameter("idEvaluacion", dto.getIdEvaluacion());
            processStored.setParameter("codigoEvaluacionDet", dto.getCodigoEvaluacionDet());
            processStored.setParameter("codigoEvaluacionDetSub", dto.getCodigoEvaluacionDetSub());
            processStored.setParameter("codigoEvaluacionDetPost", dto.getCodigoEvaluacionDetPost());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("estadoEvaluacionDet", dto.getEstadoEvaluacionDet());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("fechaEvaluacionDetIni", dto.getFechaEvaluacionDetInicial());
            processStored.setParameter("fechaEvaluacionDetFin", dto.getFechaEvaluacionDetFinal());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity eliminarEvaluacionPermisoForestalDetalle(EvaluacionPermisoForestalDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestalDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idEvaluacionDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEvaluacionDetalle", dto.getIdEvaluacionPermisoDet());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

 
    @Override
    public ResultClassEntity eliminarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idEvaluacionDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
 
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEvaluacionDetalle", dto.getIdEvaluacionDet());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
