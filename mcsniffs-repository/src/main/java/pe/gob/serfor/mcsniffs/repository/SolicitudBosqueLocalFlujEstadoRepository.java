package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalFlujEstadoEntity;

public interface SolicitudBosqueLocalFlujEstadoRepository {

    ResultClassEntity listarSolicitudBosqueLocalFlujEstado(SolicitudBosqueLocalFlujEstadoEntity obj);
}
