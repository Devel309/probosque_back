package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ConsultaEntidadRepository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Repository
public class ConsultaEntidadRepositoryImpl extends JdbcDaoSupport implements ConsultaEntidadRepository {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {repositorio de la tabla Consulta entidad}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoRepositoryImpl.class);
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Lista las entidades a las cuales se les puede hacer consultas}
     * @param:
     */
    @Override
    public ResultEntity<ConsultaEntidadEntity> listarEntidades() {
        ResultEntity<ConsultaEntidadEntity> result = new ResultEntity<>();
        try {
            String sql = "EXEC pa_ConsultaEntidad_Listar";
            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
            List<ConsultaEntidadEntity> lstentidades = new ArrayList<>();
            if (rows.size()>=1) {
                for(Map<String, Object> row:rows){
                    ConsultaEntidadEntity obj = new ConsultaEntidadEntity();
                    obj.setIdConsultaEntidad((Integer)row.get("NU_ID_CONSULTA_ENTIDAD"));
                    obj.setDescripcion((String)row.get("TX_DESCRIPCION"));
                    lstentidades.add(obj);
                }
            }
            result.setData(lstentidades);
            result.setIsSuccess(true);
            result.setMessage("Se listo correctamente");
            return result;
        } catch (Exception e) {
            log.error("TipoProceso - listarTipoProceso", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }


}
