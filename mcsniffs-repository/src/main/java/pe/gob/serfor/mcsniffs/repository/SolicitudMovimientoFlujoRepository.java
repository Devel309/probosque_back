package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudMovimientoFlujoEntity;

public interface SolicitudMovimientoFlujoRepository {
    ResultClassEntity  registroSolicitudMovimientoFlujo(SolicitudMovimientoFlujoEntity entity);
}
