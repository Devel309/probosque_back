package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudBosqueLocalEvaluacionDetalleRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalEvaluacionDetalleRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(SolicitudBosqueLocalEvaluacionDetalleRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalEvaluacionDetalleDto> lista = new ArrayList<SolicitudBosqueLocalEvaluacionDetalleDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolBosqueLocalEvuacionDetalle_Listar");
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());
            processStored.setParameter("codigoSeccion",obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion",obj.getCodigoSubSeccion());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalEvaluacionDetalleDto c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalEvaluacionDetalleDto();
                        c.setIdSolBosqueLocalEvaluacionDetalle(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolBosqueLocalEvaluacion(row[1]==null?null:(Integer) row[1]);
                        c.setCodigoSeccion(row[2]==null?null:(String) row[2]);
                        c.setCodigoSubSeccion(row[3]==null?null:(String) row[3]);
                        c.setValidado(row[4]==null?null:(Boolean) row[4]);
                        c.setObservacion(row[5]==null?null:(String) row[5]);
                        //c.setEstado(row[6]==null?null:(String) row[6]);
                        c.setIdSolBosqueLocal(row[7]==null?null:(Integer) row[7]);
                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar solicitud bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocaldetalle", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar solicitud bosque local");
            return  result;
        }
    }


    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_EvaluacionDetalle_Registrar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocalEvaluacionDetalle", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("validado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocalEvaluacion", obj.getIdSolBosqueLocalEvaluacion());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("validado", obj.getValidado());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idEvaluacionDetalleSolicitudBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocalEvaluacionDetalle");
            obj.setIdSolBosqueLocalEvaluacionDetalle(idEvaluacionDetalleSolicitudBosqueLocal);
            result.setCodigo(idEvaluacionDetalleSolicitudBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la evaluación de la solicitud de bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la evaluación de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocalEvaluacionDetalle(List<SolicitudBosqueLocalEvaluacionDetalleDto> listObj){
        ResultClassEntity result = new ResultClassEntity();
        try {

            for(SolicitudBosqueLocalEvaluacionDetalleDto obj:listObj){
                StoredProcedureQuery processStored = entityManager
                        .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_EvaluacionDetalle_Actualizar");

                processStored.registerStoredProcedureParameter("idSolBosqueLocalEvaluacionDetalle", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idSolBosqueLocalEvaluacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("validado", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idSolBosqueLocalEvaluacionDetalle", obj.getIdSolBosqueLocalEvaluacionDetalle());
                processStored.setParameter("idSolBosqueLocalEvaluacion", obj.getIdSolBosqueLocalEvaluacion());
                processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
                processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
                processStored.setParameter("validado", obj.getValidado());
                processStored.setParameter("observacion", obj.getObservacion());
                processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

                processStored.execute();
            }
            result.setSuccess(true);
            result.setMessage("Se actualizó la evaluación  de bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar la evaluación  de bosque local correctamente.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

}
