package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;

public interface TalaRepository {
    ResultClassEntity registrarTala(TalaEntity item) throws Exception;
    ResultClassEntity registrarTalaDetalle (TalaDetalleEntity item) throws Exception;
}
