package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.CoordenadaUtmEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.CoordenadaUtmRepository;


import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Carlos Tang [20-08-2021]
 * @modificado:
 * @descripción: {Repository unidad manejo}
 */
@Repository
public class CoordenadaUtmRepositoryImpl extends JdbcDaoSupport implements CoordenadaUtmRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(UnidadManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity RegistrarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CoordenadaUtm_Registrar");
            processStored.registerStoredProcedureParameter("idCoordenadaUtm", Integer.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("idUnidadManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anexoSector", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vertice", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("este", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("norte", Double.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);

            processStored.setParameter("idCoordenadaUtm", coordenadaUtmEntity.getIdCoordUtm());
            processStored.setParameter("idUnidadManejo", coordenadaUtmEntity.getIdUnidadManejo());
            processStored.setParameter("anexoSector", coordenadaUtmEntity.getAnexoSector()==null?"":coordenadaUtmEntity.getAnexoSector());
            processStored.setParameter("referencia", coordenadaUtmEntity.getReferencia()==null?"":coordenadaUtmEntity.getReferencia());
            processStored.setParameter("vertice", coordenadaUtmEntity.getVertice());
            processStored.setParameter("tipo", coordenadaUtmEntity.getTipo());
            processStored.setParameter("idUsuarioRegistro", coordenadaUtmEntity.getIdUsuarioRegistro());
            processStored.setParameter("este", coordenadaUtmEntity.getEste());
            processStored.setParameter("norte", coordenadaUtmEntity.getNorte());

            processStored.setParameter("estado", coordenadaUtmEntity.getEstado());
            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró coordenada utm.");
        return  result;
    }

    @Override
    public ResultClassEntity ActualizarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CoordenadaUtm_Actualizar");
            processStored.registerStoredProcedureParameter("idCoordenadaUtm", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUnidadManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anexoSector", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vertice", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("este", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("norte", Double.class, ParameterMode.IN);

            processStored.setParameter("idCoordenadaUtm", coordenadaUtmEntity.getIdCoordUtm());
            processStored.setParameter("idUnidadManejo", coordenadaUtmEntity.getIdUnidadManejo());
            processStored.setParameter("anexoSector", coordenadaUtmEntity.getAnexoSector()==null?"":coordenadaUtmEntity.getAnexoSector());
            processStored.setParameter("referencia", coordenadaUtmEntity.getReferencia()==null?"":coordenadaUtmEntity.getReferencia());
            processStored.setParameter("vertice", coordenadaUtmEntity.getVertice()==null?"":coordenadaUtmEntity.getVertice());
            processStored.setParameter("tipo", coordenadaUtmEntity.getTipo());
            processStored.setParameter("estado", coordenadaUtmEntity.getEstado());
            processStored.setParameter("idUsuarioModificacion", coordenadaUtmEntity.getIdUsuarioModificacion());

            processStored.setParameter("este", coordenadaUtmEntity.getEste());
            processStored.setParameter("norte", coordenadaUtmEntity.getNorte());


            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró coordenada utm.");
        return  result;
    }

    @Override
    public ResultClassEntity EliminarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CoordenadaUtm_Eliminar");
            processStored.registerStoredProcedureParameter("idUnidadManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idUnidadManejo", coordenadaUtmEntity.getIdUnidadManejo());
            processStored.setParameter("tipo", coordenadaUtmEntity.getTipo());
            processStored.setParameter("idUsuarioElimina", coordenadaUtmEntity.getIdUsuarioElimina());
            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return  result;
    }

    @Override
    public ResultClassEntity ListarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_CoordenadaUtm_Listar");
            sp.registerStoredProcedureParameter("idUnidadManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipo", Integer.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idUnidadManejo", coordenadaUtmEntity.getIdUnidadManejo());
            sp.setParameter("idTipo", coordenadaUtmEntity.getTipo());
            sp.execute();
            List<CoordenadaUtmEntity> resultDet = new ArrayList<>();
            List<Object[]> spResultDet = sp.getResultList();
            if (!spResultDet.isEmpty()){
                CoordenadaUtmEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new CoordenadaUtmEntity();
                    temp.setIdCoordUtm((Integer) item[0]);
                    temp.setIdUnidadManejo((Integer) item[1]);
                    temp.setAnexoSector((String) item[2]);
                    temp.setReferencia((String) item[3]);
                    temp.setVertice((Integer) item[4]);
                    temp.setTipo((Integer) item[5]);;
                    BigDecimal bd=(BigDecimal) item[6];
                    BigDecimal bd_=(BigDecimal)item[7];
                    temp.setEste((Double) bd.doubleValue());
                    temp.setNorte((Double) bd_.doubleValue());
                    resultDet.add(temp);
                }
            }

            result.setData(resultDet);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }


    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }
}
