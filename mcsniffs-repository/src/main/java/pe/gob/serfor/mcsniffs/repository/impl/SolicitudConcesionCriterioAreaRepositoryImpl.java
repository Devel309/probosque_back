package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionCriterioAreaEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionCriterioAreaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudConcesionCriterioAreaRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionCriterioAreaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudConcesionCriterioArea (SolicitudConcesionCriterioAreaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionCriterioAreaEntity> lista = new ArrayList<>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionCriterioArea_ListarFiltro");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionCriterioArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCriterio", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionCriterioArea",obj.getIdSolicitudConcesionCriterioArea());
            processStored.setParameter("idSolicitudConcesionArea",obj.getIdSolicitudConcesionArea());
            processStored.setParameter("idSolicitudConcesion",obj.getIdSolicitudConcesion());
            processStored.setParameter("codigoCriterio", obj.getCodigoCriterio());
            processStored.setParameter("nombreCriterio", obj.getNombreCriterio());


            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudConcesionCriterioAreaEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionCriterioAreaEntity();
                        c.setIdSolicitudConcesionCriterioArea(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolicitudConcesionArea(row[1]==null?null:(Integer) row[1]);
                        c.setIdSolicitudConcesion(row[2]==null?null:(Integer) row[2]);
                        c.setCodigoCriterio(row[3]==null?null:(String) row[3]);
                        c.setNombreCriterio(row[4]==null?null:(String) row[4]);
                        c.setDescripcionOtroCriterio(row[5]==null?null:(String) row[5]);
                        c.setEstadoCriterio(row[6]==null?false:(Boolean) row[6]);
                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de criterios de área de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarCriterioAreaSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de criterios de área de la solicitud de concesión.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionCriterioArea (SolicitudConcesionCriterioAreaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionCriterioArea_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionCriterioArea", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionOtroCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoCriterio", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionArea", obj.getIdSolicitudConcesionArea());
            processStored.setParameter("codigoCriterio", obj.getCodigoCriterio());
            processStored.setParameter("nombreCriterio", obj.getNombreCriterio());
            processStored.setParameter("descripcionOtroCriterio", obj.getDescripcionOtroCriterio());
            processStored.setParameter("estadoCriterio", obj.getEstadoCriterio());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolicitudConcesionCriterioArea = (Integer) processStored.getOutputParameterValue("idSolicitudConcesionCriterioArea");
            obj.setIdSolicitudConcesionCriterioArea(idSolicitudConcesionCriterioArea);
            result.setCodigo(idSolicitudConcesionCriterioArea);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el criterio de área de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el criterio de área de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionCriterioArea (SolicitudConcesionCriterioAreaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionCriterioArea_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionCriterioArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionOtroCriterio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoCriterio", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionCriterioArea", obj.getIdSolicitudConcesionCriterioArea());
            processStored.setParameter("idSolicitudConcesionArea", obj.getIdSolicitudConcesionArea());
            processStored.setParameter("codigoCriterio", obj.getCodigoCriterio());
            processStored.setParameter("nombreCriterio", obj.getNombreCriterio());
            processStored.setParameter("descripcionOtroCriterio", obj.getDescripcionOtroCriterio());
            processStored.setParameter("estadoCriterio", obj.getEstadoCriterio());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el criterio de área de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el criterio de área de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionCriterioArea (SolicitudConcesionCriterioAreaEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionCriterioArea_Eliminar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionCriterioArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionCriterioArea", obj.getIdSolicitudConcesionCriterioArea());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el criterio de área de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo eliminar el criterio de área de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }
}
