package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.apache.commons.io.FilenameUtils;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;
import pe.gob.serfor.mcsniffs.repository.PotencialProduccionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;

@Repository
public class PotencialProduccionForestalRepositoryImpl extends JdbcDaoSupport
        implements PotencialProduccionForestalRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(PotencialProduccionForestalRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";

    @Autowired
    private FileServerConexion fileServerConexion;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ArchivoRepositoryImpl respositoryArchivo;

    @Autowired
    private PGMFArchivoRepositoryImpl repositoryPGMFArchivo;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registrar(PotencialProduccionForestalEntity item, MultipartFile file) {
        ResultClassEntity result = new ResultClassEntity();
        ResultEntity<ArchivoEntity> archivo = null;
        PGMFArchivoEntity archivoDetalle = new PGMFArchivoEntity();
        try {
            if (file != null) {
                ArchivoEntity request = new ArchivoEntity();
                String archivoGenerado = fileServerConexion.uploadFile(file);
                request.setIdArchivo(0);
                request.setEstado(item.getEstado());
                request.setIdUsuarioModificacion(item.getIdUsuarioModificacion());
                request.setIdUsuarioRegistro(item.getIdUsuarioRegistro());
                request.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
                request.setNombre(FilenameUtils.getBaseName(file.getOriginalFilename()));
                request.setExtension(FilenameUtils.getExtension(archivoGenerado));
                request.setRuta(fileServerPath.concat(archivoGenerado));
                request.setTipoDocumento("1");
                archivo = this.respositoryArchivo.registroArchivo(request);
                archivoDetalle.setCodigoTipoPGMF(item.getCodigoTipoPotProdForestal());
                archivoDetalle.setIdPlanManejo(item.getPlanManejo().getIdPlanManejo());
                archivoDetalle.setIdUsuarioRegistro(item.getIdUsuarioRegistro());
                archivoDetalle.setIdArchivo(archivo == null ? 0 : archivo.getCodigo());
                this.repositoryPGMFArchivo.registrarArchivo(archivoDetalle);
            }

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PotencialProduccionForestal_upsert");
            processStored.registerStoredProcedureParameter("idPotProdForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoPotProdForestal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("variable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("disenio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diametroMinimoInventariadaCM", Double.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tamanioParcela", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroParcela", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("distanciaParcela", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalAreaInventariada", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("rangoDiametroCM", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaMuestreada", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("metodoMuestreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("intensidadMuestreoPorcentaje", Double.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("errorMuestreoPorcentaje", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPotProdForestal", item.getIdPotProdForestal());
            processStored.setParameter("idPlanManejo", item.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("codigoTipoPotProdForestal", item.getCodigoTipoPotProdForestal());
            processStored.setParameter("especie", item.getEspecie());
            processStored.setParameter("variable", item.getVariable());
            processStored.setParameter("anexo", item.getAnexo());
            processStored.setParameter("disenio", item.getDisenio());
            processStored.setParameter("diametroMinimoInventariadaCM", item.getDiametroMinimoInventariadaCM());
            processStored.setParameter("tamanioParcela", item.getTamanioParcela());
            processStored.setParameter("nroParcela", item.getNroParcela());
            processStored.setParameter("distanciaParcela", item.getDistanciaParcela());
            processStored.setParameter("totalAreaInventariada", item.getTotalAreaInventariada());
            processStored.setParameter("rangoDiametroCM", item.getRangoDiametroCM());
            processStored.setParameter("areaMuestreada", item.getAreaMuestreada());
            processStored.setParameter("metodoMuestreo", item.getMetodoMuestreo());
            processStored.setParameter("intensidadMuestreoPorcentaje", item.getIntensidadMuestreoPorcentaje());
            processStored.setParameter("errorMuestreoPorcentaje", item.getErrorMuestreoPorcentaje());
            processStored.setParameter("idUsuario", item.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return result;
        }
    }


    @Override
    public ResultClassEntity listar(Integer idPlanManejo) {
        ResultClassEntity result = new ResultClassEntity();
        List<PotencialProduccionForestalEntity> list = new ArrayList<PotencialProduccionForestalEntity>();
        try {
            StoredProcedureQuery ps = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccionforestal_Listar");
            ps.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            ps.setParameter("idPlanManejo", idPlanManejo);
            ps.execute();
            List<Object[]> spResult = ps.getResultList();
            for (Object[] row : spResult) {
                PlanManejoEntity pm = new PlanManejoEntity();
                PotencialProduccionForestalEntity ppf = new PotencialProduccionForestalEntity();
                ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
                pm.setIdPlanManejo((Integer) row[0]);
                ppf.setPlanManejo(pm);
                ppf.setIdPotProdForestal((Integer) row[1]);
                ppf.setCodigoTipoPotProdForestal((String) row[2]);
                ppf.setEspecie((String) row[3]);
                ppf.setVariable((String) row[4]);
                ppf.setAnexo((String) row[5]);
                ppf.setDisenio((String) row[6]);
                BigDecimal diametro = (BigDecimal) row[7];
                BigDecimal tamanio = (BigDecimal) row[8];
                ppf.setDiametroMinimoInventariadaCM((Double) diametro.doubleValue());
                ppf.setTamanioParcela((Double) tamanio.doubleValue());
                ppf.setNroParcela((Integer) row[9]);
                BigDecimal distancia = (BigDecimal) row[10];
                BigDecimal totalArea = (BigDecimal) row[11];
                BigDecimal rango = (BigDecimal) row[12];
                BigDecimal areaMuestra = (BigDecimal) row[13];
                ppf.setDistanciaParcela((Double) distancia.doubleValue());
                ppf.setTotalAreaInventariada((Double) totalArea.doubleValue());
                ppf.setRangoDiametroCM((Double) rango.doubleValue());
                ppf.setAreaMuestreada((Double) areaMuestra.doubleValue());
                ppf.setMetodoMuestreo((String) row[14]);
                BigDecimal intensidad = (BigDecimal) row[15];
                BigDecimal error = (BigDecimal) row[16];
                ppf.setIntensidadMuestreoPorcentaje((Double) intensidad.doubleValue());
                ppf.setErrorMuestreoPorcentaje((Double) error.doubleValue());
                resultArchivo.setNombeArchivoGenerado((String) row[17]);
                resultArchivo.setCodigo((Integer) row[10]);
                if ((String) row[17] != null) {
                    String nombreArchivoGenerado = ((String) row[17]).concat(SEPARADOR_ARCHIVO).concat((String) row[18]);
                    String nombreArchivo = ((String) row[19]).concat(SEPARADOR_ARCHIVO).concat((String) row[18]);
                    byte[] byteFile = fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                    resultArchivo.setArchivo(byteFile);
                    resultArchivo.setNombeArchivo(nombreArchivo);
                    resultArchivo.setContenTypeArchivo("application/octet-stream");
                }
                ppf.setArchivo(resultArchivo);
                list.add(ppf);
            }
            result.setSuccess(true);
            result.setMessage("Se listó correctamente.");
            result.setData(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
        }
        return result;
    }

    @Override
    public List<PotencialProduccionForestalEntity> ListarPotencialProducForestalTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,Integer idPlanManejo,String codigoProceso) throws Exception {
        List<PotencialProduccionForestalEntity> listaCab = new ArrayList<PotencialProduccionForestalEntity>();


        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccion_Listar_Titular");
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumento", tipoDocumento);
            processStored.setParameter("nroDocumento", nroDocumento);
            processStored.setParameter("codigoProcesoTitular", codigoProcesoTitular);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigoProceso", codigoProceso);
            processStored.execute();
            int idPotProdForestal=0;
            int idPotProdForestalDet=0;
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        PotencialProduccionForestalEntity temp = new PotencialProduccionForestalEntity();
                        temp.setIdPotProdForestal((Integer) row[0]);
                        idPotProdForestal=(Integer) row[0];
                        temp.setCodigoTipoPotProdForestal((String) row[1]);
                        temp.setIdPlanManejo((Integer) row[2]);
                        temp.setIdTipoBosque((Integer) row[3]);
                        temp.setTipoBosque((String) row[4]);
                        temp.setCodigoSubTipoPotencialProdForestal((String) row[5]);

                        temp.setErrorMuestreo(((BigDecimal) row[17]));


                        temp.setDisenio(row[6] == null ? null : (String) row[6]);
                        temp.setDiametroMinimoInventariadaCM(row[7] == null ? null : ((BigDecimal) row[7]).doubleValue());
                        temp.setIntensidadMuestreoPorcentaje(row[8] == null ? null : ((BigDecimal) row[8]).doubleValue());
                        temp.setTamanioParcela(row[9] == null ? null : ((BigDecimal) row[9]).doubleValue());
                        temp.setDistanciaParcela(row[10] == null ? null : ((BigDecimal) row[10]).doubleValue());
                        temp.setNroParcela(row[11] == null ? null : (Integer) row[11]);
                        temp.setTotalAreaInventariada(row[12] == null ? null : ((BigDecimal) row[12]).doubleValue());
                        temp.setMetodoMuestreo(row[13] == null ? null : (String) row[13]);
                        temp.setRangoDiametroCM(row[14] == null ? null : ((BigDecimal) row[14]).doubleValue());
                        temp.setAreaMuestreada(row[15] == null ? null : ((BigDecimal) row[15]).doubleValue());
                        temp.setAnexo(row[16] == null ? null : String.valueOf((Character) row[16]));


                        /***************************************** INICIO DETALLE ********************************************************/
                        List<PotencialProduccionForestalDto> listaDet = new ArrayList<PotencialProduccionForestalDto>();
                        StoredProcedureQuery processStoredDetalle = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccionDetalle_Listar");
                        processStoredDetalle.registerStoredProcedureParameter("idPotProdForestal", Integer.class, ParameterMode.IN);
                        processStoredDetalle.setParameter("idPotProdForestal", idPotProdForestal);
                        processStoredDetalle.execute();
                        List<Object[]> spResultDetalle = processStoredDetalle.getResultList();
                        if(spResultDetalle!=null){

                            if (spResultDetalle.size() >= 1){
                                for (Object[] rowDetalle : spResultDetalle) {

                                    PotencialProduccionForestalDto tempDetalle = new PotencialProduccionForestalDto();

                                    tempDetalle.setIdPotProdForestalDet((Integer) rowDetalle[0]);
                                    idPotProdForestalDet=(Integer) rowDetalle[0];
                                    tempDetalle.setCodigoTipoPotencialProdForestal((String) rowDetalle[1]);
                                    tempDetalle.setCodigoSubTipoPotencialProdForestalDet((String) rowDetalle[2]);
                                    tempDetalle.setIdEspecie((Integer) rowDetalle[3]);
                                    tempDetalle.setEspecie((String) rowDetalle[4]);
                                    tempDetalle.setGrupoComercial(rowDetalle[5] == null ? null : (String) rowDetalle[5]);
                                    tempDetalle.setTotalHa(rowDetalle[6] == null ? null : (BigDecimal) rowDetalle[6]);
                                    tempDetalle.setPorcentaje(rowDetalle[7] == null ? null : (BigDecimal) rowDetalle[7]);
                                    /***************************************** INICIO VARIABLE ********************************************************/
                                    List<PotencialProduccionForestalVariableEntity> listaVa = new ArrayList<PotencialProduccionForestalVariableEntity>();
                                    StoredProcedureQuery processStoredVa = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccionVariable_Listar");
                                    processStoredVa.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class, ParameterMode.IN);
                                    processStoredVa.setParameter("idPotProdForestalDet", idPotProdForestalDet);
                                    processStoredVa.execute();
                                    List<Object[]> spResultVa = processStoredVa.getResultList();
                                    if(spResultVa!=null){

                                        if (spResultVa.size() >= 1){
                                            for (Object[] rowVa : spResultVa) {

                                                PotencialProduccionForestalVariableEntity tempVa = new PotencialProduccionForestalVariableEntity();

                                                tempVa.setIdVariable((Integer) rowVa[0]);
                                                tempVa.setVariable((String) rowVa[1]);
                                                tempVa.setNuTotalTipoBosque((BigDecimal) rowVa[2]);
                                                tempVa.setTotalHa((BigDecimal) rowVa[3]);
                                                tempVa.setAccion((Boolean) rowVa[4]);
                                                tempVa.setDap((BigDecimal) rowVa[5]);
                                                tempVa.setSubdap((BigDecimal) rowVa[6]);
                                                listaVa.add(tempVa);
                                            }
                                        }
                                    }
                                    /***************************************** FIN DETALLE ********************************************************/
                                    tempDetalle.setListPotencialProduccionVariable(listaVa);
                                    listaDet.add(tempDetalle);
                                }
                            }
                        }
                        /***************************************** FIN DETALLE ********************************************************/
                        temp.setListPotencialProduccion(listaDet);
                        listaCab.add(temp);
                    }
                }
            }
            return listaCab;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PotencialProduccionForestalEntity> ListarPotencialProducForestal(Integer idPlanManejo,String codigo) throws Exception {
        List<PotencialProduccionForestalEntity> listaCab = new ArrayList<PotencialProduccionForestalEntity>();


        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccion_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoPotencialProdForestal", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigoTipoPotencialProdForestal", codigo);
            processStored.execute();
            int idPotProdForestal=0;
            int idPotProdForestalDet=0;
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        PotencialProduccionForestalEntity temp = new PotencialProduccionForestalEntity();
                        temp.setIdPotProdForestal((Integer) row[0]);
                        idPotProdForestal=(Integer) row[0];
                        temp.setCodigoTipoPotProdForestal((String) row[1]);
                        temp.setIdPlanManejo((Integer) row[2]);
                        temp.setIdTipoBosque((Integer) row[3]);
                        temp.setTipoBosque((String) row[4]);
                        temp.setCodigoSubTipoPotencialProdForestal((String) row[5]);

                        temp.setErrorMuestreo(((BigDecimal) row[17]));


                        temp.setDisenio(row[6] == null ? null : (String) row[6]);
                        temp.setDiametroMinimoInventariadaCM(row[7] == null ? null : ((BigDecimal) row[7]).doubleValue());
                        temp.setIntensidadMuestreoPorcentaje(row[8] == null ? null : ((BigDecimal) row[8]).doubleValue());
                        temp.setTamanioParcela(row[9] == null ? null : ((BigDecimal) row[9]).doubleValue());
                        temp.setDistanciaParcela(row[10] == null ? null : ((BigDecimal) row[10]).doubleValue());
                        temp.setNroParcela(row[11] == null ? null : (Integer) row[11]);
                        temp.setTotalAreaInventariada(row[12] == null ? null : ((BigDecimal) row[12]).doubleValue());
                        temp.setMetodoMuestreo(row[13] == null ? null : (String) row[13]);
                        temp.setRangoDiametroCM(row[14] == null ? null : ((BigDecimal) row[14]).doubleValue());
                        temp.setAreaMuestreada(row[15] == null ? null : ((BigDecimal) row[15]).doubleValue());
                        temp.setAnexo(row[16] == null ? null : String.valueOf((Character) row[16]));


                        /***************************************** INICIO DETALLE ********************************************************/
                        List<PotencialProduccionForestalDto> listaDet = new ArrayList<PotencialProduccionForestalDto>();
                        StoredProcedureQuery processStoredDetalle = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccionDetalle_Listar");
                        processStoredDetalle.registerStoredProcedureParameter("idPotProdForestal", Integer.class, ParameterMode.IN);
                        processStoredDetalle.setParameter("idPotProdForestal", idPotProdForestal);
                        processStoredDetalle.execute();
                        List<Object[]> spResultDetalle = processStoredDetalle.getResultList();
                        if(spResultDetalle!=null){

                            if (spResultDetalle.size() >= 1){
                                for (Object[] rowDetalle : spResultDetalle) {

                                    PotencialProduccionForestalDto tempDetalle = new PotencialProduccionForestalDto();

                                    tempDetalle.setIdPotProdForestalDet((Integer) rowDetalle[0]);
                                    idPotProdForestalDet=(Integer) rowDetalle[0];
                                    tempDetalle.setCodigoTipoPotencialProdForestal((String) rowDetalle[1]);
                                    tempDetalle.setCodigoSubTipoPotencialProdForestalDet((String) rowDetalle[2]);
                                    tempDetalle.setIdEspecie((Integer) rowDetalle[3]);
                                    tempDetalle.setEspecie((String) rowDetalle[4]);
                                    tempDetalle.setGrupoComercial(rowDetalle[5] == null ? null : (String) rowDetalle[5]);
                                    tempDetalle.setTotalHa(rowDetalle[6] == null ? null : (BigDecimal) rowDetalle[6]);
                                    tempDetalle.setPorcentaje(rowDetalle[7] == null ? null : (BigDecimal) rowDetalle[7]);
                                    /***************************************** INICIO VARIABLE ********************************************************/
                                    List<PotencialProduccionForestalVariableEntity> listaVa = new ArrayList<PotencialProduccionForestalVariableEntity>();
                                    StoredProcedureQuery processStoredVa = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccionVariable_Listar");
                                    processStoredVa.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class, ParameterMode.IN);
                                    processStoredVa.setParameter("idPotProdForestalDet", idPotProdForestalDet);
                                    processStoredVa.execute();
                                    List<Object[]> spResultVa = processStoredVa.getResultList();
                                    if(spResultVa!=null){

                                        if (spResultVa.size() >= 1){
                                            for (Object[] rowVa : spResultVa) {

                                                PotencialProduccionForestalVariableEntity tempVa = new PotencialProduccionForestalVariableEntity();

                                                tempVa.setIdVariable((Integer) rowVa[0]);
                                                tempVa.setVariable((String) rowVa[1]);
                                                tempVa.setNuTotalTipoBosque((BigDecimal) rowVa[2]);
                                                tempVa.setTotalHa((BigDecimal) rowVa[3]);
                                                tempVa.setAccion((Boolean) rowVa[4]);
                                                tempVa.setDap((BigDecimal) rowVa[5]);
                                                tempVa.setSubdap((BigDecimal) rowVa[6]);
                                                listaVa.add(tempVa);
                                            }
                                        }
                                    }
                                    /***************************************** FIN DETALLE ********************************************************/
                                    tempDetalle.setListPotencialProduccionVariable(listaVa);
                                    listaDet.add(tempDetalle);
                                }
                            }
                        }
                        /***************************************** FIN DETALLE ********************************************************/
                        temp.setListPotencialProduccion(listaDet);
                        listaCab.add(temp);
                    }
                }
            }
            return listaCab;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity RegistrarPotencialProducForestal(List<PotencialProduccionForestalEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PotencialProduccionForestalEntity> list_ = new ArrayList<>();
        try {
            for (PotencialProduccionForestalEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccion_Registrar");
                sp.registerStoredProcedureParameter("idPotProdForestal", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoPotencialProdForestal", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoBosque", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoSubTipoPotencialProdForestal", String.class, ParameterMode.IN);

                sp.registerStoredProcedureParameter("errorMuestreo", BigDecimal.class, ParameterMode.IN);

                sp.registerStoredProcedureParameter("disenio", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("diametroMinimoInventariadaCM", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("intensidadMuestreoPorcentaje", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tamanioParcela", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("distanciaParcela", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroParcela", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("totalAreaInventariada", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("metodoMuestreo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("rangoDiametroCM", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("areaMuestreada", Double.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);


                SpUtil.enableNullParams(sp);
                sp.setParameter("idPotProdForestal", p.getIdPotProdForestal());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codigoTipoPotencialProdForestal", p.getCodigoTipoPotProdForestal());
                sp.setParameter("idTipoBosque", p.getIdTipoBosque());
                sp.setParameter("tipoBosque", p.getTipoBosque());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("codigoSubTipoPotencialProdForestal", p.getCodigoSubTipoPotencialProdForestal());

                sp.setParameter("errorMuestreo", p.getErrorMuestreo());

                sp.setParameter("disenio", p.getDisenio());
                sp.setParameter("diametroMinimoInventariadaCM", p.getDiametroMinimoInventariadaCM());
                sp.setParameter("intensidadMuestreoPorcentaje", p.getIntensidadMuestreoPorcentaje());
                sp.setParameter("tamanioParcela", p.getTamanioParcela());
                sp.setParameter("distanciaParcela", p.getDistanciaParcela());
                sp.setParameter("nroParcela", p.getNroParcela());
                sp.setParameter("totalAreaInventariada", p.getTotalAreaInventariada());
                sp.setParameter("metodoMuestreo", p.getMetodoMuestreo());
                sp.setParameter("rangoDiametroCM", p.getRangoDiametroCM());
                sp.setParameter("areaMuestreada", p.getAreaMuestreada());
                sp.setParameter("anexo", p.getAnexo());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        PotencialProduccionForestalEntity obj_ = new PotencialProduccionForestalEntity();
                        obj_.setIdPotProdForestal((Integer) row_[0]);
                        obj_.setCodigoTipoPotProdForestal((String) row_[1]);
                        obj_.setIdPlanManejo((Integer) row_[2]);
                        obj_.setIdTipoBosque((Integer) row_[3]);
                        obj_.setTipoBosque((String) row_[4]);

                        /****************************************** DETALLE ********************************************************************/
                        List<PotencialProduccionForestalDto> listDet = new ArrayList<>();
                        for (PotencialProduccionForestalDto param : p.getListPotencialProduccion()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_PotencialProduccionDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idPotProdForestal", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoTipoPotencialProdForestalDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idEspecie", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoSubTipoPotencialProdForestalDet", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("grupoComercial", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("totalHa", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("porcentaje", BigDecimal.class, ParameterMode.IN);

                            SpUtil.enableNullParams(pa);

                            pa.setParameter("idPotProdForestalDet", param.getIdPotProdForestalDet());
                            pa.setParameter("idPotProdForestal", obj_.getIdPotProdForestal());
                            pa.setParameter("codigoTipoPotencialProdForestalDet", param.getCodigoTipoPotencialProdForestalDet());
                            pa.setParameter("idEspecie", param.getIdEspecie());
                            pa.setParameter("especie", param.getEspecie());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.setParameter("codigoSubTipoPotencialProdForestalDet", param.getCodigoSubTipoPotencialProdForestalDet());
                            pa.setParameter("grupoComercial", param.getGrupoComercial());
                            pa.setParameter("totalHa", param.getTotalHa());
                            pa.setParameter("porcentaje", param.getPorcentaje());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                PotencialProduccionForestalDto obj = null;
                                for (Object[] row : spResult) {
                                    obj = new PotencialProduccionForestalDto();

                                    obj.setIdPotProdForestalDet((Integer) row[0]);
                                    obj.setIdPotProdForestal((Integer) row[1]);
                                    obj.setEspecie((String) row[2]);
                                    obj.setCodigoTipoPotencialProdForestalDet((String) row[3]);
                                    obj.setIdEspecie((Integer) row[4]);
                                    /***************************************** INICIO VARIABLE ********************************************************/

                                    List<PotencialProduccionForestalVariableEntity> listVariable = new ArrayList<>();
                                    for (PotencialProduccionForestalVariableEntity paramVa : param.getListPotencialProduccionVariable()) {
                                        StoredProcedureQuery paVa = entityManager
                                                .createStoredProcedureQuery("dbo.pa_PotencialProduccionVariable_Registrar");
                                        paVa.registerStoredProcedureParameter("idVariable", Integer.class,ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class,ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("variable", String.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("nuTotalTipoBosque", BigDecimal.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("totalHa", BigDecimal.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("dap", BigDecimal.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("subdap", BigDecimal.class, ParameterMode.IN);
                                        SpUtil.enableNullParams(paVa);
                                        paVa.setParameter("idVariable", paramVa.getIdVariable());
                                        paVa.setParameter("idPotProdForestalDet", obj.getIdPotProdForestalDet());
                                        paVa.setParameter("variable", paramVa.getVariable());
                                        paVa.setParameter("nuTotalTipoBosque", paramVa.getNuTotalTipoBosque());
                                        paVa.setParameter("totalHa", paramVa.getTotalHa());
                                        paVa.setParameter("accion", paramVa.getAccion());
                                        paVa.setParameter("idUsuarioRegistro", paramVa.getIdUsuarioRegistro());
                                        paVa.setParameter("dap", paramVa.getDap());
                                        paVa.setParameter("subdap", paramVa.getSubdap());
                                        paVa.execute();
                                        System.out.println(paVa.getResultList());
                                        List<Object[]> spResultVa = paVa.getResultList();
                                        if (!spResultVa.isEmpty()) {
                                            PotencialProduccionForestalVariableEntity objVa = null;
                                            for (Object[] rowVa : spResultVa) {
                                                objVa = new PotencialProduccionForestalVariableEntity();

                                                objVa.setIdVariable((Integer) rowVa[0]);
                                                objVa.setVariable((String) rowVa[1]);
                                                objVa.setAccion((Boolean) rowVa[2]);
                                                objVa.setNuTotalTipoBosque((BigDecimal) rowVa[3]);
                                                objVa.setTotalHa((BigDecimal) rowVa[4]);
                                                listVariable.add(objVa);
                                            }
                                        }
                                    }
                                    obj.setListPotencialProduccionVariable(listVariable);

                                    /***************************************** FIN VARIABLE ********************************************************/


                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListPotencialProduccion(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el Manejo del Bosque.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /*
    @Override
    public ResultClassEntity RegistrarPotencialProducForestal(List<PotencialProduccionForestalEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PotencialProduccionForestalEntity> list_ = new ArrayList<>();
        System.out.println("ACA 1");
        try {
            System.out.println("ACA 2");
            for (PotencialProduccionForestalEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccion_Registrar");
                sp.registerStoredProcedureParameter("idPotProdForestal", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoPotencialProdForestal", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoBosque", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoSubTipoPotencialProdForestal", String.class, ParameterMode.IN);
                //SpUtil.enableNullParams(sp);
                setStoreProcedureEnableNullParameters(sp);
                sp.setParameter("idPotProdForestal", p.getIdPotProdForestal());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codigoTipoPotencialProdForestal", p.getCodigoTipoPotProdForestal());
                sp.setParameter("idTipoBosque", p.getIdTipoBosque());
                sp.setParameter("tipoBosque", p.getTipoBosque());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("codigoSubTipoPotencialProdForestal", p.getCodigoSubTipoPotencialProdForestal());
                sp.execute();
                System.out.println(sp.getResultList());
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        PotencialProduccionForestalEntity obj_ = new PotencialProduccionForestalEntity();
                        obj_.setIdPotProdForestal((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setIdTipoBosque((Integer) row_[2]);
                        obj_.setTipoBosque((String) row_[3]);
                        obj_.setCodigoTipoPotProdForestal((String) row_[4]);

                        /***************************************** INICIO DETALLE ********************************************************/
/*
                        List<PotencialProduccionForestalDto> listDet = new ArrayList<>();
                        for (PotencialProduccionForestalDto param : p.getListPotencialProduccion()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_PotencialProduccionDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idPotProdForestal", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoTipoPotencialProdForestalDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idEspecie", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoSubTipoPotencialProdForestalDet", String.class, ParameterMode.IN);
                            SpUtil.enableNullParams(pa);
                            pa.setParameter("idPotProdForestalDet", param.getIdPotProdForestalDet());
                            pa.setParameter("idPotProdForestal", obj_.getIdPotProdForestal());
                            pa.setParameter("codigoTipoPotencialProdForestalDet", param.getCodigoTipoPotencialProdForestalDet());
                            pa.setParameter("idEspecie", param.getIdEspecie());
                            pa.setParameter("especie", param.getEspecie());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.setParameter("codigoSubTipoPotencialProdForestalDet", param.getCodigoSubTipoPotencialProdForestalDet());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            System.out.println(pa.getResultList());
                            if (!spResult.isEmpty()) {
                                PotencialProduccionForestalDto obj = null;
                                for (Object[] row : spResult) {
                                    obj = new PotencialProduccionForestalDto();
                                    obj.setIdPotProdForestalDet((Integer) row[0]);
                                    obj.setIdPotProdForestal((Integer) row[1]);
                                    obj.setEspecie((String) row[2]);
                                    obj.setCodigoTipoPotencialProdForestalDet((String) row[3]);
                                    obj.setIdEspecie((Integer) row[4]);


                                    /***************************************** INICIO VARIABLE ********************************************************/
/*
                                    List<PotencialProduccionForestalVariableEntity> listVariable = new ArrayList<>();
                                    for (PotencialProduccionForestalVariableEntity paramVa : param.getListPotencialProduccionVariable()) {
                                        StoredProcedureQuery paVa = entityManager
                                                .createStoredProcedureQuery("dbo.pa_PotencialProduccionVariable_Registrar");
                                        paVa.registerStoredProcedureParameter("idVariable", Integer.class,ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class,ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("variable", String.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("nuTotalTipoBosque", BigDecimal.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("totalHa", BigDecimal.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                                        paVa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                                        SpUtil.enableNullParams(paVa);
                                        paVa.setParameter("idVariable", paramVa.getIdVariable());
                                        paVa.setParameter("idPotProdForestalDet", obj.getIdPotProdForestalDet());
                                        paVa.setParameter("variable", paramVa.getVariable());
                                        paVa.setParameter("nuTotalTipoBosque", paramVa.getNuTotalTipoBosque());
                                        paVa.setParameter("totalHa", paramVa.getTotalHa());
                                        paVa.setParameter("accion", paramVa.getAccion());
                                        paVa.setParameter("idUsuarioRegistro", paramVa.getIdUsuarioRegistro());
                                        paVa.execute();
                                        System.out.println(paVa.getResultList());
                                        List<Object[]> spResultVa = paVa.getResultList();
                                        if (!spResultVa.isEmpty()) {
                                            PotencialProduccionForestalVariableEntity objVa = null;
                                            for (Object[] rowVa : spResultVa) {
                                                objVa = new PotencialProduccionForestalVariableEntity();

                                                objVa.setIdVariable((Integer) rowVa[0]);
                                                objVa.setVariable((String) rowVa[1]);
                                                objVa.setAccion((Boolean) rowVa[2]);
                                                objVa.setNuTotalTipoBosque((BigDecimal) rowVa[3]);
                                                objVa.setTotalHa((BigDecimal) rowVa[4]);
                                                listVariable.add(objVa);
                                            }
                                        }
                                    }
                                    obj.setListPotencialProduccionVariable(listVariable);
*/
                                    /***************************************** FIN VARIABLE ********************************************************/
/*
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListPotencialProduccion(listDet);*/
                        /***************************************** FIN DETALLE ********************************************************/

                   /*     list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el potencial de produccion.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }*/


    @Override
    public ResultClassEntity EliminarPotencialProducForestal(PotencialProduccionForestalDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_PotencialProduccion_Eliminar]");
        processStored.registerStoredProcedureParameter("idPotProdForestal", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPotProdForestalDet", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);


        processStored.setParameter("idPotProdForestal", param.getIdPotProdForestal());
        processStored.setParameter("idPotProdForestalDet", param.getIdPotProdForestalDet());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }
    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

}
