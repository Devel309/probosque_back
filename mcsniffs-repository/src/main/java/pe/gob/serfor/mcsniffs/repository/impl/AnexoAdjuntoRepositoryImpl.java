package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.AnexoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AnexoAdjuntoRepository;
import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AnexoAdjuntoRepositoryImpl extends JdbcDaoSupport implements AnexoAdjuntoRepository {


    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.AnexoAdjuntoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Julio Meza [14-08-2021]
     * @modificado:
     * @descripción: {Marca el tab/sección para Ampliación en Anexo Adjunto}
     * @param:AnexoAdjunto
     */
    @Override
    public ResultClassEntity MarcarParaAmpliacionAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AnexoAdjunto_MarcarParaAmpliacion");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTab", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjuntaAnexo", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistra", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", anexoAdjuntoEntity.getIdPlanManejo());
            processStored.setParameter("codigoTab", anexoAdjuntoEntity.getCodigoTab());
            processStored.setParameter("adjuntaAnexo", anexoAdjuntoEntity.getAdjuntaAnexo());
            processStored.setParameter("idUsuarioRegistra", anexoAdjuntoEntity.getIdUsuarioRegistro());
            processStored.execute();
            result.setData(anexoAdjuntoEntity);
            result.setSuccess(true);
            result.setMessage("Se registró la marca para ampliación con éxito.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public List<AnexoAdjuntoEntity> ListarAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AnexoAdjunto_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", anexoAdjuntoEntity.getIdPlanManejo());
            processStored.execute();
            List<AnexoAdjuntoEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    AnexoAdjuntoEntity temp = new AnexoAdjuntoEntity();
                    temp.setIdAnexoAdjunto((Integer) row[0]);
                    temp.setIdAnexo((Integer) row[1]);
                    temp.setCodigoTab((String) row[2]);
                    temp.setAdjuntaAnexo((Boolean) row[3]);
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

}
