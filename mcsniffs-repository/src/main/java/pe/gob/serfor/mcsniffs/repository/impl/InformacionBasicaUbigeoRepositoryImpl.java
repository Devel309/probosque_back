package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaUbigeoRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

/**
 * @autor: Carlos Tang [03-09-2021]
 * @modificado:
 * @descripción: {Repositorio Informacion Basica}
 *
 */
@Repository
public class InformacionBasicaUbigeoRepositoryImpl extends JdbcDaoSupport implements InformacionBasicaUbigeoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final String SEPARADOR_ARCHIVO = ".";
    private static final Logger log = LogManager.getLogger(InformacionBasicaRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;
    @Autowired
    private FileServerConexion fileServerConexion;

    @Autowired
    private ArchivoRepositoryImpl respositoryArchivo;
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarInformacionBasicaDistrito(InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaUbigeo_Registrar");
            processStored.registerStoredProcedureParameter("idInfBasicaUbi", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUbigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("principal", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoDepartamento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProvincia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoDistrito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);

            setStoreProcedureEnableNullParameters(processStored);            

            processStored.setParameter("idInfBasicaUbi", informacionBasicaUbigeoEntity.getIdInfBasicaUbigeo());
            processStored.setParameter("codigoUbigeo", informacionBasicaUbigeoEntity.getCodigoUbigeo());
            processStored.setParameter("principal", informacionBasicaUbigeoEntity.isPrincipal());
            processStored.setParameter("idInfBasica", informacionBasicaUbigeoEntity.getIdInfBasica());
            processStored.setParameter("idUsuarioRegistro", informacionBasicaUbigeoEntity.getIdUsuarioRegistro());
            processStored.setParameter("codigoDepartamento", informacionBasicaUbigeoEntity.getCodigoDepartamento());
            processStored.setParameter("codigoProvincia", informacionBasicaUbigeoEntity.getCodigoProvincia());
            processStored.setParameter("codigoDistrito", informacionBasicaUbigeoEntity.getCodigoDistrito());
            processStored.setParameter("cuenca", informacionBasicaUbigeoEntity.getCuenca());
 
            processStored.execute();
            
            result.setSuccess(true);
            result.setMessage("Se registró la información básica distrito.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<InformacionBasicaUbigeoEntity> ListarInformacionBasicaDistrito(InformacionBasicaEntity obj) throws Exception {
        
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaUbigeo_Listar");
            sp.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codSubInfBasica", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            setStoreProcedureEnableNullParameters(sp);
            
            sp.setParameter("idInfBasica", obj.getIdInfBasica());
            sp.setParameter("codInfBasica", obj.getCodInfBasica());
            sp.setParameter("codSubInfBasica", obj.getCodSubInfBasica());   
            sp.setParameter("idPlanManejo", obj.getIdPlanManejo());    
            sp.execute();
            List<InformacionBasicaUbigeoEntity> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();

            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                InformacionBasicaUbigeoEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new InformacionBasicaUbigeoEntity();
                    temp.setIdInfBasicaUbigeo(item[0]==null?null:(Integer) item[0]);
                    temp.setCodigoUbigeo(item[1]==null?null:(String) item[1]);
                    temp.setPrincipal(item[2]==null?null:(Boolean) item[2]);
                    temp.setIdInfBasica(item[3]==null?null:(Integer) item[3]);
                    temp.setCuenca(item[4]==null?null:(String) item[4]);
                    temp.setCodigoDepartamento(item[5]==null?null:(String) item[5]);
                    temp.setCodigoProvincia(item[6]==null?null:(String) item[6]); 
                    temp.setCodigoDistrito(item[7]==null?null:(String) item[7]);  
                    temp.setNombreDepartamento(item[8]==null?null:(String) item[8]);  
                    temp.setNombreProvincia(item[9]==null?null:(String) item[9]);
                    temp.setNombreDistrito(item[10]==null?null:(String) item[10]);                   
                    resultDet.add(temp);
                }
            }

            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }


    @Override
    public ResultClassEntity ActualizarInformacionBasicaDistrito(InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaUbigeo_Actualizar");
            processStored.registerStoredProcedureParameter("idInfBasicaUbi", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUbigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("principal", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
           

            setStoreProcedureEnableNullParameters(processStored);            

            processStored.setParameter("idInfBasicaUbi", informacionBasicaUbigeoEntity.getIdInfBasicaUbigeo());
            processStored.setParameter("codigoUbigeo", informacionBasicaUbigeoEntity.getCodigoUbigeo());
            processStored.setParameter("principal", informacionBasicaUbigeoEntity.isPrincipal());
            processStored.setParameter("idInfBasica", informacionBasicaUbigeoEntity.getIdInfBasica());
            processStored.setParameter("idUsuarioModificacion", informacionBasicaUbigeoEntity.getIdUsuarioModificacion());
            processStored.setParameter("estado", informacionBasicaUbigeoEntity.getEstado());
          
            processStored.execute();
            
            result.setSuccess(true);
            result.setMessage("Se actualizó la información básica distrito.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }    

  
    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

    @Override
    public ResultClassEntity eliminarInformacionBasicaUbigeo(InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaUbigeo_Eliminar");
            processStored.registerStoredProcedureParameter("idInfBasicaUbi", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN); 

            setStoreProcedureEnableNullParameters(processStored);            

            processStored.setParameter("idInfBasicaUbi", informacionBasicaUbigeoEntity.getIdInfBasicaUbigeo());
            processStored.setParameter("idUsuarioElimina", informacionBasicaUbigeoEntity.getIdUsuarioElimina());
       
            processStored.execute();
            
            result.setSuccess(true);
            result.setMessage("Se elimino la información básica distrito.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }
}
