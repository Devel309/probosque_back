package pe.gob.serfor.mcsniffs.repository.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.eclipse.persistence.internal.sessions.DirectCollectionChangeRecord.NULL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoRespuestaEntity;
import pe.gob.serfor.mcsniffs.entity.GeneralAnexoEntity;
import pe.gob.serfor.mcsniffs.entity.ListarProcesoPosEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.repository.AnexoRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ProcesoPostulacionRepositoryImpl extends JdbcDaoSupport implements ProcesoPostulacionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Repository de los procesos de postulacion}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProcesoPostulacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private AnexoRepository obteneranexo;

    @Autowired
    private ProcesoOfertaRepository po;

    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Guardar proceso postulacion}
     * @param:ProcesoPostulacionResquestEntity
     */
    @Override
    public ResultClassEntity guardaProcesoPostulacion(ProcesoPostulacionResquestEntity proceso) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            Boolean exito = false;
            Boolean exitoupdate = false;
            int salida = -1;
            if(proceso.getIdProcesoPostulacion().equals(null) || proceso.getIdProcesoPostulacion().equals(0)){
                Integer idProcesoOferta = proceso.getProcesopostulacion().getIdProcesoOferta();
                ListarProcesoPosEntity filtro = new ListarProcesoPosEntity();
                filtro.setIdUsuarioPostulacion(null);
                filtro.setIdStatus(null);
                filtro.setIdProcesoPostulacion(null);
                filtro.setIdProcesoOferta(idProcesoOferta);
                filtro.setPageNum(1);
                filtro.setPageSize(1);
                ResultEntity<ProcesoPostulacionEntity> procesoPostu =  listarProcesoPostulacion(filtro);
                Integer IdStatus = 3;
                if(!(procesoPostu.getData().size()>0)){
                    ResultClassEntity objre= po.actualizarEsttatusProcesoOferta(idProcesoOferta , IdStatus, proceso.getProcesopostulacion().getIdUsuarioRegistro());
                }

                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_insertar");
                processStored.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idRegenteForestal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idStatusProceso", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaPostulacion", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("recepcionDocumentos", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaRecepcionDocumentos", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaResgitro", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("IdProPostulacion", Integer.class, ParameterMode.OUT);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idUsuarioPostulacion", proceso.getIdUsuarioPostulacion());
                processStored.setParameter("idRegenteForestal", proceso.getProcesopostulacion().getIdRegenteForestal());
                processStored.setParameter("idStatusProceso", proceso.getProcesopostulacion().getIdEstatusProceso());
                processStored.setParameter("fechaPostulacion", new java.sql.Timestamp(date.getTime()));
                processStored.setParameter("observacion", proceso.getProcesopostulacion().getObservacion());
                processStored.setParameter("recepcionDocumentos", proceso.getProcesopostulacion().getRecepcionDocumentos());
                processStored.setParameter("fechaRecepcionDocumentos", proceso.getProcesopostulacion().getFechaRecepcionDocumentos());
                processStored.setParameter("idProcesoOferta", proceso.getProcesopostulacion().getIdProcesoOferta());
                processStored.setParameter("idUsuarioRegistro", proceso.getProcesopostulacion().getIdUsuarioRegistro());
                processStored.setParameter("fechaResgitro", new java.sql.Timestamp(date.getTime()));
                processStored.execute();

                Integer IdProPostulacion = (Integer) processStored.getOutputParameterValue("IdProPostulacion");
                salida = IdProPostulacion;




                if(!(proceso.getAnexo1()==null)){
                    List<GeneralAnexoEntity> lst_data = new ArrayList<>();
                    lst_data.add(agregarColumnas("Solicitante",proceso.getAnexo1().getSolicitante()));
                    lst_data.add(agregarColumnas("Representante Legal",proceso.getAnexo1().getRepresentanteLegal()));
                    lst_data.add(agregarColumnas("Superficie Solicitada(ha)",proceso.getAnexo1().getSuperficieSolicitada()));
                    lst_data.add(agregarColumnas("Plazo de Concesion(años)",proceso.getAnexo1().getPlazoConcesion().toString()));
                    lst_data.add(agregarColumnas("Sector/Anexo/Caserio",proceso.getAnexo1().getSectorAnexoCaserio()));
                    lst_data.add(agregarColumnas("IdDistrito",proceso.getAnexo1().getIdDistrito().getIdDistrito().toString()));
                    lst_data.add(agregarColumnas("IdProvincia",proceso.getAnexo1().getIdProvincia().getIdProvincia().toString()));
                    lst_data.add(agregarColumnas("IdDepartamento",proceso.getAnexo1().getIdDepartamento().getIdDepartamento().toString()));
                    lst_data.add(agregarColumnas("Objetivo",proceso.getAnexo1().getObjetivo()));
                    lst_data.add(agregarColumnas("Autoridad Forestal",proceso.getAnexo1().getAutoridadForestal()));
                    exito = InsertarGeneralAnexo(lst_data,proceso.getAnexo1().getCodigoAnexo(),salida,proceso.getIdUsuarioPostulacion(),proceso.getAnexo1().getIdStatusAnexo());

                }else if(!(proceso.getAnexo2()==null)){
                    List<GeneralAnexoEntity> lst_data = new ArrayList<>();
                    lst_data.add(agregarColumnas("Nombres y Apellidos del Regente",proceso.getAnexo2().getNombreapellidos()));
                    lst_data.add(agregarColumnas("Dni",proceso.getAnexo2().getDni()));
                    lst_data.add(agregarColumnas("Direccion",proceso.getAnexo2().getDireccion()));
                    lst_data.add(agregarColumnas("Sector",proceso.getAnexo2().getSector()));
                    lst_data.add(agregarColumnas("IdDistrito",proceso.getAnexo2().getIdDistrito().getIdDistrito().toString()));
                    lst_data.add(agregarColumnas("IdProvincia",proceso.getAnexo2().getIdProvincia().getIdProvincia().toString()));
                    lst_data.add(agregarColumnas("IdDepartamento",proceso.getAnexo2().getIdDepartamento().getIdDepartamento().toString()));
                    lst_data.add(agregarColumnas("Telf. Fijo",proceso.getAnexo2().getTelefono()));
                    lst_data.add(agregarColumnas("Telf. Celular",proceso.getAnexo2().getCelular()));
                    lst_data.add(agregarColumnas("Correo Electronico",proceso.getAnexo2().getCorreo()));
                    lst_data.add(agregarColumnas("Razon Social del Solicitante de la Concesion",proceso.getAnexo2().getRazonsocialsolicitante()));
                    lst_data.add(agregarColumnas("Profesion",proceso.getAnexo2().getProfesion()));
                    lst_data.add(agregarColumnas("N° de Registro en el Colegio Profesional",proceso.getAnexo2().getNroregistrocolegioprofesional()));
                    lst_data.add(agregarColumnas("N° de Registro de Regente en el SERFOR",proceso.getAnexo2().getNroregistroserfor()));
                    lst_data.add(agregarColumnas("Equipo Tecnico",proceso.getAnexo2().getEquipotecnico()));
                    lst_data.add(agregarColumnas("Lugar y Fecha",proceso.getAnexo2().getLugarfirma()));
                    exito = InsertarGeneralAnexo(lst_data,proceso.getAnexo2().getCodigoAnexo(),salida,proceso.getIdUsuarioPostulacion(),proceso.getAnexo2().getIdStatusAnexo());

                }else if(!(proceso.getAnexo3()==null)){
                    List<GeneralAnexoEntity> lst_data = new ArrayList<>();
                    lst_data.add(agregarColumnas("Nombres y Apellidos del Funcionario Responsable de la Concesion",proceso.getAnexo3().getNomapellfuncionrespon()));
                    lst_data.add(agregarColumnas("Cargo del funcionario de la ARFFS respinsable de la concesion",proceso.getAnexo3().getCargoFuncioArffssRespo()));
                    lst_data.add(agregarColumnas("Nombres y Apellidos del Representante Legal",proceso.getAnexo3().getNomapellreprelegal()));
                    lst_data.add(agregarColumnas("Dni del Representante Legal",proceso.getAnexo3().getDnireprelegal()));
                    lst_data.add(agregarColumnas("Denominacion de la Persona Juridica",proceso.getAnexo3().getDenomipersojuridica()));
                    lst_data.add(agregarColumnas("Ruc de la Persona Juridic",proceso.getAnexo3().getRucpersojuridica()));
                    lst_data.add(agregarColumnas("Domicilio de la Persona Juridic",proceso.getAnexo3().getDireccpersojuridica()));
                    lst_data.add(agregarColumnas("Descripcion de Documentos",proceso.getAnexo3().getDescdocumentos()));
                    lst_data.add(agregarColumnas("Monto Estados Financieros",proceso.getAnexo3().getMonto1()));
                    lst_data.add(agregarColumnas("Monto Reporte Situciacion Crediticia",proceso.getAnexo3().getMonto2()));
                    lst_data.add(agregarColumnas("Monto Documentos de Bienes",proceso.getAnexo3().getMonto3()));
                    lst_data.add(agregarColumnas("Lugar de Firma",proceso.getAnexo3().getLugarfirma()));
                    lst_data.add(agregarColumnas("Fecha de Firma",formatter.format(date)));
                    exito = InsertarGeneralAnexo(lst_data,proceso.getAnexo3().getCodigoAnexo(),salida,proceso.getIdUsuarioPostulacion(),proceso.getAnexo3().getIdStatusAnexo());

                }
            }else{
                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_update");
                processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idEstatusProceso", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("recepcionDocumentos", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaRecepcionDocumentos", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroTramite", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idProcesoPostulacion", proceso.getIdProcesoPostulacion());
                processStored.setParameter("idEstatusProceso", proceso.getProcesopostulacion().getIdEstatusProceso());
                processStored.setParameter("recepcionDocumentos", proceso.getProcesopostulacion().getRecepcionDocumentos());
                processStored.setParameter("fechaRecepcionDocumentos", proceso.getProcesopostulacion().getFechaRecepcionDocumentos());
                processStored.setParameter("numeroTramite", proceso.getProcesopostulacion().getNumeroTramite());
                processStored.setParameter("idUsuarioModificacion", proceso.getProcesopostulacion().getIdUsuarioModificacion());
                processStored.setParameter("fechaModificacion", new java.sql.Timestamp(date.getTime()));
                processStored.execute();

                AnexoRequestEntity anexore = new AnexoRequestEntity();
                anexore.setIdProcesoPostulacion(proceso.getIdProcesoPostulacion());
                anexore.setIdUsuarioPostulacion(proceso.getIdUsuarioPostulacion());
                Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false;
                if(!(proceso.getAnexo1()==null)){
                    anexore.setCodigoAnexo("anexo1");
                    ResultEntity<GeneralAnexoEntity> anexo1 =obteneranexo.ObtenerAnexo(anexore);
                    List<GeneralAnexoEntity> lst_data = new ArrayList<>();
                    List<GeneralAnexoEntity> lst_dataupdate = new ArrayList<>();
                    for (GeneralAnexoEntity obj:anexo1.getData()) {
                        if(obj.getDescripcion().equals("Solicitante")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Solicitante",proceso.getAnexo1().getSolicitante(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Representante Legal")){
                            p2=true;
                            lst_dataupdate.add(agregarColumnasupdate("Representante Legal",proceso.getAnexo1().getRepresentanteLegal(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Superficie Solicitada(ha)")){
                            p3 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Superficie Solicitada(ha)",proceso.getAnexo1().getSuperficieSolicitada(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Plazo de Concesion(años)")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("Plazo de Concesion(años)",proceso.getAnexo1().getPlazoConcesion().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Sector/Anexo/Caserio")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("Sector/Anexo/Caserio",proceso.getAnexo1().getSectorAnexoCaserio(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("IdDistrito")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("IdDistrito",proceso.getAnexo1().getIdDistrito().getIdDistrito().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("IdProvincia")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("IdProvincia",proceso.getAnexo1().getIdProvincia().getIdProvincia().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("IdDepartamento")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("IdDepartamento",proceso.getAnexo1().getIdDepartamento().getIdDepartamento().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Objetivo")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("Objetivo",proceso.getAnexo1().getObjetivo(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Autoridad Forestal")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("Autoridad Forestal",proceso.getAnexo1().getAutoridadForestal(),obj.getIdGeneralAnexos()));
                        }
                    }
                    if(!p1){lst_data.add(agregarColumnas("Solicitante",proceso.getAnexo1().getSolicitante())); }
                    if(!p2){lst_data.add(agregarColumnas("Representante Legal",proceso.getAnexo1().getRepresentanteLegal())); }
                    if(!p3){lst_data.add(agregarColumnas("Superficie Solicitada(ha)",proceso.getAnexo1().getSuperficieSolicitada())); }
                    if(!p4){lst_data.add(agregarColumnas("Plazo de Concesion(años)",proceso.getAnexo1().getPlazoConcesion().toString())); }
                    if(!p5){lst_data.add(agregarColumnas("Sector/Anexo/Caserio",proceso.getAnexo1().getSectorAnexoCaserio())); }
                    if(!p6){lst_data.add(agregarColumnas("IdDistrito",proceso.getAnexo1().getIdDistrito().getIdDistrito().toString())); }
                    if(!p7){lst_data.add(agregarColumnas("IdProvincia",proceso.getAnexo1().getIdProvincia().getIdProvincia().toString())); }
                    if(!p8){lst_data.add(agregarColumnas("IdDepartamento",proceso.getAnexo1().getIdDepartamento().getIdDepartamento().toString())); }
                    if(!p9){lst_data.add(agregarColumnas("Objetivo", proceso.getAnexo1().getObjetivo())); }
                    if(!p10){lst_data.add(agregarColumnas("Autoridad Forestal",proceso.getAnexo1().getAutoridadForestal())); }

                    exito = InsertarGeneralAnexo(lst_data,proceso.getAnexo1().getCodigoAnexo(),proceso.getIdProcesoPostulacion(),proceso.getIdUsuarioPostulacion(),proceso.getAnexo1().getIdStatusAnexo());
                    exitoupdate = ActualizarGeneralAnexo(lst_dataupdate,proceso.getIdUsuarioPostulacion(),proceso.getAnexo1().getIdStatusAnexo());

                }else if(!(proceso.getAnexo2()==null)){
                    anexore.setCodigoAnexo("anexo2");
                    ResultEntity<GeneralAnexoEntity> anexo2 =obteneranexo.ObtenerAnexo(anexore);
                    p1= false;p2= false;p3= false;p4= false;p5= false;p6= false;p7= false;p8= false;p9= false;p10= false;p13= false;
                    Boolean p14= false,p15= false,p16= false;
                    List<GeneralAnexoEntity> lst_data = new ArrayList<>();
                    List<GeneralAnexoEntity> lst_dataupdate = new ArrayList<>();
                    for (GeneralAnexoEntity obj:anexo2.getData()) {
                        if(obj.getDescripcion().equals("Nombres y Apellidos del Regente")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Nombres y Apellidos del Regente",proceso.getAnexo2().getNombreapellidos(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Dni")){
                            p2=true;
                            lst_dataupdate.add(agregarColumnasupdate("Dni",proceso.getAnexo2().getDni(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Direccion")){
                            p3 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Direccion",proceso.getAnexo2().getDireccion(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Sector")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("Sector",proceso.getAnexo2().getSector(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("IdDistrito")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("IdDistrito",proceso.getAnexo2().getIdDistrito().getIdDistrito().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("IdProvincia")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("IdProvincia",proceso.getAnexo2().getIdProvincia().getIdProvincia().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("IdDepartamento")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("IdDepartamento",proceso.getAnexo2().getIdDepartamento().getIdDepartamento().toString(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Telf. Fijo")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("Telf. Fijo",proceso.getAnexo2().getTelefono(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Telf. Celular")){
                            p9 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Telf. Celular",proceso.getAnexo2().getCelular(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Correo Electronico")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("Correo Electronico",proceso.getAnexo2().getCorreo(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Razon Social del Solicitante de la Concesion")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("Razon Social del Solicitante de la Concesion",proceso.getAnexo2().getRazonsocialsolicitante(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Profesion")){
                            p12=true;
                            lst_dataupdate.add(agregarColumnasupdate("Profesion",proceso.getAnexo2().getProfesion(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("N° de Registro en el Colegio Profesional")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("N° de Registro en el Colegio Profesional",proceso.getAnexo2().getNroregistrocolegioprofesional(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("N° de Registro de Regente en el SERFOR")){
                            p14=true;
                            lst_dataupdate.add(agregarColumnasupdate("N° de Registro de Regente en el SERFOR",proceso.getAnexo2().getNroregistroserfor(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Equipo Tecnico")){
                            p15=true;
                            lst_dataupdate.add(agregarColumnasupdate("Equipo Tecnico",proceso.getAnexo2().getEquipotecnico(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Lugar y Fecha")){
                            p16=true;
                            lst_dataupdate.add(agregarColumnasupdate("Lugar y Fecha",proceso.getAnexo2().getLugarfirma(),obj.getIdGeneralAnexos()));
                        }
                    }
                    if(!p1){lst_data.add(agregarColumnas("Nombres y Apellidos del Regente",proceso.getAnexo2().getNombreapellidos())); }
                    if(!p2){lst_data.add(agregarColumnas("Dni",proceso.getAnexo2().getDni())); }
                    if(!p3){lst_data.add(agregarColumnas("Direccion",proceso.getAnexo2().getDireccion()));}
                    if(!p4){lst_data.add(agregarColumnas("Sector",proceso.getAnexo2().getSector()));}
                    if(!p5){lst_data.add(agregarColumnas("IdDistrito",proceso.getAnexo2().getIdDistrito().getIdDistrito().toString()));}
                    if(!p6){lst_data.add(agregarColumnas("IdProvincia",proceso.getAnexo2().getIdProvincia().getIdProvincia().toString()));}
                    if(!p7){lst_data.add(agregarColumnas("IdDepartamento",proceso.getAnexo2().getIdDepartamento().getIdDepartamento().toString()));}
                    if(!p8){lst_data.add(agregarColumnas("Telf. Fijo",proceso.getAnexo2().getTelefono()));}
                    if(!p9){lst_data.add(agregarColumnas("Telf. Celular",proceso.getAnexo2().getCelular()));}
                    if(!p10){lst_data.add(agregarColumnas("Correo Electronico",proceso.getAnexo2().getCorreo()));}
                    if(!p11){lst_data.add(agregarColumnas("Razon Social del Solicitante de la Concesion",proceso.getAnexo2().getRazonsocialsolicitante()));}
                    if(!p12){lst_data.add(agregarColumnas("Profesion",proceso.getAnexo2().getProfesion()));}
                    if(!p13){lst_data.add(agregarColumnas("N° de Registro en el Colegio Profesional",proceso.getAnexo2().getNroregistrocolegioprofesional()));}
                    if(!p14){lst_data.add(agregarColumnas("N° de Registro de Regente en el SERFOR",proceso.getAnexo2().getNroregistroserfor()));}
                    if(!p15){lst_data.add(agregarColumnas("Equipo Tecnico",proceso.getAnexo2().getEquipotecnico()));}
                    if(!p16){lst_data.add(agregarColumnas("Lugar y Fecha",proceso.getAnexo2().getLugarfirma()));}

                    exito = InsertarGeneralAnexo(lst_data,proceso.getAnexo2().getCodigoAnexo(),proceso.getIdProcesoPostulacion(),proceso.getIdUsuarioPostulacion(),proceso.getAnexo2().getIdStatusAnexo());
                    exitoupdate = ActualizarGeneralAnexo(lst_dataupdate,proceso.getIdUsuarioPostulacion(),proceso.getAnexo2().getIdStatusAnexo());

                }else if(!(proceso.getAnexo3()==null)){
                    anexore.setCodigoAnexo("anexo3");
                    ResultEntity<GeneralAnexoEntity> anexo3 =obteneranexo.ObtenerAnexo(anexore);
                    p1= false;p2= false;p3= false;p4= false;p5= false;p6= false;p7= false;p8= false;p9= false;p10= false;p11= false;p12= false;p13= false;
                    List<GeneralAnexoEntity> lst_data = new ArrayList<>();
                    List<GeneralAnexoEntity> lst_dataupdate = new ArrayList<>();
                    for (GeneralAnexoEntity obj:anexo3.getData()) {
                        if(obj.getDescripcion().equals("Nombres y Apellidos del Funcionario Responsable de la Concesion")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Nombres y Apellidos del Funcionario Responsable de la Concesion",proceso.getAnexo3().getNomapellfuncionrespon(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Cargo del funcionario de la ARFFS respinsable de la concesion")){
                            p2=true;
                            lst_dataupdate.add(agregarColumnasupdate("Cargo del funcionario de la ARFFS respinsable de la concesion",proceso.getAnexo3().getCargoFuncioArffssRespo(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Nombres y Apellidos del Representante Legal")){
                            p3=true;
                            lst_dataupdate.add(agregarColumnasupdate("Nombres y Apellidos del Representante Legal",proceso.getAnexo3().getNomapellreprelegal(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Dni del Representante Legal")){
                            p4 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Dni del Representante Legal",proceso.getAnexo3().getDnireprelegal(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Denominacion de la Persona Juridica")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("Denominacion de la Persona Juridica",proceso.getAnexo3().getDenomipersojuridica(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Ruc de la Persona Juridica")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("Ruc de la Persona Juridica",proceso.getAnexo3().getRucpersojuridica(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Domicilio de la Persona Juridica")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("Domicilio de la Persona Juridica",proceso.getAnexo3().getDireccpersojuridica(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Descripcion de Documentos")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("Descripcion de Documentos",proceso.getAnexo3().getDescdocumentos(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Monto Estados Financieros")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("Monto Estados Financieros",proceso.getAnexo3().getMonto1(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Monto Reporte Situciacion Crediticia")){
                            p10 =true;
                            lst_dataupdate.add(agregarColumnasupdate("Monto Reporte Situciacion Crediticia",proceso.getAnexo3().getMonto2(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Monto Documentos de Bienes")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("Monto Documentos de Bienes",proceso.getAnexo3().getMonto3(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Lugar de Firma")){
                            p12=true;
                            lst_dataupdate.add(agregarColumnasupdate("Lugar de Firma",proceso.getAnexo3().getLugarfirma(),obj.getIdGeneralAnexos()));
                        }else if(obj.getDescripcion().equals("Fecha de Firma")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("Fecha de Firma",formatter.format(date),obj.getIdGeneralAnexos()));
                        }
                    }
                    if(!p1){lst_data.add(agregarColumnas("Nombres y Apellidos del Funcionario Responsable de la Concesion",proceso.getAnexo3().getNomapellfuncionrespon())); }
                    if(!p2){lst_data.add(agregarColumnas("Cargo del funcionario de la ARFFS respinsable de la concesion",proceso.getAnexo3().getCargoFuncioArffssRespo())); }
                    if(!p3){lst_data.add(agregarColumnas("Nombres y Apellidos del Representante Legal",proceso.getAnexo3().getNomapellreprelegal())); }
                    if(!p4){lst_data.add(agregarColumnas("Dni del Representante Legal",proceso.getAnexo3().getDnireprelegal()));}
                    if(!p5){lst_data.add(agregarColumnas("Denominacion de la Persona Juridica",proceso.getAnexo3().getDenomipersojuridica()));}
                    if(!p6){lst_data.add(agregarColumnas("Ruc de la Persona Juridica",proceso.getAnexo3().getRucpersojuridica()));}
                    if(!p7){lst_data.add(agregarColumnas("Domicilio de la Persona Juridica",proceso.getAnexo3().getDireccpersojuridica()));}
                    if(!p8){lst_data.add(agregarColumnas("Descripcion de Documentos",proceso.getAnexo3().getDescdocumentos()));}
                    if(!p9){lst_data.add(agregarColumnas("Monto Estados Financieros",proceso.getAnexo3().getMonto1()));}
                    if(!p10){lst_data.add(agregarColumnas("Monto Reporte Situciacion Crediticia",proceso.getAnexo3().getMonto2()));}
                    if(!p11){lst_data.add(agregarColumnas("Monto Documentos de Bienes",proceso.getAnexo3().getMonto3()));}
                    if(!p12){lst_data.add(agregarColumnas("Lugar de Firma",proceso.getAnexo3().getLugarfirma()));}
                    if(!p13){lst_data.add(agregarColumnas("Fecha de Firma",formatter.format(date)));}
                    exito = InsertarGeneralAnexo(lst_data,proceso.getAnexo3().getCodigoAnexo(),proceso.getIdProcesoPostulacion(),proceso.getIdUsuarioPostulacion(),proceso.getAnexo3().getIdStatusAnexo());
                    exitoupdate = ActualizarGeneralAnexo(lst_dataupdate,proceso.getIdUsuarioPostulacion(),proceso.getAnexo3().getIdStatusAnexo());
                }
            }
            if(exito){
                result.setInformacion("Se registró el anexo correctamente.");
            }
            if(exitoupdate){
                result.setInformacion("Se actualizó el anexo correctamente.");
            }
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se inició tu postulación.");
            return result;
        }
        catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - guardaProcesoPostulacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    public Boolean ActualizarGeneralAnexo(List<GeneralAnexoEntity> lst_data,Integer IdUsuarioModificacion, Integer IdEstatusAnexo){
        ResultClassEntity result = new ResultClassEntity();
        Boolean exito = false;
        Date date = new Date();
        try{

            if(lst_data.size()>0) {
                for (GeneralAnexoEntity obj : lst_data) {

                    StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_GeneralAnexo_updateanexo");
                    processStored.registerStoredProcedureParameter("idGeneralAnexos", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("value", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idEstatusAnexo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored);

                    processStored.setParameter("idGeneralAnexos", obj.getIdGeneralAnexos());
                    processStored.setParameter("value", obj.getValue());
                    processStored.setParameter("idEstatusAnexo", IdEstatusAnexo);
                    processStored.setParameter("idUsuarioModificacion", IdUsuarioModificacion);
                    processStored.setParameter("fechaModificacion", new java.sql.Timestamp(date.getTime()));
                    processStored.execute();
                }
            }
            exito = true;
        }
        catch (Exception e){
            exito = false;
            log.error(e.getMessage());
        }
        return exito;
    }

    public Boolean InsertarGeneralAnexo(List<GeneralAnexoEntity> lst_data, String codigoanexo,Integer IdProcesoPostulacion, Integer IdUsuarioPostulacion, Integer IdEstatusAnexo){
        ResultClassEntity result = new ResultClassEntity();
        Boolean exito = false;
        Date date = new Date();
        Integer salida = 0;
        String idgenerados = "";
        try{

            if(lst_data.size()>0) {
                for (GeneralAnexoEntity obj : lst_data) {

                    StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_GeneralAnexo_insertaranexo");
                    processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("codigoanexo", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("value", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idEstatusAnexo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("fechaResgitro", Date.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idGeneralAnexos", Integer.class, ParameterMode.OUT);
                    SpUtil.enableNullParams(processStored);

                    processStored.setParameter("idProcesoPostulacion", IdProcesoPostulacion);
                    processStored.setParameter("idUsuarioPostulacion", IdUsuarioPostulacion);
                    processStored.setParameter("codigoanexo", codigoanexo);
                    processStored.setParameter("descripcion", obj.getDescripcion());
                    processStored.setParameter("value", obj.getValue());
                    processStored.setParameter("idEstatusAnexo", IdEstatusAnexo);
                    processStored.setParameter("idUsuarioRegistro", IdUsuarioPostulacion);
                    processStored.setParameter("fechaResgitro", new java.sql.Timestamp(date.getTime()));
                    processStored.execute();
                    Integer idGeneralAnexos = (Integer) processStored.getOutputParameterValue("idGeneralAnexos");
                    salida = idGeneralAnexos;
                    idgenerados += salida+",";
                }
            }
            exito = true;
        }
        catch (Exception e){
            exito = false;
        }
        return exito;
    }

    public GeneralAnexoEntity agregarColumnas(String descripcion, String value){
        GeneralAnexoEntity objanexo = new GeneralAnexoEntity();
        objanexo.setDescripcion(descripcion);
        objanexo.setValue(value);
        return objanexo;
    }

    public GeneralAnexoEntity agregarColumnasupdate(String descripcion, String value, Integer IdGeneralAnexos){
        GeneralAnexoEntity objanexo = new GeneralAnexoEntity();
        objanexo.setDescripcion(descripcion);
        objanexo.setValue(value);
        objanexo.setIdGeneralAnexos(IdGeneralAnexos);
        return objanexo;
    }

    /**
     * @autor: JaquelineDB [24-06-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Listar proceso de postulación por usuario}
     * @param:ListarProcesoPosEntity
     */
    @Override
    public ResultEntity<ProcesoPostulacionEntity> listarProcesoPostulacion(ListarProcesoPosEntity filtro) {
        ResultEntity<ProcesoPostulacionEntity> result = new ResultEntity<ProcesoPostulacionEntity>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ListarPorFiltros");
            sp.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idStatus", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);


            SpUtil.enableNullParams(sp);
            sp.setParameter("idUsuarioPostulacion", filtro.getIdUsuarioPostulacion());
            sp.setParameter("idStatus", filtro.getIdStatus());
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idProcesoOferta", filtro.getIdProcesoOferta());
            sp.setParameter("pageNum", filtro.getPageNum());
            sp.setParameter("pageSize", filtro.getPageSize());
            sp.setParameter("perfil", filtro.getPerfil());
            sp.execute();

            ProcesoPostulacionEntity c = null;
            List<ProcesoPostulacionEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ProcesoPostulacionEntity();
                    c.setIdProcesoPostulacion((Integer) row[0]);
                    c.setNombrePostulante((String) row[1]);
                    c.setIdUsuarioPostulacion((Integer) row[2]);
                    c.setIdRegenteForestal((Integer) row[3]);
                    c.setNombreRegente((String) row[4]);
                    c.setFechaPostulacion((Date) row[5]);
                    c.setObservacion((String) row[6]);
                    c.setRecepcionDocumentos((Boolean) row[7]);
                    c.setFechaRecepcionDocumentos((Date) row[8]);
                    c.setIdProcesoOferta((Integer.parseInt(row[9].toString())));
                    c.setUnidadAProvechamiento((String) row[10]);
                    c.setIdEstatusProceso((Integer) row[11]);
                    c.setStatusProceso((String) row[12]);
                    c.setNumeroTramite((String) row[13]);
                    c.setAutorizacionpublicacion((Integer) row[14]);
                    c.setPruebaspublicacion((Integer) row[15]);
                    result.setTotalrecord((Integer) row[16]);
                    c.setObservacionARFFS((String) row[17]);
                    c.setObservado((Boolean) row[18]);
                    c.setNumeroResolucion((Integer) row[19]);
                    c.setFechaResolucion((Date) row[20]);
                    c.setEnvioPrueba(row[21]==null?null: (Boolean)row[21]);
                    c.setPublicacionPrueba(row[22]==null?null: (Boolean)row[22]);
                    c.setIdResolucion(row[23]==null?null: (Integer)row[23]);
                    c.setResolucionVigencia(row[24]==null?null: (Boolean)row[24]);

                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron Procesos de Postulación.");

            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - listarProcesoPostulacion", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [30-06-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Envía el documento de autorización para la publicación}
     * @param:file
     */
    @Override
    public ResultClassEntity<DocumentoRespuestaEntity> autorizarPublicacion(MultipartFile file, String CodigoDoc,String NombreArchivo,Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto) {
        ResultClassEntity<DocumentoRespuestaEntity> result = new ResultClassEntity<DocumentoRespuestaEntity>();
        try {
            ResultClassEntity archivo = obteneranexo.AdjuntarAnexo(file,IdProcesoPostulacion,IdUsuarioRegistro,CodigoDoc,NombreArchivo,IdTipoDocumento,IdPostulacionPFDM,idDocumentoAdjunto);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String asunto ="Autorizacion de publicación";
            String contenido = "Se adjunta el documento que acredita la autorización de la publicación: Descargar adjunto: ";
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoRespuesta_Registrar");
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioAutorizar", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaEnvioCorreo", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asuntoEmail", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("contenidoEmail", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocRespuesta", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idDocumentoAdjunto", archivo.getCodigo());
            sp.setParameter("idProcesoPostulacion", IdProcesoPostulacion);
            sp.setParameter("idUsuarioAutorizar", IdSolicitante);
            sp.setParameter("fechaEnvioCorreo", new java.sql.Timestamp(date.getTime()));
            sp.setParameter("asuntoEmail", asunto);
            sp.setParameter("contenidoEmail", contenido);
            sp.setParameter("idUsuarioRegistro", IdUsuarioRegistro);
            sp.setParameter("fechaRegistro", new java.sql.Timestamp(date.getTime()));
            sp.execute();
            Integer idDocRespuesta = (Integer) sp.getOutputParameterValue("idDocRespuesta");
            DocumentoRespuestaEntity obj = new DocumentoRespuestaEntity();
            obj.setContenidoEmail(asunto);
            obj.setContenidoEmail(contenido);
            //actualizar el estado a en publicacion
            Integer IdEstatus = 2;
            ResultClassEntity resultup = actualizarEstadoProcesoPostulacion(IdProcesoPostulacion,IdEstatus,IdUsuarioRegistro,null);
            //actualizar el estatus de proceso oferta
            ListarProcesoPosEntity filtro = new ListarProcesoPosEntity();
            filtro.setIdUsuarioPostulacion(null);
            filtro.setIdStatus(null);
            filtro.setIdProcesoPostulacion(IdProcesoPostulacion);
            filtro.setIdProcesoOferta(null);
            ResultEntity<ProcesoPostulacionEntity> lista = listarProcesoPostulacion(filtro);
            Integer StatusOferta = 2;
            Integer ProcesoOfertaID = lista.getData().get(0).getIdProcesoOferta();
            ResultClassEntity objre = po.actualizarEsttatusProcesoOferta( ProcesoOfertaID == null?0:ProcesoOfertaID, StatusOferta, IdUsuarioRegistro);

            result.setData(obj);
            result.setCodigo(idDocRespuesta);
            result.setSuccess(true);
            result.setMessage("Se adjunto la autorización.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - autorizarPublicacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [01-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Obtiene las autorizaciones de publicación necesarias}
     * @param:file
     */
    @Override
    public ResultEntity<DocumentoRespuestaEntity> obtenerAutorizacion(AnexoRequestEntity filtro) {
        ResultEntity<DocumentoRespuestaEntity> result = new ResultEntity<DocumentoRespuestaEntity>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoRespuesta_Listar");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("IdUsuarioAutorizar", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("IdUsuarioAutorizar", filtro.getIdUsuarioPostulacion());
            sp.execute();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            DocumentoRespuestaEntity c = null;
            List<DocumentoRespuestaEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new DocumentoRespuestaEntity();
                    c.setIdDocumentoRespuesta((Integer) row[0]);
                    c.setIdDocumentoAdjunto((Integer) row[1]);
                    c.setIdProcesoPostulacion((Integer) row[2]);
                    c.setFechaEnvioCorreo((Date) row[3]);
                    c.setStr_fechaenviocorreo(formatter.format((Date) row[3]));
                    c.setAsuntoEmail((String) row[4]);
                    c.setContenidoEmail((String) row[5]);
                    c.setRuta((String) row[6]);
                    c.setIdSolicitante((Integer) row[7]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron autorizaciones enviadas.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - obtenerAutorizacion", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [01-07-2021]
     * @modificado:
     * @descripción: {Adjunta la resolucion terminando con el proceso de postulacion}
     * @param:file
     */
    @Override
    public ResultClassEntity<DocumentoRespuestaEntity> AdjuntarResolucionDenegada(MultipartFile file, String CodigoDoc, String NombreArchivo,Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto) {
        ResultClassEntity<DocumentoRespuestaEntity> result = new ResultClassEntity();
        try {
            ResultClassEntity archivo = obteneranexo.AdjuntarAnexo(file,IdProcesoPostulacion,IdUsuarioRegistro,CodigoDoc,NombreArchivo,IdTipoDocumento, IdPostulacionPFDM, idDocumentoAdjunto);

            Date date = new Date();
            String asunto ="Postulacion denegada";
            String contenido = "Se adjunta la resolucion que acredita la cancelacion de la publicación: " +
                    "Link de descarga: "+ archivo.getInformacion();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoRespuesta_Registrar");
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioAutorizar", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaEnvioCorreo", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asuntoEmail", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("contenidoEmail", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocRespuesta", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idDocumentoAdjunto", archivo.getCodigo());
            sp.setParameter("idProcesoPostulacion", IdProcesoPostulacion);
            sp.setParameter("idUsuarioAutorizar", IdSolicitante);
            sp.setParameter("fechaEnvioCorreo", new java.sql.Timestamp(date.getTime()));
            sp.setParameter("asuntoEmail", asunto);
            sp.setParameter("contenidoEmail", contenido);
            sp.setParameter("idUsuarioRegistro", IdUsuarioRegistro);
            sp.setParameter("fechaRegistro", new java.sql.Timestamp(date.getTime()));
            sp.execute();
            Integer idDocRespuesta = (Integer) sp.getOutputParameterValue("idDocRespuesta");
            result.setCodigo(idDocRespuesta);
            result.setSuccess(true);
            result.setMessage("Se adjuntó la autorizacion.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - AdjuntarResolucionDenegada", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [01-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Actualiza el estado del proceso de proceso de postulación}
     * @param:file
     */
    @Override
    public ResultClassEntity actualizarEstadoProcesoPostulacion(Integer idProcesoPostulacion, Integer idEstatus, Integer idUsuarioModificacion, Date fechaPublicacion) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_actualizarestatus");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstatus", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN); 
            sp.registerStoredProcedureParameter("fechaPublicacion", Date.class, ParameterMode.IN); 
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", idProcesoPostulacion);
            sp.setParameter("idEstatus", idEstatus);
            sp.setParameter("idUsuarioModificacion", idUsuarioModificacion); 
            sp.setParameter("fechaPublicacion", fechaPublicacion);
            sp.execute(); 
            
            result.setSuccess(true);
            result.setMessage("Se actualizó el proceso de postulación.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - actualizarEstadoProcesoPostulacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jordy Zamata [07-12-2021]
     * @descripción: {Actualiza las observaciones agregadas por el ARFFS }
     * @param:file
     */
    @Override
    public ResultClassEntity actualizarObservacionARFFSProcesoPostulacion (ProcesoPostulacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_actualizarobservacion_arffs");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("observado", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("observacionARFFS", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", obj.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            sp.setParameter("observado", obj.getObservado());
            sp.setParameter("observacionARFFS", obj.getObservacionARFFS());
            sp.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el proceso de postulación.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - actualizarObservacionARFFSProcesoPostulacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jordy Zamata [10-12-2021]
     * @descripción: {Actualiza la carga de resolución }
     * @param:file
     */
    @Override
    public ResultClassEntity actualizarResolucionProcesoPostulacion(ProcesoPostulacionDto obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_actualizarresolucion");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("numeroResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaResolucion", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", obj.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            sp.setParameter("numeroResolucion", obj.getNumeroResolucion());
            sp.setParameter("fechaResolucion", obj.getFechaResolucion());
            sp.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el proceso de postulación.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - actualizarResolucionProcesoPostulacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }


    @Override
    public ResultEntity obtenerInformacionProcesoPostulacion(ProcesoPostulacionDto param) {
        ResultEntity resul=new ResultEntity();
        try
        {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ObtenerInformacion");
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumento",param.getCodigoTipoDocumentoPostulante());
            processStored.setParameter("nroDocumento",param.getNumeroDocumentoPostulante());

            processStored.execute();
            List<ProcesoPostulacionEntity> objList=new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                ProcesoPostulacionEntity objOrdPro = null;
                for (Object[] row: spResult){
                    objOrdPro = new ProcesoPostulacionEntity();
                    objOrdPro.setIdContrato(row[0]==null?null:(Integer) row[0]);
                    objOrdPro.setNroContrato(row[1]==null?null:(String) row[1]);
                    objOrdPro.setDistrito(row[2]==null?null:(String) row[2]);
                    objOrdPro.setProvincia(row[3]==null?null:(String) row[3]);
                    objOrdPro.setDepartamento(row[4]==null?null:(String) row[4]);
                    objOrdPro.setDomicilio(row[5]==null?null:(String) row[5]);
                    objOrdPro.setNumeroDocumentoPostulante(row[6]==null?null:(String) row[6]);
                    objOrdPro.setNombrePostulante(row[7]==null?null:(String) row[7]);
                    objOrdPro.setApellidoPaternoPostulante(row[8]==null?null:(String) row[8]);
                    objOrdPro.setApellidoMaternoPostulante(row[9]==null?null:(String) row[9]);
                    objOrdPro.setNumeroDocumentoRepresentante(row[10]==null?null:(String) row[10]);
                    objOrdPro.setNombreRepresentante(row[11]==null?null:(String) row[11]);
                    objOrdPro.setApellidoPaternoRepresentante(row[12]==null?null:(String) row[12]);
                    objOrdPro.setApellidoMaternoRepresentante(row[13]==null?null:(String) row[13]);
                    objList.add(objOrdPro);
                }

            }

            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Información no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
            resul.setTotalrecord(objList.size());
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public  ResultClassEntity obtenerProcesoPostulacion(ProcesoPostulacionDto obj) {
         
         ResultClassEntity result= new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_Obtener");     
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoPostulacion",obj.getIdProcesoPostulacion());
            processStored.execute();
                ProcesoPostulacionEntity temp  = new ProcesoPostulacionEntity();
                List<Object[]> spResult = processStored.getResultList();
                if(spResult!=null){
                    if (spResult.size() >= 1){
                        for (Object[] row : spResult) {
                            temp.setIdProcesoOferta(row[0]==null?null:(Integer) row[0]);
                            temp.setIdProcesoPostulacion(row[1]==null?null:(Integer) row[1]);
                            temp.setIdUsuarioPostulacion(row[2]==null?null:(Integer) row[2]);
                            temp.setCodigoTipoPersonaPostulante(row[3]==null?null:(String) row[3]);
                            temp.setCodigoTipoDocumentoPostulante(row[4]==null?null:(String) row[4]);
                            temp.setNumeroDocumentoPostulante(row[5]==null?null:(String) row[5]);
                            temp.setNombrePostulante(row[6]==null?null:(String) row[6]);
                            temp.setApellidoPaternoPostulante(row[7]==null?null:(String) row[7]);
                            temp.setApellidoMaternoPostulante(row[8]==null?null:(String) row[8]);
                            temp.setCorreoPostulante(row[9]==null?null:(String) row[9]);
                            temp.setCodigoTipoPersonaRepresentante(row[10]==null?null:(String) row[10]);
                            temp.setCodigoTipoDocumentoRepresentante(row[11]==null?null:(String) row[11]);
                            temp.setNumeroDocumentoRepresentante(row[12]==null?null:(String) row[12]);
                            temp.setNombreRepresentante(row[13]==null?null:(String) row[13]);
                            temp.setApellidoPaternoRepresentante(row[14]==null?null:(String) row[14]);
                            temp.setApellidoMaternoRepresentante(row[15]==null?null:(String) row[15]);
                            temp.setAutorizacionExploracion(row[16]==null?null:(Boolean) row[16]);
                        }
                    }
                }
                result.setMessage("Se obtuvo Proceso Postulación correctamente.");
                result.setSuccess(true);
                result.setData(temp);
            return result;
        } catch (Exception e) {
            log.error("obtenerProcesoPostulacion", e.getMessage());

            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public  ResultClassEntity actualizarProcesoPostulacion(ProcesoPostulacionDto obj) {

        ResultClassEntity result= new ResultClassEntity();

        try {
                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_Actualizar");
                processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idEstatusProceso", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("recepcionDocumentos", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaRecepcionDocumentos", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroTramite", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tieneObservacion", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idProcesoPostulacion", obj.getIdProcesoPostulacion());
                processStored.setParameter("idEstatusProceso", obj.getIdEstatusProceso());
                processStored.setParameter("recepcionDocumentos", obj.getRecepcionDocumentos());
                processStored.setParameter("fechaRecepcionDocumentos", obj.getFechaRecepcionDocumentos());
                processStored.setParameter("numeroTramite", obj.getNumeroTramite());
                processStored.setParameter("tieneObservacion", obj.getTieneObservacion());
                processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

                processStored.execute();

            result.setMessage("Se actualizó correctamente.");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("obtenerProcesoPostulacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public  ResultClassEntity tieneObservacion(ProcesoPostulacionDto obj) {

        ResultClassEntity result= new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_TieneObs");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tieneObservacion", Boolean.class, ParameterMode.OUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoPostulacion",obj.getIdProcesoPostulacion());
            processStored.execute();

            Boolean tieneObs = (Boolean) processStored.getOutputParameterValue("tieneObservacion");


            result.setMessage("Se obtuvo Proceso Postulación correctamente.");
            result.setSuccess(true);
            result.setData(tieneObs);
            return result;
        } catch (Exception e) {
            log.error("obtenerProcesoPostulacion", e.getMessage());

            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity obtenerProcesoPostulacionValidarRequisito(ProcesoPostulacionDto obj) {
        ResultClassEntity result= new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ValidarRequisitos_Obtener");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoPostulacion",obj.getIdProcesoPostulacion());
            processStored.execute();
            ProcesoPostulacionEntity temp  = new ProcesoPostulacionEntity();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {
                        temp.setIdProcesoPostulacion(row[0]==null?null:(Integer) row[0]);
                        temp.setIdUsuarioPostulacion(row[1]==null?null:(Integer) row[1]);
                        temp.setIdProcesoOferta(row[2]==null?null:(Integer) row[2]);
                        temp.setFechaRecepcionDocumentos(row[3]==null?null: (Date) row[3]);
                        temp.setNumeroTramite(row[4]==null?null: (String) row[4]);
                        temp.setIdEstatusProceso(row[5]==null?null: (Integer) row[5]);
                    }
                }
            }
            result.setMessage("Se obtuvo Proceso Postulación para Validar Requisitos correctamente.");
            result.setSuccess(true);
            result.setData(temp);
            return result;
        } catch (Exception e) {
            log.error("obtenerProcesoPostulacionValidarRequisito", e.getMessage());

            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public  ResultClassEntity actualizarEnvioPrueba(ProcesoPostulacionDto obj) {

        ResultClassEntity result= new ResultClassEntity();

        try {
                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ActualizarEnvioPrueba");
                processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("envioPrueba", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idProcesoPostulacion", obj.getIdProcesoPostulacion());
                processStored.setParameter("envioPrueba", obj.getEnvioPrueba());
                processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

                processStored.execute();

            result.setMessage("Se actualizó correctamente.");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("actualizarEnvioPrueba", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public Integer validarPublicacion(ProcesoPostulacionEntity dto) throws Exception {
        Integer result = null;
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ValidarPublicacion");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diaAmpliacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("publicar", Integer.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoPostulacion", dto.getIdProcesoPostulacion());
            processStored.setParameter("fechaInicio", dto.getFechaPostulacion());
            processStored.setParameter("diaAmpliacion", dto.getDiaAmpliacion()); 
            processStored.execute();
            result =  (Integer) processStored.getOutputParameterValue("publicar");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  result;
        }
    }

    @Override
    public ResultClassEntity ImpugnarGanadorProcesoPostulacion(ProcesoPostulacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ImpugnacionGanador");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOferta", param.getIdProcesoOferta());
            sp.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            sp.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el Proceso Postulación correctamente.");
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity validarPublicacionId(ProcesoPostulacionDto obj){
        ResultClassEntity result = new ResultClassEntity();
        try{

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ValidarPublicacionPorId");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion2", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idProcesoPostulacion2", obj.getIdProcesoPostulacion());
            processStored.execute();
                
            result.setSuccess(true);
            result.setMessage("Se válido la publicación correctamente.");
            return result;
        }
        catch (Exception e){
            result.setSuccess(false);
            result.setMessage("Ocurrió un error, No se válido la publicación");
            return result;
        }
    }



}
