package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.MedioTransporteRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class MedioTransporteRepositoryImpl extends JdbcDaoSupport implements MedioTransporteRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(MedioTransporteRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public List<MedioTransporteEntity> ListarMedioTransporte(Integer idMedio) throws Exception {
        List<MedioTransporteEntity> lista = new ArrayList<MedioTransporteEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_MedioTransporte_Listar]");
            processStored.registerStoredProcedureParameter("idMedio", Integer.class, ParameterMode.IN);
            processStored.setParameter("idMedio", idMedio);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {

                    MedioTransporteEntity temp = new MedioTransporteEntity();
                    temp.setIdMedioTransporte((Integer) row[0]);
                    temp.setCodTipoMedio((String) row[1]);
                    temp.setMedioTransporte((String) row[2]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("ListarMedioTransporte", e.getMessage());
            throw e;
        }

    }



}
