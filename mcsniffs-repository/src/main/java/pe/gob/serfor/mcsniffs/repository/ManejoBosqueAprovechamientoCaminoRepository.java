package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;

import java.util.List;

public interface ManejoBosqueAprovechamientoCaminoRepository {
    ResultClassEntity RegistrarManejoBosqueAprovechamientoCamino(List<ManejoBosqueAprovechamientoCaminoEntity> list) throws Exception;
    ResultClassEntity EliminarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoCaminoEntity manejoBosqueAprovechamientoCamino) throws Exception;
    ResultClassEntity ListarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamientoCamino) throws Exception;
}
