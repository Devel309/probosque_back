package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.InformacionBasicaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

/**
 * @autor: Harry Coa [04-08-2021]
 * @modificado:
 * @descripción: {Repositorio Informacion Basica}
 *
 */
@Repository
public class InformacionBasicaDetalleRepositoryImpl extends JdbcDaoSupport implements InformacionBasicaDetalleRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final String SEPARADOR_ARCHIVO = ".";
    private static final Logger log = LogManager.getLogger(InformacionBasicaRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;
    @Autowired
    private FileServerConexion fileServerConexion;

    @Autowired
    private ArchivoRepositoryImpl respositoryArchivo;
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarInformacionBasicaDetalle(InformacionBasicaDetalleEntity informacionBasicaDetalleEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codInfBasicaDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubInfBasicaDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("puntoVertice", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaEsteIni", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaNorteIni", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaEsteFin", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaNorteFin", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("distanciaKm", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tiempo", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medioTransporte", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("rio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("quebrada", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("laguna", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("unidadFisiografica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("valor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHa", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHaPorcentaje", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("especieExtraida", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conflicto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solucion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idFauna", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfraestructura", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaUtm", String.class, ParameterMode.IN);


            setStoreProcedureEnableNullParameters(processStored);            

            processStored.setParameter("idInfBasicaDet", informacionBasicaDetalleEntity.getIdInfBasicaDet());
            processStored.setParameter("codInfBasicaDet", informacionBasicaDetalleEntity.getCodInfBasicaDet());
            processStored.setParameter("codSubInfBasicaDet", informacionBasicaDetalleEntity.getCodSubInfBasicaDet());
            processStored.setParameter("puntoVertice", informacionBasicaDetalleEntity.getPuntoVertice());
            processStored.setParameter("coordenadaEsteIni", informacionBasicaDetalleEntity.getCoordenadaEsteIni());
            processStored.setParameter("coordenadaNorteIni", informacionBasicaDetalleEntity.getCoordenadaNorteIni());
            processStored.setParameter("coordenadaEsteFin", informacionBasicaDetalleEntity.getCoordenadaEsteFin());
            processStored.setParameter("coordenadaNorteFin", informacionBasicaDetalleEntity.getCoordenadaNorteFin());
            processStored.setParameter("referencia", informacionBasicaDetalleEntity.getReferencia());
            processStored.setParameter("distanciaKm", informacionBasicaDetalleEntity.getDistanciaKm());
            processStored.setParameter("tiempo", informacionBasicaDetalleEntity.getTiempo());
            processStored.setParameter("medioTransporte", informacionBasicaDetalleEntity.getMedioTransporte());
            processStored.setParameter("descripcion", informacionBasicaDetalleEntity.getDescripcion());
            processStored.setParameter("rio", informacionBasicaDetalleEntity.getRio());
            processStored.setParameter("quebrada", informacionBasicaDetalleEntity.getQuebrada());
            processStored.setParameter("laguna", informacionBasicaDetalleEntity.getLaguna());
            processStored.setParameter("unidadFisiografica", informacionBasicaDetalleEntity.getUnidadFisiografica());
            processStored.setParameter("valor", informacionBasicaDetalleEntity.getValor());
            processStored.setParameter("areaHa", informacionBasicaDetalleEntity.getAreaHa());
            processStored.setParameter("areaHaPorcentaje", informacionBasicaDetalleEntity.getAreaHaPorcentaje());
            processStored.setParameter("actividad", informacionBasicaDetalleEntity.getActividad());
            processStored.setParameter("especieExtraida", informacionBasicaDetalleEntity.getEspecieExtraida());
            processStored.setParameter("observaciones", informacionBasicaDetalleEntity.getObservaciones());
            processStored.setParameter("conflicto", informacionBasicaDetalleEntity.getConflicto());
            processStored.setParameter("solucion", informacionBasicaDetalleEntity.getSolucion());
            processStored.setParameter("idInfBasica", informacionBasicaDetalleEntity.getIdInfBasica());
            processStored.setParameter("idFauna", informacionBasicaDetalleEntity.getIdFauna());
            processStored.setParameter("idInfraestructura", informacionBasicaDetalleEntity.getIdInfraestructura());
            processStored.setParameter("idTipoBosque", informacionBasicaDetalleEntity.getIdTipoBosque());
            processStored.setParameter("idUsuarioRegistro", informacionBasicaDetalleEntity.getIdUsuarioRegistro());
            processStored.setParameter("coordenadaUtm", informacionBasicaDetalleEntity.getCoordenadaUtm());             

            processStored.execute();
            
            result.setSuccess(true);
            result.setMessage("Se registró la informacion basica.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<InformacionBasicaDetalleEntity> ListarInformacionBasicaDetalle(Integer idPlanManejo, Integer idInfBasica) throws Exception {
        
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaDetalle_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("idInfBasica", idInfBasica);     
            sp.execute();
            List<InformacionBasicaDetalleEntity> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();
            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                InformacionBasicaDetalleEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new InformacionBasicaDetalleEntity();
                    temp.setIdInfBasicaDet(item[0]==null?null:(Integer) item[0]);
                    temp.setCodSubInfBasicaDet(item[1]==null?null:(String) item[1]);
                    temp.setCodInfBasicaDet(item[2]==null?null:(String) item[2]);
                    temp.setPuntoVertice(item[3]==null?null:(String) item[3]);
                    temp.setCoordenadaEsteIni(item[4]==null?null:(BigDecimal) item[4]);
                    temp.setCoordenadaNorteIni(item[5]==null?null:(BigDecimal) item[5]);
                    temp.setCoordenadaEsteFin(item[6]==null?null:(BigDecimal) item[6]);
                    temp.setCoordenadaNorteFin(item[7]==null?null:(BigDecimal) item[7]);
                    temp.setReferencia(item[8]==null?null:(String) item[8]);
                    temp.setDistanciaKm(item[9]==null?null:(BigDecimal) item[9]);
                    temp.setTiempo(item[10]==null?null:(BigDecimal) item[10]);
                    temp.setMedioTransporte(item[11]==null?null:(String) item[11]);
                    temp.setDescripcion(item[12]==null?null:(String) item[12]);
                    temp.setRio(item[13]==null?null:(String) item[13]);
                    temp.setQuebrada(item[14]==null?null:(String) item[14]);
                    temp.setLaguna(item[15]==null?null:(String) item[15]);
                    temp.setUnidadFisiografica(item[16]==null?null:(String) item[16]);
                    temp.setValor(item[17]==null?null:(String) item[17]);
                    temp.setAreaHa(item[18]==null?null:(BigDecimal) item[18]);
                    temp.setAreaHaPorcentaje(item[19]==null?null:(BigDecimal) item[19]);
                    temp.setActividad(item[20]==null?null:(String) item[20]);
                    temp.setEspecieExtraida(item[21]==null?null:(String) item[21]);
                    temp.setObservaciones(item[22]==null?null:(String) item[22]);
                    temp.setConflicto(item[23]==null?null:(String) item[23]);
                    temp.setSolucion(item[24]==null?null:(String) item[24]);
                    temp.setCoordenadaUtm(item[25]==null?null:(String) item[25]);
                    resultDet.add(temp);
                }
            }

            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }    


    @Override
    public ResultClassEntity ActualizarInformacionBasicaDetalle(InformacionBasicaDetalleEntity informacionBasicaDetalleEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasicaDetalle_Actualizar");
            processStored.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codInfBasicaDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubInfBasicaDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("puntoVertice", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaEsteIni", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaNorteIni", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaEsteFin", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaNorteFin", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("distanciaKm", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tiempo", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medioTransporte", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("rio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("quebrada", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("laguna", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("unidadFisiografica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("valor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHa", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHaPorcentaje", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("especieExtraida", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conflicto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solucion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idFauna", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfraestructura", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaUtm", String.class, ParameterMode.IN);


            setStoreProcedureEnableNullParameters(processStored);            

            processStored.setParameter("idInfBasicaDet", informacionBasicaDetalleEntity.getIdInfBasicaDet());
            processStored.setParameter("codInfBasicaDet", informacionBasicaDetalleEntity.getCodInfBasicaDet());
            processStored.setParameter("codSubInfBasicaDet", informacionBasicaDetalleEntity.getCodSubInfBasicaDet());
            processStored.setParameter("puntoVertice", informacionBasicaDetalleEntity.getPuntoVertice());
            processStored.setParameter("coordenadaEsteIni", informacionBasicaDetalleEntity.getCoordenadaEsteIni());
            processStored.setParameter("coordenadaNorteIni", informacionBasicaDetalleEntity.getCoordenadaNorteIni());
            processStored.setParameter("coordenadaEsteFin", informacionBasicaDetalleEntity.getCoordenadaEsteFin());
            processStored.setParameter("coordenadaNorteFin", informacionBasicaDetalleEntity.getCoordenadaNorteFin());
            processStored.setParameter("referencia", informacionBasicaDetalleEntity.getReferencia());
            processStored.setParameter("distanciaKm", informacionBasicaDetalleEntity.getDistanciaKm());
            processStored.setParameter("tiempo", informacionBasicaDetalleEntity.getTiempo());
            processStored.setParameter("medioTransporte", informacionBasicaDetalleEntity.getMedioTransporte());
            processStored.setParameter("descripcion", informacionBasicaDetalleEntity.getDescripcion());
            processStored.setParameter("rio", informacionBasicaDetalleEntity.getRio());
            processStored.setParameter("quebrada", informacionBasicaDetalleEntity.getQuebrada());
            processStored.setParameter("laguna", informacionBasicaDetalleEntity.getLaguna());
            processStored.setParameter("unidadFisiografica", informacionBasicaDetalleEntity.getUnidadFisiografica());
            processStored.setParameter("valor", informacionBasicaDetalleEntity.getValor());
            processStored.setParameter("areaHa", informacionBasicaDetalleEntity.getAreaHa());
            processStored.setParameter("areaHaPorcentaje", informacionBasicaDetalleEntity.getAreaHaPorcentaje());
            processStored.setParameter("actividad", informacionBasicaDetalleEntity.getActividad());
            processStored.setParameter("especieExtraida", informacionBasicaDetalleEntity.getEspecieExtraida());
            processStored.setParameter("observaciones", informacionBasicaDetalleEntity.getObservaciones());
            processStored.setParameter("conflicto", informacionBasicaDetalleEntity.getConflicto());
            processStored.setParameter("solucion", informacionBasicaDetalleEntity.getSolucion());
            processStored.setParameter("idInfBasica", informacionBasicaDetalleEntity.getIdInfBasica());
            processStored.setParameter("idFauna", informacionBasicaDetalleEntity.getIdFauna());
            processStored.setParameter("idInfraestructura", informacionBasicaDetalleEntity.getIdInfraestructura());
            processStored.setParameter("idTipoBosque", informacionBasicaDetalleEntity.getIdTipoBosque());
            processStored.setParameter("idUsuarioModificacion", informacionBasicaDetalleEntity.getIdUsuarioModificacion());
            processStored.setParameter("coordenadaUtm", informacionBasicaDetalleEntity.getCoordenadaUtm());             

            processStored.execute();
            
            result.setSuccess(true);
            result.setMessage("Se actualizó la informacion basica detalle.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }
    
    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }
}
