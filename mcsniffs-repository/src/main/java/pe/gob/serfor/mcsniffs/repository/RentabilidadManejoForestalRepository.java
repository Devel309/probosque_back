package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;

import java.util.List;

public interface RentabilidadManejoForestalRepository {
    List<Dropdown> ComboRubroRentabilidadForestal(ParametroEntity param) throws Exception;
    ResultClassEntity RegistrarRentabilidadManejoForestal(List<RentabilidadManejoForestalDto> list)throws Exception;
    ResultClassEntity ListarRentabilidadManejoForestal(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarRentabilidadManejoForestal(RentabilidadManejoForestalEntity param)throws Exception;
    ResultArchivoEntity descargarFormato();
    ResultClassEntity ListarRentabilidadManejoForestalTitular(PlanManejoEntity param)throws Exception;
}
