package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoInfraccionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoInfraccionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EvaluacionCampoInfraccionRepositoryImpl extends JdbcDaoSupport implements EvaluacionCampoInfraccionRepository {
        @Autowired
        @Qualifier("dataSourceBDMCSNIFFS")
        DataSource dataSource;
        private static final Logger log = LogManager.getLogger(EvaluacionCampoInfraccionRepositoryImpl.class);
        @PersistenceContext
        private EntityManager em;

        @PostConstruct
        private void initialize(){
            setDataSource(dataSource);
        }

    @Override
    public ResultClassEntity ObtenerEvaluacionCampoInfraccion(EvaluacionCampoInfraccionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_EvaluacionCampoInfraccion_Obtener");
            sp.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            sp.setParameter("idEvaluacionCampo", param.getIdEvaluacionCampo());
            sp.execute();
            List<EvaluacionCampoInfraccionDto> list = new ArrayList<EvaluacionCampoInfraccionDto>();
            List<Object[]> spResult =sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    EvaluacionCampoInfraccionDto obj = new EvaluacionCampoInfraccionDto();
                    obj.setIdEvaluacionCampoInfraccion((Integer) row[0]);
                    obj.setIdEvaluacionCampo((Integer) row[1]);
                    obj.setIdInfraccion((Integer) row[2]);
                    obj.setInfraccion((String) row[3]);
                    obj.setCalificacion((String) row[4]);
                    obj.setSancionNoMonetaria((String) row[5]);
                    obj.setSancionMonetaria((String) row[6]);
                    obj.setSubsanable((String) row[7]);
                    obj.setEstado((String) row[8]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity RegistrarEvaluacionCampoInfraccion(List<EvaluacionCampoInfraccionDto> list) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            EvaluacionCampoInfraccionDto param = new EvaluacionCampoInfraccionDto();
            param =list.get(0);
            StoredProcedureQuery processStored_ = em.createStoredProcedureQuery("dbo.pa_EvaluacionCampoInfraccion_Inactivar");
            processStored_.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            processStored_.setParameter("idEvaluacionCampo", param.getIdEvaluacionCampo());
            processStored_.execute();
            List<EvaluacionCampoInfraccionDto> list_ = new ArrayList<EvaluacionCampoInfraccionDto>() ;
            for(EvaluacionCampoInfraccionDto p:list){

                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_EvaluacionCampoInfraccion_Registrar");
                processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idEvaluacionCampoInfraccion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idInfraccion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("infraccion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("calificacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("sancionNoMonetaria", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("sancionMonetaria", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("subsanable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
                processStored.setParameter("idEvaluacionCampoInfraccion", p.getIdEvaluacionCampoInfraccion());
                processStored.setParameter("idInfraccion", p.getIdInfraccion());
                processStored.setParameter("infraccion", p.getInfraccion());
                processStored.setParameter("calificacion", p.getCalificacion());
                processStored.setParameter("sancionNoMonetaria", p.getSancionNoMonetaria());
                processStored.setParameter("sancionMonetaria", p.getSancionMonetaria());
                processStored.setParameter("subsanable", p.getSubsanable());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
//                List<Object[]> spResult =processStored.getResultList();
//                if (spResult.size() >= 1) {
//                    for (Object[] row : spResult) {
//                        EvaluacionCampoInfraccionDto obj = new EvaluacionCampoInfraccionDto();
//                        obj.setIdEvaluacionCampo((Integer) row[0]);
//                        obj.setIdEvaluacionCampoInfraccion((Integer) row[1]);
//                        obj.setIdInfraccion((Integer) row[2]);
//                        obj.setInfraccion((String) row[3]);
//                        obj.setCalificacion((String) row[4]);
//                        obj.setSancionNoMonetaria((String) row[5]);
//                        obj.setSancionMonetaria((String) row[6]);
//                        obj.setSubsanable((String) row[7]);
//                        list_.add(obj);
//                    }
//                }
            }
          //  result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el cuadro de infracciones y sanciones de materia forestal correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


}
