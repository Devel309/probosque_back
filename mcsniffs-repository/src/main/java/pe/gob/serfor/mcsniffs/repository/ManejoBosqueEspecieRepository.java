package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqEntity;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueEspecieRepository {
    ResultClassEntity RegistrarManejoBosqueEspecie(List<ManejoBosqueEspecieEntity> manejoBosqueEspecie) throws Exception;
    ResultClassEntity EliminarManejoBosqueEspecie(ManejoBosqueEspecieEntity manejoBosqueEspecie) throws Exception;
    ResultClassEntity ListarManejoBosqueEspecie(ManejoBosqEntity manejoBosqueEspecie) throws Exception;
}
