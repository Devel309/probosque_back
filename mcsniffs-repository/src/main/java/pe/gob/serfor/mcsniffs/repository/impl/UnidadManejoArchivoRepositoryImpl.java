package pe.gob.serfor.mcsniffs.repository.impl;

import org.springframework.beans.factory.annotation.Value;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.OrdenamientoAreaManejoDto;
import pe.gob.serfor.mcsniffs.repository.UnidadManejoArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class UnidadManejoArchivoRepositoryImpl extends JdbcDaoSupport implements UnidadManejoArchivoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(OrdenamientoAreaManejoRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    @Autowired
    private FileServerConexion fileServerConexion;
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista categoria de ordenamiento de Parametos}
     * @param:ParametroEntity
     */

    @Override
    public ResultClassEntity RegistrarUnidadManejoArchivo(UnidadManejoArchivoEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoAreaManejoEntity> list = new ArrayList<OrdenamientoAreaManejoEntity>();
        ArchivoEntity archivo = new ArchivoEntity();

        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_UnidadManejoArchivo_Registrar");

            processStored_.registerStoredProcedureParameter("idUnidadManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idTipo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idUnidadManejo",p.getIdUnidadManejo());
            processStored_.setParameter("idTipo", p.getIdTipo());
            processStored_.setParameter("idArchivo", p.getIdArchivo());
            processStored_.setParameter("estado", p.getEstado());
            processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored_.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }

        result.setSuccess(true);
        result.setMessage("Se registró unidad de manejo archivo.");
        result.setData(archivo);
        return  result;
    }


    @Override
    public ResultClassEntity EliminarUnidadManejoArchivo(UnidadManejoArchivoEntity unidadManejoArchivoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_UnidadManejoArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idUnidadArchivoManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idUnidadArchivoManejo", unidadManejoArchivoEntity.getIdUnidadArchivoManejo());
            processStored.setParameter("idUsuarioElimina", unidadManejoArchivoEntity.getIdUsuarioElimina());

            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó registro correctamente.");
        return  result;
    }


    @Override
    public ResultClassEntity ListarUnidadManejoArchivo(
        UnidadManejoArchivoEntity request) {
        ResultClassEntity result = new ResultClassEntity();
            List<UnidadManejoArchivoEntity> objList=new ArrayList<UnidadManejoArchivoEntity>();
            String sql = "EXEC pa_UnidadManejoArchivo_Obtener " + request.getIdUnidadManejo();

                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
                 
                    for(Map<String, Object> row:rows) {
                        UnidadManejoArchivoEntity obj=new UnidadManejoArchivoEntity();
                        obj.setIdUnidadArchivoManejo((Integer)row.get("NU_ID_UNIDADARCHIVOMANEJO")) ;
                        obj.setIdUnidadManejo((Integer)row.get("NU_ID_UNIDAD_MANEJO")) ;
                        obj.setIdArchivo((Integer)row.get("NU_ID_ARCHIVO")) ;
                        obj.setRuta((String)row.get("TX_RUTA")) ;
                        obj.setNombre((String)row.get("TX_NOMBRE")) ;
                        obj.setExtension((String)row.get("TX_EXTENSION")) ;

                        String nombreArchivoGenerado = ((String)row.get("TX_NOMBRE_GENERADO"));
                        byte[] byteFile =fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                        obj.setFile(byteFile);
                        obj.setContenType("application/octet-stream");

                        objList.add(obj);
                    }
        result.setData(objList);
        return result;
    }


}
