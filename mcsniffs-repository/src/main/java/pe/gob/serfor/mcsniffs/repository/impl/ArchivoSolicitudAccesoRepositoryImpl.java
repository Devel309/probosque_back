package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.ArchivoSolicitudAccesoRepository;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Victor Matos [14-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar y obtener una solicitud de archivo}
 *
 */

@Repository
public class ArchivoSolicitudAccesoRepositoryImpl extends JdbcDaoSupport implements ArchivoSolicitudAccesoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ArchivoSolicitudAccesoRepositoryImpl.class);

    @Value("${smb.file.server.path}")
    private String fileServerPath;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Autowired
    private ArchivoRepository archivo;
    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Registra informacion del archivo cargado al file server}
     * @param: Nombre de archivo y numero de solicitud asociado
     */
    @Override
    public ResultClassEntity<ArchivoEntity> insertarArchivoSolicitudAcceso(String archivoGenerado,
            String archivoArchivo, Integer idSolicitudAcceso, String tipoDocumento) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoEntity entity = new ArchivoEntity();
        try {
            ArchivoEntity entityArchivo = new ArchivoEntity();
            entityArchivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
            entityArchivo.setNombre(FilenameUtils.getBaseName(archivoArchivo));
            entityArchivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
            entityArchivo.setRuta(fileServerPath.concat(archivoGenerado));

            ArchivoSolicitudAccesoEntity entitySolicitudAccesoEntity = new ArchivoSolicitudAccesoEntity();
            entitySolicitudAccesoEntity.setIdArchivoSolicitud(idSolicitudAcceso);

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_archivoSolicitudAcceso_Registrar");
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoSolicitudAcceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.setParameter("ruta", entityArchivo.getRuta());
            processStored.setParameter("nombre", entityArchivo.getNombre());
            processStored.setParameter("nombreGenerado", entityArchivo.getNombreGenerado());
            processStored.setParameter("extension", entityArchivo.getExtension());
            processStored.setParameter("tipoDocumento", tipoDocumento);
            processStored.setParameter("idArchivoSolicitudAcceso", idSolicitudAcceso);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo((Integer) row[0]);
                    entity.setRuta((String) row[1]);
                    entity.setNombre((String) row[2]);
                    entity.setNombreGenerado((String) row[3]);
                    entity.setExtension((String) row[4]);
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se insertar archivo solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al insertar archivo solicitud.");
            return result;
        }
    }

    /**
     * @autor: Victor Matos [14-07-2021]
     * @modificado:
     * @descripción: {Obtiene la informacion del archivo registrado}
     * @param: numero de archivo asociado
     */
    @Override
    public ResultClassEntity<ArchivoEntity> obtenerArchivoSolicitudAcceso(Integer idSolicitudAcceso) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoEntity entity = new ArchivoEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_archivoSolicitudAcceso_ObtenerPorCodigo");
            processStored.registerStoredProcedureParameter("idSolicitudAcceso", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitudAcceso", idSolicitudAcceso);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    entity.setIdArchivo(Integer.parseInt(row[0].toString()));
                    entity.setRuta(String.valueOf(row[1]));
                    entity.setNombre(String.valueOf(row[2]));
                    entity.setNombreGenerado(String.valueOf(row[3]));
                    entity.setExtension(String.valueOf(row[4]));
                    break;
                }
            }
            result.setData(entity);
            result.setSuccess(true);
            result.setMessage("Se obtiene el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al obtener el archivo.");
            return result;
        }
    }

    /**
     * @autor: Abner Valdez [18-11-2021]
     * @modificado:
     * @descripción: {registrar detalle archivo}
     * @param:ArchivoSolicitudEntity
     */
    @Override
    public ResultClassEntity registrarDetalleArchivo(ArchivoSolicitudEntity item) {
        ResultClassEntity result = new ResultClassEntity();
        ArchivoSolicitudEntity entity = new ArchivoSolicitudEntity();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_archivosolicituddetalle_Registrar");
            processStored.registerStoredProcedureParameter("idArchivo", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitud", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", int.class, ParameterMode.IN);
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("idSolicitud", item.getIdSolicitud());
            processStored.setParameter("idUsuario", item.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se inserta el  archivo solicitud detalle.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Error al insertar archivo solicitud detalle.");
            return result;
        }
    }
    /**
     * @autor: Abner Valdez [18-11-2021]
     * @modificado:
     * @descripción: {Listar detalle archivo por idSolicitud}
     * @param:idSolicitud
     */
    @Override
    public ResultEntity<ArchivoSolicitudEntity> listarDetalleArchivo(Integer idSolicitud) {
        ResultEntity<ArchivoSolicitudEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery ps = entityManager.createStoredProcedureQuery("dbo.pa_archivosolicitudDetalle_Listar");
            ps.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            ps.setParameter("idSolicitud", idSolicitud);
            ps.execute();
            List<Object[]> spResult = ps.getResultList();
            List<ArchivoSolicitudEntity> lstresult = new ArrayList<>();
            if (!spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ArchivoSolicitudEntity item = new ArchivoSolicitudEntity();
                    item.setIdArchivoSolicitud((Integer) row[0]);
                    item.setIdSolicitud((Integer) row[1]);
                    item.setIdArchivo((Integer) row[2]);
                    item.setNombreArchivo((String) row[3]);
                    if (!item.getIdArchivo().equals(null)) {
                        ResultClassEntity<ArchivoEntity> file = archivo.obtenerArchivo(item.getIdArchivo());
                        item.setFile(file.getData().getFile());
                    }
                    lstresult.add(item);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron correctamente.");
        } catch (Exception e) {
            result.setData(null);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;
    }
    /**
     * @autor: Abner Valdez [18-11-2021]
     * @modificado:
     * @descripción: {eliminar detalle archivo}
     * @param:Integer idArchivo, Integer idUsuario
     */
    @Override
    public ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception {
        ResultClassEntity<Integer> result = new ResultClassEntity<>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_archivoSolicitudDetalle_Eliminar");
            sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            sp.setParameter("idArchivo", idArchivo);
            sp.setParameter("idUsuario", idUsuario);
            sp.execute();
            result.setData(idArchivo);
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
        } catch (Exception e) {
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;
    }
}