package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;

public interface OtorgamientoRepository {
    ResultClassEntity registrarOtorgamiento(OtorgamientoEntity obj) throws Exception;
    ResultClassEntity listarOtorgamiento(OtorgamientoEntity filtro) throws Exception;
    ResultClassEntity eliminarOtorgamiento(OtorgamientoEntity obj) throws Exception;
    ResultClassEntity listarPorFiltroOtorgamiento(String nroDocumento, String nroRucEmpresa, Integer idPlanManejo) throws Exception;
}
