package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;
import pe.gob.serfor.mcsniffs.repository.NotificacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class NotificacionRepositoryImpl extends JdbcDaoSupport implements NotificacionRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(NotificacionRepositoryImpl.class);

   @Override
    public ResultClassEntity registrarNotificacion(NotificacionEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Notificacion_Registrar");
            processStored.registerStoredProcedureParameter("idNotificacion", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("codigoDocgestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numDocgestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("mensaje", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoMensaje", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoPerfil", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cantidadDias", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("url", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("usuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idNotificacion", obj.getIdNotificacion());
            processStored.setParameter("codigoDocgestion", obj.getCodigoDocgestion());
            processStored.setParameter("numDocgestion", obj.getNumDocgestion());
            processStored.setParameter("mensaje", obj.getMensaje());
            processStored.setParameter("codigoTipoMensaje", obj.getCodigoTipoMensaje());
            processStored.setParameter("idUsuario", obj.getIdUsuario());
            processStored.setParameter("codigoPerfil", obj.getCodigoPerfil());
            processStored.setParameter("fechaInicio", obj.getFechaInicio());
            processStored.setParameter("cantidadDias", obj.getCantidadDias());
            processStored.setParameter("url", obj.getUrl());
            processStored.setParameter("usuarioRegistro", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idNotificacion = (Integer) processStored.getOutputParameterValue("idNotificacion");
            obj.setIdNotificacion(idNotificacion);
            result.setCodigo(idNotificacion);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la actividad Notificación correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la actividad Notificación");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }



    @Override
    public List<NotificacionEntity> listarNotificacion(NotificacionEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Notificacion_Listar");
            processStored.registerStoredProcedureParameter("idNotificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codPerfil", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codGestion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idNotificacion", param.getIdNotificacion());
            processStored.setParameter("codPerfil", param.getCodigoPerfil());
            processStored.setParameter("idUsuario", param.getIdUsuario());
            processStored.setParameter("codGestion", param.getCodigoDocgestion());

            processStored.execute();
            List<NotificacionEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                NotificacionEntity temp = null;
                for (Object[] row: spResult){
                    temp = new NotificacionEntity();
                    temp.setIdNotificacion(row[0]==null?null:(Integer) row[0]);
                    temp.setCodigoDocgestion(row[1]==null?null:(String) row[1]);
                    temp.setNumDocgestion(row[2]==null?null:(Integer) row[2]);
                    temp.setMensaje(row[3]==null?null:(String) row[3]);
                    temp.setCodigoTipoMensaje(row[4]==null?null:(String) row[4]);
                    temp.setIdUsuario(row[5]==null?null:(Integer) row[5]);
                    temp.setCodigoPerfil(row[6]==null?null:(String) row[6]);
                    temp.setFechaInicio(row[7]==null?null:(Timestamp) row[7]);
                    temp.setCantidadDias(row[8]==null?null:(Integer) row[8]);
                    temp.setUrl(row[9]==null?null:(String) row[9]);
                    temp.setEstado(row[10]==null?null:String.valueOf((Character) row[10]));
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }



    @Override
    public ResultClassEntity EliminarNotificacion(NotificacionEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_Notificacion_Eliminar");
        processStored.registerStoredProcedureParameter("idNotificacion", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idNotificacion", param.getIdNotificacion());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }



}