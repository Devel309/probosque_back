package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.AspectoHidrograficoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AspectoHidrograficoRepository;

/**
 * @autor: Carlos Tang [25-08-2021]
 * @modificado:
 * @descripción: {Repository aspecto hidrografico}
 */
@Repository
public class AspectoHidrograficoRepositoryImpl extends JdbcDaoSupport implements AspectoHidrograficoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(UnidadManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_AspectoHidrografico_Registrar");
            processStored.registerStoredProcedureParameter("idAspectoHidrografico", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            
            processStored.setParameter("idAspectoHidrografico", aspectoHidrograficoEntity.getIdAspectoHidrografico());
            processStored.setParameter("idPlanManejo", aspectoHidrograficoEntity.getIdPlanManejo());
            processStored.setParameter("descripcion", aspectoHidrograficoEntity.getDescripcion());
            processStored.setParameter("idUsuarioRegistro", aspectoHidrograficoEntity.getIdUsuarioRegistro());

            processStored.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró aspecto hidrografico");
        return result;
    }

    @Override
    public ResultClassEntity ActualizarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_AspectoHidrografico_Actualizar");
            processStored.registerStoredProcedureParameter("idAspectoHidrografico", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);

            processStored.setParameter("idAspectoHidrografico", aspectoHidrograficoEntity.getIdAspectoHidrografico());
            processStored.setParameter("idPlanManejo", aspectoHidrograficoEntity.getIdPlanManejo());
            processStored.setParameter("descripcion", aspectoHidrograficoEntity.getDescripcion());
            processStored.setParameter("idUsuarioModificacion", aspectoHidrograficoEntity.getIdUsuarioModificacion());
            processStored.setParameter("estado", aspectoHidrograficoEntity.getEstado());

            processStored.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
        result.setSuccess(true);
        result.setMessage("Se actualizó aspecto hidrografico");
        return result;
    }

    @Override
    public ResultClassEntity ListarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_AspectoHidrografico_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", aspectoHidrograficoEntity.getIdPlanManejo());
            sp.execute();
            List<AspectoHidrograficoEntity> list = new ArrayList<AspectoHidrograficoEntity>();
            List<Object[]> spResultDet = sp.getResultList();
            if (spResultDet.size() >= 1) {
                for (Object[] row : spResultDet) {
                         AspectoHidrograficoEntity temp = new AspectoHidrograficoEntity();
                        temp = new AspectoHidrograficoEntity();
                        temp.setIdAspectoHidrografico((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setDescripcion((String) row[2]);
                    list.add(temp);
                    }

            }
            result.setMessage("Se obtuvo los aspectos hidrograficos correctamente.");
            result.setData(list);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public ResultClassEntity EliminarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_AspectoHidrografico_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idAspectoHidrografico", aspectoHidrograficoEntity.getIdAspectoHidrografico());
            processStored.setParameter("idUsuarioElimina", aspectoHidrograficoEntity.getIdUsuarioElimina());
            processStored.execute();

            result.setMessage("Se eliminó el registro correctamente.");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }
}
