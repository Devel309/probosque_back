package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;
import pe.gob.serfor.mcsniffs.repository.FiscalizacionArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class FiscalizacionArchivoRepositoryImpl extends JdbcDaoSupport implements FiscalizacionArchivoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(FiscalizacionArchivoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    

    @Override
    public ResultClassEntity registrarFiscalizacionArchivo(FiscalizacionArchivoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_FiscalizacionArchivo_Registrar");
            processStored.registerStoredProcedureParameter("idFiscalizacionArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idFiscalizacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);            
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idFiscalizacionArchivoOut", Integer.class, ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idFiscalizacionArchivo", dto.getIdFiscalizacionArchivo());
            processStored.setParameter("idFiscalizacion", dto.getIdFiscalizacion());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("asunto", dto.getDescripcion());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();

            Integer idFiscalizacionArchivoOut = (Integer) processStored.getOutputParameterValue("idFiscalizacionArchivoOut");
            result.setCodigo(idFiscalizacionArchivoOut);

            result.setSuccess(true);
            result.setMessage("Se registró fiscalización archivo correctamente.");

            LogAuditoriaEntity logAuditoriaEntity;

            if(dto.getIdFiscalizacionArchivo() == 0 || dto.getIdFiscalizacionArchivo()==null) {
                logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MVD_FISCALIZACION_ARCHIVO,
                        LogAuditoria.REGISTRAR,
                        LogAuditoria.DescripcionAccion.Registrar.T_MVD_FISCALIZACION_ARCHIVO + idFiscalizacionArchivoOut,
                        dto.getIdUsuarioRegistro());



            }else {

                logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MVD_FISCALIZACION_ARCHIVO,
                        LogAuditoria.ACTUALIZAR,
                        LogAuditoria.DescripcionAccion.Actualizar.T_MVD_FISCALIZACION_ARCHIVO + dto.getIdFiscalizacionArchivo(),
                        dto.getIdUsuarioRegistro());

            }

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

    @Override
    public List<FiscalizacionArchivoDto> listarFiscalizacionArchivo(FiscalizacionArchivoDto obj) {
        
        List<FiscalizacionArchivoDto> list = new ArrayList<FiscalizacionArchivoDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_FiscalizacionArchivo_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("idFiscalizacionArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idFiscalizacion", Integer.class, ParameterMode.IN);
            
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idFiscalizacionArchivo", obj.getIdFiscalizacionArchivo());
            processStored.setParameter("idFiscalizacion", obj.getIdFiscalizacion()); 

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    FiscalizacionArchivoDto temp = new FiscalizacionArchivoDto();
                    temp.setIdFiscalizacionArchivo(row[0]==null?null:(Integer) row[0]);
                    temp.setIdFiscalizacion(row[1]==null?null:(Integer) row[1]);
                    temp.setAsunto(row[2]==null?null:(String) row[2]);
                    temp.setIdArchivo(row[3]==null?null:(Integer) row[3]);
                    temp.setDescripcion(row[4]==null?null:(String) row[4]);
                    temp.setTipoDocumento(row[5]==null?null:(String) row[5]);
                    temp.setNombre(row[6]==null?null:(String) row[6]);
                    temp.setEstado(row[7]==null?null:(String) row[7]);                
                    list.add(temp);
                }
            }
            
            return list;
        } catch (Exception e) {
            log.error("ListarFiscalizacionArchivo", e.getMessage());
            throw e;
        }
    
    
    }

    @Override
    public ResultClassEntity eliminarFiscalizacionArchivo(FiscalizacionArchivoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_FiscalizacionArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó fiscalización archivo correctamente.");

            
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }
}
