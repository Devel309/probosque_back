package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;

public interface SolicitudBosqueLocalArchivoRepository {

    ResultClassEntity listarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj);
    ResultClassEntity listarSolicitudBosqueLocalArchivoPaginado(SolicitudBosqueLocalArchivoEntity obj);
    ResultClassEntity registrarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj);
    ResultClassEntity actualizarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj);
    ResultClassEntity eliminarSolicitudBosqueLocalArchivo(SolicitudBosqueLocalArchivoEntity obj);
}
