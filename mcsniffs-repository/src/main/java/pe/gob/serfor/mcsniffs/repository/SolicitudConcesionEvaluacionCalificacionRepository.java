package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface SolicitudConcesionEvaluacionCalificacionRepository {

    ResultClassEntity listarSolicitudConcesionEvaluacionCalificacion(
            SolicitudConcesionEvaluacionCalificacionDto obj);

    ResultClassEntity registrarSolicitudConcesionEvaluacionCalificacion(
            SolicitudConcesionEvaluacionCalificacionDto obj);

    ResultClassEntity actualizarSolicitudConcesionEvaluacionCalificacion(
            SolicitudConcesionEvaluacionCalificacionDto obj);
}
