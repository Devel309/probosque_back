package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;
import pe.gob.serfor.mcsniffs.repository.AreaSistemaPlantacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class AreaSistemaPlantacionRepositoryImpl extends JdbcDaoSupport implements AreaSistemaPlantacionRepository{
    
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AreaSistemaPlantacionRepositoryImpl.class);

    @Override
    public ResultClassEntity registrarAreaSistemaPlantacion(AreaSistemaPlantacionDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AreaSistemaPlantacion_Registrar");
            processStored.registerStoredProcedureParameter("idAreaSistemaPlantacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForestArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUm", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cantidad", BigDecimal.class, ParameterMode.IN);       
            processStored.registerStoredProcedureParameter("fines", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idAreaSistemaPlantacion", obj.getIdAreaSistemaPlantacion());
            processStored.setParameter("idSolPlantForestArea", obj.getIdSolPlantForestArea());
            processStored.setParameter("idSolPlantForest", obj.getIdSolPlantForest());
            processStored.setParameter("nombre", obj.getNombre());
            processStored.setParameter("codigoUm", obj.getCodigoUm());
            processStored.setParameter("cantidad", obj.getCantidad());      
            processStored.setParameter("fines", obj.getFines());
            processStored.setParameter("idUsuario", obj.getIdUsuario());
       
            processStored.execute();
            //Integer id = (Integer) processStored.getOutputParameterValue("idAreaSistemaPlantacion");
            //obj.setIdAreaSistemaPlantacion(id);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage(obj.getIdAreaSistemaPlantacion()!=null?"Se Actualizó Área Sistema Plantación correctamente.":"Se registró Área Sistema Plantación correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    public ResultClassEntity listarAreaSistemaPlantacion(AreaSistemaPlantacionDto filtro){
        List<AreaSistemaPlantacionDto> lista = new ArrayList<AreaSistemaPlantacionDto>();
        ResultClassEntity result = new ResultClassEntity();
        try {
         StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_AreaSistemaPlantacion_ListarFiltro");

            sp.registerStoredProcedureParameter("idAreaSistemaPlantacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolPlantForestArea", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            

            sp.setParameter("idAreaSistemaPlantacion", filtro.getIdAreaSistemaPlantacion());
            sp.setParameter("idSolPlantForestArea", filtro.getIdSolPlantForestArea());
            sp.setParameter("idSolPlantForest", filtro.getIdSolPlantForest());
            sp.execute();
            AreaSistemaPlantacionDto c = null;
            List<Object[]> spResult = sp.getResultList();
 
            result.setTotalRecord(0);
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new AreaSistemaPlantacionDto();
 
 
                    c.setIdAreaSistemaPlantacion((Integer) row[0]);
                    c.setIdSolPlantForestArea((Integer) row[1]);
                    c.setNombre((String) row[2]);
                    c.setCodigoUm((String) row[3]);
                    c.setCantidad((BigDecimal) row[4]);
                    c.setFines((String) row[5]);

                    //result.setTotalRecord((Integer) row[6]);
                    lista.add(c);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
            return result;
        } catch (Exception e) {
            log.error("listar Area Sistema Plantacion", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity eliminarAreaSistemaPlantacion(AreaSistemaPlantacionDto p) {

        ResultClassEntity result = new ResultClassEntity();

        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AreaSistemaPlantacion_Eliminar");
            processStored.registerStoredProcedureParameter("idAreaSistemaPlantacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idAreaSistemaPlantacion", p.getIdAreaSistemaPlantacion());
            processStored.setParameter("idUsuarioElimina", p.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarEspecieForestalSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto param)  throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try
        {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("pa_SistemaPlantacionEspecie_ListarEspecieForestal");
            sp.registerStoredProcedureParameter("idEspecie", Short.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("autor", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("IdAreaSistemaPlantacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("IdSistemaPlantacionEspecie", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idEspecie", param.getIdEspecie());
            sp.setParameter("nombreComun", param.getNombreComun());
            sp.setParameter("nombreCientifico", param.getNombreCientifico());
            sp.setParameter("autor", param.getAutor());
            sp.setParameter("familia", param.getFamilia());
            sp.setParameter("IdAreaSistemaPlantacion", param.getIdAreaSistemaPlantacion());
            sp.setParameter("IdSistemaPlantacionEspecie", param.getIdSistemaPlantacionEspecie());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());

            sp.execute();


            List<EspecieForestalSistemaPlantacionEspecieDto> objList=new ArrayList<EspecieForestalSistemaPlantacionEspecieDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult.size() >= 1){
                EspecieForestalSistemaPlantacionEspecieDto objOrdPro = null;
                for (Object[] row: spResult){
                    objOrdPro = new EspecieForestalSistemaPlantacionEspecieDto();
                    objOrdPro.setIdAreaSistemaPlantacion((Integer) row[0]);
                    objOrdPro.setIdSistemaPlantacionEspecie((Integer) row[1]);
                    objOrdPro.setIdEspecie((Integer) row[2]);
                    objOrdPro.setNombreComun((String) row[3]);
                    objOrdPro.setNombreCientifico((String) row[4]);
                    objOrdPro.setAutor(row[5]==null?null:(String) row[5]);
                    objOrdPro.setFamilia(row[6]==null?null:(String) row[6]);

                    result.setTotalRecord((Integer) row[7]);
                    objList.add(objOrdPro);
                }
            }

            result.setData(objList);
            result.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            result.setSuccess(true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrio un error");
            result.setSuccess(false);
        }
        return result;
    }


    @Override
    public ResultClassEntity eliminarSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto param) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SistemaPlantacionEspecie_Eliminar");
            processStored.registerStoredProcedureParameter("idSistemaPlantacionEspecie", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSistemaPlantacionEspecie", param.getIdSistemaPlantacionEspecie());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarSistemaPlantacionEspecie(EspecieForestalSistemaPlantacionEspecieDto param) throws Exception{
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SistemaPlantacionEspecie_Guardar");
            processStored.registerStoredProcedureParameter("idAreaSistemaPlantacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEspecie", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSistemaPlantacionEspecie", Integer.class, ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idAreaSistemaPlantacion", param.getIdAreaSistemaPlantacion());
            processStored.setParameter("idEspecie", param.getIdEspecie());
            processStored.setParameter("nombreComun", param.getNombreComun());
            processStored.setParameter("nombreCientifico", param.getNombreCientifico());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());

            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idSistemaPlantacionEspecie");
            param.setIdSistemaPlantacionEspecie(id);

            result.setSuccess(true);
            result.setData(param);
            result.setMessage(param.getIdAreaSistemaPlantacion()!=null?"Se Actualizó Área Sistema Plantación correctamente.":"Se registró Área Sistema Plantación correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


}
