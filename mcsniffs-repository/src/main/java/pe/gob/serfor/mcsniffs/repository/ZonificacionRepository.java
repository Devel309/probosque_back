package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ZonificacionDTO;

import java.util.List;

public interface ZonificacionRepository {
    
    ResultClassEntity ListarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception;
    ResultClassEntity listarZona(ZonaEntity item) throws Exception;
    ResultClassEntity listarZonaAnexo(Integer idZona) throws Exception;
    Integer registrarZona(ZonaEntity item) throws Exception;
    Integer registrarZonaAnexo(ZonaAnexoEntity item) throws Exception;
    ResultClassEntity eliminarZona(ZonaEntity item) throws Exception;
    ResultClassEntity RegistrarZonificacion(List<ZonificacionDTO> zonificacionDTOList) throws Exception;
    ResultClassEntity RegistraZonificacion(ZonificacionEntity zonificacionEntity) throws Exception;
    ResultClassEntity ActualizarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception;
    ResultClassEntity EliminarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception;

}
