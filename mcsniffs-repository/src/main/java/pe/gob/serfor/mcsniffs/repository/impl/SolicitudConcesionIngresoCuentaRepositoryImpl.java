package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionIngresoCuentaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionResponsableRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudConcesionIngresoCuentaRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionIngresoCuentaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionIngresoCuentaRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionIngresoCuentaDto> lista = new ArrayList<SolicitudConcesionIngresoCuentaDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionIngresoCuenta_ListarFiltro");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionIngresoCuenta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionIngresoCuenta", obj.getIdSolicitudConcesionIngresoCuenta());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());

            processStored.execute();

            SolicitudConcesionIngresoCuentaDto c = null;


            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {

                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionIngresoCuentaDto();
                        c.setIdSolicitudConcesionIngresoCuenta(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitudConcesion(row[1] == null ? null : (Integer) row[1]);
                        c.setCodigoTipoDocumento(row[2] == null ? null : (String) row[2]);
                        c.setNumeroTipoDocumento(row[3] == null ? null : (String) row[3]);
                        c.setDescripcion(row[4] == null ? null : (String) row[4]);
                        BigDecimal bigMonto=(BigDecimal) row[5];
                        c.setMonto(bigMonto.doubleValue());

                        lista.add(c);

                    }

                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción listar  Solicitud Concesion Ingreso Cuenta correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarActividadSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción obtener Solicitud Ingreso Cuenta Concesion.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {

                StoredProcedureQuery processStored = entityManager
                        .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionIngresoCuenta_Registrar");

                processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroTipoDocumento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("monto", Double.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idSolicitudConcesionIngresoCuenta", Integer.class, ParameterMode.INOUT);

                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
                processStored.setParameter("codigoTipoDocumento", obj.getCodigoTipoDocumento());
                processStored.setParameter("numeroTipoDocumento", obj.getNumeroTipoDocumento());
                processStored.setParameter("descripcion", obj.getDescripcion());
                processStored.setParameter("monto", obj.getMonto());
                processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

                processStored.execute();


            Integer idSolicitudConcesionIngresoCuenta = (Integer) processStored.getOutputParameterValue("idSolicitudConcesionIngresoCuenta");
            obj.setIdSolicitudConcesionIngresoCuenta(idSolicitudConcesionIngresoCuenta);
            result.setCodigo(idSolicitudConcesionIngresoCuenta);
            result.setSuccess(true);
            //result.setData(obj);
            result.setMessage("Se registró el Ingreso correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el ingreso.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionIngresoCuenta(SolicitudConcesionIngresoCuentaDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionIngresoCuenta_Eliminar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionIngresoCuenta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionIngresoCuenta", obj.getIdSolicitudConcesionIngresoCuenta());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


}
