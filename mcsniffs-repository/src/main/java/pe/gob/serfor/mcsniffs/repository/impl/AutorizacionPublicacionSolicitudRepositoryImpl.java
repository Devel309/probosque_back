package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AutorizacionPublicacionSolicitudRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class AutorizacionPublicacionSolicitudRepositoryImpl extends JdbcDaoSupport implements AutorizacionPublicacionSolicitudRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(AutorizacionPublicacionSolicitudRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity guardarAutorizacionPublicacionSolicitud(AutorizacionPublicacionSolicitudEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        try{

            StoredProcedureQuery processStored;

            if(p.getIdAutorizacionPublicacionSolicitud() <= 0) {
                processStored = entityManager.createStoredProcedureQuery("dbo.pa_AutorizacionPublicacionSolicitud_Registrar");
                processStored.registerStoredProcedureParameter("idAutorizacionPublicacionSolicitud", Integer.class, ParameterMode.OUT);
                processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroDocumento", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fechaAutorizacion", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idProcesoPostulacion", p.getIdProcesoPostulacion());
                processStored.setParameter("numeroDocumento", p.getNumeroDocumento());
                processStored.setParameter("fechaAutorizacion", p.getFechaAutorizacion());
                processStored.setParameter("idUsuario", p.getIdUsuarioRegistro());

                processStored.execute();
                Integer idAutorizacionPublicacion = (Integer) processStored.getOutputParameterValue("idAutorizacionPublicacionSolicitud");
                result.setCodigo(idAutorizacionPublicacion);
                result.setSuccess(true);
                result.setMessage("Se registró la autorización de la publicación correctamente.");

            }
            result.setSuccess(true);
            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultArchivoEntity descargarPlantillaCartaAutorizacion() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/PlantillaFormatoFiscalizacion.docx");
            File fileCopi = File.createTempFile("PlantillaCartaAutorizacion",".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("PlantillaCartaAutorizacion.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargó la Plantilla de Carta de Autorización correctamente.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Autorización Publicación de Solicitud - DescagarPlantillaCartaAutorizacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo descargar la plantilla de carta de autorización");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }



}
