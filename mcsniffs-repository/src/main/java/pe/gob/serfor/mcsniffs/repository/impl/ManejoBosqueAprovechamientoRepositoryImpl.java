package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueAprovechamientoRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class ManejoBosqueAprovechamientoRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueAprovechamientoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueAprovechamientoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueAprovechamiento_Registrar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idManBosqueAprove", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoMetodoAprove", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoTransMaderable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoTransAreaDestino", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("caminoAcceso", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("caminoPrincipal", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("caminoDescripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idManBosqueAprove", param.getIdManBosqueAprove());
            processStored.setParameter("idTipo", param.getIdTipo());
            processStored.setParameter("idTipoMetodoAprove", param.getIdTipoMetodoAprove());
            processStored.setParameter("idTipoTransMaderable", param.getIdTipoTransMaderable());
            processStored.setParameter("idTipoTransAreaDestino", param.getIdTipoTransAreaDestino());
            processStored.setParameter("caminoAcceso", param.getCaminoAcceso());
            processStored.setParameter("caminoPrincipal", param.getCaminoPrincipal());
            processStored.setParameter("descripcion", param.getDescripcion());
            processStored.setParameter("caminoDescripcion", param.getCaminoDescripcion());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();
            String des="";
            if(param.getIdTipo().trim()=="1"){
                des="el Método de Aprovechamiento correctamente.";
            }
            else{
                des="la Infraestructura para Aprovechamiento y Transporte correctamente.";
            }
            result.setMessage("Se registró "+des);
            ManejoBosqueAprovechamientoEntity obj = new ManejoBosqueAprovechamientoEntity() ;
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity plan = new PlanManejoEntity();
                    plan.setIdPlanManejo((Integer) row[0]);
                    obj.setPlanManejo(plan);
                    obj.setIdManBosqueAprove((Integer) row[1]);
                    obj.setIdTipo((String) row[2]);
                    obj.setIdTipoMetodoAprove((String) row[3]);
                    obj.setIdTipoTransMaderable((String) row[4]);
                    obj.setIdTipoTransAreaDestino((String) row[5]);
                    BigDecimal bd=(BigDecimal)row[6];
                    obj.setCaminoAcceso((Double) bd.doubleValue());
                    BigDecimal bd_=(BigDecimal)row[7];
                    obj.setCaminoPrincipal((Double) bd_.doubleValue());
                    obj.setDescripcion((String) row[8]);
                    obj.setCaminoDescripcion((String) row[9]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);

            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ObtenerManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueAprovechamiento_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", manejoBosqueAprovechamiento.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idTipo", manejoBosqueAprovechamiento.getIdTipo());
            processStored.execute();
            ManejoBosqueAprovechamientoEntity obj = new ManejoBosqueAprovechamientoEntity();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity plan = new PlanManejoEntity();
                    plan.setIdPlanManejo((Integer) row[0]);
                    obj.setPlanManejo(plan);
                    obj.setIdManBosqueAprove((Integer) row[1]);
                    obj.setIdTipo((String) row[2]);
                    obj.setIdTipoMetodoAprove((String) row[3]);
                    obj.setIdTipoTransMaderable((String) row[4]);
                    obj.setIdTipoTransAreaDestino((String) row[5]);
                    BigDecimal bd=(BigDecimal)row[6];
                    obj.setCaminoAcceso((Double) bd.doubleValue());
                    BigDecimal bd_=(BigDecimal)row[7];
                    obj.setCaminoPrincipal((Double) bd_.doubleValue());
                    obj.setDescripcion((String) row[8]);
                    obj.setCaminoDescripcion((String) row[9]);

                }
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el manejo de bosque aprocechamiento.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
