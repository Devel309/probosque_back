package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionBasicaDetalle.InformacionBasicaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.PoblacionAledaniaDto;
import pe.gob.serfor.mcsniffs.repository.InformacionBasicaRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

/**
 * @autor: Harry Coa [04-08-2021]
 * @modificado:
 * @descripción: {Repositorio Informacion Basica}
 *
 */
@Repository
public class InformacionBasicaRepositoryImpl extends JdbcDaoSupport implements InformacionBasicaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final String SEPARADOR_ARCHIVO = ".";
    private static final Logger log = LogManager.getLogger(InformacionBasicaRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;
    @Autowired
    private FileServerConexion fileServerConexion;

    @Autowired
    private ArchivoRepositoryImpl respositoryArchivo;
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registrarInformacionBasica(InformacionBasicaEntity informacionBasicaEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasica_Registrar");
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codNombreInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("departamento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("provincia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("distrito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroPc", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaTotal", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroHojaCatastral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreHojaCatastral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idHidrografia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("frenteCorta", String.class, ParameterMode.IN);


            setStoreProcedureEnableNullParameters(processStored);

            processStored.setParameter("idInfBasica", informacionBasicaEntity.getIdInfBasica());
            processStored.setParameter("codInfBasica", informacionBasicaEntity.getCodInfBasica());
            processStored.setParameter("codSubInfBasica", informacionBasicaEntity.getCodSubInfBasica());
            processStored.setParameter("codNombreInfBasica", informacionBasicaEntity.getCodNombreInfBasica());
            processStored.setParameter("departamento", informacionBasicaEntity.getDepartamento());
            processStored.setParameter("provincia", informacionBasicaEntity.getProvincia());
            processStored.setParameter("distrito", informacionBasicaEntity.getDistrito());
            processStored.setParameter("cuenca", informacionBasicaEntity.getCuenca());
            processStored.setParameter("anexo", informacionBasicaEntity.getAnexo());
            processStored.setParameter("idPlanManejo", informacionBasicaEntity.getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", informacionBasicaEntity.getIdUsuarioRegistro());
            processStored.setParameter("numeroPc", informacionBasicaEntity.getNumeroPc());
            processStored.setParameter("areaTotal", informacionBasicaEntity.getAreaTotal());
            processStored.setParameter("nroHojaCatastral", informacionBasicaEntity.getNroHojaCatastral());
            processStored.setParameter("nombreHojaCatastral", informacionBasicaEntity.getNombreHojaCatastral());
            processStored.setParameter("idHidrografia", informacionBasicaEntity.getIdHidrografia());
            processStored.setParameter("frenteCorta", informacionBasicaEntity.getFrenteCorta());

            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idInfBasica");
            informacionBasicaEntity.setIdInfBasica(id);

            result.setSuccess(true);
            result.setMessage("Se registró la información básica.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<InformacionBasicaEntity> list_ = new ArrayList<>();
        //String idDetalles="";
       // Integer idInfBasica=0;
        try {
            for (InformacionBasicaEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaArea_Registrar");
                sp.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codNombreInfBasica", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("departamento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("provincia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("distrito", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("comunidad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codSubInfBasica", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("numeroPc", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("areaTotal", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroHojaCatastral", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nombreHojaCatastral", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subCuenca", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroPc", String.class, ParameterMode.IN);
                setStoreProcedureEnableNullParameters(sp);
                sp.setParameter("idInfBasica", p.getIdInfBasica());
                sp.setParameter("codInfBasica", p.getCodInfBasica());
                sp.setParameter("codNombreInfBasica", p.getCodNombreInfBasica());
                sp.setParameter("departamento", p.getDepartamento());
                sp.setParameter("provincia", p.getProvincia());
                sp.setParameter("distrito", p.getDistrito());
                sp.setParameter("comunidad", p.getComunidad());
                sp.setParameter("cuenca", p.getCuenca());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("codSubInfBasica", p.getCodSubInfBasica());
                sp.setParameter("anexo", p.getAnexo());
                sp.setParameter("numeroPc", p.getNumeroPc());
                sp.setParameter("areaTotal", p.getAreaTotal());
                sp.setParameter("nroHojaCatastral", p.getNroHojaCatastral());
                sp.setParameter("nombreHojaCatastral", p.getNombreHojaCatastral());
                sp.setParameter("subCuenca", p.getSubCuenca());
                sp.setParameter("nroPc", p.getNroPc());
              //  idInfBasica=p.getIdInfBasica();

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        InformacionBasicaEntity obj_ = new InformacionBasicaEntity();
                        obj_.setIdInfBasica((Integer) row_[0]);
                        obj_.setCodInfBasica((String) row_[1]);
                        obj_.setCodNombreInfBasica((String) row_[2]);
                        obj_.setDepartamento((String) row_[3]);
                        obj_.setProvincia((String) row_[4]);
                        obj_.setDistrito((String) row_[5]);
                        obj_.setCuenca((String) row_[6]);
                        obj_.setIdPlanManejo((Integer) row_[7]);
                        obj_.setComunidad((String) row_[8]);

                        List<InformacionBasicaDetalleEntity> listDet = new ArrayList<>();
                        for (InformacionBasicaDetalleEntity param : p.getListInformacionBasicaDet()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_InfBasicaAreaDetalle_Registrar");

                            pa.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codInfBasicaDet", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("puntoVertice", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEsteIni", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorteIni", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEsteFin", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorteFin", BigDecimal.class, ParameterMode.IN);                            
                            pa.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("distanciaKm", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tiempo", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("medioTransporte", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("epoca", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idRios", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("desccripcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("areaPorcentaje", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("zonaVida", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idFauna", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idFlora", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreRio", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreQuebrada", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreLaguna", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codSubInfBasicaDet", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("acceso", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("numeroFamilia", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("subsistencia", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("perenne", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("ganaderia", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("caza", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("pesca", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("madera", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("otroProducto", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("ampliarAnexo", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("especieExtraida", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("marcar", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("conflicto", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("solucion", String.class, ParameterMode.IN);
                            setStoreProcedureEnableNullParameters(pa);
                            pa.setParameter("idInfBasicaDet", param.getIdInfBasicaDet());
                            pa.setParameter("idInfBasica", obj_.getIdInfBasica());
                            pa.setParameter("codInfBasicaDet", param.getCodInfBasicaDet());
                            pa.setParameter("puntoVertice", param.getPuntoVertice());
                            pa.setParameter("coordenadaEsteIni", param.getCoordenadaEsteIni());
                            pa.setParameter("coordenadaNorteIni", param.getCoordenadaNorteIni());
                            pa.setParameter("coordenadaEsteFin", param.getCoordenadaEsteFin());
                            pa.setParameter("coordenadaNorteFin", param.getCoordenadaNorteFin());
                            pa.setParameter("referencia", param.getReferencia());
                            pa.setParameter("distanciaKm", param.getDistanciaKm());
                            pa.setParameter("tiempo", param.getTiempo());
                            pa.setParameter("medioTransporte", param.getMedioTransporte());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.setParameter("epoca", param.getEpoca());
                            pa.setParameter("idRios", param.getIdRios());
                            pa.setParameter("desccripcion", param.getDescripcion());
                            pa.setParameter("area", param.getAreaHa());
                            pa.setParameter("areaPorcentaje", param.getAreaHaPorcentaje());
                            pa.setParameter("zonaVida", param.getZonaVida());
                            pa.setParameter("idFauna", param.getIdFauna());
                            pa.setParameter("idFlora", param.getIdFlora());
                            pa.setParameter("nombreRio", param.getNombreRio());
                            pa.setParameter("nombreQuebrada", param.getNombreQuebrada());
                            pa.setParameter("nombreLaguna", param.getNombreLaguna());
                            pa.setParameter("codSubInfBasicaDet", param.getCodSubInfBasicaDet());
                            pa.setParameter("idTipoBosque", param.getIdTipoBosque());
                            pa.setParameter("nombreComun", param.getNombreComun());
                            pa.setParameter("nombreCientifico", param.getNombreCientifico());
                            pa.setParameter("familia", param.getFamilia());
                            pa.setParameter("nombre", param.getNombre());
                            pa.setParameter("acceso", param.getAcceso());
                            pa.setParameter("numeroFamilia", param.getNumeroFamilia());
                            pa.setParameter("actividad", param.getActividad());
                            pa.setParameter("subsistencia", param.getSubsistencia());
                            pa.setParameter("perenne", param.getPerenne());
                            pa.setParameter("ganaderia", param.getGanaderia());
                            pa.setParameter("caza", param.getCaza());
                            pa.setParameter("pesca", param.getPesca());
                            pa.setParameter("madera", param.getMadera());
                            pa.setParameter("otroProducto", param.getOtroProducto());
                            pa.setParameter("ampliarAnexo", param.getAmpliarAnexo());
                            pa.setParameter("justificacion", param.getJustificacion());
                            pa.setParameter("especieExtraida", param.getEspecieExtraida());
                            pa.setParameter("marcar", param.getMarcar());
                            pa.setParameter("observaciones", param.getObservaciones());
                            pa.setParameter("conflicto", param.getConflicto());
                            pa.setParameter("solucion", param.getSolucion());
                            pa.execute();
                            //idDetalles+=param.getIdInfBasicaDet()+",";
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                InformacionBasicaDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new InformacionBasicaDetalleEntity();
                                    obj.setIdInfBasicaDet((Integer) row[0]);
                                    obj.setCodInfBasicaDet((String) row[1]);
                                    obj.setPuntoVertice((String) row[2]);
                                    obj.setCoordenadaEsteIni((BigDecimal) row[3]);
                                    obj.setCoordenadaNorteIni((BigDecimal) row[4]);
                                    obj.setReferencia((String) row[5]);
                                    obj.setDistanciaKm((BigDecimal) row[6]);
                                    obj.setTiempo((BigDecimal) row[7]);
                                    obj.setMedioTransporte((String) row[8]);
                                    obj.setIdInfBasica((Integer) row[9]);
                                    obj.setEpoca((String) row[10]);
                                    obj.setIdRios((Integer) row[11]);
                                    obj.setDescripcion((String) row[12]);
                                    obj.setAreaHa((BigDecimal) row[13]);
                                    obj.setAreaHaPorcentaje((BigDecimal) row[14]);
                                    obj.setZonaVida((String) row[15]);
                                    obj.setIdFauna((Integer) row[16]);
                                    obj.setIdFlora((Integer) row[17]);
                                    obj.setNombreRio((String) row[18]);
                                    obj.setNombreQuebrada((String) row[19]);
                                    obj.setNombreLaguna((String) row[20]);
                                    obj.setIdTipoBosque((Integer) row[21]);
                                    obj.setNombreComun((String) row[22]);
                                    obj.setNombreCientifico((String) row[23]);
                                    obj.setFamilia((String) row[24]);
                                    obj.setNombre((String) row[25]);
                                    obj.setAcceso((String) row[26]);
                                    obj.setNumeroFamilia((Integer) row[27]);
                                    obj.setActividad((String) row[28]);
                                    obj.setSubsistencia((Boolean) row[29]);
                                    obj.setPerenne((Boolean) row[30]);
                                    obj.setGanaderia((Boolean) row[31]);
                                    obj.setCaza((Boolean) row[32]);
                                    obj.setPesca((Boolean) row[33]);
                                    obj.setMadera((Boolean) row[34]);
                                    obj.setOtroProducto((Boolean) row[35]);
                                    obj.setAmpliarAnexo((Boolean) row[36]);
                                    obj.setJustificacion(row[37] == null ? null : (String) row[37]);
                                    obj.setEspecieExtraida(row[38] == null ? null : (String) row[38]);
                                    obj.setMarcar((Boolean) row[39]);
                                    obj.setObservaciones(row[40] == null ? null : (String) row[40]);
                                    obj.setConflicto(row[41] == null ? null : (String) row[41]);
                                    obj.setSolucion(row[42] == null ? null : (String) row[42]);

                                    List<InformacionBasicaDetalleSubEntity> lstDetSub = new ArrayList<>();
                                    if(param.getListInformacionBasicaDetSub()!=null){
                                        for(InformacionBasicaDetalleSubEntity paramSub: param.getListInformacionBasicaDetSub()){
                                            StoredProcedureQuery pas = entityManager
                                                    .createStoredProcedureQuery("dbo.pa_InfBasicaArea_Detalle_Sub_Registrar");
                                            pas.registerStoredProcedureParameter("idInfBasicaDetSub", Integer.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("codTipoInfBasicaDet", String.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("codSubTipoInfBasicaDet", String.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                                            pas.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                                            setStoreProcedureEnableNullParameters(pas);
                                            pas.setParameter("idInfBasicaDetSub", paramSub.getIdInfBasicaDetSub());
                                            pas.setParameter("idInfBasicaDet", obj.getIdInfBasicaDet());
                                            pas.setParameter("codTipoInfBasicaDet", paramSub.getCodTipoInfBasicaDet());
                                            pas.setParameter("codSubTipoInfBasicaDet", paramSub.getCodSubTipoInfBasicaDet());
                                            pas.setParameter("descripcion", paramSub.getDescripcion());
                                            pas.setParameter("detalle", paramSub.getDetalle());
                                            pas.setParameter("observacion", paramSub.getObservacion());
                                            pas.setParameter("idUsuarioRegistro", paramSub.getIdUsuarioRegistro());
                                            pas.execute();
                                            List<Object[]> spResult__ = pas.getResultList();
                                            if (!spResult__.isEmpty()){
                                                InformacionBasicaDetalleSubEntity obj__ = null;
                                                for (Object[] row__ : spResult__){
                                                    obj__ = new InformacionBasicaDetalleSubEntity();
                                                    obj__.setIdInfBasicaDetSub((Integer) row__[0]);
                                                    obj__.setCodTipoInfBasicaDet((String) row__[1]);
                                                    obj__.setCodSubTipoInfBasicaDet((String) row__[2]);
                                                    obj__.setIdInfBasicaDet((Integer) row__[3]);
                                                    obj__.setDescripcion((String) row__[4]);
                                                    obj__.setDetalle((String) row__[5]);
                                                    obj__.setObservacion((String) row__[6]);
                                                    lstDetSub.add(obj__);
                                                }
                                            }
                                        }
                                    }
                                    obj.setListInformacionBasicaDetSub(lstDetSub);
                                    listDet.add(obj);
                                }
                            }
                        }

                        obj_.setListInformacionBasicaDet(listDet);
                        list_.add(obj_);
                    }
                }
            }


           /* idDetalles = idDetalles.substring(0, idDetalles.length()-1);
            System.out.println(idDetalles);
            System.out.println(idInfBasica);
            EliminarInfBasicaAereaRestante(idDetalles,idInfBasica);*/


            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la información básica.");
            return result;



        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ActualizarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<InformacionBasicaEntity> list_ = new ArrayList<>();



        try {
            for (InformacionBasicaEntity p : list) {

                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("[dbo].[pa_InfBasicaArea_Actualizar]");
                sp.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codNombreInfBasica", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("departamento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("provincia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("distrito", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("comunidad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codCabecera1", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codCabecera2", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codCabecera3", String.class, ParameterMode.IN);



                setStoreProcedureEnableNullParameters(sp);
                sp.setParameter("idInfBasica", p.getIdInfBasica());
                sp.setParameter("codInfBasica", p.getCodInfBasica());
                sp.setParameter("codNombreInfBasica", p.getCodNombreInfBasica());
                sp.setParameter("departamento", p.getDepartamento());
                sp.setParameter("provincia", p.getProvincia());
                sp.setParameter("distrito", p.getDistrito());
                sp.setParameter("comunidad", p.getComunidad());
                sp.setParameter("cuenca", p.getCuenca());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.setParameter("codCabecera1", p.getCodCabecera1());
                sp.setParameter("codCabecera2", p.getCodCabecera2());
                sp.setParameter("codCabecera3", p.getCodCabecera3());

               // EliminarInformacion(p.getIdPlanManejo(),p.getCodSubInfBasica());
                //  idInfBasica=p.getIdInfBasica();

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        InformacionBasicaEntity obj_ = new InformacionBasicaEntity();
                        obj_.setIdInfBasica((Integer) row_[0]);
                        obj_.setCodInfBasica((String) row_[1]);
                        obj_.setCodNombreInfBasica((String) row_[2]);
                        obj_.setDepartamento((String) row_[3]);
                        obj_.setProvincia((String) row_[4]);
                        obj_.setDistrito((String) row_[5]);
                        obj_.setCuenca((String) row_[6]);
                        obj_.setIdPlanManejo((Integer) row_[7]);
                        obj_.setComunidad((String) row_[8]);

                        List<InformacionBasicaDetalleEntity> listDet = new ArrayList<>();
                        for (InformacionBasicaDetalleEntity param : p.getListInformacionBasicaDet()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_InfBasicaAreaDetalle_Registrar");

                            pa.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codInfBasicaDet", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("puntoVertice", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEsteIni", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorteIni", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEsteFin", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorteFin", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("distanciaKm", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tiempo", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("medioTransporte", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("epoca", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idRios", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("desccripcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("areaPorcentaje", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("zonaVida", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idFauna", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idFlora", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreRio", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreQuebrada", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreLaguna", String.class, ParameterMode.IN);

                            setStoreProcedureEnableNullParameters(pa);
                            pa.setParameter("idInfBasicaDet", param.getIdInfBasicaDet());
                            pa.setParameter("idInfBasica", obj_.getIdInfBasica());
                            pa.setParameter("codInfBasicaDet", param.getCodInfBasicaDet());
                            pa.setParameter("puntoVertice", param.getPuntoVertice());
                            pa.setParameter("coordenadaEsteIni", param.getCoordenadaEsteIni());
                            pa.setParameter("coordenadaNorteIni", param.getCoordenadaNorteIni());
                            pa.setParameter("coordenadaEsteFin", param.getCoordenadaEsteFin());
                            pa.setParameter("coordenadaNorteFin", param.getCoordenadaNorteFin());
                            pa.setParameter("referencia", param.getReferencia());
                            pa.setParameter("distanciaKm", param.getDistanciaKm());
                            pa.setParameter("tiempo", param.getTiempo());
                            pa.setParameter("medioTransporte", param.getMedioTransporte());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.setParameter("epoca", param.getEpoca());
                            pa.setParameter("idRios", param.getIdRios());
                            pa.setParameter("desccripcion", param.getDescripcion());
                            pa.setParameter("area", param.getAreaHa());
                            pa.setParameter("areaPorcentaje", param.getAreaHaPorcentaje());
                            pa.setParameter("zonaVida", param.getZonaVida());
                            pa.setParameter("idFauna", param.getIdFauna());
                            pa.setParameter("idFlora", param.getIdFlora());
                            pa.setParameter("nombreRio", param.getNombreRio());
                            pa.setParameter("nombreQuebrada", param.getNombreQuebrada());
                            pa.setParameter("nombreLaguna", param.getNombreLaguna());
                            pa.execute();
                            //idDetalles+=param.getIdInfBasicaDet()+",";
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                InformacionBasicaDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new InformacionBasicaDetalleEntity();
                                    obj.setIdInfBasicaDet((Integer) row[0]);
                                    obj.setCodInfBasicaDet((String) row[1]);
                                    obj.setPuntoVertice((String) row[2]);
                                    obj.setCoordenadaEsteIni((BigDecimal) row[3]);
                                    obj.setCoordenadaNorteIni((BigDecimal) row[4]);
                                    obj.setReferencia((String) row[5]);
                                    obj.setDistanciaKm((BigDecimal) row[6]);
                                    obj.setTiempo((BigDecimal) row[7]);
                                    obj.setMedioTransporte((String) row[8]);
                                    obj.setIdInfBasica((Integer) row[9]);
                                    obj.setEpoca((String) row[10]);
                                    obj.setIdRios((Integer) row[11]);
                                    obj.setDescripcion((String) row[12]);
                                    obj.setAreaHa((BigDecimal) row[13]);
                                    obj.setAreaHaPorcentaje((BigDecimal) row[14]);
                                    obj.setZonaVida((String) row[15]);
                                    obj.setIdFauna((Integer) row[16]);
                                    obj.setIdFlora((Integer) row[17]);
                                    obj.setNombreRio((String) row[18]);
                                    obj.setNombreQuebrada((String) row[19]);
                                    obj.setNombreLaguna((String) row[20]);
                                    listDet.add(obj);

                                }
                            }
                        }

                        obj_.setListInformacionBasicaDet(listDet);
                        list_.add(obj_);
                    }
                }
            }


            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la información básica.");
            return result;



        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    public ResultClassEntity EliminarInformacion(Integer idPlanManejo,String codCabecera) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_InfBasicaArea_Eliminar_Restante]");
        processStored.registerStoredProcedureParameter("idPlanManejo", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codCabecera", Integer.class, ParameterMode.IN);

        processStored.setParameter("idPlanManejo", idPlanManejo);
        processStored.setParameter("codCabecera", codCabecera);

        processStored.execute();
       // result.setData(param);
        result.setSuccess(true);
        //result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    @Override
    public List<InfBasicaAereaDetalleDto> listarInfBasicaAereaTitular (String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso) throws Exception {
        List<InfBasicaAereaDetalleDto> lista = new ArrayList<InfBasicaAereaDetalleDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaArea_Listar_Titular");
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subcodigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumento", tipoDocumento);
            processStored.setParameter("nroDocumento", nroDocumento);
            processStored.setParameter("codigoProcesoTitular", codigoProcesoTitular);
            processStored.setParameter("subcodigoProcesoTitular", subcodigoProcesoTitular);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigoProceso", codigoProceso);
            processStored.setParameter("subCodigoProceso", subCodigoProceso);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    InfBasicaAereaDetalleDto temp = new InfBasicaAereaDetalleDto();
                    temp.setIdInfBasica((Integer) row[0]);
                    temp.setCodInfBasica((String) row[1]);
                    temp.setCodNombreInfBasica((String) row[2]);
                    temp.setDepartamento((String) row[3]);
                    temp.setProvincia((String) row[4]);
                    temp.setDistrito((String) row[5]);
                    temp.setCuenca((String) row[6]);
                    temp.setIdPlanManejo((Integer) row[7]);
                    temp.setComunidad((String) row[8]);
                    temp.setIdInfBasicaDet((Integer) row[9]);
                    temp.setCodInfBasicaDet((String) row[10]);
                    temp.setCodSubInfBasicaDet((String) row[11]);
                    temp.setPuntoVertice((String) row[12]);
                    temp.setCoordenadaEste((BigDecimal) row[13]);
                    temp.setCoordenadaNorte((BigDecimal) row[14]);
                    temp.setReferencia((String) row[15]);
                    temp.setDistanciaKm((BigDecimal) row[16]);
                    temp.setTiempo((BigDecimal) row[17]);
                    temp.setMedioTransporte((String) row[18]);
                    temp.setEpoca((String) row[19]);
                    temp.setIdRios((Integer) row[20]);
                    temp.setDescripcion((String) row[21]);
                    temp.setAreaHa((BigDecimal) row[22]);
                    temp.setAreaHaPorcentaje((BigDecimal) row[23]);
                    temp.setZonaVida((String) row[24]);
                    temp.setIdFauna((Integer) row[25]);
                    temp.setIdFlora((Integer) row[26]);
                    temp.setNombreRio((String) row[27]);
                    temp.setNombreQuebrada((String) row[28]);
                    temp.setNombreLaguna((String) row[29]);
                    temp.setAnexo(String.valueOf((Character) row[30]));
                    temp.setNumeroPc((Integer) row[31]);
                    temp.setAreaTotal((BigDecimal) row[32]);
                    temp.setNroHojaCatastral((String) row[33]);
                    temp.setNombreHojaCatastral((String) row[34]);
                    temp.setIdTipoBosque((Integer) row[35]);
                    temp.setNombreComun((String) row[36]);
                    temp.setNombreCientifico((String) row[37]);
                    temp.setFamilia((String) row[38]);
                    temp.setNombre((String) row[39]);
                    temp.setAcceso((String) row[40]);
                    temp.setNumeroFamilia((Integer) row[41]);
                    temp.setActividad((String) row[42]);
                    temp.setSubsistencia((Boolean) row[43]);
                    temp.setPerenne((Boolean) row[44]);
                    temp.setGanaderia((Boolean) row[45]);
                    temp.setCaza((Boolean) row[46]);
                    temp.setPesca((Boolean) row[47]);
                    temp.setMadera((Boolean) row[48]);
                    temp.setOtroProducto((Boolean) row[49]);
                    temp.setAmpliarAnexo((Boolean) row[50]);
                    temp.setJustificacion(row[51] == null ? null : (String) row[51]);
                    temp.setEspecieExtraida(row[52] == null ? null : (String) row[52]);
                    temp.setMarcar((Boolean) row[53]);
                    temp.setObservaciones(row[54] == null ? null : (String) row[54]);
                    temp.setCoordenadaEsteFin(row[55] == null ? null : (BigDecimal) row[55]);
                    temp.setCoordenadaNorteFin(row[56] == null ? null : (BigDecimal) row[56]);
                    temp.setConflicto(row[57] == null ? null : (String) row[57]);
                    temp.setSolucion(row[58] == null ? null : (String) row[58]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarRecursoForestal", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<InfBasicaAereaDetalleDto> listarInfBasicaAerea(String idInfBasica, Integer idPlanManejo, String codCabecera) throws Exception {
        List<InfBasicaAereaDetalleDto> lista = new ArrayList<InfBasicaAereaDetalleDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaArea_Listar");
            processStored.registerStoredProcedureParameter("idInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codCabecera", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idInfBasica", idInfBasica);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codCabecera", codCabecera);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    InfBasicaAereaDetalleDto temp = new InfBasicaAereaDetalleDto();
                    temp.setIdInfBasica((Integer) row[0]);
                    temp.setCodInfBasica((String) row[1]);
                    temp.setCodNombreInfBasica((String) row[2]);
                    temp.setDepartamento((String) row[3]);
                    temp.setProvincia((String) row[4]);
                    temp.setDistrito((String) row[5]);
                    temp.setCuenca((String) row[6]);
                    temp.setIdPlanManejo((Integer) row[7]);
                    temp.setComunidad((String) row[8]);
                    temp.setIdInfBasicaDet((Integer) row[9]);
                    temp.setCodInfBasicaDet((String) row[10]);
                    temp.setCodSubInfBasicaDet((String) row[11]);
                    temp.setPuntoVertice((String) row[12]);
                    temp.setCoordenadaEste((BigDecimal) row[13]);
                    temp.setCoordenadaNorte((BigDecimal) row[14]);
                    temp.setReferencia((String) row[15]);
                    temp.setDistanciaKm((BigDecimal) row[16]);
                    temp.setTiempo((BigDecimal) row[17]);
                    temp.setMedioTransporte((String) row[18]);
                    temp.setEpoca((String) row[19]);
                    temp.setIdRios((Integer) row[20]);
                    temp.setDescripcion((String) row[21]);
                    temp.setAreaHa((BigDecimal) row[22]);
                    temp.setAreaHaPorcentaje((BigDecimal) row[23]);
                    temp.setZonaVida((String) row[24]);
                    temp.setIdFauna((Integer) row[25]);
                    temp.setIdFlora((Integer) row[26]);
                    temp.setNombreRio((String) row[27]);
                    temp.setNombreQuebrada((String) row[28]);
                    temp.setNombreLaguna((String) row[29]);
                    temp.setAnexo(String.valueOf((Character) row[30]));
                    temp.setNumeroPc((Integer) row[31]);
                    temp.setAreaTotal((BigDecimal) row[32]);
                    temp.setNroHojaCatastral((String) row[33]);
                    temp.setNombreHojaCatastral((String) row[34]);
                    temp.setIdTipoBosque((Integer) row[35]);
                    temp.setNombreComun((String) row[36]);
                    temp.setNombreCientifico((String) row[37]);
                    temp.setFamilia((String) row[38]);
                    temp.setNombre((String) row[39]);
                    temp.setAcceso((String) row[40]);
                    temp.setNumeroFamilia((Integer) row[41]);
                    temp.setActividad((String) row[42]);
                    temp.setSubsistencia((Boolean) row[43]);
                    temp.setPerenne((Boolean) row[44]);
                    temp.setGanaderia((Boolean) row[45]);
                    temp.setCaza((Boolean) row[46]);
                    temp.setPesca((Boolean) row[47]);
                    temp.setMadera((Boolean) row[48]);
                    temp.setOtroProducto((Boolean) row[49]);
                    temp.setAmpliarAnexo((Boolean) row[50]);
                    temp.setJustificacion(row[51] == null ? null : (String) row[51]);
                    temp.setEspecieExtraida(row[52] == null ? null : (String) row[52]);
                    temp.setMarcar((Boolean) row[53]);
                    temp.setObservaciones(row[54] == null ? null : (String) row[54]);
                    temp.setCoordenadaEsteFin(row[55] == null ? null : (BigDecimal) row[55]);
                    temp.setCoordenadaNorteFin(row[56] == null ? null : (BigDecimal) row[56]);
                    temp.setConflicto(row[57] == null ? null : (String) row[57]);
                    temp.setSolucion(row[58] == null ? null : (String) row[58]);
                    temp.setSubCuenca(row[59] == null ? null : (String) row[59]);
                    temp.setNroPc(row[60] == null ? null : (String) row[60]);

                    /********************************* DETALLE SUB**********************************************/
                    StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaArea_Detalle_Sub_Listar");
                    sp.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(sp);
                    sp.setParameter("idInfBasicaDet", temp.getIdInfBasicaDet());
                    sp.execute();
                    List<Object[]> spResultDetSub = sp.getResultList();
                    List<InformacionBasicaDetalleSubEntity> listaDetSub = new ArrayList<InformacionBasicaDetalleSubEntity>();
                    if (spResultDetSub.size() >= 1) {
                        InformacionBasicaDetalleSubEntity temp2 = null;
                        for (Object[] item2: spResultDetSub) {
                            temp2=new InformacionBasicaDetalleSubEntity();
                            temp2.setIdInfBasicaDetSub(item2[0] == null ? null : (Integer) item2[0]);
                            temp2.setCodTipoInfBasicaDet(item2[1] == null ? null : (String) item2[1]);
                            temp2.setCodSubTipoInfBasicaDet(item2[2] == null ? null : (String) item2[2]);
                            temp2.setIdInfBasicaDet(item2[3] == null ? null : (Integer) item2[3]);
                            temp2.setDescripcion(item2[4] == null ? null : (String) item2[4]);
                            temp2.setDetalle(item2[5] == null ? null : (String) item2[5]);
                            temp2.setObservacion(item2[6] == null ? null : (String) item2[6]);
                            temp2.setEstado(item2[7] == null ? null : (String) item2[7]);
                            listaDetSub.add(temp2);
                        }
                    }
                    temp.setListInformacionBasicaDetSub(listaDetSub);
                    /******************************************************************************************************/

                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarRecursoForestal", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<InfBasicaAereaDetalleDto> listarInfBasicaAereaUbigeo(String departamento, String provincia, String distrito) throws Exception {
        List<InfBasicaAereaDetalleDto> lista = new ArrayList<InfBasicaAereaDetalleDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ubigeo_Listar");
            processStored.registerStoredProcedureParameter("departamento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("provincia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("distrito", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("departamento", departamento);
            processStored.setParameter("provincia", provincia);
            processStored.setParameter("distrito", distrito);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    InfBasicaAereaDetalleDto temp = new InfBasicaAereaDetalleDto();
                    temp.setDepartamento((String) row[0]);
                    temp.setProvincia((String) row[1]);
                    temp.setDistrito((String) row[2]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarRecursoForestal", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity EliminarInfBasicaAerea(InfBasicaAereaDetalleDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_InfBasicaArea_Eliminar]");
        processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idInfBasicaDetalle", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codCabecera", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

        processStored.setParameter("idInfBasica", param.getIdInfBasica());
        processStored.setParameter("idInfBasicaDetalle", param.getIdInfBasicaDet());
        processStored.setParameter("codCabecera", param.getCodInfBasicaDet());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity<List<FaunaEntity>> obtenerFauna(Integer idPlanManejo, String tipoFauna, String nombre,
            String nombreCientifico, String familia, String estatus, String codigoTipo) throws Exception {

        StoredProcedureQuery ps = entityManager.createStoredProcedureQuery("dbo.pa_Fauna_ListarPorFiltro");
        ps.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("tipoFauna", String.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("estatus", String.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(ps);

        ps.setParameter("idPlanManejo", idPlanManejo);
        ps.setParameter("tipoFauna", tipoFauna);
        ps.setParameter("nombre", nombre);
        ps.setParameter("nombreCientifico", nombreCientifico);
        ps.setParameter("familia", familia);
        ps.setParameter("estatus", estatus);
        ps.setParameter("codigoTipo", codigoTipo);
        ps.execute();
        List<FaunaEntity> data = new ArrayList<>();
        List<Object[]> spResult = ps.getResultList();
        if (!spResult.isEmpty()) {
            for (Object[] row : spResult) {
                FaunaEntity item = new FaunaEntity();


                item.setIdFauna(row[0]==null?null:(Integer) row[0]);
                item.setTipoFauna(row[1]==null?null:(String) row[1]);
                item.setNombre(row[2]==null?null:(String) row[2]);
                item.setNombreCientifico(row[3]==null?null:(String) row[3]);
                item.setFamilia(row[4]==null?null:(String) row[4]);
                item.setEstatus(row[5]==null?null:(String) row[5]);
                item.setCodigoTipo(row[6]==null?null:(String) row[6]);
                item.setAdjunto(row[7]==null?null: String.valueOf((Character) row[7]));
                item.setEstadoSolicitud(row[8]==null?null:(String) row[8]);
                item.setIdPlanManejo(row[9]==null?null:(Integer) row[9]);
                item.setIdArchivo(row[10]==null?null:(Integer) row[10]);
                data.add(item);
            }
        }

        ResultClassEntity<List<FaunaEntity>> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity registrarSolicitudFauna(FaunaEntity faunaEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<FaunaEntity> list_ = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Fauna_Registrar");
            processStored.registerStoredProcedureParameter("idFauna", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoFauna", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estatus", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(processStored);
            processStored.setParameter("idFauna", faunaEntity.getIdFauna());
            processStored.setParameter("idPlanManejo", faunaEntity.getIdPlanManejo());
            processStored.setParameter("codigoTipo", faunaEntity.getCodigoTipo());
            processStored.setParameter("tipoFauna", faunaEntity.getTipoFauna());
            processStored.setParameter("nombre", faunaEntity.getNombre());
            processStored.setParameter("nombreCientifico", faunaEntity.getNombreCientifico());
            processStored.setParameter("familia", faunaEntity.getFamilia());
            processStored.setParameter("estatus", faunaEntity.getEstatus());
            processStored.setParameter("estadoSolicitud", faunaEntity.getEstadoSolicitud());
            processStored.setParameter("adjunto", faunaEntity.getAdjunto());
            processStored.setParameter("idUsuarioRegistro", faunaEntity.getIdUsuarioRegistro());
            processStored.setParameter("idArchivo", faunaEntity.getIdArchivo());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (!spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    FaunaEntity temp = new FaunaEntity();
                    temp.setIdFauna((Integer) row[0]);
                    temp.setCodigoTipo((String) row[1]);
                    temp.setTipoFauna((String) row[2]);
                    temp.setNombre((String) row[3]);
                    temp.setNombreCientifico((String) row[4]);
                    temp.setFamilia((String) row[5]);
                    temp.setEstatus((String) row[6]);
                    temp.setEstadoSolicitud((String) row[7]);
                    temp.setAdjunto(String.valueOf((Character) row[8]));
                    temp.setIdPlanManejo((Integer) row[9]);
                    temp.setIdArchivo((Integer) row[10]);
                    list_.add(temp);
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la Solicitud de Fauna.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<List<PoblacionAledaniaDto>> obtenerPoblacion() throws Exception {
        StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasica_ObtenerPoblacion");
        sp.execute();
        List<PoblacionAledaniaDto> data = new ArrayList<>();
        List<Object[]> spResult = sp.getResultList();
        if (!spResult.isEmpty()) {
            for (Object[] row : spResult) {
                PoblacionAledaniaDto item = new PoblacionAledaniaDto();
                item.setIdPoblacion((Integer) row[0]);
                item.setCodigo((String) row[1]);
                item.setPoblacionAledania((String) row[2]);
                item.setActividades((String) row[3]);
                data.add(item);
            }
        }

        ResultClassEntity<List<PoblacionAledaniaDto>> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity<List<TipoBosqueEntity>> obtenerTipoBosque(Integer idTipoBosque, String descripcion,
            String region) throws Exception {

        StoredProcedureQuery ps = entityManager
                .createStoredProcedureQuery("dbo.pa_InformacionBasica_ObtenerTipoBosque");
        ps.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
        ps.registerStoredProcedureParameter("region", String.class, ParameterMode.IN);
        ps.setParameter("idTipoBosque", idTipoBosque);
        ps.setParameter("descripcion", descripcion);
        ps.setParameter("region", region);
        ps.execute();

        List<TipoBosqueEntity> data = new ArrayList<>();
        List<Object[]> spResult = ps.getResultList();

        if (!spResult.isEmpty()) {

            for (Object[] row : spResult) {
                TipoBosqueEntity item = new TipoBosqueEntity();
                item.setIdTipoBosque((Short) row[0]);
                item.setDescripcion((String) row[1]);
                item.setRegion((String) row[2]);
                data.add(item);
            }
        }

        ResultClassEntity<List<TipoBosqueEntity>> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    @Override
    public List<HidrografiaEntity> obtenerHidrografia(Integer idHidrografia, String tipoHidrografia) throws Exception {
        List<HidrografiaEntity> lista = new ArrayList<HidrografiaEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_Hidrografia_ListarPorFiltro]");
            processStored.registerStoredProcedureParameter("idHidrografia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoHidrografia", String.class, ParameterMode.IN);
            processStored.setParameter("idHidrografia", idHidrografia);
            processStored.setParameter("tipoHidrografia", tipoHidrografia);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    HidrografiaEntity temp = new HidrografiaEntity();
                    temp.setIdHidrografia((Integer) row[0]);
                    temp.setTipoHidrografia((String) row[1]);
                    temp.setNombre((String) row[2]);
                    temp.setDescripcion((String) row[3]);
                    temp.setDesembocadura((String) row[4]);
                    temp.setRegion((String) row[5]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaT anexo 2", e.getMessage());
            throw e;
        }

    }





    @Override
    public ResultClassEntity registrarArchivoInfBasica(InformacionBasicaEntity param, MultipartFile file) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            // registroArchivo
            if (!file.isEmpty()) {
                ArchivoEntity request = new ArchivoEntity();
                String archivoGenerado = fileServerConexion.uploadFile(file);
                request.setIdArchivo(0);
                request.setEstado(param.getEstado());
                request.setIdUsuarioModificacion(param.getIdUsuarioModificacion());
                request.setIdUsuarioRegistro(param.getIdUsuarioRegistro());
                request.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
                request.setNombre(FilenameUtils.getBaseName(file.getOriginalFilename()));
                request.setExtension(FilenameUtils.getExtension(archivoGenerado));
                request.setRuta(fileServerPath.concat(archivoGenerado));
                request.setTipoDocumento("1");

                ResultEntity resultArchivo = this.respositoryArchivo.registroArchivo(request);
                if (resultArchivo.getCodigo() != null) {
                    StoredProcedureQuery ps = entityManager
                            .createStoredProcedureQuery("dbo.pa_InfBasicaArchivo_Registrar");
                    ps.registerStoredProcedureParameter("NU_ID_INFBASICA", Integer.class, ParameterMode.IN);
                    ps.registerStoredProcedureParameter("NU_ID_ARCHIVO", Integer.class, ParameterMode.IN);
                    ps.registerStoredProcedureParameter("P_ID_USUARIO_REGISTRO", Integer.class, ParameterMode.IN);
                    ps.setParameter("NU_ID_INFBASICA", param.getIdInfBasica());
                    ps.setParameter("NU_ID_ARCHIVO", resultArchivo.getCodigo());
                    ps.setParameter("P_ID_USUARIO_REGISTRO", param.getIdUsuarioRegistro());
                    ps.execute();
                    result.setMessage("Se registró correctamente");
                    result.setSuccess(true);
                }
            }else{
                result.setMessage("No Se registró, no contiene ningun archivo");
                result.setSuccess(false);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
        }
        return result;
    }

    @Override
    public ResultClassEntity listarInfBasicaArchivo(Integer id) {
        ResultClassEntity result = new ResultClassEntity();
        List<ResultArchivoEntity> list = new ArrayList<>();
        try {
            StoredProcedureQuery ps = entityManager
                    .createStoredProcedureQuery("dbo.pa_InfBasicaArchivo_BuscarPorFiltro");
            ps.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            ps.setParameter("idInfBasica", id);
            ps.execute();
            List<Object[]> spResult = ps.getResultList();
            if (!spResult.isEmpty()) {
                for (Object[] row_ : spResult) {
                    ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
                    String nombreArchivoGenerado = ((String) row_[0]).concat(SEPARADOR_ARCHIVO)
                            .concat((String) row_[1]);
                    String nombreArchivo = ((String) row_[2]).concat(SEPARADOR_ARCHIVO).concat((String) row_[1]);
                    byte[] byteFile = fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                    resultArchivo.setArchivo(byteFile);
                    resultArchivo.setNombeArchivo(nombreArchivo);
                    resultArchivo.setContenTypeArchivo("application/octet-stream");
                    resultArchivo.setNombeArchivoGenerado(nombreArchivoGenerado);
                    resultArchivo.setCodigo((Integer)row_[3]);
                    resultArchivo.setTipoDocumento((String)row_[4]);
                    list.add(resultArchivo);
                }
            }
            if (list.size() == 0) {
                result.setSuccess(true);
                result.setMessage("No se encontraron datos.");
                result.setData(list);
            } else {
                result.setSuccess(true);
                result.setMessage("Se obtuvo registros.");
                result.setData(list);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
        }
        return result;
    }

    @Override
    public ResultClassEntity eliminarInfBasicaArchivo(Integer id, Integer idArchivo,Integer idUsuario) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery ps = entityManager
                    .createStoredProcedureQuery("dbo.pa_InfBasicaArchivo_Eliminar");
            ps.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            ps.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            ps.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            ps.setParameter("idInfBasica", id);
            ps.setParameter("idArchivo", idArchivo);
            ps.setParameter("idUsuarioElimina", idUsuario);
            ps.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
        }
        return result;
    }

    /**
     * @autor: Jason Retamozo [26-01-2022]
     * @modificado:
     * @descripción: {Listar Información Básica Socioeconomica}
     * @param: InfBasicaAereaDetalleDto
     */
    @Override
    public List<InformacionBasicaEntity> listarInformacionSocioeconomica(Integer idPlanManejo, String codigoTipoInfBasica, String codTipoInfBasicaDet) throws Exception{
        try{
            int idDetalle=0,idDetalleDet=0;
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionSocioEconomica_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoInfBasica", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("subcodigoTipoInfBasica", String.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("codigoTipoInfBasica", codigoTipoInfBasica);
            sp.setParameter("subcodigoTipoInfBasica", codTipoInfBasicaDet);
            sp.execute();
            List<InformacionBasicaEntity> result = null;
            List<Object[]> spResult = sp.getResultList();
            if (spResult!=null && !spResult.isEmpty()){
                result = new ArrayList<>();
                InformacionBasicaEntity temp = null;
                for (Object[] item: spResult) {
                    temp = new InformacionBasicaEntity();
                    temp.setIdInfBasica(item[0]==null?null:(Integer) item[0]);
                    temp.setCodInfBasica(item[1]==null?null:(String) item[1]);
                    temp.setCodSubInfBasica(item[2]==null?null:(String) item[2]);
                    temp.setCodNombreInfBasica(item[3]==null?null:(String) item[3]);
                    temp.setDepartamento(item[4]==null?null:(String) item[4]);
                    temp.setProvincia(item[5]==null?null:(String) item[5]);
                    temp.setDistrito(item[6]==null?null:(String) item[6]);
                    temp.setCuenca(item[7]==null?null:(String) item[7]);
                    temp.setAnexo(item[8]==null?null: Character.toString((Character)item[8]) );
                    temp.setIdPlanManejo(item[9]==null?null:(Integer) item[9]);
                    temp.setNumeroPc(item[10]==null?null:(Integer) item[10]);
                    temp.setAreaTotal(item[11]==null?null:(BigDecimal) item[11]);
                    temp.setNroHojaCatastral(item[12]==null?null:(String) item[12]);
                    temp.setNombreHojaCatastral(item[13]==null?null:(String) item[13]);
                    temp.setIdHidrografia(item[14]==null?null:(Integer) item[14]);
                    temp.setFrenteCorta(item[15]==null?null : Character.toString((Character)item[15]) );
                    temp.setComunidad(item[16]==null?null:(String) item[16]);
                    List<InformacionBasicaDetalleEntity> listaDet = null;
                    if(item[0]==null){
                        idDetalle=0;
                    }
                    else{
                        idDetalle=(Integer) item[0];
                    }
                    /********************************** DETALLE **********************************************/
                    StoredProcedureQuery sp_ = entityManager.createStoredProcedureQuery("dbo.pa_InformacionSocioEconomicaDetalle_Listar");
                    sp_.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
                    sp_.registerStoredProcedureParameter("codProceso", String.class, ParameterMode.IN);
                    sp_.registerStoredProcedureParameter("subCodProceso", String.class, ParameterMode.IN);
                    setStoreProcedureEnableNullParameters(sp_);
                    if(idDetalle==0){sp_.setParameter("idInfBasica", null); }
                    else{sp_.setParameter("idInfBasica", idDetalle);}
                    sp_.setParameter("codProceso", codigoTipoInfBasica);
                    sp_.setParameter("subCodProceso", codTipoInfBasicaDet);
                    sp_.execute();
                    List<Object[]> spResultDet = sp_.getResultList();
                    if (spResultDet!=null && !spResultDet.isEmpty()){
                        listaDet= new ArrayList<>();
                        InformacionBasicaDetalleEntity temp_ = null;
                        for (Object[] item_: spResultDet) {
                            temp_ = new InformacionBasicaDetalleEntity();
                            temp_.setIdInfBasica(temp.getIdInfBasica());
                            temp_.setIdInfBasicaDet(item_[0]==null?null:(Integer) item_[0]);
                            temp_.setCodSubInfBasicaDet(item_[1]==null?null:(String) item_[1]);
                            temp_.setCodInfBasicaDet(item_[2]==null?null:(String) item_[2]);
                            temp_.setPuntoVertice(item_[3]==null?null:(String) item_[3]);
                            temp_.setCoordenadaEsteIni(item_[4]==null?null:(BigDecimal) item_[4]);
                            temp_.setCoordenadaNorteIni(item_[5]==null?null:(BigDecimal) item_[5]);
                            temp_.setCoordenadaEsteFin(item_[6]==null?null:(BigDecimal) item_[6]);
                            temp_.setCoordenadaNorteFin(item_[7]==null?null:(BigDecimal) item_[7]);
                            temp_.setReferencia(item_[8]==null?null:(String) item_[8]);
                            temp_.setDistanciaKm(item_[9]==null?null:(BigDecimal) item_[9]);
                            temp_.setTiempo(item_[10]==null?null:(BigDecimal) item_[10]);
                            temp_.setMedioTransporte(item_[11]==null?null:(String) item_[11]);
                            temp_.setDescripcion(item_[12]==null?null:(String) item_[12]); //Nombre
                            temp_.setRio(item_[13]==null?null:(String) item_[13]);
                            temp_.setQuebrada(item_[14]==null?null:(String) item_[14]);
                            temp_.setLaguna(item_[15]==null?null:(String) item_[15]);
                            temp_.setUnidadFisiografica(item_[16]==null?null:(String) item_[16]);
                            temp_.setValor(item_[17]==null?null:(String) item_[17]); // marcar
                            temp_.setAreaHa(item_[18]==null?null:(BigDecimal) item_[18]);
                            temp_.setAreaHaPorcentaje(item_[19]==null?null:(BigDecimal) item_[19]);
                            temp_.setActividad(item_[20]==null?null:(String) item_[20]);
                            temp_.setEspecieExtraida(item_[21]==null?null:(String) item_[21]);
                            temp_.setObservaciones(item_[22]==null?null:(String) item_[22]); // familia
                            temp_.setConflicto(item_[23]==null?null:(String) item_[23]);
                            temp_.setSolucion(item_[24]==null?null:(String) item_[24]);
                            temp_.setCoordenadaUtm(item_[25]==null?null:(String) item_[25]);
                            if(item_[0]==null){
                                idDetalleDet=0;
                            }
                            else{
                                idDetalleDet=(Integer) item_[0];
                            }
                            List<InformacionBasicaDetalleSubEntity> listaDetSub = null;

                            /********************************* DETALLE SUB**********************************************/
                            StoredProcedureQuery sp__ = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaArea_Detalle_Sub_Listar");
                            sp__.registerStoredProcedureParameter("idInfBasicaDet", Integer.class, ParameterMode.IN);
                            setStoreProcedureEnableNullParameters(sp__);
                            if(idDetalle==0){sp__.setParameter("idInfBasicaDet", null); }
                            else{sp__.setParameter("idInfBasicaDet", idDetalleDet);}
                            sp__.execute();
                            List<Object[]> spResultDetSub = sp__.getResultList();
                            if (spResultDetSub!=null && !spResultDetSub.isEmpty()){
                                listaDetSub= new ArrayList<>();
                                InformacionBasicaDetalleSubEntity temp__ = null;
                                for (Object[] item__: spResultDetSub) {
                                    temp__=new InformacionBasicaDetalleSubEntity();
                                    temp__.setIdInfBasicaDetSub(item__[0]==null?null:(Integer) item__[0]);
                                    temp__.setCodTipoInfBasicaDet(item__[1]==null?null:(String) item__[1]);
                                    temp__.setCodSubTipoInfBasicaDet(item__[2]==null?null:(String) item__[2]);
                                    temp__.setIdInfBasicaDet(item__[3]==null?null:(Integer) item__[3]);
                                    temp__.setDescripcion(item__[4]==null?null:(String) item__[4]);
                                    temp__.setDetalle(item__[5]==null?null:(String) item__[5]);
                                    temp__.setObservacion(item__[6]==null?null:(String) item__[6]);
                                    temp__.setEstado(item__[7]==null?null:(String) item__[7]);
                                    listaDetSub.add(temp__);
                                }
                            }
                            /******************************************************************************************************/
                            temp_.setListInformacionBasicaDetSub(listaDetSub);
                            listaDet.add(temp_);
                        }
                    }
                    /******************************************************************************************************/
                    temp.setListInformacionBasicaDet(listaDet);
                    result.add(temp);
                }
            }
            return result;
        }catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public List<InformacionBasicaEntity> listarInformacionBasica(Integer idPlanManejo) throws Exception {
        

        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasica_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);            
            sp.execute();
            List<InformacionBasicaEntity> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();
            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                InformacionBasicaEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new InformacionBasicaEntity();
                    temp.setIdInfBasica(item[0]==null?null:(Integer) item[0]);
                    temp.setCodInfBasica(item[1]==null?null:(String) item[1]);
                    temp.setCodSubInfBasica(item[2]==null?null:(String) item[2]);
                    temp.setCodNombreInfBasica(item[3]==null?null:(String) item[3]);
                    temp.setDepartamento(item[4]==null?null:(String) item[4]);
                    temp.setProvincia(item[5]==null?null:(String) item[5]);
                    temp.setDistrito(item[6]==null?null:(String) item[6]);
                    temp.setCuenca(item[7]==null?null:(String) item[7]);
                    temp.setAnexo(item[8]==null?null: Character.toString((Character)item[8]) );
                    temp.setIdPlanManejo(item[9]==null?null:(Integer) item[9]);
                    temp.setNumeroPc(item[10]==null?null:(Integer) item[10]);
                    temp.setAreaTotal(item[11]==null?null:(BigDecimal) item[11]);
                    temp.setNroHojaCatastral(item[12]==null?null:(String) item[12]);
                    temp.setNombreHojaCatastral(item[13]==null?null:(String) item[13]);
                    temp.setIdHidrografia(item[14]==null?null:(Integer) item[14]);
                    temp.setFrenteCorta(item[15]==null?null : Character.toString((Character)item[15]) );
                    resultDet.add(temp);
                }
            } 
            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity actualizarInformacionBasica(InformacionBasicaEntity informacionBasicaEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        try {
      
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasica_Actualizar");
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codNombreInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("departamento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("provincia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("distrito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroPc", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaTotal", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroHojaCatastral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreHojaCatastral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idHidrografia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("frenteCorta", String.class, ParameterMode.IN);


            setStoreProcedureEnableNullParameters(processStored);

            processStored.setParameter("idInfBasica", informacionBasicaEntity.getIdInfBasica());
            processStored.setParameter("codInfBasica", informacionBasicaEntity.getCodInfBasica());
            processStored.setParameter("codSubInfBasica", informacionBasicaEntity.getCodSubInfBasica());
            processStored.setParameter("codNombreInfBasica", informacionBasicaEntity.getCodNombreInfBasica());
            processStored.setParameter("departamento", informacionBasicaEntity.getDepartamento());
            processStored.setParameter("provincia", informacionBasicaEntity.getProvincia());
            processStored.setParameter("distrito", informacionBasicaEntity.getDistrito());
            processStored.setParameter("cuenca", informacionBasicaEntity.getCuenca());
            processStored.setParameter("anexo", informacionBasicaEntity.getAnexo());
            processStored.setParameter("idPlanManejo", informacionBasicaEntity.getIdPlanManejo());
            processStored.setParameter("idUsuarioModificacion", informacionBasicaEntity.getIdUsuarioModificacion());
            processStored.setParameter("numeroPc", informacionBasicaEntity.getNumeroPc());
            processStored.setParameter("areaTotal", informacionBasicaEntity.getAreaTotal());
            processStored.setParameter("nroHojaCatastral", informacionBasicaEntity.getNroHojaCatastral());
            processStored.setParameter("nombreHojaCatastral", informacionBasicaEntity.getNombreHojaCatastral());
            processStored.setParameter("idHidrografia", informacionBasicaEntity.getIdHidrografia());
            processStored.setParameter("frenteCorta", informacionBasicaEntity.getFrenteCorta());

            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idInfBasica");
            informacionBasicaEntity.setIdInfBasica(id);
            
            result.setSuccess(true);
            result.setMessage("Se registró la información básica.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<InformacionBasicaEntity> listarInformacionBasicaPorFiltro(Integer idPlanManejo, String codTipoInfBasica) throws Exception {
        

        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_InformacionBasica_ListarPorFiltro");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoInfBasica", String.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("idPlancodTipoInfBasicaManejo", codTipoInfBasica); 
            sp.execute();
            List<InformacionBasicaEntity> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();
            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                InformacionBasicaEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new InformacionBasicaEntity();
                    temp.setIdInfBasica(item[0]==null?null:(Integer) item[0]);
                    temp.setCodInfBasica(item[1]==null?null:(String) item[1]);
                    temp.setCodSubInfBasica(item[2]==null?null:(String) item[2]);
                    temp.setCodNombreInfBasica(item[3]==null?null:(String) item[3]);
                    temp.setDepartamento(item[4]==null?null:(String) item[4]);
                    temp.setProvincia(item[5]==null?null:(String) item[5]);
                    temp.setDistrito(item[6]==null?null:(String) item[6]);
                    temp.setCuenca(item[7]==null?null:(String) item[7]);
                    temp.setAnexo(item[8]==null?null: Character.toString((Character)item[8]) );
                    temp.setIdPlanManejo(item[9]==null?null:(Integer) item[9]);
                    temp.setNumeroPc(item[10]==null?null:(Integer) item[10]);
                    temp.setAreaTotal(item[11]==null?null:(BigDecimal) item[11]);
                    temp.setNroHojaCatastral(item[12]==null?null:(String) item[12]);
                    temp.setNombreHojaCatastral(item[13]==null?null:(String) item[13]);
                    temp.setIdHidrografia(item[14]==null?null:(Integer) item[14]);
                    temp.setFrenteCorta(item[15]==null?null : Character.toString((Character)item[15]) );
                    resultDet.add(temp);
                }
            } 
            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }


    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

    /**
	 * @autor: Jaqueline Diaz Barrientos [04-10-2021]
	 * @modificado:
	 * @descripción: {Eliminar fauna}
	 * @param: InfBasicaAereaDetalleDto
	 */
    @Override
    public ResultClassEntity EliminarFauna(InfBasicaAereaDetalleDto infBasicaAereaDetalleDto) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery ps = entityManager.createStoredProcedureQuery("dbo.pa_Fauna_Eliminar");
            ps.registerStoredProcedureParameter("idFauna", Integer.class, ParameterMode.IN);
            ps.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            ps.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            ps.setParameter("idFauna", infBasicaAereaDetalleDto.getIdFauna());
            ps.setParameter("codigoProceso", infBasicaAereaDetalleDto.getCodInfBasica());
            ps.setParameter("idUsuarioElimina", infBasicaAereaDetalleDto.getIdUsuarioElimina());
            ps.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
        }
        return result;
    }

    /**
     * @autor: Danny Nazario [22-12-2021]
     * @modificado:
     * @descripción: {Listar Detalle Información Básica Área}
     * @param: InfBasicaAereaDetalleDto
     */
    @Override
    public ResultClassEntity listarInfBasicaAereaDetalle(String codTipo, String codSubTipo, Integer idInfBasica) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<InfBasicaAereaDetalleDto> lista = new ArrayList<InfBasicaAereaDetalleDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaAreaDetalle_Listar");
            processStored.registerStoredProcedureParameter("codTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codTipo", codTipo);
            processStored.setParameter("codSubTipo", codSubTipo);
            processStored.setParameter("idInfBasica", idInfBasica);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    InfBasicaAereaDetalleDto temp = new InfBasicaAereaDetalleDto();
                    temp.setIdInfBasicaDet(row[0] == null ? null : (Integer) row[0]);
                    temp.setCodInfBasicaDet(row[1] == null ? null : (String) row[1]);
                    temp.setCodSubInfBasicaDet(row[2] == null ? null : (String) row[2]);
                    temp.setPuntoVertice(row[3] == null ? null : (String) row[3]);
                    temp.setCoordenadaEste(row[4] == null ? null : (BigDecimal) row[4]);
                    temp.setCoordenadaNorte(row[5] == null ? null : (BigDecimal) row[5]);
                    temp.setReferencia(row[6] == null ? null : (String) row[6]);
                    temp.setDistanciaKm(row[7] == null ? null : (BigDecimal) row[7]);
                    temp.setTiempo(row[8] == null ? null : (BigDecimal) row[8]);
                    temp.setMedioTransporte(row[9] == null ? null : (String) row[9]);
                    temp.setEpoca(row[10] == null ? null : (String) row[10]);
                    temp.setIdRios(row[11] == null ? null : (Integer) row[11]);
                    temp.setDescripcion(row[12] == null ? null : (String) row[12]);
                    temp.setAreaHa(row[13] == null ? null : (BigDecimal) row[13]);
                    temp.setAreaHaPorcentaje(row[14] == null ? null : (BigDecimal) row[14]);
                    temp.setZonaVida(row[15] == null ? null : (String) row[15]);
                    temp.setIdFauna(row[16] == null ? null : (Integer) row[16]);
                    temp.setIdFlora(row[17] == null ? null : (Integer) row[17]);
                    temp.setNombreRio(row[18] == null ? null : (String) row[18]);
                    temp.setNombreQuebrada(row[19] == null ? null : (String) row[19]);
                    temp.setNombreLaguna(row[20] == null ? null : (String) row[20]);
                    temp.setIdTipoBosque(row[21] == null ? null : (Integer) row[21]);
                    temp.setNombreComun(row[22] == null ? null : (String) row[22]);
                    temp.setNombreCientifico(row[23] == null ? null : (String) row[23]);
                    temp.setFamilia(row[24] == null ? null : (String) row[24]);
                    temp.setNombre(row[25] == null ? null : (String) row[25]);
                    temp.setAcceso(row[26] == null ? null : (String) row[26]);
                    temp.setNumeroFamilia(row[27] == null ? null : (Integer) row[27]);
                    temp.setActividad(row[28] == null ? null : (String) row[28]);
                    temp.setSubsistencia(row[29] == null ? null : (Boolean) row[29]);
                    temp.setPerenne(row[30] == null ? null : (Boolean) row[30]);
                    temp.setGanaderia(row[31] == null ? null : (Boolean) row[31]);
                    temp.setCaza(row[32] == null ? null : (Boolean) row[32]);
                    temp.setPesca(row[33] == null ? null : (Boolean) row[33]);
                    temp.setMadera(row[34] == null ? null : (Boolean) row[34]);
                    temp.setOtroProducto(row[35] == null ? null : (Boolean) row[35]);
                    temp.setAmpliarAnexo(row[36] == null ? null : (Boolean) row[36]);
                    temp.setJustificacion(row[37] == null ? null : (String) row[37]);
                    temp.setEspecieExtraida(row[38] == null ? null : (String) row[38]);
                    temp.setMarcar(row[39] == null ? null : (Boolean) row[39]);
                    temp.setObservaciones(row[40] == null ? null : (String) row[40]);
                    temp.setCoordenadaUtm(row[41] == null ? null : (String) row[41]);
                    lista.add(temp);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity listarPorFiltrosInfBasicaAerea(InfBasicaAereaDetalleDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InfBasicaArea_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codCabecera", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            processStored.setParameter("codInfBasica", param.getCodInfBasica());
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codCabecera", param.getCodInfBasicaDet());
            processStored.setParameter("pageNum", param.getPageNum());
            processStored.setParameter("pageSize", param.getPageSize());
            processStored.execute();
            List<InfBasicaAereaDetalleDto> list = new ArrayList<InfBasicaAereaDetalleDto>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    InfBasicaAereaDetalleDto temp = new InfBasicaAereaDetalleDto();
                    temp.setIdInfBasica((Integer) row[0]);
                    temp.setCodInfBasica((String) row[1]);
                    temp.setCodNombreInfBasica((String) row[2]);
                    temp.setDepartamento((String) row[3]);
                    temp.setProvincia((String) row[4]);
                    temp.setDistrito((String) row[5]);
                    temp.setCuenca((String) row[6]);
                    temp.setIdPlanManejo((Integer) row[7]);
                    temp.setComunidad((String) row[8]);
                    temp.setIdInfBasicaDet((Integer) row[9]);
                    temp.setCodInfBasicaDet((String) row[10]);
                    temp.setCodSubInfBasicaDet((String) row[11]);
                    temp.setPuntoVertice((String) row[12]);
                    temp.setCoordenadaEste((BigDecimal) row[13]);
                    temp.setCoordenadaNorte((BigDecimal) row[14]);
                    temp.setReferencia((String) row[15]);
                    temp.setDistanciaKm((BigDecimal) row[16]);
                    temp.setTiempo((BigDecimal) row[17]);
                    temp.setMedioTransporte((String) row[18]);
                    temp.setEpoca((String) row[19]);
                    temp.setIdRios((Integer) row[20]);
                    temp.setDescripcion((String) row[21]);
                    temp.setAreaHa((BigDecimal) row[22]);
                    temp.setAreaHaPorcentaje((BigDecimal) row[23]);
                    temp.setZonaVida((String) row[24]);
                    temp.setIdFauna((Integer) row[25]);
                    temp.setIdFlora((Integer) row[26]);
                    temp.setNombreRio((String) row[27]);
                    temp.setNombreQuebrada((String) row[28]);
                    temp.setNombreLaguna((String) row[29]);
                    temp.setAnexo(String.valueOf((Character) row[30]));
                    temp.setNumeroPc((Integer) row[31]);
                    temp.setAreaTotal((BigDecimal) row[32]);
                    temp.setNroHojaCatastral((String) row[33]);
                    temp.setNombreHojaCatastral((String) row[34]);
                    temp.setIdTipoBosque((Integer) row[35]);
                    temp.setNombreComun((String) row[36]);
                    temp.setNombreCientifico((String) row[37]);
                    temp.setFamilia((String) row[38]);
                    temp.setNombre((String) row[39]);
                    temp.setAcceso((String) row[40]);
                    temp.setNumeroFamilia((Integer) row[41]);
                    temp.setActividad((String) row[42]);
                    temp.setSubsistencia((Boolean) row[43]);
                    temp.setPerenne((Boolean) row[44]);
                    temp.setGanaderia((Boolean) row[45]);
                    temp.setCaza((Boolean) row[46]);
                    temp.setPesca((Boolean) row[47]);
                    temp.setMadera((Boolean) row[48]);
                    temp.setOtroProducto((Boolean) row[49]);
                    temp.setAmpliarAnexo((Boolean) row[50]);
                    temp.setJustificacion(row[51] == null ? null : (String) row[51]);
                    temp.setEspecieExtraida(row[52] == null ? null : (String) row[52]);
                    temp.setMarcar((Boolean) row[53]);
                    temp.setObservaciones(row[54] == null ? null : (String) row[54]);
                    temp.setCoordenadaEsteFin(row[55] == null ? null : (BigDecimal) row[55]);
                    temp.setCoordenadaNorteFin(row[56] == null ? null : (BigDecimal) row[56]);
                    result.setTotalRecord((Integer) row[57]);
                    list.add(temp);

                }
            }
            result.setData(list);
            result.setMessage((result.getTotalRecord()!=0?"Información encontrada":"Informacion no encontrada"));
            result.setSuccess((result.getTotalRecord()!=0?true:false));
        } catch (Exception e) {
            result.setMessage("Ocurrio un error");
            result.setSuccess(false);
        }
        return result;
    
    }

    @Override
    public ResultClassEntity ListarFaunaInformacionBasicaDetalle(InformacionBasicaDetalleDto param)  throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try
        {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("pa_InformacionBasicaDetalle_ListarFauna");
            sp.registerStoredProcedureParameter("idEspecie", Short.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("autor", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codInfBasica", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codNombreInfBasica", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codSubInfBasicaDet", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idEspecie", param.getIdEspecie());
            sp.setParameter("nombreComun", param.getNombreComun());
            sp.setParameter("nombreCientifico", param.getNombreCientifico());
            sp.setParameter("autor", param.getAutor());
            sp.setParameter("familia", param.getFamilia());
            sp.setParameter("idPlanManejo", param.getIdPlanManejo());
            sp.setParameter("codInfBasica", param.getCodInfBasica());
            sp.setParameter("codNombreInfBasica", param.getCodNombreInfBasica());
            sp.setParameter("codSubInfBasicaDet", param.getCodInfBasicaDet());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());

            sp.execute();


            List<InformacionBasicaDetalleDto> objList=new ArrayList<InformacionBasicaDetalleDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult.size() >= 1){
                InformacionBasicaDetalleDto objOrdPro = null;
                for (Object[] row: spResult){
                    objOrdPro = new InformacionBasicaDetalleDto();
                    objOrdPro.setIdInfBasica((Integer) row[0]);
                    objOrdPro.setIdInfBasicaDet((Integer) row[1]);
                    objOrdPro.setIdFauna((Integer) row[2]);
                    objOrdPro.setNombreComun((String) row[3]);
                    objOrdPro.setNombreCientifico((String) row[4]);
                    objOrdPro.setAutor(row[5]==null?null:(String) row[5]);
                    objOrdPro.setFamilia(row[6]==null?null:(String) row[6]);
                    objOrdPro.setAmenaza(row[7]==null?null:(String) row[7]);
                    result.setTotalRecord((Integer) row[8]);
                    objOrdPro.setCodInfBasica(param.getCodInfBasica());
                    objOrdPro.setCodNombreInfBasica(param.getCodNombreInfBasica());
                    objOrdPro.setCodSubInfBasicaDet(param.getCodSubInfBasicaDet());
                    objList.add(objOrdPro);
                }
            }

            result.setData(objList);
            result.setMessage((result.getTotalRecord()!=0?"Información encontrada":"Informacion no encontrada"));
            result.setSuccess((result.getTotalRecord()!=0?true:false));
        } catch (Exception e) {
            result.setMessage("Ocurrio un error");
            result.setSuccess(false);
        }
        return result;
    }

}
