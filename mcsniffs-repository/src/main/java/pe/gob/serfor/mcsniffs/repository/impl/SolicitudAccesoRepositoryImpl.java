package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudAccesoRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.Security;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SolicitudAccesoRepositoryImpl extends JdbcDaoSupport implements SolicitudAccesoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @Autowired
    private Security security;

    private static final Logger log = LogManager.getLogger(SolicitudAccesoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para consultar Solicitudes de Acceso}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: List<SolicitudAccesoEntity>
     */
    @Override
    public List<SolicitudAccesoEntity> ListarSolicitudAcceso(SolicitudAccesoEntity param) {
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudAcceso_Listar");

            processStored.registerStoredProcedureParameter("codigoTipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroRucEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("razonSocialEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoActor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoCncc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudAcceso", int.class, ParameterMode.IN);

            //PARA PRUEBAS
            String pru1 = (param.getCodigoTipoPersona()==null)?"":param.getCodigoTipoPersona();
            String pru2 = (param.getCodigoTipoDocumento()==null)?"":param.getCodigoTipoDocumento();
            String pru3 = (param.getNumeroDocumento()==null)?"":param.getNumeroDocumento();
            String pru4 = (param.getNombres()==null)?"":param.getNombres();
            String pru7 = (param.getNumeroRucEmpresa()==null)?"":param.getNumeroRucEmpresa();
            String pru8 = (param.getRazonSocialEmpresa()==null)?"":param.getRazonSocialEmpresa();
            String pru9 = (param.getCodigoTipoActor()==null)?"":param.getCodigoTipoActor();
            String pru10 = (param.getEstadoSolicitud()==null)?"":param.getCodigoEstadoSolicitud();
            String pru11 = (param.getCodigoTipoCncc()==null)?"":param.getCodigoTipoCncc();

            processStored.setParameter("codigoTipoPersona", (param.getCodigoTipoPersona()==null)?"":param.getCodigoTipoPersona());
            processStored.setParameter("codigoTipoDocumento", (param.getCodigoTipoDocumento()==null)?"":param.getCodigoTipoDocumento());
            processStored.setParameter("numeroDocumento", (param.getNumeroDocumento()==null)?"":param.getNumeroDocumento());
            processStored.setParameter("nombres", (param.getNombres()==null)?"":param.getNombres());
            processStored.setParameter("numeroRucEmpresa", (param.getNumeroRucEmpresa()==null)?"":param.getNumeroRucEmpresa());
            processStored.setParameter("razonSocialEmpresa", (param.getRazonSocialEmpresa()==null)?"":param.getRazonSocialEmpresa());
            processStored.setParameter("codigoTipoActor", (param.getCodigoTipoActor()==null)?"":param.getCodigoTipoActor());
            processStored.setParameter("estado", (param.getEstadoSolicitud()==null)?"":param.getEstadoSolicitud());
            processStored.setParameter("codigoTipoCncc", (param.getCodigoTipoCncc()==null)?"":param.getCodigoTipoCncc());
            processStored.setParameter("idSolicitudAcceso", (param.getIdSolicitudAcceso()==null)?0:param.getIdSolicitudAcceso());

            processStored.execute();
            List<SolicitudAccesoEntity> result = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    SolicitudAccesoEntity temp = new SolicitudAccesoEntity();
                    temp.setIdSolicitudAcceso((Integer) row[0]);
                    temp.setIdSolicitudAccesoTexto("");
                    temp.setFechaIngreso((Date) row[1]);
                    temp.setFechaRespuesta( (Date) row[2]);
                    temp.setCodigoTipoPersona((String) row[3]);
                    temp.setTipoPersona((String) row[4]);
                    temp.setEsReprLegal((char) row[5]);
                    temp.setTieneAdjReprLegal((char) row[6]);
                    temp.setCodigoTipoDocumento((String) row[7]);
                    temp.setTipoDocumento((String) row[8]);
                    temp.setNumeroDocumento((String) row[9]);
                    temp.setCodigoEvalSolic((String) row[10]);
                    temp.setEvalSolic((String) row[11]);
                    temp.setNombres((String) row[12]);
                    temp.setApellidoPaterno((String) row[13]);
                    temp.setApellidoMaterno((String) row[14]);
                    temp.setEmail((String) row[15]);
                    temp.setNumeroRucEmpresa((String) row[16]);
                    temp.setRazonSocialEmpresa((String) row[17]);
                    temp.setDireccionEmpresa((String) row[18]);
                    temp.setTelefonoEmpresa((String) row[19]);
                    temp.setEmailEmpresa((String) row[20]);
                    temp.setCodigoEvalEmpresa((String) row[21]);
                    temp.setEvalEmpresa((String) row[22]);
                    temp.setCodigoTipoActor((String) row[23]);
                    temp.setTipoActor((String) row[24]);
                    temp.setTieneAdjActor((char) row[25]);
                    temp.setNotificaRegistro((char) row[27]);
                    temp.setNotificaRevision((char) row[26]);
                    temp.setObservacion((String) row[28]);
                    temp.setIdUsuarioRevision((Integer) row[29]);
                    temp.setIdUsuarioRevision((Integer) row[29]);
                    temp.setCodigoTipoCncc((String) row[30]);
                    temp.setTipoCncc((String) row[31]);
                    temp.setCodigoEstadoSolicitud((String) row[32]);
                    temp.setEstadoSolicitud((String) row[33]);

                    temp.setIdDepartamentoPersona((String) row[34]);
                    temp.setIdProvinciaPersona((String) row[35]);
                    temp.setIdDistritoPersona((String) row[36]);

                    temp.setNombreDepartamento((String) row[37]);
                    temp.setNombreProvincia((String) row[38]);
                    temp.setNombreDistrito((String) row[39]);

                    temp.setNombreUsuarioRevision((String) row[40]);
                    result.add(temp);
                }
            }
            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para obtener datos de una Solicitud de Acceso específica}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: SolicitudAccesoEntity
     */
    @Override
    public SolicitudAccesoEntity ObtenerSolicitudAcceso(SolicitudAccesoEntity param) {
        try{
            //obtenemos el registro usando la funcion listar
            List<SolicitudAccesoEntity> lista = ListarSolicitudAcceso(param);

            //obtenemos el registro específico
            SolicitudAccesoEntity solicitudAcceso = lista.get(0);

            //retornamos el objeto
            return solicitudAcceso;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }


    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para registrar Solicitudes de Acceso}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: ResultClassEntity
     */
    @Override
    public ResultClassEntity RegistrarSolicitudAcceso(SolicitudAccesoEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudAcceso_Registrar");
            processStored.registerStoredProcedureParameter("codigoTipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("esReprLegal", char.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tieneAdjReprLegal", char.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvalSolic", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("email", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroRucEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("razonSocialEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccionEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("emailEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvalEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoActor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tieneAdjActor", char.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificaRevision", char.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoCncc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistritoPersona", String.class, ParameterMode.IN);

            processStored.setParameter("codigoTipoPersona", (param.getCodigoTipoPersona()==null)?"":param.getCodigoTipoPersona());
            processStored.setParameter("esReprLegal", param.getEsReprLegal());
            processStored.setParameter("tieneAdjReprLegal", param.getTieneAdjReprLegal());
            processStored.setParameter("codigoTipoDocumento", (param.getCodigoTipoDocumento()==null)?"":param.getCodigoTipoDocumento());
            processStored.setParameter("numeroDocumento", (param.getNumeroDocumento()==null)?"":param.getNumeroDocumento());
            processStored.setParameter("codigoEvalSolic", (param.getCodigoEvalSolic()==null)?"":param.getCodigoEvalSolic());
            processStored.setParameter("nombres", (param.getNombres()==null)?"":param.getNombres());
            processStored.setParameter("apellidoPaterno", (param.getApellidoPaterno()==null)?"":param.getApellidoPaterno());
            processStored.setParameter("apellidoMaterno", (param.getApellidoMaterno()==null)?"":param.getApellidoMaterno());
            processStored.setParameter("email", (param.getEmail()==null)?"":param.getEmail());
            processStored.setParameter("numeroRucEmpresa", (param.getNumeroRucEmpresa()==null)?"":param.getNumeroRucEmpresa());
            processStored.setParameter("razonSocialEmpresa", (param.getRazonSocialEmpresa()==null)?"":param.getRazonSocialEmpresa());
            processStored.setParameter("direccionEmpresa", (param.getDireccionEmpresa()==null)?"":param.getDireccionEmpresa());
            processStored.setParameter("telefonoEmpresa", (param.getTelefonoEmpresa()==null)?"":param.getTelefonoEmpresa());
            processStored.setParameter("emailEmpresa", (param.getEmailEmpresa()==null)?"":param.getEmailEmpresa());
            processStored.setParameter("codigoEvalEmpresa", (param.getCodigoEvalEmpresa()==null)?"":param.getCodigoEvalEmpresa());
            processStored.setParameter("codigoTipoActor", (param.getCodigoTipoActor()==null)?"":param.getCodigoTipoActor());
            processStored.setParameter("tieneAdjActor", param.getTieneAdjActor());
            processStored.setParameter("notificaRevision", param.getNotificaRevision());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.setParameter("codigoTipoCncc", (param.getCodigoTipoCncc()==null)?"":param.getCodigoTipoCncc());
            processStored.setParameter("idDistritoPersona", param.getIdDistritoPersona());

            processStored.execute();
            Object holis = processStored.getSingleResult();
            Integer objre = Integer.parseInt(holis.toString());
            result.setCodigo(objre);
            if(objre == -1){
                result.setSuccess(false);
                result.setMessage("El número de documento ya se encuentra registrado en el sistema.");
            }else{
                result.setSuccess(true);
                result.setMessage("La solicitud se registró correctamente.");

                LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MVC_SOLICITUDACCESO,
                        LogAuditoria.REGISTRAR,
                        LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLICITUDACCESO + objre,
                        param.getIdUsuarioRegistro());

                logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            }

            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(e.getCause()!=null?e.getCause().getMessage():e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para actualizar Solicitudes de Acceso cambiando su estado y datos de sus revisión.}
     * @param: Objeto SolicitudAccesoEntity
     *
     * @return: ResultClassEntity
     */
    @Override
    public ResultClassEntity ActualizarRevisionSolicitudAcceso(SolicitudAccesoEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudAcceso_ActualizarRevision");
            processStored.registerStoredProcedureParameter("idSolicitudAcceso", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRevision", int.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoAccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("claveEncriptada", String.class, ParameterMode.IN);

            processStored.setParameter("idSolicitudAcceso", param.getIdSolicitudAcceso());
            processStored.setParameter("idUsuarioRevision", param.getIdUsuarioRevision());
            processStored.setParameter("codigoAccion", param.getCodigoAccion());
            processStored.setParameter("observacion", (param.getObservacion()==null)?"":param.getObservacion());
            processStored.setParameter("claveEncriptada", (param.getNumeroDocumento()==null)?"": security.aesEncrypt(param.getNumeroDocumento()));

            processStored.execute();
            Object holis = processStored.getSingleResult();
            result.setCodigo(param.getIdSolicitudAcceso());
            result.setSuccess(true);
            result.setMessage(holis.toString());
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(e.getCause()!=null?e.getCause().getMessage():e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarSolicitudAccesoUsuario(SolicitudAccesoEntity solicitudAcceso) {
        ResultClassEntity resultClassEntity= new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudAcceso_ListarPorUsuario");
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoActor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoCNCC", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("razonSocialEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUsuario", String.class, ParameterMode.IN);

            processStored.setParameter("estadoSolicitud", (solicitudAcceso.getEstadoSolicitud()==null)? "":solicitudAcceso.getEstadoSolicitud());
            processStored.setParameter("codigoTipoPersona", (solicitudAcceso.getCodigoTipoPersona()==null)?"":solicitudAcceso.getCodigoTipoPersona());
            processStored.setParameter("codigoTipoActor", (solicitudAcceso.getCodigoTipoActor()==null)?"":solicitudAcceso.getCodigoTipoActor());
            processStored.setParameter("codigoTipoCNCC", (solicitudAcceso.getCodigoTipoCncc()==null)?"":solicitudAcceso.getCodigoTipoCncc());
            processStored.setParameter("numeroDocumento", (solicitudAcceso.getNumeroDocumento()==null)?"":solicitudAcceso.getNumeroDocumento());
            processStored.setParameter("nombres", (solicitudAcceso.getNombres()==null)?"":solicitudAcceso.getNombres());
            processStored.setParameter("razonSocialEmpresa", (solicitudAcceso.getRazonSocialEmpresa()==null)?"":solicitudAcceso.getRazonSocialEmpresa());
            processStored.setParameter("codigoUsuario", (solicitudAcceso.getCodigoUsuario()==null)?"":solicitudAcceso.getCodigoUsuario());

            processStored.execute();
            List<SolicitudAccesoEntity> result = new ArrayList<>();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    SolicitudAccesoEntity temp = new SolicitudAccesoEntity();
                    temp.setIdSolicitudAcceso((Integer) row[0]);
                    temp.setIdSolicitudAccesoTexto("");
                    temp.setFechaIngreso((Date) row[1]);
                    temp.setFechaRespuesta( (Date) row[2]);
                    temp.setCodigoTipoPersona((String) row[3]);
                    temp.setTipoPersona((String) row[4]);
                    temp.setEsReprLegal((char) row[5]);
                    temp.setTieneAdjReprLegal((char) row[6]);
                    temp.setCodigoTipoDocumento((String) row[7]);
                    temp.setTipoDocumento((String) row[8]);
                    temp.setNumeroDocumento((String) row[9]);
                    temp.setCodigoEvalSolic((String) row[10]);
                    temp.setEvalSolic((String) row[11]);
                    temp.setNombres((String) row[12]);
                    temp.setApellidoPaterno((String) row[13]);
                    temp.setApellidoMaterno((String) row[14]);
                    temp.setEmail((String) row[15]);
                    temp.setNumeroRucEmpresa((String) row[16]);
                    temp.setRazonSocialEmpresa((String) row[17]);
                    temp.setDireccionEmpresa((String) row[18]);
                    temp.setTelefonoEmpresa((String) row[19]);
                    temp.setEmailEmpresa((String) row[20]);
                    temp.setCodigoEvalEmpresa((String) row[21]);
                    temp.setEvalEmpresa((String) row[22]);
                    temp.setCodigoTipoActor((String) row[23]);
                    temp.setTipoActor((String) row[24]);
                    temp.setTieneAdjActor((char) row[25]);
                    temp.setNotificaRegistro((char) row[27]);
                    temp.setNotificaRevision((char) row[26]);
                    temp.setObservacion((String) row[28]);
                    temp.setIdUsuarioRevision((Integer) row[29]);
                    temp.setCodigoTipoCncc((String) row[30]);
                    temp.setTipoCncc((String) row[31]);
                    temp.setCodigoEstadoSolicitud((String) row[32]);
                    temp.setEstadoSolicitud((String) row[33]);
                    temp.setIdDepartamentoPersona((String) row[34]);
                    temp.setIdProvinciaPersona((String) row[35]);
                    temp.setIdDistritoPersona((String) row[36]);
                    temp.setNombreDepartamento((String) row[37]);
                    temp.setNombreProvincia((String) row[38]);
                    temp.setNombreDistrito((String) row[39]);
                    temp.setNombreUsuarioRevision((String) row[40]);
                    result.add(temp);
                }
            }

            resultClassEntity.setData(result);
            resultClassEntity.setSuccess(true);
            resultClassEntity.setMessage("Se obtiene resultado de consulta.");
            return resultClassEntity;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultClassEntity.setSuccess(false);
            resultClassEntity.setMessage("Error al obtener resultado de consulta.");
            return resultClassEntity;
        }
    }

}
