package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudBosqueLocalRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalEntity> lista = new ArrayList<SolicitudBosqueLocalEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Listar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumentoSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bandejaBosqueLocal", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());
            processStored.setParameter("nombreSolicitante",obj.getNombreSolicitante());
            processStored.setParameter("numeroDocumentoSolicitante",obj.getNumeroDocumentoSolicitante());
            processStored.setParameter("estadoSolicitud",obj.getEstadoSolicitud());
            processStored.setParameter("pageNum",obj.getPageNum());
            processStored.setParameter("pageSize",obj.getPageSize());
            processStored.setParameter("perfil",obj.getPerfil());
            processStored.setParameter("idUsuario",obj.getIdUsuario());
            processStored.setParameter("bandejaBosqueLocal",obj.getBandejaBosqueLocal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalEntity();

                        c.setIdSolBosqueLocal(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolicitante(row[1]==null?null:(Integer) row[1]);
                        c.setTipoPersonaSolicitante(row[2]==null?null:(String) row[2]);
                        c.setTipoDocumentoSolicitante(row[3]==null?null:(String) row[3]);
                        c.setNumeroDocumentoSolicitante(row[4]==null?null:(String) row[4]);
                        c.setNombreSolicitante(row[5]==null?null:(String) row[5]);
                        c.setGobiernoLocal(row[6]==null?null:(String) row[6]);
                        c.setIdDistritoSolicitante(row[7]==null?null:(String) row[7]);
                        c.setNombreDistritoSolicitante(row[8]==null?null:(String) row[8]);
                        c.setNombreProvinciaSolicitante(row[9]==null?null:(String) row[9]);
                        c.setNombreDepartamentoSolicitante(row[10]==null?null:(String) row[10]);
                        c.setDireccionSolicitante(row[11]==null?null:(String) row[11]);
                        c.setIdReprLegal(row[12]==null?null:(Integer) row[12]);
                        c.setTipoPersonaReprLegal(row[13]==null?null:(String) row[13]);
                        c.setTipoDocumentoReprLegal(row[14]==null?null:(String) row[14]);
                        c.setNumeroDocumentoReprLegalUnico(row[15]==null?null:(String) row[15]);
                        c.setRepresentanteLegal(row[16]==null?null:(String) row[16]);
                        c.setCorreoElectronicoReprLegal(row[17]==null?null:(String) row[17]);
                        c.setIdDepartamentoReprLegal(row[18]==null?null:(Integer) row[18]);
                        c.setIdProvinciaReprLegal(row[19]==null?null:(Integer) row[19]);
                        c.setIdDistritoReprLegal(row[20]==null?null:(String) row[20]);
                        c.setNombreDepartamentoReprLegal(row[21]==null?null:(String) row[21]);
                        c.setNombreProvinciaReprLegal(row[22]==null?null:(String) row[22]);
                        c.setNombreDistritoReprLegal(row[23]==null?null:(String) row[23]);
                        c.setFechaPresentacionTexto(row[24]==null?null:(String) row[24]);
                        c.setResultadoSolicitud(row[25]==null?null:(String) row[25]);
                        c.setEstado(row[26]==null?null:(String) row[26]);
                        c.setEstadoSolicitud(row[27]==null?null:(String) row[27]);
                        c.setEstadoSolicitudTexto(row[28]==null?null:(String) row[28]);
                        c.setDescripcionTipoPersonaSolicitante(row[30]==null?null:(String) row[30]);
                        c.setEvaluacionGabinete(row[31]==null?null:(Boolean) row[31]);
                        c.setEvaluacionCampo(row[32]==null?null:(Boolean) row[32]);
                        c.setCodigoTH(row[33]==null?null:(String) row[33]);
                        c.setIdGeometria(row[34]==null?null:(Integer) row[34]);
                        lista.add(c);
                        result.setTotalRecord((Integer) row[29]);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar solicitud bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar solicitud bosque local");
            return  result;
        }
    }

   @Override
    public ResultClassEntity registrarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Registrar");
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resultadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("gobiernoLocal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idSolicitante", obj.getIdSolicitante());
            processStored.setParameter("fechaPresentacion", obj.getFechaPresentacion());
            processStored.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            processStored.setParameter("resultadoSolicitud", obj.getResultadoSolicitud());
            processStored.setParameter("gobiernoLocal", obj.getGobiernoLocal());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocal");
            obj.setIdSolBosqueLocal(idSolBosqueLocal);
            result.setCodigo(idSolBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la actividad de la solicitud bosque Local correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la actividad de la solicitud Bosque Local");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }
    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Actualizar");
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resultadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("gobiernoLocal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumentoSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombresSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apePaternoSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apeMaternoSolicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumentoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombresRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDepartamento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idProvincia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTH", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idSolicitante", obj.getIdSolicitante());
            processStored.setParameter("fechaPresentacion", obj.getFechaPresentacion());
            processStored.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            processStored.setParameter("resultadoSolicitud", obj.getResultadoSolicitud());
            processStored.setParameter("gobiernoLocal", obj.getGobiernoLocal());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());
            processStored.setParameter("nroDocumentoSolicitante", obj.getNumeroDocumentoSolicitante());
            processStored.setParameter("nombresSolicitante", obj.getNombreSolicitante());
            processStored.setParameter("apePaternoSolicitante", obj.getApellidoPaternoSolicitante());
            processStored.setParameter("apeMaternoSolicitante", obj.getApellidoMaternoSolicitante());
            processStored.setParameter("numeroDocumentoRepresentante", obj.getNumeroDocumento());
            processStored.setParameter("nombresRepresentante", obj.getNombresRepresentante());
            processStored.setParameter("idPersona", obj.getIdPersona());
            processStored.setParameter("idDepartamento", obj.getIdDepartamento());
            processStored.setParameter("idProvincia", obj.getIdProvincia());
            processStored.setParameter("idGeometria", obj.getIdGeometria());
            processStored.setParameter("codigoTH", obj.getCodigoTH());
            processStored.execute();

            Integer idSolBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocal");
            obj.setIdSolBosqueLocal(idSolBosqueLocal);
            result.setCodigo(idSolBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó la actividad de la solicitud bosque Local correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se actualizó la actividad de la solicitud Bosque Local");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }
    @Override
    public ResultClassEntity actualizarEstadoSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_ActualizarEstado");
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitudEnviada", Boolean.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            processStored.setParameter("idUsuario", obj.getIdUsuarioModificacion());
            processStored.setParameter("solicitudEnviada", obj.getSolicitudEnviada());

            processStored.execute();

            Integer idSolBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocal");
            obj.setIdSolBosqueLocal(idSolBosqueLocal);
            result.setCodigo(idSolBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó el estado de la solicitud bosque Local correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se actualizó el estado de la solicitud Bosque Local");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Eliminar");
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolBosqueLocal = (Integer) processStored.getOutputParameterValue("idSolBosqueLocal");
            obj.setIdSolBosqueLocal(idSolBosqueLocal);
            result.setCodigo(idSolBosqueLocal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se elimino la actividad de la solicitud bosque Local correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo aliminar la actividad de la solicitud Bosque Local");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity obtenerSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalEntity> lista = new ArrayList<SolicitudBosqueLocalEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Obtener");

            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalEntity();
                        c.setIdSolBosqueLocal(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolicitante(row[1]==null?null:(Integer) row[1]);
                        c.setTipoPersonaSolicitante(row[2]==null?null:(String) row[2]);
                        c.setTipoDocumentoSolicitante(row[3]==null?null:(String) row[3]);
                        c.setNumeroDocumentoSolicitante(row[4]==null?null:(String) row[4]);
                        c.setNombreSolicitante(row[5]==null?null:(String) row[5]);
                        c.setGobiernoLocal(row[6]==null?null:(String) row[6]);
                        c.setIdDistritoSolicitante(row[7]==null?null:(String) row[7]);
                        c.setNombreDistritoSolicitante(row[8]==null?null:(String) row[8]);
                        c.setNombreProvinciaSolicitante(row[9]==null?null:(String) row[9]);
                        c.setNombreDepartamentoSolicitante(row[10]==null?null:(String) row[10]);
                        c.setDireccionSolicitante(row[11]==null?null:(String) row[11]);
                        c.setIdReprLegal(row[12]==null?null:(Integer) row[12]);
                        c.setTipoPersonaReprLegal(row[13]==null?null:(String) row[13]);
                        c.setTipoDocumentoReprLegal(row[14]==null?null:(String) row[14]);
                        c.setNumeroDocumentoReprLegalUnico(row[15]==null?null:(String) row[15]);
                        c.setRepresentanteLegal(row[16]==null?null:(String) row[16]);
                        c.setCorreoElectronicoReprLegal(row[17]==null?null:(String) row[17]);
                        c.setIdDepartamentoReprLegal(row[18]==null?null:(Integer) row[18]);
                        c.setIdProvinciaReprLegal(row[19]==null?null:(Integer) row[19]);
                        c.setIdDistritoReprLegal(row[20]==null?null:(String) row[20]);
                        c.setNombreDepartamentoReprLegal(row[21]==null?null:(String) row[21]);
                        c.setNombreProvinciaReprLegal(row[22]==null?null:(String) row[22]);
                        c.setNombreDistritoReprLegal(row[23]==null?null:(String) row[23]);
                        c.setFechaPresentacionTexto(row[24]==null?null:(String) row[24]);
                        c.setResultadoSolicitud(row[25]==null?null:(String) row[25]);
                        c.setEstado(row[26]==null?null:(String) row[26]);
                        c.setEstadoSolicitud(row[27]==null?null:(String) row[27]);
                        c.setEstadoSolicitudTexto(row[28]==null?null:(String) row[28]);
                        c.setNombresSolicitante(row[29]==null?null:(String) row[29]);
                        c.setApellidoMaternoSolicitante(row[30]==null?null:(String) row[30]);
                        c.setApellidoPaternoSolicitante(row[31]==null?null:(String) row[31]);
                        c.setNombresRepresentante(row[32]==null?null:(String) row[32]);
                        c.setIdPersona(row[33]==null?null:(Integer) row[33]);
                        c.setNumeroDocumento(row[34]==null?null:(String) row[34]);
                        c.setIdProvincia(row[35]==null?null:(Integer) row[35]);
                        c.setIdDepartamento(row[36]==null?null:(Integer) row[36]);
                        c.setCodigoTH(row[37]==null?null:(String) row[37]);
                        c.setIdGeometria(row[38]==null?null:(Integer) row[38]);

                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Obtener solicitud bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Obtener solicitud bosque local");
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarUsuarioSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalEntity> lista = new ArrayList<SolicitudBosqueLocalEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_ListarUsuario");

            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("numeroDocumento",obj.getNumeroDocumento());
            processStored.setParameter("apellidoPaterno",obj.getApellidoPaternoUsuario());
            processStored.setParameter("apellidoMaterno",obj.getApellidoMaternoUsuario());
            processStored.setParameter("perfil",obj.getPerfilUsuario());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalEntity();

                        c.setIdPersona(row[0]==null?null:(Integer) row[0]);
                        c.setIdUsuario(row[1]==null?null:(Integer) row[1]);
                        c.setNombresUsuario(row[2]==null?null:(String) row[2]);
                        c.setTelefonoUsuario(row[3]==null?null:(String) row[5]);
                        c.setNumeroDocumentoUsuario(row[4]==null?null:(String) row[4]);
                        c.setEmailUsuario(row[5]==null?null:(String) row[5]);
                        c.setPerfilUsuario(row[6]==null?null:(String) row[6]);
                        c.setIdDistritoUsuario(row[7]==null?null:(Integer) row[7]);
                        c.setIdProvinciaUsuario(row[8]==null?null:(Integer) row[8]);
                        c.setIdDepartamento(row[9]==null?null:(Integer) row[9]);
                        c.setNombreDepartamentoUsuario(row[10]==null?null:(String) row[10]);
                        lista.add(c);


                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Usuario solicitud bosque local correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Usuario solicitud bosque local");
            return  result;
        }
    }


    @Override
    public ResultClassEntity generarTHSolicitudBosqueLocal(SolicitudBosqueLocalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalEntity> lista = new ArrayList<SolicitudBosqueLocalEntity>();
        SolicitudBosqueLocalEntity c = null;
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_BosqueLocal_GENERARTH");

            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTituloHabilitante", Integer.class, ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());
            processStored.setParameter("idUsuarioRegistro",obj.getIdUsuarioRegistro());


            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalEntity();

                        c.setIdSolBosqueLocalGenerarTH(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolBosqueLocal(row[1]==null?null:(Integer) row[1]);
                        c.setCodigoTH(row[2]==null?null:(String) row[2]);




                    }
                }

            }
            Integer idTituloHabilitante = (Integer) processStored.getOutputParameterValue("idTituloHabilitante");
            result.setCodigo(idTituloHabilitante);
            result.setData(c);

            result.setSuccess(true);
            result.setMessage("Se generó el Título TH correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Usuario solicitud bosque local");
            return  result;
        }
    }



}