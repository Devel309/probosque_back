package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionActividadRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudConcesionActividadRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionActividadRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionActividadEntity> lista = new ArrayList<SolicitudConcesionActividadEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionActividad_ListarFiltro");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("periodo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion",obj.getIdSolicitudConcesion());
            processStored.setParameter("idSolicitudConcesionActividad",obj.getIdSolicitudConcesionActividad());
            processStored.setParameter("codigoActividad",obj.getCodigoActividad());
            processStored.setParameter("periodo", obj.getPeriodo());


            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudConcesionActividadEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionActividadEntity();
                        c.setIdSolicitudConcesionActividad(row[0]==null?null:(Integer) row[0]);
                        c.setIdSolicitudConcesion(row[1]==null?null:(Integer) row[1]);
                        c.setCodigoActividad((String) row[2]);
                        c.setNombre(row[3]==null?null:(String) row[3]);
                        c.setDescripcion(row[4]==null?null:(String) row[4]);
                        c.setDescripcionRecurso(row[5]==null?null:(String) row[5]);
                        c.setPeriodo(row[6]==null?null:(Integer) row[6]);
                        c.setPresupuesto(row[7]==null?null:((BigDecimal) row[7]).doubleValue());                       
                        c.setComplementario((Boolean) row[8]);

                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Actividad Solicitud Concesion correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarActividadSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Actividad Solicitud Concesion.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionActividad_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionActividad", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionRecursoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("periodo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("presupuesto", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("complementario", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("codigoActividad", obj.getCodigoActividad());
            processStored.setParameter("nombre", obj.getNombre());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("descripcionRecursoAprovechamiento", obj.getDescripcionRecurso());
            processStored.setParameter("periodo", obj.getPeriodo());
            processStored.setParameter("presupuesto", obj.getPresupuesto());
            processStored.setParameter("complementario", obj.getComplementario());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolicitudConcesionActividad = (Integer) processStored.getOutputParameterValue("idSolicitudConcesionActividad");
            obj.setIdSolicitudConcesionActividad(idSolicitudConcesionActividad);
            result.setCodigo(idSolicitudConcesionActividad);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró la actividad de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la actividad de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarActividadSolicitudConcesion(SolicitudConcesionActividadEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionActividad_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionRecursoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("periodo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("presupuesto", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("complementario", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionActividad", obj.getIdSolicitudConcesionActividad());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("codigoActividad", obj.getCodigoActividad());
            processStored.setParameter("nombre", obj.getNombre());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("descripcionRecursoAprovechamiento", obj.getDescripcionRecurso());
            processStored.setParameter("periodo", obj.getPeriodo());
            processStored.setParameter("presupuesto", obj.getPresupuesto());
            processStored.setParameter("complementario", obj.getComplementario());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó la actividad de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar la actividad de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }
}
