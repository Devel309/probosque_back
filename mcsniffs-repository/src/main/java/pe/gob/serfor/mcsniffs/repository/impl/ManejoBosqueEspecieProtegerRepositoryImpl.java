package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueEspecieProtegerRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManejoBosqueEspecieProtegerRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueEspecieProtegerRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueEspecieProtegerRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosqueEspecieProteger(List<ManejoBosqueEspecieProtegerEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueEspecieProtegerEntity> list = new ArrayList<ManejoBosqueEspecieProtegerEntity>();
            for(ManejoBosqueEspecieProtegerEntity p:param){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueEspecieProteger_Registrar");
                processStored.registerStoredProcedureParameter("idManBosqueEspPro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoEspecie", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idEspecie", short.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("justification", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idManBosqueEspPro", p.getIdManBosqueEspPro());
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("idTipoEspecie", p.getIdTipoEspecie());
                processStored.setParameter("tipoEspecie", p.getTipoEspecie());
                processStored.setParameter("idEspecie", p.getIdEspecie());
                processStored.setParameter("especie", p.getEspecie());
                processStored.setParameter("justification", p.getJustification());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult_ =processStored.getResultList();

                if (spResult_.size() >= 1) {
                    for (Object[] row : spResult_) {
                        ManejoBosqueEspecieProtegerEntity obj = new ManejoBosqueEspecieProtegerEntity();
                        obj.setIdManBosqueEspPro((Integer) row[0]);
                        obj.setIdPlanManejo((Integer) row[1]);
                        obj.setIdTipoEspecie((String) row[2]);
                        obj.setTipoEspecie((String) row[3]);
                        obj.setIdEspecie((short) row[4]);
                        obj.setEspecie((String) row[5]);
                        obj.setJustification((String) row[6]);
                        list.add(obj);
                    }
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró especificación de camino.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueEspecieProteger(ManejoBosqueEspecieProtegerEntity manejoBosqueEspecieProteger) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueEspecieProteger_Eliminar");
            processStored.registerStoredProcedureParameter("idManBosqueEspPro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManBosqueEspPro", manejoBosqueEspecieProteger.getIdManBosqueEspPro());
            processStored.setParameter("idUsuarioElimina", manejoBosqueEspecieProteger.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó registro.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarManejoBosqueEspecieProteger(PlanManejoEntity planManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ManejoBosqueEspecieProtegerEntity> list = new ArrayList<ManejoBosqueEspecieProtegerEntity>();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueEspecieProteger_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", planManejo.getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();

            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    ManejoBosqueEspecieProtegerEntity obj = new ManejoBosqueEspecieProtegerEntity();
                    obj.setIdManBosqueEspPro((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setIdTipoEspecie((String) row[2]);
                    obj.setTipoEspecie((String) row[3]);
                    obj.setIdEspecie((short) row[4]);
                    obj.setEspecie((String) row[5]);
                    obj.setJustification((String) row[6]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
