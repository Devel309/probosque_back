package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoArchivoDto;

public interface ContratoArchivoRepository {

    ResultClassEntity registrarContratoArchivo(ContratoArchivoDto dto) throws Exception;
    List<ContratoArchivoDto> listarContratoArchivo(ContratoArchivoDto dto) throws Exception;
    ResultClassEntity eliminarContratoArchivo(ContratoArchivoDto dto) throws Exception;
}   
