package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueEspecieProtegerEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueEspecieProtegerRepository {
    ResultClassEntity RegistrarManejoBosqueEspecieProteger(List<ManejoBosqueEspecieProtegerEntity> manejoBosqueEspecieProteger) throws Exception;
    ResultClassEntity EliminarManejoBosqueEspecieProteger(ManejoBosqueEspecieProtegerEntity manejoBosqueEspecieProteger) throws Exception;
    ResultClassEntity ListarManejoBosqueEspecieProteger(PlanManejoEntity planManejo) throws Exception;
}
