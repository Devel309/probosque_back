package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.AspectoHidrograficoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface AspectoHidrograficoRepository {
   ResultClassEntity RegistrarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception;
   ResultClassEntity ActualizarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception;
   ResultClassEntity ListarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception;
   ResultClassEntity EliminarAspectoHidrografico(AspectoHidrograficoEntity aspectoHidrograficoEntity) throws Exception;

}
