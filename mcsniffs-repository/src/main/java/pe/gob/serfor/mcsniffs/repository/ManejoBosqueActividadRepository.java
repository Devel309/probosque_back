package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import java.util.List;

public  interface  ManejoBosqueActividadRepository {
    ResultClassEntity RegistrarManejoBosqueActividad(List<ManejoBosqueActividadEntity> list)throws Exception;
    ResultClassEntity ListarManejoBosqueActividad(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarManejoBosqueActividad(ManejoBosqueActividadEntity param)throws Exception;
}
