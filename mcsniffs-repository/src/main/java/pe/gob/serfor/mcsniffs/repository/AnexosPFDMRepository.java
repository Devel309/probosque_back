package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;

public interface AnexosPFDMRepository {
    ResultArchivoEntity generarAnexo1PFDM(AnexosPFDMResquestEntity filtro);
}
