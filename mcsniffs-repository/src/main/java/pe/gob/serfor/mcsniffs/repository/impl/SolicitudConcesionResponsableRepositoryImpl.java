package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionResponsableRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudConcesionResponsableRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionResponsableRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionResponsableRepositoryImpl.class);

    @Override
    public ResultClassEntity obtenerSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) {
        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionResponsable_ListarFiltro");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apePaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apeMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("email", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("profesion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroColegiatura", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionResponsable", null);
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("nombres", null);
            processStored.setParameter("apePaterno", null);
            processStored.setParameter("apeMaterno", null);
            processStored.setParameter("numeroDocumento", null);
            processStored.setParameter("email", null);
            processStored.setParameter("profesion", null);
            processStored.setParameter("numeroColegiatura", null);
            processStored.execute();

            SolicitudConcesionResponsableDto c = null;
            c = new SolicitudConcesionResponsableDto();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {

                if (spResult.size() >= 1) {

                    Object[] row = spResult.get(0);


                    c.setIdSolicitudConcesionResponsable((Integer) row[0]);
                    c.setIdSolicitudConcesion((Integer) row[1]);
                    c.setNombres((String) row[2]);
                    c.setApellidoPaterno((String) row[3]);
                    c.setApellidoMaterno( (String) row[4]);
                    c.setCodigoTipoDocumento((String) row[5]);
                    c.setNumeroDocumento((String) row[6]);
                    c.setSector((String) row[7]);
                    c.setUbigeo( (Integer) row[8]);
                    c.setEmail((String) row[9]);
                    c.setProfesion((String) row[10]);
                    c.setNumeroColegiatura((String) row[11]);
                    c.setIdDepartamento((Integer) row[12]);
                    c.setIdProvincia((Integer) row[13]);
                    c.setIdDistrito((Integer) row[14]);
                    c.setNombreDepartamento((String) row[15]);
                    c.setNombreProvincia((String) row[16]);
                    c.setNombreDistrito((String) row[17]);
                    c.setTelefonoCelular((String) row[18]);
                    c.setTelefonoFijo((String) row[19]);
                    c.setDireccion((String) row[20]);

                }
            }

            result.setData(c);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción obtener  Solicitud Concesion correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarActividadSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción obtener Solicitud Concesion.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionReponsable_Registrar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sector", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoFijo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoCelular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("email", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("profesion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroColegiatura", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class, ParameterMode.INOUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("nombres", obj.getNombres());
            processStored.setParameter("apellidoPaterno", obj.getApellidoPaterno());
            processStored.setParameter("apellidoMaterno", obj.getApellidoMaterno());
            processStored.setParameter("codigoTipoDocumento", obj.getCodigoTipoDocumento());
            processStored.setParameter("numeroDocumento", obj.getNumeroDocumento());
            processStored.setParameter("sector", obj.getSector());
            processStored.setParameter("ubigeo", obj.getIdDistrito());
            processStored.setParameter("telefonoFijo", obj.getTelefonoFijo());
            processStored.setParameter("telefonoCelular", obj.getTelefonoCelular());
            processStored.setParameter("email", obj.getEmail());
            processStored.setParameter("profesion", obj.getProfesion());
            processStored.setParameter("numeroColegiatura", obj.getNumeroColegiatura());
            processStored.setParameter("direccion", obj.getDireccion());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolicitudConcesionResponsable = (Integer) processStored.getOutputParameterValue("idSolicitudConcesionResponsable");
            obj.setIdSolicitudConcesionResponsable(idSolicitudConcesionResponsable);
            result.setCodigo(idSolicitudConcesionResponsable);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el responsable correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el responsable.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionReponsable_Actualizar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionResponsable", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sector", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoFijo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoCelular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("email", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("profesion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroColegiatura", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionResponsable", obj.getIdSolicitudConcesionResponsable());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("nombres", obj.getNombres());
            processStored.setParameter("apellidoPaterno", obj.getApellidoPaterno());
            processStored.setParameter("apellidoMaterno", obj.getApellidoMaterno());
            processStored.setParameter("codigoTipoDocumento", obj.getCodigoTipoDocumento());
            processStored.setParameter("numeroDocumento", obj.getNumeroDocumento());
            processStored.setParameter("sector", obj.getSector());
            processStored.setParameter("ubigeo", obj.getIdDistrito());
            processStored.setParameter("telefonoFijo", obj.getTelefonoFijo());
            processStored.setParameter("telefonoCelular", obj.getTelefonoCelular());
            processStored.setParameter("email", obj.getEmail());
            processStored.setParameter("profesion", obj.getProfesion());
            processStored.setParameter("numeroColegiatura", obj.getNumeroColegiatura());
            processStored.setParameter("direccion", obj.getDireccion());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó al responsable.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }





}
