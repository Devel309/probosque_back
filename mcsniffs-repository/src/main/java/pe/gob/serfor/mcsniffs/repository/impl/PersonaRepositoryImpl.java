package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.PersonaEntity;
import pe.gob.serfor.mcsniffs.repository.PersonaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository("repositoryPersona")
public class PersonaRepositoryImpl extends JdbcDaoSupport implements PersonaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }
    
    
    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Abner Valdez [06-12-2021]
     * @descripción: {Registrar la oposicion hacia una postulacion}
     * @param:OposicionEntity
     */
    @Override
    public Integer registrarPersona(PersonaEntity item) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Persona_Registrar");
            processStored.registerStoredProcedureParameter("Nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ApellidoMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ApellidoPaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("RazonSocialEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Telefono", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("NumeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoActor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Direccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCNCC", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Correo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("esRepresentanteLegal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("personaQueRepresenta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdSolicitudAcceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdPersona", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("Nombres",item.getNombres());
            processStored.setParameter("ApellidoMaterno", item.getApellidoMaterno());
            processStored.setParameter("ApellidoPaterno", item.getApellidoPaterno());
            processStored.setParameter("RazonSocialEmpresa", item.getRazonSocialEmpresa());
            processStored.setParameter("Telefono", item.getTelefono());
            processStored.setParameter("tipoDocumento", item.getTipo_documento());
            processStored.setParameter("NumeroDocumento", item.getNumeroDocumento());
            processStored.setParameter("tipoPersona", item.getTipoPersona());
            processStored.setParameter("tipoActor", item.getTipoActor());
            processStored.setParameter("idDistrito", item.getIdDistrito());
            processStored.setParameter("Direccion", item.getDireccion());
            processStored.setParameter("tipoCNCC", item.getTipoCNCC());
            processStored.setParameter("Correo", item.getCorreo());
            processStored.setParameter("esRepresentanteLegal", item.getEs_repr_legal());
            processStored.setParameter("personaQueRepresenta", item.getPersonaQueRepresenta());
            processStored.setParameter("IdUsuarioRegistro", item.getIdUsuario());
            processStored.setParameter("IdSolicitudAcceso", item.getIdSolicitudAcceso());
            processStored.execute();
            Integer idOposicion = (Integer) processStored.getOutputParameterValue("IdPersona");
            return idOposicion;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0;
        }
    }

    @Override
    public PersonaEntity ObtnerPersonaPorNumDocumento(String numDocumento) throws Exception {
        PersonaEntity persona = new PersonaEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Persona_ObtenerPorNumDocumento");
            processStored.registerStoredProcedureParameter("numDocumento", String.class, ParameterMode.IN);

            processStored.setParameter("numDocumento", numDocumento);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {
                        persona.setIdPersona((Integer) row[0]);
                        persona.setNombres((String) row[1]);
                        persona.setRazonSocialEmpresa((String) row[2]);
                        persona.setTipo_documento((String) row[3]);
                        persona.setNumeroDocumento((String) row[4]);
                    }
                }
            }
            return persona;
        } catch (Exception e) {
            log.error("ObtnerPersonaPorNumDocumento", e.getMessage());
            throw e;
        }
    }
}
