package pe.gob.serfor.mcsniffs.repository.impl;

import org.springframework.stereotype.Repository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralPlanificacionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralPlanificacionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @autor: Harry Coa [02-08-2021]
 * @modificado:
 * @descripción: { Repositorio Regente }
 *
 */
@Repository
public class InformacionGeneralPlanificacionBosqueRepositoryImpl extends JdbcDaoSupport implements InformacionGeneralPlanificacionBosqueRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.InformacionGeneralPlanificacionBosqueRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    @Override
    public ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralPlanificacionBosqueEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneralPlanificacion_Registrar");
        processStored.registerStoredProcedureParameter("idInformacionGeneral", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoInfoGeneral", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("nombreTitular", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("docIdentidadTitular", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("rucTitular", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("domicilioLegal", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("departamento", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("provincia", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("distrito", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("nombreRepresentanteLegal", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("rucRepLegal", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("objetivoGeneral", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("numeroPO", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("duracion", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("areaTotalConcesion", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("potencialMaderable", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("potencialNoMaderable", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("nroBloquesQuinquenales", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("volumenCortaPermisible", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("nroPcPo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("areaPcPo", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("areaBosqueHa", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("areaProteccionHa", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("volumenTotalSolicitado", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("volumenNoMaderable", Double.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idRegente", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idInformacionGeneral", param.getIdInformacionGeneral());
        processStored.setParameter("tipoInfoGeneral", param.getTipoInfoGeneral());
        processStored.setParameter("nombreTitular", param.getNombreTitular());
        processStored.setParameter("docIdentidadTitular", param.getDocIdentidadTitular());
        processStored.setParameter("rucTitular", param.getRucTitular());
        processStored.setParameter("domicilioLegal", param.getDomicilioLegal());
        processStored.setParameter("departamento", param.getDepartamento());
        processStored.setParameter("provincia", param.getProvincia());
        processStored.setParameter("distrito", param.getDistrito());
        processStored.setParameter("nombreRepresentanteLegal", param.getNombreRepresentanteLegal());
        processStored.setParameter("rucRepLegal", param.getRucRepLegal());
        processStored.setParameter("objetivoGeneral", param.getObjetivoGeneral());
        processStored.setParameter("numeroPO", param.getNumeroPO());
        processStored.setParameter("fechaPresentacion", param.getFechaPresentacion());
        processStored.setParameter("duracion", param.getDuracion());
        processStored.setParameter("fechaInicio", param.getFechaInicio());
        processStored.setParameter("fechaFin", param.getFechaFin());
        processStored.setParameter("areaTotalConcesion", param.getAreaTotalConcesion());
        processStored.setParameter("potencialMaderable", param.getPotencialMaderable());
        processStored.setParameter("potencialNoMaderable", param.getPotencialNoMaderable());
        processStored.setParameter("nroBloquesQuinquenales", param.getNroBloquesQuinquenales());
        processStored.setParameter("volumenCortaPermisible", param.getVolumenCortaPermisible());
        processStored.setParameter("nroPcPo", param.getNroPcPo());
        processStored.setParameter("areaPcPo", param.getAreaPcPo());
        processStored.setParameter("areaBosqueHa", param.getAreaBosqueHa());
        processStored.setParameter("areaProteccionHa", param.getAreaProteccionHa());
        processStored.setParameter("volumenTotalSolicitado", param.getVolumenTotalSolicitado());
        processStored.setParameter("volumenNoMaderable", param.getVolumenNoMaderable());
        processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
        processStored.setParameter("idRegente", param.getIdRegente());
        processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
        processStored.execute();
        Object[] objArr = (Object[]) processStored.getSingleResult();
        param.setIdInformacionGeneral((Integer) objArr[0]);
        result.setData(param);
        result.setCodigo(200);
        result.setInformacion("Registro Exitoso");
        result.setSuccess(true);
        result.setMessage("Se registró la información general con éxito.");
        return  result;
        
    }
}
