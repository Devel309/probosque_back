package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudAccesoEntity;

import java.util.List;

public interface ParametroRepository {

    public List<ParametroValorEntity> ListarPorCodigoParametroValor(ParametroValorEntity param) throws Exception;

}
