package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;

import java.util.List;

public interface ProcesoOfertaRepository {
    ResultClassEntity guardarProcesoOferta(ProcesoOfertaEntity obj) throws Exception;
    ResultClassEntity listarProcesoOferta(ProcesoOfertaRequestEntity obj);
    ResultClassEntity actualizarEsttatusProcesoOferta(Integer IdProcesoOferta, Integer IdStatus, Integer IdUsuarioModificacion);
    ResultClassEntity eliminarProcesoOferta(ProcesoOfertaRequestEntity obj)throws Exception;

}
