package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalDto;

import java.util.List;

public interface ProduccionRecursoForestalRepository {
    ResultClassEntity RegistrarProduccionRecursoForestal(List<ProduccionRecursoForestalDto> list)throws Exception;
    ResultClassEntity ListarProduccionRecursoForestal(ProduccionRecursoForestalEntity param)throws Exception;
    ResultClassEntity EliminarProduccionRecursoForestal(ProduccionRecursoForestalEntity param)throws Exception;
    ResultClassEntity EliminarProduccionRecursoForestalEspecie(ProduccionRecursoForestalEspecieEntity param) throws Exception;
}
