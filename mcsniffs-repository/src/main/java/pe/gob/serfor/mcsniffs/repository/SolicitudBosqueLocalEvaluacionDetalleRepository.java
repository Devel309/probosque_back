package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;

import java.util.List;


public interface SolicitudBosqueLocalEvaluacionDetalleRepository {

    ResultClassEntity registrarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj);
    ResultClassEntity actualizarSolicitudBosqueLocalEvaluacionDetalle(List<SolicitudBosqueLocalEvaluacionDetalleDto> listObj);
    ResultClassEntity listarSolicitudBosqueLocalEvaluacionDetalle(SolicitudBosqueLocalEvaluacionDetalleDto obj);

}
