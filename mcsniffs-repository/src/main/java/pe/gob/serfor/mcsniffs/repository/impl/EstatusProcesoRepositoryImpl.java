package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.EstatusProceso.ListarEstadoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import pe.gob.serfor.mcsniffs.repository.EstatusProcesoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EstatusProcesoRepositoryImpl extends JdbcDaoSupport implements EstatusProcesoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.CondicionMinimaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity listarEstadoEstatusProceso(ListarEstadoDto request){
        ResultClassEntity<List<ListarEstadoDto>> result = new ResultClassEntity();

        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_StatusProceso_ListarPorTipoEstatus");
            sp.registerStoredProcedureParameter("tipoStatus", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("tipoStatus", request.getTipoStatus());
            sp.execute();
            List<ListarEstadoDto> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();

            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                ListarEstadoDto temp = null;
                for (Object[] item: spResultDet) {
                    temp = new ListarEstadoDto();
                    temp.setIdStatusProceso(item[0]==null?null:(Integer) item[0]);
                    temp.setDescripcion(item[1]==null?null:(String) item[1]);
                    temp.setTipoStatus(item[2]==null?null:(String) item[2]);
                    resultDet.add(temp);
                }
            }

            result.setData(resultDet);
            result.setSuccess(true);
            result.setMessage("Se listó correctamente.");
            return  result;
        } catch (Exception e) {

            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;

        }
    }

}
