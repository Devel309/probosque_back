package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesPlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface LibroOperacionesPlanManejoRepository {
    ResultClassEntity registrarLibroOperacionesPlanManejo(List<LibroOperacionesPlanManejoDto> dto) throws Exception;
    ResultClassEntity eliminarLibroOperacionesPlanManejo(LibroOperacionesPlanManejoDto dto) throws Exception;



}   
