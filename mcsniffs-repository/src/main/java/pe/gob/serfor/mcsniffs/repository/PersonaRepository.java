package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PersonaEntity;

public interface PersonaRepository {
    Integer registrarPersona(PersonaEntity item) throws Exception;
    PersonaEntity ObtnerPersonaPorNumDocumento(String numDocumento) throws Exception;
}
