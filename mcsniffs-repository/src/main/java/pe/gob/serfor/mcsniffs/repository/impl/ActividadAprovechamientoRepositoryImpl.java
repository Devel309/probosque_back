package pe.gob.serfor.mcsniffs.repository.impl;

import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoDetEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoDetSubEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoParam;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.ActividadAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class ActividadAprovechamientoRepositoryImpl implements ActividadAprovechamientoRepository{
    /**
     * @autor: Jaqueline DiazB [26-11-2021]
     * @modificado:
     * @descripción: {Repository actividades aprovechamiento}
     */
    private static final Logger log = LogManager.getLogger(ActividadSilviculturalRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

     /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {Lista Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultEntity<ActividadAprovechamientoEntity> ListarActvidadAprovechamiento(
            ActividadAprovechamientoParam request) throws Exception {
         ResultEntity<ActividadAprovechamientoEntity> result = new ResultEntity<ActividadAprovechamientoEntity>();
        List<ActividadAprovechamientoEntity> list_ = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamiento_Listar");
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoActividad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoASubTipoctividad", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idPlanManejo", request.getIdPlanManejo());
                sp.setParameter("codigoTipoActividad", request.getCodActvAprove());
                sp.setParameter("codigoASubTipoctividad", request.getCodSubActvAprove());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadAprovechamientoEntity obj_ = new ActividadAprovechamientoEntity();
                        obj_.setIdActvAprove((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setCodActvAprove((String) row_[2]);
                        obj_.setCodSubActvAprove((String) row_[3]);
                        obj_.setMetodologia((String) row_[4]);
                        obj_.setTrochas((String) row_[5]);
                        obj_.setPostes((String) row_[6]);
                        obj_.setLimites((String) row_[7]);
                        obj_.setDistancia((BigDecimal) row_[8]);
                        obj_.setSubdivision((String) row_[9]);
                        obj_.setAnexo((String) row_[10]);
                        obj_.setDimension((BigDecimal) row_[11]);
                        obj_.setSuperficie((BigDecimal) row_[12]);
                        obj_.setNumero((BigDecimal) row_[13]);
                        obj_.setAreaTotal((BigDecimal) row_[14]);
                        obj_.setNroEspecies((Integer) row_[15]);
                        obj_.setNroArboles((Integer) row_[16]);
                        obj_.setDescripcion((String) row_[17]);
                        obj_.setObservacion((String) row_[18]);
                        obj_.setDetalle((String) row_[19]);
                        if(obj_.getIdActvAprove()!=null){
                            ActividadAprovechamientoParam sub = new ActividadAprovechamientoParam();
                            sub.setIdActvAprove(obj_.getIdActvAprove());
                            ResultEntity<ActividadAprovechamientoDetEntity> lstsub= ListarActvidadAprovechamientoDet(sub);
                            obj_.setLstactividadDet(lstsub.getData());
                        }
                        list_.add(obj_);
                    }
                }
            result.setData(list_);
            result.setIsSuccess(true);
            result.setMessage("Se listo la actividad aprovechamiento correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    public ResultEntity<ActividadAprovechamientoDetEntity> ListarActvidadAprovechamientoDet(
            ActividadAprovechamientoParam request) throws Exception {
         ResultEntity<ActividadAprovechamientoDetEntity> result = new ResultEntity<ActividadAprovechamientoDetEntity>();
        List<ActividadAprovechamientoDetEntity> list_ = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamientoDet_Listar");
                sp.registerStoredProcedureParameter("idActividad", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idActividad", request.getIdActvAprove());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadAprovechamientoDetEntity obj_ = new ActividadAprovechamientoDetEntity();
                        obj_.setIdActvAproveDet((Integer) row_[0]);
                        obj_.setIdActvAprove((Integer) row_[1]);
                        obj_.setCodActvAproveDet((String) row_[2]);
                        obj_.setCodSubActvAproveDet((String) row_[3]);
                        obj_.setTipoDetalle((String) row_[4]);
                        obj_.setNumeracion((Integer) row_[5]);
                        obj_.setNombreComun((String) row_[6]);
                        obj_.setNombreCientifica((String) row_[7]);
                        obj_.setFamilia((String) row_[8]);
                        obj_.setLineaProduccion((String) row_[9]);
                        obj_.setDcm((BigDecimal) row_[10]);
                        obj_.setSector((String) row_[11]);
                        obj_.setArea((BigDecimal) row_[12]);
                        obj_.setTotalIndividuos((Integer) row_[13]);
                        obj_.setIndividuosHA((String) row_[14]);
                        obj_.setVolumen((String) row_[15]);
                        obj_.setOperaciones((String) row_[16]);
                        obj_.setUnidad((String) row_[17]);
                        obj_.setCarcteristicaTec((String) row_[18]);
                        obj_.setPersonal((String) row_[19]);
                        obj_.setMaquinariaMaterial((String) row_[20]);
                        obj_.setDescripcion((String) row_[21]);
                        obj_.setDetalle((String) row_[22]);
                        obj_.setObservacion((String) row_[23]);
                        if(obj_.getIdActvAproveDet()!=null){
                            ActividadAprovechamientoParam sub = new ActividadAprovechamientoParam();
                            sub.setIdActvAproveDet(obj_.getIdActvAproveDet());
                            ResultEntity<ActividadAprovechamientoDetSubEntity> lstsub= ListarActvidadAprovechamientoDetSub(sub);
                            obj_.setLstactividadDetSub(lstsub.getData());
                        }
                        list_.add(obj_);
                    }
                }
            result.setData(list_);
            result.setIsSuccess(true);
            result.setMessage("Se listo la actividad aprovechamiento detalle correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }
    
    public ResultEntity<ActividadAprovechamientoDetSubEntity> ListarActvidadAprovechamientoDetSub(
            ActividadAprovechamientoParam request) throws Exception {
         ResultEntity<ActividadAprovechamientoDetSubEntity> result = new ResultEntity<ActividadAprovechamientoDetSubEntity>();
        List<ActividadAprovechamientoDetSubEntity> list_ = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamientoDetSub_Listar");
                sp.registerStoredProcedureParameter("idActividadDet", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idActividadDet", request.getIdActvAproveDet());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadAprovechamientoDetSubEntity obj_ = new ActividadAprovechamientoDetSubEntity();
                        obj_.setIdActvAproveDetSub((Integer) row_[0]);
                        obj_.setIdActvAproveDet((Integer) row_[1]);
                        obj_.setCodActvAproveDetSub((String) row_[2]);
                        obj_.setCodSubActvAproveDetSub((String) row_[3]);
                        obj_.setVar((String) row_[4]);
                        obj_.setTotalHa((BigDecimal) row_[5]);
                        obj_.setTotalPC((BigDecimal) row_[6]);
                        obj_.setDapCM((String) row_[7]);
                        obj_.setArea((BigDecimal) row_[8]);
                        obj_.setNroPC((Integer) row_[9]);
                        obj_.setNroArbol((Integer) row_[10]);
                        obj_.setVolM((BigDecimal) row_[11]);
                        obj_.setDescripcion((String) row_[12]);
                        obj_.setDetalle((String) row_[13]);
                        obj_.setObservacion((String) row_[14]);
                        list_.add(obj_);
                    }
                }
            result.setData(list_);
            result.setIsSuccess(true);
            result.setMessage("Se listo la actividad aprovechamiento detalle sub correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

     /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {registra Aprovechamiento Actividad}
     * @param: List<ActividadAprovechamientoEntity>
     */
    @Override
    public ResultClassEntity RegistrarActvidadAprovechamiento(List<ActividadAprovechamientoEntity> request)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ActividadAprovechamientoEntity> list_ = new ArrayList<>();
        try {
            for (ActividadAprovechamientoEntity p : request) {
                StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamiento_Registrar");
                sp.registerStoredProcedureParameter("idActividad", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoActividad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoASubTipoctividad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("metodologia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("trochas", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("postes", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("limitesNaturales", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("distanciaM", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subdivision", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("dimension", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("superficie", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("numero", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("areaTotal", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroEspecies", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroArboles", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idActividad", p.getIdActvAprove());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codigoTipoActividad", p.getCodActvAprove());
                sp.setParameter("codigoASubTipoctividad", p.getCodSubActvAprove());
                sp.setParameter("metodologia", p.getMetodologia());
                sp.setParameter("trochas", p.getTrochas());
                sp.setParameter("postes", p.getPostes());
                sp.setParameter("limitesNaturales", p.getLimites());
                sp.setParameter("distanciaM", p.getDistancia());
                sp.setParameter("subdivision", p.getSubdivision());
                sp.setParameter("anexo", p.getAnexo());
                sp.setParameter("dimension", p.getDimension());
                sp.setParameter("superficie", p.getSuperficie());
                sp.setParameter("numero", p.getNumero());
                sp.setParameter("areaTotal", p.getAreaTotal());
                sp.setParameter("nroEspecies", p.getNroEspecies());
                sp.setParameter("nroArboles", p.getNroArboles());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadAprovechamientoEntity obj_ = new ActividadAprovechamientoEntity();
                        obj_ = p;
                        obj_.setIdActvAprove((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setCodActvAprove((String) row_[2]);
                        obj_.setCodSubActvAprove((String) row_[3]);
                        List<ActividadAprovechamientoDetEntity> listDet = new ArrayList<>();
                        if(p.getLstactividadDet()!=null){
                            List<ActividadAprovechamientoDetEntity> listDet2 = new ArrayList<>();
                            for (ActividadAprovechamientoDetEntity obj : p.getLstactividadDet()) {
                                obj.setIdActvAprove( obj_.getIdActvAprove());
                                listDet2.add(obj);
                            }
                            ResultEntity<ActividadAprovechamientoDetEntity> resultdet= RegistrarAccesibilidadManejoRequisitoDet(listDet2);
                            listDet = resultdet.getData();
                        }
                        obj_.setLstactividadDet(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la actividad de aprovechamiento correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    public ResultEntity<ActividadAprovechamientoDetEntity> RegistrarAccesibilidadManejoRequisitoDet(List<ActividadAprovechamientoDetEntity> request)
            throws Exception {
        ResultEntity<ActividadAprovechamientoDetEntity> result = new ResultEntity<ActividadAprovechamientoDetEntity>();
        List<ActividadAprovechamientoDetEntity> list_ = new ArrayList<>();
        try {
            for (ActividadAprovechamientoDetEntity p : request) {
                StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamientoDet_Registrar");
                sp.registerStoredProcedureParameter("idActividadDet", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idActividad", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoActividadDet", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoASubTipoctividadDet", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoDetalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("numeracion", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("familia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("lineaProduccion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("dcm", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("sector", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("totalIndividuos", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("individuosHA", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("volumen", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("operaciones", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("unidad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("caracteristicasTec", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("personal", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("maquinariaMaterial", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idActividadDet", p.getIdActvAproveDet());
                sp.setParameter("idActividad", p.getIdActvAprove());
                sp.setParameter("codigoTipoActividadDet", p.getCodActvAproveDet());
                sp.setParameter("codigoASubTipoctividadDet", p.getCodSubActvAproveDet());
                sp.setParameter("tipoDetalle", p.getTipoDetalle());
                sp.setParameter("numeracion", p.getNumeracion());
                sp.setParameter("nombreComun", p.getNombreComun());
                sp.setParameter("nombreCientifico", p.getNombreCientifica());
                sp.setParameter("familia", p.getFamilia());
                sp.setParameter("lineaProduccion", p.getLineaProduccion());
                sp.setParameter("dcm", p.getDcm());
                sp.setParameter("sector", p.getSector());
                sp.setParameter("area", p.getArea());
                sp.setParameter("totalIndividuos", p.getTotalIndividuos());
                sp.setParameter("individuosHA", p.getIndividuosHA());
                sp.setParameter("volumen", p.getVolumen());
                sp.setParameter("operaciones", p.getOperaciones());
                sp.setParameter("unidad", p.getUnidad());
                sp.setParameter("caracteristicasTec", p.getCarcteristicaTec());
                sp.setParameter("personal", p.getPersonal());
                sp.setParameter("maquinariaMaterial", p.getMaquinariaMaterial());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadAprovechamientoDetEntity obj_ = new ActividadAprovechamientoDetEntity();
                        obj_ = p;
                        obj_.setIdActvAproveDet((Integer) row_[0]);
                        obj_.setIdActvAprove((Integer) row_[1]);
                        obj_.setCodActvAproveDet((String) row_[2]);
                        obj_.setCodSubActvAproveDet((String) row_[3]);
                        List<ActividadAprovechamientoDetSubEntity> listDet = new ArrayList<>();
                        if(p.getLstactividadDetSub()!=null){
                            List<ActividadAprovechamientoDetSubEntity> listDet2 = new ArrayList<>();
                            for (ActividadAprovechamientoDetSubEntity obj : p.getLstactividadDetSub()) {
                                obj.setIdActvAproveDet( obj_.getIdActvAproveDet());
                                listDet2.add(obj);
                            }
                            ResultEntity<ActividadAprovechamientoDetSubEntity> resultdetsub= RegistrarAccesibilidadManejoRequisitoDetSub(listDet2);
                            listDet = resultdetsub.getData();
                        }
                        obj_.setLstactividadDetSub(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setIsSuccess(true);
            result.setMessage("Se registró la actividad aprovechamiento detalle correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    public ResultEntity<ActividadAprovechamientoDetSubEntity> RegistrarAccesibilidadManejoRequisitoDetSub(List<ActividadAprovechamientoDetSubEntity> request)
            throws Exception {
            ResultEntity<ActividadAprovechamientoDetSubEntity> result = new ResultEntity<ActividadAprovechamientoDetSubEntity>();
        List<ActividadAprovechamientoDetSubEntity> list_ = new ArrayList<>();
        try {
            for (ActividadAprovechamientoDetSubEntity p : request) {
                StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamientoDetSub_Registrar");
                sp.registerStoredProcedureParameter("idActividadDetSub", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idActividadDet", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoActividadDetSub", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoASubTipoctividadDetSub", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("var", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("totalHA", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("TotalPC", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("dapCM", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroPC", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroArbol", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("volM", BigDecimal.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idActividadDetSub", p.getIdActvAproveDetSub());
                sp.setParameter("idActividadDet", p.getIdActvAproveDet());
                sp.setParameter("codigoTipoActividadDetSub", p.getCodActvAproveDetSub());
                sp.setParameter("codigoASubTipoctividadDetSub", p.getCodSubActvAproveDetSub());
                sp.setParameter("var", p.getVar());
                sp.setParameter("totalHA", p.getTotalHa());
                sp.setParameter("TotalPC", p.getTotalPC());
                sp.setParameter("dapCM", p.getDapCM());
                sp.setParameter("area", p.getArea());
                sp.setParameter("nroPC", p.getNroPC());
                sp.setParameter("nroArbol", p.getNroArbol());
                sp.setParameter("volM", p.getVolM());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadAprovechamientoDetSubEntity obj_ = new ActividadAprovechamientoDetSubEntity();
                        obj_ = p;
                        obj_.setIdActvAproveDetSub((Integer) row_[0]);
                        obj_.setIdActvAproveDet((Integer) row_[1]);
                        obj_.setCodActvAproveDetSub((String) row_[2]);
                        obj_.setCodSubActvAproveDetSub((String) row_[3]);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setIsSuccess(true);
            result.setMessage("Se registró la actividad aprovechamiento detalle sub correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity EliminarActvidadAprovechamiento(ActividadAprovechamientoParam request) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamiento_Eliminar");
            sp.registerStoredProcedureParameter("idActividad", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idActividad", request.getIdActvAprove());
            sp.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());
            sp.execute();
            result.setData(request);
            result.setSuccess(true);
            result.setMessage("Se elimino la Actividad aprovechamiento Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad det}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity EliminarActvidadAprovechamientoDet(ActividadAprovechamientoParam request)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamientoDet_Eliminar");
            sp.registerStoredProcedureParameter("idActividadDet", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idActividadDet", request.getIdActvAproveDet());
            sp.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());
            sp.execute();
            result.setData(request);
            result.setSuccess(true);
            result.setMessage("Se elimino la Actividad aprovechamiento Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad det sub}
     * @param: ActividadAprovechamientoParam
     */
    @Override
    public ResultClassEntity EliminarActvidadAprovechamientoDetSub(ActividadAprovechamientoParam request)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadArpovechamientoDetSub_Eliminar");
            sp.registerStoredProcedureParameter("idActividadDetSub", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idActividadDetSub", request.getIdActvAproveDetSub());
            sp.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());
            sp.execute();
            result.setData(request);
            result.setSuccess(true);
            result.setMessage("Se elimino la Actividad aprovechamiento Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrio un error.");
            result.setInformacion(e.getMessage());
            return result;
        }
    }

   

}
