package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionCategoriaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionDivAdmUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProyecionVigilanciaUMFEntity;

public interface OrdenamientoProteccionRepository {
    ResultEntity RegistrarOrdenamientoProteccionCategoria(List<OrdenProteccionCategoriaUMFEntity> request) ;
    ResultEntity EliminarOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request);
    ResultEntity ObtenerOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) ;
  
   //** divicion de administracion */
    ResultEntity RegistrarOrdenamientoProteccionDivAdm(List<OrdenProteccionDivAdmUMFEntity>  request) ;
     ResultEntity EliminarOrdenamientoProteccionDivAdm(OrdenProteccionDivAdmUMFEntity request) ;
    ResultEntity ObtenerOrdenamientoProteccionDivAdm(OrdenProteccionDivAdmUMFEntity request)  ;

    //** vigilancia */
    ResultEntity RegistrarOrdenamientoProteccionVigilancia(List<OrdenProyecionVigilanciaUMFEntity>  request) ;
    ResultEntity EliminarOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request) ;
    ResultEntity ObtenerOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request)  ;



    // ResultEntity RegistrarOrdenamientoProteccionDemarcMant(List<OrdenProyecionVigilanciaUMFEntity>  request) ;
    // ResultEntity EliminarOrdenamientoProteccionDemarcMant(OrdenProyecionVigilanciaUMFEntity request) ;
    // ResultEntity ObtenerOrdenamientoProteccionDemarcMant(OrdenProyecionVigilanciaUMFEntity request)  ;


    ResultEntity ListarOrdenamientoProteccion(OrdenamientoProteccionEntity request);
    ResultEntity ListarOrdenamientoInterno(OrdenamientoProteccionEntity request);
    ResultEntity ListarOrdenamientoInternoDetalle(OrdenamientoProteccionEntity request);
    ResultEntity ListarOrdenamientoInternoDetalleTitular(OrdenamientoProteccionEntity request);
    List<OrdenamientoInternoDetalleDto> ListaROrdenamientoInternoPMFI(OrdenamientoInternoDetalleDto request ) throws Exception;


 ResultClassEntity RegistrarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request);
    ResultClassEntity ActualizarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request);

    List<OrdenamientoProteccionEntity> ListarOrdenamientoProteccionFiltro(Integer idPlanManejo, String codTipoOrdenamiento) throws Exception;
    List<OrdenamientoProteccionDetalleEntity> ListarOrdenamientoProteccionDetalle(Integer idOrdenamientoProteccion, Integer idOrdenamientoProteccionDet) throws Exception;

    ResultClassEntity EliminarOrdenamientoProteccionDetalle(Integer idOrdenamientoProteccionDet, Integer idUsuarioElimina) throws Exception;
    ResultClassEntity ActualizarOrdenamientoProteccionDetalle(OrdenamientoProteccionDetalleEntity request);
    ResultClassEntity RegistrarOrdenamientoProteccionDetalle(OrdenamientoProteccionDetalleEntity request);

    ResultClassEntity registrarOrdenamientoInterno(List<OrdenamientoProteccionEntity> list) throws Exception;
    ResultClassEntity obtenerOrdenamientoInterno(Integer idPlanManejo, String codTipoOrdenamiento) throws Exception;
 ResultClassEntity EliminarOrdenamientoInterno(OrdenamientoProteccionDetalleEntity ordenamientoProteccionDetalleEntity) throws Exception;

 ResultEntity<OrdenamientoProteccionEntity> ListarOrdenamientoInternoCompleto(OrdenamientoProteccionEntity request);


}
