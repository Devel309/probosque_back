package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.OrdenamientoAreaManejoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalEspecieDto;
import pe.gob.serfor.mcsniffs.repository.ProduccionRecursoForestalRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProduccionRecursoForestalRepositoryImpl extends JdbcDaoSupport implements ProduccionRecursoForestalRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ProduccionRecursoForestalRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarProduccionRecursoForestal(List<ProduccionRecursoForestalDto> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ProduccionRecursoForestalDto> list = new ArrayList<ProduccionRecursoForestalDto>();
            for(ProduccionRecursoForestalDto p:param){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestal_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idProdRecursoForestal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoBosque", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoProdRecursoForestal", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("idProdRecursoForestal", p.getIdProdRecursoForestal());
                processStored.setParameter("idTipoBosque", p.getIdTipoBosque());
                processStored.setParameter("tipoBosque", p.getTipoBosque());
                processStored.setParameter("idTipoProdRecursoForestal", p.getIdTipoProdRecursoForestal());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ProduccionRecursoForestalDto obj=new ProduccionRecursoForestalDto();
                        obj.setIdPlanManejo((Integer) row[0]);
                        obj.setIdProdRecursoForestal((Integer) row[1]);
                        obj.setIdTipoProdRecursoForestal((String) row[2]);
                        obj.setIdTipoBosque((String) row[3]);
                        obj.setTipoBosque((String) row[4]);


                        List<ProduccionRecursoForestalEspecieDto> listEsp = new ArrayList<ProduccionRecursoForestalEspecieDto>();
                        for(ProduccionRecursoForestalEspecieDto esp:p.getListproduccionRecursoForestalEspecieDto()){
                            StoredProcedureQuery processStoredEsp = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestalESP_Registrar");
                            processStoredEsp.registerStoredProcedureParameter("idProdRecursoForestal", Integer.class, ParameterMode.IN);
                            processStoredEsp.registerStoredProcedureParameter("idProdRecursoForestalEsp", Integer.class, ParameterMode.IN);
                            processStoredEsp.registerStoredProcedureParameter("idEspecie", String.class, ParameterMode.IN);
                            processStoredEsp.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
                            processStoredEsp.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                            processStoredEsp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            processStoredEsp.setParameter("idProdRecursoForestal", obj.getIdProdRecursoForestal());
                            processStoredEsp.setParameter("idProdRecursoForestalEsp", esp.getIdProdRecursoForestalEsp());
                            processStoredEsp.setParameter("idEspecie", esp.getIdEspecie());
                            processStoredEsp.setParameter("especie", esp.getEspecie());
                            processStoredEsp.setParameter("estado", p.getEstado());
                            processStoredEsp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                            processStoredEsp.execute();

                            List<Object[]> spResultEsp =processStoredEsp.getResultList();
                            if (spResultEsp.size() >= 1) {
                                for (Object[] rowEsp : spResultEsp) {
                                    ProduccionRecursoForestalEspecieDto objEsp=new ProduccionRecursoForestalEspecieDto();
                                    objEsp.setIdProdRecursoForestalEsp((Integer) rowEsp[0]);
                                    objEsp.setIdEspecie((String) rowEsp[1]);
                                    objEsp.setEspecie((String) rowEsp[2]);



                                    List<ProduccionRecursoForestalDetalleEntity> listDet = new ArrayList<ProduccionRecursoForestalDetalleEntity>();
                                    for(ProduccionRecursoForestalDetalleEntity det:esp.getListProduccionRecursoForestalDetalle()){
                                        StoredProcedureQuery processStoredDet = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestalDet_Registrar");
                                        processStoredDet.registerStoredProcedureParameter("idProdRecursoForestalEsp", Integer.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("idProdRecursoForestalDet", Integer.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("idVariable", String.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("variable", String.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("totalTipo", Double.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("totalArea", Double.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                                        processStoredDet.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                                        processStoredDet.setParameter("idProdRecursoForestalEsp", objEsp.getIdProdRecursoForestalEsp());
                                        processStoredDet.setParameter("idProdRecursoForestalDet", det.getIdProdRecursoForestalDet());
                                        processStoredDet.setParameter("idVariable", det.getIdVariable());
                                        processStoredDet.setParameter("variable", det.getVariable());
                                        processStoredDet.setParameter("totalTipo", det.getTotalTipo());
                                        processStoredDet.setParameter("totalArea", det.getTotalArea());
                                        processStoredDet.setParameter("accion", det.getAccion());
                                        processStoredDet.setParameter("estado", p.getEstado());
                                        processStoredDet.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                                        processStoredDet.execute();

                                        List<Object[]> spResultDet =processStoredDet.getResultList();
                                        if (spResultDet.size() >= 1) {
                                            for (Object[] rowdet : spResultDet) {
                                                ProduccionRecursoForestalDetalleEntity objDet=new ProduccionRecursoForestalDetalleEntity();
                                                objDet.setIdProdRecursoForestalDet((Integer) rowdet[0]);
                                                objDet.setIdVariable((String) rowdet[1]);
                                                objDet.setVariable((String) rowdet[2]);
                                                BigDecimal bd=(BigDecimal) rowdet[3];
                                                BigDecimal bd_=(BigDecimal)rowdet[4];
                                                objDet.setTotalTipo((Double) bd.doubleValue());
                                                objDet.setTotalArea((Double) bd_.doubleValue());
                                                objDet.setAccion((Boolean) rowdet[5]);
                                                listDet.add(objDet);
                                            }

                                        }
                                    }
                                    objEsp.setListProduccionRecursoForestalDetalle(listDet);
                                    listEsp.add(objEsp);
                                }
                            }
                        }
                        obj.setListproduccionRecursoForestalEspecieDto(listEsp);
                        list.add(obj);
                    }

                }


            }

            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró Producto de Recurso Forestal correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarProduccionRecursoForestal(ProduccionRecursoForestalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ProduccionRecursoForestalDto> list = new ArrayList<ProduccionRecursoForestalDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestal_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoProdRecursoForestal", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idTipoProdRecursoForestal", param.getIdTipoProdRecursoForestal());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {

                ProduccionRecursoForestalDto obj=new ProduccionRecursoForestalDto();
                    obj.setIdPlanManejo((Integer) row[0]);
                    obj.setIdProdRecursoForestal((Integer) row[1]);
                    obj.setIdTipoProdRecursoForestal((String) row[2]);
                    obj.setIdTipoBosque((String) row[3]);
                    obj.setTipoBosque((String) row[4]);

                    List<ProduccionRecursoForestalEspecieDto> listEsp = new ArrayList<ProduccionRecursoForestalEspecieDto>();
                    StoredProcedureQuery processStoredEsp = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestalEspecie_Listar");
                    processStoredEsp.registerStoredProcedureParameter("idProdRecursoForestal", Integer.class, ParameterMode.IN);
                    processStoredEsp.setParameter("idProdRecursoForestal", obj.getIdProdRecursoForestal());
                    processStoredEsp.execute();
                    List<Object[]> spResultEsp =processStoredEsp.getResultList();
                    if (spResultEsp.size() >= 1) {
                        for (Object[] rowEsp : spResultEsp) {

                            ProduccionRecursoForestalEspecieDto objEsp=new ProduccionRecursoForestalEspecieDto();
                            objEsp.setIdProdRecursoForestalEsp((Integer) rowEsp[0]);
                            objEsp.setIdEspecie((String) rowEsp[1]);
                            objEsp.setEspecie((String) rowEsp[2]);

                            List<ProduccionRecursoForestalDetalleEntity> listDet = new ArrayList<ProduccionRecursoForestalDetalleEntity>();
                            StoredProcedureQuery processStoredDet = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestalDetalle_Listar");
                            processStoredDet.registerStoredProcedureParameter("idProdRecursoForestalEsp", Integer.class, ParameterMode.IN);
                            processStoredDet.setParameter("idProdRecursoForestalEsp", objEsp.getIdProdRecursoForestalEsp());
                            processStoredDet.execute();
                            List<Object[]> spResultDet =processStoredDet.getResultList();
                            if (spResultDet.size() >= 1) {
                                for (Object[] row_ : spResultDet) {
                                    ProduccionRecursoForestalDetalleEntity objDet=new ProduccionRecursoForestalDetalleEntity();
                                    objDet.setIdProdRecursoForestalDet((Integer) row_[0]);
                                    objDet.setIdVariable((String) row_[1]);
                                    objDet.setVariable((String) row_[2]);
                                    BigDecimal bd=(BigDecimal) row_[3];
                                    BigDecimal bd_=(BigDecimal)row_[4];
                                    objDet.setAccion((Boolean) row_[5]);
                                    objDet.setTotalTipo((Double) bd.doubleValue());
                                    objDet.setTotalArea((Double) bd_.doubleValue());
                                    listDet.add(objDet);
                                }
                            }
                            objEsp.setListProduccionRecursoForestalDetalle(listDet);
                            listEsp.add(objEsp);

                        }
                    }
                    obj.setListproduccionRecursoForestalEspecieDto(listEsp);
                    list.add(obj);
                }


            }

            if (list.size() ==0){
                result.setSuccess(true);
                result.setMessage("No se encontraron datos.");
                result.setData(list);
                return result;
            }
            result.setSuccess(true);
            result.setMessage("Se obtuvo Producto de Recurso Forestal correctamente.");
            result.setData(list);
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarProduccionRecursoForestal(ProduccionRecursoForestalEntity param) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestal_Eliminar");
            processStored.registerStoredProcedureParameter("idProdRecursoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idProdRecursoForestal", param.getIdProdRecursoForestal());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();


            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity EliminarProduccionRecursoForestalEspecie(ProduccionRecursoForestalEspecieEntity param) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProduccionRecursoForestalEsp_Eliminar");
            processStored.registerStoredProcedureParameter("idProdRecursoForestalEsp", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idProdRecursoForestalEsp", param.getIdProdRecursoForestalEsp());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();


            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
