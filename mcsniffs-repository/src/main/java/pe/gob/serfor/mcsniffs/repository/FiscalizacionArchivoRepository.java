package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;

public interface FiscalizacionArchivoRepository {

    
    ResultClassEntity registrarFiscalizacionArchivo(FiscalizacionArchivoDto dto) throws Exception;
    List<FiscalizacionArchivoDto> listarFiscalizacionArchivo(FiscalizacionArchivoDto obj);
    ResultClassEntity eliminarFiscalizacionArchivo(FiscalizacionArchivoDto dto) throws Exception;
}   
