package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoEntity;
import pe.gob.serfor.mcsniffs.repository.UnidadManejoRepository;


import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Carlos Tang [20-08-2021]
 * @modificado:
 * @descripción: {Repository unidad manejo}
 */
@Repository
public class UnidadManejoRepositoryImpl extends JdbcDaoSupport implements UnidadManejoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(UnidadManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity RegistrarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_UnidadManejo_Registrar");
            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idDistrito", unidadManejoEntity.getIdDistrito());
            processStored.setParameter("cuenca", unidadManejoEntity.getCuenca());
            processStored.setParameter("idPlanManejo", unidadManejoEntity.getIdPlanManejo());
            processStored.setParameter("estado", unidadManejoEntity.getEstado());
            processStored.setParameter("idUsuarioRegistro", unidadManejoEntity.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró unidad manejo.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_UnidadManejo_Actualizar");
            processStored.registerStoredProcedureParameter("idUnidadManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idUnidadManejo", unidadManejoEntity.getIdUnidadManejo());
            processStored.setParameter("idDistrito", unidadManejoEntity.getIdDistrito());
            processStored.setParameter("cuenca", unidadManejoEntity.getCuenca());
            processStored.setParameter("idPlanManejo", unidadManejoEntity.getIdPlanManejo());
            processStored.setParameter("estado", unidadManejoEntity.getEstado());
            processStored.setParameter("idUsuarioModificacion", unidadManejoEntity.getIdUsuarioModificacion());

            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró unidad manejo.");
        return  result;
    }

    @Override
    public ResultClassEntity ListarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_UnidadManejo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", unidadManejoEntity.getIdPlanManejo());
            processStored.execute();

            //crear el listado
            List<UnidadManejoEntity> listUnidadManejoEntity = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    UnidadManejoEntity temp = new UnidadManejoEntity();
                    temp.setIdUnidadManejo((Integer) row[0]);
                    temp.setIdDistrito((Integer) row[1]);
                    temp.setCuenca((String) row[2]);
                    temp.setIdPlanManejo((Integer) row[3]);
                    listUnidadManejoEntity.add(temp);
                }
            }


            if(listUnidadManejoEntity.size()>=1) result.setData(listUnidadManejoEntity.get(0));


            result.setSuccess(true);
            result.setMessage("Se obtuvo la unidad de manejo correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }



    }
}
