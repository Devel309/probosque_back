package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionArchivoDto;

public interface EvaluacionArchivoRepository {
    List<EvaluacionArchivoDto> listarEvaluacionArchivo(EvaluacionArchivoDto dto) throws Exception;
    ResultClassEntity registrarEvaluacionArchivo(EvaluacionArchivoDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacionArchivo(EvaluacionArchivoDto dto) throws Exception;
}
