package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

import java.util.List;

public interface FeriadoRepository {

    List<FeriadoEntity> ListarFeriados(String codUbbigeo) throws Exception;
    ResultClassEntity RegistrarFeriados(FeriadoEntity feriadoEntity) throws Exception;


}
