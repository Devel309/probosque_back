package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoOfertaSolicitudOpinion.ProcesoOfertaSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaSolicitudOpinionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ProcesoOfertaSolicitudOpinionRepositoryImpl extends JdbcDaoSupport implements ProcesoOfertaSolicitudOpinionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(ProcesoOfertaSolicitudOpinionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoOfertaSolicitudOpinion_Listar");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOferta", param.getIdProcesoOferta());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());
            sp.execute();
            List<ProcesoOfertaSolicitudOpinionDto> list = new ArrayList<ProcesoOfertaSolicitudOpinionDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    ProcesoOfertaSolicitudOpinionDto obj = new ProcesoOfertaSolicitudOpinionDto();
                    obj.setIdProcesoOfertaSolicitudOpinion((Integer) row[0]);
                    obj.setIdProcesoOferta((Integer) row[1]);
                    obj.setIdSolicitudOpinion((Integer) row[2]);
                    obj.setSiglaEntidad((String) row[3]);
                    obj.setAsunto((String) row[4]);
                    obj.setEstado((String) row[5]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }

    }

    @Override
    public ResultClassEntity RegistrarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoOfertaSolicitudOpinion_Registrar");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOferta", param.getIdProcesoOferta());
            sp.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se registró la solicitud de opinión del proceso de oferta correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoOfertaSolicitudOpinion_Actualizar");
            sp.registerStoredProcedureParameter("idProcesoOfertaSolicitudOpinion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOfertaSolicitudOpinion", param.getIdProcesoOfertaSolicitudOpinion());
            sp.setParameter("idProcesoOferta", param.getIdProcesoOferta());
            sp.setParameter("idSolicitudOpinion", param.getIdSolicitudOpinion());
            sp.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Proceso Oferta Solicitud Opinión correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoOfertaSolicitudOpinion_Eliminar");
            sp.registerStoredProcedureParameter("idProcesoOfertaSolicitudOpinion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOfertaSolicitudOpinion", param.getIdProcesoOfertaSolicitudOpinion());
            sp.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó la solicitud de opinión del proceso de oferta correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ObtenerProcesoOfertaSolicitudOpinion(ProcesoOfertaSolicitudOpinionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoOfertaSolicitudOpinion_Obtener");
            sp.registerStoredProcedureParameter("idProcesoOfertaSolicitudOpinion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOfertaSolicitudOpinion", param.getIdProcesoOfertaSolicitudOpinion());
            sp.execute();
            List<ProcesoOfertaSolicitudOpinionDto> list = new ArrayList<ProcesoOfertaSolicitudOpinionDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    ProcesoOfertaSolicitudOpinionDto obj = new ProcesoOfertaSolicitudOpinionDto();
                    obj.setIdProcesoOfertaSolicitudOpinion((Integer) row[0]);
                    obj.setIdProcesoOferta((Integer) row[1]);
                    obj.setIdSolicitudOpinion((Integer) row[2]);
                    obj.setSiglaEntidad((String) row[3]);
                    obj.setAsunto((String) row[4]);
                    obj.setEstado((String) row[5]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }

    }
}
