package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ActividadesDesarrolloPFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo1PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo2PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo3PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo4PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo5PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo6PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.AreaSolicitadaUTMEntity;
import pe.gob.serfor.mcsniffs.entity.ExperienciaLaboralEntity;
import pe.gob.serfor.mcsniffs.entity.FormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.entity.IngresosCuentasEntity;
import pe.gob.serfor.mcsniffs.entity.PostulacionPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.PostulacionPFDMEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.PostulacionPFDMRepository;
// 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Repository
public class PostulacionPFDMRepositoryImpl extends JdbcDaoSupport implements PostulacionPFDMRepository{
    /**
     * @autor: JaquelineDB [06-08-2021]
     * @modificado:
     * @descripción: {Repository de las postulacion pfdme}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PostulacionPFDMRepositoryImpl.class);

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    //@Autowired
    //private AnexoRepository obteneranexo;


    /**
     * @autor: JaquelineDB [06-08-2021]
     * @modificado:
     * @descripción: {Guardar postulacion}
     * @param:PostulacionPFDMEResquestEntity
     */
    @Override
    public ResultClassEntity guardaPostulacion(PostulacionPFDMResquestEntity postulacion) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            if(postulacion.getIdPostulacionPFDM().equals(null) || postulacion.getIdPostulacionPFDM().equals(0)){
                ResultClassEntity primerap = guardarPostulacionPFDME(postulacion.getPostulacionPFDM());
                if(primerap.getSuccess()){
                    //insertar datos del anexo 1
                    if(!(postulacion.getAnexo1()==null)){
                        List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                        //Solicitante
                        lst_data.add(agregarColumnas("soliNombreRS",postulacion.getAnexo1().getSoliNombreRS(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliDni",postulacion.getAnexo1().getSoliDni(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliRuc",postulacion.getAnexo1().getSoliRuc(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliNroPartidaRegistral",postulacion.getAnexo1().getSoliNroPartidaRegistral(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliDireccion",postulacion.getAnexo1().getSoliDireccion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliNroDireccion",postulacion.getAnexo1().getSoliNroDireccion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliSectorCaserio",postulacion.getAnexo1().getSoliSectorCaserio(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliIdDistrito",postulacion.getAnexo1().getSoliIdDistrito(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliIdProvincia",postulacion.getAnexo1().getSoliIdProvincia(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliIdDepartamento",postulacion.getAnexo1().getSoliIdDepartamento(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliTelefono",postulacion.getAnexo1().getSoliTelefono(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliCelular",postulacion.getAnexo1().getSoliCelular(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliCorreoPersonal",postulacion.getAnexo1().getSoliCorreoPersonal(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliModalidad",postulacion.getAnexo1().getSoliModalidad(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliVigencia",postulacion.getAnexo1().getSoliVigencia(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliObjetivos",postulacion.getAnexo1().getSoliObjetivos(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliRecursos",postulacion.getAnexo1().getSoliRecursos(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliImportancia",postulacion.getAnexo1().getSoliImportancia(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliDescripcionProyecto",postulacion.getAnexo1().getSoliDescripcionProyecto(),null,null,null,null,null));
                        //Area solicitada
                        lst_data.add(agregarColumnas("areaSuperficie",postulacion.getAnexo1().getAreaSuperficie(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("areaIdDistrito",postulacion.getAnexo1().getAreaIdDistrito(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("areaIdProvincia",postulacion.getAnexo1().getAreaIdProvincia(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("areaIdDepartamento",postulacion.getAnexo1().getAreaIdDepartamento(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("areaZona",postulacion.getAnexo1().getAreaZona(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("areaObservaciones",postulacion.getAnexo1().getAreaObservaciones(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("idDocumentoAdjuntoAreaSolicitada",postulacion.getAnexo1().getIdDocumentoAdjuntoAreaSolicitada(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("idDocumentoAdjuntoVertices",postulacion.getAnexo1().getIdDocumentoAdjuntoVertices(),null,null,null,null,null));
                        //Cuadro de coordenadas UTM
                        for (AreaSolicitadaUTMEntity utm : postulacion.getAnexo1().getAreaCoordenadasUTM()) {
                            lst_data.add(agregarColumnas("areaCoordenadasUTM",utm.getVertice(),utm.getEste(),utm.getNorte(),null,null,null));
                        }
                        //Actividades a desarrollar
                        for (ActividadesDesarrolloPFDMEntity act : postulacion.getAnexo1().getActividadesDesarrollar()) {
                            lst_data.add(agregarColumnas("actividadesDesarrollar",act.getActividad(),act.getDescripcion(),act.getPeriodoAnios(),act.getPresupuesto(),null,null));
                        }
                        lst_data.add(agregarColumnas("ecoturismoDescripcion",postulacion.getAnexo1().getEcoturismoDescripcion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("faunaSilvestreDescripcion",postulacion.getAnexo1().getFaunaSilvestreDescripcion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("pfdmDescripcion",postulacion.getAnexo1().getPfdmDescripcion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("ecosistemasDescripcion",postulacion.getAnexo1().getEcosistemasDescripcion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("maderableDescripcion",postulacion.getAnexo1().getMaderableDescripcion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("financiamientoDescripcion",postulacion.getAnexo1().getFinanciamientoDescripcion(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("soliCorreolegal",postulacion.getAnexo1().getSoliCorreolegal(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("lugarFirma",postulacion.getAnexo1().getLugarFirma(),null,null,null,null,null));
                        lst_data.add(agregarColumnas("fechaFirma",postulacion.getAnexo1().getFechaFirma(),null,null,null,null,null));
                        
                        Boolean exito = InsertarAnexosPDFM(lst_data,postulacion.getAnexo1().getCodigoAnexo(),primerap.getCodigo(),postulacion.getIdUsuarioPostulacion());
                        primerap.setSuccess(exito);
                        result = primerap;
                    }
                }else{
                    result = primerap;
                }
            }else{
                //actualizar datos de la postulacion pdfm
                ResultClassEntity primerap = actualizarPostulacionPFDME(postulacion.getPostulacionPFDM());
                if(primerap.getSuccess()){
                    if(!(postulacion.getAnexo1()==null)){
                        //Obtener el anexo por el idpostulacionpfdm y el idusuariopostulacion
                        ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM("anexo1", postulacion.getIdPostulacionPFDM(),postulacion.getIdUsuarioPostulacion());
                        //actualizar datos del anexo 1
                        Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false,
                        p14= false,p15= false,p16= false,p17= false,p18= false,p19= false,p20= false,p21= false,p22= false,p23= false,p24= false,p25 = false,p26 = false
                        ,p27= false,p28= false,p29= false,p30= false,p31= false,p32= false,p33 = false,p34 = false,p35 = false,p36 = false;
                        List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                        List<AnexosPFDMEntity> lst_dataupdate = new ArrayList<>();
                        for (AnexosPFDMEntity obj:lstAnexo.getData()) {
                            if(obj.getDescripcion().equals("soliNombreRS")){
                                p1 =true;
                                lst_dataupdate.add(agregarColumnasupdate("soliNombreRS",postulacion.getAnexo1().getSoliNombreRS(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliDni")){
                                p2=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliDni",postulacion.getAnexo1().getSoliDni(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliRuc")){
                                p3 =true;
                                lst_dataupdate.add(agregarColumnasupdate("soliRuc",postulacion.getAnexo1().getSoliRuc(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliNroPartidaRegistral")){
                                p4=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliNroPartidaRegistral",postulacion.getAnexo1().getSoliNroPartidaRegistral(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliDireccion")){
                                p5=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliDireccion",postulacion.getAnexo1().getSoliDireccion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliNroDireccion")){
                                p6=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliNroDireccion",postulacion.getAnexo1().getSoliNroDireccion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliSectorCaserio")){
                                p7=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliSectorCaserio",postulacion.getAnexo1().getSoliSectorCaserio(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliIdDistrito")){
                                p8=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliIdDistrito",postulacion.getAnexo1().getSoliIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliIdProvincia")){
                                p9=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliIdProvincia",postulacion.getAnexo1().getSoliIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliIdDepartamento")){
                                p10=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliIdDepartamento",postulacion.getAnexo1().getSoliIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliTelefono")){
                                p11=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliTelefono",postulacion.getAnexo1().getSoliTelefono(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliCelular")){
                                p12 =true;
                                lst_dataupdate.add(agregarColumnasupdate("soliCelular",postulacion.getAnexo1().getSoliCelular(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliCorreoPersonal")){
                                p13=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliCorreoPersonal",postulacion.getAnexo1().getSoliCorreoPersonal(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliModalidad")){
                                p14=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliModalidad",postulacion.getAnexo1().getSoliModalidad(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliVigencia")){
                                p15=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliVigencia",postulacion.getAnexo1().getSoliVigencia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliObjetivos")){
                                p16=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliObjetivos",postulacion.getAnexo1().getSoliObjetivos(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliRecursos")){
                                p17=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliRecursos",postulacion.getAnexo1().getSoliRecursos(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliImportancia")){
                                p18=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliImportancia",postulacion.getAnexo1().getSoliImportancia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliDescripcionProyecto")){
                                p19=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliDescripcionProyecto",postulacion.getAnexo1().getSoliDescripcionProyecto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaSuperficie")){
                                p20=true;
                                lst_dataupdate.add(agregarColumnasupdate("areaSuperficie",postulacion.getAnexo1().getAreaSuperficie(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaIdDistrito")){
                                p21 =true;
                                lst_dataupdate.add(agregarColumnasupdate("areaIdDistrito",postulacion.getAnexo1().getAreaIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaIdProvincia")){
                                p22=true;
                                lst_dataupdate.add(agregarColumnasupdate("areaIdProvincia",postulacion.getAnexo1().getAreaIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaIdDepartamento")){
                                p23=true;
                                lst_dataupdate.add(agregarColumnasupdate("areaIdDepartamento",postulacion.getAnexo1().getAreaIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaZona")){
                                p24=true;
                                lst_dataupdate.add(agregarColumnasupdate("areaZona",postulacion.getAnexo1().getAreaZona(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaObservaciones")){
                                p25=true;
                                lst_dataupdate.add(agregarColumnasupdate("areaObservaciones",postulacion.getAnexo1().getAreaObservaciones(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("areaCoordenadasUTM")){
                                //Cuadro de coordenadas UTM
                                //Se inactivan los registros - pendiente sp que inactiva los registros
                                Boolean eliminar = EliminarAnexosPDFM(obj.getIdAnexosPFDM(), postulacion.getIdPostulacionPFDM());
                                //Se registran los registros
                                if(eliminar){
                                    for (AreaSolicitadaUTMEntity utm : postulacion.getAnexo1().getAreaCoordenadasUTM()) {
                                        lst_data.add(agregarColumnas("areaCoordenadasUTM",utm.getVertice(),utm.getEste(),utm.getNorte(),null,null,null));
                                    }
                                }
                            }else if(obj.getDescripcion().equals("actividadesDesarrollar")){
                                //Actividades a desarrollar
                                //Se inactivan los registros - pendiente sp que inactiva los registros
                                Boolean eliminar = EliminarAnexosPDFM(obj.getIdAnexosPFDM(), postulacion.getIdPostulacionPFDM());
                                //Se registran los registros
                                if(eliminar){
                                    for (ActividadesDesarrolloPFDMEntity act : postulacion.getAnexo1().getActividadesDesarrollar()) {
                                        lst_data.add(agregarColumnas("actividadesDesarrollar",act.getActividad(),act.getDescripcion(),act.getPeriodoAnios(),act.getPresupuesto(),null,null));
                                    }
                                }
                            }else if(obj.getDescripcion().equals("ecoturismoDescripcion")){
                                p26=true;
                                lst_dataupdate.add(agregarColumnasupdate("ecoturismoDescripcion",postulacion.getAnexo1().getEcoturismoDescripcion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("faunaSilvestreDescripcion")){
                                p27=true;
                                lst_dataupdate.add(agregarColumnasupdate("faunaSilvestreDescripcion",postulacion.getAnexo1().getFaunaSilvestreDescripcion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("pfdmDescripcion")){
                                p28 =true;
                                lst_dataupdate.add(agregarColumnasupdate("pfdmDescripcion",postulacion.getAnexo1().getPfdmDescripcion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("ecosistemasDescripcion")){
                                p29=true;
                                lst_dataupdate.add(agregarColumnasupdate("ecosistemasDescripcion",postulacion.getAnexo1().getEcosistemasDescripcion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("maderableDescripcion")){
                                p30=true;
                                lst_dataupdate.add(agregarColumnasupdate("maderableDescripcion",postulacion.getAnexo1().getMaderableDescripcion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("financiamientoDescripcion")){
                                p31=true;
                                lst_dataupdate.add(agregarColumnasupdate("financiamientoDescripcion",postulacion.getAnexo1().getFinanciamientoDescripcion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("soliCorreolegal")){
                                p32=true;
                                lst_dataupdate.add(agregarColumnasupdate("soliCorreolegal",postulacion.getAnexo1().getSoliCorreolegal(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("lugarFirma")){
                                p33=true;
                                lst_dataupdate.add(agregarColumnasupdate("lugarFirma",postulacion.getAnexo1().getLugarFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("fechaFirma")){
                                p34=true;
                                lst_dataupdate.add(agregarColumnasupdate("fechaFirma",postulacion.getAnexo1().getFechaFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("idDocumentoAdjuntoAreaSolicitada")){
                                p35=true;
                                lst_dataupdate.add(agregarColumnasupdate("idDocumentoAdjuntoAreaSolicitada",postulacion.getAnexo1().getIdDocumentoAdjuntoAreaSolicitada(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }else if(obj.getDescripcion().equals("idDocumentoAdjuntoVertices")){
                                p36=true;
                                lst_dataupdate.add(agregarColumnasupdate("idDocumentoAdjuntoVertices",postulacion.getAnexo1().getIdDocumentoAdjuntoVertices(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                            }

                        }
                        //insertar datos del anexo 1
                        if(!p1){lst_data.add(agregarColumnas("soliNombreRS",postulacion.getAnexo1().getSoliNombreRS(),null,null,null,null,null));}
                        if(!p2){lst_data.add(agregarColumnas("soliDni",postulacion.getAnexo1().getSoliDni(),null,null,null,null,null));}
                        if(!p3){lst_data.add(agregarColumnas("soliRuc",postulacion.getAnexo1().getSoliRuc(),null,null,null,null,null));}
                        if(!p4){lst_data.add(agregarColumnas("soliNroPartidaRegistral",postulacion.getAnexo1().getSoliNroPartidaRegistral(),null,null,null,null,null));}
                        if(!p5){lst_data.add(agregarColumnas("soliDireccion",postulacion.getAnexo1().getSoliDireccion(),null,null,null,null,null));}
                        if(!p6){lst_data.add(agregarColumnas("soliNroDireccion",postulacion.getAnexo1().getSoliNroDireccion(),null,null,null,null,null));}
                        if(!p7){lst_data.add(agregarColumnas("soliSectorCaserio",postulacion.getAnexo1().getSoliSectorCaserio(),null,null,null,null,null));}
                        if(!p8){lst_data.add(agregarColumnas("soliIdDistrito",postulacion.getAnexo1().getSoliIdDistrito(),null,null,null,null,null));}
                        if(!p9){lst_data.add(agregarColumnas("soliIdProvincia",postulacion.getAnexo1().getSoliIdProvincia(),null,null,null,null,null));}
                        if(!p10){lst_data.add(agregarColumnas("soliIdDepartamento",postulacion.getAnexo1().getSoliIdDepartamento(),null,null,null,null,null));}
                        if(!p11){lst_data.add(agregarColumnas("soliTelefono",postulacion.getAnexo1().getSoliTelefono(),null,null,null,null,null));}
                        if(!p12){lst_data.add(agregarColumnas("soliCelular",postulacion.getAnexo1().getSoliCelular(),null,null,null,null,null));}
                        if(!p13){lst_data.add(agregarColumnas("soliCorreoPersonal",postulacion.getAnexo1().getSoliCorreoPersonal(),null,null,null,null,null));}
                        if(!p14){lst_data.add(agregarColumnas("soliModalidad",postulacion.getAnexo1().getSoliModalidad(),null,null,null,null,null));}
                        if(!p15){lst_data.add(agregarColumnas("soliVigencia",postulacion.getAnexo1().getSoliVigencia(),null,null,null,null,null));}
                        if(!p16){lst_data.add(agregarColumnas("soliObjetivos",postulacion.getAnexo1().getSoliObjetivos(),null,null,null,null,null));}
                        if(!p14){lst_data.add(agregarColumnas("soliRecursos",postulacion.getAnexo1().getSoliRecursos(),null,null,null,null,null));}
                        if(!p18){lst_data.add(agregarColumnas("soliImportancia",postulacion.getAnexo1().getSoliImportancia(),null,null,null,null,null));}
                        if(!p19){lst_data.add(agregarColumnas("soliDescripcionProyecto",postulacion.getAnexo1().getSoliDescripcionProyecto(),null,null,null,null,null));}
                        //Area solicitada
                        if(!p20){lst_data.add(agregarColumnas("areaSuperficie",postulacion.getAnexo1().getAreaSuperficie(),null,null,null,null,null));}
                        if(!p21){lst_data.add(agregarColumnas("areaIdDistrito",postulacion.getAnexo1().getAreaIdDistrito(),null,null,null,null,null));}
                        if(!p22){lst_data.add(agregarColumnas("areaIdProvincia",postulacion.getAnexo1().getAreaIdProvincia(),null,null,null,null,null));}
                        if(!p23){lst_data.add(agregarColumnas("areaIdDepartamento",postulacion.getAnexo1().getAreaIdDepartamento(),null,null,null,null,null));}
                        if(!p24){lst_data.add(agregarColumnas("areaZona",postulacion.getAnexo1().getAreaZona(),null,null,null,null,null));}
                        if(!p25){lst_data.add(agregarColumnas("areaObservaciones",postulacion.getAnexo1().getAreaObservaciones(),null,null,null,null,null));}
                        if(!p26){lst_data.add(agregarColumnas("ecoturismoDescripcion",postulacion.getAnexo1().getEcoturismoDescripcion(),null,null,null,null,null));}
                        if(!p27){lst_data.add(agregarColumnas("faunaSilvestreDescripcion",postulacion.getAnexo1().getFaunaSilvestreDescripcion(),null,null,null,null,null));}
                        if(!p28){lst_data.add(agregarColumnas("pfdmDescripcion",postulacion.getAnexo1().getPfdmDescripcion(),null,null,null,null,null));}
                        if(!p29){lst_data.add(agregarColumnas("ecosistemasDescripcion",postulacion.getAnexo1().getEcosistemasDescripcion(),null,null,null,null,null));}
                        if(!p30){lst_data.add(agregarColumnas("maderableDescripcion",postulacion.getAnexo1().getMaderableDescripcion(),null,null,null,null,null));}
                        if(!p31){lst_data.add(agregarColumnas("financiamientoDescripcion",postulacion.getAnexo1().getFinanciamientoDescripcion(),null,null,null,null,null));}
                        if(!p32){lst_data.add(agregarColumnas("soliCorreolegal",postulacion.getAnexo1().getSoliCorreolegal(),null,null,null,null,null));}
                        if(!p33){lst_data.add(agregarColumnas("lugarFirma",postulacion.getAnexo1().getLugarFirma(),null,null,null,null,null));}
                        if(!p34){lst_data.add(agregarColumnas("fechaFirma",postulacion.getAnexo1().getFechaFirma(),null,null,null,null,null));}
                        if(!p35){lst_data.add(agregarColumnas("idDocumentoAdjuntoAreaSolicitada",postulacion.getAnexo1().getIdDocumentoAdjuntoAreaSolicitada(),null,null,null,null,null));}
                        if(!p36){lst_data.add(agregarColumnas("idDocumentoAdjuntoVertices",postulacion.getAnexo1().getIdDocumentoAdjuntoVertices(),null,null,null,null,null));}
                        //actulizar le valor de los que ya se reistraron
                        Boolean exitoupdate = ActualizarAnexosPDFM(lst_dataupdate,postulacion.getIdUsuarioPostulacion());
                        //insertar los parametros que fatan insertar
                        Boolean exito = InsertarAnexosPDFM(lst_data,postulacion.getAnexo1().getCodigoAnexo(),postulacion.getIdPostulacionPFDM(),postulacion.getIdUsuarioPostulacion());
                        if(exito && exitoupdate){
                            result.setInformacion("Se actualizaron los campos del anexo1");
                            result.setSuccess(true);
                        }else{
                            result.setInformacion("Ocurrió un error al actualizar los campos del anexo1");
                            result.setSuccess(true);
                        }
                        
                    }
                   
                }else{
                    result = primerap;
                }
            }
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardaPostulacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardaPostulacion", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    public ResultClassEntity guardarPostulacionPFDME(PostulacionPFDMEntity postulacion){
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_PostulacionPFDM_insertar (?,?,?,?,?)}");
            pstm.setObject (1, postulacion.getIdUsuarioPostulacion(), Types.INTEGER);
            pstm.setObject(2, postulacion.getIdEstatusProceso(),Types.INTEGER);
            pstm.setObject(3, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP, Types.TIMESTAMP);
            pstm.setObject(4, postulacion.getIdUsuarioRegistro(), Types.INTEGER);
            pstm.setObject(5, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            salida = pstm.executeUpdate();
            ResultSet rsa = pstm.getGeneratedKeys();
            if(rsa.next())
            {
                salida = rsa.getInt(1);
            }                 
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se registró su solicitud");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarPostulacionPFDME", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarPostulacionPFDME", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    public ResultClassEntity actualizarPostulacionPFDME(PostulacionPFDMEntity postulacion){
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_PostulacionPFDM_update(?,?,?,?,?,?)}");
            pstm.setObject (1,postulacion.getIdPostulacionPFDM(), Types.INTEGER);
            pstm.setObject(2, postulacion.getRecepcionDocumentos(),Types.BOOLEAN);
            pstm.setObject(3, postulacion.getFechaRecepcionDocumentos(), Types.TIMESTAMP);
            pstm.setObject(4, postulacion.getNumeroTramite(), Types.VARCHAR);
            pstm.setObject(5, postulacion.getIdUsuarioModificacion(), Types.INTEGER);
            pstm.setObject(6, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            salida = pstm.executeUpdate();                   
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se actualizó su postulacionPFDM");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarPostulacionPFDME", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarPostulacionPFDME", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    public Boolean ActualizarAnexosPDFM(List<AnexosPFDMEntity> lst_data,Integer IdUsuarioModificacion){
        return null;
        /*
        Connection con2 = null;
        PreparedStatement pstm2 = null;
        String idgenerados = "";
        Boolean exito =false;
        try {
            con2 = dataSource.getConnection();
            pstm2 = con2.prepareCall("{call pa_AnexosPFDM_updateanexo (?,?,?,?,?,?,?,?,?)}");
            Integer salida =0;
            Date date = new Date();
            if(lst_data.size()>0){
                for (AnexosPFDMEntity obj :lst_data) {
                    pstm2.setObject (1, obj.getIdAnexosPFDM(), Types.INTEGER);
                    pstm2.setObject(2, obj.getValue1(),Types.VARCHAR);
                    pstm2.setObject(3, obj.getValue2(),Types.VARCHAR);
                    pstm2.setObject(4, obj.getValue3(),Types.VARCHAR);
                    pstm2.setObject(5, obj.getValue4(),Types.VARCHAR);
                    pstm2.setObject(6, obj.getValue5(),Types.VARCHAR);
                    pstm2.setObject(7, obj.getValue6(),Types.VARCHAR);
                    pstm2.setObject(8, IdUsuarioModificacion,Types.INTEGER);
                    pstm2.setObject(9, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
                    salida = pstm2.executeUpdate();
                    idgenerados += salida+",";
                }
            }

            exito =true;
        } catch (Exception e) {
            exito =false;
            log.error(e.getMessage());
        } finally{
            try {
                pstm2.close();
                con2.close();
            } catch (Exception e) {
                exito=false;
            }
        }
        return exito;
        */
    }

    public Boolean EliminarAnexosPDFM(Integer IdAnexoPFDM,Integer IdUsuarioModificacion){
        return null;
        /*
        Connection con2 = null;
        PreparedStatement pstm2 = null;
        Boolean exito =false;
        try {
            Integer salida =0;
            Date date = new Date();
            con2 = dataSource.getConnection();
            pstm2 = con2.prepareCall("{call pa_AnexosPFDM_eliminarpropiedad (?,?,?)}");
            pstm2.setObject (1, IdAnexoPFDM, Types.INTEGER);
            pstm2.setObject(2, IdUsuarioModificacion,Types.INTEGER);
            pstm2.setObject(3, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            salida = pstm2.executeUpdate(); 
            exito =true;
        } catch (Exception e) {
            exito =false;
            log.error(e.getMessage());
        } finally{
            try {
                pstm2.close();
                con2.close();
            } catch (Exception e) {
                exito=false;
            }
        }
        return exito;
        */
    }

    public Boolean InsertarAnexosPDFM(List<AnexosPFDMEntity> lst_data, String codigoanexo,Integer IdPostulacionPFDM, Integer IdUsuarioPostulacion){
        return null;
        /*
        Connection con = null;
        PreparedStatement pstm  = null;
        String idgenerados = "";
        Boolean exito =false;
        try {
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_AnexosPFDM_insertaranexo (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            Integer salida =0;
            Date date = new Date();
            if(lst_data.size()>0){
                for (AnexosPFDMEntity obj :lst_data) {
                    pstm.setObject (1, IdPostulacionPFDM, Types.INTEGER);
                    pstm.setObject(2, IdUsuarioPostulacion,Types.INTEGER);
                    pstm.setObject (3, codigoanexo, Types.VARCHAR);
                    if(obj.getDescripcion().equals("areaCoordenadasUTM")){
                        pstm.setObject(4, true,Types.BOOLEAN);
                        pstm.setObject(5, "UTM",Types.VARCHAR);
                    }else if(obj.getDescripcion().equals("actividadesDesarrollar")){
                        pstm.setObject(4, true,Types.BOOLEAN);
                        pstm.setObject(5, "ACTVDESA",Types.VARCHAR);
                    }else if(obj.getDescripcion().equals("formacionAcademica")){
                        pstm.setObject(4, true,Types.BOOLEAN);
                        pstm.setObject(5, "FORMA",Types.VARCHAR);
                    }else if(obj.getDescripcion().equals("experienciaLaboral")){
                        pstm.setObject(4, true,Types.BOOLEAN);
                        pstm.setObject(5, "EXPELA",Types.VARCHAR);
                    }else if(obj.getDescripcion().equals("postulanteIngresos")){
                        pstm.setObject(4, true,Types.BOOLEAN);
                        pstm.setObject(5, "INGRE",Types.VARCHAR);
                    }
                    else{
                        pstm.setObject(4, null,Types.BOOLEAN);
                        pstm.setObject(5, null,Types.VARCHAR);
                    }
                    pstm.setObject(6, obj.getDescripcion(),Types.VARCHAR);
                    pstm.setObject(7, obj.getValue1(),Types.VARCHAR);
                    pstm.setObject(8, obj.getValue2(),Types.VARCHAR);
                    pstm.setObject(9, obj.getValue3(),Types.VARCHAR);
                    pstm.setObject(10, obj.getValue4(),Types.VARCHAR);
                    pstm.setObject(11, obj.getValue5(),Types.VARCHAR);
                    pstm.setObject(12, obj.getValue6(),Types.VARCHAR);
                    pstm.setObject(13, IdUsuarioPostulacion,Types.INTEGER);
                    pstm.setObject(14, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
                    salida = pstm.executeUpdate();
                    ResultSet rsa = pstm.getGeneratedKeys();
                    if(rsa.next())
                    {
                        salida = rsa.getInt(1);
                    }
                    idgenerados += salida+",";

                }
            }
            exito =true;
        } catch (Exception e) {
            exito =false;
            log.error(e.getMessage());
        } finally{
            try {
                pstm.close();
                con.close();
            } catch (Exception e) {
                exito=false;
            }
        }
        return exito;
        */
    }
    
    public AnexosPFDMEntity agregarColumnas(String descripcion, String value1, String value2, String value3, String value4, String value5, String value6){
        AnexosPFDMEntity objanexo = new AnexosPFDMEntity();
        objanexo.setDescripcion(descripcion);
        objanexo.setValue1(value1);
        objanexo.setValue2(value2);
        objanexo.setValue3(value3);
        objanexo.setValue4(value4);
        objanexo.setValue5(value5);
        objanexo.setValue6(value6);
        return objanexo;
    }

    public AnexosPFDMEntity agregarColumnasupdate(String descripcion, String value1, String value2, String value3, String value4, String value5, String value6, Integer IdAnexosPFDM){
        AnexosPFDMEntity objanexo = new AnexosPFDMEntity();
        objanexo.setDescripcion(descripcion);
        objanexo.setValue1(value1);
        objanexo.setValue2(value2);
        objanexo.setValue3(value3);
        objanexo.setValue4(value4);
        objanexo.setValue5(value5);
        objanexo.setValue6(value6);
        objanexo.setIdAnexosPFDM(IdAnexosPFDM);
        return objanexo;
    }

     /**
     * @autor: JaquelineDB [09-08-2021]
     * @modificado:
     * @descripción: {Obtener anexo 1 pfdm}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo1PFDMEntity> obtenerAnexo1PFDM(AnexosPFDMResquestEntity filtro) {
        ResultClassEntity<Anexo1PFDMEntity> result = new ResultClassEntity<Anexo1PFDMEntity>();
        try {
            ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM(filtro.getCodigoAnexo(), filtro.getIdPostulacionPFDM(),filtro.getIdUsuarioPostulacion());
            Anexo1PFDMEntity anexo1 = new Anexo1PFDMEntity();
            //Obtenemos la grilla de las coordenadas UTM
            List<AreaSolicitadaUTMEntity> lstUTM = new ArrayList<>();
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getGrilla() && a.getCodGrilla().equals("UTM")){
                    AreaSolicitadaUTMEntity obj = new AreaSolicitadaUTMEntity();
                    obj.setVertice(a.getValue1());
                    obj.setEste(a.getValue2());
                    obj.setNorte(a.getValue3());
                    lstUTM.add(obj);
                }
			});
            anexo1.setAreaCoordenadasUTM(lstUTM);
            //Obtener la grilla de las actividades a desarrollar
            List<ActividadesDesarrolloPFDMEntity> lstACTVDESA = new ArrayList<>();
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getGrilla() && a.getCodGrilla().equals("ACTVDESA")){
                    ActividadesDesarrolloPFDMEntity obj = new ActividadesDesarrolloPFDMEntity();
                    obj.setActividad(a.getValue1());
                    obj.setDescripcion(a.getValue2());
                    obj.setPeriodoAnios(a.getValue3());
                    obj.setPresupuesto(a.getValue4());
                    lstACTVDESA.add(obj);
                }
			});
            anexo1.setActividadesDesarrollar(lstACTVDESA);
            //Se llenan los otros campos del anexo
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getDescripcion().equals("soliNombreRS")){
                    anexo1.setSoliNombreRS(a.getValue1());
                }else if(a.getDescripcion().equals("soliDni")){
                    anexo1.setSoliDni(a.getValue1());
                }else if(a.getDescripcion().equals("soliRuc")){
                    anexo1.setSoliRuc(a.getValue1());
                }else if(a.getDescripcion().equals("soliNroPartidaRegistral")){
                    anexo1.setSoliNroPartidaRegistral(a.getValue1());
                }else if(a.getDescripcion().equals("soliDireccion")){
                    anexo1.setSoliDireccion(a.getValue1());
                }else if(a.getDescripcion().equals("soliNroDireccion")){
                    anexo1.setSoliNroDireccion(a.getValue1());
                }else if(a.getDescripcion().equals("soliSectorCaserio")){
                    anexo1.setSoliSectorCaserio(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdDistrito")){
                    anexo1.setSoliIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdProvincia")){
                    anexo1.setSoliIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdDepartamento")){
                    anexo1.setSoliIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("soliTelefono")){
                    anexo1.setSoliTelefono(a.getValue1());
                }else if(a.getDescripcion().equals("soliCelular")){
                    anexo1.setSoliCelular(a.getValue1());
                }else if(a.getDescripcion().equals("soliCorreoPersonal")){
                    anexo1.setSoliCorreoPersonal(a.getValue1());
                }else if(a.getDescripcion().equals("soliModalidad")){
                    anexo1.setSoliModalidad(a.getValue1());
                }else if(a.getDescripcion().equals("soliVigencia")){
                    anexo1.setSoliVigencia(a.getValue1());
                }else if(a.getDescripcion().equals("soliObjetivos")){
                    anexo1.setSoliObjetivos(a.getValue1());
                }else if(a.getDescripcion().equals("soliRecursos")){
                    anexo1.setSoliRecursos(a.getValue1());
                }else if(a.getDescripcion().equals("soliImportancia")){
                    anexo1.setSoliImportancia(a.getValue1());
                }else if(a.getDescripcion().equals("soliDescripcionProyecto")){
                    anexo1.setSoliDescripcionProyecto(a.getValue1());
                }else if(a.getDescripcion().equals("areaSuperficie")){
                    anexo1.setAreaSuperficie(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdDistrito")){
                    anexo1.setAreaIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdProvincia")){
                    anexo1.setAreaIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdDepartamento")){
                    anexo1.setAreaIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("areaZona")){
                    anexo1.setAreaZona(a.getValue1());
                }else if(a.getDescripcion().equals("areaObservaciones")){
                    anexo1.setAreaObservaciones(a.getValue1());
                }else if(a.getDescripcion().equals("ecoturismoDescripcion")){
                    anexo1.setEcoturismoDescripcion(a.getValue1());
                }else if(a.getDescripcion().equals("faunaSilvestreDescripcion")){
                    anexo1.setFaunaSilvestreDescripcion(a.getValue1());
                }else if(a.getDescripcion().equals("pfdmDescripcion")){
                    anexo1.setPfdmDescripcion(a.getValue1());
                }else if(a.getDescripcion().equals("ecosistemasDescripcion")){
                    anexo1.setEcosistemasDescripcion(a.getValue1());
                }else if(a.getDescripcion().equals("maderableDescripcion")){
                    anexo1.setMaderableDescripcion(a.getValue1());
                }else if(a.getDescripcion().equals("financiamientoDescripcion")){
                    anexo1.setFinanciamientoDescripcion(a.getValue1());
                }else if(a.getDescripcion().equals("soliCorreolegal")){
                    anexo1.setSoliCorreolegal(a.getValue1());
                }else if(a.getDescripcion().equals("lugarFirma")){
                    anexo1.setLugarFirma(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFirma")){
                    anexo1.setFechaFirma(a.getValue1());
                }else if(a.getDescripcion().equals("idDocumentoAdjuntoAreaSolicitada")){
                    anexo1.setIdDocumentoAdjuntoAreaSolicitada(a.getValue1());
                }else if(a.getDescripcion().equals("idDocumentoAdjuntoVertices")){
                    anexo1.setIdDocumentoAdjuntoVertices(a.getValue1());
                }
			});

            result.setData(anexo1);
            result.setSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - obtenerAnexo1PFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } 
    }

    @Override
    public ResultEntity<AnexosPFDMEntity> obtenerAnexoPFDM(String codigoAnexo, Integer IdPostulacionPFDM,
            Integer IdUsuarioPostulacion) {
        return null;
        /*
        ResultEntity<AnexosPFDMEntity> result = new ResultEntity<AnexosPFDMEntity>();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_AnexosPFDM_Obtener (?,?,?)}");
            pstm.setObject (1, IdPostulacionPFDM,Types.INTEGER);
            pstm.setObject(2, IdUsuarioPostulacion,Types.INTEGER);
            pstm.setObject(3, codigoAnexo,Types.VARCHAR);
            ResultSet rs = pstm.executeQuery();
            AnexosPFDMEntity c = null;
            List<AnexosPFDMEntity> lstresult = new ArrayList<>();
            while(rs.next()){
                c = new AnexosPFDMEntity();
                c.setIdAnexosPFDM(rs.getInt("NU_ID_ANEXOSPFDM"));
                c.setCodigoAnexo(rs.getString("TX_CODIGO_ANEXO"));
                c.setDescripcion(rs.getString("TX_DESCRIPCION"));
                c.setGrilla(rs.getBoolean("NU_GRILLA"));
                c.setCodGrilla(rs.getString("NU_CODGRILLA"));
                c.setValue1(rs.getString("TX_VALUE1"));
                c.setValue2(rs.getString("TX_VALUE2"));
                c.setValue3(rs.getString("TX_VALUE3"));
                c.setValue4(rs.getString("TX_VALUE4"));
                c.setValue5(rs.getString("TX_VALUE5"));
                c.setValue6(rs.getString("TX_VALUE6"));
                c.setIdPostulacionPFDM (rs.getInt("NU_ID_POSTULACIONPFDM"));
                c.setIdUsuarioPostulacion(rs.getInt("NU_ID_USUARIO_POSTULACION"));
                lstresult.add(c);
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            pstm.close();
            con.close();
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - ObtenerAnexoPFDM", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - ObtenerAnexoPFDM", e2.getMessage());
                result.setIsSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [10-08-2021]
     * @modificado:
     * @descripción: {se adjuntan los archivos relacionados con la postulacion pfdm}
     * @param:ContratoAdjuntosRequestEntity
     */
    @Override
    public ResultClassEntity AdjuntarArchivosPostulacionPFDM(AnexosPFDMAdjuntosEntity obj) {

        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            //Adjuntar Documentos
            pstm = con.prepareCall("{call pa_PostulacionPFDMFile_Registrar (?,?,?,?)}");
            pstm.setObject (1, obj.getIdPostulacionPFDM(), Types.INTEGER);
            pstm.setObject(2, obj.getIdDocumentoAdjunto(),Types.INTEGER);
            pstm.setObject(3, obj.getIdUsuarioRegistro(),Types.INTEGER);
            pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida2 = pstm.executeUpdate();
            ResultSet rsa2 = pstm.getGeneratedKeys();
            if(rsa2.next())
            {
                salida2 = rsa2.getInt(1);
            }
            result.setCodigo(salida2);
            result.setSuccess(true);
            result.setMessage("Se registró el adjunto del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - AdjuntarArchivosPostulacionPFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - AdjuntarArchivosPostulacionPFDM", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [12-08-2021]
     * @modificado:
     * @descripción: {se registran los datos del anexo 2}
     * @param:Anexo2PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo2(Anexo2PFDMEntity anexo) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            if(!(anexo.getIdPostulacionPFDM().equals(null) || anexo.getIdPostulacionPFDM().equals(0))){
                if(!(anexo==null)){
                    //Obtener el anexo2 por el idpostulacionpfdm y el idusuariopostulacion
                    ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM("anexo2", anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    //actualizar datos del anexo 2
                    Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false,
                    p14= false,p15= false,p16= false,p17= false,p18= false,p19= false,p20= false;
                    List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                    List<AnexosPFDMEntity> lst_dataupdate = new ArrayList<>();
                    for (AnexosPFDMEntity obj:lstAnexo.getData()) {
                        if(obj.getDescripcion().equals("profeApellidosNombres")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("profeApellidosNombres",anexo.getProfeApellidosNombres(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeDNIRUC")){
                            p2=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeDNIRUC",anexo.getProfeDNIRUC(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeDomicilio")){
                            p3 =true;
                            lst_dataupdate.add(agregarColumnasupdate("profeDomicilio",anexo.getProfeDomicilio(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeSector")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeSector",anexo.getProfeSector(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeIdDistrito")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeIdDistrito",anexo.getProfeIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeIdProvincia")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeIdProvincia",anexo.getProfeIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeIdDepartamento")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeIdDepartamento",anexo.getProfeIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeTelefono")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeTelefono",anexo.getProfeTelefono(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeCelular")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeCelular",anexo.getProfeCelular(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeCorreo")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeCorreo",anexo.getProfeCorreo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("profeNombreRSSolicitante")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("profeNombreRSSolicitante",anexo.getProfeNombreRSSolicitante(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("formaProfesion")){
                            p12 =true;
                            lst_dataupdate.add(agregarColumnasupdate("formaProfesion",anexo.getFormaProfesion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("formaNroRegistroColegioProfesional")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("formaNroRegistroColegioProfesional",anexo.getFormaNroRegistroColegioProfesional(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("formacionAcademica")){
                            p16=true;
                            //Cuadro de coordenadas UTM
                            //Se inactivan los registros - pendiente sp que inactiva los registros
                            Boolean eliminar = EliminarAnexosPDFM(obj.getIdAnexosPFDM(), anexo.getIdPostulacionPFDM());
                            //Se registran los registros
                            if(eliminar){
                                for (FormacionAcademicaEntity fc : anexo.getFormacionAcademica()) {
                                    lst_data.add(agregarColumnas("formacionAcademica",fc.getGradoTitulo(),fc.getEspecialidad(),fc.getNombreUniversidadInstituto(),fc.getAnioEmisionTitulo(),null,null));
                                }
                            }
                            
                        }else if(obj.getDescripcion().equals("experienciaLaboral")){
                            p17=true;
                            //Actividades a desarrollar
                            //Se inactivan los registros - pendiente sp que inactiva los registros
                            Boolean eliminar = EliminarAnexosPDFM(obj.getIdAnexosPFDM(), anexo.getIdPostulacionPFDM());
                            //Se registran los registros
                            if(eliminar){
                                for (ExperienciaLaboralEntity el : anexo.getExperienciaLaboral()) {
                                    lst_data.add(agregarColumnas("experienciaLaboral",el.getEntidadEmpresaOtros(),el.getCargo(),el.getTiempoanios(),el.getDescActvRealizadas(),el.getDatosContactoEmpresa(),null));
                                }
                            }
                        }else if(obj.getDescripcion().equals("lugarFirma")){
                            p14=true;
                            lst_dataupdate.add(agregarColumnasupdate("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fechaFirma")){
                            p15=true;
                            lst_dataupdate.add(agregarColumnasupdate("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntosFormacion")){
                            p18=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntosFormacion",anexo.getIdAdjuntosFormacion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntosExperiencia")){
                            p19=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntosExperiencia",anexo.getIdAdjuntosExperiencia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }
                    }
                    //agregar registros
                    if(!p1){lst_data.add(agregarColumnas("profeApellidosNombres",anexo.getProfeApellidosNombres(),null,null,null,null,null));}
                    if(!p2){lst_data.add(agregarColumnas("profeDNIRUC",anexo.getProfeDNIRUC(),null,null,null,null,null));}
                    if(!p3){lst_data.add(agregarColumnas("profeDomicilio",anexo.getProfeDomicilio(),null,null,null,null,null));}
                    if(!p4){lst_data.add(agregarColumnas("profeSector",anexo.getProfeSector(),null,null,null,null,null));}
                    if(!p5){lst_data.add(agregarColumnas("profeIdDistrito",anexo.getProfeIdDistrito(),null,null,null,null,null));}
                    if(!p6){lst_data.add(agregarColumnas("profeIdProvincia",anexo.getProfeIdProvincia(),null,null,null,null,null));}
                    if(!p7){lst_data.add(agregarColumnas("profeIdDepartamento",anexo.getProfeIdDepartamento(),null,null,null,null,null));}
                    if(!p8){lst_data.add(agregarColumnas("profeTelefono",anexo.getProfeTelefono(),null,null,null,null,null));}
                    if(!p9){lst_data.add(agregarColumnas("profeCelular",anexo.getProfeCelular(),null,null,null,null,null));}
                    if(!p10){lst_data.add(agregarColumnas("profeCorreo",anexo.getProfeCorreo(),null,null,null,null,null));}
                    if(!p11){lst_data.add(agregarColumnas("profeNombreRSSolicitante",anexo.getProfeNombreRSSolicitante(),null,null,null,null,null));}
                    if(!p12){lst_data.add(agregarColumnas("formaProfesion",anexo.getFormaProfesion(),null,null,null,null,null));}
                    if(!p13){lst_data.add(agregarColumnas("formaNroRegistroColegioProfesional",anexo.getFormaNroRegistroColegioProfesional(),null,null,null,null,null));}
                    if(!p14){lst_data.add(agregarColumnas("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null));}
                    if(!p15){lst_data.add(agregarColumnas("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null));}
                    if(!p16){
                        for (FormacionAcademicaEntity fc : anexo.getFormacionAcademica()) {
                            lst_data.add(agregarColumnas("formacionAcademica",fc.getGradoTitulo(),fc.getEspecialidad(),fc.getNombreUniversidadInstituto(),fc.getAnioEmisionTitulo(),null,null));
                        }
                    }
                    if(!p17){
                        for (ExperienciaLaboralEntity el : anexo.getExperienciaLaboral()) {
                            lst_data.add(agregarColumnas("experienciaLaboral",el.getEntidadEmpresaOtros(),el.getCargo(),el.getTiempoanios(),el.getDescActvRealizadas(),el.getDatosContactoEmpresa(),null));
                        }
                    }
                    if(!p18){lst_data.add(agregarColumnas("idAdjuntosFormacion",anexo.getIdAdjuntosFormacion(),null,null,null,null,null));}
                    if(!p19){lst_data.add(agregarColumnas("idAdjuntosExperiencia",anexo.getIdAdjuntosExperiencia(),null,null,null,null,null));}
                    //actulizar le valor de los que ya se reistraron
                    Boolean exitoupdate = ActualizarAnexosPDFM(lst_dataupdate,anexo.getIdUsuarioPostulante());
                    //insertar los parametros que fatan insertar
                    Boolean exito = InsertarAnexosPDFM(lst_data,"anexo2",anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    if(exito && exitoupdate){
                        result.setInformacion("Se actualizaron los campos del anexo2");
                        result.setSuccess(true);
                    }else{
                        result.setInformacion("Ocurrió un error al actualizar los campos del anexo2");
                        result.setSuccess(true);
                    }
                }            
                    
            }
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarAnexo2", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarAnexo2", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

     /**
     * @autor: JaquelineDB [12-08-2021]
     * @modificado:
     * @descripción: {se registran los datos del anexo 3}
     * @param:Anexo3PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo3(Anexo3PFDMEntity anexo) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            if(!(anexo.getIdPostulacionPFDM().equals(null) || anexo.getIdPostulacionPFDM().equals(0))){
                if(!(anexo==null)){
                    //Obtener el anexo2 por el idpostulacionpfdm y el idusuariopostulacion
                    ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM("anexo3", anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    //actualizar datos del anexo 2
                    Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false,
                    p14= false,p15= false,p16= false,p17= false,p18= false,p19= false,p20= false;
                    List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                    List<AnexosPFDMEntity> lst_dataupdate = new ArrayList<>();
                    for (AnexosPFDMEntity obj:lstAnexo.getData()) {
                        if(obj.getDescripcion().equals("funcioNombresApellidos")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("funcioNombresApellidos",anexo.getFuncioNombresApellidos(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("funcioCargo")){
                            p2=true;
                            lst_dataupdate.add(agregarColumnasupdate("funcioCargo",anexo.getFuncioCargo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("postulanteNombresApellidos")){
                            p3 =true;
                            lst_dataupdate.add(agregarColumnasupdate("postulanteNombresApellidos",anexo.getPostulanteNombresApellidos(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("postulanteDniRuc")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("postulanteDniRuc",anexo.getPostulanteDniRuc(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("postulanteDomicilio")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("postulanteDomicilio",anexo.getPostulanteDomicilio(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("postulanteConsecion")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("postulanteConsecion",anexo.getPostulanteConsecion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaSuperficie")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaSuperficie",anexo.getAreaSuperficie(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaSector")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaSector",anexo.getAreaSector(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areIdDistrito")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("areIdDistrito",anexo.getAreIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaIdProvincia")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaIdProvincia",anexo.getAreaIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaIdDepartamento")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaIdDepartamento",anexo.getAreaIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("postulanteIngresos")){
                            p12=true;
                            //Actividades a desarrollar
                            //Se inactivan los registros - pendiente sp que inactiva los registros
                            Boolean eliminar = EliminarAnexosPDFM(obj.getIdAnexosPFDM(), anexo.getIdPostulacionPFDM());
                            //Se registran los registros
                            if(eliminar){
                                for (IngresosCuentasEntity el : anexo.getPostulanteIngresos()) {
                                    lst_data.add(agregarColumnas("postulanteIngresos",el.getTipoNumeroCodigoDoc(),el.getDescripcion(),el.getMonto(),el.getIdDocumentoAdjunto(),null,null));
                                }
                            }
                        }else if(obj.getDescripcion().equals("lugarFirma")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fechaFirma")){
                            p14=true;
                            lst_dataupdate.add(agregarColumnasupdate("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }
                    }
                    //agregar registros
                    if(!p1){lst_data.add(agregarColumnas("funcioNombresApellidos",anexo.getFuncioNombresApellidos(),null,null,null,null,null));}
                    if(!p2){lst_data.add(agregarColumnas("funcioCargo",anexo.getFuncioCargo(),null,null,null,null,null));}
                    if(!p3){lst_data.add(agregarColumnas("postulanteNombresApellidos",anexo.getPostulanteNombresApellidos(),null,null,null,null,null));}
                    if(!p4){lst_data.add(agregarColumnas("postulanteDniRuc",anexo.getPostulanteDniRuc(),null,null,null,null,null));}
                    if(!p5){lst_data.add(agregarColumnas("postulanteDomicilio",anexo.getPostulanteDomicilio(),null,null,null,null,null));}
                    if(!p6){lst_data.add(agregarColumnas("postulanteConsecion",anexo.getPostulanteConsecion(),null,null,null,null,null));}
                    if(!p7){lst_data.add(agregarColumnas("areaSuperficie",anexo.getAreaSuperficie(),null,null,null,null,null));}
                    if(!p8){lst_data.add(agregarColumnas("areaSector",anexo.getAreaSector(),null,null,null,null,null));}
                    if(!p9){lst_data.add(agregarColumnas("areIdDistrito",anexo.getAreIdDistrito(),null,null,null,null,null));}
                    if(!p10){lst_data.add(agregarColumnas("areaIdProvincia",anexo.getAreaIdProvincia(),null,null,null,null,null));}
                    if(!p11){lst_data.add(agregarColumnas("areaIdDepartamento",anexo.getAreaIdDepartamento(),null,null,null,null,null));}
                    if(!p12){
                        for (IngresosCuentasEntity el : anexo.getPostulanteIngresos()) {
                            lst_data.add(agregarColumnas("postulanteIngresos",el.getTipoNumeroCodigoDoc(),el.getDescripcion(),el.getMonto(),el.getIdDocumentoAdjunto(),null,null));
                        }
                    }
                    if(!p13){lst_data.add(agregarColumnas("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null));}
                    if(!p14){lst_data.add(agregarColumnas("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null));}
                    

                    //actulizar le valor de los que ya se reistraron
                    Boolean exitoupdate = ActualizarAnexosPDFM(lst_dataupdate,anexo.getIdUsuarioPostulante());
                    //insertar los parametros que fatan insertar
                    Boolean exito = InsertarAnexosPDFM(lst_data,"anexo3",anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    if(exito && exitoupdate){
                        result.setInformacion("Se actualizaron los campos del anexo3");
                        result.setSuccess(true);
                    }else{
                        result.setInformacion("Ocurrió un error al actualizar los campos del anexo3");
                        result.setSuccess(true);
                    }
                }            
                    
            }
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarAnexo3", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarAnexo3", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [16-08-2021]
     * @modificado:
     * @descripción: {se obtienen los datos del anexo 2}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo2PFDMEntity> obtenerAnexo2PFDM(AnexosPFDMResquestEntity filtro) {
        ResultClassEntity<Anexo2PFDMEntity> result = new ResultClassEntity<Anexo2PFDMEntity>();
        try {
            ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM(filtro.getCodigoAnexo(), filtro.getIdPostulacionPFDM(),filtro.getIdUsuarioPostulacion());
            Anexo2PFDMEntity anexo2 = new Anexo2PFDMEntity();
            //Obtenemos la grilla de la formacion academica
            List<FormacionAcademicaEntity> lstForma= new ArrayList<>();
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getGrilla() && a.getCodGrilla().equals("FORMA")){
                    FormacionAcademicaEntity obj = new FormacionAcademicaEntity();
                    obj.setGradoTitulo(a.getValue1());
                    obj.setEspecialidad(a.getValue2());
                    obj.setNombreUniversidadInstituto(a.getValue3());
                    obj.setAnioEmisionTitulo(a.getValue4());
                    lstForma.add(obj);
                }
			});
            anexo2.setFormacionAcademica(lstForma);
            //Obtener la grilla de la experiencia laboral
            List<ExperienciaLaboralEntity> lstExperi = new ArrayList<>();
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getGrilla() && a.getCodGrilla().equals("EXPELA")){
                    ExperienciaLaboralEntity obj = new ExperienciaLaboralEntity();
                    obj.setEntidadEmpresaOtros(a.getValue1());
                    obj.setCargo(a.getValue2());
                    obj.setTiempoanios(a.getValue3());
                    obj.setDescActvRealizadas(a.getValue4());
                    obj.setDatosContactoEmpresa(a.getValue5());
                    lstExperi.add(obj);
                }
			});
            anexo2.setExperienciaLaboral(lstExperi);
            //Se llenan los otros campos del anexo
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getDescripcion().equals("profeApellidosNombres")){
                    anexo2.setProfeApellidosNombres(a.getValue1());
                }else if(a.getDescripcion().equals("profeDNIRUC")){
                    anexo2.setProfeDNIRUC(a.getValue1());
                }else if(a.getDescripcion().equals("profeDomicilio")){
                    anexo2.setProfeDomicilio(a.getValue1());
                }else if(a.getDescripcion().equals("profeSector")){
                    anexo2.setProfeSector(a.getValue1());
                }else if(a.getDescripcion().equals("profeIdDistrito")){
                    anexo2.setProfeIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("profeIdProvincia")){
                    anexo2.setProfeIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("profeIdDepartamento")){
                    anexo2.setProfeIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("profeTelefono")){
                    anexo2.setProfeTelefono(a.getValue1());
                }else if(a.getDescripcion().equals("profeCelular")){
                    anexo2.setProfeCelular(a.getValue1());
                }else if(a.getDescripcion().equals("profeCorreo")){
                    anexo2.setProfeCorreo(a.getValue1());
                }else if(a.getDescripcion().equals("profeNombreRSSolicitante")){
                    anexo2.setProfeNombreRSSolicitante(a.getValue1());
                }else if(a.getDescripcion().equals("formaProfesion")){
                    anexo2.setFormaProfesion(a.getValue1());
                }else if(a.getDescripcion().equals("formaNroRegistroColegioProfesional")){
                    anexo2.setFormaNroRegistroColegioProfesional(a.getValue1());
                }else if(a.getDescripcion().equals("lugarFirma")){
                    anexo2.setLugarFirma(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFirma")){
                    anexo2.setFechaFirma(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntosFormacion")){
                    anexo2.setIdAdjuntosFormacion(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntosExperiencia")){
                    anexo2.setIdAdjuntosExperiencia(a.getValue1());
                }
             });

            result.setData(anexo2);
            result.setSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - obtenerAnexo2PFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [16-08-2021]
     * @modificado:
     * @descripción: {se obtienen los datos del anexo 3}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo3PFDMEntity> obtenerAnexo3PFDM(AnexosPFDMResquestEntity filtro) {
        ResultClassEntity<Anexo3PFDMEntity> result = new ResultClassEntity<Anexo3PFDMEntity>();
        try {
            ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM(filtro.getCodigoAnexo(), filtro.getIdPostulacionPFDM(),filtro.getIdUsuarioPostulacion());
            Anexo3PFDMEntity anexo3 = new Anexo3PFDMEntity();
            //Obtenemos la grilla de las coordenadas UTM
            List<IngresosCuentasEntity> lstIngresos = new ArrayList<>();
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getGrilla() && a.getCodGrilla().equals("INGRE")){
                    IngresosCuentasEntity obj = new IngresosCuentasEntity();
                    obj.setTipoNumeroCodigoDoc(a.getValue1());
                    obj.setDescripcion(a.getValue2());
                    obj.setMonto(a.getValue3());
                    obj.setIdDocumentoAdjunto(a.getValue4());
                    lstIngresos.add(obj);
                }
			});
            anexo3.setPostulanteIngresos(lstIngresos);
            //Se llenan los otros campos del anexo
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getDescripcion().equals("funcioNombresApellidos")){
                    anexo3.setFuncioNombresApellidos(a.getValue1());
                }else if(a.getDescripcion().equals("funcioCargo")){
                    anexo3.setFuncioCargo(a.getValue1());
                }else if(a.getDescripcion().equals("postulanteNombresApellidos")){
                    anexo3.setPostulanteNombresApellidos(a.getValue1());
                }else if(a.getDescripcion().equals("postulanteDniRuc")){
                    anexo3.setPostulanteDniRuc(a.getValue1());
                }else if(a.getDescripcion().equals("postulanteDomicilio")){
                    anexo3.setPostulanteDomicilio(a.getValue1());
                }else if(a.getDescripcion().equals("postulanteConsecion")){
                    anexo3.setPostulanteConsecion(a.getValue1());
                }else if(a.getDescripcion().equals("areaSuperficie")){
                    anexo3.setAreaSuperficie(a.getValue1());
                }else if(a.getDescripcion().equals("areaSector")){
                    anexo3.setAreaSector(a.getValue1());
                }else if(a.getDescripcion().equals("areIdDistrito")){
                    anexo3.setAreIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdProvincia")){
                    anexo3.setAreaIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdDepartamento")){
                    anexo3.setAreaIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("lugarFirma")){
                    anexo3.setLugarFirma(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFirma")){
                    anexo3.setFechaFirma(a.getValue1());
                }
			});

            result.setData(anexo3);
            result.setSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - obtenerAnexo1PFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [17-08-2021]
     * @modificado:
     * @descripción: {guardar anexo 4}
     * @param:Anexo4PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo4(Anexo4PFDMEntity anexo) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            if(!(anexo.getIdPostulacionPFDM().equals(null) || anexo.getIdPostulacionPFDM().equals(0))){
                if(!(anexo==null)){
                    //Obtener el anexo4 por el idpostulacionpfdm y el idusuariopostulacion
                    ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM("anexo4", anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    //actualizar datos del anexo 4
                    Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false,
                    p14= false,p15= false,p16= false,p17= false,p18= false,p19= false,p20= false,p21= false,p22= false;
                    List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                    List<AnexosPFDMEntity> lst_dataupdate = new ArrayList<>();
                    for (AnexosPFDMEntity obj:lstAnexo.getData()) {
                        if(obj.getDescripcion().equals("soliNombreRS")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("soliNombreRS",anexo.getSoliNombreRS(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliSuperficie")){
                            p2=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliSuperficie",anexo.getSoliSuperficie(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliPlazo")){
                            p3 =true;
                            lst_dataupdate.add(agregarColumnasupdate("soliPlazo",anexo.getSoliPlazo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliUbicacionPolitica")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliUbicacionPolitica",anexo.getSoliUbicacionPolitica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliSector")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliSector",anexo.getSoliSector(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliIdDistrito")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliIdDistrito",anexo.getSoliIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliIdProvincia")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliIdProvincia",anexo.getSoliIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliIdDepartamento")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliIdDepartamento",anexo.getSoliIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("tamaConectividad")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("tamaConectividad",anexo.getTamaConectividad(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("tamaAmbito")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("tamaAmbito",anexo.getTamaAmbito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("tamaProteccion")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("tamaProteccion",anexo.getTamaProteccion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("tamaCuenca")){
                            p12=true;
                            lst_dataupdate.add(agregarColumnasupdate("tamaCuenca",anexo.getTamaCuenca(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("tamaOtros")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("tamaOtros",anexo.getTamaOtros(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("justificacion")){
                            p14=true;
                            lst_dataupdate.add(agregarColumnasupdate("justificacion",anexo.getJustificacion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idMapaAdjunto")){
                            p15=true;
                            lst_dataupdate.add(agregarColumnasupdate("idMapaAdjunto",anexo.getIdMapaAdjunto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("delimitacion")){
                            p16=true;
                            lst_dataupdate.add(agregarColumnasupdate("delimitacion",anexo.getDelimitacion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("observaciones")){
                            p17=true;
                            lst_dataupdate.add(agregarColumnasupdate("observaciones",anexo.getObservaciones(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fuentesBibliograficas")){
                            p18=true;
                            lst_dataupdate.add(agregarColumnasupdate("fuentesBibliograficas",anexo.getFuentesBibliograficas(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idShapeFileFirmado")){
                            p19=true;
                            lst_dataupdate.add(agregarColumnasupdate("idShapeFileFirmado",anexo.getIdShapeFileFirmado(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idImagenesConsilidado")){
                            p20=true;
                            lst_dataupdate.add(agregarColumnasupdate("idImagenesConsilidado",anexo.getIdImagenesConsilidado(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("lugarFirma")){
                            p21=true;
                            lst_dataupdate.add(agregarColumnasupdate("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fechaFirma")){
                            p22=true;
                            lst_dataupdate.add(agregarColumnasupdate("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }
                    }
                    //agregar registros
                    if(!p1){lst_data.add(agregarColumnas("soliNombreRS",anexo.getSoliNombreRS(),null,null,null,null,null));}
                    if(!p2){lst_data.add(agregarColumnas("soliSuperficie",anexo.getSoliSuperficie(),null,null,null,null,null));}
                    if(!p3){lst_data.add(agregarColumnas("soliPlazo",anexo.getSoliPlazo(),null,null,null,null,null));}
                    if(!p4){lst_data.add(agregarColumnas("soliUbicacionPolitica",anexo.getSoliUbicacionPolitica(),null,null,null,null,null));}
                    if(!p5){lst_data.add(agregarColumnas("soliSector",anexo.getSoliSector(),null,null,null,null,null));}
                    if(!p6){lst_data.add(agregarColumnas("soliIdDistrito",anexo.getSoliIdDistrito(),null,null,null,null,null));}
                    if(!p7){lst_data.add(agregarColumnas("soliIdProvincia",anexo.getSoliIdProvincia(),null,null,null,null,null));}
                    if(!p8){lst_data.add(agregarColumnas("soliIdDepartamento",anexo.getSoliIdDepartamento(),null,null,null,null,null));}
                    if(!p9){lst_data.add(agregarColumnas("tamaConectividad",anexo.getTamaConectividad(),null,null,null,null,null));}
                    if(!p10){lst_data.add(agregarColumnas("tamaAmbito",anexo.getTamaAmbito(),null,null,null,null,null));}
                    if(!p11){lst_data.add(agregarColumnas("tamaProteccion",anexo.getTamaProteccion(),null,null,null,null,null));}
                    if(!p12){lst_data.add(agregarColumnas("tamaCuenca",anexo.getTamaCuenca(),null,null,null,null,null));}
                    if(!p13){lst_data.add(agregarColumnas("tamaOtros",anexo.getTamaOtros(),null,null,null,null,null));}
                    if(!p14){lst_data.add(agregarColumnas("justificacion",anexo.getJustificacion(),null,null,null,null,null));}
                    if(!p15){lst_data.add(agregarColumnas("idMapaAdjunto",anexo.getIdMapaAdjunto(),null,null,null,null,null));}
                    if(!p16){lst_data.add(agregarColumnas("delimitacion",anexo.getDelimitacion(),null,null,null,null,null));}
                    if(!p17){lst_data.add(agregarColumnas("observaciones",anexo.getObservaciones(),null,null,null,null,null));}
                    if(!p18){lst_data.add(agregarColumnas("fuentesBibliograficas",anexo.getFuentesBibliograficas(),null,null,null,null,null));}
                    if(!p19){lst_data.add(agregarColumnas("idShapeFileFirmado",anexo.getIdShapeFileFirmado(),null,null,null,null,null));}
                    if(!p20){lst_data.add(agregarColumnas("idImagenesConsilidado",anexo.getIdImagenesConsilidado(),null,null,null,null,null));}
                    if(!p21){lst_data.add(agregarColumnas("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null));}
                    if(!p22){lst_data.add(agregarColumnas("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null));}
                    //actulizar le valor de los que ya se reistraron
                    Boolean exitoupdate = ActualizarAnexosPDFM(lst_dataupdate,anexo.getIdUsuarioPostulante());
                    //insertar los parametros que fatan insertar
                    Boolean exito = InsertarAnexosPDFM(lst_data,"anexo4",anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    if(exito && exitoupdate){
                        result.setInformacion("Se actualizaron los campos del anexo4");
                        result.setSuccess(true);
                    }else{
                        result.setInformacion("Ocurrió un error al actualizar los campos del anexo4");
                        result.setSuccess(true);
                    }
                }            
                    
            }
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarAnexo4", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarAnexo4", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }


    /**
     * @autor: JaquelineDB [17-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 4}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo4PFDMEntity> obtenerAnexo4PFDM(AnexosPFDMResquestEntity filtro) {
        ResultClassEntity<Anexo4PFDMEntity> result = new ResultClassEntity<Anexo4PFDMEntity>();
        try {
            ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM(filtro.getCodigoAnexo(), filtro.getIdPostulacionPFDM(),filtro.getIdUsuarioPostulacion());
            Anexo4PFDMEntity anexo4 = new Anexo4PFDMEntity();
            //Se llenan los otros campos del anexo
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getDescripcion().equals("soliNombreRS")){
                    anexo4.setSoliNombreRS(a.getValue1());
                }else if(a.getDescripcion().equals("soliSuperficie")){
                    anexo4.setSoliSuperficie(a.getValue1());
                }else if(a.getDescripcion().equals("soliPlazo")){
                    anexo4.setSoliPlazo(a.getValue1());
                }else if(a.getDescripcion().equals("soliUbicacionPolitica")){
                    anexo4.setSoliUbicacionPolitica(a.getValue1());
                }else if(a.getDescripcion().equals("soliSector")){
                    anexo4.setSoliSector(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdDistrito")){
                    anexo4.setSoliIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdProvincia")){
                    anexo4.setSoliIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdDepartamento")){
                    anexo4.setSoliIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("tamaConectividad")){
                    anexo4.setTamaConectividad(a.getValue1());
                }else if(a.getDescripcion().equals("tamaAmbito")){
                    anexo4.setTamaAmbito(a.getValue1());
                }else if(a.getDescripcion().equals("tamaProteccion")){
                    anexo4.setTamaProteccion(a.getValue1());
                }else if(a.getDescripcion().equals("tamaCuenca")){
                    anexo4.setTamaCuenca(a.getValue1());
                }else if(a.getDescripcion().equals("tamaOtros")){
                    anexo4.setTamaOtros(a.getValue1());
                }else if(a.getDescripcion().equals("justificacion")){
                    anexo4.setJustificacion(a.getValue1());
                }else if(a.getDescripcion().equals("idMapaAdjunto")){
                    anexo4.setIdMapaAdjunto(a.getValue1());
                }else if(a.getDescripcion().equals("delimitacion")){
                    anexo4.setDelimitacion(a.getValue1());
                }else if(a.getDescripcion().equals("observaciones")){
                    anexo4.setObservaciones(a.getValue1());
                }else if(a.getDescripcion().equals("fuentesBibliograficas")){
                    anexo4.setFuentesBibliograficas(a.getValue1());
                }else if(a.getDescripcion().equals("idShapeFileFirmado")){
                    anexo4.setIdShapeFileFirmado(a.getValue1());
                }else if(a.getDescripcion().equals("idImagenesConsilidado")){
                    anexo4.setIdImagenesConsilidado(a.getValue1());
                }else if(a.getDescripcion().equals("lugarFirma")){
                    anexo4.setLugarFirma(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFirma")){
                    anexo4.setFechaFirma(a.getValue1());
                }
			});

            result.setData(anexo4);
            result.setSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - obtenerAnexo4PFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [18-08-2021]
     * @modificado:
     * @descripción: {guardas los datos del anexo 5}
     * @param:Anexo5PFDMEntity
     */
    @Override
    public ResultClassEntity guardarAnexo5(Anexo5PFDMEntity anexo) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            if(!(anexo.getIdPostulacionPFDM().equals(null) || anexo.getIdPostulacionPFDM().equals(0))){
                if(!(anexo==null)){
                    //Obtener el anexo5 por el idpostulacionpfdm y el idusuariopostulacion
                    ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM("anexo5", anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    //actualizar datos del anexo 5
                    Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false,
                    p14= false,p15= false,p16= false,p17= false,p18= false,p19= false,p20= false,p21= false,p22= false;
                    List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                    List<AnexosPFDMEntity> lst_dataupdate = new ArrayList<>();
                    for (AnexosPFDMEntity obj:lstAnexo.getData()) {
                        if(obj.getDescripcion().equals("motivoSolicitud")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("motivoSolicitud",anexo.getMotivoSolicitud(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliNombre")){
                            p2 =true;
                            lst_dataupdate.add(agregarColumnasupdate("soliNombre",anexo.getSoliNombre(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliVigencia")){
                            p3=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliVigencia",anexo.getSoliVigencia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliSuperficie")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliSuperficie",anexo.getSoliSuperficie(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliUbicacion")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliUbicacion",anexo.getSoliUbicacion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliIdDepartamento")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliIdDepartamento",anexo.getSoliIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliIdProvincia")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliIdProvincia",anexo.getSoliIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliIdDistrito")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliIdDistrito",anexo.getSoliIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("soliSector")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("soliSector",anexo.getSoliSector(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAreaSolicitadaAdjunto")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAreaSolicitadaAdjunto",anexo.getIdAreaSolicitadaAdjunto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idVerticesAdjunto")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("idVerticesAdjunto",anexo.getIdVerticesAdjunto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("objetivoConcesion")){
                            p12=true;
                            lst_dataupdate.add(agregarColumnasupdate("objetivoConcesion",anexo.getObjetivoConcesion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("descripcionProyecto")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("descripcionProyecto",anexo.getDescripcionProyecto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fechaFinEntregaPropuesta")){
                            p14=true;
                            lst_dataupdate.add(agregarColumnasupdate("fechaFinEntregaPropuesta",anexo.getFechaFinEntregaPropuesta(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("direccionEntregaPropuesta")){
                            p15=true;
                            lst_dataupdate.add(agregarColumnasupdate("direccionEntregaPropuesta",anexo.getDireccionEntregaPropuesta(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("lugarFirma")){
                            p16=true;
                            lst_dataupdate.add(agregarColumnasupdate("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fechaFirma")){
                            p17=true;
                            lst_dataupdate.add(agregarColumnasupdate("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }
                    }
                    //agregar registros
                    if(!p1){lst_data.add(agregarColumnas("motivoSolicitud",anexo.getMotivoSolicitud(),null,null,null,null,null));}
                    if(!p2){lst_data.add(agregarColumnas("soliNombre",anexo.getSoliNombre(),null,null,null,null,null));}
                    if(!p3){lst_data.add(agregarColumnas("soliVigencia",anexo.getSoliVigencia(),null,null,null,null,null));}
                    if(!p4){lst_data.add(agregarColumnas("soliSuperficie",anexo.getSoliSuperficie(),null,null,null,null,null));}
                    if(!p5){lst_data.add(agregarColumnas("soliUbicacion",anexo.getSoliUbicacion(),null,null,null,null,null));}
                    if(!p6){lst_data.add(agregarColumnas("soliIdDepartamento",anexo.getSoliIdDepartamento(),null,null,null,null,null));}
                    if(!p7){lst_data.add(agregarColumnas("soliIdProvincia",anexo.getSoliIdProvincia(),null,null,null,null,null));}
                    if(!p8){lst_data.add(agregarColumnas("soliIdDistrito",anexo.getSoliIdDistrito(),null,null,null,null,null));}
                    if(!p9){lst_data.add(agregarColumnas("soliSector",anexo.getSoliSector(),null,null,null,null,null));}
                    if(!p10){lst_data.add(agregarColumnas("idAreaSolicitadaAdjunto",anexo.getIdAreaSolicitadaAdjunto(),null,null,null,null,null));}
                    if(!p11){lst_data.add(agregarColumnas("idVerticesAdjunto",anexo.getIdVerticesAdjunto(),null,null,null,null,null));}
                    if(!p12){lst_data.add(agregarColumnas("objetivoConcesion",anexo.getObjetivoConcesion(),null,null,null,null,null));}
                    if(!p13){lst_data.add(agregarColumnas("descripcionProyecto",anexo.getDescripcionProyecto(),null,null,null,null,null));}
                    if(!p14){lst_data.add(agregarColumnas("fechaFinEntregaPropuesta",anexo.getFechaFinEntregaPropuesta(),null,null,null,null,null));}
                    if(!p15){lst_data.add(agregarColumnas("direccionEntregaPropuesta",anexo.getDireccionEntregaPropuesta(),null,null,null,null,null));}
                    if(!p16){lst_data.add(agregarColumnas("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null));}
                    if(!p17){lst_data.add(agregarColumnas("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null));}
                    //actulizar le valor de los que ya se reistraron
                    Boolean exitoupdate = ActualizarAnexosPDFM(lst_dataupdate,anexo.getIdUsuarioPostulante());
                    //insertar los parametros que fatan insertar
                    Boolean exito = InsertarAnexosPDFM(lst_data,"anexo5",anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    if(exito && exitoupdate){
                        result.setInformacion("Se actualizaron los campos del anexo5");
                        result.setSuccess(true);
                    }else{
                        result.setInformacion("Ocurrió un error al actualizar los campos del anexo5");
                        result.setSuccess(true);
                    }
                }            
                    
            }
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarAnexo5", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarAnexo3", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }


    /**
     * @autor: JaquelineDB [18-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 5}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo5PFDMEntity> obtenerAnexo5PFDM(AnexosPFDMResquestEntity filtro) {
        ResultClassEntity<Anexo5PFDMEntity> result = new ResultClassEntity<Anexo5PFDMEntity>();
        try {
            ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM(filtro.getCodigoAnexo(), filtro.getIdPostulacionPFDM(),filtro.getIdUsuarioPostulacion());
            Anexo5PFDMEntity anexo = new Anexo5PFDMEntity();
            //Se llenan los otros campos del anexo
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getDescripcion().equals("motivoSolicitud")){
                    anexo.setMotivoSolicitud(a.getValue1());
                }else if(a.getDescripcion().equals("soliNombre")){
                    anexo.setSoliNombre(a.getValue1());
                }else if(a.getDescripcion().equals("soliVigencia")){
                    anexo.setSoliVigencia(a.getValue1());
                }else if(a.getDescripcion().equals("soliSuperficie")){
                    anexo.setSoliSuperficie(a.getValue1());
                }else if(a.getDescripcion().equals("soliUbicacion")){
                    anexo.setSoliUbicacion(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdDepartamento")){
                    anexo.setSoliIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdProvincia")){
                    anexo.setSoliIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("soliIdDistrito")){
                    anexo.setSoliIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("soliSector")){
                    anexo.setSoliSector(a.getValue1());
                }else if(a.getDescripcion().equals("idAreaSolicitadaAdjunto")){
                    anexo.setIdAreaSolicitadaAdjunto(a.getValue1());
                }else if(a.getDescripcion().equals("idVerticesAdjunto")){
                    anexo.setIdVerticesAdjunto(a.getValue1());
                }else if(a.getDescripcion().equals("objetivoConcesion")){
                    anexo.setObjetivoConcesion(a.getValue1());
                }else if(a.getDescripcion().equals("descripcionProyecto")){
                    anexo.setDescripcionProyecto(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFinEntregaPropuesta")){
                    anexo.setFechaFinEntregaPropuesta(a.getValue1());
                }else if(a.getDescripcion().equals("direccionEntregaPropuesta")){
                    anexo.setDireccionEntregaPropuesta(a.getValue1());
                }else if(a.getDescripcion().equals("lugarFirma")){
                    anexo.setLugarFirma(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFirma")){
                    anexo.setFechaFirma(a.getValue1());
                }
			});

            result.setData(anexo);
            result.setSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - obtenerAnexo5PFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [19-08-2021]
     * @modificado:
     * @descripción: {guardar datos del anexo 6}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity guardarAnexo6(Anexo6PFDMEntity anexo) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        int salida = -1;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            if(!(anexo.getIdPostulacionPFDM().equals(null) || anexo.getIdPostulacionPFDM().equals(0))){
                if(!(anexo==null)){
                    //Obtener el anexo5 por el idpostulacionpfdm y el idusuariopostulacion
                    ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM("anexo6", anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    //actualizar datos del anexo 5
                    Boolean p1= false,p2= false,p3= false,p4= false,p5= false,p6= false,p7= false,p8= false,p9= false,p10= false,p11= false,p12 = false,p13 = false,
                    p14= false,p15= false,p16= false,p17= false,p18= false,p19= false,p20= false,p21= false,p22= false,p23 = false,
                    p24= false,p25= false,p26= false,p27= false,p28= false,p29= false,p30= false,p31= false,p32= false,p33 = false,
                    p34= false,p35= false,p36= false,p37= false,p38= false,p39= false,p40= false,p41= false,p42= false,p43 = false,
                    p44= false,p45= false,p46= false,p47= false,p48= false,p49= false;
                    List<AnexosPFDMEntity> lst_data = new ArrayList<>();
                    List<AnexosPFDMEntity> lst_dataupdate = new ArrayList<>();
                    for (AnexosPFDMEntity obj:lstAnexo.getData()) {
                        if(obj.getDescripcion().equals("posNombreRS")){
                            p1 =true;
                            lst_dataupdate.add(agregarColumnasupdate("posNombreRS",anexo.getPosNombreRS(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posDni")){
                            p2 =true;
                            lst_dataupdate.add(agregarColumnasupdate("posDni",anexo.getPosDni(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posRuc")){
                            p3=true;
                            lst_dataupdate.add(agregarColumnasupdate("posRuc",anexo.getPosRuc(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posDomicilioLegal")){
                            p4=true;
                            lst_dataupdate.add(agregarColumnasupdate("posDomicilioLegal",anexo.getPosDomicilioLegal(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posTelefonoCelular")){
                            p5=true;
                            lst_dataupdate.add(agregarColumnasupdate("posTelefonoCelular",anexo.getPosTelefonoCelular(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posCorreo")){
                            p6=true;
                            lst_dataupdate.add(agregarColumnasupdate("posCorreo",anexo.getPosCorreo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posNombreRepresentante")){
                            p7=true;
                            lst_dataupdate.add(agregarColumnasupdate("posNombreRepresentante",anexo.getPosNombreRepresentante(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("posDomiciloRepresentante")){
                            p8=true;
                            lst_dataupdate.add(agregarColumnasupdate("posDomiciloRepresentante",anexo.getPosDomiciloRepresentante(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaTipoConcesion")){
                            p9=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaTipoConcesion",anexo.getAreaTipoConcesion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaVigencia")){
                            p10=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaVigencia",anexo.getAreaVigencia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaSuperficie")){
                            p11=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaSuperficie",anexo.getAreaSuperficie(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaSector")){
                            p12=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaSector",anexo.getAreaSector(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaIdDepartamento")){
                            p13=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaIdDepartamento",anexo.getAreaIdDepartamento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaIdProvincia")){
                            p14=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaIdProvincia",anexo.getAreaIdProvincia(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaIdDistrito")){
                            p15=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaIdDistrito",anexo.getAreaIdDistrito(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaRecursosAproveConserv")){
                            p16 =true;
                            lst_dataupdate.add(agregarColumnasupdate("areaRecursosAproveConserv",anexo.getAreaRecursosAproveConserv(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaMaderable")){
                            p17 =true;
                            lst_dataupdate.add(agregarColumnasupdate("areaMaderable",anexo.getAreaMaderable(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaPFDM")){
                            p18=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaPFDM",anexo.getAreaPFDM(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaEcoturismo")){
                            p19=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaEcoturismo",anexo.getAreaEcoturismo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaFuanaSilvestre")){
                            p20=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaFuanaSilvestre",anexo.getAreaFuanaSilvestre(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("areaServEcosistematicos")){
                            p21=true;
                            lst_dataupdate.add(agregarColumnasupdate("areaServEcosistematicos",anexo.getAreaServEcosistematicos(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("objetivos")){
                            p22=true;
                            lst_dataupdate.add(agregarColumnasupdate("objetivos",anexo.getObjetivos(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("metas")){
                            p23=true;
                            lst_dataupdate.add(agregarColumnasupdate("metas",anexo.getMetas(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("caractFisica")){
                            p24=true;
                            lst_dataupdate.add(agregarColumnasupdate("caractFisica",anexo.getCaractFisica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoFisica")){
                            p25=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoFisica",anexo.getIdAdjuntoFisica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("caracBiologica")){
                            p26=true;
                            lst_dataupdate.add(agregarColumnasupdate("caracBiologica",anexo.getCaracBiologica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoBiologica")){
                            p27=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoBiologica",anexo.getIdAdjuntoBiologica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("caracSocioeconomica")){
                            p28=true;
                            lst_dataupdate.add(agregarColumnasupdate("caracSocioeconomica",anexo.getCaracSocioeconomica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoSocioeconomica")){
                            p29=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoSocioeconomica",anexo.getIdAdjuntoSocioeconomica(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("justiFactoresAmbienBiolo")){
                            p30=true;
                            lst_dataupdate.add(agregarColumnasupdate("justiFactoresAmbienBiolo",anexo.getJustiFactoresAmbienBiolo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoAmbienBiolo")){
                            p31=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoAmbienBiolo",anexo.getIdAdjuntoAmbienBiolo(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("justiFactoresSocioEconoCultu")){
                            p32=true;
                            lst_dataupdate.add(agregarColumnasupdate("justiFactoresSocioEconoCultu",anexo.getJustiFactoresSocioEconoCultu(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoSocioEconoCultu")){
                            p33=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoSocioEconoCultu",anexo.getIdAdjuntoSocioEconoCultu(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("justiManejoConcesion")){
                            p34=true;
                            lst_dataupdate.add(agregarColumnasupdate("justiManejoConcesion",anexo.getJustiManejoConcesion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoManejoConcesion")){
                            p35=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoManejoConcesion",anexo.getIdAdjuntoManejoConcesion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("sustePresupuesto")){
                            p36=true;
                            lst_dataupdate.add(agregarColumnasupdate("sustePresupuesto",anexo.getSustePresupuesto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoPresupuesto")){
                            p37=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoPresupuesto",anexo.getIdAdjuntoPresupuesto(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("susteAnalisisFinanciero")){
                            p38=true;
                            lst_dataupdate.add(agregarColumnasupdate("susteAnalisisFinanciero",anexo.getSusteAnalisisFinanciero(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoAnalisisFinanciero")){
                            p39=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoAnalisisFinanciero",anexo.getIdAdjuntoAnalisisFinanciero(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("expeSolicitanteActv")){
                            p40=true;
                            lst_dataupdate.add(agregarColumnasupdate("expeSolicitanteActv",anexo.getExpeSolicitanteActv(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoSolicitanteActv")){
                            p41=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoSolicitanteActv",anexo.getIdAdjuntoSolicitanteActv(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoAreaSolicitada")){
                            p42=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoAreaSolicitada",anexo.getIdAdjuntoAreaSolicitada(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoVertices")){
                            p43=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoVertices",anexo.getIdAdjuntoVertices(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoMapaUbicacion")){
                            p44=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoMapaUbicacion",anexo.getIdAdjuntoMapaUbicacion(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoMapaBaseArea")){
                            p45=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoMapaBaseArea",anexo.getIdAdjuntoMapaBaseArea(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoDocLevantamiento")){
                            p46=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoDocLevantamiento",anexo.getIdAdjuntoDocLevantamiento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("idAdjuntoDocFuentesFinanciamiento")){
                            p47=true;
                            lst_dataupdate.add(agregarColumnasupdate("idAdjuntoDocFuentesFinanciamiento",anexo.getIdAdjuntoDocFuentesFinanciamiento(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("lugarFirma")){
                            p48=true;
                            lst_dataupdate.add(agregarColumnasupdate("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }else if(obj.getDescripcion().equals("fechaFirma")){
                            p49=true;
                            lst_dataupdate.add(agregarColumnasupdate("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null,obj.getIdAnexosPFDM()));
                        }
                    }
                    //agregar registros
                    if(!p1){lst_data.add(agregarColumnas("posNombreRS",anexo.getPosNombreRS(),null,null,null,null,null));}
                    if(!p2){lst_data.add(agregarColumnas("posDni",anexo.getPosDni(),null,null,null,null,null));}
                    if(!p3){lst_data.add(agregarColumnas("posRuc",anexo.getPosRuc(),null,null,null,null,null));}
                    if(!p4){lst_data.add(agregarColumnas("posDomicilioLegal",anexo.getPosDomicilioLegal(),null,null,null,null,null));}
                    if(!p5){lst_data.add(agregarColumnas("posTelefonoCelular",anexo.getPosTelefonoCelular(),null,null,null,null,null));}
                    if(!p6){lst_data.add(agregarColumnas("posCorreo",anexo.getPosCorreo(),null,null,null,null,null));}
                    if(!p7){lst_data.add(agregarColumnas("posNombreRepresentante",anexo.getPosNombreRepresentante(),null,null,null,null,null));}
                    if(!p8){lst_data.add(agregarColumnas("posDomiciloRepresentante",anexo.getPosDomiciloRepresentante(),null,null,null,null,null));}
                    if(!p9){lst_data.add(agregarColumnas("areaTipoConcesion",anexo.getAreaTipoConcesion(),null,null,null,null,null));}
                    if(!p10){lst_data.add(agregarColumnas("areaVigencia",anexo.getAreaVigencia(),null,null,null,null,null));}
                    if(!p11){lst_data.add(agregarColumnas("areaSuperficie",anexo.getAreaSuperficie(),null,null,null,null,null));}
                    if(!p12){lst_data.add(agregarColumnas("areaSector",anexo.getAreaSector(),null,null,null,null,null));}
                    if(!p13){lst_data.add(agregarColumnas("areaIdDepartamento",anexo.getAreaIdDepartamento(),null,null,null,null,null));}
                    if(!p14){lst_data.add(agregarColumnas("areaIdProvincia",anexo.getAreaIdProvincia(),null,null,null,null,null));}
                    if(!p15){lst_data.add(agregarColumnas("areaIdDistrito",anexo.getAreaIdDistrito(),null,null,null,null,null));}
                    if(!p16){lst_data.add(agregarColumnas("areaRecursosAproveConserv",anexo.getAreaRecursosAproveConserv(),null,null,null,null,null));}
                    if(!p17){lst_data.add(agregarColumnas("areaMaderable",anexo.getAreaMaderable(),null,null,null,null,null));}
                    if(!p18){lst_data.add(agregarColumnas("areaPFDM",anexo.getAreaPFDM(),null,null,null,null,null));}
                    if(!p19){lst_data.add(agregarColumnas("areaEcoturismo",anexo.getAreaEcoturismo(),null,null,null,null,null));}
                    if(!p20){lst_data.add(agregarColumnas("areaFuanaSilvestre",anexo.getAreaFuanaSilvestre(),null,null,null,null,null));}
                    if(!p21){lst_data.add(agregarColumnas("areaServEcosistematicos",anexo.getAreaServEcosistematicos(),null,null,null,null,null));}
                    if(!p22){lst_data.add(agregarColumnas("objetivos",anexo.getObjetivos(),null,null,null,null,null));}
                    if(!p23){lst_data.add(agregarColumnas("metas",anexo.getMetas(),null,null,null,null,null));}
                    if(!p24){lst_data.add(agregarColumnas("caractFisica",anexo.getCaractFisica(),null,null,null,null,null));}
                    if(!p25){lst_data.add(agregarColumnas("idAdjuntoFisica",anexo.getIdAdjuntoFisica(),null,null,null,null,null));}
                    if(!p26){lst_data.add(agregarColumnas("caracBiologica",anexo.getCaracBiologica(),null,null,null,null,null));}
                    if(!p27){lst_data.add(agregarColumnas("idAdjuntoBiologica",anexo.getIdAdjuntoBiologica(),null,null,null,null,null));}
                    if(!p28){lst_data.add(agregarColumnas("caracSocioeconomica",anexo.getCaracSocioeconomica(),null,null,null,null,null));}
                    if(!p29){lst_data.add(agregarColumnas("idAdjuntoSocioeconomica",anexo.getIdAdjuntoSocioeconomica(),null,null,null,null,null));}
                    if(!p30){lst_data.add(agregarColumnas("justiFactoresAmbienBiolo",anexo.getJustiFactoresAmbienBiolo(),null,null,null,null,null));}
                    if(!p31){lst_data.add(agregarColumnas("idAdjuntoAmbienBiolo",anexo.getIdAdjuntoAmbienBiolo(),null,null,null,null,null));}
                    if(!p32){lst_data.add(agregarColumnas("justiFactoresSocioEconoCultu",anexo.getJustiFactoresSocioEconoCultu(),null,null,null,null,null));}
                    if(!p33){lst_data.add(agregarColumnas("idAdjuntoSocioEconoCultu",anexo.getIdAdjuntoSocioEconoCultu(),null,null,null,null,null));}
                    if(!p34){lst_data.add(agregarColumnas("justiManejoConcesion",anexo.getJustiManejoConcesion(),null,null,null,null,null));}
                    if(!p35){lst_data.add(agregarColumnas("idAdjuntoManejoConcesion",anexo.getIdAdjuntoManejoConcesion(),null,null,null,null,null));}
                    if(!p36){lst_data.add(agregarColumnas("sustePresupuesto",anexo.getSustePresupuesto(),null,null,null,null,null));}
                    if(!p37){lst_data.add(agregarColumnas("idAdjuntoPresupuesto",anexo.getIdAdjuntoPresupuesto(),null,null,null,null,null));}
                    if(!p38){lst_data.add(agregarColumnas("susteAnalisisFinanciero",anexo.getSusteAnalisisFinanciero(),null,null,null,null,null));}
                    if(!p39){lst_data.add(agregarColumnas("idAdjuntoAnalisisFinanciero",anexo.getIdAdjuntoAnalisisFinanciero(),null,null,null,null,null));}
                    if(!p40){lst_data.add(agregarColumnas("expeSolicitanteActv",anexo.getExpeSolicitanteActv(),null,null,null,null,null));}
                    if(!p41){lst_data.add(agregarColumnas("idAdjuntoSolicitanteActv",anexo.getIdAdjuntoSolicitanteActv(),null,null,null,null,null));}
                    if(!p42){lst_data.add(agregarColumnas("idAdjuntoAreaSolicitada",anexo.getIdAdjuntoAreaSolicitada(),null,null,null,null,null));}
                    if(!p43){lst_data.add(agregarColumnas("idAdjuntoVertices",anexo.getIdAdjuntoVertices(),null,null,null,null,null));}
                    if(!p44){lst_data.add(agregarColumnas("idAdjuntoMapaUbicacion",anexo.getIdAdjuntoMapaUbicacion(),null,null,null,null,null));}
                    if(!p45){lst_data.add(agregarColumnas("idAdjuntoMapaBaseArea",anexo.getIdAdjuntoMapaBaseArea(),null,null,null,null,null));}
                    if(!p46){lst_data.add(agregarColumnas("idAdjuntoDocLevantamiento",anexo.getIdAdjuntoDocLevantamiento(),null,null,null,null,null));}
                    if(!p47){lst_data.add(agregarColumnas("idAdjuntoDocFuentesFinanciamiento",anexo.getIdAdjuntoDocFuentesFinanciamiento(),null,null,null,null,null));}
                    if(!p48){lst_data.add(agregarColumnas("lugarFirma",anexo.getLugarFirma(),null,null,null,null,null));}
                    if(!p49){lst_data.add(agregarColumnas("fechaFirma",anexo.getFechaFirma(),null,null,null,null,null));}
                    //actulizar le valor de los que ya se reistraron
                    Boolean exitoupdate = ActualizarAnexosPDFM(lst_dataupdate,anexo.getIdUsuarioPostulante());
                    //insertar los parametros que fatan insertar
                    Boolean exito = InsertarAnexosPDFM(lst_data,"anexo6",anexo.getIdPostulacionPFDM(),anexo.getIdUsuarioPostulante());
                    if(exito && exitoupdate){
                        result.setInformacion("Se actualizaron los campos del anexo6");
                        result.setSuccess(true);
                    }else{
                        result.setInformacion("Ocurrió un error al actualizar los campos del anexo5");
                        result.setSuccess(true);
                    }
                }            
                    
            }
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - guardarAnexo5", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("PostulacionPFDMERepositoryImpl - guardarAnexo3", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [19-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 6}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultClassEntity<Anexo6PFDMEntity> obtenerAnexo6PFDM(AnexosPFDMResquestEntity filtro) {
        ResultClassEntity<Anexo6PFDMEntity> result = new ResultClassEntity<Anexo6PFDMEntity>();
        try {
            ResultEntity<AnexosPFDMEntity> lstAnexo =  obtenerAnexoPFDM(filtro.getCodigoAnexo(), filtro.getIdPostulacionPFDM(),filtro.getIdUsuarioPostulacion());
            Anexo6PFDMEntity anexo = new Anexo6PFDMEntity();
            //Se llenan los otros campos del anexo
            lstAnexo.getData().stream().forEach((a) -> {
				if(a.getDescripcion().equals("posNombreRS")){
                    anexo.setPosNombreRS(a.getValue1());
                }else if(a.getDescripcion().equals("posDni")){
                    anexo.setPosDni(a.getValue1());
                }else if(a.getDescripcion().equals("posRuc")){
                    anexo.setPosRuc(a.getValue1());
                }else if(a.getDescripcion().equals("posDomicilioLegal")){
                    anexo.setPosDomicilioLegal(a.getValue1());
                }else if(a.getDescripcion().equals("posTelefonoCelular")){
                    anexo.setPosTelefonoCelular(a.getValue1());
                }else if(a.getDescripcion().equals("posCorreo")){
                    anexo.setPosCorreo(a.getValue1());
                }else if(a.getDescripcion().equals("posNombreRepresentante")){
                    anexo.setPosNombreRepresentante(a.getValue1());
                }else if(a.getDescripcion().equals("posDomiciloRepresentante")){
                    anexo.setPosDomiciloRepresentante(a.getValue1());
                }else if(a.getDescripcion().equals("areaTipoConcesion")){
                    anexo.setAreaTipoConcesion(a.getValue1());
                }else if(a.getDescripcion().equals("areaVigencia")){
                    anexo.setAreaVigencia(a.getValue1());
                }else if(a.getDescripcion().equals("areaSuperficie")){
                    anexo.setAreaSuperficie(a.getValue1());
                }else if(a.getDescripcion().equals("areaSector")){
                    anexo.setAreaSector(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdDepartamento")){
                    anexo.setAreaIdDepartamento(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdProvincia")){
                    anexo.setAreaIdProvincia(a.getValue1());
                }else if(a.getDescripcion().equals("areaIdDistrito")){
                    anexo.setAreaIdDistrito(a.getValue1());
                }else if(a.getDescripcion().equals("areaRecursosAproveConserv")){
                    anexo.setAreaRecursosAproveConserv(a.getValue1());
                }else if(a.getDescripcion().equals("areaMaderable")){
                    anexo.setAreaMaderable(a.getValue1());
                }else if(a.getDescripcion().equals("areaPFDM")){
                    anexo.setAreaPFDM(a.getValue1());
                }else if(a.getDescripcion().equals("areaEcoturismo")){
                    anexo.setAreaEcoturismo(a.getValue1());
                }else if(a.getDescripcion().equals("areaFuanaSilvestre")){
                    anexo.setAreaFuanaSilvestre(a.getValue1());
                }else if(a.getDescripcion().equals("areaServEcosistematicos")){
                    anexo.setAreaServEcosistematicos(a.getValue1());
                }else if(a.getDescripcion().equals("objetivos")){
                    anexo.setObjetivos(a.getValue1());
                }else if(a.getDescripcion().equals("metas")){
                    anexo.setMetas(a.getValue1());
                }else if(a.getDescripcion().equals("caractFisica")){
                    anexo.setCaractFisica(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoFisica")){
                    anexo.setIdAdjuntoFisica(a.getValue1());
                }else if(a.getDescripcion().equals("caracBiologica")){
                    anexo.setCaracBiologica(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoBiologica")){
                    anexo.setIdAdjuntoBiologica(a.getValue1());
                }else if(a.getDescripcion().equals("caracSocioeconomica")){
                    anexo.setCaracSocioeconomica(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoSocioeconomica")){
                    anexo.setIdAdjuntoSocioeconomica(a.getValue1());
                }else if(a.getDescripcion().equals("justiFactoresAmbienBiolo")){
                    anexo.setJustiFactoresAmbienBiolo(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoAmbienBiolo")){
                    anexo.setIdAdjuntoAmbienBiolo(a.getValue1());
                }else if(a.getDescripcion().equals("justiFactoresSocioEconoCultu")){
                    anexo.setJustiFactoresSocioEconoCultu(a.getValue1());
                }if(a.getDescripcion().equals("idAdjuntoSocioEconoCultu")){
                    anexo.setIdAdjuntoSocioEconoCultu(a.getValue1());
                }else if(a.getDescripcion().equals("justiManejoConcesion")){
                    anexo.setJustiManejoConcesion(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoManejoConcesion")){
                    anexo.setIdAdjuntoManejoConcesion(a.getValue1());
                }else if(a.getDescripcion().equals("sustePresupuesto")){
                    anexo.setSustePresupuesto(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoPresupuesto")){
                    anexo.setIdAdjuntoPresupuesto(a.getValue1());
                }else if(a.getDescripcion().equals("susteAnalisisFinanciero")){
                    anexo.setSusteAnalisisFinanciero(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoAnalisisFinanciero")){
                    anexo.setIdAdjuntoAnalisisFinanciero(a.getValue1());
                }else if(a.getDescripcion().equals("expeSolicitanteActv")){
                    anexo.setExpeSolicitanteActv(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoSolicitanteActv")){
                    anexo.setIdAdjuntoSolicitanteActv(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoAreaSolicitada")){
                    anexo.setIdAdjuntoAreaSolicitada(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoVertices")){
                    anexo.setIdAdjuntoVertices(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoMapaUbicacion")){
                    anexo.setIdAdjuntoMapaUbicacion(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoMapaBaseArea")){
                    anexo.setIdAdjuntoMapaBaseArea(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoDocLevantamiento")){
                    anexo.setIdAdjuntoDocLevantamiento(a.getValue1());
                }else if(a.getDescripcion().equals("idAdjuntoDocFuentesFinanciamiento")){
                    anexo.setIdAdjuntoDocFuentesFinanciamiento(a.getValue1());
                }else if(a.getDescripcion().equals("lugarFirma")){
                    anexo.setLugarFirma(a.getValue1());
                }else if(a.getDescripcion().equals("fechaFirma")){
                    anexo.setFechaFirma(a.getValue1());
                }
			});

            result.setData(anexo);
            result.setSuccess(true);
            result.setMessage("Se listaron los datos registrados del anexo.");
            return result;
        } catch (Exception e) {
            log.error("PostulacionPFDMERepositoryImpl - obtenerAnexo6PFDM", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
}
