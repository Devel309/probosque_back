package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.PlanManejoGeometriaRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionGeometriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository("repositorySolicitudConcesionGeometria")
public class SolicitudConcesionGeometriaRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionGeometriaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /**
     * @autor: Jordy Zamata [10-01-2022]
     * @modificado:
     * @descripción: {Registrar la geometria}
     * @param:SolicitudConcesionGeometriaEntity
     */
    @Override
    public ResultClassEntity registrarSolicitudConcesionGeometria (SolicitudConcesionGeometriaEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionGeometria_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("colorCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("geometriaWKT", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("srid", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionGeometria", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", item.getIdSolicitudConcesion());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());
            processStored.setParameter("nombreCapa", item.getNombreCapa());
            processStored.setParameter("colorCapa", item.getColorCapa());
            processStored.setParameter("geometriaWKT", item.getGeometry_wkt());
            processStored.setParameter("srid", item.getSrid());
            processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.execute();

            Integer idSolicitudConcesionGeometria = (Integer) processStored.getOutputParameterValue("idSolicitudConcesionGeometria");
            item.setIdSolicitudConcesionGeometria(idSolicitudConcesionGeometria);
            result.setCodigo(idSolicitudConcesionGeometria);
            result.setSuccess(true);
            result.setData(item);
            result.setMessage("Se registró el polígono/geometría de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el polígono/geometría de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }

    }

    /**
     * @autor: Abner Valdez [10-01-2022]
     * @modificado:
     * @descripción: {Obtener las geometrias por filtro}
     * @param:SolicitudConcesionGeometriaEntity
     */
    @Override
    public ResultClassEntity listarSolicitudConcesionGeometria(SolicitudConcesionGeometriaEntity item) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionGeometriaEntity> objList = new ArrayList<SolicitudConcesionGeometriaEntity>();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionGeometria_Listar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionGeometria", item.getIdSolicitudConcesionGeometria());
            processStored.setParameter("idSolicitudConcesion", item.getIdSolicitudConcesion());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    SolicitudConcesionGeometriaEntity item2 = new SolicitudConcesionGeometriaEntity();
                    item2.setIdSolicitudConcesionGeometria((Integer) row[0]);
                    item2.setIdSolicitudConcesion((Integer) row[1]);
                    item2.setIdArchivo((Integer) row[2]);
                    item2.setTipoGeometria((String) row[3]);
                    item2.setCodigoGeometria((String) row[4]);
                    item2.setCodigoSeccion((String) row[5]);
                    item2.setCodigoSubSeccion((String) row[6]);
                    item2.setNombreCapa((String) row[7]);
                    item2.setColorCapa((String) row[8]);
                    item2.setGeometry_wkt((String) row[9]);
                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de polígonos de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de polígonos de la solicitud de concesión.");
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionGeometria (SolicitudConcesionGeometriaEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Concesion.pa_SolicitudConcesionGeometria_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("colorCapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("geometriaWKT", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("srid", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionGeometria", item.getIdSolicitudConcesionGeometria());
            processStored.setParameter("idSolicitudConcesion", item.getIdSolicitudConcesion());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());
            processStored.setParameter("nombreCapa", item.getNombreCapa());
            processStored.setParameter("colorCapa", item.getColorCapa());
            processStored.setParameter("geometriaWKT", item.getGeometry_wkt());
            processStored.setParameter("srid", item.getSrid());
            processStored.setParameter("idUsuarioModificacion", item.getIdUsuarioModificacion());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el polígono/geometría de la solicitud de concesión correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el polígono/geometría de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return  result;
        }

    }


    /**
     * @autor: Jordy Zamata [10-01-2022]
     * @modificado:
     * @descripción: {Eliminar PlanManejoGeometria archivo y/o capa}
     * @param:SolicitudConcesionGeometriaEntity
     */
    @Override
    public ResultClassEntity eliminarSolicitudConcesionGeometriaArchivo(SolicitudConcesionGeometriaEntity item) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionGeometriaArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionGeometria", item.getIdSolicitudConcesionGeometria());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("idUsuario", item.getIdUsuarioElimina());

            processStored.execute();
            result.setData(item.getIdArchivo());
            result.setSuccess(true);
            result.setMessage("Se eliminó el polígono y/o archivo de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo eliminar el polígono y/o archivo de la solicitud de concesión.");
            return result;
        }
    }
}
