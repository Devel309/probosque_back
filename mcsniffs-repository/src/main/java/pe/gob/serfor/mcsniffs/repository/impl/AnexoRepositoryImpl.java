package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.print.DocFlavor.STRING;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.AdjuntoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo1Entity;
import pe.gob.serfor.mcsniffs.entity.Anexo2Entity;
import pe.gob.serfor.mcsniffs.entity.Anexo3Entity;
import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.EliminarDocAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.GeneralAnexoEntity;
import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaDto;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UsuarioSolicitanteEntity;
import pe.gob.serfor.mcsniffs.entity.ValidarDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.DepartamentoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.DistritoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.ProvinciaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ValidarAnexoDto;
import pe.gob.serfor.mcsniffs.repository.AnexoRepository;
import pe.gob.serfor.mcsniffs.repository.CoreCentralRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
//

@Repository
public class AnexoRepositoryImpl extends JdbcDaoSupport implements AnexoRepository {
    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los anexos}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AnexoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private FileServerConexion fileCn;

    @Autowired
    private CoreCentralRepository centralrepo;


    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {se obtiene la informacion del usuario postulante}
     * @param:Anexo1Entity
     */
    @Override
    public ResultClassEntity<UsuarioSolicitanteEntity> obtenerUsuarioSolicitante(AnexoRequestEntity filtro) {
        ResultClassEntity<UsuarioSolicitanteEntity> result = new ResultClassEntity<UsuarioSolicitanteEntity>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ObtenerUsuarioPostulacion");
            sp.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idUsuarioPostulacion", filtro.getIdUsuarioPostulacion());
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.execute();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            UsuarioSolicitanteEntity c = null;
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new UsuarioSolicitanteEntity();
                    c.setSolicitante((String) row[1]);
                    c.setIdUsuario((Integer) row[2]);
                    c.setIdDepartamento((Integer) row[3]);
                    c.setIdProvincia((Integer) row[4]);
                    c.setIdDistrito((Integer) row[5]);
                    c.setSuperficieSolicitada((String) row[6]);
                    c.setSectorAnexoCaserio((String) row[7]);
                    c.setNombre((String) row[8]);
                    c.setApellidoPaterno((String) row[9]);
                    c.setApellidoMaterno((String) row[10]);
                    c.setTipoDocumento((Integer) row[11]);
                    c.setNumeroDocumento((String) row[12]);
                    c.setCorreoElectronico((String) row[13]);
                }
            }
            result.setData(c);
            result.setSuccess(true);
            result.setMessage("Se obtuvo al postulante correctamente.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - AnexoRepositoryImpl", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity<PersonaDto> obtenerUsuario(PersonaDto obj) {
        ResultClassEntity<PersonaDto> result = new ResultClassEntity<PersonaDto>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Persona_Listar");
            sp.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idRepLegal", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idUsuario", obj.getIdUsuario());
            sp.setParameter("idRepLegal", obj.getIdRepLegal());
            sp.setParameter("idPersona", obj.getIdPersona());
            sp.execute();

            PersonaDto c = null;
            List<Object[]> spResult = sp.getResultList();
                for (Object[] row : spResult) {
                    c = new PersonaDto();
                    c.setIdPersona((Integer) row[0]);
                    c.setNombre((String) row[1]);
                    c.setApellidoMaterno((String) row[2]);
                    c.setApellidoPaterno((String) row[3]);
                    c.setTelefono((String) row[4]);
                    c.setCodTipoPersona((String) row[5]);
                    c.setCodTipoActor((String) row[6]);
                    c.setIdDistrito((String) row[7]);
                    c.setCodTipoCncc((String) row[8]);
                    c.setCodTipoDoc((String) row[9]);
                    c.setNumeroDocumento((String) row[10]);
                    c.setEmail((String) row[11]);
                    c.setEsRepLegal((char) row[12]);
                    c.setIdRepLegal((Integer) row[13]);
                    c.setRazonSocial((String) row[14]);
                    c.setIdSolicitudAcceso((Integer) row[15]);
                    c.setIdUsuario((Integer) row[16]);
                    c.setDireccion((String) row[17]);
                    c.setCodIrena((String) row[18]);
                    c.setIdTipoRegente((Integer) row[19]);
                    c.setCodEvalSolic((String) row[20]);
                    c.setCodEvalEmpresa((String) row[21]);
                }
            
            result.setData(c);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el postulante correctamente.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - AnexoRepositoryImpl", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 1 para que el usuario lo firme}
     * @param:Anexo1Entity
     */
    @Override
    public ResultArchivoEntity generarAnexo1(AnexoRequestEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        String buenas  ="no paso";
        try {
            Anexo1Entity anexo1 = new Anexo1Entity();
            anexo1.setSolicitante("");
            anexo1.setRepresentanteLegal("");
            anexo1.setSuperficieSolicitada("");
            anexo1.setPlazoConcesion(0);
            anexo1.setSectorAnexoCaserio("");
            DistritoEntity distrito = new DistritoEntity();
            distrito.setNombreDistrito("");
            ProvinciaEntity provincia = new ProvinciaEntity();
            provincia.setNombreProvincia("");
            DepartamentoEntity departamento = new DepartamentoEntity();
            departamento.setNombreDepartamento("");
            anexo1.setIdDistrito(distrito);
            anexo1.setIdProvincia(provincia);
            anexo1.setIdDepartamento(departamento);
            anexo1.setObjetivo("");
            ResultEntity<GeneralAnexoEntity> data = ObtenerAnexo(filtro);
            for (GeneralAnexoEntity obj:data.getData()) {
                if(obj.getDescripcion().equals("Solicitante")){
                    anexo1.setSolicitante(obj.getValue());
                }else if(obj.getDescripcion().equals("Representante Legal")){
                    anexo1.setRepresentanteLegal(obj.getValue());
                }else if(obj.getDescripcion().equals("Superficie Solicitada(ha)")){
                    anexo1.setSuperficieSolicitada(obj.getValue());
                }else if(obj.getDescripcion().equals("Plazo de Concesion(años)")){
                    anexo1.setPlazoConcesion(obj.getValue().equals(null) ||obj.getValue().equals("") ? 0 : Integer.parseInt(obj.getValue()));
                }else if(obj.getDescripcion().equals("Sector/Anexo/Caserio")){
                    anexo1.setSectorAnexoCaserio(obj.getValue());
                }else if(obj.getDescripcion().equals("IdDistrito")){
                    DistritoEntity od = new DistritoEntity();
                    if(obj.getValue()!=null){
                        od.setIdDistrito(Integer.parseInt(obj.getValue()));
                        ResultEntity<DistritoEntity> lstdistrito = centralrepo.listarPorFilroDistrito(od);
                        anexo1.setIdDistrito(lstdistrito.getData().get(0));
                    }
                }else if(obj.getDescripcion().equals("IdProvincia")){
                    ProvinciaEntity od = new ProvinciaEntity();
                    if(obj.getValue()!=null){
                        od.setIdProvincia(Integer.parseInt(obj.getValue()));
                        ResultEntity<ProvinciaEntity> lstprovincia = centralrepo.listarPorFilroProvincia(od);
                        anexo1.setIdProvincia(lstprovincia.getData().get(0));
                    }
                }else if(obj.getDescripcion().equals("IdDepartamento")){
                    DepartamentoEntity od = new DepartamentoEntity();
                    if(obj.getValue()!=null){
                        od.setIdDepartamento(Integer.parseInt(obj.getValue()));
                        List<DepartamentoEntity> lstdepartamento = centralrepo.ListarPorFiltroDepartamentos(od);
                        anexo1.setIdDepartamento(lstdepartamento.get(0));
                    }
                }else if(obj.getDescripcion().equals("Objetivo")){
                    anexo1.setObjetivo(obj.getValue());
                }else if(obj.getDescripcion().equals("Autoridad Forestal")){
                    anexo1.setAutoridadForestal(obj.getValue());
                }
            }

            AdjuntoRequestEntity filtro2 = new AdjuntoRequestEntity();
            filtro2.setIdProcesoPostulacion(filtro.getIdProcesoPostulacion());
            filtro2.setIdUsuarioAdjunta(filtro.getIdUsuarioPostulacion());
            filtro2.setIdTipoDocumento(19);
            filtro2.setCodigoAnexo(null);
            filtro2.setIdDocumentoAdjunto(null);
            ResultEntity<DocumentoAdjuntoEntity> archivo = ObtenerAdjuntos(filtro2);
            InputStream inputStream = AnexoRepositoryImpl.class.getResourceAsStream("/PlantillaAnexoI.docx");
            XWPFDocument doc = new XWPFDocument(inputStream);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null) {
                            if (text.contains("diamesaniox")) {
                                text = text.replace("diamesaniox", formatter.format(date));
                                r.setText(text, 0);
                            }else if (text.contains("autoridadforestalx")) {
                                text = text.replace("autoridadforestalx", anexo1.getAutoridadForestal());
                                r.setText(text, 0);
                            }else if(text.contains("imagenua")){
                                text = text.replace("imagenua", "");
                                r.setText(text,0);
                                if(archivo.getData().size()>0){
                                    DocumentoAdjuntoEntity imagen = archivo.getData().get(0);
                                    //String imgFile = imagen.getFile();
                                    //File initialFile = new File(imgFile);

                                    //FileUtils.writeByteArrayToFile(new File("pathname"), imagen.getFile());
                                    //InputStream is = new FileInputStream(imagen.getFile());
                                    //InputStream is = AnexoRepositoryImpl.class.getResourceAsStream("/Screenshot_1.png");
                                    if(imagen.getFile()!=null) {
                                        InputStream is = new ByteArrayInputStream(imagen.getFile());
                                        r.addBreak();
                                        r.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, "Mapa UA", Units.toEMU(300), Units.toEMU(300));
                                        is.close();
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }

            for (XWPFTable tbl : doc.getTables()) {
                for (XWPFTableRow row : tbl.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if(r != null){
                                    String text = r.getText(0);
                                    if(text !=null){
                                        if (text.contains("solicitantex")) {
                                            text = text.replace("solicitantex", anexo1.getSolicitante());
                                            r.setText(text,0);
                                        }else if(text.contains("representantelegalx")){
                                            text = text.replace("representantelegalx", anexo1.getRepresentanteLegal());
                                            r.setText(text,0);
                                        }else if(text.contains("superficiesolicitadax")){
                                            text = text.replace("superficiesolicitadax", anexo1.getSuperficieSolicitada());
                                            r.setText(text,0);
                                        }else if(text.contains("plazoconcesionx")){
                                            text = text.replace("plazoconcesionx", anexo1.getPlazoConcesion().toString() +" años");
                                            r.setText(text,0);
                                        }else if(text.contains("sectoranexocaseriox")){
                                            text = text.replace("sectoranexocaseriox", anexo1.getSectorAnexoCaserio());
                                            r.setText(text,0);
                                        }else if(text.contains("distritoprovinciadepartamentox")){
                                            String nombre = anexo1.getIdDistrito().getNombreDistrito()+" - "+anexo1.getIdProvincia().getNombreProvincia()+" - "+anexo1.getIdDepartamento().getNombreDepartamento();
                                            text = text.replace("distritoprovinciadepartamentox", nombre);
                                            r.setText(text,0);
                                        }else if(text.contains("objetivox")){
                                            text = text.replace("objetivox", anexo1.getObjetivo());
                                            r.setText(text,0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close(); 
            
            byte[] fileContent =b.toByteArray();
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo1.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 1.");
            //result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Throwable e) {
            log.error("AnexoRepository - generarAnexo1 "+e, e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." +buenas);
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: RegistrarImpugnacionDB [19-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 2 para que el usuario lo firme}
     * @param:Anexo1Entity
     */

    @Override
    public ResultArchivoEntity generarAnexo2(AnexoRequestEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            Anexo2Entity anexo2 = new Anexo2Entity();
            DistritoEntity distrito = new DistritoEntity();
            distrito.setNombreDistrito("");
            ProvinciaEntity provincia = new ProvinciaEntity();
            provincia.setNombreProvincia("");
            DepartamentoEntity departamento = new DepartamentoEntity();
            departamento.setNombreDepartamento("");
            anexo2.setNombreapellidos("");
            anexo2.setDni("");
            anexo2.setDireccion("");
            anexo2.setSector("");
            anexo2.setIdDistrito(distrito);
            anexo2.setIdProvincia(provincia);
            anexo2.setIdDepartamento(departamento);
            anexo2.setTelefono("");
            anexo2.setCelular("");
            anexo2.setCorreo("");
            anexo2.setRazonsocialsolicitante("");
            anexo2.setProfesion("");
            anexo2.setNroregistrocolegioprofesional("");
            anexo2.setNroregistroserfor("");
            anexo2.setEquipotecnico("");
            anexo2.setLugarfirma("");

            ResultEntity<GeneralAnexoEntity> data = ObtenerAnexo(filtro);
            for (GeneralAnexoEntity obj:data.getData()) {
                if(obj.getDescripcion().equals("Nombres y Apellidos del Regente")){anexo2.setNombreapellidos(obj.getValue());
                }else if(obj.getDescripcion().equals("Dni")){anexo2.setDni(obj.getValue());
                }else if(obj.getDescripcion().equals("Direccion")){ anexo2.setDireccion(obj.getValue());
                }else if(obj.getDescripcion().equals("Sector")){ anexo2.setSector(obj.getValue());
                }else if(obj.getDescripcion().equals("IdDistrito")){
                    DistritoEntity od = new DistritoEntity();
                    if(obj.getValue()!=null){
                        od.setIdDistrito(Integer.parseInt(obj.getValue()));
                        ResultEntity<DistritoEntity> lstdistrito = centralrepo.listarPorFilroDistrito(od);
                        anexo2.setIdDistrito(lstdistrito.getData().get(0));
                    }
                }else if(obj.getDescripcion().equals("IdProvincia")){
                    ProvinciaEntity od = new ProvinciaEntity();
                    if(obj.getValue()!=null){
                        od.setIdProvincia(Integer.parseInt(obj.getValue()));
                        ResultEntity<ProvinciaEntity> lstprovincia = centralrepo.listarPorFilroProvincia(od);
                        anexo2.setIdProvincia(lstprovincia.getData().get(0));
                    }
                }else if(obj.getDescripcion().equals("IdDepartamento")){
                    DepartamentoEntity od = new DepartamentoEntity();
                    if(obj.getValue()!=null){
                        od.setIdDepartamento(Integer.parseInt(obj.getValue()));
                        List<DepartamentoEntity> lstdepartamento = centralrepo.ListarPorFiltroDepartamentos(od);
                        anexo2.setIdDepartamento(lstdepartamento.get(0));
                    }
                }else if(obj.getDescripcion().equals("Telf. Fijo")){anexo2.setTelefono(obj.getValue());
                }else if(obj.getDescripcion().equals("Telf. Celular")){anexo2.setCelular(obj.getValue());
                }else if(obj.getDescripcion().equals("Correo Electronico")){anexo2.setCorreo(obj.getValue());
                }else if(obj.getDescripcion().equals("Razon Social del Solicitante de la Concesion")){ anexo2.setRazonsocialsolicitante(obj.getValue());
                }else if(obj.getDescripcion().equals("Profesion")){anexo2.setProfesion(obj.getValue());
                }else if(obj.getDescripcion().equals("N° de Registro en el Colegio Profesional")){anexo2.setNroregistrocolegioprofesional(obj.getValue());
                }else if(obj.getDescripcion().equals("N° de Registro de Regente en el SERFOR")){anexo2.setNroregistroserfor(obj.getValue());
                }else if(obj.getDescripcion().equals("Equipo Tecnico")){anexo2.setEquipotecnico(obj.getValue());
                }else if(obj.getDescripcion().equals("Lugar y Fecha")){anexo2.setLugarfirma(obj.getValue());
                }
            }
            int salida = -1;
            InputStream inputStream = AnexoRepositoryImpl.class.getResourceAsStream("/PlantillaAnexoll.docx");
            //BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            XWPFDocument doc = new XWPFDocument(inputStream);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.contains("lugarfechax")) {
                            text = text.replace("lugarfechax",anexo2.getLugarfirma()+" - " +formatter.format(date));
                            r.setText(text, 0);
                        }
                    }
                }
            }

            for (XWPFTable tbl : doc.getTables()) {
                for (XWPFTableRow row : tbl.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if(r != null){
                                    String text = r.getText(0);
                                    if(text !=null){
                                        if (text.contains("nomapelliregentex")) {
                                            text = text.replace("nomapelliregentex", anexo2.getNombreapellidos());
                                            r.setText(text,0);
                                        }else if(text.contains("dnix")){
                                            text = text.replace("dnix", anexo2.getDni());
                                            r.setText(text,0);
                                        }else if(text.contains("direccionx")){
                                            text = text.replace("direccionx", anexo2.getDireccion());
                                            r.setText(text,0);
                                        }else if(text.contains("sectorx")){
                                            text = text.replace("sectorx", anexo2.getSector());
                                            r.setText(text,0);
                                        }else if(text.contains("distritox")){
                                            text = text.replace("distritox", anexo2.getIdDistrito().getNombreDistrito());
                                            r.setText(text,0);
                                        }else if(text.contains("provinciax")){
                                            text = text.replace("provinciax", anexo2.getIdProvincia().getNombreProvincia());
                                            r.setText(text,0);
                                        }else if(text.contains("departamentox")){
                                            text = text.replace("departamentox", anexo2.getIdDepartamento().getNombreDepartamento());
                                            r.setText(text,0);
                                        }else if(text.contains("telefonox")){
                                            text = text.replace("telefonox", anexo2.getTelefono());
                                            r.setText(text,0);
                                        }else if(text.contains("celularx")){
                                            text = text.replace("celularx", anexo2.getCelular());
                                            r.setText(text,0);
                                        }else if(text.contains("correox")){
                                            text = text.replace("correox", anexo2.getCorreo());
                                            r.setText(text,0);
                                        }else if(text.contains("razonsocialsolicitantex")){
                                            text = text.replace("razonsocialsolicitantex", anexo2.getRazonsocialsolicitante());
                                            r.setText(text,0);
                                        }else if(text.contains("profesionx")){
                                            text = text.replace("profesionx", anexo2.getProfesion());
                                            r.setText(text,0);
                                        }else if(text.contains("nroregistrocoleprofex")){
                                            text = text.replace("nroregistrocoleprofex", anexo2.getNroregistrocolegioprofesional());
                                            r.setText(text,0);
                                        }else if(text.contains("nroregistroregenserforx")){
                                            text = text.replace("nroregistroregenserforx",anexo2.getNroregistroserfor());
                                            r.setText(text,0);
                                        }else if(text.contains("equipotecnicox")){
                                            text = text.replace("equipotecnicox", anexo2.getEquipotecnico() == null? " ":anexo2.getEquipotecnico());
                                            r.setText(text,0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            //File fileCopi = File.createTempFile("anexo2",".pdf");
            //OutputStream out = new FileOutputStream(fileCopi);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            /*InputStream docxInputStream = new ByteArrayInputStream(b.toByteArray());
            IConverter converter = LocalConverter.builder().build();
            converter.convert(docxInputStream).as(DocumentType.DOCX).to(out).as(DocumentType.PDF).execute();
            out.close();*/
            doc.close();

            byte[] fileContent = b.toByteArray();//Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo2.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 2.");
            //result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo2", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [19-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 3 para que el usuario lo firme}
     * @param:Anexo1Entity
     */

    @Override
    public ResultArchivoEntity generarAnexo3(AnexoRequestEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            Anexo3Entity anexo3 = new Anexo3Entity();
            anexo3.setNomapellfuncionrespon("");
            anexo3.setNomapellreprelegal("");
            anexo3.setDnireprelegal("");
            anexo3.setDenomipersojuridica("");
            anexo3.setRucpersojuridica("");
            anexo3.setDireccpersojuridica("");
            anexo3.setDescdocumentos("");
            anexo3.setMonto1("");
            anexo3.setMonto2("");
            anexo3.setMonto3("");
            anexo3.setLugarfirma("");
            anexo3.setFechafirma(null);
            anexo3.setCargoFuncioArffssRespo("");

            ResultEntity<GeneralAnexoEntity> data = ObtenerAnexo(filtro);
            for (GeneralAnexoEntity obj:data.getData()) {
                if(obj.getDescripcion().equals("Nombres y Apellidos del Funcionario Responsable de la Concesion")){anexo3.setNomapellfuncionrespon(obj.getValue());
                }else if(obj.getDescripcion().equals("Cargo del funcionario de la ARFFS respinsable de la concesion")){anexo3.setCargoFuncioArffssRespo(obj.getValue());
                }else if(obj.getDescripcion().equals("Nombres y Apellidos del Representante Legal")){anexo3.setNomapellreprelegal(obj.getValue());
                }else if(obj.getDescripcion().equals("Dni del Representante Legal")){ anexo3.setDnireprelegal(obj.getValue());
                }else if(obj.getDescripcion().equals("Denominacion de la Persona Juridica")){ anexo3.setDenomipersojuridica(obj.getValue());
                }else if(obj.getDescripcion().equals("Ruc de la Persona Juridica")){ anexo3.setRucpersojuridica(obj.getValue());
                }else if(obj.getDescripcion().equals("Domicilio de la Persona Juridica")){ anexo3.setDireccpersojuridica(obj.getValue());
                }else if(obj.getDescripcion().equals("Descripcion de Documentos")){anexo3.setDescdocumentos(obj.getValue());
                }else if(obj.getDescripcion().equals("Monto Estados Financieros")){ anexo3.setMonto1(obj.getValue());
                }else if(obj.getDescripcion().equals("Monto Reporte Situciacion Crediticia")){ anexo3.setMonto2(obj.getValue());
                }else if(obj.getDescripcion().equals("Monto Documentos de Bienes")){ anexo3.setMonto3(obj.getValue());
                }else if(obj.getDescripcion().equals("Lugar de Firma")){ anexo3.setLugarfirma(obj.getValue());
                }else if(obj.getDescripcion().equals("Fecha de Firma")){anexo3.setFechafirma(null);
                }
            }
            int salida = -1;
            InputStream inputStream = AnexoRepositoryImpl.class.getResourceAsStream("/PlantillaAnexolll.docx");

            XWPFDocument doc = new XWPFDocument(inputStream);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();

            for (XWPFTable tbl : doc.getTables()) {
                for (XWPFTableRow row : tbl.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if(r != null){
                                    String text = r.getText(0);
                                    if(text !=null){
                                        if (text.contains("nomaapellidofunrespo")) {
                                            text = text.replace("nomaapellidofunrespo",anexo3.getNomapellfuncionrespon());
                                            r.setText(text, 0);
                                        }else if (text.contains("cargofuncioarffsrespon")) {
                                            text = text.replace("cargofuncioarffsrespon",anexo3.getCargoFuncioArffssRespo());
                                            r.setText(text, 0);
                                        }else if (text.contains("nomapellreprelegal")) {
                                            text = text.replace("nomapellreprelegal",anexo3.getNomapellreprelegal());
                                            r.setText(text, 0);
                                        }else if (text.contains("dnireprelegal")) {
                                            text = text.replace("dnireprelegal",anexo3.getDnireprelegal());
                                            r.setText(text, 0);
                                        }else if (text.contains("denomipersojuridica")) {
                                            text = text.replace("denomipersojuridica",anexo3.getDenomipersojuridica());
                                            r.setText(text, 0);
                                        }else if (text.contains("rucpersojuridica")) {
                                            text = text.replace("rucpersojuridica",anexo3.getRucpersojuridica());
                                            r.setText(text, 0);
                                        }else if (text.contains("direccpersojuridica")) {
                                            text = text.replace("direccpersojuridica",anexo3.getDireccpersojuridica());
                                            r.setText(text, 0);
                                        }else if (text.contains("lugarx")) {
                                            text = text.replace("lugarx",anexo3.getLugarfirma());
                                            r.setText(text, 0);
                                        }else if (text.contains("fechax")) {
                                            text = text.replace("fechax",formatter.format(date));
                                            r.setText(text, 0);
                                        }else if (text.contains("descdocumentos")) {
                                            text = text.replace("descdocumentos", anexo3.getDescdocumentos());
                                            r.setText(text,0);
                                        }else if(text.contains("Monto1")){
                                            text = text.replace("Monto1", anexo3.getMonto1());
                                            r.setText(text,0);
                                        }else if(text.contains("Monto2")){
                                            text = text.replace("Monto2", anexo3.getMonto2());
                                            r.setText(text,0);
                                        }else if(text.contains("Monto3")){
                                            text = text.replace("Monto3", anexo3.getMonto3());
                                            r.setText(text,0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            byte[] fileContent = b.toByteArray();//Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo3.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 3.");
            //result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo3", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Descarga el anexo 4}
     * @param:Anexo1Entity
     */
    @Override
    public ResultArchivoEntity descargarAnexo4() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/Anexo4.pdf");
            File fileCopi = File.createTempFile("anexo4",".pdf");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo4.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 4.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo4", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Obtiene parametros del anexo}
     * @param:AnexoRequestEntity
     */
    @Override
    public ResultEntity<GeneralAnexoEntity> ObtenerAnexo(AnexoRequestEntity filtro) {
        ResultEntity<GeneralAnexoEntity> result = new ResultEntity<GeneralAnexoEntity>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_GeneralAnexos_Obtener");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoAnexo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioPostulacion", filtro.getIdUsuarioPostulacion());
            sp.setParameter("codigoAnexo", filtro.getCodigoAnexo());
            GeneralAnexoEntity c = null;
            List<GeneralAnexoEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new GeneralAnexoEntity();
                    c.setIdGeneralAnexos((Integer) row[0]);
                    c.setCodigoAnexo((String) row[1]);
                    c.setDescripcion((String) row[2]);
                    c.setValue((String) row[3]);
                    c.setIdProcesoPostulacion((Integer) row[4]);
                    c.setIdUsuarioPostulacion((Integer) row[5]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los parámetros del anexo solicitado.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Cuando la un anexo esta incorrecto}
     * @param:AnexoRequestEntity
     */
    @Override
    public ResultClassEntity ValidarAnexo(ValidarDocumentoEntity anexo) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ValidarAnexo_insertar");
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("devolucion", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("titulo", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idValidarDocumento", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("descripcion", anexo.getDescripcion());
            sp.setParameter("idDocumentoAdjunto", anexo.getIdDocumentoAdjunto());
            sp.setParameter("devolucion", anexo.getDevolucion());
            sp.setParameter("titulo", anexo.getTitulo());
            sp.setParameter("idUsuarioRegistro", anexo.getIdUsuarioRegistro());
            sp.setParameter("fechaRegistro", new java.sql.Timestamp(date.getTime()));
            sp.execute();
            Integer idValidarDocumento = (Integer) sp.getOutputParameterValue("idValidarDocumento");
            Integer salida = idValidarDocumento;
            if(anexo.getDevolucion()){
                //Se debe se insertar las observaciones
                StoredProcedureQuery sp1 = em.createStoredProcedureQuery("dbo.pa_ValidarDocDetalle_insertar");
                sp1.registerStoredProcedureParameter("idValidarAnexo", Integer.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp1.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp1);
                sp1.setParameter("idValidarAnexo", salida);
                sp1.setParameter("observacion", anexo.getObservacion());
                sp1.setParameter("idUsuarioRegistro", anexo.getIdUsuarioRegistro());
                sp1.setParameter("fechaRegistro", new java.sql.Timestamp(date.getTime()));
                sp1.execute();
                //Se debe insertar en la tabla que valida los sla
                StoredProcedureQuery sp2 = em.createStoredProcedureQuery("dbo.pa_ValidarSla_insertar");
                sp2.registerStoredProcedureParameter("idValidarAnexo", Integer.class, ParameterMode.IN);
                sp2.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
                sp2.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
                sp2.registerStoredProcedureParameter("sla", Integer.class, ParameterMode.IN);
                sp2.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp2.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp2.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp2);
                sp2.setParameter("idValidarAnexo", salida);
                sp2.setParameter("fechaInicio", new java.sql.Timestamp(date.getTime()));
                sp2.setParameter("fechaFin", anexo.getFechaFinPlazo());
                sp2.setParameter("sla", anexo.getSla());
                sp2.setParameter("descripcion", anexo.getDescripcion());
                sp2.setParameter("idUsuarioRegistro", anexo.getIdUsuarioRegistro());
                sp2.setParameter("fechaRegistro", new java.sql.Timestamp(date.getTime()));
                sp2.execute();
            }
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setInformacion("Observación insertada. "+ " validación sla insertada");
            result.setMessage("Se envio la observación.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ValidarAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }


    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Se adjunta el anexo firmado}
     * @param:AnexoRequestEntity
     */
    @Override
    public ResultClassEntity AdjuntarAnexo(MultipartFile file, Integer idProcesoPostulacion, Integer idUsuarioAdjunta, String codigoAnexo,String nombreArchivo,Integer idTipoDocumento,Integer idPostulacionPFDM, Integer idDocumentoAdjunto) {
        ResultClassEntity result = new ResultClassEntity();

        try {

            DocumentoAdjuntoEntity doc = new DocumentoAdjuntoEntity();
            doc.setIdProcesoPostulacion(idProcesoPostulacion);
            doc.setIdUsuarioAdjunta(idUsuarioAdjunta);
            doc.setDescripcion(codigoAnexo);
            doc.setIdTipoDocumento(idTipoDocumento);
            String nombreGenerado = fileCn.uploadFile(file);

            nombreGenerado=((!nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()));
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_Registrar");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioAdjunta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPostulacionPFDM", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjuntoOut", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", doc.getIdProcesoPostulacion());
            sp.setParameter("idTipoDocumento", doc.getIdTipoDocumento());
            sp.setParameter("idUsuarioAdjunta", doc.getIdUsuarioAdjunta());
            sp.setParameter("descripcion", doc.getDescripcion());
            sp.setParameter("nombre", nombreArchivo);
            sp.setParameter("idPostulacionPFDM", idPostulacionPFDM);
            sp.setParameter("idUsuarioRegistro", doc.getIdUsuarioAdjunta());
            sp.setParameter("idDocumentoAdjunto", idDocumentoAdjunto);
            sp.setParameter("nombreGenerado", nombreGenerado);
            sp.execute();
            Integer salida = (Integer) sp.getOutputParameterValue("idDocumentoAdjuntoOut");
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se adjunto el anexo.");
            result.setInformacion(doc.getRuta());
            return result;

        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - AdjuntarAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;

        }
    }
    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Obtiene los documentos adjuntos por el usuario}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<DocumentoAdjuntoEntity> ObtenerAdjuntos(AdjuntoRequestEntity filtro) {
        ResultEntity<DocumentoAdjuntoEntity> result = new ResultEntity<DocumentoAdjuntoEntity>();

        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_obtener");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioAdjunta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPostulacionPFDM", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioAdjunta", filtro.getIdUsuarioAdjunta());
            sp.setParameter("descripcion", filtro.getCodigoAnexo());
            sp.setParameter("idDocumentoAdjunto", filtro.getIdDocumentoAdjunto());
            sp.setParameter("idTipoDocumento", filtro.getIdTipoDocumento());
            sp.setParameter("idPostulacionPFDM", filtro.getIdPostulacionPFDM());
            sp.execute();
            DocumentoAdjuntoEntity c = null;
            List<DocumentoAdjuntoEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new DocumentoAdjuntoEntity();
                    c.setIdDocumentoAdjunto((Integer) row[0]);
                    c.setIdProcesoPostulacion((Integer) row[1]);
                    c.setIdPostulacionPFDM((Integer) row[2]);
                    c.setIdTipoDocumento((Integer) row[3]);
                    c.setTipoDocumento((String) row[4]);
                    c.setIdUsuarioAdjunta((Integer) row[5]);
                    c.setNombreGenerado((String) row[6]);
                    c.setDescripcion((String) row[7]);
                    c.setNombreDocumento((String) row[8]);
                    if(c.getNombreGenerado() != null){
                        byte[] byteFile = fileCn.loadFileAsResource(c.getNombreGenerado());

                        c.setFile(byteFile);
                    }
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los documentos adjuntos.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAdjuntos", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [19-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Inactivar documentos adjuntos por el usuario}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultClassEntity eliminarDocumentoAdjunto(EliminarDocAdjuntoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_eliminar");
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idDocumentoAdjunto", obj.getIdDocumentoAdjunto());
            sp.setParameter("idUsuarioModificacion", obj.getIdUsuarioModficacion());
            sp.setParameter("fechaModificacion", new java.sql.Timestamp(date.getTime()));
            sp.execute();
            result.setCodigo(obj.getIdDocumentoAdjunto());
            result.setSuccess(true);
            result.setMessage("Se eliminó el documento correctamente.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - eliminarDocumentoAdjunto", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Se actualiza el estatus de anexo}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultClassEntity actualizarEstatusAnexo(AnexoRequestEntity filtro) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_AnexoDetalle_updateanexo");
            sp.registerStoredProcedureParameter("idAnexoDetalle", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstatusAnexo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idAnexoDetalle", filtro.getIdAnexoDetalle());
            sp.setParameter("idEstatusAnexo", filtro.getIdStatusAnexo());
            sp.setParameter("idDocumentoAdjunto", filtro.getIdDocumentoAdjunto());
            sp.setParameter("idUsuarioModificacion", filtro.getIdUsuarioModificacion());
            sp.setParameter("fechaModificacion", new java.sql.Timestamp(date.getTime()));
            sp.execute();
            Integer salida = filtro.getIdAnexoDetalle();
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se actualizó el estado del anexo.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - actualizarEstatusAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Se obtiene el estatus y obs del anexo}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<ValidarDocumentoEntity> obtenerEstatusAnexo(AnexoRequestEntity filtro) {
        ResultEntity<ValidarDocumentoEntity> result = new ResultEntity<ValidarDocumentoEntity>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ValidarDocumento_obtenerAnexos");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoAnexo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("postulacionPFDM", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioPostulacion", filtro.getIdUsuarioPostulacion());
            sp.setParameter("codigoAnexo", filtro.getCodigoAnexo());
            sp.setParameter("postulacionPFDM", filtro.getIdPostulacionPFDM());
            sp.execute();
            ValidarDocumentoEntity c = null;
            List<ValidarDocumentoEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ValidarDocumentoEntity();
                    c.setIdAnexoDetalleStatus((Integer) row[0]);
                    c.setIdStatusAnexo((Integer) row[1]);
                    c.setDescripcionStatus((String) row[2]);
                    c.setIdProcesoPostulacion((Integer) row[3]);
                    c.setIdUsuarioPostulacion((Integer) row[4]);
                    c.setCodigoAnexo((String) row[5]);
                    c.setIdPostulacionPFDM((Integer) row[6]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los documentos adjuntos.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - obtenerEstatusAnexo", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene registra el estatus de los anexos}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultClassEntity insertarEstatusAnexo(AnexoRequestEntity filtro) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            Date date = new Date();
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_AnexoDetalle_insertar");
            processStored.registerStoredProcedureParameter("codigoAnexo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEstatusAnexo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPostulacionPFDM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioCreacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaCreacion", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoAnexo", filtro.getCodigoAnexo());
            processStored.setParameter("idEstatusAnexo", filtro.getIdStatusAnexo());
            processStored.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            processStored.setParameter("idUsuarioPostulacion", filtro.getIdUsuarioPostulacion());
            processStored.setParameter("idPostulacionPFDM", filtro.getIdPostulacionPFDM());
            processStored.setParameter("idUsuarioCreacion", filtro.getIdUsuarioCreacion());
            processStored.setParameter("fechaCreacion", new java.sql.Timestamp(date.getTime()));
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró el estado del anexo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Se obtiene el detalle de las observaciones}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<ValidarDocumentoEntity> obtenerDetalleObservacion(AnexoRequestEntity filtro) {
         ResultEntity<ValidarDocumentoEntity> result = new ResultEntity<ValidarDocumentoEntity>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ValidarDocumento_obtener");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioAdjunta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoAnexo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("postulacionPFDM", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioAdjunta", filtro.getIdUsuarioPostulacion());
            sp.setParameter("idTipoDocumento", filtro.getIdTipoDocumento());
            sp.setParameter("codigoAnexo", filtro.getCodigoAnexo());
            sp.setParameter("postulacionPFDM", filtro.getIdPostulacionPFDM());
            sp.execute();
            ValidarDocumentoEntity c = null;
            List<ValidarDocumentoEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ValidarDocumentoEntity();
                    c.setIdValidadAnexo((Integer) row[0]);
                    c.setDescripcion((String) row[1]);
                    c.setIdDocumentoAdjunto((Integer) row[2]);
                    c.setDevolucion((Boolean) row[3]);
                    c.setIdValidarDetalle((Integer) row[4]);
                    c.setObservacion((String) row[5]);
                    c.setFechaInicioPlazo((Date) row[6]);
                    c.setFechaFinPlazo((Date) row[7]);
                    c.setSla((Integer) row[8]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron las validaciones.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - obtenerDetalleObservacion", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity listarDocumentos(AdjuntoRequestEntity filtro) {
        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_Listar");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idTipoDocumento", filtro.getIdTipoDocumento());

            sp.execute();

            DocumentoAdjuntoEntity c = null;
            List<DocumentoAdjuntoEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new DocumentoAdjuntoEntity();
                    c.setIdDocumentoAdjunto((Integer) row[0]);
                    c.setIdProcesoPostulacion((Integer) row[1]);
                    c.setIdTipoDocumento((Integer) row[2]);
                    c.setIdUsuarioAdjunta((Integer) row[3]);
                    c.setDescripcion((String) row[4]);
                    c.setNombreDocumento((String) row[5]);
                    c.setNombreGenerado((String) row[6]);
                    c.setIdPostulacionPFDM((Integer) row[7]);
                    if(row[8]!=null){
                        c.setConforme((Boolean) row[8]);
                    }

                    c.setObservacion((String) row[9]);
                    c.setTipoDocumento((String) row[10]);
                    c.setCodigoTipoDocumento((String)row[11]);
                    c.setAsunto((String)row[12]);
                    c.setDescripcionCodigoTipoDocumento((String)row[13]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setSuccess(true);
            result.setMessage("Se listaron los documentos.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - listarDocumentos", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity guardarObservacion(List<DocumentoAdjuntoEntity> params) {
        ResultClassEntity result = new ResultClassEntity();

        try {

            if (params!= null && !params.isEmpty()) {
                for (DocumentoAdjuntoEntity reg : params) {


                    StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_RegistrarObservacion");
                    sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                    //sp.registerStoredProcedureParameter("idDocumentoAdjuntoOut", Integer.class, ParameterMode.OUT);
                    SpUtil.enableNullParams(sp);
                    sp.setParameter("idDocumentoAdjunto", reg.getIdDocumentoAdjunto());
                    sp.setParameter("observacion", reg.getObservacion());
                    sp.setParameter("conforme", reg.getConforme());
                    sp.setParameter("idUsuarioModificacion", reg.getIdUsuarioModificacion());
                    sp.execute();
                    //Integer salida = (Integer) sp.getOutputParameterValue("idDocumentoAdjuntoOut");

                }
            }

            result.setSuccess(true);
            result.setMessage("Se guardó correctamente.");

            return result;

        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - guardarObservacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;

        }
    }

    @Override
    public ResultClassEntity registrarDocumentoAdjunto(DocumentoAdjuntoDto dto) {
        ResultClassEntity result = new ResultClassEntity();

        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_Registrar");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioAdjunta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPostulacionPFDM", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idDocumentoAdjuntoOut", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", dto.getIdProcesoPostulacion());
            sp.setParameter("idTipoDocumento", dto.getIdTipoDocumento());
            sp.setParameter("idUsuarioAdjunta", dto.getIdUsuarioAdjunta());
            sp.setParameter("descripcion", dto.getDescripcion());
            sp.setParameter("nombre", dto.getNombreDocumento());
            sp.setParameter("idPostulacionPFDM", dto.getIdPostulacionPFDM());
            sp.setParameter("codigoTipoDocumento", dto.getCodigoTipoDocumento());
            sp.setParameter("asunto", dto.getAsunto());
            sp.setParameter("idUsuarioRegistro", dto.getIdUsuarioAdjunta());
            sp.setParameter("idDocumentoAdjunto", dto.getIdDocumentoAdjunto());
            sp.setParameter("nombreGenerado", dto.getNombreGenerado());
            sp.execute();
            Integer salida = (Integer) sp.getOutputParameterValue("idDocumentoAdjuntoOut");
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se adjunto el anexo.");
            result.setInformacion(dto.getRuta());
            return result;

        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - AdjuntarAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;

        }
    }

    @Override
    public List<ValidarAnexoDto> listarValidarAnexos(ValidarAnexoDto filtro) throws Exception {
    
        List<ValidarAnexoDto> lstresult = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_DocumentoAdjunto_ListarValidarAnexos");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("descripcion", filtro.getDescripcion());

            sp.execute();

            ValidarAnexoDto c = null;
            
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ValidarAnexoDto();
                    c.setDevolucion(row[0]==null?null:(Boolean)row[0]);
                    c.setObservacion(row[1]==null?null:(String)row[1]);
 
                    lstresult.add(c);
                }
            }

            return lstresult;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - listarValidarAnexos", e.getMessage());
            return lstresult;
        }
    }

     /**
     * @autor: Abner Valdez [17-01-2022]
     * @modificado: 
     * @descripción: {Obtiene el archivo de archivo para anexo mapa}
     * @param:AdjuntoRequestEntity
     */
    @Override
    public ResultEntity<PGMFArchivoEntity> ObtenerArchivoAnexoMapa(AdjuntoRequestEntity filtro) {
        ResultEntity<PGMFArchivoEntity> result = new ResultEntity<PGMFArchivoEntity>();

        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Archivo_ObtenerAnexoMapa");
            sp.registerStoredProcedureParameter("IdPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("CodigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("IdPlanManejo", filtro.getIdProcesoPostulacion());
            sp.setParameter("CodigoProceso", filtro.getCodigoAnexo());
            sp.execute();
            PGMFArchivoEntity c = null;
            List<PGMFArchivoEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new PGMFArchivoEntity();
                    c.setIdPGMFArchivo((Integer) row[0]);
                    c.setIdPlanManejo((Integer) row[1]);
                    c.setIdArchivo((Integer) row[2]);
                    c.setCodigoTipoPGMF((String) row[3]);
                    c.setCodigoSubTipoPGMF((String) row[4]);
                    c.setNombre((String) row[5]);
                    c.setNombreArchivo((String) row[6]);
                    c.setExtensionArchivo((String) row[7]);
                    c.setTipoDocumento((String) row[8]);
                    if(c.getNombreArchivo() != null){
                        byte[] byteFile = fileCn.loadFileAsResource(c.getNombreArchivo());

                        c.setDocumento(byteFile);
                    }
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los documentos adjuntos.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerArchivoAnexoMapa", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
}
