package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Oposicion.OposicionArchivoDTO;

public interface OposicionRepository {
    ResultClassEntity registrarOposicion(OposicionEntity obj) throws Exception;
    ResultClassEntity registrarOposicionArchivo(OposicionArchivoDTO item) throws Exception;
    ResultEntity<OposicionEntity> listarOposicion(OposicionRequestEntity filtro);
    ResultClassEntity registrarRespuestaOposicion(OposicionEntity obj);
    List<OposicionEntity> obtenerOposicion(Integer idOposicion) throws Exception;
    ResultClassEntity<OposicionAdjuntosEntity> obtenerOposicionRespuesta(Integer IdOposicion);
    ResultArchivoEntity descargarFormatoResulucion();
    ResultClassEntity adjuntarFormatoResolucion(OposicionEntity obj);
    ResultClassEntity actualizarOposicion(OposicionEntity obj);
    ResultClassEntity regitrarOpositor(PersonaRequestEntity obj);
    ResultClassEntity<PersonaRequestEntity> obtenerOpositor(OpositorRequestEntity obj);
    ResultClassEntity eliminarOposicionArchivo(Integer idArchivo, Integer idUsuario);
    List<OposicionArchivoDTO> listarOposicionArchivo(OposicionArchivoDTO dto) throws Exception;
}
