package pe.gob.serfor.mcsniffs.repository.impl;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoForestalContratoDto;
import pe.gob.serfor.mcsniffs.entity.PlanGeneralManejoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.UbigeoArffsEntity;
import pe.gob.serfor.mcsniffs.repository.PlanGeneralManejoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class PlanGeneralManejoForestalRepositoryImpl implements PlanGeneralManejoForestalRepository{
    
    /**
     * @autor: JaquelineDB [27-08-2021]
     * @modificado: 
     * @descripción: {repositorio del plan de manejo forestal}
     */
    @PersistenceContext
    private EntityManager em;
    
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PlanGeneralManejoForestalRepositoryImpl.class);
     /**
     * @autor: JaquelineDB [27-08-2021]
     * @modificado: Harry Coa [01-09-2021]
     * @descripción: { registrar el plan de manejo forestal }
     * @param: List<PlanGeneralManejoForestalEntity>
     */
    @Override
    public ResultClassEntity registrarPlanManejoForestal(List<PlanGeneralManejoForestalEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PlanGeneralManejoForestalEntity> list_ = new ArrayList<>();
        try {
            for (PlanGeneralManejoForestalEntity obj :list){
                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoForestal_registrar");
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoEscala", Short.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("nroPersonasEmpadronadas", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejoPadre", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("descripcion", obj.getDescripcion());
                processStored.setParameter("idSolicitud", obj.getIdSolicitud());
                processStored.setParameter("idTipoProceso", obj.getIdTipoProceso());
                processStored.setParameter("idTipoPlan", obj.getIdTipoPlan());
                processStored.setParameter("tipoEscala", obj.getTipoEscala());
                processStored.setParameter("aspectoComplementario", obj.getAspectoComplementario());
                processStored.setParameter("idPlanManejoPadre", obj.getIdPlanManejoPadre());
                processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
                processStored.execute();

                List<Object[]> spResult = processStored.getResultList();

                if( !spResult.isEmpty()){
                    for (Object[] row : spResult){
                        PlanGeneralManejoForestalEntity temp = new PlanGeneralManejoForestalEntity();
                        temp.setIdPlanManejo((Integer) row[0]);
                        temp.setDescripcion((String) row[1]);
                        temp.setIdSolicitud((Integer) row[2]);
                        temp.setIdTipoProceso((Integer) row[3]);
                        temp.setIdTipoPlan((Integer) row[4]);
                        temp.setTipoEscala((Short) row[5]);
                        temp.setAspectoComplementario((String) row[6]);
                        list_.add(temp);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el plan general de manejo forestal.");
            return  result;
        } catch (Exception e) {
            log.error("PlanGeneralManejoForestalRepositoryImpl - registrarPlanManejoForestal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
    /**
     * @autor: Harry Coa [31-08-2021]
     * @modificado:
     * @descripción: { ontiene el contrato seleccionado para PGMF}
     * @param: idContrato
     */
    @Override
    public ResultClassEntity obtenerContrato(ContratoEntity obj) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_Contrato_ListarFiltro");
            sp.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioTitularTH", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoContrato", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoEstadoContrato", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);

            sp.setParameter("idContrato", obj.getIdContrato());
            sp.setParameter("idUsuarioTitularTH", obj.getIdUsuarioTitularTH());
            sp.setParameter("codigoTipoContrato", obj.getCodigoTipoContrato());
            sp.setParameter("codigoEstadoContrato", obj.getCodigoEstadoContrato());

            sp.execute();

            List<Object[]> spResult = sp.getResultList();

            List<PlanManejoForestalContratoDto> lista = new ArrayList<PlanManejoForestalContratoDto>();

            if (spResult != null) {

                if (spResult.size() >= 1) {
                    lista = new ArrayList<PlanManejoForestalContratoDto>();

                    for (Object[] row : spResult) {

                        PlanManejoForestalContratoDto item = new PlanManejoForestalContratoDto();
                        item.setIdContrato((Integer) row[0]);
                        item.setCodigoTipoPersonaTitular((String) row[11]);
                        item.setDescripcionTipoPersonaTitular((String) row[12]);
                        item.setCodigoTipoDocumentoTitular((String) row[13]);
                        item.setDescripcionTipoDocIdentidadTitular((String) row[14]);
                        item.setNumeroDocumentoTitular((String) row[15]);
                        item.setNombreTitular((String) row[16]);
                        item.setApePaternoTitular((String) row[17]);
                        item.setApeMaternoTitular((String) row[18]);
                        item.setRazonSocialTitular((String) row[19]);
                        item.setCorreoTitular((String) row[20]);
                        item.setDomicilioLegalTitular((String) row[22]);
                        item.setCodigoTipoDocIdentidadRepLegal((String) row[25]);
                        item.setDescripcionTipoDocIdentidadRepLegal((String) row[26]);
                        item.setNumeroDocumentoRepLegal((String) row[27]);
                        item.setNombreRepresentanteLegal((String) row[28]);
                        item.setApePaternoRepLegal((String) row[29]);
                        item.setApeMaternoRepLegal((String) row[30]);
                        item.setCorreoRepLegal((String) row[31]);
                        lista.add(item);

                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de contratos correctamente.");

        }catch (Exception e) {
            result.setSuccess(false);
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error. Error: " + e.getMessage());
        }

        return result;
    }
    /**
    @Override
    public ResultClassEntity<PlanManejoForestalContratoDto> obtenerContrato(Integer idContrato) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_Contrato_ListarFiltro");
        sp.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
        sp.setParameter("idContrato", idContrato);
        sp.execute();
        PlanManejoForestalContratoDto data = setResultDataObtener(sp.getResultList());
        ResultClassEntity<PlanManejoForestalContratoDto> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

     */
    private PlanManejoForestalContratoDto setResultDataObtener(List<Object[]> dataDb) throws Exception{
        PlanManejoForestalContratoDto data = new PlanManejoForestalContratoDto();
        if (!dataDb.isEmpty()) {
            Object[] item = dataDb.get(0);
            data.setCodigoTituloH((String) item[0]);
            data.setDescripcion((String) item[1]);
            data.setNumeroDocumentoRepLegal((String) item[2]);
            data.setNombreRepresentanteLegal((String) item[3]);
            data.setCodigoPartidaReg((String) item[4]);
            data.setNumeroContratoConcesion((String) item[5]);
            data.setFirmado((Boolean) item[6]);
            data.setDatosValidados((Boolean) item[7]);
            data.setEstadoProceso((Integer) item[8]);
            data.setFechaIniContrato((Date) item[9]);
            data.setFechaFinContrato((Date) item[10]);
        }
        return data;
    }

    /**
     * @autor: Harry Coa [31-08-2021]
     * @modificado:
     * @descripción: { lista el filtro de contratos para PGMF}
     * @param: idContrato = null o 0
     */
    @Override
    public ResultClassEntity<List<PlanManejoForestalContratoDto>> listarFiltroContrato() throws Exception {
        ArrayList<PlanManejoForestalContratoDto> result = new ArrayList<>();
        ResultClassEntity<List<PlanManejoForestalContratoDto>> response = new ResultClassEntity<>();
        response.setData(result);
        return response;
    }

    /**
     * @autor: JaquelineDB [31-08-2021]
     * @modificado: 
     * @descripción: {lista los contratos vigentes sin plan de manejo}
     * @param:idContrato
     */
    @Override
    public ResultEntity<PlanManejoContratoEntity> listarContratosVigentes(Integer idContrato) {
        ResultEntity<PlanManejoContratoEntity> result = new ResultEntity<PlanManejoContratoEntity>();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejo_listarContrato");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContrato", idContrato);
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            List<PlanManejoContratoEntity> lstcontratos = new ArrayList<>();
            if ( !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    PlanManejoContratoEntity obj = new PlanManejoContratoEntity();
                    obj.setIdUsuarioTitular((Integer) row[0]);
                    obj.setNombreTitular((String) row[1]);
                    obj.setDniTitular((String) row[2]);
                    obj.setRucTitular((String) row[3]);
                    obj.setDistritoTitular(row[4]!=null? Integer.parseInt((String)row[4]) : null);
                    obj.setProvinciaTitular((Integer) row[5]);
                    obj.setDepartamentoTitular((Integer)row[6]);
                    obj.setDomicilioLegalTitular((String) row[7]);
                    obj.setDniRepresentanteLegal((String)row[8]);
                    obj.setNombreRepresentanteLegal((String)row[9]);
                    obj.setTituloHabilitante((String) row[10]);
                    obj.setSuperficieUA((String)row[11]);
                    obj.setIdUA((String) row[12]);
                    obj.setIdContrato((Integer) row[13]);
                    lstcontratos.add(obj);
                }
            }

            result.setData(lstcontratos);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron contratos viigentes.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

     /**
     * @autor: JaquelineDB [31-08-2021]
     * @modificado: 
     * @descripción: {actualizar el plan de manejo forestal}
     * @param:PlanGeneralManejoForestalEntity
     */
    @Override
    public ResultClassEntity actualizarPlanManejoForestal(PlanGeneralManejoForestalEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoForestal_actualizar");
            processStored.registerStoredProcedureParameter("IdPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdTipoProceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdTipoPlan", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("TipoEscala", Short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("AspectoComplementario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdPlanManejoPadre", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioActualizacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaActualizacion", Timestamp.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdPlanManejo", obj.getIdPlanManejo());
            processStored.setParameter("Descripcion", obj.getDescripcion());
            processStored.setParameter("IdSolicitud", obj.getIdSolicitud());
            processStored.setParameter("IdTipoProceso", obj.getIdTipoProceso());
            processStored.setParameter("IdTipoPlan", obj.getIdTipoPlan());
            processStored.setParameter("TipoEscala", obj.getTipoEscala());
            processStored.setParameter("AspectoComplementario", obj.getAspectoComplementario());
            processStored.setParameter("IdPlanManejoPadre", obj.getIdPlanManejoPadre());
            processStored.setParameter("IdUsuarioActualizacion", obj.getIdUsuarioModificacion());
            processStored.setParameter("FechaActualizacion", new java.sql.Timestamp(date.getTime()));
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizar el plan general de manejo forestal.");
            return  result;

        } catch (Exception e) {
            log.error("PlanGeneralManejoForestalRepositoryImpl - actualizarPlanManejoForestal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [29-09-2021]
     * @modificado: 
     * @descripción: {listar el ubigeo de un contrato}
     * @param:idContrato
     */
    @Override
    public ResultEntity<UbigeoArffsEntity> listarUbigeoContratos(Integer idContrato) {
        ResultEntity<UbigeoArffsEntity> result = new ResultEntity<UbigeoArffsEntity>();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_Contrato_listarUbigeo");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContrato", idContrato);
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            List<UbigeoArffsEntity> lstcontratos = new ArrayList<>();
            if ( !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    UbigeoArffsEntity obj = new UbigeoArffsEntity();
                    obj.setCodDistrito((String) row[0]);
                    obj.setCodProvincia((String) row[1]);
                    obj.setCodDepartamento((String) row[2]);
                    obj.setNombreProvincia((String) row[3]);
                    obj.setNombreDistrito((String) row[4]);;
                    obj.setNombreDepartamento((String) row[5]);
                    obj.setIdContrato((Integer) row[9]);
                    lstcontratos.add(obj);
                }
            }

            result.setData(lstcontratos);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron ubigeos de los contratos viigentes.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }
    }

}
