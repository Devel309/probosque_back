package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;

import java.util.List;

public interface OrdenamientoAreaManejoRepository {
    ResultClassEntity RegistrarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param,MultipartFile file)throws Exception;
    ResultClassEntity ListarOrdenamientoAreaManejo(PlanManejoEntity param)throws Exception;
    ResultClassEntity EliminarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param)throws Exception;
    List<Dropdown> ComboCategoriaOrdenamiento(ParametroEntity param) throws Exception;
    ResultClassEntity ObtenerOrdenamientoAreaManejoArchivo(OrdenamientoAreaManejoEntity param)throws Exception;
}
