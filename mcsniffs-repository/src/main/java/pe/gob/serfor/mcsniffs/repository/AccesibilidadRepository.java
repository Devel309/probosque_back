package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.AccesibilidadManejoMatrizDto;

import java.util.List;

import java.util.List;

public interface AccesibilidadRepository {

    ResultClassEntity ListarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception;
    ResultClassEntity RegistrarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception;
    ResultClassEntity ActualizarAccesibilidadManejo(AccesibilidadManejoEntity accesibilidadManejoEntity) throws Exception;

    ResultClassEntity ListarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception;
    ResultClassEntity RegistrarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity>  accesibilidadManejoRequisitoEntity) throws Exception;
    ResultClassEntity ActualizarAccesibilidadManejoRequisito(List<AccesibilidadManejoRequisitoEntity> accesibilidadManejoRequisitoEntity) throws Exception;
    ResultClassEntity EliminarAccesibilidadManejoRequisito(AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity) throws Exception;

    ResultClassEntity RegistrarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> accesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity ListarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity EliminarAccesibilidadManejoMatriz(AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity ActualizarAccesibilidadManejoMatriz(List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) throws Exception;
    ResultClassEntity EliminarAccesibilidadManejoMatrizDetalle(AccesibilidadManejoMatrizDetalleEntity accesibilidadManejoMatrizDetalleEntity) throws Exception;

}
