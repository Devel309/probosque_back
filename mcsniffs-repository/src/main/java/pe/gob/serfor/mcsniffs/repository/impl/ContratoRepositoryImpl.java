package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoGeometriaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.GenerarCodContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ModeloContratoDto;
import pe.gob.serfor.mcsniffs.repository.ContratoRepository;
// 
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ContratoRepositoryImpl extends JdbcDaoSupport implements ContratoRepository{
    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los contratos}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    /**
     * @autor: José Díaz [08-02-2022]
     * @modificado:
     * @descripción: {lista generación de contratos}
     * @param:ContratoEntity
     */
   public ResultClassEntity listarGeneracionContrato(ContratoDto filtro){
       List<ContratoDto> lista = new ArrayList<ContratoDto>();
       ResultClassEntity result = new ResultClassEntity();
       try {
        StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ListarGeneracion");
           sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
           sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
           sp.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
           sp.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
           SpUtil.enableNullParams(sp);
           sp.setParameter("pageNum", filtro.getPageNum());
           sp.setParameter("pageSize", filtro.getPageSize());
           sp.setParameter("tipoDocumentoGestion", filtro.getTipoDocumentoGestion());
           sp.setParameter("documentoGestion", filtro.getDocumentoGestion());
           sp.execute();
           ContratoDto c = null;
           List<Object[]> spResult = sp.getResultList();

           result.setTotalRecord(0);
           if (spResult!= null && !spResult.isEmpty()) {
               for (Object[] row : spResult) {
                   c = new ContratoDto();


                   c.setDocumentoGestion((Integer) row[0]);
                   c.setIdUsuarioPostulacion((Integer) row[1]);
                   c.setNombreUsuarioPostulacion((String) row[2]);
                   c.setFechaPostulacion((Date) row[3]);
                   c.setGanador((Boolean) row[4]);
                   c.setTipoDocumentoGestion((String) row[5]);
                   c.setTipoDocumentoGestionDescripcion((String) row[6]);
                   c.setEstadoTexto((String) row[7]);
                   result.setTotalRecord((Integer) row[8]);
                   lista.add(c);
               }
           }
           result.setData(lista);
           result.setSuccess(true);
           result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
           return result;
       } catch (Exception e) {
           log.error("listaron listarGeneracionContrato", e.getMessage());
           throw e;
       }
   }


    @Override
    public ResultClassEntity registrarContrato(ContratoEntity obj) throws Exception{
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_Registrar");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTitulo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);       
            processStored.registerStoredProcedureParameter("codigoPartidaRegistral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroContratoConcesion", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("fechaInicioContrato", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFinContrato", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEstadoContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN); 
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContrato", obj.getIdContrato());
            processStored.setParameter("tipoDocumentoGestion", obj.getTipoDocumentoGestion());
            processStored.setParameter("documentoGestion", obj.getDocumentoGestion());
            processStored.setParameter("codigoTitulo", obj.getCodigoTituloH());
            processStored.setParameter("descripcion", obj.getDescripcion());      
            processStored.setParameter("codigoPartidaRegistral", obj.getCodigoPartidaRegistral());
            processStored.setParameter("numeroContratoConcesion", obj.getNroContratoConsecion());

            processStored.setParameter("fechaInicioContrato", obj.getFechaInicioContrato());
            processStored.setParameter("fechaFinContrato", obj.getFechaFinContrato());
            processStored.setParameter("codigoTipoContrato", obj.getCodigoTipoContrato());
            processStored.setParameter("codigoEstadoContrato", obj.getCodigoEstadoContrato());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
       
            processStored.execute();
            Integer id = (Integer) processStored.getOutputParameterValue("idContrato");
            obj.setIdContrato(id);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el contrato correctamente.");

            LogAuditoriaEntity logAuditoriaEntity;

            if(obj.getIdContrato() == 0 || obj.getIdContrato() == null){

                logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MVC_SOLICITUDCONCESION,
                        LogAuditoria.REGISTRAR,
                        LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLICITUDCONCESION + id ,
                        obj.getIdUsuarioRegistro());




            }else{

                logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MVC_SOLICITUDCONCESION,
                        LogAuditoria.ACTUALIZAR,
                        LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUDCONCESION + obj.getIdContrato() ,
                        obj.getIdUsuarioRegistro());

            }

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
   

    
    @Override
    public ResultClassEntity AdjuntarArchivosContrato(ContratoAdjuntosRequestEntity obj) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            //Adjuntar Documentos
            pstm = con.prepareCall("{call pa_ContratoAdjuntos_Registrar(?,?,?,?)}");
            pstm.setObject (1, obj.getIdContrato(), Types.INTEGER);
            pstm.setObject(2, obj.getIdDocumentoAdjunto(),Types.INTEGER);
            pstm.setObject(3, obj.getIdUsuarioRegistro(),Types.INTEGER);
            pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida2 = pstm.executeUpdate();
            ResultSet rsa2 = pstm.getGeneratedKeys();
            if(rsa2.next())
            {
                salida2 = rsa2.getInt(1);
            }
            result.setCodigo(salida2);
            result.setSuccess(true);
            result.setMessage("Se registró el adjunto del contrato.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - AdjuntarArchivosContrato", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - AdjuntarArchivosContrato", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    @Override
    public ResultClassEntity listarContrato(ContratoEntity dto){
        ResultClassEntity result = new ResultClassEntity();
        List<ContratoEntity> lista = new ArrayList<ContratoEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumentoGestion", dto.getTipoDocumentoGestion());
            processStored.setParameter("documentoGestion", dto.getDocumentoGestion());
            processStored.setParameter("idUsuarioPostulacion",dto.getIdUsuarioPostulacion());
            processStored.setParameter("perfil",dto.getPerfil());
            processStored.setParameter("pageNum", dto.getPageNum());
            processStored.setParameter("pageSize", dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ContratoEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new ContratoEntity();
                        c.setIdContrato(row[0]==null?null:(Integer) row[0]);
                        c.setDocumentoGestion(row[1]==null?null:(Integer) row[1]);
                        c.setCodigoTituloH(row[2]==null?null:(String) row[2]);
                        c.setNombreRepresentanteLegal(row[3]==null?null:(String) row[3]);
                        c.setNombreUsuarioPostulante(row[4]==null?null:(String) row[4]);
                        c.setCodigoEstadoContrato(row[5]==null?null:(String) row[5]);
                        c.setDescripcionEstadoContrato(row[6]==null?null:(String) row[6]);
                        c.setIdUsuarioPostulacion(row[7]==null?null:(Integer) row[7]);

                        c.setContratoRemitir(row[8]==null?null:(Boolean) row[8]);
                        c.setIdDepartamento((String) row[9]);
                        c.setDepartamento(row[10]==null?null:(String) row[10]);
                        c.setIdProvincia(row[11]==null?null:(String) row[11]);
                        c.setProvincia(row[12]==null?null:(String) row[12]);
                        c.setIdDistrito(row[13]==null?null:(String) row[13]);
                        c.setDistrito(row[14]==null?null:(String) row[14]);
                        c.setIdGeometria(row[15]==null?null:(Integer) row[15]);
                        c.setCodigoTipoPersonaTitular(row[16]==null?null:(String) row[16]);
                        c.setCodigoTipoDocumentoTitular(row[17]==null?null:(String) row[17]);
                        c.setNumeroDocumentoTitular(row[18]==null?null:(String) row[18]);
                        c.setNombreCompletoTitular(row[19]==null?null:(String) row[19]);
                        c.setRazonSocialTitular(row[20]==null?null:(String) row[20]);
                        c.setTipoDocumentoGestionDescripcion(row[21] == null ? null : (String) row[21]);
                        c.setTipoDocumentoGestion(row[22]==null?null:(String) row[22]);

                        result.setTotalRecord((Integer) row[23]);
                        lista.add(c);
  
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("ListarContrato correctamente.");
            return result;
        } catch (Exception e) {
            log.error("ListarContrato", e.getMessage());
            throw e;
        }
    }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se listan los archivos por contrato}
     * @param:IdContrato
     */
    @Override
    public ResultEntity<ContratoAdjuntosEntity> ObtenerArchivosContrato(Integer IdContrato) {
        return null;
        /*
        ResultEntity<ContratoAdjuntosEntity> result = new ResultEntity<ContratoAdjuntosEntity>();
        List<ContratoAdjuntosEntity>lstFiles = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            ResultClassEntity<ContratoResponseRequest> lstoposicion = ObtenerContrato(IdContrato);
            ContratoResponseRequest contrato = lstoposicion.getData();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ContratoAdjuntos_ObtenerRutaAdjunto(?)}");
            pstm.setObject (1, IdContrato,Types.INTEGER);
            ResultSet rs = pstm.executeQuery();
            ContratoAdjuntosEntity resulfile =null;
            while(rs.next()){
                resulfile = new ContratoAdjuntosEntity();
                resulfile.setContrato(contrato);
                String ruta = null;
                String nombredoc=null;
                Integer idTipoDocumento =0;
                ruta = rs.getString("TX_RUTA");
                nombredoc = rs.getString("TX_NOMBRE");
                idTipoDocumento = rs.getInt("NU_ID_TIPO_DOCUMENTO");
                if(ruta!=null){
                    resulfile.setNombredocumento(nombredoc);
                    resulfile.setIdTipoDocumento(idTipoDocumento);
                    File file = new File(ruta);
                    byte[] fileContent = Files.readAllBytes(file.toPath());
                    resulfile.setDocumento(fileContent);
                    lstFiles.add(resulfile);
                }
            }

            result.setData(lstFiles);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los adjuntos para la generacion del contrato.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - ObtenerArchivosContrato", e.getMessage());
            result.setIsSuccess(false);
            result.setData(lstFiles);
            result.setMessage("Ocurrió un error al obtener el archivo.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - ObtenerArchivosContrato", e2.getMessage());
                result.setIsSuccess(false);
                result.setMessage("Ocurrió un error al obtener el archivo.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }


    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se obtiene un contrato}
     * @param:IdContrato
     */
    @Override
    public ResultClassEntity listarComboContrato(ContratoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<ContratoResponseRequest> lista = new ArrayList<ContratoResponseRequest>();


        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ListarCombo");
            processStored.registerStoredProcedureParameter("codigoEstadoContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioTitular", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoEstadoContrato", obj.getCodigoEstadoContrato());
            processStored.setParameter("codigoTipoContrato", obj.getCodigoTipoContrato());
            processStored.setParameter("idUsuarioTitular", obj.getIdUsuarioTitularTH());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        ContratoResponseRequest c = new ContratoResponseRequest();
                        c.setIdContrato((Integer) row[0]);
                        c.setTituloHabilitante((String) row[3]);
                        c.setAreaTotal(((BigDecimal) row[4]).doubleValue());
                        c.setDescripcionUnidadAprovechamiento((String) row[5]);
                        c.setDescripcionRegion((String) row[6]);
                        c.setDescripcionContrato(c.getIdContrato().toString().concat(" .. ").concat(c.getTituloHabilitante()));
                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("ListarContrato correctamente.");
            return result;
        } catch (Exception e) {
            log.error("ListarContrato", e.getMessage());
            throw e;
        }
    }


    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se obtiene un contrato}
     * @param:IdContrato
     */
    @Override
    public ResultClassEntity ObtenerContrato(ContratoEntity obj) {

        return listarContrato(obj);
    }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se descarga el docx con los datos importates}
     * @param:IdContrato
     */
    // @Override
    // public ResultArchivoEntity DescargarContrato(Integer IdContrato) {
    //     ResultArchivoEntity result = new ResultArchivoEntity();

    //     try {
    //         ResultClassEntity<ContratoResponseRequest> contrato= ObtenerContrato(IdContrato);
    //         InputStream inputStream = AnexoRepositoryImpl.class.getResourceAsStream("/PlantillaContrato.docx");
    //         XWPFDocument doc = new XWPFDocument(inputStream);
    //         SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    //         Date fecha = new Date();
    //         for (XWPFParagraph p : doc.getParagraphs()) {
    //             List<XWPFRun> runs = p.getRuns();
    //             if (runs != null) {
    //                 for (XWPFRun r : runs) {
    //                     String text = r.getText(0);
    //                     if (text != null) {
    //                         if (text.contains("titulohabilitantex")) {
    //                             text = text.replace("titulohabilitantex", contrato.getData().getCodigoTituloH() == null ? "":contrato.getData().getCodigoTituloH() );
    //                             r.setText(text, 0);
    //                         }else if (text.contains("solicitantex")) {
    //                             text = text.replace("solicitantex",  contrato.getData().getNombreUsuarioPostulante());
    //                             r.setText(text, 0);
    //                         }else if (text.contains("regenteforestalx")) {
    //                             text = text.replace("regenteforestalx", contrato.getData().getNombresApellidosRegente());
    //                             r.setText(text, 0);
    //                         }else if (text.contains("codigouax")) {
    //                             text = text.replace("codigouax",contrato.getData().getObjectIdUa());
    //                             r.setText(text, 0);
    //                         }else if (text.contains("areax")) {
    //                             text = text.replace("areax", contrato.getData().getSuperficieUA());
    //                             r.setText(text, 0);
    //                         }else if (text.contains("fechainiciox")) {
    //                             text = text.replace("fechainiciox", formatter.format(contrato.getData().getFechaInicioContrato() == null ? fecha :contrato.getData().getFechaInicioContrato()));
    //                             r.setText(text, 0);
    //                         }else if (text.contains("fechafinx")) {
    //                             text = text.replace("fechafinx", formatter.format(contrato.getData().getFechaFinContrato() == null ? fecha :contrato.getData().getFechaFinContrato()));
    //                             r.setText(text, 0);
    //                         }
    //                     }
    //                 }
    //             }
    //         }
            
    //         ByteArrayOutputStream b = new ByteArrayOutputStream();
    //         doc.write(b);
    //         doc.close(); 
            
    //         byte[] fileContent =b.toByteArray();
    //         result.setArchivo(fileContent);
    //         result.setNombeArchivo("Contrato.docx");
    //         result.setContenTypeArchivo("application/octet-stream");
    //         result.setSuccess(true);
    //         result.setMessage("Se descargo el contrato.");
    //         return result;
    //     } catch (Throwable e) {
    //         log.error("ContratoRepositoryImpl - descargarContrato ", e.getMessage());
    //         result.setSuccess(false);
    //         result.setMessage("Ocurrió un error.");
    //         result.setMessageExeption(e.getMessage());
    //         return result;
    //     }
    // }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se registran las validaciones de los datos del contrato}
     * @param:ContratoValidacionesEntity
     */
    @Override
    public ResultClassEntity RegistrarValidaciones(ContratoValidacionesEntity obj) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ContratoValidaciones_Registrar(?,?,?,?,?,?)}");
            pstm.setObject (1, obj.getIdContrato(), Types.INTEGER);
            pstm.setObject(2, obj.getIdEntidadConsulta(),Types.INTEGER);
            pstm.setObject(3, obj.getObservacion(), Types.VARCHAR);
            pstm.setObject(4, obj.getValidado(), Types.BOOLEAN);
            pstm.setObject(8, obj.getIdUsuarioRegistro(),Types.INTEGER);
            pstm.setObject(9, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida = pstm.executeUpdate();
            ResultSet rsa = pstm.getGeneratedKeys();
            if(rsa.next())
            {
                salida = rsa.getInt(1);
            }
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se registró la validacion de datos.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - RegistrarValidaciones", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - RegistrarValidaciones", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {se Listan las validaciones echas a los datos}
     * @param:IdContrato
     */
    @Override
    public ResultEntity<ContratoValidacionesEntity> ListarValidaciones(Integer IdContrato) {
        return null;
        /*
        ResultEntity<ContratoValidacionesEntity> result = new ResultEntity<ContratoValidacionesEntity>();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ContratoValidaciones_Listar(?)}");
            pstm.setObject (1, IdContrato, Types.INTEGER);
            ResultSet rs = pstm.executeQuery();
            ContratoValidacionesEntity c = null;
            List<ContratoValidacionesEntity> lstresult = new ArrayList<>();
            while(rs.next()){
                c = new ContratoValidacionesEntity();
                c.setIdContratoValidaciones(rs.getInt("NU_ID_CONTRATOVALIDACIONES"));
                c.setIdContrato(rs.getInt("NU_ID_CONTRATO"));
                c.setIdEntidadConsulta(rs.getInt("NU_ID_CONSULTA_ENTIDAD"));
                c.setObservacion(rs.getString("TX_OBSERVACION"));
                c.setValidado(rs.getObject("NU_VALIDADO") == null ? null :  rs.getBoolean("NU_VALIDADO"));
                c.setDescripcionEntidadConsulta(rs.getString("TX_DESCRIPCION"));
                lstresult.add(c);
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron autorizaciones enviadas.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - ListarValidaciones", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - ListarValidaciones", e2.getMessage());
                result.setIsSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
        }
        */
    }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se actualiza el contrato}
     * @param:IdContrato
     */
    @Override
    public ResultClassEntity ActualizarContrato(ContratoEntity obj) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Contrato_Actualizar(?,?,?,?,?,?,?,?,?,?)}");
            pstm.setObject (1, obj.getIdContrato(), Types.INTEGER);
            pstm.setObject(2, obj.getCodigoTituloH(),Types.VARCHAR);
            pstm.setObject(3, obj.getFirmado(), Types.BOOLEAN);
            pstm.setObject(4, obj.getDatosValidados(), Types.BOOLEAN);
            pstm.setObject(5, obj.getFechaInicioContrato(), Types.TIMESTAMP);
            pstm.setObject(6, obj.getFechaFinContrato(),Types.TIMESTAMP);
            pstm.setObject(7, obj.getDniRucRepresentanteLegal(), Types.VARCHAR);
            pstm.setObject(8, obj.getNombreRaSoRepresentanteLegal(), Types.VARCHAR);
            pstm.setObject(9, obj.getIdUsuarioModificacion(),Types.INTEGER);
            pstm.setObject(10, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida = pstm.executeUpdate();
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se actualizó el contrato.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - ActualizarContrato", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - ActualizarContrato", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }

    /**
     * @autor: JaquelineDB [27-07-2021]
     * @modificado:
     * @descripción: {Se actualiza el satus del contrato}
     * @param:IdContrato
     */
    @Override
    public ResultClassEntity ActualizarStatusContrato(ContratoEntity obj) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm  = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Contrato_ActualizarSatus(?,?,?,?)}");
            pstm.setObject (1, obj.getIdContrato(), Types.INTEGER);
            pstm.setObject(2, obj.getIdSatusContrato(),Types.VARCHAR);
            pstm.setObject(3, obj.getIdUsuarioModificacion(),Types.INTEGER);
            pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida = pstm.executeUpdate();
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se actualizó el estado del contrato.");
            return result;
        } catch (Exception e) {
            log.error("ContratoRepositoryImpl - ActualizarStatusContrato", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally{
			try {
				if(pstm!= null) pstm.close();
				if(con!= null) con.close();
			} catch (Exception e2) {
                log.error("ContratoRepositoryImpl - ActualizarStatusContrato", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
		}
        */
    }


    @Override
    public List<ContratoEntity> listarContratoGeneral(ContratoEntity obj){
        
        List<ContratoEntity> lista = new ArrayList<ContratoEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ListarGeneral"); 
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored); 
            processStored.setParameter("idContrato", obj.getIdContrato());
            processStored.setParameter("tipoDocumentoGestion", obj.getTipoDocumentoGestion());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ContratoEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new ContratoEntity();
                        c.setIdContrato(row[0]==null?null:(Integer) row[0]);
                        c.setDocumentoGestion(row[1]==null?null:(Integer) row[1]);
                        c.setCodigoTituloH(row[2]==null?null:(String) row[2]);
                        c.setDescripcion(row[3]==null?null:(String) row[3]);
                        c.setCodigoPartidaRegistral(row[4]==null?null:(String) row[4]);
                        c.setNroContratoConsecion(row[5]==null?null:(String) row[5]);
                        c.setDatosValidados(row[6]==null?null:(Boolean) row[6]);
                        c.setFechaInicioContrato(row[7]==null?null:(Timestamp) row[7]);
                        c.setFechaFinContrato(row[8]==null?null:(Timestamp) row[8]);
                        c.setCodigoTipoContrato(row[9]==null?null:(String) row[9]);
                        c.setCodigoEstadoContrato(row[10]==null?null:(String) row[10]);                        
                        c.setNumeroExpediente(row[11]==null?null:(String) row[11]);                        
                        c.setFechaPresentacion(row[12]==null?null:(Timestamp) row[12]);
                        lista.add(c);
  
                    }
                }
            }        
            return lista;
        } catch (Exception e) {
            log.error("listarContratoGeneral", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity generarCodigoContrato(GenerarCodContratoDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_generarCodigoContrato");
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDepartamento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("derecho", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("recurso", String.class, ParameterMode.IN);       
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idUsuario", obj.getIdUsuario());
            processStored.setParameter("idDepartamento", obj.getIdDepartamento());
            processStored.setParameter("derecho", obj.getDerecho());
            processStored.setParameter("recurso", obj.getRecurso());
       
            processStored.execute();

            String codigo = (String) processStored.getOutputParameterValue("codigo");
 
            result.setSuccess(true);
            result.setData(codigo);
            result.setMessage("Se registró el contrato correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarEstadoContrato(ContratoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ActualizarEstadoContrato");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);             
            processStored.registerStoredProcedureParameter("codigoEstadoContrato", String.class, ParameterMode.IN); 
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN); 

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idContrato", obj.getIdContrato());           
            processStored.setParameter("codigoEstadoContrato", obj.getCodigoEstadoContrato());          
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
       
            processStored.execute();
  
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó el contrato correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ModeloContratoDto obtenerModeloContrato(Integer idContrato) throws Exception {
        ModeloContratoDto temp = null;
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ModeloContrato");            
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);   

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContrato",idContrato); 

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
               
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new ModeloContratoDto();

                        temp.setDepartamentoTitular(row[0]==null?null:(String) row[0]);
                        temp.setProvinciaTitular(row[1]==null?null:(String) row[1]);
                        temp.setDistritoTitular(row[2]==null?null:(String) row[2]);
                        temp.setDepartamentoRle(row[3]==null?null:(String) row[3]);
                        temp.setProvinciaRle(row[4]==null?null:(String) row[4]);
                        temp.setDistritoRle(row[5]==null?null:(String) row[5]);
                        temp.setIdContrato(row[6]==null?null:(Integer) row[6]);
                        temp.setEstadoContrato(row[7]==null?null:(String) row[7]);
                        temp.setTipoContrato(row[8]==null?null:(String) row[8]);
                        temp.setTituloHabilitante(row[9]==null?null:(String) row[9]);
                        temp.setCodigoPartidaReg(row[10]==null?null:(String) row[10]);
                        temp.setNumeroContratoConcesion(row[11]==null?null:(String) row[11]); 
                        temp.setFechaIniContrato(row[12]==null?null:(Date) row[12]);
                        temp.setFechaFinContrato(row[13]==null?null:(Date) row[13]);
                        temp.setAreaTotal(row[14]==null?null: (BigDecimal) row[14]); 
                        temp.setUnidadAprovechamiento(row[15]==null?null:(String) row[15]);
                        temp.setRegion(row[16]==null?null:(String) row[16]); 
                        temp.setTipoPersonaTitularTH(row[17]==null?null:(String) row[17]);
                        temp.setTipoPersonaTitularTHDesc(row[18]==null?null:(String) row[18]);
                        temp.setTipoDocumentoTitularTH(row[19]==null?null:(String) row[19]); 
                        temp.setTipoDocumentoTitularTHDesc(row[20]==null?null:(String) row[20]); 
                        temp.setNumeroDocumentoTitularTH(row[21]==null?null:(String) row[21]);
                        temp.setNombreTitularTH(row[22]==null?null:(String) row[22]); 
                        temp.setApellidoPaternoTitularTH(row[23]==null?null:(String) row[23]);
                        temp.setApellidoMaternoTitularTH(row[24]==null?null:(String) row[24]); 
                        temp.setRazonSocialTitularTH(row[25]==null?null:(String) row[25]); 
                        temp.setEmailTitularTH(row[26]==null?null:(String) row[26]);
                        temp.setIdUsuarioTitularTH(row[27]==null?null:(Integer) row[27]); 
                        temp.setDireccionTitularTH(row[28]==null?null:(String) row[28]);
                        temp.setTipoPersonaRepLegal(row[29]==null?null:(String) row[29]);
                        temp.setTipoPersonaRepLegalDesc(row[30]==null?null:(String) row[30]);
                        temp.setTipoDocumentoRepLegal(row[31]==null?null:(String) row[31]); 
                        temp.setTipoDocumentoRepLegalDesc(row[32]==null?null:(String) row[32]);
                        temp.setNumeroDocumentoRepLegal(row[33]==null?null:(String) row[33]); 
                        temp.setNombreRepLegal(row[34]==null?null:(String) row[34]);
                        temp.setApellidoPaternoRepLegal(row[35]==null?null:(String) row[35]); 
                        temp.setApellidoMaternoRepLegal(row[36]==null?null:(String) row[36]); 
                        temp.setEmailRepLegal(row[37]==null?null:(String) row[37]); 
                        temp.setDireccionAu(row[38]==null?null:(String) row[38]); 
                        temp.setDepartamentoAu(row[39]==null?null:(String) row[39]); 
                        temp.setProvinciaAu(row[40]==null?null:(String) row[40]); 
                        temp.setDistritoAu(row[41]==null?null:(String) row[41]);
                        temp.setNumeroDocumentoAu(row[42]==null?null:(String) row[42]);
                        temp.setDireccionRepLegal(row[43]==null?null:(String) row[43]);
                        
                    }
                }
            }

            return temp;
        } catch (Exception e) {
            log.error("obtenerModeloContrato", e.getMessage());
            throw e;
        }
    }

    /**
     * @autor: Abner Valdez [28-12-2021]
     * @modificado:
     * @descripción: {Obtener las geometrias por idsContrato}
     * @param:idsContrato
     */
    @Override
    public ResultClassEntity listarContratoGeometria(String idsContrato) {
        ResultClassEntity result = new ResultClassEntity();
        List<ContratoGeometriaDto> objList = new ArrayList<ContratoGeometriaDto>();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_ContratoArea_ListarGeometria");
            processStored.registerStoredProcedureParameter("IdsContrato", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdsContrato", idsContrato);

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ContratoGeometriaDto item2 = new ContratoGeometriaDto();
                    item2.setIdContratoArea((Integer) row[0]);
                    item2.setIdContrato((Integer) row[1]);
                    item2.setNombreDepartamento((String) row[2]);
                    item2.setAreaHA(((BigDecimal) row[3]).doubleValue());
                    item2.setAreaAprobada(((BigDecimal) row[4]).doubleValue());
                    item2.setTipoOrigen((String) row[5]);
                    item2.setIdOrigen((Integer) row[6]);
                    item2.setIdUnidadAprovechamiento((Integer) row[7]);
                    item2.setEstadoUA((String) row[8]);
                    item2.setUbigeo((String) row[9]);
                    item2.setGeometry((String) row[10]);
                    //item2.setDescripcionContrato((String) row[11]);
                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessageExeption(e.getMessage());
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarGeometria(ContratoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_ActualizarGeometria");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);             
            processStored.registerStoredProcedureParameter("idGeometria", Integer.class, ParameterMode.IN); 
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN); 

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idContrato", obj.getIdContrato());           
            processStored.setParameter("idGeometria", obj.getIdGeometria());          
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
       
            processStored.execute();
  
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó el id Geometría correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarEstadoRemitido(ContratoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Contrato_Remision_Actualizar");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("remitido", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idContrato", obj.getIdContrato());
            processStored.setParameter("remitido", obj.getContratoRemitido());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se remitió el contrato correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo remitir el contrato.");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

    @Override
    public  ResultClassEntity obtenerPostulacion(ContratoDto obj) {

        ResultClassEntity result= new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Postulacion_Obtener");
            processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumentoGestion", obj.getTipoDocumentoGestion());
            processStored.setParameter("documentoGestion", obj.getDocumentoGestion());



            processStored.execute();
            ContratoDto temp  = new ContratoDto();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {
                        temp.setDocumentoGestion(row[0]==null?null:(Integer) row[0]);
                        temp.setTipoDocumentoGestion(row[1]==null?null:(String) row[1]);
                        temp.setIdUsuarioPostulacion(row[2]==null?null:(Integer) row[2]);
                        temp.setCodigoTipoPersonaPostulante(row[3]==null?null:(String) row[3]);
                        temp.setCodigoTipoDocumentoPostulante(row[4]==null?null:(String) row[4]);
                        temp.setNumeroDocumentoPostulante(row[5]==null?null:(String) row[5]);
                        temp.setNombrePostulante(row[6]==null?null:(String) row[6]);
                        temp.setApellidoPaternoPostulante(row[7]==null?null:(String) row[7]);
                        temp.setApellidoMaternoPostulante(row[8]==null?null:(String) row[8]);
                        temp.setCorreoPostulante(row[9]==null?null:(String) row[9]);
                        temp.setCodigoTipoPersonaRepresentante(row[10]==null?null:(String) row[10]);
                        temp.setCodigoTipoDocumentoRepresentante(row[11]==null?null:(String) row[11]);
                        temp.setNumeroDocumentoRepresentante(row[12]==null?null:(String) row[12]);
                        temp.setNombreRepresentante(row[13]==null?null:(String) row[13]);
                        temp.setApellidoPaternoRepresentante(row[14]==null?null:(String) row[14]);
                        temp.setApellidoMaternoRepresentante(row[15]==null?null:(String) row[15]);
                    }
                }
            }
            result.setMessage("Se obtuvo Proceso Postulación correctamente.");
            result.setSuccess(true);
            result.setData(temp);
            return result;
        } catch (Exception e) {
            log.error("obtenerProcesoPostulacion", e.getMessage());

            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

}
