package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface InformacionBasicaUbigeoRepository {

   ResultClassEntity RegistrarInformacionBasicaDistrito(InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity) throws Exception;
   List<InformacionBasicaUbigeoEntity> ListarInformacionBasicaDistrito(InformacionBasicaEntity obj) throws Exception;
   ResultClassEntity ActualizarInformacionBasicaDistrito(InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity) throws Exception;
   ResultClassEntity eliminarInformacionBasicaUbigeo(InformacionBasicaUbigeoEntity informacionBasicaUbigeoEntity) throws Exception;
}
