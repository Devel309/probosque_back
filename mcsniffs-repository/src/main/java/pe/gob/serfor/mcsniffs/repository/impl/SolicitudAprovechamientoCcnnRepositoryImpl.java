package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.DistritoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudAprovechamientoCcnnRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Repository
public class SolicitudAprovechamientoCcnnRepositoryImpl extends JdbcDaoSupport implements SolicitudAprovechamientoCcnnRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(SolicitudAprovechamientoCcnnRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ObtenerSolicitudAprovechamientoCcnn(SolicitudEntity solicitud) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        try{
            Connection con = null;
            PreparedStatement pstm  = null;

            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Solicitud_Obtener (?)}");
            pstm.setObject (1, solicitud.getIdSolicitud(), Types.INTEGER);

            ResultSet rs = pstm.executeQuery();
            SolicitudEntity obj = new SolicitudEntity();
            PersonaEntity persona = new PersonaEntity();
            ComunidadEntity comunidad = new ComunidadEntity();
            DistritoEntity distrito = new DistritoEntity();
            FederacionComunidadEntity  federacion = new FederacionComunidadEntity();

            while(rs.next()){
                obj.setIdSolicitud(rs.getInt(1));
                persona.setIdPersona(rs.getInt(2));
                persona.setNombres(rs.getString(3));
                persona.setApellidoPaterno(rs.getString(4));
                persona.setApellidoMaterno(rs.getString(5));
                comunidad.setIdComunidad(rs.getInt(6));
                comunidad.setNombre(rs.getString(7));
                comunidad.setCredencial(rs.getString(8));
                comunidad.setRuc(rs.getString(9));
                federacion.setIdFedComunidad(rs.getInt(10));
                federacion.setDescripcion(rs.getString(11));
                obj.setSuperficieComunidad(rs.getDouble(12));
                obj.setAreaManejoForestal(rs.getDouble(13));
                obj.setNroSectorAnexo(rs.getString(14));

                distrito.setIdDepartamento(rs.getInt(15));
                distrito.setIdProvincia(rs.getInt(16));
                distrito.setIdDistrito(rs.getInt(17));

                comunidad.setFederacionComunidad(federacion);
                persona.setComunidad(comunidad);
                obj.setDistrito(distrito);
                //obj.setPersona(persona);

            }
            result.setSuccess(true);
            result.setMessage("Se obtuvo solicitud.");
            result.setData(obj);
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        */
    }

}