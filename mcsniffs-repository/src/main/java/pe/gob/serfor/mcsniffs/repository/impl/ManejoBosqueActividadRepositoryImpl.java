package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ManejoBosqueActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueActividadRepository;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManejoBosqueActividadRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueActividadRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueActividadRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosqueActividad(List<ManejoBosqueActividadEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ManejoBosqueActividadEntity> list = new ArrayList<ManejoBosqueActividadEntity>();
        try{
            for(ManejoBosqueActividadEntity p:param){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueActividad_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idManejoBosqueAct", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("catOrdenamiento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Ordenamiento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                processStored.setParameter("idManejoBosqueAct", p.getIdManejoBosqueAct());
                processStored.setParameter("catOrdenamiento", p.getCatOrdenamiento());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("Ordenamiento", p.getOrdenamiento());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();

                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ManejoBosqueActividadEntity temp = new ManejoBosqueActividadEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();
                        pla.setIdPlanManejo((Integer) row[1]);
                        temp.setPlanManejo(pla);
                        temp.setIdManejoBosqueAct((Integer) row[0]);
                        temp.setCatOrdenamiento((String) row[2]);
                        temp.setOrdenamiento((String) row[3]);
                        temp.setActividad((String) row[4]);
                        list.add(temp);
                    }
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró el Manejo Bosque Actividad correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity ListarManejoBosqueActividad(PlanManejoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ManejoBosqueActividadEntity> list = new ArrayList<ManejoBosqueActividadEntity>() ;
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueActividad_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ManejoBosqueActividadEntity temp = new ManejoBosqueActividadEntity();
                    PlanManejoEntity pla = new PlanManejoEntity();
                    pla.setIdPlanManejo((Integer) row[0]);
                    temp.setPlanManejo(pla);
                    temp.setIdManejoBosqueAct((Integer) row[1]);
                    temp.setCatOrdenamiento((String) row[2]);
                    temp.setOrdenamiento((String) row[3]);
                    temp.setActividad((String) row[4]);
                    list.add(temp);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se actualizó Manejo Bosque Actividad correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueActividad(ManejoBosqueActividadEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueActividad_Eliminar");
            processStored.registerStoredProcedureParameter("idManejoBosqueAct", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored.setParameter("idManejoBosqueAct", param.getIdManejoBosqueAct());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
