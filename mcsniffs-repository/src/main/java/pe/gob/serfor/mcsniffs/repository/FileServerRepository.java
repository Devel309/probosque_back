package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;

import java.io.File;

///
//repository interfaces al file server
//Guardar archivos /return archivo
//Descargar archivos /return archivo
public interface FileServerRepository{
    String insertarArchivoSolicitudAcceso(MultipartFile file);

    ResultArchivoEntity descargarArchivoSolicitudAcceso(Integer idSolicitudAcceso);
    String insertarArchivoSolicitud(MultipartFile file);
    ResultArchivoEntity descargarArchivoSolicitud(Integer idArchivoSolicitud,Integer idSolicitud,String tipoArchivo);
    ResultArchivoEntity descargarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAdjunto,Integer idSolicitud,String tipoArchivo);
}