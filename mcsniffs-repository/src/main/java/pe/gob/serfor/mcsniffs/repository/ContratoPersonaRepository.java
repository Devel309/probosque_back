package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoPersonaDto;

public interface ContratoPersonaRepository {

    ResultClassEntity registrarContratoPersona(ContratoPersonaDto dto) throws Exception;
    List<ContratoPersonaDto> listarContratoPersona(ContratoPersonaDto dto) throws Exception;
}
