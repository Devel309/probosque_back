package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.OrganizacionActividadRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class OrganizacionActividadRepositoryImpl extends JdbcDaoSupport implements OrganizacionActividadRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(OrganizacionActividadRepositoryImpl.class);

    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    @Autowired
    private FileServerConexion fileServerConexion;
    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Registrar organización de actividad}
     * @param:List<OrganizacionActividadEntity>
     */
    @Override
    public ResultClassEntity RegistrarOrganizacionActividad(List<OrganizacionActividadEntity> param) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrganizacionActividadEntity> list = new ArrayList<OrganizacionActividadEntity>();
        try{
            for(OrganizacionActividadEntity p:param){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrganizacionActividad_Registrar");
                processStored.registerStoredProcedureParameter("idOrgActividad", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("organizacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idOrgActividad", p.getIdOrgActividad());
                processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("organizacion", p.getOrganizacion());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();


                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        OrganizacionActividadEntity obj = new OrganizacionActividadEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();

                        obj.setIdOrgActividad((Integer) row[0]);
                        pla.setIdPlanManejo((Integer) row[1]);
                        obj.setPlanManejo(pla);
                        obj.setActividad((String) row[2]);
                        obj.setOrganizacion((String) row[3]);
                        list.add(obj);
                    }
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró la organización para el desarrollo de la actividad correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por filtro organización de actividad}
     * @param:OrganizacionActividadEntity
     */
    @Override
    public ResultClassEntity ListarPorFiltroOrganizacionActividad(OrganizacionActividadEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<OrganizacionActividadEntity> list  = new ArrayList<OrganizacionActividadEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrganizacionActividad_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    OrganizacionActividadEntity obj = new OrganizacionActividadEntity();
                    PlanManejoEntity pla = new PlanManejoEntity();

                    obj.setIdOrgActividad((Integer) row[0]);
                    pla.setIdPlanManejo((Integer) row[1]);
                    obj.setPlanManejo(pla);
                    obj.setActividad((String) row[2]);
                    obj.setOrganizacion((String) row[3]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se obtuvieron organización de actividad.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Elimina organización de actividad}
     * @param:OrganizacionActividadEntity
     */
    @Override
    public ResultClassEntity EliminarOrganizacionActividad(OrganizacionActividadEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrganizacionActividad_Eliminar");
            processStored.registerStoredProcedureParameter("idOrgActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idOrgActividad", param.getIdOrgActividad());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");


            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarOrganizacionActividadArchivo(MultipartFile file,PlanManejoEntity planManejo) {
        ArchivoEntity archivo = new ArchivoEntity();
        ResultClassEntity result = new ResultClassEntity();
        try{
            String archivoGenerado=fileServerConexion.uploadFile(file);
            archivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
            archivo.setNombre(FilenameUtils.getBaseName(file.getOriginalFilename()));
            archivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
            archivo.setRuta(fileServerPath.concat(archivoGenerado));
            archivo.setTipoDocumento("3");
            archivo.setEstado("A");
            planManejo.setEstado("A");
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrganizacionActividadArchivo_Registrar");
            
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            //processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("tipoDocumento", archivo.getTipoDocumento());
            processStored.setParameter("idPlanManejo",planManejo.getIdPlanManejo());
            processStored.setParameter("ruta", archivo.getRuta());
            processStored.setParameter("nombre", archivo.getNombre());
            processStored.setParameter("nombreGenerado", archivo.getNombreGenerado());
            processStored.setParameter("extension", archivo.getExtension());
            processStored.setParameter("estado", planManejo.getEstado());
            processStored.setParameter("idUsuarioRegistro", planManejo.getIdUsuarioRegistro());
            //processStored.setParameter("idUsuarioModificacion", planManejo.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró organigrama.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." );
            result.setInnerException(e.getMessage());
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por OrganizacionActivdadArchivo}
     * @param:OrganizacionActividadEntity
     */
    @Override
    public ResultClassEntity ListarOrganizacionActividadArchivo(PlanManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        OrganizacionActividadArchivoEntity obj = new OrganizacionActividadArchivoEntity();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrganizacionActividadArchivo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigo", "PGMFORGACT");
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    obj.setIdPlanManejo((Integer) row[0]);
                    obj.setIdOrgActividadArchivo((Integer) row[1]);                
                    obj.setNombreArchivo((String) row[2]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se obtuvieron organización de actividad archivo.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
  /**
     * @autor: Ivan Minaya [22-09-2021]
     * @modificado:
     * @descripción: {Lista por pa_OrganizacionActividadArchivo_Listar}
     * @param:pa_OrganizacionActividadArchivo_Listar
     */
    @Override
    public ResultEntity ListarPorFiltroOrganizacionActividadArchivo(PlanManejoEntity request) {
        ResultEntity resul=new ResultEntity();
        try
        {        
            List<OrganizacionActividadArchivoEntity> objList=new ArrayList<OrganizacionActividadArchivoEntity>();
            String sql = "EXEC pa_OrganizacionActividadArchivo_Listar " + request.getIdPlanManejo()+",''";                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);                 
                 
                    for(Map<String, Object> row:rows) {
                        OrganizacionActividadArchivoEntity obj=new OrganizacionActividadArchivoEntity();                       
                        
                        obj.setIdPlanManejo((Integer)row.get("NU_ID_PLAN_MANEJO")) ;
                        obj.setIdOrgActividadArchivo((Integer)row.get("NU_ID_PLANMANEJO_ARCHIVO")) ;                       
                        obj.setNombreArchivo((String) row.get("NombreArchivo")); 

                        obj.setIdArchivo((Integer) row.get("NU_ID_ARCHIVO")); 
                        obj.setDescripcion((String) row.get("TX_DESCRIPCION"));
                        obj.setObservacion((String) row.get("TX_OBSERVACION")); 
                        obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_REGISTRO"));  
                        obj.setFechaRegistro((Date) row.get("FE_FECHA_REGISTRO")); 
                       
                        objList.add(obj);
                    }        
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage("Ocurrió un Error");
            resul.setInnerException(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

}
