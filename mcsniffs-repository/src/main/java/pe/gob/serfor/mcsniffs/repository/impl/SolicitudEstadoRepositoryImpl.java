package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;

import pe.gob.serfor.mcsniffs.entity.EstadoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.repository.SolicitudEstadoRepository;

@Service
public class SolicitudEstadoRepositoryImpl extends JdbcDaoSupport implements SolicitudEstadoRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    
    @Override
    public ResultEntity ListaComboSolicitudEstado(EstadoSolicitudEntity request) {
        ResultEntity resul=new ResultEntity();
        try
        {
            List<Dropdown> objList=new ArrayList<Dropdown>();  
            Dropdown _obj=new Dropdown();
            _obj.setValor(0) ;
            _obj.setLabel("--Seleccione--");
            objList.add(_obj);
                    String sql = "select * from sERFOR_BDMCSNIFFS.dbo.T_MAC_ESTADOSOLICITUD tme where TX_ESTADO ='A' " ;
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);  
                    for(Map<String, Object> row:rows) {
                        Dropdown obj=new Dropdown();
                        obj.setLabel((String)row.get("TX_NOMBRE"));   
                        obj.setValor((Integer)row.get("NU_ID_ESTADOSOLICITUD"));                    
                        obj.setValue((String)(row.get("NU_ID_ESTADOSOLICITUD")).toString());                    
                        objList.add(obj);
                    }
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }
}
