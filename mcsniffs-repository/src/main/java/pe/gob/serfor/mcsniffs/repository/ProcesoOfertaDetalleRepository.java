package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ProcesoOfertaDetalleRepository {
    ResultClassEntity guardarProcesoOfertaDetalle(ProcesoOfertaDetalleEntity obj);
}
