package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDetalleDto;

public interface MedidaDetalleRepository {
    List<MedidaDetalleDto> listarMedidaDetalle(MedidaDetalleDto dto) throws Exception;
    ResultClassEntity registrarMedidaDetalle(MedidaDetalleDto dto) throws Exception;
    ResultClassEntity eliminarMedidaDetalle(MedidaDetalleDto dto) throws Exception;
    
}
