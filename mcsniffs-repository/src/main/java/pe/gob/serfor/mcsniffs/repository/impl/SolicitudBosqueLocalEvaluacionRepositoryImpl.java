package pe.gob.serfor.mcsniffs.repository.impl;

 

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudBosqueLocalEvaluacionRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalEvaluacionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(SolicitudBosqueLocalEvaluacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public List<SolicitudBosqueLocalEvaluacionDto> listarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto dto) throws Exception {
         
        List<SolicitudBosqueLocalEvaluacionDto> lista = new ArrayList<SolicitudBosqueLocalEvaluacionDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Evaluacion_Listar");            
            processStored.registerStoredProcedureParameter("idBosqueLocalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idBosqueLocalEvaluacion",dto.getIdBosqueLocalEvaluacion());
            processStored.setParameter("idSolBosqueLocal",dto.getIdSolBosqueLocal());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalEvaluacionDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new SolicitudBosqueLocalEvaluacionDto();

                        temp.setIdBosqueLocalEvaluacion(row[0]==null?null:(Integer) row[0]);
                        temp.setIdSolBosqueLocal(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoResultadoEvaluacion(row[2]==null?null:(String) row[2]);
                        temp.setObservacion(row[3]==null?null:(String) row[3]);
                        temp.setIdArchivoReporte(row[4]==null?null:(Integer) row[4]);
                        temp.setEvalGabineteObservado(row[5]==null?null:(Boolean) row[5]);
                        temp.setEvalCampoObservado(row[6]==null?null:(Boolean) row[6]);
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocalEvaluacion", e.getMessage());
            throw e;
        }
    }


    @Override
    public ResultClassEntity eliminarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Evaluacion_Eliminar");
            processStored.registerStoredProcedureParameter("idBosqueLocalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idBosqueLocalEvaluacion", dto.getIdBosqueLocalEvaluacion());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }



    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalEvaluacionDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_Evaluacion_Registrar");
            processStored.registerStoredProcedureParameter("idBosqueLocalEvaluacion", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoResultadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoReporte", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("evalGabineteObservado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("evalCampoObservado", Boolean.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idBosqueLocalEvaluacion", dto.getIdBosqueLocalEvaluacion());
            processStored.setParameter("idSolBosqueLocal", dto.getIdSolBosqueLocal());
            processStored.setParameter("codigoResultadoEvaluacion", dto.getCodigoResultadoEvaluacion());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("idArchivoReporte", dto.getIdArchivoReporte());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.setParameter("evalGabineteObservado", dto.getEvalGabineteObservado());
            processStored.setParameter("evalCampoObservado", dto.getEvalCampoObservado());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se Ejecuto el Procedimiento correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    
}
