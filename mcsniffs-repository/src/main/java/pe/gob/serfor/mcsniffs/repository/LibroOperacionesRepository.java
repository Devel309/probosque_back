package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;

public interface LibroOperacionesRepository {
    ResultClassEntity registrarLibroOperaciones(LibroOperacionesDto dto) throws Exception;
    ResultClassEntity actualizarLibroOperaciones(LibroOperacionesDto dto) throws Exception;
    ResultClassEntity eliminarLibroOperaciones(LibroOperacionesDto dto) throws Exception;
    ResultClassEntity listarLibroOperaciones(LibroOperacionesDto dto);
    ResultClassEntity validarNroRegistroLibroOperaciones(LibroOperacionesDto dto) throws Exception;
    ResultClassEntity listarLibroOperacionesPlanManejo(PlanManejoEntity planManejoEntity) throws Exception;
    ResultClassEntity listarEvaluacionLibroOperaciones(LibroOperacionesDto dto)throws Exception;
}   
