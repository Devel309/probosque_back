package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.centroTansformacion.CentroTransformacionDto;
import pe.gob.serfor.mcsniffs.repository.CentroTransformacionRepository;

@Repository
public class CentroTransformacionRepositoryImpl extends JdbcDaoSupport implements CentroTransformacionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS") 
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(CentroTransformacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarCentroTransformacion(CentroTransformacionDto obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<CentroTransformacionDto> lista = new ArrayList<CentroTransformacionDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_centroTransformacion_Listar");
            processStored.execute();
            
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    CentroTransformacionDto temp = new CentroTransformacionDto();
                    temp.setIdCentroTransformacion((Integer) row[0]);
                    temp.setRazonSocial((String) row[1]);
                    temp.setRuc((String) row[2]);
                    temp.setDireccion((String) row[3]);
                    temp.setAutorizacion((String) row[4]);
                    temp.setRepLegal((String) row[5]);
                    temp.setDepositos((String) row[6]);
                    temp.setCentroComercializacion((String) row[7]);
                    temp.setCentroAcopio((String) row[8]);

                    lista.add(temp);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se ejecuto el procedimiento Correctamente");
            return result;
        } catch (Exception e) {
            log.error("AprovechamientoRepositoryImpl - ListarCentroTransformacion", e.getMessage());
            throw e;
        }

    }

   

}
