package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ConsultaEntidadEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface ConsultaEntidadRepository {
    ResultEntity<ConsultaEntidadEntity> listarEntidades();
}
