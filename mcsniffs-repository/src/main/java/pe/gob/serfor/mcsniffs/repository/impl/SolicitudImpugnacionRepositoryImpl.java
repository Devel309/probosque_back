package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.expression.spel.ast.BooleanLiteral;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.SolicitudImpugnacionEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudImpugnacionRepository;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Types;

@Repository
public class SolicitudImpugnacionRepositoryImpl extends JdbcDaoSupport implements SolicitudImpugnacionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AnexoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }
    
/**
     * @autor: Miguel F. [25-08-2021]
     * @modificado:
     * @descripción: {Solicitudes vencidas}
     *
     */
    @Override
    public ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> listarArchivoSolicitudImpugnacion(SolicitudImpugnacionArchivo2Entity param){

        return null;
        /*
        ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> result = new ResultClassEntity<>(); 
        List<SolicitudImpugnacionArchivo2Entity> list = new ArrayList<>(); 
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_SolicitudInpugnacionArchivo_obtener(?)}");
            pstm.setObject (1, param.getIdSolicitudImpugnacion(),Types.INTEGER);
           
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            while (rs.next()) {
                SolicitudImpugnacionArchivo2Entity temp = new SolicitudImpugnacionArchivo2Entity();
                temp.setIdArchivo(rs.getInt("NU_ID_ARCHIVO"));
                temp.setValorPrimario(rs.getString("TX_VALORPRIMARIO"));
                temp.setTipoParametro(rs.getString("TX_TIPO_PARAMETRO")); 
            
                list.add(temp);
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("ejecutado correctamente");
        }catch (Exception e) {
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("Ocurrió un problema en la ejecución");
            log.error("SolicitudImpugnacionRepositoryImpl - SolicitudImpugnacionActualizar", e.getMessage()); 
        }
        return result;
        */
    }

    /**
     * @autor: Miguel F. [25-08-2021]
     * @modificado:
     * @descripción: {Solicitudes vencidas}
     *
     */
    @Override
    public ResultClassEntity<List<Solicitud2Entity>> listarSolicitudesVencidas(){
        return null;
        /*
        ResultClassEntity<List<Solicitud2Entity>> result = new ResultClassEntity<>(); 
        List<Solicitud2Entity> list = new ArrayList<>(); 
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Solicitud_Vencidas()}");
           
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            while (rs.next()) {
                Solicitud2Entity temp = new Solicitud2Entity();
                temp.setIdSolicitud(rs.getInt("NU_ID_SOLICITUD"));
                temp.setMotivoSolicitud(rs.getString("TX_MOTIVO_SOLICITUD"));
                temp.setMotivoPostulacion(rs.getString("TX_MOTIVO_POSTULACION")); 
            
                list.add(temp);
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("ejecutado correctamente");
        }catch (Exception e) {
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("Ocurrió un problema en la ejecución");
            log.error("SolicitudImpugnacionRepositoryImpl - SolicitudImpugnacionActualizar", e.getMessage()); 
        }
        return result;
        */
    }

    /**
     * @autor: Miguel F. [23-08-2021]
     * @modificado:
     * @descripción: {Actualizar impugnaciones}
     *
     */
    @Override
    public ResultClassEntity<Boolean> SolicitudImpugnacionActualizar(SolicitudImpugnacionEntity params){
        return null;
        /*
        ResultClassEntity<Boolean> result = new ResultClassEntity<>(); 
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_SolicitudImpugnacion_Actualizar (?,?,?,?,?,?,?,?)}");
            pstm.setObject (1, params.getIdSolicitudImpugnacion(),Types.INTEGER);
            pstm.setObject (2, params.getHabilitado(),Types.BIT);
            pstm.setObject (3, params.getProcede(),Types.BIT);

            pstm.setObject (4, params.getDescripcion(),Types.VARCHAR);
            pstm.setObject (5, params.getObservacion(),Types.VARCHAR);
            
            // pstm.setObject (6, params.getIdDocumentoAdjunto(),Types.INTEGER); // idarchivo
            // pstm.setObject (7, params.getIdArchivoSan(),Types.INTEGER);
            // pstm.setObject (8, params.getIdArchivoInforme(),Types.INTEGER);
            
            pstm.execute();
            result.setData(true);
        } catch (Exception e) {
            log.error("SolicitudImpugnacionRepositoryImpl - SolicitudImpugnacionActualizar", e.getMessage()); 
        }
        return result;
        */
    }
    

    

    /**
     * @autor: Roberto Meiggs. [13-08-2021]
     * @modificado:
     * @descripción: {obtener lista solicitudes vencidas}
     *
     */
    @Override
    public ResultClassEntity<List<SolicitudImpugnacionEntity>> ListaSolicitudesVencidasObtener() throws SQLException {
        return null;
        /*
        ResultClassEntity<List<SolicitudImpugnacionEntity>> result = new ResultClassEntity<>();
        List<SolicitudImpugnacionEntity> list = new ArrayList<>();

        Connection con = null;
        PreparedStatement pstm = null;
        
        con = dataSource.getConnection();
        pstm = con.prepareCall("{call pa_SolicitudImpugnacion_listar()}");

        ResultSet rs = pstm.executeQuery();
        result.setData(null);
        while (rs.next()) {
            SolicitudImpugnacionEntity temp = new SolicitudImpugnacionEntity();
            
            temp.setIdSolicitud(rs.getInt("NU_ID_SOLICITUD_IMPUGNACION"));
            temp.setRutaArchivo(rs.getString("TX_RUTA"));
            temp.setCodigoTipoSolicitud(rs.getString("TX_CODIGO_TIPO_SOLICITUD")); 
            temp.setAsunto(rs.getString("TX_ASUNTO")); 
            //temp.setAuditoria(rs.getString("TX_AUDITORIA")); 
            temp.setFechaRegistro(rs.getDate("FE_FECHA_REGISTRO")); 
            temp.setFechaFin(rs.getDate("FE_FECHA_FIN")); 
            
            list.add(temp);
        }
        result.setData(list);
        result.setSuccess(true);
        result.setMessage("ListaSolicitudesVencidasObtener - obtener ");

        return result;
        */
    }
    
    @Override
    public ResultClassEntity<Boolean> SolicitudImpugnacionRegistrarActualizar(SolicitudImpugnacionEntity params){
        return null;
        /*
                ResultClassEntity<Boolean> result = new ResultClassEntity<>(); 
                try {
                    Connection con = null;
                    PreparedStatement pstm  = null;
                    con = dataSource.getConnection();
                    pstm = con.prepareCall("{call pa_SolicitudInpugnacion_RegistrarAcualizar (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                    pstm.setObject (1, params.getIdSolicitudImpugnacion(),Types.INTEGER);
                    //pstm.setObject (2, params.getIdDocumentoAdjunto(),Types.INTEGER);
                    pstm.setObject (2, params.getCodigoTipoSolicitud(),Types.VARCHAR);
                    pstm.setObject (3, params.getAsunto(),Types.VARCHAR);
                    pstm.setObject (4, params.getDescripcion(),Types.VARCHAR);
                    pstm.setObject (5, params.getObservacion(),Types.VARCHAR);
                    pstm.setObject (6, params.getIdUsuarioRegistro(),Types.INTEGER);
                    
                    pstm.setObject (7, params.getIdArchivoResultadoEvaluacion(),Types.INTEGER);  
                    pstm.setObject (8, params.getIdArchivoResolucion(),Types.INTEGER); 
                    pstm.setObject (9, params.getIdArchivoRecursoImpugnacion(),Types.INTEGER); 

                    pstm.setObject (10, params.getCalificacionImpugnacion(),Types.VARCHAR); // 
                    pstm.setObject (11, params.getFechaPresentacion(),Types.DATE);
                    pstm.setObject (12, params.getFechaInicio(),Types.DATE);
                    pstm.setObject (13, params.getFechaFin(),Types.DATE);

                    pstm.setObject (14, params.getFundadoInfundado(),Types.BOOLEAN);
                    pstm.setObject (15, params.getRemitidoARFFS(),Types.BOOLEAN); 
                    pstm.setObject (16, params.getRemitidoResolucion(),Types.BOOLEAN);

                    pstm.setObject (17, params.getHabilitado(),Types.BOOLEAN);
                    pstm.setObject (18, params.getProcede(),Types.BOOLEAN);
                    pstm.setObject (19, params.getBandeja(),Types.VARCHAR);

                    pstm.execute();
                    result.setData(true); 

                    
                } catch (Exception e) {
                    log.error("SolicitudImpugnacionRepositoryImpl - SolicitudImpugnacionRegistrar", e.getMessage()); 
                    
                }
                return result;

                */
    }
    
    @Override
    public ResultClassEntity SolicitudImpugnacionObtener(SolicitudImpugnacionEntity solicitudImpugnacionEntity) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudImpugnacion_Obtener");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.setParameter("idSolicitud", solicitudImpugnacionEntity.getIdSolicitud());
            processStored.execute();
            Object[] objArr = (Object[]) processStored.getSingleResult();
            solicitudImpugnacionEntity.setIdSolicitud((Integer)objArr[0]);
            solicitudImpugnacionEntity.setIdSolicitudImpugnacion((Integer)objArr[0]);
            solicitudImpugnacionEntity.setCalificacionImpugnacion((String)objArr[1]);
            solicitudImpugnacionEntity.setCodigoTipoSolicitud((String) objArr[2]);
            solicitudImpugnacionEntity.setAsunto((String) objArr[3]);
            solicitudImpugnacionEntity.setDescripcion((String) objArr[4]);
            solicitudImpugnacionEntity.setObservacion((String) objArr[5]);
            solicitudImpugnacionEntity.setFechaPresentacion((Date) objArr[6]);
            solicitudImpugnacionEntity.setFechaFin((Date) objArr[7]);
            solicitudImpugnacionEntity.setFundadoInfundado((Boolean) objArr[8]);
            solicitudImpugnacionEntity.setProcede((Boolean) objArr[9]);
            solicitudImpugnacionEntity.setRemitidoARFFS((Boolean) objArr[10]);
            solicitudImpugnacionEntity.setRemitidoResolucion((Boolean) objArr[11]);
            solicitudImpugnacionEntity.setEstado((String)objArr[12]);
            solicitudImpugnacionEntity.setIdTipoSolicitud((Integer) objArr[13]);
            solicitudImpugnacionEntity.setIdProcesoPostulacion((Integer) objArr[14]);
            solicitudImpugnacionEntity.setIdArchivoRecursoImpugnacion((Integer) objArr[15]);
            solicitudImpugnacionEntity.setIdArchivoResultadoEvaluacion((Integer) objArr[16]);
            solicitudImpugnacionEntity.setIdArchivoResolucion((Integer) objArr[17]);

            result.setData(solicitudImpugnacionEntity);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la solicitud de impugnación correctamente");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity<List<SolicitudImpugnacionEntity>> solicitudImpugnacionListar(
            SolicitudImpugnacionEntity param) {
        
          return null;
        /*
        ResultClassEntity<List<SolicitudImpugnacionEntity>> result = new ResultClassEntity<>();
                List<SolicitudImpugnacionEntity> list = new ArrayList<>(); 
                try {
                    Connection con = null;
                    PreparedStatement pstm  = null;
                    con = dataSource.getConnection();
                    pstm = con.prepareCall("{call pa_SolicitudImpugnacion_listar(?)}");
                    pstm.setObject (1, param.getBandeja(),Types.VARCHAR);
                   
                    ResultSet rs = pstm.executeQuery();
                    result.setData(null);
                    while (rs.next()) {
                        SolicitudImpugnacionEntity temp = new SolicitudImpugnacionEntity();
                        temp.setCodigoTipoSolicitud(rs.getString("TX_CODIGO_TIPO_SOLICITUD"));
                        temp.setAsunto(rs.getString("TX_ASUNTO"));
                        temp.setFechaRegistro(rs.getDate("FE_FECHA_REGISTRO"));
                        temp.setFechaFin(rs.getDate("FE_FECHA_FIN")); 
                        temp.setFundadoInfundado(rs.getBoolean("NU_FUNDADO_INFUNDADO"));
                        temp.setHabilitado(rs.getBoolean("NU_HABILITADO"));
                        //NU_NUEVA
                        temp.setProcede(rs.getBoolean("NU_PROCEDE"));
                        temp.setRemitidoARFFS(rs.getBoolean("NU_REMITIDO_ARFFS"));
                        temp.setRemitidoResolucion(rs.getBoolean("NU_REMITIDO_RESOLUCION"));
                    
                        list.add(temp);
                    }
                    result.setData(list);
                    result.setSuccess(true);
                    result.setMessage("ejecutado correctamente");
                }catch (Exception e) {
                    result.setData(null);
                    result.setSuccess(false);
                    result.setMessage("Ocurrió un problema en la ejecución");
                    log.error("SolicitudImpugnacionRepositoryImpl - SolicitudImpugnacionActualizar", e.getMessage()); 
                }
                return result;
                */
    }
}
