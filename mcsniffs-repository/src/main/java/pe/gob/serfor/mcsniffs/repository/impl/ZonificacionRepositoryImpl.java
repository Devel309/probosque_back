package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Parametro.ZonificacionDTO;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ZonificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ZonaEntity;
import pe.gob.serfor.mcsniffs.entity.ZonaAnexoEntity;
import pe.gob.serfor.mcsniffs.repository.ZonificacionRepository;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Repository
public class ZonificacionRepositoryImpl extends JdbcDaoSupport implements ZonificacionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ZonificacionRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity ListarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Zonificacion_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", zonificacionEntity.getIdPlanManejo());
            processStored.execute();

            List<ZonificacionEntity> listZonificacionEntity = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
        if (spResult!=null){
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ZonificacionEntity zonificacionEntity1 = new ZonificacionEntity();
                    zonificacionEntity1.setIdZonificacion((Integer) row[0]);
                    zonificacionEntity1.setIdPlanManejo((Integer) row[1]);
                    zonificacionEntity1.setCodZonificacion((String) row[2]);
                    zonificacionEntity1.setZona((String) row[3]);
                    zonificacionEntity1.setAnexo1(((BigDecimal) row[4]).doubleValue());
                    zonificacionEntity1.setAnexo2(((BigDecimal) row[5]).doubleValue());
                    zonificacionEntity1.setAnexo3(((BigDecimal) row[6]).doubleValue());
                    zonificacionEntity1.setIdZonificacionPadre((Integer) row[7]);
                    zonificacionEntity1.setTotal(((BigDecimal) row[8]).doubleValue());
                    zonificacionEntity1.setPorcentaje(((BigDecimal) row[9]).doubleValue());
                    listZonificacionEntity.add(zonificacionEntity1);
                }
            }
        }


            result.setData(listZonificacionEntity);
            result.setSuccess(true);
            result.setMessage("Se listó la zonificación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarZona(ZonaEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Zona_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", item.getIdPlanManejo());
            processStored.execute();

            List<ZonaEntity> items = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ZonaEntity zona = new ZonaEntity();
                    zona.setIdZona((Integer) row[0]);
                    zona.setIdPlanManejo((Integer) row[1]);
                    zona.setCodigoZona((String) row[2]);
                    zona.setNombre((String) row[3]);
                    zona.setIdZonaPadre((Integer) row[4]);
                    zona.setTotal(((BigDecimal) row[5]).doubleValue());
                    zona.setPorcentaje(((BigDecimal) row[6]).doubleValue());
                    items.add(zona);
                }
            }

            result.setData(items);
            result.setSuccess(true);
            result.setMessage("Se listó la zonificación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity listarZonaAnexo(Integer idZona) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ZonaAnexo_Listar");
            processStored.registerStoredProcedureParameter("idZona", Integer.class, ParameterMode.IN);
            processStored.setParameter("idZona", idZona);
            processStored.execute();

            List<ZonaAnexoEntity> items = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ZonaAnexoEntity zonaAnexo = new ZonaAnexoEntity();
                    zonaAnexo.setNombre((String) row[0]);
                    zonaAnexo.setValor((String) row[1]);
                    zonaAnexo.setIdZonaAnexo((Integer) row[2]);
                    zonaAnexo.setIdZona((Integer) row[3]);
                    items.add(zonaAnexo);
                }
            }
            result.setData(items);
            result.setSuccess(true);
            result.setMessage("Se listó la zonificación correctamente.");
            return  result;

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    @Override
    public Integer registrarZona(ZonaEntity item) throws Exception {
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_Zona_upsert");
            processStored_.registerStoredProcedureParameter("idZona", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("codigoZona", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idZonaPadre", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("total", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("porcentaje", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            
            processStored_.setParameter("idZona", item.getIdZona());
            processStored_.setParameter("codigoZona", item.getCodigoZona());
            processStored_.setParameter("nombre", item.getNombre());
            processStored_.setParameter("idZonaPadre", item.getIdZonaPadre());
            processStored_.setParameter("total", item.getTotal());
            processStored_.setParameter("porcentaje", item.getPorcentaje());
            processStored_.setParameter("idPlanManejo", item.getIdPlanManejo());
            processStored_.setParameter("idUsuario", item.getIdUsuarioRegistro());
            processStored_.execute();
            if(processStored_.getSingleResult() != null){
                return Integer.parseInt(processStored_.getSingleResult().toString());
            }else{
                return null;
            }

        }catch (Exception e ){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Integer registrarZonaAnexo(ZonaAnexoEntity item) throws Exception {
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ZonaAnexo_upsert");
            processStored_.registerStoredProcedureParameter("idZonaAnexo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idZona", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("valor", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idZonaAnexo", item.getIdZonaAnexo());
            processStored_.setParameter("idZona", item.getIdZona());
            processStored_.setParameter("nombre", item.getNombre());
            processStored_.setParameter("valor", item.getValor());
            processStored_.setParameter("idUsuario", item.getIdUsuarioRegistro());
            processStored_.execute();

            if(processStored_.getSingleResult() != null){
                return Integer.parseInt(processStored_.getSingleResult().toString());
            }else{
                return null;
            }

        }catch (Exception e ){
            throw new Exception(e.getMessage());
        }
    }
    @Override
    public ResultClassEntity eliminarZona(ZonaEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_Zona_Eliminar");
            processStored_.registerStoredProcedureParameter("idZona", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idZona", item.getIdZona());
            processStored_.setParameter("idPlanManejo", item.getIdPlanManejo());
            processStored_.setParameter("idUsuario", item.getIdUsuarioElimina());
            processStored_.execute();

            result.setData(item);
            result.setSuccess(true);
            result.setMessage("Se eliminó la zonificación correctamente");
            return  result;
        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity RegistrarZonificacion(List<ZonificacionDTO> zonificacionDTOList) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            for (ZonificacionDTO zonificacionDTO : zonificacionDTOList) {
                ZonificacionEntity ze = new ZonificacionEntity();
                Integer idPadreTmp = 0;
                ze.setIdZonificacion(zonificacionDTO.getIdZonificacion());
                ze.setIdPlanManejo(zonificacionDTO.getIdPlanManejo());
                ze.setCodZonificacion(zonificacionDTO.getCodZonificacion());
                ze.setZona(zonificacionDTO.getZona());
                ze.setAnexo1(zonificacionDTO.getAnexo1());
                ze.setAnexo2(zonificacionDTO.getAnexo2());
                ze.setAnexo3(zonificacionDTO.getAnexo3());
                ze.setIdZonificacionPadre(zonificacionDTO.getIdZonificacionPadre());
                ze.setTotal(zonificacionDTO.getTotal());
                ze.setPorcentaje(zonificacionDTO.getPorcentaje());
                ze.setIdUsuarioRegistro(zonificacionDTO.getIdUsuarioRegistro());
                ze.setIdUsuarioModificacion(zonificacionDTO.getIdUsuarioModificacion());

                if( ze.getIdZonificacion() == 0 ){
                    //registrar
                    ZonificacionEntity zeSave = (ZonificacionEntity)RegistraZonificacion(ze).getData();
                    idPadreTmp = zeSave.getIdZonificacion();
                }else{
                    ZonificacionEntity zeUpdate = (ZonificacionEntity)ActualizarZonificacion(ze).getData();
                    idPadreTmp = ze.getIdZonificacion();
                }

                for (ZonificacionEntity ze1 : zonificacionDTO.getListSubzonas()) {
                    if(ze1.getIdZonificacion() == 0){
                        ze1.setIdZonificacionPadre(idPadreTmp);
                        ZonificacionEntity zeSaveHijo = (ZonificacionEntity)RegistraZonificacion(ze1).getData();
                    }else{
                        ze1.setIdZonificacionPadre(idPadreTmp);
                        ZonificacionEntity zeUpdateHijo = (ZonificacionEntity)ActualizarZonificacion(ze1).getData();
                    }
                }
            }

            result.setData(zonificacionDTOList);
            result.setSuccess(true);
            result.setMessage("Se registró la lista de zonificación");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistraZonificacion(ZonificacionEntity zonificacionEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_Zonificacion_Registrar");
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("codZonificacion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("zona", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("anexo1", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("anexo2", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("anexo3", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idZonificacionPadre", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("total", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("porcentaje", Double.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idPlanManejo", zonificacionEntity.getIdPlanManejo());
            processStored_.setParameter("codZonificacion", zonificacionEntity.getCodZonificacion());
            processStored_.setParameter("zona", zonificacionEntity.getZona());
            processStored_.setParameter("anexo1", zonificacionEntity.getAnexo1());
            processStored_.setParameter("anexo2", zonificacionEntity.getAnexo2());
            processStored_.setParameter("anexo3", zonificacionEntity.getAnexo3());
            processStored_.setParameter("idZonificacionPadre", zonificacionEntity.getIdZonificacionPadre());
            processStored_.setParameter("total", zonificacionEntity.getTotal());
            processStored_.setParameter("porcentaje", zonificacionEntity.getPorcentaje());
            processStored_.setParameter("idUsuarioRegistro", zonificacionEntity.getIdUsuarioRegistro());
            processStored_.execute();

            Object retorno2 = processStored_.getSingleResult();
            BigDecimal retornoIdentity = (BigDecimal) retorno2;
            zonificacionEntity.setIdZonificacion(retornoIdentity.intValue());

            result.setData(zonificacionEntity);
            result.setSuccess(true);
            result.setMessage("Se registró la zonificación");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity ActualizarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_Zonificacion_Actualizar");
                processStored_.registerStoredProcedureParameter("idZonificacion", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codZonificacion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("zona", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("anexo1", Double.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("anexo2", Double.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("anexo3", Double.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idZonificacionPadre", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("total", Double.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("porcentaje", Double.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

                processStored_.setParameter("idZonificacion", zonificacionEntity.getIdZonificacion());
                processStored_.setParameter("codZonificacion", zonificacionEntity.getCodZonificacion());
                processStored_.setParameter("zona", zonificacionEntity.getZona());
                processStored_.setParameter("anexo1", zonificacionEntity.getAnexo1());
                processStored_.setParameter("anexo2", zonificacionEntity.getAnexo2());
                processStored_.setParameter("anexo3", zonificacionEntity.getAnexo3());
                processStored_.setParameter("idZonificacionPadre", zonificacionEntity.getIdZonificacionPadre());
                processStored_.setParameter("total", zonificacionEntity.getTotal());
                processStored_.setParameter("porcentaje", zonificacionEntity.getPorcentaje());
                processStored_.setParameter("idUsuarioModificacion", zonificacionEntity.getIdUsuarioModificacion());
                processStored_.execute();

            result.setData(zonificacionEntity);
            result.setSuccess(true);
            result.setMessage("Se actualizó la lista de zonificación");
            return  result;
        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarZonificacion(ZonificacionEntity zonificacionEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_Zonificacion_Eliminar");
            processStored_.registerStoredProcedureParameter("idZonificacion", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idZonificacion", zonificacionEntity.getIdZonificacion());
            processStored_.setParameter("idUsuarioElimina", zonificacionEntity.getIdUsuarioElimina());
            processStored_.execute();

            result.setData(zonificacionEntity);
            result.setSuccess(true);
            result.setMessage("Se eliminó la zonificación correctamente");
            return  result;
        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

}
