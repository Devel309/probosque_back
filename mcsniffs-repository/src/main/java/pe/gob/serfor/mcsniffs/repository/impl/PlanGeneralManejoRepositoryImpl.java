package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.PlanGeneralManejoRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Repository
public class PlanGeneralManejoRepositoryImpl extends JdbcDaoSupport implements PlanGeneralManejoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanGeneralManejoRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    @Autowired
    private FileServerConexion fileServerConexion;
    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarResumenEjecutivoEscalaAlta(InformacionGeneralEntity param, MultipartFile file,Boolean accion) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneral_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEscala", short.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoPlan", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("duracion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("potencial", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored.setParameter("idSolicitud", param.getPlanManejo().getSolicitud().getIdSolicitud());
            processStored.setParameter("idTipoProceso", param.getPlanManejo().getTipoProceso().getIdTipoProceso());
            processStored.setParameter("tipoEscala", param.getPlanManejo().getTipoEscala().getTipoEscala());
            processStored.setParameter("idTipoPlan", param.getPlanManejo().getTipoPlan().getIdTipoPlan());
            processStored.setParameter("fechaPresentacion", param.getFechaPresentacion());
            processStored.setParameter("duracion", param.getDuracion());
            processStored.setParameter("fechaInicio", param.getFechaInicio());
            processStored.setParameter("fechaFin", param.getFechaFin());
            processStored.setParameter("potencial", param.getPotencial());
            processStored.setParameter("idPersona", param.getPersona().getIdPersona());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            ArchivoEntity archivo = new ArchivoEntity();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity pla = new PlanManejoEntity();
                    pla.setIdPlanManejo((Integer) row[0]);
                    param.setPlanManejo(pla);
                    param.setIdInfGeneral((Integer) row[1]);
                  if(accion){
                      String archivoGenerado=fileServerConexion.uploadFile(file);
                      archivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
                      archivo.setNombre(FilenameUtils.getBaseName(file.getOriginalFilename()));
                      archivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
                      archivo.setRuta(fileServerPath.concat(archivoGenerado));
                      archivo.setTipoDocumento("2");

                      StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneralArchivo_Registrar");
                      processStored_.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                      processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                      processStored_.setParameter("tipoDocumento", archivo.getTipoDocumento());
                      processStored_.setParameter("idPlanManejo",param.getPlanManejo().getIdPlanManejo());
                      processStored_.setParameter("ruta", archivo.getRuta());
                      processStored_.setParameter("nombre", archivo.getNombre());
                      processStored_.setParameter("nombreGenerado", archivo.getNombreGenerado());
                      processStored_.setParameter("extension", archivo.getExtension());
                      processStored_.setParameter("estado", param.getEstado());
                      processStored_.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                      processStored_.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
                      processStored_.execute();
                  }

                }
            }

            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró Resumen Ejecutivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarResumenEjecutivoEscalaAlta(InformacionGeneralEntity param, MultipartFile file,Boolean accion) {

        ResultClassEntity result = new ResultClassEntity();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneral_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("duracion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("potencial", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("fechaPresentacion", param.getFechaPresentacion());
            processStored.setParameter("duracion", param.getDuracion());
            processStored.setParameter("fechaInicio", param.getFechaInicio());
            processStored.setParameter("fechaFin", param.getFechaFin());
            processStored.setParameter("potencial", param.getPotencial());
            processStored.setParameter("idPersona", param.getPersona().getIdPersona());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setData(param);
            ArchivoEntity archivo = new ArchivoEntity();
            if(accion){
                String archivoGenerado=fileServerConexion.uploadFile(file);
                archivo.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
                archivo.setNombre(FilenameUtils.getBaseName(file.getOriginalFilename()));
                archivo.setExtension(FilenameUtils.getExtension(archivoGenerado));
                archivo.setRuta(fileServerPath.concat(archivoGenerado));
                archivo.setTipoDocumento("2");

                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneralArchivo_Registrar");
                processStored_.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                processStored_.setParameter("tipoDocumento", archivo.getTipoDocumento());
                processStored_.setParameter("idPlanManejo",param.getPlanManejo().getIdPlanManejo());
                processStored_.setParameter("ruta", archivo.getRuta());
                processStored_.setParameter("nombre", archivo.getNombre());
                processStored_.setParameter("nombreGenerado", archivo.getNombreGenerado());
                processStored_.setParameter("extension", archivo.getExtension());
                processStored_.setParameter("estado", param.getEstado());
                processStored_.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                processStored_.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
                processStored_.execute();
            }

            result.setSuccess(true);
            result.setMessage("Se actualizó Resumen Ejecutivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ObtenerResumenEjecutivoEscalaAlta(InformacionGeneralEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneral_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            InformacionGeneralEntity informacionGeneral = new InformacionGeneralEntity();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity pla = new PlanManejoEntity();



                    SolicitudEntity sol = new SolicitudEntity();
                    TipoProcesoEntity tipoProc = new TipoProcesoEntity();
                    TipoPlanEntity tipoPlan = new TipoPlanEntity();
                    TipoEscalaEntity tipoEsc = new TipoEscalaEntity();
                    RegenteComunidadEntity regComun = new RegenteComunidadEntity();

                    PersonaEntity persona = new PersonaEntity();
                    pla.setIdPlanManejo((Integer) row[0]);
                    pla.setDescripcion((String) row[1]);
                    sol.setIdSolicitud((Integer) row[2]);
                    pla.setSolicitud(sol);
                    tipoProc.setIdTipoProceso((Integer) row[3]);
                    pla.setTipoProceso(tipoProc);
                    tipoPlan.setIdTipoPlan((Integer) row[4]);
                    pla.setTipoPlan(tipoPlan);
                    tipoEsc.setTipoEscala((short) row[5]);
                    pla.setTipoEscala((tipoEsc));
                    persona.setIdPersona((Integer) row[6]);
                    informacionGeneral.setPersona(persona);
                    informacionGeneral.setFechaPresentacion((Date)row[7]);
                    informacionGeneral.setDuracion((Integer) row[8]);
                    informacionGeneral.setFechaInicio((Date)row[9]);
                    informacionGeneral.setFechaFin((Date)row[10]);
                    double potencial =((BigDecimal) row[11]).doubleValue();
                    informacionGeneral.setPotencial((Double) potencial);
                    informacionGeneral.setNombreArchivo((String) row[12]);
                    informacionGeneral.setPlanManejo(pla);
                }
            }

            result.setData(informacionGeneral);
            result.setSuccess(true);
            result.setMessage("Se obtuvo Resumen Ejecutivo correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity ActualizarDuracionResumenEjecutivo(InformacionGeneralEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_InformacionGeneral_ActualizarDuracion");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("fechaInicio", param.getFechaInicio());
            processStored.setParameter("fechaFin", param.getFechaFin());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se actualizó Duración y Revisión del Plan correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity ActualizarAspectoComplementarioPlanManejo(PlanManejoEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoAspectoComplementario_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("aspectoComplementario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("aspectoComplementario", param.getAspectoComplementario());
            processStored.setParameter("estado", param.getEstado());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró el Aspecto Complementario correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity ObtenerPlanManejo(PlanManejoEntity param) {

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejo_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            PlanManejoEntity pla = new PlanManejoEntity();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    pla.setIdPlanManejo((Integer) row[0]);
                    pla.setAspectoComplementario((String) row[1]);


                }
            }
            result.setData(pla);
            result.setSuccess(true);
            result.setMessage("Se obtuvo Aspecto Complementario correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}