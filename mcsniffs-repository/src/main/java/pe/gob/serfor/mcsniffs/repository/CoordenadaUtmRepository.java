package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.CoordenadaUtmEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface CoordenadaUtmRepository {

    ResultClassEntity RegistrarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception;
    ResultClassEntity ActualizarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception;
    ResultClassEntity EliminarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception;
    ResultClassEntity ListarCoordenadaUtm(CoordenadaUtmEntity coordenadaUtmEntity) throws Exception;
}
