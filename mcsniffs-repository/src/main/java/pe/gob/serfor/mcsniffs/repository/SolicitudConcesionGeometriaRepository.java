package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionGeometriaEntity;

public interface SolicitudConcesionGeometriaRepository {
    ResultClassEntity registrarSolicitudConcesionGeometria(SolicitudConcesionGeometriaEntity item) throws Exception;
    ResultClassEntity listarSolicitudConcesionGeometria (SolicitudConcesionGeometriaEntity item);
    ResultClassEntity actualizarSolicitudConcesionGeometria (SolicitudConcesionGeometriaEntity item) throws Exception;
    ResultClassEntity eliminarSolicitudConcesionGeometriaArchivo(SolicitudConcesionGeometriaEntity item);
}
