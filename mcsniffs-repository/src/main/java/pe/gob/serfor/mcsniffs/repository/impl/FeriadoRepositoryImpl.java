package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.FeriadoRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class FeriadoRepositoryImpl extends JdbcDaoSupport implements FeriadoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(CensoForestalDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<FeriadoEntity> ListarFeriados(String codUbigeo) throws Exception {
        List<FeriadoEntity> lista = new ArrayList<FeriadoEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_GEFeriado_Listar");
            processStored.registerStoredProcedureParameter("codUbigeo", String.class, ParameterMode.IN);
            processStored.setParameter("codUbigeo", codUbigeo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    FeriadoEntity temp = new FeriadoEntity();
                    temp.setCodUbigeo((String) row[0]);
                    temp.setIdFeriado((Integer) row[1]);
                    temp.setFechFeriado((Timestamp) row[2]);
                    temp.setDescFeriado((String) row[3]);
                    temp.setNumFeriado(((Integer) row[4]));
                    temp.setNumAnio(((Integer) row[5]));
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("FeriadoRepositoryImpl - Lista los Feriados", e.getMessage());
            throw e;
        }

    }


    @Override
    public ResultClassEntity RegistrarFeriados(FeriadoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_GEFeriado_Registrar]");
        processStored.registerStoredProcedureParameter("idFeriado", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("fechFeriado", Timestamp.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("descFeriado", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codUbigeo", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("numFeriado", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("numAnio", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

        processStored.setParameter("idFeriado", param.getIdFeriado());
        processStored.setParameter("fechFeriado", param.getFechFeriado());
        processStored.setParameter("descFeriado", param.getDescFeriado());
        processStored.setParameter("codUbigeo", param.getCodUbigeo());
        processStored.setParameter("numFeriado", param.getNumFeriado());
        processStored.setParameter("numAnio", param.getNumAnio());
        processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se registró el feriado ");
        return result;
    }

}
