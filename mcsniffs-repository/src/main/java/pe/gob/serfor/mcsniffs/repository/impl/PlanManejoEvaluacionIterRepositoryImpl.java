package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionIterRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

/**
 * @autor: Renzo Gabriel Meneses Gamarra [24-09-2021]
 * @modificado:
 * @descripción: {Repositorio para Plan Manejo Evaluacion}
 *
 */
@Repository
public class PlanManejoEvaluacionIterRepositoryImpl extends JdbcDaoSupport implements PlanManejoEvaluacionIterRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanManejoEvaluacionIterRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultEntity<PlanManejoEvaluacionIterDto> listarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) {
        ResultEntity result = new ResultEntity();
        List<PlanManejoEvaluacionIterDto> resultDet = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionIteraccion_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacionIter", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoPlanManejoEvaluacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("titularRegente", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
 
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", planManejoEvaluacionIterDto.getIdPlanManejo());
            sp.setParameter("idPlanManejoEvaluacionIter", planManejoEvaluacionIterDto.getIdPlanManejoEvaluacionIter());
            sp.setParameter("estadoPlanManejoEvaluacion", planManejoEvaluacionIterDto.getEstadoPlanManejoEvaluacion());
            sp.setParameter("titularRegente", planManejoEvaluacionIterDto.getTitularRegente());
            sp.setParameter("fechaPresentacion", planManejoEvaluacionIterDto.getFechaPresentacion());
            sp.setParameter("P_PAGENUM", planManejoEvaluacionIterDto.getPageNum());
            sp.setParameter("P_PAGESIZE", planManejoEvaluacionIterDto.getPageSize());
            sp.execute();

            List<Object[]> spResultDet = sp.getResultList();

            

            if (spResultDet!= null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                PlanManejoEvaluacionIterDto temp = null;
                for (Object[] item: spResultDet) {
                    temp = new PlanManejoEvaluacionIterDto();
                    temp.setIdPlanManejoEvaluacionIter(item[0]==null ? null : (Integer) item[0]);
                    temp.setIdPlanManejo(item[1]==null ? null :(Integer) item[1]);
                    temp.setIdTitularRegente(item[2]==null ? null :(Integer) item[2]);
                    temp.setTitularRegente(item[3]==null ? null :(String) item[3]);
                    temp.setFechaPresentacion(item[4]==null ? null :(Date) item[4]);
                    temp.setEstadoPlanManejoEvaluacion(item[5]==null ? null :(String) item[5]);
                    temp.setDescEstadoPlanManejoEvaluacion(item[6]==null ? null :(String) item[6]);
                    result.setTotalrecord(item[7]==null ? null :(Integer) item[7]);
                    resultDet.add(temp);
                }
            }

            result.setData(resultDet);
            result.setMessage((resultDet.size()!=0?"Información encontrada":"Informacion no encontrada"));
            result.setIsSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            result.setIsSuccess((resultDet.size()!=0?true:false));
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionIteraccion_Registrar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTitularRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("titularRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoPlanManejoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejo", planManejoEvaluacionIterDto.getIdPlanManejo());
            processStored.setParameter("idTitularRegente", planManejoEvaluacionIterDto.getIdTitularRegente());
            processStored.setParameter("titularRegente", planManejoEvaluacionIterDto.getTitularRegente());
            processStored.setParameter("fechaPresentacion", planManejoEvaluacionIterDto.getFechaPresentacion());
            processStored.setParameter("estadoPlanManejoEvaluacion", planManejoEvaluacionIterDto.getEstadoPlanManejoEvaluacion());
            processStored.setParameter("idUsuarioRegistro", planManejoEvaluacionIterDto.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró el plan manejo evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionIteraccion_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacionIter", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoPlanManejoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTitularRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("titularRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejoEvaluacionIter", planManejoEvaluacionIterDto.getIdPlanManejoEvaluacionIter());
            processStored.setParameter("idPlanManejo", planManejoEvaluacionIterDto.getIdPlanManejo());
            processStored.setParameter("estadoPlanManejoEvaluacion", planManejoEvaluacionIterDto.getEstadoPlanManejoEvaluacion());
            processStored.setParameter("idTitularRegente", planManejoEvaluacionIterDto.getIdTitularRegente());
            processStored.setParameter("titularRegente", planManejoEvaluacionIterDto.getTitularRegente());
            processStored.setParameter("fechaPresentacion", planManejoEvaluacionIterDto.getFechaPresentacion());
            processStored.setParameter("idUsuarioModificacion", planManejoEvaluacionIterDto.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó el plan manejo evaluación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarPlanManejoEvaluacionIter(PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionIteraccion_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacionIter", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejoEvaluacionIter", planManejoEvaluacionIterDto.getIdPlanManejoEvaluacionIter());
            processStored.setParameter("idUsuarioElimina", planManejoEvaluacionIterDto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

}
