package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.CensoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.DatosTHDto;

import java.sql.SQLException;

public interface CensoForestalDetalleRepository {
    ResultClassEntity RegistrarCensoForestalDetalle(CensoForestalDetalleEntity censoForestalEntity) throws Exception;
    ResultClassEntity actualizarCensoForestalDetalle(CensoForestalDetalleEntity param,Integer idCensoForestalDetalle) throws Exception;
    ResultClassEntity eliminarCensoForestalDetalle(Integer idUsuarioElimina,Integer idCensoForestalDetalle) throws Exception;
    ResultClassEntity listarRecursosMaderablesCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception;
    ResultClassEntity listarRecursosDiferentesCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception;
    ResultClassEntity listarCensoForestalDetalleAnexo(CensoForestalDetalleEntity request) throws Exception;
    ResultClassEntity listarSincronizacionCensoForestalDetalle(CensoForestalDetalleEntity request) throws Exception;
}
