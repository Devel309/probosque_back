package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;

import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoOfertaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ProcesoOfertaRepositoryImpl extends JdbcDaoSupport implements ProcesoOfertaRepository {
    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Servicio de Proceso oferta en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Guarda e proceso en oferta}
     * @param:consulta
     */
    @Override
    public ResultClassEntity guardarProcesoOferta(ProcesoOfertaEntity obj) throws Exception{
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ProcesoOferta_insertar");
            sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("agrupada", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asociada", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoGrupo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idStatusProceso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProOferta", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idTipoProceso", obj.getIdTipoProceso());
            sp.setParameter("agrupada", obj.getAgrupada());
            sp.setParameter("asociada", obj.getAsociada());
            sp.setParameter("codigoGrupo", obj.getCodigoGrupo());
            sp.setParameter("idStatusProceso", obj.getIdStatusProceso());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.execute();
            Integer idProOferta = (Integer) sp.getOutputParameterValue("idProOferta");
            result.setCodigo(idProOferta);
            result.setSuccess(true);
            result.setMessage("Se inició el proceso.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoOfertaRepositoryImpl - guardarProcesoOferta", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [15-06-2021]
     * @modificado:
     * @descripción: {Lista los procesos en oferta}
     * @param:ProcesoOfertaRequestEntity
     */
    @Override
    public ResultClassEntity listarProcesoOferta(ProcesoOfertaRequestEntity obj) {
        ResultClassEntity result = new ResultClassEntity();

        try {

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoOferta_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idStatusProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("agrupada", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioConsulta", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoOferta", obj.getIdProcesoOferta());
            processStored.setParameter("idTipoProceso", obj.getIdTipoProceso());
            processStored.setParameter("idStatusProceso", obj.getIdStatusProceso());
            processStored.setParameter("agrupada", obj.getAgrupada());
            processStored.setParameter("pageNum", obj.getPageNum());
            processStored.setParameter("pageSize", obj.getPageSize());
            processStored.setParameter("idUsuarioConsulta", obj.getIdUsuarioConsulta());
            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();
            List<ProcesoOfertaEntity>lstproceso=new ArrayList<>();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    ProcesoOfertaEntity po = new ProcesoOfertaEntity();
                    po.setIdProcesoOferta((Integer) row[11]);
                    po.setIdTipoProceso((Integer) row[12]);
                    po.setNombreTipoProceso((String) row[13]);
                    po.setIdUnidadAprovechamientoTexto((String) row[14]);

                    po.setUbicacionUnidadAprovechamiento((String) row[15]);
                    po.setSuperficieUnidadAprovechamiento((String) row[16]);
                    po.setAgrupada((Boolean) row[17]);
                    po.setAgrupadaTexto((String) row[18]);
                    po.setAsociada((Boolean) row[19]);
                    po.setAsociadaTexto((String) row[20]);
                    po.setCodigoGrupo((String) row[21]);
                    po.setIdStatusProceso((Integer) row[22]);
                    po.setNombreStatusProceso((String) row[23]);
                    po.setRazonSocial((String) row[24]);
                    po.setNombrePersona((String) row[25]);
                    po.setNumeroDocumento((String) row[26]);
                    po.setNombreRegente((String) row[27]);
                    po.setFechaPostulacion((Date) row[28]);
                    result.setTotalRecord((Integer) row[30]);
                    lstproceso.add(po);
                }
            }

            result.setData(lstproceso);
            result.setSuccess(true);
            result.setMessage("Se listaron los procesos en oferta correctamente.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoOferta - listarProcesoOferta", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [15-06-2021]
     * @modificado:
     * @descripción: {Actualiza el estatus del proceso en oferta}
     * @param:ProcesoOfertaRequestEntity
     */
    @Override
    public ResultClassEntity actualizarEsttatusProcesoOferta(Integer IdProcesoOferta, Integer IdEstatus, Integer IdUsuarioModificacion){

        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoOferta_actualizarestatus");
            processStored.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEstatus", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idProcesoOferta", IdProcesoOferta);
            processStored.setParameter("idEstatus", IdEstatus);
            processStored.setParameter("idUsuarioModificacion", IdUsuarioModificacion);
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó el proceso de oferta.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MAD_PROCESO_OFERTA,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Registrar.T_MAD_PROCESO_OFERTA + IdProcesoOferta,
                    IdUsuarioModificacion);

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Julio Meza [16-11-2021]
     * @modificado:
     * @descripción: {Elimina un Proceso de oferta, tb elimina el registro de las uas seleccionadas}
     * @param:ProcesoOfertaRequestEntity
     */
    @Override
    public ResultClassEntity eliminarProcesoOferta(ProcesoOfertaRequestEntity procesoOfertaRequestEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ProcesoOferta_Eliminar");
            processStored.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idProcesoOferta", procesoOfertaRequestEntity.getIdProcesoOferta());
            processStored.setParameter("idUsuarioElimina", procesoOfertaRequestEntity.getIdUsuarioElimina());
            processStored.execute();
            result.setData(procesoOfertaRequestEntity);
            result.setSuccess(true);
            result.setMessage("Se eliminó el Proceso de Oferta correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }


}
