package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Anexo1PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo2PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo3PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo4PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo5PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo6PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.PostulacionPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface PostulacionPFDMRepository {
    ResultClassEntity guardaPostulacion(PostulacionPFDMResquestEntity postulacion);
    ResultClassEntity<Anexo1PFDMEntity> obtenerAnexo1PFDM(AnexosPFDMResquestEntity filtro);
    ResultEntity<AnexosPFDMEntity> obtenerAnexoPFDM(String codigoAnexo, Integer IdPostulacionPFDM, Integer IdUsuarioPostulacion);
    ResultClassEntity AdjuntarArchivosPostulacionPFDM(AnexosPFDMAdjuntosEntity obj); 
    ResultClassEntity guardarAnexo2(Anexo2PFDMEntity anexo);
    ResultClassEntity guardarAnexo3(Anexo3PFDMEntity anexo);
    ResultClassEntity<Anexo2PFDMEntity> obtenerAnexo2PFDM(AnexosPFDMResquestEntity filtro);
    ResultClassEntity<Anexo3PFDMEntity> obtenerAnexo3PFDM(AnexosPFDMResquestEntity filtro);
    ResultClassEntity guardarAnexo4(Anexo4PFDMEntity anexo);
    ResultClassEntity<Anexo4PFDMEntity> obtenerAnexo4PFDM(AnexosPFDMResquestEntity filtro);
    ResultClassEntity guardarAnexo5(Anexo5PFDMEntity anexo);
    ResultClassEntity<Anexo5PFDMEntity> obtenerAnexo5PFDM(AnexosPFDMResquestEntity filtro);
    ResultClassEntity guardarAnexo6(Anexo6PFDMEntity anexo);
    ResultClassEntity<Anexo6PFDMEntity> obtenerAnexo6PFDM(AnexosPFDMResquestEntity filtro);
}
