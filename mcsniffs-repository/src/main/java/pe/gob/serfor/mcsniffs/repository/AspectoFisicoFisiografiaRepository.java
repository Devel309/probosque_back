package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.AccesibilidadManejoMatrizDto;

import java.util.List;

public interface AspectoFisicoFisiografiaRepository {

    ResultClassEntity ListarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception;
    // el registrar internamente, actualiza o registra a nivel de sp, si se le manda el id
    ResultClassEntity RegistrarAspectoFisicoFisiografia(List<AspectoFisicoFisiografiaEntity> listAspectoFisicoFisiografiaEntity) throws Exception;
    ResultClassEntity EliminarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception;
    ResultClassEntity ConfigurarAspectoFisicoFisiografia(AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity) throws Exception;

}
