package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.repository.PlanManejoRepository;
import pe.gob.serfor.mcsniffs.repository.RequisitoEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class RequisitoEvaluacionRepositoryImpl extends JdbcDaoSupport implements RequisitoEvaluacionRepository {
    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {Repository de los requisitos de las evaluaciones}
     *
     */
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Autowired
    PlanManejoRepository plan;

    private static final Logger log = LogManager.getLogger(RequisitoEvaluacionRepository.class);

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {registra los requisitos de las evaluaciones}
     * @param:RequisitoEvaluacionEntity
     */
    @Override
    public ResultClassEntity<List<RequisitoEvaluacionEntity>> registrarRequisito(List<RequisitoEvaluacionEntity> requisito) {
        ResultClassEntity<List<RequisitoEvaluacionEntity>> result = new ResultClassEntity<List<RequisitoEvaluacionEntity>>();
        List<RequisitoEvaluacionEntity> requisitosalida = new ArrayList<>();
        try {
            for (RequisitoEvaluacionEntity obj : requisito) {
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Requisito_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoRequisito", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoRequisito", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("titular", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("regente", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("titularHabilitado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("regenteHabilitado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("evaluacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("comentarios", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("comunidad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("titularConsecion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tituloHabilitante", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("ruc", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("partidaRegistral", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("area", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idRequisito", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroDocumentoRegente", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroDocumentoTitular", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroDocumentoComunidad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPlanManejo", obj.getIdPlanManejo());
                processStored.setParameter("codigoRequisito", obj.getCodigoRequisito());
                processStored.setParameter("tipoRequisito", obj.getTipoRequisito());
                processStored.setParameter("descripcion", obj.getDescripcion());
                processStored.setParameter("observacion", obj.getObservacion());
                processStored.setParameter("detalle", obj.getDetalle());
                processStored.setParameter("titular", obj.getTitular());
                processStored.setParameter("regente", obj.getRegente());
                processStored.setParameter("titularHabilitado", obj.getTitularHabilitado());
                processStored.setParameter("regenteHabilitado", obj.getRegenteHabilitado());
                processStored.setParameter("evaluacion", obj.getEvaluacion());
                processStored.setParameter("comentarios", obj.getComentarios());
                processStored.setParameter("comunidad", obj.getComunidad());
                processStored.setParameter("titularConsecion", obj.getTitularConsecion());
                processStored.setParameter("tituloHabilitante", obj.getTituloHabilitante());
                processStored.setParameter("ruc", obj.getRuc());
                processStored.setParameter("partidaRegistral", obj.getPartidaRegistral());
                processStored.setParameter("area", obj.getArea());
                processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
                processStored.setParameter("idRequisito", obj.getIdRequisito());
                processStored.setParameter("numeroDocumentoRegente", obj.getNumeroDocumentoRegente());
                processStored.setParameter("numeroDocumentoTitular", obj.getNumeroDocumentoTitular());
                processStored.setParameter("numeroDocumentoComunidad", obj.getNumeroDocumentoComunidad());
                processStored.setParameter("numeroDocumento", obj.getNumeroDocumento());
                processStored.execute();
                List<Object[]> spResult = processStored.getResultList();
                if(spResult!=null){
                    if (spResult.size() >= 1){
                        for (Object[] row : spResult) {
                            obj.setIdRequisito((Integer) row[0]);
                        }
                        if(obj.getEvaluacionDetalle()!=null){
                            if(obj.getEvaluacionDetalle().size() >= 1 && obj.getIdRequisito()!=null){
                                for (RequisitoEvaluacionDetalleEntity detalle : obj.getEvaluacionDetalle()) {
                                    detalle.setIdRequisito(obj.getIdRequisito());
                                    ResultClassEntity<RequisitoEvaluacionDetalleEntity> resultDetalle= registrarRequisitoDetalle(detalle);
                                    detalle.setIdRequisitoDet(resultDetalle.getData().getIdRequisitoDet());
                                    result.setInformacion(resultDetalle.getMessage());
                                }
                            }
                        }
                    }
                    requisitosalida.add(obj);
                }
            }
            if(requisito!=null){
                Boolean completo = requisito.stream().filter(o -> !(o.getEvaluacion().equals("EPRECOMP"))).findFirst().isPresent();
                if(!completo){
                    PlanManejoDto plandto= new PlanManejoDto();
                    plandto.setIdPlanManejo(requisito.get(0).getIdPlanManejo());
                    plandto.setCodigoEstado("EPRECOMP");
                    plandto.setIdUsuarioModificacion(requisito.get(0).getIdUsuarioRegistro());
                    ResultClassEntity actualizar= plan.actualizarPlanManejoEstado(plandto);
                    result.setInformacion(actualizar.getMessage());
                }else{
                    PlanManejoDto plandto= new PlanManejoDto();
                    plandto.setIdPlanManejo(requisito.get(0).getIdPlanManejo());
                    plandto.setCodigoEstado("EPREOBS");
                    plandto.setIdUsuarioModificacion(requisito.get(0).getIdUsuarioRegistro());
                    ResultClassEntity actualizar= plan.actualizarPlanManejoEstado(plandto);
                    result.setInformacion(actualizar.getMessage());
                }
            }
            result.setData(requisitosalida);
            result.setSuccess(true);
            result.setMessage("Se registraron los requisitos correctamente");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }     
    }

    public ResultClassEntity<RequisitoEvaluacionDetalleEntity> registrarRequisitoDetalle(RequisitoEvaluacionDetalleEntity requisito) {
        ResultClassEntity<RequisitoEvaluacionDetalleEntity> result = new ResultClassEntity<RequisitoEvaluacionDetalleEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RequisitoDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idRequisito", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoRequisitoDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medida", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conformidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaIni", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idRequisitoDet", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idRequisito", requisito.getIdRequisito());
            processStored.setParameter("codigoRequisitoDet", requisito.getCodigoRequisitoDet());
            processStored.setParameter("medida", requisito.getMedida());
            processStored.setParameter("conformidad", requisito.getConformidad());
            processStored.setParameter("descripcion", requisito.getDescripcion());
            processStored.setParameter("observacion", requisito.getObservacion());
            processStored.setParameter("detalle", requisito.getDetalle());
            processStored.setParameter("fechaIni", requisito.getFechaIni());
            processStored.setParameter("fechaFin", requisito.getFechaFin());
            processStored.setParameter("idUsuarioRegistro", requisito.getIdUsuarioRegistro());
            processStored.setParameter("idRequisitoDet", requisito.getIdRequisitoDet());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (!spResult.isEmpty()){
                    for (Object[] row : spResult) {
                        requisito.setIdRequisitoDet((Integer) row[0]);
                        result.setData(requisito);
                    }
                }
            }
            result.setSuccess(true);
            result.setMessage("Se registraron los requisitos detalle correctamente");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }     
    }

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {lista los requisitos de las evaluaciones}
     * @param:RequisitoEvaluacionEntity
     */
    @Override
    public ResultEntity<RequisitoEvaluacionEntity> listarRequisito(RequisitoEvaluacionEntity param) {
        ResultEntity<RequisitoEvaluacionEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_Requisito_listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoRequisito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoRequisito", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoRequisito", param.getCodigoRequisito());
            processStored.setParameter("tipoRequisito", param.getTipoRequisito());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<RequisitoEvaluacionEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    RequisitoEvaluacionEntity obj = new RequisitoEvaluacionEntity();
                    obj.setIdRequisito((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setCodigoRequisito((String) row[2]);
                    obj.setTipoRequisito((String) row[3]);
                    obj.setDescripcion((String) row[4]);
                    obj.setObservacion((String) row[5]);
                    obj.setDetalle((String) row[6]);
                    obj.setTitular((String) row[7]);
                    obj.setRegente((String) row[8]);
                    obj.setTitularHabilitado((String) row[9]);
                    obj.setRegenteHabilitado((String) row[10]);
                    obj.setEvaluacion((String) row[11]);
                    obj.setComentarios((String) row[12]);
                    obj.setComunidad((String) row[13]);
                    obj.setTitularConsecion((String) row[14]);
                    obj.setTituloHabilitante((String) row[15]);
                    obj.setRuc((String) row[16]);
                    obj.setPartidaRegistral((String) row[17]);
                    obj.setArea((String) row[18]);
                    obj.setNumeroDocumentoRegente((String) row[19]);
                    obj.setNumeroDocumentoTitular((String) row[20]);
                    obj.setNumeroDocumentoComunidad((String) row[21]);
                    obj.setNumeroDocumento((String) row[22]);
                    ResultEntity<RequisitoEvaluacionDetalleEntity> detalle = listarRequisitoDetalle(obj.getIdRequisito());
                    obj.setEvaluacionDetalle(detalle.getData());
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los documentos vigentes.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return result;
        }
    }

    public ResultEntity<RequisitoEvaluacionDetalleEntity> listarRequisitoDetalle(Integer idRequisito) {
        ResultEntity<RequisitoEvaluacionDetalleEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_RequisitoDetalle_listar");
            processStored.registerStoredProcedureParameter("idRequisito", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idRequisito", idRequisito);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<RequisitoEvaluacionDetalleEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    RequisitoEvaluacionDetalleEntity obj = new RequisitoEvaluacionDetalleEntity();
                    obj.setIdRequisitoDet((Integer) row[0]);
                    obj.setIdRequisito((Integer) row[1]);
                    obj.setCodigoRequisitoDet((String) row[2]);
                    obj.setMedida((String) row[3]);
                    obj.setConformidad((String) row[4]);
                    obj.setDescripcion((String) row[5]);
                    obj.setObservacion((String) row[6]);
                    obj.setDetalle((String) row[7]);
                    obj.setFechaIni(row[8]==null?null:(Date) row[8]);
                    obj.setFechaFin(row[9]==null?null:(Date) row[9]);
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los documentos vigentes.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {elimina los requisitos detalle de las evaluaciones}
     * @param:RequisitoEvaluacionDetalleEntity
     */
    @Override
    public ResultClassEntity<RequisitoEvaluacionDetalleEntity> eliminarRequisitoDetalle(
            RequisitoEvaluacionDetalleEntity param) {
       ResultClassEntity<RequisitoEvaluacionDetalleEntity> result = new ResultClassEntity<RequisitoEvaluacionDetalleEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RequisitoDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idRequisitoDet", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.setParameter("idRequisitoDet", param.getIdRequisitoDet());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó los requisitos detalle correctamente");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }
    }

    
}
