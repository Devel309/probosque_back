package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SolPlantacionForestalEvaluacionRepositoryImpl extends JdbcDaoSupport implements SolPlantacionForestalEvaluacionRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(SolPlantacionForestalEvaluacionRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudPlantacionForestalEvaluacion(SolPlantacionForestalEvaluacionDto dto){
        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalEvaluacionDto> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEvaluacion_Listar");
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestal", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantacionForestalEvaluacion", dto.getIdSolPlantacionForestalEvaluacion());
            processStored.setParameter("idSolPlantacionForestal", dto.getIdSolPlantacionForestal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            if (spResult != null && !spResult.isEmpty()) {
                SolPlantacionForestalEvaluacionDto c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalEvaluacionDto();
                        c.setIdSolPlantacionForestalEvaluacion(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolPlantacionForestal(row[1] == null ? null : (Integer) row[1]);
                        c.setStrFechaInicio(row[2] == null ? null : (String) row[2]);
                        c.setStrFechaFin(row[3] == null ? null : (String) row[3]);
                        c.setDiasEvaluacion(row[4] == null ? null : (Integer) row[4]);
                        c.setCodigoResultadoEvaluacion(row[5] == null ? null : (String) row[5]);
                        c.setObservacion(row[6] == null ? null : (String) row[6]);
                        c.setComunicadoEnviado(row[7] == null ? null : (Boolean) row[7]);
                        c.setIdArchivoCertificado(row[8] == null ? null : (Integer) row[8]);
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de evaluaciones de la solicitud de plantación forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudPlantacionForestalEvaluacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de evaluaciones de la solicitud de plantación forestal.");
            return result;
        }
    }

    @Override
    public ResultClassEntity listarSolicitudPlantacionForestalEvaluacionDetalle(SolPlantacionForestalEvaluacionDetalleDto detDto){
        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalEvaluacionDetalleDto> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEvaluacionDetalle_ListarFiltro");
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacionDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codSubSeccion", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantacionForestalEvaluacionDetalle", detDto.getIdSolPlantacionForestalEvaluacionDetalle());
            processStored.setParameter("idSolPlantacionForestalEvaluacion", detDto.getIdSolPlantacionForestalEvaluacion());
            processStored.setParameter("idSolPlantacionForestal", detDto.getIdSolPlantacionForestal());
            processStored.setParameter("codSeccion", detDto.getCodigoSeccion());
            processStored.setParameter("codSubSeccion", detDto.getCodigoSubSeccion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            if (spResult != null && !spResult.isEmpty()) {
                SolPlantacionForestalEvaluacionDetalleDto c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalEvaluacionDetalleDto();
                        c.setIdSolPlantacionForestalEvaluacionDetalle(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolPlantacionForestalEvaluacion(row[1] == null ? null : (Integer) row[1]);
                        c.setCodigoSeccion(row[2] == null ? null : (String) row[2]);
                        c.setCodigoSubSeccion(row[3] == null ? null : (String) row[3]);
                        c.setLineamiento(row[4] == null ? null : (Boolean) row[4]);
                        c.setCodigoLineamiento(row[5] == null ? null : (String) row[5]);
                        c.setObservacion(row[6] == null ? null : (String) row[6]);
                        c.setConforme(row[7] == null ? null : (Boolean) row[7]);
                        c.setCodigoMotivDenegacion(row[8] == null ? null : (String) row[8]);
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista detalle de evaluaciones de la solicitud de plantación forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudPlantacionForestalEvaluacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista detalle evaluaciones de la solicitud de plantación forestal.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudPlantacionForestalEvaluacion(SolPlantacionForestalEvaluacionDto dto){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEvaluacion_Registrar");
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacion", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diasEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoResultadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("comunicadoEnviado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantacionForestal", dto.getIdSolPlantacionForestal());
            processStored.setParameter("fechaInicio", dto.getFechaInicio());
            processStored.setParameter("fechaFin", dto.getFechaFin());
            processStored.setParameter("diasEvaluacion", dto.getDiasEvaluacion());
            processStored.setParameter("codigoResultadoEvaluacion", dto.getCodigoResultadoEvaluacion());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("comunicadoEnviado", dto.getComunicadoEnviado());
            processStored.setParameter("idUsuario", dto.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolPlantacionForestalEvaluacion = (Integer) processStored.getOutputParameterValue("idSolPlantacionForestalEvaluacion");
            dto.setIdSolPlantacionForestalEvaluacion(idSolPlantacionForestalEvaluacion);
            result.setCodigo(idSolPlantacionForestalEvaluacion);
            result.setSuccess(true);
            result.setData(dto);
            result.setMessage("Se registró la evaluación de la solicitud de plantación forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la evaluación de la solicitud de plantación forestaln.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudPlantacionForestalEvaluacionDetalle(SolPlantacionForestalEvaluacionDetalleDto dto){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEvaluacionDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacionDetalle", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lineamiento", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoLineamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMotivDenegacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantacionForestalEvaluacion", dto.getIdSolPlantacionForestalEvaluacion());
            processStored.setParameter("codigoSeccion", dto.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", dto.getCodigoSubSeccion());
            processStored.setParameter("lineamiento", dto.getLineamiento());
            processStored.setParameter("codigoLineamiento", dto.getCodigoLineamiento());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("codigoMotivDenegacion", dto.getCodigoMotivDenegacion());
            processStored.setParameter("idUsuario", dto.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolPlantacionForestalEvaluacionDetalle = (Integer) processStored.getOutputParameterValue("idSolPlantacionForestalEvaluacionDetalle");
            dto.setIdSolPlantacionForestalEvaluacionDetalle(idSolPlantacionForestalEvaluacionDetalle);
            result.setCodigo(idSolPlantacionForestalEvaluacionDetalle);
            result.setSuccess(true);
            result.setData(dto);
            result.setMessage("Se registró el detalle de evaluación de la solicitud de plantación forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el detalle de evaluación de la solicitud de plantación forestaln.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudPlantacionForestalEvaluacion(SolPlantacionForestalEvaluacionDto dto){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEvaluacion_Actualizar");
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("diasEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoResultadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("comunicadoEnviado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoCertificado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantacionForestalEvaluacion", dto.getIdSolPlantacionForestalEvaluacion());
            processStored.setParameter("idSolPlantacionForestal", dto.getIdSolPlantacionForestal());
            processStored.setParameter("fechaInicio", dto.getFechaInicio());
            processStored.setParameter("fechaFin", dto.getFechaFin());
            processStored.setParameter("diasEvaluacion", dto.getDiasEvaluacion());
            processStored.setParameter("codigoResultadoEvaluacion", dto.getCodigoResultadoEvaluacion());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("comunicadoEnviado", dto.getComunicadoEnviado());
            processStored.setParameter("idArchivoCertificado", dto.getIdArchivoCertificado());
            processStored.setParameter("idUsuario", dto.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setData(dto);
            result.setMessage("Se actualizó la evaluación de la solicitud de plantación forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar la evaluación de la solicitud de plantación forestaln.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudPlantacionForestalEvaluacionDetalle(SolPlantacionForestalEvaluacionDetalleDto dto){
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEvaluacionDetalle_Actualizar");
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacionDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantacionForestalEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lineamiento", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoLineamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMotivDenegacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("alerta", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantacionForestalEvaluacionDetalle", dto.getIdSolPlantacionForestalEvaluacionDetalle());
            processStored.setParameter("idSolPlantacionForestalEvaluacion", dto.getIdSolPlantacionForestalEvaluacion());
            processStored.setParameter("codigoSeccion", dto.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", dto.getCodigoSubSeccion());
            processStored.setParameter("lineamiento", dto.getLineamiento());
            processStored.setParameter("codigoLineamiento", dto.getCodigoLineamiento());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("codigoMotivDenegacion", dto.getCodigoMotivDenegacion());
            processStored.setParameter("idUsuario", dto.getIdUsuarioModificacion());
            processStored.setParameter("alerta", dto.getNuAlerta());

            processStored.execute();

            result.setSuccess(true);
            result.setData(dto);
            result.setMessage("Se actualizó el detalle de evaluación de la solicitud de plantación forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el detalle de evaluación de la solicitud de plantación forestaln.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }
}
