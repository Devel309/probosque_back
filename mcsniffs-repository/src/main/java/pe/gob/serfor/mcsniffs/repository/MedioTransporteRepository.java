package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;

import java.util.List;

public interface MedioTransporteRepository {


    List<MedioTransporteEntity> ListarMedioTransporte(Integer idMedio ) throws Exception;

}
