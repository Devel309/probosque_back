package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.PermisoForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.PermisoForestalGeometriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository("repositoryPermisoForestalGeometria")
public class PermisoForestalGeometriaRepositoryImpl extends JdbcDaoSupport implements PermisoForestalGeometriaRepository {
    private static final Logger log = LogManager.getLogger(PermisoForestalGeometriaRepositoryImpl.class);

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Registrar Geometría Permiso Forestal}
     * @param: PermisoForestalGeometriaEntity
     */
    @Override
    public Integer registrarPermisoForestalGeometria(PermisoForestalGeometriaEntity item) throws Exception {
        Integer idPermisoForestalGeometria = 0;
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalGeometria_Registrar");
        processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("nombreCapa", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("colorCapa", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("geometriaWKT", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("srid", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPermisoForestalGeometria", Integer.class, ParameterMode.INOUT);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPermisoForestal", item.getIdPermisoForestal());
        processStored.setParameter("idArchivo", item.getIdArchivo());
        processStored.setParameter("tipoGeometria", item.getTipoGeometria());
        processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
        processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
        processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());
        processStored.setParameter("nombreCapa", item.getNombreCapa());
        processStored.setParameter("colorCapa", item.getColorCapa());
        processStored.setParameter("geometriaWKT", item.getGeometry_wkt());
        processStored.setParameter("srid", item.getSrid());
        processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
        processStored.setParameter("idPermisoForestalGeometria", item.getIdPermisoForestalGeometria());
        processStored.execute();
        idPermisoForestalGeometria = (Integer) processStored.getOutputParameterValue("idPermisoForestalGeometria");
        return idPermisoForestalGeometria;
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Obtener Geometría Permiso Forestal}
     * @param: PermisoForestalGeometriaEntity
     */
    @Override
    public ResultClassEntity listarPermisoForestalGeometria(PermisoForestalGeometriaEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<PermisoForestalGeometriaEntity> objList = new ArrayList<PermisoForestalGeometriaEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalGeometria_Listar");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestal", item.getIdPermisoForestal());
            processStored.setParameter("tipoGeometria", item.getTipoGeometria());
            processStored.setParameter("codigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("codigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", item.getCodigoSubSeccion());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PermisoForestalGeometriaEntity item2 = new PermisoForestalGeometriaEntity();
                    item2.setIdPermisoForestal((Integer) row[0]);
                    item2.setIdArchivo((Integer) row[1]);
                    item2.setTipoGeometria((String) row[2]);
                    item2.setCodigoGeometria((String) row[3]);
                    item2.setCodigoSeccion((String) row[4]);
                    item2.setCodigoSubSeccion((String) row[5]);
                    item2.setNombreCapa((String) row[6]);
                    item2.setColorCapa((String) row[7]);
                    item2.setGeometry_wkt((String) row[8]);
                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Obtener Geometría Permiso Forestal archivo y/o capa}
     * @param: PermisoForestalGeometriaEntity
     */
    @Override
    public ResultClassEntity eliminarPermisoForestalGeometriaArchivo(PermisoForestalGeometriaEntity item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestalGeometriaArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("idUsuarioElimina", item.getIdUsuarioElimina());
            processStored.execute();
            result.setData(item.getIdArchivo());
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }
}
