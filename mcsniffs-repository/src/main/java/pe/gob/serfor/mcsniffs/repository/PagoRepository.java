package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;

import java.util.List;

public interface PagoRepository {

    ResultClassEntity RegistrarPago(List<PagoEntity> list) throws Exception;
    //List<PagoEntity>ListarPago(PagoEntity param) throws Exception;
    ResultEntity ListarPago(PagoEntity request);

    ResultClassEntity EliminarPago(PagoPeriodoEntity param) throws Exception;

    ResultClassEntity RegistrarDescuento(List<DescuentoEntity> list) throws Exception;
    List<DescuentoEntity>ListarDescuento(DescuentoEntity param) throws Exception;
    ResultClassEntity EliminarDescuento(DescuentoEntity param) throws Exception;

    //List<THEntity>listarTH(THEntity param) throws Exception;
    ResultEntity listarTH(THEntity request);

    ResultClassEntity RegistrarArchivo(List<PagoArchivoEntity> list) throws Exception;
    List<PagoArchivoEntity>ListarArchivo(PagoArchivoEntity param) throws Exception;
    ResultClassEntity EliminarArchivo(PagoArchivoEntity param) throws Exception;

    ResultEntity listarPagoCronogramaAnios(PagoCronogramaEntity request);

    ResultClassEntity RegistrarGuiaPago(List<PagoGuiaEntity> list) throws Exception;
    ResultEntity ListarGuiaPago(PagoGuiaEntity request);

    List<PagoEntity>listarModalidad(PagoEntity param) throws Exception;
    List<THEntity>listarOperativosPorPGM(THEntity param) throws Exception;

}
