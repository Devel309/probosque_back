package pe.gob.serfor.mcsniffs.repository.impl;


import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoForestalContratoDto;
import pe.gob.serfor.mcsniffs.repository.PlanManejoContratoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Carlos Tang [02-09-2021]
 * @modificado:
 * @descripción: {Repository plan manejo contrato}
 */
@Repository
public class PlanManejoContratoRepositoryImpl extends JdbcDaoSupport implements PlanManejoContratoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanManejoContratoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity obtenerPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception{
        ResultClassEntity result = new ResultClassEntity();
        List<ContratoResponseRequest> lista = new ArrayList<ContratoResponseRequest>();


        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejo_Contrato_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejoContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoContrato", planManejoContratoEntity.getIdPlanManejoContrato());
            processStored.setParameter("idPlanManejo", planManejoContratoEntity.getIdPlanManejo());
            processStored.setParameter("idContrato", planManejoContratoEntity.getIdContrato());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        ContratoResponseRequest c = new ContratoResponseRequest();
                        c.setIdPlanManejoContrato((Integer) row[0]);
                        c.setIdPlanManejo((Integer) row[1]);
                        c.setIdContrato((Integer) row[2]);
                        c.setCodigoEstadoContrato((String) row[3]);
                        c.setTituloHabilitante((String) row[5]);
                        c.setAreaTotal(((BigDecimal) row[6]).doubleValue());
                        c.setDescripcionUnidadAprovechamiento((String) row[7]);
                        c.setDescripcionRegion((String) row[8]);
                        c.setContratoPrincipal((Boolean) row[9]);
                        c.setDescripcionContrato(c.getIdContrato().toString().concat(" .. ").concat(c.getTituloHabilitante()));
                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo los contratos del plan de manejo correctamente.");
        } catch (Exception e) {
            log.error("obtenerPlanManejoContrato", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }

        return result;
    }
    @Override
    public ResultClassEntity RegistrarPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoContrato_registrar");
        processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("principal", Boolean.class, ParameterMode.IN);
        setStoreProcedureEnableNullParameters(processStored);

        processStored.setParameter("idContrato", planManejoContratoEntity.getIdContrato());
        processStored.setParameter("idPlanManejo", planManejoContratoEntity.getIdPlanManejo());
        processStored.setParameter("idUsuarioRegistro", planManejoContratoEntity.getIdUsuarioRegistro());
        processStored.setParameter("principal", planManejoContratoEntity.getContratoPrincipal());
        processStored.execute();

        result.setSuccess(true);
        result.setMessage("Se registró plan manejo.");
        return result;
    }

    @Override
    public ResultClassEntity EliminarPlanManejoContrato(PlanManejoContratoEntity planManejoContratoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoContrato_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", planManejoContratoEntity.getIdPlanManejo());
            processStored.setParameter("idPlanManejoContrato", planManejoContratoEntity.getIdPlanManejoContrato());
            processStored.setParameter("idUsuarioElimina", planManejoContratoEntity.getIdUsuarioElimina());
            processStored.execute();

        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó plan de manejo.");
        return  result;
    }

    /**
     * @autor: Danny Nazario [13-01-2022]
     * @modificado: Danny Nazario [21-03-2022]
     * @descripción: {Listar PlanManejo Contrato por filtro}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPorFiltroPlanManejoContrato(String nroDocumento, String codTipoPlan) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ContratoResponseRequest> list = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PlanManejoContrato_ListarPorFiltro");
            sp.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("nroDocumento", nroDocumento);
            sp.setParameter("codTipoPlan", codTipoPlan);
            sp.execute();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ContratoResponseRequest obj = new ContratoResponseRequest();
                    obj.setIdPlanManejoContrato(row[0] == null ? null : (Integer) row[0]);
                    obj.setIdPlanManejo(row[1] == null ? null : (Integer) row[1]);
                    obj.setIdContrato(row[2] == null ? null : (Integer) row[2]);
                    obj.setCodigoEstadoContrato(row[3] == null ? null : (String) row[3]);
                    obj.setTituloHabilitante(row[4] == null ? null : (String) row[4]);
                    obj.setAreaTotal(row[5] == null ? null : ((BigDecimal) row[5]).doubleValue());
                    obj.setDescripcionUnidadAprovechamiento(row[6] == null ? null : (String) row[6]);
                    obj.setDescripcionRegion(row[7] == null ? null : (String) row[7]);
                    obj.setContratoPrincipal(row[8] == null ? null : (Boolean) row[8]);
                    obj.setDescripcionContrato(obj.getIdContrato().toString().concat(" .. ").concat(obj.getTituloHabilitante()));

                    obj.setCodigoTipoPersonaTitular(row[9] == null ? null : (String) row[9]);
                    obj.setDescripcionTipoPersonaTitular(row[10] == null ? null : (String) row[10]);
                    obj.setCodigoTipoDocumentoTitular(row[11] == null ? null : (String) row[11]);
                    obj.setDescripcionTipoDocIdentidadTitular(row[12] == null ? null : (String) row[12]);
                    obj.setNumeroDocumentoTitular(row[13] == null ? null : (String) row[13]);
                    obj.setNombreTitular(row[14] == null ? null : (String) row[14]);
                    obj.setApePaternoTitular(row[15] == null ? null : (String) row[15]);
                    obj.setApeMaternoTitular(row[16] == null ? null : (String) row[16]);
                    obj.setRazonSocialTitular(row[17] == null ? null : (String) row[17]);
                    obj.setCorreoTitular(row[18] == null ? null : (String) row[18]);
                    obj.setDomicilioLegalTitular(row[19] == null ? null : (String) row[19]);
                    obj.setCodigoTipoDocIdentidadRepLegal(row[20] == null ? null : (String) row[20]);
                    obj.setDescripcionTipoDocIdentidadRepLegal(row[21] == null ? null : (String) row[21]);
                    obj.setNumeroDocumentoRepLegal(row[22] == null ? null : (String) row[22]);
                    obj.setNombreRepresentanteLegal(row[23] == null ? null : (String) row[23]);
                    obj.setApePaternoRepLegal(row[24] == null ? null : (String) row[24]);
                    obj.setApeMaternoRepLegal(row[25] == null ? null : (String) row[25]);
                    obj.setCorreoRepLegal(row[26] == null ? null : (String) row[26]);
                    obj.setDistrito(row[27] == null ? null : (String) row[27]);
                    obj.setProvincia(row[28] == null ? null : (String) row[28]);
                    obj.setDepartamento(row[29] == null ? null : (String) row[29]);
                    obj.setIdDistritoContrato(row[30] == null ? null : (Integer) row[30]);
                    obj.setIdProvinciaContrato(row[31] == null ? null : (Integer) row[31]);
                    obj.setIdDepartamentoContrato(row[32] == null ? null : (Integer) row[32]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó plan manejo contrato correctamente.");
            return result;
        } catch (Exception e) {
            log.error("PlanManejoContarto - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }
}
