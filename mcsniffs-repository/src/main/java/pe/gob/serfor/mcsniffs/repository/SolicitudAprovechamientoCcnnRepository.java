package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudEntity;

public interface SolicitudAprovechamientoCcnnRepository {
    ResultClassEntity ObtenerSolicitudAprovechamientoCcnn(SolicitudEntity solicitud);
}
