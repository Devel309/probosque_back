package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalComiteTecnicoEntity;

public interface SolicitudBosqueLocalComiteTecnicoRepository {

    ResultClassEntity registrarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj);
    
    ResultClassEntity listarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj);

    ResultClassEntity eliminarSolicitudBosqueLocalComiteTecnico(SolicitudBosqueLocalComiteTecnicoEntity obj);
}

