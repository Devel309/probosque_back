package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.xpath.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.*;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.SolPlantacionForestalRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolPlantacionForestalRepositoryImpl extends JdbcDaoSupport implements SolPlantacionForestalRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionRepositoryImpl.class);

    @Override
    public ResultEntity<SolPlantacionForestalEntity> registroSolicitudPlantacionForestal(
            SolPlantacionForestalEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm = con.
         * prepareCall("{call pa_RegistrarSolPlantacionForestal (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2, request.getId_sol_persona(),Types.INTEGER);
         * pstm.setObject (3, request.getId_sol_representante(),Types.VARCHAR);
         * pstm.setObject (4, request.getEstado(),Types.VARCHAR);
         * pstm.setObject (5, request.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject (6, request.getIdUsuarioRegistro(),Types.VARCHAR);
         * 
         * pstm.setObject (7, request.getPersona().getIdTipoPersona(),Types.VARCHAR);
         * pstm.setObject (8, request.getPersona().getNombres(),Types.VARCHAR);
         * pstm.setObject (9, request.getPersona().getNumero_documento(),Types.VARCHAR);
         * pstm.setObject (10, request.getPersona().getRuc(),Types.VARCHAR);
         * pstm.setObject (11, request.getPersona().getDireccion(),Types.VARCHAR);
         * pstm.setObject (12,
         * request.getPersona().getDireccion_numero(),Types.VARCHAR);
         * pstm.setObject (13, request.getPersona().getSector(),Types.VARCHAR);
         * pstm.setObject (14, request.getPersona().getId_distrito(),Types.INTEGER);
         * pstm.setObject (15, request.getPersona().getId_provincia(),Types.INTEGER);
         * pstm.setObject (16, request.getPersona().getId_departamento(),Types.INTEGER);
         * pstm.setObject (17, request.getPersona().getTelefono(),Types.VARCHAR);
         * pstm.setObject (18, request.getPersona().getCelular(),Types.VARCHAR);
         * pstm.setObject (19, request.getPersona().getCorreo(),Types.VARCHAR);
         * 
         * pstm.setObject (20,
         * request.getPersona().getRazon_social_empresa(),Types.VARCHAR);
         * pstm.setObject (21,
         * request.getPersona().getNumero_documento_empresa(),Types.VARCHAR);
         * pstm.setObject (22,
         * request.getPersona().getDireccion_empresa(),Types.VARCHAR);
         * pstm.setObject (23,
         * request.getPersona().getDireccion_numero_empresa(),Types.VARCHAR);
         * pstm.setObject (24, request.getPersona().getSector_empresa(),Types.VARCHAR);
         * pstm.setObject (25,
         * request.getPersona().getId_distrito_empresa(),Types.INTEGER);
         * pstm.setObject (26,
         * request.getPersona().getId_provincia_empresa(),Types.INTEGER);
         * pstm.setObject (27,
         * request.getPersona().getId_departamento_empresa(),Types.INTEGER);
         * pstm.setObject (28,
         * request.getPersona().getTelefono_empresa(),Types.VARCHAR);
         * pstm.setObject (29, request.getPersona().getCelular_empresa(),Types.VARCHAR);
         * pstm.setObject (30, request.getPersona().getEmail_empresa(),Types.VARCHAR);
         * 
         * pstm.registerOutParameter(31,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(31));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultClassEntity eliminarSolicitudPlantacionForestal (SolPlantacionForestalEntity request)
            throws Exception{

        ResultClassEntity result = new ResultClassEntity();

        try{
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("[dbo].[pa_EliminarSolPlantacionForestal]");
            processStored.registerStoredProcedureParameter("idSolPlantacionforestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);


            processStored.setParameter("idSolPlantacionforestal", request.getIdSolicitudPlantacionForestal());
            processStored.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());

            processStored.execute();

            result.setData(request);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");

            return result;

        } catch (Exception e) {

            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }




        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm = con.prepareCall("{call pa_EliminarSolPlantacionForestal (?,?,?)}");
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2, request.getIdUsuarioElimina(),Types.INTEGER);
         * pstm.registerOutParameter(3,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(3));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultClassEntity listarSolicitudPlantacionForestal(SolPlantacionForestalEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestal_Listar");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", request.getIdSolicitudPlantacionForestal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolPlantacionForestalEntity c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalEntity();

                        c.setIdSolicitudPlantacionForestal(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitante(row[1] == null ? null : (Integer) row[1]);
                        c.setIdSolicitudRepresentante(row[2] == null ? null : (Integer) row[2]);

                        PersonaEntity objSolicitante = new PersonaEntity();

                        objSolicitante.setIdTipoPersona(row[3] == null ? null : (Integer) row[3]);
                        objSolicitante.setRazonSocialEmpresa(row[4] == null ? null : (String) row[4]);
                        objSolicitante.setNumeroDocumento(row[5] == null ? null : (String) row[5]);
                        objSolicitante.setRuc(row[6] == null ? null : (String) row[6]);
                        objSolicitante.setDireccion(row[7] == null ? null : (String) row[7]);
                        objSolicitante.setDireccionNumero(row[8] == null ? null : (String) row[8]);
                        objSolicitante.setSector(row[9] == null ? null : (String) row[9]);
                        objSolicitante.setIdDepartamento(row[10] == null ? null : (Integer) row[10]);
                        objSolicitante.setIdProvincia(row[11] == null ? null : (Integer) row[11]);
                        objSolicitante.setIdDistrito(row[12] == null ? null : (Integer) row[12]);
                        objSolicitante.setTelefono(row[13] == null ? null : (String) row[13]);
                        objSolicitante.setCelular(row[14] == null ? null : (String) row[14]);
                        objSolicitante.setCorreo(row[15] == null ? null : (String) row[15]);


                        objSolicitante.setTipoPersona(row[32] == null ? null : (String) row[32]);
                        objSolicitante.setNombreDepartamento(row[33] == null ? null : (String) row[33]);
                        objSolicitante.setNombreProvincia(row[34] == null ? null : (String) row[34]);
                        objSolicitante.setNombreDistrito(row[35] == null ? null : (String) row[35]);

                        c.setSolicitante(objSolicitante);

                        c.setBloqueTab01(row[27] == null ? null : (Boolean) row[27]);
                        c.setBloqueTab02(row[28] == null ? null : (Boolean) row[28]);
                        c.setBloqueTab03(row[29] == null ? null : (Boolean) row[29]);
                        c.setBloqueTab04(row[30] == null ? null : (Boolean) row[30]);
                        c.setBloqueTab05(row[31] == null ? null : (Boolean) row[31]);
                        





                        PersonaEntity objReprLegal = new PersonaEntity();
                        objReprLegal.setNombres(row[16] == null ? null : (String) row[16]);
                        objReprLegal.setNumeroDocumento(row[17] == null ? null : (String) row[17]);
                        objReprLegal.setIdDepartamento(row[18] == null ? null : (Integer) row[18]);
                        objReprLegal.setIdProvincia(row[19] == null ? null : (Integer) row[19]);
                        objReprLegal.setIdDistrito(row[20] == null ? null : (Integer) row[20]);
                        objReprLegal.setDireccion(row[21] == null ? null : (String) row[21]);
                        objReprLegal.setDireccionNumero(row[22] == null ? null : (String) row[22]);
                        objReprLegal.setSector(row[23] == null ? null : (String) row[23]);

                        objReprLegal.setTelefono(row[24] == null ? null : (String) row[24]);
                        objReprLegal.setCelular(row[25] == null ? null : (String) row[25]);
                        objReprLegal.setCorreo(row[26] == null ? null : (String) row[26]);

                        objReprLegal.setNombreDepartamento(row[36] == null ? null : (String) row[36]);
                        objReprLegal.setNombreProvincia(row[37] == null ? null : (String) row[37]);
                        objReprLegal.setNombreDistrito(row[38] == null ? null : (String) row[38]);

                        c.setRepresentanteLegal(objReprLegal);


                        c.setNumeroContrato(row[40] == null ? null : (String) row[40]);
                        c.setTipoContratoTH(row[41] == null ? null : (Boolean) row[41]);


                        c.setCcnn(row[42] == null ? null : (Boolean) row[42]);
                        c.setCodigoAsamblea(row[43] == null ? null : (String) row[43]);
                        c.setIdEvaluacionCampo(row[44] == null ? null : (Integer) row[44]);


                        c.setNroRegistro(row[39] == null ? null : (String) row[39]);
                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Solicitud Plantación Forestal correctamente.");

        } catch (Exception e) {
            log.error("listarSolicitudPlantacionForestal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener el listado de solicitudes de plantación forestal.");

        }
        return result;
    }
    @Override
    public ResultClassEntity obtenerSolicitudPlantacionForestal(SolPlantacionForestalEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestal_Obtener");

            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", request.getIdSolicitudPlantacionForestal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolPlantacionForestalEntity c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalEntity();

                        c.setIdSolicitudPlantacionForestal(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitante(row[1] == null ? null : (Integer) row[1]);
                        c.setIdSolicitudRepresentante(row[2] == null ? null : (Integer) row[2]);

                        PersonaEntity objSolicitante = new PersonaEntity();

                        objSolicitante.setIdTipoPersona(row[3] == null ? null : (Integer) row[3]);
                        objSolicitante.setRazonSocialEmpresa(row[4] == null ? null : (String) row[4]);
                        objSolicitante.setNumeroDocumento(row[5] == null ? null : (String) row[5]);
                        objSolicitante.setRuc(row[6] == null ? null : (String) row[6]);
                        objSolicitante.setDireccion(row[7] == null ? null : (String) row[7]);
                        objSolicitante.setDireccionNumero(row[8] == null ? null : (String) row[8]);
                        objSolicitante.setSector(row[9] == null ? null : (String) row[9]);
                        objSolicitante.setIdDepartamento(row[10] == null ? null : (Integer) row[10]);
                        objSolicitante.setIdProvincia(row[11] == null ? null : (Integer) row[11]);
                        objSolicitante.setIdDistrito(row[12] == null ? null : (Integer) row[12]);
                        objSolicitante.setTelefono(row[13] == null ? null : (String) row[13]);
                        objSolicitante.setCelular(row[14] == null ? null : (String) row[14]);
                        objSolicitante.setCorreo(row[15] == null ? null : (String) row[15]);


                        objSolicitante.setTipoPersona(row[32] == null ? null : (String) row[32]);
                        objSolicitante.setNombreDepartamento(row[33] == null ? null : (String) row[33]);
                        objSolicitante.setNombreProvincia(row[34] == null ? null : (String) row[34]);
                        objSolicitante.setNombreDistrito(row[35] == null ? null : (String) row[35]);

                        c.setSolicitante(objSolicitante);

                        c.setBloqueTab01(row[27] == null ? null : (Boolean) row[27]);
                        c.setBloqueTab02(row[28] == null ? null : (Boolean) row[28]);
                        c.setBloqueTab03(row[29] == null ? null : (Boolean) row[29]);
                        c.setBloqueTab04(row[30] == null ? null : (Boolean) row[30]);
                        c.setBloqueTab05(row[31] == null ? null : (Boolean) row[31]);






                        PersonaEntity objReprLegal = new PersonaEntity();
                        objReprLegal.setNombres(row[16] == null ? null : (String) row[16]);
                        objReprLegal.setNumeroDocumento(row[17] == null ? null : (String) row[17]);
                        objReprLegal.setIdDepartamento(row[18] == null ? null : (Integer) row[18]);
                        objReprLegal.setIdProvincia(row[19] == null ? null : (Integer) row[19]);
                        objReprLegal.setIdDistrito(row[20] == null ? null : (Integer) row[20]);
                        objReprLegal.setDireccion(row[21] == null ? null : (String) row[21]);
                        objReprLegal.setDireccionNumero(row[22] == null ? null : (String) row[22]);
                        objReprLegal.setSector(row[23] == null ? null : (String) row[23]);

                        objReprLegal.setTelefono(row[24] == null ? null : (String) row[24]);
                        objReprLegal.setCelular(row[25] == null ? null : (String) row[25]);
                        objReprLegal.setCorreo(row[26] == null ? null : (String) row[26]);

                        objReprLegal.setNombreDepartamento(row[36] == null ? null : (String) row[36]);
                        objReprLegal.setNombreProvincia(row[37] == null ? null : (String) row[37]);
                        objReprLegal.setNombreDistrito(row[38] == null ? null : (String) row[38]);

                        c.setRepresentanteLegal(objReprLegal);


                        c.setNumeroContrato(row[40] == null ? null : (String) row[40]);
                        c.setTipoContratoTH(row[41] == null ? null : (Boolean) row[41]);


                        c.setCcnn(row[42] == null ? null : (Boolean) row[42]);
                        c.setCodigoAsamblea(row[43] == null ? null : (String) row[43]);
                        c.setIdEvaluacionCampo(row[44] == null ? null : (Integer) row[44]);


                        c.setNroRegistro(row[39] == null ? null : (String) row[39]);
                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Solicitud Plantación Forestal correctamente.");

        } catch (Exception e) {
            log.error("listarSolicitudPlantacionForestal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener el listado de solicitudes de plantación forestal.");

        }
        return result;
    }

    @Override
    public ResultClassEntity actualizarSolicitudPlantacionForestal(SolPlantacionForestalEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolPlantacionForestal_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDireccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sector", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoFijo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoCelular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correoElectronico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTramite", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaTramite", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloqueTab01", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloqueTab02", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloqueTab03", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloqueTab04", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloqueTab05", Boolean.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("direccionRep", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccionNumeroRep", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("sectorRep", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("idDistritoRep", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("telefonoRep", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("celularRep", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("emailRep", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("remitir", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificarSolicitante", Boolean.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("favorable", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificarPasSolicitante", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificarPasSerfor", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroRegistro", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("numeroContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoContratoTH", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoAsamblea", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ccnn", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idGeometria", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificarActualizadoARFFS", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actualizarInformacionRNPF", Boolean.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesion", request.getIdSolicitudPlantacionForestal());
            processStored.setParameter("direccion", (request.getSolicitante()!=null?request.getSolicitante().getDireccion():null) );
            processStored.setParameter("numeroDireccion", (request.getSolicitante()!=null?request.getSolicitante().getDireccionNumero():null) );
            processStored.setParameter("sector", (request.getSolicitante()!=null?request.getSolicitante().getSector():null) );
            processStored.setParameter("idDistrito", (request.getSolicitante()!=null?request.getSolicitante().getIdDistrito():null) );
            processStored.setParameter("telefonoFijo", (request.getSolicitante()!=null?request.getSolicitante().getTelefono():null) );
            processStored.setParameter("telefonoCelular", (request.getSolicitante()!=null?request.getSolicitante().getCelular():null) );
            processStored.setParameter("correoElectronico", (request.getSolicitante()!=null?request.getSolicitante().getCorreo():null) );
            processStored.setParameter("nroTramite", request.getNroTramite());
            processStored.setParameter("fechaTramite", request.getFechaTramite());
            processStored.setParameter("estadoSolicitud", request.getEstadoSolicitud());
            processStored.setParameter("idUsuario", request.getIdUsuarioModificacion());
            processStored.setParameter("bloqueTab01", request.getBloqueTab01());
            processStored.setParameter("bloqueTab02", request.getBloqueTab02());
            processStored.setParameter("bloqueTab03", request.getBloqueTab03());
            processStored.setParameter("bloqueTab04", request.getBloqueTab04());
            processStored.setParameter("bloqueTab05", request.getBloqueTab05());



            processStored.setParameter("direccionRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getDireccion():null) );
            processStored.setParameter("direccionNumeroRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getDireccionNumero():null) );
            processStored.setParameter("sectorRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getSector():null) );

            processStored.setParameter("idDistritoRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getIdDistrito():null) );
            processStored.setParameter("telefonoRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getTelefono():null) );
            processStored.setParameter("celularRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getCelular():null) );
            processStored.setParameter("emailRep", (request.getRepresentanteLegal()!=null?request.getRepresentanteLegal().getCorreo():null) );
            processStored.setParameter("remitir", request.getRemitir());
            processStored.setParameter("notificarSolicitante", request.getNotificarSolicitante());
            processStored.setParameter("favorable", request.getFavorable());
            processStored.setParameter("notificarPasSolicitante", request.getNotificarPasSolicitante());
            processStored.setParameter("notificarPasSerfor", request.getNotificarPasSerfor());
            processStored.setParameter("nroRegistro", request.getNroRegistro());
            processStored.setParameter("numeroContrato", request.getNumeroContrato());
            processStored.setParameter("tipoContratoTH", request.getTipoContratoTH());

            processStored.setParameter("codigoAsamblea", request.getCodigoAsamblea());
            processStored.setParameter("ccnn", request.getCcnn());
            processStored.setParameter("idGeometria", request.getIdGeometria());
            processStored.setParameter("notificarActualizadoARFFS", request.getNotificarActualizadoARFFS());
            processStored.setParameter("actualizarInformacionRNPF", request.getActualizarInformacionRNPF());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó la Solicitud de Plantación Forestal correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_SOLPLANTACIONFORESTAL,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLPLANTACIONFORESTAL + request.getIdSolicitudPlantacionForestal(),
                    request.getIdUsuarioModificacion());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar la Solicitud de Plantación Forestal.");
            return result;
        }
    }

    @Override
    public ResultEntity<Dropdown> listarTipoComboSistemaPlantacionForestal(SistemaPlantacionEntity request) {
        ResultEntity resul = new ResultEntity();
//        try {
//            List<Dropdown> objList = new ArrayList<Dropdown>();
//            String sql = "select * from T_MAP_SISTEMAPLANTACION where TX_ESTADO='A' ";
//            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
//            for (Map<String, Object> row : rows) {
//                Dropdown obj = new Dropdown();
//                obj.setValor((Integer) row.get("NU_ID_SISTEMAPLANTACION"));
//                obj.setLabel((String) row.get("DESCRIPCION"));
//                objList.add(obj);
//            }
//            resul.setData(objList);
//            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
//            resul.setIsSuccess((objList.size() != 0 ? true : false));
//        } catch (Exception e) {
//            resul.setMessage(e.getMessage());
//            resul.setIsSuccess(false);
//        }
        return resul;
    }

    @Override
    public ResultClassEntity listarBandejaSolicitudPlantacionForestal(
            SolPlantacionForestalDto request) {
        ResultClassEntity resul = new ResultClassEntity();
        List<SolPlantacionForestalDto> lstresult = new ArrayList<SolPlantacionForestalDto>();
        try

        {

            StoredProcedureQuery sp = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestal_ListarPorFiltro");
            sp.registerStoredProcedureParameter("idSolicitud", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoModalidad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreSolicitante", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("remitir", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("numeroDocSolicitante", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("notificarActualizadoARFFS", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("actualizarInformacionRNPF", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoPerfil", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            
            sp.setParameter("idSolicitud", request.getIdSolicitudPlantacion());
            sp.setParameter("tipoModalidad", request.getTipoModalidad());
            sp.setParameter("nombreSolicitante", request.getNombreSolicitante());
            sp.setParameter("numeroDocSolicitante", request.getNumeroDocumentoSolicitante());
            sp.setParameter("remitir", request.getRemitir());
            sp.setParameter("estadoSolicitud", request.getEstadoSolicitud());
            sp.setParameter("notificarActualizadoARFFS", request.getNotificarActualizadoARFFS());
            sp.setParameter("actualizarInformacionRNPF", request.getActualizarInformacionRNPF());
            sp.setParameter("pageNum", request.getPageNum());
            sp.setParameter("pageSize", request.getPageSize());
            sp.setParameter("codigoPerfil", request.getCodigoPerfil());



            List<Object[]> spResult = sp.getResultList();

            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {

                    SolPlantacionForestalDto obj = new SolPlantacionForestalDto();

                    obj.setIdSolicitudPlantacion((Integer) row[0]);
                    obj.setTipoModalidad((String) row[1]);
                    obj.setTipoModalidadTexto((String) row[2]);
                    obj.setIdSolicitante((Integer) row[3]);
                    obj.setTipoPersonaSolicitante((String) row[4]);
                    obj.setTipoDocumentoSolicitante((String) row[5]);

                    obj.setNumeroDocumentoSolicitante((String) row[6]);


                    obj.setNombreSolicitanteNatural((String) row[7]);
                    obj.setNombreSolicitanteJuridico((String) row[8]);

                    obj.setNombreSolicitante((String) row[9]);

                    obj.setTelefonoFijo((String) row[10]);
                    obj.setCorreoElectronico((String) row[11]);
                    obj.setIdDepartamentoSolicitante((Integer) row[12]);
                    obj.setIdProvinciaSolicitante((Integer) row[13]);
                    obj.setIdDistritoSolicitante((Integer) row[14]);
                    obj.setNombreDepartamentoSolicitante((String) row[15]);

                    obj.setNombreProvinciaSolicitante((String) row[16]);
                    obj.setNombreDistritoSolicitante((String) row[17]);


                    obj.setDireccionSolicitante((String) row[18]);

                    obj.setEstadoSolicitud((String) row[19]);
                    obj.setEstadoSolicitudTexto((String) row[20]);
                    obj.setFechaRegistroTexto((String) row[21]);
                    obj.setNumeroTH((String) row[22]);
                    resul.setTotalRecord((Integer) row[23]);
                    obj.setNroTramite((String) row[24]);
                    obj.setFechaTramite((Date) row[25]);
                    obj.setRemitir((Boolean) row[26]);
                    obj.setNotificarSolicitante((Boolean) row[27]);
                    obj.setFavorable((Boolean) row[28]);
                    obj.setNotificarPasSolicitante((Boolean) row[29]);
                    obj.setNotificarPasSerfor((Boolean) row[30]);
                    obj.setIdEvaluacionCampo((Integer) row[31]);
                    obj.setIdSolicitudPlantacionHijo((Integer) row[32]);

                    obj.setNotificarActualizadoARFFS((Boolean) row[33]);
                    obj.setActualizarInformacionRNPF((Boolean) row[34]);
                    lstresult.add(obj);

                }
            }

            resul.setData(lstresult);
            resul.setMessage((lstresult.size() != 0 ? "Información encontrada aqui" : "Informacion no encontrada"));
            resul.setSuccess(true);
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultClassEntity clonarPlantacionForestal(
            SolPlantacionForestalDto request) {


        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestal_Clonar");

            processStored.registerStoredProcedureParameter("idSolicitudPlantacionForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudPlantacionForestalOutPut", Integer.class, ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudPlantacionForestal", request.getIdSolicitudPlantacion());

            processStored.execute();
            Integer salida = (Integer) processStored.getOutputParameterValue("idSolicitudPlantacionForestalOutPut");
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se realizó el registro de información opcional correctamente");

            return result;

        } catch (Exception e) {
            log.error("SolPlantacionForestalRepositoryImpl - clonarPlantacionForestal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error al registrar información opcional.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }


    @Override
    public ResultClassEntity validarClonPlantacionForestal(
            SolPlantacionForestalDto request) {


        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalInformacionOpcional_validar");

            processStored.registerStoredProcedureParameter("idSolicitudPlantacionForestalHijo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("informacionOpcionalActualizado", Boolean.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudPlantacionForestalHijo", request.getIdSolicitudPlantacionHijo());
            processStored.setParameter("informacionOpcionalActualizado", request.getInformacionOpcionalActualizado());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se realizó la validación de la clonación de la información opcional correctamente");

            return result;

        } catch (Exception e) {
            log.error("SolPlantacionForestalRepositoryImpl - validarClonPlantacionForestal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error al validar la clonación información opcional.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }


    @Override
    public ResultEntity<SolPlantacionForestalEstadoEntity> RegistrarSolicitudPlantacionForestalEstado(
            SolPlantacionForestalEstadoEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_RegistrarSolPlantacionForestalEstado (?,?,?,?,?)}"
         * );
         * pstm.setObject (1, request.getId_solplantforestal(),Types.INTEGER);
         * pstm.setObject (2, request.getId_solplantforestal_estado(),Types.INTEGER);
         * pstm.setObject (3, request.getEstado(),Types.INTEGER);
         * pstm.setObject (4, request.getId_usuario_registro(),Types.INTEGER);
         * pstm.registerOutParameter(5,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(5));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> registroSolicitudPlantacionForestalAreaPlantada(
            List<SolPlantacionForestalAreaPlantadaEntity> request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * for(SolPlantacionForestalAreaPlantadaEntity obj:request){
         * pstm = con.
         * prepareCall("{call pa_RegistrarSolPlantacionForestalAreaPlant (?,?,?,?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1, obj.getId_solplantforest_areaplantada(),Types.INTEGER);
         * pstm.setObject (2, obj.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (3, obj.getArea_total_plantacion(),Types.INTEGER);
         * pstm.setObject (4, obj.getMes_anio_plantacion(),Types.DATE);
         * pstm.setObject (5, obj.getId_sistemaplantacion(),Types.INTEGER);
         * pstm.setObject (6, obj.getSuperficie_medida(),Types.VARCHAR);
         * pstm.setObject (7, obj.getSuperficie_cantidad(),Types.INTEGER);
         * pstm.setObject (8, obj.getFines(),Types.INTEGER);
         * pstm.setObject (9, obj.getEspecies_establecidas(),Types.VARCHAR);
         * pstm.setObject (10, obj.getEstado(),Types.VARCHAR);
         * pstm.setObject (11, obj.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject (12, obj.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.registerOutParameter(13,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(13));
         * 
         * res+= ( pstm.getInt(13)>0?"":"Item:" +
         * obj.getId_solplantforest_areaplantada().toString()
         * +" => ItemPrincipal "+obj.getId_solplantforest() +",") ;
         * resul.setInformacion(res);
         * }
         * resul.setMessage((resul.getInformacion().length()<1?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getInformacion().length()<1?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> eliminarSolicitudPlantacionForestalAreaPlantada(
            SolPlantacionForestalAreaPlantadaEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_EliminarSolPlantacionForestalAreaPlant (?,?,?,?)}"
         * );
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2,
         * request.getId_solplantforest_areaplantada(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * 
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> listarSolicitudPlantacionForestalAreaPlantada(
            SolPlantacionForestalAreaPlantadaEntity request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultEntity<SolPlantacionForestalAreaPlantadaEntity> obtenerSolicitudPlantacionForestalAreaPlantada(
            SolPlantacionForestalAreaPlantadaEntity request) {
        ResultEntity resul = new ResultEntity();
        try {

            List<SolPlantacionForestalAreaPlantadaEntity> objList = new ArrayList<SolPlantacionForestalAreaPlantadaEntity>();

            String sql = "EXEC pa_ObtenerSolPlantacionForestalAreaPlant " + request.getId_solplantforest();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {
                SolPlantacionForestalAreaPlantadaEntity obj = new SolPlantacionForestalAreaPlantadaEntity();
                obj.setId_solplantforest_areaplantada((Integer) row.get("NU_ID_SOLPLANTFOREST_AREAPLANTADA"));
                obj.setId_solplantforest((Integer) row.get("NU_ID_SOLPLANTFOREST"));
                obj.setArea_total_plantacion((Integer) row.get("NU_AREA_TOTAL_PLANTACION"));
                obj.setMes_anio_plantacion((Date) row.get("FE_MES_ANIO_PLANTACION"));
                obj.setId_sistemaplantacion((Integer) row.get("NU_ID_SISTEMAPLANTACION"));
                obj.setSuperficie_medida((String) row.get("TX_SUPERFICIE_MEDIDA"));
                obj.setSuperficie_cantidad((Integer) row.get("NU_SUPERFICIE_CANTIDAD"));
                obj.setFines((Integer) row.get("NU_FINES"));
                obj.setEspecies_establecidas((String) row.get("TX_ESPECIES_ESTABLECIDAS"));
                obj.setEstado((String) row.get("TX_ESTADO"));
                obj.setIdUsuarioModificacion((Integer) row.get("NU_ID_USUARIO_MODIFICACION"));
                obj.setFechaModificacion((Date) row.get("FE_FECHA_MODIFICACION"));
                obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_REGISTRO"));
                obj.setFechaRegistro((Date) row.get("FE_FECHA_REGISTRO"));
                // resul.setTotalRegistro((Integer)row.get("TotalRegistro"));
                objList.add(obj);
            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    /* lISTAR */

    @Override
    public ResultEntity<SolPlantacionForestalAreaEntity> listarSolPlantacionForestalArea(
            SolPlantacionForestalAreaEntity request) {
        ResultEntity result = new ResultEntity();
        List<SolPlantacionForestalAreaEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalArea_Obtener");

            processStored.registerStoredProcedureParameter("idSolPlantaForestalArea", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantaForestalArea", request.getIdSolPlantaForestalArea());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolPlantacionForestalAreaEntity c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalAreaEntity();

                        c.setIdSolPlantaForestalArea(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolPlantaForestal(row[1] == null ? null : (Integer) row[1]);
                        c.setPredio(row[2] == null ? null : (String) row[2]);
                        c.setArea(row[3] == null ? null : (BigDecimal) row[3]);
                        c.setRuc(row[4] == null ? null : (String) row[4]);
                        c.setCaserioComunidad(row[5] == null ? null : (String) row[5]);
                        c.setIdDistrito(row[6] == null ? null : (Integer) row[6]);
                        c.setIdDocAutorizaPlantacion(row[7] == null ? null : (Integer) row[7]);
                        c.setDocAutorizaPlantacion(row[8] == null ? null : (String) row[8]);
                        c.setNumCesionAgroforestal(row[9] == null ? null : (String) row[9]);
                        c.setNumConcesionAgroforestal(row[10] == null ? null : (String) row[10]);
                        c.setEstado(row[11] == null ? null : (String) row[11]);
                        c.setInversionista(row[12] == null ? null : (Boolean) row[12]);
                        c.setCodigoTipodocInversionista(row[13] == null ? null : (String) row[13]);
                        c.setNumerDocInversionista(row[14] == null ? null : (String) row[14]);
                        c.setPropietario(row[15] == null ? null : (Boolean) row[15]);
                        c.setCodigoTipoDocPropietario(row[16] == null ? null : (String) row[16]);
                        c.setNumeroDocPropietario(row[17] == null ? null : (String) row[17]);
                        c.setDesAdjuntoInversionista(row[19] == null ? null : (String) row[19]);
                        c.setDesAdjuntoPropietario(row[20] == null ? null : (String) row[20]);
                        c.setZona(row[21] == null ? null : (String) row[21]);
                        c.setIdProvincia(row[27] == null ? null : (Integer) row[27]);
                        c.setIdDepartamento(row[28] == null ? null : (Integer) row[28]);
                        c.setAreaTotalPlantacion(row[29] == null ? null : (BigDecimal) row[29]);
                        c.setMesAnhoPlantacion(row[30] == null ? null : (Timestamp) row[30]);

                        c.setNombreDistrito(row[31] == null ? null : (String) row[31]);
                        c.setNombreProvincia(row[32] == null ? null : (String) row[32]);
                        c.setNombreDepartamento(row[33] == null ? null : (String) row[33]);

                        c.setAlturaPromedio(row[34] == null ? null : (BigDecimal) row[34]);
                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setIsSuccess(true);
            result.setMessage("Se realizó la acción Listar Solicitud Plantación Forestal correctamente.");

        } catch (Exception e) {
            log.error("listarSolicitudPlantacionForestal", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener el listado de solicitudes de plantación forestal.");

        }
        return result;
    }

    /**** area */

    @Override
    public ResultClassEntity registroSolPlantacionForestalArea(
            SolPlantacionForestalAreaEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_RegistrarSolPlantacionForestalArea");
            processStored.registerStoredProcedureParameter("P_ID_SOLPLANTFOREST_AREA", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_ID_SOLPLANTFOREST", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PREDIO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_AREA", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_CASERIO_COMUNIDAD", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_ID_DISTRITO", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_ID_DOC_AUTORIZA_PLANTACION", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DOC_AUTORIZA_PLANTACION", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_NUM_CESION_AGROFORESTAL", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_NUM_CONCESION_AGROFORESTAL", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("TX_RUC", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_NU_INVERSIONISTA", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_TX_CODIGO_TIPODOC_INVERSIONISTA", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_TX_NUMERO_DOC_INVERSIONISTA", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_NU_PROPIETARIO", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_TX_CODIGO_TIPODOC_PROPIETARIO", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_NU_AREA_TOTAL_PLANTACION", BigDecimal.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_FE_MES_ANIO_PLANTACION", Timestamp.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_TX_NUMERO_DOC_PROPIETARIO", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESCRIPCIONADJUNTO_INVERSIONISTA", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_DESCRIPCIONADJUNTO_PROPIETARIO", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_ZONA", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_ID_USUARIO_REGISTRO", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_NU_ALTURAPROMEDIO", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_ID_SOLPLANTFOREST_AREA_OUT", Integer.class,
                    ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("P_ID_SOLPLANTFOREST_AREA", request.getIdSolPlantaForestalArea());
            processStored.setParameter("P_ID_SOLPLANTFOREST", request.getIdSolPlantaForestal());
            processStored.setParameter("P_PREDIO", request.getPredio());
            processStored.setParameter("P_AREA", request.getArea());
            processStored.setParameter("P_CASERIO_COMUNIDAD", request.getCaserioComunidad());
            processStored.setParameter("P_ID_DISTRITO", request.getIdDistrito());
            processStored.setParameter("P_ID_DOC_AUTORIZA_PLANTACION", request.getIdDocAutorizaPlantacion());
            processStored.setParameter("P_DOC_AUTORIZA_PLANTACION", request.getDocAutorizaPlantacion());
            processStored.setParameter("P_NUM_CESION_AGROFORESTAL", request.getNumCesionAgroforetsal());
            processStored.setParameter("P_NUM_CONCESION_AGROFORESTAL", request.getNumConcesionAgroforestal());
            processStored.setParameter("TX_RUC", request.getRuc());
            processStored.setParameter("P_NU_INVERSIONISTA", request.getInversionista());
            processStored.setParameter("P_TX_CODIGO_TIPODOC_INVERSIONISTA", request.getCodigoTipodocInversionista());
            processStored.setParameter("P_TX_NUMERO_DOC_INVERSIONISTA", request.getNumerDocInversionista());
            processStored.setParameter("P_NU_PROPIETARIO", request.getPropietario());
            processStored.setParameter("P_TX_CODIGO_TIPODOC_PROPIETARIO", request.getCodigoTipoDocPropietario());
            processStored.setParameter("P_TX_NUMERO_DOC_PROPIETARIO", request.getNumeroDocPropietario());
            processStored.setParameter("P_NU_AREA_TOTAL_PLANTACION", request.getAreaTotalPlantacion());
            processStored.setParameter("P_FE_MES_ANIO_PLANTACION", request.getMesAnhoPlantacion());
            processStored.setParameter("P_DESCRIPCIONADJUNTO_INVERSIONISTA", request.getDesAdjuntoInversionista());
            processStored.setParameter("P_DESCRIPCIONADJUNTO_PROPIETARIO", request.getDesAdjuntoPropietario());
            processStored.setParameter("P_ZONA", request.getZona());
            processStored.setParameter("P_NU_ALTURAPROMEDIO", request.getAlturaPromedio());
            processStored.setParameter("P_ID_USUARIO_REGISTRO", request.getIdUsuarioRegistro());
            processStored.execute();
            Integer salida = (Integer) processStored.getOutputParameterValue("P_ID_SOLPLANTFOREST_AREA_OUT");
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se registró correctamente la información del Area");

            return result;

        } catch (Exception e) {
            log.error("SolPlantacionForestalRepositoryImpl - registroSolPlantacionForestalArea", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**** det coord */
    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> registroSolicitudPlantacionForestalDetCoord(
            List<SolPlantacionForestalDetCoordenadaEntity> request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * 
         * for(SolPlantacionForestalDetCoordenadaEntity obj:request){
         * 
         * for(SolPlantacionForestalDetCoordenadaDetEntity itemobj:obj.getDetalle()){
         * pstm = con.
         * prepareCall("{call pa_RegistrarSolPlantacionForestalDetCoord (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1, obj.getId_solplantforest_coord_det(),Types.INTEGER);
         * pstm.setObject (2, obj.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (3, obj.getId_bloquesector(),Types.INTEGER);
         * pstm.setObject (4, obj.getBloquesector(),Types.VARCHAR);
         * 
         * pstm.setObject (5, itemobj.getVertice(),Types.INTEGER);
         * pstm.setObject (6, itemobj.getZonaeste(),Types.VARCHAR);
         * pstm.setObject (7, itemobj.getZonanorte(),Types.VARCHAR);
         * pstm.setObject (8, itemobj.getObservaciones(),Types.VARCHAR);
         * 
         * pstm.setObject (9, obj.getAreabloque(),Types.INTEGER);
         * pstm.setObject (10, obj.getAreabloque_unidad(),Types.VARCHAR);
         * 
         * pstm.setObject (11, obj.getEspecies_establecidas(),Types.VARCHAR);
         * pstm.setObject (12, obj.getEstado(),Types.VARCHAR);
         * pstm.setObject (13, obj.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject (14, obj.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.registerOutParameter(15,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(15));
         * res+= ( pstm.getInt(15)>0?"":"Item:" +
         * obj.getId_solplantforest_coord_det().toString()
         * +" => ItemPrincipal "+obj.getId_solplantforest().toString() +",") ;
         * resul.setInformacion(res);
         * }
         * 
         * }
         * resul.setMessage((resul.getInformacion().length()<1?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getInformacion().length()<1?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> eliminarSolicitudPlantacionForestalDetCoord(
            SolPlantacionForestalDetCoordenadaEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_EliminarSolPlantacionForestalDetCoord (?,?,?,?)}");
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2, request.getId_solplantforest_coord_det(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * 
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> listarSolicitudPlantacionForestalDetCoord(
            SolPlantacionForestalDetCoordenadaEntity request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetCoordenadaEntity> obtenerSolicitudPlantacionForestalDetCoord(
            SolPlantacionForestalDetCoordenadaEntity request) {
        ResultEntity resul = new ResultEntity();
        try {
            List<SolPlantacionForestalDetCoordenadaEntity> objList = new ArrayList<SolPlantacionForestalDetCoordenadaEntity>();
            List<SolPlantacionForestalDetCoordenadaDetEntity> objListdet;
            String sql = "EXEC pa_ObtenerSolPlantacionForestalDetCoord " + request.getId_solplantforest();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
            final Map<Object, List<Map<String, Object>>> rowsc = rows.stream()
                    .collect(Collectors.groupingBy(x -> x.get("TX_BLOQUESECTOR")));

            for (List<Map<String, Object>> row : rowsc.values()) {
                objListdet = new ArrayList<SolPlantacionForestalDetCoordenadaDetEntity>();
                String blo = (String) row.get(0).get("TX_BLOQUESECTOR");
                if (objList.stream()
                        .filter(x -> x.getBloquesector() == blo)
                        .collect(Collectors.toList())
                        .size() == 0) {
                    SolPlantacionForestalDetCoordenadaEntity obj = new SolPlantacionForestalDetCoordenadaEntity();
                    for (Map<String, Object> item : row) {
                        obj.setBloquesector((String) item.get("TX_BLOQUESECTOR"));

                        obj.setId_solplantforest((Integer) item.get("NU_ID_SOLPLANTFOREST"));
                        obj.setId_bloquesector((Integer) item.get("NU_ID_BLOQUESECTOR"));
                        obj.setAreabloque((Integer) item.get("NU_AREABLOQUE"));
                        obj.setBloquesector((String) item.get("TX_BLOQUESECTOR"));
                        obj.setAreabloque_unidad((String) item.get("TX_AREABLOQUE_UNIDAD"));

                        obj.setEstado((String) row.get(0).get("TX_ESTADO"));
                        obj.setIdUsuarioRegistro((Integer) row.get(0).get("NU_ID_USUARIO_REGISTRO"));
                        obj.setFechaRegistro((Date) row.get(0).get("FE_FECHA_REGISTRO"));
                        obj.setId_solplantforest_coord_det((Integer) item.get("NU_ID_SOLPLANTFOREST_COORD_DET"));
                        SolPlantacionForestalDetCoordenadaDetEntity item_obj = new SolPlantacionForestalDetCoordenadaDetEntity();
                        // item_obj.setId_bloquesector((Integer)item.get("NU_ID_BLOQUESECTOR"));
                        item_obj.setId_solplantforest_coord_det((Integer) item.get("NU_ID_SOLPLANTFOREST_COORD_DET"));
                        item_obj.setVertice((Integer) item.get("NU_VERTICE"));
                        item_obj.setZonaeste((String) item.get("TX_ZONAESTE"));
                        item_obj.setZonanorte((String) item.get("TX_ZONANORTE"));
                        item_obj.setObservaciones((String) item.get("TX_OBSERVACIONES"));
                        objListdet.add(item_obj);
                    }

                    obj.setDetalle(objListdet);
                    objList.add(obj);
                }
            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    /**** det */

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> registroSolicitudPlantacionForestalDet(
            List<SolPlantacionForestalDetalleEntity> request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * for(SolPlantacionForestalDetalleEntity obj:request){
         * pstm = con.
         * prepareCall("{call pa_RegistrarSolPlantacionForestalDet (?,?,?,?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1, obj.getId_solplantforest_det(),Types.INTEGER);
         * pstm.setObject (2, obj.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (3, obj.getEspcs_nom_comun(),Types.VARCHAR);
         * pstm.setObject (4, obj.getEspcs_nom_cientifico(),Types.VARCHAR);
         * pstm.setObject (5,
         * obj.getTotal_arbol_matas_espcs_existentes(),Types.INTEGER);
         * pstm.setObject (6, obj.getProduccion_estimada(),Types.INTEGER);
         * pstm.setObject (7, obj.getProducion_estimada_und_medida(),Types.VARCHAR);
         * pstm.setObject (8, obj.getCoord_este(),Types.VARCHAR);
         * pstm.setObject (9, obj.getCoord_norte(),Types.VARCHAR);
         * pstm.setObject (10, obj.getEstado(),Types.VARCHAR);
         * pstm.setObject (11, obj.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.setObject (12, obj.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.registerOutParameter(13,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(13));
         * 
         * res+= ( pstm.getInt(13)>0?"":"Item:" +
         * obj.getId_solplantforest_det().toString()
         * +" => ItemPrincipal "+obj.getId_solplantforest().toString() +",") ;
         * resul.setInformacion(res);
         * }
         * resul.setMessage((resul.getInformacion().length()<1?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getInformacion().length()<1?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> eliminarSolicitudPlantacionForestalDet(
            SolPlantacionForestalDetalleEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_EliminarSolPlantacionForestalDet (?,?,?,?)}");
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2, request.getId_solplantforest_det(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * 
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> listarSolicitudPlantacionForestalDet(
            SolPlantacionForestalDetalleEntity request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultEntity<SolPlantacionForestalDetalleEntity> obtenerSolicitudPlantacionForestalDet(
            SolPlantacionForestalDetalleEntity request) {
        ResultEntity resul = new ResultEntity();
        try {
            List<SolPlantacionForestalDetalleEntity> objList = new ArrayList<SolPlantacionForestalDetalleEntity>();
            String sql = "EXEC pa_ObtenerSolPlantacionForestalDet " + request.getId_solplantforest();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {
                SolPlantacionForestalDetalleEntity obj = new SolPlantacionForestalDetalleEntity();
                obj.setId_solplantforest_det((Integer) row.get("NU_ID_SOLPLANTFOREST_DET"));
                obj.setId_solplantforest((Integer) row.get("NU_ID_SOLPLANTFOREST"));
                obj.setEspcs_nom_comun((String) row.get("TX_ESPCS_NOM_COMUN"));
                obj.setEspcs_nom_cientifico((String) row.get("TX_ESPCS_NOM_CIENTIFICO"));
                obj.setTotal_arbol_matas_espcs_existentes((Integer) row.get("NU_TOTAL_ARBOL_MATAS_ESPCS_EXISTENTES"));
                obj.setProduccion_estimada((BigDecimal) row.get("NU_PRODUCCION_ESTIMADA"));
                obj.setProducion_estimada_und_medida((String) row.get("TX_PRODUCION_ESTIMADA_UND_MEDIDA"));
                obj.setCoord_este((String) row.get("TX_COORD_ESTE"));
                obj.setCoord_norte((String) row.get("TX_COORD_NORTE"));
                obj.setEstado((String) row.get("TX_ESTADO"));
                obj.setIdUsuarioModificacion((Integer) row.get("NU_ID_USUARIO_MODIFICACION"));
                obj.setFechaModificacion((Date) row.get("FE_FECHA_MODIFICACION"));

                obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_REGISTRO"));
                obj.setFechaRegistro((Date) row.get("FE_FECHA_REGISTRO"));
                // resul.setTotalRegistro((Integer)row.get("TotalRegistro"));
                objList.add(obj);
            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    /*********** anexo */

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> registroSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * 
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm = con.
         * prepareCall("{call pa_RegistrarSolPlantacionForestalAnexo (?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1,
         * request.getId_archivo_solplantacionforest(),Types.INTEGER);
         * pstm.setObject (2, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (3, request.getIdArchivo(),Types.INTEGER);
         * pstm.setObject (4, request.getId_tipodetalleseccion(),Types.INTEGER);
         * pstm.setObject (5, request.getId_tipodetalleanexo(),Types.INTEGER);
         * pstm.setObject (6, request.getDescripcion(),Types.VARCHAR);
         * pstm.setObject (7, request.getEstado(),Types.VARCHAR);
         * pstm.setObject (8, request.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject (9, request.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.registerOutParameter(10,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(10));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * 
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> eliminarSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_EliminarSolPlantacionForestalAnexo (?,?,?,?)}");
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2,
         * request.getId_archivo_solplantacionforest(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> listarSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultEntity<SolPlantacionForestalAnexoEntity> obtenerSolPlantacionForestalAnexo(
            SolPlantacionForestalAnexoEntity request) {
        ResultEntity resul = new ResultEntity();
        try {

            List<SolPlantacionForestalAnexoEntity> objList = new ArrayList<SolPlantacionForestalAnexoEntity>();
            String sql = "EXEC pa_ObtenerSolPlantacionForestalAnexo "
                    + request.getId_solplantforest()
                    + "," + request.getId_tipodetalleanexo()
                    + "," + request.getId_tipodetalleseccion()
                    + "," + request.getPageNum()
                    + "," + request.getPageSize();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {
                SolPlantacionForestalAnexoEntity obj = new SolPlantacionForestalAnexoEntity();
                obj.setId_archivo_solplantacionforest((Integer) row.get("NU_ID_SOLPLANTACIONFOREST_ARCHIVO"));
                obj.setId_solplantforest((Integer) row.get("NU_ID_SOLPLANTFOREST"));
                obj.setIdArchivo((Integer) row.get("NU_ID_ARCHIVO"));
                obj.setId_tipodetalleseccion((Integer) row.get("NU_ID_TIPODETALLESECCION"));
                obj.setId_tipodetalleanexo((Integer) row.get("NU_ID_TIPODETALLEANEXO"));
                obj.setDescripcion((String) row.get("TX_DESCRIPCION"));
                obj.setEstado((String) row.get("TX_ESTADO"));
                obj.setRuta((String) row.get("TX_RUTA"));
                obj.setNombre((String) row.get("TX_NOMBRE"));
                obj.setExtension((String) row.get("TX_EXTENSION"));
                obj.setDes_tipodetalleanexo((String) row.get("des_TIPODETALLEANEXO"));
                obj.setExtension((String) row.get("TX_EXTENSION"));
                obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_REGISTRO"));
                obj.setUsuarioCreacion((String) row.get("USUARIO_REGISTRO"));
                obj.setFechaRegistro((Date) row.get("FE_FECHA_REGISTRO"));
                resul.setTotalrecord((Integer) row.get("TotalRegistro"));
                objList.add(obj);
            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    /*********** Observacion */
    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> registroSolPlantacionForestalObservacion(
            List<SolPlantacionForestalObservacionEntity> request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * for(SolPlantacionForestalObservacionEntity item:request){
         * pstm = con.
         * prepareCall("{call pa_RegistrarSolPlantacionForestalAnexo (?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1,
         * item.getId_solplantacionforest_observacion(),Types.INTEGER);
         * pstm.setObject (2, item.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (3, item.getId_detalleseccion(),Types.VARCHAR);
         * pstm.setObject (4, item.getEvaluacion(),Types.INTEGER);
         * pstm.setObject (5, item.getAlerta(),Types.BOOLEAN);
         * pstm.setObject (6, item.getObservacion(),Types.VARCHAR);
         * pstm.setObject (7, item.getEstado(),Types.VARCHAR);
         * pstm.setObject (8, item.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject (9, item.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.registerOutParameter(10,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(10));
         * res+= ( pstm.getInt(10)>0?"":"Item:" +
         * item.getId_solplantacionforest_observacion().toString()
         * +" => ItemPrincipal "+item.getId_solplantforest().toString() +",") ;
         * resul.setInformacion(res);
         * }
         * 
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> eliminarSolPlantacionForestalObservacion(
            SolPlantacionForestalObservacionEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm = con.
         * prepareCall("{call pa_EliminarSolPlantacionForestalObservacion (?,?,?,?)}");
         * pstm.setObject (1, request.getId_solplantforest(),Types.INTEGER);
         * pstm.setObject (2,
         * request.getId_solplantacionforest_observacion(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> listarSolPlantacionForestalObservacion(
            SolPlantacionForestalObservacionEntity request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultEntity<SolPlantacionForestalObservacionEntity> obtenerSolPlantacionForestalObservacion(
            SolPlantacionForestalObservacionEntity request) {
        ResultEntity resul = new ResultEntity();
        try {

            List<SolPlantacionForestalObservacionEntity> objList = new ArrayList<SolPlantacionForestalObservacionEntity>();

            String sql = "EXEC pa_ObtenerSolPlantacionForestalObservacion "
                    + request.getId_solplantforest()
                    + "," + request.getId_solplantacionforest_observacion();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {
                SolPlantacionForestalObservacionEntity obj = new SolPlantacionForestalObservacionEntity();
                obj.setId_solplantacionforest_observacion((Integer) row.get("NU_ID_SOLPLANTACIONFOREST_OBSERVACION"));
                obj.setId_solplantforest((Integer) row.get("NU_ID_SOLPLANTFOREST"));
                obj.setId_detalleseccion((Integer) row.get("NU_ID_DETALLESECCION"));
                obj.setEvaluacion((Integer) row.get("NU_EVALUACION"));
                obj.setAlerta((Boolean) row.get("BL_ALERTA"));
                obj.setObservacion((String) row.get("TX_OBSERVACION"));
                obj.setEstado((String) row.get("TX_ESTADO"));

                obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_MODIFICACION"));
                obj.setFechaRegistro((Date) row.get("FE_FECHA_MODIFICACION"));
                // resul.setTotalrecord((Integer)row.get("TotalRegistro"));
                objList.add(obj);
            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultClassEntity listarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalArchivoEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalArchivo_Listar");
            processStored.registerStoredProcedureParameter("idSolPlantForestArchivo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForestArchivo", obj.getIdSolPlantForestArchivo());
            processStored.setParameter("idSolPlantForest", obj.getIdSolPlantForest());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                SolPlantacionForestalArchivoEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalArchivoEntity();
                        c.setIdSolPlantForestArchivo(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolPlantForest(row[1] == null ? null : (Integer) row[1]);
                        c.setIdArchivo(row[2] == null ? null : (Integer) row[2]);
                        c.setCodigoSeccion(row[3] == null ? null : (String) row[3]);
                        c.setDescripcionSeccion(row[4] == null ? null : (String) row[4]);
                        c.setCodigoSubSeccion(row[5] == null ? null : (String) row[5]);
                        c.setNombreArchivo(row[6] == null ? null : (String) row[6]);
                        c.setNombreGeneradoArchivo(row[7] == null ? null : (String) row[7]);
                        c.setExtensionArchivo(row[8] == null ? null : (String) row[8]);
                        c.setRutaArchivo(row[9] == null ? null : (String) row[9]);
                        c.setTipoDocumento(row[10] == null ? null : (String) row[10]);
                        c.setConforme(row[11] == null ? null : (Boolean) row[11]);
                        c.setObservacion(row[12] == null ? null : (String) row[12]);
                        c.setTipoArchivo(row[13] == null ? null : (String) row[13]);
                        c.setConformeFiscalizacion(row[14] == null ? null : (Boolean) row[14]);
                        c.setObsFiscalizacion(row[15] == null ? null : (String) row[15]);

                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de archivos de la solicitud de Plantación Forestal correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolPlantacionForestalArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo obtener la lista de archivos de la solicitud de Forestal correctamente.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalArchivo_Registrar");

            processStored.registerStoredProcedureParameter("idSolPlantForestArchivo", Integer.class,
                    ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantForest", obj.getIdSolPlantForest());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idArchivoSolPlantacionForestal = (Integer) processStored
                    .getOutputParameterValue("idSolPlantForestArchivo");
            obj.setIdSolPlantForest(idArchivoSolPlantacionForestal);
            result.setCodigo(idArchivoSolPlantacionForestal);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el archivo de la solicitud de Plantación Forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar el archivo de la solicitud de Plantación Forestal.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalArchivo_Actualizar");

            processStored.registerStoredProcedureParameter("idSolPlantForestArchivo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoArchivo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conformeFiscalizacion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("obsFiscalizacion", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantForestArchivo", obj.getIdSolPlantForestArchivo());
            processStored.setParameter("idSolPlantForest", obj.getIdSolPlantForest());
            processStored.setParameter("idArchivo", obj.getIdArchivo());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("tipoArchivo", obj.getTipoArchivo());
            processStored.setParameter("conforme", obj.getConforme());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("idUsuario", obj.getIdUsuarioModificacion());
            processStored.setParameter("conformeFiscalizacion", obj.getConformeFiscalizacion());
            processStored.setParameter("obsFiscalizacion", obj.getObsFiscalizacion());
            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó el archivo de la solicitud de Plantación Forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo actualizar el archivo de la solicitud de Plantación Forestal.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolPlantacionForestalArchivo(SolPlantacionForestalArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalArchivo_Eliminar");

            processStored.registerStoredProcedureParameter("idSolPlantForestArchivo", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolPlantForestArchivo", obj.getIdSolPlantForestArchivo());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se eliminó el archivo de la solicitud de Plantación Forestal correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo eliminar el archivo de la solicitud de Plantación Forestal.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolPlantacacionForestal(SolicitudPlantacionForestalEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestal_Registrar");

            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class,
                    ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class,
                    ParameterMode.IN);



            processStored.registerStoredProcedureParameter("codigoModalidad", String.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idContrato", request.getIdContrato());
            processStored.setParameter("codigoModalidad", request.getCodigoModalidad());
            processStored.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolPlantForest = (Integer) processStored
                    .getOutputParameterValue("idSolPlantForest");
            request.setIdSolPlantForest(idSolPlantForest);
            result.setCodigo(idSolPlantForest);
            result.setSuccess(true);
            result.setData(request);
            result.setMessage("Se registró la solicitud de Plantación Forestal correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la solicitud de Plantación Forestal.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity listarEspeciesSistemaPlantacion(Integer id) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudPlantacionForestalDetalleEntity> lista = new ArrayList<SolicitudPlantacionForestalDetalleEntity>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalEspecies_Listar");

            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForest", id);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                SolicitudPlantacionForestalDetalleEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolicitudPlantacionForestalDetalleEntity();
                        c.setIdSolPlantForestDetalle(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolPlantForest(row[1] == null ? null : (Integer) row[1]);
                        c.setIdEspecie(row[2] == null ? null : (Integer) row[2]);

                        c.setEspecieNombreComun(row[3] == null ? null : (String) row[3]);
                        c.setEspecieNombreCientifico(row[4] == null ? null : (String) row[4]);

                        c.setTotalArbolesExistentes(row[5] == null ? null : (Integer) row[5]);
                        c.setProduccionEstimada(row[6] == null ? null : (BigDecimal) row[6]);
                        c.setCoordenadaEste(row[7] == null ? null : (String) row[7]);
                        c.setCoordenadaNorte(row[8] == null ? null : (String) row[8]);
                        c.setNumVertice(row[9] == null ? null : (Integer) row[9]);
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de especies de la solicitud de Plantación Forestal correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolPlantacionForestalArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo obtener la lista de especies de la solicitud de Forestal correctamente.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolPlantacionForestalDetalle(List<SolicitudPlantacionForestalDetalleEntity> request) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            for (SolicitudPlantacionForestalDetalleEntity r : request) {
                
                StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalDetalle_Registrar");

                processStored.registerStoredProcedureParameter("idSolPlantForestDetalle", Integer.class,ParameterMode.INOUT);
                processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idEspecie", Integer.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("especieNombreComun", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("especieNombreCientifico", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("totalArbolesExistentes", Integer.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("produccionEstimada", BigDecimal.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("coordenadaEste", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("coordenadaNorte", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("numVertice", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idSolPlantForest", r.getIdSolPlantForest());
                processStored.setParameter("idEspecie", r.getIdEspecie());
                processStored.setParameter("especieNombreComun", r.getEspecieNombreComun());
                processStored.setParameter("especieNombreCientifico", r.getEspecieNombreCientifico());
                processStored.setParameter("totalArbolesExistentes", r.getTotalArbolesExistentes());
                processStored.setParameter("produccionEstimada", r.getProduccionEstimada());
                processStored.setParameter("coordenadaEste", r.getCoordenadaEste());
                processStored.setParameter("coordenadaNorte", r.getCoordenadaNorte());
                processStored.setParameter("idUsuarioRegistro", r.getIdUsuarioRegistro());
                processStored.setParameter("numVertice", r.getNumVertice());
                processStored.execute();

                Integer idSolPlantForestDetalle = (Integer) processStored
                        .getOutputParameterValue("idSolPlantForestDetalle");

                r.setIdSolPlantForestDetalle(idSolPlantForestDetalle);

            }

            result.setSuccess(true);
            result.setData(request);
            result.setMessage("Se registró el detalle de Plantación Forestal correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar el detalle de Plantación Forestal.");
            result.setInnerException(e.getMessage());
            return result;
        }

    }

    @Override
    public ResultClassEntity registrarAreaBloque(AreaBloqueEntity request) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_AreaBloque_Registrar");

            processStored.registerStoredProcedureParameter("idAreaBloque", Integer.class,
                    ParameterMode.INOUT);

            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForestArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idAreaBloque", request.getIdAreaBloque());
            processStored.setParameter("idSolPlantForest", request.getIdSolPlantForest());
            processStored.setParameter("idSolPlantForestArea", request.getIdSolPlantForestArea());
            processStored.setParameter("bloque", request.getBloque());
            processStored.setParameter("area", request.getArea());
            processStored.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());

            processStored.execute();

            Integer idAreaBloque = (Integer) processStored.getOutputParameterValue("idAreaBloque");
            request.setIdAreaBloque(idAreaBloque);

            for (AreaBloqueDetalleEntity r : request.getAreaBloqueDetalle()) {

                StoredProcedureQuery processStoredDetalle = entityManager
                        .createStoredProcedureQuery("dbo.pa_AreaBloqueDetalle_Registrar");

                processStoredDetalle.registerStoredProcedureParameter("idAreaBloqueDetalle", Integer.class,
                        ParameterMode.INOUT);
                processStoredDetalle.registerStoredProcedureParameter("idAreaBloque", Integer.class, ParameterMode.IN);

                processStoredDetalle.registerStoredProcedureParameter("nroVertice", Integer.class, ParameterMode.IN);
                processStoredDetalle.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class,
                        ParameterMode.IN);
                processStoredDetalle.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class,
                        ParameterMode.IN);
                processStoredDetalle.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStoredDetalle.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class,
                        ParameterMode.IN);
                processStoredDetalle.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class,
                        ParameterMode.IN);

                SpUtil.enableNullParams(processStoredDetalle);

                processStoredDetalle.setParameter("idAreaBloqueDetalle", r.getIdAreaBloqueDetalle());
                processStoredDetalle.setParameter("idAreaBloque", idAreaBloque);
                processStoredDetalle.setParameter("nroVertice", r.getNroVertice());
                processStoredDetalle.setParameter("coordenadaEste", r.getCoordenadaEste());
                processStoredDetalle.setParameter("coordenadaNorte", r.getCoordenadaNorte());
                processStoredDetalle.setParameter("observacion", r.getObservacion());
                processStoredDetalle.setParameter("idUsuarioRegistro", r.getIdUsuarioRegistro());
                processStoredDetalle.setParameter("idUsuarioModificacion", r.getIdUsuarioModificacion());

                processStoredDetalle.execute();

                Integer idAreaBloqueDetalle = (Integer) processStoredDetalle
                        .getOutputParameterValue("idAreaBloqueDetalle");

                r.setIdAreaBloqueDetalle(idAreaBloqueDetalle);

            }

            result.setCodigo(idAreaBloque);
            result.setSuccess(true);
            result.setData(request);
            result.setMessage("Se registró la solicitud de Plantación Forestal correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la solicitud de Plantación Forestal.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity listarAreaBloque(AreaBloqueEntity request) {

        ResultClassEntity result = new ResultClassEntity<>();

        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AreaBloque_Listar");

            processStored.registerStoredProcedureParameter("idAreaBloque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idAreaBloque", request.getIdAreaBloque());
            processStored.setParameter("idSolPlantForest", request.getIdSolPlantForest());
            processStored.setParameter("bloque", request.getBloque());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            AreaBloqueEntity bloque = new AreaBloqueEntity();

            if (spResult != null && !spResult.isEmpty() && spResult.size() == 1) {

                for (Object[] row : spResult) {

                    bloque.setIdAreaBloque(row[0] == null ? null : (Integer) row[0]);
                    bloque.setIdSolPlantForest(row[1] == null ? null : (Integer) row[1]);
                    bloque.setIdSolPlantForestArea(row[2] == null ? null : (Integer) row[2]);
                    bloque.setBloque(row[3] == null ? null : (String) row[3]);
                    bloque.setArea(row[4] == null ? null : (BigDecimal) row[4]);

                    StoredProcedureQuery processStored_det = entityManager.createStoredProcedureQuery("dbo.pa_AreaBloqueDetalle_Listar");
                    processStored_det.registerStoredProcedureParameter("idAreaBloque", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored_det);
                    processStored_det.setParameter("idAreaBloque", bloque.getIdAreaBloque());

                    processStored_det.execute();

                    List<Object[]> spResult_det = processStored_det.getResultList();

                    if(spResult_det != null && !spResult_det.isEmpty()){    

                        List<AreaBloqueDetalleEntity> detalle = new ArrayList<AreaBloqueDetalleEntity>();

                        AreaBloqueDetalleEntity d;

                        for (Object[] det : spResult_det) {

                            d = new AreaBloqueDetalleEntity();

                            d.setIdAreaBloqueDetalle(det[0] == null ? null : (Integer) det[0]);
                            d.setNroVertice(det[2] == null ? null : (Integer) det[2]);
                            d.setCoordenadaEste(det[3] == null ? null : (BigDecimal) det[3]);
                            d.setCoordenadaNorte(det[4] == null ? null : (BigDecimal) det[4]);
                            d.setObservacion(det[5] == null ? null : (String) det[5]);

                            detalle.add(d);
                        }

                        bloque.setAreaBloqueDetalle(detalle);
                    }
                }
            }

            result.setSuccess(true);
            result.setData(bloque);
            result.setMessage("Se listo los bloques de área correctamente.");
            return result;
            
        } catch (Exception e) {

            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo Listar los bloques de area.");
            result.setInnerException(e.getMessage());
            return result;

        }
    }




    @Override
    public ResultClassEntity listarListaBloque(AreaBloqueEntity request) {

        ResultClassEntity result = new ResultClassEntity<>();

        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_AreaBloque_Listar");

            processStored.registerStoredProcedureParameter("idAreaBloque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idAreaBloque", request.getIdAreaBloque());
            processStored.setParameter("idSolPlantForest", request.getIdSolPlantForest());
            processStored.setParameter("bloque", request.getBloque());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            List<AreaBloqueEntity> listaBloques = new ArrayList<>();
            AreaBloqueEntity bloque = new AreaBloqueEntity();

            if (spResult != null && !spResult.isEmpty()) {

                for (Object[] row : spResult) {

                    bloque = new AreaBloqueEntity();
                    bloque.setIdAreaBloque(row[0] == null ? null : (Integer) row[0]);
                    bloque.setIdSolPlantForest(row[1] == null ? null : (Integer) row[1]);
                    bloque.setIdSolPlantForestArea(row[2] == null ? null : (Integer) row[2]);
                    bloque.setBloque(row[3] == null ? null : (String) row[3]);
                    bloque.setArea(row[4] == null ? null : (BigDecimal) row[4]);

                    StoredProcedureQuery processStored_det = entityManager.createStoredProcedureQuery("dbo.pa_AreaBloqueDetalle_Listar");
                    processStored_det.registerStoredProcedureParameter("idAreaBloque", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored_det);
                    processStored_det.setParameter("idAreaBloque", bloque.getIdAreaBloque());

                    processStored_det.execute();

                    List<Object[]> spResult_det = processStored_det.getResultList();

                    if(spResult_det != null && !spResult_det.isEmpty()){    

                        List<AreaBloqueDetalleEntity> detalle = new ArrayList<AreaBloqueDetalleEntity>();

                        AreaBloqueDetalleEntity d;

                        for (Object[] det : spResult_det) {

                            d = new AreaBloqueDetalleEntity();

                            d.setIdAreaBloqueDetalle(det[0] == null ? null : (Integer) det[0]);
                            d.setNroVertice(det[2] == null ? null : (Integer) det[2]);
                            d.setCoordenadaEste(det[3] == null ? null : (BigDecimal) det[3]);
                            d.setCoordenadaNorte(det[4] == null ? null : (BigDecimal) det[4]);
                            d.setObservacion(det[5] == null ? null : (String) det[5]);

                            detalle.add(d);
                        }

                        bloque.setAreaBloqueDetalle(detalle);
                    }

                    listaBloques.add(bloque);
                }
            }

            result.setSuccess(true);
            result.setData(listaBloques);
            result.setMessage("Se listo los bloques de área correctamente.");
            return result;
            
        } catch (Exception e) {

            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo Listar los bloques de area.");
            result.setInnerException(e.getMessage());
            return result;

        }
    }

    @Override
    public ResultClassEntity registrarSolPlantacionForestalPersona(List<SolPlantacionForestalPersonaEntity> request) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            for (SolPlantacionForestalPersonaEntity r : request) {

                StoredProcedureQuery processStored = entityManager
                        .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalPersona_Registrar");

                processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("nombres", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("apellidoPaterno", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("apellidoMaterno", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistra", Integer.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("dni", String.class,ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idSolPlantForestPersona", Integer.class,ParameterMode.OUT);

                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idSolPlantForest", r.getIdSolPlantacionForestal());
                processStored.setParameter("nombres", r.getNombres());
                processStored.setParameter("apellidoPaterno", r.getApellidoPaterno());
                processStored.setParameter("apellidoMaterno", r.getApellidoMaterno());
                processStored.setParameter("idUsuarioRegistra", r.getIdUsuarioRegistro());
                processStored.setParameter("dni", r.getDni());
                processStored.execute();

                Integer idSolPlantacionForestalPersona  = (Integer) processStored
                        .getOutputParameterValue("idSolPlantForestPersona");

                r.setIdSolPlantacionForestalPersona(idSolPlantacionForestalPersona);

            }

            result.setSuccess(true);
            result.setData(request);
            result.setMessage("Se registró el Solicitud Plantación Forestal Persona correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la Solicitud de Plantación Forestal Persona.");
            result.setInnerException(e.getMessage());
            return result;
        }

    }



    @Override
    public ResultClassEntity listarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolPlantacionForestalPersonaEntity> lista = new ArrayList<SolPlantacionForestalPersonaEntity>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_SolPlantacionForestalPersona_Listar");

            processStored.registerStoredProcedureParameter("idSolPlantForest", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolPlantForest", request.getIdSolPlantacionForestal());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {

                SolPlantacionForestalPersonaEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolPlantacionForestalPersonaEntity();
                        c.setIdSolPlantacionForestalPersona(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolPlantacionForestal(row[1] == null ? null : (Integer) row[1]);
                        c.setNombres(row[2] == null ? null : (String) row[2]);

                        c.setApellidoPaterno(row[3] == null ? null : (String) row[3]);
                        c.setApellidoMaterno(row[4] == null ? null : (String) row[4]);
                        c.setIdUsuarioRegistro(row[5] == null ? null : (Integer) row[5]);
                        c.setFechaRegistro(row[6] == null ? null : (Date) row[6]);
                        c.setDni(row[7] == null ? null : (String) row[7]);

                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de personas de la solicitud de Plantación Forestal correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolPlantacionForestalArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo obtener la lista de personas de la solicitud de Forestal correctamente.");
            return result;
        }
    }


    @Override
    public ResultClassEntity eliminarSolPlantacionForestalPersona(SolPlantacionForestalPersonaEntity request)
            {

        ResultClassEntity result = new ResultClassEntity();

        try{
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("[dbo].[pa_SolPlantacionForestalPersona_Eliminar]");
            processStored.registerStoredProcedureParameter("idSolPlantForestPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);


            processStored.setParameter("idSolPlantForestPersona", request.getIdSolPlantacionForestalPersona());
            processStored.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());

            processStored.execute();

            result.setData(request);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");

            return result;

        } catch (Exception e) {

            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }

}
