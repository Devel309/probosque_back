package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.sql.DataSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.repository.UnidadFisiograficaRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;

@Repository
public class UnidadFisiograficaRepositoryImpl extends JdbcDaoSupport implements UnidadFisiograficaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(UnidadFisiograficaRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    @Autowired
    private FileServerConexion fileServerConexion;
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ArchivoRepositoryImpl respositoryArchivo;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity listarUnidadFisiografica(Integer idInfBasica) {
        ResultClassEntity result = new ResultClassEntity();
        List<UnidadFisiograficaUMFEntity> list = new ArrayList<UnidadFisiograficaUMFEntity>();
        try {
            StoredProcedureQuery ps = entityManager.createStoredProcedureQuery("dbo.pa_UnidadFisiografica_Listar");
            ps.registerStoredProcedureParameter("NU_ID_INFBASICA", Integer.class, ParameterMode.IN);
            ps.setParameter("NU_ID_INFBASICA", idInfBasica);
            ps.execute();
            List<Object[]> spResult = ps.getResultList();
            for (Object[] row : spResult) {
                InformacionBasicaEntity ib = new InformacionBasicaEntity();
                UnidadFisiograficaUMFEntity uf = new UnidadFisiograficaUMFEntity();
                ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
                ib.setIdInfBasica((Integer) row[0]);
                uf.setInformacionBasica(ib);
                uf.setIdUnidadFisiografica((Integer) row[1]);
                BigDecimal area = (BigDecimal) row[2];
                BigDecimal porcentaje = (BigDecimal) row[3];
                uf.setArea((Double) area.doubleValue());
                uf.setPorcentaje((Double) porcentaje.doubleValue());
                uf.setFile((Boolean) row[4]);
                uf.setAccion((Boolean) row[5]);
                uf.setDescripcion((String) row[6]);
                resultArchivo.setNombeArchivoGenerado((String) row[7]);
                resultArchivo.setCodigo((Integer) row[10]);
                if ((String) row[7] != null) {
                    String nombreArchivoGenerado = ((String) row[7]).concat(SEPARADOR_ARCHIVO).concat((String) row[8]);
                    String nombreArchivo = ((String) row[9]).concat(SEPARADOR_ARCHIVO).concat((String) row[8]);
                    byte[] byteFile = fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                    resultArchivo.setArchivo(byteFile);
                    resultArchivo.setNombeArchivo(nombreArchivo);
                    resultArchivo.setContenTypeArchivo("application/octet-stream");
                }
                uf.setArchivo(resultArchivo);
                list.add(uf);
            }
            result.setSuccess(true);
            result.setMessage("Se listó correctamente.");
            result.setData(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
        }
        return result;
    }

    @Override
    public ResultClassEntity registrarUnidadFisiograficaArchivo(UnidadFisiograficaUMFEntity item, MultipartFile file) {
        ResultClassEntity result = new ResultClassEntity();
        ResultEntity<ArchivoEntity> archivo = null;
        try {
            if (file != null) {
                ArchivoEntity request = new ArchivoEntity();
                String archivoGenerado = fileServerConexion.uploadFile(file);
                request.setIdArchivo(0);
                request.setEstado(item.getEstado());
                request.setIdUsuarioModificacion(item.getIdUsuarioModificacion());
                request.setIdUsuarioRegistro(item.getIdUsuarioRegistro());
                request.setNombreGenerado(FilenameUtils.getBaseName(archivoGenerado));
                request.setNombre(FilenameUtils.getBaseName(file.getOriginalFilename()));
                request.setExtension(FilenameUtils.getExtension(archivoGenerado));
                request.setRuta(fileServerPath.concat(archivoGenerado));
                request.setTipoDocumento("1");

                archivo = this.respositoryArchivo.registroArchivo(request);
            }
            if(archivo == null){

            }
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_UnidadFisiograficaArchivo_upsert");
            processStored.registerStoredProcedureParameter("idUnidadFisiografica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("area", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("porcentaje", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("file", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idUnidadFisiografica", item.getIdUnidadFisiografica());
            processStored.setParameter("idArchivo", archivo == null ? 0 : archivo.getCodigo());
            processStored.setParameter("idInfBasica", item.getInformacionBasica().getIdInfBasica());
            processStored.setParameter("area", item.getArea());
            processStored.setParameter("porcentaje", item.getPorcentaje());
            processStored.setParameter("accion", item.getAccion());
            processStored.setParameter("file", item.getAccion());
            processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarUnidadFisiograficaArchivo(Integer idInfBasica,Integer idUnidadFisiografica,Integer idUsuario) {
        ResultClassEntity result = new ResultClassEntity();
        try {
        StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_UnidFisiograficaArchivo_Eliminar");
        processStored_.registerStoredProcedureParameter("idInfBasica", Integer.class, ParameterMode.IN);
        processStored_.registerStoredProcedureParameter("idUnidadFisiografica", Integer.class, ParameterMode.IN);
        processStored_.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
        processStored_.setParameter("idInfBasica", idInfBasica);
        processStored_.setParameter("idUnidadFisiografica", idUnidadFisiografica);
        processStored_.setParameter("idUsuario", idUsuario);
            processStored_.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó ordenamiento de área de manejo.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }
    }
}
