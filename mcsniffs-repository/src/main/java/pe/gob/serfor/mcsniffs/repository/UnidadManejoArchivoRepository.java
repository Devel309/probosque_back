package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoArchivoEntity;

import java.util.List;

public interface UnidadManejoArchivoRepository {

    ResultClassEntity RegistrarUnidadManejoArchivo(UnidadManejoArchivoEntity p);
    ResultClassEntity EliminarUnidadManejoArchivo(UnidadManejoArchivoEntity unidadManejoArchivoEntity) throws Exception;
    ResultClassEntity ListarUnidadManejoArchivo( UnidadManejoArchivoEntity request);
}
