package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.PropertiesEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadAprovechamientoEntity;

public interface UnidadAprovechamientoRepository {
    ResultClassEntity guardarUnidadAprovechamiento(UnidadAprovechamientoEntity obj) throws Exception;
    ResultClassEntity<UnidadAprovechamientoEntity> obtenerUnidadAprovechamiento(Integer ObjectId);
    ResultEntity<PropertiesEntity> listar();
    ResultEntity<PropertiesEntity> buscarByIdProcesoOferta(Integer id);
    ResultEntity<PropertiesEntity> buscarByIdPlanManejo(Integer id);
}
