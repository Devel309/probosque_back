package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.OficinaRegionalEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;

import java.util.List;

public interface OficinaRegionalRepository {
    List<OficinaRegionalEntity> listarOficinaRegional();
}
