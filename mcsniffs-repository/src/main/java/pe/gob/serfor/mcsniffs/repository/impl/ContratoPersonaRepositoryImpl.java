package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoPersonaDto;
import pe.gob.serfor.mcsniffs.repository.ContratoPersonaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ContratoPersonaRepositoryImpl extends JdbcDaoSupport implements ContratoPersonaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ContratoPersonaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    

    @Override
    public ResultClassEntity registrarContratoPersona(ContratoPersonaDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ContratoPersona_Registrar");
            processStored.registerStoredProcedureParameter("idContratoPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaterno", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("razonSocial", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoRelacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idContratoPersona", dto.getIdContratoPersona());
            processStored.setParameter("idContrato", dto.getIdContrato());
            processStored.setParameter("tipoPersona", dto.getTipoPersona());
            processStored.setParameter("tipoDocumento", dto.getTipoDocumento());
            processStored.setParameter("numeroDocumento", dto.getNumeroDocumento());
            processStored.setParameter("nombres", dto.getNombres());
            processStored.setParameter("apellidoPaterno", dto.getApellidoPaterno());
            processStored.setParameter("apellidoMaterno", dto.getApellidoMaterno());
            processStored.setParameter("razonSocial", dto.getRazonSocial());
            processStored.setParameter("tipoRelacion", dto.getTipoRelacion());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();
 

            result.setSuccess(true);
            result.setMessage("Se registró contrato persona correctamente.");

            
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }


    @Override
    public List<ContratoPersonaDto> listarContratoPersona(ContratoPersonaDto dto) throws Exception {
         
        List<ContratoPersonaDto> lista = new ArrayList<ContratoPersonaDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ContratoPersona_Listar");            
            processStored.registerStoredProcedureParameter("idContratoPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContratoPersona",dto.getIdContratoPersona());
            processStored.setParameter("idContrato",dto.getIdContrato());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ContratoPersonaDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new ContratoPersonaDto();

                        temp.setIdContratoPersona(row[0]==null?null:(Integer) row[0]);
                        temp.setIdContrato(row[1]==null?null:(Integer) row[1]);
                        temp.setTipoPersona(row[2]==null?null:(String) row[2]);
                        temp.setTipoDocumento(row[3]==null?null:(String) row[3]);
                        temp.setNumeroDocumento(row[4]==null?null:(String) row[4]);
                        temp.setNombres(row[5]==null?null:(String) row[5]);
                        temp.setApellidoPaterno(row[6]==null?null:(String) row[6]);
                        temp.setApellidoMaterno(row[7]==null?null:(String) row[7]);
                        temp.setRazonSocial(row[8]==null?null:(String) row[8]);
                        temp.setTipoRelacion(row[9]==null?null:(String) row[9]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarContratoPersona", e.getMessage());
            throw e;
        }
    } 

 
}
