package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ProcedimientoAdministrativo.ProcedimientoAdministrativoDto;

public interface ProcedimientoAdministrativoRepository {
    ResultClassEntity listarProcedimientoAdministrativo(ProcedimientoAdministrativoDto dto) throws Exception;
}
