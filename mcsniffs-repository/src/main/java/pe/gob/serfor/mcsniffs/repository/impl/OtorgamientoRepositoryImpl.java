package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.OtorgamientoRepository;
import pe.gob.serfor.mcsniffs.repository.RegenteRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OtorgamientoRepositoryImpl extends JdbcDaoSupport implements OtorgamientoRepository {

    private static final Logger log = LogManager.getLogger(OtorgamientoRepositoryImpl.class);

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Autowired
    RegenteRepository regente;

    @Autowired
    private ArchivoRepository fileUp;

    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado: Danny Nazario [23-12-2021]
     * @descripción: {Registrar Otorgamiento}
     *
     */
    @Override
    public ResultClassEntity registrarOtorgamiento(OtorgamientoEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Otorgamiento_Registrar");
            sp.registerStoredProcedureParameter("idOtorgamiento", Integer.class, ParameterMode.INOUT);
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codPermiso", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codOtorgamiento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroRuc", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroTituloPropiedad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroPartidaRegistral", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroActaAsambleaRrll", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroOperacionComprobantePago", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroActaAcuerdoAsamblea", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoAprovechamiento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroContratoTercero", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroActaAsamblea", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("escalaManejo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("areaComunidad", BigDecimal.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            sp.setParameter("idOtorgamiento", obj.getIdOtorgamiento());
            sp.setParameter("codPermiso", obj.getCodPermiso());
            sp.setParameter("codOtorgamiento", obj.getCodOtorgamiento());
            sp.setParameter("codTipo", obj.getCodTipo());
            sp.setParameter("estadoSolicitud", obj.getEstadoSolicitud());
            sp.setParameter("nroRuc", obj.getNroRuc());
            sp.setParameter("nroTituloPropiedad", obj.getNroTituloPropiedad());
            sp.setParameter("nroPartidaRegistral", obj.getNroPartidaRegistral());
            sp.setParameter("nroActaAsambleaRrll", obj.getNroActaAsambleaRrll());
            sp.setParameter("nroOperacionComprobantePago", obj.getNroOperacionComprobantePago());
            sp.setParameter("nroActaAcuerdoAsamblea", obj.getNroActaAcuerdoAsamblea());
            sp.setParameter("codTipoAprovechamiento", obj.getCodTipoAprovechamiento());
            sp.setParameter("nroContratoTercero", obj.getNroContratoTercero());
            sp.setParameter("nroActaAsamblea", obj.getNroActaAsamblea());
            sp.setParameter("escalaManejo", obj.getEscalaManejo());
            sp.setParameter("descripcion", obj.getDescripcion());
            sp.setParameter("detalle", obj.getDetalle());
            sp.setParameter("observacion", obj.getObservacion());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.setParameter("areaComunidad", obj.getAreaComunidad());
            sp.execute();
            Integer id = (Integer) sp.getOutputParameterValue("idOtorgamiento");
            //Regente
            ResultClassEntity rege = new ResultClassEntity<>();
            if (obj.getRegente() != null) {
                rege = regente.RegistrarRegente(obj.getRegente());
            }

            result.setCodigo(id);
            result.setSuccess(true);
            result.setMessage("Se registró otorgamiento.");
            result.setMessageExeption(rege.getMessage());
            return  result;
        } catch (Exception e) {
            log.error("registrarOtorgamiento - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Danny Nazario [08-12-2021]
     * @modificado: Danny Nazario [23-12-2021]
     * @descripción: {Listar Otorgamiento}
     *
     */
    @Override
    public ResultClassEntity listarOtorgamiento(OtorgamientoEntity filtro) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Otorgamiento_Listar");
            sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codPermiso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPermisoForestal", filtro.getIdPermisoForestal());
            sp.setParameter("codPermiso", filtro.getCodPermiso());
            sp.execute();

            List<Object[]> spResult = sp.getResultList();
            List<OtorgamientoEntity> list = new ArrayList<>();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    OtorgamientoEntity obj = new OtorgamientoEntity();
                    obj.setIdOtorgamiento(row[0] == null ? null : (Integer) row[0]);
                    obj.setIdPermisoForestal(row[1] == null ? null : (Integer) row[1]);
                    obj.setCodPermiso(row[2] == null ? null : (String) row[2]);
                    obj.setCodOtorgamiento(row[3] == null ? null : (String) row[3]);
                    obj.setCodTipo(row[4] == null ? null : (String) row[4]);
                    obj.setEstadoSolicitud(row[5] == null ? null : (String) row[5]);
                    obj.setNroRuc(row[6] == null ? null : (String) row[6]);
                    obj.setNroTituloPropiedad(row[7] == null ? null : (String) row[7]);
                    obj.setNroPartidaRegistral(row[8] == null ? null : (String) row[8]);
                    obj.setNroActaAsambleaRrll(row[9] == null ? null : (String) row[9]);
                    obj.setNroOperacionComprobantePago(row[10] == null ? null : (String) row[10]);
                    obj.setNroActaAcuerdoAsamblea(row[11] == null ? null : (String) row[11]);
                    obj.setCodTipoAprovechamiento(row[12] == null ? null : (String) row[12]);
                    obj.setNroContratoTercero(row[13] == null ? null : (String) row[13]);
                    obj.setNroActaAsamblea(row[14] == null ? null : (String) row[14]);
                    obj.setEscalaManejo(row[15] == null ? null : (String) row[15]);
                    obj.setDescripcion(row[16] == null ? null : (String) row[16]);
                    obj.setDetalle(row[17] == null ? null : (String) row[17]);
                    obj.setObservacion(row[18] == null ? null : (String) row[18]);
                    obj.setAreaComunidad(row[19] == null ? null : (BigDecimal) row[19]);
                    //Regente
                    RegenteEntity rege = new RegenteEntity();
                    rege.setIdPlanManejo(obj.getIdPermisoForestal());
                    rege.setCodigoProceso(obj.getCodPermiso());
                    ResultEntity<RegenteEntity> listRegente= regente.ListarRegente(rege);
                    if (listRegente.getData() != null) {
                        if (listRegente.getData().size() > 0) {
                            obj.setRegente(listRegente.getData().get(0));
                        }
                    }
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó otorgamiento.");
            return result;
        } catch (Exception e) {
            log.error("listarOtorgamiento - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Danny Nazario [09-12-2021]
     * @modificado: Danny Nazario [10-12-2021]
     * @descripción: {Eliminar cabecera y detalle de Otorgamiento}
     * @param: OtorgamientoEntity
     */

    @Override
    public ResultClassEntity eliminarOtorgamiento(OtorgamientoEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("[dbo].pa_Otorgamiento_Eliminar");
            sp.registerStoredProcedureParameter("idOtorgamiento", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idOtorgamiento", obj.getIdOtorgamiento());
            sp.setParameter("idUsuarioElimina", obj.getIdUsuarioElimina());
            sp.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return result;
        } catch (Exception e) {
            log.error("eliminarOtorgamiento - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Danny Nazario [23-12-2021]
     * @modificado: Danny Nazario [21-01-2022]
     * @descripción: {Listar Otorgamiento por filtro}
     * @param: OtorgamientoEntity
     */
    @Override
    public ResultClassEntity listarPorFiltroOtorgamiento(String nroDocumento, String nroRucEmpresa, Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<OtorgamientoEntity> list = new ArrayList<>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Otorgamiento_ListarPorFiltro");
            sp.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroRucEmpresa", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("nroDocumento", nroDocumento);
            sp.setParameter("nroRucEmpresa", nroRucEmpresa);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.execute();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    OtorgamientoEntity obj = new OtorgamientoEntity();
                    obj.setIdOtorgamiento(row[0] == null ? null : (Integer) row[0]);
                    obj.setIdPermisoForestal(row[1] == null ? null : (Integer) row[1]);
                    obj.setCodPermiso(row[2] == null ? null : (String) row[2]);
                    obj.setNroTituloPropiedad(row[3] == null ? null : (String) row[3]);
                    obj.setAreaComunidad(row[4] == null ? null : (BigDecimal) row[4]);
                    InformacionGeneralPermisoForestalEntity info = new InformacionGeneralPermisoForestalEntity();
                    info.setNombreElaborador(row[5] == null ? null : (String) row[5]);
                    info.setApellidoPaternoElaborador(row[6] == null ? null : (String) row[6]);
                    info.setApellidoMaternoElaborador(row[7] == null ? null : (String) row[7]);
                    info.setRucComunidad(row[8] == null ? null : (String) row[8]);
                    info.setFederacionComunidad(row[9] == null ? null : (String) row[9]);
                    info.setUbigeoTitular(row[10] == null ? null : (String) row[10]);
                    info.setDistritoTitular(row[11] == null ? null : (String) row[11]);
                    info.setProvinciaTitular(row[12] == null ? null : (String) row[12]);
                    info.setDepartamentoTitular(row[13] == null ? null : (String) row[13]);
                    info.setIdDistrito(row[14] == null ? null : (Integer) row[14]);
                    info.setIdProvincia(row[15] == null ? null : (Integer) row[15]);
                    info.setIdDepartamento(row[16] == null ? null : (Integer) row[16]);
                    info.setCodDistrito(row[17] == null ? null : (String) row[17]);
                    info.setCodProvincia(row[18] == null ? null : (String) row[18]);
                    info.setCodDepartamento(row[19] == null ? null : (String) row[19]);
                    info.setCodTipoDocumentoElaborador(row[20] == null ? null : (String) row[20]);
                    info.setTipoDocumentoElaborador(row[21] == null ? null : (String) row[21]);
                    info.setDocumentoElaborador(row[22] == null ? null : (String) row[22]);
                    info.setCodTipoPersona(row[23] == null ? null : (String) row[23]);
                    info.setTipoPersona(row[24] == null ? null : (String) row[24]);
                    obj.setInformacionGeneral(info);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó otorgamiento correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarPorFiltroOtorgamiento - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
