package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudOpinionEntity;

import java.util.List;

public interface SolicitudOpinionRepository {

    ResultClassEntity ListarSolicitudOpinion(SolicitudOpinionDto param);

    ResultClassEntity RegistrarSolicitudOpinion(SolicitudOpinionDto param);

    ResultClassEntity ActualizarSolicitudOpinion(SolicitudOpinionDto param);

    ResultClassEntity ActualizarSolicitudOpinionArchivo(SolicitudOpinionDto param);

    ResultClassEntity EliminarSolicitudOpinionArchivo(SolicitudOpinionDto param);

    ResultClassEntity ObtenerSolicitudOpinion(SolicitudOpinionDto param);

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {CRUD DE LA SOLICITUD OPINION}
     * @param:SolicitudOpinionEntity
     */

    ResultClassEntity RegistrarSolicitudOpinionEvaluacion(List<SolicitudOpinionEntity> list) throws Exception;
    List<SolicitudOpinionEntity> ListarSolicitudOpinionEvaluacion(Integer idPlanManejo, String codSolicitud,String subCodSolicitud) throws Exception;
    ResultClassEntity EliminarSolicitudOpinionEvaluacion(SolicitudOpinionEntity solicitudOpinionEntity) throws Exception;

}
