package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ClonarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;

public interface SolicitudConcesionRepository {
    ResultClassEntity listarSolicitudConcesionPorFiltro(SolicitudConcesionDto obj);

    ResultClassEntity listarSolicitudConcesion(SolicitudConcesionDto obj);

    ResultClassEntity registrarSolicitudConcesion(SolicitudConcesionDto obj) throws Exception;

    ResultClassEntity actualizarSolicitudConcesion(SolicitudConcesionDto obj);

    ResultClassEntity eliminarSolicitudConcesion(SolicitudConcesionDto obj);

    ResultClassEntity listarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj);

    ResultClassEntity registrarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj);

    ResultClassEntity actualizarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj);

    ResultClassEntity eliminarSolicitudConcesionArchivo(SolicitudConcesionArchivoEntity obj);

    ResultClassEntity registrarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto);

    List<SolicitudConcesionAnexoDto> listarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto);

    ResultClassEntity eliminarSolicitudConcesionAnexo(SolicitudConcesionAnexoDto dto);

    ResultClassEntity clonarSolicitudConcesion(ClonarSolicitudConcesionDto dto);

    ResultClassEntity listarSolicitudConcesionCalificacionPorFiltro(SolicitudConcesionCalificacionDto obj);
}
