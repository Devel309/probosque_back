package pe.gob.serfor.mcsniffs.repository.impl;

import org.springframework.beans.factory.annotation.Value;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.OrdenamientoAreaManejoDto;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoAreaManejoRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrdenamientoAreaManejoRepositoryImpl extends JdbcDaoSupport implements OrdenamientoAreaManejoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(OrdenamientoAreaManejoRepositoryImpl.class);
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    @Autowired
    private FileServerConexion fileServerConexion;
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista categoria de ordenamiento de Parametos}
     * @param:ParametroEntity
     */
    @Override
    public List<Dropdown> ComboCategoriaOrdenamiento(ParametroEntity param) throws Exception  {
        return null;
        /*
        try {
            List<ParametroEntity> listPar = new ArrayList<>();

            List<Dropdown> result = new ArrayList<Dropdown>();
            List<ParametroEntity> detalle = new ArrayList<ParametroEntity>();
            List<ParametroEntity> cabecera = new ArrayList<ParametroEntity>();

            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Parametro_ListarPorFiltro (?,?,?,?,?)}");
            pstm.setObject (1, param.getCodigo(), Types.VARCHAR);
            pstm.setObject(2, param.getValorPrimario(),Types.VARCHAR);
            pstm.setObject(3, param.getValorSecundario(),Types.VARCHAR);
            pstm.setObject (4, param.getIdTipoParametro(), Types.INTEGER);
            pstm.setObject(5, param.getIdParametroPadre(),Types.INTEGER);
            ResultSet rs = pstm.executeQuery();

            while(rs.next()){
                ParametroEntity temp = new ParametroEntity();
                temp.setCodigo(rs.getString(1));
                temp.setValorPrimario(rs.getString(2));
                temp.setValorSecundario(rs.getString(3));
                temp.setIdTipoParametro(rs.getInt(4));
                temp.setIdParametroPadre(rs.getInt(5));
                if(rs.getInt(5)==0){
                    cabecera.add(temp);
                }
                else{
                    detalle.add(temp);
                }


            }
            for (ParametroEntity customer : cabecera) {
                Dropdown obj = new Dropdown();
                obj.setLabel(customer.getValorPrimario());
                obj.setValue(customer.getValorSecundario());
                obj.setValor(Integer.parseInt(customer.getValorSecundario()));
                List<Dropdown.item> list = new ArrayList<Dropdown.item>();
                for (ParametroEntity customer_ : detalle) {
                    if (obj.getValor().equals(customer_.getIdParametroPadre())) {
                        Dropdown.item item = new Dropdown.item();
                        item.setLabel(customer_.getValorPrimario());
                        item.setValue(customer_.getValorSecundario());
                        item.setValor(Integer.parseInt(customer_.getValorSecundario()));
                        list.add(item);
                    }
                }
                obj.setItems(list);
                result.add(obj);
            }
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e); log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        */
    }

    @Override
    public ResultClassEntity RegistrarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity p,MultipartFile file) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoAreaManejoEntity> list = new ArrayList<OrdenamientoAreaManejoEntity>();

        try {
         //   for(OrdenamientoAreaManejoEntity p:param){{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoAreaManejo_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idOrdAreaManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("catOrdenamiento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("area", Double.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("porcentaje", Double.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("file", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                processStored.setParameter("idOrdAreaManejo", p.getIdOrdAreaManejo());
                processStored.setParameter("catOrdenamiento", p.getCatOrdenamiento());
                processStored.setParameter("area", p.getArea());
                processStored.setParameter("porcentaje", p.getPorcentaje());
                processStored.setParameter("accion", p.getAccion());
                processStored.setParameter("file", p.getAccion());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.setParameter("estado", p.getEstado());
                processStored.execute();
                OrdenamientoAreaManejoEntity obj = new OrdenamientoAreaManejoEntity();
                List<Object[]> spResult =processStored.getResultList();
                ArchivoEntity archivo = new ArchivoEntity();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        PlanManejoEntity pla = new PlanManejoEntity();
                        pla.setIdPlanManejo((Integer) row[0]);
                        obj.setPlanManejo(pla);
                        obj.setIdOrdAreaManejo((Integer) row[1]);
                        obj.setCatOrdenamiento((String) row[2]);
                        BigDecimal bd=(BigDecimal) row[3];
                        BigDecimal bd_=(BigDecimal)row[4];
                        obj.setArea((Double) bd.doubleValue());
                        obj.setPorcentaje((Double) bd_.doubleValue());
                        obj.setAccion((Boolean) row[5]);
                        obj.setFile((Boolean) row[6]);

                        String nombreGenerado = fileServerConexion.uploadFile(file);
                        archivo.setRuta(fileServerPath + ( !nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()) ) ;
                        archivo.setExtension(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),file.getOriginalFilename().length()));
                        archivo.setNombre(file.getOriginalFilename());
                        archivo.setNombreGenerado((!nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()));


                        archivo.setTipoDocumento("1");
                        StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoAreaManejoArchivo_Registrar");
                        processStored_.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("idOrdAreaManejo", Integer.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                        processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                        processStored_.setParameter("tipoDocumento", archivo.getTipoDocumento());
                        processStored_.setParameter("idOrdAreaManejo",obj.getIdOrdAreaManejo());
                        processStored_.setParameter("ruta", archivo.getRuta());
                        processStored_.setParameter("nombre", archivo.getNombre());
                        processStored_.setParameter("nombreGenerado", archivo.getNombreGenerado());
                        processStored_.setParameter("extension", archivo.getExtension());
                        processStored_.setParameter("estado", p.getEstado());
                        processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                        processStored_.execute();

                    }
                }
            result.setSuccess(true);
            result.setMessage("Se registró Ordenamiento del Área de Manejo correctamente.");
            result.setData(archivo);
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarOrdenamientoAreaManejo(PlanManejoEntity param) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoAreaManejoDto> list = new ArrayList<OrdenamientoAreaManejoDto>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoAreaManejo_Configurar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoParametro", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoParametro", param.getCodigoParametro());
            processStored.execute();
            pstm = con.prepareCall("{call pa_OrdenamientoAreaManejo_Listar (?,?)}");
            pstm.setObject (1, param.getIdPlanManejo(), Types.INTEGER);
            pstm.setObject (2, param.getCodigoParametro(), Types.NVARCHAR);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                OrdenamientoAreaManejoDto o=new OrdenamientoAreaManejoDto();
                PlanManejoEntity p = new PlanManejoEntity();
                p.setIdPlanManejo(rs.getInt(1));
                o.setPlanManejo(p);
                o.setIdOrdAreaManejo(rs.getInt(2));
                o.setCatOrdenamiento(rs.getString(3));
                o.setArea(rs.getDouble(4));
                o.setPorcentaje(rs.getDouble(5));
                o.setAccion(rs.getBoolean(6));
                o.setFile(rs.getBoolean(7));
                o.setDescripcion(rs.getString(8));

                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoAreaManejoArchivo_Obtener");
                processStored_.registerStoredProcedureParameter("idOrdAreaManejo", Integer.class, ParameterMode.IN);
                processStored_.setParameter("idOrdAreaManejo", o.getIdOrdAreaManejo());
                processStored_.execute();

                List<Object[]> spResult_ =processStored_.getResultList();
                ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {

                        String nombreArchivoGenerado = (String) row_[0];
                        String nombreArchivo = (String) row_[2];
                        byte[] byteFile =fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                        resultArchivo.setArchivo(byteFile);
                        resultArchivo.setNombeArchivo(nombreArchivo);
                        resultArchivo.setContenTypeArchivo("application/octet-stream");
                        resultArchivo.setSuccess(true);
                        resultArchivo.setMessage("Se descargo el archivo del file server  con éxito.");
                    }

                    o.setResultArchivo(resultArchivo);
                }

                list.add(o);
            }
            pstm.close();
            con.close();
            if (list.size() ==0){
                result.setSuccess(true);
                result.setMessage("No se encontraron datos.");
                result.setData(list);
                return result;
            }
            result.setSuccess(true);
            result.setMessage("Se obtuvo ordenamiento de area de manejo.");
            result.setData(list);
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }
        */
    }

    @Override
    public ResultClassEntity EliminarOrdenamientoAreaManejo(OrdenamientoAreaManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
        StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoAreaManejo_Eliminar");
        processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored_.registerStoredProcedureParameter("idOrdAreaManejo", Integer.class, ParameterMode.IN);
        processStored_.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        processStored_.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
        processStored_.setParameter("idOrdAreaManejo", param.getIdOrdAreaManejo());
        processStored_.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored_.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó Ordenamiento de Área de Manejo correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }
    }

    @Override
    public ResultClassEntity ObtenerOrdenamientoAreaManejoArchivo(OrdenamientoAreaManejoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoAreaManejoDto> list = new ArrayList<OrdenamientoAreaManejoDto>();
        try {

                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoAreaManejoArchivo_Obtener");
                processStored_.registerStoredProcedureParameter("idOrdAreaManejo", Integer.class, ParameterMode.IN);
                processStored_.setParameter("idOrdAreaManejo", param.getIdOrdAreaManejo());
                processStored_.execute();

                List<Object[]> spResult_ =processStored_.getResultList();
                ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {

                        String nombreArchivoGenerado = ((String) row_[0]).
                                concat(SEPARADOR_ARCHIVO).
                                concat((String) row_[1]);
                        String nombreArchivo = ((String) row_[2]).
                                concat(SEPARADOR_ARCHIVO).
                                concat((String) row_[1]);
                        byte[] byteFile =fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                        resultArchivo.setArchivo(byteFile);
                        resultArchivo.setNombeArchivo(nombreArchivo);
                        resultArchivo.setContenTypeArchivo("application/octet-stream");
                        resultArchivo.setSuccess(true);
                        resultArchivo.setMessage("Se descargo el archivo del file server  con éxito.");
                    }


                }

            result.setSuccess(true);
            result.setMessage("Se obtuvo archivo.");
            result.setData(resultArchivo);
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setInnerException(e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return  result;
        }
    }


}
