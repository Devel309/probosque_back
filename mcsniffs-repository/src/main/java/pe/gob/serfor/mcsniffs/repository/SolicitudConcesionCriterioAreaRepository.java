package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionCriterioAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionGeometriaEntity;

public interface SolicitudConcesionCriterioAreaRepository {
    ResultClassEntity listarSolicitudConcesionCriterioArea (SolicitudConcesionCriterioAreaEntity obj);
    ResultClassEntity registrarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj);
    ResultClassEntity actualizarSolicitudConcesionCriterioArea (SolicitudConcesionCriterioAreaEntity obj);
    ResultClassEntity eliminarSolicitudConcesionCriterioArea(SolicitudConcesionCriterioAreaEntity obj);
}
