package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;

public interface SeguridadRepository {
	 SeguridadEntity LoginUser(SeguridadEntity obj_seguridad);
	SeguridadEntity  UserByUsername(String username);
	ResultEntity ValidaloginApp(String username);
}
