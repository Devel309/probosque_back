package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetallePgmfDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;

import java.util.List;

public interface ActividadSilviculturalRepository {
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetalle(Integer idPlanManejo) throws Exception;
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalFiltroTipo(Integer idPlanManejo,String idTipo) throws Exception;
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalFiltroTipoTitular(String tipoDocumento, String nroDocumento,String codigoProcesoTitular,String codigoProceso,Integer idPlanManejo) throws Exception;

    ResultClassEntity ConfigurarActividadSilviculturalPgmf(ActividadSilviculturalEntity actividadSilvicultural) throws Exception;
    ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetallePgmf(Integer idPlanManejo, Integer intIdTipo) throws Exception;
    ResultClassEntity ActualizarActividadSilviculturalPgmf(ActividadSilviculturalDetallePgmfDto actividadSilviculturalActualizarDto) throws Exception;

    ResultClassEntity EliminarActividadSilviculturalPgmf(ActividadSilviculturalDetalleEntity actividadSilviculturalDetalleEntity) throws Exception;


    ResultClassEntity RegistrarLaborSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception;
    ResultClassEntity EliminarActividadSilviculturalDema(ActividadSilviculturalDetalleEntity actividadSilviculturalDetalleEntity) throws Exception;
    ResultClassEntity<List<ActividadSilviculturalDto>> ListarActividadSilviculturalCabecera(Integer idPlanManejo) throws Exception;

    List<ActividadSilviculturalDetalleDto> ListarActividadSilvicultural(Integer idPlanManejo,String codigoPlan) throws Exception;

    List<ActividadSilviculturalEntity>ListarActSilviCulturalDetalle(ActividadSilviculturalEntity param) throws Exception;

    ResultClassEntity RegistrarActividadSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception;
}
