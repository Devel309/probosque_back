package pe.gob.serfor.mcsniffs.repository.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FechaUtil {
	
	private static Calendar cal = Calendar.getInstance();

	public FechaUtil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static Date getFormatDate(Date fecha, String formato) throws Exception {
		Date fechaFormateada = null;
		try {
			// Fecha a string con el formato establecido
			SimpleDateFormat formatter = new SimpleDateFormat(formato);
			String fechaStr = formatter.format(fecha);

			// String a date
			fechaFormateada = formatter.parse(fechaStr);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fechaFormateada;
	}

	public static Date stringFormatIniToDateFormatFin(String fecha, String formatoOrigen, String formatoFinal)
			throws Exception {
		Date fechaFormateada = null;
		try {

			SimpleDateFormat formatterInicio = new SimpleDateFormat(formatoOrigen);
			SimpleDateFormat formatterFinal = new SimpleDateFormat(formatoFinal);

			Date date = formatterInicio.parse(fecha);

			String dateStr = formatterFinal.format(date);

			fechaFormateada = formatterInicio.parse(dateStr);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fechaFormateada;
	}

	public static Date stringToDate(String fecha, String formatoFinal) throws Exception {
		Date fechaFormateada = null;
		try {
			SimpleDateFormat formatterFinal = new SimpleDateFormat(formatoFinal);

			fechaFormateada = formatterFinal.parse(fecha);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fechaFormateada;
	}

	/**
	 * obtiene el dia del mes de la fecha en formato de dos digitos
	 *
	 * @return String
	 */
	public static String getDia() {
		return ((cal.get(Calendar.DAY_OF_MONTH)) < 10 ? "0" : "")
				+ new Integer(cal.get(Calendar.DAY_OF_MONTH)).toString();
	}

	/**
	 * obtiene el numero de mes de la fecha en formato de dos digitos
	 *
	 * @return String
	 */
	public static String getMes() {
		return ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" : "") + new Integer(cal.get(Calendar.MONTH) + 1).toString();
	}

	/**
	 * obtiene el a&ntilde;o de la fecha en formato de cuatro digitos
	 *
	 * @return String
	 */
	public static String getAnho() {
		return new Integer(cal.get(Calendar.YEAR)).toString();
	}

	/**
	 * retorna la hora de la fecha en formato de 24 hras
	 * 
	 * @return String
	 */
	public static String getHora24() {
		return new Integer(cal.get(Calendar.HOUR_OF_DAY)).toString();
	}

	/**
	 * retorna la hora en formato de 12hras
	 *
	 * @return String
	 */
	public static String getHora() {
		return new Integer(cal.get(Calendar.HOUR)).toString();
	}

	/**
	 * retorna los minutos
	 * 
	 * @return String
	 */
	public static String getMinuto() {
		return new Integer(cal.get(Calendar.MINUTE)).toString();
	}

	/**
	 * retorna los segundos
	 *
	 * @return String
	 */
	public static String getSegundo() {
		return new Integer(cal.get(Calendar.SECOND)).toString();
	}

	/**
	 * retorna los milisegundos
	 *
	 * @return String
	 */
	public static String getMiliSegundo() {
		return new Integer(cal.get(Calendar.MILLISECOND)).toString();
	}

	/**
	 * retorna el mes de la fecha del Calendar en letras
	 * 
	 * @return String
	 */
	public static String getMesletras() {
		return getMesletras(cal.get(Calendar.MONTH));
	}

	/**
	 * retorna el mes en letras segun el parametro mes
	 *
	 * @param mes
	 *            int
	 * @return String
	 */
	public static String getMesletras(int mes) {
		String dmes = "---";
		switch (mes) {
		case 0:
			dmes = "Enero";
			break;
		case 1:
			dmes = "Febrero";
			break;
		case 2:
			dmes = "Marzo";
			break;
		case 3:
			dmes = "Abril";
			break;
		case 4:
			dmes = "Mayo";
			break;
		case 5:
			dmes = "Junio";
			break;
		case 6:
			dmes = "Julio";
			break;
		case 7:
			dmes = "Agosto";
			break;
		case 8:
			dmes = "Setiembre";
			break;
		case 9:
			dmes = "Octubre";
			break;
		case 10:
			dmes = "Noviembre";
			break;
		case 11:
			dmes = "Diciembre";
			break;
		}
		return dmes;
	}
	
	public static String obtenerFechaActualString (String formato){
		
		Date fechaActual = new Date();
		
		SimpleDateFormat formatterFinal = new SimpleDateFormat(formato);

		return formatterFinal.format(fechaActual);

	}
	
	public static String obtenerFechaString (Date fecha, String formato){
		
		SimpleDateFormat formatterFinal = new SimpleDateFormat(formato);

		return formatterFinal.format(fecha);

	}

	public static String convertirNumeroALetras(Integer $num){
        return doThings($num);
    }

    private static String doThings(Integer _counter){
   
        if(_counter >2000000)
            return "DOS MILLONES";
               
        switch(_counter){
            case 0: return "CERO";
            case 1: return "UNO";
            case 2: return "DOS";
            case 3: return "TRES";
            case 4: return "CUATRO";
            case 5: return "CINCO"; 
            case 6: return "SEIS";
            case 7: return "SIETE";
            case 8: return "OCHO";
            case 9: return "NUEVE";
            case 10: return "DIEZ";
            case 11: return "ONCE"; 
            case 12: return "DOCE"; 
            case 13: return "TRECE";
            case 14: return "CATORCE";
            case 15: return "QUINCE";
            case 20: return "VEINTE";
            case 30: return "TREINTA";
            case 40: return "CUARENTA";
            case 50: return "CINCUENTA";
            case 60: return "SESENTA";
            case 70: return "SETENTA";
            case 80: return "OCHENTA";
            case 90: return "NOVENTA";
            case 100: return "CIEN";
            
            case 200: return "DOSCIENTOS";
            case 300: return "TRESCIENTOS";
            case 400: return "CUATROCIENTOS";
            case 500: return "QUINIENTOS";
            case 600: return "SEISCIENTOS";
            case 700: return "SETECIENTOS";
            case 800: return "OCHOCIENTOS";
            case 900: return "NOVECIENTOS";
            
            case 1000: return "MIL";
            
            case 1000000: return "UN MILLON";
            case 2000000: return "DOS MILLONES";
        }
        if(_counter<20){
            return "DIECI"+ doThings(_counter-10);
        }
        if(_counter<30){
            return "VEINTI" + doThings(_counter-20);
        }
        if(_counter<100){
            return doThings( (int)(_counter/10)*10 ) + " Y " + doThings(_counter%10);
        }        
        if(_counter<200){
            return "CIENTO " + doThings( _counter - 100 );
        }         
        if(_counter<1000){
            return doThings( (int)(_counter/100)*100 ) + " " + doThings(_counter%100);
        } 
        if(_counter<2000){
            return "MIL " + doThings( _counter % 1000 );
        } 
        if(_counter<1000000){
            String var="";
            var = doThings((int)(_counter/1000)) + " MIL" ;
            if(_counter % 1000!=0){
                var += " " + doThings(_counter % 1000);
            }
            return var;
        }
        if(_counter<2000000){
            return "UN MILLON " + doThings( _counter % 1000000 );
        }
        
        return "";
   }

	private static int GetNumber(char roman)
	{
		return roman == 'M' ? 1000 :
				roman == 'D' ? 500 :
				roman == 'C' ? 100 :
				roman == 'L' ? 50 :
				roman == 'X' ? 10 :
				roman == 'V' ? 5 :
				roman == 'I' ? 1 : 0;
	}
	public static int RomanToNumber(String numberRoman)
	{
		int number = 0;
		int back = 0;
		for (int j = numberRoman.length() - 1; j >= 0; j--)
		{
			int compare = GetNumber(numberRoman.charAt(j));
			if (compare < back)
				number -= compare;
			else
				number += compare;
			back = compare;
		}
		return number;
	}

}
