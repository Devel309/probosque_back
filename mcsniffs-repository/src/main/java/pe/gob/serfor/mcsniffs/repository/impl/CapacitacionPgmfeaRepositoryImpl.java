package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.repository.CapacitacionPgmfeaRepository;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
@Repository
public class CapacitacionPgmfeaRepositoryImpl extends JdbcDaoSupport implements CapacitacionPgmfeaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(CapacitacionPgmfeaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Registrar capacitación}
     * @param:List<CapacitacionDto>
     */
    @Override
    public ResultClassEntity RegistrarCapacitacionPgmfea(List<CapacitacionDto> list) {
        ResultClassEntity result = new ResultClassEntity();
        List<CapacitacionDto> list_ = new ArrayList<CapacitacionDto>();
        try{
            for(CapacitacionDto p:list){
                p.setCodTipoCapacitacion("TCAPPGMFEA");
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPgmfea_Registrar");
                processStored_.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("personaCapacitar", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idTipoModalidad", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored_.setParameter("idCapacitacion", p.getIdCapacitacion());
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.setParameter("codTipoCapacitacion",p.getCodTipoCapacitacion());
                processStored_.setParameter("personaCapacitar", p.getPersonaCapacitar());
                processStored_.setParameter("idTipoModalidad", p.getIdTipoModalidad());
                processStored_.setParameter("lugar", p.getLugar());
                processStored_.setParameter("estado", p.getEstado());
                processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored_.execute();

                List<Object[]> spResult_ =processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        CapacitacionDto obj_ = new CapacitacionDto();

                        obj_.setIdCapacitacion((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setIdTipoModalidad((String) row_[2]);
                        obj_.setPersonaCapacitar((String) row_[3]);
                        obj_.setLugar((String) row_[4]);
                       List<CapacitacionDetalleEntity> listDet = new ArrayList<CapacitacionDetalleEntity>();
                        for(CapacitacionDetalleEntity param:p.getListCapacitacionDetalle()){
                            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionDetallePgmfea_Registrar");
                            processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idCapacitacionDet", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            processStored.setParameter("idCapacitacion", obj_.getIdCapacitacion());
                            processStored.setParameter("idCapacitacionDet", param.getIdCapacitacionDet());
                            processStored.setParameter("descripcion", param.getDescripcion());
                            processStored.setParameter("estado", p.getEstado());
                            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                            processStored.execute();
                            List<Object[]> spResult =processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    CapacitacionDetalleEntity obj = new CapacitacionDetalleEntity();
                                    obj.setIdCapacitacionDet((Integer) row[0]);
                                    obj.setDescripcion((String) row[1]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListCapacitacionDetalle(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró capacitación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Listar  la capacitación}
     * @param:CapacitacionEntity
     */
    @Override
    public ResultClassEntity ListarCapacitacionPgmfea(PlanManejoEntity p) {
        ResultClassEntity result = new ResultClassEntity();
        List<CapacitacionDto> list_ = new ArrayList<CapacitacionDto>();
        try{
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPgmfea_Listar");
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.execute();
                List<Object[]> spResult_ =processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        CapacitacionDto obj_ = new CapacitacionDto();
                        obj_.setIdCapacitacion((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setIdTipoModalidad((String) row_[2]);
                        obj_.setPersonaCapacitar((String) row_[3]);
                        obj_.setLugar((String) row_[4]);
                        List<CapacitacionDetalleEntity> listDet = new ArrayList<CapacitacionDetalleEntity>();
                        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionDetallePgmfea_Listar");
                        processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
                        processStored.setParameter("idCapacitacion", obj_.getIdCapacitacion());
                        processStored.execute();
                        List<Object[]> spResult =processStored.getResultList();
                        if (spResult.size() >= 1) {
                            for (Object[] row : spResult) {
                                CapacitacionDetalleEntity obj = new CapacitacionDetalleEntity();
                                obj.setIdCapacitacionDet((Integer) row[0]);
                                obj.setDescripcion((String) row[1]);
                                listDet.add(obj);
                            }
                        }
                        obj_.setListCapacitacionDetalle(listDet);
                        list_.add(obj_);
                    }
                }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la capacitación.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Eliminar la capacitación}
     * @param:CapacitacionEntity
     */
    @Override
    public ResultClassEntity EliminarCapacitacionPgmfea(CapacitacionEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPgmfea_Eliminar");
            processStored_.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored_.setParameter("idCapacitacion", param.getIdCapacitacion());
            processStored_.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored_.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
