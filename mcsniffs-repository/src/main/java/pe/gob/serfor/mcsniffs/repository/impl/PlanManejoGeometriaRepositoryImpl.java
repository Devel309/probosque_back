package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoGeometriaActualizarCatastroDto;
import pe.gob.serfor.mcsniffs.repository.PlanManejoGeometriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository("repositoryPlanManejoGeometria")
public class PlanManejoGeometriaRepositoryImpl extends JdbcDaoSupport implements PlanManejoGeometriaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Registrar la geometria}
     * @param:PlanManejoGeometriaEntity
     */
    @Override
    public Integer registrarPlanManejoGeometria(PlanManejoGeometriaEntity item) throws Exception {
        Integer idPlanManejoGeometria = 0;
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_PlanManejoGeometria_Registrar");
        processStored.registerStoredProcedureParameter("IdPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("IdArchivo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("TipoGeometria", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("CodigoGeometria", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("CodigoSeccion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("CodigoSubSeccion", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("NombreCapa", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("ColorCapa", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("GeometriaWKT", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("Srid", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("Propiedad", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("IdUsuarioRegistro", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("IdPlanManejoGeometria", Integer.class, ParameterMode.OUT);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("IdPlanManejo", item.getIdPlanManejo());
        processStored.setParameter("IdArchivo", item.getIdArchivo());
        processStored.setParameter("TipoGeometria", item.getTipoGeometria());
        processStored.setParameter("CodigoGeometria", item.getCodigoGeometria());
        processStored.setParameter("CodigoSeccion", item.getCodigoSeccion());
        processStored.setParameter("CodigoSubSeccion", item.getCodigoSubSeccion());
        processStored.setParameter("NombreCapa", item.getNombreCapa());
        processStored.setParameter("ColorCapa", item.getColorCapa());
        processStored.setParameter("GeometriaWKT", item.getGeometry_wkt());
        processStored.setParameter("Srid", item.getSrid());
        processStored.setParameter("Propiedad", item.getPropiedad());
        processStored.setParameter("IdUsuarioRegistro", item.getIdUsuarioRegistro());
        processStored.execute();
        idPlanManejoGeometria = (Integer) processStored.getOutputParameterValue("IdPlanManejoGeometria");
        return idPlanManejoGeometria;
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Obtener las geometrias por filtro}
     * @param:PlanManejoGeometriaEntity
     */
    @Override
    public ResultClassEntity listarPlanManejoGeometria(PlanManejoGeometriaEntity item) {
        ResultClassEntity result = new ResultClassEntity();
        List<PlanManejoGeometriaEntity> objList = new ArrayList<PlanManejoGeometriaEntity>();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PlanManejoGeometria_Listar");
            processStored.registerStoredProcedureParameter("IdPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("TipoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("CodigoGeometria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("CodigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("CodigoSubSeccion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdPlanManejo", item.getIdPlanManejo());
            processStored.setParameter("TipoGeometria", item.getTipoGeometria());
            processStored.setParameter("CodigoGeometria", item.getCodigoGeometria());
            processStored.setParameter("CodigoSeccion", item.getCodigoSeccion());
            processStored.setParameter("CodigoSubSeccion", item.getCodigoSubSeccion());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoGeometriaEntity item2 = new PlanManejoGeometriaEntity();
                    item2.setIdPlanManejo((Integer) row[0]);
                    item2.setIdArchivo((Integer) row[1]);
                    item2.setTipoGeometria((String) row[2]);
                    item2.setCodigoGeometria((String) row[3]);
                    item2.setCodigoSeccion((String) row[4]);
                    item2.setCodigoSubSeccion((String) row[5]);
                    item2.setNombreCapa((String) row[6]);
                    item2.setColorCapa((String) row[7]);
                    item2.setGeometry_wkt((String) row[8]);
                    item2.setDescripcion((String) row[9]);
                    item2.setPropiedad((String) row[10]);
                    item2.setObservacion(row[11] == null ? null : (String) row[11]);
                    item2.setConforme(row[12] == null ? null : (Boolean) row[12]);
                    item2.setDetalle(row[13] == null ? null : (String) row[13]);
                    item2.setAsunto(row[14] == null ? null : (String) row[14]);
                    item2.setAcapite(row[15] == null ? null : (String) row[15]);
                    item2.setIdsCatastro(row[16] == null ? null : (String) row[16]);
                    objList.add(item2);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Abner Valdez [17-12-2021]
     * @modificado:
     * @descripción: {Eliminar PlanManejoGeometria archivo y/o capa}
     * @param:idArchivo,idUsuario
     */
    @Override
    public ResultClassEntity eliminarPlanManejoGeometriaArchivo(Integer idArchivo, Integer idUsuario) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PlanManejoGeometriaArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("IdArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuario", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdArchivo", idArchivo);
            processStored.setParameter("IdUsuario", idUsuario);

            processStored.execute();
            result.setData(idArchivo);
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Abner Valdez [06-04-2022]
     * @modificado:
     * @descripción: {Actualiza el campo de Catastro de la tabla
     *               planmanejogeometria}
     * @param:PlanManejoGeometriaActualizarCatastroDto
     */
    @Override
    public ResultClassEntity actualizarCatastroPlanManejoGeometria(PlanManejoGeometriaActualizarCatastroDto item)
            throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PlanManejoGeometria_ActualizarCatastro");
            processStored.registerStoredProcedureParameter("IdPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdsCatastro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdPlanManejoGeometria", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdPlanManejo", item.getIdPlanManejo());
            processStored.setParameter("IdsCatastro", item.getIdsCatastro());
            processStored.setParameter("IdUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.setParameter("IdPlanManejoGeometria", item.getIdPlanManejoGeometria());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se realizó el proceso correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo actualizar el campo de catastro.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }
}
