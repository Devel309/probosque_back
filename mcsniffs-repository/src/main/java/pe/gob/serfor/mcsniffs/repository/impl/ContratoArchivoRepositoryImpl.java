package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoArchivoDto;
import pe.gob.serfor.mcsniffs.repository.ContratoArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ContratoArchivoRepositoryImpl extends JdbcDaoSupport implements ContratoArchivoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ContratoPersonaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    

    @Override
    public ResultClassEntity registrarContratoArchivo(ContratoArchivoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ContratoArchivo_Registrar");
            processStored.registerStoredProcedureParameter("idArchivoContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);            
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idArchivoContrato", dto.getIdArchivoContrato());
            processStored.setParameter("idContrato", dto.getIdContrato());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();
 

            result.setSuccess(true);
            result.setMessage("Se registró contrato archivo correctamente.");

            
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

 
    @Override
    public List<ContratoArchivoDto> listarContratoArchivo(ContratoArchivoDto dto) throws Exception {
         
        List<ContratoArchivoDto> lista = new ArrayList<ContratoArchivoDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ContratoArchivo_Listar");            
            processStored.registerStoredProcedureParameter("idArchivoContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idArchivoContrato",dto.getIdArchivoContrato());
            processStored.setParameter("idContrato",dto.getIdContrato());
            processStored.setParameter("prefijo",dto.getPrefijo());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ContratoArchivoDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new ContratoArchivoDto();

                        temp.setIdArchivoContrato(row[0]==null?null:(Integer) row[0]);
                        temp.setIdContrato(row[1]==null?null:(Integer) row[1]);
                        temp.setIdArchivo(row[2]==null?null:(Integer) row[2]);
                        temp.setTipoDocumento(row[3]==null?null:(String) row[3]);
                        temp.setNombreArchivo(row[4]==null?null:(String) row[4]);
                        temp.setTipoArchivo(row[5]==null?null:(String) row[5]);
                        temp.setDescripcionTipoDocumento(row[6]==null?null:(String) row[6]);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarContratoArchivo", e.getMessage());
            throw e;
        }
    } 


    @Override
    public ResultClassEntity eliminarContratoArchivo(ContratoArchivoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ContratoArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);            
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());

            processStored.execute();
 

            result.setSuccess(true);
            result.setMessage("Se eliminó contrato archivo correctamente.");

            
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

}
