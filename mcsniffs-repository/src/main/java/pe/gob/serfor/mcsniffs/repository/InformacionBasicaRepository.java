package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionBasicaDetalle.InformacionBasicaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.PoblacionAledaniaDto;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;

import java.util.List;

public interface InformacionBasicaRepository {

        ResultClassEntity registrarInformacionBasica(InformacionBasicaEntity informacionBasicaEntity) throws Exception;


        ResultClassEntity registrarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception;
        ResultClassEntity ActualizarInformacionBasicaDetalle(List<InformacionBasicaEntity> list) throws Exception;

        List<InfBasicaAereaDetalleDto> listarInfBasicaAerea(String idInfBasica, Integer idPlanManejo, String codCabecera) throws Exception;
        List<InfBasicaAereaDetalleDto> listarInfBasicaAereaTitular (String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso) throws Exception;
        List<InfBasicaAereaDetalleDto> listarInfBasicaAereaUbigeo(String departamento, String provincia, String distrito) throws Exception;


        ResultClassEntity EliminarInfBasicaAerea(InfBasicaAereaDetalleDto infBasicaAereaDetalleDto) throws Exception;



        ResultClassEntity<List<TipoBosqueEntity>> obtenerTipoBosque(Integer idTipoBosque, String descripcion,
                        String region) throws Exception;

        ResultClassEntity<List<FaunaEntity>> obtenerFauna(Integer idPlanManejo, String tipoFauna, String nombre,
                        String nombreCientifico, String familia, String estatus, String codigoTipo) throws Exception;

        ResultClassEntity registrarSolicitudFauna(FaunaEntity faunaEntity) throws Exception;

        List<HidrografiaEntity> obtenerHidrografia(Integer idHidrografia,String tipoHidrografia) throws Exception;


        ResultClassEntity<List<PoblacionAledaniaDto>> obtenerPoblacion() throws Exception;

        ResultClassEntity registrarArchivoInfBasica(InformacionBasicaEntity param, MultipartFile file)throws Exception;

        ResultClassEntity listarInfBasicaArchivo(Integer id)throws Exception;
        ResultClassEntity eliminarInfBasicaArchivo(Integer id, Integer idArchivo,Integer idUsuario)throws Exception;
        List<InformacionBasicaEntity> listarInformacionBasica(Integer idPlanManejo) throws Exception;
        ResultClassEntity actualizarInformacionBasica(InformacionBasicaEntity informacionBasicaEntity) throws Exception;
        List<InformacionBasicaEntity> listarInformacionBasicaPorFiltro(Integer idPlanManejo, String codTipoInfBasica) throws Exception;
        //Jaqueline DB
        ResultClassEntity EliminarFauna(InfBasicaAereaDetalleDto infBasicaAereaDetalleDto);

        ResultClassEntity listarInfBasicaAereaDetalle(String codTipo, String codSubTipo, Integer idInfBasica) throws Exception;

        ResultClassEntity listarPorFiltrosInfBasicaAerea(InfBasicaAereaDetalleDto param) throws Exception;
        ResultClassEntity ListarFaunaInformacionBasicaDetalle(InformacionBasicaDetalleDto param) throws Exception;

        //Jason Retamozo 26-01-2022
        List<InformacionBasicaEntity> listarInformacionSocioeconomica(Integer idPlanManejo, String codigoTipoInfBasica, String codTipoInfBasicaDet) throws Exception;
}
