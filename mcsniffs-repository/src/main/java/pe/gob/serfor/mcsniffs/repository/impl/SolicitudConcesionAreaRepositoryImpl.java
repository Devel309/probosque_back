package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionAreaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudConcesionAreaRepositoryImpl extends JdbcDaoSupport implements SolicitudConcesionAreaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudConcesionAreaEntity> lista = new ArrayList<>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArea_ListarFiltro");

            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionArea", obj.getIdSolicitudConcesionArea());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("ubigeo", obj.getCodigoUbigeo());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                SolicitudConcesionAreaEntity c = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        c = new SolicitudConcesionAreaEntity();
                        c.setIdSolicitudConcesionArea(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolicitudConcesion(row[1] == null ? null : (Integer) row[1]);
                        c.setSuperficieHa(row[2] == null ? null : ((BigDecimal) row[2]).doubleValue());
                        c.setCodigoUbigeo(row[3] == null ? null : (String) row[3]);
                        c.setZonaUTM(row[4] == null ? null : (Integer) row[4]);
                        c.setObservacion(row[5] == null ? null : (String) row[5]);
                        c.setJustificacion(row[6] == null ? null : (String) row[6]);
                        c.setAnalisisMapa(row[7] == null ? null : (String) row[7]);
                        c.setDelimitacion(row[8] == null ? null : (String) row[8]);
                        c.setObservacionTamanio(row[9] == null ? null : (String) row[9]);
                        c.setFuenteBibliografica(row[10] == null ? null : (String) row[10]);
                        c.setIdDistritoArea(row[11] == null ? null : (Integer) row[11]);
                        c.setIdProvinciaArea(row[12] == null ? null : (Integer) row[12]);
                        c.setIdDepartamentoArea(row[13] == null ? null : (Integer) row[13]);
                        c.setNombreDistritoArea(row[14] == null ? null : (String) row[14]);
                        c.setNombreProvinciaArea(row[15] == null ? null : (String) row[15]);
                        c.setNombreDepartamentoArea(row[16] == null ? null : (String) row[16]);
                        c.setCaractFisica(row[17] == null ? null : (String) row[17]);
                        c.setCaractBiologica(row[18] == null ? null : (String) row[18]);
                        c.setCaractSocioeconomica(row[19] == null ? null : (String) row[19]);
                        c.setFactorAmbientalBiologico(row[20] == null ? null : (String) row[20]);
                        c.setFactorSocioEconocultural(row[21] == null ? null : (String) row[21]);
                        c.setManejoConcesion(row[22] == null ? null : (String) row[22]);
                        c.setPadre(row[23] == null ? null : (String) row[23]);
                        lista.add(c);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de áreas de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarAreaSolicitudConcesion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de áreas de la solicitud de concesión.");
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArea_Registrar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class,
                    ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficieHa", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("zonaUtm", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("analisisMapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("delimitacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacionTamanio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fuenteBibliografica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("superficieHa", obj.getSuperficieHa());
            processStored.setParameter("ubigeo", obj.getCodigoUbigeo());
            processStored.setParameter("zonaUtm", obj.getZonaUTM());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("justificacion", obj.getJustificacion());
            processStored.setParameter("analisisMapa", obj.getAnalisisMapa());
            processStored.setParameter("delimitacion", obj.getDelimitacion());
            processStored.setParameter("observacionTamanio", obj.getObservacionTamanio());
            processStored.setParameter("fuenteBibliografica", obj.getFuenteBibliografica());
            processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

            processStored.execute();

            Integer idSolicitudConcesionArea = (Integer) processStored
                    .getOutputParameterValue("idSolicitudConcesionArea");
            obj.setIdSolicitudConcesionArea(idSolicitudConcesionArea);
            result.setCodigo(idSolicitudConcesionArea);
            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se registró el área de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar el área de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArea_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficieHa", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("zonaUtm", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("analisisMapa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("delimitacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacionTamanio", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fuenteBibliografica", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("caractFisica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("caractBiologica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("caractSocioeconomica", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("factorAmbientalBiologico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("factorSocioEonocultural", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("manejoConcesion", String.class, ParameterMode.IN);

            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionArea", obj.getIdSolicitudConcesionArea());
            processStored.setParameter("idSolicitudConcesion", obj.getIdSolicitudConcesion());
            processStored.setParameter("superficieHa", obj.getSuperficieHa());
            processStored.setParameter("ubigeo", obj.getCodigoUbigeo());
            processStored.setParameter("zonaUtm", obj.getZonaUTM());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("justificacion", obj.getJustificacion());
            processStored.setParameter("analisisMapa", obj.getAnalisisMapa());
            processStored.setParameter("delimitacion", obj.getDelimitacion());
            processStored.setParameter("observacionTamanio", obj.getObservacionTamanio());
            processStored.setParameter("fuenteBibliografica", obj.getFuenteBibliografica());

            processStored.setParameter("caractFisica", obj.getCaractFisica());
            processStored.setParameter("caractBiologica", obj.getCaractBiologica());
            processStored.setParameter("caractSocioeconomica", obj.getCaractSocioeconomica());
            processStored.setParameter("factorAmbientalBiologico", obj.getFactorAmbientalBiologico());
            processStored.setParameter("factorSocioEonocultural", obj.getFactorSocioEconocultural());
            processStored.setParameter("manejoConcesion", obj.getManejoConcesion());

            processStored.setParameter("idUsuario", obj.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el área de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo actualizar el área de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity eliminarSolicitudConcesionArea(SolicitudConcesionAreaEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionArea_Eliminar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionArea", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolicitudConcesionArea", obj.getIdSolicitudConcesionArea());
            processStored.setParameter("idUsuario", obj.getIdUsuarioElimina());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el área de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo eliminar el área de la solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }
}
