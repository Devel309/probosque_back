package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;

public interface UnidadFisiograficaRepository {
    ResultClassEntity listarUnidadFisiografica(Integer idInfBasica)throws Exception;
    ResultClassEntity registrarUnidadFisiograficaArchivo(UnidadFisiograficaUMFEntity param,MultipartFile file)throws Exception;
    ResultClassEntity eliminarUnidadFisiograficaArchivo(Integer idInfBasica,Integer idUnidadFisiografica,Integer idUsuario)throws Exception;
}
