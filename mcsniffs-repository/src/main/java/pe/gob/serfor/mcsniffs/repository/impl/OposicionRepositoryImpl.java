package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Oposicion.OposicionArchivoDTO;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.OposicionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class OposicionRepositoryImpl extends JdbcDaoSupport implements OposicionRepository {
    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Reposotorio donde se implementa
     *               todo lo referente a las opociones hacia una postulacion}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Abner Valdez [06-12-2021]
     * @descripción: {Registrar la oposicion hacia una postulacion}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity registrarOposicion(OposicionEntity p) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Oposicion_Registrar");
            processStored.registerStoredProcedureParameter("idOposicion", Integer.class, ParameterMode.INOUT);
            //processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEnvioOposicion", Timestamp.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioOpositor", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fundada", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("respuesta", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreDocumentoOposicion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumentoOposicion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreDocumentoTH", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tituloHabilitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaDocumentoTH", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idOposicion", p.getIdOposicion());
            //processStored.setParameter("idProcesoPostulacion", p.getIdProcesoPostulacion());
            processStored.setParameter("descripcion", p.getDescripcion());
            processStored.setParameter("fechaEnvioOposicion", new java.sql.Timestamp(date.getTime()));
            processStored.setParameter("idUsuarioOpositor", p.getIdUsuarioOpositor());
            processStored.setParameter("fundada", p.getFundada());
            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
            processStored.setParameter("idPersona", p.getPersona().getIdPersona());
            processStored.setParameter("respuesta", p.getRespuesta());
            processStored.setParameter("tipoDocumentoGestion", p.getTipoDocumentoGestion());
            processStored.setParameter("documentoGestion", p.getDocumentoGestion());
            processStored.setParameter("nombreDocumentoOposicion", p.getNombreDocumentoOposicion());
            processStored.setParameter("numeroDocumentoOposicion", p.getNumeroDocumentoOposicion());
            processStored.setParameter("nombreDocumentoTH", p.getNombreDocumentoTH());
            processStored.setParameter("tituloHabilitante", p.getTituloHabilitante());
            processStored.setParameter("fechaDocumentoTH", p.getFechaDocumentoTH());
            processStored.execute();
            Integer idOposicion = (Integer) processStored.getOutputParameterValue("idOposicion");

            p.setIdOposicion(idOposicion);
            result.setMessage("se registró la oposición correctamente.");
            result.setSuccess(true);

            LogAuditoriaEntity logAuditoriaEntity;
            if(p.getIdOposicion() == 0 || p.getIdOposicion() == null){
                logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MAC_OPOSICION,
                        LogAuditoria.REGISTRAR,
                        LogAuditoria.DescripcionAccion.Registrar.T_MAC_OPOSICION + idOposicion,
                        p.getIdUsuarioRegistro());

            }else {

                 logAuditoriaEntity = new LogAuditoriaEntity(
                        LogAuditoria.Table.T_MAC_OPOSICION,
                        LogAuditoria.ACTUALIZAR,
                        LogAuditoria.DescripcionAccion.Actualizar.T_MAC_OPOSICION + p.getIdOposicion(),
                        p.getIdUsuarioRegistro());

            }

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);




            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Abner Valdez [10-12-2021]
     * @modificado:
     * @descripción: {Registrar el detalle de oposicion archivo}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity registrarOposicionArchivo(OposicionArchivoDTO item) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OposicionArchivo_Registrar");
            processStored.registerStoredProcedureParameter("idOposicionArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idOposicion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("resolucion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEmision", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conformidad", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idOposicionArchivo", item.getIdOposicionArchivo());
            processStored.setParameter("idOposicion", item.getIdOposicion());
            processStored.setParameter("idArchivo", item.getIdArchivo());
            processStored.setParameter("resolucion", item.getResolucion());
            processStored.setParameter("asunto", item.getAsunto());
            processStored.setParameter("fechaEmision", item.getFechaEmision());
            processStored.setParameter("descripcion", item.getDescripcion());
            processStored.setParameter("conformidad", item.getConformidad());
            processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.execute();

            result.setMessage("se registró la oposición archivo correctamente.");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Renzo Meneses [11-11-2021]
     * @descripción: {Lista las oposiciones activas}
     * @param:OposicionRequestEntity
     */
    @Override
    public ResultEntity<OposicionEntity> listarOposicion(OposicionRequestEntity filtro) {
        ResultEntity<OposicionEntity> result = new ResultEntity<OposicionEntity>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Oposicion_ListarPorFiltros");
            //sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstado", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idOposicion", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("numDocumentoGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            //sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idEstado", filtro.getIdEstado());
            sp.setParameter("idOposicion", filtro.getIdOposicion());
            sp.setParameter("idUsuarioRegistro", filtro.getIdUsuarioRegistro());
            sp.setParameter("tipoDocumentoGestion", filtro.getTipoDocumentoGestion());
            sp.setParameter("numDocumentoGestion", filtro.getNumDocumentoGestion());
            sp.setParameter("idUsuarioPostulacion", filtro.getIdUsuarioPostulacion());
            sp.setParameter("pageNum", filtro.getPageNum());
            sp.setParameter("pageSize", filtro.getPageSize());
            sp.execute();
            OposicionEntity c = null;
            List<OposicionEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new OposicionEntity();
                    c.setIdOposicion((Integer) row[0]);
                    //c.setIdProcesoPostulacion((Integer) row[1]);
                    c.setDescripcion((String) row[1]);
                    c.setFechaEnvio((Date) row[2]);
                    c.setIdUsuarioOpositor((Integer) row[3]);
                    c.setFundada((Boolean) row[4]);
                    c.setRespuesta((Boolean) row[5]);
                    c.setDescripcionRespuesta((String) row[6]);
                    result.setTotalrecord((Integer) row[7]);
                    c.setCorreoPostulante((String) row[8]);
                    c.setTipoDocumentoGestion((String) row[9]);
                    c.setDocumentoGestion((Integer) row[10]);
                    c.setDescTipoDocumentoGestion((String) row[11]);
                    c.setFechaEnvioFormato((String) row[12]);
                    c.setFueraFecha((Boolean) row[13]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron las oposiciones activas.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoPostulacionRepositoryImpl - listarProcesoPostulacion", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Registrar la respuesta de la oposicion}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity registrarRespuestaOposicion(OposicionEntity obj) {
        return null;
        /*
         * ResultClassEntity result = new ResultClassEntity();
         * try {
         * SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
         * Date date = new Date();
         * Connection con = null;
         * PreparedStatement pstm = null;
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_OposicionRespuesta_Registrar(?,?,?,?,?,?)}");
         * pstm.setObject (1, obj.getIdOposicion(), Types.INTEGER);
         * pstm.setObject(2, obj.getIdArchivoAdjunto(),Types.INTEGER);
         * pstm.setObject(3, obj.getDescripcion(),Types.VARCHAR);
         * pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * pstm.setObject(5, obj.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.setObject(6, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * Integer salida = pstm.executeUpdate();
         * ResultSet rsa = pstm.getGeneratedKeys();
         * if(rsa.next())
         * {
         * salida = rsa.getInt(1);
         * }
         * result.setCodigo(salida);
         * result.setSuccess(true);
         * result.setMessage("Se envio su oposicion de ampliacion.");
         * pstm.close();
         * con.close();
         * return result;
         * } catch (Exception e) {
         * log.error("Oposicion - registrarOposicion", e.getMessage());
         * result.setSuccess(false);
         * result.setMessage("Ocurrió un error.");
         * result.setMessageExeption(e.getMessage());
         * return result;
         * }
         */
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:Abner Valdez [09-12-2021]
     * @descripción: {Obtener la oposicion por su IdOposicion}
     * @param:IdOposicion
     */
    /*
     * @Override
     * public ResultClassEntity obtenerOposicion(Integer idOposicion) {
     * ResultClassEntity result = new ResultClassEntity();
     * List<OposicionEntity> objList = new ArrayList<OposicionEntity>();
     * try {
     * 
     * StoredProcedureQuery processStored =
     * entityManager.createStoredProcedureQuery("dbo.pa_Oposicion_ObtenerDetalle");
     * processStored.registerStoredProcedureParameter("idOposicion", Integer.class,
     * ParameterMode.IN);
     * SpUtil.enableNullParams(processStored);
     * processStored.setParameter("idOposicion", idOposicion);
     * 
     * processStored.execute();
     * List<Object[]> spResult = processStored.getResultList();
     * 
     * if (spResult.size() >= 1) {
     * OposicionEntity item = new OposicionEntity();
     * for (Object[] row : spResult) {
     * item = new OposicionEntity();
     * item.setIdOposicion((Integer) row[0]);
     * item.setIdProcesoPostulacion((Integer) row[1]);
     * PersonaEntity persona = new PersonaEntity();
     * persona.setTipo_documento((String) row[2]);
     * persona.setNumero_documento((String) row[3]);
     * persona.setNombres((String) row[4]);
     * persona.setApellidoPaterno((String) row[5]);
     * persona.setApellidoMaterno((String) row[6]);
     * persona.setRazon_social_empresa((String) row[7]);
     * persona.setTelefono((String) row[8]);
     * persona.setCorreo((String) row[9]);
     * item.setPersona(persona);
     * item.setDescripcion((String) row[10]);
     * item.setFundada((Boolean) row[11]);
     * item.setIdArchivo((Integer) row[12]);
     * boolean resolucion = Boolean.valueOf(row[13].toString());
     * item.setResolucion(resolucion);
     * objList.add(item);
     * }
     * }
     * result.setData(objList);
     * result.setSuccess(true);
     * return result;
     * } catch (Exception e) {
     * log.error(e.getMessage(), e);
     * result.setSuccess(false);
     * result.setMessage("Ocurrió un error.");
     * return result;
     * }
     * 
     * }
     */

    @Override
    public List<OposicionEntity> obtenerOposicion(Integer idOposicion) throws Exception {

        List<OposicionEntity> lista = new ArrayList<OposicionEntity>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Oposicion_ObtenerDetalle");
            processStored.registerStoredProcedureParameter("idOposicion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idOposicion", idOposicion);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                OposicionEntity temp = null;
                PersonaEntity persona = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        temp = new OposicionEntity();

                        persona = new PersonaEntity();

                        temp.setIdOposicion(row[0] == null ? null : (Integer) row[0]);
                        //temp.setIdProcesoPostulacion(row[1] == null ? null : (Integer) row[1]);
                        temp.setDescripcion(row[1] == null ? null : (String) row[1]);
                        temp.setFechaEnvio(row[2] == null ? null : (Date) row[2]);
                        temp.setFundada(row[3] == null ? null : (Boolean) row[3]);

                        persona.setCodigo_tipo_documento(row[4] == null ? null : (String) row[4]);
                        persona.setNumeroDocumento(row[5] == null ? null : (String) row[5]);
                        persona.setNombres(row[6] == null ? null : (String) row[6]);
                        persona.setApellidoPaterno(row[7] == null ? null : (String) row[7]);
                        persona.setApellidoMaterno(row[8] == null ? null : (String) row[8]);
                        persona.setRazonSocialEmpresa(row[9] == null ? null : (String) row[9]);
                        persona.setTelefono(row[10] == null ? null : (String) row[10]);
                        persona.setEmail_empresa(row[11] == null ? null : (String) row[11]);
                        persona.setIdPersona(row[12] == null ? null : (Integer) row[12]);
                        temp.setRespuesta((Boolean) row[13]);
                        temp.setTipoDocumentoGestion(row[14] == null ? null : (String) row[14]);
                        temp.setDocumentoGestion(row[15] == null ? null : (Integer) row[15]);
                        temp.setNumeroDocumentoOposicion(row[16] == null ? null : (Integer) row[16]);
                        temp.setNombreDocumentoOposicion(row[17] == null ? null : (String) row[17]);
                        temp.setNombreDocumentoTH(row[18] == null ? null : (String) row[18]);
                        temp.setTituloHabilitante(row[19] == null ? null : (String) row[19]);
                        temp.setFechaDocumentoTH(row[20] == null ? null : (Date) row[20]);
                        temp.setPersona(persona);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacion", e.getMessage());
            throw e;
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Obtener la oposicion respuesta por su IdOposicion}
     * @param:IdOposicion
     */
    @Override
    public ResultClassEntity<OposicionAdjuntosEntity> obtenerOposicionRespuesta(Integer IdOposicion) {
        return null;
        /*
         * ResultClassEntity<OposicionAdjuntosEntity> result = new
         * ResultClassEntity<OposicionAdjuntosEntity>();
         * OposicionAdjuntosEntity datacom = new OposicionAdjuntosEntity();
         * try {
         * //Obtener la oposicion
         * OposicionRequestEntity filtro = new OposicionRequestEntity();
         * filtro.setFundada(null);
         * filtro.setIdProcesoPostulacion(null);
         * filtro.setIdOposicion(IdOposicion);
         * ResultEntity<OposicionEntity> lstoposicion = listarOposicion(filtro);
         * OposicionEntity oposicion = lstoposicion.getData().get(0);
         * Connection con = null;
         * PreparedStatement pstm = null;
         * con = dataSource.getConnection();
         * pstm = con.prepareCall("{call pa_OposicionRespuesta_ObtenerRutaAdjunto(?)}");
         * pstm.setObject (1, IdOposicion,Types.INTEGER);
         * ResultSet rs = pstm.executeQuery();
         * String ruta = null;
         * String nombredoc=null;
         * datacom.setOposicion(oposicion);
         * while(rs.next()){
         * ruta = rs.getString("TX_RUTA");
         * nombredoc = rs.getString("TX_NOMBRE");
         * }
         * if(ruta!=null){
         * datacom.setNombredocumento(nombredoc);
         * File file = new File(ruta);
         * byte[] fileContent = Files.readAllBytes(file.toPath());
         * datacom.setDocumento(fileContent);
         * }
         * result.setData(datacom);
         * result.setSuccess(true);
         * result.setMessage("Se listaron las oposiciones activas.");
         * pstm.close();
         * con.close();
         * return result;
         * } catch (Exception e) {
         * log.error("ProcesoPostulacionRepositoryImpl - listarProcesoPostulacion",
         * e.getMessage());
         * result.setSuccess(false);
         * result.setData(datacom);
         * result.setMessage("Ocurrió un error al obtener el archivo.");
         * result.setMessageExeption(e.getMessage());
         * return result;
         * }
         */
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Descargar el formato de resolucion}
     *
     */
    @Override
    public ResultArchivoEntity descargarFormatoResulucion() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream("/FormatoExploracion.docx");
            File fileCopi = File.createTempFile("FormatoExploracion", ".docx");
            FileUtils.copyInputStreamToFile(inputStream, fileCopi);
            byte[] fileContent = Files.readAllBytes(fileCopi.toPath());
            result.setArchivo(fileContent);
            result.setNombeArchivo("FormatoExploracion.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo la formato de exploracion.");
            result.setInformacion(fileCopi.getAbsolutePath());
            return result;
        } catch (Exception e) {
            log.error("Solicitud - DescargarPropuestaExploracion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {adjuntar el formato de resolucion}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity adjuntarFormatoResolucion(OposicionEntity obj) {
        return null;
        /*
         * ResultClassEntity result = new ResultClassEntity();
         * try {
         * SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
         * Date date = new Date();
         * Connection con = null;
         * PreparedStatement pstm = null;
         * con = dataSource.getConnection();
         * //Adjuntar Documentos
         * pstm = con.prepareCall("{call pa_OposicionAdjuntos_Registrar(?,?,?,?)}");
         * pstm.setObject (1, obj.getIdOposicion(), Types.INTEGER);
         * pstm.setObject(2, obj.getIdArchivoAdjunto(),Types.INTEGER);
         * pstm.setObject(3, obj.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * Integer salida = pstm.executeUpdate();
         * ResultSet rsa = pstm.getGeneratedKeys();
         * if(rsa.next())
         * {
         * salida = rsa.getInt(1);
         * }
         * result.setCodigo(salida);
         * result.setSuccess(true);
         * result.setMessage("Se adjunto la resolucion.");
         * pstm.close();
         * con.close();
         * return result;
         * } catch (Exception e) {
         * log.error("Oposicion - registrarOposicion", e.getMessage());
         * result.setSuccess(false);
         * result.setMessage("Ocurrió un error.");
         * result.setMessageExeption(e.getMessage());
         * return result;
         * }
         */
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {actualizar la oposicion}
     * @param:OposicionEntity
     */
    @Override
    public ResultClassEntity actualizarOposicion(OposicionEntity obj) {
        return null;
        /*
         * ResultClassEntity result = new ResultClassEntity();
         * try {
         * SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
         * Date date = new Date();
         * Connection con = null;
         * PreparedStatement pstm = null;
         * con = dataSource.getConnection();
         * pstm = con.prepareCall("{call pa_Oposicion_ActualizarEstado(?,?,?,?)}");
         * pstm.setObject (1, obj.getIdOposicion(), Types.INTEGER);
         * pstm.setObject(2, obj.getFundada(),Types.INTEGER);
         * pstm.setObject(3, obj.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * Integer salida = pstm.executeUpdate();
         * OposicionRequestEntity filtro= new OposicionRequestEntity();
         * filtro.setIdProcesoPostulacion(null);
         * filtro.setFundada(null);
         * filtro.setIdOposicion(obj.getIdOposicion());
         * ResultEntity<OposicionEntity> oposicion = listarOposicion(filtro);
         * OposicionEntity objopsi = oposicion.getData().get(0);
         * if(obj.getFundada()){
         * Integer IdEstatus = 27;
         * pstm =
         * con.prepareCall("{call pa_ProcesoPostulacion_actualizarestatus(?,?,?,?)}");
         * pstm.setObject(1,objopsi.getIdProcesoPostulacion(),Types.INTEGER);
         * pstm.setObject(2, IdEstatus,Types.INTEGER);
         * pstm.setObject(3, obj.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * Integer salida3 = pstm.executeUpdate();
         * }else{
         * Integer IdEstatus = 2;
         * pstm =
         * con.prepareCall("{call pa_ProcesoPostulacion_actualizarestatus(?,?,?,?)}");
         * pstm.setObject(1,objopsi.getIdProcesoPostulacion(),Types.INTEGER);
         * pstm.setObject(2, IdEstatus,Types.INTEGER);
         * pstm.setObject(3, obj.getIdUsuarioModificacion(),Types.INTEGER);
         * pstm.setObject(4, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * Integer salida4 = pstm.executeUpdate();
         * }
         * result.setCodigo(salida);
         * result.setSuccess(true);
         * result.setMessage("Se actualizó el estado de la oposicion.");
         * pstm.close();
         * con.close();
         * return result;
         * } catch (Exception e) {
         * log.error("Oposicion - actualizarOposicion", e.getMessage());
         * result.setSuccess(false);
         * result.setMessage("Ocurrió un error.");
         * result.setMessageExeption(e.getMessage());
         * return result;
         * }
         */
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {registrar opositor}
     * @param:PersonaRequestEntity
     */
    @Override
    public ResultClassEntity regitrarOpositor(PersonaRequestEntity obj) {
        return null;
        /*
         * ResultClassEntity result = new ResultClassEntity();
         * try {
         * SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
         * Date date = new Date();
         * Connection con = null;
         * PreparedStatement pstm = null;
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_Persona_RegistrarOpositor(?,?,?,?,?,?,?,?,?,?)}");
         * pstm.setObject (1, obj.getNombres(), Types.VARCHAR);
         * pstm.setObject(2, obj.getApellidoMaterno(),Types.VARCHAR);
         * pstm.setObject(3, obj.getApellidoPaterno(),Types.VARCHAR);
         * pstm.setObject (4, obj.getTelefono(), Types.VARCHAR);
         * pstm.setObject(5, obj.getNumeroDocumento(),Types.VARCHAR);
         * pstm.setObject(6, obj.getCorreo(),Types.VARCHAR);
         * pstm.setObject (7, obj.getRuc(), Types.VARCHAR);
         * pstm.setObject(8, obj.getRazonSocial(),Types.VARCHAR);
         * pstm.setObject(9, obj.getIdUsuarioRegistro(),Types.VARCHAR);
         * pstm.setObject(10, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
         * Integer salida = pstm.executeUpdate();
         * ResultSet rsa = pstm.getGeneratedKeys();
         * if(rsa.next())
         * {
         * salida = rsa.getInt(1);
         * }
         * result.setCodigo(salida);
         * result.setSuccess(true);
         * result.setMessage("Se actualizó el estado de la oposicion.");
         * pstm.close();
         * con.close();
         * return result;
         * } catch (Exception e) {
         * log.error("Oposicion - regitrarOpositor", e.getMessage());
         * result.setSuccess(false);
         * result.setMessage("Ocurrió un error.");
         * result.setMessageExeption(e.getMessage());
         * return result;
         * }
         */
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {Busca al opositor por su numero de documento}
     * @param:NumeroDocumento
     */
    @Override
    public ResultClassEntity<PersonaRequestEntity> obtenerOpositor(OpositorRequestEntity obj) {
        return null;
        /*
         * ResultClassEntity<PersonaRequestEntity> result = new
         * ResultClassEntity<PersonaRequestEntity>();
         * try {
         * Connection con = null;
         * PreparedStatement pstm = null;
         * con = dataSource.getConnection();
         * pstm = con.prepareCall("{call pa_Persona_ObtenerxNumeroDoc(?,?,?)}");
         * pstm.setObject (1, obj.getDni(),Types.VARCHAR);
         * pstm.setObject (2, obj.getRuc(),Types.VARCHAR);
         * pstm.setObject (3, obj.getIdOpositor(),Types.INTEGER);
         * ResultSet rs = pstm.executeQuery();
         * PersonaRequestEntity c = null;
         * while(rs.next()){
         * c = new PersonaRequestEntity();
         * c.setIdPersona(rs.getInt("NU_ID_PERSONA"));
         * c.setNombres(rs.getString("TX_NOMBRES"));
         * c.setApellidoMaterno(rs.getString("TX_APELLIDO_MATERNO"));
         * c.setApellidoPaterno(rs.getString("TX_APELLIDO_PATERNO"));
         * c.setTelefono(rs.getString("TX_TELEFONO"));
         * c.setNumeroDocumento(rs.getString("TX_NUMERO_DOCUMENTO"));
         * c.setCorreo(rs.getString("TX_EMAIL"));
         * c.setRuc(rs.getString("TX_NUMERO_RUC_EMPRESA"));
         * c.setRazonSocial(rs.getString("TX_RAZON_SOCIAL_EMPRESA"));
         * break;
         * }
         * result.setData(c);
         * result.setSuccess(true);
         * result.setMessage("Se listo al opositor correctamente.");
         * pstm.close();
         * con.close();
         * return result;
         * } catch (Exception e) {
         * log.error("Oposicion - obtenerOpositor", e.getMessage());
         * result.setSuccess(false);
         * result.setMessage("Ocurrió un error.");
         * result.setMessageExeption(e.getMessage());
         * return result;
         * }
         */
    }

    /**
     * @autor: Abner Valdez [10-12-2021]
     * @modificado:
     * @descripción: {Eliminar oposicion archivo}
     * @param:idArchivo,idUsuario
     */
    @Override
    public ResultClassEntity eliminarOposicionArchivo(Integer idArchivo, Integer idUsuario) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OposicionArchivo_Eliminar");
            processStored.registerStoredProcedureParameter("IdArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuario", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdArchivo", idArchivo);
            processStored.setParameter("IdUsuario", idUsuario);

            processStored.execute();
            result.setData(idArchivo);
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente el archivo.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<OposicionArchivoDTO> listarOposicionArchivo(OposicionArchivoDTO dto) throws Exception {

        List<OposicionArchivoDTO> lista = new ArrayList<OposicionArchivoDTO>();

        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OposicionArchivo_ListarPorFiltros");
            processStored.registerStoredProcedureParameter("idOposicionArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idOposicion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idOposicionArchivo", dto.getIdOposicionArchivo());
            processStored.setParameter("idOposicion", dto.getIdOposicion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null) {
                OposicionArchivoDTO temp = null;
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {

                        temp = new OposicionArchivoDTO();

                        temp.setIdOposicionArchivo(row[0] == null ? null : (Integer) row[0]);
                        temp.setIdOposicion(row[1] == null ? null : (Integer) row[1]);
                        temp.setIdArchivo(row[2] == null ? null : (Integer) row[2]);
                        temp.setResolucion(row[3] == null ? null : (Boolean) row[3]);
                        temp.setAsunto(row[4] == null ? null : (String) row[4]);
                        temp.setFechaEmision(row[5] == null ? null : (Date) row[5]);
                        temp.setTipoDocumento(row[6] == null ? null : (String) row[6]);
                        temp.setNombre(row[7] == null ? null : (String) row[7]);
                        temp.setDescripcion(row[8] == null ? null : (String) row[8]);
                        temp.setConformidad(row[9] == null ? null : (Boolean) row[9]);
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarOposicionArchivoDTO", e.getMessage());
            throw e;
        }
    }
}
