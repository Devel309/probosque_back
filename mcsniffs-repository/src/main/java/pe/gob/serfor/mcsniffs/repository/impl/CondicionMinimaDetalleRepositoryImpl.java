package pe.gob.serfor.mcsniffs.repository.impl;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ListarCondicionMinimaDetalleDto;
import pe.gob.serfor.mcsniffs.repository.CondicionMinimaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;


@Repository
public class CondicionMinimaDetalleRepositoryImpl extends JdbcDaoSupport implements CondicionMinimaDetalleRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanManejoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }


    @Override
    public List<ListarCondicionMinimaDetalleDto> listarCondicionMinimaDetalle(Integer idCondicionMinima) throws Exception {
        
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_CondicionMinimaDetalle_Listar");
            sp.registerStoredProcedureParameter("idCondicionMinima", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idCondicionMinima", idCondicionMinima);
            sp.execute();
            List<ListarCondicionMinimaDetalleDto> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();
            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                ListarCondicionMinimaDetalleDto temp = null;
                for (Object[] item: spResultDet) {
                    temp = new ListarCondicionMinimaDetalleDto();
                    temp.setIdCondicionMinimaDet(item[0]==null?null:(Integer) item[0]);
                    temp.setCodigoTipoCondicionMinimaDet(item[1]==null?null:(String) item[1]);
                    temp.setResultado(item[2]==null?null:(String) item[2]);
                    temp.setFechaIngreso(item[3]==null?null:(String) item[3]);
                    temp.setFechaVigencia(item[4]==null?null:(String) item[4]);
                    temp.setIdCondicionMinima(item[5]==null?null:(Integer) item[5]);
                    temp.setEstado(item[6]==null?null:(String) item[6]);
                    resultDet.add(temp);
                }
            }

            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }




}
