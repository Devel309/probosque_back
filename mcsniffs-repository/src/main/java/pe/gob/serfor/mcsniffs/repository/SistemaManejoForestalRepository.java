package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SistemaManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.SistemaManejoForestalDto;

public interface SistemaManejoForestalRepository {

	ResultClassEntity<SistemaManejoForestalDto> obtener(Integer idPlanManejo, String codigoProceso) throws Exception;

	ResultClassEntity<List<SistemaManejoForestalDto>> listar(Integer idSistemaManejoForestal, Integer idPlanManejo,
			String descripcionFinMaderable, String descripcionCicloCorta, String descripcionFinNoMaderable,
			String descripcionCicloAprovechamiento) throws Exception;

	ResultClassEntity<SistemaManejoForestalDto> guardar(SistemaManejoForestalDto request) throws Exception;

	ResultClassEntity<SistemaManejoForestalDetalleEntity> guardarDetalle(SistemaManejoForestalDetalleEntity request)
			throws Exception;

	ResultClassEntity<Integer> eliminarDetalle(Integer idDetalle, Integer idUsuario) throws Exception;

}
