package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.FichaEvaluacionEntitty;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;

public interface EvaluacionRepository {

    List<EvaluacionDto> listarEvaluacion(EvaluacionDto dto) throws Exception;
    ResultClassEntity registrarEvaluacion(EvaluacionDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacion(EvaluacionDto dto) throws Exception;
    List<EvaluacionResumidoDto> listarEvaluacionResumido(EvaluacionResumidoDto dto) throws Exception;
    /**
     * @autor: Rafael Azaña [31/10-2021]
     * @modificado:
     * @descripción: {Evaluacion}
     * @param:EvaluacionDto
     */
    List<EvaluacionDetalleDto> listarEvaluacionTab(Integer idPlanManejo, String codigo,String codigoEvaluacionDet, String conforme, String codigoEvaluacionDetSub) throws Exception;
    ResultEntity listarEvaluacionGeneral(EvaluacionGeneralDto request);

    // Permisos forestales
    ResultClassEntity obtenerUltimaEvaluacionPermisoForestal(Integer idPermisoForestal) throws Exception;
    ResultClassEntity registrarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception;
    List<EvaluacionPermisoForestalDto> listarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception;
    List<EvaluacionPermisoForestalDto> listarEvaluacionPermisoForestalUsuario(EvaluacionPermisoForestalDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception;

    //JaquelineDB
    ResultEntity<PlanManejoEvaluacionDetalleDto> listarEvaluacionDetalle(Integer idPlanManejoEval);
    ResultEntity<FichaEvaluacionEntitty> listarFichaCabera(Integer idPlanManejo, String codigoProceso);

    ResultClassEntity diasHabiles(DiasHabilesDto dto) throws Exception;
    ResultClassEntity obtenerUltimaEvaluacion(Integer idPlanManejo) throws Exception;
    ResultClassEntity evaluacionActualizarPlan(EvaluacionActualizarPlan dto) throws Exception;
}
