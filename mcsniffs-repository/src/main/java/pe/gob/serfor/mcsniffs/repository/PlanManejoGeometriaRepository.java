package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoGeometriaActualizarCatastroDto;

public interface PlanManejoGeometriaRepository {
    Integer registrarPlanManejoGeometria(PlanManejoGeometriaEntity item) throws Exception;
    ResultClassEntity listarPlanManejoGeometria (PlanManejoGeometriaEntity item);
    ResultClassEntity eliminarPlanManejoGeometriaArchivo(Integer idArchivo, Integer idUsuario);
    ResultClassEntity actualizarCatastroPlanManejoGeometria(PlanManejoGeometriaActualizarCatastroDto item) throws Exception;
}
