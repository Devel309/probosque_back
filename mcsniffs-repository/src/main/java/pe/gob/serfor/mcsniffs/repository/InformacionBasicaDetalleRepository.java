package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.InformacionBasicaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface InformacionBasicaDetalleRepository {

   ResultClassEntity RegistrarInformacionBasicaDetalle(InformacionBasicaDetalleEntity informacionBasicaDetalleEntity) throws Exception;
   List<InformacionBasicaDetalleEntity> ListarInformacionBasicaDetalle(Integer idPlanManejo, Integer idInfBasica) throws Exception;
   ResultClassEntity ActualizarInformacionBasicaDetalle(InformacionBasicaDetalleEntity informacionBasicaDetalleEntity) throws Exception;
}
