package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionSubDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface PlanManejoEvaluacionSubDetalleRepository {
    ResultClassEntity listarplanManejoEvaluacionEspecie(PlanManejoEvaluacionDto request)throws Exception;
    List<PlanManejoEvaluacionSubDetalleDto> listarPlanManejoEvaluacionSubDetalle(Integer idPlanManejoEvalDet);
    ResultClassEntity registrarPlanManejoEvaluacionSubDetalle(PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalleDto) throws Exception;
    ResultClassEntity actualizarPlanManejoEvaluacionSubDetalle(PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalleDto) throws Exception;
    ResultClassEntity eliminarPlanManejoEvaluacionSubDetalle(PlanManejoEvaluacionSubDetalleDto planManejoEvaluacionSubDetalleDto) throws Exception;
    
}
