package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoBiologicoFaunaSilvestrePgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActEconomicaPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActividadesPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaConfictosPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaInfraestructuraPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaPgmf;

public interface InfoEconomicaPgmfRepository {
    

    ResultEntity<InfoEconomicaPgmf> registroInfoEconomicaPgmf( InfoEconomicaPgmf request);
    ResultClassEntity obtenerInfoEconomicaPgmf(InfoEconomicaPgmf request) ;

    ResultEntity<InfoEconomicaActEconomicaPgmf> registroInfoEconomicaActEconomicaPgmf( List<InfoEconomicaActEconomicaPgmf> request);
    ResultEntity<InfoEconomicaActEconomicaPgmf> obtenerInfoEconomicaActEconomicaPgmf( InfoEconomicaActEconomicaPgmf request) ;
    ResultEntity<InfoEconomicaActEconomicaPgmf> EliminarInfoEconomicaActEconomicaPgmf( InfoEconomicaActEconomicaPgmf request);

    ResultEntity<InfoEconomicaInfraestructuraPgmf> registroInfoEconomicaInfraestructuraPgmf( List<InfoEconomicaInfraestructuraPgmf> request);
    ResultEntity<InfoEconomicaInfraestructuraPgmf> obtenerInfoEconomicaInfraestructuraPgmf( InfoEconomicaInfraestructuraPgmf request) ;
    ResultEntity<InfoEconomicaInfraestructuraPgmf> EliminarInfoEconomicaInfraestructuraPgmf( InfoEconomicaInfraestructuraPgmf request);
   
    ResultEntity<InfoEconomicaActividadesPgmf> registroInfoEconomicaActividadesPgmf( List<InfoEconomicaActividadesPgmf> request);
    ResultEntity<InfoEconomicaActividadesPgmf> obtenerInfoEconomicaActividadesPgmf( InfoEconomicaActividadesPgmf request) ;

    ResultEntity<InfoEconomicaConfictosPgmf> registroInfoEconomicaConfictosPgmf( List<InfoEconomicaConfictosPgmf> request);
    ResultEntity<InfoEconomicaConfictosPgmf> obtenerInfoEconomicaConfictosPgmf( InfoEconomicaConfictosPgmf request) ;
    ResultEntity<InfoEconomicaConfictosPgmf> EliminarInfoEconomicaConfictosPgmf( InfoEconomicaConfictosPgmf request);


    ResultEntity<InfoBiologicoFaunaSilvestrePgmf> registroInfoBiologicoFaunaSilvestrePgmf( List<InfoBiologicoFaunaSilvestrePgmf> request);
    ResultEntity<InfoBiologicoFaunaSilvestrePgmf> obtenerInfoBiologicoFaunaSilvestrePgmf( InfoBiologicoFaunaSilvestrePgmf request) ;
    ResultEntity<InfoBiologicoFaunaSilvestrePgmf> EliminarBiologicoFaunaSilvestrePgmf( InfoBiologicoFaunaSilvestrePgmf request);

}
