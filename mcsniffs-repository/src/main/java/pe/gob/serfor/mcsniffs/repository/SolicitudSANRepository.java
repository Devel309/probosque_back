package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.*;

import java.util.List;

public interface SolicitudSANRepository {

    ResultClassEntity RegistrarSolicitudSAN(List<SolicitudSANEntity> list) throws Exception;
    List<SolicitudSANEntity>ListarSolicitudSAN(SolicitudSANEntity param) throws Exception;
    ResultClassEntity EliminarSolicitudSAN(SolicitudSANEntity solicitudSANEntity) throws Exception;
}
