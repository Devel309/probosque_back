package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import java.util.List;

public interface SolicitudRepository {

    ResponseEntity ListarPorFiltroSolicitud() throws Exception;

    ResultClassEntity RegistrarSolicitudAmpliacion(SolicitudDto obj);
    ResultClassEntity RegistrarDocumentoAdjunto(SolicitudEntity param);
    ResultEntity<SolicitudAdjuntosEntity> ObtenerArchivosSolicitudes(Integer IdSolicitud);
    ResultClassEntity AprobarRechazarSolicitud(SolicitudDto obj);
    ResultClassEntity listarSolicitudesAmpliacion(SolicitudDto param) ;
    ResultClassEntity<SolicitudDto> ObtenerSolicitudAmpliacion(SolicitudDto param);
    ResultEntity<TipoSolicitudEntity>ListarTipoSolicitud();
    ResultEntity<TipoDocumentoEntity>ListarTipoDocumento();
    ResultArchivoEntity DescagarDeclaracionJurada();
    ResultArchivoEntity DescargarPropuestaExploracion();
    ResultArchivoEntity DescargarFormatoAprobacion();
    ResultClassEntity<ImpugnacionNuevoGanadorEntity> ObtenerNuevoUsuarioGanador(FiltroSolicitudEntity filtro);
    ResultClassEntity ActualizarAprobadoSolicitud(SolicitudDto obj);
    ResultClassEntity RegistrarObservacionDocumentoAdjunto(List<DocumentoAdjuntoDto> obj);
    ResultArchivoEntity DescagarPlantillaResolucionResumen();
}
