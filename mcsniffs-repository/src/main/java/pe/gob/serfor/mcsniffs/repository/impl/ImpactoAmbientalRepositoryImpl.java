package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.ImpactoAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Repository
public class ImpactoAmbientalRepositoryImpl extends JdbcDaoSupport implements ImpactoAmbientalRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ImpactoAmbientalRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbiental_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", impactoAmbientalEntity.getIdPlanManejo());
            processStored.execute();

            List<ImpactoAmbientalEntity> impactoAmbientalEntityList = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ImpactoAmbientalEntity impactoAmbientalEntity1 = new ImpactoAmbientalEntity();
                    impactoAmbientalEntity1.setIdImpactoambiental((Integer)row[0]);
                    impactoAmbientalEntity1.setIdPlanManejo((Integer) row[1]);
                    impactoAmbientalEntity1.setActividad((String) row[2]);
                    impactoAmbientalEntity1.setDescripcion((String) row[3]);
                    impactoAmbientalEntityList.add(impactoAmbientalEntity1);
                }
            }

            result.setData(impactoAmbientalEntityList);
            result.setSuccess(true);
            result.setMessage("Se listó el impacto ambiental correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarImpactoAmbientalDetalle(ImpactoAmbientalEntity impactoAmbientalEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbientalDetalle_Listar");
            processStored.registerStoredProcedureParameter("idImpactoambiental", Integer.class, ParameterMode.IN);
            processStored.setParameter("idImpactoambiental", impactoAmbientalEntity.getIdImpactoambiental());
            processStored.execute();

            List<ImpactoAmbientalDetalleEntity> impactoAmbientalDetalleEntityList = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row: spResult){
                    ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity = new ImpactoAmbientalDetalleEntity();
                    impactoAmbientalDetalleEntity.setIdImpactoambientaldetalle((Integer) row[0]);
                    impactoAmbientalDetalleEntity.setIdImpactoambiental((Integer)row[1]);
                    impactoAmbientalDetalleEntity.setMedidasprevencion((String) row[2]);
                    impactoAmbientalDetalleEntityList.add(impactoAmbientalDetalleEntity);
                }
            }

            result.setData(impactoAmbientalDetalleEntityList);
            result.setSuccess(true);
            result.setMessage("Se listó el impacto ambiental detalle correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbiental_Registrar");
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idPlanManejo", impactoAmbientalEntity.getIdPlanManejo());
            processStored_.setParameter("actividad", impactoAmbientalEntity.getActividad());
            processStored_.setParameter("descripcion", impactoAmbientalEntity.getDescripcion());
            processStored_.setParameter("idUsuarioRegistro", impactoAmbientalEntity.getIdUsuarioRegistro());
            processStored_.execute();

            Object retorno2 = processStored_.getSingleResult();
            BigDecimal retornoIdentity = (BigDecimal) retorno2;
            impactoAmbientalEntity.setIdImpactoambiental(retornoIdentity.intValue());

            result.setData(impactoAmbientalEntity);
            result.setSuccess(true);
            result.setMessage("Se registró el impacto ambiental correctamente");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity RegistrarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbientalDetalle_Registrar");
            processStored_.registerStoredProcedureParameter("idImpactoAmbiental", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("medidasprevencion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idImpactoAmbiental", impactoAmbientalDetalleEntity.getIdImpactoambiental());
            processStored_.setParameter("medidasprevencion", impactoAmbientalDetalleEntity.getMedidasprevencion());
            processStored_.setParameter("idUsuarioRegistro", impactoAmbientalDetalleEntity.getIdUsuarioRegistro());
            processStored_.execute();

            Object retorno2 = processStored_.getSingleResult();
            BigDecimal retornoIdentity = (BigDecimal) retorno2;
            impactoAmbientalDetalleEntity.setIdImpactoambientaldetalle(retornoIdentity.intValue());

            result.setData(impactoAmbientalDetalleEntity);
            result.setSuccess(true);
            result.setMessage("Se registró el impacto ambiental detalle correctamente");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbiental_Actualizar");
            processStored_.registerStoredProcedureParameter("idImpactoambiental", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idImpactoambiental", impactoAmbientalEntity.getIdImpactoambiental());
            processStored_.setParameter("actividad", impactoAmbientalEntity.getActividad());
            processStored_.setParameter("descripcion", impactoAmbientalEntity.getDescripcion());
            processStored_.setParameter("idUsuarioModificacion", impactoAmbientalEntity.getIdUsuarioModificacion());
            processStored_.execute();

            result.setData(impactoAmbientalEntity);
            result.setSuccess(true);
            result.setMessage("Se actualizó el impacto ambiental correctamente");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbientalDetalle_Actualizar");
            processStored_.registerStoredProcedureParameter("idImpactoambientaldetalle", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("medidasprevencion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idImpactoambientaldetalle", impactoAmbientalDetalleEntity.getIdImpactoambientaldetalle());
            processStored_.setParameter("medidasprevencion", impactoAmbientalDetalleEntity.getMedidasprevencion());
            processStored_.setParameter("idUsuarioModificacion", impactoAmbientalDetalleEntity.getIdUsuarioModificacion());
            processStored_.execute();

            result.setData(impactoAmbientalDetalleEntity);
            result.setSuccess(true);
            result.setMessage("Se actualizó el impacto ambiental detalle correctamente");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbiental_Eliminar");
            processStored_.registerStoredProcedureParameter("idImpactoAmbiental", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idImpactoAmbiental", impactoAmbientalEntity.getIdImpactoambiental());
            processStored_.setParameter("idUsuarioElimina", impactoAmbientalEntity.getIdUsuarioElimina());
            processStored_.execute();

            result.setData(impactoAmbientalEntity);
            result.setSuccess(true);
            result.setMessage("Se eliminó el impacto ambiental correctamente");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity EliminarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_ImpactoAmbientalDetalle_Eliminar");
            processStored_.registerStoredProcedureParameter("idImpactoAmbientalDetalle", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored_.setParameter("idImpactoAmbientalDetalle", impactoAmbientalDetalleEntity.getIdImpactoambientaldetalle());
            processStored_.setParameter("idUsuarioElimina", impactoAmbientalDetalleEntity.getIdUsuarioElimina());
            processStored_.execute();

            result.setData(impactoAmbientalDetalleEntity);
            result.setSuccess(true);
            result.setMessage("Se eliminó el impacto ambiental detalle correctamente");
            return  result;

        }catch (Exception e ){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }


    }

    @Override
    public ResultClassEntity ListarImpactoAmbientalPmfi(MedidasPmfiDto medidasPmfiDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Pmfi_ImpactoAmbiental_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoImpactoAmbiental", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoImpactoAmbientalDet", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", medidasPmfiDto.getIdPlanManejo());
            processStored.setParameter("codigoImpactoAmbiental", medidasPmfiDto.getCodigoImpactoAmbiental());
            processStored.setParameter("codigoImpactoAmbientalDet", medidasPmfiDto.getCodigoImpactoAmbientalDet());

            processStored.execute();

            List<ImpactoAmbientalDetalleEntity> impactoAmbientalDetalleEntityList = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            ImpactoAmbientalPmfiDto impactoAmbientalPmfiDto = new ImpactoAmbientalPmfiDto();

            if (spResult.size() >= 1) {
                Object[] row = spResult.get(0);
                impactoAmbientalPmfiDto.setIdImpactoAmbiental((Integer) row[0]);
                impactoAmbientalPmfiDto.setIdPlanManejo((Integer)row[1]);
                impactoAmbientalPmfiDto.setCodigoImpactoAmbiental((String)row[2]);
            }

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity = new ImpactoAmbientalDetalleEntity();
                    impactoAmbientalDetalleEntity.setIdImpactoambiental((Integer) row[0]);
                    impactoAmbientalDetalleEntity.setIdImpactoambientaldetalle((Integer) row[5]);
                    impactoAmbientalDetalleEntity.setActividad((String) row[6]);
                    impactoAmbientalDetalleEntity.setDescripcion((String) row[7]);
                    impactoAmbientalDetalleEntity.setCodigoImpactoAmbientalDet((String) row[8]);
                    impactoAmbientalDetalleEntity.setMedidasProteccion((String) row[9]);
                    impactoAmbientalDetalleEntity.setMedidasprevencion((String) row[10]);
                    impactoAmbientalDetalleEntity.setMedidasMitigacion((String) row[11]);
                    impactoAmbientalDetalleEntityList.add(impactoAmbientalDetalleEntity);
                }
            }

            impactoAmbientalPmfiDto.setDetalle(impactoAmbientalDetalleEntityList);

            //seteamos idImpactoAmbiental e idImpactoAmbientalDetalle a 0 en caso idPlanManejo == null para casos por defecto
            if(impactoAmbientalPmfiDto.getIdPlanManejo() == null){
                impactoAmbientalPmfiDto.setIdPlanManejo(medidasPmfiDto.getIdPlanManejo());
                impactoAmbientalPmfiDto.setIdImpactoAmbiental(0);

                impactoAmbientalDetalleEntityList.forEach(f -> {
                    f.setIdImpactoambiental(0);
                    f.setIdImpactoambientaldetalle(0);
                });
            }

            result.setData(impactoAmbientalPmfiDto);
            result.setSuccess(true);
            result.setMessage("Se listó el impacto ambiental pmif correctamente");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity RegistrarImpactoAmbientalPmfi(ImpactoAmbientalPmfiDto impactoAmbientalPmfiDto) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            if(impactoAmbientalPmfiDto.getIdImpactoAmbiental() == 0) {
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Pmfi_ImpactoAmbiental_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoImpactoAmbiental", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPlanManejo", impactoAmbientalPmfiDto.getIdPlanManejo());
                processStored.setParameter("codigoImpactoAmbiental", impactoAmbientalPmfiDto.getCodigoImpactoAmbiental());
                processStored.setParameter("idUsuarioRegistro", impactoAmbientalPmfiDto.getIdUsuarioRegistro());
                processStored.execute();

                Object idRetorno = processStored.getSingleResult();
                Integer idImpactoAmbiental = Integer.parseInt(idRetorno.toString());

                impactoAmbientalPmfiDto.setIdImpactoAmbiental(idImpactoAmbiental);
            }

            List<ImpactoAmbientalDetalleEntity> impactoAmbientalDetalleEntityList = impactoAmbientalPmfiDto.getDetalle();

            impactoAmbientalDetalleEntityList.forEach(detalle -> {
                if(detalle.getIdImpactoambientaldetalle() == 0) {
                    StoredProcedureQuery processStored2 = entityManager.createStoredProcedureQuery("dbo.pa_Pmfi_ImpactoAmbientalDetalle_Registrar");
                    processStored2.registerStoredProcedureParameter("idImpactoambiental", Integer.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("medidasprevencion", String.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("codigoImpactoAmbientalDet", String.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("medidasProteccion", String.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("medidasMitigacion", String.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                    processStored2.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored2);

                    processStored2.setParameter("idImpactoambiental", impactoAmbientalPmfiDto.getIdImpactoAmbiental());
                    processStored2.setParameter("medidasprevencion", detalle.getMedidasprevencion());
                    processStored2.setParameter("codigoImpactoAmbientalDet", detalle.getCodigoImpactoAmbientalDet());
                    processStored2.setParameter("medidasProteccion", detalle.getMedidasProteccion());
                    processStored2.setParameter("medidasMitigacion", detalle.getMedidasMitigacion());
                    processStored2.setParameter("descripcion", detalle.getDescripcion());
                    processStored2.setParameter("actividad", detalle.getActividad());
                    processStored2.setParameter("idUsuarioRegistro", detalle.getIdUsuarioRegistro());
                    processStored2.execute();

                    Object objRertorno = processStored2.getSingleResult();
                    BigDecimal retornoIdentity = (BigDecimal) objRertorno;
                    Integer idImpactoAmbientaldetalle = retornoIdentity.intValue();
                    detalle.setIdImpactoambientaldetalle(idImpactoAmbientaldetalle);
                    detalle.setIdImpactoambiental(impactoAmbientalPmfiDto.getIdImpactoAmbiental());

                }else {
                    StoredProcedureQuery processStored3 = entityManager.createStoredProcedureQuery("dbo.pa_Pmfi_ImpactoAmbientalDetalle_Actualizar");
                    processStored3.registerStoredProcedureParameter("idImpactoambientaldetalle", Integer.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("medidasprevencion", String.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("codigoImpactoAmbientalDet", String.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("medidasProteccion", String.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("medidasMitigacion", String.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                    processStored3.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored3);

                    processStored3.setParameter("idImpactoambientaldetalle", detalle.getIdImpactoambientaldetalle());
                    processStored3.setParameter("medidasprevencion", detalle.getMedidasprevencion());
                    processStored3.setParameter("codigoImpactoAmbientalDet", detalle.getCodigoImpactoAmbientalDet());
                    processStored3.setParameter("medidasProteccion", detalle.getMedidasProteccion());
                    processStored3.setParameter("medidasMitigacion", detalle.getMedidasMitigacion());
                    processStored3.setParameter("descripcion", detalle.getDescripcion());
                    processStored3.setParameter("actividad", detalle.getActividad());
                    processStored3.setParameter("idUsuarioModificacion", detalle.getIdUsuarioModificacion());
                    processStored3.execute();
                    detalle.setIdImpactoambiental(impactoAmbientalPmfiDto.getIdImpactoAmbiental());
                }
            });

            result.setData(impactoAmbientalPmfiDto);
            result.setSuccess(true);
            result.setMessage("Se registró el Impacto Ambiental PMFI correctamente");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }
}
