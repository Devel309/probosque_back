package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.EstadoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface SolicitudEstadoRepository {
    ResultEntity ListaComboSolicitudEstado(EstadoSolicitudEntity request);

}
