package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface RequisitoEvaluacionRepository {
    ResultClassEntity<List<RequisitoEvaluacionEntity>> registrarRequisito(List<RequisitoEvaluacionEntity> requisito);
    ResultEntity<RequisitoEvaluacionEntity> listarRequisito(RequisitoEvaluacionEntity param);
    ResultClassEntity<RequisitoEvaluacionDetalleEntity> eliminarRequisitoDetalle(RequisitoEvaluacionDetalleEntity param);
}
