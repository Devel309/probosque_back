package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04.OrdenamientoInternoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionCategoriaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionDivAdmUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProyecionVigilanciaUMFEntity;
import pe.gob.serfor.mcsniffs.repository.OrdenamientoProteccionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar y obtener un Ordenamiento Proteccion}
 *
 */

@Repository
public class OrdenamientoProteccionRepositoryImpl extends JdbcDaoSupport implements OrdenamientoProteccionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager
            .getLogger(pe.gob.serfor.mcsniffs.repository.impl.OrdenamientoProteccionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultEntity RegistrarOrdenamientoProteccionCategoria(List<OrdenProteccionCategoriaUMFEntity> request) {
        // ResultEntity resul=new ResultEntity();
        // CallableStatement pstm = null;
        // Connection con = null;
        // try
        // {
        // String res="";
        // con = dataSource.getConnection();
        //
        // for(OrdenProteccionCategoriaUMFEntity item: request){
        // pstm = con.prepareCall("{call pa_RegistrarOrdenProteccionCategoria
        // (?,?,?,?,?,?,?,?,?,?,?)}");
        // pstm.setObject (1, item.getId_ordenprotec_categ_umf(),Types.INTEGER);
        // pstm.setObject (2, item.getId_plan_manejo(),Types.INTEGER);
        // pstm.setObject (3, item.getId_coberturaboscosa(),Types.INTEGER);
        // pstm.setObject (4, item.getId_tipocoberturaboscosa(),Types.INTEGER);
        // pstm.setObject (5, item.getArea(),Types.VARCHAR);
        // pstm.setObject (6, item.getPorcentaje(),Types.INTEGER);
        // pstm.setObject (7, item.getEspecificarinformacion(),Types.BOOLEAN);
        //
        // pstm.setObject (8, item.getEstado(),Types.VARCHAR);
        // pstm.setObject (9, item.getIdUsuarioRegistro(),Types.INTEGER);
        // pstm.setObject (10, item.getIdUsuarioModificacion(),Types.INTEGER);
        //
        //
        // pstm.registerOutParameter(11,Types.INTEGER);
        // pstm.execute();
        // resul.setCodigo(pstm.getInt(11));
        //
        // }
        //
        // resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion
        // no Actualizada"));
        // resul.setIsSuccess((resul.getCodigo()>0?true:false));
        //
        // } catch (Exception e) {
        // resul.setMessage(e.getMessage());
        // resul.setCodigo(0);
        // resul.setIsSuccess(false);
        // }finally{
        // try {
        // pstm.close();
        // con.close();
        // } catch (Exception e) {
        // resul.setMessage(e.getMessage());
        // resul.setCodigo(0);
        // resul.setIsSuccess(false);
        // }
        // }
        // return resul;
        return null;
    }

    @Override
    public ResultEntity EliminarOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_EliminarOrdenProteccionCategoria (?,?,?,?)}");
         * pstm.setObject (1, request.getId_ordenprotec_categ_umf(),Types.INTEGER);
         * pstm.setObject (2, request.getId_plan_manejo(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * pstm.setObject (4, request.getEstado(),Types.VARCHAR);
         * 
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity ObtenerOrdenamientoProteccionCategoria(OrdenProteccionCategoriaUMFEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * try
         * {
         * List<OrdenProteccionCategoriaUMFEntity> objList=new
         * ArrayList<OrdenProteccionCategoriaUMFEntity>();
         * String sql = "EXEC pa_ObtenerOrdenProtecionCategoria " +
         * request.getId_plan_manejo();
         * 
         * List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
         * 
         * for(Map<String, Object> row:rows) {
         * OrdenProteccionCategoriaUMFEntity obj=new
         * OrdenProteccionCategoriaUMFEntity();
         * obj.setId_ordenprotec_categ_umf((Integer)row.get(
         * "NU_ID_ORDENPROTEC_CATEG_UMF")) ;
         * obj.setId_plan_manejo((Integer)row.get("NU_ID_PLAN_MANEJO"));
         * obj.setId_coberturaboscosa((Integer)row.get("NU_ID_COBERTURABOSCOSA"));
         * obj.setId_tipocoberturaboscosa((Integer)row.get("NU_ID_TIPOCOBERTURABOSCOSA")
         * );
         * obj.setArea((String)row.get("TX_AREA"));
         * obj.setPorcentaje((Integer)row.get("NU_PORCENTAJE"));
         * obj.setEspecificarinformacion((Boolean)row.get("BL_ESPECIFICARINFORMACION"));
         * obj.setEstado((String)row.get("TX_ESTADO"));
         * 
         * //resul.setTotalRegistro((Integer)row.get("TotalRegistro"));
         * objList.add(obj);
         * }
         * resul.setData(objList);
         * resul.setMessage((objList.size()!=0?"Información encontrada"
         * :"Informacion no encontrada"));
         * resul.setIsSuccess((objList.size()!=0?true:false));
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setIsSuccess(false);
         * }
         * return resul;
         * 
         */
    }

    @Override
    public ResultEntity RegistrarOrdenamientoProteccionDivAdm(List<OrdenProteccionDivAdmUMFEntity> request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * for(OrdenProteccionDivAdmUMFEntity item:request){
         * pstm = con.
         * prepareCall("{call pa_RegistrarOrdenProteccionDivAdm(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1, item.getId_ordenprotec_divadm_umf(),Types.INTEGER);
         * pstm.setObject (2, item.getId_plan_manejo(),Types.INTEGER);
         * pstm.setObject (3, item.getNumero(),Types.INTEGER);
         * pstm.setObject (4, item.getAnios(),Types.INTEGER);
         * pstm.setObject (5, item.getTipobloque_seccion(),Types.INTEGER);
         * pstm.setObject (6, item.getNumerobloque(),Types.INTEGER);
         * pstm.setObject (7, item.getBloque(),Types.VARCHAR);
         * pstm.setObject (8, item.getTipobosque(),Types.INTEGER);
         * pstm.setObject (9, item.getArea(),Types.VARCHAR);
         * pstm.setObject (10, item.getPorcentaje(),Types.INTEGER);
         * 
         * pstm.setObject (11, item.getEstado(),Types.VARCHAR);
         * pstm.setObject (12, item.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.setObject (13, item.getIdUsuarioModificacion(),Types.INTEGER);
         * 
         * pstm.registerOutParameter(14,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(14));
         * 
         * res+= ( pstm.getInt(14)>0?"":"Item:" + item.getId_plan_manejo().toString()
         * +" => ItemPrincipal "+item.getId_ordenprotec_divadm_umf().toString() +",") ;
         * resul.setInformacion(res);
         * }
         * resul.setMessage((resul.getInformacion().length()<1?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getInformacion().length()<1?true:false));
         * 
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity EliminarOrdenamientoProteccionDivAdm(OrdenProteccionDivAdmUMFEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm = con.prepareCall("{call pa_EliminarOrdenProteccionDivAdm (?,?,?,?)}");
         * pstm.setObject (1, request.getId_ordenprotec_divadm_umf(),Types.INTEGER);
         * pstm.setObject (2, request.getId_plan_manejo(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * 
         * 
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity ObtenerOrdenamientoProteccionDivAdm(OrdenProteccionDivAdmUMFEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * try
         * {
         * List<OrdenProteccionDivAdmUMFEntity> objList=new
         * ArrayList<OrdenProteccionDivAdmUMFEntity>();
         * String sql = "EXEC pa_ObtenerOrdenProtecionDivAdm " +
         * request.getId_plan_manejo();
         * 
         * List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
         * 
         * for(Map<String, Object> row:rows) {
         * OrdenProteccionDivAdmUMFEntity obj=new OrdenProteccionDivAdmUMFEntity();
         * obj.setId_ordenprotec_divadm_umf((Integer)row.get(
         * "NU_ID_ORDENPROTEC_DIVADM_UMF")) ;
         * obj.setId_plan_manejo((Integer)row.get("NU_ID_PLAN_MANEJO"));
         * obj.setNumero((Integer)row.get("NU_NUMERO"));
         * obj.setAnios((Integer)row.get("NU_ANIOS"));
         * obj.setTipobloque_seccion((Integer)row.get("NU_TIPOBLOQUE_SECCION"));
         * obj.setNumerobloque((Integer)row.get("NU_NUMEROBLOQUE"));
         * obj.setBloque((String)row.get("TX_BLOQUE"));
         * obj.setTipobosque((Integer)row.get("NU_TIPOBOSQUE"));
         * obj.setArea((String)row.get("TX_AREA"));
         * obj.setPorcentaje((Integer)row.get("NU_PORCENTAJE"));
         * obj.setEstado((String)row.get("TX_ESTADO"));
         * obj.setIdUsuarioRegistro((Integer)row.get("NU_ID_USUARIO_REGISTRO"));
         * obj.setFechaRegistro((Date)row.get("FE_FECHA_REGISTRO"));
         * //resul.setTotalRegistro((Integer)row.get("TotalRegistro"));
         * objList.add(obj);
         * }
         * resul.setData(objList);
         * resul.setMessage((objList.size()!=0?"Información encontrada"
         * :"Informacion no encontrada"));
         * resul.setIsSuccess((objList.size()!=0?true:false));
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setIsSuccess(false);
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity RegistrarOrdenamientoProteccionVigilancia(List<OrdenProyecionVigilanciaUMFEntity> request) {

        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * for(OrdenProyecionVigilanciaUMFEntity item:request){
         * pstm = con.
         * prepareCall("{call pa_RegistrarOrdenProteccionVigilancia(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
         * );
         * pstm.setObject (1, item.getId_ordenprotec_vigilancia_umf(),Types.INTEGER);
         * pstm.setObject (2, item.getId_plan_manejo(),Types.INTEGER);
         * pstm.setObject (3, item.getTipobloque_seccion(),Types.INTEGER);
         * pstm.setObject (4, item.getNumerolugar(),Types.INTEGER);
         * pstm.setObject (5, item.getLugar(),Types.VARCHAR);
         * pstm.setObject (6, item.getNumerovertice(),Types.INTEGER);
         * pstm.setObject (7, item.getVertice(),Types.VARCHAR);
         * pstm.setObject (8, item.getCoord_este(),Types.VARCHAR);
         * pstm.setObject (9, item.getCoord_oste(),Types.VARCHAR);
         * pstm.setObject (10, item.getCoord_zonautm(),Types.VARCHAR);
         * 
         * pstm.setObject (11, item.getMaterial(),Types.VARCHAR);
         * pstm.setObject (12, item.getObservacion(),Types.VARCHAR);
         * 
         * 
         * pstm.setObject (13, item.getEstado(),Types.VARCHAR);
         * pstm.setObject (14, item.getIdUsuarioRegistro(),Types.INTEGER);
         * pstm.setObject (15, item.getIdUsuarioModificacion(),Types.INTEGER);
         * 
         * pstm.registerOutParameter(16,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(16));
         * 
         * res+= ( pstm.getInt(16)>0?"":"Item:" + item.getId_plan_manejo().toString()
         * +" => ItemPrincipal "+item.getId_ordenprotec_vigilancia_umf().toString()
         * +",") ;
         * resul.setInformacion(res);
         * }
         * resul.setMessage((resul.getInformacion().length()<1?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getInformacion().length()<1?true:false));
         * 
         * 
         * // resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * // resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         */
    }

    @Override
    public ResultEntity EliminarOrdenamientoProteccionVigilancia(OrdenProyecionVigilanciaUMFEntity request) {
        return null;
        /*
         * ResultEntity resul=new ResultEntity();
         * CallableStatement pstm = null;
         * Connection con = null;
         * try
         * {
         * String res="";
         * con = dataSource.getConnection();
         * pstm =
         * con.prepareCall("{call pa_EliminarOrdenProteccionVigilancia(?,?,?,?)}");
         * pstm.setObject (1, request.getId_ordenprotec_vigilancia_umf(),Types.INTEGER);
         * pstm.setObject (2, request.getId_plan_manejo(),Types.INTEGER);
         * pstm.setObject (3, request.getIdUsuarioElimina(),Types.INTEGER);
         * 
         * 
         * pstm.registerOutParameter(4,Types.INTEGER);
         * pstm.execute();
         * resul.setCodigo(pstm.getInt(4));
         * 
         * resul.setMessage((resul.getCodigo()>0?"Información Actualizada"
         * :"Informacion no Actualizada"));
         * resul.setIsSuccess((resul.getCodigo()>0?true:false));
         * 
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }finally{
         * try {
         * pstm.close();
         * con.close();
         * } catch (Exception e) {
         * resul.setMessage(e.getMessage());
         * resul.setCodigo(0);
         * resul.setIsSuccess(false);
         * }
         * }
         * return resul;
         * 
         */
    }

    @Override
    public ResultEntity ObtenerOrdenamientoProteccionVigilancia(
            OrdenProyecionVigilanciaUMFEntity request) {
        ResultEntity resul = new ResultEntity();
        try {
            List<OrdenProyecionVigilanciaUMFEntity> objList = new ArrayList<OrdenProyecionVigilanciaUMFEntity>();
            String sql = "EXEC pa_ObtenerOrdenProtecionVigilancia " + request.getId_plan_manejo();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {
                OrdenProyecionVigilanciaUMFEntity obj = new OrdenProyecionVigilanciaUMFEntity();
                obj.setId_ordenprotec_vigilancia_umf((Integer) row.get("NU_ID_ORDENPROTEC_VIGILANCIA_UMF"));
                obj.setId_plan_manejo((Integer) row.get("NU_ID_PLAN_MANEJO"));
                obj.setTipobloque_seccion((Integer) row.get("NU_TIPOBLOQUE_SECCION"));
                obj.setNumerolugar((Integer) row.get("NU_NUMEROLUGAR"));
                obj.setLugar((String) row.get("TX_LUGAR"));
                obj.setNumerovertice((Integer) row.get("NU_NUMEROVERTICE"));
                obj.setVertice((String) row.get("TX_VERTICE"));
                obj.setCoord_este((String) row.get("TX_COORD_ESTE"));
                obj.setCoord_oste((String) row.get("TX_COORD_OSTE"));
                obj.setCoord_zonautm((String) row.get("TX_COORD_ZONAUTM"));
                obj.setMaterial((String) row.get("TX_MATERIAL"));
                obj.setObservacion((String) row.get("TX_OBSERVACION"));

                obj.setEstado((String) row.get("TX_ESTADO"));
                obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_REGISTRO"));
                obj.setFechaRegistro((Date) row.get("FE_FECHA_REGISTRO"));
                // resul.setTotalRegistro((Integer)row.get("TotalRegistro"));
                objList.add(obj);
            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity ListarOrdenamientoProteccion(OrdenamientoProteccionEntity request) {
        ResultEntity resul = new ResultEntity();
        try {
            List<OrdenamientoProteccionEntity> objList = new ArrayList<>();
            String sql = "EXEC pa_OrdenamientoProteccion_Listar " + request.getIdPlanManejo();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {

                OrdenamientoProteccionEntity objOrdPro = new OrdenamientoProteccionEntity();
                objOrdPro.setIdOrdenamientoProteccion((Integer) row.get("idOrdenamientoProteccion"));
                objOrdPro.setCodTipoOrdenamiento((String) row.get("codigoTipoOrdenamientoProteccion"));
                objOrdPro.setAnexo((String) row.get("anexo"));
                objOrdPro.setEsSistemaPoliciclico((Boolean) row.get("esSistemaPoliciclico"));
                objOrdPro.setJustificacion((String) row.get("justificacion"));

                OrdenamientoProteccionDetalleEntity objOrdProDet = new OrdenamientoProteccionDetalleEntity();
                objOrdProDet.setIdOrdenamientoProteccionDet((Integer) row.get("idOrdenamientoProteccionDet"));
                ;
                objOrdProDet.setCodigoTipoOrdenamientoDet((String) row.get("codigoTipoOrdenamientoDet"));
                objOrdProDet.setCategoria((String) row.get("categoria"));
                objOrdProDet.setActividad((String) row.get("actividad"));
                objOrdProDet.setMeta((String) row.get("meta"));
                if (row.get("areaHA") != null) {
                    BigDecimal areaHA = ((BigDecimal) row.get("areaHA"));
                    objOrdProDet.setAreaHA(areaHA);
                } else {
                    objOrdProDet.setAreaHA(BigDecimal.ZERO);
                }

                if (row.get("areaHAPorcentaje") != null) {
                    BigDecimal areaHAPor = ((BigDecimal) row.get("areaHAPorcentaje"));
                    objOrdProDet.setAreaHAPorcentaje(areaHAPor);
                } else {
                    objOrdProDet.setAreaHA(BigDecimal.ZERO);
                }

                objOrdProDet.setDescripcion((String) row.get("descripcion"));
                objOrdProDet.setObservacion((String) row.get("observacion"));
                objOrdProDet.setVerticeBloque((String) row.get("verticeBloque"));

                if (row.get("coordenadaEste") != null) {
                    BigDecimal coordEste = ((BigDecimal) row.get("coordenadaEste"));
                    objOrdProDet.setCoordenadaEste(coordEste);
                } else {
                    objOrdProDet.setCoordenadaEste(BigDecimal.ZERO);
                }

                if (row.get("coordenadaNorte") != null) {
                    BigDecimal coordNorte = ((BigDecimal) row.get("coordenadaNorte"));
                    objOrdProDet.setCoordenadaNorte(coordNorte);
                } else {
                    objOrdProDet.setCoordenadaNorte(BigDecimal.ZERO);
                }

                objOrdProDet.setZonaUTM((String) row.get("zonaUTM"));
                objOrdProDet.setObservacionDetalle((String) row.get("observacionDetalle"));
                objOrdProDet.setUsoPotencial((String) row.get("usoPotencial"));
                objOrdProDet.setActividadesRealizar((String) row.get("actividadesRealizar"));

                List<OrdenamientoProteccionDetalleEntity> objListDet = new ArrayList<>();
                objListDet.add(objOrdProDet);

                objOrdPro.setListOrdenamientoProteccionDet(objListDet);
                objList.add(objOrdPro);

            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity ListarOrdenamientoInterno(OrdenamientoProteccionEntity request) {
        ResultEntity resul = new ResultEntity();
        try {

            List<OrdenamientoProteccionEntity> objList = new ArrayList<>();

            String sql = "EXEC pa_OrdenamientoInterno_Listar " + request.getIdPlanManejo() + ","
                    + request.getCodTipoOrdenamiento();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
            OrdenamientoProteccionEntity objOrdPro = new OrdenamientoProteccionEntity();
            List<OrdenamientoProteccionDetalleEntity> objListDet = new ArrayList<>();
            for (Map<String, Object> row : rows) {
                objOrdPro.setIdOrdenamientoProteccion((Integer) row.get("idOrdenamientoProteccion"));
                objOrdPro.setCodTipoOrdenamiento((String) row.get("codigoTipoOrdenamientoProteccion"));
                objOrdPro.setAnexo((String) row.get("anexo"));
                objOrdPro.setEsSistemaPoliciclico((Boolean) row.get("esSistemaPoliciclico"));
                objOrdPro.setJustificacion((String) row.get("justificacion"));
                OrdenamientoProteccionDetalleEntity objOrdProDet = new OrdenamientoProteccionDetalleEntity();
                objOrdProDet.setIdOrdenamientoProteccionDet((Integer) row.get("idOrdenamientoProteccionDet"));
                ;
                objOrdProDet.setCodigoTipoOrdenamientoDet((String) row.get("codigoTipoOrdenamientoDet"));
                objOrdProDet.setCategoria((String) row.get("categoria"));
                objOrdProDet.setActividad((String) row.get("actividad"));
                objOrdProDet.setMeta((String) row.get("meta"));
                objOrdProDet.setAreaHA((BigDecimal) row.get("areaHA"));
                objOrdProDet.setAreaHAPorcentaje((BigDecimal) row.get("areaHAPorcentaje"));
                /*
                 * if(row.get("areaHA") !=null){
                 * BigDecimal areaHA =((BigDecimal) row.get("areaHA"));
                 * objOrdProDet.setAreaHA(areaHA);
                 * }else{
                 * objOrdProDet.setAreaHA(BigDecimal.ZERO);
                 * }
                 * 
                 * if(row.get("areaHAPorcentaje") !=null){
                 * BigDecimal areaHAPor =((BigDecimal) row.get("areaHAPorcentaje"));
                 * objOrdProDet.setAreaHAPorcentaje(areaHAPor);
                 * }else{
                 * objOrdProDet.setAreaHA(BigDecimal.ZERO);
                 * }
                 */

                objOrdProDet.setDescripcion((String) row.get("descripcion"));
                objOrdProDet.setObservacion((String) row.get("observacion"));
                objOrdProDet.setVerticeBloque((String) row.get("verticeBloque"));

                if (row.get("coordenadaEste") != null) {
                    BigDecimal coordEste = ((BigDecimal) row.get("coordenadaEste"));
                    objOrdProDet.setCoordenadaEste(coordEste);
                } else {
                    objOrdProDet.setCoordenadaEste(BigDecimal.ZERO);
                }

                if (row.get("coordenadaNorte") != null) {
                    BigDecimal coordNorte = ((BigDecimal) row.get("coordenadaNorte"));
                    objOrdProDet.setCoordenadaNorte(coordNorte);
                } else {
                    objOrdProDet.setCoordenadaNorte(BigDecimal.ZERO);
                }

                objOrdProDet.setZonaUTM((String) row.get("zonaUTM"));
                objOrdProDet.setObservacionDetalle((String) row.get("observacionDetalle"));
                objOrdProDet.setUsoPotencial((String) row.get("usoPotencial"));
                objOrdProDet.setActividadesRealizar((String) row.get("actividadesRealizar"));
                objListDet.add(objOrdProDet);
            }
            objOrdPro.setListOrdenamientoProteccionDet(objListDet);
            objList.add(objOrdPro);
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity ListarOrdenamientoInternoDetalle(OrdenamientoProteccionEntity request) {
        ResultEntity resul = new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoInternoDetalle_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoOrdenamiento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("codTipoOrdenamiento", request.getCodTipoOrdenamiento());

            processStored.execute();
            List<OrdenamientoProteccionEntity> objList = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                OrdenamientoProteccionEntity objOrdPro = null;
                List<OrdenamientoProteccionDetalleEntity> objListDet = new ArrayList<>();
                for (Object[] row : spResult) {
                    objOrdPro = new OrdenamientoProteccionEntity();
                    objOrdPro.setIdOrdenamientoProteccion(row[0] == null ? null : (Integer) row[0]);
                    // objOrdPro.setIdOrdenamientoProteccion((Integer)row[0]);
                    objOrdPro.setCodTipoOrdenamiento(row[1] == null ? null : (String) row[1]);
                    objOrdPro.setSubCodTipoOrdenamiento(row[2] == null ? null : (String) row[2]);
                    // objOrdPro.setAnexo(row[3]==null?null:(String) row[3]);
                    objOrdPro.setAnexo(row[3] == null ? null : String.valueOf((Character) row[3]));
                    if (row[4] != null) {
                        objOrdPro.setEsSistemaPoliciclico((boolean) row[4]);
                    } else {
                        objOrdPro.setEsSistemaPoliciclico(false);
                    }
                    // objOrdPro.setEsSistemaPoliciclico((Boolean) row[4]);
                    objOrdPro.setJustificacion(row[5] == null ? null : (String) row[5]);
                    OrdenamientoProteccionDetalleEntity objOrdProDet = new OrdenamientoProteccionDetalleEntity();
                    // objOrdProDet.setIdOrdenamientoProteccionDet((Integer)row[6]);
                    objOrdProDet.setIdOrdenamientoProteccionDet(row[6] == null ? null : (Integer) row[6]);
                    objOrdProDet.setCodigoTipoOrdenamientoDet(row[7] == null ? null : (String) row[7]);
                    objOrdProDet.setCategoria(row[8] == null ? null : (String) row[8]);
                    objOrdProDet.setActividad(row[9] == null ? null : (String) row[9]);
                    objOrdProDet.setMeta(row[10] == null ? null : (String) row[10]);
                    objOrdProDet.setAreaHA(row[11] == null ? null : (BigDecimal) row[11]);
                    objOrdProDet.setAreaHAPorcentaje(row[12] == null ? null : (BigDecimal) row[12]);
                    objOrdProDet.setDescripcion(row[13] == null ? null : (String) row[13]);
                    objOrdProDet.setObservacion(row[14] == null ? null : (String) row[14]);
                    objOrdProDet.setVerticeBloque(row[15] == null ? null : (String) row[15]);

                    if (row[15] != null) {
                        BigDecimal coordEste = ((BigDecimal) row[16]);
                        objOrdProDet.setCoordenadaEste(coordEste);
                    } else {
                        objOrdProDet.setCoordenadaEste(BigDecimal.ZERO);
                    }

                    if (row[16] != null) {
                        BigDecimal coordNorte = ((BigDecimal) row[17]);
                        objOrdProDet.setCoordenadaNorte(coordNorte);
                    } else {
                        objOrdProDet.setCoordenadaNorte(BigDecimal.ZERO);
                    }

                    objOrdProDet.setZonaUTM(row[18] == null ? null : (String) row[18]);
                    objOrdProDet.setObservacionDetalle(row[19] == null ? null : (String) row[19]);
                    objOrdProDet.setUsoPotencial(row[20] == null ? null : (String) row[20]);
                    objOrdProDet.setActividadesRealizar(row[21] == null ? null : (String) row[21]);
                    if (row[22] != null) {
                        objOrdProDet.setAccion((boolean) row[22]);
                    } else {
                        objOrdProDet.setAccion(false);
                    }
                    objOrdProDet.setTipoBosque(row[23] == null ? null : (String) row[23]);
                    objOrdProDet.setBloqueQuinquenal(row[24] == null ? null : (String) row[24]);
                    objOrdProDet.setParcelaCorta(row[25] == null ? null : (String) row[25]);
                    objOrdProDet.setIdArchivo(row[26] == null ? null : (Integer) row[26]);
                    // objOrdProDet.setActRealizar(row[27]==null?null:(String) row[27]);
                    objListDet.add(objOrdProDet);

                }
                objOrdPro.setListOrdenamientoProteccionDet(objListDet);
                objList.add(objOrdPro);
            }

            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess(true);
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity ListarOrdenamientoInternoDetalleTitular(OrdenamientoProteccionEntity request) {
        ResultEntity resul = new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoInternoDetalle_Listar_Titular");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoOrdenamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumento", request.getTipoDocumento());
            processStored.setParameter("nroDocumento", request.getNroDocumento());
            processStored.setParameter("codigoProcesoTitular", request.getCodigoProcesoTitular());
            processStored.setParameter("codigoProceso", request.getCodTipoOrdenamiento());
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());

            processStored.execute();
            List<OrdenamientoProteccionEntity> objList = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                OrdenamientoProteccionEntity objOrdPro = null;
                List<OrdenamientoProteccionDetalleEntity> objListDet = new ArrayList<>();
                for (Object[] row : spResult) {
                    objOrdPro = new OrdenamientoProteccionEntity();
                    objOrdPro.setIdOrdenamientoProteccion(row[0] == null ? null : (Integer) row[0]);
                    // objOrdPro.setIdOrdenamientoProteccion((Integer)row[0]);
                    objOrdPro.setCodTipoOrdenamiento(row[1] == null ? null : (String) row[1]);
                    objOrdPro.setSubCodTipoOrdenamiento(row[2] == null ? null : (String) row[2]);
                    // objOrdPro.setAnexo(row[3]==null?null:(String) row[3]);
                    objOrdPro.setAnexo(row[3] == null ? null : String.valueOf((Character) row[3]));
                    if (row[4] != null) {
                        objOrdPro.setEsSistemaPoliciclico((boolean) row[4]);
                    } else {
                        objOrdPro.setEsSistemaPoliciclico(false);
                    }
                    // objOrdPro.setEsSistemaPoliciclico((Boolean) row[4]);
                    objOrdPro.setJustificacion(row[5] == null ? null : (String) row[5]);
                    OrdenamientoProteccionDetalleEntity objOrdProDet = new OrdenamientoProteccionDetalleEntity();
                    // objOrdProDet.setIdOrdenamientoProteccionDet((Integer)row[6]);
                    objOrdProDet.setIdOrdenamientoProteccionDet(row[6] == null ? null : (Integer) row[6]);
                    objOrdProDet.setCodigoTipoOrdenamientoDet(row[7] == null ? null : (String) row[7]);
                    objOrdProDet.setCategoria(row[8] == null ? null : (String) row[8]);
                    objOrdProDet.setActividad(row[9] == null ? null : (String) row[9]);
                    objOrdProDet.setMeta(row[10] == null ? null : (String) row[10]);
                    objOrdProDet.setAreaHA(row[11] == null ? null : (BigDecimal) row[11]);
                    objOrdProDet.setAreaHAPorcentaje(row[12] == null ? null : (BigDecimal) row[12]);
                    objOrdProDet.setDescripcion(row[13] == null ? null : (String) row[13]);
                    objOrdProDet.setObservacion(row[14] == null ? null : (String) row[14]);
                    objOrdProDet.setVerticeBloque(row[15] == null ? null : (String) row[15]);

                    if (row[15] != null) {
                        BigDecimal coordEste = ((BigDecimal) row[16]);
                        objOrdProDet.setCoordenadaEste(coordEste);
                    } else {
                        objOrdProDet.setCoordenadaEste(BigDecimal.ZERO);
                    }

                    if (row[16] != null) {
                        BigDecimal coordNorte = ((BigDecimal) row[17]);
                        objOrdProDet.setCoordenadaNorte(coordNorte);
                    } else {
                        objOrdProDet.setCoordenadaNorte(BigDecimal.ZERO);
                    }

                    objOrdProDet.setZonaUTM(row[18] == null ? null : (String) row[18]);
                    objOrdProDet.setObservacionDetalle(row[19] == null ? null : (String) row[19]);
                    objOrdProDet.setUsoPotencial(row[20] == null ? null : (String) row[20]);
                    objOrdProDet.setActividadesRealizar(row[21] == null ? null : (String) row[21]);
                    if (row[22] != null) {
                        objOrdProDet.setAccion((boolean) row[22]);
                    } else {
                        objOrdProDet.setAccion(false);
                    }
                    objOrdProDet.setTipoBosque(row[23] == null ? null : (String) row[23]);
                    objOrdProDet.setBloqueQuinquenal(row[24] == null ? null : (String) row[24]);
                    objOrdProDet.setParcelaCorta(row[25] == null ? null : (String) row[25]);
                    objListDet.add(objOrdProDet);

                }
                objOrdPro.setListOrdenamientoProteccionDet(objListDet);
                objList.add(objOrdPro);
            }

            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public List<OrdenamientoInternoDetalleDto> ListaROrdenamientoInternoPMFI(OrdenamientoInternoDetalleDto request)
            throws Exception {
        List<OrdenamientoInternoDetalleDto> lista = new ArrayList<OrdenamientoInternoDetalleDto>();
        try {
            List<OrdenamientoProteccionEntity> objList = new ArrayList<>();
            String sql = "EXEC pa_OrdenamientoInterno_Listar " + request.getIdPlanManejo() + ","
                    + request.getCodTipoOrdenamiento();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {

                OrdenamientoInternoDetalleDto temp = new OrdenamientoInternoDetalleDto();

                temp.setIdOrdenamientoProteccion((Integer) row.get("idOrdenamientoProteccion"));
                temp.setCodTipoOrdenamiento((String) row.get("codigoTipoOrdenamientoProteccion"));
                temp.setAnexo((String) row.get("anexo"));
                temp.setEsSistemaPoliciclico((Boolean) row.get("esSistemaPoliciclico"));
                temp.setJustificacion((String) row.get("justificacion"));
                temp.setIdOrdenamientoProteccionDet((Integer) row.get("idOrdenamientoProteccionDet"));
                ;
                temp.setCodigoTipoOrdenamientoDet((String) row.get("codigoTipoOrdenamientoDet"));
                temp.setCategoria((String) row.get("categoria"));
                temp.setActividad((String) row.get("actividad"));
                temp.setMeta((String) row.get("meta"));
                temp.setAreaHA((BigDecimal) row.get("areaHA"));
                temp.setAreaHAPorcentaje((BigDecimal) row.get("areaHAPorcentaje"));
                temp.setDescripcion((String) row.get("descripcion"));
                temp.setObservacion((String) row.get("observacion"));
                temp.setVerticeBloque((String) row.get("verticeBloque"));

                if (row.get("coordenadaEste") != null) {
                    BigDecimal coordEste = ((BigDecimal) row.get("coordenadaEste"));
                    temp.setCoordenadaEste(coordEste);
                } else {
                    temp.setCoordenadaEste(BigDecimal.ZERO);
                }

                if (row.get("coordenadaNorte") != null) {
                    BigDecimal coordNorte = ((BigDecimal) row.get("coordenadaNorte"));
                    temp.setCoordenadaNorte(coordNorte);
                } else {
                    temp.setCoordenadaNorte(BigDecimal.ZERO);
                }

                temp.setZonaUTM((String) row.get("zonaUTM"));
                temp.setObservacionDetalle((String) row.get("observacionDetalle"));
                temp.setUsoPotencial((String) row.get("usoPotencial"));
                temp.setActividadesRealizar((String) row.get("actividadesRealizar"));

                lista.add(temp);
            }

            return lista;
        } catch (Exception e) {
            log.error("listarRecursoForestal", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity RegistrarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoProteccionEntity> list_ = new ArrayList<>();
        try {
            for (OrdenamientoProteccionEntity p : request) {

                StoredProcedureQuery processStored_ = entityManager
                        .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccion_Registrar");
                processStored_.registerStoredProcedureParameter("codigoTipoOrdenamientoProteccion", String.class,
                        ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("superficie", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("punto", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("medidas", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("esSistemaPoliciclico", Boolean.class,
                        ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);

                SpUtil.enableNullParams(processStored_);
                processStored_.setParameter("codigoTipoOrdenamientoProteccion", p.getCodTipoOrdenamiento());
                processStored_.setParameter("bloque", "");
                processStored_.setParameter("superficie", "");
                processStored_.setParameter("punto", 0);
                processStored_.setParameter("coordenadaEste", 0.0);
                processStored_.setParameter("coordenadaNorte", 0.0);
                processStored_.setParameter("anexo", (p.getAnexo() != null ? p.getAnexo() : ""));
                processStored_.setParameter("observaciones",
                        (p.getObservaciones() != null ? p.getObservaciones() : ""));
                processStored_.setParameter("medidas", "");
                processStored_.setParameter("descripcion", (p.getDescripcion() != null ? p.getDescripcion() : ""));
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored_.setParameter("esSistemaPoliciclico",
                        (p.getEsSistemaPoliciclico() != null ? p.getEsSistemaPoliciclico() : false));
                processStored_.setParameter("justificacion",
                        (p.getJustificacion() != null ? p.getJustificacion() : ""));
                processStored_.execute();
                List<Object[]> spResult_ = processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        OrdenamientoProteccionEntity obj_ = new OrdenamientoProteccionEntity();
                        obj_.setIdOrdenamientoProteccion((Integer) row_[0]);
                        obj_.setCodTipoOrdenamiento((String) row_[1]);
                        String anexo = String.valueOf(row_[7]);
                        obj_.setAnexo(anexo);
                        obj_.setObservaciones((String) row_[8]);
                        obj_.setDescripcion((String) row_[10]);
                        obj_.setIdPlanManejo((Integer) row_[11]);
                        obj_.setEsSistemaPoliciclico((Boolean) row_[12]);
                        obj_.setJustificacion((String) row_[13]);

                        List<OrdenamientoProteccionDetalleEntity> listDet = new ArrayList<>();
                        for (OrdenamientoProteccionDetalleEntity param : p.getListOrdenamientoProteccionDet()) {
                            StoredProcedureQuery processStored = entityManager
                                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccionDetalle_Registrar");
                            processStored.registerStoredProcedureParameter("codigotipoOrdenamientoDet", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("meta", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("areaHA", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("areaHAPorcentaje", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("descripcion", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("observacion", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("verticeBloque", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("zonaUTM", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("observacionDetalle", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idOrdenamientoProteccion", Integer.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("usoPotencial", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("actividadesRealizar", String.class,
                                    ParameterMode.IN);
                            SpUtil.enableNullParams(processStored);
                            processStored.setParameter("codigotipoOrdenamientoDet",
                                    param.getCodigoTipoOrdenamientoDet());
                            processStored.setParameter("categoria",
                                    (param.getCategoria() != null ? param.getCategoria() : ""));
                            processStored.setParameter("actividad",
                                    (param.getActividad() != null ? param.getActividad() : ""));
                            processStored.setParameter("meta", (param.getMeta() != null ? param.getMeta() : ""));
                            processStored.setParameter("areaHA", (param.getAreaHA() != null ? param.getAreaHA() : 0.0));
                            processStored.setParameter("areaHAPorcentaje",
                                    (param.getAreaHAPorcentaje() != null ? param.getAreaHAPorcentaje() : 0.0));
                            processStored.setParameter("descripcion",
                                    (param.getDescripcion() != null ? param.getDescripcion() : ""));
                            processStored.setParameter("observacion", "");
                            processStored.setParameter("verticeBloque",
                                    (param.getVerticeBloque() != null ? param.getVerticeBloque() : ""));
                            processStored.setParameter("coordenadaEste",
                                    (param.getCoordenadaEste() != null ? param.getCoordenadaEste() : 0.0));
                            processStored.setParameter("coordenadaNorte",
                                    (param.getCoordenadaNorte() != null ? param.getCoordenadaNorte() : 0.0));
                            processStored.setParameter("zonaUTM",
                                    (param.getZonaUTM() != null ? param.getZonaUTM() : ""));
                            processStored.setParameter("observacionDetalle",
                                    (param.getObservacionDetalle() != null ? param.getObservacionDetalle() : ""));
                            processStored.setParameter("idOrdenamientoProteccion", obj_.getIdOrdenamientoProteccion());
                            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            processStored.setParameter("usoPotencial",
                                    (param.getUsoPotencial() != null ? param.getUsoPotencial() : ""));
                            processStored.setParameter("actividadesRealizar",
                                    (param.getActividadesRealizar() != null ? param.getActividadesRealizar() : ""));

                            processStored.execute();

                            List<Object[]> spResult = processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    OrdenamientoProteccionDetalleEntity objDet = new OrdenamientoProteccionDetalleEntity();
                                    objDet.setIdOrdenamientoProteccionDet((Integer) row[0]);
                                    objDet.setCodigoTipoOrdenamientoDet((String) row[1]);
                                    objDet.setCategoria((String) row[2]);
                                    objDet.setActividad((String) row[3]);
                                    objDet.setMeta((String) row[4]);
                                    BigDecimal areaHA = ((BigDecimal) row[5]);
                                    objDet.setAreaHA(areaHA);
                                    BigDecimal areaHAPor = ((BigDecimal) row[6]);
                                    objDet.setAreaHAPorcentaje(areaHAPor);
                                    objDet.setDescripcion((String) row[7]);
                                    objDet.setObservacion((String) row[8]);
                                    objDet.setVerticeBloque((String) row[9]);

                                    if (row[10] != null) {
                                        BigDecimal coordEste = ((BigDecimal) row[10]);
                                        objDet.setCoordenadaEste(coordEste);
                                    } else {
                                        objDet.setCoordenadaEste(BigDecimal.ZERO);
                                    }

                                    if (row[11] != null) {
                                        BigDecimal coordNorte = ((BigDecimal) row[11]);
                                        objDet.setCoordenadaNorte(coordNorte);
                                    } else {
                                        objDet.setCoordenadaNorte(BigDecimal.ZERO);
                                    }

                                    objDet.setZonaUTM((String) row[12]);
                                    objDet.setObservacionDetalle((String) row[13]);
                                    objDet.setIdOrdenamientoProtecccion((Integer) row[14]);
                                    objDet.setUsoPotencial((String) row[15]);
                                    objDet.setActividadesRealizar((String) row[16]);
                                    objDet.setEstado((String) row[17]);
                                    listDet.add(objDet);
                                }
                            }
                        }
                        obj_.setListOrdenamientoProteccionDet(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró El Resumen Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ActualizarOrdenamientoProteccion(List<OrdenamientoProteccionEntity> request) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoProteccionEntity> list_ = new ArrayList<>();
        try {
            for (OrdenamientoProteccionEntity p : request) {

                StoredProcedureQuery processStored_ = entityManager
                        .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccion_Actualizar");
                processStored_.registerStoredProcedureParameter("idOrdenamientoProteccion", Integer.class,
                        ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codigoTipoOrdenamientoProteccion", String.class,
                        ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("superficie", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("punto", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("observaciones", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("medidas", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class,
                        ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("esSistemaPoliciclico", Boolean.class,
                        ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);

                processStored_.setParameter("idOrdenamientoProteccion", p.getIdOrdenamientoProteccion());
                processStored_.setParameter("codigoTipoOrdenamientoProteccion", p.getCodTipoOrdenamiento());
                processStored_.setParameter("bloque", "");
                processStored_.setParameter("superficie", "");
                processStored_.setParameter("punto", 0);
                processStored_.setParameter("coordenadaEste", 0.0);
                processStored_.setParameter("coordenadaNorte", 0.0);
                processStored_.setParameter("anexo", (p.getAnexo() != null ? p.getAnexo() : ""));
                processStored_.setParameter("observaciones",
                        (p.getObservaciones() != null ? p.getObservaciones() : ""));
                processStored_.setParameter("medidas", "");
                processStored_.setParameter("descripcion", (p.getDescripcion() != null ? p.getDescripcion() : ""));
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.setParameter("estado", p.getEstado());
                processStored_.setParameter("idUsuarioModificacion", p.getIdUsuarioModificacion());
                processStored_.setParameter("esSistemaPoliciclico",
                        (p.getEsSistemaPoliciclico() != null ? p.getEsSistemaPoliciclico() : false));
                processStored_.setParameter("justificacion",
                        (p.getJustificacion() != null ? p.getJustificacion() : ""));
                processStored_.execute();
                List<Object[]> spResult_ = processStored_.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row_ : spResult_) {
                        OrdenamientoProteccionEntity obj_ = new OrdenamientoProteccionEntity();
                        obj_.setIdOrdenamientoProteccion((Integer) row_[0]);
                        obj_.setCodTipoOrdenamiento((String) row_[1]);
                        String anexo = String.valueOf(row_[7]);
                        obj_.setAnexo(anexo);
                        obj_.setObservaciones((String) row_[8]);
                        obj_.setDescripcion((String) row_[10]);
                        obj_.setEsSistemaPoliciclico((Boolean) row_[11]);
                        obj_.setJustificacion((String) row_[12]);
                        obj_.setIdPlanManejo((Integer) row_[13]);

                        List<OrdenamientoProteccionDetalleEntity> listDet = new ArrayList<>();
                        for (OrdenamientoProteccionDetalleEntity param : p.getListOrdenamientoProteccionDet()) {
                            StoredProcedureQuery processStored = entityManager
                                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccionDetalle_Actualizar");
                            processStored.registerStoredProcedureParameter("idOrdenamientoProteccionDet", Integer.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("codigotipoOrdenamientoDet", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("meta", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("areaHA", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("areaHAPorcentaje", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("descripcion", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("observacion", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("verticeBloque", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("zonaUTM", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("observacionDetalle", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idOrdenamientoProteccion", Integer.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("usoPotencial", String.class,
                                    ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("actividadesRealizar", String.class,
                                    ParameterMode.IN);

                            processStored.setParameter("idOrdenamientoProteccionDet",
                                    param.getIdOrdenamientoProteccionDet());
                            processStored.setParameter("codigotipoOrdenamientoDet",
                                    param.getCodigoTipoOrdenamientoDet());
                            processStored.setParameter("categoria",
                                    (param.getCategoria() != null ? param.getCategoria() : ""));
                            processStored.setParameter("actividad",
                                    (param.getActividad() != null ? param.getActividad() : ""));
                            processStored.setParameter("meta", (param.getMeta() != null ? param.getMeta() : ""));
                            processStored.setParameter("areaHA", (param.getAreaHA() != null ? param.getAreaHA() : 0.0));
                            processStored.setParameter("areaHAPorcentaje",
                                    (param.getAreaHAPorcentaje() != null ? param.getAreaHAPorcentaje() : 0.0));
                            processStored.setParameter("descripcion",
                                    (param.getDescripcion() != null ? param.getDescripcion() : ""));
                            processStored.setParameter("observacion", "");
                            processStored.setParameter("verticeBloque",
                                    (param.getVerticeBloque() != null ? param.getVerticeBloque() : ""));
                            processStored.setParameter("coordenadaEste",
                                    (param.getCoordenadaEste() != null ? param.getCoordenadaEste() : 0.0));
                            processStored.setParameter("coordenadaNorte",
                                    (param.getCoordenadaNorte() != null ? param.getCoordenadaNorte() : 0.0));
                            processStored.setParameter("zonaUTM",
                                    (param.getZonaUTM() != null ? param.getZonaUTM() : ""));
                            processStored.setParameter("observacionDetalle",
                                    (param.getObservacionDetalle() != null ? param.getObservacionDetalle() : ""));
                            processStored.setParameter("idOrdenamientoProteccion", obj_.getIdOrdenamientoProteccion());
                            processStored.setParameter("estado", param.getEstado());
                            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
                            processStored.setParameter("usoPotencial",
                                    (param.getUsoPotencial() != null ? param.getUsoPotencial() : ""));
                            processStored.setParameter("actividadesRealizar",
                                    (param.getActividadesRealizar() != null ? param.getActividadesRealizar() : ""));

                            processStored.execute();
                            List<Object[]> spResult = processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    OrdenamientoProteccionDetalleEntity objDet = new OrdenamientoProteccionDetalleEntity();
                                    objDet.setIdOrdenamientoProteccionDet((Integer) row[0]);
                                    objDet.setCodigoTipoOrdenamientoDet((String) row[1]);
                                    objDet.setCategoria((String) row[2]);
                                    objDet.setActividad((String) row[3]);
                                    objDet.setMeta((String) row[4]);
                                    BigDecimal areaHA = ((BigDecimal) row[5]);
                                    objDet.setAreaHA(areaHA);
                                    BigDecimal areaHAPor = ((BigDecimal) row[6]);
                                    objDet.setAreaHAPorcentaje(areaHAPor);
                                    objDet.setDescripcion((String) row[7]);
                                    objDet.setObservacion((String) row[8]);

                                    objDet.setVerticeBloque((String) row[9]);

                                    if (row[10] != null) {
                                        BigDecimal coordEste = ((BigDecimal) row[10]);
                                        objDet.setCoordenadaEste(coordEste);
                                    } else {
                                        objDet.setCoordenadaEste(BigDecimal.ZERO);
                                    }

                                    if (row[11] != null) {
                                        BigDecimal coordNorte = ((BigDecimal) row[11]);
                                        objDet.setCoordenadaNorte(coordNorte);
                                    } else {
                                        objDet.setCoordenadaNorte(BigDecimal.ZERO);
                                    }

                                    objDet.setZonaUTM((String) row[12]);
                                    objDet.setObservacionDetalle((String) row[13]);
                                    objDet.setIdOrdenamientoProtecccion((Integer) row[14]);
                                    objDet.setUsoPotencial((String) row[15]);
                                    objDet.setActividadesRealizar((String) row[16]);
                                    objDet.setEstado((String) row[17]);
                                    listDet.add(objDet);
                                }
                            }
                        }
                        obj_.setListOrdenamientoProteccionDet(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró El Resumen Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<OrdenamientoProteccionEntity> ListarOrdenamientoProteccionFiltro(Integer idPlanManejo,
            String codTipoOrdenamiento) throws Exception {

        try {
            StoredProcedureQuery sp = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccion_ListarFiltro");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoOrdenamiento", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("codTipoOrdenamiento", codTipoOrdenamiento);
            sp.execute();
            List<Object[]> spResultDet = sp.getResultList();

            List<OrdenamientoProteccionEntity> resultDet = null;
            if (spResultDet != null && !spResultDet.isEmpty()) {
                resultDet = new ArrayList<>();
                OrdenamientoProteccionEntity temp = null;
                for (Object[] item : spResultDet) {
                    temp = new OrdenamientoProteccionEntity();
                    temp.setIdOrdenamientoProteccion(item[0] == null ? null : (Integer) item[0]);
                    temp.setCodTipoOrdenamiento(item[1] == null ? null : (String) item[1]);
                    temp.setBloque(item[2] == null ? null : (String) item[2]);
                    temp.setSuperficie(item[3] == null ? null : (String) item[3]);
                    temp.setPunto(item[4] == null ? null : (Integer) item[4]);
                    temp.setCoordenadaEste(item[5] == null ? null : (BigDecimal) item[5]);
                    temp.setCoordenadaNorte(item[6] == null ? null : (BigDecimal) item[6]);
                    temp.setAnexo(item[7] == null ? null : String.valueOf((Character) item[7]));
                    temp.setObservaciones(item[8] == null ? null : (String) item[8]);
                    temp.setMedidas(item[9] == null ? null : (String) item[9]);
                    temp.setDescripcion(item[10] == null ? null : (String) item[10]);
                    temp.setIdPlanManejo(item[11] == null ? null : (Integer) item[11]);
                    temp.setSubCodTipoOrdenamiento(item[12] == null ? null : (String) item[12]);
                    resultDet.add(temp);
                }
            }

            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public List<OrdenamientoProteccionDetalleEntity> ListarOrdenamientoProteccionDetalle(
            Integer idOrdenamientoProteccion, Integer idOrdenamientoProteccionDet) throws Exception {

        try {
            StoredProcedureQuery sp = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccionDetalle_Listar");
            sp.registerStoredProcedureParameter("idOrdenamientoProteccion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idOrdenamientoProteccionDet", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            sp.setParameter("idOrdenamientoProteccion", idOrdenamientoProteccion);
            sp.setParameter("idOrdenamientoProteccionDet", idOrdenamientoProteccionDet);
            sp.execute();
            List<Object[]> spResultDet = sp.getResultList();

            List<OrdenamientoProteccionDetalleEntity> resultDet = null;
            if (spResultDet != null && !spResultDet.isEmpty()) {
                resultDet = new ArrayList<>();
                OrdenamientoProteccionDetalleEntity temp = null;
                for (Object[] item : spResultDet) {
                    temp = new OrdenamientoProteccionDetalleEntity();
                    temp.setIdOrdenamientoProteccionDet(item[0] == null ? null : (Integer) item[0]);
                    temp.setCodigoTipoOrdenamientoDet(item[1] == null ? null : (String) item[1]);
                    temp.setCategoria(item[2] == null ? null : (String) item[2]);
                    temp.setActividad(item[3] == null ? null : (String) item[3]);
                    temp.setMeta(item[4] == null ? null : (String) item[4]);
                    temp.setAreaHA(item[5] == null ? null : (BigDecimal) item[5]);
                    temp.setAreaHAPorcentaje(item[6] == null ? null : (BigDecimal) item[6]);
                    temp.setDescripcion(item[7] == null ? null : (String) item[7]);
                    temp.setObservacion(item[8] == null ? null : (String) item[8]);
                    temp.setVerticeBloque(item[9] == null ? null : (String) item[9]);
                    temp.setCoordenadaEste(item[10] == null ? null : (BigDecimal) item[10]);
                    temp.setCoordenadaNorte(item[11] == null ? null : (BigDecimal) item[11]);
                    temp.setZonaUTM(item[12] == null ? null : (String) item[12]);
                    temp.setObservacionDetalle(item[13] == null ? null : (String) item[13]);
                    temp.setIdOrdenamientoProtecccion(item[14] == null ? null : (Integer) item[14]);
                    temp.setEstado(item[15] == null ? null : ((String) item[15]).trim());
                    resultDet.add(temp);
                }
            }
            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public ResultClassEntity RegistrarOrdenamientoProteccionDetalle(OrdenamientoProteccionDetalleEntity request) {
        ResultClassEntity result = new ResultClassEntity();

        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccionDetalle_Registrar");

            processStored.registerStoredProcedureParameter("codigotipoOrdenamientoDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("meta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHA", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHAPorcentaje", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("verticeBloque", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("zonaUTM", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacionDetalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idOrdenamientoProteccion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("usoPotencial", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividadesRealizar", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("codigotipoOrdenamientoDet", request.getCodigoTipoOrdenamientoDet());
            processStored.setParameter("categoria", request.getCategoria());
            processStored.setParameter("actividad", request.getActividad());
            processStored.setParameter("meta", request.getMeta());
            processStored.setParameter("areaHA", request.getAreaHA());
            processStored.setParameter("areaHAPorcentaje", request.getAreaHAPorcentaje());
            processStored.setParameter("descripcion", request.getDescripcion());
            processStored.setParameter("observacion", request.getObservacion());
            processStored.setParameter("verticeBloque", request.getVerticeBloque());
            processStored.setParameter("coordenadaEste", request.getCoordenadaEste());
            processStored.setParameter("coordenadaNorte", request.getCoordenadaNorte());
            processStored.setParameter("zonaUTM", request.getZonaUTM());
            processStored.setParameter("observacionDetalle", request.getObservacionDetalle());
            processStored.setParameter("idOrdenamientoProteccion", request.getIdOrdenamientoProtecccion());
            processStored.setParameter("idUsuarioRegistro", request.getIdUsuarioModificacion());
            processStored.setParameter("usoPotencial", request.getUsoPotencial());
            processStored.setParameter("actividadesRealizar", request.getActividadesRealizar());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó correctamente ordenamiento proteccion detalle.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ActualizarOrdenamientoProteccionDetalle(OrdenamientoProteccionDetalleEntity request) {
        ResultClassEntity result = new ResultClassEntity();

        try {

            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccionDetalle_Actualizar");

            processStored.registerStoredProcedureParameter("idOrdenamientoProteccionDet", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigotipoOrdenamientoDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("meta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHA", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaHAPorcentaje", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("verticeBloque", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("zonaUTM", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacionDetalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idOrdenamientoProteccion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("usoPotencial", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividadesRealizar", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idOrdenamientoProteccionDet", request.getIdOrdenamientoProteccionDet());
            processStored.setParameter("codigotipoOrdenamientoDet", request.getCodigoTipoOrdenamientoDet());
            processStored.setParameter("categoria", request.getCategoria());
            processStored.setParameter("actividad", request.getActividad());
            processStored.setParameter("meta", request.getMeta());
            processStored.setParameter("areaHA", request.getAreaHA());
            processStored.setParameter("areaHAPorcentaje", request.getAreaHAPorcentaje());
            processStored.setParameter("descripcion", request.getDescripcion());
            processStored.setParameter("observacion", request.getObservacion());
            processStored.setParameter("verticeBloque", request.getVerticeBloque());
            processStored.setParameter("coordenadaEste", request.getCoordenadaEste());
            processStored.setParameter("coordenadaNorte", request.getCoordenadaNorte());
            processStored.setParameter("zonaUTM", request.getZonaUTM());
            processStored.setParameter("observacionDetalle", request.getObservacionDetalle());
            processStored.setParameter("idOrdenamientoProteccion", request.getIdOrdenamientoProtecccion());
            processStored.setParameter("estado", request.getEstado());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());
            processStored.setParameter("usoPotencial", request.getUsoPotencial());
            processStored.setParameter("actividadesRealizar", request.getActividadesRealizar());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó correctamente ordenamiento proteccion detalle.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity EliminarOrdenamientoProteccionDetalle(Integer idOrdenamientoProteccionDet,
            Integer idUsuarioElimina) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoProteccionDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idOrdenamientoProteccionDet", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idOrdenamientoProteccionDet", idOrdenamientoProteccionDet);
            processStored.setParameter("idUsuarioElimina", idUsuarioElimina);
            processStored.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó plan de manejo.");
        return result;
    }

    @Override
    public ResultClassEntity registrarOrdenamientoInterno(List<OrdenamientoProteccionEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoProteccionEntity> list_ = new ArrayList<>();
        try {
            for (OrdenamientoProteccionEntity p : list) {
                StoredProcedureQuery sp = entityManager
                        .createStoredProcedureQuery("dbo.pa_OrdenamientoInterno_Registrar");
                sp.registerStoredProcedureParameter("idOrdenamiento", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codTipoOrdenamiento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subCodTipoOrdenamiento", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                setStoreProcedureEnableNullParameters(sp);

                sp.setParameter("idOrdenamiento", p.getIdOrdenamientoProteccion());
                sp.setParameter("codTipoOrdenamiento", p.getCodTipoOrdenamiento());
                sp.setParameter("subCodTipoOrdenamiento", p.getSubCodTipoOrdenamiento());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("anexo", p.getAnexo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        OrdenamientoProteccionEntity obj_ = new OrdenamientoProteccionEntity();
                        obj_.setIdOrdenamientoProteccion((Integer) row_[0]);
                        obj_.setCodTipoOrdenamiento((String) row_[1]);
                        obj_.setIdPlanManejo((Integer) row_[2]);

                        List<OrdenamientoProteccionDetalleEntity> listDet = new ArrayList<>();
                        for (OrdenamientoProteccionDetalleEntity param : p.getListOrdenamientoProteccionDet()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_OrdenamientoInternoDetalle_Registrar");

                            pa.registerStoredProcedureParameter("idOrdenamientoDetalle", Integer.class,
                                    ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idOrdenamiento", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codTipoOrdenamientoDet", String.class,
                                    ParameterMode.IN);
                            pa.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("areaPorcentaje", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);

                            pa.registerStoredProcedureParameter("verticeBloque", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEste", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorte", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("zonaUTM", String.class, ParameterMode.IN);

                            pa.registerStoredProcedureParameter("observacionDetalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("actividadesRealizar", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoBosque", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("bloqueQuinquenal", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("parcelaCorta", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("usoPotencial", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("actRealizar", String.class, ParameterMode.IN);

                            setStoreProcedureEnableNullParameters(pa);

                            pa.setParameter("idOrdenamientoDetalle", param.getIdOrdenamientoProteccionDet());
                            pa.setParameter("idOrdenamiento", obj_.getIdOrdenamientoProteccion());
                            pa.setParameter("codTipoOrdenamientoDet", param.getCodigoTipoOrdenamientoDet());
                            pa.setParameter("categoria", param.getCategoria());
                            pa.setParameter("area", param.getAreaHA());
                            pa.setParameter("areaPorcentaje", param.getAreaHAPorcentaje());
                            pa.setParameter("descripcion", param.getDescripcion());
                            pa.setParameter("observacion", param.getObservacion());

                            pa.setParameter("verticeBloque", param.getVerticeBloque());
                            pa.setParameter("coordenadaEste", param.getCoordenadaEste());
                            pa.setParameter("coordenadaNorte", param.getCoordenadaNorte());
                            pa.setParameter("zonaUTM", param.getZonaUTM());

                            pa.setParameter("observacionDetalle", param.getObservacionDetalle());
                            pa.setParameter("actividad", param.getActividad());
                            pa.setParameter("actividadesRealizar", param.getActividadesRealizar());
                            pa.setParameter("accion", param.getAccion());
                            pa.setParameter("tipoBosque", param.getTipoBosque());
                            pa.setParameter("bloqueQuinquenal", param.getBloqueQuinquenal());
                            pa.setParameter("parcelaCorta", param.getParcelaCorta());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.setParameter("idArchivo", param.getIdArchivo());
                            pa.setParameter("usoPotencial", param.getUsoPotencial());
                            pa.setParameter("actRealizar", param.getActRealizar());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                OrdenamientoProteccionDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new OrdenamientoProteccionDetalleEntity();
                                    obj.setIdOrdenamientoProteccionDet((Integer) row[0]);
                                    obj.setCodigoTipoOrdenamientoDet((String) row[1]);
                                    obj.setCategoria((String) row[2]);
                                    obj.setAreaHA((BigDecimal) row[3]);
                                    obj.setDescripcion((String) row[4]);
                                    obj.setObservacion((String) row[5]);

                                    obj.setVerticeBloque((String) row[6]);
                                    obj.setCoordenadaEste((BigDecimal) row[7]);
                                    obj.setCoordenadaNorte((BigDecimal) row[8]);
                                    obj.setZonaUTM((String) row[9]);

                                    obj.setObservacionDetalle((String) row[10]);
                                    obj.setActividad((String) row[11]);
                                    obj.setActividadesRealizar((String) row[12]);
                                    obj.setAreaHAPorcentaje((BigDecimal) row[13]);
                                    obj.setTipoBosque((String) row[14]);
                                    obj.setBloqueQuinquenal((String) row[15]);
                                    obj.setParcelaCorta((String) row[16]);
                                    obj.setUsoPotencial((String) row[17]);
                                    obj.setParcelaCorta((String) row[18]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListOrdenamientoProteccionDet(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el ordenamiento interno correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity obtenerOrdenamientoInterno(Integer idPlanManejo, String codTipoOrdenamiento)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        OrdenamientoProteccionEntity ordenamientoProteccionEntity = new OrdenamientoProteccionEntity();
        try {

            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_OrdenamientoInterno_Obtener");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoOrdenamiento", String.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("codTipoOrdenamiento", codTipoOrdenamiento);
            sp.execute();

            List<Object[]> spResult_ = sp.getResultList();
            if (spResult_.size() > 0) {
                ordenamientoProteccionEntity.setIdPlanManejo(Integer.parseInt(spResult_.get(0)[0].toString()));
                ordenamientoProteccionEntity
                        .setIdOrdenamientoProteccion(Integer.parseInt(spResult_.get(0)[1].toString()));
            } else {
                ordenamientoProteccionEntity.setIdPlanManejo(idPlanManejo);
                ordenamientoProteccionEntity.setIdOrdenamientoProteccion(0);
            }

            result.setData(ordenamientoProteccionEntity);
            result.setSuccess(true);
            result.setMessage("Se obtuvo el ordenamiento interno correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity EliminarOrdenamientoInterno(OrdenamientoProteccionDetalleEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_OrdenamientoInterno_Eliminar]");
        processStored.registerStoredProcedureParameter("idOrdenamiento", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idOrdenamientoDetalle", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codCabecera", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idOrdenamiento", param.getIdOrdenamientoProtecccion());
        processStored.setParameter("idOrdenamientoDetalle", param.getIdOrdenamientoProteccionDet());
        processStored.setParameter("codCabecera", param.getCodigoTipoOrdenamientoDet());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

    @Override
    public ResultEntity<OrdenamientoProteccionEntity> ListarOrdenamientoInternoCompleto(
            OrdenamientoProteccionEntity request) {
        ResultEntity<OrdenamientoProteccionEntity> resul = new ResultEntity<OrdenamientoProteccionEntity>();
        try {
            List<OrdenamientoProteccionEntity> objList = new ArrayList<>();
            String sql = "EXEC pa_OrdenamientoInterno_Listar " + request.getIdPlanManejo() + ","
                    + request.getCodTipoOrdenamiento();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for (Map<String, Object> row : rows) {

                OrdenamientoProteccionEntity objOrdPro = new OrdenamientoProteccionEntity();
                objOrdPro.setIdOrdenamientoProteccion((Integer) row.get("idOrdenamientoProteccion"));
                objOrdPro.setCodTipoOrdenamiento((String) row.get("codigoTipoOrdenamientoProteccion"));
                objOrdPro.setAnexo((String) row.get("anexo"));
                objOrdPro.setEsSistemaPoliciclico((Boolean) row.get("esSistemaPoliciclico"));
                objOrdPro.setJustificacion((String) row.get("justificacion"));

                OrdenamientoProteccionDetalleEntity objOrdProDet = new OrdenamientoProteccionDetalleEntity();
                objOrdProDet.setIdOrdenamientoProteccionDet((Integer) row.get("idOrdenamientoProteccionDet"));
                ;
                objOrdProDet.setCodigoTipoOrdenamientoDet((String) row.get("codigoTipoOrdenamientoDet"));
                objOrdProDet.setCategoria((String) row.get("categoria"));
                objOrdProDet.setActividad((String) row.get("actividad"));
                objOrdProDet.setMeta((String) row.get("meta"));
                objOrdProDet.setAreaHA((BigDecimal) row.get("areaHA"));
                objOrdProDet.setAreaHAPorcentaje((BigDecimal) row.get("areaHAPorcentaje"));
                objOrdProDet.setDescripcion((String) row.get("descripcion"));
                objOrdProDet.setObservacion((String) row.get("observacion"));
                objOrdProDet.setVerticeBloque((String) row.get("verticeBloque"));

                if (row.get("coordenadaEste") != null) {
                    BigDecimal coordEste = ((BigDecimal) row.get("coordenadaEste"));
                    objOrdProDet.setCoordenadaEste(coordEste);
                } else {
                    objOrdProDet.setCoordenadaEste(BigDecimal.ZERO);
                }

                if (row.get("coordenadaNorte") != null) {
                    BigDecimal coordNorte = ((BigDecimal) row.get("coordenadaNorte"));
                    objOrdProDet.setCoordenadaNorte(coordNorte);
                } else {
                    objOrdProDet.setCoordenadaNorte(BigDecimal.ZERO);
                }

                objOrdProDet.setZonaUTM((String) row.get("zonaUTM"));
                objOrdProDet.setObservacionDetalle((String) row.get("observacionDetalle"));
                objOrdProDet.setUsoPotencial((String) row.get("usoPotencial"));
                objOrdProDet.setActividadesRealizar((String) row.get("actividadesRealizar"));

                List<OrdenamientoProteccionDetalleEntity> objListDet = new ArrayList<>();
                objListDet.add(objOrdProDet);

                objOrdPro.setListOrdenamientoProteccionDet(objListDet);
                objList.add(objOrdPro);

            }
            resul.setData(objList);
            resul.setMessage((objList.size() != 0 ? "Información encontrada" : "Informacion no encontrada"));
            resul.setIsSuccess((objList.size() != 0 ? true : false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

}
