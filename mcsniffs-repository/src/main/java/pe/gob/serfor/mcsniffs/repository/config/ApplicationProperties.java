package pe.gob.serfor.mcsniffs.repository.config;

import lombok.Data;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Configuration
@Data 
//@PropertySource(value="file:${jboss.server.config.dir}/mcsniffs-properties/application.properties")
public class ApplicationProperties {
}