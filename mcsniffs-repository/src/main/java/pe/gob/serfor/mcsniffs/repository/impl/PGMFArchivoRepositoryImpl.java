package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.CensoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFArchivoRepository;

import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import com.google.common.io.Files;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;

import javax.annotation.PostConstruct;
import javax.persistence.Parameter;
import javax.sql.DataSource;

import org.slf4j.LoggerFactory;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;

@Repository
public class PGMFArchivoRepositoryImpl extends JdbcDaoSupport implements PGMFArchivoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final String SEPARADOR_ARCHIVO = ".";

    @Value("${smb.file.server.path}")
    private String fileServerPath;
    @Autowired
    private FileServerConexion fileServerConexion;

    @Autowired
    private ArchivoRepositoryImpl respositoryArchivo;
    
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {Repository de los archivos relaciones con el plan de manejo}
     *
     */

    @PersistenceContext
    private EntityManager em;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PGMFArchivoRepositoryImpl.class);

    @Autowired
    private ArchivoRepository archivo;

    @Autowired
    private CensoForestalRepository censo;

    // @Autowired
    // private AnexoRepository obteneranexo;

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {registrar archivos}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultClassEntity registrarArchivo(PGMFArchivoEntity obj) throws Exception{
        ResultClassEntity result = new ResultClassEntity();
        execGuardarArchivo(obj);
        result.setData(obj);
        result.setSuccess(true);
        result.setMessage("Se registró el adjunto el archivo PGMF.");
        return result;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm = null;
        try {
            Date date = new Date();
            con = dataSource.getConnection();
            // Adjuntar Documentos
            pstm = con.prepareCall("{call pa_PGMFArchivo_insertar (?,?,?,?,?,?,?,?)}");
            pstm.setObject(1, obj.getCodigoTipoPGMF(), Types.VARCHAR);
            pstm.setObject(2, obj.getCodigoSubTipoPGMF(), Types.VARCHAR);
            pstm.setObject(3, obj.getDescripcion(), Types.VARCHAR);
            pstm.setObject(4, obj.getObservacion(), Types.VARCHAR);
            pstm.setObject(5, obj.getIdPlanManejo(), Types.INTEGER);
            pstm.setObject(6, obj.getIdArchivo(), Types.INTEGER);
            pstm.setObject(7, obj.getIdUsuarioRegistro(), Types.INTEGER);
            pstm.setObject(8, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            Integer salida2 = pstm.executeUpdate();
            ResultSet rsa2 = pstm.getGeneratedKeys();
            if (rsa2.next()) {
                salida2 = rsa2.getInt(1);
            }
            result.setCodigo(salida2);
            result.setSuccess(true);
            result.setMessage("Se registró el adjunto el archivo PGMF.");
            return result;
        } catch (Exception e) {
            log.error("PGMFArchivoRepositoryImpl - registrarArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally {
            try {
                if (pstm != null)
                    pstm.close();
                if (con != null)
                    con.close();
            } catch (Exception e2) {
                log.error("PGMFArchivoRepositoryImpl - registrarArchivo", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
        }
        */
    }

    @Override
    public ResultClassEntity<PGMFArchivoEntity> registrarPlanManejoArchivo(PGMFArchivoEntity item) throws Exception {
        ResultClassEntity<PGMFArchivoEntity> result = new ResultClassEntity<>();

        execGuardarArchivo(item);
        result.setData(item);
        result.setSuccess(true);
        result.setMessage("Se registró el adjunto el archivo PGMF.");
        return result;
    }

    private Integer execGuardarArchivo(PGMFArchivoEntity item) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PlanManejoArchivo_Insertar");
        sp.registerStoredProcedureParameter("idPlanManejoArchivo", Integer.class, ParameterMode.INOUT);
        sp.registerStoredProcedureParameter("codigoTipoPGMF", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("codigoSubTipoPGMF", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("acapite", String.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(sp);
        sp.setParameter("idPlanManejoArchivo", item.getIdPGMFArchivo());
        sp.setParameter("codigoTipoPGMF", item.getCodigoTipoPGMF());
        sp.setParameter("codigoSubTipoPGMF", item.getCodigoSubTipoPGMF());
        sp.setParameter("descripcion", item.getDescripcion());
        sp.setParameter("observacion", item.getObservacion());
        sp.setParameter("idPlanManejo", item.getIdPlanManejo());
        sp.setParameter("idArchivo", item.getIdArchivo());
        sp.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
        sp.setParameter("asunto", item.getAsunto());
        sp.setParameter("acapite", item.getAcapite());
        sp.setParameter("conforme", item.getConforme());
        sp.setParameter("detalle", item.getDetalle());
        sp.execute();
        Integer id = (Integer) sp.getOutputParameterValue("idPlanManejoArchivo");
        item.setIdPGMFArchivo(id);
        return id;
    }

    /**
     * @autor: Abner Valdez [02-09-2021]
     * @modificado:
     * @descripción: {Listar detalle archivo por idPlanManejo}
     * @param:idPlanManejo
     */
    @Override
    public ResultEntity<PGMFArchivoEntity> listarDetalleArchivo(Integer idPlanManejo, String codigoTipoPGMF) {
        ResultEntity<PGMFArchivoEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery ps = entityManager.createStoredProcedureQuery("dbo.pa_PGMFArchivoDetalle_Listar");
            ps.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            ps.registerStoredProcedureParameter("codigoTipoPGMF", String.class, ParameterMode.IN);
            ps.setParameter("idPlanManejo", idPlanManejo);
            ps.setParameter("codigoTipoPGMF", codigoTipoPGMF);
            ps.execute();
            List<Object[]> spResult = ps.getResultList();
            List<PGMFArchivoEntity> lstresult = new ArrayList<>();
            if (!spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    PGMFArchivoEntity item = new PGMFArchivoEntity();
                    item.setIdPGMFArchivo((Integer) row[0]);
                    item.setCodigoTipoPGMF((String) row[1]);
                    item.setCodigoSubTipoPGMF((String) row[2]);
                    item.setDescripcion((String) row[3]);
                    item.setObservacion((String) row[4]);
                    item.setIdPlanManejo((Integer) row[5]);
                    item.setIdArchivo((Integer) row[6]);
                    item.setEstado((String) row[7]);
                    item.setIdUsuarioRegistro((Integer) row[8]);
                    item.setNombre((String) row[9]);
                    item.setTipoDocumento((String) row[10]);
                    if (item.getIdArchivo()!=null) {
                        ResultClassEntity<ArchivoEntity> file = archivo.obtenerArchivo(item.getIdArchivo());

                        if(file.getData() != null){
                            item.setDocumento(file.getData().getFile());
                        }
                        else{
                            item.setDocumento(null);
                        }

                    }
                    lstresult.add(item);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron correctamente.");
        } catch (Exception e) {
            result.setData(null);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;
    }
    /**
     * @autor: Abner Valdez [6-09-2021]
     * @modificado:
     * @descripción: {eliminar detalle archivo}
     * @param:Integer idArchivo, Integer idUsuario
     */
    @Override
    public ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception {
        ResultClassEntity<Integer> result = new ResultClassEntity<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_PGMFArchivoDetalle_Eliminar");
            sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
            sp.setParameter("idArchivo", idArchivo);
            sp.setParameter("idUsuario", idUsuario);
            sp.execute();
            result.setData(idArchivo);
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
        } catch (Exception e) {
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;
    }

    /**
     * @autor: JaquelineDB [1-09-2021]
     * @modificado:
     * @descripción: {listar los archivos del proceso}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultEntity<PGMFArchivoEntity> listarArchivosPGMF(PGMFArchivoEntity filtro) {
        ResultEntity<PGMFArchivoEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PGMFArchivo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", filtro.getIdPlanManejo());
            processStored.setParameter("idArchivo", filtro.getIdArchivo());
            processStored.setParameter("idTipoDocumento", filtro.getIdTipoDocumento());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<PGMFArchivoEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PGMFArchivoEntity obj = new PGMFArchivoEntity();
                    obj.setIdPGMFArchivo((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setIdArchivo((Integer) row[2]);
                    obj.setCodigoTipoPGMF((String) row[3]);
                    obj.setCodigoSubTipoPGMF((String) row[4]);
                    obj.setDescripcion((String) row[5]);
                    obj.setObservacion((String) row[6]);
                    obj.setNombreArchivo((String) row[7]);
                    obj.setExtensionArchivo((String) row[8]);
                    obj.setIdTipoDocumento((Integer) row[9]);
                    obj.setTipoDocumento((String) row[10]);
                    if (!obj.getIdArchivo().equals(null)) {
                        ResultClassEntity<ArchivoEntity> file = archivo.obtenerArchivo(obj.getIdArchivo());
                        obj.setDocumento(file.getData().getFile());
                    }
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los documentos vigentes.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [2-09-2021]
     * @modificado:
     * @descripción: {se genera el anexo2pgmf}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo2PGMF(PGMFArchivoEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            List<CensoForestalDetalleEntity> lstLista = censo.ListarCensoForestalDetalle(filtro.getIdPlanManejo(),"","");

            InputStream inputStream = AnexosPFDMRepositoryImpl.class.getResourceAsStream("/plantillaprueba.docx");

            XWPFDocument doc = new XWPFDocument(inputStream);

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row2 = table.getRow(1);
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("namex")) {
                                        text5 = text5.replace("namex", lstLista.get(i).getCorrelativo());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("namecientificox")) {
                                        text5 = text5.replace("namecientificox",
                                                lstLista.get(i).getIdNombreCientifico());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("familiax")) {
                                        text5 = text5.replace("familiax", lstLista.get(i).getCodigoArbolCalidad());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("gcx")) {
                                        text5 = text5.replace("gcx", lstLista.get(i).getDescripcionOtros());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            byte[] fileContent = b.toByteArray();
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo3.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 3.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo3", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [2-09-2021]
     * @modificado:
     * @descripción: {se genera el anexo3pgmf}
     * @param:PGMFArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo3PGMF(PGMFArchivoEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            List<CensoForestalDetalleEntity> lstLista = censo.ListarCensoForestalDetalle(filtro.getIdPlanManejo(),"","");

            InputStream inputStream = AnexosPFDMRepositoryImpl.class.getResourceAsStream("/plantillaquincenal.docx");

            XWPFDocument doc = new XWPFDocument(inputStream);

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row2 = table.getRow(4);
            XWPFTableRow row5 = table.getRow(5);
            XWPFTableRow row6 = table.getRow(6);
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                XWPFTableRow copiedRow5 = new XWPFTableRow((CTRow) row5.getCtRow().copy(), table);
                XWPFTableRow copiedRow6 = new XWPFTableRow((CTRow) row6.getCtRow().copy(), table);
                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("primero1")) {
                                        text5 = text5.replace("primero1", lstLista.get(i).getCorrelativo());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("segundo1")) {
                                        text5 = text5.replace("segundo1", lstLista.get(i).getIdNombreCientifico());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                for (XWPFTableCell cell : copiedRow5.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("primero2")) {
                                        text5 = text5.replace("primero2", lstLista.get(i).getCorrelativo());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("segundo3")) {
                                        text5 = text5.replace("segundo3", lstLista.get(i).getIdNombreCientifico());
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
                table.addRow(copiedRow5);
                table.addRow(copiedRow6);
            }

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            byte[] fileContent = b.toByteArray();
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo3.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 3.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo3", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

}
