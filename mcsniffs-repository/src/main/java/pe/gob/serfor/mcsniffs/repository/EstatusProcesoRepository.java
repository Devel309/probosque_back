package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.Dto.EstatusProceso.ListarEstadoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;


public interface EstatusProcesoRepository {
    ResultClassEntity<List<ListarEstadoDto>> listarEstadoEstatusProceso(ListarEstadoDto request);
}
