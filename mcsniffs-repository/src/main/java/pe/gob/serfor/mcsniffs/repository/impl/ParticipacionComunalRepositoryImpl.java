package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ParticipacionComunalDto;
import pe.gob.serfor.mcsniffs.repository.ParticipacionComunalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Repository
public class ParticipacionComunalRepositoryImpl extends JdbcDaoSupport implements ParticipacionComunalRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(ParticipacionComunalRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    /**
     * @autor: Ivan Minaya Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método que registra la participacion comunal}
     * @param: Objeto ParticipacionComunalEntity
     *
     * @return: List<ParticipacionComunalEntity>
     */
    @Override
    public ResultClassEntity RegistrarParticipacionComunal(List<ParticipacionComunalEntity> list) {
        ResultClassEntity result = new ResultClassEntity();
        List<ParticipacionComunalEntity> list_ = new ArrayList<>();
        try{
            for(ParticipacionComunalEntity p:list){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionComunal_Registrar");
                processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("seguimientoActividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("CodigoParticipacionC", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPartComunal", p.getIdPartComunal());
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("seguimientoActividad", p.getSeguimientoActividad());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.setParameter("CodigoParticipacionC", p.getCodTipoPartComunal());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();
                        temp.setIdPartComunal((Integer) row[0]);
                        pla.setIdPlanManejo((Integer) row[1]);
                        temp.setPlanManejo(pla);
                        temp.setActividad((String) row[2]);
                        temp.setResponsable((String) row[3]);
                        temp.setSeguimientoActividad((String) row[4]);
                        list_.add(temp);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la participación comunal correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método que actualizar la participacion comunal}
     * @param: Objeto ParticipacionComunalEntity
     *
     * @return: List<ParticipacionComunalEntity>
     */
    @Override
    public ResultClassEntity ActualizarParticipacionComunal(List<ParticipacionComunalEntity> list) {
        ResultClassEntity result = new ResultClassEntity();
        List<ParticipacionComunalEntity> list_ = new ArrayList<>();
        try{
            for(ParticipacionComunalEntity p:list){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionComunal_Actualizar");
                processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("seguimientoActividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPartComunal", p.getIdPartComunal());
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("seguimientoActividad", p.getSeguimientoActividad());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();
                        temp.setIdPartComunal((Integer) row[0]);
                        pla.setIdPlanManejo((Integer) row[1]);
                        temp.setPlanManejo(pla);
                        temp.setActividad((String) row[2]);
                        temp.setResponsable((String) row[3]);
                        temp.setSeguimientoActividad((String) row[4]);
                        list_.add(temp);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se actualizó la Participación Comunal correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método que elimina la participacion comunal}
     * @param: Objeto ParticipacionComunalEntity
     *
     * @return: List<ParticipacionComunalEntity>
     */
    @Override
    public ResultClassEntity EliminarParticipacionComunal(ParticipacionComunalEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionComunal_Eliminar");
                processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPartComunal", param.getIdPartComunal());
                processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
                processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método para obtener la participacion comunal}
     * @param: Objeto ParticipacionComunalEntity
     *
     * @return: List<ParticipacionComunalEntity>
     */
    @Override
    public ResultClassEntity ObtenerParticipacionComunal(ParticipacionComunalEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionComunal_Obtener");
            processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPartComunal", param.getIdPartComunal());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {

                    PlanManejoEntity pla = new PlanManejoEntity();
                    temp.setIdPartComunal((Integer) row[0]);
                    pla.setIdPlanManejo((Integer) row[1]);
                    temp.setPlanManejo(pla);
                    temp.setActividad((String) row[2]);
                    temp.setResponsable((String) row[3]);
                    temp.setSeguimientoActividad((String) row[4]);
                }
            }
            result.setData(temp);
            result.setSuccess(true);
            result.setMessage("Se obtiene el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    /**
     * @autor: Ivan Minaya Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método que lista por filtro la participacion comunal}
     * @param: Objeto ParticipacionComunalEntity
     *
     * @return: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity ListarPorFiltroParticipacionComunal(ParticipacionComunalEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        List<ParticipacionComunalEntity> list_ = new ArrayList<>();
        try{
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionComunal_ListarPorFiltro");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idParticiComunal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoPartComunal", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
                processStored.setParameter("idParticiComunal", param.getIdPartComunal());
                processStored.setParameter("codigoPartComunal", param.getCodTipoPartComunal());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                        temp.setIdPartComunal((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setActividad((String) row[2]);
                        temp.setResponsable((String) row[3]);
                        temp.setSeguimientoActividad((String) row[4]);
                        temp.setCodTipoPartComunal((String) row[5]);
                        list_.add(temp);
                    }
                }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se listo la participacion comunal comunal.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    /**
     * @autor: Harry Coa [23-07-2021]
     * @modificado:
     * @descripción: {Método que RegistrarParticipacionCiudadana}
     * @param: Objeto ParticipacionComunalDto
     *
     * @return: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity RegistrarParticipacionCiudadana(ParticipacionComunalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionCiudadanaPo_Registrar");
            processStored.registerStoredProcedureParameter("codTipoPartComunal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("seguimientoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fecha", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("codTipoPartComunal", param.getCodTipoPartComunal());
            processStored.setParameter("actividad", param.getActividad());
            processStored.setParameter("responsable", param.getResponsable());
            processStored.setParameter("seguimientoActividad", param.getSeguimientoActividad());
            processStored.setParameter("lugar", param.getLugar());
            processStored.setParameter("fecha", param.getFecha());
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();

            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró la participación ciudadana correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    /**
     * @autor: Harry Coa [23-07-2021]
     * @modificado:
     * @descripción: {Método que RegistrarParticipacionCiudadana}
     * @param: Objeto ParticipacionComunalDto
     *
     * @return: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity ActualizarParticipacionCiudadana(ParticipacionComunalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionCiudadanaPo_Actualizar");
            processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoPartComunal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("seguimientoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fecha", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPartComunal", param.getIdPartComunal());
            processStored.setParameter("codTipoPartComunal", param.getCodTipoPartComunal());
            processStored.setParameter("actividad", param.getActividad());
            processStored.setParameter("responsable", param.getResponsable());
            processStored.setParameter("seguimientoActividad", param.getSeguimientoActividad());
            processStored.setParameter("lugar", param.getLugar());
            processStored.setParameter("fecha", param.getFecha());
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró la participación ciudadana correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }


    /**
     * @autor: Harry Coa [23-07-2021]
     * @modificado:
     * @descripción: {Método que RegistrarParticipacionCiudadana}
     * @param: Objeto ParticipacionComunalDto
     *
     * @return: ParticipacionComunalEntity
     */
    @Override
    public ResultClassEntity EliminarParticipacionCiudadana(ParticipacionComunalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionCiudadanaPo_Eliminar");
            processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPartComunal", param.getIdPartComunal());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado:
     * @descripción: {Listar Participacion Ciudadana}
     * @param:Capacitacion
     */
    @Override
    public List<ParticipacionComunalEntity> ListarParticipacionCiudadana(ParticipacionComunalEntity param) throws Exception {


        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionCiudadanaPo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.execute();
            List<ParticipacionComunalEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                    PlanManejoEntity pla = new PlanManejoEntity();
                    temp.setIdPartComunal((Integer) row[0]);
                    pla.setIdPlanManejo((Integer) row[1]);
                    temp.setPlanManejo(pla);
                    temp.setCodTipoPartComunal((String) row[2]);
                    temp.setActividad((String) row[3]);
                    temp.setResponsable((String) row[4]);
                    temp.setSeguimientoActividad((String) row[5]);
                    temp.setLugar((String) row[6]);
                    temp.setFecha((Date) row[7]);
                    result.add(temp);
                }
            }
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity RegistrarParticipacionCiudadanaList(List<ParticipacionComunalEntity> list) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        List<ParticipacionComunalEntity> list_ = new ArrayList<>();
        try{
            for(ParticipacionComunalEntity p:list){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionCiudadanaPo_RegistrarList");
                processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("seguimientoActividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codTipoPartComunal", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fecha", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPartComunal", p.getIdPartComunal());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("seguimientoActividad", p.getSeguimientoActividad());
                processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                processStored.setParameter("codTipoPartComunal", p.getCodTipoPartComunal());
                processStored.setParameter("lugar", p.getLugar());
                if (p.getFecha() != null) {
                    processStored.setParameter("fecha", p.getFecha());
                } else{
                    processStored.setParameter("fecha",new Date());
                }
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                        PlanManejoEntity pla = new PlanManejoEntity();
                        temp.setIdPartComunal((Integer) row[0]);
                        pla.setIdPlanManejo((Integer) row[1]);
                        temp.setPlanManejo(pla);
                        temp.setActividad((String) row[2]);
                        temp.setResponsable((String) row[3]);
                        temp.setSeguimientoActividad((String) row[4]);
                        temp.setCodTipoPartComunal((String) row[5]);
                        temp.setLugar((String) row[6]);
                        temp.setFecha((Date) row[7]);
                        list_.add(temp);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la participación ciudadana correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity registrarParticipacionComunalCaberaDetalle(List<ParticipacionComunalEntity> participacionComunal) {
         ResultClassEntity result = new ResultClassEntity();
        List<ParticipacionComunalEntity> list_ = new ArrayList<>();
        try{
            for(ParticipacionComunalEntity p:participacionComunal){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticipacionCiudadanaPo_RegistrarList");
                processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("seguimientoActividad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codTipoPartComunal", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("fecha", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPartComunal", p.getIdPartComunal());
                processStored.setParameter("actividad", p.getActividad());
                processStored.setParameter("responsable", p.getResponsable());
                processStored.setParameter("seguimientoActividad", p.getSeguimientoActividad());
                processStored.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored.setParameter("codTipoPartComunal", p.getCodTipoPartComunal());
                processStored.setParameter("lugar", p.getLugar());
                if (p.getFecha() != null) {
                    processStored.setParameter("fecha", p.getFecha());
                } else{
                    processStored.setParameter("fecha",new Date());
                }
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                        temp.setIdPartComunal((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setPlanManejo(null);
                        temp.setActividad((String) row[2]);
                        temp.setResponsable((String) row[3]);
                        temp.setSeguimientoActividad((String) row[4]);
                        temp.setCodTipoPartComunal((String) row[5]);
                        temp.setLugar((String) row[6]);
                        temp.setFecha((Date) row[7]);
                        for(ParticipacionComunalDetEntity d:p.getLstDetalle()){
                            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ParticiapacionComunalDetalle_Registrar");
                            sp.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("idPartComunalDet", Integer.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("codigoPartComunal", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("codPartComunalDet", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("participacion", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("metodologia", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("mecanismos", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("fecha", Date.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(sp);
                            sp.setParameter("idPartComunal", temp.getIdPartComunal());
                            sp.setParameter("idPartComunalDet", d.getIdPartComunalDet());
                            sp.setParameter("codigoPartComunal", d.getCodigoPartComunal());
                            sp.setParameter("codPartComunalDet", d.getCodPartComunalDet());
                            sp.setParameter("participacion", d.getParticipacion());
                            sp.setParameter("metodologia", d.getMetodologia());
                            sp.setParameter("mecanismos", d.getMecanismos());
                            sp.setParameter("actividad", d.getActividad());
                            sp.setParameter("lugar", d.getLugar());
                            sp.setParameter("fecha", d.getFecha());
                            sp.setParameter("descripcion", d.getDescripcion());
                            sp.setParameter("observacion", d.getObservacion());
                            sp.setParameter("detalle", d.getDetalle());
                            sp.setParameter("idUsuarioRegistro", d.getIdUsuarioRegistro());
                            processStored.execute();
                            List<ParticipacionComunalDetEntity> lstdetalle= new ArrayList<>();
                            List<Object[]> spResult2 =sp.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row2 : spResult2) {
                                    ParticipacionComunalDetEntity temp2 = new ParticipacionComunalDetEntity();
                                    temp2.setIdPartComunalDet((Integer) row2[0]);
                                    temp2.setIdPartComunal((Integer) row2[1]);
                                    temp2.setCodigoPartComunal((String) row2[2]);
                                    temp2.setCodPartComunalDet((String) row2[3]);
                                    temp2.setParticipacion((String) row2[4]);
                                    temp2.setMetodologia((String) row2[5]);
                                    temp2.setMecanismos((String) row2[6]);
                                    temp2.setActividad((String) row2[7]);
                                    temp2.setLugar((String) row2[8]);
                                    temp2.setFecha((Date) row2[9]);
                                    temp2.setDescripcion((String) row2[3]);
                                    temp2.setObservacion((String) row2[4]);
                                    temp2.setDetalle((String) row2[5]);
                                    lstdetalle.add(temp2);
                                }
                                temp.setLstDetalle(lstdetalle);
                            }      
                        }
                        list_.add(temp);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la participación ciudadana correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultEntity<ParticipacionComunalEntity> listarParticipacionComunalCaberaDetalle(ParticipacionComunalParamEntity participacionComunal) {
        ResultEntity<ParticipacionComunalEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticiapacionComunalDetalle_ListarCabera");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoPartComunal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdPartComunal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", participacionComunal.getIdPlanManejo());
            processStored.setParameter("codTipoPartComunal", participacionComunal.getCodTipoPartComunal());
            processStored.setParameter("IdPartComunal", participacionComunal.getIdPartComunal());
            processStored.execute();
            List<ParticipacionComunalEntity> lstcabera = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    ParticipacionComunalEntity temp = new ParticipacionComunalEntity();
                    temp.setIdPartComunal((Integer) row[0]);
                    temp.setIdPlanManejo((Integer) row[1]);
                    temp.setPlanManejo(null);
                    temp.setActividad((String) row[2]);
                    temp.setResponsable((String) row[3]);
                    temp.setSeguimientoActividad((String) row[4]);
                    temp.setCodTipoPartComunal((String) row[5]);
                    temp.setLugar((String) row[6]);
                    temp.setFecha((Date) row[7]);
                    if(temp.getIdPartComunal()!=null){
                        StoredProcedureQuery processStored2 = entityManager.createStoredProcedureQuery("dbo.pa_ParticiapacionComunalDetalle_Listar");
                        processStored2.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                        processStored2.registerStoredProcedureParameter("codTipoPartComunal", String.class, ParameterMode.IN);
                        processStored2.registerStoredProcedureParameter("codTipoPartComunalDet", String.class, ParameterMode.IN);
                        processStored2.registerStoredProcedureParameter("IdPartComunal", Integer.class, ParameterMode.IN);
                        SpUtil.enableNullParams(processStored2);
                        processStored2.setParameter("idPlanManejo", participacionComunal.getIdPlanManejo());
                        processStored2.setParameter("codTipoPartComunal", participacionComunal.getCodTipoPartComunal());
                        processStored2.setParameter("codTipoPartComunalDet", participacionComunal.getCodTipoPartComunalDet());
                        processStored2.setParameter("IdPartComunal", temp.getIdPartComunal());
                        processStored2.execute();
                        List<ParticipacionComunalDetEntity> lstcabera2 = new ArrayList<>();
                        List<Object[]> spResult2 = processStored2.getResultList();
                        if (spResult2.size() >= 1){
                            for (Object[] row2: spResult2){
                                ParticipacionComunalDetEntity temp2 = new ParticipacionComunalDetEntity();
                                temp2.setIdPartComunalDet((Integer) row2[8]);
                                temp2.setCodigoPartComunal((String) row2[9]);
                                temp2.setCodPartComunalDet((String) row2[10]);
                                temp2.setParticipacion((String) row2[11]);
                                temp2.setMetodologia((String) row2[12]);
                                temp2.setMecanismos((String) row2[13]);
                                temp2.setActividad((String) row2[14]);
                                temp2.setLugar((String) row2[15]);
                                temp2.setFecha((Date) row2[16]);
                                temp2.setDescripcion((String) row2[17]);
                                temp2.setObservacion((String) row2[18]);
                                temp2.setDetalle((String) row2[19]);
                                lstcabera2.add(temp2);
                            }
                        }
                        temp.setLstDetalle(lstcabera2);
                    }
                    lstcabera.add(temp);
                }
            }
            result.setData(lstcabera);
            result.setIsSuccess(true);
            result.setMessage(lstcabera.size()>0?"Se listo correctamente":"No se econtraron registros");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error. "+e.getMessage());
            return  result;
        }
    }
    @Override
    public ResultClassEntity eliminarParticipacionComunalCaberaDetalle(
            ParticipacionComunalParamEntity participacionComunal) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParticiapacionComunalDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idPartComunal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPartComunalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPartComunal", participacionComunal.getIdPartComunal());
            processStored.setParameter("idPartComunalDet", participacionComunal.getIdPartComunalDet());
            processStored.setParameter("idUsuarioElimina", participacionComunal.getIdUsuarioElimina());
            processStored.execute();
            result.setData(participacionComunal);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


}
