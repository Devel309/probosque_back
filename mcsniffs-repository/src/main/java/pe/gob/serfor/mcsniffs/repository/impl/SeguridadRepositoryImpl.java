package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;
import pe.gob.serfor.mcsniffs.entity.seguridad.UserLoginDto;
import pe.gob.serfor.mcsniffs.repository.SeguridadRepository;
import pe.gob.serfor.mcsniffs.repository.util.Security;

@Repository
public class SeguridadRepositoryImpl extends JdbcDaoSupport implements SeguridadRepository {
	@Autowired
	@Qualifier("DataSourceBDSEGURIDAD")
	DataSource dataSource;

	@Autowired
	private Security security;
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}

	@Override
	public SeguridadEntity LoginUser(SeguridadEntity request) {
		SeguridadEntity userEnty = new SeguridadEntity();
		try
		{
			String sql = "EXEC SVC_BuscarUsuarioLogin '"
					+ request.getUsername()
					+"','"+  security.aesEncrypt(request.getPassword())
					+"','MC'";
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
			UserLoginDto obj = new UserLoginDto();
				if(rows.size()!=0) {
				for(Map<String, Object> row:rows){
					obj.setIDUsuario((Integer)row.get("IDUsuario"));
					obj.setNombres((String)row.get("nombres")) ;
					obj.setApellidoPaterno((String)row.get("ApellidoPaterno")) ;
					obj.setApellidoMaterno((String)row.get("ApellidoMaterno")) ;
					obj.setNumeroDocumento((String)row.get("NumeroDocumento")) ;
					obj.setCodigoEmpleado((String)row.get("CodigoEmpleado")) ;
					obj.setCodigoUsuario((String)row.get("CodigoUsuario")) ;
					obj.setUsuario((String)row.get("Usuario")) ;
					obj.setUsuarioDescripcion((String)row.get("Descripcion")) ;
					obj.setContraseña((String)row.get("Contraseña"));
					obj.setCorreoElectronico((String)row.get("CorreoElectronico")) ;
					obj.setEstado((String)row.get("Estado")) ;
					obj.setSYSUsuario((Integer)row.get("SYSUsuario")) ;
					obj.setSYSFecha((Date)row.get("SYSFecha")) ;
				};
				if(!obj.getContraseña().equals("")){
					userEnty.setUsername(obj.getUsuario());
					userEnty.setPassword(obj.getContraseña());
					userEnty.setMensaje("usuario y password correcto");
				}else{
					userEnty.setUsername(null);
					userEnty.setPassword(null);
					userEnty.setMensaje("usuario y password incorrecto");
				}
			}else{
				userEnty.setUsername(null);
				userEnty.setPassword(null);
				userEnty.setMensaje("usuario y password no encontrado o incorrecto");
			}
		} catch (Exception e) {
			userEnty.setMensaje(e.getMessage());
		}
		return userEnty;
	}
	@Override
	public SeguridadEntity UserByUsername(String username) {
		SeguridadEntity userEnty = new SeguridadEntity();
		try
		{
			String sql = "EXEC SVC_VALIDA_USUARIO '"+ username +"'";
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
			UserLoginDto obj = new UserLoginDto();
			if(rows.size()!=0) {
				for (Map<String, Object> row : rows) {
					obj.setIDUsuario((Integer) row.get("IDUsuario"));
					//obj.setNombres((String)row.get("nombres")) ;
					//obj.setApellidoPaterno((String)row.get("ApellidoPaterno")) ;
					//obj.setApellidoMaterno((String)row.get("ApellidoMaterno")) ;
					//obj.setNumeroDocumento((String)row.get("NumeroDocumento")) ;
					obj.setUsuario((String) row.get("TX_Usuario"));
					obj.setCorreoElectronico((String) row.get("TX_CorreoElectronico"));
					obj.setContraseña((String) row.get("TX_Contraseña"));
				}

				if (!obj.getContraseña().equals("")) {
					userEnty.setUsername(obj.getUsuario());
					userEnty.setPassword(obj.getContraseña());
					userEnty.setMensaje("usuario y password correcto");
				} else {
					userEnty.setUsername(null);
					userEnty.setPassword(null);
					userEnty.setMensaje("usuario y password incorrecto");
				}
			}else{
					userEnty.setUsername(null);
					userEnty.setPassword(null);
					userEnty.setMensaje("usuario y password no encontrado o incorrecto");
			}

		} catch (Exception e) {
			userEnty.setMensaje(e.getMessage());
		}
		return userEnty;
	}
	@Override
	public ResultEntity ValidaloginApp(String username) {
		ResultEntity result = new ResultEntity();
		try
		{
			String sql = "EXEC SVC_VALIDA_USUARIO '"+ username +"'";
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
			UserLoginDto obj = new UserLoginDto();
			if(rows.size()!=0) {
				for (Map<String, Object> row : rows) {
					obj.setIDUsuario((Integer) row.get("IDUsuario"));
					//obj.setNombres((String)row.get("nombres")) ;
					//obj.setApellidoPaterno((String)row.get("ApellidoPaterno")) ;
					//obj.setApellidoMaterno((String)row.get("ApellidoMaterno")) ;
					obj.setUsuario((String) row.get("TX_Usuario"));
					obj.setNumeroDocumento((String)row.get("NumeroDocumento")) ;
					obj.setUsuario((String) row.get("TX_CorreoElectronico"));
					obj.setContraseña((String) row.get("TX_Contraseña"));
				}

				if (!obj.getContraseña().equals("")) {
					result.setIsSuccess(true);
					result.setMessage("usuario y password correcto");
				} else {
					result.setIsSuccess(false);
					result.setMessage("usuario y password incorrecto");
				}
			}else{
				result.setIsSuccess(false);
				result.setMessage("usuario y password no encontrado o incorrecto");
			}

		} catch (Exception e) {
			result.setIsSuccess(false);
			result.setMessage("usuario y password no encontrado o incorrecto");
			result.setInnerException(e.getMessage());
		}
		return result;
	}
	


}
