package pe.gob.serfor.mcsniffs.repository.impl;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.microsoft.sqlserver.jdbc.Geometry;
// import jdk.nashorn.internal.ir.ReturnNode;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.UnidadAprovechamientoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;


import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class UnidadAprovechamientoRepositoryImpl extends JdbcDaoSupport implements UnidadAprovechamientoRepository {
    /**
     * @autor: JaquelineDB [12-06-2021]
     * @modificado:
     * @descripción: {Servicio de Unidad aprovechamiento, en esta clase consultan
     *               todo lo referente a esta tabla}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @PersistenceContext
    private EntityManager entityManager;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoRepositoryImpl.class);

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Lista todas las unidades de aprovechamiento}
     * @param:consulta
     */
    /*
     * @Override public ResultEntity<UnidadAprovechamientoEntity>
     * ListarUnidadAprovechamiento(ConsultaUAEntity consulta) {
     * ResultEntity<UnidadAprovechamientoEntity> result = new ResultEntity<>(); try
     * { List<PropertiesEntity> lista = new ArrayList<>(); Gson gson = new Gson();
     * String obj = gson.toJson(consulta); HttpResponse<String> response =
     * Unirest.post(
     * "https://apigeoforestal.azurewebsites.net/servicios/unidades/aprovechamiento")
     * .header("Content-Type", "application/json") .body(obj).asString();
     * ResponseApiEntity resulttotal =
     * gson.fromJson(response.getBody(),ResponseApiEntity.class); List<CapasEntity>
     * lstcapas = new ArrayList<>(); for (Object x:resulttotal.getData().getCapas())
     * { LinkedTreeMap<Object,Object> t = (LinkedTreeMap) x; CapasEntity capa = new
     * CapasEntity(); capa.setCodigoCapa(t.get("codigoCapa").toString());
     * capa.setNombreCapa(t.get("nombreCapa").toString()); String valor =
     * t.get("numeroGeometrias").toString(); capa.setNumeroGeometrias(valor == "" ?
     * 0 : (int) Double.parseDouble(valor)); String strgeo =
     * gson.toJson(t.get("geoJson")); GeoJasonEntity geojson=
     * gson.fromJson(strgeo,GeoJasonEntity.class); capa.setGeoJson(geojson);
     * lstcapas.add(capa); } List<FeaturesEntity> lst_features = new ArrayList<>();
     * 
     * List<UnidadAprovechamientoEntity> lstua = new ArrayList<>(); for (CapasEntity
     * e:lstcapas) { for (Object x:e.getGeoJson().getFeatures()) {
     * LinkedTreeMap<Object,Object> t = (LinkedTreeMap)x; FeaturesEntity fea = new
     * FeaturesEntity(); fea.setType(t.get("type").toString());
     * fea.setId(t.get("id") == null ? 0:Integer.parseInt(t.get("id").toString()));
     * String strpro = gson.toJson(t.get("properties")); PropertiesEntity
     * properties= gson.fromJson(strpro,PropertiesEntity.class);
     * fea.setProperties(properties); UnidadAprovechamientoEntity ua = new
     * UnidadAprovechamientoEntity(); ua.setIdUnidadAprovechamiento(null);
     * ua.setUa(properties); lst_features.add(fea); lstua.add(ua);
     * lista.add(properties); } }
     * 
     * result.setData(lstua); result.setIsSuccess(true);
     * result.setMessage("Se listo correctamente"); return result; } catch
     * (Exception e) { log.error("TipoProceso - listarTipoProceso", e.getMessage());
     * result.setIsSuccess(false); result.setMessage("Ocurrió un error.");
     * result.setMessageExeption(e.getMessage()); return result; } }
     */

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {inserta y actualiza la unidad aprovechamiento}
     * @param:obj
     */
    // @Override
    public ResultClassEntity guardarUnidadAprovechamiento3(UnidadAprovechamientoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            if (obj.getIdUnidadAprovechamiento().equals(null) || obj.getIdUnidadAprovechamiento().equals(0)) {

                StoredProcedureQuery processStored = entityManager
                        .createStoredProcedureQuery("dbo.pa_UnidadAprovechamiento_insertar");

                processStored.registerStoredProcedureParameter("ObjectId", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Fuente", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Docreg", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Fecreg", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Observ", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Zonautm", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Origen", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Lote", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Nomdep", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Contra", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Nomtit", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Nomrel", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Concur", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Medoto", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Fecoto", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Supsig", BigDecimal.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Supapr", BigDecimal.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Docleg", String.class, ParameterMode.IN);

                processStored.registerStoredProcedureParameter("Fecleg", Date.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Geojason", Geometry.class, ParameterMode.IN);

                processStored.registerStoredProcedureParameter("Fecleg", String.class, ParameterMode.IN);

                processStored.registerStoredProcedureParameter("Estua", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Feceua", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("Autfor", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("IdUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("FechaResgitro", Date.class, ParameterMode.IN);

                processStored.setParameter("ObjectId", obj.getUa().getOBJECTID());
                processStored.setParameter("Fuente", obj.getUa().getFUENTE());
                processStored.setParameter("Docreg", obj.getUa().getDOCREG());
                processStored.setParameter("Fecreg", obj.getUa().getFECREG());
                processStored.setParameter("Observ", obj.getUa().getOBSERV());
                processStored.setParameter("Zonautm", obj.getUa().getZONUTM());
                processStored.setParameter("Origen", obj.getUa().getORIGEN());
                processStored.setParameter("Lote", obj.getUa().getLOTE());
                processStored.setParameter("Nomdep", obj.getUa().getNOMDEP());
                processStored.setParameter("Contra", obj.getUa().getCONTRA());
                processStored.setParameter("Nomtit", obj.getUa().getNOMTIT());
                processStored.setParameter("Nomrel", obj.getUa().getNOMREL());
                processStored.setParameter("Concur", obj.getUa().getCONCUR());
                processStored.setParameter("Medoto", obj.getUa().getMODOTO());
                processStored.setParameter("Fecoto", obj.getUa().getFECOTO());
                processStored.setParameter("Supsig", obj.getUa().getSUPSIG());
                processStored.setParameter("Supapr", obj.getUa().getSUPAPR());
                processStored.setParameter("Docleg", obj.getUa().getDOCLEG());
                processStored.setParameter("Fecleg", obj.getUa().getFECLEG());
                processStored.setParameter("Geojason", obj.getGeojson());
                processStored.setParameter("Estua", obj.getUa().getESTUA());
                processStored.setParameter("Feceua", obj.getUa().getFECEUA());
                processStored.setParameter("Autfor", obj.getUa().getAUTFOR());
                processStored.setParameter("IdUsuarioRegistro", obj.getIdUsuarioRegistro());
                processStored.setParameter("FechaResgitro", obj.getFechaRegistro());
                processStored.execute();
                /*
                 * String sql = "EXEC pa_UnidadAprovechamiento_insertar " +
                 * obj.getUa().getOBJECTID() +",'"+obj.getUa().getFUENTE() +"','"
                 * +obj.getUa().getDOCREG()+"'," +obj.getUa().getFECREG()
                 * +",'"+obj.getUa().getOBSERV()+"','" +obj.getUa().getZONUTM()+"','"
                 * +obj.getUa().getORIGEN() +"','"+obj.getUa().getLOTE()+"','"
                 * +obj.getUa().getNOMDEP()+"','" +obj.getUa().getCONTRA()
                 * +"','"+obj.getUa().getNOMTIT()+"','" +obj.getUa().getNOMREL()+"',"
                 * +obj.getUa().getCONCUR() +",'"+obj.getUa().getMODOTO()+"','"
                 * +obj.getUa().getFECOTO()+"','" +obj.getUa().getSUPSIG()
                 * +"','"+obj.getUa().getSUPAPR()+"','" +obj.getUa().getDOCLEG()+"',"
                 * +obj.getUa().getFECLEG() +",'"+obj.getUa().getESTUA()+"','"
                 * +obj.getUa().getFECEUA()+"','" +obj.getUa().getAUTFOR()
                 * +"',"+obj.getIdUsuarioRegistro()+",'" + formatter.format(date)+"'" ;
                 */
                Object holis = processStored.getSingleResult();
                // Integer objre= getJdbcTemplate().queryForObject(sql,Integer.class);
                Integer objre = Integer.parseInt(holis.toString());
                result.setCodigo(objre);
                result.setSuccess(true);
                result.setMessage("La UA Se registró correctamente.");
            } else {
                // implementar el update
                result.setSuccess(true);
                result.setMessage("La UA se actualizó correctamente.");
            }
            return result;
        } catch (Exception e) {
            log.error("TipoProceso - listarTipoProceso", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity guardarUnidadAprovechamiento(UnidadAprovechamientoEntity obj) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_UnidadAprovechamiento_insertar");
            sp.registerStoredProcedureParameter("objectId", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fuente", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("docreg", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fecreg", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("observ", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("zonautm", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("origen", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("lote", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nomdep", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("contra", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nomtit", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nomrel", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("concur", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("medoto", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fecoto", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("supsig", BigDecimal.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("supapr", BigDecimal.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("docleg", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fecleg", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estua", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("feceua", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("autfor", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaResgitro", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("geometry_wkt", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("srid", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("grupo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUniAprovechamiento", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("objectId", obj.getUa().getOBJECTID());
            sp.setParameter("fuente", obj.getUa().getFUENTE());
            sp.setParameter("docreg", obj.getUa().getDOCREG());
            sp.setParameter("fecreg", obj.getUa().getFECREG());
            sp.setParameter("observ", obj.getUa().getOBSERV());
            sp.setParameter("zonautm", obj.getUa().getZONUTM());
            sp.setParameter("origen", obj.getUa().getORIGEN());
            sp.setParameter("lote", obj.getUa().getLOTE());
            sp.setParameter("nomdep", obj.getUa().getNOMDEP());
            sp.setParameter("contra", obj.getUa().getCONTRA());
            sp.setParameter("nomtit", obj.getUa().getNOMTIT());
            sp.setParameter("nomrel", obj.getUa().getNOMREL());
            sp.setParameter("concur", obj.getUa().getCONCUR());
            sp.setParameter("medoto", obj.getUa().getMODOTO());
            sp.setParameter("fecoto", obj.getUa().getFECOTO());
            sp.setParameter("supsig", obj.getUa().getSUPSIG());
            sp.setParameter("supapr", obj.getUa().getSUPAPR());
            sp.setParameter("docleg", obj.getUa().getDOCLEG());
            sp.setParameter("fecleg", obj.getUa().getFECLEG());
            sp.setParameter("estua", obj.getUa().getESTUA());
            sp.setParameter("feceua", obj.getUa().getFECEUA());
            sp.setParameter("autfor", obj.getUa().getAUTFOR());
            sp.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            sp.setParameter("fechaResgitro", new java.sql.Timestamp(date.getTime()));
            sp.setParameter("geometry_wkt", obj.getUa().getGeometry_wkt());
            sp.setParameter("srid", obj.getUa().getSrid());
            sp.setParameter("grupo", obj.getUa().getGrupo());
            sp.execute();
            Integer idUniAprovechamiento = (Integer) sp.getOutputParameterValue("idUniAprovechamiento");
            result.setCodigo(idUniAprovechamiento);
            result.setSuccess(true);
            result.setMessage("Se guarda la unidad de aprovechamiento.");
            return result;
        } catch (Exception e) {
            log.error("ProcesoOfertaRepositoryImpl - guardarProcesoOferta", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity<UnidadAprovechamientoEntity> obtenerUnidadAprovechamiento(Integer ObjectId) {
       return null;
        /*
        ResultClassEntity<UnidadAprovechamientoEntity> result = new ResultClassEntity<UnidadAprovechamientoEntity>();
        try {
            Connection con = null;
            PreparedStatement pstm = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_UnidadAprovechamiento_ObtenerObjectId(?)}");
            pstm.setObject(1, ObjectId, Types.INTEGER);
            ResultSet rs = pstm.executeQuery();
            UnidadAprovechamientoEntity c = null;
            while (rs.next()) {
                c = new UnidadAprovechamientoEntity();
                c.setIdUnidadAprovechamiento(rs.getInt("NU_ID_UNIDAD_APROVECHAMIENTO"));
                PropertiesEntity uapropiedades = new PropertiesEntity();
                uapropiedades.setOBJECTID(rs.getInt("NU_OBJECTID"));
                uapropiedades.setZONUTM(rs.getString("TX_ZONUTM"));
                uapropiedades.setORIGEN(rs.getString("TX_ORIGEN"));
                uapropiedades.setLOTE(rs.getString("TX_LOTE"));
                uapropiedades.setNOMDEP(rs.getString("TX_NOMDEP"));
                uapropiedades.setNOMTIT(rs.getString("TX_NOMTIT"));
                uapropiedades.setNOMREL(rs.getString("TX_NOMREL"));
                c.setUa(uapropiedades);
            }
            result.setData(c);
            result.setSuccess(true);
            result.setMessage("Se listaron las UA.");
            pstm.close();
            con.close();
            return result;
        } catch (Exception e) {
            log.error("UnidadAprovechamiento - obtenerUnidadAprovechamiento", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
        */
    }

    /**
     * @autor: Abner Valdez [07-07-2021]
     * @modificado:
     * @descripción: {Lista unidad de aprovechamiento}
     * 
     */
    @Override
    public ResultEntity<PropertiesEntity> listar() {
        ResultEntity<PropertiesEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_UnidadAprovechamiento_Listar");
            sp.execute();
            List<PropertiesEntity> items = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    PropertiesEntity uapropiedades = new PropertiesEntity();
                    uapropiedades.setOBJECTID((Integer) row[0]);
                    uapropiedades.setZONUTM((String) row[1]);
                    uapropiedades.setORIGEN((String) row[2]);
                    uapropiedades.setLOTE((String) row[3]);
                    uapropiedades.setNOMDEP((String) row[4]);
                    uapropiedades.setNOMTIT((String) row[5]);
                    uapropiedades.setNOMREL((String) row[6]);
                    items.add(uapropiedades);
                }
            }
            result.setData(items);
            result.setIsSuccess(true);
            result.setMessage("Se listó correctamente");
            return result;
        } catch (Exception e) {
            log.error("TipoProceso - listarTipoProceso", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Abner Valdez [07-07-2021]
     * @modificado:
     * @descripción: {Lista unidad de aprovechamiento}
     * 
     */
    @Override
    public ResultEntity<PropertiesEntity> buscarByIdProcesoOferta(Integer idProcesoOferta) {
        ResultEntity<PropertiesEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_UnidadAprovechamiento_BuscarByIdProOfert");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            sp.setParameter("idProcesoOferta", idProcesoOferta);
            List<PropertiesEntity> items = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    PropertiesEntity uapropiedades = new PropertiesEntity();
                    uapropiedades.setOBJECTID((Integer) row[0]);
                    uapropiedades.setZONUTM((String) row[1]);
                    uapropiedades.setORIGEN((String) row[2]);
                    uapropiedades.setLOTE((String) row[3]);
                    uapropiedades.setNOMDEP((String) row[4]);
                    uapropiedades.setNOMTIT((String) row[5]);
                    uapropiedades.setNOMREL((String) row[7]);
                    uapropiedades.setGeometry_wkt((String) row[6]);
                    items.add(uapropiedades);
                }
            }
            result.setData(items);
            result.setIsSuccess(true);
            result.setMessage("Se listó correctamente");
            return result;
        } catch (Exception e) {
            log.error("UnidadAprovechamiento - BuscarUnidadAprovechamiento", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
    /**
     * @autor: Abner Valdez [07-07-2021]
     * @modificado:
     * @descripción: {Lista unidad de aprovechamiento}
     * 
     */
    @Override
    public ResultEntity<PropertiesEntity> buscarByIdPlanManejo(Integer idPlanManejo) {
        return null;
        /*
        ResultEntity<PropertiesEntity> result = new ResultEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_UnidadAprovechamiento_BuscarByIdPlanManejo(?)}");
            pstm.setObject(1, idPlanManejo, Types.INTEGER);
            ResultSet rs = pstm.executeQuery();
            List<PropertiesEntity> items = new ArrayList<>();
            while (rs.next()) {
                PropertiesEntity uapropiedades = new PropertiesEntity();
                uapropiedades.setOBJECTID(rs.getInt("NU_OBJECTID"));
                uapropiedades.setZONUTM(rs.getString("TX_ZONUTM"));
                uapropiedades.setORIGEN(rs.getString("TX_ORIGEN"));
                uapropiedades.setLOTE(rs.getString("TX_LOTE"));
                uapropiedades.setNOMDEP(rs.getString("TX_NOMDEP"));
                uapropiedades.setNOMTIT(rs.getString("TX_NOMTIT"));
                uapropiedades.setNOMREL(rs.getString("TX_NOMREL"));
                uapropiedades.setGeometry_wkt(rs.getString("TX_GEOMETRY"));
                items.add(uapropiedades);
            }
            result.setData(items);
            result.setIsSuccess(true);
            result.setMessage("Se listó correctamente");
            return result;
        } catch (Exception e) {
            log.error("UnidadAprovechamiento - BuscarUnidadAprovechamiento", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
        */
    }

}
