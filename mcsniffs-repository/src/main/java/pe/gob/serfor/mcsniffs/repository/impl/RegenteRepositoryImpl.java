package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.RegenteEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.PlanManejoRepository;
import pe.gob.serfor.mcsniffs.repository.RegenteRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

/**
 * @autor: Harry Coa [31-07-2021]
 * @modificado:
 * @descripción: {Repositorio Regente}
 *
 */

@Repository
public class RegenteRepositoryImpl  extends JdbcDaoSupport implements RegenteRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.RegenteRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Autowired
    private PlanManejoRepository planManejoRepository;

    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @Override
    public ResultEntity<RegenteEntity> ListarRegente(RegenteEntity param) throws Exception {
        ResultEntity<RegenteEntity> lstResult = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoRegente_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idPlanManejoRegente", param.getIdPlanManejoRegente());
            processStored.setParameter("codigoProceso", param.getCodigoProceso());
            processStored.setParameter("numeroDocumento", param.getNumeroDocumento());
            processStored.execute();
            List<RegenteEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    RegenteEntity temp = new RegenteEntity();
                    temp.setIdPlanManejoRegente((Integer) row[0]);
                    temp.setCodigoProceso((String) row[1]);
                    temp.setCodigoTipoRegente((String) row[2]);
                    temp.setNombres((String) row[3]);
                    temp.setApellidos((String) row[4]);
                    temp.setNumeroDocumento((String) row[5]);
                    temp.setDomicilioLegal((String) row[6]);
                    temp.setEstadoRegente((String) row[7]);
                    temp.setNumeroLicencia((String) row[8]);
                    temp.setPeriodo((String) row[9]);
                    temp.setContratoSuscrito((String) row[10]);
                    temp.setCertificadoHabilitacion((String) row[11]);
                    temp.setNumeroInscripcion((String) row[12]);
                    temp.setAdjuntoContrato(String.valueOf((Character) row[13]));
                    temp.setAdjuntoCertificado(String.valueOf((Character) row[14]));
                    temp.setIdPlanManejo((Integer) row[15]);
                    result.add(temp);
                }
            }

            lstResult.setData(result);
            lstResult.setIsSuccess(true);
            lstResult.setMessage("Se obtuvo los regentes de Manejo correctamente.");
            return  lstResult;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            lstResult.setIsSuccess(false);
            lstResult.setMessage("Ocurrió un error. "+e.getMessage());
            return  lstResult;
        }
    }

    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @Override
    public ResultClassEntity RegistrarRegente(RegenteEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_PlanManejoRegente_Registrar");
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTipoRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombres", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidos", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("domicilioLegal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroLicencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("contratoSuscrito", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("certificadoHabilitacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroInscripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjuntoContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjuntoCertificado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idplanManejoRegente", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoProceso", param.getCodigoProceso());
            processStored.setParameter("codigoTipoRegente", param.getCodigoTipoRegente());
            processStored.setParameter("nombres", param.getNombres());
            processStored.setParameter("apellidos", param.getApellidos());
            processStored.setParameter("numeroDocumento", param.getNumeroDocumento());
            processStored.setParameter("domicilioLegal", param.getDomicilioLegal());
            processStored.setParameter("estadoRegente", param.getEstadoRegente());
            processStored.setParameter("numeroLicencia", param.getNumeroLicencia());
            processStored.setParameter("periodo", param.getPeriodo());
            processStored.setParameter("contratoSuscrito", param.getContratoSuscrito());
            processStored.setParameter("certificadoHabilitacion", param.getCertificadoHabilitacion());
            processStored.setParameter("numeroInscripcion", param.getNumeroInscripcion());
            processStored.setParameter("adjuntoContrato", param.getAdjuntoContrato());
            processStored.setParameter("adjuntoCertificado", param.getAdjuntoCertificado());
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idplanManejoRegente", param.getIdPlanManejoRegente());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute(); 
            Integer id = (Integer) processStored.getOutputParameterValue("idplanManejoRegente");
            result.setCodigo(id);
            result.setMessage((id>0?"registró al regente":"No se realizó el registro"));
            result.setSuccess((id>0?true:false));
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.   - "+e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @Override
    public ResultEntity<RegenteEntity> ListarRegenteArchivos(RegenteEntity param) throws Exception {
        ResultEntity<RegenteEntity> lstResult = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoRegente_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idPlanManejoRegente", param.getIdPlanManejoRegente());
            processStored.setParameter("codigoProceso", param.getCodigoProceso());
            processStored.setParameter("numeroDocumento", param.getNumeroDocumento());
            processStored.execute();
            List<RegenteEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoArchivoDto> lista=new ArrayList<PlanManejoArchivoDto>();
            String codigo="";
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    RegenteEntity temp = new RegenteEntity();
                    temp.setIdPlanManejoRegente((Integer) row[0]);
                    temp.setCodigoProceso((String) row[1]);
                    temp.setCodigoTipoRegente((String) row[2]);
                    temp.setNombres((String) row[3]);
                    temp.setApellidos((String) row[4]);
                    temp.setNumeroDocumento((String) row[5]);
                    temp.setDomicilioLegal((String) row[6]);
                    temp.setEstadoRegente((String) row[7]);
                    temp.setNumeroLicencia((String) row[8]);
                    temp.setPeriodo((String) row[9]);
                    temp.setContratoSuscrito((String) row[10]);
                    temp.setCertificadoHabilitacion((String) row[11]);
                    temp.setNumeroInscripcion((String) row[12]);
                    temp.setAdjuntoContrato(String.valueOf((Character) row[13]));
                    temp.setAdjuntoCertificado(String.valueOf((Character) row[14]));
                    temp.setIdPlanManejo((Integer) row[15]);
                    if(param.getCodigoProceso().equals("PMFIC")){
                        codigo="PMFICINFGCHP";
                    }else if(param.getCodigoProceso().equals("PMFI")) {
                        codigo="PMFICONREGEN";
                    }
                    lista= planManejoRepository.listarPlanManejoListar(param.getIdPlanManejo(), null, codigo,null,null);

                    temp.setArchivos(lista);
                    result.add(temp);
                }
            }

            lstResult.setData(result);
            lstResult.setIsSuccess(true);
            lstResult.setMessage("Se obtuvo los regentes de Manejo correctamente.");
            return  lstResult;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            lstResult.setIsSuccess(false);
            lstResult.setMessage("Ocurrió un error. "+e.getMessage());
            return  lstResult;
        }
    }
}
