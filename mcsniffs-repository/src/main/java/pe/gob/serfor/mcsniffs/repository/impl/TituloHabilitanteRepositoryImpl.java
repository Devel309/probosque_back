package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.TituloHabilitanteRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class TituloHabilitanteRepositoryImpl extends JdbcDaoSupport implements TituloHabilitanteRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(TituloHabilitanteRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity registrarTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<TituloHabilitanteEntity> list_ = new ArrayList<>();
        try {
            for (TituloHabilitanteEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_TituloHabilitante_Registrar");
                sp.registerStoredProcedureParameter("codigoUnico", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoResumido", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("procedencia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("estadoTH", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("modalidad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("codigoUnico", p.getCodigoUnico());
                sp.setParameter("codigoResumido", p.getCodigoResumido());
                sp.setParameter("procedencia", p.getProcedencia());
                sp.setParameter("estadoTH", p.getEstadoTH());
                sp.setParameter("modalidad", p.getModalidad());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        TituloHabilitanteEntity obj_ = new TituloHabilitanteEntity();
                        obj_.setIdTituloHabilitante(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodigoUnico(row_[1]==null?null:(String) row_[1]);
                        obj_.setCodigoResumido(row_[2]==null?null:(String) row_[2]);
                        obj_.setProcedencia(row_[3]==null?null:(String) row_[3]);
                        obj_.setEstadoTH(row_[4]==null?null:(String) row_[4]);
                        obj_.setModalidad(row_[5]==null?null:(String) row_[5]);

                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el titulo habilitante.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<TituloHabilitanteEntity> list_ = new ArrayList<>();
        try {
            for (TituloHabilitanteEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_TituloHabilitante_Actualizar");
                sp.registerStoredProcedureParameter("idTituloHabilitante", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoUnico", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoResumido", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("procedencia", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("estadoTH", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("modalidad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idTituloHabilitante", p.getIdTituloHabilitante());
                sp.setParameter("codigoUnico", p.getCodigoUnico());
                sp.setParameter("codigoResumido", p.getCodigoResumido());
                sp.setParameter("procedencia", p.getProcedencia());
                sp.setParameter("estadoTH", p.getEstadoTH());
                sp.setParameter("modalidad", p.getModalidad());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        TituloHabilitanteEntity obj_ = new TituloHabilitanteEntity();
                        obj_.setIdTituloHabilitante(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodigoUnico(row_[1]==null?null:(String) row_[1]);
                        obj_.setCodigoResumido(row_[2]==null?null:(String) row_[2]);
                        obj_.setProcedencia(row_[3]==null?null:(String) row_[3]);
                        obj_.setEstadoTH(row_[4]==null?null:(String) row_[4]);
                        obj_.setModalidad(row_[5]==null?null:(String) row_[5]);

                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el titulo habilitante.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public ResultClassEntity actualizarCodigoCifradoTituloHabilitante(List<TituloHabilitanteEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<TituloHabilitanteEntity> list_ = new ArrayList<>();
        String codigoCifrado="";
        try {
            for (TituloHabilitanteEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PermisoForestal_ActualizarCodigoCifrado");
                sp.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoCifrado", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idPermisoForestal", p.getIdPermisoForestal());
                codigoCifrado=PermisoForestalRepositoryImpl.cifrar(p.getCodigoUnico(),PermisoForestalRepositoryImpl.SGD_SECRET_KEY_PROPERTIES);
                sp.setParameter("codigoCifrado", codigoCifrado);
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        TituloHabilitanteEntity obj_ = new TituloHabilitanteEntity();
                        obj_.setIdTituloHabilitante(row_[0]==null?null:(Integer) row_[0]);
                        obj_.setCodigoUnico(row_[1]==null?null:(String) row_[1]);
                        obj_.setCodigoResumido(row_[2]==null?null:(String) row_[2]);
                        obj_.setProcedencia(row_[3]==null?null:(String) row_[3]);
                        obj_.setEstadoTH(row_[4]==null?null:(String) row_[4]);
                        obj_.setModalidad(row_[5]==null?null:(String) row_[5]);

                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el titulo habilitante.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }



    @Override
    public List<TituloHabilitanteEntity> listarTituloHabilitante(TituloHabilitanteEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TituloHabilitante_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idTituloHabilitante", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoUnico", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idTituloHabilitante", param.getIdTituloHabilitante());
            processStored.setParameter("codigoUnico", param.getCodigoUnico());
            processStored.setParameter("idPermisoForestal", param.getIdPermisoForestal());

            processStored.execute();
            List<TituloHabilitanteEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                TituloHabilitanteEntity temp = null;
                for (Object[] row_: spResult){
                    temp = new TituloHabilitanteEntity();
                    temp.setIdTituloHabilitante(row_[0]==null?null:(Integer) row_[0]);
                    temp.setCodigoUnico(row_[1]==null?null:(String) row_[1]);
                    temp.setCodigoResumido(row_[2]==null?null:(String) row_[2]);
                    temp.setProcedencia(row_[3]==null?null:(String) row_[3]);
                    temp.setEstadoTH(row_[4]==null?null:(String) row_[4]);
                    temp.setModalidad(row_[5]==null?null:(String) row_[5]);

                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }



    @Override
    public ResultClassEntity eliminarTituloHabilitante(TituloHabilitanteEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_TituloHabilitante_Eliminar");
        processStored.registerStoredProcedureParameter("idTituloHabilitante", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idTituloHabilitante", param.getIdTituloHabilitante());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity listarInformacionTH(InformacionTHDto param) throws Exception {

        try {
            ResultClassEntity result = new ResultClassEntity();

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Sincronizacion.pa_informacionTH_Listar");
            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioTitularTH", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idContrato", param.getIdContrato());
            processStored.setParameter("idUsuarioTitularTH", param.getIdUsuarioTitularTH());


            processStored.execute();
            List<InformacionTHDto> lsInfo = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                InformacionTHDto temp = null;
                for (Object[] row_: spResult){
                    temp = new InformacionTHDto();
                    temp.setIdContrato(row_[0]==null?null:(Integer) row_[0]);
                    temp.setTituloHabilitante(row_[1]==null?null:(String) row_[1]);
                    temp.setIdUsuarioTitularTH(row_[2]==null?null:(Integer) row_[2]);
                    temp.setNombreTitular(row_[3]==null?null:(String) row_[3]);
                    temp.setDireccion(row_[4]==null?null:(String) row_[4]);
                    temp.setUbigeo(row_[5]==null?null:(String) row_[5]);
                    temp.setNroPlan(row_[6]==null?null:(Integer) row_[6]);
                    temp.setNroResolucion(row_[7]==null?null:(String) row_[7]);
                    temp.setParcelas(row_[8]==null?null:(String) row_[8]);
                    temp.setFechaIniPlan(row_[9]==null?null:(Date) row_[9]);
                    temp.setFechaFinPlan(row_[10]==null?null:(Date) row_[10]);
                    temp.setDepartamento(row_[11]==null?null:(String) row_[11]);
                    temp.setProvincia(row_[12]==null?null:(String) row_[12]);
                    temp.setDistrito(row_[13]==null?null:(String) row_[13]);
                    temp.setRuc(row_[14]==null?null:(String) row_[14]);
                    temp.setTipoPlan(row_[15]==null?null:(String) row_[15]);
                    temp.setNroRegistroLO(row_[16]==null?null:(String) row_[16]);
                    temp.setCodigoRegente(row_[17]==null?null:(String) row_[17]);
                    temp.setNombreRegente(row_[18]==null?null:(String) row_[18]);
                    temp.setNroRegistroLO(row_[19]==null?null:(String) row_[19]);
                    lsInfo.add(temp);
                }
            }
            result.setData(lsInfo);
            result.setSuccess(true);
            result.setMessage("Se lista la información TH correctamente.");
            return result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }
}
