package pe.gob.serfor.mcsniffs.repository;

import java.util.List;


import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.CensoForestalListarDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ContratoTHDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface CensoForestalListarRepository {

   List<CensoForestalListarDto> censoForestalListar(CensoForestalListarDto dto) throws Exception;
  List<ContratoTHDto> listaTHContratoPorFiltro(ContratoTHDto data) throws Exception;
}
