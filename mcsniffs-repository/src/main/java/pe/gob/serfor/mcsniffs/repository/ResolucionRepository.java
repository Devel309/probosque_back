package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ResolucionRepository {

    ResultClassEntity ListarResolucion(ResolucionDto param);
    ResultClassEntity ObtenerResolucion(ResolucionDto param);
    ResultClassEntity registrarResolucion(ResolucionDto param);
    ResultClassEntity ActualizarResolucion(ResolucionDto param);
    ResultClassEntity ActualizarResolucionEstado(ResolucionDto param);
    String resolucionValidar(Integer idProcesoOferta);
}
