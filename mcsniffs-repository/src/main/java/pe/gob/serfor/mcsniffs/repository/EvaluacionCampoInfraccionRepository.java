package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoInfraccionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface EvaluacionCampoInfraccionRepository {

    ResultClassEntity ObtenerEvaluacionCampoInfraccion(EvaluacionCampoInfraccionDto param);

    ResultClassEntity RegistrarEvaluacionCampoInfraccion(List<EvaluacionCampoInfraccionDto> list);

}
