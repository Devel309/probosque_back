package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;

import java.util.List;

public interface NotificacionRepository {
 

    ResultClassEntity registrarNotificacion(NotificacionEntity obj);
    List<NotificacionEntity> listarNotificacion(NotificacionEntity param) throws Exception;
    ResultClassEntity EliminarNotificacion(NotificacionEntity param) throws Exception;


}
