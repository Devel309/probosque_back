package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.EnviarConsultaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface EnviarConsultaRepository {
    ResultClassEntity EnviarConsulta(EnviarConsultaEntity obj);
}
