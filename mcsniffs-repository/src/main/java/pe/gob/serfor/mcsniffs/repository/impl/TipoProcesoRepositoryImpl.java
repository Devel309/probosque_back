package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoEntity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoNivel1Entity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoNivel2Entity;
import pe.gob.serfor.mcsniffs.repository.TipoProcesoRepository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Repository
public class TipoProcesoRepositoryImpl  extends JdbcDaoSupport implements TipoProcesoRepository {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Servicio de tipo de proceso, en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoRepositoryImpl.class);

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Lista los tipos de procesos con sus sub procesos}
     * @param:
     */
    @Override
    public ResultEntity<TipoProcesoNivel1Entity> listarTipoProceso() {
        ResultEntity<TipoProcesoNivel1Entity> result = new ResultEntity<>();
        try {
            String sql = "EXEC pa_TipoProceso_Listar";
            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
            List<TipoProcesoEntity> lsttipopro = new ArrayList<>();
            List<TipoProcesoNivel1Entity> lstnivel1 = new ArrayList<>();
            if (rows.size()>=1) {
                for(Map<String, Object> row:rows){
                    TipoProcesoEntity obj = new TipoProcesoEntity();
                    obj.setIdTipoProceso((Integer)row.get("NU_ID_TIPO_PROCESO"));
                    obj.setDescripcion((String)row.get("TX_DESCRIPCION"));
                    obj.setNivel((Integer)row.get("NU_NIVEL"));
                    obj.setCodigo((String)row.get("TX_CODIGO"));
                    obj.setCodigoPadre((String)row.get("TX_CODIGOPADRE"));
                    lsttipopro.add(obj);
                }
            }
            lsttipopro.stream().sorted(Comparator.comparing(TipoProcesoEntity::getIdTipoProceso)).forEach((a) -> {
                if (a.getNivel().equals(1)) {
                    TipoProcesoNivel1Entity obj =  new TipoProcesoNivel1Entity();
                    obj.setIdTipoProceso(a.getIdTipoProceso());
                    obj.setDescripcion(a.getDescripcion());
                    obj.setCodigo(a.getCodigo());
                    lstnivel1.add(obj);
                }
            });
            for (TipoProcesoNivel1Entity n1:lstnivel1) {
                List<TipoProcesoNivel2Entity> lstnivel2 = new ArrayList<>();
                if(n1.getCodigo()!=null){
                    lsttipopro.stream().sorted(Comparator.comparing(TipoProcesoEntity::getIdTipoProceso)).forEach((a) -> {
                        if (a.getNivel().equals(2) && n1.getCodigo().equals(a.getCodigo())) {
                            TipoProcesoNivel2Entity obj =  new TipoProcesoNivel2Entity();
                            obj.setIdTipoProceso(a.getIdTipoProceso());
                            obj.setDescripcion(a.getDescripcion());
                            obj.setCodigo(a.getCodigo());
                            obj.setCodigoPadre(a.getCodigoPadre());
                            lstnivel2.add(obj);
                        }
                    });
                    for (TipoProcesoNivel2Entity n2:lstnivel2) {
                        List<TipoProcesoEntity> lstnivel3 = new ArrayList<>();
                        if(n2.getCodigoPadre() != null){
                            lsttipopro.stream().sorted(Comparator.comparing(TipoProcesoEntity::getIdTipoProceso)).forEach((a) -> {
                                if (a.getNivel().equals(3) && n2.getCodigoPadre().equals(a.getCodigoPadre())) {
                                    TipoProcesoEntity obj =  new TipoProcesoEntity();
                                    obj.setIdTipoProceso(a.getIdTipoProceso());
                                    obj.setDescripcion(a.getDescripcion());
                                    obj.setCodigo(a.getCodigo());
                                    lstnivel3.add(obj);
                                }
                            });
                            n2.setNivel3(lstnivel3);
                        }
                    }
                    n1.setNivel2(lstnivel2);
                }
            }
            result.setData(lstnivel1);
            result.setIsSuccess(true);
            result.setMessage("Se listo correctamente");
            return result;
        } catch (Exception e) {
            log.error("TipoProceso - listarTipoProceso", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
}
