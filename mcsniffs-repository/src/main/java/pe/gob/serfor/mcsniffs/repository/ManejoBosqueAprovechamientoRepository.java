package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ManejoBosqueAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ManejoBosqueAprovechamientoRepository {
    ResultClassEntity RegistrarManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception;
    ResultClassEntity ObtenerManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception;
}
