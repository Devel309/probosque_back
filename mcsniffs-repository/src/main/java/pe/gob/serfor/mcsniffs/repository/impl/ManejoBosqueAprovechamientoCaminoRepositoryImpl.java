package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueAprovechamientoCaminoRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManejoBosqueAprovechamientoCaminoRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueAprovechamientoCaminoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueAprovechamientoCaminoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosqueAprovechamientoCamino(List<ManejoBosqueAprovechamientoCaminoEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueAprovechamientoCaminoEntity> list = new ArrayList<ManejoBosqueAprovechamientoCaminoEntity>();
            for(ManejoBosqueAprovechamientoCaminoEntity p:param){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueAprovechamientoCamino_Registrar");
                processStored.registerStoredProcedureParameter("idManBosqueAprove", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idManBosqueAproveCamino", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoInfraestructura", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoInfraestructura", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("caracteristica", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("construccion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("manoObra", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("maquina", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idManBosqueAprove", p.getManejoBosqueAprovechamiento().getIdManBosqueAprove());
                processStored.setParameter("idManBosqueAproveCamino", p.getIdManBosqueAproveCamino());
                processStored.setParameter("idTipoInfraestructura", p.getIdTipoInfraestructura());
                processStored.setParameter("tipoInfraestructura", p.getTipoInfraestructura());
                processStored.setParameter("caracteristica", p.getCaracteristica());
                processStored.setParameter("construccion", p.getConstruccion());
                processStored.setParameter("manoObra", p.getManoObra());
                processStored.setParameter("maquina", p.getMaquina());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult_ =processStored.getResultList();

                if (spResult_.size() >= 1) {
                    for (Object[] row : spResult_) {
                        ManejoBosqueAprovechamientoCaminoEntity obj = new ManejoBosqueAprovechamientoCaminoEntity() ;
                        ManejoBosqueAprovechamientoEntity ma = new ManejoBosqueAprovechamientoEntity() ;
                        ma.setIdManBosqueAprove((Integer) row[0]);
                        obj.setManejoBosqueAprovechamiento(ma);
                        obj.setIdManBosqueAproveCamino((Integer) row[1]);
                        obj.setTipoInfraestructura((String) row[2]);
                        obj.setIdTipoInfraestructura((String) row[3]);
                        obj.setCaracteristica((String) row[4]);
                        obj.setConstruccion((String) row[5]);
                        obj.setManoObra((String) row[6]);
                        obj.setMaquina((String) row[7]);
                        list.add(obj);
                    }
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró especificación de camino.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoCaminoEntity manejoBosqueAprovechamientoCamino) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueAprovechamientoCamino_Eliminar");
            processStored.registerStoredProcedureParameter("idManBosqueAproveCamino", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManBosqueAproveCamino", manejoBosqueAprovechamientoCamino.getIdManBosqueAproveCamino());
            processStored.setParameter("idUsuarioElimina", manejoBosqueAprovechamientoCamino.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarManejoBosqueAprovechamientoCamino(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueAprovechamientoCaminoEntity> list = new ArrayList<ManejoBosqueAprovechamientoCaminoEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueAprovechamientoCamino_Listar");
            processStored.registerStoredProcedureParameter("idManBosqueAprove", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManBosqueAprove", manejoBosqueAprovechamiento.getIdManBosqueAprove());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();

            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    ManejoBosqueAprovechamientoCaminoEntity obj = new ManejoBosqueAprovechamientoCaminoEntity() ;
                    ManejoBosqueAprovechamientoEntity ma = new ManejoBosqueAprovechamientoEntity() ;
                    ma.setIdManBosqueAprove((Integer) row[0]);
                    obj.setManejoBosqueAprovechamiento(ma);
                    obj.setIdManBosqueAproveCamino((Integer) row[1]);
                    obj.setTipoInfraestructura((String) row[2]);
                    obj.setIdTipoInfraestructura((String) row[3]);
                    obj.setCaracteristica((String) row[4]);
                    obj.setConstruccion((String) row[5]);
                    obj.setManoObra((String) row[6]);
                    obj.setMaquina((String) row[7]);
                    list.add(obj);
                }

            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró obtuvo el manejo de aprovechamiento camino.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
