package pe.gob.serfor.mcsniffs.repository;

import java.sql.SQLException;
import java.util.List;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudImpugnacionEntity;


public interface SolicitudImpugnacionRepository {
    
    ResultClassEntity<List<Solicitud2Entity>> listarSolicitudesVencidas(); 

    ResultClassEntity<Boolean> SolicitudImpugnacionActualizar(SolicitudImpugnacionEntity params); 

    ResultClassEntity<List<SolicitudImpugnacionEntity>> ListaSolicitudesVencidasObtener() throws SQLException;

    ResultClassEntity<Boolean> SolicitudImpugnacionRegistrarActualizar(SolicitudImpugnacionEntity params);

    ResultClassEntity SolicitudImpugnacionObtener(SolicitudImpugnacionEntity solicitudImpugnacionEntity) throws Exception;

    ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> listarArchivoSolicitudImpugnacion(SolicitudImpugnacionArchivo2Entity param); 

    ResultClassEntity<List<SolicitudImpugnacionEntity>> solicitudImpugnacionListar(SolicitudImpugnacionEntity param); 
}
