package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Anexo1PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.AreaSolicitadaUTMEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.AnexosPFDMRepository;
import pe.gob.serfor.mcsniffs.repository.PostulacionPFDMRepository;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.common.io.Files;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Repository
public class AnexosPFDMRepositoryImpl implements AnexosPFDMRepository{
    /**
     * @autor: JaquelineDB [24-08-2021]
     * @modificado:
     * @descripción: {Repository de los anexos pfdm}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AnexosPFDMRepositoryImpl.class);
    
    @Autowired
    private PostulacionPFDMRepository postulacion;


    /**
     * @autor: JaquelineDB [24-08-2021]
     * @modificado:
     * @descripción: {Generar el anexo 1}
     * @param:AnexosPFDMResquestEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo1PFDM(AnexosPFDMResquestEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            Anexo1PFDMEntity anexo3 = new Anexo1PFDMEntity();
            ResultClassEntity<Anexo1PFDMEntity> anexo = postulacion.obtenerAnexo1PFDM(filtro);
            anexo3 = anexo.getData();
            int salida = -1;
            InputStream inputStream = AnexosPFDMRepositoryImpl.class.getResourceAsStream("/PlantillaAnexo1PFDM.docx");

            XWPFDocument doc = new XWPFDocument(inputStream);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row2 = table.getRow(17);
            for (int i = 0; i < anexo3.getAreaCoordenadasUTM().size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);
                copiedRow.getTableCells().get(0).setText("SOME MODIFICATION HERE");
                table.addRow(copiedRow);
            }
            

            /*for (XWPFTable tbl : doc.getTables()) {
                for (XWPFTableRow row : tbl.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                if(r != null){
                                    String text = r.getText(0);
                                    if(text !=null){
                                        if (text.contains("nombrex")) {
                                            text = text.replace("nombrex",anexo3.getSoliNombreRS());
                                            r.setText(text, 0);
                                        }else if (text.contains("dnix")) {
                                            text = text.replace("dnix",anexo3.getSoliDni());
                                            r.setText(text, 0);
                                        }else if (text.contains("rucx")) {
                                            text = text.replace("rucx",anexo3.getSoliRuc());
                                            r.setText(text, 0);
                                        }else if (text.contains("partidax")) {
                                            text = text.replace("partidax",anexo3.getSoliNroPartidaRegistral());
                                            r.setText(text, 0);
                                        }else if (text.contains("direccionx")) {
                                            text = text.replace("direccionx",anexo3.getSoliDireccion());
                                            r.setText(text, 0);
                                        }else if (text.contains("nrox")) {
                                            text = text.replace("nrox",anexo3.getSoliNroDireccion());
                                            r.setText(text, 0);
                                        }else if (text.contains("caseriox")) {
                                            text = text.replace("caseriox",anexo3.getSoliSectorCaserio());
                                            r.setText(text, 0);
                                        }else if (text.contains("distritox")) {
                                            text = text.replace("distritox",anexo3.getSoliIdDistrito());
                                            r.setText(text, 0);
                                        }else if (text.contains("provinciax")) {
                                            text = text.replace("provinciax",formatter.format(date));
                                            r.setText(text, 0);
                                        }else if (text.contains("depaartamentox")) {
                                            text = text.replace("depaartamentox", anexo3.getSoliIdDepartamento());
                                            r.setText(text,0);
                                        }else if(text.contains("fijox")){
                                            text = text.replace("fijox", anexo3.getSoliTelefono());
                                            r.setText(text,0);
                                        }else if(text.contains("celularx")){
                                            text = text.replace("celularx", anexo3.getSoliCelular());
                                            r.setText(text,0);
                                        }else if(text.contains("correox")){
                                            text = text.replace("correox", anexo3.getSoliCorreoPersonal());
                                            r.setText(text,0);
                                        }else if(text.contains("verticex")){
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }*/

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            byte[] fileContent = b.toByteArray();
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo3.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 3.");
            Files.write(fileContent, new File("C:\\docs"));
            try (FileOutputStream fos = new FileOutputStream("C:\\docs")) {
                fos.write(fileContent);
                //fos.close(); There is no more need for this line since you had created the instance of "fos" inside the try. And this will automatically close the OutputStream
             }
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo3", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

}
