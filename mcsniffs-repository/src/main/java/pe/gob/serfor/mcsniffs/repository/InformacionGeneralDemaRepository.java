package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDetalle;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;


public interface InformacionGeneralDemaRepository {
    ResultClassEntity registrarInformacionGeneralDema(InformacionGeneralDEMAEntity obj);
    ResultClassEntity actualizarInformacionGeneralDema(InformacionGeneralDEMAEntity obj);
    ResultEntity<InformacionGeneralDEMAEntity>listarInformacionGeneralDema(InformacionGeneralDEMAEntity filtro);
    ResultClassEntity registrarInformacionGeneralDetalle(InformacionGeneralDetalle obj);
    ResultClassEntity actualizarInformacionGeneralDetalle(InformacionGeneralDetalle obj);
    ResultEntity<InformacionGeneralDetalle>listarInformacionGeneralDetalle(Integer IdInformacionGeneral);
    ResultClassEntity listarPorFiltroInformacionGeneral(String nroDocumento, String nroRucEmpresa, String codTipoPlan, Integer idPlanManejo) throws Exception;
}
