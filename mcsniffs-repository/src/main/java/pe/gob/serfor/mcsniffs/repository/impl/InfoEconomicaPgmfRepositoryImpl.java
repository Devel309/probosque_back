package pe.gob.serfor.mcsniffs.repository.impl;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.fasterxml.jackson.databind.node.BooleanNode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoBiologicoFaunaSilvestrePgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActEconomicaPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActividadesPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaConfictosPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaInfraestructuraPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaPgmf;
import pe.gob.serfor.mcsniffs.repository.InfoEconomicaPgmfRepository;

@Repository
public class InfoEconomicaPgmfRepositoryImpl extends JdbcDaoSupport implements InfoEconomicaPgmfRepository{


    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    
    @Override
    public ResultEntity<InfoEconomicaPgmf> registroInfoEconomicaPgmf(InfoEconomicaPgmf request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                            pstm = con.prepareCall("{call pa_InfoEconomicaPgmf_Registrar(?,?,?,?,?,?,?)}");
                            pstm.setObject (1, request.getIdInformacionSocioEconomica(),Types.INTEGER);
                            pstm.setObject (2, request.getIdPlanManejo(),Types.INTEGER);         
                            pstm.setObject (3, request.getPeronasTrabajan(),Types.INTEGER);  
                            pstm.setObject (4, request.getPersonasEmpadronada(),Types.INTEGER);                
                            pstm.setObject (5, request.getIdUsuarioRegistro(),Types.INTEGER);
                            pstm.setObject (6, request.getIdUsuarioModificacion(),Types.INTEGER);

                            pstm.registerOutParameter(7,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(7));        
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }

    @Override
    public ResultClassEntity obtenerInfoEconomicaPgmf(InfoEconomicaPgmf request) {
        // TODO Auto-generated method stub
        ResultClassEntity resul=new ResultClassEntity();
        InfoEconomicaPgmf obj=new InfoEconomicaPgmf();
        try
        {
            String sql = "EXEC pa_InfoEconomicaPgmf_Obtener " + request.getIdPlanManejo();                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

                    for(Map<String, Object> row:rows) {
                        obj.setIdInformacionSocioEconomica((Integer)row.get("NU_ID_INFORMACIONSOCIOECONOMICA")) ;
                        obj.setIdPlanManejo((Integer)row.get("NU_ID_PLAN_MANEJO")) ;
                        obj.setPeronasTrabajan((Integer)row.get("NU_PERSONAS_EMPADRONADA")) ;
                        obj.setPersonasEmpadronada((Integer)row.get("NU_PERONAS_TRABAJAN")) ;
                        resul.setData(obj);
                    }        

            resul.setMessage("Información no encontrada");
            resul.setSuccess(true);
        } catch (Exception e) {
            resul.setMessage("Ocurrió un Error");
            resul.setInnerException(e.getMessage());
            resul.setSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity<InfoEconomicaActEconomicaPgmf> registroInfoEconomicaActEconomicaPgmf(
        List<InfoEconomicaActEconomicaPgmf>  request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
        CallableStatement pstm  = null;
        Connection con = null;    
        try
        {                                  
        String res="";        
            con = dataSource.getConnection();
            for(InfoEconomicaActEconomicaPgmf item:request){
                pstm = con.prepareCall("{call pa_InfoEconomicaActEconomicaPgmf_Registrar(?,?,?,?,?,?,?,?)}");
                pstm.setObject (1, item.getIdInfoeconomicaActiEconomica(),Types.INTEGER);
                pstm.setObject (2, item.getIdInformacionSocioEconomica(),Types.INTEGER);         
                pstm.setObject (3, item.getIdTipoactividadEconomica(),Types.VARCHAR);
                pstm.setObject (4, item.getTipoActividadEconomica(),Types.VARCHAR);
                pstm.setObject (5, item.getDescripcion(),Types.VARCHAR);
                pstm.setObject (6, item.getActivo(),Types.BOOLEAN);
                pstm.setObject (7, item.getIdUsuarioRegistro(),Types.INTEGER);
                pstm.registerOutParameter(8,Types.INTEGER);
                pstm.execute();
                resul.setCodigo(pstm.getInt(8));
            }
              
            resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
            resul.setIsSuccess((resul.getCodigo()>0?true:false));

        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setCodigo(0);
            resul.setIsSuccess(false);
        }finally{
                try {
                    pstm.close();
                    con.close();
                } catch (Exception e) {
                    resul.setMessage(e.getMessage());
                    resul.setCodigo(0);
                    resul.setIsSuccess(false);
                }
            }
        return resul;*/
    }

    @Override
    public ResultEntity<InfoEconomicaActEconomicaPgmf> obtenerInfoEconomicaActEconomicaPgmf(
            InfoEconomicaActEconomicaPgmf request) {
        // TODO Auto-generated method stub
        ResultEntity resul=new ResultEntity();
        try
        {        
            List<InfoEconomicaActEconomicaPgmf> objList=new ArrayList<InfoEconomicaActEconomicaPgmf>();
            String sql = "EXEC pa_InfoEconomicaActEconomicaPgmf_Obtener " + request.getIdInformacionSocioEconomica();                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);                 
                 
                    for(Map<String, Object> row:rows) {
                        InfoEconomicaActEconomicaPgmf obj=new InfoEconomicaActEconomicaPgmf();
                        obj.setIdInformacionSocioEconomica((Integer)row.get("NU_ID_INFORMACIONSOCIOECONOMICA")) ;
                        obj.setIdInfoeconomicaActiEconomica((Integer)row.get("NU_ID_INFOECONOMICA_ACTIECONOMICA")) ;
                        obj.setTipoActividadEconomica((String)row.get("TX_TIPOACTIVIDADECONOMICA")) ;
                        obj.setIdTipoactividadEconomica((String)row.get("TX_ID_TIPOACTIVIDADECONOMICA")) ;
                        obj.setDescripcion((String)row.get("TX_DESCRIPCION")); ;
                        obj.setActivo((Boolean) row.get("NU_ACTIVO")) ;

                        objList.add(obj);
                    }        
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity<InfoEconomicaActEconomicaPgmf> EliminarInfoEconomicaActEconomicaPgmf(
        InfoEconomicaActEconomicaPgmf request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                            
                            pstm = con.prepareCall("{call pa_InfoEconomicaActEconomicaPgmf_Eliminar(?,?,?)}");
                            pstm.setObject (1, request.getIdInfoeconomicaActiEconomica(),Types.INTEGER);                           
                            pstm.setObject (2, request.getIdUsuarioElimina(),Types.INTEGER);
                            pstm.registerOutParameter(3,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(3));       
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }


    @Override
    public ResultEntity<InfoEconomicaInfraestructuraPgmf> registroInfoEconomicaInfraestructuraPgmf(
            List<InfoEconomicaInfraestructuraPgmf> request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                        for(InfoEconomicaInfraestructuraPgmf item:request){
                            pstm = con.prepareCall("{call pa_InfoEconomicaInfraestructuraPgmf_Registrar(?,?,?,?,?,?,?,?,?,?,?,?)}");
                            pstm.setObject (1, item.getIdInfoEconomicaInfraestructura(),Types.INTEGER);
                            pstm.setObject (2, item.getIdInformacionSocioEconomica(),Types.INTEGER);         
                            pstm.setObject (3, item.getIdTipoinfraestructura(),Types.VARCHAR);
                            pstm.setObject (4, item.getTipoInfraestructura(),Types.VARCHAR);                
                            pstm.setObject (5, item.getEste(),Types.INTEGER);
                            pstm.setObject (6, item.getNorte(),Types.INTEGER);
                            pstm.setObject (7, item.getZonautm(),Types.VARCHAR);
                            pstm.setObject (8, item.getReferencia(),Types.VARCHAR);
                            pstm.setObject (9, item.getActivo(),Types.BOOLEAN);
                            pstm.setObject (10, item.getDescripcion(),Types.VARCHAR);

                            pstm.setObject (11, item.getIdUsuarioRegistro(),Types.INTEGER);
                            pstm.registerOutParameter(12,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(12));
                        }
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }

    @Override
    public ResultEntity<InfoEconomicaInfraestructuraPgmf> obtenerInfoEconomicaInfraestructuraPgmf(
            InfoEconomicaInfraestructuraPgmf request) {
        // TODO Auto-generated method stub
        ResultEntity resul=new ResultEntity();
        try
        {        
            List<InfoEconomicaInfraestructuraPgmf> objList=new ArrayList<InfoEconomicaInfraestructuraPgmf>();
            String sql = "EXEC pa_InfoEconomicaInfraestructuraPgmf_Obtener " + request.getIdInformacionSocioEconomica();                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);                 
                 
                    for(Map<String, Object> row:rows) {
                        InfoEconomicaInfraestructuraPgmf obj=new InfoEconomicaInfraestructuraPgmf();
                        obj.setIdInformacionSocioEconomica((Integer)row.get("NU_ID_INFORMACIONSOCIOECONOMICA")) ;
                        obj.setIdInfoEconomicaInfraestructura((Integer)row.get("NU_ID_INFOECONOMICA_INFRAESTRUCTURA")) ;
                        obj.setIdTipoinfraestructura((String)row.get("TX_ID_TIPOINFRAESTRUCTURA")) ;
                        obj.setTipoInfraestructura((String)row.get("TX_TIPOINFRAESTRUCTURA")) ;
                        obj.setEste((Integer)row.get("NU_ESTE")) ;
                        obj.setNorte((Integer)row.get("NU_NORTE")); ;
                        obj.setZonautm((String)row.get("TX_ZONAUTM")) ;
                        obj.setReferencia((String)row.get("TX_REFERENCIA")) ;
                        obj.setActivo((Boolean) row.get("NU_ACTIVO")) ;
                        obj.setDescripcion((String) row.get("TX_DESCRIPCION")); ;
                       
                        objList.add(obj);
                    }        
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage("Ocurrió un Error");
            resul.setInnerException(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity<InfoEconomicaInfraestructuraPgmf> EliminarInfoEconomicaInfraestructuraPgmf(
        InfoEconomicaInfraestructuraPgmf request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                            
                            pstm = con.prepareCall("{call pa_InfoEconomicaInfraestructuraPgmf_Eliminar(?,?,?)}");
                            pstm.setObject (1, request.getIdInfoEconomicaInfraestructura(),Types.INTEGER);                           
                            pstm.setObject (2, request.getIdUsuarioElimina(),Types.INTEGER);
                            pstm.registerOutParameter(3,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(3));       
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }


    @Override
    public ResultEntity<InfoEconomicaActividadesPgmf> registroInfoEconomicaActividadesPgmf(
            List<InfoEconomicaActividadesPgmf> request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
        CallableStatement pstm  = null;
        Connection con = null;    
        try
        {                                  
        String res="";        
            con = dataSource.getConnection();
            for(InfoEconomicaActividadesPgmf item:request){
                pstm = con.prepareCall("{call pa_InfoEconomicaActividadesPgmf_Registrar(?,?,?,?,?,?,?,?,?,?)}");
                pstm.setObject (1, item.getIdInfoeconomicaActividades(),Types.INTEGER);
                pstm.setObject (2, item.getIdInformacionSocioEconomica(),Types.INTEGER);         
                pstm.setObject (3, item.getIdTipoActividad(),Types.INTEGER);  
                pstm.setObject (4, item.getTipoActividad(),Types.VARCHAR);   
                pstm.setObject (5, item.getEspeciesExtraidas(),Types.VARCHAR);  
                pstm.setObject (6, item.getObservacion(),Types.VARCHAR);  
                pstm.setObject (7, item.getActivo(),Types.BOOLEAN);  

                pstm.setObject (8, item.getIdUsuarioRegistro(),Types.INTEGER);

                pstm.registerOutParameter(10,Types.INTEGER);
                pstm.execute();
                resul.setCodigo(pstm.getInt(10));     
            }

            resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
            resul.setIsSuccess((resul.getCodigo()>0?true:false));

        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setCodigo(0);
            resul.setIsSuccess(false);
        }finally{
                try {
                    pstm.close();
                    con.close();
                } catch (Exception e) {
                    resul.setMessage(e.getMessage());
                    resul.setCodigo(0);
                    resul.setIsSuccess(false);
                }
            }
        return resul;*/
    }

    @Override
    public ResultEntity<InfoEconomicaActividadesPgmf> obtenerInfoEconomicaActividadesPgmf(
            InfoEconomicaActividadesPgmf request) {
        // TODO Auto-generated method stub
        ResultEntity resul=new ResultEntity();
        try
        {        
            List<InfoEconomicaActividadesPgmf> objList=new ArrayList<InfoEconomicaActividadesPgmf>();
            String sql = "EXEC pa_InfoEconomicaActividadesPgmf_Obtener " + request.getIdInformacionSocioEconomica();                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);                 
                 
                    for(Map<String, Object> row:rows) {
                        InfoEconomicaActividadesPgmf obj=new InfoEconomicaActividadesPgmf();
                        obj.setIdInfoeconomicaActividades((Integer)row.get("NU_ID_INFOECONOMICA_ACTIVIDADES")) ;
                        obj.setIdInformacionSocioEconomica((Integer)row.get("NU_ID_INFORMACIONSOCIOECONOMICA")) ;
                        obj.setIdTipoActividad((Integer)row.get("NU_ID_TIPOACTIVIDAD")) ;
                        obj.setTipoActividad((String)row.get("TX_TIPOACTIVIDAD")) ;
                        obj.setEspeciesExtraidas((String)row.get("TX_ESPECIESEXTRAIDAS")) ;
                        obj.setObservacion((String)row.get("TX_OBSERVACION")) ;
                        obj.setActivo((Boolean) row.get("NU_ACTIVO")) ;
                       
                        objList.add(obj);
                    }        
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

  
    @Override
    public ResultEntity<InfoEconomicaConfictosPgmf> registroInfoEconomicaConfictosPgmf(
            List<InfoEconomicaConfictosPgmf> request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                           for(InfoEconomicaConfictosPgmf item:request){
                            pstm = con.prepareCall("{call pa_InfoEconomicaConfictosPgmf_Registrar(?,?,?,?,?,?,?,?)}");
                            pstm.setObject (1, item.getIdInfoEconomicaConfictos(),Types.INTEGER);
                            pstm.setObject (2, item.getIdInformacionSocioEconomica(),Types.INTEGER);         
                            pstm.setObject (3, item.getTipoConficto(),Types.VARCHAR);  
                            pstm.setObject (4, item.getPropuestaSolucion(),Types.VARCHAR);                
                            pstm.setObject (5, item.getIdUsuarioRegistro(),Types.INTEGER);
                            pstm.setObject (6, item.getIdUsuarioModificacion(),Types.INTEGER);
                            pstm.setObject (7, item.getActivo(),Types.INTEGER);

                            pstm.registerOutParameter(8,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(8));        
                        }
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }

    @Override
    public ResultEntity<InfoEconomicaConfictosPgmf> obtenerInfoEconomicaConfictosPgmf(
            InfoEconomicaConfictosPgmf request) {
        // TODO Auto-generated method stub
        ResultEntity resul=new ResultEntity();
        try
        {        
            List<InfoEconomicaConfictosPgmf> objList=new ArrayList<InfoEconomicaConfictosPgmf>();
            String sql = "EXEC pa_InfoEconomicaConfictosPgmf_Obtener " + request.getIdInformacionSocioEconomica();                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);                 
                 
                    for(Map<String, Object> row:rows) {
                        InfoEconomicaConfictosPgmf obj=new InfoEconomicaConfictosPgmf();
                        obj.setIdInfoEconomicaConfictos((Integer)row.get("NU_ID_INFOECONOMICA_CONFICTOS")) ;
                        obj.setIdInformacionSocioEconomica((Integer)row.get("NU_ID_INFORMACIONSOCIOECONOMICA")) ;
                        obj.setTipoConficto((String)row.get("TX_TIPOCONFICTO")) ;
                        obj.setPropuestaSolucion((String)row.get("TX_PROPUESTASOLUCION"));
                        obj.setActivo((Boolean) row.get("NU_ACTIVO")) ;
                        obj.setIdUsuarioRegistro((Integer)row.get("NU_ID_USUARIO_REGISTRO")) ;
                        obj.setIdUsuarioModificacion((Integer)row.get("NU_ID_USUARIO_MODIFICACION")) ;
                       
                        objList.add(obj);
                    }        
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity<InfoEconomicaConfictosPgmf> EliminarInfoEconomicaConfictosPgmf(
            InfoEconomicaConfictosPgmf request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                            
                            pstm = con.prepareCall("{call pa_InfoEconomicaConfictosPgmf_Eliminar(?,?,?)}");
                            pstm.setObject (1, request.getIdInfoEconomicaConfictos(),Types.INTEGER);                           
                            pstm.setObject (2, request.getIdUsuarioElimina(),Types.INTEGER);
                            pstm.registerOutParameter(3,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(3));        
                         
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }

  
    @Override
    public ResultEntity<InfoBiologicoFaunaSilvestrePgmf> registroInfoBiologicoFaunaSilvestrePgmf(
            List<InfoBiologicoFaunaSilvestrePgmf> request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                           for(InfoBiologicoFaunaSilvestrePgmf item:request){
                            pstm = con.prepareCall("{call pa_BiologicaFaunaSilvestrePgmf_Registrar(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                            pstm.setObject (1, item.getIdInfobioFaunaSilvestre(),Types.INTEGER);
                            pstm.setObject (2, item.getIdPlanManejo(),Types.INTEGER);  
                            pstm.setObject (3, item.getIdEspecieForestal(),Types.INTEGER);         
                            pstm.setObject (4, item.getNombreComun(),Types.VARCHAR);  
                            pstm.setObject (5, item.getNombreCientifico(),Types.VARCHAR); 
                            pstm.setObject (6, item.getCategoriaAmenaza(),Types.VARCHAR); 
                            pstm.setObject (7, item.getNombreNativo(),Types.VARCHAR);
                            pstm.setObject (8, item.getCites(),Types.VARCHAR);
                            pstm.setObject (9, item.getObservacion(),Types.VARCHAR);                     
                            pstm.setObject (10, item.getEstatus(),Types.VARCHAR);
                            pstm.setObject (11, item.getIdUsuarioRegistro(),Types.INTEGER);
                            pstm.setObject (12, item.getIdUsuarioModificacion(),Types.INTEGER);

                            pstm.registerOutParameter(13,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(13));        
                        }
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }

    @Override
    public ResultEntity<InfoBiologicoFaunaSilvestrePgmf> obtenerInfoBiologicoFaunaSilvestrePgmf(
            InfoBiologicoFaunaSilvestrePgmf request) {
        // TODO Auto-generated method stub
        ResultEntity resul=new ResultEntity();
        try
        {        
            List<InfoBiologicoFaunaSilvestrePgmf> objList=new ArrayList<InfoBiologicoFaunaSilvestrePgmf>();
            String sql = "EXEC pa_InfoBiologicaFaunaSilvestrePgmf_Obtener " + request.getIdPlanManejo();                                 
                  
                    List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);                 
                 
                    for(Map<String, Object> row:rows) {
                        InfoBiologicoFaunaSilvestrePgmf obj=new InfoBiologicoFaunaSilvestrePgmf();
                        obj.setIdInfobioFaunaSilvestre((Integer)row.get("NU_ID_INFOBIOFAUNASILVESTRE")) ;
                        obj.setIdPlanManejo((Integer)row.get("NU_ID_PLAN_MANEJO")) ;
                        obj.setIdEspecieForestal((Integer)row.get("NU_ID_ESPECIEFORESTAL")) ;
                        obj.setNombreComun((String)row.get("TX_NOMBRECOMUN")) ;
                        obj.setNombreCientifico((String)row.get("TX_NOMBRECIENTIFICO")) ;
                        obj.setCategoriaAmenaza((String)row.get("TX_CATEGORIAAMENAZA")) ;
                        obj.setNombreNativo((String)row.get("TX_NOMBRENATIVO")) ;
                        obj.setCites((String)row.get("TX_CITES")) ;
                        obj.setObservacion((String)row.get("TX_OBSERVACION")) ;
                        obj.setEstatus((String)row.get("TX_ESTATUS")) ;
                        obj.setEstado((String)row.get("TX_ESTADO")) ;
                        obj.setIdUsuarioModificacion((Integer)row.get("NU_ID_USUARIO_MODIFICACION")) ;
                       
                        objList.add(obj);
                    }        
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }

    @Override
    public ResultEntity<InfoBiologicoFaunaSilvestrePgmf> EliminarBiologicoFaunaSilvestrePgmf(
            InfoBiologicoFaunaSilvestrePgmf request) {
        // TODO Auto-generated method stub
        return null;
/*        ResultEntity resul=new ResultEntity();
                    CallableStatement pstm  = null;
                    Connection con = null;    
                    try
                    {                                  
                    String res="";        
                        con = dataSource.getConnection();
                            
                            pstm = con.prepareCall("{call pa_BiologicaFaunaSilvestrePgmf_Eliminar(?,?,?)}");
                            pstm.setObject (1, request.getIdInfobioFaunaSilvestre(),Types.INTEGER);                           
                            pstm.setObject (2, request.getIdUsuarioElimina(),Types.INTEGER);
                            pstm.registerOutParameter(3,Types.INTEGER);
                            pstm.execute();
                            resul.setCodigo(pstm.getInt(3));       
                          
                        resul.setMessage((resul.getCodigo()>0?"Información Actualizada":"Informacion no Actualizada"));
                        resul.setIsSuccess((resul.getCodigo()>0?true:false));
            
                    } catch (Exception e) {
                        resul.setMessage(e.getMessage());
                        resul.setCodigo(0);
                        resul.setIsSuccess(false);
                    }finally{
                            try {
                                pstm.close();
                                con.close();
                            } catch (Exception e) {
                                resul.setMessage(e.getMessage());
                                resul.setCodigo(0);
                                resul.setIsSuccess(false);
                            }
                        }
                    return resul;*/
    }

    

    
}
