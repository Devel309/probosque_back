package pe.gob.serfor.mcsniffs.repository.impl;

 

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.FichaEvaluacionEntitty;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class EvaluacionRepositoryImpl extends JdbcDaoSupport implements EvaluacionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(EvaluacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public List<EvaluacionDto> listarEvaluacion(EvaluacionDto dto) throws Exception {
         
        List<EvaluacionDto> lista = new ArrayList<EvaluacionDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_Listar");            
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
            processStored.setParameter("codigoEvaluacion",dto.getCodigoEvaluacion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                EvaluacionDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new EvaluacionDto();

                        temp.setIdEvaluacion(row[0]==null?null:(Integer) row[0]);
                        temp.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoEvaluacion(row[2]==null?null:(String) row[2]);
                        temp.setTipoEvaluacion(row[3]==null?null:(String) row[3]);
                        temp.setFechaEvaluacionInicial(row[4]==null?null:(Timestamp) row[4]);
                        temp.setFechaEvaluacionFinal(row[5]==null?null:(Timestamp) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setDetalle(row[7]==null?null:(String) row[7]);
                        temp.setDescripcion(row[8]==null?null:(String) row[8]);
                        temp.setEstadoEvaluacion(row[9]==null?null:(String) row[9]);
                        temp.setResponsable(row[10]==null?null:(String) row[10]);
                        temp.setEstado(row[11]==null?null:(String) row[11]);
                        temp.setIdUsuarioRegistro(row[12]==null?null:(Integer) row[12]);
          
                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacion", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<EvaluacionResumidoDto> listarEvaluacionResumido(EvaluacionResumidoDto dto) throws Exception {

        List<EvaluacionResumidoDto> lista = new ArrayList<EvaluacionResumidoDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionResumido_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                EvaluacionResumidoDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new EvaluacionResumidoDto();

                        temp.setIdEvaluacion(row[0]==null?null:(Integer) row[0]);
                        temp.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoEvaluacion(row[2]==null?null:(String) row[2]);


                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacion", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<EvaluacionPermisoForestalDto> listarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception {

        List<EvaluacionPermisoForestalDto> lista = new ArrayList<EvaluacionPermisoForestalDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestal_Listar");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEvaluacion", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestal",dto.getIdPermisoForestal());
            processStored.setParameter("codigoEvaluacion",dto.getCodigoEvaluacion());
            processStored.setParameter("tipoEvaluacion",dto.getTipoEvaluacion());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                EvaluacionPermisoForestalDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new EvaluacionPermisoForestalDto();

                        temp.setIdEvaluacionPermiso(row[0]==null?null:(Integer) row[0]);
                        temp.setIdPermisoForestal(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoEvaluacion(row[2]==null?null:(String) row[2]);
                        temp.setTipoEvaluacion(row[3]==null?null:(String) row[3]);
                        temp.setFechaEvaluacionInicial(row[4]==null?null:(Timestamp) row[4]);
                        temp.setFechaEvaluacionFinal(row[5]==null?null:(Timestamp) row[5]);
                        temp.setObservacion(row[6]==null?null:(String) row[6]);
                        temp.setDetalle(row[7]==null?null:(String) row[7]);
                        temp.setDescripcion(row[8]==null?null:(String) row[8]);
                        temp.setEstadoEvaluacion(row[9]==null?null:(String) row[9]);
                        temp.setResponsable(row[10]==null?null:(String) row[10]);
                        temp.setEstado(row[11]==null?null:(String) row[11]);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacion", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<EvaluacionPermisoForestalDto> listarEvaluacionPermisoForestalUsuario(EvaluacionPermisoForestalDto dto) throws Exception {

        List<EvaluacionPermisoForestalDto> lista = new ArrayList<EvaluacionPermisoForestalDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestal_ListarUsuario");
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correo", String.class, ParameterMode.INOUT);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPermisoForestal",dto.getIdPermisoForestal());
            processStored.setParameter("correo",null);
            processStored.execute();


            EvaluacionPermisoForestalDto  temp = new EvaluacionPermisoForestalDto();
                    String correo = (String) processStored.getOutputParameterValue("correo");
                    temp.setResponsable(correo);
                    lista.add(temp);


            return lista;
        } catch (Exception e) {
            log.error("LISTAR USUARIO", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity eliminarEvaluacion(EvaluacionDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_Eliminar");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEvaluacion", dto.getIdEvaluacion());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestal_Eliminar");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idEvaluacion", dto.getIdEvaluacionPermiso());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }




    @Override
    public List<EvaluacionDetalleDto> listarEvaluacionTab(Integer idPlanManejo, String codigo,String codigoEvaluacionDet, String conforme, String codigoEvaluacionDetSub) throws Exception {
        List<EvaluacionDetalleDto> lista = new ArrayList<EvaluacionDetalleDto>();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionTab_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigo", codigo);
            processStored.setParameter("codigoEvaluacionDet", codigoEvaluacionDet);
            processStored.setParameter("codigoEvaluacionDetSub", codigoEvaluacionDetSub);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        EvaluacionDetalleDto temp = new EvaluacionDetalleDto();
                        temp.setCodigoEvaluacionDet((String) row[0]);
                        temp.setConforme((String) row[1]);
                        temp.setObservacion((String) row[2]);
                        temp.setCodigoEvaluacion((String) row[3]);
                        temp.setCodigoEvaluacionDetSub((String) row[4]);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }

/*
    @Override
    public List<EvaluacionGeneralDto> listarEvaluacionGeneral(EvaluacionGeneralDto param) throws Exception {
        List<EvaluacionGeneralDto> lista = new ArrayList<EvaluacionGeneralDto>();
        try {
            String codigoEvaluacionDetSub="";
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionGeneral_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoEvaluacion", param.getCodigoEvaluacion());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        EvaluacionGeneralDto temp = new EvaluacionGeneralDto();
                        temp.setCodigoEvaluacionDetSub((String) row[0]);
                        codigoEvaluacionDetSub=(String) row[0];
                        ********************************** ACORDIONES **********************************************
                        List<EvaluacionDetalleDto> listDetAcordion = new ArrayList<EvaluacionDetalleDto>();
                        StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionGeneralDetalle_Listar");
                        pa.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                        pa.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
                        pa.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class, ParameterMode.IN);
                        SpUtil.enableNullParams(pa);
                        pa.setParameter("idPlanManejo", param.getIdPlanManejo());
                        pa.setParameter("codigoEvaluacion", param.getCodigoEvaluacion());
                        pa.setParameter("codigoEvaluacionDetSub", codigoEvaluacionDetSub);
                        pa.execute();
                        List<Object[]> spResultAcor = pa.getResultList();
                        if(spResultAcor!=null) {
                            if (spResultAcor.size() >= 1) {
                                for (Object[] rowAcor : spResultAcor) {
                                    EvaluacionDetalleDto temp2 = new EvaluacionDetalleDto();
                                    temp2.setCodigoEvaluacionDetPost((String) rowAcor[0]);
                                    temp2.setConforme((String) rowAcor[1]);
                                    temp2.setObservacion((String) rowAcor[2]);
                                    temp2.setIdArchivo((Integer) rowAcor[3]);
                                    listDetAcordion.add(temp2);
                                }
                            }
                        }
                        ********************************************************************************
                        temp.setListAcordion(listDetAcordion);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarEvaluacion", e.getMessage());
            throw e;
        }
    }

/*
    @Override
    public List<EvaluacionGeneralDto>  listarEvaluacionGeneral(EvaluacionGeneralDto param) throws Exception {
        List<EvaluacionGeneralDto> lista = new ArrayList<EvaluacionGeneralDto>();
        try {

            String codigoEvaluacionDetSub="";
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionGeneral_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoEvaluacion", param.getCodigoEvaluacion());
            processStored.execute();

            List<Map<String, Object>> rows = processStored.getResultList();
            for(Map<String, Object> row:rows) {
                EvaluacionGeneralDto objOrdPro=new EvaluacionGeneralDto();
                objOrdPro.setCodigoEvaluacionDetSub((String)row.get("codigoEvaluacionDetSub"));
                       // EvaluacionGeneralDto temp = new EvaluacionGeneralDto();
                        //temp.setCodigoEvaluacionDetSub((String) row[0]);
                        //codigoEvaluacionDetSub=(String) row[0];
                        //lista.add(temp);

            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }*/



    @Override
    public ResultEntity listarEvaluacionGeneral(EvaluacionGeneralDto request) {
        ResultEntity resul=new ResultEntity();
        try
        {
            String codigoEvaluacionDetSub="";
            List<EvaluacionGeneralDto> objList=new ArrayList<>();
            String sql = "EXEC pa_EvaluacionGeneral_Listar " + request.getIdPlanManejo()+","+request.getCodigoEvaluacion();

            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

            for(Map<String, Object> row:rows) {
                EvaluacionGeneralDto objOrdPro=new EvaluacionGeneralDto();
                objOrdPro.setCodigoEvaluacionDetSub((String)row.get("codigoEvaluacionDetSub"));
                codigoEvaluacionDetSub=(String)row.get("codigoEvaluacionDetSub");

                /**************************************** ACORDIONES **************************************************/

                List<EvaluacionDetalleDto> objListAcor=new ArrayList<>();
                String sqlAcor = "EXEC pa_EvaluacionGeneralDetalle_Listar " + request.getIdPlanManejo()+","+request.getCodigoEvaluacion()+","+codigoEvaluacionDetSub;

                List<Map<String, Object>> rowsAcor = getJdbcTemplate().queryForList(sqlAcor);
                for(Map<String, Object> rowAcor:rowsAcor) {
                    EvaluacionDetalleDto objOrdProAcor=new EvaluacionDetalleDto();
                    objOrdProAcor.setCodigoEvaluacionDetPost((String)rowAcor.get("codigoEvaluacionDetPost"));
                    objOrdProAcor.setConforme((String)rowAcor.get("conforme"));
                    objOrdProAcor.setObservacion((String)rowAcor.get("observacion"));
                    objOrdProAcor.setIdArchivo((Integer)rowAcor.get("idArchivo"));
                    objListAcor.add(objOrdProAcor);
                }
                objOrdPro.setListAcordion(objListAcor);
                objList.add(objOrdPro);

            }
            resul.setData(objList);
            resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            resul.setIsSuccess((objList.size()!=0?true:false));
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setIsSuccess(false);
        }
        return resul;
    }


/*
    @Override
    public ResultClassEntity RegistrarEvaluacion(List<EvaluacionDto> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<EvaluacionDto> list_ = new ArrayList<>();
        try {
            for (EvaluacionDto p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_Registrar");
                sp.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.INOUT);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoEvaluacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaEvaluacionIni", Timestamp.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaEvaluacionFin", Timestamp.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("estadoEvaluacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                SpUtil.enableNullParams(sp);

                sp.setParameter("idEvaluacion", p.getIdEvaluacion());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("codigoEvaluacion", p.getCodigoEvaluacion());
                sp.setParameter("tipoEvaluacion", p.getTipoEvaluacion());
                sp.setParameter("fechaEvaluacionIni", p.getFechaEvaluacionIni());
                sp.setParameter("fechaEvaluacionFin", p.getFechaEvaluacionFin());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("estadoEvaluacion", p.getEstadoEvaluacion());
                sp.setParameter("responsable", p.getResponsable());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        EvaluacionDto obj_ = new EvaluacionDto();
                        obj_.setIdEvaluacion((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        obj_.setCodigoEvaluacion((String) row_[2]);
                        obj_.setTipoEvaluacion((String) row_[3]);
                        obj_.setFechaEvaluacionInicial((Timestamp) row_[4]);
                        obj_.setFechaEvaluacionFinal((Timestamp) row_[5]);
                        obj_.setObservacion((String) row_[6]);
                        obj_.setDetalle((String) row_[7]);
                        obj_.setDescripcion((String) row_[8]);
                        obj_.setEstadoEvaluacion((String) row_[9]);
                        obj_.setResponsable((String) row_[10]);

                        List<EvaluacionDetalleDto> listDet = new ArrayList<>();
                        for (EvaluacionDetalleDto param : p.getListarEvaluacionDetalle()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_EvaluacionDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idEvaluacionDet", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idEvaluacion", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoEvaluacionDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoEvaluacionDetSub", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoEvaluacionDetPost", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("conforme", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacion", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("detalle", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcion", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("estadoEvaluacionDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idArchivo", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("fechaEvaluacionDetIni", Timestamp.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("fechaEvaluacionDetFin", Timestamp.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class,ParameterMode.IN);

                            SpUtil.enableNullParams(pa);
                            pa.setParameter("idEvaluacionDet", param.getIdEvaluacionDet());
                            pa.setParameter("idEvaluacion", obj_.getIdEvaluacion());
                            pa.setParameter("codigoEvaluacionDet", param.getCodigoEvaluacionDet());
                            pa.setParameter("codigoEvaluacionDetSub", param.getCodigoEvaluacionDetSub());
                            pa.setParameter("codigoEvaluacionDetPost", param.getCodigoEvaluacionDetPost());
                            pa.setParameter("conforme", param.getConforme());
                            pa.setParameter("observacion", param.getObservacion());
                            pa.setParameter("detalle", param.getDetalle());
                            pa.setParameter("descripcion", param.getDescripcion());
                            pa.setParameter("estadoEvaluacionDet", param.getEstadoEvaluacionDet());
                            pa.setParameter("idArchivo", param.getIdArchivo());
                            pa.setParameter("fechaEvaluacionDetIni", param.getFechaEvaluacionDetInicial());
                            pa.setParameter("fechaEvaluacionDetFin", param.getFechaEvaluacionDetIni());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());


                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                EvaluacionDetalleDto obj = null;
                                for (Object[] row : spResult) {
                                    obj = new EvaluacionDetalleDto();
                                    obj.setIdEvaluacionDet((Integer) row[0]);
                                    obj.setIdEvaluacion((Integer) row[1]);
                                    obj.setCodigoEvaluacionDet((String) row[2]);
                                    obj.setCodigoEvaluacionDetSub((String) row[3]);
                                    obj.setCodigoEvaluacionDetPost((String) row[4]);
                                    obj.setConforme((String) row[5]);
                                    obj.setObservacion((String) row[6]);
                                    obj.setDetalle((String) row[7]);
                                    obj.setDescripcion((String) row[8]);
                                    obj.setEstadoEvaluacionDet((String) row[9]);
                                    obj.setIdArchivo((Integer) row[10]);
                                    obj.setFechaEvaluacionDetInicial((Timestamp) row[11]);
                                    obj.setFechaEvaluacionDetFinal((Timestamp) row[12]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListarEvaluacionDetalle(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la evaluación .");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }
    */

    @Override
    public ResultClassEntity registrarEvaluacionPermisoForestal(EvaluacionPermisoForestalDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionPermisoForestal_Registrar");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionIni", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacion", dto.getIdEvaluacionPermiso());
            processStored.setParameter("idPermisoForestal", dto.getIdPermisoForestal());
            processStored.setParameter("codigoEvaluacion", dto.getCodigoEvaluacion());
            processStored.setParameter("tipoEvaluacion", dto.getTipoEvaluacion());
            processStored.setParameter("fechaEvaluacionIni", dto.getFechaEvaluacionInicial());
            processStored.setParameter("fechaEvaluacionFin", dto.getFechaEvaluacionFinal());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("estadoEvaluacion", dto.getEstadoEvaluacion());
            processStored.setParameter("responsable", dto.getResponsable());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();
            Integer id = (Integer) processStored.getOutputParameterValue("idEvaluacion");
            dto.setIdEvaluacionPermiso(id);
            result.setSuccess(true);

            result.setMessage(dto.getTipoEvaluacion());
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity registrarEvaluacion(EvaluacionDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_Registrar");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionIni", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEvaluacionFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacion", dto.getIdEvaluacion());
            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("codigoEvaluacion", dto.getCodigoEvaluacion());
            processStored.setParameter("tipoEvaluacion", dto.getTipoEvaluacion());
            processStored.setParameter("fechaEvaluacionIni", dto.getFechaEvaluacionInicial());
            processStored.setParameter("fechaEvaluacionFin", dto.getFechaEvaluacionFinal());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("estadoEvaluacion", dto.getEstadoEvaluacion());
            processStored.setParameter("responsable", dto.getResponsable());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();
            Integer id = (Integer) processStored.getOutputParameterValue("idEvaluacion");
            dto.setIdEvaluacion(id);
            result.setSuccess(true);
            result.setMessage("Se registró la Evaluacion correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultEntity<PlanManejoEvaluacionDetalleDto> listarEvaluacionDetalle(Integer idEvaluacion) {
        ResultEntity<PlanManejoEvaluacionDetalleDto> result= new ResultEntity<>();
        List<PlanManejoEvaluacionDetalleDto> lstresult = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionDetalle_ListarEvaluacion");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacion",idEvaluacion);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            PlanManejoEvaluacionDetalleDto c = null;
            String data = null;
            if(spResult!=null){
                EvaluacionDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {
                        c = new PlanManejoEvaluacionDetalleDto();
                        c.setIdPlanManejoEvalDet(row[0]==null?null:(Integer) row[0]);
                        c.setIdPlanManejoEvaluacion(row[1]==null?null:(Integer) row[1]);
                        c.setCodigoTipo(row[2]==null?null:(String) row[2]);
                        c.setCodigoTramite(row[3]==null?null:(String) row[3]);
                        data = (row[5]==null?null:(String) row[5]);
                        c.setObservacion(row[6]==null?null:(String) row[6]);
                        c.setIdArchivo(row[11]==null?null:(Integer) row[11]);
                        c.setDescArchivo(row[12]==null?null:(String) row[12]);
                        if (data!=null) {
                            c.setConformidad(data);
                        }
                        lstresult.add(c);
                    }
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInformacion(e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultEntity<FichaEvaluacionEntitty> listarFichaCabera(Integer idPlanManejo, String codigoProceso) {
        ResultEntity<FichaEvaluacionEntitty> result= new ResultEntity<>();
        List<FichaEvaluacionEntitty> lstresult = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_InformoCabera_ListarPGMFA_POAC");
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("codigoProceso",codigoProceso);
            processStored.setParameter("idPlanManejo",idPlanManejo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            FichaEvaluacionEntitty c = null;
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {
                        c = new FichaEvaluacionEntitty();
                        c.setCodigoProceso(row[0]==null?null:(String) row[0]);
                        c.setTitular(row[1]==null?null:(String) row[1]);
                        c.setAsunto(row[2]==null?null:(String) row[2]);
                        c.setElaborador(row[3]==null?null:(String) row[3]);
                        c.setIdPlanManejo(row[4]==null?null:(Integer) row[4]);
                        c.setTipoRequisito(row[5]==null?null:(String) row[5]);
                        c.setObservacion(row[6]==null?null:(String) row[6]);
                        c.setEvaluacion(row[7]==null?null:(String) row[7]);
                        c.setSubtitulo(row[8]==null?null:(String) row[8]);
                        c.setDetalle(row[9]==null?null:(String) row[9]);
                        c.setConforme(row[10]==null?null:(String) row[10]);
                        c.setObservacionDet(row[11]==null?null:(String) row[11]);
                        c.setCodigoEvaluacionDet(row[12]==null?null:(String) row[12]);
                        lstresult.add(c);
                    }
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setInformacion(e.getMessage());
            return  result;
        }
    }

    public ResultClassEntity diasHabiles(DiasHabilesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.fn_DiasHabiles");
            processStored.registerStoredProcedureParameter("fechaDesde", Timestamp.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaHasta", Timestamp.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codUbigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codNacional", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("fechaDesde", dto.getFechaDesde());
            processStored.setParameter("fechaHasta", dto.getFechaHasta());
            processStored.setParameter("codUbigeo", dto.getCodUbigeo());
            processStored.setParameter("codNacional", dto.getCodNacional());
            processStored.execute();
            Integer diasHabiles = (Integer) processStored.getSingleResult();
            dto.setDiasHabiles(diasHabiles);
            result.setSuccess(true);
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /*@Override
    public ResultClassEntity ListarEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionLineamiento_Listar");
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoParametro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());
            sp.setParameter("idTipoParametro", param.getIdTipoParametro());
            sp.execute();
            List<PlanManejoEvaluacionDetalleDto> list = new ArrayList<PlanManejoEvaluacionDetalleDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    PlanManejoEvaluacionDetalleDto obj = new PlanManejoEvaluacionDetalleDto();
                    obj.setIdPlanManejoEvalDet((Integer) row[0]);
                    obj.setCodigoTipo((String) row[1]);
                    obj.setDescripcion((String) row[2]);
                    obj.setConforme((Boolean) row[3]);
                    obj.setObservacion((String) row[4]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }*/

    @Override
    public ResultClassEntity obtenerUltimaEvaluacion(Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_UltimaEvaluacion");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idEvaluacion");

            result.setSuccess(true);
            result.setCodigo(id);
            result.setMessage("Se obtuvo la ultima Evaluacion correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity obtenerUltimaEvaluacionPermisoForestal(Integer idPermisoForestal) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_UltimaEvaluacionPermisoForestal");
            processStored.registerStoredProcedureParameter("idEvaluacion", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPermisoForestal", idPermisoForestal);
            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idEvaluacion");

            result.setSuccess(true);
            result.setCodigo(id);
            result.setMessage("Se obtuvo la ultima Evaluacion de permisos forestales correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity evaluacionActualizarPlan(EvaluacionActualizarPlan dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Evaluacion_ActualizarEstPlan");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("codigo", dto.getCodigo());
            processStored.setParameter("idUsuarioModificacion", dto.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó correctamente.");


            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_PLANMANEJO,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PLANMANEJO + dto.getIdPlanManejo(),
                    dto.getIdUsuarioModificacion());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
