package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ListarCondicionMinimaDetalleDto;

public interface CondicionMinimaDetalleRepository {
 
    List<ListarCondicionMinimaDetalleDto> listarCondicionMinimaDetalle(Integer idCondicionMinima) throws Exception;
}
