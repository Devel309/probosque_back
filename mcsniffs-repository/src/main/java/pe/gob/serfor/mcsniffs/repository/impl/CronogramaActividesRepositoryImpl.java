package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;


import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import antlr.collections.impl.LList;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividesEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad.CronogramaActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.CronogramaActividesRepository;
// 
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Repository
public class CronogramaActividesRepositoryImpl extends JdbcDaoSupport implements CronogramaActividesRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager em;
    
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CronogramaActividesRepositoryImpl.class);


    @Override
    public ResultEntity<CronogramaActividesEntity> ListarCronogramaActividades(CronogramaActividesEntity request) {
        ResultEntity result=new ResultEntity();
        List<CronogramaActividesEntity> objList=new ArrayList<CronogramaActividesEntity>();
        try
        {
            StoredProcedureQuery processStored = em
                    .createStoredProcedureQuery("pa_LitarCronogramaActividad_PlanManejo_Cabecera");

            processStored.registerStoredProcedureParameter("NU_ID_PLAN_MANEJO", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("NU_ID_PLAN_MANEJO", request.getID_PLAN_MANEJO());
            processStored.execute();

            List spResult = processStored.getResultList();

            if (spResult != null) {
                String[] cab=new String[spResult.size()];
                int cont=0;

                if (spResult.size() >= 1) {
                    for (Object row : spResult) {
                        cab[cont]=(row.toString());
                        cont++;

                    }
                    result.setCabecera(cab);
                    String sql = "EXEC pa_LitarCronogramaActividad_PlanManejo " + request.getID_PLAN_MANEJO();
                    List<Map<String, Object>> rows_cab = getJdbcTemplate().queryForList(sql);
                    result.setLista(rows_cab);
                }
            }
            result.setData(objList);
            result.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
            result.setIsSuccess((objList.size()!=0?true:false));

        } catch (Exception e) {
            result.setMessage(e.getMessage());
            result.setIsSuccess(false);
        }
        return result;
    }

    @Override
    public ResultEntity<CronogramaActividesEntity> RegistrarCronogramaActividades(List<CronogramaActividesEntity> request) {
          return null;
        /*
        ResultEntity resul=new ResultEntity();
                CallableStatement pstm  = null;
                Connection con = null;    
                try
                {                                  
                String res="";        
                    con = dataSource.getConnection();
                    for(CronogramaActividesEntity obj:request){
                        pstm = con.prepareCall("{call pa_RegistroCronogramaActividad_PlanManejo (?,?,?,?,?,?,?,?,?,?)}");
                        pstm.setObject (1, obj.getID_CRON_ACTIVIDAD(),Types.INTEGER);
                        pstm.setObject (2, obj.getID_PLAN_MANEJO(),Types.INTEGER);                      
                        pstm.setObject (3, obj.getACTIVIDAD(),Types.VARCHAR);
                        pstm.setObject (4, obj.getACTIVIDAD_ACTUAL(),Types.VARCHAR);
                        pstm.setObject (5, obj.getANO(),Types.INTEGER);  
                        pstm.setObject (6, Arrays.toString(obj.getANIO()),Types.VARCHAR);  
                        pstm.setObject (7, obj.getEstado(),Types.VARCHAR);
                        pstm.setObject (8, obj.getIdUsuarioRegistro(),Types.INTEGER);           
                        pstm.setObject (9, obj.getIdUsuarioModificacion(),Types.INTEGER);  
                        pstm.registerOutParameter(10,Types.INTEGER);
                        pstm.execute();
                        resul.setCodigo(pstm.getInt(10));        
                        res+= ( pstm.getInt(10)>0?"":"Item:" + obj.getID_PLAN_MANEJO().toString()  +" => "+obj.getACTIVIDAD()  +",") ;
                        resul.setInformacion(res);
                    }                       
                    resul.setMessage("Se registró el Cronograma de Actividades correctamente.");
                    resul.setIsSuccess((resul.getCodigo()>0?true:false));
        
                } catch (Exception e) {
                    resul.setMessage("Ocurrió un error.");
                    resul.setCodigo(0);
                    resul.setIsSuccess(false);
                }finally{
                        try {
                            pstm.close();
                            con.close();
                        } catch (Exception e) {
                            resul.setMessage("Ocurrió un error.");
                            resul.setCodigo(0);
                            resul.setIsSuccess(false);
                        }
                    }
                return resul;
                */
    }

    @Override
    public ResultEntity<CronogramaActividesEntity> EliminarCronogramaActividades(CronogramaActividesEntity request) {
        return null;
        /*
                ResultEntity resul=new ResultEntity();   
                CallableStatement pstm  = null;
                Connection con = null;    
                try
                { 
                     con = dataSource.getConnection();                    
                        pstm = con.prepareCall("{call pa_EliminarItemCronogramaActividad_PlanManejo (?,?,?,?,?)}");
                        pstm.setObject (1, request.getID_CRON_ACTIVIDAD(),Types.INTEGER);
                        pstm.setObject (2, request.getID_PLAN_MANEJO(),Types.INTEGER);                      
                        pstm.setObject (3, request.getACTIVIDAD(),Types.VARCHAR);
                        pstm.setObject (4, request.getANO(),Types.INTEGER);                   
                        pstm.registerOutParameter(5,Types.INTEGER);
                        pstm.execute();
                        resul.setCodigo(pstm.getInt(5));                                              
                    resul.setMessage((resul.getCodigo()>0?"Se eliminó el registro correctamente.":"No se eliminó el registro."));
                    resul.setIsSuccess((resul.getCodigo()>0?true:false));
        
                } catch (Exception e) {
                    resul.setMessage("Ocurrió un error.");
                    resul.setCodigo(0);
                    resul.setIsSuccess(false);
                }finally{
                        try {
                            pstm.close();
                            con.close();
                        } catch (Exception e) {
                            resul.setMessage("Ocurrió un error.");
                            resul.setCodigo(0);
                            resul.setIsSuccess(false);
                        }
                    }
                return resul;
                */
    }

    /***Métodos para el manejo de Cronograma Actividades usando tabla detalle*******/


    @Override
    public  ResultClassEntity RegistrarConfiguracionCronogramaActividad(CronogramaActividadDto request) {
        ResultClassEntity result=new ResultClassEntity();
        try
        {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividad_RegistrarConfiguracion");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("prefijo", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", request.getIdPlanManejo());
            sp.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
            sp.setParameter("codigoProceso", request.getCodigoProceso());
            sp.setParameter("prefijo", request.getPrefijo());
            sp.execute();

            result.setMessage("Se configuro el cronograma de actividad correctamente.");
            result.setSuccess(true);

        } catch (Exception e) {
            result.setMessage("Ocurrió un error.");
            result.setSuccess(false);
            log.error("CronogramaActividesRepositoryImpl - RegistrarConfiguracionCronogramaActividad", e.getMessage());
        }
        return result;
    }



    @Override
    public ResultEntity<CronogramaActividadEntity> RegistrarCronogramaActividad(CronogramaActividadEntity request) {
        ResultEntity<CronogramaActividadEntity> resul=new ResultEntity<CronogramaActividadEntity>();
        try
        {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividad_Registrar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("importe", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistra", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idNuevo", Integer.class, ParameterMode.INOUT);
            sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoActividad", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", request.getIdPlanManejo());
            sp.setParameter("actividad", request.getActividad());
            sp.setParameter("anio", request.getAnio());
            sp.setParameter("importe", request.getImporte());
            sp.setParameter("idUsuarioRegistra", request.getIdUsuarioRegistro());
            sp.setParameter("idNuevo", request.getIdCronogramaActividad());
            sp.setParameter("codigoProceso", request.getCodigoProceso());
            sp.setParameter("codigoActividad", request.getCodigoActividad());
            sp.execute();
            Integer id = (Integer) sp.getOutputParameterValue("idNuevo");

            if(request.getActividad().equals("") || request.getActividad()==null){
                CronogramaActividadEntity crono = new CronogramaActividadEntity();
                crono.setIdPlanManejo(request.getIdPlanManejo());
                crono.setCodigoProceso(request.getCodigoProceso());
               // crono.setIdCronogramaActividad(id);
                ResultEntity<CronogramaActividadEntity> listaCrongorama = ListarCronogramaActividad(crono);
                for(CronogramaActividadEntity cronograma : listaCrongorama.getData()){
                    RegistrarMarcaCronogramaActividadDetallePadre(cronograma);
                }

            }

            resul.setCodigo(id);
            resul.setMessage((resul.getCodigo()>0?"Se registró el Cronograma de actividades correctamente.":"Ocurrió un error"));
            resul.setIsSuccess((resul.getCodigo()>0?true:false));

        } catch (Exception e) {
            resul.setMessage("Ocurrió un error.");
            resul.setCodigo(0);
            resul.setIsSuccess(false);
            log.error("CronogramaActividesRepositoryImpl - RegistrarCronogramaActividad", e.getMessage());
        }
        return resul;
    }

    @Override
    public ResultEntity<CronogramaActividadEntity> EliminarCronogramaActividad(CronogramaActividadEntity request) {
        ResultEntity<CronogramaActividadEntity> resul=new ResultEntity<CronogramaActividadEntity>();
        try
        {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividad_Eliminar");
            sp.registerStoredProcedureParameter("idCronActividad", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idCronActividad", request.getIdCronogramaActividad());
            sp.setParameter("idUsuarioElimina", request.getIdUsuarioElimina());
            sp.execute();
            resul.setMessage("Se eliminó el registro correctamente.");
            resul.setIsSuccess(true);

        } catch (Exception e) {
            resul.setMessage("Ocurrió un error.");
            resul.setIsSuccess(false);
            log.error("CronogramaActividesRepositoryImpl - EliminarCronogramaActividad", e.getMessage());
        }
        return resul;
    }

    @Override
    public ResultClassEntity ListarPorFiltroCronogramaActividad(CronogramaActividadDto request) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CronogramaActividad_ListarPorFiltro");
            processStored.registerStoredProcedureParameter("idCronogramaActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idCronogramaActividad", request.getIdCronogramaActividad());
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("codigoProceso", request.getCodigoProceso());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<CronogramaActividadDto> lstdocs = new ArrayList<CronogramaActividadDto>();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    CronogramaActividadDto obj = new CronogramaActividadDto();
                    obj.setIdCronogramaActividad((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setActividad((String) row[2]);
                    obj.setCodigoProceso((String) row[3]);
                    obj.setCodigoActividad((String) row[4]);
                    obj.setGrupo((String) row[5]);
                    obj.setAccion((Integer) row[6]);
                    obj.setCantidadAnio((Integer) row[7]);
                    ResultEntity<CronogramaActividadDetalleEntity> detalle = new ResultEntity<>();
                    detalle = listarCronogramaDetalleCabecera(obj.getIdCronogramaActividad());
                    obj.setDetalle(detalle.getData());
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setSuccess(true);
            result.setMessage("Se listaron los cronogramas.");
            return result;
        } catch (Exception e) {
            log.error("ContratoActividades - ListarCronogramaActividad Ocurrió un error" +e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [07-09-2021]
     * @modificado:
     * @descripción: {lista el cronograma con su detalle}
     * @param:
     */
    @Override
    public ResultEntity<CronogramaActividadEntity> ListarCronogramaActividad(CronogramaActividadEntity request) {
        ResultEntity<CronogramaActividadEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CronogramaActividad_ListarCabecera");
            processStored.registerStoredProcedureParameter("idCronogramaActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idCronogramaActividad", request.getIdCronogramaActividad());
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("codigoProceso", request.getCodigoProceso());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<CronogramaActividadEntity> lstdocs = new ArrayList<>();
            
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    CronogramaActividadEntity obj = new CronogramaActividadEntity();
                    obj.setIdCronogramaActividad((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setActividad((String) row[2]);
                    obj.setCodigoProceso((String) row[3]);
                    obj.setCodigoActividad((String) row[4]);
                    ResultEntity<CronogramaActividadDetalleEntity> detalle = new ResultEntity<>();

                    detalle = listarCronogramaDetalleCabecera(obj.getIdCronogramaActividad());
                    obj.setDetalle(detalle.getData());
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los cronogramas.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [20-01-2022]
     * @modificado:
     * @descripción: {lista el cronograma con su detalle Titular}
     * @param:
     */
    @Override
    public ResultEntity<CronogramaActividadEntity> ListarCronogramaActividadTitular(CronogramaActividadEntity request) {
        ResultEntity<CronogramaActividadEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CronogramaActividad_ListarCabecera_Titular");
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumento", request.getTipoDocumento());
            processStored.setParameter("nroDocumento", request.getNroDocumento());
            processStored.setParameter("codigoProcesoTitular", request.getCodigoProcesoTitular());
            processStored.setParameter("codigoProceso", request.getCodigoProceso());
            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<CronogramaActividadEntity> lstdocs = new ArrayList<>();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    CronogramaActividadEntity obj = new CronogramaActividadEntity();
                    obj.setIdCronogramaActividad((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setActividad((String) row[2]);
                    obj.setCodigoProceso((String) row[3]);
                    obj.setCodigoActividad((String) row[4]);
                    ResultEntity<CronogramaActividadDetalleEntity> detalle = new ResultEntity<>();
                    detalle = listarCronogramaDetalleCabecera(obj.getIdCronogramaActividad());
                    obj.setDetalle(detalle.getData());
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los cronogramas.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }



    /**
     * @autor: JaquelineDB [07-09-2021]
     * @modificado:
     * @descripción: {Elimina el detalle del cronograma}
     * @param:
     */
    @Override
    public ResultClassEntity EliminarCronogramaActividadDetalle(CronogramaActividadEntity filtro) {
        ResultClassEntity resul=new ResultClassEntity<>();
        try
        {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividadDetalle_Eliminar");
            sp.registerStoredProcedureParameter("idCronActividadDetalle", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idCronActividadDetalle", filtro.getIdCronogramaActividadDetalle());
            sp.setParameter("idUsuarioElimina", filtro.getIdUsuarioElimina());
            sp.execute();
            resul.setMessage("Cronograma eliminado");
            resul.setSuccess(true);

        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setSuccess(false);
            log.error("CronogramaActividesRepositoryImpl - EliminarCronogramaActividadDetalle", e.getMessage());
        }
        return resul;
    }

    private ResultEntity<CronogramaActividadDetalleEntity> listarCronogramaDetalleCabecera(Integer IdActividad){
        ResultEntity<CronogramaActividadDetalleEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_CronogramaActividad_ListarDetalle");
            processStored.registerStoredProcedureParameter("idCronogramaActividad", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idCronogramaActividad", IdActividad);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<CronogramaActividadDetalleEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    CronogramaActividadDetalleEntity obj = new CronogramaActividadDetalleEntity();
                    obj.setIdCronActividad((Integer) row[0]);
                    obj.setIdCronogramaActividadDetalle((Integer) row[1]);
                    obj.setAnio((Integer) row[2]);
                    obj.setMes((Integer) row[3]);
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los cronogramas.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultEntity<CronogramaActividadEntity> RegistrarMarcaCronogramaActividadDetalle(CronogramaActividadEntity request) {
        ResultEntity<CronogramaActividadEntity> resul=new ResultEntity<CronogramaActividadEntity>();
        try
        {

            if(request.getMesesAnios()!=null){
               String[] mesesanios = request.getMesesAnios().split(",");
                String strMes=null;
                String strAnio=null;
                Integer intMes=null;
                Integer intAnio=null;
               for (String mesanio : mesesanios) {
                String[] lstmesanio = mesanio.split("-");
                if(lstmesanio.length>=1){
                    strMes=lstmesanio[1];
                    strAnio=lstmesanio[0];
                }   
                intMes=strMes!=null?Integer.parseInt(strMes):null;
                intAnio=strAnio!=null?Integer.parseInt(strAnio):null;
                StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividadDetalle_RegistrarMarca");
                sp.registerStoredProcedureParameter("idCronActividadDetalle", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idCronActividad", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
                   sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idCronActividadDetalle", request.getIdCronogramaActividadDetalle());
                sp.setParameter("idCronActividad", request.getIdCronogramaActividad());
                sp.setParameter("anio", intAnio);
                sp.setParameter("mes", intMes);
                sp.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
                sp.setParameter("codigoProceso", request.getCodigoProceso());
                sp.setParameter("idPlanManejo", request.getIdPlanManejo());
                sp.execute();
               }
            }else{
                StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividadDetalle_RegistrarMarca");
                sp.registerStoredProcedureParameter("idCronActividadDetalle", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idCronActividad", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idCronActividadDetalle", request.getIdCronogramaActividadDetalle());
                sp.setParameter("idCronActividad", request.getIdCronogramaActividad());
                sp.setParameter("anio", request.getAnio());
                sp.setParameter("mes", request.getMes());
                sp.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
                sp.setParameter("codigoProceso", request.getCodigoProceso());
                sp.setParameter("idPlanManejo", request.getIdPlanManejo());
                sp.execute();
            }
            
                resul.setMessage("Detalle cronograma registrado");
                resul.setIsSuccess(true);    
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setCodigo(0);
            resul.setIsSuccess(false);
            log.error("CronogramaActividesRepositoryImpl - RegistrarMarcaCronogramaActividadDetalle", e.getMessage());
        }
        return resul;
    }

    public ResultEntity<CronogramaActividadEntity> RegistrarMarcaCronogramaActividadDetallePadre(CronogramaActividadEntity request) {
        ResultEntity<CronogramaActividadEntity> resul=new ResultEntity<CronogramaActividadEntity>();
        try
        {

            if(request.getMesesAnios()!=null){
                String[] mesesanios = request.getMesesAnios().split(",");
                String strMes=null;
                String strAnio=null;
                Integer intMes=null;
                Integer intAnio=null;
                for (String mesanio : mesesanios) {
                    String[] lstmesanio = mesanio.split("-");
                    if(lstmesanio.length>=1){
                        strMes=lstmesanio[1];
                        strAnio=lstmesanio[0];
                    }
                    intMes=strMes!=null?Integer.parseInt(strMes):null;
                    intAnio=strAnio!=null?Integer.parseInt(strAnio):null;
                    StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividadDetalle_RegistrarMarca");
                    sp.registerStoredProcedureParameter("idCronActividadDetalle", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("idCronActividad", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(sp);
                    sp.setParameter("idCronActividadDetalle", request.getIdCronogramaActividadDetalle());
                    sp.setParameter("idCronActividad", request.getIdCronogramaActividad());
                    sp.setParameter("anio", intAnio);
                    sp.setParameter("mes", intMes);
                    sp.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
                    sp.setParameter("codigoProceso", request.getCodigoProceso());
                    sp.setParameter("idPlanManejo", request.getIdPlanManejo());
                    sp.execute();
                }
            }else{
                StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_CronogramaActividadDetalle_RegistrarMarca");
                sp.registerStoredProcedureParameter("idCronActividadDetalle", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idCronActividad", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idCronActividadDetalle", request.getIdCronogramaActividadDetalle());
                sp.setParameter("idCronActividad", request.getIdCronogramaActividad());
                sp.setParameter("anio", request.getAnio());
                sp.setParameter("mes", request.getMes());
                sp.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
                sp.setParameter("codigoProceso", request.getCodigoProceso());
                sp.setParameter("idPlanManejo", request.getIdPlanManejo());
                sp.execute();
            }

            resul.setMessage("Detalle cronograma registrado");
            resul.setIsSuccess(true);
        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setCodigo(0);
            resul.setIsSuccess(false);
            log.error("CronogramaActividesRepositoryImpl - RegistrarMarcaCronogramaActividadDetalle", e.getMessage());
        }
        return resul;
    }

}
