package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.AnexoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;


public interface AnexoAdjuntoRepository {
    ResultClassEntity MarcarParaAmpliacionAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception;

    List<AnexoAdjuntoEntity> ListarAnexoAdjunto(AnexoAdjuntoEntity anexoAdjuntoEntity) throws Exception;

}
