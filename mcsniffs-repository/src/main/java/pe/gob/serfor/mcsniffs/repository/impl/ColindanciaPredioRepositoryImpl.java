package pe.gob.serfor.mcsniffs.repository.impl;



import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Colindancia.ColindanciaPredioDto;
import pe.gob.serfor.mcsniffs.repository.ColindanciaPredioRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ColindanciaPredioRepositoryImpl extends JdbcDaoSupport implements ColindanciaPredioRepository{
    
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ColindanciaPredioRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registrarColindanciaPredio(ColindanciaPredioDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ColindanciaPredio_Registrar");
            processStored.registerStoredProcedureParameter("idColindancia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoColindancia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionColindante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionLimite", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idColindancia", dto.getIdColindancia());
            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("codTipoColindancia", dto.getCodTipoColindancia());
            processStored.setParameter("descripcionColindante", dto.getDescripcionColindante());
            processStored.setParameter("descripcionLimite", dto.getDescripcionLimite());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró Colindancia Predio correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public List<ColindanciaPredioDto> listarColindanciaPredio(ColindanciaPredioDto dto) throws Exception {

        List<ColindanciaPredioDto> lista = new ArrayList<>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ColindanciaPredio_Listar");
            processStored.registerStoredProcedureParameter("idColindancia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idColindancia",dto.getIdColindancia());
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                ColindanciaPredioDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new ColindanciaPredioDto();
                        temp.setIdColindancia(row[0]==null?null:(Integer) row[0]);
                        temp.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                        temp.setCodTipoColindancia(row[2]==null?null:(String) row[2]);
                        temp.setDescripcionColindante(row[3]==null?null:(String) row[3]);
                        temp.setDescripcionLimite(row[4]==null?null:(String) row[4]);
                        temp.setTipoColindancia(row[5]==null?null:(String) row[5]);

                        lista.add(temp);
                    }
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("listarColindanciaPredio", e.getMessage());
            throw e;
        }
    }


}
