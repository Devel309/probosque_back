package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.EnviarConsultaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.repository.EnviarConsultaRepository;
import pe.gob.serfor.mcsniffs.repository.UnidadAprovechamientoRepository;
// 

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;

@Repository
public class EnviarConsultaRepositoryImpl extends JdbcDaoSupport implements EnviarConsultaRepository {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {repositorio de la tabla Enviar consulta}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    UnidadAprovechamientoRepository uarepo;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EnviarConsultaRepositoryImpl.class);

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Envia la consulta a una entidad}
     * @param:
     */

    @Override
    public ResultClassEntity EnviarConsulta(EnviarConsultaEntity obj) {

        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        try {
            Date date = new Date();
            int salida = -1;
            Connection con = null;
            PreparedStatement pstm  = null;
            ResultClassEntity<UnidadAprovechamientoEntity> ua= uarepo.obtenerUnidadAprovechamiento(obj.getIdUnidadAprovechamiento());
            //1 Conectar a la base de  datos
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_EnviarConsulta_insertar (?,?,?,?,?,?,?)}");
            pstm.setInt(1, ua.getData().getIdUnidadAprovechamiento());
            pstm.setInt(2, obj.getIdConsultaEntidad());
            pstm.setString(3, obj.getAsunto());
            pstm.setString(4, obj.getDescripcion());
            pstm.setInt(5, obj.getIdUsuarioRemite());
            pstm.setInt(6, obj.getIdUsuarioRegistro());
            pstm.setTimestamp(7, new java.sql.Timestamp(date.getTime()));

            //3 envia el sql y se recibe la cantidad de registrados
            salida = pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if(rs.next())
            {
                salida = rs.getInt(1);
            }
                result.setCodigo(salida);
                result.setSuccess(true);
                result.setMessage("Se envio la consulta.");
                pstm.close();
                con.close();
            return result;
        } catch (Exception e) {
            log.error("EnviarConsultaRepositoryImpl - EnviarConsulta", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }

         */
    }

}
