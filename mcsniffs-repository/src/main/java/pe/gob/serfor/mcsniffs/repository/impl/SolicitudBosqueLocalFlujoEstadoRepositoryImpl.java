package pe.gob.serfor.mcsniffs.repository.impl;

 

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFlujoEstadoDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalFlujoEstadoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudBosqueLocalFlujoEstadoRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalFlujoEstadoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(SolicitudBosqueLocalFlujoEstadoRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }
 

    @Override
    public ResultClassEntity registrarSolicitudBosqueLocalEvaluacion(SolicitudBosqueLocalFlujoEstadoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_FlujoEstado_Registrar");
            processStored.registerStoredProcedureParameter("idFlujoEstado", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitud", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idFlujoEstado", dto.getIdSolBosqueLocal());
            processStored.setParameter("idSolBosqueLocal", dto.getIdSolBosqueLocal());
            processStored.setParameter("estadoSolicitud", dto.getEstado());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se Ejecuto el Procedimiento correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    
}
