package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.xpath.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEstudioTecnicoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalEstudioTecnicoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudBosqueLocalEstudioTecnicoRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalEstudioTecnicoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalEstudioTecnicoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalEstudioTecnicoEntity> lista = new ArrayList<SolicitudBosqueLocalEstudioTecnicoEntity>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_EstudioTecnico_Listar");

            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocal",obj.getIdSolBosqueLocal());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                SolicitudBosqueLocalEstudioTecnicoEntity c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new SolicitudBosqueLocalEstudioTecnicoEntity();

                        c.setIdSolBosqueLocalEstudioTecnico(row[0]==null?null:(Integer) row[0]);
                        c.setIdRegente(row[1]==null?null:(Integer) row[1]);
                        c.setNumeroDocumentoRegente(row[2]==null?null:(String) row[2]);
                        c.setNombreRegente(row[3]==null?null:(String) row[3]);
                        c.setLicenciaRegente(row[4]==null?null:(String) row[4]);
                        c.setProcedimientoProyectado(row[5]==null?null:(String) row[5]);
                        c.setProcedimientoEmision(row[6]==null?null:(String) row[6]);
                        c.setProcedimientoCobro(row[7]==null?null:(String) row[7]);
                        c.setActividadProteccion(row[8]==null?null:(String) row[8]);
                        c.setIdUsuarioRegistro(row[9]==null?null:(Integer) row[9]);
                        c.setEnvioNotificacion(row[10]==null?null:(Boolean) row[10]);
                        c.setNotificacionTexto(row[11]==null?null:(String) row[11]);

                        lista.add(c);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar solicitud bosque local de estudio técnico correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocalEstudioTecnico", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar solicitud bosque de estudio técnico local");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudBosqueLocalEstudioTecnico(SolicitudBosqueLocalEstudioTecnicoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_EstudioTecnico_Actualizar");
            processStored.registerStoredProcedureParameter("idSolBosqueLocalEstudioTecnico", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumentoRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("licenciaRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("procedimientoProyectado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("procedimientoEmision", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("procedimientoCobro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividadProteccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("envioNotificacion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notificacionTexto", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idSolBosqueLocalEstudioTecnico", obj.getIdSolBosqueLocalEstudioTecnico());
            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("idRegente", obj.getIdRegente());
            processStored.setParameter("numeroDocumentoRegente", obj.getNumeroDocumentoRegente());
            processStored.setParameter("nombreRegente", obj.getNombreRegente());
            processStored.setParameter("licenciaRegente", obj.getLicenciaRegente());
            processStored.setParameter("procedimientoProyectado", obj.getProcedimientoProyectado());
            processStored.setParameter("procedimientoEmision", obj.getProcedimientoEmision());
            processStored.setParameter("procedimientoCobro", obj.getProcedimientoCobro());
            processStored.setParameter("actividadProteccion", obj.getActividadProteccion());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            processStored.setParameter("envioNotificacion", obj.getEnvioNotificacion());
            processStored.setParameter("notificacionTexto", obj.getNotificacionTexto());

            processStored.execute();


            result.setSuccess(true);
            result.setData(obj);
            result.setMessage("Se actualizó la actividad de la solicitud bosque Local de estudio técnico correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se actualizó la actividad de la solicitud Bosque Local de estudio técnico");
            result.setInnerException(e.getMessage());
            return  result;
        }
    }

}