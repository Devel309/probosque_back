package pe.gob.serfor.mcsniffs.repository.impl;

 
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.repository.LibroOperacionesRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LibroOperacionesRepositoryImpl extends JdbcDaoSupport implements LibroOperacionesRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(LibroOperacionesRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    

    @Override
    public ResultClassEntity registrarLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_Registrar");
            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("nroRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoEvidencia", Integer.class, ParameterMode.IN);            
            processStored.registerStoredProcedureParameter("estadoLO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("permisoOtorgado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            
            processStored.setParameter("nroRegistro", dto.getNroRegistro());
            processStored.setParameter("idArchivoEvidencia", dto.getIdArchivoEvidencia());
            processStored.setParameter("estadoLO", dto.getEstadoLO());
            processStored.setParameter("permisoOtorgado", dto.getPermisoOtorgado());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();
 
            result.setSuccess(true);
            Integer idLibroOperaciones = (Integer) processStored.getOutputParameterValue("idLibroOperaciones");
            dto.setIdLibroOperaciones(idLibroOperaciones);
            result.setCodigo(idLibroOperaciones);
            result.setData(dto);
            result.setMessage("Se registró Libro Operaciones correctamente.");

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_Actualizar");
            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("nroRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivoEvidencia", Integer.class, ParameterMode.IN);            
            processStored.registerStoredProcedureParameter("estadoLO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("permisoOtorgado", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idLibroOperaciones", dto.getIdLibroOperaciones());
            processStored.setParameter("nroRegistro", dto.getNroRegistro());
            processStored.setParameter("idArchivoEvidencia", dto.getIdArchivoEvidencia());
            processStored.setParameter("estadoLO", dto.getEstadoLO());
            processStored.setParameter("permisoOtorgado", dto.getPermisoOtorgado());
            processStored.setParameter("idUsuarioModificacion", dto.getIdUsuarioModificacion());
            processStored.execute();
 
            result.setSuccess(true);
            Integer idLibroOperaciones = (Integer) processStored.getOutputParameterValue("idLibroOperaciones");
            dto.setIdLibroOperaciones(idLibroOperaciones);
            result.setData(dto);
            result.setMessage("Se actualizó el Libro Operaciones correctamente.");

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_Eliminar");
            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idLibroOperaciones", dto.getIdLibroOperaciones());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
 
            result.setSuccess(true);
            result.setCodigo(dto.getIdLibroOperaciones());
            result.setMessage("Se eliminó el Libro Operaciones correctamente.");

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity validarNroRegistroLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_ValidarNumeroRegistro");
            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("disposicionNroRegistro", Boolean.class, ParameterMode.OUT);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idLibroOperaciones", dto.getIdLibroOperaciones());
            processStored.setParameter("nroRegistro", dto.getNroRegistro());

            processStored.execute();

            Boolean disposicionNroRegistro =(Boolean) processStored.getOutputParameterValue("disposicionNroRegistro");

            result.setSuccess(true);
            result.setValidateBusiness(!disposicionNroRegistro);
            result.setMessage(disposicionNroRegistro?"Número de Registro Ocupado.": "Número de Registro Desocupado." );

            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setValidateBusiness(true);
            result.setMessage("Ocurrio un error al validar el número de registro en Libro Operaciones.");
            return  result;
        }
    }

    public ResultClassEntity listarLibroOperaciones(LibroOperacionesDto dto){
        ResultClassEntity result = new ResultClassEntity();
        List<LibroOperacionesDto> lista = new ArrayList<LibroOperacionesDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_Listar");

            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoLO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idLibroOperaciones",dto.getIdLibroOperaciones());
            processStored.setParameter("solicitante",dto.getNombreSolicitanteUnico());
            processStored.setParameter("numeroDocumento",dto.getNumeroDocumento());
            processStored.setParameter("nroRegistro",dto.getNroRegistro());
            processStored.setParameter("estadoLO",dto.getEstadoLO());
            processStored.setParameter("pageNum",dto.getPageNum());
            processStored.setParameter("pageSize",dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                LibroOperacionesDto c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new LibroOperacionesDto();

                        c.setIdLibroOperaciones(row[0]==null?null:(Integer) row[0]);
                        c.setNroRegistro(row[1]==null?null:(String) row[1]);
                        c.setIdArchivoEvidencia(row[2]==null?null:(Integer) row[2]);
                        c.setEstadoLO(row[3]==null?null:(String) row[3]);
                        c.setPermisoOtorgado(row[4]==null?null:(Boolean) row[4]);
                        c.setNumeroDocumento(row[6]==null?null:(String) row[6]);
                        c.setTipoDocumento(row[7]==null?null:(String) row[7]);
                        c.setTipoPersona(row[8]==null?null:(String) row[8]);
                        c.setDescripcionEstadoLO(row[9]==null?null:(String) row[9]);
                        c.setNombreSolicitanteUnico(row[10]==null?null:(String) row[10]);
                        c.setFechaSolicitud(row[11]==null?null:(String) row[11]);
                        lista.add(c);
                        result.setTotalRecord((Integer) row[5]);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Libro Operaciones correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Libro Operaciones");
            return  result;
        }
    }

    @Override
    public ResultClassEntity listarLibroOperacionesPlanManejo(PlanManejoEntity planManejoEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_PlanManejo_Listar");
            sp.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoPlan", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoProceso", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(sp);
            sp.setParameter("idLibroOperaciones", planManejoEntity.getIdLibroOperaciones());
            sp.setParameter("codigoTipoPlan", planManejoEntity.getCodigoTipoPlan());
            sp.setParameter("idTipoProceso", planManejoEntity.getIdTipoProceso());
            sp.setParameter("idPlanManejo", planManejoEntity.getIdPlanManejo());
            sp.setParameter("pageNum", planManejoEntity.getPageNum());
            sp.setParameter("pageSize", planManejoEntity.getPageSize());
            sp.execute();
            List<Object[]> spResultDet = sp.getResultList();

            List<PlanManejoEntity> resultDet = null;
            if (spResultDet!= null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                PlanManejoEntity temp = null;
                for (Object[] item: spResultDet) {
                    temp = new PlanManejoEntity();
                    temp.setIdPlanManejo(item[0]==null?null:(Integer) item[0]);
                    temp.setTipoPlanDesripcion((String) item[1]);
                    temp.setIdTipoPlan((Integer) item[2]);
                    temp.setIdLibroOperacionesPlanManejo((Integer) item[3]);
                    result.setTotalRecord((Integer) item[4]);
                    resultDet.add(temp);
                }
            }

            result.setData(resultDet);
            result.setSuccess(true);
            result.setMessage("Se listo Plan de Manejo correctamente.");
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    public ResultClassEntity listarEvaluacionLibroOperaciones(LibroOperacionesDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<LibroOperacionesDto> lista = new ArrayList<LibroOperacionesDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("LibroOperaciones.pa_LibroOperaciones_EvaluacionListar");

            processStored.registerStoredProcedureParameter("idLibroOperaciones", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("solicitante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoLO", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoTH", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigente", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idLibroOperaciones",dto.getIdLibroOperaciones());
            processStored.setParameter("solicitante",dto.getNombreSolicitanteUnico());
            processStored.setParameter("numeroDocumento",dto.getNumeroDocumento());
            processStored.setParameter("nroRegistro",dto.getNroRegistro());
            processStored.setParameter("estadoLO",dto.getEstadoLO());
            processStored.setParameter("codigoTH",dto.getCodigoTH());
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
            processStored.setParameter("idResolucion",dto.getIdResolucion());
            processStored.setParameter("vigente",dto.getVigente());
            processStored.setParameter("pageNum",dto.getPageNum());
            processStored.setParameter("pageSize",dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                LibroOperacionesDto c = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        c = new LibroOperacionesDto();

                        c.setIdLibroOperaciones(row[0]==null?null:(Integer) row[0]);
                        c.setNroRegistro(row[1]==null?null:(String) row[1]);
                        c.setIdArchivoEvidencia(row[2]==null?null:(Integer) row[2]);
                        c.setEstadoLO(row[3]==null?null:(String) row[3]);
                        c.setPermisoOtorgado(row[4]==null?null:(Boolean) row[4]);
                        c.setNumeroDocumento(row[6]==null?null:(String) row[6]);
                        c.setTipoDocumento(row[7]==null?null:(String) row[7]);
                        c.setTipoPersona(row[8]==null?null:(String) row[8]);
                        c.setDescripcionEstadoLO(row[9]==null?null:(String) row[9]);
                        c.setNombreSolicitanteUnico(row[10]==null?null:(String) row[10]);
                        c.setFechaSolicitud(row[11]==null?null:(String) row[11]);
                        c.setVigente(row[12]==null?null:(Boolean) row[12]);
                        lista.add(c);
                        result.setTotalRecord((Integer) row[5]);

                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se realizó la acción Listar Libro Operaciones correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocal", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo realizar la acción Listar Libro Operaciones");
            return  result;
        }
    }

}
