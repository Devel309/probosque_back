package pe.gob.serfor.mcsniffs.repository;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoRespuestaEntity;
import pe.gob.serfor.mcsniffs.entity.ListarProcesoPosEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;

public interface ProcesoPostulacionRepository {
    ResultClassEntity guardaProcesoPostulacion(ProcesoPostulacionResquestEntity proceso);
    ResultEntity<ProcesoPostulacionEntity>listarProcesoPostulacion(ListarProcesoPosEntity filtro);
    ResultClassEntity<DocumentoRespuestaEntity> autorizarPublicacion(MultipartFile file, String CodigoDoc,String NombreArchivo,Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto);
    ResultEntity<DocumentoRespuestaEntity> obtenerAutorizacion(AnexoRequestEntity filtro);
    ResultClassEntity<DocumentoRespuestaEntity> AdjuntarResolucionDenegada(MultipartFile file,String CodigoDoc,String NombreArchivo,Integer IdProcesoPostulacion, Integer IdSolicitante, Integer IdUsuarioRegistro, Integer IdTipoDocumento,Integer IdPostulacionPFDM, Integer idDocumentoAdjunto);
    ResultClassEntity actualizarEstadoProcesoPostulacion(Integer idProcesoPostulacion, Integer idEstatus, Integer idUsuarioModificacion, Date fechaPublicacion);
    ResultClassEntity actualizarObservacionARFFSProcesoPostulacion(ProcesoPostulacionDto obj);
    ResultClassEntity actualizarResolucionProcesoPostulacion(ProcesoPostulacionDto obj);
    ResultClassEntity obtenerProcesoPostulacion(ProcesoPostulacionDto obj);
   // ResultClassEntity obtenerInformacionProcesoPostulacion (ProcesoPostulacionDto obj);
    ResultEntity obtenerInformacionProcesoPostulacion(ProcesoPostulacionDto request);

    ResultClassEntity actualizarProcesoPostulacion(ProcesoPostulacionDto obj);
    ResultClassEntity tieneObservacion(ProcesoPostulacionDto obj);
    ResultClassEntity obtenerProcesoPostulacionValidarRequisito(ProcesoPostulacionDto obj);
    ResultClassEntity actualizarEnvioPrueba(ProcesoPostulacionDto obj);
    Integer validarPublicacion(ProcesoPostulacionEntity dto) throws Exception;
    ResultClassEntity ImpugnarGanadorProcesoPostulacion(ProcesoPostulacionDto param);
    ResultClassEntity validarPublicacionId(ProcesoPostulacionDto obj);
}
