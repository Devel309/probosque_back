package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionResumidoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionResumidoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDetalleDto;

public interface EvaluacionDetalleRepository {
    List<EvaluacionDetalleDto> listarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception;
    ResultClassEntity registrarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacionDetalle(EvaluacionDetalleDto dto) throws Exception;
    List<EvaluacionResumidoDetalleDto> listarEvaluacionDetalleResumido(EvaluacionResumidoDto dto) throws Exception;

    // Permisos forestales
    ResultClassEntity registrarEvaluacionPermisoForestalDetalle(EvaluacionPermisoForestalDetalleDto dto) throws Exception;
    List<EvaluacionPermisoForestalDetalleDto> listarEvaluacionPermisoForestaDetalle(EvaluacionPermisoForestalDetalleDto dto) throws Exception;
    ResultClassEntity eliminarEvaluacionPermisoForestalDetalle(EvaluacionPermisoForestalDetalleDto dto) throws Exception;
}
