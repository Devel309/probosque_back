package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDto;
import pe.gob.serfor.mcsniffs.repository.MedidaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class MedidaRepositoryImpl extends JdbcDaoSupport implements MedidaRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(MedidaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<MedidaDto> listarMedida(MedidaDto dto) throws Exception {
       
        List<MedidaDto> lista = new ArrayList<MedidaDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Medida_Listar");
            processStored.registerStoredProcedureParameter("idMedida", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMedida", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idMedida",dto.getIdMedida());
            processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
            processStored.setParameter("codigoMedida",dto.getCodigoMedida());
            processStored.setParameter("P_PAGENUM", dto.getPageNum());
            processStored.setParameter("P_PAGESIZE", dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                MedidaDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new MedidaDto();
                        temp.setIdMedida(row[0]==null?null:(Integer) row[0]);
                        temp.setIdPlanManejo(row[1]==null?null:(Integer) row[1]);
                        temp.setCodigoMedida(row[2]==null?null:(String) row[2]);
                        temp.setNumeroContrato(row[3]==null?null:(String) row[3]);
                        temp.setCumplimiento(row[4]==null?null:(String) row[4]);
                        temp.setIdArchivo(row[5]==null?null:(Integer) row[5]);
                        temp.setFechaCumplimiento(row[6]==null?null:(Date) row[6]);
                        temp.setProceso(row[7]==null?null:(Integer) row[7]);
                        temp.setDetalle(row[8]==null?null:(String) row[8]);
                        temp.setObservacion(row[9]==null?null:(String) row[9]);
                        temp.setDescripcion(row[10]==null?null:(String) row[10]);
                        temp.setConforme(row[11]==null?null:(String) row[11]);
                        temp.setEstado(row[12]==null?null:(String) row[12]);
          
                        lista.add(temp);
                    }
                }
            }
 
            return lista;
        } catch (Exception e) {
            log.error("listarProcedimientoAdministrativo", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity registrarMedida(MedidaDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Medida_Registrar");
            processStored.registerStoredProcedureParameter("idMedida", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMedida", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cumplimiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaCumplimiento", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("proceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMedida", dto.getIdMedida());
            processStored.setParameter("idPlanManejo", dto.getIdPlanManejo());
            processStored.setParameter("codigoMedida", dto.getCodigoMedida());
            processStored.setParameter("numeroContrato", dto.getNumeroContrato());
            processStored.setParameter("cumplimiento", dto.getCumplimiento());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("fechaCumplimiento", dto.getFechaCumplimiento());
            processStored.setParameter("proceso", dto.getProceso());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("conforme", dto.getConforme());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());
            processStored.execute();

            Integer id = (Integer) processStored.getOutputParameterValue("idMedida");
            dto.setIdMedida(id);

            result.setSuccess(true);
            result.setMessage("Se registró la medida correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity eliminarMedida(MedidaDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Medida_Eliminar");
            processStored.registerStoredProcedureParameter("idMedida", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMedida", dto.getIdMedida());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó medida correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }    
 
}
