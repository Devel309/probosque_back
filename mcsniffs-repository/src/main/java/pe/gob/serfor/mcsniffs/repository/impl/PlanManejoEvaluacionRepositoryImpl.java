package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.*;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.NotificacionDTO;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.PlanManejoEvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class PlanManejoEvaluacionRepositoryImpl extends JdbcDaoSupport implements PlanManejoEvaluacionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(PlanManejoEvaluacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<PlanManejoEvaluacionDto> listarPlanManejoEvaluacion(PlanManejoEvaluacionDto param) throws Exception {
        List<PlanManejoEvaluacionDto> lista = new ArrayList<PlanManejoEvaluacionDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejoEval", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoPlanManejoEval", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("TitularRegente", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Timestamp.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);

            setStoreProcedureEnableNullParameters(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idPlanManejoEval", param.getIdPlanManejoEval());
            processStored.setParameter("estadoPlanManejoEval", param.getEstadoPlanManejoEvaluacion());
            processStored.setParameter("TitularRegente", param.getTitularRegente());
            processStored.setParameter("fechaPresentacion", param.getFechaPresentacion());
            processStored.setParameter("perfil", param.getPerfil());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        PlanManejoEvaluacionDto temp = new PlanManejoEvaluacionDto();
                        temp.setIdPlanManejoEval((Integer) row[0]);
                        temp.setIdPlanManejo((Integer) row[1]);
                        temp.setIdTitularRegente((Integer) row[2]);
                        temp.setFechaPresentacion((Timestamp) row[3]);
                        temp.setEstadoPlanManejoEvaluacion((String) row[4]);
                        temp.setFechaEstadoEvaluacion((Timestamp) row[5]);
                        temp.setDescEstadoPlanManejoEvaluacion((String) row[6]);
                        temp.setEstado((String) row[7]);
                        temp.setTotalRegistro((Integer) row[8]);
                        temp.setTitularRegente((String) row[9]);
                        temp.setDescEstadoPlanManejo((String) row[10]);
                        temp.setEstadoPlanManejo((String) row[11]);
                        temp.setMaximoObs((Integer) row[12]);
                        temp.setIdUsuarioRegistro((Integer) row[13]);
                        temp.setIdMincul((Integer) row[14]);
                        temp.setIdAna((Integer) row[15]);
                        temp.setIdSernap((Integer) row[16]);
                        temp.setIdEvaluacionCampo((Integer) row[17]);
                        temp.setNotificacion((Boolean) row[18]);
                        temp.setFechaNotificacion((Date) row[19]);
                        temp.setFechaAprobacion((Date) row[20]);
                        temp.setIdResolucion((Integer) row[21]);
                        temp.setCodigoTituloHabilitante((String) row[22]);
                        temp.setFechaInicioVigencia((Date) row[23]);
                        temp.setFechaFinVigencia((Date) row[24]);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarRecursoForestal", e.getMessage());
            throw e;
        }
    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

    @Override
    public ResultClassEntity ListarPlanManejoEvaluacionLineamiento(PlanManejoEvaluacionDetalleDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacionLineamiento_Listar");
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipoParametro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());
            sp.setParameter("idTipoParametro", param.getIdTipoParametro());
            sp.execute();
            List<PlanManejoEvaluacionDetalleDto> list = new ArrayList<PlanManejoEvaluacionDetalleDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    PlanManejoEvaluacionDetalleDto obj = new PlanManejoEvaluacionDetalleDto();
                    obj.setIdPlanManejoEvalDet((Integer) row[0]);
                    obj.setCodigoTipo((String) row[1]);
                    obj.setDescripcion((String) row[2]);
                    obj.setConforme((Boolean) row[3]);
                    obj.setObservacion((String) row[4]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity obtenerUltimaEvaluacion(Integer idPlanManejo) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_ObtenerUltimaEvaluacion");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            //SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.execute();

            Object idPlanManejoEvaluacion = sp.getSingleResult();
            Integer retornoIdentitie = (Integer) idPlanManejoEvaluacion;

            if (idPlanManejoEvaluacion!= null){
                result.setCodigo(retornoIdentitie);
            }

            // result.setData(result);
            result.setMessage("Se listo ");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            result.setSuccess(false);
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarEstadoPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_ActualizarEstado");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoPlanManejoEvaluacion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", planManejoEvaluacionDto.getIdPlanManejoEval());
            processStored.setParameter("estadoPlanManejoEvaluacion", planManejoEvaluacionDto.getEstadoPlanManejoEvaluacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó el estado del plan manejo evaluación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarIdSolicitudOpcionPlanManejoEvaluacion(PlanManejoEvalSolicitudOpinionDto planManejoEvalSolicitudOpinionDto) throws Exception {

        ResultClassEntity result = new ResultClassEntity();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Actualizar_Solicitud_Opinion");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudOpinion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoSolicitud", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", planManejoEvalSolicitudOpinionDto.getIdPlanManejoEvaluacion());
            processStored.setParameter("idSolicitudOpinion", planManejoEvalSolicitudOpinionDto.getIdSolicitudOpinion());
            processStored.setParameter("tipoSolicitud", planManejoEvalSolicitudOpinionDto.getTipoSolicitud());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó la solicitud opinión al plan manejo evaluación correctamente.");
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

    @Override
    public ResultClassEntity registrarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionIterDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Registrar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTitularRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoPlanManejoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEstadoEvaluacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejo", planManejoEvaluacionIterDto.getIdPlanManejo());
            processStored.setParameter("idTitularRegente", planManejoEvaluacionIterDto.getIdTitularRegente());
            processStored.setParameter("fechaPresentacion", planManejoEvaluacionIterDto.getFechaPresentacion());
            processStored.setParameter("estadoPlanManejoEvaluacion", planManejoEvaluacionIterDto.getEstadoPlanManejoEvaluacion());
            processStored.setParameter("fechaEstadoEvaluacion", planManejoEvaluacionIterDto.getFechaEstadoEvaluacion());
            processStored.setParameter("idUsuarioRegistro", planManejoEvaluacionIterDto.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró el plan manejo evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Actualizar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTitularRegente", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoPlanManejoEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("maximoObservaciones", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEstadoEvaluacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idPlanManejoEvaluacion", planManejoEvaluacionDto.getIdPlanManejoEval());
            processStored.setParameter("idPlanManejo", planManejoEvaluacionDto.getIdPlanManejo());
            processStored.setParameter("idTitularRegente", planManejoEvaluacionDto.getIdTitularRegente());
            processStored.setParameter("fechaPresentacion", planManejoEvaluacionDto.getFechaPresentacion());
            processStored.setParameter("estadoPlanManejoEvaluacion", planManejoEvaluacionDto.getEstadoPlanManejoEvaluacion());
            processStored.setParameter("maximoObservaciones", planManejoEvaluacionDto.getMaximoObs());
            processStored.setParameter("fechaEstadoEvaluacion", planManejoEvaluacionDto.getFechaEstadoEvaluacion());
            processStored.setParameter("idUsuarioModificacion", planManejoEvaluacionDto.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó el plan manejo evaluación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }



    @Override
    public ResultClassEntity eliminarPlanManejoEvaluacion(PlanManejoEvaluacionDto planManejoEvaluacionDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Eliminar");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejoEvaluacion", planManejoEvaluacionDto.getIdPlanManejoEval());
            processStored.setParameter("idUsuarioElimina", planManejoEvaluacionDto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }



    @Override
    public ResultClassEntity ActualizarPlanManejoEvaluacionEstado(PlanManejoEvaluacionDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_ActualizarInsert_Estado");
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);


        processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
        processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se Actualizaron los estados correctamente.");

        LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                LogAuditoria.Table.T_MVC_PLANMANEJO,
                LogAuditoria.ACTUALIZAR,
                LogAuditoria.DescripcionAccion.Actualizar.T_MVC_PLANMANEJO + param.getIdPlanManejo(),
                param.getIdUsuarioModificacion());

        logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);



        return result;
    }

    @Override
    public ResultClassEntity obtenerEvaluacionPlanManejo(Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_Validar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();

            List<NotificacionDTO> notificacionDTOS = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    NotificacionDTO notificacionDTO = new NotificacionDTO();
                    notificacionDTO.setIdPlanManejo((Integer)row[0]);
                    notificacionDTO.setIdTitularRegente((Integer) row[1]);
                    notificacionDTO.setNombreTitularRegente((String) row[2]);
                    notificacionDTO.setCorreoTitularRegente((String) row[3]);
                    notificacionDTO.setNroObservaciones((Integer) row[4]);
                    notificacionDTOS.add(notificacionDTO);
                }
            }

            result.setData(notificacionDTOS);
            result.setSuccess(true);
            result.setMessage("Se listó los datos de la evaluación correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarEvaluacionCampoPlanManejoEvaluacion(PlanManejoEvaluacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_ActualizarEvaluacionCampo");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());
            processStored.setParameter("idEvaluacionCampo", param.getIdEvaluacionCampo());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó evaluación campo del plan manejo evaluación correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
    @Override
    public ResultClassEntity ActualizarNotificacionPlanManejoEvaluacion(PlanManejoEvaluacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_ActualizarNotificacion");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaNotificacion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());
            processStored.setParameter("fechaNotificacion", param.getFechaNotificacion());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioRegistro());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó notificación la correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity validarLineamiento(PlanManejoEvaluacionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("pa_PlanManejoEvaluacion_ValidarLineamiento");
            processStored.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("completado", Boolean.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());

            processStored.execute();
            Boolean completado = (Boolean) processStored.getOutputParameterValue("completado");
            Boolean conforme = (Boolean) processStored.getOutputParameterValue("conforme");

            ValidarLineamientoDto valida= new ValidarLineamientoDto();
            valida.setCompletado(completado);
            valida.setConforme(conforme);
            result.setData(valida);
            result.setSuccess(true);
            result.setMessage("Se valida lineamientos");
            return  result;
        }
        catch (Exception e){
            log.error("validarLineamiento - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity retrotraerPlanManejoEvaluacion(PlanManejoEvaluacionDto param) throws Exception {

        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PlanManejoEvaluacion_RetrotraerPlanManejoEvaluacion");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            sp.execute();

            result.setMessage("Se actualizo correctamente");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            result.setSuccess(false);
            return result;
        }
    }
}
