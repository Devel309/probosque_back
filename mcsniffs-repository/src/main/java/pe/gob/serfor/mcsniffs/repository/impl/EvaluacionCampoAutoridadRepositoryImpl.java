package pe.gob.serfor.mcsniffs.repository.impl;

import javassist.compiler.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad.EvaluacionCampoAutoridadDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.repository.EvaluacionCampoAutoridadRepository;
import pe.gob.serfor.mcsniffs.repository.ObjetivoManejoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EvaluacionCampoAutoridadRepositoryImpl extends JdbcDaoSupport implements EvaluacionCampoAutoridadRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(EvaluacionCampoAutoridadRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por filtro Autoridad de Parametos}
     * @param:ParametroEntity
     */
    @Override
    public ResultClassEntity ComboPorFiltroAutoridad(EvaluacionCampoAutoridadDto p) throws Exception  {
        ResultClassEntity result = new ResultClassEntity();
        try {
            List<Dropdown> listDropdown = new ArrayList<Dropdown>();
            List<EvaluacionCampoAutoridadDto> detalle = new ArrayList<EvaluacionCampoAutoridadDto>();
            List<EvaluacionCampoAutoridadDto> cabecera = new ArrayList<EvaluacionCampoAutoridadDto>();

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoAutoridad_Listar");
            processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
            processStored.execute();

            List<Object[]> spResult =processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    EvaluacionCampoAutoridadDto temp = new EvaluacionCampoAutoridadDto();
                    temp.setIdEvaluacionCampo((Integer)row[0]);
                    temp.setIdEvaluacionCampoAutoridad((Integer)row[1]);
                    temp.setIdTipoAutoridad((String) row[2]);
                    temp.setTipoAutoridad((String)row[3]);
                    temp.setIdParametroPadre((Integer)row[4]);
                    Boolean activo =((Integer) row[5]==0?true:false);
                    temp.setActivo((Boolean)activo);
                    if(temp.getIdParametroPadre()==0){
                        cabecera.add(temp);
                    }
                    else{
                        detalle.add(temp);
                    }
                }
            }
            for (EvaluacionCampoAutoridadDto customer : cabecera) {
                Dropdown obj = new Dropdown();
                obj.setLabel(customer.getTipoAutoridad());
                obj.setValue(customer.getIdTipoAutoridad());
                obj.setValor(customer.getIdEvaluacionCampoAutoridad());
                obj.setIdPadre(Integer.parseInt(customer.getIdTipoAutoridad()));
                obj.setActivo(customer.getActivo());
                List<Dropdown.item> list = new ArrayList<Dropdown.item>();
                for (EvaluacionCampoAutoridadDto customer_ : detalle) {
                    if (obj.getIdPadre().equals(customer_.getIdParametroPadre())) {
                        Dropdown.item item = new Dropdown.item();
                        item.setLabel(customer_.getTipoAutoridad());
                        item.setValue(customer_.getIdTipoAutoridad());
                        item.setValor(customer_.getIdEvaluacionCampoAutoridad());
                        item.setActivo(customer_.getActivo());
                        list.add(item);
                    }
                }
                obj.setItems(list);
                listDropdown.add(obj);
            }
            result.setData(listDropdown);
            return  result;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionCampoAutoridad(List<EvaluacionCampoAutoridadDto> list) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            EvaluacionCampoAutoridadDto obj_ = new EvaluacionCampoAutoridadDto();
            obj_=list.get(0);

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoAutoridad_Inactivar");
            processStored_.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored_);
            processStored_.setParameter("idEvaluacionCampo", obj_.getIdEvaluacionCampo());
            processStored_.execute();
            for(EvaluacionCampoAutoridadDto p:list){

                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionCampoAutoridad_Registrar");
                processStored.registerStoredProcedureParameter("idEvaluacionCampo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idEvaluacionCampoAutoridad", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoAutoridad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoAutoridad", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idEvaluacionCampo", p.getIdEvaluacionCampo());
                processStored.setParameter("idEvaluacionCampoAutoridad", p.getIdEvaluacionCampoAutoridad());
                processStored.setParameter("idTipoAutoridad", p.getIdTipoAutoridad());
                processStored.setParameter("tipoAutoridad", p.getTipoAutoridad());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();

            }

            result.setSuccess(true);
            result.setMessage("Se registró Evaluación de Campo Autoridad correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }

}
