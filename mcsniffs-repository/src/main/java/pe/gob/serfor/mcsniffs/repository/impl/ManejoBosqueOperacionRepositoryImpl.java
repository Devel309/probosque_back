package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueOperacionRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManejoBosqueOperacionRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueOperacionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueOperacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosqueOperacion(List<ManejoBosqueOperacionEntity> param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueOperacionEntity> list = new ArrayList<ManejoBosqueOperacionEntity>();
            for(ManejoBosqueOperacionEntity p:param){
                StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueOperacion_Registrar");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idManBosqueOperacion", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idTipoOperacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("tipoOperacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("manoObra", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("maquina", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("operacionDescripcion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.setParameter("idPlanManejo", p.getPlanManejo().getIdPlanManejo());
                processStored.setParameter("idManBosqueOperacion", p.getIdManBosqueOperacion());
                processStored.setParameter("idTipoOperacion", p.getIdTipoOperacion());
                processStored.setParameter("tipoOperacion", p.getTipoOperacion());
                processStored.setParameter("manoObra", p.getManoObra());
                processStored.setParameter("maquina", p.getMaquina());
                processStored.setParameter("descripcion", p.getDescripcion());
                processStored.setParameter("operacionDescripcion", p.getOperacionDescripcion());
                processStored.setParameter("estado", p.getEstado());
                processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored.execute();
                List<Object[]> spResult_ =processStored.getResultList();

                if (spResult_.size() >= 1) {
                    for (Object[] row : spResult_) {
                        ManejoBosqueOperacionEntity obj = new ManejoBosqueOperacionEntity() ;
                        PlanManejoEntity ma = new PlanManejoEntity() ;
                        ma.setIdPlanManejo((Integer) row[0]);
                        obj.setPlanManejo(ma);
                        obj.setIdManBosqueOperacion((Integer) row[1]);
                        obj.setIdTipoOperacion((String) row[2]);
                        obj.setTipoOperacion((String) row[3]);
                        obj.setManoObra((String) row[4]);
                        obj.setMaquina((String) row[5]);
                        obj.setDescripcion((String) row[6]);
                        obj.setOperacionDescripcion((String) row[7]);
                        list.add(obj);
                    }
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró las Operacines de corta y arrastre correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity EliminarManejoBosqueOperacion(ManejoBosqueOperacionEntity manejoBosqueOperacion) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueOperacion_Eliminar");
            processStored.registerStoredProcedureParameter("idManBosqueOperacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idManBosqueOperacion", manejoBosqueOperacion.getIdManBosqueOperacion());
            processStored.setParameter("idUsuarioElimina", manejoBosqueOperacion.getIdUsuarioElimina());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se eliminó registro correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ListarManejoBosqueOperacion(PlanManejoEntity planManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqueOperacionEntity> list = new ArrayList<ManejoBosqueOperacionEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueOperacion_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", planManejo.getIdPlanManejo());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();

            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    ManejoBosqueOperacionEntity obj = new ManejoBosqueOperacionEntity() ;
                    PlanManejoEntity ma = new PlanManejoEntity() ;
                    ma.setIdPlanManejo((Integer) row[0]);
                    obj.setPlanManejo(ma);
                    obj.setIdManBosqueOperacion((Integer) row[1]);
                    obj.setIdTipoOperacion((String) row[2]);
                    obj.setTipoOperacion((String) row[3]);
                    obj.setManoObra((String) row[4]);
                    obj.setMaquina((String) row[5]);
                    obj.setDescripcion((String) row[6]);
                    obj.setOperacionDescripcion((String) row[7]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se registró obtuvo el manejo bosque operación.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }
}
