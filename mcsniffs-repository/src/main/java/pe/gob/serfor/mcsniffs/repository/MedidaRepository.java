package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDto;

public interface MedidaRepository {
    List<MedidaDto> listarMedida(MedidaDto dto) throws Exception;
    ResultClassEntity registrarMedida(MedidaDto dto) throws Exception;     
    ResultClassEntity eliminarMedida(MedidaDto dto) throws Exception;
    
}
