package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface LogAuditoriaRepository {

    ResultClassEntity RegistrarLogAuditoria(LogAuditoriaEntity request) throws Exception;


}
