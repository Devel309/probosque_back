package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.xpath.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.ResultadoPP.ResultadoPPDto;
import pe.gob.serfor.mcsniffs.entity.EvaluacionRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoNotalListEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPBandejaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPNotasEntity;
import pe.gob.serfor.mcsniffs.repository.AnexoRepository;
import pe.gob.serfor.mcsniffs.repository.ProcesoPostulacionRepository;
import pe.gob.serfor.mcsniffs.repository.ResultadoPPRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ResultadoPPRepositoryImpl extends JdbcDaoSupport implements ResultadoPPRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;
    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Repository de los resultados de los procesos de postulacion}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ResultadoPPRepositoryImpl.class);

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Autowired
    private ProcesoPostulacionRepository pp;

    @Autowired
    private AnexoRepository adjunto;
    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Lista los resultados de las evaluaciones}
     * @param:ResultadoPPEntity
     */
    @Override
    public ResultEntity<ResultadoPPEntity> ListarResultadosEvaluaciones(ResultadoPPEntity filtro) {
        ResultEntity<ResultadoPPEntity> result = new ResultEntity<ResultadoPPEntity>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPP_ListarPorFiltros");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ganador", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("ganador", filtro.getGanador());
            sp.setParameter("idResultadoPP", filtro.getIdResultadoPP());
            sp.execute();
            ResultadoPPEntity c = null;
            List<ResultadoPPEntity> lstresult = new ArrayList<>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ResultadoPPEntity();
                    c.setIdResultadoPP((Integer) row[0]);
                    c.setIdProcesoPostulacion((Integer) row[1]);
                    c.setAsuntoEmail((String) row[2]);
                    c.setContenidoEmail((String) row[3]);
                    c.setIdAutoridadRegional((Integer) row[4]);
                    c.setFechaEnvioResultado((Date) row[5]);
                    c.setNotaTotal((Integer) row[6]);
                    c.setGanador((Boolean) row[7]);
                    c.setIdUsuarioPostulacion((Integer) row[8]);
                    lstresult.add(c);
                }
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los resultados de los PP.");
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ListarResultadosEvaluaciones", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
    /**REGISTRAR resultadoPp */
    @Override
    public ResultClassEntity registrarResultadoPp(ResultadoPPDto dto) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPP_RegistrarGeneral");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("asuntoMail", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("contenidoMail", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idAutoridadRegional", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaEnvioResultados", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("notaTotal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ganador", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoPostulacion", dto.getIdProcesoPostulacion());
            processStored.setParameter("asuntoMail", dto.getAsuntoMail());
            processStored.setParameter("contenidoMail", dto.getContenidoMail());
            processStored.setParameter("idAutoridadRegional", dto.getIdAutoridadRegional());
            processStored.setParameter("fechaEnvioResultados", dto.getFechaEnvioResultado());
            processStored.setParameter("notaTotal", dto.getNotaTotal());
            processStored.setParameter("ganador", dto.getGanador());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioPostulacion());
            processStored.execute();
            
            result.setSuccess(true);
            result.setMessage("Se registró el resultado de propuesta técnica correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Registra y envia la resulusion a los participantes}
     * @param:ResultadoPPEntity
     */
    @Override
    public ResultClassEntity RegistrarResultados(ResultadoPPEntity obj) {

        ResultClassEntity result = new ResultClassEntity();
        try {


            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPP_Registrar");
            sp.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("asuntomail", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("contenidomail", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idAutoridadRegional", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaEnvioResultados", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ganador", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaModificacion", Date.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResultadoPP", obj.getIdResultadoPP());
            sp.setParameter("asuntomail", obj.getAsuntoEmail());
            sp.setParameter("contenidomail", obj.getContenidoEmail());
            sp.setParameter("idAutoridadRegional", obj.getIdAutoridadRegional());
            sp.setParameter("fechaEnvioResultados", new Date());
            sp.setParameter("ganador", obj.getGanador());
            sp.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            sp.setParameter("fechaModificacion", new Date());
            sp.execute();


            StoredProcedureQuery sp2 = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPPAdjuntos_Registrar");
            sp2.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            sp2.registerStoredProcedureParameter("idDocumentoAdjunto", Integer.class, ParameterMode.IN);
            sp2.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp2.registerStoredProcedureParameter("fechaRegistro", Date.class, ParameterMode.IN);
            sp2.registerStoredProcedureParameter("idResultadoPPAdjuntos", Integer.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(sp2);
            sp2.setParameter("idResultadoPP", obj.getIdResultadoPP());
            sp2.setParameter("idDocumentoAdjunto", obj.getIdDocumentoAdjunto());
            sp2.setParameter("idUsuarioRegistro", obj.getIdUsuarioModificacion());
            sp2.setParameter("fechaRegistro", new Date());
            sp2.execute();


            Integer idResult = (Integer) sp2.getOutputParameterValue("idResultadoPPAdjuntos");

            //actualizar el estado a en publicacion
            Integer Status = 11;
            Integer StatusP = 12;
            if(obj.getGanador()){
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(obj.getIdProcesoPostulacion(),Status,obj.getIdUsuarioModificacion(),null);
                result.setData(resultup);
            }else{
                ResultClassEntity resultup = pp.actualizarEstadoProcesoPostulacion(obj.getIdProcesoPostulacion(),StatusP,obj.getIdUsuarioModificacion(), null);
                result.setData(resultup);
            }
            result.setCodigo(idResult);
            result.setSuccess(true);
            result.setMessage("Se registraron los resultados.");
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - RegistrarResultados", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }

    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Obtiene los resultados}
     * @param:IdResultadoPP
     */
    @Override
    public ResultClassEntity<ResultadoPPAdjuntoEntity> ObtenerResultados(Integer IdResultadoPP) {

        ResultClassEntity<ResultadoPPAdjuntoEntity> result = new ResultClassEntity<ResultadoPPAdjuntoEntity>();
        try {
            ResultadoPPAdjuntoEntity resultobj = new ResultadoPPAdjuntoEntity();
            ResultadoPPEntity filtro = new ResultadoPPEntity();
            filtro.setIdProcesoPostulacion(null);
            filtro.setIdResultadoPP(IdResultadoPP);
            filtro.setGanador(null);
            ResultEntity<ResultadoPPEntity> lstresultado = ListarResultadosEvaluaciones(filtro);
            resultobj.setResultado(lstresultado.getData().get(0));

            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPPAdjunto_ObtenerRutaAdjunto");
            sp.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResultadoPP", IdResultadoPP);
            sp.execute();

            List<Object[]> spResult = sp.getResultList();
            String ruta = null;
            String nombredoc=null;

            if (spResult.size() >= 1){
                for (Object[] row: spResult){

                    ruta = (String) row[3];
                    nombredoc = (String) row[4];

                }
            }

            if(ruta!=null){
                resultobj.setNombredocumento(nombredoc);
                File file = new File(ruta);
                byte[] fileContent = Files.readAllBytes(file.toPath());
                resultobj.setDocumento(fileContent);
                //resultobj.setDocumentoFile(file);
            }
            result.setData(resultobj);
            result.setSuccess(true);
            result.setMessage("Se listaron los resultados.");

            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ObtenerResultados", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }

    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Registra las notas del proceso de postulacion}
     * @param:ResultadoPPNotasEntity
     */
    public ResultClassEntity RegistrarNotas(ResultadoPPNotasEntity notas) {

        ResultClassEntity result = new ResultClassEntity();

        try {
            Date date = new Date();
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPPEvaluacion_Registrar");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            sp.registerStoredProcedureParameter("puntaje11", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntaje12", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntaje21", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntaje22", Integer.class, ParameterMode.IN);

            sp.registerStoredProcedureParameter("puntajeAdecMeta311", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3121", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3122", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3123", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3131", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3132", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3141", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3142", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3211", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3212", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3213", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3221", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeAdecMeta3222", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta311", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3121", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3122", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3123", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3131", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3132", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3141", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3142", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3211", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3212", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3213", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3221", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoAdecMeta3222", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene311", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3121", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3122", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3123", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3131", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3132", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3141", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3142", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3211", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3212", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3213", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3221", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoTiene3222", Integer.class, ParameterMode.IN);

            sp.registerStoredProcedureParameter("puntajeCoherente4111", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente4112", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente412", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente413", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente414", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente415", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente416", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente417", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente418", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente419", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente4110", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente421", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente4221", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeCoherente4222", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente4111", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente4112", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente412", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente413", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente414", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente415", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente416", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente417", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente418", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente419", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente4110", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente421", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente4221", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoCoherente4222", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna4111", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna4112", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna412", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna413", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna414", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna415", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna416", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna417", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna418", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna419", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna4110", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna421", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna4221", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("puntajeNoConsigna4222", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", notas.getIdProcesoPostulacion());
            sp.setParameter("idUsuarioRegistro", notas.getIdUsuarioRegistro());

            sp.setParameter("puntaje11", notas.getPuntaje11());
            sp.setParameter("puntaje12", notas.getPuntaje12());
            sp.setParameter("puntaje21", notas.getPuntaje21());
            sp.setParameter("puntaje22", notas.getPuntaje22());

            sp.setParameter("puntajeAdecMeta311", notas.getPuntajeAdecMeta311());
            sp.setParameter("puntajeAdecMeta3121", notas.getPuntajeAdecMeta3121());
            sp.setParameter("puntajeAdecMeta3122", notas.getPuntajeAdecMeta3122());
            sp.setParameter("puntajeAdecMeta3123", notas.getPuntajeAdecMeta3123());
            sp.setParameter("puntajeAdecMeta3131", notas.getPuntajeAdecMeta3131());
            sp.setParameter("puntajeAdecMeta3132", notas.getPuntajeAdecMeta3132());
            sp.setParameter("puntajeAdecMeta3141", notas.getPuntajeAdecMeta3141());
            sp.setParameter("puntajeAdecMeta3142", notas.getPuntajeAdecMeta3142());
            sp.setParameter("puntajeAdecMeta3211", notas.getPuntajeAdecMeta3211());
            sp.setParameter("puntajeAdecMeta3212", notas.getPuntajeAdecMeta3212());
            sp.setParameter("puntajeAdecMeta3213", notas.getPuntajeAdecMeta3213());
            sp.setParameter("puntajeAdecMeta3221", notas.getPuntajeAdecMeta3221());
            sp.setParameter("puntajeAdecMeta3222", notas.getPuntajeAdecMeta3222());
            sp.setParameter("puntajeNoAdecMeta311", notas.getPuntajeNoAdecMeta311());
            sp.setParameter("puntajeNoAdecMeta3121", notas.getPuntajeNoAdecMeta3121());
            sp.setParameter("puntajeNoAdecMeta3122", notas.getPuntajeNoAdecMeta3122());
            sp.setParameter("puntajeNoAdecMeta3123", notas.getPuntajeNoAdecMeta3123());
            sp.setParameter("puntajeNoAdecMeta3131", notas.getPuntajeNoAdecMeta3131());
            sp.setParameter("puntajeNoAdecMeta3132", notas.getPuntajeNoAdecMeta3132());
            sp.setParameter("puntajeNoAdecMeta3141", notas.getPuntajeNoAdecMeta3141());
            sp.setParameter("puntajeNoAdecMeta3142", notas.getPuntajeNoAdecMeta3142());
            sp.setParameter("puntajeNoAdecMeta3211", notas.getPuntajeNoAdecMeta3211());
            sp.setParameter("puntajeNoAdecMeta3212", notas.getPuntajeNoAdecMeta3212());
            sp.setParameter("puntajeNoAdecMeta3213", notas.getPuntajeNoAdecMeta3213());
            sp.setParameter("puntajeNoAdecMeta3221", notas.getPuntajeNoAdecMeta3221());
            sp.setParameter("puntajeNoAdecMeta3222", notas.getPuntajeNoAdecMeta3222());
            sp.setParameter("puntajeNoTiene311", notas.getPuntajeNoTiene311());
            sp.setParameter("puntajeNoTiene3121", notas.getPuntajeNoTiene3121());
            sp.setParameter("puntajeNoTiene3122", notas.getPuntajeNoTiene3122());
            sp.setParameter("puntajeNoTiene3123", notas.getPuntajeNoTiene3123());
            sp.setParameter("puntajeNoTiene3131", notas.getPuntajeNoTiene3131());
            sp.setParameter("puntajeNoTiene3132", notas.getPuntajeNoTiene3132());
            sp.setParameter("puntajeNoTiene3141", notas.getPuntajeNoTiene3141());
            sp.setParameter("puntajeNoTiene3142", notas.getPuntajeNoTiene3142());
            sp.setParameter("puntajeNoTiene3211", notas.getPuntajeNoTiene3211());
            sp.setParameter("puntajeNoTiene3212", notas.getPuntajeNoTiene3212());
            sp.setParameter("puntajeNoTiene3213", notas.getPuntajeNoTiene3213());
            sp.setParameter("puntajeNoTiene3221", notas.getPuntajeNoTiene3221());
            sp.setParameter("puntajeNoTiene3222", notas.getPuntajeNoTiene3222());

            sp.setParameter("puntajeCoherente4111", notas.getPuntajeCoherente4111());
            sp.setParameter("puntajeCoherente4112", notas.getPuntajeCoherente4112());
            sp.setParameter("puntajeCoherente412", notas.getPuntajeCoherente412());
            sp.setParameter("puntajeCoherente413", notas.getPuntajeCoherente413());
            sp.setParameter("puntajeCoherente414", notas.getPuntajeCoherente414());
            sp.setParameter("puntajeCoherente415", notas.getPuntajeCoherente415());
            sp.setParameter("puntajeCoherente416", notas.getPuntajeCoherente416());
            sp.setParameter("puntajeCoherente417", notas.getPuntajeCoherente417());
            sp.setParameter("puntajeCoherente418", notas.getPuntajeCoherente418());
            sp.setParameter("puntajeCoherente419", notas.getPuntajeCoherente419());
            sp.setParameter("puntajeCoherente4110", notas.getPuntajeCoherente4110());
            sp.setParameter("puntajeCoherente421", notas.getPuntajeCoherente421());
            sp.setParameter("puntajeCoherente4221", notas.getPuntajeCoherente4221());
            sp.setParameter("puntajeCoherente4222", notas.getPuntajeCoherente4222());
            sp.setParameter("puntajeNoCoherente4111", notas.getPuntajeNoCoherente4111());
            sp.setParameter("puntajeNoCoherente4112", notas.getPuntajeNoCoherente4112());
            sp.setParameter("puntajeNoCoherente412", notas.getPuntajeNoCoherente412());
            sp.setParameter("puntajeNoCoherente413", notas.getPuntajeNoCoherente413());
            sp.setParameter("puntajeNoCoherente414", notas.getPuntajeNoCoherente414());
            sp.setParameter("puntajeNoCoherente415", notas.getPuntajeNoCoherente415());
            sp.setParameter("puntajeNoCoherente416", notas.getPuntajeNoCoherente416());
            sp.setParameter("puntajeNoCoherente417", notas.getPuntajeNoCoherente417());
            sp.setParameter("puntajeNoCoherente418", notas.getPuntajeNoCoherente418());
            sp.setParameter("puntajeNoCoherente419", notas.getPuntajeNoCoherente419());
            sp.setParameter("puntajeNoCoherente4110", notas.getPuntajeNoCoherente4110());
            sp.setParameter("puntajeNoCoherente421", notas.getPuntajeNoCoherente421());
            sp.setParameter("puntajeNoCoherente4221", notas.getPuntajeNoCoherente4221());
            sp.setParameter("puntajeNoCoherente4222", notas.getPuntajeNoCoherente4222());
            sp.setParameter("puntajeNoConsigna4111", notas.getPuntajeNoConsigna4111());
            sp.setParameter("puntajeNoConsigna4112", notas.getPuntajeNoConsigna4112());
            sp.setParameter("puntajeNoConsigna412", notas.getPuntajeNoConsigna412());
            sp.setParameter("puntajeNoConsigna413", notas.getPuntajeNoConsigna413());
            sp.setParameter("puntajeNoConsigna414", notas.getPuntajeNoConsigna414());
            sp.setParameter("puntajeNoConsigna415", notas.getPuntajeNoConsigna415());
            sp.setParameter("puntajeNoConsigna416", notas.getPuntajeNoConsigna416());
            sp.setParameter("puntajeNoConsigna417", notas.getPuntajeNoConsigna417());
            sp.setParameter("puntajeNoConsigna418", notas.getPuntajeNoConsigna418());
            sp.setParameter("puntajeNoConsigna419", notas.getPuntajeNoConsigna419());
            sp.setParameter("puntajeNoConsigna4110", notas.getPuntajeNoConsigna4110());
            sp.setParameter("puntajeNoConsigna421", notas.getPuntajeNoConsigna421());
            sp.setParameter("puntajeNoConsigna4221", notas.getPuntajeNoConsigna4221());
            sp.setParameter("puntajeNoConsigna4222", notas.getPuntajeNoConsigna4222());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se registraron las notas correctamente.");

            return result;


        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - RegistrarNotas", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }


    }


    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {obtiene las notas del proceso de postulacion}
     * @param:IdResultadoPP
     */
    @Override
    public ResultEntity<ResultadoNotalListEntity> ObtenerNotas(Integer IdResultadoPP) {
        ResultEntity<ResultadoNotalListEntity> result = new ResultEntity<ResultadoNotalListEntity>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPPEvaluacion_Listar");
            sp.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResultadoPP", IdResultadoPP);
            sp.execute();
            List<ResultadoNotalListEntity> lstresult = new ArrayList<>();
            ResultadoNotalListEntity c = null;
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ResultadoNotalListEntity();
                    c.setIdResultadoPP((Integer) row[0]);
                    c.setCodigoSeccion((String) row[1]);
                    c.setCodigoSubSeccion((String) row[2]);
                    c.setPuntajeOtorgado((Integer) row[3]);
                    c.setPuntajeAdecuadoMeta((Integer) row[4]);
                    c.setPuntajeNoAdecuadoMeta((Integer) row[5]);
                    c.setPuntajeNoTiene((Integer) row[6]);
                    c.setPuntajeCoherente((Integer) row[7]);
                    c.setPuntajeNoCoherente((Integer) row[8]);
                    c.setPuntajeNoConsigna((Integer) row[9]);
                    c.setObservacion((String) row[10]);
                    c.setCalificacionTotal((Integer) row[11]);
                    lstresult.add(c);
                }
            }

            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron autorizaciones enviadas.");
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ObtenerNotas", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {obtiene el primer proceso postulacion
     * @param:IdProcesoOferta
     */
    @Override
    public ResultClassEntity<ProcesoPostulacionEntity> ObtenerPrimerPP(Integer IdProcesoOferta) {
        ResultClassEntity<ProcesoPostulacionEntity> result = new ResultClassEntity<ProcesoPostulacionEntity>();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_PrimerPostulante");
            sp.registerStoredProcedureParameter("idProcesoOferta", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoOferta", IdProcesoOferta);
            sp.execute();
            ProcesoPostulacionEntity c = null;
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ProcesoPostulacionEntity();
                    c.setIdProcesoPostulacion((Integer) row[0]);
                    c.setIdUsuarioPostulacion((Integer) row[1]);
                    c.setIdRegenteForestal((Integer) row[2]);
                    c.setNombreRegente((String) row[3]);
                    c.setFechaPostulacion((Date) row[4]);
                    c.setObservacion((String) row[5]);
                    c.setRecepcionDocumentos((Boolean) row[6]);
                    c.setFechaRecepcionDocumentos((Date) row[7]);
                    c.setIdProcesoOferta((Integer) row[8]);
                    c.setUnidadAProvechamiento((String) row[9]);
                    c.setIdEstatusProceso((Integer) row[10]);
                    c.setStatusProceso((String) row[11]);
                }
            }

            result.setData(c);
            result.setSuccess(true);
            result.setMessage("Se listaron Procesos de Postulacion.");
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ObtenerPrimerPP", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }
    


    @Override
    public ResultClassEntity listarPorFiltroOtrogamientoProcesoPostulacion(EvaluacionRequestEntity filtro){
        List<ResultadoPPBandejaEntity> lista = new ArrayList<ResultadoPPBandejaEntity>();
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ProcesoPostulacion_ListarOrtorgamientoPorFiltros");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstadoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ganador",Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoOferta",Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idEstadoPostulacion", filtro.getIdEstadoPostulacion());
            sp.setParameter("P_PAGENUM", filtro.getPageNum());
            sp.setParameter("P_PAGESIZE", filtro.getPageSize());
            sp.setParameter("ganador", filtro.getGanador());
            sp.setParameter("idProcesoOferta", filtro.getIdProcesoOferta());
            sp.execute();
            ResultadoPPBandejaEntity c = null;
            List<Object[]> spResult = sp.getResultList();

            result.setTotalRecord(0);
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ResultadoPPBandejaEntity();
                    c.setIdProcesoOferta((Integer) row[0]);
                    c.setIdProcesoPostulacion((Integer) row[1]);
                    c.setIdUsuarioPostulacion((Integer) row[2]);
                    c.setNombreUsuarioPostulacion((String) row[3]);
                    c.setIdStatusProcesoPostulacion((Integer) row[4]);
                    c.setNombreStatusProcesoPostulacion((String) row[5]);
                    c.setFechaPostulacion((Date) row[6]);
                    c.setNotaTotal((Integer) row[7]);
                    c.setGanador((Boolean) row[8]);
                    c.setIdResultadoPP((Integer) row[9]);
                    c.setRepresentanteLegal((String) row[10]);
                    result.setTotalRecord((Integer) row[11]);
                    lista.add(c);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
            return result;
        } catch (Exception e) {
            log.error("listaron PPEvaluacion", e.getMessage());
            throw e;
        }
    }


    @Override
    public ResultClassEntity ListarPPEvaluacion(EvaluacionRequestEntity filtro){
        List<ResultadoPPBandejaEntity> lista = new ArrayList<ResultadoPPBandejaEntity>();
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ProcesoPostulacionListarPorFiltros");
            sp.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idEstadoPostulacion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ganador",Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idProcesoOferta",Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProcesoPostulacion", filtro.getIdProcesoPostulacion());
            sp.setParameter("idEstadoPostulacion", filtro.getIdEstadoPostulacion());
            sp.setParameter("P_PAGENUM", filtro.getPageNum());
            sp.setParameter("P_PAGESIZE", filtro.getPageSize());
            sp.setParameter("ganador", filtro.getGanador());
            sp.setParameter("idProcesoOferta", filtro.getIdProcesoOferta());
            sp.execute();
            ResultadoPPBandejaEntity c = null;
            List<Object[]> spResult = sp.getResultList();

            result.setTotalRecord(0);
            if (spResult!= null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    c = new ResultadoPPBandejaEntity();
                    c.setIdProcesoOferta((Integer) row[0]);
                    c.setIdProcesoPostulacion((Integer) row[1]);
                    c.setIdUsuarioPostulacion((Integer) row[2]);
                    c.setNombreUsuarioPostulacion((String) row[3]);
                    c.setIdStatusProcesoPostulacion((Integer) row[4]);
                    c.setNombreStatusProcesoPostulacion((String) row[5]);
                    c.setFechaPostulacion((Date) row[6]);
                    c.setNotaTotal((Integer) row[7]);
                    c.setGanador((Boolean) row[8]);
                    c.setIdResultadoPP((Integer) row[9]);
                    c.setRepresentanteLegal((String) row[10]);
                    result.setTotalRecord((Integer) row[11]);
                    lista.add(c);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage(spResult.size() > 0 ? "Información encontrada" : "No se encontró información");
            return result;
        } catch (Exception e) {
            log.error("listaron PPEvaluacion", e.getMessage());
            throw e;
        }
    }


    /**
     * @autor: JaquelineDB [14-07-2021]
     * @modificado:
     * @descripción: {Listar los procesos en oferta con potulaciones evaluadas
     * @param:
     */
    @Override
    public ResultClassEntity<ProcesoOfertaEntity> ListarProcesosOferta() {
        ResultClassEntity result = new ResultClassEntity();

        try {

            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPP_ListarProcesoOferta");
            sp.execute();
            List<Object[]> spResult =sp.getResultList();
            List<ProcesoOfertaEntity> lstresult = new ArrayList<>();
            ProcesoOfertaEntity c = null;

            if (spResult!= null && !spResult.isEmpty()){
                for (Object row : spResult) {
                    c = new ProcesoOfertaEntity();
                    c.setIdProcesoOferta((Integer) row);
                    lstresult.add(c);
                }
            }

            result.setData(lstresult);
            result.setSuccess(true);
            result.setMessage("Se listaron los resultados de los PP.");
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ListarProcesosOferta", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }

    }

    /*
    @Override
    public ResultEntity<ProcesoOfertaEntity> ListarProcesosOferta() {
        return null;

        ResultEntity<ProcesoOfertaEntity> result = new ResultEntity<ProcesoOfertaEntity>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ResultadoPP_ListarProcesoOferta()}");

            ResultSet rs = pstm.executeQuery();
            ProcesoOfertaEntity c = null;
            List<ProcesoOfertaEntity> lstresult = new ArrayList<>();
            while(rs.next()){
                c = new ProcesoOfertaEntity();
                c.setIdProcesoOferta(rs.getInt("TX_ID_PROCESO_OFERTA"));
                lstresult.add(c);
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los resultados de los PP.");
            pstm.close();
            con.close();
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ListarProcesosOferta", e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }

    }*/

    /**
     * @autor: JaquelineDB [01-08-2021]
     * @modificado:
     * @descripción: {Listar los procesos en oferta con potulaciones evaluadas
     * @param:
     */
    @Override
    public ResultClassEntity ActualizarGanador(Integer IdProcesoPostulacionPerdedor, Integer IdProcesoPostulacionGenador, Integer IdUsuarioModificacion) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            //obtener perdedor
            ResultadoPPEntity filtro = new ResultadoPPEntity();
            filtro.setIdProcesoPostulacion(IdProcesoPostulacionPerdedor);
            filtro.setGanador(null);
            filtro.setIdResultadoPP(null);
            ResultEntity<ResultadoPPEntity> lista= ListarResultadosEvaluaciones(filtro);
            Integer IdResultadoPP = lista.getData().get(0).getIdResultadoPP();
            //actualizar estatus de resultado a perdedor
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPP_updateGanador");
            sp.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("ganador", Boolean.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResultadoPP", IdResultadoPP);
            sp.setParameter("ganador", false);
            sp.setParameter("idUsuarioModificacion", IdUsuarioModificacion);
            sp.execute();
            //obtener ganador
            ResultadoPPEntity filtro2 = new ResultadoPPEntity();
            filtro2.setIdProcesoPostulacion(IdProcesoPostulacionGenador);
            filtro2.setGanador(null);
            filtro2.setIdResultadoPP(null);
            ResultEntity<ResultadoPPEntity> lista2= ListarResultadosEvaluaciones(filtro2);
            Integer IdResultadoPP2 = lista2.getData().get(0).getIdResultadoPP();
            StoredProcedureQuery sp2 = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoPP_updateGanador");
            sp2.registerStoredProcedureParameter("idResultadoPP", Integer.class, ParameterMode.IN);
            sp2.registerStoredProcedureParameter("ganador", Boolean.class, ParameterMode.IN);
            sp2.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp2);
            sp2.setParameter("idResultadoPP", IdResultadoPP2);
            sp2.setParameter("ganador", true);
            sp2.setParameter("idUsuarioModificacion", IdUsuarioModificacion);
            sp2.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó el ganador.");
            return result;
        } catch (Exception e) {
            log.error("ResultadoPPRepositoryImpl - ActualizarGanador", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: Abner Valdez [06-12-2021]
     * @modificado:
     * @descripción: {obtener el resultado PP por IdPP }
     * @param:
     */

    @Override
    public ResultClassEntity obtenerResultadoPPPorIdPP(Integer idProcesoPostulacion) {

        ResultClassEntity result = new ResultClassEntity();
        List<ResultadoPPEntity> list = new ArrayList<ResultadoPPEntity>();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ResultadoAPP_ObtenerPorIdPP");
            processStored.registerStoredProcedureParameter("idProcesoPostulacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcesoPostulacion", idProcesoPostulacion);

            processStored.execute();
            List<Object[]> spResult =processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ResultadoPPEntity temp = new ResultadoPPEntity();
                    temp.setIdResultadoPP((Integer) row[0]);
                    temp.setIdProcesoPostulacion((Integer) row[1]);
                    list.add(temp);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            return  result;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            return  result;
        }
    }
}
