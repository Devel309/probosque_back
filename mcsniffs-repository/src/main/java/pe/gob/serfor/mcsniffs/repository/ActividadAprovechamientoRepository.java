package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoParam;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

public interface ActividadAprovechamientoRepository {
    ResultEntity<ActividadAprovechamientoEntity> ListarActvidadAprovechamiento(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity RegistrarActvidadAprovechamiento(List<ActividadAprovechamientoEntity>  request) throws Exception;
    ResultClassEntity EliminarActvidadAprovechamiento(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity EliminarActvidadAprovechamientoDet(ActividadAprovechamientoParam request) throws Exception;
    ResultClassEntity EliminarActvidadAprovechamientoDetSub(ActividadAprovechamientoParam request) throws Exception;
}
