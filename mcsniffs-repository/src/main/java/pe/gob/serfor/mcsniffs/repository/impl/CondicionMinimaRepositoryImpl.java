package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ListarCondicionMinimaDto;
import pe.gob.serfor.mcsniffs.repository.CondicionMinimaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class CondicionMinimaRepositoryImpl extends JdbcDaoSupport implements CondicionMinimaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(CondicionMinimaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registrarCondicionMinima(CondicionMinimaDto request) {

        ResultClassEntity result = new ResultClassEntity();

        try{

            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CondicionMinima_Registrar");
            processStored_.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("resultadoEvaluacion", String.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("fechaEvaluacion", Date.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            processStored_.setParameter("tipoDocumento",request.getTipoDocumento());
            processStored_.setParameter("nroDocumento", request.getNroDocumento());
            processStored_.setParameter("descripcion",request.getDescripcion());
            processStored_.setParameter("resultadoEvaluacion", request.getResultadoEvaluacion());
            processStored_.setParameter("fechaEvaluacion", request.getFechaEvaluacion());
            processStored_.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());

            processStored_.execute();
            List<Object[]> spResult_ =processStored_.getResultList();
            CondicionMinimaDto obj_ = new CondicionMinimaDto();

            if (spResult_.size() >= 1) {
                for (Object[] row_ : spResult_) {

                    obj_.setIdCondicionMinima((Integer) row_[0]);
                    obj_.setTipoDocumento((String) row_[1]);
                    obj_.setNroDocumento((String) row_[2]);
                    obj_.setDescripcion((String) row_[3]);
                    obj_.setResultadoEvaluacion((String) row_[4]);
                    obj_.setFechaEvaluacion((Date) row_[5]);


                    List<CondicionMinimaDetalleDto> listDet = new ArrayList<>();
                    for(CondicionMinimaDetalleDto param: request.getCondicionMinimaDetalle()){
                        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CondicionMinimaDetalle_Registrar");
                        processStored.registerStoredProcedureParameter("codigoTipoCondicionMinimaDet", String.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("resultado", String.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("fechaIngreso", Date.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("fechaVigencia", Date.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idCondicionMinima", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                        SpUtil.enableNullParams(processStored);
                        processStored.setParameter("codigoTipoCondicionMinimaDet", param.getCodigoTipoCondicionMinimaDet());
                        processStored.setParameter("resultado", param.getResultado());
                        processStored.setParameter("fechaIngreso", param.getFechaIngreso());
                        processStored.setParameter("fechaVigencia", param.getFechaVigencia());
                        processStored.setParameter("idCondicionMinima", obj_.getIdCondicionMinima());
                        processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());


                        processStored.execute();

                        List<Object[]> spResult =processStored.getResultList();
                        if (spResult.size() >= 1) {
                            for (Object[] row : spResult) {
                                CondicionMinimaDetalleDto objDet = new CondicionMinimaDetalleDto();
                                objDet.setIdCondicionMinimaDet((Integer) row[0]);
                                objDet.setCodigoTipoCondicionMinimaDet((String) row[1]);
                                objDet.setResultado((String) row[2]);
                                objDet.setFechaIngreso((Date) row[3]);
                                objDet.setFechaVigencia((Date) row[4]);
                                objDet.setIdCondicionMinima((Integer) row[5]);
                                objDet.setEstado((String) row[6]);

                                listDet.add(objDet);
                            }
                        }
                    }
                    obj_.setCondicionMinimaDetalle(listDet);

                }
            }

            result.setData(obj_);
            result.setSuccess(true);
            result.setMessage("Se registró correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public List<ListarCondicionMinimaDto> obtenerCondicionMinima(Integer idCondicionMinima) throws Exception {
        
        try {
            StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_CondicionMinima_Obtener");
            sp.registerStoredProcedureParameter("idCondicionMinima", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idCondicionMinima", idCondicionMinima);
            sp.execute();
            List<ListarCondicionMinimaDto> resultDet = null;
            List<Object[]> spResultDet = sp.getResultList();
            if (spResultDet!=null && !spResultDet.isEmpty()){
                resultDet = new ArrayList<>();
                ListarCondicionMinimaDto temp = null;
                for (Object[] item: spResultDet) {
                    temp = new ListarCondicionMinimaDto();
                    temp.setIdCondicionMinima(item[0]==null?null:(Integer) item[0]);
                    temp.setTipoDocumento(item[1]==null?null:(String) item[1]);
                    temp.setNroDocumento(item[2]==null?null:(String) item[2]);
                    temp.setDescripcion(item[3]==null?null:(String) item[3]);
                    temp.setResultadoEvaluacion(item[4]==null?null:(String) item[4]);
                    temp.setFechaEvaluacion(item[5]==null?null:(String) item[5]);
                    temp.setEstado(item[6]==null?null:(String) item[6]);
                    resultDet.add(temp);
                }
            }

            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

}
