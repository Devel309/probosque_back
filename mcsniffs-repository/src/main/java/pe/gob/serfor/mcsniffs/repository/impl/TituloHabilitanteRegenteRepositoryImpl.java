package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHRegenteDto;
import pe.gob.serfor.mcsniffs.repository.MonitoreoRepository;
import pe.gob.serfor.mcsniffs.repository.TituloHabilitanteRegenteRepository;
import pe.gob.serfor.mcsniffs.repository.TituloHabilitanteRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class TituloHabilitanteRegenteRepositoryImpl extends JdbcDaoSupport implements TituloHabilitanteRegenteRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(TituloHabilitanteRegenteRepository.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity listarInformacionTHRegente(InformacionTHRegenteDto param) throws Exception {

        try {
            ResultClassEntity result = new ResultClassEntity();

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("Sincronizacion.pa_informacionRegente_Listar");
            processStored.registerStoredProcedureParameter("codigoTH", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoTH", param.getCodigoTH());


            processStored.execute();
            List<InformacionTHRegenteDto> lsInfo = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                InformacionTHRegenteDto temp = null;
                for (Object[] row_: spResult){
                    temp = new InformacionTHRegenteDto();
                   
                    temp.setIdPlanManejoRegente(row_[0]==null?null:(Integer) row_[0]);
                    temp.setIdPlanManejo(row_[1]==null?null:(Integer) row_[1]);
                    temp.setCodigoProceso(row_[2]==null?null:(String) row_[2]);
                   temp.setCodigoTipoRegente(row_[3]==null?null:(String) row_[3]);
                    temp.setNombres(row_[4]==null?null:(String) row_[4]);
                    temp.setIdContrato(row_[5]==null?null:(Integer) row_[5]);
                    temp.setIdPlanManejoContrato(row_[6]==null?null:(Integer) row_[6]);
                    temp.setCodigoTH(row_[7]==null?null:(String) row_[7]);
                    lsInfo.add(temp);
                }
            }
            result.setData(lsInfo);
            result.setSuccess(true);
            result.setMessage("Se lista la información TH-Regente correctamente.");
            return result;

        } catch (Exception e) {
            log.error("Listar Información TH-Regente", e.getMessage());
            throw e;
        }
    }
}
