package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ResultadoPP.ResultadoPPDto;

public interface ResultadoPPRepository {
    ResultEntity<ResultadoPPEntity> ListarResultadosEvaluaciones(ResultadoPPEntity filtro);
    ResultClassEntity RegistrarResultados(ResultadoPPEntity obj);
    ResultClassEntity<ResultadoPPAdjuntoEntity> ObtenerResultados(Integer IdResultadoPP);
    ResultClassEntity RegistrarNotas(ResultadoPPNotasEntity notas);
    ResultEntity<ResultadoNotalListEntity> ObtenerNotas(Integer IdResultadoPP);
    ResultClassEntity<ProcesoPostulacionEntity> ObtenerPrimerPP(Integer IdProcesoOferta);
    ResultClassEntity ListarPPEvaluacion(EvaluacionRequestEntity filtro);
    ResultClassEntity<ProcesoOfertaEntity> ListarProcesosOferta();
    ResultClassEntity ActualizarGanador(Integer IdProcesoPostulacionPerdedor, Integer IdProcesoPostulacionGenador, Integer IdUsuarioModificacion);
    ResultClassEntity obtenerResultadoPPPorIdPP(Integer idProcesoPostulacion);
    ResultClassEntity registrarResultadoPp(ResultadoPPDto dto);
    ResultClassEntity listarPorFiltroOtrogamientoProcesoPostulacion(EvaluacionRequestEntity filtro);
}
