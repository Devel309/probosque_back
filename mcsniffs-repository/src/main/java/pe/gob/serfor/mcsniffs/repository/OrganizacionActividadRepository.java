 package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.OrganizacionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import java.util.List;

public interface OrganizacionActividadRepository {
    ResultClassEntity RegistrarOrganizacionActividad(List<OrganizacionActividadEntity> list);
    ResultClassEntity ListarPorFiltroOrganizacionActividad(OrganizacionActividadEntity param);
    ResultClassEntity EliminarOrganizacionActividad(OrganizacionActividadEntity param);
    ResultClassEntity RegistrarOrganizacionActividadArchivo(MultipartFile file, PlanManejoEntity planManejo);
    ResultClassEntity ListarOrganizacionActividadArchivo(PlanManejoEntity planManejo);

    ResultEntity ListarPorFiltroOrganizacionActividadArchivo(PlanManejoEntity planManejo);

}
