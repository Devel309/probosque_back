package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.AprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PotencialProduccionForestalRepository {

    ResultClassEntity registrar (PotencialProduccionForestalEntity item,MultipartFile file)throws Exception;
    ResultClassEntity listar(Integer idPlanManejo)throws Exception;

    /**
     * @autor: Rafael Azaña [16/10-2021]
     * @modificado:
     * @descripción: {CRUD DE POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */

    List<PotencialProduccionForestalEntity> ListarPotencialProducForestal(Integer idPlanManejo,String codigo) throws Exception;
    List<PotencialProduccionForestalEntity> ListarPotencialProducForestalTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,Integer idPlanManejo,String codigoProceso) throws Exception;

    ResultClassEntity RegistrarPotencialProducForestal(List<PotencialProduccionForestalEntity> list) throws Exception;

    ResultClassEntity EliminarPotencialProducForestal(PotencialProduccionForestalDto potencialProduccionForestalDto) throws Exception;

}
