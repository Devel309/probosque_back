package pe.gob.serfor.mcsniffs.repository.impl;

 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDetalleDto;
import pe.gob.serfor.mcsniffs.repository.MedidaDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class MedidaDetalleRepositoryImpl extends JdbcDaoSupport implements MedidaDetalleRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(MedidaDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<MedidaDetalleDto> listarMedidaDetalle(MedidaDetalleDto dto) throws Exception {
        
        List<MedidaDetalleDto> lista = new ArrayList<MedidaDetalleDto>();

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MedidaDetalle_Listar");
            processStored.registerStoredProcedureParameter("idMedidaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMedida", Integer.class, ParameterMode.IN); 

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idMedidaDet",dto.getIdMedidaDet());
            processStored.setParameter("idMedida",dto.getIdMedida()); 

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                MedidaDetalleDto temp = null;
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        temp = new MedidaDetalleDto();

                        temp.setIdMedidaDet(row[0]==null?null:(Integer) row[0]);
                        temp.setAutoridadSancionadora(row[1]==null?null:(String) row[1]);
                        temp.setFechaResolucion(row[2]==null?null:(Date) row[2]);
                        temp.setNumeroResolucion(row[3]==null?null:(String) row[3]);
                        temp.setIdArchivo(row[4]==null?null:(Integer) row[4]);
                        temp.setProceso(row[5]==null?null:(Integer) row[5]);
                        temp.setPlazo(row[6]==null?null:(String) row[6]);
                        temp.setIdMedida(row[7]==null?null:(Integer) row[7]);
                        temp.setDetalle(row[8]==null?null:(String) row[8]);
                        temp.setObservacion(row[9]==null?null:(String) row[9]);
                        temp.setDescripcion(row[10]==null?null:(String) row[10]);
                        temp.setContrato(row[11]==null?null:(String) row[11]);
                        temp.setEstado(row[12]==null?null:(String) row[12]);
          
                        lista.add(temp);
                    }
                }
            }

            
            return lista;
        } catch (Exception e) {
            log.error("listarProcedimientoAdministrativo", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity registrarMedidaDetalle(MedidaDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MedidaDetalle_Registrar");

            processStored.registerStoredProcedureParameter("idMedidaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("autoridadSancionadora", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaResolucion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroResolucion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("proceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("plazo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idMedida", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("contrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMedidaDet", dto.getIdMedidaDet());
            processStored.setParameter("autoridadSancionadora", dto.getAutoridadSancionadora());
            processStored.setParameter("fechaResolucion", dto.getFechaResolucion());
            processStored.setParameter("numeroResolucion", dto.getNumeroResolucion());
            processStored.setParameter("idArchivo", dto.getIdArchivo());
            processStored.setParameter("proceso", dto.getProceso());
            processStored.setParameter("plazo", dto.getPlazo());
            processStored.setParameter("idMedida", dto.getIdMedida());
            processStored.setParameter("detalle", dto.getDetalle());
            processStored.setParameter("observacion", dto.getObservacion());
            processStored.setParameter("descripcion", dto.getDescripcion());
            processStored.setParameter("contrato", dto.getContrato());
            processStored.setParameter("idUsuarioRegistro", dto.getIdUsuarioRegistro());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se registró la medida correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    } 
 

    @Override
    public ResultClassEntity eliminarMedidaDetalle(MedidaDetalleDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_MedidaDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idMedidaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idMedidaDet", dto.getIdMedidaDet());
            processStored.setParameter("idUsuarioElimina", dto.getIdUsuarioElimina());
            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se eliminó medida correctamente.");
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    } 
 
}
