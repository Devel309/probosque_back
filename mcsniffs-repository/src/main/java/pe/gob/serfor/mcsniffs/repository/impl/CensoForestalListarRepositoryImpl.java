package pe.gob.serfor.mcsniffs.repository.impl;


import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;


import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.CensoForestalListarDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ContratoTHDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.CensoForestalListarRepository;
// 
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;



@Repository
public class CensoForestalListarRepositoryImpl extends JdbcDaoSupport implements CensoForestalListarRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;
    
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CensoForestalListarRepositoryImpl.class);

   @Override
   public List<CensoForestalListarDto> censoForestalListar(CensoForestalListarDto dto) throws Exception {
        
       List<CensoForestalListarDto> lista = new ArrayList<CensoForestalListarDto>();

       try {
           StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestalDetalle_AnexoC");            
           processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);

           SpUtil.enableNullParams(processStored);
           processStored.setParameter("idPlanManejo",dto.getIdPlanManejo());
           processStored.execute();

           List<Object[]> spResult = processStored.getResultList();
           if(spResult!=null){
            CensoForestalListarDto temp = null;
               if (spResult.size() >= 1){
                   for (Object[] row : spResult) {

                       temp = new CensoForestalListarDto();

                       temp.setIdPlanManejo(row[0]==null?null:(Integer) row[0]);
                       temp.setNombreComun(row[1]==null?null:(String) row[1]);
                       temp.setNombreCientifico(row[2]==null?null:(String) row[2]);
                       temp.setEsteUTM(row[3]==null?null:(String) row[3]);
                       temp.setNorteUTM(row[4]==null?null:(String) row[4]);
                       temp.setNroArbolesArea(row[5]==null?null:(Integer) row[5]);
                       temp.setProducto(row[6]==null?null:(String) row[6]);
                       temp.setProductoAprov(row[7]==null?null:(String) row[7]);
                       temp.setObservaciones(row[8]==null?null:(String) row[8]);
                       temp.setIdPlanManejoEspecieGrupo(row[9]==null?null:(Integer) row[9]);

                       lista.add(temp);
                   }
               }
           }

           return lista;
       } catch (Exception e) {
           log.error("listarContratoArchivo", e.getMessage());
           throw e;
       }
   }

    @Override
    public List<ContratoTHDto> listaTHContratoPorFiltro(ContratoTHDto param)  {
        List<ContratoTHDto> lista = new ArrayList<ContratoTHDto>();
        try {
            StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_TituloHabilitante_ObtenePorFiltro");
            proc.registerStoredProcedureParameter("tipoProceso", Integer.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("codigoTituloTh", String.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(proc);
            proc.setParameter("tipoProceso", param.getTipoProceso());
            proc.setParameter("codigoTituloTh", param.getCodigoTituloTh());
            proc.setParameter("pageNum", param.getPageNum());
            proc.setParameter("pageSize", param.getPageSize());
            proc.setParameter("idUsuario", param.getIdUsuarioRegistro());

            proc.execute();
            List<Object[]> spResult = proc.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ContratoTHDto temp = new ContratoTHDto();
                    temp.setIdContrato((Integer) row[0]);
                    temp.setCodigoContrato((String) row[1]);
                    temp.setUbigeo((String) row[2]);
                    temp.setTipoProceso((Integer) row[3]);
                    temp.setTotalRecord((Integer) row[4]);
                    lista.add(temp);

                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalListarRepositoryImpl - listaTHContratoPorFiltro", e.getMessage());
            throw e;
        }
    }
}
