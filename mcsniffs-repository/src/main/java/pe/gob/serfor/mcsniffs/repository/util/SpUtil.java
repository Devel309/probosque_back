package pe.gob.serfor.mcsniffs.repository.util;

import javax.persistence.Parameter;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.query.procedure.internal.ProcedureParameterImpl;

public class SpUtil {

	private SpUtil() {
	}

	public static void enableNullParams(StoredProcedureQuery sp) {
		if (sp == null || sp.getParameters() == null)
			return;

		for (Parameter<?> parameter : sp.getParameters()) {
			((ProcedureParameterImpl<?>) parameter).enablePassingNulls(true);
		}
	}

	public static Long toLong(Object o) {
		try {
			return Long.parseLong(String.valueOf(o));
		} catch (Exception e) {
			return null;
		}
	}

}
