package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralDemaRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.RegenteRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;
import pe.gob.serfor.mcsniffs.repository.util.Urls.regente;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Repository
public class InformacionGeneralDemaRepositoryImpl implements InformacionGeneralDemaRepository{

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {Repository  de la informacion general relacionada con el proceso DEMA}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(InformacionGeneralDemaRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    RegenteRepository regente;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {Registrar informacion general dema}
     *
     */
    @Override
    public ResultClassEntity registrarInformacionGeneralDema(InformacionGeneralDEMAEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_InformacionGeneral_RegistrarDema");
            processStored.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaElaboracion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroAnexos", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroResolucion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTituloPropiedad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTotalFamilias", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ruccomunidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaternoElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaternoElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("dniElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPersonasInvolucradas", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArbolesMaderables", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArboleMaderSemilleros", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficeMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumenMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArbolesNoMaderables", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArboleNoMaderSemilleros", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficeNoMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumenNoMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalIngresoEstimado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalCostoEstimado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalUtilidadesEstimado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPersonaComunidad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("aprovechamientoNoMaderable", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInformacionGeneralDema", Integer.class, ParameterMode.INOUT);
            processStored.registerStoredProcedureParameter("federacionComunidad", String.class, ParameterMode.IN);
            //para pfmi
            processStored.registerStoredProcedureParameter("vigencia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccionTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccionRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("representanteLegal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("celular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistritoTitular", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            //para PGMFA
            processStored.registerStoredProcedureParameter("areaTotal", BigDecimal.class, ParameterMode.IN);
            //Para POAC
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            //Para PFCR
            processStored.registerStoredProcedureParameter("codTipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoActor", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoCncc", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("esReprLegal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correoEmpresa", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaFinal", Integer.class, ParameterMode.IN);
            //POCC
            processStored.registerStoredProcedureParameter("codTipoDocumentoElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoDocumentoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaternoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaternoRepresentante", String.class, ParameterMode.IN);
            //PMFIC
            processStored.registerStoredProcedureParameter("areaBosqueProduccionForestal", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoTipo", obj.getCodigoProceso());
            processStored.setParameter("fechaElaboracion",  obj.getFechaElaboracionPmfi());
            processStored.setParameter("fechInicio", obj.getFechaInicioDema());
            processStored.setParameter("fechaFin", obj.getFechaFinDema());
            processStored.setParameter("cuenca", obj.getCuenca());
            processStored.setParameter("subCuenca", obj.getSubCuenca());
            processStored.setParameter("nroAnexos", obj.getNroAnexosComunidad());
            processStored.setParameter("nroResolucion", obj.getNroResolucionComunidad());
            processStored.setParameter("nroTituloPropiedad", obj.getNroTituloPropiedadComunidad());
            processStored.setParameter("nroTotalFamilias", obj.getNroTotalFamiliasComunidad());
            processStored.setParameter("ruccomunidad", obj.getNroRucComunidad());
            processStored.setParameter("nombreElaborador", obj.getNombreElaboraDema());
            processStored.setParameter("apellidoPaternoElaborador", obj.getApellidoPaternoElaboraDema());
            processStored.setParameter("apellidoMaternoElaborador", obj.getApellidoMaternoElaboraDema());
            processStored.setParameter("dniElaborador", obj.getDniElaboraDema());
            processStored.setParameter("nroPersonasInvolucradas", obj.getNroPersonasInvolucradas());
            processStored.setParameter("nroArbolesMaderables", obj.getNroArbolesMaderables());
            processStored.setParameter("nroArboleMaderSemilleros", obj.getNroArbolesMaderablesSemilleros());
            processStored.setParameter("superficeMaderables", obj.getSuperficieHaMaderables());
            processStored.setParameter("volumenMaderables", obj.getVolumenM3rMaderables());
            processStored.setParameter("nroArbolesNoMaderables", obj.getNroArbolesNoMaderables());
            processStored.setParameter("nroArboleNoMaderSemilleros", obj.getNroArbolesNoMaderablesSemilleros());
            processStored.setParameter("superficeNoMaderables", obj.getSuperficieHaNoMaderables());
            processStored.setParameter("volumenNoMaderables", obj.getVolumenM3rNoMaderables());
            processStored.setParameter("totalIngresoEstimado", obj.getTotalIngresosEstimado());
            processStored.setParameter("totalCostoEstimado", obj.getTotalCostoEstimado());
            processStored.setParameter("totalUtilidadesEstimado", obj.getTotalUtilidadesEstimado());
            processStored.setParameter("idPlanManejo", obj.getIdPlanManejo());
            processStored.setParameter("idPersonaComunidad", obj.getIdPersonaComunidad());
            processStored.setParameter("aprovechamientoNoMaderable", obj.getAprovechamientoNoMaderable());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            processStored.setParameter("idInformacionGeneralDema", obj.getIdInformacionGeneralDema());
            processStored.setParameter("federacionComunidad", obj.getFederacionComunidad());
            //para pfmi
            processStored.setParameter("vigencia", obj.getVigencia());
            processStored.setParameter("direccionTitular", obj.getDireccionLegalTitular());
            processStored.setParameter("direccionRepresentante", obj.getDireccionLegalRepresentante());
            processStored.setParameter("idContrato", (obj.getIdContrato()!=null? obj.getIdContrato().toString():null));
            processStored.setParameter("representanteLegal", obj.getRepresentanteLegal());
            processStored.setParameter("correo", obj.getCorreoTitular());
            processStored.setParameter("celular", obj.getCelularTitular());
            processStored.setParameter("idDistritoTitular", obj.getIdDistritoTitular());
            processStored.setParameter("idDistrito", obj.getIdDistritoRepresentante());
            //para PGMFA
            processStored.setParameter("areaTotal", obj.getAreaTotal());
            //Para POAC
            processStored.setParameter("detalle", obj.getDetalle());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("descripcion", obj.getDescripcion());
            //Para PFCR
            processStored.setParameter("codTipoDocumento", obj.getCodTipoDocumento());
            processStored.setParameter("codTipoPersona", obj.getCodTipoPersona());
            processStored.setParameter("codTipoActor", obj.getCodTipoActor());
            processStored.setParameter("codTipoCncc", obj.getCodTipoCncc());
            processStored.setParameter("esReprLegal", obj.getEsReprLegal());
            processStored.setParameter("correoEmpresa", obj.getCorreoEmpresa());

            processStored.setParameter("vigenciaFinal", obj.getVigenciaFinal());
            //POCC
            processStored.setParameter("codTipoDocumentoElaborador", obj.getCodTipoDocumentoElaborador());
            processStored.setParameter("codTipoDocumentoRepresentante", obj.getCodTipoDocumentoRepresentante());
            processStored.setParameter("documentoRepresentante", obj.getDocumentoRepresentante());
            processStored.setParameter("nombreRepresentante", obj.getNombreRepresentante());
            processStored.setParameter("apellidoPaternoRepresentante", obj.getApellidoPaternoRepresentante());
            processStored.setParameter("apellidoMaternoRepresentante", obj.getApellidoMaternoRepresentante());
            //PMFIC
            processStored.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            processStored.setParameter("areaBosqueProduccionForestal", obj.getAreaBosqueProduccionForestal());
            processStored.execute();  
            Integer id = (Integer) processStored.getOutputParameterValue("idInformacionGeneralDema");   
            //Registrar detalle
            if(obj.getLstUmf()!=null){
                for (InformacionGeneralDetalle deta : obj.getLstUmf()) {
                    deta.setIdInforGeneral(id);
                    ResultClassEntity salida = registrarInformacionGeneralDetalle(deta);
                    if(!salida.getSuccess()){
                        result.setInformacion(salida.getMessage());
                        break;
                    }
                }
            }
            ResultClassEntity rege = new ResultClassEntity<>();
            if(obj.getConRegente()!=null){
                if(obj.getConRegente()){
                rege = regente.RegistrarRegente(obj.getRegente());
              }
            }
            
            result.setInformacion("Se registró el detalle correctamente.");
            result.setCodigo(id);  
            result.setSuccess(true);
            result.setMessage("Se registró la información general.");
            result.setMessageExeption(rege.getMessage());
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error."+e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {actualizar informacion general dema}
     *
     */
    @Override
    public ResultClassEntity actualizarInformacionGeneralDema(InformacionGeneralDEMAEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_InformacionGeneral_ActualizarDema");
            processStored.registerStoredProcedureParameter("fechaElaboracion", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("cuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCuenca", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroAnexos", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroResolucion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTituloPropiedad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroTotalFamilias", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ruccomunidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaternoElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaternoElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("dniElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroPersonasInvolucradas", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArbolesMaderables", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArboleMaderSemilleros", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficeMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumenMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArbolesNoMaderables", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroArboleNoMaderSemilleros", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("superficeNoMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("volumenNoMaderables", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalIngresoEstimado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalCostoEstimado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("totalUtilidadesEstimado", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("aprovechamientoNoMaderable", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInformacionGeneralDema", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("federacionComunidad", String.class, ParameterMode.IN);
            //para pfmi
            processStored.registerStoredProcedureParameter("vigencia", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccionTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("direccionRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idContrato", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("representanteLegal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("correo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("celular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistritoTitular", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idDistrito", Integer.class, ParameterMode.IN);
            //para PGMFA
            processStored.registerStoredProcedureParameter("areaTotal", BigDecimal.class, ParameterMode.IN);
            //Para POAC
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaFinal", Integer.class, ParameterMode.IN);
            //POCC
            processStored.registerStoredProcedureParameter("codTipoDocumentoElaborador", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoDocumentoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("documentoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoPaternoRepresentante", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("apellidoMaternoRepresentante", String.class, ParameterMode.IN);
            //PMFIC
            processStored.registerStoredProcedureParameter("idPermisoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaBosqueProduccionForestal", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoPersona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("esReprLegal", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("zona", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("datum", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("horizontal", String.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("fechaElaboracion",  obj.getFechaElaboracionPmfi());
            processStored.setParameter("fechInicio", obj.getFechaInicioDema());
            processStored.setParameter("fechaFin", obj.getFechaFinDema());
            processStored.setParameter("cuenca", obj.getCuenca());
            processStored.setParameter("subCuenca", obj.getSubCuenca());
            processStored.setParameter("nroAnexos", obj.getNroAnexosComunidad());
            processStored.setParameter("nroResolucion", obj.getNroResolucionComunidad());
            processStored.setParameter("nroTituloPropiedad", obj.getNroTituloPropiedadComunidad());
            processStored.setParameter("nroTotalFamilias", obj.getNroTotalFamiliasComunidad());
            processStored.setParameter("ruccomunidad", obj.getNroRucComunidad());
            processStored.setParameter("nombreElaborador", obj.getNombreElaboraDema());
            processStored.setParameter("apellidoPaternoElaborador", obj.getApellidoPaternoElaboraDema());
            processStored.setParameter("apellidoMaternoElaborador", obj.getApellidoMaternoElaboraDema());
            processStored.setParameter("dniElaborador", obj.getDniElaboraDema());
            processStored.setParameter("nroPersonasInvolucradas", obj.getNroPersonasInvolucradas());
            processStored.setParameter("nroArbolesMaderables", obj.getNroArbolesMaderables());
            processStored.setParameter("nroArboleMaderSemilleros", obj.getNroArbolesMaderablesSemilleros());
            processStored.setParameter("superficeMaderables", obj.getSuperficieHaMaderables());
            processStored.setParameter("volumenMaderables", obj.getVolumenM3rMaderables());
            processStored.setParameter("nroArbolesNoMaderables", obj.getNroArbolesNoMaderables());
            processStored.setParameter("nroArboleNoMaderSemilleros", obj.getNroArbolesNoMaderablesSemilleros());
            processStored.setParameter("superficeNoMaderables", obj.getSuperficieHaNoMaderables());
            processStored.setParameter("volumenNoMaderables", obj.getVolumenM3rNoMaderables());
            processStored.setParameter("totalIngresoEstimado", obj.getTotalIngresosEstimado());
            processStored.setParameter("totalCostoEstimado", obj.getTotalCostoEstimado());
            processStored.setParameter("totalUtilidadesEstimado", obj.getTotalUtilidadesEstimado());
            processStored.setParameter("aprovechamientoNoMaderable", obj.getAprovechamientoNoMaderable());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioRegistro());
            processStored.setParameter("idInformacionGeneralDema", obj.getIdInformacionGeneralDema());
            processStored.setParameter("federacionComunidad", obj.getFederacionComunidad());
            //para pfmi
            processStored.setParameter("vigencia", obj.getVigencia());
            processStored.setParameter("direccionTitular", obj.getDireccionLegalTitular());
            processStored.setParameter("direccionRepresentante", obj.getDireccionLegalRepresentante());
            processStored.setParameter("idContrato", (obj.getIdContrato()!=null? obj.getIdContrato().toString():null));
            processStored.setParameter("representanteLegal", obj.getRepresentanteLegal());
            processStored.setParameter("correo", obj.getCorreoTitular());
            processStored.setParameter("celular", obj.getCelularTitular());
            processStored.setParameter("idDistritoTitular", obj.getIdDistritoTitular());
            processStored.setParameter("idDistrito", obj.getIdDistritoRepresentante());
            //para PGMFA
            processStored.setParameter("areaTotal", obj.getAreaTotal());
            //Para POAC
            processStored.setParameter("detalle", obj.getDetalle());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("vigenciaFinal", obj.getVigenciaFinal());
            //POCC
            processStored.setParameter("codTipoDocumentoElaborador", obj.getCodTipoDocumentoElaborador());
            processStored.setParameter("codTipoDocumentoRepresentante", obj.getCodTipoDocumentoRepresentante());
            processStored.setParameter("documentoRepresentante", obj.getDocumentoRepresentante());
            processStored.setParameter("nombreRepresentante", obj.getNombreRepresentante());
            processStored.setParameter("apellidoPaternoRepresentante", obj.getApellidoPaternoRepresentante());
            processStored.setParameter("apellidoMaternoRepresentante", obj.getApellidoMaternoRepresentante());
            //PMFIC
            processStored.setParameter("idPermisoForestal", obj.getIdPermisoForestal());
            processStored.setParameter("areaBosqueProduccionForestal", obj.getAreaBosqueProduccionForestal());
            processStored.setParameter("codTipoPersona", obj.getCodTipoPersona());
            processStored.setParameter("esReprLegal", obj.getEsReprLegal());
            processStored.setParameter("zona", obj.getZona());
            processStored.setParameter("datum", obj.getDatum());
            processStored.setParameter("horizontal", obj.getHorizontal());

            processStored.execute();  
            //Registrar detalle
            if(obj.getLstUmf()!=null){
                for (InformacionGeneralDetalle deta : obj.getLstUmf()) {
                    ResultClassEntity salida = actualizarInformacionGeneralDetalle(deta);
                    if(!salida.getSuccess()){
                        result.setInformacion(salida.getMessage());
                        break;
                    }
                }
            }

            ResultClassEntity rege = new ResultClassEntity<>();
            if(obj.getConRegente()!=null){
                if(obj.getConRegente()){
                rege = regente.RegistrarRegente(obj.getRegente());
              }
            }

            result.setInformacion("Se actualizó el detalle correctamente.");
            result.setCodigo(obj.getIdInformacionGeneralDema());  
            result.setSuccess(true);
            result.setMessage("Se actualizó la información general.");
            result.setMessageExeption(rege.getMessage());


            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_INFORMACIONGENERAL,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_INFORMACIONGENERAL + obj.getIdInformacionGeneralDema(),
                    obj.getIdUsuarioRegistro());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {listar informacion general dema}
     *
     */
    @Override
    public ResultEntity<InformacionGeneralDEMAEntity> listarInformacionGeneralDema(
            InformacionGeneralDEMAEntity filtro) {
        ResultEntity<InformacionGeneralDEMAEntity> result = new ResultEntity<>();
        try{
            List<InformacionGeneralDEMAEntity> list = new ArrayList<>();
                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_InformacionGeneral_listarDema");
                processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idInformacionGeneral", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idPlanManejo", filtro.getIdPlanManejo());
                processStored.setParameter("codigoProceso", filtro.getCodigoProceso());
                processStored.setParameter("idInformacionGeneral", filtro.getIdInformacionGeneralDema());
                processStored.execute();

                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        InformacionGeneralDEMAEntity obj = new InformacionGeneralDEMAEntity();
                        obj.setIdInformacionGeneralDema((Integer) row[0]);
                        obj.setCodigoProceso((String) row[1]);
                        obj.setFechaInicioDema((Date) row[2]);
                        obj.setFechaFinDema((Date) row[3]);
                        obj.setCuenca((String) row[4]);
                        obj.setSubCuenca((String) row[5]);
                        obj.setNroAnexosComunidad((Integer) row[6]);
                        obj.setNroResolucionComunidad((String) row[7]);
                        obj.setNroTituloPropiedadComunidad((String) row[8]);
                        obj.setNroTotalFamiliasComunidad((Integer) row[9]);
                        obj.setNroRucComunidad((String) row[10]);
                        obj.setNombreElaboraDema((String) row[11]);
                        obj.setApellidoPaternoElaboraDema((String) row[12]);
                        obj.setApellidoMaternoElaboraDema((String) row[13]);
                        obj.setDniElaboraDema((String) row[14]);
                        obj.setNroPersonasInvolucradas((Integer) row[15]);
                        obj.setNroArbolesMaderables((Integer) row[16]);
                        obj.setNroArbolesMaderablesSemilleros((Integer) row[17]);
                        obj.setSuperficieHaMaderables((String) row[18]);
                        obj.setVolumenM3rMaderables((String) row[19]);
                        obj.setNroArbolesNoMaderables((Integer) row[20]);
                        obj.setNroArbolesNoMaderablesSemilleros((Integer) row[21]);
                        obj.setSuperficieHaNoMaderables((String) row[22]);
                        obj.setVolumenM3rNoMaderables((String) row[23]);
                        obj.setTotalIngresosEstimado((BigDecimal) row[24]);
                        obj.setTotalCostoEstimado((BigDecimal) row[25]);
                        obj.setTotalUtilidadesEstimado((BigDecimal) row[26]);
                        obj.setIdPlanManejo((Integer) row[27]);
                        obj.setIdPersonaComunidad((Integer) row[28]);
                        obj.setAprovechamientoNoMaderable((Boolean) row[29]);
                        obj.setFederacionComunidad(((String) row[30]));
                        obj.setDniJefecomunidad(((String) row[31]));
                        obj.setFechaElaboracionPmfi((Date) row[32]);
                        ResultEntity<InformacionGeneralDetalle> listdetalle=listarInformacionGeneralDetalle(obj.getIdInformacionGeneralDema());
                        obj.setLstUmf(listdetalle.getData());
                        RegenteEntity param = new RegenteEntity();
                        param.setIdPlanManejo(obj.getIdPlanManejo());
                        param.setCodigoProceso(obj.getCodigoProceso());
                        ResultEntity<RegenteEntity> lstregente= regente.ListarRegente(param);
                        if(lstregente.getData()!=null){
                            if(lstregente.getData().size()>0){
                                obj.setRegente(lstregente.getData().get(0));
                            }
                        }
                        //proceso pmfi
                        obj.setVigencia((Integer) row[33]);
                        obj.setDireccionLegalTitular((String) row[34]);
                        obj.setDireccionLegalRepresentante((String) row[35]);
                        String contrato= row[36]!=null? (String) row[36]:null;
                        obj.setIdContrato(contrato!=null?Integer.parseInt(contrato):null);
                        obj.setRepresentanteLegal((String) row[37]);
                        obj.setCorreoTitular((String) row[38]);
                        obj.setCelularTitular((String) row[39]);
                        obj.setIdDistritoTitular(((Integer) row[40]));
                        obj.setIdDistritoRepresentante((Integer) row[41]);
                        obj.setDistrito((String) row[42]);
                        obj.setProvincia((String) row[43]);
                        obj.setDepartamento((String) row[44]);
                        obj.setDistritoRepresentante((String) row[45]);
                        obj.setProvinciaRepresentante((String) row[46]);
                        obj.setDepartamentoRepresentante((String) row[47]);
                        //proceso PGMFA
                        obj.setAreaTotal((BigDecimal) row[48]);
                        //Para POAC
                        obj.setDetalle((String) row[49]);
                        obj.setObservacion((String) row[50]);
                        obj.setDescripcion((String) row[51]);
                        //Para PFCR
                        obj.setCodTipoDocumento((String) row[52]);
                        obj.setTipoDocumento((String) row[53]);
                        obj.setCodTipoPersona((String) row[54]);
                        obj.setTipoPersona((String) row[55]);
                        obj.setCodTipoActor((String) row[56]);
                        obj.setTipoActor((String) row[57]);
                        obj.setCodTipoCncc((String) row[58]);
                        obj.setTipoCncc((String) row[59]);
                        obj.setEsReprLegal(String.valueOf((Character) row[60]));
                        obj.setCorreoEmpresa((String) row[61]);
                        obj.setVigenciaFinal((Integer) row[62]);
                        //POAC
                        obj.setIdDistrito(row[63] == null ? null : (Integer) row[63]);
                        obj.setIdProvincia(row[64] == null ? null : (Integer) row[64]);
                        obj.setIdDepartamento(row[65] == null ? null : (Integer) row[65]);
                        //POCC
                        obj.setCodTipoDocumentoElaborador(row[66] == null ? null : (String) row[66]);
                        obj.setTipoDocumentoElaborador(row[67] == null ? null : (String) row[67]);
                        obj.setCodTipoDocumentoRepresentante(row[68] == null ? null : (String) row[68]);
                        obj.setTipoDocumentoRepresentante(row[69] == null ? null : (String) row[69]);
                        obj.setDocumentoRepresentante(row[70] == null ? null : (String) row[70]);
                        obj.setNombreRepresentante(row[71] == null ? null : (String) row[71]);
                        obj.setApellidoPaternoRepresentante(row[72] == null ? null : (String) row[72]);
                        obj.setApellidoMaternoRepresentante(row[73] == null ? null : (String) row[73]);
                        //PMFIC
                        obj.setIdPermisoForestal(row[74] == null ? null : (Integer) row[74]);
                        obj.setAreaBosqueProduccionForestal(row[75] == null ? null : (BigDecimal) row[75]);

                        obj.setZona(row[76] == null ? null : (String) row[76]);
                        obj.setDatum(row[77] == null ? null : (String) row[77]);
                        obj.setHorizontal(row[78] == null ? null : (String) row[78]);
                        obj.setIdProvinciaTitular(row[79] == null ? null : (Integer) row[79]);
                        obj.setIdDepartamentoTitular(row[80] == null ? null : (Integer) row[80]);
                        obj.setNombreTituloHabilitante(row[81] == null ? "" : (String) row[81]);
                        list.add(obj);
                    }
                }
            
            result.setData(list);
            result.setIsSuccess(true);
            result.setMessage("Se listó la información general.");
            return  result;
        }
        catch (Exception e){
            log.error("listarInformacionGeneralDema - "+e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity registrarInformacionGeneralDetalle(InformacionGeneralDetalle obj) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_InformacionGeneralDetalle_Registrar");
            processStored.registerStoredProcedureParameter("idInformacionGeneral", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoInformacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoInformacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaFinalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInformacionDetalle", Integer.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idInformacionGeneral", obj.getIdInforGeneral());
            processStored.setParameter("codigo", obj.getCodigo());
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("detalle", obj.getDetalle());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());
            processStored.setParameter("codigoInformacionDet", obj.getCodigoInformacionDet());
            processStored.setParameter("tipoInformacionDet", obj.getTipoInformacionDet());
            processStored.setParameter("vigenciaDet", obj.getVigenciaDet());
            processStored.setParameter("vigenciaFinalDet", obj.getVigenciaFinalDet());
            processStored.setParameter("area", obj.getArea());
            processStored.setParameter("idInformacionDetalle", obj.getIdInforGeneDetalle());
            processStored.execute();  
            Integer id = (Integer) processStored.getOutputParameterValue("idInformacionDetalle");             
            result.setCodigo(id);  
            result.setSuccess(true);
            result.setMessage("Se registró el detalle de la información general.");
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity actualizarInformacionGeneralDetalle(InformacionGeneralDetalle obj) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_InformacionGeneralDetalle_actualizar");
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoInformacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoInformacionDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("vigenciaFinalDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("area", BigDecimal.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idInformacionDetalle", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("descripcion", obj.getDescripcion());
            processStored.setParameter("detalle", obj.getDetalle());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());
            processStored.setParameter("codigoInformacionDet", obj.getCodigoInformacionDet());
            processStored.setParameter("tipoInformacionDet", obj.getTipoInformacionDet());
            processStored.setParameter("vigenciaDet", obj.getVigenciaDet());
            processStored.setParameter("vigenciaFinalDet", obj.getVigenciaFinalDet());
            processStored.setParameter("area", obj.getArea());
            processStored.setParameter("idInformacionDetalle", obj.getIdInforGeneDetalle());
            processStored.execute();    
            result.setCodigo(obj.getIdInforGeneDetalle());  
            result.setSuccess(true);
            result.setMessage("Se actualizó el detalle de la información general.");
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultEntity<InformacionGeneralDetalle> listarInformacionGeneralDetalle(Integer IdInformacionGeneral) {
        ResultEntity<InformacionGeneralDetalle> result = new ResultEntity<>();
        try{
            List<InformacionGeneralDetalle> list = new ArrayList<>();
                StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_InformacionGeneralDetalle_listarDema");
                processStored.registerStoredProcedureParameter("idInformacionGeneral", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored);
                processStored.setParameter("idInformacionGeneral", IdInformacionGeneral);
                processStored.execute();
                List<Object[]> spResult =processStored.getResultList();
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        InformacionGeneralDetalle obj = new InformacionGeneralDetalle();
                        obj.setIdInforGeneDetalle((Integer) row[0]);
                        obj.setIdInforGeneral((Integer) row[1]);
                        obj.setCodigo((String) row[2]);
                        obj.setDescripcion((String) row[3]);
                        obj.setDetalle((String) row[4]);
                        obj.setObservacion((String) row[5]);
                        obj.setCodigoInformacionDet(row[6]==null?null:(String) row[6]);
                        obj.setTipoInformacionDet(row[7]==null?null:(String) row[7]);
                        obj.setVigenciaDet(row[8]==null?null:(Integer) row[8]);
                        obj.setVigenciaFinalDet(row[9]==null?null:(Integer) row[9]);
                        obj.setArea(row[10]==null?null:(BigDecimal) row[10]);
                        list.add(obj);
                    }
                }
            
            result.setData(list);
            result.setIsSuccess(true);
            result.setMessage("Se listo el detalle de la información general.");
            return  result;
        }
        catch (Exception e){
            log.error("registrarInformacionGeneralDetalle - "+e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Danny Nazario [19-01-2022]
     * @modificado: Danny Nazario [04-02-2022]
     * @descripción: {Listar Información General por filtro}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarPorFiltroInformacionGeneral(String nroDocumento, String nroRucEmpresa, String codTipoPlan, Integer idPlanManejo) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<InformacionGeneralDEMAEntity> list = new ArrayList<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_InformacionGeneral_ListarPorFiltro");
            sp.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroRucEmpresa", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codTipoPlan", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("nroDocumento", nroDocumento);
            sp.setParameter("nroRucEmpresa", nroRucEmpresa);
            sp.setParameter("codTipoPlan", codTipoPlan);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.execute();
            List<Object[]> spResult = sp.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    InformacionGeneralDEMAEntity obj = new InformacionGeneralDEMAEntity();
                    obj.setIdInformacionGeneralDema(row[0] == null ? null : (Integer) row[0]);
                    obj.setIdPlanManejo(row[1] == null ? null : (Integer) row[1]);
                    obj.setCodigoProceso(row[2] == null ? null : (String) row[2]);
                    obj.setCodTipoDocumentoElaborador(row[3] == null ? null : (String) row[3]);
                    obj.setTipoDocumentoElaborador(row[4] == null ? null : (String) row[4]);
                    obj.setDniElaboraDema(row[5] == null ? null : (String) row[5]);
                    obj.setNombreElaboraDema(row[6] == null ? null : (String) row[6]);
                    obj.setApellidoPaternoElaboraDema(row[7] == null ? null : (String) row[7]);
                    obj.setApellidoMaternoElaboraDema(row[8] == null ? null : (String) row[8]);
                    obj.setCodTipoDocumentoRepresentante(row[9] == null ? null : (String) row[9]);
                    obj.setTipoDocumentoRepresentante(row[10] == null ? null : (String) row[10]);
                    obj.setDocumentoRepresentante(row[11] == null ? null : (String) row[11]);
                    obj.setNombreRepresentante(row[12] == null ? null : (String) row[12]);
                    obj.setApellidoPaternoRepresentante(row[13] == null ? null : (String) row[13]);
                    obj.setApellidoMaternoRepresentante(row[14] == null ? null : (String) row[14]);
                    obj.setRepresentanteLegal(row[15] == null ? null : (String) row[15]);
                    obj.setNroRucComunidad(row[16] == null ? null : (String) row[16]);
                    obj.setAreaTotal(row[17] == null ? null : (BigDecimal) row[17]);
                    obj.setDistrito(row[18] == null ? null : (String) row[18]);
                    obj.setProvincia(row[19] == null ? null : (String) row[19]);
                    obj.setDepartamento(row[20] == null ? null : (String) row[20]);
                    obj.setIdDistrito(row[21] == null ? null : (Integer) row[21]);
                    obj.setIdProvincia(row[22] == null ? null : (Integer) row[22]);
                    obj.setIdDepartamento(row[23] == null ? null : (Integer) row[23]);
                    obj.setCodTipoPersona(row[24] == null ? null : (String) row[24]);
                    obj.setTipoPersona(row[25] == null ? null : (String) row[25]);
                    obj.setEsReprLegal(row[26] == null ? null : String.valueOf((Character) row[26]));
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("Se listó información correctamente.");
            return result;
        } catch (Exception e) {
            log.error("listarPorFiltroInformacionGeneral - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

}
