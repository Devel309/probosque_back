package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalBeneficiarioEntity;


public interface SolicitudBosqueLocalBeneficarioRepository {

    ResultClassEntity listarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj);

    //ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj);

    ResultClassEntity eliminarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity obj) ;

    ResultClassEntity registrarSolicitudBosqueLocalBeneficiario(List<SolicitudBosqueLocalBeneficiarioEntity> obj) throws Exception;
    
}
