package pe.gob.serfor.mcsniffs.repository.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Constantes implements Serializable {

	private static final long serialVersionUID = 1L;

	public final static String MESSAGE_ERROR_500 = "Estamos teniendo problemas. Por favor comuníquese con el administrador del sistema.";

	public static final String DRIVER="org.sqlite.JDBC";  
	/**
	 * FORMATO DE FECHAS
	 **/
	public final static String HORA_FORMATO = "HH:mm:ss";
	public final static String FECHA_FORMATO = "dd/MM/yyyy";
	public final static String FECHA_FORMATO_HORAS = "dd/MM/yyyy HH:mm:ss";
	public final static String HORA_VALUE_INICIO_DEFAULT = "00:00:00";
	public final static String HORA_VALUE_FINAL_DEFAULT = "23:59:59";

	public static final Character ESTADO_ACTIVO = '1';
	public static final Character ESTADO_INACTIVO = '0';
	public static final String CODIGO_EMPRESA_DEFAULT = "99";

	public static final String MENSAJE_SIN_CODIGO = "Debe generar un código.";
	public static final String MENSAJE_CODIGO_DISTINTO = "El código ingresado es incorrecto.";
	public static final String MENSAJE_CODIGO_GENERADO_CORRECTAMENTE = "El código generado se envió a su correo institucional.";
	public static final String MENSAJE_DNI_NO_ENCONTRADO = "No se encontró coincidencias con el número de DNI ingresado.";
	public static final String MENSAJE_CODIGO_EXISTE = "Ya cuenta con un código generado el día de hoy, verifique su correo.";
	public static final String MENSAJE_REGISTRO_MARCACION_CORRECTAMENTE = "Se registró su marcación, correctamente.";

	public final static String MESSAGE_SUCCESS = "Operation Success! ";
	public final static String MESSAGE_ERROR = "Operation Failed! ";

	public final static boolean STATUS_SUCCESS = Boolean.TRUE;
	public final static boolean STATUS_ERROR = Boolean.FALSE;

	public final static String MENSAJE_CONSULTA_SIN_RESULTADO = "No se encontraron coincidencias con los parámetros ingresados.";

	public static final String ACTIVO = "A";

	public static final String HORARIO_TIPO_01 = "33";
	public static final String HORARIO_TIPO_02 = "34";
	public static final String HORARIO_TIPO_03 = "35";
	public static final String HORARIO_TIPO_04 = "36";

	public static final String BODY_HTML_EMAIL = "<style>\n" +
			" table {\n" +
			" border-collapse: collapse;\n" +
			" }\n" +
			" \n" +
			" table.Listado ,table.Listado th,table.Listado td\n" +
			"{\n" +
			"   border: 1px solid black;\n" +
			"   text-align: Center;\n" +
			"}\n" +
			"\n" +
			".firma{\n" +
			"\tcolor: #0060a0;\n" +
			"}\n" +
			"\n" +
			".negrita{\n" +
			"\tfont-weight: bold;\n" +
			"}\n" +
			"\n" +
			".titulo{\n" +
			"\t width:200px;\n" +
			"\t background-color: #0060a0;\n" +
			"\t color: #FFF;\n" +
			"}\n" +
			"\n" +
			"</style>\n" +
			"\n" +
			"<table>\n" +
			"\t<tr>\n" +
			"\t\t<td>Estimado(s) <span class=\"negrita\">[NOMBRES_USUARIO_ACTUAL]</span>,</td>\n" +
			"\t</tr>\t\n" +
			"\t<tr>\n" +
			"\t\t<td>\n" +
			"        <br>Se comunica lo siguiente:\n" +
			"\t\t<br>[MENSAJE_DEL_PROCESO]\n" +
			"\t\t</td>\n" +
			"\t</tr>\n" +
			"\t\n" +
			"\t<tr>\n" +
			"\t\t<td>&nbsp</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td>\n" +
			"\t\tNota:\n" +
			"\t\t<br>[NOTA_COMPLEMENTARIA]\n" +
			"\t\t<!-- <br>Tenga presente que usted est&aacute; recibiendo este correo porque ha seleccionado la opci&oacute;n \"Env&iacute;o de Password\" en el aplicativo MCSNIFFS. -->\n" +
			"\t\t</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td>&nbsp</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td class=\"negrita\">Atentamente,</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td>&nbsp</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td class=\"firma\">Departamento de Sistemas</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td class=\"firma\">MCSNIFFS</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td>&nbsp</td>\n" +
			"\t</tr>\n" +
			"\t<tr>\n" +
			"\t\t<td>P.D.: No necesita responder este correo debido a que ha sido generado por un proceso autom&aacute;tico.</td>\n" +
			"\t</tr>\n" +
			"</table>";


}


