package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ImpactoAmbientalRepository {
    
    ResultClassEntity ListarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) throws Exception;
    ResultClassEntity ListarImpactoAmbientalDetalle(ImpactoAmbientalEntity impactoAmbientalEntity) throws Exception;

    ResultClassEntity RegistrarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity) ;
    ResultClassEntity RegistrarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity);

    ResultClassEntity ActualizarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity);
    ResultClassEntity ActualizarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity);

    ResultClassEntity EliminarImpactoAmbiental(ImpactoAmbientalEntity impactoAmbientalEntity);
    ResultClassEntity EliminarImpactoAmbientalDetalle(ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity);


    ResultClassEntity ListarImpactoAmbientalPmfi(MedidasPmfiDto medidasPmfiDto) throws Exception;

    ResultClassEntity RegistrarImpactoAmbientalPmfi(ImpactoAmbientalPmfiDto impactoAmbientalPmfiDto) throws Exception;

}
