package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;

import java.util.List;

public interface CoreCentralRepository {

    List<AutoridadForestalEntity> ListarPorFiltroAutoridadForestal(AutoridadForestalEntity param) throws Exception;
    List<TipoComunidadEntity> ListarPorFiltroTipoComunidad (TipoComunidadEntity param) throws Exception;
    List<TipoTrasporteEntity> ListarPorFiltroTipoTrasporte (TipoTrasporteEntity param) throws Exception;
    List<TipoZonaEntity> listarPorFiltroTipoZona(TipoZonaEntity param) throws Exception;
    List<DepartamentoEntity> ListarPorFiltroDepartamentos(DepartamentoEntity param) throws Exception;
    //IVAN
    List<TipoBosqueEntity> listarPorFiltroTipoBosque(TipoBosqueEntity tipoBosque) throws Exception;
    List<TipoConcesionEntity> listarPorFiltroTipoConcesion(TipoConcesionEntity tipoConcesion) throws Exception;
    List<TipoContratoEntity> listarPorFiltroTipoContrato(TipoContratoEntity tipoContrato) throws Exception;
    List<TipoDocGestionErgtfEntity> listarPorFiltroTipoDocGestionErgtf(TipoDocGestionErgtfEntity tipoDocGestionErgtf) throws Exception;
    List<TipoDocGestionEntity> listarPorFiltroTipoDocGestion(TipoDocGestionEntity tipoDocGestion) throws Exception;
    List<TipoDocumentoEntity> listarPorFiltroTipoDocumento(TipoDocumentoEntity tipoDocumento) throws Exception;
    List<TipoEscalaEntity> listarPorFiltroTipoEscala(TipoEscalaEntity tipoEscala) throws Exception;
    List<TipoMonedaEntity> listarPorFiltroTipoMoneda(TipoMonedaEntity tipoMoneda) throws Exception;
    List<TipoOficinaEntity> listarPorFiltroTipoOficina(TipoOficinaEntity tipoOficina) throws Exception;
    List<TipoPagoEntity> listarPorFiltroTipoPago(TipoPagoEntity tipoPago) throws Exception;
    List<TipoPermisoEntity> listarPorFiltroTipoPermiso(TipoPermisoEntity tipoPermiso) throws Exception;
    List<TipoProcedimientoConcesEntity> listarPorFiltroTipoProcedimientoConces(TipoProcedimientoConcesEntity tipoProcedimientoConces) throws Exception;
    List<TipoProductoEntity> listarPorFiltroTipoProducto(TipoProductoEntity tipoProducto) throws Exception;
    List<TipoRegenteEntity> listarPorFiltroTipoRegente(TipoRegenteEntity tipoRegente) throws Exception;
    List<UbigeoArffsEntity> listarPorFiltroUbigeoArffs(UbigeoArffsEntity ubigeoArffs) throws Exception;
    List<UnidadMedidaEntity> listarPorFiltroUnidadMedida(UnidadMedidaEntity UnidadMedida) throws Exception;
    List<RegenteComunidadEntity> listarPorFiltroRegenteComunidad(RegenteComunidadEntity regenteComunidad) throws Exception;
    //Jaqueline DB :3
    ResultEntity<ProvinciaEntity> listarPorFilroProvincia(ProvinciaEntity provincia);
    ResultEntity<DistritoEntity> listarPorFilroDistrito(DistritoEntity distrito);


    ResultEntity<EspecieForestalEntity> ListaPorFiltroEspecieForestal(EspecieForestalEntity request);
    ResultEntity<EspecieFaunaEntity> ListaPorFiltroEspecieFauna(EspecieFaunaEntity request);

    //List<EspecieForestalEntity>ListaEspecieForestalPorFiltro(EspecieForestalEntity param) throws Exception;
    ResultEntity ListaEspecieForestalPorFiltro(EspecieForestalEntity request);
    ResultEntity ListaEspecieFaunaPorFiltro(EspecieFaunaEntity request);
    ResultClassEntity listarEspecieForestalSincronizacion(EspecieForestalEntity param);
    ResultClassEntity listarCuencaSubcuenca(CuencaEntity param);
}
