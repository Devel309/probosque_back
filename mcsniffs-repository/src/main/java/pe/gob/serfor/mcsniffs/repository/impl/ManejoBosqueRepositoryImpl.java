package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManejoBosqueRepositoryImpl extends JdbcDaoSupport implements ManejoBosqueRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ManejoBosqueRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarManejoBosque(ManejoBosqEntity manejoBosque) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosque_Registrar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", manejoBosque.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idManejoBosque", manejoBosque.getIdManejoBosque());
            processStored.setParameter("idTipo", manejoBosque.getIdTipo());
            processStored.setParameter("accion", manejoBosque.getAccion());
            processStored.setParameter("descripcion", manejoBosque.getDescripcion());
            processStored.setParameter("estado", manejoBosque.getEstado());
            processStored.setParameter("idUsuarioRegistro", manejoBosque.getIdUsuarioRegistro());
            processStored.execute();
            ManejoBosqEntity obj = new ManejoBosqEntity() ;

            String des="";
            String idTipo="";
            idTipo=manejoBosque.getIdTipo();
            if(idTipo.equals("1")){
                des="Sistema de manejo correctamente.";
            }
            else{
                des="Producción forestal correctamente.";
            }

            List<Object[]> spResult =processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoEntity plan = new PlanManejoEntity();
                    plan.setIdPlanManejo((Integer) row[0]);
                    obj.setPlanManejo(plan);
                    obj.setIdManejoBosque((Integer) row[1]);
                    obj.setIdTipo((String) row[2]);
                    obj.setAccion((Boolean) row[3]);
                    obj.setDescripcion((String) row[4]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);

            result.setMessage("Se registró "+des);
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public ResultClassEntity ObtenerManejoBosque(ManejoBosqEntity manejoBosque) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try{
            List<ManejoBosqEntity> list = new ArrayList<ManejoBosqEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosque_Obtener");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", manejoBosque.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idTipo",manejoBosque.getIdTipo());
            processStored.execute();
            List<Object[]> spResult_ =processStored.getResultList();
            ManejoBosqEntity obj = new ManejoBosqEntity() ;
            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    PlanManejoEntity plan = new PlanManejoEntity();
                    plan.setIdPlanManejo((Integer) row[0]);
                    obj.setPlanManejo(plan);
                    obj.setIdManejoBosque((Integer) row[1]);
                    obj.setIdTipo((String) row[2]);
                    obj.setAccion((Boolean) row[3]);
                    obj.setDescripcion((String) row[4]);
                }
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("Se obtuvo manejo de bosque.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {Listado de cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueDto
     */

    @Override
    public List<ManejoBosqueEntity> ListarManejoBosque(Integer idPlanManejo, String codigo,String subCodigoGeneral,String subCodigoGeneralDet) throws Exception {
        List<ManejoBosqueEntity> lista = new ArrayList<ManejoBosqueEntity>();
        try {
            int idDetalle=0;
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosque_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoGeneral", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodigoGeneral", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigoGeneral", codigo);
            processStored.setParameter("subCodigoGeneral", subCodigoGeneral);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row2 : spResult) {

                        ManejoBosqueEntity temp2 = new ManejoBosqueEntity();
                        temp2.setIdManejoBosque((Integer) row2[0]);
                        temp2.setTipo((String) row2[1]);
                        temp2.setCodigoManejo((String) row2[2]);
                        temp2.setSubCodigoManejo((String) row2[3]);
                        temp2.setDescripcionCab((String) row2[4]);
                        temp2.setIdPlanManejo((Integer) row2[5]);
                        temp2.setTxDetalle((String) row2[6]);
                        temp2.setTxObservacion((String) row2[7]);
                        temp2.setCodigoEscala(row2[8]==null? null: (String) row2[8]);
                        temp2.setAnio(row2[9]==null? null: (Integer) row2[9]);
                        if(row2[0]==null){
                            idDetalle=0;
                        }
                        else{
                            idDetalle=(Integer) row2[0];
                        }

                        /********************************** DETALLE **********************************************/

                        List<ManejoBosqueDetalleEntity> listDet = new ArrayList<ManejoBosqueDetalleEntity>();
                        StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueDetalle_Listar");
                        pa.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
                        pa.registerStoredProcedureParameter("codigoGeneral", String.class, ParameterMode.IN);
                        pa.registerStoredProcedureParameter("codigoGeneralDet", String.class, ParameterMode.IN);
                        pa.registerStoredProcedureParameter("subCodigoGeneralDet", String.class, ParameterMode.IN);
                        SpUtil.enableNullParams(pa);
                        if(idDetalle==0){pa.setParameter("idManejoBosque", null); }
                        else{pa.setParameter("idManejoBosque", idDetalle);}
                        pa.setParameter("codigoGeneral", codigo);
                        pa.setParameter("codigoGeneralDet", subCodigoGeneral);
                        pa.setParameter("subCodigoGeneralDet", subCodigoGeneralDet);
                        pa.execute();
                        List<Object[]> spResultDet = pa.getResultList();
                        if(spResultDet!=null) {
                            if (spResultDet.size() >= 1) {
                                for (Object[] row : spResultDet) {
                                    ManejoBosqueDetalleEntity temp = new ManejoBosqueDetalleEntity();
                                    temp.setIdManejoBosqueDet((Integer) row[0]);
                                    temp.setCodtipoManejoDet((String) row[1]);
                                    temp.setSubCodtipoManejoDet((String) row[2]);
                                    temp.setCatOrdenamiento((String) row[3]);
                                    temp.setOrdenamiento((String) row[4]);
                                    temp.setActividad((String) row[5]);
                                    temp.setCodOpcion((String) row[6]);

                                    temp.setDescripcionOrd((String) row[7]);

                                    temp.setIdtipoMetodoAprov((String) row[8]);
                                    temp.setTipoMetodoAprov((String) row[9]);

                                    temp.setIdtipoTransMaderable((String) row[10]);
                                    temp.setTipoTransMaderable((String) row[11]);

                                    temp.setIdtipoTransDestino((String) row[12]);
                                    temp.setTipoTransDestino((String) row[13]);

                                    //temp.setNuCaminoAcceso((BigDecimal) row[14]);
                                    if (row[14] != null) {
                                        temp.setNuCaminoAcceso((BigDecimal) row[14]);
                                    }else{
                                        temp.setNuCaminoAcceso(BigDecimal.ZERO);
                                    }
                                    //temp.setNuCaminoPrincipal((BigDecimal) row[15]);
                                    if (row[15] != null) {
                                        temp.setNuCaminoPrincipal((BigDecimal) row[15]);
                                    }else{
                                        temp.setNuCaminoPrincipal(BigDecimal.ZERO);
                                    }
                                    temp.setDescripcionCamino((String) row[16]);
                                    temp.setDescripcionOtro((String) row[17]);

                                    temp.setNuIdOperaciones((Integer) row[18]);
                                    temp.setManoObra((String) row[19]);
                                    temp.setMaquina((String) row[20]);
                                    temp.setDescripcionManoObra((String) row[21]);

                                    temp.setNuIdEspecie((Integer) row[22]);
                                    temp.setEspecie((String) row[23]);
                                    temp.setJustificacionEspecie((String) row[24]);

                                    temp.setTratamientoSilvicultural((String) row[25]);
                                    temp.setDescripcionTratamiento((String) row[26]);
                                    temp.setReforestacion((String) row[27]);

                                    temp.setInfraestructura((String) row[28]);
                                    temp.setCaracteristicasTecnicas((String) row[29]);
                                    temp.setMetodoConstruccion((String) row[30]);

                                    temp.setDescripcionOtros((String) row[31]);
                                    temp.setIdOperacion((Integer) row[32]);
                                    temp.setLineaProduccion((String) row[33]);
                                    //temp.setNuDiametro((BigDecimal) row[34]);
                                    if (row[34] != null) {
                                        temp.setNuDiametro((BigDecimal) row[34]);
                                    }else{
                                        temp.setNuDiametro(BigDecimal.ZERO);
                                    }
                                    //temp.setAccion((Boolean) row[35]);
                                    if (row[35] != null) {
                                        temp.setAccion((boolean) row[35]);
                                    } else {
                                        temp.setAccion(false);
                                    }
                                    temp.setTipoEspecie((String) row[36]);

                                    //MANEJO FORESTAL 311
                                    if (row[37] != null) {
                                        temp.setNuTotalNroArboles((BigDecimal) row[37]);
                                    }else{
                                        temp.setNuTotalNroArboles(BigDecimal.ZERO);
                                    }
                                    if (row[38] != null) {
                                        temp.setNuTotalVcp((BigDecimal) row[38]);
                                    }else{
                                        temp.setNuTotalVcp(BigDecimal.ZERO);
                                    }
                                    if (row[39] != null) {
                                        temp.setNuValorVcpProd((BigDecimal) row[39]);
                                    }else{
                                        temp.setNuValorVcpProd(BigDecimal.ZERO);
                                    }
                                    if (row[40] != null) {
                                        temp.setNuValorVcap((BigDecimal) row[40]);
                                    }else{
                                        temp.setNuValorVcap(BigDecimal.ZERO);
                                    }
                                    //VERTICES
                                    temp.setPuntoVertice(row[41] == null ? null:  (String) row[41]);
                                    temp.setCoordenadaEsteIni(row[42] == null ? null:  (BigDecimal) row[42]);
                                    temp.setCoordenadaNorteIni(row[43] == null ? null:  (BigDecimal) row[43]);
                                    temp.setReferencia(row[44] == null ? null:  (String) row[44]);
                                    temp.setIdArchivo(row[45] == null ? null:  (Integer) row[45]);

                                    //DEMAC 6.1
                                    temp.setEquipo(row[46] == null ? null:  (String) row[46]);
                                    temp.setInsumo(row[47] == null ? null:  (String) row[47]);
                                    temp.setPersonal(row[48] == null ? null:  (String) row[48]);
                                    temp.setObservacion(row[49] == null ? null:  (String) row[49]);
                                    /**************************************************************************************************/
                                    List<ManejoBosqueAniosEntity> listAnios = new ArrayList<ManejoBosqueAniosEntity>();
                                    StoredProcedureQuery panios = entityManager
                                            .createStoredProcedureQuery("dbo.pa_ManejoBosqueAnios_Listar");
                                    panios.registerStoredProcedureParameter("idManejoBosqueDet", Integer.class,ParameterMode.IN);
                                    SpUtil.enableNullParams(panios);
                                    if(temp.getIdManejoBosqueDet()==null){panios.setParameter("idManejoBosqueDet", 0);
                                    }else { panios.setParameter("idManejoBosqueDet", temp.getIdManejoBosqueDet());}
                                    panios.execute();
                                    List<Object[]> spResultAnios = panios.getResultList();
                                    if(spResultAnios!=null) {
                                        if (spResultAnios.size() >= 1) {
                                            for (Object[] rowAnios : spResultAnios) {
                                                ManejoBosqueAniosEntity objAnios = new ManejoBosqueAniosEntity();
                                                objAnios.setIdManejoBosqueAnios(rowAnios[0] == null ?null :(Integer) rowAnios[0]);
                                                objAnios.setCodigo(rowAnios[1] == null ?null :(String) rowAnios[1]);
                                                objAnios.setDescripcion(rowAnios[2] == null ?null :(String) rowAnios[2]);
                                                objAnios.setDetalle(rowAnios[3] == null ?null :(String) rowAnios[3]);
                                                listAnios.add(objAnios);
                                            }
                                        }
                                    }
                                    /******************************************************************************************************/
                                    temp.setListManejoBosqueAnios(listAnios);
                                    listDet.add(temp);
                                }
                            }
                        }
                        /********************************************************************************/
                        temp2.setListManejoBosqueDetalle(listDet);

                        lista.add(temp2);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ManejoBosqueEntity> ListarManejoBosqueTitular(String tipoDocumento,String nroDocumento,String codigoProcesoTitular,String subcodigoProcesoTitular,Integer idPlanManejo,String codigoProceso,String subCodigoProceso) throws Exception {
        List<ManejoBosqueEntity> lista = new ArrayList<ManejoBosqueEntity>();
        try {
            int idDetalle=0;
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosque_Listar_Titular");
            processStored.registerStoredProcedureParameter("tipoDocumento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subcodigoProcesoTitular", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subcodigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("tipoDocumento", tipoDocumento);
            processStored.setParameter("nroDocumento", nroDocumento);
            processStored.setParameter("codigoProcesoTitular", codigoProcesoTitular);
            processStored.setParameter("subcodigoProcesoTitular", subcodigoProcesoTitular);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("codigoProceso", codigoProceso);
            processStored.setParameter("subcodigoProceso", subCodigoProceso);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row2 : spResult) {

                        ManejoBosqueEntity temp2 = new ManejoBosqueEntity();
                        temp2.setIdManejoBosque((Integer) row2[0]);
                        temp2.setTipo((String) row2[1]);
                        temp2.setCodigoManejo((String) row2[2]);
                        temp2.setSubCodigoManejo((String) row2[3]);
                        temp2.setDescripcionCab((String) row2[4]);
                        temp2.setIdPlanManejo((Integer) row2[5]);
                        temp2.setTxDetalle((String) row2[6]);
                        temp2.setTxObservacion((String) row2[7]);
                        temp2.setCodigoEscala(row2[8]==null? null: (String) row2[8]);
                        temp2.setAnio(row2[9]==null? null: (Integer) row2[9]);
                        if(row2[0]==null){
                            idDetalle=0;
                        }
                        else{
                            idDetalle=(Integer) row2[0];
                        }

                        /********************************** DETALLE **********************************************/

                        List<ManejoBosqueDetalleEntity> listDet = new ArrayList<ManejoBosqueDetalleEntity>();
                        StoredProcedureQuery pa = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosqueDetalle_Listar_Titular");
                        pa.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
                        SpUtil.enableNullParams(pa);
                        if(idDetalle==0){pa.setParameter("idManejoBosque", null); }
                        else{pa.setParameter("idManejoBosque", idDetalle);}
                        pa.execute();
                        List<Object[]> spResultDet = pa.getResultList();
                        if(spResultDet!=null) {
                            if (spResultDet.size() >= 1) {
                                for (Object[] row : spResultDet) {
                                    ManejoBosqueDetalleEntity temp = new ManejoBosqueDetalleEntity();
                                    temp.setIdManejoBosqueDet((Integer) row[0]);
                                    temp.setCodtipoManejoDet((String) row[1]);
                                    temp.setSubCodtipoManejoDet((String) row[2]);
                                    temp.setCatOrdenamiento((String) row[3]);
                                    temp.setOrdenamiento((String) row[4]);
                                    temp.setActividad((String) row[5]);
                                    temp.setCodOpcion((String) row[6]);

                                    temp.setDescripcionOrd((String) row[7]);

                                    temp.setIdtipoMetodoAprov((String) row[8]);
                                    temp.setTipoMetodoAprov((String) row[9]);

                                    temp.setIdtipoTransMaderable((String) row[10]);
                                    temp.setTipoTransMaderable((String) row[11]);

                                    temp.setIdtipoTransDestino((String) row[12]);
                                    temp.setTipoTransDestino((String) row[13]);

                                    //temp.setNuCaminoAcceso((BigDecimal) row[14]);
                                    if (row[14] != null) {
                                        temp.setNuCaminoAcceso((BigDecimal) row[14]);
                                    }else{
                                        temp.setNuCaminoAcceso(BigDecimal.ZERO);
                                    }
                                    //temp.setNuCaminoPrincipal((BigDecimal) row[15]);
                                    if (row[15] != null) {
                                        temp.setNuCaminoPrincipal((BigDecimal) row[15]);
                                    }else{
                                        temp.setNuCaminoPrincipal(BigDecimal.ZERO);
                                    }
                                    temp.setDescripcionCamino((String) row[16]);
                                    temp.setDescripcionOtro((String) row[17]);

                                    temp.setNuIdOperaciones((Integer) row[18]);
                                    temp.setManoObra((String) row[19]);
                                    temp.setMaquina((String) row[20]);
                                    temp.setDescripcionManoObra((String) row[21]);

                                    temp.setNuIdEspecie((Integer) row[22]);
                                    temp.setEspecie((String) row[23]);
                                    temp.setJustificacionEspecie((String) row[24]);

                                    temp.setTratamientoSilvicultural((String) row[25]);
                                    temp.setDescripcionTratamiento((String) row[26]);
                                    temp.setReforestacion((String) row[27]);

                                    temp.setInfraestructura((String) row[28]);
                                    temp.setCaracteristicasTecnicas((String) row[29]);
                                    temp.setMetodoConstruccion((String) row[30]);

                                    temp.setDescripcionOtros((String) row[31]);
                                    temp.setIdOperacion((Integer) row[32]);
                                    temp.setLineaProduccion((String) row[33]);
                                    //temp.setNuDiametro((BigDecimal) row[34]);
                                    if (row[34] != null) {
                                        temp.setNuDiametro((BigDecimal) row[34]);
                                    }else{
                                        temp.setNuDiametro(BigDecimal.ZERO);
                                    }
                                    //temp.setAccion((Boolean) row[35]);
                                    if (row[35] != null) {
                                        temp.setAccion((boolean) row[35]);
                                    } else {
                                        temp.setAccion(false);
                                    }
                                    temp.setTipoEspecie((String) row[36]);

                                    //MANEJO FORESTAL 311
                                    if (row[37] != null) {
                                        temp.setNuTotalNroArboles((BigDecimal) row[37]);
                                    }else{
                                        temp.setNuTotalNroArboles(BigDecimal.ZERO);
                                    }
                                    if (row[38] != null) {
                                        temp.setNuTotalVcp((BigDecimal) row[38]);
                                    }else{
                                        temp.setNuTotalVcp(BigDecimal.ZERO);
                                    }
                                    if (row[39] != null) {
                                        temp.setNuValorVcpProd((BigDecimal) row[39]);
                                    }else{
                                        temp.setNuValorVcpProd(BigDecimal.ZERO);
                                    }
                                    if (row[40] != null) {
                                        temp.setNuValorVcap((BigDecimal) row[40]);
                                    }else{
                                        temp.setNuValorVcap(BigDecimal.ZERO);
                                    }
                                    //VERTICES
                                    temp.setPuntoVertice(row[41] == null ? null:  (String) row[41]);
                                    temp.setCoordenadaEsteIni(row[42] == null ? null:  (BigDecimal) row[42]);
                                    temp.setCoordenadaNorteIni(row[43] == null ? null:  (BigDecimal) row[43]);
                                    temp.setReferencia(row[44] == null ? null:  (String) row[44]);
                                    temp.setIdArchivo(row[45] == null ? null:  (Integer) row[45]);
                                    /**************************************************************************************************/
                                    List<ManejoBosqueAniosEntity> listAnios = new ArrayList<ManejoBosqueAniosEntity>();
                                    StoredProcedureQuery panios = entityManager
                                            .createStoredProcedureQuery("dbo.pa_ManejoBosqueAnios_Listar");
                                    panios.registerStoredProcedureParameter("idManejoBosqueDet", Integer.class,ParameterMode.IN);
                                    SpUtil.enableNullParams(panios);
                                    if(temp.getIdManejoBosqueDet()==null){panios.setParameter("idManejoBosqueDet", 0);
                                    }else { panios.setParameter("idManejoBosqueDet", temp.getIdManejoBosqueDet());}
                                    panios.execute();
                                    List<Object[]> spResultAnios = panios.getResultList();
                                    if(spResultAnios!=null) {
                                        if (spResultAnios.size() >= 1) {
                                            for (Object[] rowAnios : spResultAnios) {
                                                ManejoBosqueAniosEntity objAnios = new ManejoBosqueAniosEntity();
                                                objAnios.setIdManejoBosqueAnios(rowAnios[0] == null ?null :(Integer) rowAnios[0]);
                                                objAnios.setCodigo(rowAnios[1] == null ?null :(String) rowAnios[1]);
                                                objAnios.setDescripcion(rowAnios[2] == null ?null :(String) rowAnios[2]);
                                                objAnios.setDetalle(rowAnios[3] == null ?null :(String) rowAnios[3]);
                                                listAnios.add(objAnios);
                                            }
                                        }
                                    }
                                    /******************************************************************************************************/
                                    temp.setListManejoBosqueAnios(listAnios);
                                    listDet.add(temp);
                                }
                            }
                        }
                        /********************************************************************************/
                        temp2.setListManejoBosqueDetalle(listDet);

                        lista.add(temp2);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarProteccionBosque", e.getMessage());
            throw e;
        }
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {Registra y actualizar la cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueEntity
     */

    @Override
    public ResultClassEntity RegistrarManejoBosque(List<ManejoBosqueEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ManejoBosqueEntity> list_ = new ArrayList<>();
        try {
            for (ManejoBosqueEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosque_Registrar");
                sp.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoManejo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subCodigoManejo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcionCab", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("txDetalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("txObservacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoEscala", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idManejoBosque", p.getIdManejoBosque());
                sp.setParameter("tipo", p.getTipo());
                sp.setParameter("codigoManejo", p.getCodigoManejo());
                sp.setParameter("subCodigoManejo", p.getSubCodigoManejo());
                sp.setParameter("descripcionCab", p.getDescripcionCab());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("txDetalle", p.getTxDetalle());
                sp.setParameter("txObservacion", p.getTxObservacion());
                sp.setParameter("codigoEscala", p.getCodigoEscala());
                sp.setParameter("anio", p.getAnio());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ManejoBosqueEntity obj_ = new ManejoBosqueEntity();
                        obj_.setIdManejoBosque(row_[0] ==null? null: (Integer) row_[0]);
                        obj_.setTipo(row_[1]==null? null: (String) row_[1]);
                        obj_.setCodigoManejo(row_[2]==null? null: (String) row_[2]);
                        obj_.setDescripcionCab(row_[3]==null? null: (String) row_[3]);
                        obj_.setIdPlanManejo(row_[4]==null? null: (Integer) row_[4]);
                        obj_.setSubCodigoManejo(row_[5]==null? null: (String) row_[5]);
                        obj_.setTxDetalle(row_[6]==null? null: (String) row_[6]);
                        obj_.setTxObservacion(row_[7]==null? null: (String) row_[7]);
                        obj_.setCodigoEscala(row_[8]==null? null: (String) row_[8]);
                        obj_.setAnio(row_[9]==null? null: (Integer) row_[9]);
                        List<ManejoBosqueDetalleEntity> listDet = new ArrayList<>();
                        for (ManejoBosqueDetalleEntity param : p.getListManejoBosqueDetalle()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_ManejoBosqueDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idManejoBosqueDet", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idManejoBosque", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codtipoManejoDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("subCodtipoManejoDet", String.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("catOrdenamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("ordenamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codOpcion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionOrd", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idtipoMetodoAprov", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoMetodoAprov", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idtipoTransMaderable", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoTransMaderable", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idtipoTransDestino", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoTransDestino", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuCaminoAcceso", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuCaminoPrincipal", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionCamino", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionOtro", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuIdOperaciones", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("manoObra", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("maquina", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionManoObra", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuIdEspecie", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("especie", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("justificacionEspecie", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tratamientoSilvicultural", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionTratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("Reforestacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("infraestructura", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("caracteristicasTecnicas", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("metodoConstruccion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionOtros", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idOperacion", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("lineaProduccion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuDiametro", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            //MANEJO FORESTAL 311
                            pa.registerStoredProcedureParameter("nuTotalNroArboles", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuTotalVcp", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuValorVcpProd", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuValorVcap", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
                            //VERTICES
                            pa.registerStoredProcedureParameter("puntoVertice", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEsteIni", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorteIni", BigDecimal.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("referencia", String.class, ParameterMode.IN);
                            // DEMAC
                            pa.registerStoredProcedureParameter("equipo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("insumo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("personal", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                            SpUtil.enableNullParams(pa);

                            pa.setParameter("idManejoBosqueDet", param.getIdManejoBosqueDet());
                            pa.setParameter("idManejoBosque", obj_.getIdManejoBosque());
                            pa.setParameter("codtipoManejoDet", param.getCodtipoManejoDet());
                            pa.setParameter("subCodtipoManejoDet", param.getSubCodtipoManejoDet());
                            pa.setParameter("catOrdenamiento", param.getCatOrdenamiento());
                            pa.setParameter("ordenamiento", param.getOrdenamiento());
                            pa.setParameter("actividad", param.getActividad());
                            pa.setParameter("codOpcion", param.getCodOpcion());
                            pa.setParameter("descripcionOrd", param.getDescripcionOrd());
                            pa.setParameter("idtipoMetodoAprov", param.getIdtipoMetodoAprov());
                            pa.setParameter("tipoMetodoAprov", param.getTipoMetodoAprov());
                            pa.setParameter("idtipoTransMaderable", param.getIdtipoTransMaderable());
                            pa.setParameter("tipoTransMaderable", param.getTipoTransMaderable());
                            pa.setParameter("idtipoTransDestino", param.getIdtipoTransDestino());
                            pa.setParameter("tipoTransDestino", param.getTipoTransDestino());
                            pa.setParameter("nuCaminoAcceso", param.getNuCaminoAcceso());
                            pa.setParameter("nuCaminoPrincipal", param.getNuCaminoPrincipal());
                            pa.setParameter("descripcionCamino", param.getDescripcionCamino());
                            pa.setParameter("descripcionOtro", param.getDescripcionOtro());
                            pa.setParameter("nuIdOperaciones", param.getNuIdOperaciones());
                            pa.setParameter("manoObra", param.getManoObra());
                            pa.setParameter("maquina", param.getMaquina());
                            pa.setParameter("descripcionManoObra", param.getDescripcionManoObra());
                            pa.setParameter("nuIdEspecie", param.getNuIdEspecie());
                            pa.setParameter("especie", param.getEspecie());
                            pa.setParameter("justificacionEspecie", param.getJustificacionEspecie());
                            pa.setParameter("tratamientoSilvicultural", param.getTratamientoSilvicultural());
                            pa.setParameter("descripcionTratamiento", param.getDescripcionTratamiento());
                            pa.setParameter("Reforestacion", param.getReforestacion());
                            pa.setParameter("infraestructura", param.getInfraestructura());
                            pa.setParameter("caracteristicasTecnicas", param.getCaracteristicasTecnicas());
                            pa.setParameter("metodoConstruccion", param.getMetodoConstruccion());
                            pa.setParameter("descripcionOtros", param.getDescripcionOtros());
                            pa.setParameter("idOperacion", param.getIdOperacion());
                            pa.setParameter("lineaProduccion", param.getLineaProduccion());
                            pa.setParameter("nuDiametro", param.getNuDiametro());
                            pa.setParameter("accion", param.getAccion());
                            pa.setParameter("tipoEspecie", param.getTipoEspecie());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            //MANEJO FORESTAL 311
                            pa.setParameter("nuTotalNroArboles", param.getNuTotalNroArboles());
                            pa.setParameter("nuTotalVcp", param.getNuTotalVcp());
                            pa.setParameter("nuValorVcpProd", param.getNuValorVcpProd());
                            pa.setParameter("nuValorVcap", param.getNuValorVcap());
                            pa.setParameter("idArchivo", param.getIdArchivo());
                            //VERTICES
                            pa.setParameter("puntoVertice", param.getPuntoVertice());
                            pa.setParameter("coordenadaEsteIni", param.getCoordenadaEsteIni());
                            pa.setParameter("coordenadaNorteIni", param.getCoordenadaNorteIni());
                            pa.setParameter("referencia", param.getReferencia());
                            // DEMAC 6.1
                            pa.setParameter("equipo", param.getEquipo() );
                            pa.setParameter("insumo", param.getInsumo() );
                            pa.setParameter("personal", param.getPersonal() );
                            pa.setParameter("observacion", param.getObservacion() );
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                ManejoBosqueDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new ManejoBosqueDetalleEntity();

                                    obj.setIdManejoBosqueDet(row[0]==null? null: (Integer) row[0]);
                                    obj.setCodtipoManejoDet(row[1]==null? null:  (String) row[1]);
                                    obj.setCatOrdenamiento(row[2]==null? null:  (String) row[2]);
                                    obj.setOrdenamiento(row[3]==null? null:  (String) row[3]);
                                    obj.setActividad(row[4]==null? null:  (String) row[4]);
                                    obj.setCodOpcion(row[5]==null? null:  (String) row[5]);
                                    obj.setDescripcionOrd(row[6]==null? null:  (String) row[6]);

                                    obj.setIdtipoMetodoAprov(row[7]==null? null:  (String) row[7]);
                                    obj.setTipoMetodoAprov(row[8]==null? null:  (String) row[8]);

                                    obj.setIdtipoTransMaderable(row[9]==null? null:  (String) row[9]);
                                    obj.setTipoTransMaderable(row[10]==null? null:  (String) row[10]);

                                    obj.setIdtipoTransDestino(row[11]==null? null:  (String) row[11]);
                                    obj.setTipoTransDestino(row[12]==null? null:  (String) row[12]);

                                    obj.setNuCaminoAcceso(row[13]==null? null:  (BigDecimal) row[13]);
                                    obj.setNuCaminoPrincipal(row[14]==null? null:  (BigDecimal) row[14]);
                                    obj.setDescripcionCamino(row[15]==null? null:  (String) row[15]);
                                    obj.setDescripcionOtro(row[16]==null? null:  (String) row[16]);

                                    obj.setNuIdOperaciones(row[17]==null? null:  (Integer) row[17]);
                                    obj.setManoObra(row[18]==null? null:  (String) row[18]);
                                    obj.setMaquina(row[19]==null? null:  (String) row[19]);
                                    obj.setDescripcionManoObra(row[20]==null? null:  (String) row[20]);

                                    obj.setNuIdEspecie(row[21]==null? null:  (Integer) row[21]);
                                    obj.setEspecie(row[22]==null? null:  (String) row[22]);
                                    obj.setJustificacionEspecie(row[23]==null? null:  (String) row[23]);

                                    obj.setTratamientoSilvicultural(row[24]==null? null:  (String) row[24]);
                                    obj.setDescripcionTratamiento(row[24]==null? null:  (String) row[25]);
                                    obj.setReforestacion(row[26]==null? null:  (String) row[26]);

                                    obj.setInfraestructura(row[27]==null? null:  (String) row[27]);
                                    obj.setCaracteristicasTecnicas(row[28]==null? null:  (String) row[28]);
                                    obj.setMetodoConstruccion(row[29]==null? null:  (String) row[29]);

                                    obj.setDescripcionOtros(row[30]==null? null:  (String) row[30]);
                                    obj.setIdOperacion(row[31]==null? null:  (Integer) row[31]);
                                    obj.setLineaProduccion(row[32]==null? null:  (String) row[32]);
                                    obj.setNuDiametro(row[33]==null? null:  (BigDecimal) row[33]);
                                    obj.setAccion(row[34]==null? null:  (Boolean) row[34]);
                                    obj.setTipoEspecie(row[35]==null? null:  (String) row[35]);
                                    obj.setSubCodtipoManejoDet(row[36]==null? null:  (String) row[36]);
                                    //MANEJO FORESTAL 311
                                    obj.setNuTotalNroArboles(row[37]==null? null:  (BigDecimal) row[37]);
                                    obj.setNuTotalVcp(row[38]==null? null:  (BigDecimal) row[38]);
                                    obj.setNuValorVcpProd(row[39]==null? null:  (BigDecimal) row[39]);
                                    obj.setNuValorVcap(row[40]==null? null:  (BigDecimal) row[40]);
                                    //VERTICES
                                    obj.setPuntoVertice(row[41] == null ? null:  (String) row[41]);
                                    obj.setCoordenadaEsteIni(row[42] == null ? null:  (BigDecimal) row[42]);
                                    obj.setCoordenadaNorteIni(row[43] == null ? null:  (BigDecimal) row[43]);
                                    obj.setReferencia(row[44] == null ? null:  (String) row[44]);
                                    obj.setIdArchivo(row[45] == null ? null:  (Integer) row[45]);
                                    //DEMAC 6.1
                                    obj.setEquipo(row[46] == null ? null:  (String) row[46]);
                                    obj.setInsumo(row[47] == null ? null:  (String) row[47]);
                                    obj.setPersonal(row[48] == null ? null:  (String) row[48]);
                                    obj.setObservacion(row[49] == null ? null:  (String) row[49]);
                                    /**************************************************************************************************/
                                    List<ManejoBosqueAniosEntity> listAnios = new ArrayList<>();

                                    if (param.getListManejoBosqueAnios() != null) {
                                        for (ManejoBosqueAniosEntity paramAnios : param.getListManejoBosqueAnios()) {
                                            StoredProcedureQuery panios = entityManager
                                                    .createStoredProcedureQuery("dbo.pa_ManejoBosqueAnios_Registrar");
                                            panios.registerStoredProcedureParameter("idManejoBosqueAnios", Integer.class,ParameterMode.IN);
                                            panios.registerStoredProcedureParameter("idManejoBosqueDet", Integer.class,ParameterMode.IN);
                                            panios.registerStoredProcedureParameter("codigo", String.class,ParameterMode.IN);
                                            panios.registerStoredProcedureParameter("descripcion", String.class,ParameterMode.IN);
                                            panios.registerStoredProcedureParameter("detalle", String.class,ParameterMode.IN);
                                            panios.registerStoredProcedureParameter("observacion", String.class,ParameterMode.IN);
                                            panios.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class,ParameterMode.IN);
                                            SpUtil.enableNullParams(panios);
                                            panios.setParameter("idManejoBosqueAnios", paramAnios.getIdManejoBosqueAnios());
                                            panios.setParameter("idManejoBosqueDet", obj.getIdManejoBosqueDet());
                                            panios.setParameter("codigo", paramAnios.getCodigo());
                                            panios.setParameter("descripcion", paramAnios.getDescripcion());
                                            panios.setParameter("detalle", paramAnios.getDetalle());
                                            panios.setParameter("observacion", paramAnios.getObservacion());
                                            panios.setParameter("idUsuarioRegistro", paramAnios.getIdUsuarioRegistro());
                                            panios.execute();
                                            List<Object[]> spResultAnios = panios.getResultList();
                                            if (!spResultAnios.isEmpty()) {
                                                ManejoBosqueAniosEntity objAnios = null;
                                                for (Object[] rowAnios : spResultAnios) {
                                                    objAnios = new ManejoBosqueAniosEntity();
                                                    objAnios.setIdManejoBosqueAnios(rowAnios[0] == null ?null :(Integer) rowAnios[0]);
                                                    objAnios.setCodigo(rowAnios[1] == null ?null :(String) rowAnios[1]);
                                                    objAnios.setDescripcion(rowAnios[2] == null ?null :(String) rowAnios[2]);
                                                    objAnios.setDetalle(rowAnios[3] == null ?null :(String) rowAnios[3]);
                                                    listAnios.add(objAnios);
                                                }
                                            }
                                        }
                                        /**************************************************************************************************/
                                        obj.setListManejoBosqueAnios(listAnios);
                                    }
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListManejoBosqueDetalle(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el Manejo del Bosque.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {REliminar la cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueDto
     */

    @Override
    public ResultClassEntity EliminarManejoBosque(ManejoBosqueDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].pa_ManejoBosque_Eliminar");
        processStored.registerStoredProcedureParameter("idManejoBosque", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idManejoBosqueDet", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);


        processStored.setParameter("idManejoBosque", param.getIdManejoBosque());
        processStored.setParameter("idManejoBosqueDet", param.getIdManejoBosqueDet());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

}
