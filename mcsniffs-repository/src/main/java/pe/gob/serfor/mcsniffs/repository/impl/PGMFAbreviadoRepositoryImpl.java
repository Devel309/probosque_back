package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.PGMFAbreviadoRepository;
import pe.gob.serfor.mcsniffs.repository.util.File_Util;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class PGMFAbreviadoRepositoryImpl extends JdbcDaoSupport implements PGMFAbreviadoRepository {
    /**
     * @autor: JaquelineDB [27-07-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los anexos}
     *
     */
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AnexoRepositoryImpl.class);

    /**
     * @autor: Miguel F. [31-07-2021]
     * @modificado:
     * @descripción: {obtener anexo1 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List<OrdenamientoDetalleEntity>> protecionVigilanciaObtener(OrdenamientoDetalleEntity param) {
        return null;
/*        ResultClassEntity<List<OrdenamientoDetalleEntity>> result = new ResultClassEntity<>();
        List<OrdenamientoDetalleEntity> list = new ArrayList<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_OrdenamientoDetalle_Obtener (?,?)}");
            pstm.setObject (1, param.getIdPlanManejo(),Types.INTEGER);
            pstm.setObject (2, param.getCodigoTipoDetalle(),Types.VARCHAR);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            while(rs.next()){
                OrdenamientoDetalleEntity temp = new OrdenamientoDetalleEntity();    
                temp.setIdPlanManejo(rs.getInt("NU_ID_PLAN_MANEJO"));
                temp.setCodigoTipoDetalle(rs.getString("TX_CODIGO_TIPO_DETALLE"));
                temp.setVertice(rs.getString("TX_VERTICE"));
                temp.setLugar(rs.getString("TX_LUGAR"));
                temp.setCoordenadas(rs.getString("TX_COORDENADAS"));
                temp.setMaterial(rs.getString("TX_MATERIAL"));
                temp.setObservacion(rs.getString("TX_OBSERVACION"));
                temp.setFilaExcel(rs.getInt("NU_FILA_EXCEL"));

                temp.setSectorLindero(rs.getString("TX_SECTOR_LINDERO"));
                temp.setRiesgoIdentificado(rs.getString("TX_RIESGO_IDENTIFICADO"));
                temp.setLongitudLindero(rs.getString("TX_LONGITUD_LINDERO"));
                temp.setIdRiesgoVulnerabilidad(rs.getString("TX_ID_RIESGO_VULNERABILIDAD"));
                temp.setActividadVigilancia(rs.getString("TX_ACTIVIDAD_VIGILANCIA"));
                temp.setInfraestructuraPersonalReq(rs.getString("TX_INFRAESTRUCTURA_PERSONAL_REQ"));               
                
                list.add(temp);
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Ordenamiento y protecion de la UMF(Proteción y vigilancia)");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    /**
     * @autor: Miguel F. [28-07-2021]
     * @modificado:
     * @descripción: {carga masiva - protecion vigilancia (H05)}
     *
     */
    @Override
    public ResultClassEntity<String> protecionVigilanciaArchivo (MultipartFile file, OrdenamientoDetalleEntity params) throws Exception {
        
        ResultClassEntity<String> result = new ResultClassEntity<>();
        String text_resulr = ""; 

        for(Integer i=0; i<4; i++){
            Integer int_sheet  = i ;     
            File_Util fl = new File_Util();
            XSSFWorkbook workbook = fl.getWorkBook(file); //new XSSFWorkbook(file_stream); 
            //Create a blank sheet
            XSSFSheet sheet = workbook.getSheetAt(int_sheet);

            int countRow = 0; 
            if(int_sheet == 0)  params.setCodigoTipoDetalle("ORDUBI");
            if(int_sheet == 1)  params.setCodigoTipoDetalle("ORDSEN");
            if(int_sheet == 2)  params.setCodigoTipoDetalle("ORDEMALI");
            if(int_sheet == 3)  params.setCodigoTipoDetalle("ORVIGUMF");
            eliminarOrdenamientoDetalle(params);
            for(Row rowx : sheet) {    
                countRow++; 
                OrdenamientoDetalleEntity temp = new OrdenamientoDetalleEntity();
                temp.setIdPlanManejo(params.getIdPlanManejo());
                temp.setCodigoTipoDetalle(params.getCodigoTipoDetalle());
                temp.setIdUsuarioRegistro(params.getIdUsuarioRegistro());
                
                Integer countCell = 0; 
                for(Cell cell : rowx) {              
                    countCell++;
                    
                    FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                    DataFormatter formatter = new DataFormatter();
                    if(int_sheet == 0){
                        if( countCell == 1  ){
                            temp.setVertice(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 2  ){
                            temp.setCoordenadas(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 3  ){
                            temp.setMaterial(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 4  ){
                            temp.setObservacion(formatter.formatCellValue(cell, evaluator));
                        }
                    }
                    if(int_sheet == 1){
                        if( countCell == 1  ){
                            temp.setLugar(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 2  ){
                            temp.setCoordenadas(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 3  ){
                            temp.setMaterial(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 4  ){
                            temp.setObservacion(formatter.formatCellValue(cell, evaluator));
                        }
                    }
                    if(int_sheet == 2){
                        if( countCell == 1  ){
                            temp.setSectorLindero(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 2  ){
                            temp.setRiesgoIdentificado(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 3  ){
                            temp.setLongitudLindero(formatter.formatCellValue(cell, evaluator));
                        }
                    }
                    if(int_sheet == 3){
                        if( countCell == 1  ){
                            temp.setIdRiesgoVulnerabilidad(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 2  ){
                            temp.setActividadVigilancia(formatter.formatCellValue(cell, evaluator));
                        }
                        if( countCell == 3  ){
                            temp.setInfraestructuraPersonalReq(formatter.formatCellValue(cell, evaluator));
                        }
                    }
                }
                if(countRow > 1){
                  Boolean insert =  insertarOrdenamientoDetalle(temp);
                  if(insert == true){
                    text_resulr += "Fila "+ countRow +" registrado correctamente-";
                  }
                  else{
                    text_resulr += "Fila "+ countRow +" Ocurrió un error al registar-";
                  }
                }
            }
        }
        result.setSuccess(true);
        result.setData(text_resulr);
        return result;
    }

    private Boolean insertarOrdenamientoDetalle(OrdenamientoDetalleEntity obj){
    //     Integer idPlanManejo, String codigoTipoDetalle, String vertice ,
    // String lugar, String coordenadas, String material, String observacion, Integer filaExcel, Integer idUsuarioRegistro
        return null;
/*        Boolean result = false;
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_OrdenamientoDetalle_Insertar (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            pstm.setObject (1, obj.getIdPlanManejo(),Types.INTEGER);
            pstm.setObject (2, obj.getCodigoTipoDetalle(),Types.VARCHAR);
            pstm.setObject (3, obj.getVertice()==null?"": obj.getVertice(),Types.VARCHAR);
            pstm.setObject (4, obj.getLugar()==null?"": obj.getLugar(),Types.VARCHAR);
            pstm.setObject (5, obj.getCoordenadas()==null?"":obj.getCoordenadas(),Types.VARCHAR);
            pstm.setObject (6, obj.getMaterial()==null?"":obj.getMaterial(),Types.VARCHAR);
            pstm.setObject (7, obj.getObservacion()==null?"":obj.getObservacion(),Types.VARCHAR);
            pstm.setObject (8, obj.getFilaExcel()==null?0:obj.getFilaExcel(),Types.INTEGER);
            
            pstm.setObject (9, obj.getInfraestructuraPersonalReq()==null?"":obj.getInfraestructuraPersonalReq(),Types.VARCHAR);
            pstm.setObject (10, obj.getActividadVigilancia()==null?"":obj.getActividadVigilancia(),Types.VARCHAR);
            pstm.setObject (11, obj.getIdRiesgoVulnerabilidad()==null?"":obj.getIdRiesgoVulnerabilidad(),Types.VARCHAR);
            pstm.setObject (12, obj.getLongitudLindero()==null?"":obj.getLongitudLindero(),Types.VARCHAR);
            pstm.setObject (13, obj.getRiesgoIdentificado()==null?"":obj.getRiesgoIdentificado(),Types.VARCHAR);
            pstm.setObject (14, obj.getSectorLindero()==null?"":obj.getSectorLindero(),Types.VARCHAR);

            pstm.setObject (15, obj.getIdUsuarioRegistro(),Types.INTEGER);
            
            pstm.execute();
            result = true; 
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - insertarOrdenamientoDetalle", e.getMessage()); 
        }

        return result;*/
    }

    private Boolean eliminarOrdenamientoDetalle(OrdenamientoDetalleEntity obj){
        //     Integer idPlanManejo, String codigoTipoDetalle, String vertice ,
        // String lugar, String coordenadas, String material, String observacion, Integer filaExcel, Integer idUsuarioRegistro
        return null;
/*            Boolean result = false;
            try {
                Connection con = null;
                PreparedStatement pstm  = null;
                con = dataSource.getConnection();
                pstm = con.prepareCall("{call pa_OrdenamientoDetalle_Eliminar (?,?)}");
                pstm.setObject (1, obj.getIdPlanManejo(),Types.INTEGER);
                pstm.setObject (2, obj.getCodigoTipoDetalle(),Types.VARCHAR);
                
                pstm.execute();
                result = true; 
                
            } catch (Exception e) {
                log.error("PGMFAbreviadoRepositoryImpl - eliminarOrdenamientoDetalle", e.getMessage()); 
            }
            return result;*/
        }

     /**
     * @autor: Miguel F. [21-07-2021]
     * @modificado:
     * @descripción: {obtener anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List<CronogramaActividadDetalleEntity>> cronogramaActividadDetalle_ListarDetalle (CronogramaActividadDetalleEntity param) {

        ResultClassEntity<List< CronogramaActividadDetalleEntity>> result = new ResultClassEntity<>();
        List< CronogramaActividadDetalleEntity> lista = new ArrayList<CronogramaActividadDetalleEntity>();
            
    
        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_CronogramaActividadDetalle_ListarDetalle");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idPlanManejo", param.getIdPlanManejo());
            sp.execute();

            List<Object[]> spResult_ = sp.getResultList();
            if (!spResult_.isEmpty()) {
                for (Object[] row_ : spResult_) {

                    CronogramaActividadDetalleEntity obj = new CronogramaActividadDetalleEntity();
                    obj.setIdCronActividad((Integer) row_[1]);
                    obj.setActividad((String) row_[2]);

                    List<Boolean> checksMeses = new ArrayList<>();
                    List<Boolean> checksAnios = new ArrayList<>();
                    for(Integer i = 4; i<16; i++){
                        Boolean check = ((Integer) row_[i]==1);
                        checksMeses.add(check);
                    }
                    for(Integer i = 16; i<36; i++){
                        try{
                            Boolean check = ((Integer) row_[i]==1);
                            checksAnios.add(check);
                        }catch(Exception e){}

                    }
                    obj.setMeses(checksMeses);
                    obj.setAnios(checksAnios);
                    lista.add(obj);


                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Organización manejo (Objetivo 11)");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - Organización Manejo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;
    }

    /**
     * @autor: Miguel F. [21-07-2021]
     * @modificado:
     * @descripción: {obtener anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadDetalleRegistar (CronogramaActividadDetalleEntity filtro) {
        return null;
/*        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_CronogramaActividadDetalle_RegistrarMarca (?,?,?,?,?,?)}");
            // pstm.setObject (1, filtro.getIdCronActividadDetalle(),Types.INTEGER);
            // pstm.setObject (2, filtro.getIdPlanManejo(),Types.INTEGER);
            // pstm.setObject (3, filtro.getAnio(),Types.INTEGER);
            // pstm.setObject (4, filtro.getMes(),Types.INTEGER);
            // pstm.setObject (5, filtro.getTieneMarca(),Types.BOOLEAN);
            pstm.setObject (6, filtro.getIdUsuarioRegistro(),Types.INTEGER);
            
            pstm.execute();
            result.setData(true);
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 15");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    /**
     * @autor: Miguel F. [21-07-2021]
     * @modificado:
     * @descripción: {obtener anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadEliminar (CronogramaActividadEntity filtro) {

        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_CronogramaActividad_Eliminar");
            sp.registerStoredProcedureParameter("idCronActividad", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idCronActividad", filtro.getIdCronogramaActividad());
            sp.setParameter("idUsuarioElimina", filtro.getIdUsuarioElimina());
            sp.execute();

            result.setData(true);
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 15");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;
    }

    /**
     * @autor: Miguel F. [21-07-2021]
     * @modificado:
     * @descripción: {obtener anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadActualizar (CronogramaActividadEntity filtro) {
        return null;
/*        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_CronogramaActividad_Actualizar (?,?,?,?,?)}");
            pstm.setObject (1, filtro.getIdCronogramaActividad(),Types.INTEGER);
            pstm.setObject (2, filtro.getIdPlanManejo(),Types.INTEGER);
            pstm.setObject (3, filtro.getActividad(),Types.VARCHAR);
            pstm.setObject (4, filtro.getAnio(),Types.INTEGER);
            pstm.setObject (5, filtro.getImporte(),Types.DECIMAL);
            pstm.setObject (6, filtro.getIdUsuarioRegistro(),Types.DECIMAL);
            
            pstm.execute();
            result.setData(true);
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 15");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    /**
     * @autor: Miguel F. [21-07-2021]
     * @modificado:
     * @descripción: {obtener anexo15 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> cronogramaActividadRegistrar (CronogramaActividadEntity filtro) {
        return null;
/*        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_CronogramaActividad_Registrar (?,?,?,?,?)}");
            pstm.setObject (1, filtro.getIdPlanManejo(),Types.INTEGER);
            pstm.setObject (2, filtro.getActividad(),Types.VARCHAR);
            pstm.setObject (3, filtro.getAnio(),Types.INTEGER);
            pstm.setObject (4, filtro.getImporte(),Types.DECIMAL);
            pstm.setObject (5, filtro.getIdUsuarioRegistro(),Types.DECIMAL);
            
            pstm.execute();
            result.setData(true);
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 15");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    
    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @Override
    public ResultClassEntity<PGMFAbreviadoObjetivo2Entity> obtenerObjetivo2(PGMFAbreviadoObjetivo2Entity filtro) {
        return null;
/*        ResultClassEntity<PGMFAbreviadoObjetivo2Entity> result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_PgmfAbreviadoObj2_Listar (?)}");
            pstm.setObject (1, filtro.getIdPgmf(),Types.INTEGER);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            if(rs.next()){
                PGMFAbreviadoObjetivo2Entity obj = new PGMFAbreviadoObjetivo2Entity();    
                obj.setIdPgmf(rs.getInt("NU_ID_PGMF"));
                obj.setObjetivoGeneral(rs.getString("TE_OBJETIVO_GENERAL"));
                obj.setObjetivoEspecificoA(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_A"));
                obj.setObjetivoEspecificoB(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_B"));
                obj.setObjetivoEspecificoC(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_C"));
                obj.setObjetivoEspecificoD(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_D"));
                obj.setObjetivoEspecificoE(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_E"));
                obj.setObjetivoEspecificoF(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_F"));
                obj.setObjetivoEspecificoG(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_G"));
                obj.setObjetivoEspecificoH(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_H"));
                obj.setObjetivoEspecificoI(rs.getBoolean("NU_OBJETIVO_ESPECIFICO_I"));
                obj.setObjetivoEspecificoJ(rs.getString("TE_OBJETIVO_ESPECIFICO_J"));
                result.setData(obj);
            }
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 2");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    
    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @Override
    public ResultClassEntity<String> insertarObjetivo2(PGMFAbreviadoObjetivo2Entity params) {
        return null;
/*        ResultClassEntity<String> result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_PgmfAbreviadoObj2_Registrar_Actualizar (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            pstm.setObject (1, params.getIdUsuarioRegistro(),Types.INTEGER);
            pstm.setObject (2, params.getIdUsuarioModificacion(),Types.INTEGER);
            pstm.setObject (3, params.getIdPgmf(),Types.INTEGER);
            pstm.setObject (4, params.getObjetivoGeneral(),Types.VARCHAR);
            pstm.setObject (5, params.getObjetivoEspecificoA(),Types.BIT);
            pstm.setObject (6, params.getObjetivoEspecificoB(),Types.BIT);
            pstm.setObject (7, params.getObjetivoEspecificoC(),Types.BIT);
            pstm.setObject (8, params.getObjetivoEspecificoD(),Types.BIT);
            pstm.setObject (9, params.getObjetivoEspecificoE(),Types.BIT);
            pstm.setObject (10, params.getObjetivoEspecificoF(),Types.BIT);
            pstm.setObject (11, params.getObjetivoEspecificoG(),Types.BIT);
            pstm.setObject (12, params.getObjetivoEspecificoH(),Types.BIT);
            pstm.setObject (13, params.getObjetivoEspecificoI(),Types.BIT);
            pstm.setObject (14, params.getObjetivoEspecificoJ(),Types.VARCHAR);
            
            ResultSet rs = pstm.executeQuery();
            String obj = "";
            if(rs.next()){
                obj = rs.getString("operacion");
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 2");
            
        } catch (Exception e) {
            log.error("AnexoRepositoryImpl - ObtenerAnexo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }


    /**
     * @autor: Miguel F. [06-07-2021]
     * @modificado:
     * @descripción: {obtener anexo1 PGMF}
     *
     */
    @Override
    public ResultClassEntity<PGMFAbreviadoObjetivo1Entity> pgmfAbreviadoObj1(PGMFAbreviadoObjetivo1Entity param) {
        return null;
/*        ResultClassEntity<PGMFAbreviadoObjetivo1Entity> result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_PgmfAbreviadoObj1_Listar (?)}");
            pstm.setObject (1, param.getIdPgmf(),Types.INTEGER);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            if(rs.next()){
                PGMFAbreviadoObjetivo1Entity obj = new PGMFAbreviadoObjetivo1Entity();    
                obj.setNombreTitular(rs.getString("TX_NOMBRES"));
                obj.setNombreReprecentanteLegal(rs.getString("TX_NOMBRES_REPLEGAL"));
                obj.setDni(rs.getString("TX_NUMERO_DOCUMENTO"));
                obj.setDomicilioLegal(rs.getString("TX_DIRECCION_EMPRESA"));
                obj.setNroContratoConcesion(rs.getString("TE_NRO_CONTRATO_CONCESION"));
                obj.setDepartamento(rs.getString("NU_ID_DEPARTAMENTO"));
                obj.setProvincia(rs.getString("NU_ID_PROVINCIA"));
                obj.setDistrito(rs.getString("NU_ID_DISTRITO_PERSONA"));

                obj.setFechaPrecentacionPGMF(rs.getDate("FE_FECHA_PRESENTACION"));
                obj.setDuracionPGMF(rs.getInt("NU_DURACION"));
                obj.setFechaInicioPGMF(rs.getDate("FE_FECHA_INICIO"));
                obj.setFechaFinPGMF(rs.getDate("FE_FECHA_FIN"));
                obj.setAreaTotalConsecion(rs.getInt("NU_AREA_TOTAL_CONCESION"));
                obj.setAreaBosqueProduccionForestal(rs.getInt("NU_AREA_BOSQUE_PRODUCION_FORESTAL"));
                obj.setAreaProteccion(rs.getInt("NU_AREA_PROTECION"));
                obj.setNumeroBlouesQuinquenales(rs.getString("TX_BLOQUES_QUINQUENALES"));
                obj.setPotencialMaderable(rs.getBigDecimal("NU_POTENCIAL"));
                obj.setVolumenCortaAnualPermisible(rs.getInt("NU_VOLUMEN_ANUL_CORTA_PERMISIBLE"));

                obj.setIdRegenteForestal(rs.getInt("NU_IDPERSONA_ING_FORESTAL"));
                obj.setApellidoPaternoPersona(rs.getString("TX_APEPATERNO_PER"));
                obj.setApellidoMaternoPersona(rs.getString("TX_APEMATERNO_PER"));
                obj.setApellidoPaternoRepLegal(rs.getString("TX_APELLIDO_PATERNO_REPLEGAL"));
                obj.setApellidoMaternoRepLegal(rs.getString("TX_APELLIDO_MATERNO_REPLEGAL"));
                
                obj.setNombreRegenteForestal("del api");
                obj.setDomicilioLegalRegente("del api");
                obj.setContratoSuscritoTitularTituloHabilitante("del api");
                obj.setCertificadoHabilitacionProfecionalRegenteForestal("del api");
                obj.setNumeroInsccripcion(0);
                
                result.setData(obj);
            }
            result.setSuccess(true);
            result.setMessage("obtenido correctamente: Objetivo 1");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - pgmfAbreviadoObj1", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List< ParticipacionCiudadanaEntity>> participacionCiudadana(ParticipacionCiudadanaEntity param) {
        return null;
/*        ResultClassEntity<List< ParticipacionCiudadanaEntity>> result = new ResultClassEntity<>();
        List< ParticipacionCiudadanaEntity> lista = new ArrayList<ParticipacionCiudadanaEntity>();
            
    
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ParticipacionCiudadana_Listar (?)}");
            pstm.setObject (1, param.getIdPGMF(),Types.INTEGER);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            while(rs.next()){
                ParticipacionCiudadanaEntity obj = new ParticipacionCiudadanaEntity();    
                obj.setId(rs.getInt("id"));
                obj.setTipo(rs.getString("tipo"));
                obj.setDescripcion(rs.getString("descripcion"));
                obj.setMecanismoParticipacion(rs.getString("mecanismoParticipacion"));
                obj.setMetodologia(rs.getString("metodologia"));
                obj.setLugar(rs.getString("lugar"));
                obj.setFecha(rs.getDate("fecha"));
                lista.add(obj);
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Participacion Ciudadana (Objetivo 9)");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - participacionCiudadana", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }

    
    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List< CapacitacionPGMFAbreviadoEntity>> Capacitacion(CapacitacionPGMFAbreviadoEntity param) {
        return null;
/*        ResultClassEntity<List< CapacitacionPGMFAbreviadoEntity>> result = new ResultClassEntity<>();
        List< CapacitacionPGMFAbreviadoEntity> lista = new ArrayList<CapacitacionPGMFAbreviadoEntity>();
            
    
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Capacitacion_Listar (?)}");
            pstm.setObject (1, param.getIdPGMF(),Types.INTEGER);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            while(rs.next()){
                CapacitacionPGMFAbreviadoEntity obj = new CapacitacionPGMFAbreviadoEntity();
                obj.setId(rs.getInt("id"));
                obj.setTipo(rs.getString("tipo"));
                obj.setSubTipo(rs.getString("subtipo"));
                obj.setNombreSubTipo(rs.getString("nombreSubTipo"));
                obj.setDescripcion(rs.getString("descripcion"));
                obj.setPersonalCapacitar(rs.getString("personalacapacitar"));
                obj.setModalidadCapacitar(rs.getString("modalidadcapacitar"));
                obj.setLugar(rs.getString("lugar"));
                
                lista.add(obj);
            }
            
            result.setData( lista );
            result.setInformacion("");
            result.setSuccess(true);
            result.setMessage("Capacitación (Objetivo 9)");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - Capacitacion", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;*/
    }

    
    

     /**
     * @autor: Miguel F. [08-07-2021]
     * @modificado:
     * @descripción: {insertar en participacion ciudadana y capacitación}
     *
     */
    @Override
    public ResultClassEntity<String> PGMFAbreviadoDetalleRegistrar(PGMFAbreviadoDetalleRegistrarEntity param) {

        ResultClassEntity<String> result = new ResultClassEntity<>();
        try {

            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_OrganizacionManejo_Registrar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idOrganizacionManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoDetalle", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoSubTipoDetalle", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("mecanismoParticipacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("metodologia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fecha", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("personalCapacitar", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("modalidadCapacitar", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("medidaControl", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("medidaMonitoreo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("contingencia", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("accionRealizada", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("operacion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreTipoDetalle", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("funcion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("numero", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("maquinaEquipo", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoTipoActividad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoActividad", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("esCabecera", Boolean.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);

            sp.setParameter("idPlanManejo",  param.getIdPGMF());
            sp.setParameter("idOrganizacionManejo",  param.getIdOrganizacionManejo());
            sp.setParameter("codigoSeccion",   param.getCodigoSeccion());
            sp.setParameter("codigoTipoDetalle",  param.getCodigoTipoDetalle());
            sp.setParameter("codigoSubTipoDetalle",  param.getCodigoSubTipoDetalle());
            sp.setParameter("descripcion",  param.getDescripcion());
            sp.setParameter("mecanismoParticipacion",  param.getMecanismoParticipacion());
            sp.setParameter("metodologia",  param.getMetodologia());
            sp.setParameter("lugar",  param.getLugar());
            sp.setParameter("fecha",  param.getFecha());
            sp.setParameter("personalCapacitar",  param.getPersonalCapacitar());
            sp.setParameter("modalidadCapacitar",  param.getModalidadCapacitar());

            sp.setParameter("medidaControl",  param.getMedidaControl());
            sp.setParameter("medidaMonitoreo",  param.getMedidaMonitoreo());
            sp.setParameter("frecuencia",  param.getFrecuencia());
            sp.setParameter("responsable",  param.getResponsable());
            sp.setParameter("contingencia",  param.getContingencia());
            sp.setParameter("accionRealizada",  param.getAccionRealizada());
            sp.setParameter("operacion",  param.getOperacion());
            sp.setParameter("nombreTipoDetalle",  param.getNombreTipoDetalle());

            sp.setParameter("idUsuarioRegistro",  param.getIdUsuarioRegistro());

            sp.setParameter("funcion",  param.getFuncion());
            sp.setParameter("numero",  param.getNumero());
            sp.setParameter("maquinaEquipo",  param.getMaquinaEquipo());
            sp.setParameter("codigoTipoActividad",  param.getCodigoTipoActividad());
            sp.setParameter("tipoActividad",  param.getTipoActividad());
            sp.setParameter("esCabecera",  param.isCabecera());

            sp.execute();

            result.setData("insertado correctamente");
            result.setSuccess(true);
            result.setMessage("insertado correctamente - PGMFAbreviadoDetalleRegistrar");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - PGMFAbreviadoDetalleRegistrar", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;
    }


    /**
     * @autor: Miguel F. [08-07-2021]
     * @modificado:
     * @descripción: {insertar en participacion ciudadana y capacitación}
     *
     */
    @Override
    public ResultClassEntity PGMFAbreviadoDetalleEliminar(PGMFAbreviadoDetalleEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_OrganizacionManejo_Eliminar");
            processStored.registerStoredProcedureParameter("idOrganizacionManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idOrganizacionManejo", param.getIdOrganizacionManejo());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();

        }
        catch (Exception e){
            log.error("PGMFAbreviadoDetalleEliminar. Error: " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("No se pudo eliminar el requerimiento de personal por actividad de manejo solicitado");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó el requerimiento de personal por actividad de manejo correcamente.");
        return  result;
    }


    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<List< MonitoreoAbreviadoEntity>> Monitoreo(MonitoreoAbreviadoEntity param) {
        return null;
/*        ResultClassEntity<List< MonitoreoAbreviadoEntity>> result = new ResultClassEntity<>();
        List< MonitoreoAbreviadoEntity> lista = new ArrayList<MonitoreoAbreviadoEntity>();
            
    
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Monitoreo_Listar (?)}");
            pstm.setObject (1, param.getIdPGMF(),Types.INTEGER);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);
            while(rs.next()){
                MonitoreoAbreviadoEntity obj = new MonitoreoAbreviadoEntity();    
                obj.setId(rs.getInt("id"));
                obj.setTipo(rs.getString("tipo"));
                obj.setSubTipo(rs.getString("subtipo"));
                obj.setNombreSubTipo(rs.getString("nombreSubTipo"));
                obj.setOperacion(rs.getString("operacion"));
                obj.setDescripcion(rs.getString("descripcion"));
                obj.setCheck(rs.getBoolean("checkMonitoreo"));
                lista.add(obj);
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Monitoreo (Objetivo 8)");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - participacionCiudadana", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            
        }
        return result;*/
    }


    /**
     * @autor: Miguel F. [23-07-2021]
     * @modificado:
     * @descripción: {actualizar anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> MonitoreoActualizar(List< MonitoreoAbreviadoEntity> param) {
        
        ResultClassEntity<Boolean> result = new ResultClassEntity<>();
        
         try{
            for (MonitoreoAbreviadoEntity ent : param) {
                actualizarMonitoreo(ent);
            }
            result.setSuccess(true);
            result.setMessage("monitoreo actualiado correctamente");
            result.setData(true);
         } catch(Exception e){
            log.error("PGMFAbreviadoRepositoryImpl - MonitoreoActualizar", e.getMessage());
            result.setSuccess(false);
            result.setMessage("monitoreo actualiado - Ocurrió un problema");
            result.setData(false);
         }
    
       
        return result;
    }
    
    private Boolean actualizarMonitoreo (MonitoreoAbreviadoEntity obj){
        return null;
/*        Boolean result = false;
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Monitoreo_actualizar (?,?,?)}");
            pstm.setObject (1, obj.getId(),Types.INTEGER);
            pstm.setObject (2, obj.getCheck(),Types.BOOLEAN);
            pstm.setObject (3, obj.getDescripcion(),Types.VARCHAR);
            pstm.execute();
            result = true;
        } catch (Exception e) {
            result = false;
        }

        return result;*/
    }


    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @Override
    public ResultClassEntity OrganizacionManejo(OrganizacionManejoEntity param) {

        ResultClassEntity<List< OrganizacionManejoEntity>>result = new ResultClassEntity();


        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_OrganizacionManejo_Listar");
            processStored.registerStoredProcedureParameter("idPgmf", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPgmf",param.getIdPGMF());
            processStored.execute();

            List<OrganizacionManejoEntity> resultDet = null;
            List<Object[]> spResultDet = processStored.getResultList();

            if (spResultDet!=null && !spResultDet.isEmpty()){

                resultDet = new ArrayList<>();
                OrganizacionManejoEntity temp = null;

                    for (Object[] row : spResultDet) {
                        temp = new OrganizacionManejoEntity();

                        temp.setId(row[0]==null?null:(Integer) row[0]);
                        temp.setTipo(row[1]==null?null:(String) row[1]);
                        temp.setSubTipo(row[2]==null?null:(String) row[2]);
                        temp.setNombreSubTipo(row[3]==null?null:(String) row[3]);
                        temp.setFuncion(row[4]==null?null:(String) row[4]);
                        temp.setDescripcion(row[5]==null?null:(String) row[5]);
                        temp.setNumero(row[6]==null?null: (Integer)row[6]);
                        temp.setMaquinaEquipo(row[7]==null?null:(String) row[7]);
                        temp.setTipoActividad(row[8]==null?null:(String) row[8]);
                        temp.setEsCabecera(row[9]==null?null:(boolean) row[9]);

                        resultDet.add(temp);

                }
            }
            result.setData(resultDet);
            result.setSuccess(true);
            result.setMessage("Se listó correctamente.");
            return result;



            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - Organización Manejo", e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return  result;
        }
    }

    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {obtener anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<ProgramaInversionEntity> ProgramaInversionObtener(ProgramaInversionEntity param) {
        return null;
/*        ResultClassEntity< ProgramaInversionEntity > result = new ResultClassEntity<>();
        List<ProgramaInversionDetalleEntity> detalle = new ArrayList<>();
        ProgramaInversionEntity programaInversion = new ProgramaInversionEntity();
                
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ProgramaInversion_Obtener (?)}");
            pstm.setObject (1, param.getIdPgmf(),Types.INTEGER);
            
            ResultSet rs = pstm.executeQuery();
            result.setData(null);

            Integer idInversion = 0; 
            String descripcion = ""; 
            while(rs.next()){
                ProgramaInversionDetalleEntity obj = new ProgramaInversionDetalleEntity();    
                idInversion =  rs.getInt("NU_ID_INVERSION");
                descripcion =  rs.getString("TX_DESCRIPCION");

                obj.setIdInversionDetalle(rs.getInt("NU_ID_INVERSION_DET"));
                obj.setIngreso(rs.getString("TX_INGRESO"));
                obj.setEgreso(rs.getString("TX_EGRESO"));
                obj.setIngresoMonto(rs.getBigDecimal("NU_INGRESO_MONTO"));
                obj.setEgresoMonto(rs.getBigDecimal("NU_EGRESO_MONTO"));
                obj.setAnio(rs.getInt("NU_ANIO"));
                obj.setRubro(rs.getString("TX_RUBRO"));
                detalle.add(obj);
            }
            programaInversion.setDetalle(detalle);
            programaInversion.setIdInversion(idInversion);
            programaInversion.setDescripcion(descripcion.split("#"));


            result.setData(programaInversion);
            result.setSuccess(true);
            result.setMessage("Programa inversion (Objetivo 12)");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - ProgramaInversionObtener", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;*/
    }


    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {obtener anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> ProgramaInversionRegistrar(ProgramaInversionEntity param) {
        return null;
/*        ResultClassEntity< Boolean > result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ProgramaInversion_Registrar_Actualizar (?,?,?,?,?,?,?,?,?,?,?,?)}");
            pstm.setObject (1, param.getIdInversion(),Types.INTEGER);
            pstm.setObject (2, "SECPROIN",Types.VARCHAR);
            pstm.setObject (3, param.getDescripcion()[0],Types.VARCHAR);
            pstm.setObject (4, param.getIdPgmf(),Types.INTEGER);
            
            pstm.setObject (5, param.getIdUsuarioRegistro(),Types.INTEGER);
            pstm.setObject (6, param.getFechaRegistro(),Types.DATE);

            
            pstm.setObject (7, param.getDetalle().size() != 0 ? param.getDetalle().get(0).getIngreso() : "",Types.VARCHAR);
            pstm.setObject (8, param.getDetalle().size() != 0 ? param.getDetalle().get(0).getEgreso() : "",Types.VARCHAR);
            pstm.setObject (9, param.getDetalle().size() != 0 ? param.getDetalle().get(0).getIngresoMonto() : 0,Types.DECIMAL);
            pstm.setObject (10, param.getDetalle().size() != 0 ? param.getDetalle().get(0).getEgresoMonto() : 0,Types.DECIMAL);
            pstm.setObject (11, param.getDetalle().size() != 0 ? param.getDetalle().get(0).getAnio() : 0,Types.INTEGER);
            pstm.setObject (12, param.getDetalle().size() != 0 ? param.getDetalle().get(0).getRubro() : "",Types.VARCHAR);
            
            pstm.execute();
            result.setData(true);
            result.setSuccess(true);
            result.setMessage("Programa inversion - registro");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - ProgramaInversionObtener", e.getMessage());
            result.setSuccess(false);
            result.setData(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
        }
        return result;*/
    }

        /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {obtener anexo12 PGMF}
     *
     */
    @Override
    public ResultClassEntity<Boolean> ProgramaInversionEliminar(ProgramaInversionEntity param) {
        return null;
/*        ResultClassEntity< Boolean > result = new ResultClassEntity<>();
        try {
            Connection con = null;
            PreparedStatement pstm  = null;
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_ProgramaInversionDetalle_Eliminar (?,?,?)}");
            pstm.setObject (1, param.getIdInversion(),Types.INTEGER);
            pstm.setObject (2, param.getFechaRegistro(),Types.DATE);
            pstm.setObject (3, param.getIdUsuarioRegistro(),Types.INTEGER);
            
            
            pstm.execute();
            result.setData(true);
            result.setSuccess(true);
            result.setMessage("Programa inversion - eliminar");
            
        } catch (Exception e) {
            log.error("PGMFAbreviadoRepositoryImpl - ProgramaInversionObtener", e.getMessage());
            result.setSuccess(false);
            result.setData(false);
            result.setMessage("Ocurrió un error." + e.getMessage());
            result.setMessageExeption(e.getMessage());
        }
        return result;*/
    }
}