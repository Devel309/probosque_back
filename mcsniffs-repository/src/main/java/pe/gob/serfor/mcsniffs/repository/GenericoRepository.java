package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.ParametroNivel1Entity;
import pe.gob.serfor.mcsniffs.entity.PersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Generico.ParametroDto;

public interface GenericoRepository {
    List<ParametroEntity> ListarPorFiltroParametro(ParametroEntity param);
    ResultEntity<PersonaEntity> listarPorFilroPersona(PersonaEntity persona) throws Exception;
    List<ParametroEntity> listarParametroPorPrefijo(ParametroDto dto) throws Exception;
    List<ParametroNivel1Entity> listarParametroLineamiento(ParametroNivel1Entity param) throws Exception;
    List<PersonaEntity> listarPersonaPorFiltro(PersonaEntity persona);
}
