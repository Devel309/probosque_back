package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalFlujEstadoEntity;
import pe.gob.serfor.mcsniffs.repository.SolicitudBosqueLocalFlujEstadoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolicitudBosqueLocalFlujEstadoRepositoryImpl extends JdbcDaoSupport implements SolicitudBosqueLocalFlujEstadoRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

    @Override
    public ResultClassEntity listarSolicitudBosqueLocalFlujEstado(SolicitudBosqueLocalFlujEstadoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudBosqueLocalFlujEstadoEntity> lista = new ArrayList<>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("BosqueLocal.pa_SolicitudBosqueLocal_FlujoEstado");
            processStored.registerStoredProcedureParameter("idSolBosqueLocal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("perfil", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolBosqueLocal", obj.getIdSolBosqueLocal());
            processStored.setParameter("perfil", obj.getPerfil());
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if (spResult != null && !spResult.isEmpty()) {
                SolicitudBosqueLocalFlujEstadoEntity c = null;
                if (spResult.size() >= 1) {

                    for (Object[] row : spResult) {
                        c = new SolicitudBosqueLocalFlujEstadoEntity();
                        c.setIdUsuario(row[0] == null ? null : (Integer) row[0]);
                        c.setIdSolBosqueLocal(row[1] == null ? null : (Integer) row[1]);
                        c.setIdPersona(row[2] == null ? null : (Integer) row[2]);
                        c.setPersona(row[3] == null ? null : (String) row[3]);
                        c.setTelefono(row[4] == null ? null : (String) row[4]);
                        c.setNumeroDocumento(row[5] == null ? null : (String) row[5]);
                        c.setEmail(row[6] == null ? null : (String) row[6]);
                        c.setIdDistrito(row[7] == null ? null : (Integer) row[7]);
                        c.setIdProvincia(row[8] == null ? null : (Integer) row[8]);
                        c.setIdDepartamento(row[9] == null ? null : (Integer) row[9]);
                        c.setDepartamento(row[10] == null ? null : (String) row[10]);
                        c.setPerfil(row[11] == null ? null : (String) row[11]);
                        lista.add(c);
                    }
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            result.setMessage("Se obtuvo la lista de archivos de la solicitud de bosque local correctamente.");
            return result;

        } catch (Exception e) {
            log.error("listarSolicitudBosqueLocalArchivo", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo obtener la lista de archivos de la solicitud de bosque local.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

   
}
