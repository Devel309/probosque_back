package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadManejoEntity;

public interface UnidadManejoRepository {

    ResultClassEntity RegistrarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception;
    ResultClassEntity ActualizarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception;
    ResultClassEntity ListarUnidadManejo(UnidadManejoEntity unidadManejoEntity) throws Exception;
}
