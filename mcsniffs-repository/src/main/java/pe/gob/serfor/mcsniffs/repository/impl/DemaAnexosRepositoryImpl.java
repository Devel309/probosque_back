package pe.gob.serfor.mcsniffs.repository.impl;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.docx4j.Docx4J;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.DemaAnexosRepository;
import pe.gob.serfor.mcsniffs.repository.PGMFArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Repository
public class DemaAnexosRepositoryImpl implements DemaAnexosRepository{

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Repository en donde se procesos los anexos referentes a dema}
     * @param:
     */
    @Autowired
    CensoForestalRepository censo;

    @Autowired
    PGMFArchivoRepository repoarchivo;

    @Autowired
    private ArchivoRepository fileUp;
    
    @PersistenceContext
    private EntityManager em;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(DemaAnexosRepositoryImpl.class);

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se relacion los archivos cargados con dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultClassEntity<PGMFArchivoEntity> registrarArchivo(PGMFArchivoEntity archivo) throws Exception{
        return repoarchivo.registrarPlanManejoArchivo(archivo);
    }

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se listan los archivos cargados con dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultEntity<PlanManejoArchivoEntity> listarArchivoDema(PlanManejoArchivoEntity filtro) {
        ResultEntity<PlanManejoArchivoEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_PlanManejoArchivo_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoDocumento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", filtro.getIdPlanManejo());
            processStored.setParameter("idArchivo", filtro.getIdArchivo());
            processStored.setParameter("idTipoDocumento", filtro.getIdTipoDocumento()!=null?Integer.parseInt(filtro.getIdTipoDocumento()): null);
            processStored.setParameter("codigoProceso", filtro.getCodigoProceso());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<PlanManejoArchivoEntity> lstdocs = new ArrayList<>();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoArchivoEntity obj = new PlanManejoArchivoEntity();
                    obj.setIdPlanManejoArchivo((Integer) row[0]);
                    obj.setIdPlanManejo((Integer) row[1]);
                    obj.setIdArchivo((Integer) row[2]);
                    obj.setCodigoProceso((String) row[3]);
                    obj.setCodigoSubTipo((String) row[4]);
                    obj.setNombreArchivo((String) row[5]);
                    obj.setExtensionArchivo((String) row[6]);
                    obj.setIdTipoDocumento((String) row[7]);
                    obj.setTipoDocumento((String) row[8]);
                    obj.setDescripcion((String) row[9]);
                    obj.setObservacion((String) row[10]);
                    if (!obj.getIdArchivo().equals(null)) {
                        ResultClassEntity<ArchivoEntity> file = fileUp.obtenerArchivo(obj.getIdArchivo());
                        obj.setDocumento(file.getData().getFile());
                    }
                    lstdocs.add(obj);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se encontraron los documentos vigentes.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se genera el anexo2 de dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo2Dema(PlanManejoArchivoEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            List<Anexo2CensoEntity> lstLista = censo.ListarCensoForestalAnexo2(filtro.getIdPlanManejo(),"1");

            InputStream inputStream = AnexosPFDMRepositoryImpl.class.getResourceAsStream("/PlantillaAnexo2Dema.docx");

            XWPFDocument doc = new XWPFDocument(inputStream);

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row2 = table.getRow(1);
            XWPFTableRow total = new XWPFTableRow((CTRow) table.getRow(2).getCtRow().copy(), table);
            table.removeRow(2);
            Double totalVolumen=0.0;
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row2.getCtRow().copy(), table);

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        p.setAlignment(ParagraphAlignment.CENTER);
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text5 = r.getText(0);
                                if (text5 != null) {
                                    if (text5.contains("sectorx")) {
                                        text5 = text5.replace("sectorx", lstLista.get(i).getNombreCientificoDesc());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("codigox")) {
                                        text5 = text5.replace("codigox",
                                                lstLista.get(i).getCodArbol());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("especiex")) {
                                        text5 = text5.replace("especiex", lstLista.get(i).getNombreComunDesc());
                                        r.setText(text5, 0);
                                    } else if (text5.contains("diametrox")) {
                                        text5 = text5.replace("diametrox", toString(lstLista.get(i).getDiametro()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("alturax")) {
                                        text5 = text5.replace("alturax", toString(lstLista.get(i).getAltura()));
                                        r.setText(text5, 0);
                                    } else if (text5.contains("volumenx")) {
                                        text5 = text5.replace("volumenx", toString(lstLista.get(i).getVolumen()));
                                        r.setText(text5, 0);
                                        totalVolumen+= lstLista.get(i).getVolumen();
                                    } else if (text5.contains("observacionx")) {
                                        text5 = text5.replace("observacionx", "");
                                        r.setText(text5, 0);
                                    }
                                }
                            }
                        }
                    }
                }
                table.addRow(copiedRow);
            }
            table.removeRow(1);
            for (XWPFTableCell cell : total.getTableCells()) {
                for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        if (r != null) {
                            String text5 = r.getText(0);
                            if (text5 != null) {
                                if (text5.contains("totalx")) {
                                    text5 = text5.replace("totalx", toString(totalVolumen));
                                    r.setText(text5, 0);
                                }
                            }
                        }
                    }
                }
            }
            table.addRow(total);

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            byte[] fileContent = b.toByteArray();
            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo2.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 2.");
            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo2", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se genera el anexo3 de dema}
     * @param:PlanManejoArchivoEntity
     */
    @Override
    public ResultArchivoEntity generarAnexo3Dema(PlanManejoArchivoEntity filtro) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            //List<Anexo3CensoEntity> lstLista = censo.ListarCensoForestalAnexo3(filtro.getIdPlanManejo(), "2").getData();

            InputStream inputStream = AnexosPFDMRepositoryImpl.class.getResourceAsStream("/PlantillaAnexo3Dema.docx");

            XWPFDocument doc = new XWPFDocument(inputStream);

            XWPFTable table = doc.getTables().get(0);
            XWPFTableRow row = table.getRow(2);

            /***** Se comenta para pruebas de Lib PDF
            for (int i = 0; i < lstLista.size(); i++) {
                XWPFTableRow copiedRow = new XWPFTableRow((CTRow) row.getCtRow().copy(), table);

                for (XWPFTableCell cell : copiedRow.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            if (r != null) {
                                String text = r.getText(0);
                                if (text != null) {
                                    if (text.contains("CODIGOX")) {
                                        text = text.replace("CODIGOX", lstLista.get(i).getIdIndividuo());
                                        r.setText(text, 0);
                                    } else if (text.contains("NOMBREESX")) {
                                        text = text.replace("NOMBREESX", lstLista.get(i).getNombreComunDesc());
                                        r.setText(text, 0);
                                    } else if (text.contains("NOMBRECX")) {
                                        text = text.replace("NOMBRECX", lstLista.get(i).getNombreCientificoDesc());
                                        r.setText(text, 0);
                                    } else if (text.contains("NOMBRENX")) {
                                        text = text.replace("NOMBRENX", lstLista.get(i).getNombreNativo());
                                        r.setText(text, 0);
                                    } else if (text.contains("EX")) {
                                        text = text.replace("EX", lstLista.get(i).getEste());
                                        r.setText(text, 0);
                                    } else if (text.contains("NX")) {
                                        text = text.replace("NX", lstLista.get(i).getNorte());
                                        r.setText(text, 0);
                                    } else if (text.contains("PRODUCTOX")) {
                                        text = text.replace("PRODUCTOX", lstLista.get(i).getProducto());
                                        r.setText(text, 0);
                                    } else if (text.contains("VOLX")) {
                                        text = text.replace("VOLX", toString(lstLista.get(i).getVolumenComercial()));
                                        r.setText(text, 0);
                                    } else if (text.contains("OBSX")) {
                                        text = text.replace("OBSX", lstLista.get(i).getObservaciones());
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                table.addRow(copiedRow);
            }
            */
            table.removeRow(2);
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            doc.write(b);
            doc.close();
            //byte[] fileContent = b.toByteArray(); lib pdf

            /*********************  USO LIBRERIA PDF   *****************/
            InputStream templateInputStream =new ByteArrayInputStream(b.toByteArray());
            WordprocessingMLPackage wordMLPackageRecept = WordprocessingMLPackage.load(templateInputStream);
            File archivoConvert = File.createTempFile("DetalleRegente", ".pdf");
            String rutaArchivo = archivoConvert.getAbsolutePath(); // informacion para la descarga
            FileOutputStream outputs = new FileOutputStream(archivoConvert);
            Docx4J.toPDF(wordMLPackageRecept,outputs);
            outputs.flush();
            outputs.close();
            byte[] fileContent = Files.readAllBytes(archivoConvert.toPath());
            /******************** USO LIBRERIA PDF  ***********************/

            result.setArchivo(fileContent);
            result.setNombeArchivo("Anexo3.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se descargo el anexo 3.");

            return result;
        } catch (Exception e) {
            log.error("AnexoRepository - generarAnexo3", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
    }

    public String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString();
    }
}
