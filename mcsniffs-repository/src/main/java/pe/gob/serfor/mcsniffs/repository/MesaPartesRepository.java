package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;

public interface MesaPartesRepository {
    List<MesaPartesDto> listarMesaPartes(MesaPartesDto dto) throws Exception;
    ResultClassEntity registrarMesaPartes(MesaPartesDto dto) throws Exception;    
    ResultClassEntity eliminarMesaPartes(MesaPartesDto dto) throws Exception;
    List<MesaPartesDto> listarMesaPartesSendEmail(Integer idPlanManejo) throws Exception;
}
