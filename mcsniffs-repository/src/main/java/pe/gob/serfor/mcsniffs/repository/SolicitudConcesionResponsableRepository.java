package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface SolicitudConcesionResponsableRepository {
    ResultClassEntity obtenerSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj);
    ResultClassEntity registrarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj);
    ResultClassEntity actualizarSolicitudConcesionResponsable(SolicitudConcesionResponsableDto obj);

}
