package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;

import java.util.List;

public interface EvaluacionAmbientalRepository {

        ResultClassEntity RegistrarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
                        throws Exception;

        ResultClassEntity ActualizarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
                        throws Exception;

        ResultClassEntity EliminarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
                        throws Exception;

        ResultClassEntity ObtenerEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity)
                        throws Exception;

        ResultClassEntity RegistrarEvaluacionAmbientalDetalle(
                        List<EvaluacionAmbientalDetalleEntity> evaluacionAmbientalDetalleList) throws Exception;

        ResultClassEntity ActualizarEvaluacionAmbientalDetalle(
                        List<EvaluacionAmbientalDetalleEntity> evaluacionAmbientalDetalleList) throws Exception;

        ResultClassEntity EliminarEvaluacionAmbientalDetalle(
                        EvaluacionAmbientalDetalleEntity evaluacionAmbientalDetalleEntity) throws Exception;

        ResultClassEntity ObtenerEvaluacionAmbientalDetalle(
                        EvaluacionAmbientalDetalleEntity evaluacionAmbientalDetalleEntity) throws Exception;

        List<EvaluacionAmbientalAprovechamientoEntity> ListarEvaluacionAprovechamientoFiltro(
                        EvaluacionAmbientalAprovechamientoEntity EvaluacionAmbientalAprovechamientoEntity)
                        throws Exception;

        ResultClassEntity RegistrarAprovechamientoEvaluacionAmbiental(
                        EvaluacionAmbientalAprovechamientoEntity EvaluacionAmbientalAprovechamientoEntity)
                        throws Exception;

        List<EvaluacionAmbientalActividadEntity> ListarEvaluacionActividadFiltro(
                        EvaluacionAmbientalActividadEntity evaluacionAmbientalActividadEntity) throws Exception;

        ResultClassEntity RegistrarActividadEvaluacionAmbiental(
                        EvaluacionAmbientalActividadEntity evaluacionAmbientalActividadEntity) throws Exception;

        ResultClassEntity RegistrarEvaluacionAmbientalDetalleSub(List<EvaluacionAmbientalEntity> list) throws Exception;

        ResultClassEntity RegistrarEvaluacionAmbientalDetalleSubXls(EvaluacionAmbientalEntity item);

        List<EvaluacionAmbientalDto> ListarEvaluacionAmbiental(
                        EvaluacionAmbientalDto EvaluacionAmbientalAprovechamientoEntity) throws Exception;

        ResultClassEntity EvaluacionAmbientalAprovechamiento_Eliminar(EvaluacionAmbientalAprovechamientoEntity param) throws Exception;

        ResultClassEntity eliminarEvaluacionAmbientalActividad(EvaluacionAmbientalActividadEntity param) throws Exception;
        ResultClassEntity eliminarEvaluacionAmbientalAprovechamientoList(EvaluacionAmbientalAprovechamientoEntity param) throws Exception;
        ResultClassEntity eliminarEvaluacionAmbiental(EvaluacionAmbientalEntity param) throws Exception;
}
