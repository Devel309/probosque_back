package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ResponsableFormacionAcademicaRepository {

    ResultClassEntity listarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj);
    ResultClassEntity registrarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj);
    ResultClassEntity actualizarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj);
    ResultClassEntity eliminarResponsableFormacionAcademica(ResponsableFormacionAcademicaEntity obj);
}
