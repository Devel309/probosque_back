package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CensoForestal.CensoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha.ProyeccionCosechaCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha.ProyeccionCosechaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha.ProyeccionCosechaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioCabeceraDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio.VolumenComercialPromedioEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.CensoForestalRepository;
import pe.gob.serfor.mcsniffs.repository.ParametroRepository;
import pe.gob.serfor.mcsniffs.repository.util.FechaUtil;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

@Repository
public class CensoForestalRepositoryImpl extends JdbcDaoSupport implements CensoForestalRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    @Autowired
    private ParametroRepository parametroValorRepository;

    private static final Logger log = LogManager.getLogger(CensoForestalDetalleRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public Integer RegistrarCensoForestal(int idUsuario, String Correlativo,String tipoCenso) throws Exception {
        Integer idCensoForestal = null;
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_CensoForestal_Registrar");
            processStored.registerStoredProcedureParameter("IdUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Correlativo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCenso", String.class, ParameterMode.IN);

            processStored.setParameter("IdUsuarioRegistro", idUsuario);
            processStored.setParameter("Correlativo", Correlativo);
            processStored.setParameter("tipoCenso", tipoCenso);

            processStored.execute();

            List<BigDecimal> spResult = processStored.getResultList();
            idCensoForestal = spResult.get(0).intValueExact();
        } finally {

        }

        return idCensoForestal;
    }

    

    
    @Override
    public List<ParametroValorEntity> ListarTipoBosque() throws Exception {
        // ResultClassEntity<List<ParametroValorEntity>> result = new
        // ResultClassEntity<>();
        List<ParametroValorEntity> lista = new ArrayList<ParametroValorEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoBosque_Listar");

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ParametroValorEntity temp = new ParametroValorEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValor1((String) row[1]);
                    temp.setPrefijo((String) row[2]);
                    lista.add(temp);
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListarTipoBosque", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ParametroValorEntity> ListarEspecieNombreComun() throws Exception {
        List<ParametroValorEntity> lista = new ArrayList<ParametroValorEntity>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Especie_ListarEspecieNombreComun");

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ParametroValorEntity temp = new ParametroValorEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValor1((String) row[1]);
                    temp.setPrefijo((String) row[0]);
                    lista.add(temp);
                }
            }

        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListarEspecieNombreComun", e.getMessage());
        }

        return lista;
    }

    @Override
    public List<ParametroValorEntity> ListarEspecieNombreCientifico() throws Exception {
        List<ParametroValorEntity> lista = new ArrayList<ParametroValorEntity>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Especie_ListarEspecieNombreCientifico");

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ParametroValorEntity temp = new ParametroValorEntity();
                    temp.setCodigo((String) row[0]);
                    temp.setValor1((String) row[1]);
                    temp.setPrefijo((String) row[2]);

                    lista.add(temp);
                }
            }

        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListarEspecieNombreCientifico", e.getMessage());
        }

        return lista;
    }

    @Override
    public CensoForestalDto CensoForestalObtener() throws Exception {
        CensoForestalDto censoForestal = new CensoForestalDto();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_CensoForestal_ObtenerEstadoA");

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    censoForestal.setIdCensoForestal((Integer) row[0]);
                }
            }

            return censoForestal;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - CensoForestalObtener", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ContratoTHDto> ListaTHContratosObtener(ContratoTHDto param) throws Exception {
        List<ContratoTHDto> lista = new ArrayList<ContratoTHDto>();
        try {
            StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_TituloHabilitante_Listar");
            proc.registerStoredProcedureParameter("tipoProceso", Integer.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("codigoTituloTh", String.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(proc);
            proc.setParameter("tipoProceso", param.getTipoProceso());
            proc.setParameter("codigoTituloTh", param.getCodigoTituloTh());
            proc.setParameter("pageNum", param.getPageNum());
            proc.setParameter("pageSize", param.getPageSize());

            proc.execute();
            List<Object[]> spResult = proc.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ContratoTHDto temp = new ContratoTHDto();
                    temp.setIdContrato((Integer) row[0]);
                    temp.setCodigoContrato((String) row[1]);
                    temp.setUbigeo((String) row[2]);
                    temp.setTipoPlanManejo((String) row[3]);
                    temp.setTipoProceso((Integer) row[4]);
                    temp.setTotalRecord((Integer) row[5]);
                    lista.add(temp);
                    
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaTHContratosObtener", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PlanManejoAppEntity> ListaPlanManejoPorContratoObtener(Integer idContrato,String tipoPlanManejo,String tipoCensoForestal) throws Exception {
        List<PlanManejoAppEntity> lista = new ArrayList<PlanManejoAppEntity>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PlanManejo_FiltrarPorContrato");

            processStored.registerStoredProcedureParameter("idContrato", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlanManejo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCensoForestal", String.class, ParameterMode.IN);
            processStored.setParameter("idContrato", idContrato);
            processStored.setParameter("tipoPlanManejo", tipoPlanManejo);
            processStored.setParameter("tipoCensoForestal", tipoCensoForestal);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PlanManejoAppEntity temp = new PlanManejoAppEntity();
                    temp.setCodigoTipoInfogeneral((String) ""+row[0]);
                    temp.setIdContratoTH((String) ""+row[1]);
                    temp.setIdPlanManejoTH((String) ""+row[2]);
                    temp.setAreaTotalConcesion((String) ""+row[3]);
                    temp.setAreaBosqueProduccionForestal((String) ""+row[4]);
                    temp.setAreaProteccion((String) ""+row[5]);
                    temp.setBloqueQuinquenales((String) ""+row[6]);
                    temp.setSuperficieMaderable((String) ""+row[7]);
                    temp.setSuperficieNoMaderable((String) ""+row[8]);
                    temp.setNombreCompleto((String) ""+row[9]);
                    temp.setUbigeoPlanManejo((String) ""+row[10]);
                    temp.setIdCensoForestal((String) ""+row[11]);
                    temp.setTipoCensoForestal((String) ""+row[12]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaPlanManejoPorContratoObtener", e.getMessage());
            throw e;
        }
    }

    @Override
    public DatosTHDto ContratoObtener(Integer idContrato) throws Exception {
        DatosTHDto data = new DatosTHDto();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Contrato_ObtenerPorIdContrato");

            processStored.registerStoredProcedureParameter("IdContrato", Integer.class, ParameterMode.IN);
            processStored.setParameter("IdContrato", idContrato);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    data.setArea((String) row[0]);
                    data.setAreaParcela((String) row[1]);
                    data.setFechaFin((Date) row[2]);
                    data.setFechaInicio((Date) row[3]);
                }
            }

            return data;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ContratoObtener", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<ListaEspecieDto> ListaTipoRecursoForestal(String tipoEspecie,String tipoCenso) throws Exception {
        
        List<ListaEspecieDto> lista = new ArrayList<ListaEspecieDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ListaTipoRecursoForestal");
            processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCenso", String.class, ParameterMode.IN);
            processStored.setParameter("tipoEspecie", tipoEspecie);
            processStored.setParameter("tipoCenso", tipoCenso);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    
                    ListaEspecieDto temp = new ListaEspecieDto();
                    temp.setNombreComun((Integer) row[0]);
                    temp.setNombreCientifico((Integer) row[1]);
                    temp.setNombreIdiomaNativo((Integer) row[2]);
                    temp.setNumeroArbolesAprovechables((Integer) row[3]);
                    temp.setNumeroArbolSemilleros((Integer) row[4]);
                    temp.setVolumenComercial(((BigDecimal) row[5]).doubleValue());
                    temp.setNumeroArbolTotal((Integer) row[6]);
                    temp.setSuperficieComercial(((BigDecimal) row[7]).doubleValue());
                    lista.add(temp);
                }
            }
            

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaTipoRecursoForestal", e.getMessage());
            throw e;
        }
        
    }

    @Override
    public ResultadosRecursosForestalesMaderablesDto  ObtenerDatosCensoComercial(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        ResultadosRecursosForestalesMaderablesDto temp = new ResultadosRecursosForestalesMaderablesDto();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_ActividadAprovechamientorRFM_Obtener");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaTotalCensada", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("contadorEspecies", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("numeroArbolesMaderables", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("numeroArbolesNoMaderables", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("numeroArbolesTotal", Integer.class, ParameterMode.OUT);

            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            System.out.println("ÄNTES ObtenerDatosCensoComercial");
            processStored.execute();
            System.out.println("DESPUES  ObtenerDatosCensoComercial");
            temp.setAreaTotalCensada(""+processStored.getOutputParameterValue("areaTotalCensada"));
            temp.setContadorEspecies(""+processStored.getOutputParameterValue("contadorEspecies"));
            temp.setNumeroArbolesMaderables(""+processStored.getOutputParameterValue("numeroArbolesMaderables"));
            temp.setNumeroArbolesNoMaderables(""+processStored.getOutputParameterValue("numeroArbolesNoMaderables"));
            temp.setNumeroArbolesTotal(""+processStored.getOutputParameterValue("numeroArbolesTotal"));
            return temp;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ObtenerDatosCensoComercial", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ListaEspecieDto> ListaTipoRecursoForestalManejo(Integer idPlanManejo,String tipoCenso,String tipoEspecie) throws Exception {

        List<ListaEspecieDto> lista = new ArrayList<ListaEspecieDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RecursoForestal_ListarTipoEspecie");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCenso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoCenso", tipoCenso);
            processStored.setParameter("tipoEspecie", tipoEspecie);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    ListaEspecieDto temp = new ListaEspecieDto();
                    temp.setNombreComun((Integer) row[0]);
                    temp.setNombreCientifico((Integer) row[1]);
                    temp.setNombreIdiomaNativo((Integer) row[2]);
                    temp.setNumeroArbolesAprovechables((Integer) row[3]);
                    temp.setCantProducto((Integer) row[4]);
                    temp.setNumeroArbolSemilleros((Integer) row[5]);
                    temp.setVolumenComercial(((BigDecimal) row[6]).doubleValue());
                    temp.setNumeroArbolTotal((Integer) row[7]);
                    temp.setSuperficieComercial(((BigDecimal) row[8]).doubleValue());
                    temp.setNombre((String) row[9]);
                    lista.add(temp);
                }
            }


            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaTipoRecursoForestal", e.getMessage());
            throw e;
        }

    }

    @Override
    public List<ListaEspecieDto> ListaResumenEspecies(Integer idPlanManejo,String tipoCenso,String tipoEspecie) throws Exception {

        List<ListaEspecieDto> lista = new ArrayList<ListaEspecieDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RecursoForestal_ResumenEspecies");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCenso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoCenso", tipoCenso);
            processStored.setParameter("tipoEspecie", tipoEspecie);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        ListaEspecieDto temp = new ListaEspecieDto();
                        temp.setNombreComun((Integer) row[0]);
                        temp.setCientifico((String) row[1]);
                        temp.setNativo((String) row[2]);
                        temp.setNumeroArbolesAprovechables((Integer) row[3]);
                        temp.setCantProducto((Integer) row[4]);
                        temp.setNumeroArbolSemilleros((Integer) row[5]);
                        temp.setVolumenComercial(((BigDecimal) row[6]).doubleValue());
                        temp.setNumeroArbolTotal((Integer) row[7]);
                        temp.setSuperficieComercial(((BigDecimal) row[8]).doubleValue());
                        temp.setNombre((String) row[9]);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaTipoRecursoForestal", e.getMessage());
            throw e;
        }

    }

    @Override
    public List<ListaEspecieDto> ListaResumenEspeciesInventario(Integer idPlanManejo,String tipoRecurso,String tipoInventario)  throws Exception {

        List<ListaEspecieDto> lista = new ArrayList<ListaEspecieDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RecursoForestal_ResumenInventario");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoRecurso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoInventario", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoRecurso", tipoRecurso);
            processStored.setParameter("tipoInventario", tipoInventario);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    ListaEspecieDto temp = new ListaEspecieDto();
                    temp.setNombre((String) row[0]);
                    temp.setParcela((Integer) row[1]);
                    temp.setNumeroArbolTotal((Integer) row[2]);
                    temp.setNumeroArbolesAprovechables((Integer) row[3]);
                    temp.setNumeroArbolSemilleros((Integer) row[4]);
                    temp.setCantProducto((Integer) row[5]);
                    temp.setProducto((String) row[6]);
                    temp.setVolumenAprovechable((BigDecimal) row[7]);
                    temp.setTotalBarricas((Integer) row[8]);
                    temp.setUnidadMedida((String) row[9]);
                    temp.setCantidad((Integer) row[10]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaResumenEspeciesInventario", e.getMessage());
            throw e;
        }

    }


    @Override
    public List<CensoForestalDetalleEntity> ListarCensoForestalDetalle(Integer idPlanManejo, String tipoCenso, String tipoRecurso) throws Exception {
        List<CensoForestalDetalleEntity> lista = new ArrayList<CensoForestalDetalleEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestalDetalle_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoCenso", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoRecurso", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoCenso", tipoCenso);
            processStored.setParameter("tipoRecurso", tipoRecurso);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        CensoForestalDetalleEntity temp = new CensoForestalDetalleEntity();
                        temp.setIdCensoForestalDetalle((Integer) row[0]);
                        temp.setParcelaCorte((Integer) row[1]);
                        temp.setBloque((Integer) row[2]);
                        temp.setFaja((Integer) row[3]);
                        temp.setIdTipoRecurso((String) row[4]);
                        temp.setIdTipoBosque(((Integer) row[5]));
                        temp.setIdTipoEvaluacion(((Integer) row[6]));
                        temp.setCorrelativo((String) row[7]);
                        temp.setIdCodigoEspecie(((Integer) row[8]));
                        temp.setIdNombreComun(Integer.toString((Integer) row[9]));
                        temp.setIdNombreCientifico(Integer.toString((Integer) row[10]));
                        temp.setDescripcionOtros((String) row[11]);
                        temp.setIdEspecieCodigo((Integer) row[12]);
                        temp.setIdTipoArbol((String) row[13]);
                        temp.setDap(((BigDecimal) row[14]).doubleValue());
                        temp.setAlturaComercial(((BigDecimal) row[15]).doubleValue());
                        temp.setAlturaTotal(((BigDecimal) row[16]).doubleValue());
                        temp.setCodigoArbolCalidad((String) row[17]);
                        temp.setVolumen(((BigDecimal) row[18]).doubleValue());
                        temp.setFactorForma(((BigDecimal) row[19]).doubleValue());
                        temp.setCategoriaDiametrica(((BigDecimal) row[20]).doubleValue());
                        temp.setCoorEste((String) row[21]);
                        temp.setCoorNorte((String)  row[22]);
                        temp.setZona((Integer) row[23]);
                        temp.setNota((String) row[24]);
                        temp.setCantidad(((BigDecimal) row[25]).doubleValue());
                        temp.setCodigoUnidadMedida((String) row[26]);
                        temp.setProductoTipo((String) row[27]);
                        temp.setSuperficieComercial(((BigDecimal) row[28]).doubleValue());
                        temp.setNombreComun((String) row[29]);
                        temp.setNombreCientifico((String) row[30]);
                        temp.setNombreNativo((String) row[31]);
                        temp.setFechaRegistro((Timestamp) row[32]);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaT Censo Forestal Detalle", e.getMessage());
            throw e;
        }

    }

    @Override
    public List<CensoForestalEntity> SincronizacionCenso(Integer idPlanManejo, String tipoCenso, String tipoRecurso) throws Exception {
        List<CensoForestalEntity> lista = new ArrayList<CensoForestalEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestal_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1){
                    for (Object[] row1 : spResult) {

                        CensoForestalEntity temp1 = new CensoForestalEntity();
                        temp1.setIdCensoForestal((Integer) row1[0]);
                        temp1.setIdPlanManejo((Integer) row1[1]);
                        temp1.setTipoCenso((String) row1[2]);
                        temp1.setStrFechaInicio((String) row1[3]);
                        temp1.setStrFechaFin((String) row1[4]);

                        List<CensoForestalDetalleEntity> listDet = new ArrayList<CensoForestalDetalleEntity>();
                        StoredProcedureQuery processStored2 = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestalDet_Listar");
                        processStored2.registerStoredProcedureParameter("idCensoForestal", Integer.class, ParameterMode.IN);
                        SpUtil.enableNullParams(processStored2);
                        processStored2.setParameter("idCensoForestal", temp1.getIdCensoForestal());
                        processStored2.execute();
                        List<Object[]> spResult2 = processStored2.getResultList();
                        if(spResult2!=null){
                            if (spResult2.size() >= 1){
                                for (Object[] row : spResult2) {

                                    CensoForestalDetalleEntity temp = new CensoForestalDetalleEntity();
                                    temp.setIdCensoForestalDetalle((Integer) row[0]);
                                    temp.setParcelaCorte((Integer) row[1]);
                                    temp.setBloque((Integer) row[2]);
                                    temp.setFaja((Integer) row[3]);
                                    temp.setIdTipoRecurso((String) row[4]);
                                    temp.setIdTipoBosque(((Integer) row[5]));
                                    temp.setIdTipoEvaluacion(((Integer) row[6]));
                                    temp.setCorrelativo((String) row[7]);
                                    temp.setIdCodigoEspecie(((Integer) row[8]));
                                    temp.setIdNombreComun(Integer.toString((Integer) row[9]));
                                    temp.setIdNombreCientifico(Integer.toString((Integer) row[10]));
                                    temp.setDescripcionOtros((String) row[11]);
                                    temp.setIdEspecieCodigo((Integer) row[12]);
                                    temp.setIdTipoArbol((String) row[13]);
                                    temp.setDap(((BigDecimal) row[14]).doubleValue());
                                    temp.setAlturaComercial(((BigDecimal) row[15]).doubleValue());
                                    temp.setAlturaTotal(((BigDecimal) row[16]).doubleValue());
                                    temp.setCodigoArbolCalidad((String) row[17]);
                                    temp.setVolumen(((BigDecimal) row[18]).doubleValue());
                                    temp.setFactorForma(((BigDecimal) row[19]).doubleValue());
                                    temp.setCategoriaDiametrica(((BigDecimal) row[20]).doubleValue());
                                    temp.setCoorEste((String) row[21]);
                                    temp.setCoorNorte((String)  row[22]);
                                    temp.setZona((Integer) row[23]);
                                    temp.setNota((String) row[24]);
                                    temp.setCantidad(((BigDecimal) row[25]).doubleValue());
                                    temp.setCodigoUnidadMedida((String) row[26]);
                                    temp.setProductoTipo((String) row[27]);
                                    temp.setSuperficieComercial(((BigDecimal) row[28]).doubleValue());
                                    temp.setNombreComun((String) row[29]);
                                    temp.setNombreCientifico((String) row[30]);
                                    temp.setNombreNativo((String) row[31]);
                                    listDet.add(temp);
                                }
                            }
                        }
                        temp1.setListCensoForestalDetalle(listDet);
                        lista.add(temp1);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - Sincronizacion del censo", e.getMessage());
            throw e;
        }

    }


    @Override
    public List<Anexo2CensoEntity> ListarCensoForestalAnexo2(Integer idPlanManejo, String tipoEspecie) throws Exception {
        List<Anexo2CensoEntity> lista = new ArrayList<Anexo2CensoEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_Anexo2_Listar]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoEspecie", tipoEspecie);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    Anexo2CensoEntity temp = new Anexo2CensoEntity();
                    temp.setCodArbol((String) row[0]);
                    temp.setNombreComun(((Integer) row[1]));
                    temp.setDiametro(((BigDecimal) row[2]).doubleValue());
                    temp.setAltura(((BigDecimal) row[3]).doubleValue());
                    temp.setVolumen(((BigDecimal) row[4]).doubleValue());
                    temp.setEste("");
                    temp.setNorte("");
                    temp.setTipoArbol((String) row[5]);
                    temp.setNombreComunDesc((String) row[6]);
                    temp.setNombreCientificoDesc((String) row[7]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaT anexo 2", e.getMessage());
            throw e;
        }

    }

    @Override
    public ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo2VolumenTotal(Integer idPlanManejo, String tipoEspecie)  {
        return null;
        /*
        ResultEntity<Anexo3CensoEntity> result = new ResultEntity<Anexo3CensoEntity>();
        List<Anexo3CensoEntity> lista = new ArrayList<Anexo3CensoEntity>();
        try {
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Anexo2_Total");
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPlanManejo",idPlanManejo);
        processStored.setParameter("tipoEspecie",tipoEspecie);
        processStored.execute();
        List<Object[]> spResult = processStored.getResultList();
        if(spResult!=null){
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    Anexo3CensoEntity temp = new Anexo3CensoEntity();
                    temp.setVolumenComercial((Double) row[0]);
                    lista.add(temp);
                }
            }
        }

            result.setData(lista);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los totales.");
            return  result;
        }  catch (Exception e) {
            log.error("lista", e.getMessage());
            throw e;
        }
        */

    }


    @Override
    public ResultEntity<Anexo3CensoEntity> ListarCensoForestalAnexo3(Integer idPlanManejo, String tipoEspecie)  {
        return null;
        /*
        ResultEntity<Anexo3CensoEntity> result = new ResultEntity<Anexo3CensoEntity>();
        List<Anexo3CensoEntity> lista = new ArrayList<Anexo3CensoEntity>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Anexo3_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoEspecie", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo",idPlanManejo);
            processStored.setParameter("tipoEspecie",tipoEspecie);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        Anexo3CensoEntity temp = new Anexo3CensoEntity();
                        temp.setVolumenComercial((Double) row[0]);
                        temp.setNombreComun((Integer) row[1]);
                        temp.setNombreCientifico((Integer) row[2]);
                        temp.setVolumenComercial((Double) row[3]);
                        temp.setProducto((String) row[4]);
                        temp.setIdIndividuo((String) row[5]);
                        temp.setNombreNativo((String) row[6]);
                        temp.setEste((String) row[7]);
                        temp.setNorte((String) row[8]);
                        temp.setObservaciones((String) row[9]);
                        temp.setNombreComunDesc((String) row[10]);
                        temp.setNombreCientificoDesc((String) row[10]);

                        lista.add(temp);
                    }
                }
            }

            result.setData(lista);
            result.setIsSuccess(true);
            result.setMessage("Se listo la información del anexo 3.");
            return  result;
        } catch (Exception e) {
            log.error("lista", e.getMessage());
            throw e;
        }
        */
    }

    @Override
    public List<PoligonosDto> ContratolistaPoligono(Integer idPlanManejo) throws Exception {
        List<PoligonosDto> lista = new ArrayList<PoligonosDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_Contrato_ListarPoligono");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    PoligonosDto temp = new PoligonosDto();
                    temp.setIdPlanManejo(""+row[0]);
                    temp.setTipoGeometria(""+row[1]);
                    temp.setGeometria(""+row[2]);
                    temp.setIdArchivo(""+row[3]);
                    temp.setOrdenamientoPoligonoDtoList(ListaOrdenamientoPoligonos(idPlanManejo,Integer.parseInt(temp.getIdArchivo())));
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ContratolistaPoligono", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<OrdenamientoPoligonoDto>  ListaOrdenamientoPoligonos(Integer idPlanManejo,Integer idArchivo) throws Exception {
        List<OrdenamientoPoligonoDto> lista = new ArrayList<OrdenamientoPoligonoDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_OrdenamientoPoligono_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("idArchivo", idArchivo);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    OrdenamientoPoligonoDto temp = new OrdenamientoPoligonoDto();
                    temp.setIdOrdenamientoDet(""+row[0]);
                    temp.setAreaHa(""+row[1]);
                    temp.setBloqueQuinquenal(""+row[2]);
                    temp.setParcelaCorta(""+row[3]);
                    temp.setIdPlanManejo(""+row[4]);
                    temp.setIdArchivo(""+row[5]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListaOrdenamientoPoligonos", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultEntity<CensoForestalDetalleDto> listarEspecieXCenso(CensoForestalDto censoForestalDto) throws Exception {
        List<CensoForestalDetalleDto> lista = new ArrayList<CensoForestalDetalleDto>();
        ResultEntity result = new ResultEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoForestal_ListarEspecie");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCensoForestal", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", censoForestalDto.getIdPlanManejo());
            processStored.setParameter("idCensoForestal", censoForestalDto.getIdCensoForestal());

            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {

                    CensoForestalDetalleDto temp = new CensoForestalDetalleDto();

                    temp.setIdCodigoEspecie(((Integer) row[0]));
                    temp.setNombreCientifico((String) row[1]);
                    temp.setNombreComun((String) row[2]);


                    lista.add(temp);
                }
            }


            result.setData(lista);
            result.setMessage("Información encontrada");
            result.setIsSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - Lista especie Censo Forestal ", e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setIsSuccess(false);
            return result;
        }

    }

    @Override
    public List<ListaEspecieDto>  ListaEspecieDto(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        List<ListaEspecieDto> lista = new ArrayList<ListaEspecieDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ActividadAprovechamientoEspecies_Listar");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ListaEspecieDto temp = new ListaEspecieDto();
                    temp.setIdCodigoEspecie(""+  row[0]);
                    temp.setTextNombreComun(""+  row[1]);
                    temp.setTextNombreCientifico(""+ row[2]);
                    temp.setFamilia(""+ row[3]);
                    temp.setLineaProduccion("");
                    temp.setDmc(row[4] != null ?(String)row[4]: "");
                    temp.setGc(row[5] != null ?(String)row[5]: "");
                    lista.add(temp);
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl --- ListaEspecieDto", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ListaEspecieDto>  ListaEspeciesInventariadasMaderables(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        List<ListaEspecieDto> lista = new ArrayList<ListaEspecieDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ActividadAprovechamientoEspecies_Listar");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ListaEspecieDto temp = new ListaEspecieDto();
                    temp.setIdCodigoEspecie(""+  row[0]);
                    temp.setTextNombreComun(""+  row[1]);
                    temp.setTextNombreCientifico(""+ row[2]);
                    temp.setFamilia(""+ row[3]);
                    temp.setLineaProduccion("");
                    temp.setDmc(row[4] != null ?(String)row[4]: "");
                    temp.setGc(row[5] != null ?(String)row[5]: "");
                    temp.setNombreNativo(""+  row[6]);
                    lista.add(temp);
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl --- ListaEspecieDto", e.getMessage());
            throw e;
        }
    }

    @Override
    public  List<ResultadosRFMTabla>  ResultadosRecursosForestalesMaderablesDto(Integer idPlanDeManejo, String tipoPlan,String  areaPC) throws Exception {
        List<ResultadosRFMTabla> lista = new ArrayList<ResultadosRFMTabla>();

        try {
            System.out.println("ÄNTES areaPC "+areaPC);
            Double area=Double.parseDouble(areaPC);
            DecimalFormat df = new DecimalFormat("0.0000");
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ActividadAprovechamientorRFM_Listar");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ResultadosRFMTabla temp = new ResultadosRFMTabla();
                    temp.setNombreEspecie(""+ row[0]);
                    temp.setDap30a39(""+ row[1]);
                    temp.setDap40a49(""+ row[2]);
                    temp.setDap50a59(""+ row[3]);
                    temp.setDap60a69(""+ row[4]);
                    temp.setDap70a79(""+ row[5]);
                    temp.setDap80a89(""+ row[6]);
                    temp.setDap90aMas(""+ row[7]);
                    Double  dapTotalPC=Double.parseDouble(temp.getDap30a39())+
                            Double.parseDouble(temp.getDap40a49())+
                            Double.parseDouble(temp.getDap50a59())+
                            Double.parseDouble(temp.getDap60a69())+
                            Double.parseDouble(temp.getDap70a79())+
                            Double.parseDouble(temp.getDap80a89())+
                            Double.parseDouble(temp.getDap90aMas());
                    temp.setDapTotalPC(""+dapTotalPC);
                    temp.setDapTotalporHa(""+df.format(dapTotalPC/area)  );
                    temp.setVol30a39(""+ row[8]);
                    temp.setVol40a49(""+ row[9]);
                    temp.setVol50a59(""+ row[10]);
                    temp.setVol60a69(""+ row[11]);
                    temp.setVol70a79(""+ row[12]);
                    temp.setVol80a89(""+ row[13]);
                    temp.setVol90aMas(""+ row[14]);
                    Double  volTotalPC=Double.parseDouble(temp.getVol30a39())+
                            Double.parseDouble(temp.getVol40a49())+
                            Double.parseDouble(temp.getVol50a59())+
                            Double.parseDouble(temp.getVol60a69())+
                            Double.parseDouble(temp.getVol70a79())+
                            Double.parseDouble(temp.getVol80a89())+
                            Double.parseDouble(temp.getVol90aMas());
                    temp.setVolTotalPC(""+volTotalPC);
                    temp.setVolTotalporHa(""+df.format(volTotalPC/area) );
                    lista.add(temp);
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl --- ResultadosRecursosForestalesMaderablesDto", e.getMessage());
            throw e;
        }
    }

    @Override
    public  List<ResultadosRFMTabla>  ResultadosCensoComercialAprovechamientoNoMaderable(Integer idPlanDeManejo, String tipoPlan,String  areaPC) throws Exception {
        List<ResultadosRFMTabla> lista = new ArrayList<ResultadosRFMTabla>();

        try {
            Double area=Double.parseDouble(areaPC);
            DecimalFormat df = new DecimalFormat("0.0000");
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CensoComercialAprovechamientoNM_Listar");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ResultadosRFMTabla temp = new ResultadosRFMTabla();
                    temp.setNombreEspecie(""+ row[0]);
                    temp.setUnidadC(""+ row[1]);
                    temp.setDapTotalPC(""+row[2]);
                    temp.setDapTotalporHa(""+row[3]);
                    temp.setVolTotalPC(""+row[4]);
                    temp.setVolTotalporHa(""+row[5]);
                    lista.add(temp);
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl --- ResultadosCensoComercialAprovechamientoNoMaderable", e.getMessage());
            throw e;
        }
    }


    @Override
    public  List<ResultadosVolumenDeCortaTabla>  ResultadosVolumenDeCorta(Integer idPlanDeManejo, String tipoPlan,Integer numeroPc) throws Exception {
        List<ResultadosVolumenDeCortaTabla> lista = new ArrayList<ResultadosVolumenDeCortaTabla>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ActividadAprovechamientoVolumenCorta_Listar");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ResultadosVolumenDeCortaTabla temp = new ResultadosVolumenDeCortaTabla();
                    temp.setNombreComun(""+ row[0]);
                    temp.setNombreCientifico(""+ row[1]);
                    List<ParcelaCorteDto> parcelaCorteDtos= new ArrayList<ParcelaCorteDto>();
                    for(int j=0;j< numeroPc;j++){
                        ParcelaCorteDto parcelaCorteTemporal=new ParcelaCorteDto();
                        String valor1=""+ row[2*j+2];
                        String valor2=""+ row[2*j+3];
                        parcelaCorteTemporal.setNumArbolesPC( (valor1.equals("") || valor1.equals("null") ) ? "0" : valor1 );
                        parcelaCorteTemporal.setVolumenPC((valor2.equals("") || valor2.equals("null") ) ? "0" : valor2);
                        parcelaCorteDtos.add(parcelaCorteTemporal);
                    }
                    temp.setParcelaCorteDtos(parcelaCorteDtos);

                    String numArbolesTotales=""+ row[2*numeroPc+2];
                    String volumenTotal=""+ row[2*numeroPc+3];
                    temp.setNumArbolesTotales((numArbolesTotales.equals("") || numArbolesTotales.equals("null") ) ? "0" : numArbolesTotales );
                    temp.setVolumenTotal((volumenTotal.equals("") || volumenTotal.equals("null") ) ? "0" : volumenTotal );
                    lista.add(temp);
                }
            }

            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosVolumenDeCorta", e.getMessage());
            throw e;
        }
    }

    @Override
    public VolumenCortaDto  ObtenerDatosVolumenCorta(Integer idPlanDeManejo, String tipoPlan) throws Exception {
        VolumenCortaDto temp = new VolumenCortaDto();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_ActividadAprovechamientoVolumenCorta_Obtener");
            processStored.registerStoredProcedureParameter("idPlanDeManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("areaTotalCPc", Integer.class, ParameterMode.OUT);
            processStored.registerStoredProcedureParameter("numeroPc", String.class, ParameterMode.OUT);  // de Integer a String

            processStored.setParameter("idPlanDeManejo", idPlanDeManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            String areaTotalPC=""+processStored.getOutputParameterValue("areaTotalCPc");
            String contadorPC=""+processStored.getOutputParameterValue("numeroPc");
            areaTotalPC=(areaTotalPC.equals("") || areaTotalPC.equals("null") ) ? "0" : areaTotalPC;
            contadorPC=(contadorPC.equals("") || contadorPC.equals("null") ) ? "0" : contadorPC;
            temp.setAreaTotalPc(areaTotalPC);
            temp.setContadorPc(Integer.parseInt(contadorPC));
            return temp;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ObtenerDatosCensoComercial", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Anexo2Dto>  ResultadosPOConcesionesAnexos2(Integer idPlanManejo, String tipoPlan,Integer tipoProceso,String unidadMedida) throws Exception {
        List<Anexo2Dto> lista = new ArrayList<Anexo2Dto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_POConcesionesAnexos2_Listar]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoProceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("unidadMedidaResumen", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("tipoProceso", tipoProceso);
            processStored.setParameter("unidadMedidaResumen", unidadMedida);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    Anexo2Dto temp = new Anexo2Dto();
                    temp.setUnidadTrabajo(""+row[0]);
                    temp.setNumeroFaja(""+row[1]);
                    temp.setnArbol(""+row[2]);
                    temp.setCodigo(""+row[3]);
                    temp.setNombreEspecies(""+row[4]);
                    temp.setDap(""+row[5]);
                    temp.setAlturaComercial(""+row[6]);
                    temp.setVolumen(""+row[7]);
                    temp.setCalidadFuste(""+row[8]);
                    temp.setEste(""+row[9]);
                    temp.setNorte(""+row[10]);
                    temp.setCategoria(""+row[11]);
                    temp.setIdCensoForestalDetalle(""+row[12]);
                    temp.setNombreComun(""+row[13]);
                    temp.setNombreCientifico(""+row[14]);
                    temp.setNombreNativo(""+row[15]);
                    temp.setProducto(""+row[16]);
                    temp.setCondicion(""+row[17]);
                    temp.setTipoRecurso(""+row[18]);
                    temp.setnArbolesAprovechables((Integer)row[19]);
                    temp.setnArbolesSemilleros((Integer)row[20]);
                    temp.setnTotalArboles((Integer)row[21]);
                    temp.setUnidadMedida(""+row[22]);
                    temp.setCantidadProducto(""+row[23]);
                    temp.setCodEspecie(""+row[24]);
                    temp.setNumeroDeParcelas((Integer)row[25]);
                    temp.setCantidadDeEspecies((Integer)row[26]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosPOConcesionesAnexos2", e.getMessage());
            throw e;
        }
    }


    @Override
    public ResultClassEntity  AprovechamientoForestalNMResumen(Integer idPlanManejo, String tipoPlan,Integer tipoProceso) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            ParametroValorEntity param=new ParametroValorEntity(null,null,"UNDME");
            List<ParametroValorEntity>  listaUnidadMedida=parametroValorRepository.ListarPorCodigoParametroValor(param);
            List<ResultadoAprovechamientoForestalNMResumenDto> lista = new ArrayList<>();
            if (listaUnidadMedida != null && !listaUnidadMedida.isEmpty()) {
                for (ParametroValorEntity row : listaUnidadMedida) {
                    ResultadoAprovechamientoForestalNMResumenDto resumen = new ResultadoAprovechamientoForestalNMResumenDto();
                    resumen.setUnidadMedidaResumen(row.getValor1());
                    resumen.setListaAnexo2Dto(ResultadosPOConcesionesAnexos2( idPlanManejo,  tipoPlan, tipoProceso, row.getCodigo()));
                    if(resumen.getListaAnexo2Dto().size()<1) continue;
                    lista.add(resumen);
                }
            }
            result.setData(lista);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("AprovechamientoForestalNMResumen - AprovechamientoForestalNMResumen", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return  result;
        }
    }

    @Override
    public ResultClassEntity  ResultadoInventarioPorParcela(Integer idPlanManejo, String tipoPlan,Integer tipoProceso) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            System.out.println("idPlanManejo "+idPlanManejo);
            System.out.println("tipoPlan "+tipoPlan);
            System.out.println("tipoProceso "+tipoProceso);
            List<ResultadoAprovechamientoForestalNMResumenDto> lista = new ArrayList<>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_ResultadoInventarioPorParcela]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoProceso", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("tipoProceso", tipoProceso);
            System.out.println("Antes nombreEspecie ");
            processStored.execute();
            System.out.println("DESPUES nombreEspecie ");
            List<Object[]> spResult = processStored.getResultList();
            System.out.println("DESPUES spResult  "+spResult.size());
            if (spResult.size() >= 1){
                System.out.println("IF DESPUES spResult  "+spResult.size());
                for (Object[] row : spResult) {
                    System.out.println("nombreEspecie "+row[0]);
                    String nombreEspecie=""+row[0];
                    ResultadoAprovechamientoForestalNMResumenDto resumen = new ResultadoAprovechamientoForestalNMResumenDto();
                    resumen.setNombreEspecie(nombreEspecie);
                    resumen.setListaAnexo2Dto(ResultadoInventarioParcelaAgrupadoPorEspecie( idPlanManejo,  tipoPlan, tipoProceso,nombreEspecie));
                    if(resumen.getListaAnexo2Dto().size()<1) continue;
                    lista.add(resumen);
                }
            }else  System.out.println("ELSE  ");
            result.setData(lista);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("ResultadoInventarioPorParcela - ResultadoInventarioPorParcela", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. "+ e.getMessage());
            return  result;
        }
    }


    @Override
    public List<Anexo2Dto>  ResultadoInventarioParcelaAgrupadoPorEspecie(Integer idPlanManejo, String tipoPlan,Integer tipoProceso,String nombreEspecie) throws Exception {
        List<Anexo2Dto> lista = new ArrayList<Anexo2Dto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_ResultadoInventarioPorParcela]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoProceso", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreEspecie", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("tipoProceso", tipoProceso);
            processStored.setParameter("nombreEspecie", nombreEspecie);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    Anexo2Dto temp = new Anexo2Dto();
                    temp.setNombreParcelaCorta(""+row[0]);
                    temp.setnArbolesAprovechables((Integer) row[1]);
                    temp.setnArbolesSemilleros((Integer)row[2]);
                    temp.setProducto(""+row[3]);
                    temp.setUnidadMedida(""+row[4]);
                    temp.setnTotalArboles((Integer)row[5]);
                    temp.setAprov(""+row[6]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadoInventarioParcelaAgrupadoPorEspecie", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Anexo2Dto>  ResultadosAnexo7NoMaderable(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<Anexo2Dto> lista = new ArrayList<Anexo2Dto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_Anexo7CensoComercialNM_Listar]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    Anexo2Dto temp = new Anexo2Dto();
                    temp.setUnidadTrabajo(""+row[0]);
                    temp.setNumeroFaja(""+row[1]);
                    temp.setnArbol(""+row[2]);
                    temp.setCodigo(""+row[3]);
                    temp.setNombreEspecies(""+row[4]);
                    temp.setDap(""+row[5]);
                    temp.setAlturaComercial(""+row[6]);
                    temp.setVolumen(""+row[7]);
                    temp.setCalidadFuste(""+row[8]);
                    temp.setEste(""+row[9]);
                    temp.setNorte(""+row[10]);
                    temp.setCategoria(""+row[11]);
                    temp.setIdCensoForestalDetalle(""+row[12]);
                    temp.setC(""+row[13]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosAnexo7NoMaderable", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Anexo2Dto>  ResultadosBuscarAnexo7NoMaderable(Integer idPlanManejo, String tipoPlan,Anexo2Dto data) throws Exception {
        List<Anexo2Dto> lista = new ArrayList<Anexo2Dto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("[dbo].[pa_Anexo7CensoComercialNM_Buscar]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("faja", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("numeroArbol", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoArbol", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreEspecie", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("este", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("norte", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("campoC", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("categoria", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("faja", data.getNumeroFaja());
            processStored.setParameter("numeroArbol", data.getnArbol());
            processStored.setParameter("codigoArbol", data.getCodigo());
            processStored.setParameter("nombreEspecie", data.getNombreEspecies());
            processStored.setParameter("este", data.getEste());
            processStored.setParameter("norte", data.getNorte());
            processStored.setParameter("campoC", data.getC());
            processStored.setParameter("categoria", data.getCategoria());
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row : spResult) {
                    Anexo2Dto temp = new Anexo2Dto();
                    temp.setUnidadTrabajo(""+row[0]);
                    temp.setNumeroFaja(""+row[1]);
                    temp.setnArbol(""+row[2]);
                    temp.setCodigo(""+row[3]);
                    temp.setNombreEspecies(""+row[4]);
                    temp.setDap(""+row[5]);
                    temp.setAlturaComercial(""+row[6]);
                    temp.setVolumen(""+row[7]);
                    temp.setCalidadFuste(""+row[8]);
                    temp.setEste(""+row[9]);
                    temp.setNorte(""+row[10]);
                    temp.setCategoria(""+row[11]);
                    temp.setIdCensoForestalDetalle(""+row[12]);
                    temp.setC(""+row[13]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosBuscarAnexo7NoMaderable", e.getMessage());
            throw e;
        }
    }


    @Override
    public  List<List<Anexo2PGMFDto>> ResultadosPMFICAnexo3(Integer idPlanManejo, String tipoPlan)  throws Exception {
        List<List<Anexo2PGMFDto>> lista = new  ArrayList<List<Anexo2PGMFDto>>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    String   idTipoBosque=""+row[0];
                    String   nombreBosque=""+row[1];
                    List<Anexo2PGMFDto>  temp =  new  ArrayList<Anexo2PGMFDto>();
                    temp=ResultadosFormatoPGMFAnexo2( idPlanManejo,  tipoPlan, idTipoBosque,nombreBosque);
                    if(temp!=null && temp.size()>0)
                        lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosPMFICAnexo3  ", e.getMessage());
            throw e;
        }
    }

    @Override
    public  List<List<Anexo3PGMFDto>> ResultadosPMFICAnexo4(Integer idPlanManejo, String tipoPlan)  throws Exception {
        List<List<Anexo3PGMFDto>> lista = new  ArrayList<List<Anexo3PGMFDto>>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    String   idTipoBosque=""+row[0];
                    String   nombreBosque=""+row[1];
                    List<Anexo3PGMFDto>  temp =  new  ArrayList<Anexo3PGMFDto>();
                    temp=ResultadosFormatoPGMFAnexo3( idPlanManejo,  tipoPlan, idTipoBosque,nombreBosque);
                    if(temp!=null && temp.size()>0)
                        lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosPMFICAnexo4  ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Anexo2PGMFDto> ResultadosFormatoPGMFAnexo2(Integer idPlanManejo, String tipoPlan,String idTipoBosque,String   nombreBosque) throws Exception {
        List<Anexo2PGMFDto> lista = new ArrayList<Anexo2PGMFDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_FormatoPGMFAnexos2");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    String valorVacio="0.0000";
                    if(valorVacio.equals(""+row[8]))
                        continue;
                    Anexo2PGMFDto temp = new Anexo2PGMFDto();
                    temp.setNombreComun(""+row[0]);
                    temp.setArbHaDap30a39(""+row[1]);
                    temp.setArbHaDap40a49(""+row[2]);
                    temp.setArbHaDap50a59(""+row[3]);
                    temp.setArbHaDap60a69(""+row[4]);
                    temp.setArbHaDap70a79(""+row[5]);
                    temp.setArbHaDap80a89(""+row[6]);
                    temp.setArbHaDap90aMas(""+row[7]);
                    temp.setArbHaTotalTipoBosque(""+row[8]);
                    temp.setArbHaTotalPorArea(""+row[9]);
                    temp.setAbHaDap30a39(""+row[10]);
                    temp.setAbHaDap40a49(""+row[11]);
                    temp.setAbHaDap50a59(""+row[12]);
                    temp.setAbHaDap60a69(""+row[13]);
                    temp.setAbHaDap70a79(""+row[14]);
                    temp.setAbHaDap80a89(""+row[15]);
                    temp.setAbHaDap90aMas(""+row[16]);
                    temp.setAbHaTotalTipoBosque(""+row[17]);
                    temp.setAbHaTotalPorArea(""+row[18]);
                    temp.setVolHaDap30a39(""+row[19]);
                    temp.setVolHaDap40a49(""+row[20]);
                    temp.setVolHaDap50a59(""+row[21]);
                    temp.setVolHaDap60a69(""+row[22]);
                    temp.setVolHaDap70a79(""+row[23]);
                    temp.setVolHaDap80a89(""+row[24]);
                    temp.setVolHaDap90aMas(""+row[25]);
                    temp.setVolHaTotalTipoBosque(""+row[26]);
                    temp.setVolHaTotalPorArea(""+row[27]);
                    temp.setAreaBosque(""+row[28]);
                    temp.setNombreBosque(nombreBosque);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosFormatoPGMFAnexo2 ", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<Anexo3PGMFDto> ResultadosFormatoPGMFAnexo3(Integer idPlanManejo, String tipoPlan,String idTipoBosque,String nombreBosque) throws Exception {
        List<Anexo3PGMFDto> lista = new ArrayList<Anexo3PGMFDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_FormatoPGMFAnexos3");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    String valorVacio="0.0000";
                    if(valorVacio.equals(""+row[7]))
                        continue;
                    Anexo3PGMFDto temp = new Anexo3PGMFDto();
                    temp.setNombreComun(""+row[0]);
                    temp.setArbHaDap10a19(""+row[1]);
                    temp.setArbHaDap20a29(""+row[2]);
                    temp.setArbHaTotalTipoBosque(""+row[3]);
                    temp.setArbHaTotalPorArea(""+row[4]);
                    temp.setAbHaDap10a19(""+row[5]);
                    temp.setAbHaDap20a29(""+row[6]);
                    temp.setAbHaTotalTipoBosque(""+row[7]);
                    temp.setAbHaTotalPorArea(""+row[8]);
                    temp.setAreaBosque(""+row[13]);
                    temp.setNombreBosque(nombreBosque);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - pa_FormatoPGMFAnexos3 ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Anexo3PGMFDto> PGMFInventarioExploracionFustales(Integer idPlanManejo, String tipoPlan,String idTipoBosque) throws Exception {
        List<Anexo3PGMFDto> lista = new ArrayList<Anexo3PGMFDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PGMFInventarioExploracionBloque");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            /****/
            if (spResult.size() >= 1) {

                for (Object[] row : spResult) {

                    Integer bloque =  (Integer)row[0];

                     processStored = entityManager
                            .createStoredProcedureQuery("dbo.pa_PGMFInventarioExploracionFustales");
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("bloque", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idPlanManejo", idPlanManejo);
                    processStored.setParameter("tipoPlan", tipoPlan);
                    processStored.setParameter("idTipoBosque", idTipoBosque);
                    processStored.setParameter("bloque", bloque);
                    processStored.execute();
                    List<Object[]> spResult2 = processStored.getResultList();

                    if (spResult2.size() >= 1) {
                        for (Object[] row2 : spResult2) {
                            String valorVacio="0.0000";
                            //if(valorVacio.equals(""+row2[7]))
                                //continue;
                            Anexo3PGMFDto temp = new Anexo3PGMFDto();
                            temp.setNombreComun(""+row2[0]);
                            temp.setArbHaDap10a19(""+row2[1]);
                            temp.setArbHaDap20a29(""+row2[2]);
                            temp.setArbHaTotalTipoBosque(""+row2[3]);
                            temp.setArbHaTotalPorArea(""+row2[4]);
                            temp.setAbHaDap10a19(""+row2[5]);
                            temp.setAbHaDap20a29(""+row2[6]);
                            temp.setAbHaTotalTipoBosque(""+row2[7]);
                            temp.setAbHaTotalPorArea(""+row2[8]);
                            temp.setBloque(bloque);
                            lista.add(temp);
                        }
                    }

                }
            }
            /***/
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - pa_PGMFInventarioExploracionFustales ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ResultadosEspeciesMuestreoDto> ResultadosEspecieMuestreo(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<ResultadosEspeciesMuestreoDto> lista = new ArrayList<ResultadosEspeciesMuestreoDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    ResultadosEspeciesMuestreoDto  temp = new ResultadosEspeciesMuestreoDto();
                    Integer idTipoBosque=Integer.parseInt(""+row[0]);
                    temp.setIdTipoBosque(""+row[0]);
                    temp.setNombreBosque(""+row[1]);
                    List<TablaEspeciesMuestreo> listaEspeciesMuestreo = new ArrayList<TablaEspeciesMuestreo>();
                    StoredProcedureQuery procedimientoAlmacenado = entityManager
                            .createStoredProcedureQuery("dbo.pa_EspeciesMuestreo_Lista");
                    procedimientoAlmacenado.registerStoredProcedureParameter("tipoBosque", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    procedimientoAlmacenado.setParameter("tipoBosque", idTipoBosque);
                    procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                    procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                    procedimientoAlmacenado.execute();
                    List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] columna : spResult) {
                            TablaEspeciesMuestreo temporal = new TablaEspeciesMuestreo();
                            temporal.setEspecies(""+columna[0]);
                            temporal.setNumeroArbolesTotalBosque(""+columna[1]);
                            temporal.setAreaBasalTotalBosque(""+columna[2]);
                            temporal.setVolumenTotalBosque(""+columna[3]);
                            temporal.setNumeroArbolesTotalHa(""+columna[4]);
                            temporal.setAreaBasalTotalHa(""+columna[5]);
                            temporal.setVolumenTotalHa(""+columna[6]);
                            listaEspeciesMuestreo.add(temporal);
                        }
                    }
                    temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosEspecieMuestreo  ", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultadosArbolesAprovechablesMaderables ResultadosArbolesAprovechables(Integer idPlanManejo, String tipoPlan) throws Exception {
        ResultadosArbolesAprovechablesMaderables resultado = new ResultadosArbolesAprovechablesMaderables();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            System.out.println("ANTES ResultadosArbolesAprovechables");
            processStored.execute();
            System.out.println("DESPUES ResultadosArbolesAprovechables");
            Double areaBosqueTotal=0.00;
            List<String>  listaTipoBosque= new ArrayList<String>();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    Integer idTipoBosque=Integer.parseInt(""+row[0]);
                    listaTipoBosque.add(""+row[1]);
                    areaBosqueTotal=areaBosqueTotal+Double.parseDouble(""+row[2]);
                }
                resultado.setnHaInventariadas(""+areaBosqueTotal);
                resultado.setListaTipoBosque(listaTipoBosque);

                StoredProcedureQuery procedimientoAlmacenado = entityManager
                        .createStoredProcedureQuery("dbo.pa_ArbolesAprovechablesMaderables_listar");
                procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                procedimientoAlmacenado.execute();
                List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                List<TablaArbolesAprovechablesMaderablesDto> lista = new ArrayList<TablaArbolesAprovechablesMaderablesDto>();
                if (spResult.size() >= 1) {
                    for (Object[] columna : spResult) {
                        TablaArbolesAprovechablesMaderablesDto temp = new TablaArbolesAprovechablesMaderablesDto();
                        temp.setEspecie(""+ columna[0]);
                        List<DatosTipoBosqueDto>  datosTipoBosqueDtos= new ArrayList<DatosTipoBosqueDto>();
                        Integer totalTipoBosque=resultado.getListaTipoBosque().size();
                        for(int j=0;j<totalTipoBosque;j++){
                            DatosTipoBosqueDto datosTipoBosqueTemporal=new DatosTipoBosqueDto();
                            String valor1=""+ columna[2*j+1];
                            String valor2=""+ columna[2*j+2];
                            datosTipoBosqueTemporal.setnArboles( (valor1.equals("") || valor1.equals("null") ) ? "0" : valor1 );
                            datosTipoBosqueTemporal.setVolumenComercial((valor2.equals("") || valor2.equals("null") ) ? "0" : valor2);
                            datosTipoBosqueDtos.add(datosTipoBosqueTemporal);
                        }
                        temp.setTipoBosqueDtos(datosTipoBosqueDtos);
                        String numArbolesTotales=""+ columna[2*totalTipoBosque+1];
                        String volumenTotal=""+ columna[2*totalTipoBosque+2];
                        temp.setnArbolesTotal((numArbolesTotales.equals("") || numArbolesTotales.equals("null") ) ? "0" : numArbolesTotales );
                        temp.setVolumenTotal((volumenTotal.equals("") || volumenTotal.equals("null") ) ? "0" : volumenTotal );
                        lista.add(temp);
                    }
                    resultado.setTablaArbolesAprovechablesMaderablesDtoList(lista);
                }
            }
            return resultado;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosArbolesAprovechables  ", e.getMessage());
            throw e;
        }
    }


    @Override
    public ResultadosArbolesAprovechablesMaderables ResultadosAprovechableForestalNoMaderable(Integer idPlanManejo, String tipoPlan) throws Exception {
        ResultadosArbolesAprovechablesMaderables resultado = new ResultadosArbolesAprovechablesMaderables();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            Double areaBosqueTotal=0.00;
            List<String>  listaTipoBosque= new ArrayList<String>();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    Integer idTipoBosque=Integer.parseInt(""+row[0]);
                    listaTipoBosque.add(""+row[1]);
                    areaBosqueTotal=areaBosqueTotal+Double.parseDouble(""+row[2]);
                }
                resultado.setnHaInventariadas(""+areaBosqueTotal);
                resultado.setListaTipoBosque(listaTipoBosque);

                StoredProcedureQuery procedimientoAlmacenado = entityManager
                        .createStoredProcedureQuery("dbo.pa_AprovechableForestalNoMaderable_listar");
                procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                procedimientoAlmacenado.execute();
                List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                List<TablaArbolesAprovechablesMaderablesDto> lista = new ArrayList<TablaArbolesAprovechablesMaderablesDto>();
                if (spResult.size() >= 1) {
                    for (Object[] columna : spResult) {
                        TablaArbolesAprovechablesMaderablesDto temp = new TablaArbolesAprovechablesMaderablesDto();
                        temp.setEspecie(""+ columna[0]);
                        List<DatosTipoBosqueDto>  datosTipoBosqueDtos= new ArrayList<DatosTipoBosqueDto>();
                        Integer totalTipoBosque=resultado.getListaTipoBosque().size();
                        for(int j=0;j<totalTipoBosque;j++){
                            DatosTipoBosqueDto datosTipoBosqueTemporal=new DatosTipoBosqueDto();
                            String valor1=""+ columna[2*j+1];
                            String valor2=""+ columna[2*j+2];
                            datosTipoBosqueTemporal.setnArboles( (valor1.equals("") || valor1.equals("null") ) ? "0" : valor1 );
                            datosTipoBosqueTemporal.setUnidadCantidad((valor2.equals("") || valor2.equals("null") ) ? "0" : valor2);
                            datosTipoBosqueDtos.add(datosTipoBosqueTemporal);
                        }
                        temp.setTipoBosqueDtos(datosTipoBosqueDtos);
                        String numArbolesTotales=""+ columna[2*totalTipoBosque+1];
                        String volumenTotal=""+ columna[2*totalTipoBosque+2];
                        temp.setnArbolesTotal((numArbolesTotales.equals("") || numArbolesTotales.equals("null") ) ? "0" : numArbolesTotales );
                        temp.setUnidadCantidadTotal((volumenTotal.equals("") || volumenTotal.equals("null") ) ? "0" : volumenTotal );
                        lista.add(temp);
                    }
                    resultado.setTablaArbolesAprovechablesMaderablesDtoList(lista);
                }
            }
            return resultado;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosAprovechableForestalNoMaderable  ", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultadosArbolesAprovechablesMaderables ResultadosArbolesAprovechablesFustales(Integer idPlanManejo, String tipoPlan) throws Exception {
        ResultadosArbolesAprovechablesMaderables resultado = new ResultadosArbolesAprovechablesMaderables();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            Double areaBosqueTotal=0.00;
            List<String>  listaTipoBosque= new ArrayList<String>();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    Integer idTipoBosque=Integer.parseInt(""+row[0]);
                    listaTipoBosque.add(""+row[1]);
                    areaBosqueTotal=areaBosqueTotal+Double.parseDouble(""+row[2]);
                }
                resultado.setnHaInventariadas(""+areaBosqueTotal);
                resultado.setListaTipoBosque(listaTipoBosque);

                StoredProcedureQuery procedimientoAlmacenado = entityManager
                        .createStoredProcedureQuery("dbo.pa_ArbolesAprovechablesMaderablesFustales_listar");
                procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                procedimientoAlmacenado.execute();
                List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                List<TablaArbolesAprovechablesMaderablesDto> lista = new ArrayList<TablaArbolesAprovechablesMaderablesDto>();
                if (spResult.size() >= 1) {
                    for (Object[] columna : spResult) {
                        TablaArbolesAprovechablesMaderablesDto temp = new TablaArbolesAprovechablesMaderablesDto();
                        temp.setEspecie(""+ columna[0]);
                        List<DatosTipoBosqueDto>  datosTipoBosqueDtos= new ArrayList<DatosTipoBosqueDto>();
                        Integer totalTipoBosque=resultado.getListaTipoBosque().size();
                        for(int j=0;j<totalTipoBosque;j++){
                            DatosTipoBosqueDto datosTipoBosqueTemporal=new DatosTipoBosqueDto();
                            String valor1=""+ columna[2*j+1];
                            String valor2=""+ columna[2*j+2];
                            datosTipoBosqueTemporal.setnArboles( (valor1.equals("") || valor1.equals("null") ) ? "0" : valor1 );
                            datosTipoBosqueTemporal.setAreaBasal((valor2.equals("") || valor2.equals("null") ) ? "0" : valor2);
                            datosTipoBosqueDtos.add(datosTipoBosqueTemporal);
                        }
                        temp.setTipoBosqueDtos(datosTipoBosqueDtos);
                        String numArbolesTotales=""+ columna[2*totalTipoBosque+1];
                        String areaBasalTotal=""+ columna[2*totalTipoBosque+2];
                        temp.setnArbolesTotal((numArbolesTotales.equals("") || numArbolesTotales.equals("null") ) ? "0" : numArbolesTotales );
                        temp.setAreaBasalTotal((areaBasalTotal.equals("") || areaBasalTotal.equals("null") ) ? "0" : areaBasalTotal );
                        lista.add(temp);
                    }
                    resultado.setTablaArbolesAprovechablesMaderablesDtoList(lista);
                }
            }
            return resultado;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosArbolesAprovechablesFustales  ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ResultadosAnexo6> ResultadosAnexo6(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<ResultadosAnexo6> lista = new ArrayList<ResultadosAnexo6>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    ResultadosAnexo6  temp = new ResultadosAnexo6();
                    Integer idTipoBosque=Integer.parseInt(""+row[0]);
                    temp.setIdTipoBosque(""+row[0]);
                    temp.setNombreBosque(""+row[1]);
                    temp.setAreaBosque(""+row[2]);
                    List<TablaAnexo6Dto> listaTablaAnexo6Dto = new ArrayList<TablaAnexo6Dto>();
                    StoredProcedureQuery procedimientoAlmacenado = entityManager
                            .createStoredProcedureQuery("dbo.pa_Anexo6NoMaderable_Listar");
                    procedimientoAlmacenado.registerStoredProcedureParameter("tipoBosque", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    procedimientoAlmacenado.setParameter("tipoBosque", idTipoBosque);
                    procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                    procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                    procedimientoAlmacenado.execute();
                    List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] columna : spResult) {
                            TablaAnexo6Dto temporal = new TablaAnexo6Dto();
                            temporal.setNombreComun(""+columna[0]);
                            temporal.setnTotalPorTipoBosque(""+columna[1]);
                            temporal.setnTotalPorHa( ""+columna[2]);
                            temporal.setcTotalPorTipoBosque(""+columna[3]);
                            temporal.setcTotalPorHa(""+columna[3]);
                            listaTablaAnexo6Dto.add(temporal);
                        }
                    }
                    temp.setListaTablaAnexo6Dto(listaTablaAnexo6Dto);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosAnexo6  ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ResultadosEspeciesMuestreoDto> ResultadosEspecieFrutales(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<ResultadosEspeciesMuestreoDto> lista = new ArrayList<ResultadosEspeciesMuestreoDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spListaBosquesCensados = processStored.getResultList();
            if (spListaBosquesCensados.size() >= 1) {
                for (Object[] row : spListaBosquesCensados) {
                    ResultadosEspeciesMuestreoDto  temp = new ResultadosEspeciesMuestreoDto();
                    Integer idTipoBosque=Integer.parseInt(""+row[0]);
                    temp.setIdTipoBosque(""+row[0]);
                    temp.setNombreBosque(""+row[1]);
                    List<TablaEspeciesMuestreo> listaEspeciesMuestreo = new ArrayList<TablaEspeciesMuestreo>();

                    StoredProcedureQuery procedimientoAlmacenado = entityManager
                            .createStoredProcedureQuery("dbo.pa_EspeciesFrutales_Lista");
                    procedimientoAlmacenado.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    procedimientoAlmacenado.setParameter("idTipoBosque", idTipoBosque);
                    procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                    procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                    procedimientoAlmacenado.execute();
                    List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] columna : spResult) {
                            TablaEspeciesMuestreo temporal = new TablaEspeciesMuestreo();
                            temporal.setEspecies(""+columna[0]);
                            temporal.setNumeroArbolesTotalBosque(""+columna[1]);
                            temporal.setAreaBasalTotalBosque(""+columna[2]);
                            temporal.setNumeroArbolesTotalHa(""+columna[3]);
                            temporal.setAreaBasalTotalHa(""+columna[4]);
                            listaEspeciesMuestreo.add(temporal);
                        }
                    }
                    temp.setListTablaEspeciesMuestreo(listaEspeciesMuestreo);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ResultadosEspecieFrutales  ", e.getMessage());
            throw e;
        }
    }


    @Override
    public List<AprovechamientoRFNMDto> ResultadosAprovechamientoRFNM(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<AprovechamientoRFNMDto> lista = new ArrayList<AprovechamientoRFNMDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_EspeciesCensadas_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spListaEspeciesCensados = processStored.getResultList();
            if (spListaEspeciesCensados.size() >= 1) {
                for (Object[] row : spListaEspeciesCensados) {
                    String nombreEspecie=""+row[0];
                    AprovechamientoRFNMDto  temp = new AprovechamientoRFNMDto();
                    temp.setEspecie(""+row[0]);
                    temp.setProductoAprovechar(""+row[1]);
                    List<ResultadosAprovechamientoRFNMDto> resultadosAprovechamientoRFNMDtoList = new ArrayList<ResultadosAprovechamientoRFNMDto>();
                    StoredProcedureQuery procedimientoAlmacenado = entityManager
                            .createStoredProcedureQuery("dbo.pa_ResultadosAprovechamientoRFNM_Lista");
                    procedimientoAlmacenado.registerStoredProcedureParameter("nombreEspecie", String.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    procedimientoAlmacenado.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    procedimientoAlmacenado.setParameter("nombreEspecie", nombreEspecie);
                    procedimientoAlmacenado.setParameter("idPlanManejo", idPlanManejo);
                    procedimientoAlmacenado.setParameter("tipoPlan", tipoPlan);
                    procedimientoAlmacenado.execute();
                    List<Object[]> spResult = procedimientoAlmacenado.getResultList();
                    if (spResult.size() >= 1) {
                        for (Object[] columna : spResult) {
                            ResultadosAprovechamientoRFNMDto temporal = new ResultadosAprovechamientoRFNMDto();
                            temporal.setSector(""+columna[0]);
                            temporal.setArea(""+columna[1]);
                            temporal.setnTotalIndividuos(""+columna[2]);
                            temporal.setIndividuosHa(""+columna[3]);
                            temporal.setVolumen(""+columna[4]);
                            resultadosAprovechamientoRFNMDtoList.add(temporal);
                        }
                    }
                    temp.setResultadosAprovechamientoRFNMDtos(resultadosAprovechamientoRFNMDtoList);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - AprovechamientoRFNMDto  ", e.getMessage());
            throw e;
        }
    }

    /**
     * @autor: Danny Nazario [07-01-2022]
     * @modificado:
     * @descripción: { Lista Potencial Producción Resultados Fustales }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @Override
    public ResultClassEntity listarResultadosFustales(Integer idPlanManejo, String tipoPlan) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoBosquesCensados_Lista");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            List<ResultadosEspeciesMuestreoDto> objList = new ArrayList<>();
            if (spResult != null && !spResult.isEmpty()) {
                for (Object[] row : spResult) {
                    ResultadosEspeciesMuestreoDto  obj = new ResultadosEspeciesMuestreoDto();
                    Integer tipoBosque = row[0] == null ? null : (Integer) row[0];
                    obj.setIdTipoBosque(row[0] == null ? null : (String) row[0].toString());
                    obj.setNombreBosque(row[1] == null ? null : (String) row[1]);

                    StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_PotencialProduccionFustales_Listar");
                    sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    sp.registerStoredProcedureParameter("tipoBosque", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(sp);
                    sp.setParameter("idPlanManejo", idPlanManejo);
                    sp.setParameter("tipoPlan", tipoPlan);
                    sp.setParameter("tipoBosque", tipoBosque);
                    sp.execute();

                    List<Object[]> spResult2 = sp.getResultList();
                    List<TablaEspeciesMuestreo> objList2 = new ArrayList<>();
                    if (spResult2 != null && !spResult2.isEmpty()) {
                        for (Object[] row2 : spResult2) {
                            TablaEspeciesMuestreo obj2 = new TablaEspeciesMuestreo();
                            obj2.setEspecies(row2[0] == null ? null : (String) row2[0]);
                            obj2.setNumeroArbolesDap10a14(row2[1] == null ? null : (String) row2[1].toString());
                            obj2.setNumeroArbolesDap15a19(row2[2] == null ? null : (String) row2[2].toString());
                            obj2.setNumeroArbolesTotalHa(row2[3] == null ? null : (String) row2[3].toString());
                            obj2.setNumeroArbolesTotalBosque(row2[4] == null ? null : (String) row2[4].toString());
                            obj2.setAreaBasalDap10a14(row2[5] == null ? null : (String) row2[5].toString());
                            obj2.setAreaBasalDap15a19(row2[6] == null ? null : (String) row2[6].toString());
                            obj2.setAreaBasalTotalHa(row2[7] == null ? null : (String) row2[7].toString());
                            obj2.setAreaBasalTotalBosque(row2[8] == null ? null : (String) row2[8].toString());
                            objList2.add(obj2);
                        }
                    }
                    obj.setListTablaEspeciesMuestreo(objList2);
                    objList.add(obj);
                }
            }
            result.setData(objList);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error("CensoForestal - " + e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public Integer obtenerCensoForestalCabecera(Integer idPlanManejo) throws Exception {
        Integer censoCabecera=null;
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_CensoCabeceraVigente");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCensoForestalCabecera", Integer.class, ParameterMode.OUT);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.execute();
            censoCabecera=Integer.parseInt(""+processStored.getOutputParameterValue("idCensoForestalCabecera"));
            return censoCabecera;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - obtenerCensoForestalCabecera ", e.getMessage());
            throw e;
        }
    }

    
    @Override
    public List<Anexo2PGMFDto>  PGMFInventarioExploracionVolumenP(Integer idPlanManejo, String tipoPlan,String idTipoBosque) throws Exception {
        List<Anexo2PGMFDto> lista = new ArrayList<Anexo2PGMFDto>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PGMFInventarioExploracionBloque");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();

            if (spResult.size() >= 1) {

                for (Object[] row : spResult) {

                    Integer bloque =  (Integer)row[0];
                        /**********/
                    processStored = entityManager
                            .createStoredProcedureQuery("dbo.pa_PGMFInventarioExploracionVP");
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("bloque", Integer.class, ParameterMode.IN);
                    processStored.setParameter("idPlanManejo", idPlanManejo);
                    processStored.setParameter("tipoPlan", tipoPlan);
                    processStored.setParameter("idTipoBosque", idTipoBosque);
                    processStored.setParameter("bloque", bloque);
                    processStored.execute();
                    List<Object[]> spResult2 = processStored.getResultList();

                    if (spResult2.size() >= 1) {
                        for (Object[] row2 : spResult2) {
                            String valorVacio="0.0000";
                            if(valorVacio.equals(""+row2[8]))
                                continue;
                            Anexo2PGMFDto temp = new Anexo2PGMFDto();
                            temp.setNombreComun(""+row2[0]);
                            temp.setArbHaDap20a29(""+row2[1]);
                            temp.setArbHaDap30a39(""+row2[2]);
                            temp.setArbHaDap40a49(""+row2[3]);
                            temp.setArbHaDap50a59(""+row2[4]);
                            temp.setArbHaDap60a69(""+row2[5]);
                            temp.setArbHaDap70aMas(""+row2[6]);
                            temp.setArbHaTotalTipoBosque(""+row2[7]);
                            temp.setArbHaTotalPorArea(""+row2[8]);
                            temp.setAbHaDap20a29(""+row2[9]);
                            temp.setAbHaDap30a39(""+row2[10]);
                            temp.setAbHaDap40a49(""+row2[11]);
                            temp.setAbHaDap50a59(""+row2[12]);
                            temp.setAbHaDap60a69(""+row2[13]);
                            temp.setAbHaDap70aMas(""+row2[14]);
                            temp.setAbHaTotalTipoBosque(""+row2[15]);
                            temp.setAbHaTotalPorArea(""+row2[16]);
                            temp.setVolHaDap20a29(""+row2[17]);
                            temp.setVolHaDap30a39(""+row2[18]);
                            temp.setVolHaDap40a49(""+row2[19]);
                            temp.setVolHaDap50a59(""+row2[20]);
                            temp.setVolHaDap60a69(""+row2[21]);
                            temp.setVolHaDap70aMas(""+row2[22]);
                            temp.setVolHaTotalTipoBosque(""+row2[23]);
                            temp.setVolHaTotalPorArea(""+row2[24]);
                            temp.setBloque(bloque);
                            lista.add(temp);
                        }
                    }
                        /*********/
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - PGMFInventarioExploraciónVolumenP ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<TablaAnexo3PGMF>   ObtenerTablaAnexo3PGMFVolumenPotencial(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<TablaAnexo3PGMF> lista = new ArrayList<TablaAnexo3PGMF>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PGMFEncabezadoAnexo3");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TablaAnexo3PGMF temp = new TablaAnexo3PGMF();
                    temp.setBloquequinquenal(""+ row[0]);
                    temp.setIdTipoBosque(""+ row[1]);
                    temp.setTipoBosque(""+ row[2]);
                    temp.setAreaBloqueQuinquenal(""+ row[3]);
                    temp.setAreaTipoBosque(""+ row[4]);
                   // Integer bloque=Integer.parseInt(temp.getBloquequinquenal());   // de Integer a String
                    String bloque=temp.getBloquequinquenal();
                    Integer idTipoBosque=Integer.parseInt(temp.getIdTipoBosque());
                    Double areaTipoBosque=Double.parseDouble(temp.getAreaTipoBosque());
                    List<ResultadosAnexo3PGMF>  resultadosAnexo3PGMFList = new ArrayList<ResultadosAnexo3PGMF>();
                    resultadosAnexo3PGMFList= ResultadosAnexo3PGMFVolumenPotencial(idPlanManejo,tipoPlan,bloque, idTipoBosque, areaTipoBosque);
                    if(resultadosAnexo3PGMFList==null){
                        continue;
                    }
                    else if(resultadosAnexo3PGMFList.size()>0){
                        temp.setResultadosAnexo3PGMFList(resultadosAnexo3PGMFList);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ObtenerTablaAnexo3PGMFVolumenPotencial", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<TablaAnexo3PGMF>   ObtenerTablaAnexo3PGMFFustales(Integer idPlanManejo, String tipoPlan) throws Exception {
        List<TablaAnexo3PGMF> lista = new ArrayList<TablaAnexo3PGMF>();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("dbo.pa_PGMFEncabezadoAnexo3");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    TablaAnexo3PGMF temp = new TablaAnexo3PGMF();
                    temp.setBloquequinquenal(""+ row[0]);
                    temp.setIdTipoBosque(""+ row[1]);
                    temp.setTipoBosque(""+ row[2]);
                    temp.setAreaBloqueQuinquenal(""+ row[3]);
                    temp.setAreaTipoBosque(""+ row[4]);
                    //Integer bloque=Integer.parseInt(temp.getBloquequinquenal());
                    String bloque=temp.getBloquequinquenal();
                    Integer idTipoBosque=Integer.parseInt(temp.getIdTipoBosque());
                    Double areaTipoBosque=Double.parseDouble(temp.getAreaTipoBosque());
                    List<ResultadosAnexo3PGMF>  resultadosAnexo3PGMFList = new ArrayList<ResultadosAnexo3PGMF>();
                    resultadosAnexo3PGMFList= ResultadosAnexo3PGMFFustales(idPlanManejo,tipoPlan,bloque, idTipoBosque, areaTipoBosque);
                    if(resultadosAnexo3PGMFList==null){
                        continue;
                    }
                    else if(resultadosAnexo3PGMFList.size()>0){
                        temp.setResultadosAnexo3PGMFList(resultadosAnexo3PGMFList);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ObtenerTablaAnexo3PGMFFustales", e.getMessage());
            throw e;
        }
    }

    @Override
    public  List<ResultadosAnexo3PGMF>  ResultadosAnexo3PGMFVolumenPotencial(Integer idPlanManejo, String tipoPlan,String  bloque,Integer idTipoBosque,Double areaTipoBosque) throws Exception {
        List<ResultadosAnexo3PGMF> lista = new ArrayList<ResultadosAnexo3PGMF>();
        try {
            DecimalFormat df = new DecimalFormat("0.000000");
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PGMFResultadosAnexo3VolumenPotencial");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN); // Integer a String
            processStored.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("bloque", bloque);// Integer a String
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ResultadosAnexo3PGMF temp = new ResultadosAnexo3PGMF();
                    temp.setEspecie(""+ row[0]);
                    temp.setnDap20a30(""+ row[1]);
                    temp.setnDap30a40(""+ row[2]);
                    temp.setnDap40a50(""+ row[3]);
                    temp.setnDap50a60(""+ row[4]);
                    temp.setnDap60a70(""+ row[5]);
                    temp.setnDap70aMas(""+ row[6]);
                    Integer  ndapTotalTipoBosque=Integer.parseInt(temp.getnDap20a30())
                            +Integer.parseInt(temp.getnDap30a40())
                            +Integer.parseInt(temp.getnDap40a50())
                            +Integer.parseInt(temp.getnDap50a60())
                            +Integer.parseInt(temp.getnDap60a70())
                            +Integer.parseInt(temp.getnDap70aMas());
                    if(ndapTotalTipoBosque==0){
                        continue;
                    }
                    temp.setnDapTotalTipoBosque(""+ndapTotalTipoBosque);
                    temp.setnDapTotalTipoBosquePorHa(""+df.format(ndapTotalTipoBosque/areaTipoBosque)  );
                    temp.setAbDap20a30(""+ row[7]);
                    temp.setAbDap30a40(""+ row[8]);
                    temp.setAbDap40a50(""+ row[9]);
                    temp.setAbDap50a60(""+ row[10]);
                    temp.setAbDap60a70(""+ row[11]);
                    temp.setAbDap70aMas(""+ row[12]);
                    Double  abdapTotalTipoBosque=Double.parseDouble(temp.getAbDap20a30())
                            +Double.parseDouble(temp.getAbDap30a40())
                            +Double.parseDouble(temp.getAbDap40a50())
                            +Double.parseDouble(temp.getAbDap50a60())
                            +Double.parseDouble(temp.getAbDap60a70())
                            +Double.parseDouble(temp.getAbDap70aMas());
                    temp.setAbDapTotalTipoBosque(""+abdapTotalTipoBosque);
                    temp.setAbDapTotalTipoBosquePorHa(""+df.format(abdapTotalTipoBosque/areaTipoBosque)  );
                    temp.setVolDap20a30(""+ row[7]);
                    temp.setVolDap30a40(""+ row[8]);
                    temp.setVolDap40a50(""+ row[9]);
                    temp.setVolDap50a60(""+ row[10]);
                    temp.setVolDap60a70(""+ row[11]);
                    temp.setVolDap70aMas(""+ row[12]);
                    Double  voldapTotalTipoBosque=Double.parseDouble(temp.getVolDap20a30())
                            +Double.parseDouble(temp.getVolDap30a40())
                            +Double.parseDouble(temp.getVolDap40a50())
                            +Double.parseDouble(temp.getVolDap50a60())
                            +Double.parseDouble(temp.getVolDap60a70())
                            +Double.parseDouble(temp.getVolDap70aMas());
                    temp.setVolDapTotalTipoBosque(""+voldapTotalTipoBosque);
                    temp.setVolDapTotalTipoBosquePorHa(""+df.format(voldapTotalTipoBosque/areaTipoBosque)  );
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl --- ResultadosAnexo3PGMFFustales", e.getMessage());
            throw e;
        }
    }

    @Override
    public  List<ResultadosAnexo3PGMF>  ResultadosAnexo3PGMFFustales(Integer idPlanManejo, String tipoPlan,String  bloque,Integer idTipoBosque,Double areaTipoBosque) throws Exception {
        List<ResultadosAnexo3PGMF> lista = new ArrayList<ResultadosAnexo3PGMF>();
        try {
            DecimalFormat df = new DecimalFormat("0.000000");
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_PGMFResultadosAnexo3Fustales");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("bloque", String.class, ParameterMode.IN);  //Integer a String
            processStored.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.setParameter("bloque", bloque); //Integer a String
            processStored.setParameter("idTipoBosque", idTipoBosque);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ResultadosAnexo3PGMF temp = new ResultadosAnexo3PGMF();
                    temp.setEspecie(""+ row[0]);
                    temp.setnDap10a15(""+ row[1]);
                    temp.setnDap15a20(""+ row[2]);
                    Integer  ndapTotalTipoBosque=Integer.parseInt(temp.getnDap10a15())+
                            Integer.parseInt(temp.getnDap15a20());
                    if(ndapTotalTipoBosque==0){
                        continue;
                    }
                    temp.setnDapTotalTipoBosque(""+ndapTotalTipoBosque);
                    temp.setnDapTotalTipoBosquePorHa(""+df.format(ndapTotalTipoBosque/areaTipoBosque)  );
                    temp.setAbDap10a15(""+ row[3]);
                    temp.setAbDap15a20(""+ row[4]);
                    Double  abdapTotalTipoBosque=Double.parseDouble(temp.getAbDap10a15())+
                            Double.parseDouble(temp.getAbDap15a20());
                    temp.setAbDapTotalTipoBosque(""+abdapTotalTipoBosque);
                    temp.setAbDapTotalTipoBosquePorHa(""+df.format(abdapTotalTipoBosque/areaTipoBosque)  );
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl --- ResultadosAnexo3PGMFFustales", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity ListarProyeccionCosecha(Integer idPlanManejo, String tipoPlan) {
        ResultClassEntity result = new ResultClassEntity();
        List<ProyeccionCosechaCabeceraDto> lista = new ArrayList<ProyeccionCosechaCabeceraDto>();
        try {
            StoredProcedureQuery proc1 = entityManager.createStoredProcedureQuery("dbo.pa_ManejoBosque_Listar");
            proc1.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            proc1.registerStoredProcedureParameter("codigoGeneral", String.class, ParameterMode.IN);
            proc1.registerStoredProcedureParameter("subCodigoGeneral", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(proc1);
            proc1.setParameter("idPlanManejo", idPlanManejo);
            proc1.setParameter("codigoGeneral", "PGMF");
            proc1.setParameter("subCodigoGeneral", "PGMFCFFMMFCC");
            proc1.execute();

            List<Object[]> spResultP1 = proc1.getResultList();

                if (spResultP1.size() >= 1){
                    for (Object[] row1 : spResultP1) {

                        ManejoBosqueEntity temp2 = new ManejoBosqueEntity();
                        temp2.setAnio(row1[9]==null? null: (Integer) row1[9]);

                        /*********/
                        List<ParametroValorEntity> listaParam = new ArrayList<ParametroValorEntity>();
                        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoBosque_ListarPorPlanManejo");
                        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                        processStored.setParameter("idPlanManejo", idPlanManejo);
                        processStored.setParameter("tipoPlan", tipoPlan);
                        processStored.execute();
                        List<Object[]> spResult = processStored.getResultList();
                        if (spResult.size() >= 1) {
                            for (Object[] rowTB : spResult) {
                                ParametroValorEntity param = new ParametroValorEntity();
                                param.setCodigo((String) rowTB[0]);
                                param.setValor1((String) rowTB[1]);
                                param.setPrefijo((String) rowTB[2]);
                                /*********/
                                StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_PGMFInventarioExploracionBloque");
                                proc.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                                proc.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                                proc.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
                                proc.setParameter("idPlanManejo", idPlanManejo);
                                proc.setParameter("tipoPlan", tipoPlan);
                                proc.setParameter("idTipoBosque", param.getCodigo());
                                proc.execute();
                                List<Object[]> spResult_ = proc.getResultList();

                                if (spResult.size() >= 1) {

                                    for (Object[] row : spResult) {

                                        Integer bloque =    Integer.parseInt((String) row[0]);
                                        /**********/
                                        processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProyeccionCosecha_Listar");
                                        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                                        processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                                        processStored.registerStoredProcedureParameter("idTipoBosque", String.class, ParameterMode.IN);
                                        processStored.registerStoredProcedureParameter("bloque", Integer.class, ParameterMode.IN);
                                        SpUtil.enableNullParams(processStored);
                                        processStored.setParameter("idPlanManejo", idPlanManejo);
                                        processStored.setParameter("tipoPlan", tipoPlan);
                                        processStored.setParameter("idTipoBosque", param.getCodigo());
                                        processStored.setParameter("bloque", bloque);
                                        processStored.execute();
                                        List<Object[]> spResult2 = processStored.getResultList();

                                        if (spResult2.size() >= 1) {
                                            for (Object[] row2 : spResult2) {
                                                ProyeccionCosechaDto temp = new ProyeccionCosechaDto();

                                                ProyeccionCosechaCabeceraDto cab = new ProyeccionCosechaCabeceraDto();
                                                if(lista.size()==8){


                                                    lista.stream().forEach((a) -> {

                                                        if (a.getClaseDiametrica().equals("10-15 cm")) {

                                                            double str1 = Double.parseDouble((String) row2[1]);
                                                            a.setValor(a.getValor() + str1);

                                                        }
                                                        if (a.getClaseDiametrica().equals("15-20 cm")) {

                                                            double str2 = Double.parseDouble((String) row2[2]);
                                                            a.setValor(a.getValor() + str2);
                                                        }
                                                        if (a.getClaseDiametrica().equals("20-30 cm")) {

                                                            double str3 = Double.parseDouble((String) row2[3]);
                                                            a.setValor(a.getValor() + str3);
                                                        }
                                                        if (a.getClaseDiametrica().equals("30-40 cm")) {

                                                            double str4 = Double.parseDouble((String) row2[4]);
                                                            a.setValor(a.getValor() + str4);
                                                        }
                                                        if (a.getClaseDiametrica().equals("40-50 cm")) {

                                                            double str5 = Double.parseDouble((String) row2[5]);
                                                            a.setValor(a.getValor() + str5);
                                                        }
                                                        if (a.getClaseDiametrica().equals("50-60 cm")) {

                                                            double str6 = Double.parseDouble((String) row2[6]);
                                                            a.setValor(a.getValor() + str6);
                                                        }
                                                        if (a.getClaseDiametrica().equals("60-70 cm")) {

                                                            double str7 = Double.parseDouble((String) row2[7]);
                                                            a.setValor(a.getValor() + str7);
                                                        }
                                                        if (a.getClaseDiametrica().equals("70 +")) {

                                                            double str8 = Double.parseDouble((String) row2[8]);
                                                            a.setValor(a.getValor() + str8);
                                                        }
                                                    });
                                                }
                                                else {
                                                    double str1 = Double.parseDouble((String) row2[1]);
                                                    cab.setClaseDiametrica("10-15 cm");
                                                    cab.setValor(str1);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(1,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);


                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str2 = Double.parseDouble((String) row2[2]);
                                                    cab.setClaseDiametrica("15-20 cm");
                                                    cab.setValor(str2);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(2,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);
                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str3 = Double.parseDouble((String) row2[3]);
                                                    cab.setClaseDiametrica("20-30 cm");
                                                    cab.setValor(str3);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(3,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);

                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str4 = Double.parseDouble((String) row2[4]);
                                                    cab.setClaseDiametrica("30-40 cm");
                                                    cab.setValor( str4);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(4,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);

                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str5 = Double.parseDouble((String) row2[5]);
                                                    cab.setClaseDiametrica("40-50 cm");
                                                    cab.setValor(str5);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(5,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);


                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str6 = Double.parseDouble((String) row2[6]);
                                                    cab.setClaseDiametrica("50-60 cm");
                                                    cab.setValor(str6);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(6,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);

                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str7 = Double.parseDouble((String) row2[7]);
                                                    cab.setClaseDiametrica("60-70 cm");
                                                    cab.setValor(str7);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(7,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);

                                                    cab = new ProyeccionCosechaCabeceraDto();
                                                    double str8 = Double.parseDouble((String) row2[8]);
                                                    cab.setClaseDiametrica("70 +");
                                                    cab.setValor(str8);
                                                    cab.setDetalle(ListarProyeccionCosechaDetalle(8,idPlanManejo, tipoPlan,param.getCodigo(),bloque, temp2.getAnio()));
                                                    lista.add(cab);
                                                }





                                            }
                                        }

                                        /*********/
                                    }
                                }
                                /*********/


                            }
                        }
                        /*********/

                    }
                }
                else{
                    result.setSuccess(true);
                    result.setMessage("No existen datos sincronizados");
                    return result;
                }






            result.setData(lista);
            result.setSuccess(true);
            if(lista.size()>0 )
                result.setMessage("lista Proyección de Cosecha.");
            else
                result.setMessage("No existen datos sincronizados");
            return result;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - PGMProyeccionCosecha ", e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setSuccess(false);
            return result;
        }
    }

    public List<ProyeccionCosechaDetalleDto> ListarProyeccionCosechaDetalle(Integer position,Integer idPlanManejo, String tipoPlan , String tipoBosque, Integer bloque,Integer anio) {

        List<ProyeccionCosechaDetalleDto> lista = new ArrayList<ProyeccionCosechaDetalleDto>();
   try{
        for(Integer x=1;anio>=x;x++){


            Double crecimiento = x*0.5;

            List<ParametroValorEntity> listaParam = new ArrayList<ParametroValorEntity>();
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_TipoBosque_ListarPorPlanManejo");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("tipoPlan", tipoPlan);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] rowTB : spResult) {
                    ParametroValorEntity param = new ParametroValorEntity();
                    param.setCodigo((String) rowTB[0]);
                    param.setValor1((String) rowTB[1]);
                    param.setPrefijo((String) rowTB[2]);

                    //**
                    Integer idTipoBosque=Integer.parseInt(param.getCodigo());
                    StoredProcedureQuery proSt = entityManager.createStoredProcedureQuery("dbo.pa_ProyeccionCosecha_Listar");
                    proSt.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    proSt.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                    proSt.registerStoredProcedureParameter("idTipoBosque", Integer.class, ParameterMode.IN);
                    proSt.registerStoredProcedureParameter("bloque", Integer.class, ParameterMode.IN);
                    proSt.registerStoredProcedureParameter("crecimiento", Double.class, ParameterMode.IN);
                    SpUtil.enableNullParams(proSt);
                    proSt.setParameter("idPlanManejo", idPlanManejo);
                    proSt.setParameter("tipoPlan", tipoPlan);
                    proSt.setParameter("idTipoBosque", idTipoBosque);
                    proSt.setParameter("bloque", bloque);
                    proSt.setParameter("crecimiento", crecimiento);
                    proSt.execute();
                    List<Object[]> spResult2 = proSt.getResultList();

                     Integer indice =x;
                    if (spResult2.size() >= 1) {
                        for (Object[] row2 : spResult2) {

                            ProyeccionCosechaDetalleDto cab = new ProyeccionCosechaDetalleDto();
                            Integer i=1;
                            List<ProyeccionCosechaDetalleDto> list = new ArrayList<ProyeccionCosechaDetalleDto>();
                            if(lista.size() >= 1 ){
                                list= lista.stream()
                                        .filter(p ->p.getAnio().equals(indice))
                                        .collect(Collectors.toList());
                            }

                          if(lista.size() >= 1 && list.size()>0){

                              lista.stream().forEach((a) -> {

                                  if (a.getAnio()==indice && position==1) {

                                      double str1 = Double.parseDouble((String) row2[1]);

                                      a.setValor(a.getValor()+ str1);

                                  }
                                  if (a.getAnio()==indice && position==2) {

                                      double str2 = Double.parseDouble((String) row2[2]);
                                      a.setValor(a.getValor()+ str2);
                                  }
                                  if (a.getAnio()==indice && position==3) {

                                      double str3 = Double.parseDouble((String) row2[3]);
                                      a.setValor(a.getValor() + str3);
                                  }
                                  if (a.getAnio()==indice && position==4) {

                                      double str4 = Double.parseDouble((String) row2[4]);
                                      a.setValor(a.getValor() + str4);
                                  }
                                  if (a.getAnio()==indice && position==5) {

                                      double str5 = Double.parseDouble((String) row2[5]);
                                      a.setValor(a.getValor() + str5);
                                  }
                                  if (a.getAnio()==indice && position==6) {

                                      double str6 = Double.parseDouble((String) row2[6]);
                                      a.setValor(a.getValor() + str6);
                                  }
                                  if (a.getAnio()==indice && position==7) {

                                      double str7 = Double.parseDouble((String) row2[7]);
                                      a.setValor(a.getValor() + str7);
                                  }
                                  if (a.getAnio()==indice && position==8) {

                                      double str8 = Double.parseDouble((String) row2[8]);
                                      a.setValor(a.getValor() + str8);
                                  }

                              });
                              i++;
                          }
                          else{
                              if(position==1){
                                  double str1 = Double.parseDouble((String) row2[1]);
                                  cab.setAnio(x);
                                  cab.setValor(str1);
                                  lista.add(cab);
                              }
                              if(position==2){
                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str2 = Double.parseDouble((String) row2[2]);
                                  cab.setAnio(x);
                                  cab.setValor(str2);
                                  lista.add(cab);
                              }


                              if(position==3){
                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str3 = Double.parseDouble((String) row2[3]);
                                  cab.setAnio(x);
                                  cab.setValor(str3);
                                  lista.add(cab);
                              }


                              if(position==4){
                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str4 = Double.parseDouble((String) row2[4]);
                                  cab.setAnio(x);
                                  cab.setValor( str4);
                                  lista.add(cab);
                              }


                              if(position==5){
                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str5 = Double.parseDouble((String) row2[5]);
                                  cab.setAnio(x);
                                  cab.setValor(str5);
                                  lista.add(cab);
                              }


                              if(position==6){
                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str6 = Double.parseDouble((String) row2[6]);
                                  cab.setAnio(x);
                                  cab.setValor(str6);
                                  lista.add(cab);
                              }


                              if(position==7){

                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str7 = Double.parseDouble((String) row2[7]);
                                  cab.setAnio(x);
                                  cab.setValor(str7);
                                  lista.add(cab);
                              }

                              if(position==8){

                                  cab = new ProyeccionCosechaDetalleDto();
                                  double str8 = Double.parseDouble((String) row2[8]);
                                  cab.setAnio(x);
                                  cab.setValor(str8);
                                  lista.add(cab);
                              }
                          }


                        }
                    }
                    //**
                }
            }
            //**

        }


        return lista;
    } catch (Exception e) {
        log.error("CensoForestalRepositoryImpl - PGMProyeccionCosecha ", e.getMessage());
        return lista;
    }
 }

    public ResultClassEntity listarVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan) {
        ResultClassEntity result = new ResultClassEntity();
        List<VolumenComercialPromedioDetalleDto> lista = new ArrayList<VolumenComercialPromedioDetalleDto>();
        try{
        StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_VolumenComercialPromedio_Listar");
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
        processStored.setParameter("idPlanManejo", idPlanManejo);
        processStored.setParameter("tipoPlan", tipoPlan);
        processStored.execute();
        List<Object[]> spResult = processStored.getResultList();
        if (spResult.size() >= 1) {
            for (Object[] row : spResult) {
                VolumenComercialPromedioDetalleDto temp = new VolumenComercialPromedioDetalleDto();
                temp.setBloquequinquenal((String) row[0]);
                temp.setTipoBosque((String) row[1]);
                BigDecimal dec=(BigDecimal) row[2];
                temp.setAreaHa(dec.doubleValue());

                String txt=(String) row[0];
                String str1 = txt.replaceFirst("BLOQUE ", "");
                temp.setBloque(FechaUtil.RomanToNumber(str1));

                lista.add(temp);
            }
        }

            List<VolumenComercialPromedioCabeceraDto> listCab = new ArrayList<VolumenComercialPromedioCabeceraDto>();


            lista.stream().forEach((a) -> {
                VolumenComercialPromedioCabeceraDto cab = new VolumenComercialPromedioCabeceraDto();
                if(listCab.size() >= 1 ){

                    List<VolumenComercialPromedioCabeceraDto> temp= new ArrayList<VolumenComercialPromedioCabeceraDto>();
                    temp= listCab.stream()
                            .filter(employee ->employee.getBloquequinquenal().equals(a.getBloquequinquenal()))
                            .collect(Collectors.toList());


                    if(temp.size()==0){
                         cab.setToTalAreaHa(a.getAreaHa());
                         cab.setBloquequinquenal(a.getBloquequinquenal());

                        String str1 = cab.getBloquequinquenal().replaceFirst("BLOQUE ", "");
                        cab.setBloque(FechaUtil.RomanToNumber(str1));

                         listCab.add(cab);
                     }
                    else{
                        listCab.stream().forEach((x) -> {
                            if(x.getBloquequinquenal().equals(a.getBloquequinquenal())){
                                x.setToTalAreaHa(x.getToTalAreaHa()+a.getAreaHa());
                            }
                        });
                    }
                }
                else{
                    cab.setToTalAreaHa(a.getAreaHa());
                    cab.setBloquequinquenal(a.getBloquequinquenal());
                    String str1 = cab.getBloquequinquenal().replaceFirst("BLOQUE ", "");
                    cab.setBloque(FechaUtil.RomanToNumber(str1));
                    listCab.add(cab);
                }
            });
            listCab.stream().forEach((a) -> {
                List<VolumenComercialPromedioDetalleDto> listCabDet=  new ArrayList<VolumenComercialPromedioDetalleDto>();
                List<VolumenComercialPromedioEspecieDto> listEspecie=  new ArrayList<VolumenComercialPromedioEspecieDto>();

                StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_EspecieVolumenComercialPromedio_Listar");
                proc.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                proc.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                proc.registerStoredProcedureParameter("bloque", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(proc);
                proc.setParameter("idPlanManejo", idPlanManejo);
                proc.setParameter("tipoPlan", tipoPlan);
                proc.setParameter("bloque", a.getBloque());
                proc.execute();
                List<Object[]> spResult_ = proc.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row : spResult_) {
                        VolumenComercialPromedioEspecieDto temp = new VolumenComercialPromedioEspecieDto();
                        temp.setIdTipoBosque((Integer) row[0]);
                        temp.setNombreComun((String) row[1]);
                        temp.setNombreCientifico((String) row[2]);
                        temp.setDMC((Integer) row[3]);

                        temp.setCantidadArb((Integer) row[4]);
                        temp.setTotalArb((Integer) row[5]);
                        temp.setVolumenComercialPromedio((BigDecimal) row[6]);
                        temp.setVolumenComercialPromedioTotal((BigDecimal) row[7]);
                        listEspecie.add(temp);
                    }
                }
                if(listEspecie.size() >= 1)
                a.setListEspecie(listEspecie);

                listCabDet= lista.stream()
                        .filter(employee ->employee.getBloquequinquenal().equals(a.getBloquequinquenal()))
                        .collect(Collectors.toList());

                //--
                if(listCabDet.size()>0)
                {
                    List<VolumenComercialPromedioDetalleDto> listCabDetF=  new ArrayList<VolumenComercialPromedioDetalleDto>();

                    listCabDet.stream().forEach((z) -> {

                        if(listCabDetF.size()==0){
                            listCabDetF.add(z);
                        }
                        else{
                            List<VolumenComercialPromedioDetalleDto> listCabDetFil=  new ArrayList<VolumenComercialPromedioDetalleDto>();
                            listCabDetFil=  listCabDetF.stream()
                                    .filter(y ->y.getTipoBosque().equals(z.getTipoBosque()))
                                    .collect(Collectors.toList());

                            if(listCabDetFil.size()>0){
                                listCabDetF.stream().forEach((b) -> {
                                    if(b.getTipoBosque().equals(b.getTipoBosque())){
                                        b.setAreaHa(b.getAreaHa()+z.getAreaHa());
                                    }
                                });
                            }
                            else{
                                listCabDetF.add(z);
                            }

                        }

                    });

                    a.setDetalle(listCabDetF);
                }
                //--
            });

            result.setSuccess(true);
            result.setData(listCab);
        return result;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - PGMProyeccionCosecha ", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return result;
        }
    }


    public ResultClassEntity listarEspecieVolumenComercialPromedio(Integer idPlanManejo, String tipoPlan) {
        ResultClassEntity result = new ResultClassEntity();
        List<VolumenComercialPromedioEspecieDto> listEspecie=  new ArrayList<VolumenComercialPromedioEspecieDto>();

        try{
                StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_EspecieVolumenComercialPromedio_Listar");
                proc.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                proc.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
                SpUtil.enableNullParams(proc);
                proc.setParameter("idPlanManejo", idPlanManejo);
                proc.setParameter("tipoPlan", tipoPlan);
                proc.execute();
                List<Object[]> spResult_ = proc.getResultList();
                if (spResult_.size() >= 1) {
                    for (Object[] row : spResult_) {
                        VolumenComercialPromedioEspecieDto temp = new VolumenComercialPromedioEspecieDto();
                        temp.setIdTipoBosque((Integer) row[0]);
                        temp.setNombreComun((String) row[1]);
                        temp.setNombreCientifico((String) row[2]);
                        temp.setDMC((Integer) row[3]);

                        temp.setCantidadArb((Integer) row[4]);
                        temp.setTotalArb((Integer) row[5]);
                        temp.setVolumenComercialPromedio((BigDecimal) row[6]);
                        temp.setVolumenComercialPromedioTotal((BigDecimal) row[7]);
                        listEspecie.add(temp);
                    }
                }




            result.setSuccess(true);
            result.setData(listEspecie);
            return result;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - listarEspecieVolumenComercialPromedio ", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return result;
        }
    }
    public ResultClassEntity listarEspecieVolumenCortaAnualPermisible(Integer idPlanManejo, String tipoPlan) {
        ResultClassEntity result = new ResultClassEntity();
        List<VolumenComercialPromedioEspecieDto> listEspecie=  new ArrayList<VolumenComercialPromedioEspecieDto>();

        try{
            StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_EspecieVolumenCortaAnualPermisible_Listar");
            proc.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("tipoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(proc);
            proc.setParameter("idPlanManejo", idPlanManejo);
            proc.setParameter("tipoPlan", tipoPlan);
            proc.execute();
            List<Object[]> spResult_ = proc.getResultList();
            if (spResult_.size() >= 1) {
                for (Object[] row : spResult_) {
                    VolumenComercialPromedioEspecieDto temp = new VolumenComercialPromedioEspecieDto();
                    temp.setIdTipoBosque((Integer) row[0]);
                    temp.setNombreComun((String) row[1]);
                    temp.setNombreCientifico((String) row[2]);
                    temp.setDMC((Integer) row[3]);

                    temp.setCantidadArb((Integer) row[4]);
                    temp.setTotalArb((Integer) row[5]);

                    if(temp.getCantidadArb()>0)
                    {
                        Double  a = (double)temp.getCantidadArb();
                        Double  b = (double)temp.getTotalArb();


                        Double porcentaje =(a/ b)*100;

                        BigDecimal bd = new BigDecimal(porcentaje).setScale(2, RoundingMode.HALF_UP);
                        double newInput = bd.doubleValue();

                        temp.setPorcentajeArb(newInput);
                    }



                    temp.setVolumenComercialPromedio((BigDecimal) row[6]);
                    temp.setVolumenComercialPromedioTotal((BigDecimal) row[7]);

                    temp.setVolumenComercialPromedioPonderado((BigDecimal) row[8]);
                    temp.setVolumenCortaAnualPermisible((BigDecimal) row[9]);

                    listEspecie.add(temp);
                }
            }




            result.setSuccess(true);
            result.setData(listEspecie);
            return result;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - listarEspecieVolumenCortaAnualPermisible ", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return result;
        }
    }

    @Override
    public List<ContratoTHDto> ListarContratoTHMovilObtener(ContratoTHDto param) throws Exception {
        List<ContratoTHDto> lista = new ArrayList<ContratoTHDto>();
        try {
            StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("dbo.pa_ContratoTHMovil_Listar");
            SpUtil.enableNullParams(proc);
            proc.execute();
            List<Object[]> spResult = proc.getResultList();
            if (spResult.size() >= 1) {
                for (Object[] row : spResult) {
                    ContratoTHDto temp = new ContratoTHDto();
                    temp.setIdContrato((Integer) row[0]);
                    temp.setCodigoContrato((String) row[1]);
                    temp.setUbigeo((String) row[2]);
                    temp.setTipoPlanManejo((String) row[3]);
                    temp.setTipoProceso((Integer) row[4]);
                    lista.add(temp);
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - ListarContratoTHMovilObtener", e.getMessage());
            throw e;
        }
    }



    @Override
    public ResultClassEntity actualizarCensoForestal(CensoForestalEntity request) throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        try{
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("[dbo].[pa_CensoForestal_Actualizar]");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModifica", Integer.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejo", request.getIdPlanManejo());
            processStored.setParameter("fechaInicio", request.getFechaInicio());
            processStored.setParameter("fechaFin", request.getFechaFin());
            processStored.setParameter("idUsuarioModifica", request.getIdUsuarioModificacion());
            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó Censo Forestal Corectamente ");
            return result;
        } catch (Exception e) {
            log.error("CensoForestalRepositoryImpl - actualizarCensoForestal ", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrio un error.");
            return result;
        }
    }


}
