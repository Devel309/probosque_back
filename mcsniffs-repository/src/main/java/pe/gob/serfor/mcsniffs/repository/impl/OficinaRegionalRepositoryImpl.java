package pe.gob.serfor.mcsniffs.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.OficinaRegionalEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;
import pe.gob.serfor.mcsniffs.repository.OficinaRegionalRepository;
import pe.gob.serfor.mcsniffs.repository.SeguridadRepository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Repository
public class OficinaRegionalRepositoryImpl  extends JdbcDaoSupport implements OficinaRegionalRepository{
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public List<OficinaRegionalEntity> listarOficinaRegional() {
        List<OficinaRegionalEntity> lst_result = new ArrayList<>();
        try {
            String sql = "SELECT * from view_listaroficinaregional";
            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
            if (rows.size()>=1) {
                for(Map<String, Object> row:rows){
                    OficinaRegionalEntity obj = new OficinaRegionalEntity();
                    obj.setIdOficinaReginal((Integer) row.get("NU_ID_OFICINA_REGIONAL"));
                    obj.setIdTipoOficina((Integer)row.get("NU_ID_TIPO_OFICINA"));
                    obj.setNombreOficina((String)row.get("TX_NOMBRE_OFICINA"));
                    obj.setDireccion((String)row.get("TX_DIRECCION"));
                    obj.setNombrePoblado((String)row.get("TX_NOMBRE_POBLADO"));
                    obj.setCodUbigeo((String)row.get("TX_COD_UBIGEO"));
                    obj.setZonaUtm((Short) row.get("NU_ZONA_UTM"));
                    obj.setIdOrigen((Short) row.get("NU_ID_ORIGEN"));
                    obj.setIdAutoridadForestal((Short)row.get("NU_ID_AUTORIDAD_FORESTAL"));
                    obj.setCoordenadaNo((BigDecimal) row.get("NU_COORDENADA_NO"));
                    obj.setCoordenadaEs((BigDecimal)row.get("NU_COORDENADA_ES"));
                    obj.setTipoUbicacion((Integer) row.get("NU_TIPOUBICACION"));
                    obj.setFuente((String)row.get("TX_FUENTE"));
                    obj.setDocumentoRegistro((String)row.get("TX_DOCUMENTO_REGISTRO"));
                    obj.setObservacion((String)row.get("TX_OBSERVACION"));
                    obj.setEstado((String)row.get("TX_ESTADO"));
                    obj.setIdUsuarioRegistro((Integer) row.get("NU_ID_USUARIO_REGISTRO"));
                    obj.setFechaRegistro((Date) row.get("FE_FECHA_REGISTRO"));
                    obj.setIdUsuarioModificacion((Integer)row.get("NU_ID_USUARIO_MODIFICACION"));
                    obj.setFechaModificacion((Date) row.get("FE_FECHA_MODIFICACION"));
                    lst_result.add(obj);
                }
            }
            return lst_result;
        }catch (Exception ex){
            logger.error("OficinaRegionalRepository - listarOficinaRegional - Ocurrió un error", ex);
        }
        return null;
    }
}
