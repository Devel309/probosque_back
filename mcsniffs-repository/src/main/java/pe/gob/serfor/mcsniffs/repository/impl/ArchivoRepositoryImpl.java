package pe.gob.serfor.mcsniffs.repository.impl;

import java.io.File;
import java.nio.file.Files;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.OrdenamientoAreaManejoDto;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
// 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.*;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ArchivoRepositoryImpl extends JdbcDaoSupport implements ArchivoRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager em;
    @Value("${smb.file.server.path}")
    private String fileServerPath;

    private static final String SEPARADOR_ARCHIVO = ".";
    @Autowired
    private FileServerConexion fileServerConexion;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ArchivoRepositoryImpl.class);

    @Autowired
    private FileServerConexion fileCn;

    @Override
    public ResultEntity<ArchivoEntity> registroArchivo(ArchivoEntity request) {
        return null;
        /*
        ResultEntity resul = new ResultEntity();
        CallableStatement pstm = null;
        Connection con = null;
        try {
            String res = "";
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_RegistrarArchivo (?,?,?,?,?,?,?,?,?,?)}");
            pstm.setObject(1, request.getIdArchivo(), Types.INTEGER);
            pstm.setObject(2, request.getEstado(), Types.VARCHAR);
            pstm.setObject(3, request.getIdUsuarioModificacion(), Types.INTEGER);
            pstm.setObject(4, request.getIdUsuarioRegistro(), Types.INTEGER);
            pstm.setObject(5, request.getRuta(), Types.VARCHAR);
            pstm.setObject(6, request.getNombre(), Types.VARCHAR);
            pstm.setObject(7, request.getExtension(), Types.VARCHAR);
            pstm.setObject(8, request.getNombreGenerado(), Types.VARCHAR);
            pstm.setObject(9, request.getTipoDocumento(), Types.VARCHAR);
            pstm.registerOutParameter(10, Types.INTEGER);
            pstm.execute();
            resul.setCodigo(pstm.getInt(10));

            resul.setMessage((resul.getCodigo() > 0 ? "Información Actualizada" : "Informacion no Actualizada"));
            resul.setIsSuccess((resul.getCodigo() > 0 ? true : false));

        } catch (Exception e) {
            resul.setMessage(e.getMessage());
            resul.setCodigo(0);
            resul.setIsSuccess(false);
        } finally {
            try {
                pstm.close();
                con.close();
            } catch (Exception e) {
                resul.setMessage(e.getMessage());
                resul.setCodigo(0);
                resul.setIsSuccess(false);
            }
        }
        return resul;
        */
    }

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {eliminar un archivo}
     * @param:ArchivoEntity
     */

    public ResultClassEntity eliminarArchivo(ArchivoEntity request) {
        return null;
        /*
        ResultClassEntity result = new ResultClassEntity();
        Connection con = null;
        PreparedStatement pstm = null;
        try {
            con = dataSource.getConnection();
            pstm = con.prepareCall("{call pa_Archivo_eliminar (?,?,?)}");
            Integer salida = 0;
            Date date = new Date();
            pstm.setObject(1, request.getIdArchivo(), Types.INTEGER);
            pstm.setObject(2, request.getIdUsuarioModificacion(), Types.INTEGER);
            pstm.setObject(3, new java.sql.Timestamp(date.getTime()), Types.TIMESTAMP);
            salida = pstm.executeUpdate();
            result.setCodigo(salida);
            result.setSuccess(true);
            result.setMessage("Se eliminó el archivo.");
            pstm.close();
            con.close();
            return result;
        } catch (Exception e) {
            log.error("ArchivoRepositoryImpl - eliminarEliminar", e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        } finally {
            try {
                if (pstm != null)
                    pstm.close();
                if (con != null)
                    con.close();
            } catch (Exception e2) {
                log.error("ArchivoRepositoryImpl - eliminarEliminar", e2.getMessage());
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                result.setMessageExeption(e2.getMessage());
                return result;
            }
        }
        */
    }

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {listar los archivos}
     * @param:ArchivoEntity
     */
    @Override
    public ResultEntity<ArchivoEntity> listarArchivo(ArchivoEntity request) {
        ResultEntity<ArchivoEntity> result = new ResultEntity<ArchivoEntity>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_Archivo_listar");
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nombreGenerado", request.getNombreGenerado());
            processStored.setParameter("idArchivo", request.getIdArchivo());
            processStored.setParameter("tipoDocumento", request.getTipoDocumento());
            processStored.execute();
            List<Object[]> spResult_ = processStored.getResultList();
            List<ArchivoEntity>lstresult = new ArrayList<>();
            for (Object[] row_ : spResult_) {
                ArchivoEntity c = new ArchivoEntity();
                c.setIdArchivo((Integer) row_[0]);
                c.setRuta((String) row_[1]);
                c.setNombre((String) row_[2]);
                c.setExtension((String) row_[3]);
                c.setNombreGenerado((String) row_[4]);
                c.setTipoDocumento((String) row_[5]);
                if (c.getNombreGenerado() != null) {
                    byte[] fileContent = fileCn.loadFileAsResource(c.getNombreGenerado());
                    c.setFile(fileContent);
                }
                lstresult.add(c);
            }
            result.setData(lstresult);
            result.setIsSuccess(true);
            result.setMessage("Se listaron autorizaciones enviadas.");
            return result;
        } catch (Exception e) {
            log.error("ArchivoRepositoryImpl -listarArchivo ocurrio un error: \n"+e.getMessage());
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setMessageExeption(e.getMessage());
            return result;
        }
        
    }

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {obtener un archivo}
     * @param:IdArchivo
     */
    @Override
    public ResultClassEntity<ArchivoEntity> obtenerArchivo(Integer IdArchivo) {
        ResultClassEntity<ArchivoEntity> archivo = new ResultClassEntity<>();
        ArchivoEntity filtro = new ArchivoEntity();
        filtro.setNombreGenerado(null);
        filtro.setIdArchivo(IdArchivo);
        filtro.setTipoDocumento(null);
        ResultEntity<ArchivoEntity> lstarchivo = listarArchivo(filtro);
        archivo.setData(lstarchivo.getData().size() > 0 ? lstarchivo.getData().get(0) : null);
        archivo.setSuccess(true);
        archivo.setMessage(lstarchivo.getData().size() > 0 ? "Se encontro el archivo" : "No se encontraron resultados");
        return archivo;
    }

    private ArchivoEntity setResultDataObtener(List<Object[]> dataDb) throws Exception {
        ArchivoEntity data = new ArchivoEntity();
        if (!dataDb.isEmpty()) {
            Object[] item = dataDb.get(0);
            data.setIdArchivo((Integer) item[0]);
            data.setRuta((String) item[1]);
            data.setNombre((String) item[2]);
            data.setExtension((String) item[3]);
            data.setNombreGenerado((String) item[4]);
            data.setTipoDocumento((String) item[5]);
        }
        return data;
    }

    @Override
    public ResultClassEntity<ArchivoEntity> ObtenerArchivoGeneral(Integer idArchivo) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_ArchivoGeneral_Listar");
        sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
        sp.setParameter("idArchivo", idArchivo);
        sp.execute();
        ArchivoEntity data = setResultDataObtener(sp.getResultList());
        ResultClassEntity<ArchivoEntity> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    private void execEliminar(Integer idArchivo, Integer idUsuario) throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_Archivo_Eliminar");
        sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
        sp.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        sp.setParameter("idArchivo", idArchivo);
        sp.setParameter("idUsuarioElimina", idUsuario);
        sp.execute();
    }

    @Override
    public ResultClassEntity<Integer> EliminarArchivoGeneral(Integer idArchivo, Integer idUsuario) throws Exception {
        execEliminar(idArchivo, idUsuario);
        ResultClassEntity<Integer> result = new ResultClassEntity<>();
        result.setData(idArchivo);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity<Integer> RegistrarArchivoGeneral(ArchivoEntity request) throws Exception {

        ResultClassEntity<Integer> result = new ResultClassEntity<>();

        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_ArchivoGeneral_Registrar");
            sp.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.OUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("ruta", request.getRuta());
            sp.setParameter("nombre", request.getNombre());
            sp.setParameter("nombreGenerado", request.getNombreGenerado());
            sp.setParameter("extension", request.getExtension());
            sp.setParameter("tipoDocumento", request.getTipoDocumento());
            sp.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());
            sp.execute();

            Integer idArchivo = (Integer) sp.getOutputParameterValue("idArchivo");

            result.setCodigo(idArchivo);
            result.setData(idArchivo);
            result.setMessage((idArchivo > 0 ? "Información Registrada" : "Informacion No Registrada"));
            result.setSuccess((idArchivo > 0 ? true : false));
            return result;
        } catch (Exception e) {
            log.error("ArchivoRepositoryImpl -registrarArchivo ocurrio un error: \n"+e.getMessage());
            result.setCodigo(0);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity DescargarArchivoGeneral(ArchivoEntity param) {
        ResultClassEntity result = new ResultClassEntity();
        List<OrdenamientoAreaManejoDto> list = new ArrayList<OrdenamientoAreaManejoDto>();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_Archivo_Listar");
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nombreGenerado", param.getNombreGenerado());
            processStored.setParameter("idArchivo", param.getIdArchivo());
            processStored.setParameter("tipoDocumento", param.getTipoDocumento());
            processStored.execute();

            List<Object[]> spResult_ = processStored.getResultList();
            ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
            for (Object[] row_ : spResult_) {

                String nombreArchivoGenerado = ((String) row_[4]);
                String nombreArchivo = ((String) row_[2]);
                byte[] byteFile = fileServerConexion.loadFileAsResource(nombreArchivoGenerado);
                resultArchivo.setArchivo(byteFile);
                resultArchivo.setNombeArchivo(nombreArchivo);
                resultArchivo.setContenTypeArchivo("application/octet-stream");
                resultArchivo.setSuccess(true);
                resultArchivo.setMessage("Se descargo el archivo del file server  con éxito.");
            }

            result.setSuccess(true);
            result.setMessage("Se obtuvo ordenamiento de área de manejo.");
            result.setData(resultArchivo);
            return result;
        } catch (Exception e) {
            log.error("ArchivoRepositoryImpl -DescargarArchivoGeneral ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarArchivo(ArchivoEntity request) {

        ResultClassEntity result = new ResultClassEntity();
        try {

            StoredProcedureQuery processStored = em.createStoredProcedureQuery("pa_Archivo_actualizar");
            processStored.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ruta", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombre", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("extension", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreGenerado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);

            processStored.setParameter("idArchivo", request.getIdArchivo());
            processStored.setParameter("ruta", request.getRuta());
            processStored.setParameter("nombre", request.getNombre());
            processStored.setParameter("extension", request.getExtension());
            processStored.setParameter("nombreGenerado", request.getNombreGenerado());
            processStored.setParameter("tipoDocumento", request.getTipoDocumento());
            processStored.setParameter("idUsuarioModificacion", request.getIdUsuarioModificacion());

            processStored.execute();

            result.setSuccess(true);
            result.setMessage("Se actualizó el Archivo");
            return result;
        } catch (Exception e) {
            log.error("ArchivoRepositoryImpl -actualizarArchivo ocurrio un error: \n"+e.getMessage());
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }

    }



    @Override
    public ResultClassEntity eliminarArchivoGeometria(ArchivoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = em
                .createStoredProcedureQuery("dbo.pa_ArchivoGeometria_Eliminar");
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    /**
     * @autor: Abner [16-11-2021]
     * @modificado:
     * @descripción: {obtener un archivo}
     * @param:nombreArchivo
     */
    @Override
    public ResultClassEntity downloadFile(String name) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            ResultArchivoEntity resultArchivo = new ResultArchivoEntity();
            byte[] byteFile = fileServerConexion.loadFileAsResource(name);
            resultArchivo.setArchivo(byteFile);
            resultArchivo.setNombeArchivo(name);
            resultArchivo.setContenTypeArchivo("application/octet-stream");
            resultArchivo.setSuccess(true);
            resultArchivo.setMessage("Se descargo el archivo del file server  con éxito.");

            result.setSuccess(true);
            result.setMessage("Se obtuvo ordenamiento de área de manejo.");
            result.setData(resultArchivo);
            return result;
        } catch (Exception e) {
            result.setSuccess(false);
            log.error("ArchivoRepositoryImpl - downloadFile  \n Ocurrio un error:" +e.getMessage());
            result.setMessage("Ocurrió un error.");
            result.setData(null);
            return result;
        }
    }
}
