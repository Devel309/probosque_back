package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ProcedimientoAdministrativo.ProcedimientoAdministrativoDto;
import pe.gob.serfor.mcsniffs.repository.ProcedimientoAdministrativoRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ProcedimientoAdministrativoRepositoryImpl extends JdbcDaoSupport implements ProcedimientoAdministrativoRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(PlanManejoEvaluacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public ResultClassEntity listarProcedimientoAdministrativo(ProcedimientoAdministrativoDto dto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ProcedimientoAdministrativoDto> lista = new ArrayList<ProcedimientoAdministrativoDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ProcedimientoAdministrativo_Listar");
            processStored.registerStoredProcedureParameter("idProcedimiento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoProcedimiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ubigeo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idProcedimiento",dto.getIdProcedimiento());
            processStored.setParameter("codigoProcedimiento", dto.getCodigoProcedimiento());
            processStored.setParameter("ubigeo", dto.getUbigeo());
            processStored.setParameter("P_PAGENUM", dto.getPageNum());
            processStored.setParameter("P_PAGESIZE", dto.getPageSize());

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        ProcedimientoAdministrativoDto temp = new ProcedimientoAdministrativoDto();
                        temp.setIdProcedimiento(row[0]==null?null:(Integer) row[0]);
                        temp.setCodigoProcedimiento(row[1]==null?null:(String) row[1]);
                        temp.setProcedimientoAdministrativo(row[2]==null?null:(String) row[2]);
                        temp.setDescripcion(row[3]==null?null:(String) row[3]);
                        temp.setObservacion(row[4]==null?null:(String) row[4]);
                        temp.setUbigeo(row[5]==null?null:(String) row[5]);
                        temp.setEstado(row[6]==null?null:(String) row[6]);
          
                        lista.add(temp);
                    }
                }
            }

            result.setData(lista);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return result;
        } catch (Exception e) {
            log.error("listarProcedimientoAdministrativo", e.getMessage());
            throw e;
        }
    }
 
}
