package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Parametro.ResumenActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;

import java.util.List;

public interface ResumenActividadPoRepository {

    ResultClassEntity RegistrarResumenActividad(List<ResumenActividadPoEntity> list)throws Exception;
    List<ResumenActividadDto> ListarResumenActividad(ResumenActividadDto param) throws Exception;
    List<ResumenActividadPoEntity>ListarResumenActividad_Detalle(ResumenActividadPoEntity param) throws Exception;
    ResultClassEntity EliminarResumenActividad(ResumenActividadPoDetalleEntity param) throws Exception;
    List<ResumenActividadDto> ListarResumenActividadPO(ResumenActividadDto param) throws Exception;
}
