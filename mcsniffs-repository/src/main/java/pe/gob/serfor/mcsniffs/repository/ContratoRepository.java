package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ContratoAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoAdjuntosRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoResponseRequest;
import pe.gob.serfor.mcsniffs.entity.ContratoValidacionesEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.GenerarCodContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ModeloContratoDto;

public interface ContratoRepository {
    ResultClassEntity registrarContrato(ContratoEntity obj) throws Exception;
    ResultClassEntity AdjuntarArchivosContrato(ContratoAdjuntosRequestEntity obj);
    ResultClassEntity listarContrato(ContratoEntity obj);
    ResultEntity<ContratoAdjuntosEntity> ObtenerArchivosContrato(Integer IdContrato);
    ResultClassEntity ObtenerContrato(ContratoEntity obj);
   // ResultArchivoEntity DescargarContrato(Integer IdContrato);
    ResultClassEntity RegistrarValidaciones(ContratoValidacionesEntity obj);
    ResultEntity<ContratoValidacionesEntity> ListarValidaciones(Integer IdContrato);
    ResultClassEntity ActualizarContrato(ContratoEntity obj);
    ResultClassEntity ActualizarStatusContrato(ContratoEntity obj);
    List<ContratoEntity> listarContratoGeneral(ContratoEntity obj);
    ResultClassEntity generarCodigoContrato(GenerarCodContratoDto obj);
    ResultClassEntity actualizarEstadoContrato(ContratoEntity obj);
    ResultClassEntity listarComboContrato(ContratoEntity obj);
    ModeloContratoDto obtenerModeloContrato(Integer idContrato) throws Exception;
    ResultClassEntity listarContratoGeometria (String idsContrato);
    ResultClassEntity actualizarGeometria(ContratoEntity obj);
    ResultClassEntity actualizarEstadoRemitido(ContratoEntity obj);
    ResultClassEntity listarGeneracionContrato(ContratoDto obj);
    ResultClassEntity obtenerPostulacion(ContratoDto obj);
    
}
