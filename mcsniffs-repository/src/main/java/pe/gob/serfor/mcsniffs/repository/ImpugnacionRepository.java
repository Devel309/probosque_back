package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.Dto.Impugnacion.ImpugnacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public interface ImpugnacionRepository {

    ResultClassEntity ListarImpugnacion(ImpugnacionDto param);

    ResultClassEntity notificarSolicitudImpugnacion(ImpugnacionDto param);

    ResultClassEntity RegistrarImpugnacion(ImpugnacionDto param);

    ResultClassEntity ActualizarImpugnacion(ImpugnacionDto param);

    ResultClassEntity ObtenerImpugnacion(ImpugnacionDto param);

    ResultClassEntity ActualizarImpugnacionArchivo(ImpugnacionDto param);

    ResultClassEntity EliminarImpugnacionArchivo(ImpugnacionDto param);
    ResultClassEntity ListarResolucionImpugnada(ImpugnacionDto param);
    ResultClassEntity ValidarImpugnacionResolucion(ImpugnacionDto param);
}
