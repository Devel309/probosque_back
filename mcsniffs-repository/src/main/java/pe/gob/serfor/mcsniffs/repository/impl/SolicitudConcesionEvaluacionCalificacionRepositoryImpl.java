package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionCalificacionDto;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionCalificacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudConcesionEvaluacionCalificacionRepositoryImpl extends JdbcDaoSupport
                implements SolicitudConcesionEvaluacionCalificacionRepository {

        @Autowired
        @Qualifier("dataSourceBDMCSNIFFS")
        DataSource dataSource;

        @PersistenceContext
        private EntityManager entityManager;

        @PostConstruct
        private void initialize() {
                setDataSource(dataSource);
        }

        private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ContratoRepositoryImpl.class);

        @Override
        public ResultClassEntity listarSolicitudConcesionEvaluacionCalificacion(
                        SolicitudConcesionEvaluacionCalificacionDto obj) {
                ResultClassEntity result = new ResultClassEntity();
                List<SolicitudConcesionEvaluacionCalificacionDto> lista = new ArrayList<SolicitudConcesionEvaluacionCalificacionDto>();

                try {
                        StoredProcedureQuery processStored = entityManager
                                        .createStoredProcedureQuery(
                                                        "Concesion.pa_SolicitudConcesion_Evaluacion_Calificacion_Listar");

                        processStored.registerStoredProcedureParameter("idSolicitudconcesionevaluacionCalificacion",
                                        Integer.class,
                                        ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idSolicitudconcesionevaluacion", Integer.class,
                                        ParameterMode.IN);

                        SpUtil.enableNullParams(processStored);
                        processStored.setParameter("idSolicitudconcesionevaluacionCalificacion",
                                        obj.getIdSolicitudConcesionEvaluacionCalificacion());
                        processStored.setParameter("idSolicitudconcesionevaluacion",
                                        obj.getIdSolicitudConcesionEvaluacion());

                        processStored.execute();

                        List<Object[]> spResult = processStored.getResultList();
                        if (spResult != null) {
                                SolicitudConcesionEvaluacionCalificacionDto c = null;
                                if (spResult.size() >= 1) {
                                        for (Object[] row : spResult) {

                                                c = new SolicitudConcesionEvaluacionCalificacionDto();
                                                c.setIdSolicitudConcesionEvaluacionCalificacion(
                                                                row[0] == null ? null : (Integer) row[0]);
                                                c.setIdSolicitudConcesionEvaluacion(
                                                                row[1] == null ? null : (Integer) row[1]);
                                                c.setCodigoCriterio(row[2] == null ? null : (String) row[2]);
                                                c.setNumPuntaje(row[3] == null ? null : (Integer) row[3]);

                                                lista.add(c);

                                        }
                                }
                        }

                        result.setData(lista);
                        result.setSuccess(true);
                        result.setMessage(
                                        "Se realizó la acción Listar Calificación Solicitud Concesion correctamente.");
                        return result;
                } catch (Exception e) {
                        log.error("listarSolicitudConcesionEvaluacionCalificacion", e.getMessage());
                        result.setSuccess(false);
                        result.setMessage(
                                        "Ocurrió un error. No se pudo realizar la acción Listar Calificación Solicitud Concesion.");
                        return result;
                }
        }

        @Override
        public ResultClassEntity registrarSolicitudConcesionEvaluacionCalificacion(
                        SolicitudConcesionEvaluacionCalificacionDto obj) {
                ResultClassEntity result = new ResultClassEntity();
                try {
                        StoredProcedureQuery processStored = entityManager
                                        .createStoredProcedureQuery(
                                                        "Concesion.pa_SolicitudConcesion_Evaluacion_Calificacion_Registrar");
                        processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                                        ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("codigoCriterio", String.class,
                                        ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("numPuntaje", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idSolicitudconcesionevaluacionCalificacion",
                                        Integer.class,
                                        ParameterMode.INOUT);

                        SpUtil.enableNullParams(processStored);

                        processStored.setParameter("idSolicitudConcesionEvaluacion",
                                        obj.getIdSolicitudConcesionEvaluacion());
                        processStored.setParameter("codigoCriterio", obj.getCodigoCriterio());
                        processStored.setParameter("numPuntaje", obj.getNumPuntaje());
                        processStored.setParameter("idUsuario", obj.getIdUsuarioRegistro());

                        processStored.execute();

                        Integer idSolicitudConcesionEvaluacionCalificacion = (Integer) processStored
                                        .getOutputParameterValue("idSolicitudConcesionEvaluacionCalificacion");
                        obj.setIdSolicitudConcesionEvaluacionCalificacion(idSolicitudConcesionEvaluacionCalificacion);
                        result.setCodigo(idSolicitudConcesionEvaluacionCalificacion);
                        result.setSuccess(true);
                        result.setData(obj);
                        result.setMessage("Se registró la calificación de la solicitud de concesión correctamente.");
                        return result;
                } catch (

                Exception e) {
                        log.error(e.getMessage(), e);
                        result.setSuccess(false);
                        result.setMessage(
                                        "Ocurrió un error. No se pudo registrar la calificación de la solicitud de concesión.");
                        result.setInnerException(e.getMessage());
                        return result;
                }
        }

        @Override
        public ResultClassEntity actualizarSolicitudConcesionEvaluacionCalificacion(
                        SolicitudConcesionEvaluacionCalificacionDto obj) {
                ResultClassEntity result = new ResultClassEntity();
                try {
                        StoredProcedureQuery processStored = entityManager
                                        .createStoredProcedureQuery(
                                                        "Concesion.pa_SolicitudConcesion_Evaluacion_Calificacion_Actualizar");
                        processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacionCalificacion",
                                        Integer.class,
                                        ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                                        ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("codigoCriterio", String.class,
                                        ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("numPuntaje", Integer.class, ParameterMode.IN);
                        processStored.registerStoredProcedureParameter("idUsuario", Integer.class, ParameterMode.IN);

                        SpUtil.enableNullParams(processStored);

                        processStored.setParameter("idSolicitudConcesionEvaluacionCalificacion",
                                        obj.getIdSolicitudConcesionEvaluacionCalificacion());
                        processStored.setParameter("idSolicitudConcesionEvaluacion",
                                        obj.getIdSolicitudConcesionEvaluacion());
                        processStored.setParameter("codigoCriterio", obj.getCodigoCriterio());
                        processStored.setParameter("numPuntaje", obj.getNumPuntaje());
                        processStored.setParameter("idUsuario", obj.getIdUsuarioModificacion());

                        processStored.execute();

                        result.setSuccess(true);
                        result.setMessage("Se actualizó la calificación de la solicitud de concesión correctamente.");
                        return result;
                } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        result.setSuccess(false);
                        result.setMessage(
                                        "Ocurrió un error. No se pudo calificación la actividad de la solicitud de concesión.");
                        result.setInnerException(e.getMessage());
                        return result;
                }
        }
}
