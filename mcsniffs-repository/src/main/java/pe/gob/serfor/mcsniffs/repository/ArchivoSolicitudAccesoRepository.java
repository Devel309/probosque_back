package pe.gob.serfor.mcsniffs.repository;

//interface repositori a la bd
//Guardar archivos solicitudacceso /return archivo
//Descargar archivos solicitudacceso /return archivo
//ee

import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;

import java.util.List;

public interface ArchivoSolicitudAccesoRepository{
    ResultClassEntity<ArchivoEntity> insertarArchivoSolicitudAcceso(String nombreArchivoGenerado,String nombreArchivo,Integer idSolicitudAcceso,String tipoDocumento);
    ResultClassEntity<ArchivoEntity> obtenerArchivoSolicitudAcceso(Integer idSolicitudAcceso);

    ResultClassEntity registrarDetalleArchivo(ArchivoSolicitudEntity item);
    ResultEntity<ArchivoSolicitudEntity> listarDetalleArchivo(Integer idPlanManejo);
    ResultClassEntity<Integer> eliminarDetalleArchivo(Integer idArchivo, Integer idUsuario) throws Exception;
}