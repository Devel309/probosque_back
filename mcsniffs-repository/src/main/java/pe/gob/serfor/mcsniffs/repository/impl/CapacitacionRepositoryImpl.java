package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.CapacitacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.CapacitacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Harry Coa [20-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar una Capacitacion}
 */

@Repository
public class CapacitacionRepositoryImpl extends JdbcDaoSupport implements CapacitacionRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.CapacitacionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado:
     * @descripción: {Registrar capacitación}
     * @param:Capacitacion
     */

    @Override
    public ResultClassEntity RegistrarCapacitacion(CapacitacionEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Capacitacion_Registrar");
            processStored.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tema", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("personaCapacitar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoModalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("codTipoCapacitacion", param.getCodTipoCapacitacion());
            processStored.setParameter("tema", param.getTema());
            processStored.setParameter("personaCapacitar", param.getPersonaCapacitar());
            processStored.setParameter("idTipoModalidad", param.getIdTipoModalidad());
            processStored.setParameter("lugar", param.getLugar());
            processStored.setParameter("periodo", param.getPeriodo());
            processStored.setParameter("responsable", param.getResponsable());
            processStored.setParameter("adjunto", param.getAdjunto());
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró la Capacitación correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado:
     * @descripción: {actualizar capacitación}
     * @param:Capacitacion
     */
    @Override
    public ResultClassEntity ActualizarCapacitacion(CapacitacionEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Capacitacion_Actualizar");
            processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tema", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("personaCapacitar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoModalidad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.setParameter("idCapacitacion", param.getIdCapacitacion());
            processStored.setParameter("codTipoCapacitacion", param.getCodTipoCapacitacion());
            processStored.setParameter("tema", param.getTema());
            processStored.setParameter("personaCapacitar", param.getPersonaCapacitar());
            processStored.setParameter("idTipoModalidad", param.getIdTipoModalidad());
            processStored.setParameter("lugar", param.getLugar());
            processStored.setParameter("periodo", param.getPeriodo());
            processStored.setParameter("responsable", param.getResponsable());
            processStored.setParameter("adjunto", param.getAdjunto());
            processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
            processStored.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se actualizó la Capacitación correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado:
     * @descripción: {eliminar capacitación}
     * @param:Capacitacion
     */
    @Override
    public ResultClassEntity EliminarCapacitacion(CapacitacionEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Capacitacion_Eliminar");
            processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idCapacitacion", param.getIdCapacitacion());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó el registro correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado:
     * @descripción: {Listar capacitación}
     * @param:Capacitacion
     */
    @Override
    public List<CapacitacionDto> ListarCapacitacion(CapacitacionDto param) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Capacitacion_ListarCabecera");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idCapacitacion", param.getIdCapacitacion());
            processStored.setParameter("codTipoCapacitacion", param.getCodTipoCapacitacion());
            processStored.execute();
            List<CapacitacionDto> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (!spResult.isEmpty()) {
                for (Object[] item : spResult) {
                    CapacitacionDto temp = new CapacitacionDto();
                    temp.setIdPlanManejo(item[0] == null ? null : (Integer) item[0]);
                    temp.setIdCapacitacion(item[1] == null ? null : (Integer) item[1]);
                    temp.setCodTipoCapacitacion(item[2] == null ? null : (String) item[2]);
                    temp.setTema(item[3] == null ? null : (String) item[3]);
                    temp.setPersonaCapacitar(item[4] == null ? null : (String) item[4]);
                    temp.setIdTipoModalidad(item[5] == null ? null : (String) item[5]);
                    temp.setLugar(item[6] == null ? null : (String) item[6]);
                    temp.setPeriodo(item[7] == null ? null : (String) item[7]);
                    temp.setResponsable(item[8] == null ? null : (String) item[8]);
                    temp.setAdjunto(item[9] == null ? null : item[9].toString());

                    ResultEntity<CapacitacionDetalleEntity> detalle = new ResultEntity<>();
                    detalle = listarCapacitacionDetalle(temp.getIdCapacitacion());
                    temp.setListCapacitacionDetalle(detalle.getData());
                    result.add(temp);
                }
            }
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private ResultEntity<CapacitacionDetalleEntity> listarCapacitacionDetalle(Integer idCapacitacion) {
        ResultEntity<CapacitacionDetalleEntity> result = new ResultEntity<>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_Capacitacion_ListarDetalle");
            processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idCapacitacion", idCapacitacion);
            processStored.execute();
            List<Object[]> spResult = processStored.getResultList();
            List<CapacitacionDetalleEntity> lstdocs = new ArrayList<>();
            if (!spResult.isEmpty()) {
                for (Object[] row_ : spResult) {
                    CapacitacionDetalleEntity detalle = new CapacitacionDetalleEntity();
                    detalle.setIdCapacitacionDet(row_[0] == null ? null : (Integer) row_[0]);
                    detalle.setDescripcion(row_[1] == null ? null : (String) row_[1]);
                    detalle.setActividad(row_[2] == null ? null : (String) row_[2]);
                    detalle.setPersonal(row_[3] == null ? null : (String) row_[3]);
                    detalle.setModalidad(row_[4] == null ? null : (String) row_[4]);
                    detalle.setLugar(row_[5] == null ? null : (String) row_[5]);
                    detalle.setPeriodo(row_[6] == null ? null : (String) row_[6]);
                    detalle.setResponsable(row_[7] == null ? null : (String) row_[7]);
                    lstdocs.add(detalle);
                }
            }

            result.setData(lstdocs);
            result.setIsSuccess(true);
            result.setMessage("Se listaron los detalles de la capacitacion.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setIsSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    /**
     * @autor: Harry Coa [20-07-2021]
     * @modificado:
     * @descripción: {eliminar capacitación}
     * @param:Capacitacion
     */
    @Override
    public ResultClassEntity EliminarCapacitacionDetalle(CapacitacionDetalleEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionDetalle_Eliminar");
            processStored.registerStoredProcedureParameter("idCapacitacionDet", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idCapacitacionDet", param.getIdCapacitacionDet());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó la capacitación correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }


    @Override
    public ResultClassEntity RegistrarCapacitacionDetalle(List<CapacitacionDto> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<CapacitacionDto> list_ = new ArrayList<CapacitacionDto>();
        try {
            for (CapacitacionDto p : list) {
                StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPo_Registrar");
                processStored_.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("tema", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("personaCapacitar", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idTipoModalidad", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("adjunto", String.class, ParameterMode.IN);
                processStored_.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(processStored_);
                processStored_.setParameter("idCapacitacion", p.getIdCapacitacion());
                processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
                processStored_.setParameter("codTipoCapacitacion", p.getCodTipoCapacitacion());
                processStored_.setParameter("tema", p.getTema());
                processStored_.setParameter("personaCapacitar", p.getPersonaCapacitar());
                processStored_.setParameter("idTipoModalidad", p.getIdTipoModalidad());
                processStored_.setParameter("lugar", p.getLugar());
                processStored_.setParameter("periodo", p.getPeriodo());
                processStored_.setParameter("responsable", p.getResponsable());
                processStored_.setParameter("adjunto", p.getAdjunto());
                processStored_.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                processStored_.execute();
                List<Object[]> spResult_ = processStored_.getResultList();
                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        CapacitacionDto obj_ = new CapacitacionDto();
                        obj_.setIdCapacitacion((Integer) row_[0]);
                        obj_.setIdPlanManejo((Integer) row_[1]);
                        List<CapacitacionDetalleEntity> listDet = new ArrayList<CapacitacionDetalleEntity>();
                        for (CapacitacionDetalleEntity param : p.getListCapacitacionDetalle()) {
                            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPoDetalle_Registrar");
                            processStored.registerStoredProcedureParameter("idCapacitacion", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idCapacitacionDet", Integer.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("personal", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("modalidad", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("lugar", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            SpUtil.enableNullParams(processStored);
                            processStored.setParameter("idCapacitacion", obj_.getIdCapacitacion());
                            processStored.setParameter("idCapacitacionDet", param.getIdCapacitacionDet());
                            processStored.setParameter("descripcion", param.getDescripcion());
                            processStored.setParameter("actividad", param.getActividad());
                            processStored.setParameter("personal", param.getPersonal());
                            processStored.setParameter("modalidad", param.getModalidad());
                            processStored.setParameter("lugar", param.getLugar());
                            processStored.setParameter("periodo", param.getPeriodo());
                            processStored.setParameter("responsable", param.getResponsable());
                            processStored.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                            processStored.execute();
                            List<Object[]> spResult = processStored.getResultList();
                            if (spResult.size() >= 1) {
                                for (Object[] row : spResult) {
                                    CapacitacionDetalleEntity obj = new CapacitacionDetalleEntity();
                                    obj.setIdCapacitacionDet((Integer) row[0]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListCapacitacionDetalle(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la capacitación correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error al registrar la capacitacion.");
            return result;
        }
    }

    @Override
    public ResultClassEntity ListarCapacitacionDetalle(CapacitacionEntity p) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        CapacitacionDto capacitacion = new CapacitacionDto();
        capacitacion.setListCapacitacionDetalle(new ArrayList<>());
        try {
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPo_Listar");
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
            processStored_.setParameter("idPlanManejo", p.getIdPlanManejo());
            processStored_.setParameter("codTipoCapacitacion", p.getCodTipoCapacitacion());
            processStored_.execute();
            List<Object[]> spResult_ = processStored_.getResultList();
            if (spResult_.size() >= 1) {
                Object[] item = spResult_.get(0);
                capacitacion.setIdPlanManejo(item[0] == null ? null : (Integer) item[0]);
                capacitacion.setIdCapacitacion(item[1] == null ? null : (Integer) item[1]);
                capacitacion.setCodTipoCapacitacion(item[2] == null ? null : (String) item[2]);
                capacitacion.setTema(item[3] == null ? null : (String) item[3]);
                capacitacion.setPersonaCapacitar(item[4] == null ? null : (String) item[4]);
                capacitacion.setIdTipoModalidad(item[5] == null ? null : (String) item[5]);
                capacitacion.setLugar(item[6] == null ? null : (String) item[6]);
                capacitacion.setPeriodo(item[7] == null ? null : (String) item[7]);
                capacitacion.setResponsable(item[8] == null ? null : (String) item[8]);
                capacitacion.setAdjunto(item[9] == null ? null : item[9].toString());
                for (Object[] row_ : spResult_) {
                    CapacitacionDetalleEntity detalle = new CapacitacionDetalleEntity();
                    detalle.setIdCapacitacionDet(row_[10] == null ? null : (Integer) row_[10]);
                    detalle.setDescripcion(row_[11] == null ? null : (String) row_[11]);
                    detalle.setActividad(row_[12] == null ? null : (String) row_[12]);
                    detalle.setPersonal(row_[13] == null ? null : (String) row_[13]);
                    detalle.setModalidad(row_[14] == null ? null : (String) row_[14]);
                    detalle.setLugar(row_[15] == null ? null : (String) row_[15]);
                    detalle.setPeriodo(row_[16] == null ? null : (String) row_[16]);
                    detalle.setResponsable(row_[17] == null ? null : (String) row_[17]);
                    capacitacion.getListCapacitacionDetalle().add(detalle);
                }
            } else {
                capacitacion.setIdPlanManejo(p.getIdPlanManejo());
                capacitacion.setIdCapacitacion(0);
            }
            result.setData(capacitacion);
            result.setSuccess(true);
            result.setMessage("Se listo la capacitación correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<CapacitacionDto> ListarCapacitacionCabeceraDetalle(Integer idPlanManejo, String codTipoCapacitacion) throws Exception {
        List<CapacitacionDto> list = new ArrayList<>();
        CapacitacionDto capacitacion = new CapacitacionDto();
        capacitacion.setListCapacitacionDetalle(new ArrayList<>());
        try {
            StoredProcedureQuery processStored_ = entityManager.createStoredProcedureQuery("dbo.pa_CapacitacionPo_Listar");
            processStored_.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored_.registerStoredProcedureParameter("codTipoCapacitacion", String.class, ParameterMode.IN);
            processStored_.setParameter("idPlanManejo", idPlanManejo);
            processStored_.setParameter("codTipoCapacitacion", codTipoCapacitacion);
            processStored_.execute();
            List<Object[]> spResult_ = processStored_.getResultList();
            if (!spResult_.isEmpty()) {
                for (Object[] row_ : spResult_) {


                    capacitacion.setIdPlanManejo(row_[0] == null ? null : (Integer) row_[0]);
                    capacitacion.setIdCapacitacion(row_[1] == null ? null : (Integer) row_[1]);
                    capacitacion.setCodTipoCapacitacion(row_[2] == null ? null : (String) row_[2]);
                    capacitacion.setTema(row_[3] == null ? null : (String) row_[3]);
                    capacitacion.setPersonaCapacitar(row_[4] == null ? null : (String) row_[4]);
                    capacitacion.setIdTipoModalidad(row_[5] == null ? null : (String) row_[5]);
                    capacitacion.setLugar(row_[6] == null ? null : (String) row_[6]);
                    capacitacion.setPeriodo(row_[7] == null ? null : (String) row_[7]);
                    capacitacion.setResponsable(row_[8] == null ? null : (String) row_[8]);
                    capacitacion.setAdjunto(row_[9] == null ? null : row_[9].toString());

                    for (Object[] row : spResult_) {
                        CapacitacionDetalleEntity detalle = new CapacitacionDetalleEntity();
                        detalle.setIdCapacitacionDet(row[10] == null ? null : (Integer) row[10]);
                        detalle.setDescripcion(row[11] == null ? null : (String) row[11]);
                        detalle.setActividad(row[12] == null ? null : (String) row[12]);
                        detalle.setPersonal(row[13] == null ? null : (String) row[13]);
                        detalle.setModalidad(row[14] == null ? null : (String) row[14]);
                        detalle.setLugar(row[15] == null ? null : (String) row[15]);
                        detalle.setPeriodo(row[16] == null ? null : (String) row[16]);
                        detalle.setResponsable(row[17] == null ? null : (String) row[17]);
                        capacitacion.getListCapacitacionDetalle().add(detalle);
                    }

                    list.add(capacitacion);
                }
            }
            return list;
        } catch (Exception e) {
            log.error("listar Capacitacion", e.getMessage());
            throw e;
        }
    }
}
