package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.ResolucionRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class ResolucionRepositoryImpl extends JdbcDaoSupport implements ResolucionRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(ResolucionRepositoryImpl.class);
    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity ListarResolucion(ResolucionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Resolucion_Listar");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocumentoGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("estadoResolucion", param.getEstadoResolucion());
            sp.setParameter("nroDocumentoGestion", param.getNroDocumentoGestion());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());
            sp.execute();
            List<ResolucionDto> list = new ArrayList<ResolucionDto>();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {
                    ResolucionDto obj = new ResolucionDto();
                    obj.setIdResolucion((Integer) row[0]);
                    obj.setTipoDocumentoGestion((String) row[1]);
                    obj.setNroDocumentoGestion((Integer) row[2]);
                    obj.setIdArchivoResolucion((Integer) row[3]);
                    obj.setFechaResolucion((Date) row[4]);
                    obj.setEstadoResolucion((String) row[5]);
                    obj.setComentarioResolucion((String) row[6]);
                    obj.setFechaCancelacion((Date) row[7]);
                    obj.setTextoConcatenado((String) row[8]);
                    obj.setIdResolucionPrevia((Integer) row[9]);
                    result.setTotalRecord((Integer) row[10]);
                    list.add(obj);
                }
            }
            result.setData(list);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity ObtenerResolucion(ResolucionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Resolucion_Listar");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocumentoGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageNum", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("estadoResolucion", param.getEstadoResolucion());
            sp.setParameter("nroDocumentoGestion", param.getNroDocumentoGestion());
            sp.setParameter("pageNum", param.getPageNum());
            sp.setParameter("pageSize", param.getPageSize());
            sp.execute();
            ResolucionDto obj = new ResolucionDto();
            List<Object[]> spResult = sp.getResultList();
            if (spResult!= null && !spResult.isEmpty()){
                for (Object[] row : spResult) {

                    obj.setIdResolucion((Integer) row[0]);
                    obj.setTipoDocumentoGestion((String) row[1]);
                    obj.setNroDocumentoGestion((Integer) row[2]);
                    obj.setIdArchivoResolucion((Integer) row[3]);
                    obj.setFechaResolucion((Date) row[4]);
                    obj.setEstadoResolucion((String) row[5]);
                    obj.setComentarioResolucion((String) row[6]);
                    obj.setFechaCancelacion((Date) row[7]);
                    obj.setTextoConcatenado((String) row[8]);
                    obj.setIdResolucionPrevia((Integer) row[9]);

                }
            }
            result.setData(obj);
            result.setSuccess(true);
            result.setMessage(spResult.size()>0?"Información encontrada":"No se encontró información");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }

    }
    @Override
    public ResultClassEntity registrarResolucion(ResolucionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Resolucion_Registrar");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.INOUT);
            sp.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocumentoGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaResolucion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idResolucionPrevia", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("comentarioResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaCancelacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idPlanManejoEvaluacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("tipoDocumentoGestion", param.getTipoDocumentoGestion());
            sp.setParameter("nroDocumentoGestion", param.getNroDocumentoGestion());
            sp.setParameter("idArchivoResolucion", param.getIdArchivoResolucion());
            sp.setParameter("fechaResolucion", param.getFechaResolucion());
            sp.setParameter("idResolucionPrevia", param.getIdResolucionPrevia());
            sp.setParameter("estadoResolucion", param.getEstadoResolucion());
            sp.setParameter("comentarioResolucion", param.getComentarioResolucion());
            sp.setParameter("fechaCancelacion", param.getFechaCancelacion());
            sp.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            sp.setParameter("idPlanManejoEvaluacion", param.getIdPlanManejoEvaluacion());
            sp.execute();

            Integer id = (Integer) sp.getOutputParameterValue("idResolucion");
            param.setIdResolucion(id);
            result.setCodigo(id);
            result.setSuccess(true);
            result.setMessage("Se registró Resolución correctamente.");

            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_RESOLUCION,
                    LogAuditoria.REGISTRAR,
                    LogAuditoria.DescripcionAccion.Registrar.T_MVC_RESOLUCION + id,
                    param.getIdUsuarioRegistro());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarResolucion(ResolucionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Resolucion_Actualizar");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("tipoDocumentoGestion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("nroDocumentoGestion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idArchivoResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaResolucion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("comentarioResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("fechaCancelacion", Date.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("tipoDocumentoGestion", param.getTipoDocumentoGestion());
            sp.setParameter("nroDocumentoGestion", param.getNroDocumentoGestion());
            sp.setParameter("idArchivoResolucion", param.getIdArchivoResolucion());
            sp.setParameter("fechaResolucion", param.getFechaResolucion());
            sp.setParameter("estadoResolucion", param.getEstadoResolucion());
            sp.setParameter("comentarioResolucion", param.getComentarioResolucion());
            sp.setParameter("fechaCancelacion", param.getFechaCancelacion());
            sp.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Resolución correctamente.");


            LogAuditoriaEntity logAuditoriaEntity = new LogAuditoriaEntity(
                    LogAuditoria.Table.T_MVC_RESOLUCION,
                    LogAuditoria.ACTUALIZAR,
                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_RESOLUCION + param.getIdResolucion(),
                    param.getIdUsuarioModificacion());

            logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);

            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity ActualizarResolucionEstado(ResolucionDto param) {
        ResultClassEntity result = new ResultClassEntity();
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Resolucion_ActualizarEstado");
            sp.registerStoredProcedureParameter("idResolucion", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("estadoResolucion", String.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idResolucion", param.getIdResolucion());
            sp.setParameter("estadoResolucion", param.getEstadoResolucion());
            sp.setParameter("idUsuarioModificacion", param.getIdUsuarioModificacion());
            sp.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Resolución Estado correctamente.");
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public String resolucionValidar(Integer idProcesoOferta) {
        String result = null;
        
        try{
            StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_Resolucion_Validar");
            sp.registerStoredProcedureParameter("idProceoOferta", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("msg", String.class, ParameterMode.INOUT);
            SpUtil.enableNullParams(sp);
            sp.setParameter("idProceoOferta", idProcesoOferta) ;           
            
            sp.execute();
            result = (String) sp.getOutputParameterValue("msg");
            
            return  result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            
        }

        return  result;
    }

}
