package pe.gob.serfor.mcsniffs.repository.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;
import pe.gob.serfor.mcsniffs.repository.TalaRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository("repositoryTala")
public class TalaRepositoryImpl extends JdbcDaoSupport implements TalaRepository{

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger log = LogManager.getLogger(OposicionRepositoryImpl.class);

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity registrarTala(TalaEntity item)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Sincronizacion.pa_Tala_Registrar");
            processStored.registerStoredProcedureParameter("IdTala", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdCensoForestalDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaTala", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("CantidadRama", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("EstadoTala", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("NumeroTrozado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaTrozado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("ObservacionTrozado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaRegistro", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioModificacion", Integer.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaModificacion", Date.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioElimina", Integer.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaElimina", Date.class,ParameterMode.IN);
            

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdTala", item.getIdTala());
            processStored.setParameter("IdCensoForestalDetalle", item.getIdCensoForestalDetalle());
            processStored.setParameter("FechaTala", item.getFechaTala());
            processStored.setParameter("CantidadRama", item.getCantidadRama());
            processStored.setParameter("Observaciones", item.getObservaciones());
            processStored.setParameter("EstadoTala", item.getEstadoTala());
            processStored.setParameter("NumeroTrozado", item.getNumeroTrozado());
            processStored.setParameter("FechaTrozado", item.getFechaTrozado());
            processStored.setParameter("ObservacionTrozado", item.getObservacionTrozado());
            processStored.setParameter("Estado", item.getEstado());
            processStored.setParameter("IdUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.setParameter("FechaRegistro", item.getFechaRegistro());
            processStored.setParameter("IdUsuarioModificacion", item.getIdUsuarioModificacion());
            processStored.setParameter("FechaModificacion", item.getFechaModificacion());
            processStored.setParameter("IdUsuarioElimina", item.getIdUsuarioElimina());
            processStored.setParameter("FechaElimina", item.getFechaElimina());
            processStored.execute();
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la tala.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity registrarTalaDetalle(TalaDetalleEntity item)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Sincronizacion.pa_TalaDetalle_Registrar");
            processStored.registerStoredProcedureParameter("IdTalaDetalle", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdTalaDetalleCab", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("TipoTalaDetalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdTala", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("DiametroMa", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("DiametroMe", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Longitud", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Volumen ", Double.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Observaciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("NivelTrozado", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdOrdenProduccion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("EstadoTalaDetalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("Estado", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioRegistro", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaRegistro", Date.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioModificacion", Integer.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaModificacion", Date.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("IdUsuarioElimina", Integer.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("FechaElimina", Date.class,ParameterMode.IN);
            

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("IdTalaDetalle", item.getIdTalaDetalle());
            processStored.setParameter("IdTalaDetalleCab", item.getIdTalaDetalleCab());
            processStored.setParameter("TipoTalaDetalle", item.getTipoTalaDetalle());
            processStored.setParameter("IdTala", item.getIdTala());
            processStored.setParameter("DiametroMa", item.getDiametroMa());
            processStored.setParameter("DiametroMe", item.getDiametroMe());
            processStored.setParameter("Longitud", item.getLongitud());
            processStored.setParameter("Volumen ", item.getVolumen());
            processStored.setParameter("Observaciones", item.getObservaciones());
            processStored.setParameter("NivelTrozado", item.getNivelTrozado());
            processStored.setParameter("IdOrdenProduccion", item.getIdOrdenProduccion());
            processStored.setParameter("EstadoTalaDetalle", item.getEstadoTalaDetalle());
            processStored.setParameter("Estado", item.getEstado());
            processStored.setParameter("IdUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.setParameter("FechaRegistro", item.getFechaRegistro());
            processStored.setParameter("IdUsuarioModificacion", item.getIdUsuarioModificacion());
            processStored.setParameter("FechaModificacion", item.getFechaModificacion());
            processStored.setParameter("IdUsuarioElimina", item.getIdUsuarioElimina());
            processStored.setParameter("FechaElimina", item.getFechaElimina());
            processStored.execute();
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage(
                    "Ocurrió un error. No se pudo registrar la tala.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }
}
