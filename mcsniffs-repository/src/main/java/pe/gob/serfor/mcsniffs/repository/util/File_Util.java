package pe.gob.serfor.mcsniffs.repository.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

public class File_Util {

    public File_Util() {
		super();
	
	}
    
    public  XSSFSheet getSheet(MultipartFile file , Integer indice) throws Exception{
        byte[]  fileContent = file.getBytes();
        File file2= File.createTempFile("xxx", ".xlsx");
			OutputStream os = new FileOutputStream(file2);
            os.write(fileContent);
            os.close();

        FileInputStream file_stream = new FileInputStream(file2);
        XSSFWorkbook workbook = new XSSFWorkbook(file_stream); 
        return workbook.getSheetAt(indice);
    }

    public  XSSFWorkbook getWorkBook(MultipartFile file) throws Exception{
        byte[]  fileContent = file.getBytes();
        File file2= File.createTempFile("xxx", ".xlsx");
			OutputStream os = new FileOutputStream(file2);
            os.write(fileContent);
            os.close();

        FileInputStream file_stream = new FileInputStream(file2);
        XSSFWorkbook workbook = new XSSFWorkbook(file_stream); 
        return workbook; 
    }

    public List getExcel(MultipartFile file, String nombreHoja, int numeroFila, int numeroColumna) throws Exception{
        List listaExcel = new ArrayList();
        List listaExcelFila = new ArrayList();
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            if (workbook.getSheetName(i).equals(nombreHoja)) {
                XSSFSheet sheet = workbook.getSheetAt(i);
                for (int rowNum = numeroFila - 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
                    Row row = sheet.getRow(rowNum);
                    listaExcelFila = new ArrayList();
                    for (int colNum = numeroColumna - 1; colNum < sheet.getRow(rowNum).getLastCellNum(); colNum++) {
                        String validaCelda = String.valueOf(row==null || row.getCell(colNum)==null?"":row.getCell(colNum));
                        listaExcelFila.add(validaCelda);
                    }
                    listaExcel.add(listaExcelFila);
                }
            }
        }
        return listaExcel;
    }

    public List getExcelRow(MultipartFile file, String nombreHoja, int numeroFila, int numeroFilaUltimo, int numeroColumna) throws Exception{
        List listaExcel = new ArrayList();
        List listaExcelFila = new ArrayList();
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            if (workbook.getSheetName(i).equals(nombreHoja)) {
                XSSFSheet sheet = workbook.getSheetAt(i);
                if(numeroFilaUltimo == -1) numeroFilaUltimo = sheet.getLastRowNum();
                for (int rowNum = numeroFila - 1; rowNum <= numeroFilaUltimo; rowNum++) {
                    Row row = sheet.getRow(rowNum);
                    listaExcelFila = new ArrayList();
                    for (int colNum = numeroColumna - 1; colNum < sheet.getRow(rowNum).getLastCellNum(); colNum++) {
                        String validaCelda = String.valueOf(row==null || row.getCell(colNum)==null?"":row.getCell(colNum));
                        listaExcelFila.add(validaCelda);
                    }
                    listaExcel.add(listaExcelFila);
                }
            }
        }
        return listaExcel;
    }
}
