package pe.gob.serfor.mcsniffs.repository;
 import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

public interface ArchivoSolicitudRepository {
    ResultClassEntity<Boolean> archivoEliminar(Integer idArchivo); 

    ResultClassEntity<ArchivoEntity> insertarArchivoSolicitud(String nombreArchivoGenerado, String originalFilename, Integer idSolicitud, String tipoArchivo);

    ResultClassEntity<ArchivoEntity> actualizarArchivoSolicitud(String nombreArchivoGenerado, String originalFilename, Integer idSolicitud, String tipoArchivo);

    ResultClassEntity<ArchivoEntity> insertarArchivoSolicitudAdjunto(String nombreArchivoGenerado, String originalFilename, Integer idSolicitud, String tipoArchivo, String descripcionArchivo);

    List<ArchivoSolicitudEntity> obtenerArchivoSolicitud(Integer idSolicitud);

    List<ArchivoSolicitudEntity> obtenerArchivoSolicitudAdjunto(Integer idSolicitud);

    ResultClassEntity<ArchivoSolicitudEntity> obtenerArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo);

    ResultClassEntity<ArchivoSolicitudEntity> obtenerArchivoSolicitudAdjunto(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo);

    ResultClassEntity eliminarArchivoSolicitud(Integer idArchivoSolicitud, Integer idSolicitud, String tipoArchivo);

    ResultClassEntity<ArchivoEntity> actualizarArchivoSolicitudAdjunto(String nombreArchivoGenerado, String originalFilename, Integer idSolicitud, String tipoArchivo, String descripcionArchivo);

    ResultClassEntity eliminarArchivoSolicitudAdjunto(Integer idArchivoSolicitudAjunto, Integer idSolicitud, String tipoArchivo);
}