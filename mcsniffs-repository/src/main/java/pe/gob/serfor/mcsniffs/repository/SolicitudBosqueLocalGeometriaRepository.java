package pe.gob.serfor.mcsniffs.repository;


import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalGeometriaEntity;

public interface SolicitudBosqueLocalGeometriaRepository {
    ResultClassEntity registrarSolicitudBosqueLocalGeometria(SolicitudBosqueLocalGeometriaEntity items)
            throws Exception;

    ResultClassEntity actualizarSolicitudBosqueLocalGeometria(SolicitudBosqueLocalGeometriaEntity items)
            throws Exception;

    ResultClassEntity listarSolicitudBosqueLocalGeometria(SolicitudBosqueLocalGeometriaEntity item);

    ResultClassEntity eliminarSolicitudBosqueLocalGeometriaArchivo(SolicitudBosqueLocalGeometriaEntity item);
}
