package pe.gob.serfor.mcsniffs.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetallePgmfDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.repository.ActividadSilviculturalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

/**
 * @autor: Harry Coa [18-08-2021]
 * @modificado:
 * @descripción: {Repository actividades silviculturales}
 */
@Repository
public class ActividadSilviculturalRepositoryImpl extends JdbcDaoSupport implements ActividadSilviculturalRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(ActividadSilviculturalRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetalle(Integer idPlanManejo)
            throws Exception {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_ActividadSilvicultural_Listar");
        sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        sp.setParameter("idPlanManejo", idPlanManejo);
        sp.execute();
        ActividadSilviculturalDto data = setResultData(sp.getResultList());
        ResultClassEntity<ActividadSilviculturalDto> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalFiltroTipo(Integer idPlanManejo,
            String idTipo) throws Exception {
        StoredProcedureQuery store = em.createStoredProcedureQuery("pa_ActividadSilvicultural_ListarFiltroTipo");
        store.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        store.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
        store.setParameter("idPlanManejo", idPlanManejo);
        store.setParameter("idTipo", idTipo);
        
        store.execute();
        ActividadSilviculturalDto data = setResultData(store.getResultList());
        ResultClassEntity<ActividadSilviculturalDto> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalFiltroTipoTitular(String tipoDocumento, String nroDocumento,String codigoProcesoTitular,String codigoProceso,Integer idPlanManejo)throws Exception {
        StoredProcedureQuery store = em.createStoredProcedureQuery("pa_ActividadSilvicultural_ListarFiltroTipo_Titular");
        store.registerStoredProcedureParameter("tipoDocumento", String.class, ParameterMode.IN);
        store.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
        store.registerStoredProcedureParameter("codigoProcesoTitular", String.class, ParameterMode.IN);
        store.registerStoredProcedureParameter("codigoProceso", String.class, ParameterMode.IN);
        store.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(store);
        store.setParameter("tipoDocumento", tipoDocumento);
        store.setParameter("nroDocumento", nroDocumento);
        store.setParameter("codigoProcesoTitular", codigoProcesoTitular);
        store.setParameter("codigoProceso", codigoProceso);
        store.setParameter("idPlanManejo", idPlanManejo);

        store.execute();
        ActividadSilviculturalDto data = setResultData(store.getResultList());
        ResultClassEntity<ActividadSilviculturalDto> result = new ResultClassEntity<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ResultClassEntity<List<ActividadSilviculturalDto>> ListarActividadSilviculturalCabecera(Integer idPlanManejo) throws Exception {
        List<ActividadSilviculturalDto> resultList = new ArrayList<>();        
        StoredProcedureQuery store = em.createStoredProcedureQuery("pa_ActividadSilviculturalCabecera_Listar");
        store.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        store.setParameter("idPlanManejo", idPlanManejo);
        
        store.execute();
        List<Object[]> result = store.getResultList();

        if (result != null && !result.isEmpty()) {
            for (Object[] row : result) {
                ActividadSilviculturalDto data = new ActividadSilviculturalDto();
                
                // Object[] item = result.get(0);
                data.setIdPlanManejo((Integer) row[0]);
                data.setIdActividadSilvicultural((Integer) row[1]);
                data.setCodActividad((String) row[2]);
                data.setActividad((String) row[3]);
                data.setMonitoreo(String.valueOf((Character) row[4]));
                data.setAnexo(String.valueOf((Character) row[5]));
                data.setDescripcion((String) row[6]);
                data.setObservacion((String) row[7]);

                resultList.add(data);
            }
        }
        
        ResultClassEntity<List<ActividadSilviculturalDto>> resultProc = new ResultClassEntity<>();
        resultProc.setData(resultList);
        resultProc.setSuccess(true);
        return resultProc;
    }

    @Override
    public ResultClassEntity RegistrarActividadSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ActividadSilviculturalEntity> list_ = new ArrayList<>();
        try {
            for (ActividadSilviculturalEntity p : list) {
                StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadSilvicultural_Registrar");
                sp.registerStoredProcedureParameter("idActSilvicultural", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoActSilvicultural", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("monitoreo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                setStoreProcedureEnableNullParameters(sp);
                sp.setParameter("idActSilvicultural", p.getIdActSilvicultural());
                sp.setParameter("codigoTipoActSilvicultural", p.getCodigoTipoActSilvicultural());
                sp.setParameter("actividad", p.getActividad());
                sp.setParameter("monitoreo", p.getMonitoreo());
                sp.setParameter("anexo", p.getAnexo());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadSilviculturalEntity obj_ = new ActividadSilviculturalEntity();
                        obj_.setIdActSilvicultural((Integer) row_[0]);
                        obj_.setCodigoTipoActSilvicultural((String) row_[1]);
                        obj_.setActividad((String) row_[2]);
                        obj_.setMonitoreo(String.valueOf((Character) row_[3]));
                        obj_.setAnexo(String.valueOf((Character) row_[4]));
                        obj_.setDescripcion((String) row_[5]);
                        obj_.setObservacion((String) row_[6]);
                        obj_.setIdPlanManejo((Integer) row_[7]);
                        List<ActividadSilviculturalDetalleEntity> listDet = new ArrayList<>();
                        for (ActividadSilviculturalDetalleEntity param : p.getListActividadSilvicultural()) {
                            StoredProcedureQuery pa = em
                                    .createStoredProcedureQuery("dbo.pa_ActividadSilviculturalDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idActividadSilviculturalDet", Integer.class,
                                    ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idTipoTratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoTratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("accionSilvicultural", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionDetalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idActSilvicultural", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            setStoreProcedureEnableNullParameters(pa);
                            pa.setParameter("idActividadSilviculturalDet", param.getIdActividadSilviculturalDet());
                            pa.setParameter("idTipo", param.getIdTipo());
                            pa.setParameter("idTipoTratamiento", param.getIdTipoTratamiento());
                            pa.setParameter("tipoTratamiento", param.getTipoTratamiento());
                            pa.setParameter("accion", param.getAccion());
                            pa.setParameter("accionSilvicultural", param.getIdActSilvicultural());
                            pa.setParameter("descripcionDetalle", param.getDescripcionDetalle());
                            pa.setParameter("tratamiento", param.getTratamiento());
                            pa.setParameter("justificacion", param.getJustificacion());
                            pa.setParameter("idActSilvicultural", obj_.getIdActSilvicultural());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                ActividadSilviculturalDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new ActividadSilviculturalDetalleEntity();
                                    obj.setIdActividadSilviculturalDet((Integer) row[0]);
                                    obj.setIdTipo((String) row[1]);
                                    obj.setIdTipoTratamiento((String) row[2]);
                                    obj.setTipoTratamiento((String) row[3]);
                                    obj.setAccion((boolean) row[4]);
                                    obj.setDescripcionDetalle((String) row[5]);
                                    obj.setTratamiento((String) row[6]);
                                    obj.setJustificacion((String) row[7]);
                                    obj.setIdActSilvicultural((Integer) row[8]);

                                    //obj.setAccionSilvicultural((boolean) row[9]);
                                    listDet.add(obj);

                                }
                            }
                        }
                        obj_.setListActividadSilvicultural(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la actividad silvicultural correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<ActividadSilviculturalDetalleDto> ListarActividadSilvicultural(Integer idPlanManejo,String codigoPlan) throws Exception {
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_ActividadSilvicultural_Listar");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("codigoPlan", String.class, ParameterMode.IN);
            setStoreProcedureEnableNullParameters(sp);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("codigoPlan", codigoPlan);
            sp.execute();
            List<ActividadSilviculturalDetalleDto> resultDet = new ArrayList<>();
            List<Object[]> spResultDet = sp.getResultList();
            if (!spResultDet.isEmpty()) {
                ActividadSilviculturalDetalleDto temp = null;
                for (Object[] item : spResultDet) {
                    temp = new ActividadSilviculturalDetalleDto();
                    temp.setIdPlanManejo((Integer) item[0]);
                    temp.setIdActividadSilvicultural((Integer) item[1]);
                    temp.setCodActividad((String) item[2]);
                    temp.setActividad((String) item[3]);
                    temp.setMonitoreo(String.valueOf((Character) item[4]));
                    temp.setAnexo(String.valueOf((Character) item[5]));
                    temp.setDescripcion((String) item[6]);
                    temp.setObservacion((String) item[7]);
                    temp.setIdActividadSilviculturalDet((Integer) item[8]);
                    temp.setIdTipo((String) item[9]);
                    temp.setIdTipoTratamiento((String) item[10]);
                    temp.setTipoTratamiento((String) item[11]);

                    if (item[12] != null) {
                        temp.setAccion((boolean) item[12]);
                    } else {
                        temp.setAccion(false);
                    }

                    // temp.setAccion((boolean) item[12]);
                    temp.setDescripcionDetalle((String) item[13]);
                    temp.setTratamiento((String) item[14]);
                    temp.setJustificacion((String) item[15]);
                    temp.setIdActividadSilvicultural((Integer) item[16]);
                    if (item[17] != null) {
                        temp.setAccionSilvicultural((boolean) item[17]);
                    } else {
                        temp.setAccionSilvicultural(false);
                    }
                    resultDet.add(temp);
                }
            }
            return resultDet;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }


    @Override
    public List<ActividadSilviculturalEntity> ListarActSilviCulturalDetalle(ActividadSilviculturalEntity param) throws Exception {

        try {
            int idDetalle=0;
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_ActSilvicultural_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoPlan", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("codigoPlan", param.getCodigoTipoActSilvicultural());

            processStored.execute();
            List<ActividadSilviculturalEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                ActividadSilviculturalEntity temp = null;
                for (Object[] row: spResult){
                    temp = new ActividadSilviculturalEntity();
                    temp.setIdPlanManejo((Integer) row[0]);
                    temp.setIdActSilvicultural((Integer) row[1]);
                    temp.setCodigoTipoActSilvicultural(row[2]==null?null:(String) row[2]);
                    temp.setActividad(row[3]==null?null:(String) row[3]);
                    //temp.setMonitoreo(row[4]==null?null:(String) row[4]);
                    temp.setMonitoreo(String.valueOf((Character) row[4]));
                    temp.setAnexo(String.valueOf((Character) row[5]));
                    //temp.setAnexo(row[5]==null?null:(String) row[5]);
                    temp.setDescripcion(row[6]==null?null:(String) row[6]);
                    temp.setObservacion(row[7]==null?null:(String) row[7]);

                    if(row[1]==null){
                        idDetalle=0;
                    }
                    else{
                        idDetalle=(Integer) row[1];
                    }

                    /********************************** OPERACION **********************************************/
                    List<ActividadSilviculturalDetalleEntity> listDetActividad = new ArrayList<ActividadSilviculturalDetalleEntity>();
                    StoredProcedureQuery pa = em.createStoredProcedureQuery("dbo.pa_ActSilviculturalDetalle_Listar");
                    pa.registerStoredProcedureParameter("idActividad", Integer.class, ParameterMode.IN);
                    pa.registerStoredProcedureParameter("codigoPlan", String.class, ParameterMode.IN);
                    SpUtil.enableNullParams(pa);
                    if(idDetalle==0){pa.setParameter("idActividad", null); }
                    else{pa.setParameter("idActividad", idDetalle);}
                    pa.setParameter("codigoPlan", param.getCodigoTipoActSilvicultural());
                    pa.execute();
                    List<Object[]> spResultAct = pa.getResultList();
                    if(spResultAct!=null) {
                        if (spResultAct.size() >= 1) {
                            for (Object[] rowAct : spResultAct) {
                                ActividadSilviculturalDetalleEntity temp2 = new ActividadSilviculturalDetalleEntity();
                                temp2.setIdActividadSilviculturalDet(rowAct[0]==null?null:(Integer) rowAct[0]);
                                temp2.setIdTipo(rowAct[1]==null?null:(String) rowAct[1]);
                                temp2.setIdTipoTratamiento(rowAct[2]==null?null:(String) rowAct[2]);
                                temp2.setTipoTratamiento(rowAct[3]==null?null:(String) rowAct[3]);
                                if (rowAct[4] != null) {
                                    temp2.setAccion((boolean) rowAct[4]);
                                } else {
                                    temp2.setAccion(false);
                                }
                                temp2.setDescripcionDetalle(rowAct[5]==null?null:(String) rowAct[5]);
                                temp2.setTratamiento(rowAct[6]==null?null:(String) rowAct[6]);
                                temp2.setJustificacion(rowAct[7]==null?null:(String) rowAct[7]);
                                //temp2.setIdActividadSilvicultural(rowAct[8]==null?null:(Integer) rowAct[8]);
                                if (rowAct[9] != null) {
                                    temp2.setAccionSilvicultural((boolean) rowAct[9]);
                                } else {
                                    temp2.setAccionSilvicultural(false);
                                }
                                listDetActividad.add(temp2);
                            }
                        }
                    }
                    /********************************************************************************/
                    temp.setListActividadSilvicultural(listDetActividad);



                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity RegistrarLaborSilvicultural(List<ActividadSilviculturalEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<ActividadSilviculturalEntity> list_ = new ArrayList<>();
        try {
            for (ActividadSilviculturalEntity p : list) {
                StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_ActividadSilvicultural_Registrar");
                sp.registerStoredProcedureParameter("idActSilvicultural", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoActSilvicultural", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("monitoreo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("anexo", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                setStoreProcedureEnableNullParameters(sp);
                sp.setParameter("idActSilvicultural", p.getIdActSilvicultural());
                sp.setParameter("codigoTipoActSilvicultural", p.getCodigoTipoActSilvicultural());
                sp.setParameter("actividad", p.getActividad());
                sp.setParameter("monitoreo", p.getMonitoreo());
                sp.setParameter("anexo", p.getAnexo());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        ActividadSilviculturalEntity obj_ = new ActividadSilviculturalEntity();
                        obj_.setIdActSilvicultural((Integer) row_[0]);
                        obj_.setCodigoTipoActSilvicultural((String) row_[1]);
                        obj_.setActividad((String) row_[2]);
                        obj_.setMonitoreo(String.valueOf((Character) row_[3]));
                        obj_.setAnexo(String.valueOf((Character) row_[4]));
                        obj_.setDescripcion((String) row_[5]);
                        obj_.setObservacion((String) row_[6]);
                        obj_.setIdPlanManejo((Integer) row_[7]);
                        List<ActividadSilviculturalDetalleEntity> listDet = new ArrayList<>();
                        for (ActividadSilviculturalDetalleEntity param : p.getListActividadSilvicultural()) {
                            StoredProcedureQuery pa = em
                                    .createStoredProcedureQuery("dbo.pa_LaborSilvicultural_Registrar");
                            pa.registerStoredProcedureParameter("idActividadSilviculturalDet", Integer.class,
                                    ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idTipoTratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tipoTratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("descripcionDetalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("tratamiento", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("equipo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("insumo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("personal", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("observacionDetalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idActSilvicultural", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                            setStoreProcedureEnableNullParameters(pa);
                            pa.setParameter("idActividadSilviculturalDet", param.getIdActividadSilviculturalDet());
                            pa.setParameter("idTipo", param.getIdTipo());
                            pa.setParameter("idTipoTratamiento", param.getIdTipoTratamiento());
                            pa.setParameter("tipoTratamiento", param.getTipoTratamiento());
                            pa.setParameter("accion", param.getAccion());
                            pa.setParameter("descripcionDetalle", param.getDescripcionDetalle());
                            pa.setParameter("tratamiento", param.getTratamiento());
                            pa.setParameter("justificacion", param.getJustificacion());
                            pa.setParameter("actividad", param.getActividad());
                            pa.setParameter("equipo", param.getEquipo());
                            pa.setParameter("insumo", param.getInsumo());
                            pa.setParameter("personal", param.getPersonal());
                            pa.setParameter("observacionDetalle", param.getObservacionDetalle());
                            pa.setParameter("detalle", param.getDetalle());
                            pa.setParameter("idActSilvicultural", obj_.getIdActSilvicultural());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                ActividadSilviculturalDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new ActividadSilviculturalDetalleEntity();
                                    obj.setIdActividadSilviculturalDet((Integer) row[0]);
                                    obj.setIdTipo((String) row[1]);
                                    obj.setIdTipoTratamiento((String) row[2]);
                                    obj.setTipoTratamiento((String) row[3]);
                                    obj.setAccion((boolean) row[4]);
                                    obj.setDescripcionDetalle((String) row[5]);
                                    obj.setTratamiento((String) row[6]);
                                    obj.setJustificacion((String) row[7]);
                                    obj.setActividad((String) row[8]);
                                    obj.setEquipo((String) row[9]);
                                    obj.setInsumo((String) row[10]);
                                    obj.setPersonal((String) row[11]);
                                    obj.setObservacionDetalle((String) row[12]);
                                    obj.setIdActSilvicultural((Integer) row[13]);
                                    obj.setDetalle((String) row[14]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListActividadSilvicultural(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la actividad silvicultural correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    private ActividadSilviculturalDto setResultData(List<Object[]> dataDb) throws Exception {
        ActividadSilviculturalDto data = null;
        if (dataDb != null && !dataDb.isEmpty()) {
            data = new ActividadSilviculturalDto();
            Object[] item = dataDb.get(0);
            data.setIdPlanManejo((Integer) item[0]);
            data.setIdActividadSilvicultural((Integer) item[1]);
            data.setCodActividad((String) item[2]);
            data.setActividad((String) item[3]);
            data.setMonitoreo(String.valueOf((Character) item[4]));
            data.setAnexo(String.valueOf((Character) item[5]));
            data.setDescripcion((String) item[6]);
            data.setObservacion((String) item[7]);
            ActividadSilviculturalDetalleEntity detalle = null;
            for (Object[] row : dataDb) {
                detalle = new ActividadSilviculturalDetalleEntity();
                detalle.setIdActividadSilviculturalDet((Integer) row[8]);
                detalle.setIdTipo((String) row[9]);
                detalle.setIdTipoTratamiento((String) row[10]);
                detalle.setTipoTratamiento((String) row[11]);
                detalle.setAccion(row[12] == null ? false : (boolean) row[12]);
                detalle.setDescripcionDetalle((String) row[13]);
                detalle.setTratamiento((String) row[14]);
                detalle.setJustificacion((String) row[15]);
                detalle.setActividad((String) row[16]);
                detalle.setEquipo((String) row[17]);
                detalle.setInsumo((String) row[18]);
                detalle.setPersonal((String) row[19]);
                detalle.setObservacionDetalle((String) row[20]);
                if(row.length >22) detalle.setDetalle((String) row[22]);
                data.getDetalle().add(detalle);
            }
        }
        return data;
    }

    @Override
    public ResultClassEntity ConfigurarActividadSilviculturalPgmf(ActividadSilviculturalEntity actividadSilvicultural)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em
                    .createStoredProcedureQuery("dbo.pa_ActividadSilvicultural_ConfigurarPgmf");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", actividadSilvicultural.getIdPlanManejo());
            processStored.setParameter("idTipo", actividadSilvicultural.getIdTipoTratamiento());
            processStored.setParameter("idUsuarioRegistro", actividadSilvicultural.getIdUsuarioRegistro());
            processStored.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
        result.setSuccess(true);
        result.setMessage("Se registró configuracion actividad silvicultural.");
        return result;
    }

    @Override
    public ResultClassEntity ActualizarActividadSilviculturalPgmf(
            ActividadSilviculturalDetallePgmfDto actividadSilviculturalActualizarDto) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em
                    .createStoredProcedureQuery("dbo.pa_ActividadSilvicultural_ActualizarPgmf");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idActividadSilviculturalDet", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("accion", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionDetalle", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tratamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("justificacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idTipoTratamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoTratamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", actividadSilviculturalActualizarDto.getIdPlanManejo());
            processStored.setParameter("idUsuarioModificacion",
                    actividadSilviculturalActualizarDto.getIdUsuarioModificacion());
            processStored.setParameter("idActividadSilviculturalDet",
                    actividadSilviculturalActualizarDto.getIdActividadSilviculturalDet());
            processStored.setParameter("accion", actividadSilviculturalActualizarDto.getAccion());
            processStored.setParameter("descripcionDetalle",
                    actividadSilviculturalActualizarDto.getDescripcionDetalle());
            processStored.setParameter("tratamiento", actividadSilviculturalActualizarDto.getTratamiento());
            processStored.setParameter("justificacion", actividadSilviculturalActualizarDto.getJustificacion());
            processStored.setParameter("idTipo", actividadSilviculturalActualizarDto.getIdTipo());
            processStored.setParameter("idTipoTratamiento", actividadSilviculturalActualizarDto.getIdTipoTratamiento());
            processStored.setParameter("tipoTratamiento", actividadSilviculturalActualizarDto.getTipoTratamiento());
            processStored.setParameter("estado", actividadSilviculturalActualizarDto.getEstado());

            processStored.execute();

            result.setSuccess(true);

            if (actividadSilviculturalActualizarDto.getIdTipo() == "1") {
                result.setMessage("Se registró los Tratamientos Silvicultural correctamente.");
            } else {
                result.setMessage("Se registró la Reforestación correctamente.");
            }

            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public ResultClassEntity<ActividadSilviculturalDto> ListarActividadSilviculturalDetallePgmf(Integer idPlanManejo,
            Integer intIdTipo) throws Exception {
        ResultClassEntity<ActividadSilviculturalDto> result = new ResultClassEntity<>();
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_ActividadSilvicultural_ListarPgmf");
            sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            sp.registerStoredProcedureParameter("idTipo", Integer.class, ParameterMode.IN);
            sp.setParameter("idPlanManejo", idPlanManejo);
            sp.setParameter("idTipo", intIdTipo);
            sp.execute();
            ActividadSilviculturalDto data = setResultData(sp.getResultList());

            result.setData(data);
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return result;
        }
    }

    @Override
    public ResultClassEntity EliminarActividadSilviculturalPgmf(
            ActividadSilviculturalDetalleEntity actividadSilviculturalDetalleEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em
                    .createStoredProcedureQuery("dbo.pa_ActividadSilvicultural_EliminarPgmf");
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idActividadSilviculturalDet", Integer.class,
                    ParameterMode.IN);
            processStored.setParameter("idUsuarioElimina", actividadSilviculturalDetalleEntity.getIdUsuarioElimina());
            processStored.setParameter("idActividadSilviculturalDet",
                    actividadSilviculturalDetalleEntity.getIdActividadSilviculturalDet());
            processStored.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity EliminarActividadSilviculturalDema(
            ActividadSilviculturalDetalleEntity actividadSilviculturalDetalleEntity) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em
                    .createStoredProcedureQuery("dbo.pa_ActividadSilvicultural_EliminarDema");
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idActividadSilviculturalDet", Integer.class,
                    ParameterMode.IN);
            processStored.setParameter("idUsuarioElimina", actividadSilviculturalDetalleEntity.getIdUsuarioElimina());
            processStored.setParameter("idActividadSilviculturalDet",
                    actividadSilviculturalDetalleEntity.getIdActividadSilviculturalDet());
            processStored.execute();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }
    }
}
