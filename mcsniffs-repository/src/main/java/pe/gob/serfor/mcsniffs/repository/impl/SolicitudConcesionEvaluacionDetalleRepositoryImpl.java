package pe.gob.serfor.mcsniffs.repository.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.ArchivoRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudConcesionEvaluacionDetalleRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

@Repository
public class SolicitudConcesionEvaluacionDetalleRepositoryImpl extends JdbcDaoSupport
        implements SolicitudConcesionEvaluacionDetalleRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ArchivoRepository repositoryArchivo;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionEvaluacionDetalleRepositoryImpl.class);

    @Override
    public ResultClassEntity registrarSolicitudConcesionEvaluacionDetalle(
            List<SolicitudConcesionEvaluacionDetalleDto> lsobj) {
        ResultClassEntity result = new ResultClassEntity();
        try {

            for (SolicitudConcesionEvaluacionDetalleDto obj : lsobj) {
                StoredProcedureQuery processStored = entityManager
                        .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionEvaluacionDetalle_Registrar");

                processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                        ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("lineamiento", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoLineamiento", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("codigoMotivDenegacion", String.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacionDetalle", Integer.class,
                        ParameterMode.INOUT);

                SpUtil.enableNullParams(processStored);

                processStored.setParameter("idSolicitudConcesionEvaluacion", obj.getIdSolicitudConcesionEvaluacion());
                processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
                processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
                processStored.setParameter("lineamiento", obj.getLineamiento());
                processStored.setParameter("codigoLineamiento", obj.getCodigoLineamiento());
                processStored.setParameter("conforme", obj.getConforme());
                processStored.setParameter("observacion", obj.getObservacion());
                processStored.setParameter("codigoMotivDenegacion", obj.getCodigoMotivDenegacion());
                processStored.setParameter("idUsuarioRegistro", obj.getIdUsuarioRegistro());

                processStored.execute();
            }

            // Integer idSolicitudConcesionEvaluacionDetalle = (Integer)
            // processStored.getOutputParameterValue("idSolicitudConcesionEvaluacionDetalle");
            // obj.setIdSolicitudConcesionEvaluacionDetalle(idSolicitudConcesionEvaluacionDetalle);
            // result.setCodigo(idSolicitudConcesionEvaluacionDetalle);
            result.setSuccess(true);
            // result.setData(obj);
            result.setMessage("Se registró la evaluación de la solicitud de concesión correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error. No se pudo registrar la evaluación de la  solicitud de concesión.");
            result.setInnerException(e.getMessage());
            return result;
        }
    }

    @Override
    public ResultClassEntity actualizarSolicitudConcesionEvaluacionDetalle(SolicitudConcesionEvaluacionDetalleDto obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager
                    .createStoredProcedureQuery("Concesion.pa_SolicitudConcesionEvaluacionDetalle_Actualizar");
            processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacionDetalle", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idSolicitudConcesionEvaluacion", Integer.class,
                    ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoSubSeccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("lineamiento", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoLineamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("conforme", Boolean.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codigoMotivDenegacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioModificacion", Integer.class, ParameterMode.IN);

            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idSolicitudConcesionEvaluacionDetalle",
                    obj.getIdSolicitudConcesionEvaluacionDetalle());
            processStored.setParameter("idSolicitudConcesionEvaluacion", obj.getIdSolicitudConcesionEvaluacion());
            processStored.setParameter("codigoSeccion", obj.getCodigoSeccion());
            processStored.setParameter("codigoSubSeccion", obj.getCodigoSubSeccion());
            processStored.setParameter("lineamiento", obj.getLineamiento());
            processStored.setParameter("codigoLineamiento", obj.getCodigoLineamiento());
            processStored.setParameter("conforme", obj.getConforme());
            processStored.setParameter("codigoMotivDenegacion", obj.getCodigoMotivDenegacion());
            processStored.setParameter("observacion", obj.getObservacion());
            processStored.setParameter("idUsuarioModificacion", obj.getIdUsuarioModificacion());

            processStored.execute();
            result.setSuccess(true);
            result.setMessage("Se actualizó Solicitud Concesión Evaluación Detalle.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

}
