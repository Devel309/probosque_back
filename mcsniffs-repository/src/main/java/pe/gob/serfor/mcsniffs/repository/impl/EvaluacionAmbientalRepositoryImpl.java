package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionAmbientalRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar y obtener una Evaluacion Ambiental}
 *
 */

@Repository
public class EvaluacionAmbientalRepositoryImpl extends JdbcDaoSupport implements EvaluacionAmbientalRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager.getLogger(pe.gob.serfor.mcsniffs.repository.impl.EvaluacionAmbientalRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbiental(EvaluacionAmbientalEntity param) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ActualizarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity EliminarEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ObtenerEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbientalEntity) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbientalDetalle(List<EvaluacionAmbientalDetalleEntity> evaluacionAmbientalDetalleList) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ActualizarEvaluacionAmbientalDetalle(List<EvaluacionAmbientalDetalleEntity> evaluacionAmbientalDetalleList) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity EliminarEvaluacionAmbientalDetalle(EvaluacionAmbientalDetalleEntity evaluacionAmbientalDetalleEntity) throws Exception {
        return null;
    }

    @Override
    public ResultClassEntity ObtenerEvaluacionAmbientalDetalle(EvaluacionAmbientalDetalleEntity evaluacionAmbientalDetalleEntity) throws Exception {
        return null;
    }

    @Override
    public List<EvaluacionAmbientalAprovechamientoEntity> ListarEvaluacionAprovechamientoFiltro(EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalAprovechamiento_ListarFiltro");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoAprovechamiento", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("tipoAprovechamiento", param.getTipoAprovechamiento());
            processStored.execute();
            List<EvaluacionAmbientalAprovechamientoEntity> result = new ArrayList<EvaluacionAmbientalAprovechamientoEntity>();
            List<Object[]> spResult = processStored.getResultList();
            if (!spResult.isEmpty()){
                for (Object[] row: spResult){
                    EvaluacionAmbientalAprovechamientoEntity temp = new EvaluacionAmbientalAprovechamientoEntity();
                    temp.setIdEvalAprovechamiento((Integer) row[0]);
                    temp.setCodTipoAprovechamiento((String) row[1]);
                    temp.setTipoAprovechamiento((String) row[2]);
                    temp.setTipoNombreAprovechamiento((String) row[3]);
                    temp.setNombreAprovechamiento((String) row[4]);
                    temp.setImpacto((String) row[5]);
                    temp.setMedidasControl((String) row[6]);
                    temp.setMedidasMonitoreo((String) row[7]);
                    temp.setFrecuencia((String) row[8]);
                    temp.setAcciones((String) row[9]);
                    temp.setResponsable((String) row[10]);
                    temp.setResponsableSecundario((String) row[11]);
                    temp.setContingencia((String) row[12]);
                    temp.setAuxiliar((String) row[13]);
                    temp.setIdPlanManejo((Integer) row[14]);
                    temp.setSubCodTipoAprovechamiento((String) row[15]);
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity RegistrarAprovechamientoEvaluacionAmbiental(EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalAprovechamientoList_Registrar");
            processStored.registerStoredProcedureParameter("idEvalAprovechamiento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodTipoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoNombreAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("impacto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasControl", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasMonitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("acciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsableSecundario", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("contingencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("auxiliar", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvalAprovechamiento", param.getIdEvalAprovechamiento());
            processStored.setParameter("codTipoAprovechamiento", param.getCodTipoAprovechamiento());
            processStored.setParameter("subCodTipoAprovechamiento", param.getSubCodTipoAprovechamiento());
            processStored.setParameter("tipoAprovechamiento", param.getTipoAprovechamiento());
            processStored.setParameter("tipoNombreAprovechamiento", param.getTipoNombreAprovechamiento());
            processStored.setParameter("nombreAprovechamiento", param.getNombreAprovechamiento());
            processStored.setParameter("impacto", param.getImpacto());
            processStored.setParameter("medidasControl", param.getMedidasControl());
            processStored.setParameter("medidasMonitoreo", param.getMedidasMonitoreo());
            processStored.setParameter("frecuencia", param.getFrecuencia());
            processStored.setParameter("acciones", param.getAcciones());
            processStored.setParameter("responsable", param.getResponsable());
            processStored.setParameter("responsableSecundario", param.getResponsableSecundario());
            processStored.setParameter("contingencia", param.getContingencia());
            processStored.setParameter("auxiliar", param.getAuxiliar());
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
            processStored.execute();
            //Object[] objArr = (Object[]) processStored.getSingleResult();
            //param.setIdEvalAprovechamiento((Integer) objArr[0]);
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró el aprovechamiento para la evaluación ambiental correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }


    @Override
    public List<EvaluacionAmbientalActividadEntity> ListarEvaluacionActividadFiltro(EvaluacionAmbientalActividadEntity param) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalActividad_ListarFiltro");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class,ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoActividad", String.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("tipoActividad", param.getTipoActividad());
            processStored.execute();
            List<EvaluacionAmbientalActividadEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                for (Object[] row: spResult){
                    EvaluacionAmbientalActividadEntity temp = new EvaluacionAmbientalActividadEntity();
                    temp.setIdEvalActividad((Integer) row[0]);
                    temp.setCodTipoActividad((String) row[1]);
                    temp.setTipoActividad((String) row[2]);
                    temp.setTipoNombreActividad((String) row[3]);
                    temp.setNombreActividad((String) row[4]);
                    temp.setMedidasControl((String) row[5]);
                    temp.setMedidasMonitoreo((String) row[6]);
                    temp.setFrecuencia((String) row[7]);
                    temp.setAcciones((String) row[8]);
                    temp.setResponsable((String) row[9]);
                    temp.setIdPlanManejo((Integer) row[10]);
                    temp.setSubCodTipoActividad((String) row[11]);
                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity RegistrarActividadEvaluacionAmbiental(EvaluacionAmbientalActividadEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalActividadList_Registrar");
            processStored.registerStoredProcedureParameter("idEvalActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodTipoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoNombreActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasControl", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasMonitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("acciones", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvalActividad", param.getIdEvalActividad());
            processStored.setParameter("codTipoActividad", param.getCodTipoActividad());
            processStored.setParameter("subCodTipoActividad", param.getSubCodTipoActividad());
            processStored.setParameter("tipoActividad", param.getTipoActividad());
            processStored.setParameter("tipoNombreActividad", param.getTipoNombreActividad());
            processStored.setParameter("nombreActividad", param.getNombreActividad());
            processStored.setParameter("medidasControl", param.getMedidasControl()==null? "": param.getMedidasControl());
            processStored.setParameter("medidasMonitoreo", param.getMedidasMonitoreo()==null?"":param.getMedidasMonitoreo());
            processStored.setParameter("frecuencia", param.getFrecuencia()==null?"":param.getFrecuencia());
            processStored.setParameter("acciones", param.getAcciones()==null?"":param.getAcciones());
            processStored.setParameter("responsable", param.getResponsable()==null?"":param.getResponsable());
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro()==null?0:param.getIdUsuarioRegistro());
            processStored.execute();
            Object[] objArr = (Object[]) processStored.getSingleResult();
            param.setIdEvalActividad((Integer) objArr[0]);
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se registró la actividad para la evaluacion ambiental correctamente.");
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return  result;
        }
    }

    @Override
    public ResultClassEntity<List<EvaluacionAmbientalEntity>> RegistrarEvaluacionAmbientalDetalleSub(
            List<EvaluacionAmbientalEntity> list) throws Exception {
        ResultClassEntity<List<EvaluacionAmbientalEntity>> result = new ResultClassEntity<>();
        for (EvaluacionAmbientalEntity item : list) {
            try {
                if (item.getIdEvalAprovechamiento() == 0 || item.getIdEvalAprovechamiento() == null ) {
                    StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalActividadList_Registrar");
                    processStored.registerStoredProcedureParameter("idEvalActividad", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("codTipoActividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("subCodTipoActividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tipoActividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("tipoNombreActividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("nombreActividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("medidasControl", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("medidasMonitoreo", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("frecuencia", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("acciones", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("responsable", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored);
                    processStored.setParameter("idEvalActividad", item.getIdEvalActividad());
                    processStored.setParameter("codTipoActividad", "");
                    processStored.setParameter("subCodTipoActividad", "");
                    processStored.setParameter("tipoActividad", "");
                    processStored.setParameter("tipoNombreActividad", "");
                    processStored.setParameter("nombreActividad", "");
                    processStored.setParameter("medidasControl", "");
                    processStored.setParameter("medidasMonitoreo", item.getMedidasMitigacion());
                    processStored.setParameter("frecuencia", "");
                    processStored.setParameter("acciones", "");
                    processStored.setParameter("responsable", "");
                    processStored.setParameter("idPlanManejo", item.getIdPlanManejo());
                    processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
                    processStored.execute();
                    //Object[] objArr = (Object[]) processStored.getSingleResult();
                    //item.setIdEvalActividad((Integer) objArr[0]);
                } else {
                    StoredProcedureQuery processStored = entityManager
                            .createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalList_Registrar");
                    processStored.registerStoredProcedureParameter("idEvaluacionAmbiental", Integer.class,
                            ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idEvalAprovechamiento", Integer.class,
                            ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idEvalActividad", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("codTipoEvaluacionAmbiental", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("subCodTipoEvaluacionAmbiental", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("codTipoEvaluacionAmbientalDet", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("valorEvaluacion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("medidasMitigacion", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("adjunto", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("nombreAprovechamiento", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("nombreActividad", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("caracterizacionImpacto", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("medidasMonitoreo", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("medidasControl", String.class, ParameterMode.IN);
                    processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
                    SpUtil.enableNullParams(processStored);
                    processStored.setParameter("idEvaluacionAmbiental", item.getIdEvaluacionAmbiental());
                    processStored.setParameter("idEvalAprovechamiento", item.getIdEvalAprovechamiento());
                    processStored.setParameter("idEvalActividad", item.getIdEvalActividad());
                    processStored.setParameter("idPlanManejo", item.getIdPlanManejo());
                    processStored.setParameter("codTipoEvaluacionAmbiental", item.getCodTipoEvaluacionAmbiental());
                    processStored.setParameter("subCodTipoEvaluacionAmbiental", item.getSubCodTipoEvaluacionAmbiental());
                    processStored.setParameter("codTipoEvaluacionAmbientalDet", item.getCodTipoEvaluacionAmbientalDet());
                    processStored.setParameter("valorEvaluacion", item.getValorEvaluacion());
                    processStored.setParameter("descripcion", item.getDescripcion());
                    processStored.setParameter("medidasMitigacion", item.getMedidasMitigacion());
                    processStored.setParameter("adjunto", item.getAdjunto());
                    processStored.setParameter("nombreAprovechamiento", item.getNombreAprovechamiento());
                    processStored.setParameter("nombreActividad", item.getNombreActividad());
                    processStored.setParameter("caracterizacionImpacto", item.getCaracterizacionImpacto());
                    processStored.setParameter("medidasMonitoreo", item.getMedidasMonitoreo());
                    processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
                    processStored.setParameter("medidasControl", item.getAuxiliar());
                    processStored.setParameter("actividad", item.getResponsableSecundario());
                    processStored.execute();
                    //Object[] objArr = (Object[]) processStored.getSingleResult();
                    //item.setIdEvaluacionAmbiental((Integer) objArr[0]);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                result.setSuccess(false);
                result.setMessage("Ocurrió un error.");
                return result;
            }
        }
        result.setData(list);
        result.setSuccess(true);
        result.setMessage("Se registró la evaluación ambiental correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity RegistrarEvaluacionAmbientalDetalleSubXls(EvaluacionAmbientalEntity item) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalList_Registrar");
            processStored.registerStoredProcedureParameter("idEvaluacionAmbiental", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvalAprovechamiento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idEvalActividad", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoEvaluacionAmbiental", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("subCodTipoEvaluacionAmbiental", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codTipoEvaluacionAmbientalDet", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("valorEvaluacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasMitigacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("adjunto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreAprovechamiento", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("nombreActividad", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("caracterizacionImpacto", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasMonitoreo", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("medidasControl", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("actividad", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("idEvaluacionAmbiental", item.getIdEvaluacionAmbiental());
            processStored.setParameter("idEvalAprovechamiento", item.getIdEvalAprovechamiento());
            processStored.setParameter("idEvalActividad", item.getIdEvalActividad());
            processStored.setParameter("idPlanManejo", item.getIdPlanManejo());
            processStored.setParameter("codTipoEvaluacionAmbiental", item.getCodTipoEvaluacionAmbiental());
            processStored.setParameter("subCodTipoEvaluacionAmbiental", item.getSubCodTipoEvaluacionAmbiental());
            processStored.setParameter("codTipoEvaluacionAmbientalDet", item.getCodTipoEvaluacionAmbientalDet());
            processStored.setParameter("valorEvaluacion", item.getValorEvaluacion());
            processStored.setParameter("descripcion", item.getDescripcion());
            processStored.setParameter("medidasMitigacion", item.getMedidasMitigacion());
            processStored.setParameter("adjunto", item.getAdjunto());
            processStored.setParameter("nombreAprovechamiento", item.getNombreAprovechamiento());
            processStored.setParameter("nombreActividad", item.getNombreActividad());
            processStored.setParameter("caracterizacionImpacto", item.getCaracterizacionImpacto());
            processStored.setParameter("medidasMonitoreo", item.getMedidasMonitoreo());
            processStored.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());
            processStored.setParameter("medidasControl", item.getAuxiliar());
            processStored.setParameter("actividad", item.getResponsableSecundario());
            processStored.execute();
            //Object[] objArr = (Object[]) processStored.getSingleResult();
            //item.setIdEvaluacionAmbiental((Integer) objArr[0]);
            result.setData(item);
            result.setSuccess(true);
            result.setMessage("Se registró la evaluación ambiental correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }

    @Override
    public List<EvaluacionAmbientalDto> ListarEvaluacionAmbiental(EvaluacionAmbientalDto param) throws Exception {
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbiental_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
            processStored.execute();
            List<EvaluacionAmbientalDto> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (!spResult.isEmpty()){
                for (Object[] row: spResult){
                    EvaluacionAmbientalDto temp = new EvaluacionAmbientalDto();
                    temp.setIdEvaluacionAmbiental((Integer) row[0]);
                    temp.setValorEvaluacion(String.valueOf((Character) row[1]));
                    temp.setDescripcion((String) row[2]);
                    temp.setMedidasMitigacion((String) row[3]);
                    temp.setAdjunto(String.valueOf((Character) row[4]));
                    temp.setIdPlanManejo((Integer) row[5]);
                    temp.setIdEvalActividad((Integer) row[6]);
                    temp.setCodTipoActividad((String) row[7]);
                    temp.setTipoActividad((String) row[8]);
                    temp.setTipoNombreActividad((String) row[9]);
                    temp.setNombreActividad((String) row[10]);
                    temp.setMedidasControl((String) row[11]);
                    temp.setMedidasMonitoreo((String) row[12]);
                    temp.setFrecuencia((String) row[13]);
                    temp.setAcciones((String) row[14]);
                    temp.setResponsable((String) row[15]);
                    temp.setIdEvalAprovechamiento((Integer) row[16]);
                    temp.setCodTipoAprovechamiento((String) row[17]);
                    temp.setTipoAprovechamiento((String) row[18]);
                    temp.setTipoNombreAprovechamiento((String) row[19]);
                    temp.setNombreAprovechamiento((String) row[20]);
                    temp.setCodTipoEvaluacionAmbiental((String) row[21]);
                    temp.setSubCodTipoEvaluacionAmbiental((String) row[22]);
                    temp.setCodTipoEvaluacionAmbientalDet((String) row[23]);
                    temp.setSubCodTipoActividad((String) row[24]);
                    temp.setSubCodTipoAprovechamiento((String) row[25]);

                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }

    @Override
    public ResultClassEntity EvaluacionAmbientalAprovechamiento_Eliminar(EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalAprovechamiento_Eliminar");
            processStored.registerStoredProcedureParameter("idEvalAprovechamiento", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
            processStored.setParameter("idEvalAprovechamiento", param.getIdEvalAprovechamiento());
            processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
            processStored.execute();
            result.setData(param);
            result.setSuccess(true);
            result.setMessage("Se eliminó correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return  null;
        }
    }


    @Override
    public ResultClassEntity eliminarEvaluacionAmbientalActividad(EvaluacionAmbientalActividadEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalActividadList_Eliminar");
        processStored.registerStoredProcedureParameter("idEvaluacionAmbientalActividad", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idEvaluacionAmbientalActividad", param.getIdEvalActividad());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity eliminarEvaluacionAmbientalAprovechamientoList(EvaluacionAmbientalAprovechamientoEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalAprovechamientoList_Eliminar");
        processStored.registerStoredProcedureParameter("idEvaluacionAmbientalAprovechamiento", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idEvaluacionAmbientalAprovechamiento", param.getIdEvalAprovechamiento());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

    @Override
    public ResultClassEntity eliminarEvaluacionAmbiental(EvaluacionAmbientalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_EvaluacionAmbientalList_Eliminar");
        processStored.registerStoredProcedureParameter("idEvaluacionAmbiental", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idEvaluacionAmbiental", param.getIdEvaluacionAmbiental());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }

}
