package pe.gob.serfor.mcsniffs.repository;

import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudEntity;

public interface AutorizacionPublicacionSolicitudRepository {
    ResultClassEntity guardarAutorizacionPublicacionSolicitud(AutorizacionPublicacionSolicitudEntity p);
    ResultArchivoEntity descargarPlantillaCartaAutorizacion();

}
