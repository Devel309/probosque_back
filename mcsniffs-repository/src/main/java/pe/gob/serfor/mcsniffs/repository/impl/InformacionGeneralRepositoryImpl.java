package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Repositorio registrar y obtener un Ordenamiento Proteccion}
 *
 */
@Repository
public class InformacionGeneralRepositoryImpl extends JdbcDaoSupport implements InformacionGeneralRepository {

	@Autowired
	@Qualifier("dataSourceBDMCSNIFFS")
	DataSource dataSource;

	private static final Logger log = LogManager
			.getLogger(pe.gob.serfor.mcsniffs.repository.impl.InformacionGeneralRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public ResultClassEntity RegistrarInformacionGeneral(InformacionGeneralEntity param) throws Exception {
		ResultClassEntity result = new ResultClassEntity();
		try {
			StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_InformacionGeneral_Registrar");
			processStored.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("duracion", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("potencial", Double.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("estado", String.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
			processStored.setParameter("fechaPresentacion", param.getFechaPresentacion());
			processStored.setParameter("duracion", param.getDuracion());
			processStored.setParameter("fechaInicio", param.getFechaInicio());
			processStored.setParameter("fechaFin", param.getFechaFin());
			processStored.setParameter("potencial", param.getPotencial());
			processStored.setParameter("estado", param.getEstado());
			processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
			processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
			List<Object[]> spResult = processStored.getResultList();
			if (spResult.size() >= 1) {
				for (Object[] row : spResult) {
					param.setIdInfGeneral((Integer) row[0]);
				}
			}

			result.setData(param);
			result.setSuccess(true);
			result.setMessage("Se registró la Información General del PO Anterior.");
			return result;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result.setSuccess(false);
			result.setMessage("Ocurrió un error.");
			return result;
		}
	}

	@Override
	public ResultClassEntity ActualizarInformacionGeneral(InformacionGeneralEntity param) throws Exception {
		ResultClassEntity result = new ResultClassEntity();
		try {
			StoredProcedureQuery processStored = em
					.createStoredProcedureQuery("dbo.pa_InformacionGeneralPGMF_Registrar");
			processStored.registerStoredProcedureParameter("idInfGeneral", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("idPersona", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("fechaPresentacionPGMF", Date.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("duracion", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("areaConcesion", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("areaBosqueHa", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("areaProteccion", Integer.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("potencialMaderable", Double.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("volumenCort", Double.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
			processStored.setParameter("idInfGeneral", param.getIdInfGeneral());
			processStored.setParameter("idPersona", param.getPersona().getIdPersona());
			processStored.setParameter("idPlanManejo", param.getPlanManejo().getIdPlanManejo());
			processStored.setParameter("fechaPresentacionPGMF", param.getFechaPresentacion());
			processStored.setParameter("duracion", param.getDuracion());
			processStored.setParameter("areaConcesion", param.getAreaTotal());
			processStored.setParameter("fechaInicio", param.getFechaInicio());
			processStored.setParameter("fechaFin", param.getFechaFin());
			processStored.setParameter("areaBosqueHa", param.getAreaBosque());
			processStored.setParameter("areaProteccion", param.getAreaProteccion());
			processStored.setParameter("potencialMaderable", param.getPotencial());
			processStored.setParameter("volumenCort", param.getVolumen());
			processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
			result.setData(param);
			result.setSuccess(true);
			result.setMessage("Se registró la Información General.");
			return result;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result.setSuccess(false);
			result.setMessage("Ocurrió un error.");
			return result;
		}
	}

	@Override
	public ResultClassEntity EliminarInformacionGeneral(InformacionGeneralEntity informacionGeneralEntity)
			throws Exception {
		return null;
	}

	@Override
	public ResultClassEntity<InformacionGeneralDto> obtener(Integer idPlanManejo) throws Exception {
		ResultClassEntity<InformacionGeneralDto> result = new ResultClassEntity<>();
		try {

		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_InformacionGeneral_Obtener_PGMF");
		sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
		sp.setParameter("idPlanManejo", idPlanManejo);
		sp.execute();


		InformacionGeneralDto data = setResultDataObtener((Object[]) sp.getSingleResult());
		data.setIdPlanManejo(idPlanManejo);
		data.setArchivos(listarArchivos(idPlanManejo));

		result.setData(data);
		result.setSuccess(true);
		return result;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result.setSuccess(false);
			result.setMessage("Ocurrió un error.");
			return result;
		}

	}

	private InformacionGeneralDto setResultDataObtener(Object[] row) throws Exception {
		InformacionGeneralDto data = new InformacionGeneralDto();

		data.setIdContrato((Integer) row[0]);
		data.setTipoPersonaTitularTH((String) row[1]);
		data.setTipoPersonaTitularTHDesc((String) row[2]);
		data.setTipoDocumentoTitularTH((String) row[3]);
		data.setTipoDocumentoTitularTHDesc((String) row[4]);
		data.setNumeroDocumentoTitularTH((String) row[5]);
		data.setNombreTitularTH((String) row[6]);
		data.setApellidoPaternoTitularTH((String) row[7]);
		data.setApellidoMaternoTitularTH((String) row[8]);
		data.setRazonSocialTitularTH((String) row[9]);
		data.setEmailTitularTH((String) row[10]);
		data.setDireccionTitularTH((String) row[11]);
		data.setNombreDepartamentoContrato((String) row[12]);
		data.setNombreProvinciaContrato((String) row[13]);
		data.setNombreDistritoContrato((String) row[14]);


		data.setTipoPersonaRepLegal((String) row[15]);
		data.setTipoPersonaRepLegalDesc((String) row[16]);
		data.setTipoDocumentoRepLegal((String) row[17]);
		data.setTipoDocumentoRepLegalDesc((String) row[18]);
		data.setNumeroDocumentoRepLegal((String) row[19]);
		data.setNombreRepresentanteLegal((String) row[20]);
		data.setApellidoPaternoRepLegal((String) row[21]);
		data.setApellidoMaternoRepLegal((String) row[22]);
		data.setEmailRepLegal((String) row[23]);

		data.setDistrito((String) row[24]);
		data.setDomicilioLegal((String) row[25]);
		data.setNroContratoConcesion((String) row[26]);
		data.setProvincia(((Integer) row[27]).toString());
		data.setDepartamento(((Integer) row[28]).toString());
		data.setFechaPresentacion((Date) row[29]);
		data.setDuracion((Integer) row[30]);
		data.setFechaInicio((Date) row[31]);
		data.setFechaFin((Date) row[32]);

		data.setAreaTotalConcesion((BigDecimal)row[33]);
		data.setAreaBosqueProduccionForestal((BigDecimal) row[34]);
		data.setAreaProteccion((BigDecimal) row[35]);
		data.setNumeroBloquesQuinquenales((String) row[36]);
		data.setPotencialMaderable((BigDecimal) row[37]);
		data.setVolumenCortaAnualPermisible((BigDecimal) row[38]);
		data.setIdRegenteForestal((Integer) row[39]);
		data.setDomicilioLegalRegente((String) row[40]);
		data.setContratoSuscritoTitularTituloHabilitante((String) row[41]);
		data.setCertificadoHabilitacionProfesionalRegenteForestal((String) row[42]);
		data.setNumeroInscripcionRegistroRegente((String) row[43]);
		data.setIdInformacionGeneral((Integer) row[44]);
		data.setIdPersona(row[45]==null?null:(Integer) row[45]);
		data.setPrincipal(row[46]==null?null:(Boolean) row[46]);
		data.setNroContrato(row[47]==null?null:(String) row[47]);
		//data.setIdPersona(row[41]==null?null:(Integer) row[41]);


		return data;
	}

	private List<PGMFArchivoEntity> listarArchivos(Integer idPlanManejo) throws Exception {
		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PlanManejoArchivo_Listar");
		sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
		sp.setParameter("idPlanManejo", idPlanManejo);
		sp.execute();

		return setResultObtenerArchivos(sp.getResultList());
	}

	private List<PGMFArchivoEntity> setResultObtenerArchivos(List<Object[]> dataDb) throws Exception {

		List<PGMFArchivoEntity> archivos = new ArrayList<>();

		for (Object[] row : dataDb) {
			PGMFArchivoEntity item = new PGMFArchivoEntity();
			item.setIdPGMFArchivo((Integer) row[0]);
			item.setIdPlanManejo((Integer) row[1]);
			item.setIdArchivo((Integer) row[2]);
			item.setCodigoTipoPGMF((String) row[3]);
			item.setCodigoSubTipoPGMF((String) row[4]);
			item.setDescripcion((String) row[5]);
			item.setObservacion((String) row[6]);
			item.setNombre((String) row[7]);
			item.setExtension((String) row[8]);
			item.setTipoDocumento((String) row[9]);

			archivos.add(item);
		}
		return archivos;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResultClassEntity<InformacionGeneralDto> guardar(InformacionGeneralDto dto) throws Exception {
		ResultClassEntity<InformacionGeneralDto> result = new ResultClassEntity<>();
		try{
			dto.setIdInformacionGeneral(execGuardar(dto));

			for (PGMFArchivoEntity item : dto.getArchivos()) {
				Integer itemId = execGuardarArchivo(item);
				item.setIdPGMFArchivo(itemId);
			}


		if (dto.getArchivos()!=null){
			for (PGMFArchivoEntity item : dto.getArchivos()) {
				Integer itemId = execGuardarArchivo(item);
				item.setIdPGMFArchivo(itemId);
			}
		}
		
		result.setData(dto);
		result.setSuccess(true);
		return result;


		}
		catch(Exception e){
			result.setMessage(e.getMessage());
			result.setSuccess(true);
			return result;
		}

	}


	@Override
	public ResultEntity obtenerInformacionPlanOperativo(InformacionGeneralDEMAEntity param) {
		ResultEntity resul=new ResultEntity();
		try
		{
			StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_InformacionGneral_ObtenerPO");
			processStored.registerStoredProcedureParameter("nroDocumento", String.class, ParameterMode.IN);
			processStored.registerStoredProcedureParameter("codigoTipo", String.class, ParameterMode.IN);
			SpUtil.enableNullParams(processStored);
			processStored.setParameter("nroDocumento",param.getDniElaboraDema());
			processStored.setParameter("codigoTipo",param.getCodigoProceso());

			processStored.execute();
			List<InformacionGeneralDEMAEntity> objList=new ArrayList<>();
			List<Object[]> spResult = processStored.getResultList();
			if (spResult.size() >= 1){
				InformacionGeneralDEMAEntity objOrdPro = null;
				for (Object[] row: spResult){
					objOrdPro = new InformacionGeneralDEMAEntity();
					objOrdPro.setCantidadProceso(row[0]==null?null:(Integer) row[0]);
					objList.add(objOrdPro);
				}
			}
			resul.setData(objList);
			resul.setMessage((objList.size()!=0?"Información encontrada":"Informacion no encontrada"));
			resul.setIsSuccess(true);
			resul.setTotalrecord(objList.size());
		} catch (Exception e) {
			resul.setMessage(e.getMessage());
			resul.setIsSuccess(false);
		}
		return resul;
	}

	private Integer execGuardar(InformacionGeneralDto item) throws Exception {

		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_InformacionGeneral_Guardar");
		sp.registerStoredProcedureParameter("idInformacionGeneral", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("fechaPresentacion", Date.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("duracion", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("fechaInicio", Date.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("fechaFin", Date.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("areaTotalConcesion", BigDecimal.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("areaBosqueProduccionForestal", BigDecimal.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("areaProteccion", BigDecimal.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("potencialMaderable", BigDecimal.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("volumenCortaAnualPermisible", BigDecimal.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idRegenteForestal", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("domicilioLegalRegente", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("contratoSuscritoTitularTituloHabilitante", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("certificadoHabilitacionProfesionalRegenteForestal", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("numeroInscripcionRegistroRegente", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("bloquesQuinquenales", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

		sp.registerStoredProcedureParameter("tipoDocElaborador", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("dniElaborador", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("nombreElaborador", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("apellidoPaternoElaborador", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("apellidoMaternoElaborador", String.class, ParameterMode.IN);

		sp.registerStoredProcedureParameter("tipoDocRepresentante", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("docRepresentante", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("nombreRepresentante", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("apellidoPaternoRepresentante", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("apellidoMaternoRepresentante", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcionPredio", String.class, ParameterMode.IN);

		SpUtil.enableNullParams(sp);
		sp.setParameter("idInformacionGeneral", item.getIdInformacionGeneral());
		sp.setParameter("idPlanManejo", item.getIdPlanManejo());
		sp.setParameter("fechaPresentacion", item.getFechaPresentacion());
		sp.setParameter("duracion", item.getDuracion());
		sp.setParameter("fechaInicio", item.getFechaInicio());
		sp.setParameter("fechaFin", item.getFechaFin());
		sp.setParameter("areaTotalConcesion", item.getAreaTotalConcesion());
		sp.setParameter("areaBosqueProduccionForestal", item.getAreaBosqueProduccionForestal());
		sp.setParameter("areaProteccion", item.getAreaProteccion());
		sp.setParameter("potencialMaderable", item.getPotencialMaderable());
		sp.setParameter("volumenCortaAnualPermisible", item.getVolumenCortaAnualPermisible());
		sp.setParameter("idRegenteForestal", item.getIdRegenteForestal());
		sp.setParameter("domicilioLegalRegente", item.getDomicilioLegalRegente());
		sp.setParameter("contratoSuscritoTitularTituloHabilitante", item.getContratoSuscritoTitularTituloHabilitante());
		sp.setParameter("certificadoHabilitacionProfesionalRegenteForestal", item.getCertificadoHabilitacionProfesionalRegenteForestal());
		sp.setParameter("numeroInscripcionRegistroRegente", item.getNumeroInscripcionRegistroRegente());
		sp.setParameter("bloquesQuinquenales", item.getNumeroBloquesQuinquenales());
		sp.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());

		sp.setParameter("tipoDocElaborador", item.getTipoDocElaborador());
		sp.setParameter("dniElaborador", item.getDniElaborador());
		sp.setParameter("nombreElaborador", item.getNombreElaborador());
		sp.setParameter("apellidoPaternoElaborador", item.getApellidoPaternoElaborador());
		sp.setParameter("apellidoMaternoElaborador", item.getApellidoMaternoElaborador());

		sp.setParameter("tipoDocRepresentante", item.getTipoDocRepresentante());
		sp.setParameter("docRepresentante", item.getDocRepresentante());
		sp.setParameter("nombreRepresentante", item.getNombreRepresentante());
		sp.setParameter("apellidoPaternoRepresentante", item.getApellidoPaternoRepresentante());
		sp.setParameter("apellidoMaternoRepresentante", item.getApellidoMaternoRepresentante());
		sp.setParameter("descripcionPredio", item.getDescripcionPredio());

		sp.execute();
		return Integer.parseInt(sp.getSingleResult().toString());
	}

	private Integer execGuardarArchivo(PGMFArchivoEntity item) throws Exception {
		StoredProcedureQuery sp = em.createStoredProcedureQuery("pa_PGMFArchivo_Guardar");
		sp.registerStoredProcedureParameter("idPGMFArchivo", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoTipoPGMF", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("codigoSubTipoPGMF", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
		SpUtil.enableNullParams(sp);
		sp.setParameter("idPGMFArchivo", item.getIdPGMFArchivo());
		sp.setParameter("idPlanManejo", item.getIdPlanManejo());
		sp.setParameter("idArchivo", item.getIdArchivo());
		sp.setParameter("codigoTipoPGMF", item.getCodigoTipoPGMF());
		sp.setParameter("codigoSubTipoPGMF", item.getCodigoSubTipoPGMF());
		sp.setParameter("descripcion", item.getDescripcion());
		sp.setParameter("observacion", item.getObservacion());
		sp.setParameter("idUsuarioRegistro", item.getIdUsuarioRegistro());

		sp.execute();
		return Integer.parseInt(sp.getSingleResult().toString());
	}



	@Override
	public ResultClassEntity actualizarInfGeneralResumido(List<InfGeneralEntity> list) throws Exception {
		ResultClassEntity result = new ResultClassEntity();
		List<InfGeneralEntity> list_ = new ArrayList<>();
		try {
			for (InfGeneralEntity p : list) {
				StoredProcedureQuery sp = em.createStoredProcedureQuery("dbo.pa_InformacionGeneral_ActualizarResumido");
				sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
				sp.registerStoredProcedureParameter("codigo", String.class, ParameterMode.IN);
				sp.registerStoredProcedureParameter("idUsuarioRegistro", String.class, ParameterMode.IN);
				SpUtil.enableNullParams(sp);

				sp.setParameter("idPlanManejo", p.getIdPlanManejo());
				sp.setParameter("codigo", p.getCodigo());
				sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());

				sp.execute();
				List<Object[]> spResult_ = sp.getResultList();

				if (!spResult_.isEmpty()) {
					for (Object[] row_ : spResult_) {
						InfGeneralEntity obj_ = new InfGeneralEntity();
						obj_.setIdPlanManejo(row_[0]==null?null:(Integer) row_[0]);
						obj_.setCodigo(row_[1]==null?null:(String) row_[1]);

						list_.add(obj_);
					}
				}
			}
			result.setData(list_);
			result.setSuccess(true);
			result.setMessage("Se actualizó la información general correctamente.");
			return result;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result.setSuccess(false);
			result.setMessage("Ocurrió un error.");
			return result;
		}
	}


	@Override
	public List<InfGeneralEntity> listarInfGeneralResumido(InfGeneralEntity param) throws Exception {

		try {
			StoredProcedureQuery processStored = em.createStoredProcedureQuery("dbo.pa_InformacionGeneral_ListarResumido");
			processStored.registerStoredProcedureParameter("idPLanManejo", Integer.class, ParameterMode.IN);
			SpUtil.enableNullParams(processStored);
			processStored.setParameter("idPLanManejo", param.getIdPlanManejo());

			processStored.execute();
			List<InfGeneralEntity> result = new ArrayList<>();
			List<Object[]> spResult = processStored.getResultList();
			if (spResult.size() >= 1){
				InfGeneralEntity temp = null;
				for (Object[] row_: spResult){
					temp = new InfGeneralEntity();
					temp.setIdPlanManejo(row_[0]==null?null:(Integer) row_[0]);
					temp.setCodigo(row_[1]==null?null:(String) row_[1]);
					temp.setCodigoEstado(row_[2]==null?null:(String) row_[2]);
					result.add(temp);
				}
			}
			return  result;

		} catch (Exception e) {
			log.error("SolicitudSan", e.getMessage());
			throw e;
		}
	}



}
