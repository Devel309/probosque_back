package pe.gob.serfor.mcsniffs.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.ProteccionBosqueRepository;
import pe.gob.serfor.mcsniffs.repository.SolicitudSANRepository;
import pe.gob.serfor.mcsniffs.repository.util.LogAuditoria;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class SolicitudSANRepositoryImpl extends JdbcDaoSupport implements SolicitudSANRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    @Autowired
    LogAuditoriaRepository logAuditoriaRepository;

    private static final Logger log = LogManager.getLogger(SolicitudSANRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarSolicitudSAN(List<SolicitudSANEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<SolicitudSANEntity> list_ = new ArrayList<>();
        LogAuditoriaEntity logAuditoriaEntity;
        try {
            for (SolicitudSANEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudSAN_Registrar");
                sp.registerStoredProcedureParameter("idSolicitudSAN", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codSolicitudSAN", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("subCodSolicitudSAN", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("estadoSolicitudSAN", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("tipoPostulacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("procesoPostulacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("motivoSolicitud", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("asunto", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idArchivo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("detalle", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("observacion", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("diasAdicionales", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
                SpUtil.enableNullParams(sp);
                sp.setParameter("idSolicitudSAN", p.getIdSolicitudSAN());
                sp.setParameter("codSolicitudSAN", p.getCodSolicitudSAN());
                sp.setParameter("subCodSolicitudSAN", p.getSubCSolicitudSAN());
                sp.setParameter("estadoSolicitudSAN", p.getEstadoSolicitudSAN());
                sp.setParameter("nroGestion", p.getNroGestion());
                sp.setParameter("tipoPostulacion", p.getTipoPostulacion());
                sp.setParameter("procesoPostulacion", p.getProcesoPostulacion());
                sp.setParameter("motivoSolicitud", p.getMotivoSolicitud());
                sp.setParameter("asunto", p.getAsunto());
                sp.setParameter("idArchivo", p.getIdArchivo());
                sp.setParameter("descripcion", p.getDescripcion());
                sp.setParameter("detalle", p.getDetalle());
                sp.setParameter("observacion", p.getObservacion());
                sp.setParameter("diasAdicionales", p.getDiasAdicionales());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());

                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        SolicitudSANEntity obj_ = new SolicitudSANEntity();
                        obj_.setIdSolicitudSAN((Integer) row_[0]);
                        obj_.setCodSolicitudSAN((String) row_[1]);
                        obj_.setSubCSolicitudSAN((String) row_[2]);
                        obj_.setEstadoSolicitudSAN((String) row_[3]);
                        obj_.setNroGestion((Integer) row_[4]);
                        obj_.setTipoPostulacion((String) row_[5]);
                        obj_.setProcesoPostulacion((String) row_[6]);
                        obj_.setMotivoSolicitud((String) row_[7]);
                        obj_.setAsunto((String) row_[8]);
                        obj_.setDescripcion((String) row_[9]);
                        obj_.setDetalle((String) row_[10]);
                        obj_.setObservacion((String) row_[11]);
                        obj_.setIdArchivo((Integer) row_[12]);
                        list_.add(obj_);

                        if(p.getIdSolicitudSAN() == 0 || p.getIdSolicitudSAN() == null){

                            logAuditoriaEntity = new LogAuditoriaEntity(
                                    LogAuditoria.Table.T_MVC_SOLICITUDSAN,
                                    LogAuditoria.REGISTRAR,
                                    LogAuditoria.DescripcionAccion.Registrar.T_MVC_SOLICITUDSAN + (Integer) row_[0],
                                    p.getIdUsuarioRegistro());

                        }else {
                            logAuditoriaEntity = new LogAuditoriaEntity(
                                    LogAuditoria.Table.T_MVC_SOLICITUDSAN,
                                    LogAuditoria.ACTUALIZAR,
                                    LogAuditoria.DescripcionAccion.Actualizar.T_MVC_SOLICITUDSAN + p.getIdSolicitudSAN(),
                                    p.getIdUsuarioRegistro());
                        }

                        logAuditoriaRepository.RegistrarLogAuditoria(logAuditoriaEntity);


                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró la Solicitud.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }



    @Override
    public List<SolicitudSANEntity> ListarSolicitudSAN(SolicitudSANEntity param) throws Exception {

        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_SolicitudSAN_Listar");
            processStored.registerStoredProcedureParameter("nroGestion", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("estadoSolicitudSAN", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoPostulacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("procesoPostulacion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGENUM", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("P_PAGESIZE", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nroGestion", param.getNroGestion());
            processStored.setParameter("estadoSolicitudSAN", param.getEstadoSolicitudSAN());
            processStored.setParameter("tipoPostulacion", param.getTipoPostulacion());
            processStored.setParameter("procesoPostulacion", param.getProcesoPostulacion());
            processStored.setParameter("P_PAGENUM", param.getPageNum());
            processStored.setParameter("P_PAGESIZE", param.getPageSize());
            String estadoPlan="";
            processStored.execute();
            List<SolicitudSANEntity> result = new ArrayList<>();
            List<Object[]> spResult = processStored.getResultList();
            if (spResult.size() >= 1){
                SolicitudSANEntity temp = null;
                for (Object[] row: spResult){
                    temp = new SolicitudSANEntity();
                    temp.setIdSolicitudSAN((Integer) row[0]);
                    temp.setCodSolicitudSAN((String) row[1]);
                    temp.setSubCSolicitudSAN((String) row[2]);
                    temp.setEstadoSolicitudSAN(row[3]==null?null:(String) row[3]);
                    temp.setNroGestion(row[4]==null?null:(Integer) row[4]);
                    temp.setTipoPostulacion(row[5]==null?null:(String) row[5]);
                    temp.setProcesoPostulacion(row[6]==null?null:(String) row[6]);
                    temp.setMotivoSolicitud(row[7]==null?null:(String) row[7]);
                    temp.setAsunto(row[8]==null?null:(String) row[8]);
                    temp.setDescripcion(row[9]==null?null:(String) row[9]);
                    temp.setDetalle(row[10]==null?null:(String) row[10]);
                    temp.setObservacion(row[11]==null?null:(String) row[11]);
                    temp.setIdArchivo(row[12]==null?null:(Integer) row[12]);
                    temp.setFechaRegistro((Date)(row[13]));
                    temp.setFechaModificacion((Date)(row[14]));
                    temp.setEstadoPLan(row[15]==null?null:(String) row[15]);
                    temp.setDiasAdicionales(row[16]==null?null:(Integer) row[16]);
                    estadoPlan=row[15]==null?"":(String) row[15];
                    Calendar c = Calendar.getInstance();
                    c.setTime(temp.getFechaRegistro());
                    c.add(Calendar.DATE, 30);
                    temp.setFechaFinal(c.getTime());
                    int diasRestante = (int) ChronoUnit.DAYS.between(Instant.now(), temp.getFechaFinal().toInstant());
                    if(diasRestante<0)diasRestante = 0;
                    temp.setDiasRestantes(diasRestante);
                    /*************************************/
                    Calendar c2 = Calendar.getInstance();
                    if(estadoPlan.equals("EMDPRES")){
                        c2.setTime(temp.getFechaRegistro());
                        c2.add(Calendar.DATE, 60);
                        temp.setFechaFinalPlan(c.getTime());
                        int diasRestantePlan = (int) ChronoUnit.DAYS.between(Instant.now(), temp.getFechaFinalPlan().toInstant());
                        if(diasRestantePlan<0)diasRestantePlan = 0;
                        temp.setDiasRestantesPlan(diasRestantePlan);
                    }else{
                        temp.setFechaFinalPlan(null);
                        temp.setDiasRestantesPlan(null);
                    }



                    result.add(temp);
                }
            }
            return  result;

        } catch (Exception e) {
            log.error("SolicitudSan", e.getMessage());
            throw e;
        }
    }



    @Override
    public ResultClassEntity EliminarSolicitudSAN(SolicitudSANEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("dbo.pa_SolicitudSAN_Eliminar");
        processStored.registerStoredProcedureParameter("idSolicitudSAN", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);
        SpUtil.enableNullParams(processStored);
        processStored.setParameter("idSolicitudSAN", param.getIdSolicitudSAN());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());
        processStored.execute();
        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }


}
