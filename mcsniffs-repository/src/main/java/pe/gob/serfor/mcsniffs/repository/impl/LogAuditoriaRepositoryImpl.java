package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.repository.InformacionGeneralRepository;
import pe.gob.serfor.mcsniffs.repository.LogAuditoriaRepository;
import pe.gob.serfor.mcsniffs.repository.ManejoBosqueActividadRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class LogAuditoriaRepositoryImpl extends JdbcDaoSupport implements LogAuditoriaRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;

    private static final Logger log = LogManager
            .getLogger(pe.gob.serfor.mcsniffs.repository.impl.InformacionGeneralRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public ResultClassEntity RegistrarLogAuditoria(LogAuditoriaEntity request) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        try {
            StoredProcedureQuery processStored = em.createStoredProcedureQuery("Auditoria.pa_LogAuditoria_Registrar");
            processStored.registerStoredProcedureParameter("nombreTabla", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("tipoAccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("descripcionAccion", String.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("nombreTabla", request.getNombreTabla());
            processStored.setParameter("tipoAccion", request.getTipoAccion());
            processStored.setParameter("descripcionAccion", request.getDescripcionAccion());
            processStored.setParameter("idUsuarioRegistro", request.getIdUsuarioRegistro());

            List<Object[]> spResult = processStored.getResultList();

            result.setSuccess(true);
            result.setMessage("Se registró el Log de auditoria correctamente.");
            return result;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error al registrar Log de auditoria.");
            return result;
        }
    }


}
