package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;
import pe.gob.serfor.mcsniffs.repository.ParametroRepository;
import pe.gob.serfor.mcsniffs.repository.util.SpUtil;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ParametroRepositoryImpl extends JdbcDaoSupport implements ParametroRepository {
    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;
    private static final Logger log = LogManager.getLogger(ParametroRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para consultar Parametros por código}
     * @param: Objeto ParametroValorEntity
     *
     * @return: List<ParametroValorEntity>
     */
    @Override
    public List<ParametroValorEntity> ListarPorCodigoParametroValor(ParametroValorEntity param) {
        List<ParametroValorEntity> result = new ArrayList<>();
        try{

            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_ParametroValor_ListarPorCodigo");
            processStored.registerStoredProcedureParameter("codigoParametro", String.class, ParameterMode.IN);
            SpUtil.enableNullParams(processStored);
            processStored.setParameter("codigoParametro",param.getPrefijo());
            processStored.execute();

            List<Object[]> spResult =processStored.getResultList();
            if(spResult!=null){
                if (spResult.size() >= 1) {
                    for (Object[] row : spResult) {
                        ParametroValorEntity temp = new ParametroValorEntity();
                        temp.setCodigo((String) row[0]);
                        temp.setValor1((String) row[1]);
                        result.add(temp);
                    }
                }
            }

            return result;
        }
        catch (Exception e){
            log.error(e.getMessage(), e);
            return  null;
        }
    }
}
