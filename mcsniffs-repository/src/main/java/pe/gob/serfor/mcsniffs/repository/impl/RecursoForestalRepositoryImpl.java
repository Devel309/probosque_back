package pe.gob.serfor.mcsniffs.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDto;
import pe.gob.serfor.mcsniffs.repository.RecursoForestalRepository;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @autor: Rafael Azaña [07-09-2021]
 * @modificado:
 * @descripción: {Repository recursos forestales}
 */

@Repository
public class RecursoForestalRepositoryImpl extends JdbcDaoSupport implements RecursoForestalRepository {

    @Autowired
    @Qualifier("dataSourceBDMCSNIFFS")
    DataSource dataSource;



    private static final Logger log = LogManager.getLogger(RecursoForestalRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }




    @Override
    public List<RecursoForestalDetalleDto> listarRecursoForestal(Integer idPlanManejo, Integer idRecursoForestal, String codCabecera) throws Exception {
        List<RecursoForestalDetalleDto> lista = new ArrayList<RecursoForestalDetalleDto>();
        try {
            StoredProcedureQuery processStored = entityManager.createStoredProcedureQuery("dbo.pa_RecursoForestal_Listar");
            processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("idRecursoForestal", Integer.class, ParameterMode.IN);
            processStored.registerStoredProcedureParameter("codCabecera", String.class, ParameterMode.IN);

            processStored.setParameter("idPlanManejo", idPlanManejo);
            processStored.setParameter("idRecursoForestal", idRecursoForestal);
            processStored.setParameter("codCabecera", codCabecera);

            processStored.execute();

            List<Object[]> spResult = processStored.getResultList();
            if(spResult!=null){

                if (spResult.size() >= 1){
                    for (Object[] row : spResult) {

                        RecursoForestalDetalleDto temp = new RecursoForestalDetalleDto();
                        temp.setIdPlanManejo((Integer) row[0]);
                        temp.setIdRecursoForestal((Integer) row[1]);
                        temp.setIdRecursoForestalDetalle((Integer) row[2]);
                        temp.setCodigoTipoRecursoForestal((String) row[3]);
                        temp.setCodCabecera((String) row[4]);
                        temp.setNombreComun((String) row[5]);
                        temp.setNombreCientifico((String) row[6]);
                        temp.setNombreNativo((String) row[7]);
                        temp.setCoordenadaEste((String) row[8]);
                        temp.setCoordenadaNorte((String) row[9]);
                        temp.setNumeroArbolesAprovechables((Integer) row[10]);
                        temp.setVolumenComercial((String) row[11]);
                        temp.setNumeroArbolesSemilleros((Integer) row[12]);
                        temp.setNumeroArbolesTotal((Integer) row[13]);
                        temp.setFechaRealizacionInventario((Timestamp) row[14]);
                        temp.setNumParcela((Integer) row[15]);
                        temp.setProducto((String) row[16]);
                        temp.setUniMedida((String) row[17]);
                        temp.setDescripcionFechaRealizacion((String) row[18]);
                        temp.setCantidad((Integer) row[19]);
                        lista.add(temp);
                    }
                }
            }
            return lista;
        } catch (Exception e) {
            log.error("listarRecursoForestal", e.getMessage());
            throw e;
        }
    }

    @Override
    public ResultClassEntity RegistrarRecursoForestal(List<RecursoForestalEntity> list) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        List<RecursoForestalEntity> list_ = new ArrayList<>();
        try {
            for (RecursoForestalEntity p : list) {
                StoredProcedureQuery sp = entityManager.createStoredProcedureQuery("dbo.pa_RecursoForestal_Registrar");
                sp.registerStoredProcedureParameter("idRecursoForestal", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("codigoTipoRecursoForestal", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("fechaRealizacion", Timestamp.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("descripcionInventario", String.class, ParameterMode.IN);
                sp.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                setStoreProcedureEnableNullParameters(sp);
                sp.setParameter("idRecursoForestal", p.getIdRecursoForestal());
                sp.setParameter("codigoTipoRecursoForestal", p.getCodigoTipoRecursoForestal());
                sp.setParameter("idPlanManejo", p.getIdPlanManejo());
                sp.setParameter("fechaRealizacion", p.getFechaRealizacionInventario());
                sp.setParameter("descripcionInventario", p.getDescripcionFechaRealizacion());
                sp.setParameter("idUsuarioRegistro", p.getIdUsuarioRegistro());
                sp.execute();
                List<Object[]> spResult_ = sp.getResultList();

                if (!spResult_.isEmpty()) {
                    for (Object[] row_ : spResult_) {
                        RecursoForestalEntity obj_ = new RecursoForestalEntity();
                        obj_.setIdRecursoForestal((Integer) row_[0]);
                        obj_.setCodigoTipoRecursoForestal((String) row_[1]);
                        obj_.setIdPlanManejo((Integer) row_[2]);
                        List<RecursoForestalDetalleEntity> listDet = new ArrayList<>();
                        for (RecursoForestalDetalleEntity param : p.getListRecursoForestal()) {
                            StoredProcedureQuery pa = entityManager
                                    .createStoredProcedureQuery("dbo.pa_RecursoForestalDetalle_Registrar");
                            pa.registerStoredProcedureParameter("idRecursoForestalDetalle", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idRecursoForestal", Integer.class,ParameterMode.IN);
                            pa.registerStoredProcedureParameter("codigoTipoCabecera", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreComun", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreCientifico", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nombreNativo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaEste", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("coordenadaNorte", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("numeroArbolesAprovechables", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("volumenComercial", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("numeroArbolesSemilleros", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("numeroArbolesTotal", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("nuParcela", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("productoTipo", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("unidad", String.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("cantidad", Integer.class, ParameterMode.IN);
                            pa.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);

                            setStoreProcedureEnableNullParameters(pa);
                            pa.setParameter("idRecursoForestalDetalle", param.getIdRecursoForestalDetalle());
                            pa.setParameter("idRecursoForestal", obj_.getIdRecursoForestal());
                            pa.setParameter("codigoTipoCabecera", param.getCodigoCabecera());
                            pa.setParameter("nombreComun", param.getNombreComun());
                            pa.setParameter("nombreCientifico", param.getNombreCientifico());
                            pa.setParameter("nombreNativo", param.getNombreNativo());
                            pa.setParameter("coordenadaEste", param.getCoordenadaEste());
                            pa.setParameter("coordenadaNorte", param.getCoordenadaNorte());
                            pa.setParameter("numeroArbolesAprovechables", param.getNumeroArbolesAprovechables());
                            pa.setParameter("volumenComercial", param.getVolumenComercial());
                            pa.setParameter("numeroArbolesSemilleros", param.getNumeroArbolesSemilleros());
                            pa.setParameter("numeroArbolesTotal", param.getNumeroArbolesTotal());
                            pa.setParameter("nuParcela", param.getNumParcelaCorte());
                            pa.setParameter("productoTipo", param.getProductoTipo());
                            pa.setParameter("unidad", param.getUnidadMedida());
                            pa.setParameter("cantidad", param.getCantidad());
                            pa.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());
                            pa.execute();
                            List<Object[]> spResult = pa.getResultList();
                            if (!spResult.isEmpty()) {
                                RecursoForestalDetalleEntity obj = null;
                                for (Object[] row : spResult) {
                                    obj = new RecursoForestalDetalleEntity();
                                    obj.setIdRecursoForestalDetalle((Integer) row[0]);
                                    obj.setCodigoCabecera((String) row[1]);
                                    obj.setNombreComun((String) row[2]);
                                    obj.setNombreCientifico((String) row[3]);
                                    obj.setNombreNativo((String) row[4]);
                                    obj.setCoordenadaEste((String) row[5]);
                                    obj.setCoordenadaNorte((String) row[6]);
                                    obj.setNumeroArbolesAprovechables((Integer) row[7]);
                                    obj.setVolumenComercial((String) row[8]);
                                    obj.setNumeroArbolesSemilleros((Integer) row[9]);
                                    obj.setNumeroArbolesTotal((Integer) row[10]);
                                    obj.setNumParcelaCorte((Integer) row[11]);
                                    obj.setProductoTipo((String) row[12]);
                                    obj.setUnidadMedida((String) row[13]);
                                    obj.setCantidad((Integer) row[14]);
                                    listDet.add(obj);
                                }
                            }
                        }
                        obj_.setListRecursoForestal(listDet);
                        list_.add(obj_);
                    }
                }
            }
            result.setData(list_);
            result.setSuccess(true);
            result.setMessage("Se registró el recurso forestal Correctamente.");
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("Ocurrió un error.");
            return result;
        }
    }
/*
    @Override
    public ResultClassEntity RegistrarRecursoForestal(RecursoForestalEntity param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_RecursoForestal_Registrar]");
        processStored.registerStoredProcedureParameter("idRecursoForestal", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codigoTipoRecursoForestal", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idPlanManejo", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioRegistro", Integer.class, ParameterMode.IN);


        processStored.setParameter("idRecursoForestal", param.getIdRecursoForestal());
        processStored.setParameter("codigoTipoRecursoForestal", param.getCodigoTipoRecursoForestal());
        processStored.setParameter("idPlanManejo", param.getIdPlanManejo());
        processStored.setParameter("idUsuarioRegistro", param.getIdUsuarioRegistro());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se registró el Recurso Forestal con éxito.");
        return result;
    }

*/

    @Override
    public ResultClassEntity EliminarRecursoForestal(RecursoForestalDetalleDto param) throws Exception {
        ResultClassEntity result = new ResultClassEntity();
        StoredProcedureQuery processStored = entityManager
                .createStoredProcedureQuery("[dbo].[pa_RecursoForestal_Eliminar]");
        processStored.registerStoredProcedureParameter("idRecursoForestal", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idRecursoForestalDetalle", Integer.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("codCabecera", String.class, ParameterMode.IN);
        processStored.registerStoredProcedureParameter("idUsuarioElimina", Integer.class, ParameterMode.IN);

        processStored.setParameter("idRecursoForestal", param.getIdRecursoForestal());
        processStored.setParameter("idRecursoForestalDetalle", param.getIdRecursoForestalDetalle());
        processStored.setParameter("codCabecera", param.getCodCabecera());
        processStored.setParameter("idUsuarioElimina", param.getIdUsuarioElimina());

        processStored.execute();

        result.setData(param);
        result.setSuccess(true);
        result.setMessage("Se eliminó el registro correctamente.");
        return result;
    }



    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }

    }

/* temp = new RecursoForestalDto();
                    temp.setIdPlanManejo((Integer) item[0]);
                    temp.setIdRecursoForestal((Integer) item[1]);
                    temp.setCodigoTipoRecursoForestal((String) item[2]);
                    temp.setNombreComun((String) item[3]);
                    temp.setNombreCientifico(String.valueOf((Character) item[4]));
                    temp.setNombreNativo(String.valueOf((Character) item[5]));
                    temp.setCoordenadaEste((String) item[6]);
                    temp.setCoordenadaNorte((String) item[7]);
                    temp.setNumeroArbolesAprovechables((int) item[8]);
                    temp.setVolumenComercial((String) item[9]);
                    temp.setNumeroArbolesSemilleros((int) item[10]);
                    temp.setNumeroArbolesTotal((int) item[11]);*/

}
