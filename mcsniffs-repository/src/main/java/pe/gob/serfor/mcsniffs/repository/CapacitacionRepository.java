package pe.gob.serfor.mcsniffs.repository;

import pe.gob.serfor.mcsniffs.entity.CapacitacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;


public interface CapacitacionRepository {
    ResultClassEntity RegistrarCapacitacion(CapacitacionEntity capacitacionEntity) throws Exception;
    ResultClassEntity ActualizarCapacitacion(CapacitacionEntity capacitacionEntity) throws Exception;
    ResultClassEntity EliminarCapacitacion(CapacitacionEntity capacitacionEntity) throws Exception;
    List<CapacitacionDto> ListarCapacitacion(CapacitacionDto capacitacionEntity) throws Exception;
    ResultClassEntity RegistrarCapacitacionDetalle(List<CapacitacionDto> list)throws Exception;
    ResultClassEntity EliminarCapacitacionDetalle(CapacitacionDetalleEntity capacitacionDetalleEntity) throws Exception;
    ResultClassEntity ListarCapacitacionDetalle(CapacitacionEntity capacitacionEntity) throws Exception;
    List<CapacitacionDto> ListarCapacitacionCabeceraDetalle(Integer idPlanManejo, String codTipoCapacitacion) throws Exception;

}
