package pe.gob.serfor.mcsniffs.repository;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionArchivoDto;

public interface ResolucionArchivoRepository {

    ResultClassEntity registrarResolucionArchivo(ResolucionArchivoDto param);
    List<ResolucionArchivoDto> listarResolucionArchivo(ResolucionArchivoDto dto) throws Exception;
}
