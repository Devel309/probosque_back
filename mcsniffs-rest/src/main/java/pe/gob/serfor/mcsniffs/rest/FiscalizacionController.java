package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Fiscalizacion.FiscalizacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.FiscalizacionService;

@RestController
@RequestMapping(Urls.fiscalizacion.BASE)
public class FiscalizacionController  extends AbstractRestController {
     /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los contratos}
     *
     */
    @Autowired
    private FiscalizacionService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    @Autowired
    private EmailService enviarcorreo;
 

    @PostMapping(path = "/registrarFiscalizacion")
    @ApiOperation(value = "registrarFiscalizacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarFiscalizacion(@RequestBody FiscalizacionDto param){
        log.info("fiscalizacion - registrarFiscalizacion",param.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.registrarFiscalizacion(param);
            log.info("fiscalizacion - registrarFiscalizacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("fiscalizacion - registrarFiscalizacion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/adjuntarArchivosFiscalizacion")
    @ApiOperation(value = "Adjuntar Archivos Fiscalizacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> AdjuntarArchivosFiscalizacion(@RequestBody FiscalizacionEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.AdjuntarArchivosFiscalizacion(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("FiscalizacionController - AdjuntarArchivosFiscalizacion", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

 
    @PostMapping(path = "/listarFiscalizacion")
    @ApiOperation(value = "listarFiscalizacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarFiscalizacion(@RequestBody FiscalizacionDto param){
        log.info("fiscalizacion - listarFiscalizacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.listarFiscalizacion(param);
            log.info("fiscalizacion - listarFiscalizacion","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("fiscalizacion - listarFiscalizacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 


    /**
     * @autor: JaquelineDB [28-07-2021]
     * @modificado:
     * @descripción: {Se  obtienen todos los archivos de una fiscalizacion}
     * @param:IdFiscalizacion
     */
    @GetMapping(path = "/obtenerArchivosFiscalizacion/{IdFiscalizacion}")
    @ApiOperation(value = "Obtener Archivos Fiscalizacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<FiscalizacionAdjuntosEntity>> ObtenerArchivosFiscalizacion(@PathVariable("IdFiscalizacion") Integer IdFiscalizacion){
        ResultEntity<FiscalizacionAdjuntosEntity> result = new ResultEntity<FiscalizacionAdjuntosEntity>();
        try {
            result = service.ObtenerArchivosFiscalizacion(IdFiscalizacion);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Contrato - ObtenerArchivosFiscalizacion", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/obtenerFiscalizacion")
    @ApiOperation(value = "obtenerFiscalizacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerFiscalizacion(@RequestBody FiscalizacionDto dto){
        log.info("Fiscalización - obtenerFiscalizacion",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.ObtenerFiscalizacion(dto);
            log.info("Fiscalización - obtenerFiscalizacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Fiscalizacion - listarFiscalizacionPorId","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarFiscalizacionArchivo")
    @ApiOperation(value = "eliminarFiscalizacionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarFiscalizacionArchivo(@RequestBody FiscalizacionArchivoDto param){
        log.info("fiscalizacion - eliminarFiscalizacionArchivo",param.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.eliminarFiscalizacionArchivo(param);
            log.info("fiscalizacion - eliminarFiscalizacionArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("fiscalizacion - eliminarFiscalizacionArchivo","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/descagarPlantillaFormatoFiscalizacion")
    @ApiOperation(value = "descagarPlantillaFormatoFiscalizacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> descagarPlantillaFormatoFiscalizacion(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.DescagarPlantillaFormatoFiscalizacion();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - DescagarPlantillaFormatoFiscalizacion", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
}
