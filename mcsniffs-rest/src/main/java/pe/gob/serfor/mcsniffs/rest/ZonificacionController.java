package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ZonificacionDTO;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;

import java.util.List;

@RestController
@RequestMapping(Urls.zonificacionDema.BASE)
public class ZonificacionController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ZonificacionController.class);

    @Autowired
    private ZonificacionService zonificacionService;

    @PostMapping(path = "/listarZonificacion")
    @ApiOperation(value = "listarZonificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarZonificacion(@RequestBody ZonificacionEntity zonificacionEntity){
        log.info("Zonificacion - listarZonificacion", zonificacionEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = zonificacionService.ListarZonificacion(zonificacionEntity);
            log.info("Zonificacion - ListarZonificacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - ListarZonificacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarZona")
    @ApiOperation(value = "listarZona" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarZona(@RequestBody ZonaEntity item){
        log.info("Zona - listarZona", item);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = zonificacionService.listarZona(item);
            log.info("Zona - ListarZona","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - ListarZona","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarZona")
    @ApiOperation(value = "registrarZona" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarZona(@RequestBody List<ZonaEntity> item){
        log.info("Zonificacion - registrarZona", item);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = zonificacionService.registrarZona(item);
            log.info("Zonificacion - RegistrarZona","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - RegistrarZona","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/eliminarZona")
    @ApiOperation(value = "eliminarZona" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarZona(@RequestBody ZonaEntity item){
        log.info("Zonificacion - eliminarZona", item);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = zonificacionService.eliminarZona(item);
            log.info("Zonificacion - EliminarZona","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - EliminarZona","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/registrarZonificacion")
    @ApiOperation(value = "registrarZonificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarZonificacion(@RequestBody List<ZonificacionDTO> zonificacionDTOList){
        log.info("Zonificacion - registrarZonificacion", zonificacionDTOList);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = zonificacionService.RegistrarZonificacion(zonificacionDTOList);
            log.info("Zonificacion - RegistrarZonificacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - RegistrarZonificacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarZonificacion")
    @ApiOperation(value = "eliminarZonificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarZonificacion(@RequestBody ZonificacionEntity zonificacionEntity){
        log.info("Zonificacion - eliminarZonificacion", zonificacionEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = zonificacionService.EliminarZonificacion(zonificacionEntity);
            log.info("Zonificacion - EliminarZonificacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - EliminarZonificacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
