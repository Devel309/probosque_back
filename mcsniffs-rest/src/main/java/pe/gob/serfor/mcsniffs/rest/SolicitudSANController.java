package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.EvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;

import java.util.List;

@RestController
@RequestMapping(Urls.solicitudSAN.BASE)
public class SolicitudSANController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudSANController.class);
    @Autowired
    private EmailService service;

    @Autowired
    private EvaluacionRepository evaluacionRepository;


    @Autowired
    private pe.gob.serfor.mcsniffs.service.SolicitudSANService solicitudSANService;


    @PostMapping(path = "/registrarSolicitudSAN")
    @ApiOperation(value = "registrarSolicitudSAN", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarSolicitudSAN(
            @RequestBody List<SolicitudSANEntity> list) {
        log.info("registrarSolicitudSAN - registrarSolicitudSAN", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = solicitudSANService.RegistrarSolicitudSAN(list);
            boolean EnvioCorreo = false;
            String[] emailCC = new String[3];
            emailCC[0] = "rafitogglan@gmail.com";
            emailCC[1] = "rcoal@valtx.pe";
            emailCC[2] = "kpaucard@valtx.pe";
            for(SolicitudSANEntity solicitud : list ){

                if(solicitud.getIdSolicitudSAN()==0){
                    String[] email = new String[1];
                    EvaluacionPermisoForestalDto d2 = new EvaluacionPermisoForestalDto();
                    d2.setIdPermisoForestal(solicitud.getNroGestion());
                    List<EvaluacionPermisoForestalDto> listaUsuario = evaluacionRepository.listarEvaluacionPermisoForestalUsuario(d2);
                    for (EvaluacionPermisoForestalDto elementUsuario: listaUsuario) {
                        email[0] = elementUsuario.getResponsable();
                    }
                    EmailEntity emailModel = new EmailEntity();
                    emailModel.setContent("Su solicitud de Impugnación al SAN ha sido registrada para el permiso forestal "+solicitud.getNroGestion());
                    emailModel.setEmail(email);
                    emailModel.setSubject("IMPUGNACIÓN AL SAN");
                    emailModel.setEmailCc(emailCC);
                    EnvioCorreo = service.sendEmail(emailModel);
                }else if(solicitud.getIdSolicitudSAN()!=0){
                    String[] email = new String[1];
                    EvaluacionPermisoForestalDto d2 = new EvaluacionPermisoForestalDto();
                    d2.setIdPermisoForestal(solicitud.getNroGestion());
                    List<EvaluacionPermisoForestalDto> listaUsuario = evaluacionRepository.listarEvaluacionPermisoForestalUsuario(d2);
                    for (EvaluacionPermisoForestalDto elementUsuario: listaUsuario) {
                        email[0] = elementUsuario.getResponsable();
                    }
                    EmailEntity emailModel = new EmailEntity();
                    String estado="";
                    if(solicitud.getDetalle()==null || solicitud.getDetalle().equals("") || solicitud.getDetalle().equals(null)){
                        estado="";
                    }else{estado=solicitud.getDetalle();}
                    emailModel.setContent("Su solicitud de Impugnación al SAN para el nro. de Documento de Gestión: "+solicitud.getNroGestion()+" "+estado);
                    emailModel.setEmail(email);
                    emailModel.setSubject("IMPUGNACIÓN AL SAN");
                    emailModel.setEmailCc(emailCC);
                    EnvioCorreo = service.sendEmail(emailModel);
                }




            }

            log.info("SolicitudSAN - registrarSolicitudSAN", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("SolicitudSAN -registrarSolicitudSAN", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @PostMapping(path = "/listarSolicitudSAN")
    @ApiOperation(value = "listarSolicitudSAN" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudSAN(@RequestBody SolicitudSANEntity param){
        try{
            return  new ResponseEntity (true, "ok", solicitudSANService.ListarSolicitudSAN(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/eliminarSolicitudSAN")
    @ApiOperation(value = "eliminarSolicitudSAN" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarSolicitudSAN(@RequestBody SolicitudSANEntity solicitudSANEntity){
        log.info("SolicitudSAN - eliminarSolicitudSAN",solicitudSANEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudSANService.EliminarSolicitudSAN(solicitudSANEntity);
            log.info("SolicitudSAN - eliminarSolicitudSAN","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudSAN - eliminarSolicitudSAN","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
