package pe.gob.serfor.mcsniffs.rest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResumenActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ResumenActividadPoService;

import java.util.List;

@RestController
@RequestMapping(Urls.resumenActividadPo.BASE)
public class ResumenActividadPoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ResumenActividadPoController.class);

    @Autowired
    private ResumenActividadPoService resumenActividadPoService;

    @PostMapping(path = "/registrarResumenDetalle")
    @ApiOperation(value = "registrarResumenDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarResumenDetalle(@RequestBody List<ResumenActividadPoEntity> list){
        log.info("ProteccionBosque - registrarResumenDetalle",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = resumenActividadPoService.RegistrarResumenActividad(list);
            log.info("ProteccionBosque - registrarResumenDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ProteccionBosque -registrarResumenDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/listarResumenDetalle")
    @ApiOperation(value = "listarResumenDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarResumenDetalle(@RequestBody ResumenActividadDto param){
        try{
            return  new ResponseEntity (true, "ok", resumenActividadPoService.ListarResumenActividad(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/listarResumen_Detalle")
    @ApiOperation(value = "listarResumen_Detalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarResumen_Detalle(@RequestBody ResumenActividadPoEntity param){
        try{
            return  new ResponseEntity (true, "ok", resumenActividadPoService.ListarResumenActividad_Detalle(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }


    @PostMapping(path = "/eliminarResumenDetalle")
    @ApiOperation(value = "eliminarResumenDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarResumenDetalle(@RequestBody ResumenActividadPoDetalleEntity param){
        log.info("ProteccionBosque - eliminarResumenDetalle",param.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = resumenActividadPoService.EliminarResumenActividad(param);
            log.info("ProteccionBosque - eliminarResumenDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }                  
        }catch (Exception e){
            log.error("ProteccionBosque - eliminarCapacitacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarResumenActividadPO")
    @ApiOperation(value = "registrarResumenActividadPO" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarResumenActividadPO(@RequestBody ResumenActividadPoEntity param){
        try{
            return  new ResponseEntity (true, "ok", resumenActividadPoService.registrarResumenActividadPO(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }
    @PostMapping(path = "/listarResumenActividadPO")
    @ApiOperation(value = "listarResumenActividadPO" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarResumenActividadPO(@RequestBody ResumenActividadDto param){
        try{
            return  new ResponseEntity (true, "ok", resumenActividadPoService.ListarResumenActividadPO(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }
}
