package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ResponsableFormacionAcademicaService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.responsableSolicitudConcesion.BASE)
public class ResponsableFormacionAcademicaController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionAreaController.class);

    @Autowired
    ResponsableFormacionAcademicaService serviceResponsableFormacionAcademica;

    @PostMapping(path = "/obtenerInfoResponsableFormacionProfesional")
    @ApiOperation(value = "Obtener Informacion de Formacion Profesional del Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoResponsableFormacionProfesional(@RequestBody ResponsableFormacionAcademicaEntity request) {
        log.info("ResponsableFormacionAcademicaController - obtenerInfoResponsableFormacionProfesional", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceResponsableFormacionAcademica.obtenerInformacionFormacionProfesional(request);
            log.info("ResponsableFormacionAcademicaController - obtenerInfoResponsableFormacionProfesional", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ResponsableFormacionAcademicaController - obtenerInfoResponsableFormacionProfesional", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarInfoResponsableFormacionProfesional")
    @ApiOperation(value = "Registrar Informacion de Formacion Profesional del Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInfoResponsableFormacionProfesional(@RequestBody List<ResponsableFormacionAcademicaEntity> request){
        log.info("ResponsableFormacionAcademicaController - registrarInfoResponsableFormacionProfesional",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceResponsableFormacionAcademica.registrarInformacionFormacionProfesional(request);
            log.info("ResponsableFormacionAcademicaController - registrarInfoResponsableFormacionProfesional","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("ResponsableFormacionAcademicaController - registrarInfoResponsableFormacionProfesional","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarInformacionFormacionProfesional")
    @ApiOperation(value = "Eliminar Informacion de Formacion Profesional del Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarInformacionFormacionProfesional(@RequestBody ResponsableFormacionAcademicaEntity request){
        log.info("ResponsableFormacionAcademicaController - eliminarInfoResponsableFormacionProfesional",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceResponsableFormacionAcademica.eliminarInformacionFormacionProfesional(request);
            log.info("ResponsableFormacionAcademicaController - eliminarInfoResponsableFormacionProfesional","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("ResponsableFormacionAcademicaController - eliminarInfoResponsableFormacionProfesional","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarInfoResponsableFormacionProfesional")
    @ApiOperation(value = "Actualizar Informacion de Formacion Profesional del Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInforResponsableFormacionProfesional(@RequestBody SolicitudConcesionResponsableDto request){
        log.info("ResponsableFormacionAcademicaController - actualizarInfoResponsableFormacionProfesional",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceResponsableFormacionAcademica.actualizarInformacionFormacionProfesional(request);
            log.info("ResponsableFormacionAcademicaController - actualizarInfoResponsableFormacionProfesional","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("ResponsableFormacionAcademicaController - actualizarInfoResponsableFormacionProfesional","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
