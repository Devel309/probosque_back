package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import pe.gob.serfor.mcsniffs.entity.PlanManejoGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoGeometriaActualizarCatastroDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PlanManejoGeometriaService;

@RestController
@RequestMapping(Urls.planManejoGeometria.BASE)
public class PlanManejoGeometriaController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PlanManejoGeometriaController.class);
    @Autowired
    PlanManejoGeometriaService service;

    @PostMapping(path = "/registrarPlanManejoGeometria")
    @ApiOperation(value = "registrarPlanManejoGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarPlanManejoGeometria(@RequestBody List<PlanManejoGeometriaEntity> request) {
        log.info("PlanManejoGeometria - registrarPlanManejoGeometria", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarPlanManejoGeometria(request);

            if (result.getSuccess()) {
                log.info("PlanManejoGeometria - registrarPlanManejoGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejoGeometria - registrarPlanManejoGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado: 
     * @descripción: {Obtener planManejoGeometria por  filtros}
     * @param:planManejoGeometriaDto
     */
    @PostMapping(path = "/listarPlanManejoGeometria")
    @ApiOperation(value = "listarPlanManejoGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPlanManejoGeometria(@RequestBody PlanManejoGeometriaEntity item) {
        log.info("PlanManejoGeometriaController - listarPlanManejoGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarPlanManejoGeometria(item);

            if (result.getSuccess()) {
                log.info("PlanManejoGeometriaController - listarPlanManejoGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejoGeometriaController - listarPlanManejoGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Abner Valdez [17-12-2021]
     * @modificado: 
     * @descripción: {Eliminar PlanManejoGeometria archivo y/o capa}
     * @param:idArchivo, idUsuario
     */
    @GetMapping(path = "/eliminarPlanManejoGeometriaArchivo/{idArchivo}/{idUsuario}")
    @ApiOperation(value = "eliminarPlanManejoGeometriaArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarPlanManejoGeometriaArchivo(@PathVariable Integer idArchivo,@PathVariable Integer idUsuario) {
        log.info("PlanManejoGeometriaController - eliminarPlanManejoGeometriaArchivo", idArchivo);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarPlanManejoGeometriaArchivo(idArchivo,idUsuario);

            if (result.getSuccess()) {
                log.info("PlanManejoGeometriaController - eliminarPlanManejoGeometriaArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejoGeometriaController - eliminarPlanManejoGeometriaArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarCatastroPlanManejoGeometria")
    @ApiOperation(value = "actualizarCatastroPlanManejoGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarCatastroPlanManejoGeometria(@RequestBody PlanManejoGeometriaActualizarCatastroDto request) {
        log.info("PlanManejoGeometria - actualizarCatastroPlanManejoGeometria", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarCatastroPlanManejoGeometria(request);

            if (result.getSuccess()) {
                log.info("PlanManejoGeometria - actualizarCatastroPlanManejoGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejoGeometria - actualizarCatastroPlanManejoGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
