package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHRegenteDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.tituloHabilitanteRegente.BASE)
public class TituloHabilitanteRegenteController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TituloHabilitanteRegenteController.class);


    @Autowired
    private pe.gob.serfor.mcsniffs.service.TituloHabilitanteRegenteService tituloHabilitanteRegenteService;



    @PostMapping(path = "/listarInformacionTHRegente")
    @ApiOperation(value = "listarInformacionTHRegente" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarInformacionTHRegente(@RequestBody InformacionTHRegenteDto request) {
        log.info("tituloHabilitante - listarInformacionTHRegente", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = tituloHabilitanteRegenteService.listarInformacionTHRegente(request);
            log.info("tituloHabilitante- listarInformacionTHRegente", "Proceso realizado correctamente");

            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("tituloHabilitante- listarInformacionTHRegente", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

