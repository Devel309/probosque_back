package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EnviarConsultaService;

@RestController
@RequestMapping(Urls.enviarConsulta.BASE)
public class EnviarConsultaController {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {repositorio de la tabla Enviar consulta}
     *
     */
    @Autowired
    private EnviarConsultaService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Envia la consulta a una entidad}
     * @param:
     */

    @PostMapping(path = "/enviarConsulta")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> EnviarConsulta(@RequestBody EnviarConsultaEntity consulta){
        ResultClassEntity lst_ua = new ResultClassEntity();
        try {
            lst_ua = service.EnviarConsulta(consulta);
            return new ResponseEntity<>(lst_ua, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("EnviarConsultaController - EnviarConsulta", "Ocurrió un error al Listar en: "+Ex.getMessage());
            lst_ua.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_ua,HttpStatus.FORBIDDEN);
        }
    }
}
