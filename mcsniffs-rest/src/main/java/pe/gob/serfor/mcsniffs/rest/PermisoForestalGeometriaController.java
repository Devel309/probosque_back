package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.PermisoForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PermisoForestalGeometriaService;

import java.util.List;

@RestController
@RequestMapping(Urls.permisoForestalGeometria.BASE)
public class PermisoForestalGeometriaController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PermisoForestalGeometriaController.class);

    @Autowired
    private PermisoForestalGeometriaService permisoForestalGeometriaService;

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Registrar Geometría Permiso Forestal}
     * @param: PermisoForestalGeometriaEntity
     */
    @PostMapping(path = "/registrarPermisoForestalGeometria")
    @ApiOperation(value = "registrarPermisoForestalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarPermisoForestalGeometria(@RequestBody List<PermisoForestalGeometriaEntity> request) {
        log.info("PermisoForestalGeometria - registrarPermisoForestalGeometria", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalGeometriaService.registrarPermisoForestalGeometria(request);

            if (result.getSuccess()) {
                log.info("PermisoForestalGeometria - registrarPermisoForestalGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestalGeometria - registrarPermisoForestalGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Obtener Geometría Permiso Forestal}
     * @param: PermisoForestalGeometriaEntity
     */
    @PostMapping(path = "/listarPermisoForestalGeometria")
    @ApiOperation(value = "listarPermisoForestalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPermisoForestalGeometria(@RequestBody PermisoForestalGeometriaEntity item) {
        log.info("PermisoForestalGeometria - listarPermisoForestalGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalGeometriaService.listarPermisoForestalGeometria(item);

            if (result.getSuccess()) {
                log.info("PermisoForestalGeometria - listarPermisoForestalGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestalGeometria - listarPermisoForestalGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [21-12-2021]
     * @modificado:
     * @descripción: {Obtener Geometría Permiso Forestal archivo y/o capa}
     * @param: PermisoForestalGeometriaEntity
     */
    @PostMapping(path = "/eliminarPermisoForestalGeometriaArchivo")
    @ApiOperation(value = "eliminarPermisoForestalGeometriaArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarPlanManejoGeometriaArchivo(@RequestBody PermisoForestalGeometriaEntity item) {
        log.info("PermisoForestalGeometria - eliminarPermisoForestalGeometriaArchivo", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalGeometriaService.eliminarPermisoForestalGeometriaArchivo(item);

            if (result.getSuccess()) {
                log.info("PermisoForestalGeometria - eliminarPermisoForestalGeometriaArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestalGeometria - eliminarPermisoForestalGeometriaArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
