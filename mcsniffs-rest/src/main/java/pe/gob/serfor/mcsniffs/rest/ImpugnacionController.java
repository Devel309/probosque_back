package pe.gob.serfor.mcsniffs.rest;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Impugnacion.ImpugnacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.ResolucionService;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.ImpugnacionService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;


@RestController
@RequestMapping(Urls.impugnacion.BASE)
public class ImpugnacionController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ImpugnacionController.class);

    @Autowired
    private ImpugnacionService impugnacionService;

    @Autowired
    private EmailService serMail;

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ResolucionService resolucionService;

    @PostMapping(path = "/listarImpugnacion")
    @ApiOperation(value = "listarImpugnacion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity listarImpugnacion(@RequestBody ImpugnacionDto param) {
        log.info("Impugnacion - ListarImpugnacion", param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = impugnacionService.ListarImpugnacion(param);
            log.info("Impugnacion - ListarImpugnacion", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Impugnacion - ListarImpugnacion", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/notificarSolicitudImpugnacion")
    @ApiOperation(value = "notificarSolicitudImpugnacion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity notificarSolicitudImpugnacion(@RequestBody ImpugnacionDto param) {
        log.info("Impugnacion - notificarSolicitudImpugnacion", param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {


            response = impugnacionService.notificarSolicitudImpugnacion(param);
            log.info("Impugnacion - notificarSolicitudImpugnacion", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Impugnacion - notificarSolicitudImpugnacion", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarResolucionImpugnada")
    @ApiOperation(value = "listarResolucionImpugnada", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity listarResolucionImpugnada(@RequestBody ImpugnacionDto param) {
        log.info("Impugnacion - ListarResolucionImpugnada", param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = impugnacionService.ListarResolucionImpugnada(param);
            log.info("Impugnacion - ListarResolucionImpugnada", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Impugnacion - ListarResolucionImpugnada", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/validarPlazoImpugnacionResolucion")
    @ApiOperation(value = "Validar Plazo de Impugnacion de resolución", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity validarPlazoImpugnacionResolucion(@RequestBody ImpugnacionDto param) {
        log.info("Impugnacion - validarPlazoImpugnacionResolucion", param.toString());

        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = impugnacionService.ValidarImpugnacionResolucion(param);
            log.info("Impugnacion - validarPlazoImpugnacionResolucion", "Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Impugnacion - validarPlazoImpugnacionResolucion", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;
    @PostMapping(path = "/registrarImpugnacion")
    @ApiOperation(value = "registrarImpugnacion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity registrarImpugnacion(@RequestParam("idResolucion") Integer idResolucion,
                                               @RequestParam("asunto") String asunto,
                                               @RequestParam("descripcion") String descripcion,
                                               @RequestParam("fechaImpugnacion") Date fechaImpugnacion,
                                               @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro,
                                               @RequestParam("region") String region,
                                               @RequestParam("fileRecurso") MultipartFile fileRecurso,
                                               @RequestParam(required = false)  MultipartFile fileSustento,
                                               HttpServletRequest request1
    ) {
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = new pe.gob.serfor.mcsniffs.entity.ResponseEntity();
        ResultClassEntity archivoResponse= new ResultClassEntity();
        ResultClassEntity archivoResponse2 = new ResultClassEntity();
        ResultClassEntity response = new ResultClassEntity();
        try{
            ImpugnacionDto obj = new ImpugnacionDto();
            Gson gson = new Gson();
            obj.setIdResolucion(idResolucion);
            obj.setAsunto(asunto);
            obj.setDescripcion(descripcion);
            obj.setFechaImpugnacion(fechaImpugnacion);
            obj.setIdUsuarioRegistro(idUsuarioRegistro);
            archivoResponse =  serArchivo.RegistrarArchivoGeneral(fileRecurso, idUsuarioRegistro,"TDOCIMPREC");


            if(archivoResponse.getSuccess() && fileSustento!=null ) {
                archivoResponse2 = serArchivo.RegistrarArchivoGeneral(fileSustento, idUsuarioRegistro, "TDOCIMPSUSTE");
                if( archivoResponse2.getSuccess()) {
                    obj.setIdArchivoRecurso(archivoResponse.getCodigo());
                    obj.setIdArchivoSustento(archivoResponse2.getCodigo());
                    response = impugnacionService.RegistrarImpugnacion(obj);
                    
                    obj.setIdImpugnacion(response.getCodigo());
                    if (!archivoResponse.getSuccess()) {
                        return new ResponseEntity(archivoResponse, HttpStatus.BAD_REQUEST);
                    }
                }
            }else if(archivoResponse.getSuccess() && fileSustento==null){

               obj.setIdArchivoRecurso(archivoResponse.getCodigo());
               response = impugnacionService.RegistrarImpugnacion(obj);
               obj.setIdImpugnacion(response.getCodigo());
               if(!archivoResponse.getSuccess()){
                   return new ResponseEntity(archivoResponse, HttpStatus.BAD_REQUEST);
               }

           }else{
               archivoResponse.setMessage(archivoResponse.getMessage());
               return new ResponseEntity(archivoResponse, HttpStatus.BAD_REQUEST);
           }


            if (response.getSuccess()) {
                UsuarioDto user = new UsuarioDto();
                user.setPerfil("ARFFS");
                user.setRegion(region);
                String token= request1.getHeader("Authorization");
                ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/listarPorRegionPerfilUsuario", gson.toJson(user), token);
                if(responseApiExt.getIsSuccess()){
                    ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
                    String[] mails = new String[resultApiExt.getData().size()];
                    int cont = 0;
                    for(Object item: resultApiExt.getData().stream().toArray()){
                        LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
                        mails[cont]=val.get("correoElectronico").toString();
                        cont++;
                    }
                    if(mails.length==0){
                        response.setMessage("El registro la impugnación correctamente , pero no se envio la notificación");
                        return new ResponseEntity(response, HttpStatus.OK);
                    }
                    EmailEntity mail = new EmailEntity();
                    mail.setSubject("SERFOR::IMPUGNACIÓN Nº "+obj.getIdImpugnacion().toString());
                    mail.setEmail(mails);
                    String body="<html><head>"
                            +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                            +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                            +"</head>"
                            +"<body lang=ES-PE style='word-wrap:break-word'>"
                            +"<div class=WordSection1>"
                            +"<p class=MsoNormal><span lang=ES>Estimado(a): </span></p>"
                            +"<p class=MsoNormal><span lang=ES>  El Código de Impugnación : "+obj.getIdImpugnacion().toString()+" </span></p>"
                            +"<p class=MsoNormal><span lang=ES>  Se encuentra pendiente</span></p>"
                            +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                            +"</div></body></html>";
                    mail.setContent(body);
                    Boolean smail = serMail.sendEmail(mail);
                    if(smail){
                        response.setInformacion("Correo Enviado Impugnación Nº "+obj.getIdImpugnacion().toString());
                        log.info("Impugnacion - RegistrarImpugnacion", "Proceso realizado correctamente");
                        return new ResponseEntity(response, HttpStatus.OK);
                    }
                    else{
                        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
                    }
                }
                else{
                    return new ResponseEntity(archivoResponse2, HttpStatus.BAD_REQUEST);
                }
            }
            else{
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Impugnacion -RegistrarImpugnacion", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarImpugnacion")
    @ApiOperation(value = "actualizarImpugnacion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity actualizarImpugnacion(@RequestParam("idImpugnacion") Integer idImpugnacion,
                                                @RequestParam(required = false) String tipoImpugnacion,
                                                @RequestParam(required = false) Boolean esfundado,
                                                @RequestParam("retrotraer") Boolean retrotraer,
                                                @RequestParam("idUsuarioModificacion") Integer idUsuarioModificacion,
                                                @RequestParam(required = false) MultipartFile fileEvaluacion,
                                                @RequestParam(required = false) MultipartFile fileFirme
    ) {
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = new pe.gob.serfor.mcsniffs.entity.ResponseEntity();
        log.info("Impugnacion - actualizarImpugnacion");
        ResultClassEntity response =new ResultClassEntity();
        try{
            response = impugnacionService.ActualizarImpugnacion(idImpugnacion,tipoImpugnacion,esfundado,retrotraer,idUsuarioModificacion,fileEvaluacion,fileFirme);

            log.info("Impugnacion - ActualizarImpugnacionArchivo","Proceso realizado correctamente");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Impugnacion - ActualizarImpugnacionArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerImpugnacion")
    @ApiOperation(value = "obtenerImpugnacion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity obtenerImpugnacion(@RequestBody ImpugnacionDto param) {
        log.info("Impugnacion - ObtenerImpugnacion", param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = impugnacionService.ObtenerImpugnacion(param);
            log.info("Impugnacion - ObtenerImpugnacion", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Impugnacion - ObtenerImpugnacion", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarImpugnacionArchivo")
    @ApiOperation(value = "actualizarImpugnacionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarImpugnacionArchivo(@RequestParam(required = false) Integer idImpugnacion,
                                                                               @RequestParam(required = false) String idTipoDocumento,
                                                                               @RequestParam(required = false) Integer documento,
                                                                               @RequestParam(required = false) Integer idUsuarioModificacion,
                                                                               @RequestParam("file") MultipartFile file
    ){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = new pe.gob.serfor.mcsniffs.entity.ResponseEntity();
        log.info("Impugnacion - ActualizarImpugnacionArchivo");
        ResultClassEntity response =new ResultClassEntity();
        try{
            ArchivoEntity objArc=new ArchivoEntity();
            response =  serArchivo.RegistrarArchivoGeneral(file, idUsuarioModificacion, idTipoDocumento);
            if(response.getSuccess()){
                ImpugnacionDto obj = new ImpugnacionDto();
                obj.setIdImpugnacion(idImpugnacion);
                if(documento == 1){
                    obj.setIdArchivoRecurso(response.getCodigo());
                }else{
                    obj.setIdArchivoSustento(response.getCodigo());
                }
                obj.setIdUsuarioModificacion(idUsuarioModificacion);
                response = impugnacionService.ActualizarImpugnacionArchivo(obj);
            }else{

                response.setMessage(response.getMessage());
            }
            log.info("Impugnacion - ActualizarImpugnacionArchivo","Proceso realizado correctamente");
            response.setMessage(""+idImpugnacion +file);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Impugnacion - ActualizarImpugnacionArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarImpugnacionArchivo")
    @ApiOperation(value = "eliminarImpugnacionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarImpugnacionArchivo(ImpugnacionDto param){
        log.info("Impugnacion - eliminarImpugnacionArchivo");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            ArchivoEntity objArc=new ArchivoEntity();
            objArc.setIdArchivo(0);
            response =  serArchivo.EliminarArchivoGeneral((param.getIdArchivoRecurso()==null?param.getIdArchivoSustento():param.getIdArchivoRecurso()), param.getIdUsuarioElimina());
            if(response.getSuccess()){
                response = impugnacionService.EliminarImpugnacionArchivo(param);
            }else{
                response.setMessage(response.getMessage());
            }
            log.info("Impugnacion - eliminarImpugnacionArchivo","Proceso realizado correctamente");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Impugnacion - eliminarImpugnacionArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}