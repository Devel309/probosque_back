package pe.gob.serfor.mcsniffs.rest;

import java.io.File;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoNotalListEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPBandejaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPEntity;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPNotasEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.ResultadoPPService;

@RestController
@RequestMapping(Urls.resultadoPP.BASE)
public class ResultadoPPController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ResultadoPPController.class);
    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Repository de los resultados de los procesos de postulacion}
     *
     */
    @Autowired
    ResultadoPPService servicio;

    @Autowired
    EmailService email;

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Lista los resultados de las evaluaciones}
     * @param:ResultadoPPEntity
     */
    @PostMapping(path = "/listarResultadosEvaluaciones")
    @ApiOperation(value = "Listar Resultados Evaluaciones", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ResultadoPPEntity>> ListarResultadosEvaluaciones(@RequestBody ResultadoPPEntity obj){
        ResultEntity<ResultadoPPEntity> result = new ResultEntity<ResultadoPPEntity>();
        try {
            result = servicio.ListarResultadosEvaluaciones(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ResultadoPP - ListarResultadosEvaluaciones", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Registra y envia la resulusion a los participantes}
     * @param:ResultadoPPEntity
     */
    @PostMapping(path = "/registrarResultados")
    @ApiOperation(value = "Registrar Resultados", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> RegistrarResultados(@RequestBody ResultadoPPEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.RegistrarResultados(obj);
            //Enviar Correo con la resolucion como adjunto
            if(result.getCodigo()!=null){
                if(result.getCodigo() > 0){
                    ResultClassEntity<ResultadoPPAdjuntoEntity> archivo = servicio.ObtenerResultados(obj.getIdResultadoPP());
                    EmailEntity body =  new EmailEntity();
                    InputStream inputStream = getClass().getClassLoader()
                            .getResourceAsStream("/Anexo4.pdf");
                    File fileCopi = File.createTempFile("anexo4",".pdf");
                    //body.setAdjunto(archivo.getData()==null?fileCopi:archivo.getData().getDocumentoFile());
                    body.setAdjunto(fileCopi);
                    body.setContent(obj.getContenidoEmail());
                    String[] mails =new String[1];
                    mails[0]=obj.getCorreoPostulante();
                    body.setEmail(mails);
                    body.setSubject(obj.getAsuntoEmail());
                    email.sendEmailWithFile(body);
                }
            }
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ResultadoPP - RegistrarResultados", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Obtiene los resultados}
     * @param:IdResultadoPP
     */
    @GetMapping(path = "/obtenerResultados/{IdResultadoPP}")
    @ApiOperation(value = "Obtener Resultados", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<ResultadoPPAdjuntoEntity>> ObtenerResultados(@PathVariable Integer IdResultadoPP){
        ResultClassEntity<ResultadoPPAdjuntoEntity> result = new ResultClassEntity<ResultadoPPAdjuntoEntity>();
        try {
            result = servicio.ObtenerResultados(IdResultadoPP);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ResultadoPP - ObtenerResultados", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {Registra las notas del proceso de postulacion}
     * @param:ResultadoPPNotasEntity
     */
    @PostMapping(path = "/registrarNotas")
    @ApiOperation(value = "Registrar Notas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> RegistrarNotas(@RequestBody ResultadoPPNotasEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.RegistrarNotas(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ResultadoPP - RegistrarNotas", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {obtiene las notas del proceso de postulacion}
     * @param:IdResultadoPP
     */
    @GetMapping(path = "/obtenerNotas/{IdResultadoPP}")
    @ApiOperation(value = "Obtener Notas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ResultadoNotalListEntity>> ObtenerNotas(@PathVariable Integer IdResultadoPP){
        ResultEntity<ResultadoNotalListEntity> result = new ResultEntity<ResultadoNotalListEntity>();
        try {
            result = servicio.ObtenerNotas(IdResultadoPP);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ResultadoPP - ObtenerNotas", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-07-2021]
     * @modificado:
     * @descripción: {obtiene el primer proceso postulacion
     * @param:IdProcesoOferta
     */
    @GetMapping(path = "/obtenerPrimerPostulante/{IdProcesoOferta}")
    @ApiOperation(value = "obtener Primer Postulante", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<ProcesoPostulacionEntity>> ObtenerPrimerPP(@PathVariable Integer IdProcesoOferta){
        ResultClassEntity<ProcesoPostulacionEntity> result = new ResultClassEntity<ProcesoPostulacionEntity>();
        try {
            result = servicio.ObtenerPrimerPP(IdProcesoOferta);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ResultadoPP - ObtenerPrimerPP", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [13-07-2021]
     * @modificado:
     * @descripción: {Listar los procesos de postulacion que estan listos para ser evaluados
     * @param:EvaluacionRequestEntity
     */
    @PostMapping(path = "/listarPPEvaluacion")
    @ApiOperation(value = "Listar Resultados Evaluaciones", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ResultadoPPBandejaEntity>> ListarPPEvaluacion(@RequestBody EvaluacionRequestEntity obj){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = servicio.ListarPPEvaluacion(obj);
            log.info("resultado pp - listarPPEvaluacion","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("resultado pp - listarPPEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarPorFiltroOtrogamientoProcesoPostulacion")
    @ApiOperation(value = "listarPorFiltroOtrogamientoProcesoPostulacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ResultadoPPBandejaEntity>> listarPorFiltroOtrogamientoProcesoPostulacion(@RequestBody EvaluacionRequestEntity obj){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = servicio.listarPorFiltroOtrogamientoProcesoPostulacion(obj);
            log.info("resultado pp - listarPorFiltroOtrogamientoProcesoPostulacion","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("resultado pp - listarPorFiltroOtrogamientoProcesoPostulacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    

    /**
     * @autor: JaquelineDB [14-07-2021]
     * @modificado:
     * @descripción: {Listar los procesos en oferta con potulaciones evaluadas
     * @param:
     */
    @GetMapping(path = "/listarProcesosOferta")
    @ApiOperation(value = "Listar Procesos Oferta con postulaciones evaluadas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ProcesoOfertaEntity>> ListarProcesosOferta(){

        ResultClassEntity<ProcesoOfertaEntity> response = null;
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result;
        try {
            response = servicio.ListarProcesosOferta();
            log.info("Resultadopp - ListarProcesosOferta","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception Ex){

            log.error("ResultadoPP - ListarProcesosOferta\", \"Ocurrió un error al listar en: "+ Ex.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, Ex);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Abner [06-12-2021]
     * @modificado:
     * @descripción: {obtener el resultado por Id Proceso de Postulacion}
     * @param:EvaluacionRequestEntity
     */

    @GetMapping(path = "/obtenerResultadoPPPorIdPP/{idProcesoPostulacion}")
    @ApiOperation(value = "obtenerResultadoPPPorIdPP" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerResultadoPPPorIdPP(@PathVariable Integer idProcesoPostulacion){
        log.info("ResultadoPP - obtenerResultadoPPPorIdPP",idProcesoPostulacion);
        ResultClassEntity result =new ResultClassEntity();
        try{
            result = servicio.obtenerResultadoPPPorIdPP(idProcesoPostulacion);

            if (result.getSuccess()) {
                log.info("ResultadoPP- obtenerResultadoPPPorIdPP","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("ResultadoPP - obtenerResultadoPPPorIdPP","Ocurrió un error :"+ e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
