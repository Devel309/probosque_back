package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalEvaluacionDetalleService;

@RestController
@RequestMapping(Urls.SolicitudBosqueLocalEvaluacionDetalle.BASE)
public class SolicitudBosqueLocalEvaluacionDetalleController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalEvaluacionDetalleController.class);

    @Autowired
    private SolicitudBosqueLocalEvaluacionDetalleService solicitudBosqueLocalEvaluacionDetalle;

  

    @PostMapping(path = "/listarSolicitudBosqueLocalEvaluacionDetalle")
    @ApiOperation(value = "listar Solicitud Bosque Local Evaluacion Detalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarSolicitudBosqueLocalEvaluacionDetalle(@RequestBody SolicitudBosqueLocalEvaluacionDetalleDto obj){
        log.info("ProteccionBosque - listarSolicitudBosqueLocalEvaluacionDetalle",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = solicitudBosqueLocalEvaluacionDetalle.listarSolicitudBosqueLocalEvaluacionDetalle(obj);
            log.info("ProteccionBosque - listarSolicitudBosqueLocalEvaluacionDetalle","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("BosqueLocal -listarSolicitudBosqueLocalEvaluacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
