package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralDEMAEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TituloHabilitanteEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralDemaService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.informacionGeneralDema.BASE)
public class InformacionGeneralDemaController {
    
    /**
     * @autor: JaquelineDB [10-09-2021]
     * @modificado:
     * @descripción: {Repository  de la informacion general relacionada con el proceso DEMA}
     *
     */
    @Autowired
    private InformacionGeneralDemaService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(InformacionGeneralDemaController.class);

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {Registrar informacion general dema}
     *
     */
    
    @PostMapping(path = "/registrarInformacionGeneralDema")
    @ApiOperation(value = "registrar Informacion General Dema", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarInformacionGeneralDema(@RequestBody InformacionGeneralDEMAEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarInformacionGeneralDema(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("InformacionGeneralDemaController - registrarInformacionGeneralDema", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {actualizar informacion general dema}
     *
     */
    @PostMapping(path = "/actualizarInformacionGeneralDema")
    @ApiOperation(value = "actualizar Informacion General Dema", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarInformacionGeneralDema(@RequestBody InformacionGeneralDEMAEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarInformacionGeneralDema(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("InformacionGeneralDemaController - actualizarInformacionGeneralDema", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [09-09-2021]
     * @modificado:
     * @descripción: {listar informacion general dema}
     *
     */
    @PostMapping(path = "/listarInformacionGeneralDema")
    @ApiOperation(value = "listar Informacion General Dema", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<InformacionGeneralDEMAEntity>> listarInformacionGeneralDema(@RequestBody InformacionGeneralDEMAEntity data){
        ResultEntity<InformacionGeneralDEMAEntity> result = new ResultEntity<InformacionGeneralDEMAEntity>();
        try {
            result = service.listarInformacionGeneralDema(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("InformacionGeneralDemaController - listarInformacionGeneralDema", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Danny Nazario [19-01-2022]
     * @modificado: Danny Nazario [04-02-2022]
     * @descripción: {Listar Información General por filtro}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroInformacionGeneral")
    @ApiOperation(value = "listarPorFiltroInformacionGeneral" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<InformacionGeneralDEMAEntity>> listarPorFiltroInformacionGeneral(
            @RequestParam(required = false) String nroDocumento,
            @RequestParam(required = false) String nroRucEmpresa,
            @RequestParam(required = false) String codTipoPlan,
            @RequestParam(required = false) Integer idPlanManejo) {
        log.info("InformacionGeneralDema - listarPorFiltroInformacionGeneral");
        ResultClassEntity<InformacionGeneralDEMAEntity> response = new ResultClassEntity<>();
        try {
            response = service.listarPorFiltroInformacionGeneral(nroDocumento, nroRucEmpresa, codTipoPlan, idPlanManejo);
            log.info("InformacionGeneralDema - listarPorFiltroInformacionGeneral", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("InformacionGeneralDema - listarPorFiltroInformacionGeneral", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
