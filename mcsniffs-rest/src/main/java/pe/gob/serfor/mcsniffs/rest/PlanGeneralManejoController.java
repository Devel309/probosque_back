package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.serfor.mcsniffs.service.PlanGeneralManejoService;

import javax.persistence.ParameterMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


@RestController
@RequestMapping(Urls.planGeneralManejo.BASE)
public class PlanGeneralManejoController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PlanGeneralManejoController.class);



    @Autowired
    private PlanGeneralManejoService planGeneralManejoService;


    @PostMapping(path = "/RegistrarResumenEjecutivoEscalaAlta")
    @ApiOperation(value = "RegistrarResumenEjecutivoEscalaAlta" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResponseEntity> RegistrarResumenEjecutivoEscalaAlta(@RequestParam("file") MultipartFile file ,
                                                                                                       @RequestParam(required = false) Integer idSolicitud,
                                                                                                       @RequestParam(required = false) Integer idPlanManejo,
                                                                                                       @RequestParam(required = false) Integer idTipoProceso,
                                                                                                       @RequestParam(required = false) short tipoEscala,
                                                                                                       @RequestParam(required = false) Integer idTipoPlan,
                                                                                                       @RequestParam(required = false) String fechaPresentacion,
                                                                                                       @RequestParam(required = false) Integer duracion,
                                                                                                       @RequestParam(required = false) String  fechaInicio,
                                                                                                       @RequestParam(required = false) String fechaFin,
                                                                                                       @RequestParam(required = false) Double potencial,
                                                                                                       @RequestParam(required = false) Integer idPersona,
                                                                                                       @RequestParam(required = false) Boolean accion,
                                                                                                       @RequestParam(required = false) Integer idUsuarioRegistro,
                                                                                                       @RequestParam(required = false) Integer idUsuarioModificacion,
                                                                                                       @RequestParam(required = false) String estado){


        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(fechaPresentacion);
            Date date2 = formatter.parse(fechaInicio);
            Date date3 = formatter.parse(fechaFin);

            InformacionGeneralEntity informacionGeneral = new InformacionGeneralEntity();
            PlanManejoEntity plan = new PlanManejoEntity();
            SolicitudEntity sol = new SolicitudEntity();
            TipoProcesoEntity pro = new TipoProcesoEntity();
            TipoEscalaEntity escala = new TipoEscalaEntity();
            TipoPlanEntity tipoPlan = new TipoPlanEntity();
            PersonaEntity persona = new PersonaEntity();
            sol.setIdSolicitud(idSolicitud);
            pro.setIdTipoProceso(idTipoProceso);
            escala.setTipoEscala(tipoEscala);
            tipoPlan.setIdTipoPlan(idTipoPlan);
            plan.setSolicitud(sol);
            plan.setTipoProceso(pro);
            plan.setTipoEscala(escala);
            plan.setIdPlanManejo(idPlanManejo);
            plan.setTipoPlan(tipoPlan);
            informacionGeneral.setPlanManejo(plan);
            informacionGeneral.setFechaPresentacion(date);
            informacionGeneral.setDuracion(duracion);
            informacionGeneral.setFechaInicio(date2);
            informacionGeneral.setFechaFin(date3);
            informacionGeneral.setPotencial(potencial);
            persona.setIdPersona(idPersona);
            informacionGeneral.setPersona(persona);
            informacionGeneral.setIdUsuarioRegistro(idUsuarioRegistro);
            informacionGeneral.setIdUsuarioModificacion(idUsuarioModificacion);
            informacionGeneral.setEstado(estado);

            log.info("PlanGeneralManejo - RegistrarResumenEjecutivoEscalaAlta",informacionGeneral.toString());
            response = planGeneralManejoService.RegistrarResumenEjecutivoEscalaAlta(informacionGeneral,file,accion);

            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("PlanGeneralManejo - RegistrarResumenEjecutivoEscalaAlta","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            log.error("PlanGeneralManejo -RegistrarResumenEjecutivoEscalaAlta","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/ActualizarResumenEjecutivoEscalaAlta")
    @ApiOperation(value = "ActualizarrResumenEjecutivoEscalaAlta" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResponseEntity> ActualizarResumenEjecutivoEscalaAlta(@RequestParam("file") MultipartFile file ,
                                                                                                        @RequestParam(required = false) Integer idSolicitud,
                                                                                                        @RequestParam(required = false) Integer idPlanManejo,
                                                                                                        @RequestParam(required = false) Integer idTipoProceso,
                                                                                                        @RequestParam(required = false) short tipoEscala,
                                                                                                        @RequestParam(required = false) Integer idTipoPlan,
                                                                                                        @RequestParam(required = false) String fechaPresentacion,
                                                                                                        @RequestParam(required = false) Integer duracion,
                                                                                                        @RequestParam(required = false) String  fechaInicio,
                                                                                                        @RequestParam(required = false) String fechaFin,
                                                                                                        @RequestParam(required = false) Double potencial,
                                                                                                        @RequestParam(required = false) Integer idPersona,
                                                                                                        @RequestParam(required = false) Boolean accion,
                                                                                                        @RequestParam(required = false) Integer idUsuarioRegistro,
                                                                                                        @RequestParam(required = false) Integer idUsuarioModificacion,
                                                                                                        @RequestParam(required = false) String estado){



        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(fechaPresentacion);
            Date date2 = formatter.parse(fechaInicio);
            Date date3 = formatter.parse(fechaFin);

            InformacionGeneralEntity informacionGeneral = new InformacionGeneralEntity();
            PlanManejoEntity plan = new PlanManejoEntity();
            SolicitudEntity sol = new SolicitudEntity();
            TipoProcesoEntity pro = new TipoProcesoEntity();
            TipoEscalaEntity escala = new TipoEscalaEntity();
            TipoPlanEntity tipoPlan = new TipoPlanEntity();
            PersonaEntity persona = new PersonaEntity();
            sol.setIdSolicitud(idSolicitud);
            pro.setIdTipoProceso(idTipoProceso);
            escala.setTipoEscala(tipoEscala);
            tipoPlan.setIdTipoPlan(idTipoPlan);
            plan.setSolicitud(sol);
            plan.setTipoProceso(pro);
            plan.setTipoEscala(escala);
            plan.setIdPlanManejo(idPlanManejo);
            plan.setTipoPlan(tipoPlan);
            informacionGeneral.setPlanManejo(plan);
            informacionGeneral.setFechaPresentacion(date);
            informacionGeneral.setDuracion(duracion);
            informacionGeneral.setFechaInicio(date2);
            informacionGeneral.setFechaFin(date3);
            informacionGeneral.setPotencial(potencial);
            persona.setIdPersona(idPersona);
            informacionGeneral.setPersona(persona);
            informacionGeneral.setIdUsuarioRegistro(idUsuarioRegistro);
            informacionGeneral.setIdUsuarioModificacion(idUsuarioModificacion);
            informacionGeneral.setEstado(estado);

            log.info("PlanGeneralManejo - ActualizarResumenEjecutivoEscalaAlta",informacionGeneral.toString());

            response = planGeneralManejoService.ActualizarResumenEjecutivoEscalaAlta(informacionGeneral,file,accion);
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("PlanGeneralManejo - ActualizarResumenEjecutivoEscalaAlta","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            log.error("PlanGeneralManejo -ActualizarResumenEjecutivoEscalaAlta","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/ObtenerResumenEjecutivoEscalaAlta")
    @ApiOperation(value = "ObtenerResumenEjecutivoEscalaAlta" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResponseEntity> ObtenerResumenEjecutivoEscalaAlta(@RequestBody InformacionGeneralEntity informacionGeneral){
        log.info("PlanGeneralManejo - ObtenerResumenEjecutivoEscalaAlta",informacionGeneral.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = planGeneralManejoService.ObtenerResumenEjecutivoEscalaAlta(informacionGeneral);
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("PlanGeneralManejo - ObtenerResumenEjecutivoEscalaAlta","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            log.error("PlanGeneralManejo -ObtenerResumenEjecutivoEscalaAlta","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/ActualizarDuracionResumenEjecutivo")
    @ApiOperation(value = "ActualizarDuracionResumenEjecutivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResponseEntity> ActualizarDuracionResumenEjecutivo(@RequestBody InformacionGeneralEntity informacionGeneral){
        log.info("PlanGeneralManejo - ActualizarDuracionResumenEjecutivo",informacionGeneral.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = planGeneralManejoService.ActualizarDuracionResumenEjecutivo(informacionGeneral);
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("PlanGeneralManejo - ActualizarDuracionResumenEjecutivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            log.error("PlanGeneralManejo -ActualizarDuracionResumenEjecutivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarAspectoComplementarioPlanManejo")
    @ApiOperation(value = "actualizarAspectoComplementarioPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResponseEntity> actualizarAspectoComplementarioPlanManejo(@RequestBody PlanManejoEntity planManejo){
        log.info("PlanGeneralManejo - ActualizarAspectoComplementarioPlanManejo",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = planGeneralManejoService.ActualizarAspectoComplementarioPlanManejo(planManejo);
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("PlanGeneralManejo - ActualizarAspectoComplementarioPlanManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            log.error("PlanGeneralManejo -ActualizarAspectoComplementarioPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/obtenerPlanManejo")
    @ApiOperation(value = "obtenerPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerPlanManejo(@RequestBody PlanManejoEntity planManejo){
        log.info("PlanGeneralManejo - ObtenerPlanManejo",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = planGeneralManejoService.ObtenerPlanManejo(planManejo);
            log.info("PlanGeneralManejo - ObtenerPlanManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("PlanGeneralManejo -ObtenerPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
