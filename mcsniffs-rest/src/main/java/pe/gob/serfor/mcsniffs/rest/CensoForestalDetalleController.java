package pe.gob.serfor.mcsniffs.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Anexo2CensoEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo3CensoEntity;
import pe.gob.serfor.mcsniffs.entity.CensoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CensoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoAppEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadesDeAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Anexo2Dto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Anexo2PGMFDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Anexo3PGMFDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.AprovechamientoRFNMDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CensoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ContratoTHDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.DatosTHDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.EspecieNombreDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ListaEspecieDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosAnexo6;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosArbolesAprovechablesMaderables;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosEspeciesMuestreoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ResultadosRecursosForestalesMaderablesDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.TablaAnexo3PGMF;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CensoForestalDetalleService;

@RestController
@RequestMapping(Urls.censoForestal.BASE)

public class CensoForestalDetalleController extends AbstractRestController {

    @Autowired
    private CensoForestalDetalleService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CensoForestalDetalleController.class);


    /**
     * @autor: Roberto Meiggs Sotelo [09-08-2021]
     * @modificado: Roberto Meiggs Sotelo [17-08-2021]
     * @descripción: {Método creado para sincronizar datos a la tabla censo forestal detalle, creado para el app movil}
     * @param: List<CensoForestalDetalleEntity> listCensoForestal, Integer IdUsuarioRegistro
     * @return: ResultClassEntity<String>
     */
    @PostMapping(path = "/sincronizarCensoForestalDetalle")
    @ApiOperation(value = "Sincronizar Censo Forestal Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> SincronizarCensoForestalDetalle(
            @RequestBody List<CensoForestalDetalleEntity> listCensoForestal,
            @RequestParam(required = true) Integer IdUsuarioRegistro,
            @RequestParam(required = true) Integer IdCensoForestal
            ) {

        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = service.RegistrarIndividualCensoForestalDetalle(listCensoForestal, IdUsuarioRegistro, IdCensoForestal);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception Ex) {

            log.error("CensoForestalDetalleController - SincronizarCensoForestalDetalle",
                    "Ocurrió un error al guardar en: " + Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Tomas Casas [01-02-2022]
     * @modificado:
     * @descripción: {Método creada para obtener Censo Comercial Aprovechamiento Maderable}
     *
     * @return: ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>
     */
    @PostMapping(path = "/CensoComercialAprovechamientoMaderable")
    @ApiOperation(value = "Obtener Censo Comercial Aprovechamiento Maderable", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>  listaCensoComercialAprovechamientoMaderable(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaCensoComercialAprovechamientoMaderable(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaCensoComercialAprovechamientoMaderable",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [19-01-2022]
     * @modificado:
     * @descripción: {Método creado para actualizar Censo Forestal}
     * @return: ResponseEntity<ResultClassEntity>
     */
    @PostMapping(path = "/actualizarCensoForestalDetalle")
    @ApiOperation(value = "Actualizar Censo Forestal Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarCensoForestalDetalle(
            @RequestParam(required = true) Integer idUsuarioRegistro,
            @RequestParam(required = true) Integer idCensoForestalDetalle ,
            @RequestBody CensoForestalDetalleEntity censoForestalDetalleEntity
    ) {
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = service.ActualizarIndividualCensoForestalDetalle(idUsuarioRegistro, idCensoForestalDetalle,censoForestalDetalleEntity);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("CensoForestalDetalleController - actualizarCensoForestalDetalle",
                    "Ocurrió un error al guardar en: " + Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Tomas Casas [20-01-2022]
     * @modificado:
     * @descripción: {Método creado para registrar Censo Forestal}
     * @return: ResponseEntity<ResultClassEntity>
     */
    @PostMapping(path = "/registrarCensoForestalDetalle")
    @ApiOperation(value = "Registrar Censo Forestal Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity>  registrarCensoForestalDetalle(
            @RequestParam(required = true) Integer idUsuarioRegistro,
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestBody CensoForestalDetalleEntity censoForestalDetalleEntity
    ) {
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = service.RegistrarCensoForestalDetalle(idUsuarioRegistro, idPlanManejo,censoForestalDetalleEntity);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("CensoForestalDetalleController - actualizarCensoForestalDetalle",
                    "Ocurrió un error al guardar en: " + Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Tomas Casas [20-01-2022]
     * @modificado:
     * @descripción: {Método creado para  eliminar Censo Forestal detalle}
     * @return: ResponseEntity<ResultClassEntity>
     */
    @PostMapping(path = "/eliminarCensoForestalDetalle")
    @ApiOperation(value = "Eliminar Censo Forestal Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> eliminarCensoForestalDetalle(
            @RequestParam(required = true) Integer idUsuarioElimina,
            @RequestParam(required = true) Integer idCensoForestalDetalle
    ) {
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = service.EliminarCensoForestalDetalle(idUsuarioElimina, idCensoForestalDetalle);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("CensoForestalDetalleController - actualizarCensoForestalDetalle",
                    "Ocurrió un error al guardar en: " + Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Roberto Meiggs Sotelo [31-07-2021]
     * @modificado:
     * @descripción: {Método creado para cargar masivamente la tabla censo forestal}
     * @param: MultipartFile file, Integer IdUsuarioRegistro
     * @return: ResultClassEntity<String>
     */
    @PostMapping(path = "/registrarMasivoCensoForestalDetalle")
    @ApiOperation(value = "Cargar masiva de censo forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResultClassEntity<String> RegistrarMasivoCensoForestalDetalle(@RequestParam("file") MultipartFile file,
            @RequestParam("IdUsuarioRegistro") Integer IdUsuarioRegistro,
            @RequestParam("tipoCenso") String tipoCenso) {
        try {
            return service.RegistrarMasivoCensoForestalDetalle(file, IdUsuarioRegistro,tipoCenso);
        } catch (Exception ex) {
            ResultClassEntity<String> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - RegistrarMasivoCensoForestalDetalle",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Roberto Meiggs Sotelo [04-08-2021]
     * @modificado:
     * @descripción: {Método creada para consultar el tipo de bosque}
     *
     * @return: ResultClassEntity<List<ParametroValorEntity>>
     */
    @PostMapping(path = "/listarTipoBosque")
    @ApiOperation(value = "Lista tipo de bosque")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ParametroValorEntity>> ListarTipoBosque() {
        try {
            return service.ListarTipoBosque();
        } catch (Exception ex) {
            ResultClassEntity<List<ParametroValorEntity>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListarTipoBosque", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Roberto Meiggs Sotelo [05-08-2021]
     * @modificado:
     * @descripción: {Método creada para listar Especie nombre}
     *
     * @return: ResultClassEntity<EspecieNombreDto>
     */
    @PostMapping(path = "/listarEspecieNombre")
    @ApiOperation(value = "Lista especie nombre")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<EspecieNombreDto> ListarEspecieNombre() {
        try {
            return service.ListarEspecie();
        } catch (Exception ex) {
            ResultClassEntity<EspecieNombreDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListarEspecieNombre",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Roberto Meiggs Sotelo [17-08-2021]
     * @modificado:
     * @descripción: {Método creada para obtener la cabecera del censo forestal}
     *
     * @return: ResultClassEntity<CensoForestalDto>
     */
    @PostMapping(path = "/obtenerCensoForestal")
    @ApiOperation(value = "Obtener censo forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<CensoForestalDto> CensoForestalObtener() {
        try {
            return service.CensoForestalObtener();
        } catch (Exception ex) {
            ResultClassEntity<CensoForestalDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - CensoForestalObtener",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Roberto Meiggs Sotelo [18-08-2021]
     * @modificado:
     * @descripción: {Método creada para obtener lista de contratos}
     *
     * @return: ResultClassEntity<List<ContratoTHDto>>
     */
    @PostMapping(path = "/listaTHContratosObtener")
    @ApiOperation(value = "Obtener Listado Contratos Titulos Habilitantes", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ContratoTHDto>> listaTHContratosObtener(@RequestBody ContratoTHDto data) {

        try {
            return service.ListaTHContratosObtener(data);
        } catch (Exception ex) {
            ResultClassEntity<List<ContratoTHDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListaTHContratosObtener",
                    "Ocurrió un error en: " + ex.getMessage());
            //result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas  [14-01-2022]
     * @modificado:
     * @descripción: {Método para Obtener Tabla Anexo 3 PGMF Fustales}
     *
     * @return: ResultClassEntity<List<TablaAnexo3PGMF>>
     */
    @PostMapping(path = "/ObtenerTablaAnexo3PGMFFustales")
    @ApiOperation(value = "Obtener Tabla Anexo 3 PGMF Fustales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<TablaAnexo3PGMF>>  ObtenerTablaAnexo3PGMFFustales(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ObtenerTablaAnexo3PGMFFustales(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<TablaAnexo3PGMF>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ObtenerTablaAnexo3PGMFFustales",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Tomas Casas  [14-01-2022]
     * @modificado:
     * @descripción: {Método para Obtener Tabla Anexo 3 PGMF VolumenPotencial}
     *
     * @return: ResultClassEntity<List<TablaAnexo3PGMF>>
     */
    @PostMapping(path = "/ObtenerTablaAnexo3PGMFVolumenPotencial")
    @ApiOperation(value = "Obtener Tabla Anexo 3 PGMF Volumen Potencial", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<TablaAnexo3PGMF>>  ObtenerTablaAnexo3PGMFVolumenPotencial(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ObtenerTablaAnexo3PGMFVolumenPotencial(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<TablaAnexo3PGMF>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ObtenerTablaAnexo3PGMFVolumenPotencial",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas  [14-01-2022]
     * @modificado:
     * @descripción: {Método creada para obtener Lista Especies Inventariadas Maderables}
     *
     * @return: ResultClassEntity<List<ListaEspecieDto>>
     */
    @PostMapping(path = "/ListaEspeciesInventariadasMaderablesObtener")
    @ApiOperation(value = "Obtener Lista Especies Inventariadas Maderables", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ListaEspecieDto>> ListaEspeciesInventariadasMaderablesObtener(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaEspeciesInventariadasMaderablesObtener(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListaEspeciesInventariadasMaderablesObtener",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }



    /**
     * @autor: Roberto Meiggs Sotelo [18-08-2021]
     * @modificado:
     * @descripción: {Método creada para obtener plan de manejo por contrato}
     *
     * @return: ResultClassEntity<List<PlanManejoDto>>
     */
    @PostMapping(path = "/listaPlanManejoPorContratoObtener")
    @ApiOperation(value = "Obtener Listado Plan Manejo por Contrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<PlanManejoAppEntity>> ListaPlanManejoPorContratoObtener(@RequestParam("idContrato") Integer idContrato,@RequestParam("tipoPlanManejo") String  tipoPlanManejo,@RequestParam("tipoCensoForestal")  String tipoCensoForestal) {
        try {
            return service.ListaPlanManejoPorContratoObtener(idContrato,tipoPlanManejo, tipoCensoForestal);
        } catch (Exception ex) {
            ResultClassEntity<List<PlanManejoAppEntity>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListaPlanManejoPorContratoObtener",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Roberto Meiggs Sotelo [18-08-2021]
     * @modificado:
     * @descripción: {Método creada para obtener plan de manejo por contrato}
     *
     * @return: ResultClassEntity<List<PlanManejoDto>>
     */
    @PostMapping(path = "/listaDatosTH")
    @ApiOperation(value = "Obtener Lista datos TH", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<DatosTHDto> ListaDatosTHObtener(@RequestParam("idPlanDeManejo") Integer idPlanDeManejo) {
        try {
            return service.ListaDatosTHObtener(idPlanDeManejo);
        } catch (Exception ex) {
            ResultClassEntity<DatosTHDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListaDatosTHObtener",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [23-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener CensoComercial :Lista de especies ,ResultadosRecursosForestalesMaderables, AprovechamientoRecursosForestalesNoMaderables
     ,VolumenCorta mediante idPlanManejo y tipo de plan}
     *
     * @return: ResultClassEntity<ActividadesDeAprovechamientoDto>
     */
    @PostMapping(path = "/actividadesDeAprovechamiento")
    @ApiOperation(value = "Obtener Censo Comercial y Volumen de Corta", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ActividadesDeAprovechamientoDto> listaDatosActividadesAprovechamiento(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
             @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaDatosActividadesAprovechamiento(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ActividadesDeAprovechamientoDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaDatosActividadesAprovechamiento",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [28-01-2022]
     * @modificado:
     * @descripción: {Método creada para obtener Resultados Arboles Aprovechables Maderables}
     * @return: ResultClassEntity<ResultadosArbolesAprovechablesMaderables>
     */
    @PostMapping(path = "/PotencialProduccionForestal/ArbolesAprovechablesMaderables")
    @ApiOperation(value = "Obtener Resultados Arboles Aprovechables Maderables", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ResultadosArbolesAprovechablesMaderables>  listaArbolesAprovechablesMaderables(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosArbolesAprovechablesMaderables(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ResultadosArbolesAprovechablesMaderables> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaResultadosArbolesAprovechablesMaderables",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [02-02-2022]
     * @modificado:
     * @descripción: {Método creada para obtener Resultados Aprovechable Forestal No Maderable}
     * @return: ResultClassEntity<ResultadosArbolesAprovechablesMaderables>
     */
    @PostMapping(path = "/PotencialProduccionForestal/AprovechableForestalNoMaderable")
    @ApiOperation(value = "Obtener Resultados Aprovechable Forestal No Maderable", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ResultadosArbolesAprovechablesMaderables>  listaAprovechableForestalNoMaderable(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosAprovechableForestalNoMaderable(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ResultadosArbolesAprovechablesMaderables> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaAprovechableForestalNoMaderable",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [02-02-2022]
     * @modificado:
     * @descripción: {Método creada para obtener Resultados Censo Comercial Aprovechamiento No Maderable}
     * @return: ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>
     */
    @PostMapping(path = "/CensoComercial/AprovechamientoNoMaderable")
    @ApiOperation(value = "Obtener Resultados Aprovechable Forestal No Maderable", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>  listaCensoComercialAprovechamientoNoMaderable(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosCensoComercialAprovechamientoNoMaderable(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaCensoComercialAprovechamientoNoMaderable",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [31-01-2022]
     * @modificado:
     * @descripción: {Método creada para obtener datos de Arboles Aprovechables Fustales}
     * @return: ResultClassEntity<ResultadosArbolesAprovechablesMaderables>
     */
    @PostMapping(path = "/PotencialProduccionForestal/ArbolesAprovechablesMaderablesFustales")
    @ApiOperation(value = "Obtener Resultados Arboles Aprovechables Maderables Fustales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ResultadosArbolesAprovechablesMaderables>  listaArbolesAprovechablesFustales(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosArbolesAprovechablesFustales(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ResultadosArbolesAprovechablesMaderables> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaResultadosArbolesAprovechablesMaderables",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [28-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener los datos de POConcesiones del Anexos2}
     * @return: ResultClassEntity<List<Anexo2Dto>>
     */
    @PostMapping(path = "/POConcesiones/Anexos/Anexo2")
    @ApiOperation(value = "Obtener Anexo 2 del PO", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo2Dto>> listaDatosPOConcesionesAnexo2(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan,
               @RequestParam(required = false) Integer tipoProceso
    ) {
        try {
            return service.listaDatosPOConcesionesAnexo2(idPlanDeManejo,tipoPlan,tipoProceso);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo2Dto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaDatosPOConcesionesAnexo2",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [26-01-2022]
     * @modificado:
     * @descripción: {Método creada para listar Anexo 7 No Maderable}
     * @return: ResultClassEntity<List<Anexo2Dto>>
     */
    @PostMapping(path = "/ListaAnexo7NoMaderable")
    @ApiOperation(value = "Obtener Anexo  7 No Maderable", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo2Dto>> listaAnexo7NoMaderable(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaAnexo7NoMaderable(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo2Dto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaAnexo7NoMaderable",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Tomas Casas [15-02-2022]
     * @modificado:
     * @descripción: {Método creada para Buscar datos de  Anexo 7 No Maderable}
     * @return: ResultClassEntity<List<Anexo2Dto>>
     */
    @PostMapping(path = "/BuscarAnexo7NoMaderable")
    @ApiOperation(value = "Obtener Busqueda datos Anexo  7 No Maderable", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo2Dto>> buscarAnexo7NoMaderable(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan,
            @RequestBody Anexo2Dto data
    ) {
        try {
            return service.BuscarAnexo7NoMaderable(idPlanDeManejo,tipoPlan,data);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo2Dto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - buscarAnexo7NoMaderable",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor:  Tomas Casas [10-01-2022]
     * @modificado:
     * @descripción: { Registrar Recursos Forestales Maderable Excel }
     * @return: ResultClassEntity<ResultadosRecursosForestalesMaderablesDto>
     */
    @PostMapping(path = "/registrarRecursosForestalesMaderableExcel")
    @ApiOperation(value = "Registrar Recursos Forestales Maderable Excel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> registrarRecursosForestalesMaderableExcel(@RequestParam("file") MultipartFile file,
                                                                                                @RequestParam(required = false) Integer idPlanDeManejo,
                                                                                                @RequestParam(required = false) String tipoPlan


    ) throws IOException {
        try {
            return service.registrarRecursosForestalesMaderableExcel(file,idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<ResultadosRecursosForestalesMaderablesDto> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - registrarRecursosForestalesMaderableExcel ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos registrados  para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor:  Tomas Casas [07-01-2022]
     * @modificado:
     * @descripción: { Registra  de resultados de inventario de especies maderables por muestreo  Excel }
     * @return: ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>
     */
    @PostMapping(path = "/registrarListaEspeciesMuestreoExcel")
    @ApiOperation(value = "Registrar Lista Especies Muestreo Excel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> registrarEspecieMuestreoExcel(@RequestParam("file") MultipartFile file,
                                                                                                @RequestParam(required = false) Integer idPlanDeManejo,
                                                                                                @RequestParam(required = false) String tipoPlan


    ) throws IOException {
        try {
            return service.registrarEspecieMuestreoExcel(file,idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - registrarEspecieMuestreoExcel ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos registrados  para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor:  Tomas Casas [07-01-2022]
     * @modificado:
     * @descripción: { Registra  de resultados de inventario de especies maderables por frutales  Excel }
     * @return: ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>
     */
    @PostMapping(path = "/registrarListaEspeciesFrutalesExcel")
    @ApiOperation(value = "Registrar Lista Especies Frutales Excel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> registrarEspecieFrutalesExcel(@RequestParam("file") MultipartFile file,
                                                                                                @RequestParam(required = false) Integer idPlanDeManejo,
                                                                                                @RequestParam(required = false) String tipoPlan


    ) throws IOException {
        try {
            return service.registrarEspecieFrutalesExcel(file,idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - registrarEspecieFrutalesExcel ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos registrados  para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Tomas Casas [04-01-2022]
     * @modificado:
     * @descripción: {Método creada para  adjuntar excel de Formato de PGMF formulado  del Anexos2}
     * @return: ResponseEntity<ResultClassEntity>
     */
    @PostMapping ("/FormatoPGMFFormuladoAnexo2Excel")
    @ApiOperation(value = "Registrar Formato PGMFA Anexo 2 Adjuntar Excel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> registrarFormatoPGMFAAnexo2Excel(@RequestParam("file") MultipartFile file,
                                                                                                        @RequestParam(required = false) Integer idPlanDeManejo,
                                                                                                        @RequestParam(required = false) String tipoPlan


    ) throws IOException {

        ResultClassEntity obj = new ResultClassEntity();
        try {
            obj =service.registrarFormatoPGMFAAnexo2Excel(file, idPlanDeManejo, tipoPlan);
            return new org.springframework.http.ResponseEntity<>(obj, HttpStatus.OK);
        } catch (Exception e) {
            obj.setSuccess(false);
            obj.setMessage("Se produjo un error al cargar archivo Anexo 2");
            obj.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new org.springframework.http.ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Tomas Casas [22-02-2022]
     * @modificado:
     * @descripción: {Método creada para  carga Tipo Plan Masivo Excel Anexo 4}
     * @return: ResultClassEntity<List<CensoForestalDetalleEntity>>
     */
    @PostMapping ("/cargaTipoPlanMasivoExcel")
    @ApiOperation(value = "Carga Tipo Plan Masivo Excel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResultClassEntity<List<CensoForestalDetalleEntity>> cargaTipoPlanMasivoExcel(@RequestParam("file") MultipartFile file
    ) throws IOException {
        try {
            return service.cargaTipoPlanMasivoExcel(file);
        } catch (Exception e) {
            ResultClassEntity<List<CensoForestalDetalleEntity>>  result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - cargaTipoPlanMasivoExcel",
                    "Ocurrió un error en: " + e.getMessage());
            result.setMessage("No existen datos sincronizados ");
            result.setInnerException(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Wilfredo Elias [30-03-2022]
     * @modificado:
     * @descripción: {Método creada para  carga Tipo Plan Masivo Excel Anexo 4}
     * @return: ResultClassEntity<List<CensoForestalDetalleEntity>>
     */
    @PostMapping ("/cargaMasivaTipoPlanExcel")
    @ApiOperation(value = "Carga Masiva Tipo Plan Excel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResultClassEntity<List<CensoForestalDetalleEntity>> cargaMasivaTipoPlanExcel(@RequestParam("file") MultipartFile file, @RequestParam("idPlanManejo") Integer idPlanManejo
    ) throws IOException {
        try {
            return service.cargaMasivaTipoPlanExcel(file,idPlanManejo);
        } catch (Exception e) {
            ResultClassEntity<List<CensoForestalDetalleEntity>>  result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - cargaTipoPlanMasivoExcel",
                    "Ocurrió un error en: " + e.getMessage());
            result.setMessage("No existen datos sincronizados ");
            result.setInnerException(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Tomas Casas [29-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Formato de PGMF formulado  del Anexos2}
     * @return: ResultClassEntity<List<Anexo2PGMFDto>>
     */
    @PostMapping(path = "/FormatoPGMFformulado/Anexos/Anexo2")
    @ApiOperation(value = "Obtener Formato PGMF Anexos 2", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo2PGMFDto>> listaFormatoPGMFAnexo2(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan,
            @RequestParam("idTipoBosque") String  idTipoBosque
    ) {
        try {
            return service.ListaFormatoPGMFAnexo2(idPlanDeManejo,tipoPlan,idTipoBosque);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo2PGMFDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaDatosPOConcesionesAnexo2",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [22-02-2022]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Formato de PGMF formulado  del Anexos3 PMFIC}
     * @return: ResultClassEntity<List<Anexo2PGMFDto>>
     */
    @PostMapping(path = "/PMFIC/Anexos/Anexo3")
    @ApiOperation(value = "Obtener Anexo 3 PMFIC", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<List<Anexo2PGMFDto>>> listaTablasPMFICAnexo3(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaTablasPMFICAnexo3(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<List<Anexo2PGMFDto>>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaTablasPMFICAnexo3",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [22-02-2022]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Formato de PGMF formulado  del Anexos 4 PMFIC}
     * @return: ResultClassEntity<List<Anexo2PGMFDto>>
     */
    @PostMapping(path = "/PMFIC/Anexos/Anexo4")
    @ApiOperation(value = "Obtener Anexo 4 PMFIC", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<List<Anexo3PGMFDto>>> listaTablasPMFICAnexo4(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaTablasPMFICAnexo4(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<List<Anexo3PGMFDto>>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaTablasPMFICAnexo4",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/pgmfInventarioExploracionVolumenPotencial")
    @ApiOperation(value = "pgmfInventarioExploracionVolumenPotencial", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo2PGMFDto>> pgmfInventarioExploracionVolumenPotencial(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan,
            @RequestParam("idTipoBosque") String  idTipoBosque
    ) {
        try {
            return service.PGMFInventarioExploracionVolumenP(idPlanDeManejo,tipoPlan,idTipoBosque);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo2PGMFDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaDatosPOConcesionesAnexo2",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Tomas Casas [04-01-2022]
     * @modificado:
     * @descripción: {Método creada para  adjuntar excel de Formato de PGMF formulado  del Anexos 3}
     * @return: ResponseEntity<ResultClassEntity>
     */
    @PostMapping ("/FormatoPGMFFormuladoAnexo3Excel")
    @ApiOperation(value = "Registrar Formato PGMFA Anexo 3 Adjuntar Excel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> registrarFormatoPGMFAAnexo3Excel(@RequestParam("file") MultipartFile file,
                                                                                                       @RequestParam(required = false) Integer idPlanDeManejo,
                                                                                                       @RequestParam(required = false) String tipoPlan


    ) throws IOException {

        ResultClassEntity obj = new ResultClassEntity();
        try {
            obj =service.registrarFormatoPGMFAAnexo3Excel(file, idPlanDeManejo, tipoPlan);
            return new org.springframework.http.ResponseEntity<>(obj, HttpStatus.OK);
        } catch (Exception e) {
            obj.setSuccess(false);
            obj.setMessage("Se produjo un error al cargar archivo Anexo 3 ");
            obj.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new org.springframework.http.ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Tomas Casas [29-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Formato de PGMF formulado  del Anexos 3}
     * @return: ResultClassEntity<List<Anexo3PGMFDto>>
     */
    @PostMapping(path = "/Anexo3")
    @ApiOperation(value = "Obtener Formato PGMF Anexos 3", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo3PGMFDto>> listaFormatoPGMFAnexo3(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan,
            @RequestParam("idTipoBosque") String  idTipoBosque
    ) {
        try {
            return service.ListaFormatoPGMFAnexo3(idPlanDeManejo,tipoPlan,idTipoBosque);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo3PGMFDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaDatosPOConcesionesAnexo3",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor:  [29-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Formato de PGMF formulado  del Anexos 3}
     * @return: ResultClassEntity<List<Anexo3PGMFDto>>
     */
    @PostMapping(path = "/PGMFInventarioExploracionFustales")
    @ApiOperation(value = "Obtener Formato PGMF Anexos 3", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo3PGMFDto>> PGMFInventarioExploracionFustales(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan,
            @RequestParam("idTipoBosque") String  idTipoBosque
    ) {
        try {
            return service.PGMFInventarioExploracionFustales(idPlanDeManejo,tipoPlan,idTipoBosque);
        } catch (Exception ex) {
            ResultClassEntity<List<Anexo3PGMFDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - PGMFInventarioExploracionFustales",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan+" Id Bosque "+idTipoBosque);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [27-01-2022]
     * @modificado:
     * @descripción: {Método creada para obtener los datos de Anexo 6 No Maderable}
     * @return: ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>
     */
    @PostMapping(path = "/Anexo6")
    @ApiOperation(value = "Obtener Anexo 6 No Maderable", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public  ResultClassEntity<List<ResultadosAnexo6>>  listaAnexo6 (
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaAnexo6(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<ResultadosAnexo6>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaAnexo6 ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Tomas Casas [30-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Inventario Especies Maderables Muestreo}
     * @return: ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>
     */
    @PostMapping(path = "/InfoPotencialProduccionForestal/ResultadosEspecieMuestreo")
    @ApiOperation(value = "Obtener Inventario Especies Maderables Muestreo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> listaResultadosEspecieMuestreo (
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosEspecieMuestreo(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ResultadosEspeciesMuestreoDto ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [30-12-2021]
     * @modificado:
     * @descripción: {Método creada para obtener los datos Inventario Especies Maderables Muestreo Frutales}
     * @return: ResultClassEntity<List<ResultadosEspeciesMuestreoDto>>
     */
    @PostMapping(path = "/InfoPotencialProduccionForestal/ResultadosEspecieFrutales")
    @ApiOperation(value = "Obtener Inventario Especies Maderables Muestreo Frutales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> listaResultadosEspecieFrutales(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosEspecieFrutales(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<ResultadosEspeciesMuestreoDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaResultadosEspecieFrutales ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Tomas Casas [06-01-2022]
     * @modificado:
     * @descripción: {Método creada para obtener Resultados Aprovechamiento RFNM }
     * @return: ResultClassEntity<List<AprovechamientoRFNMDto>>
     */
    @PostMapping(path = "/AprovechamientoRFNM/Especies")
    @ApiOperation(value = "Obtener Aprovechamiento RFNM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<AprovechamientoRFNMDto>> listaResultadosAprovechamientoRFNM(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {
        try {
            return service.ListaResultadosAprovechamientoRFNM(idPlanDeManejo,tipoPlan);
        } catch (Exception ex) {
            ResultClassEntity<List<AprovechamientoRFNMDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - AprovechamientoRFNMDto ",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setMessage("No existen datos sincronizados para el Plan de Manejo "+idPlanDeManejo+" Tipo de plan "+tipoPlan);
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    @PostMapping(path = "/ListaTipoRecursoForestal")
    @ApiOperation(value = "Listar recurso", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ListaEspecieDto>> ListaTipoRecursoForestal(
            @RequestParam("tipoEspecie") String tipoEspecie,
            @RequestParam("tipoCenso") String tipoCenso) {
        try {
            return service.ListaTipoRecursoForestal(tipoEspecie, tipoCenso);
        } catch (Exception ex) {

            ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListaTipoRecursoForestal", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/ListaTipoRecursoForestalManejo")
    @ApiOperation(value = "Listar recurso", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ListaEspecieDto>> ListaTipoRecursoForestalManejo(
            @RequestParam(required = true)  Integer idPlanManejo,
            @RequestParam("tipoCenso") String tipoCenso,
            @RequestParam("tipoEspecie") String tipoEspecie) {
        try {
            return service.ListaTipoRecursoForestalManejo(idPlanManejo, tipoCenso,tipoEspecie);
        } catch (Exception ex) {

            ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListaTipoRecursoForestal", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/listaResumenEspecies")
    @ApiOperation(value = "Listar resumen especies", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ListaEspecieDto>> ListaResumenEspecies(
            @RequestParam(required = true)  Integer idPlanManejo,
            @RequestParam("tipoCenso") String tipoCenso,
            @RequestParam("tipoEspecie") String tipoEspecie) {
        try {
            return service.ListaResumenEspecies(idPlanManejo, tipoCenso,tipoEspecie);
        } catch (Exception ex) {

            ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaResumenEspecies", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/listaResumenEspeciesInventario")
    @ApiOperation(value = "Listar resumen especies Inventario", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ListaEspecieDto>> ListaResumenEspeciesInventario(
            @RequestParam(required = true)  Integer idPlanManejo,
            @RequestParam("tipoRecurso") String tipoRecurso,
            @RequestParam("tipoInventario") String tipoInventario) {
        try {
            return service.ListaResumenEspeciesInventario(idPlanManejo, tipoRecurso,tipoInventario);
        } catch (Exception ex) {

            ResultClassEntity<List<ListaEspecieDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaResumenEspeciesInventario", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/listaCensoForestalDetalle")
    @ApiOperation(value = "Listar Censo Forestal Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<CensoForestalDetalleEntity>> ListarCensoForestalDetalle(@RequestParam("idPlanManejo") Integer idPlanManejo,@RequestParam("tipoCenso") String tipoCenso,@RequestParam("tipoRecurso") String tipoRecurso) {
        try {
            return service.ListarCensoForestalDetalle(idPlanManejo, tipoCenso,tipoRecurso);
        } catch (Exception ex) {

            ResultClassEntity<List<CensoForestalDetalleEntity>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listaCensoForestalDetalle", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/sincronizacionCenso")
    @ApiOperation(value = "Listar esincronizacionCenso", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<CensoForestalEntity>> sincronizacionCenso(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false)  String tipoCenso,
            @RequestParam(required = false)  String tipoRecurso) {
        try {
            return service.SincronizacionCenso(idPlanManejo, tipoCenso,tipoRecurso);
        } catch (Exception ex) {

            ResultClassEntity<List<CensoForestalEntity>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - SincronizacionCenso", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/ListarCensoForestalAnexo2")
    @ApiOperation(value = "Listar Anexo2", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Anexo2CensoEntity>> ListarCensoForestalAnexo2(@RequestParam("idPlanManejo") Integer idPlanManejo,@RequestParam("tipoEspecie") String tipoEspecie) {
        try {
            return service.ListarCensoForestalAnexo2(idPlanManejo,tipoEspecie);
        } catch (Exception ex) {

            ResultClassEntity<List<Anexo2CensoEntity>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - ListarCensoForestalAnexo2", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }



    @PostMapping(path = "/ListarCensoForestalAnexo2Totales")
    @ApiOperation(value = "ListarCensoForestalAnexo2Totales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<Anexo3CensoEntity>> ListarCensoForestalAnexo2VolumenTotal(@RequestParam("idPlanManejo") Integer idPlanManejo,@RequestParam("tipoEspecie") String tipoEspecie){
        ResultEntity<Anexo3CensoEntity> result = new ResultEntity<Anexo3CensoEntity>();
        try {
            result = service.ListarCensoForestalAnexo2VolumenTotal(idPlanManejo,tipoEspecie);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CensoForestalDetalleController - ListarCensoForestalAnexo2Totales", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }



    @PostMapping(path = "/ListarCensoForestalAnexo3")
    @ApiOperation(value = "listar Anexo3", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<Anexo3CensoEntity>> ListarCensoForestalAnexo3(@RequestParam("idPlanManejo") Integer idPlanManejo,@RequestParam("tipoEspecie") String tipoEspecie){
        ResultEntity<Anexo3CensoEntity> result = new ResultEntity<Anexo3CensoEntity>();
        try {
            result = service.ListarCensoForestalAnexo3(idPlanManejo,tipoEspecie);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CensoForestalDetalleController - ListarCensoForestalAnexo3", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarEspecieXCenso")
    @ApiOperation(value = "listarEspecieXCenso", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> listarEspecieXCenso(@RequestBody CensoForestalDto censoForestalDto){
        ResultEntity result = new ResultEntity();
        try {
            result = service.listarEspecieXCenso(censoForestalDto);

            log.info("CensoForestal - listarEspecieXCenso","Proceso realizado correctamente");
            if (result.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("CensoForestal - listarEspecieXCenso", "Ocurrió un error al generar en: "+e.getMessage());
            result.setIsSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [07-01-2022]
     * @modificado:
     * @descripción: { Lista Potencial Producción Resultados Fustales }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/infoPotencialProduccionForestal/listarResultadosFustales")
    @ApiOperation(value = "listarResultadosFustales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarResultadosFustales(@RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                             @RequestParam("tipoPlan") String tipoPlan) {
        log.info("CensoForestal - listarResultadosFustales");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarResultadosFustales(idPlanManejo, tipoPlan);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarResultadosFustales", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarResultadosFustales", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarProyeccionCosecha")
    @ApiOperation(value = "listarProyeccionCosecha", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarProyeccionCosecha(@RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                            @RequestParam("tipoPlan") String tipoPlan) {
        log.info("CensoForestal - listarResultadosFustales");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.ListarProyeccionCosecha(idPlanManejo, tipoPlan);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarProyeccionCosecha", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarProyeccionCosecha", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarVolumenComercialPromedio")
    @ApiOperation(value = "listarVolumenComercialPromedio", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarVolumenComercialPromedio(@RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                           @RequestParam("tipoPlan") String tipoPlan) {
        log.info("CensoForestal - listarVolumenComercialPromedio");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarVolumenComercialPromedio(idPlanManejo, tipoPlan);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarVolumenComercialPromedio", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarVolumenComercialPromedio", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarEspecieVolumenComercialPromedio")
    @ApiOperation(value = "listarEspecieVolumenComercialPromedio", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarEspecieVolumenComercialPromedio(@RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                  @RequestParam("tipoPlan") String tipoPlan) {
        log.info("CensoForestal - listarEspecieVolumenComercialPromedio");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarEspecieVolumenComercialPromedio(idPlanManejo, tipoPlan);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarEspecieVolumenComercialPromedio", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarEspecieVolumenComercialPromedio", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarEspecieVolumenCortaAnualPermisible")
    @ApiOperation(value = "listarEspecieVolumenCortaAnualPermisible", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarEspecieVolumenCortaAnualPermisible(@RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                  @RequestParam("tipoPlan") String tipoPlan) {
        log.info("CensoForestal - listarEspecieVolumenComercialPromedio");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarEspecieVolumenCortaAnualPermisible(idPlanManejo, tipoPlan);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarEspecieVolumenCortaAnualPermisible", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarEspecieVolumenCortaAnualPermisible", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Tomas Casas [24-03-2022]
     * @modificado:
     * @descripción: { Lista Aprovechamiento Recurso Forestal No Maderable Resumen  }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/DEMA/AprovechamientoRecursoForestalNoMaderableResumen")
    @ApiOperation(value = "Lista Aprovechamiento Recurso Forestal No Maderable Resumen ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarAprovechamientoRecursoForestalNoMaderableResumen(
            @RequestParam("idPlanManejo") Integer idPlanManejo,
            @RequestParam("tipoPlan") String tipoPlan,
            @RequestParam(required = false) Integer tipoProceso
    ) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.ListaAprovechamientoForestalNMResumen( idPlanManejo,tipoPlan, tipoProceso);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarAprovechamientoRecursoForestalNoMaderableResumen", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarResultadosFustales", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Tomas Casas [25-03-2022]
     * @modificado:
     * @descripción: { Lista Resultados Inventarios por parcela   }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/PMFI/ResultadosInventarios")
    @ApiOperation(value = "Lista  Resultados Inventarios por parcela   ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarResultadosInventarios(
            @RequestParam("idPlanManejo") Integer idPlanManejo,
            @RequestParam("tipoPlan") String tipoPlan,
            @RequestParam(required = false) Integer tipoProceso
    ) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.ListarResultadosInventarios( idPlanManejo,tipoPlan, tipoProceso);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarAprovechamientoRecursoForestalNoMaderableResumen", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarResultadosFustales", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarRecursosMaderablesCensoForestalDetalle")
    @ApiOperation(value = "listarRecursosMaderablesCensoForestalDetalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarRecursosMaderablesCensoForestalDetalle(@RequestBody CensoForestalDetalleEntity request
                                                                                            ) {
        log.info("CensoForestal - listarRecursosMaderablesCensoForestalDetalle");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarRecursosMaderablesCensoForestalDetalle(request);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarRecursosMaderablesCensoForestalDetalle", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarRecursosMaderablesCensoForestalDetalle", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarRecursosDiferentesCensoForestalDetalle")
    @ApiOperation(value = "listarRecursosDiferentesCensoForestalDetalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarRecursosDiferentesCensoForestalDetalle(@RequestBody CensoForestalDetalleEntity request
    ) {
        log.info("CensoForestal - listarRecursosDiferentesCensoForestalDetalle");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarRecursosDiferentesCensoForestalDetalle(request);
            if (result.getSuccess()) {
                log.info("CensoForestal - listarRecursosDiferentesCensoForestalDetalle", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - listarRecursosDiferentesCensoForestalDetalle", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [04-03-2022]
     * @modificado:
     * @descripción: {Método obtener la lista de Anexos de Censo Forestal Detalle}
     *
     * @return: ResultClassEntity<List<CensoForestalDetalleEntity>>
     */
    @GetMapping(path = "/obtenerInfoAnexoCensoForestalDetalle/{idPlanManejo}")
    @ApiOperation(value = "Obtener Información Anexo de Censo Forestal Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerInfoAnexoCensoForestalDetalle(@PathVariable("idPlanManejo") Integer idPlanManejo
    ) {
        log.info("CensoForestalDetalle - obtenerInfoAnexoCensoForestalDetalle", idPlanManejo.toString());
        ResultClassEntity result = new ResultClassEntity();
        CensoForestalDetalleEntity request = new CensoForestalDetalleEntity();
        try {
            request.setIdPlanManejo(idPlanManejo);
            result = service.listarCensoForestalDetalleAnexo(request);
            if (result.getSuccess()) {
                log.info("CensoForestalDetalle - obtenerInfoAnexoCensoForestalDetalle", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestalDetalle - obtenerInfoAnexoCensoForestalDetalle", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarCensoForestal")
    @ApiOperation(value = "actualizarCensoForestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarRecursosDiferentesCensoForestalDetalle(@RequestBody CensoForestalEntity request
    ) {
        log.info("CensoForestal - actualizarCensoForestal");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarCensoForestal(request);
            if (result.getSuccess()) {
                log.info("CensoForestal - actualizarCensoForestal", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestal - actualizarCensoForestal", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Tomas Casas [15-02-2022]
     * @modificado:
     * @descripción: {Método creada  la lista de los Contratos TH para el Movil}
     *
     * @return: ResultClassEntity<List<ContratoTHDto>>
     */
    @PostMapping(path = "/Movil/ListarContratoTHMovil")
    @ApiOperation(value = "Obtener lista de los Contratos TH para el Movil", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ContratoTHDto>> listarContratoTHMovil(@RequestBody ContratoTHDto data) {

        try {
            return service.ListarContratoTHMovil(data);
        } catch (Exception ex) {
            ResultClassEntity<List<ContratoTHDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listarContratoTHMovil",
                    "Ocurrió un error en: " + ex.getMessage());
            //result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Wilfredo Elias [07-04-2022]
     * @modificado:
     * @descripción: {Método Sincronizar Listado de Información de Censo}
     *
     * @return: ResultClassEntity<List<CensoForestalDetalleEntity>>
     */
    @PostMapping(path = "/listarSincroInformacionCenso")
    @ApiOperation(value = "Listar Sincronización de Información de Censo Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarSincroInformacionCenso(@RequestBody CensoForestalDetalleEntity request
    ) {
        log.info("CensoForestalDetalle - listarSincroInformacionCenso", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarSincronizacionCensoForestalDetalle(request);
            if (result.getSuccess()) {
                log.info("CensoForestalDetalle - listarSincroInformacionCenso", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CensoForestalDetalle - listarSincroInformacionCenso", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
