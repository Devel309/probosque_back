package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.AprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.NotificacionDTO;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.EvaluacionPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvalSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PlanManejoEvaluacionService;

/**
 * @autor: Renzo Gabriel Meneses Gamarra [24-09-2021]
 * @modificado:
 * @descripción: { Controlador para el Plan Manejo Evaluación }
 */
@RestController
@RequestMapping(Urls.planManejoEvaluacion.BASE)
public class PlanManejoEvaluacionController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PlanManejoEvaluacionController.class);


    @Autowired
    private PlanManejoEvaluacionService planManejoEvaluacionService;

    @GetMapping(path = "/listarPlanManejoEvaluacionDetalle")
    @ApiOperation(value = "listarPlanManejoEvaluacionDetalle", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarPlanManejoEvaluacionDetalle(@RequestParam(required = false) Integer idPlanManejoEval) {
        log.info("PlanManejoEvaluacion - listarPlanManejoEvaluacion: {}", idPlanManejoEval);
        ResultEntity result =new ResultEntity();
        try {
            result = planManejoEvaluacionService.ListarPlanManejoEvaluacionDetalle(idPlanManejoEval);
            log.info("PlanManejoEvaluacion - listarPlanManejoEvaluacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PlanManejoEvaluacion - listarPlanManejoEvaluacion: Ocurrió un error: {}", e.getMessage());
            result.setMessage(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/listarPlanManejoEvaluacionLineamiento")
    @ApiOperation(value = "listarPlanManejoEvaluacionLineamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarplanManejoEvaluacionLineamiento(@RequestBody PlanManejoEvaluacionDetalleDto param){
        log.info("PlanManejoEvaluacion - ListarplanManejoEvaluacionLineamiento",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = planManejoEvaluacionService.ListarPlanManejoEvaluacionLineamiento(param);
            log.info("PlanManejoEvaluacion - ListarplanManejoEvaluacionLineamiento","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - ListarplanManejoEvaluacionLineamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarPlanManejoEvaluacionEspecie")
    @ApiOperation(value = "listarPlanManejoEvaluacionEspecie" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarplanManejoEvaluacionEspecie(@RequestBody PlanManejoEvaluacionDto param){
        log.info("PlanManejoEvaluacion - listarPlanManejoEvaluacionEspecie",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = planManejoEvaluacionService.listarplanManejoEvaluacionEspecie(param);
            log.info("PlanManejoEvaluacion - listarPlanManejoEvaluacionEspecie","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - listarPlanManejoEvaluacionEspecie","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/registrarPlanManejoEvaluacionLineamiento")
    @ApiOperation(value = "registrarPlanManejoEvaluacionLineamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarplanManejoEvaluacionLineamiento(@RequestBody List<PlanManejoEvaluacionDetalleDto> param){
        log.info("PlanManejoEvaluacion - RegistrarplanManejoEvaluacionLineamiento",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = planManejoEvaluacionService.RegistrarPlanManejoEvaluacionLineamiento(param);
            log.info("PlanManejoEvaluacion - RegistrarplanManejoEvaluacionLineamiento","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - RegistrarplanManejoEvaluacionLineamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarPlanManejoEvaluacionDetalle")
    @ApiOperation(value = "registrarPlanManejoEvaluacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanManejoEvaluacionDetalle(@RequestBody List<PlanManejoEvaluacionDetalleDto> planManejoEvaluacionDetalle){
        log.info("Bandeja Plan operativo - registrarPlanManejoEvaluacion", planManejoEvaluacionDetalle);
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.registrarPlanManejoEvaluacionDetalle(planManejoEvaluacionDetalle);
            log.info("PlanManejoEvaluacion - registrarPlanManejoEvaluacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - registrarPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/actualizarPlanManejoEvaluacionDetalle")
    @ApiOperation(value = "actualizarPlanManejoEvaluacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarPlanManejoEvaluacion(@RequestBody List<PlanManejoEvaluacionDetalleDto> planManejoEvaluacionDetalle){
        log.info("Bandeja Plan operativo - actualizarPlanManejoEvaluacion", planManejoEvaluacionDetalle);
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.actualizarPlanManejoEvaluacionDetalle(planManejoEvaluacionDetalle);
            log.info("PlanManejoEvaluacion - actualizarPlanManejoEvaluacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - registrarPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarEstadoPlanManejoEvaluacion")
    @ApiOperation(value = "actualizarEstadoPlanManejoEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarEstadoPlanManejoEvaluacion(@RequestBody PlanManejoEvaluacionDto planManejoEvaluacionDto){
        log.info("Bandeja Plan operativo - actualizarEstadoPlanManejoEvaluacion", planManejoEvaluacionDto);
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.actualizarEstadoPlanManejoEvaluacion(planManejoEvaluacionDto);
            log.info("PlanManejoEvaluacion - actualizarEstadoPlanManejoEvaluacion","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - actualizarEstadoPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarEvaluacionCampoPlanManejoEvaluacion")
    @ApiOperation(value = "actualizarEvaluacionCampoPlanManejoEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarEvaluacionCampoPlanManejoEvaluacion(@RequestBody PlanManejoEvaluacionDto param){
        log.info("PlanManejoEvaluacion - actualizarEvaluacionCampoPlanManejoEvaluacion", param);
        ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.ActualizarEvaluacionCampoPlanManejoEvaluacion(param);
            log.info("PlanManejoEvaluacion - actualizarEvaluacionCampoPlanManejoEvaluacion","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.OK);

            } else {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - actualizarEvaluacionCampoPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/obtenerEvaluacionPlanManejo")
    @ApiOperation(value = "listarContrato", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerEvaluacionPlanManejo(@RequestBody NotificacionDTO dto) {
        log.info("listar");
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = planManejoEvaluacionService.obtenerEvaluacionPlanManejo(dto.getIdPlanManejo());
            log.info("PlanManejoEvaluacion - obtenerEvaluacionPlanManejo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PlanManejoEvaluacion - obtenerEvaluacionPlanManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/notificarObservacionesAlTitular")
    @ApiOperation(value = "listarContrato", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity notificarObservacionesAlTitular(@RequestBody NotificacionDTO dto) {
        log.info("listar");
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = planManejoEvaluacionService.notificarObservacionesAlTitular(dto);
            log.info("PlanManejoEvaluacion - notificarObservacionesAlTitular", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejoEvaluacion - notificarObservacionesAlTitular","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarPlanManejoEvaluacionArchivo")
    @ApiOperation(value = "registrar PlanManejoEvaluacion Archivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanManejoEvaluacionArchivo(
                                                                                 @RequestParam(name="file", required = false) MultipartFile file,
                                                                                 @RequestParam(required = false) Integer idPlanManejoEvaluacion,
                                                                                 @RequestParam(required = false) Integer idPlanManejoEvalDet,
                                                                                 @RequestParam(required = false) Boolean conforme,
                                                                                 @RequestParam(required = false) String codigoTipo,
                                                                                 @RequestParam(required = false) String observacion,
                                                                                 @RequestParam(required = false) Integer idUsuario,
                                                                                 @RequestParam(required = false) String idTipoDocumento,
                                                                                 @RequestParam(required = false) Integer idArchivo,
                                                                                 HttpServletRequest request1
                                                                                 ){
        log.info("Bandeja Plan operativo - registrarPlanManejoEvaluacionArchivo");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            PlanManejoEvaluacionDetalleDto eval = new PlanManejoEvaluacionDetalleDto();
            eval.setIdPlanManejoEvaluacion(idPlanManejoEvaluacion);
            eval.setIdPlanManejoEvalDet(idPlanManejoEvalDet);
            eval.setConforme(conforme);
            eval.setCodigoTipo(codigoTipo);
            eval.setObservacion(observacion);
            eval.setIdUsuarioRegistro(idUsuario);
            eval.setIdUsuarioModificacion(idUsuario);
            eval.setIdArchivo(idArchivo);


            response = planManejoEvaluacionService.registrarPlanManejoEvaluacionArchivo(eval, file, idTipoDocumento);
            log.info("PlanManejoEvaluacion - registrarPlanManejoEvaluacionArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - registrarPlanManejoEvaluacionArchivo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarPlanManejoEvaluacion")
    @ApiOperation(value = "Listar PlanManejoEvaluacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<PlanManejoEvaluacionDto>> listarPlanManejoEvaluacion(
            @RequestBody PlanManejoEvaluacionDto dto) {
        try {
            return planManejoEvaluacionService.listarPlanManejoEvaluacion(dto);
        } catch (Exception ex) {

            ResultClassEntity<List<PlanManejoEvaluacionDto>> result = new ResultClassEntity<>();
            log.error("PlanManejoEvaluacionController - listarPlanManejoEvaluacion", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/actualizarIdSolicitudOpinionPlanManejoEvaluacion")
    @ApiOperation(value = "actualizarIdSolicitudOpinionPlanManejoEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarIdSolicitudOpinionPlanManejoEvaluacion(
            @RequestBody PlanManejoEvalSolicitudOpinionDto planManejoEvalSolicitudOpinionDto
    ){
        log.info("PlanManejoEvaluacion - actualizarIdSolicitudOpinionPlanManejoEvaluacion",planManejoEvalSolicitudOpinionDto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = planManejoEvaluacionService.actualizarIdSolicitudOpcionPlanManejoEvaluacion(planManejoEvalSolicitudOpinionDto);
            log.info("PlanManejoEvaluacion - actualizarIdSolicitudOpinionPlanManejoEvaluacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - actualizarIdSolicitudOpinionPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/eliminarPlanManejoEvaluacion")
    @ApiOperation(value = "eliminar PlanManejoEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarPlanManejoEvaluacion(@RequestBody PlanManejoEvaluacionDto planManejoEvaluacionDto){
        log.info("PlanManejoEvaluacion - eliminarPlanManejoEvaluacion",planManejoEvaluacionDto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = planManejoEvaluacionService.EliminarPlanManejoEvaluacion(planManejoEvaluacionDto);
            log.info("PlanManejoEvaluacion - eliminarPlanManejoEvaluacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - eliminarPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/obtenerUltimaEvaluacion")
    @ApiOperation(value = "obtenerUltimaEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerUltimaEvaluacion(@RequestParam(required = false) Integer idPlanManejo){
        log.info("PlanManejoEvaluacion - obtenerUltimaEvaluacion", idPlanManejo);
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = planManejoEvaluacionService.obtenerUltimaEvaluacion(idPlanManejo);
            log.info("PlanManejoEvaluacion - ListarplanManejoEvaluacionLineamiento","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - ListarplanManejoEvaluacionLineamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/actualizarPlanManejoEvaluacionEstado")
    @ApiOperation(value = "actualizar PlanManejoEvaluacionEstado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarPlanManejoEvaluacionEstado(@RequestBody PlanManejoEvaluacionDto entidad){
        log.info("actualizarPlanManejoEvaluacionEstado", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = planManejoEvaluacionService.ActualizarPlanManejoEvaluacionEstado(entidad);
            log.info("PlanManejoEvaluacion Registrar","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("PlanManejoEvaluacion Registrar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/enviarEvaluacion")
    @ApiOperation(value = "enviarEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity enviarEvaluacion(@RequestBody PlanManejoEvaluacionDto planManejoEvaluacionDto ){
        log.info("PlanManejo - enviarEvaluacion",planManejoEvaluacionDto.toString());

        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.enviarEvaluacion(planManejoEvaluacionDto);
            log.info("PlanManejo - enviarEvaluacion","Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("PlanManejo - enviarEvaluacion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/remitirNotificacion")
    @ApiOperation(value = "remitirNotificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity remitirNotificacion(@RequestBody PlanManejoEvaluacionDto planManejoEvaluacionDto ){
        log.info("PlanManejo - remitirNotificacion",planManejoEvaluacionDto.toString());

        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.remitirNotificacion(planManejoEvaluacionDto);
            log.info("PlanManejo - remitirNotificacion","Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("PlanManejo - remitirNotificacion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/notificarTitular")
    @ApiOperation(value = "notificarTitular" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity notificarTitular(@RequestBody PlanManejoEvaluacionDto planManejoEvaluacionDto ){
        log.info("PlanManejo - notificarTitular",planManejoEvaluacionDto.toString());

        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.notificarTitular(planManejoEvaluacionDto);
            log.info("PlanManejo - notificarTitular","Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("PlanManejo - notificarTitular","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/validarLineamiento")
    @ApiOperation(value = "validarLineamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity validarLineamiento(@RequestBody PlanManejoEvaluacionDto planManejoEvaluacionDto ){
        log.info("PlanManejoEvaluacion - validarLineamiento",planManejoEvaluacionDto.toString());

        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoEvaluacionService.validarLineamiento(planManejoEvaluacionDto);
            log.info("PlanManejoEvaluacion - validarLineamiento","Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("PlanManejoEvaluacion - validarLineamiento","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
