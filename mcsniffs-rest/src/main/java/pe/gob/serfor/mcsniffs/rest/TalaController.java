package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.TalaService;


@RestController
@RequestMapping(Urls.tala.BASE)
public class TalaController {
    private static final org.apache.logging.log4j.Logger log = LogManager
    .getLogger(TalaController.class);

    @Autowired
    TalaService service;

    @PostMapping(path = "/registrarTala")
    @ApiOperation(value = "registrarTala", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarTala(
            @RequestBody List<TalaEntity> request) {
        log.info("TalaController - registrarTala",
                request.toString());
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            result = service.registrarTala(request);

            if (result.getSuccess()) {
                log.info("TalaController - registrarTala",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("TalaController - registrarTala",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarTalaDetalle")
    @ApiOperation(value = "registrarTalaDetalle", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarTalaDetalle(
            @RequestBody List<TalaDetalleEntity> request) {
        log.info("TalaController - registrarTalaDetalle",
                request.toString());
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            result = service.registrarTalaDetalle(request);

            if (result.getSuccess()) {
                log.info("TalaController - registrarTalaDetalle",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("TalaController - registrarTalaDetalle",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
