package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalArchivoEntity;


import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalArchivoService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.solicitudBosqueLocal.BASE)
public class SolicitudBosqueLocalArchivoController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionAreaController.class);

    @Autowired
    SolicitudBosqueLocalArchivoService serviceSolicitudBosqueLocalArchivo;

    /**
     * @autor: Wilfredo Elias [29-03-2022]
     * @modificado:
     * @descripción: {Listar Archivos de Solicitud Bosque Local}
     * @param:SolicitudBosqueLocalArchivoEntity
     */
    @PostMapping(path = "/listarAdjuntosSolicitudBosqueLocal")
    @ApiOperation(value = "Listar Archivos Adjuntos de Solicitud Bosque Local", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarAdjuntosSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalArchivoEntity request) {
        log.info("SolicitudBosqueLocalArchivo - listarSolicitudBosqueLocalArchivo", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudBosqueLocalArchivo.listarSolicitudBosqueLocalArchivo(request);
            log.info("SolicitudBosqueLocalArchivo - listarSolicitudBosqueLocalArchivo", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalArchivo - listarSolicitudBosqueLocalArchivo", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Wilfredo Elias [14-03-2022]
     * @modificado:
     * @descripción: {Obtener Info Archivo de Solicitud Bosque Local}
     * @param:SolicitudBosqueLocalArchivoEntity
     */
    @PostMapping(path = "/obtenerInfoAdjuntosSolicitudBosqueLocal")
    @ApiOperation(value = "Obtener Informacion Archivos Adjuntos de Solicitud Bosque Local", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoAdjuntosSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalArchivoEntity request) {
        log.info("SolicitudBosqueLocalArchivo - obtenerInfoAdjuntosSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudBosqueLocalArchivo.obtenerArchivoSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocalArchivo - obtenerInfoAdjuntosSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalArchivo - obtenerInfoAdjuntosSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Wilfredo Elias [14-03-2022]
     * @modificado:
     * @descripción: {Registrar Info Archivo de Solicitud Bosque Local}
     * @param: SolicitudBosqueLocalArchivoEntity
     */
    @PostMapping(path = "/registrarInfoAdjuntoSolicitudBosqueLocal")
    @ApiOperation(value = "Registrar Informacion Archivo Adjunto de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInfoAdjuntoSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalArchivoEntity request){
        log.info("SolicitudBosqueLocalArchivo - registrarInfoAdjuntoSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudBosqueLocalArchivo.registrarArchivoSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocalArchivo - registrarInfoAdjuntoSolicitudBosqueLocal","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocalArchivo - registrarInfoAdjuntoSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Wilfredo Elias [14-03-2022]
     * @modificado:
     * @descripción: {Actualizar Info Archivo de Solicitud Bosque Local}
     * @param:SolicitudBosqueLocalArchivoEntity
     */
    @PostMapping(path = "/actualizarInfoAdjuntoSolicitudBosqueLocal")
    @ApiOperation(value = "Actualizar Informacion Archivo Adjunto de Solicitud Bosque Local", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInfoAdjuntoSolicitudBosqueLocal( @RequestBody SolicitudBosqueLocalArchivoEntity request) {
        log.info("SolicitudBosqueLocalArchivo - actualizarInfoAdjuntoSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudBosqueLocalArchivo.actualizarArchivoSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocalArchivo - actualizarInfoAdjuntoSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalArchivo - actualizarInfoAdjuntoSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Wilfredo Elias [14-03-2022]
     * @modificado:
     * @descripción: {Registra/Actualiza Info Archivos de Solicitud Bosque Local}
     * @param:List<SolicitudBosqueLocalArchivoEntity>
     */
    @PostMapping(path = "/registrarInfoAdjuntosSolicitudBosqueLocal")
    @ApiOperation(value = "Registrar Informacion Archivos Adjuntos de Solicitud Bosque Local", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInfoAdjuntosSolicitudBosqueLocal( @RequestBody List<SolicitudBosqueLocalArchivoEntity> request) {
        log.info("SolicitudBosqueLocalArchivo - registrarInfoAdjuntosSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudBosqueLocalArchivo.registrarArchivosSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocalArchivo - registrarInfoAdjuntosSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalArchivo - registrarInfoAdjuntosSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [14-03-2022]
     * @modificado:
     * @descripción: {Eliminar Archivo de Solicitud Bosque Local}
     * @param:SolicitudBosqueLocalArchivoEntity
     */
    @PostMapping(path = "/eliminarInfoAdjuntoSolicitudBosqueLocal")
    @ApiOperation(value = "Eiminar Archivo Adjunto de Solicitud Bosque local", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarInfoAdjuntoSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalArchivoEntity request) {
        log.info("SolicitudBosqueLocalArchivo - eliminarInfoAdjuntoSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudBosqueLocalArchivo.eliminarArchivoSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocalArchivo - eliminarInfoAdjuntoSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalArchivo - eliminarInfoAdjuntoSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/descargarPlantillaIniciativaBosqueLocal")
    @ApiOperation(value = "descargarPlantillaCriteriosCalificacionPropuesta", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaIniciativaBosqueLocal() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = serviceSolicitudBosqueLocalArchivo.descargarPlantillaIniciativaBosqueLocal();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudBosqueLocalArchivo - descargarPlantillaIniciativaBosqueLocal", "Ocurrió un error al generar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }
}
