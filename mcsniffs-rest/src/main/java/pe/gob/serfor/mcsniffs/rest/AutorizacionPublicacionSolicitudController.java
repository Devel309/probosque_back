package pe.gob.serfor.mcsniffs.rest;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
//import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud.AutorizacionPublicacionSolicitudEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(Urls.autorizacionPublicacion.BASE)
public class AutorizacionPublicacionSolicitudController {

    @Autowired
    AutorizacionPublicacionSolicitudService serAutorizaPublicacion;

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private EmailService serMail;

    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private ProcesoPostulacionService serPostulacion;

    @Value("${spring.urlSeguridad}")
    private String urlSeguridad;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AutorizacionPublicacionSolicitudController.class);

    @GetMapping(path = "/descargarPlantillaCartaAutorizacion")
    @ApiOperation(value = "descargarPlantillaCartaAutorizacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaCartaAutorizacion(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = serAutorizaPublicacion.descargarPlantillaCartaAutorizacion();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Autorización Publicación de Solicitud - DescagarPlantillaCartaAutorizacion", "Ocurrió un error en la descarga de la plantilla. Error: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/guardarAutorizacionPublicacionSolicitud")
    @ApiOperation(value = "guardarAutorizacionPublicacionSolicitud" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity  guardarAutorizacionPublicacionSolicitud(@RequestBody AutorizacionPublicacionSolicitudEntity request) {

        ResultClassEntity result = new ResultClassEntity();
        try {
            result = serAutorizaPublicacion.guardarAutorizacionPublicacionSolicitud(request);

            if (result.getSuccess()) {
                log.info("AutorizarPublicacion - guardarAutorizacionPublicacionSolicitud", "Proceso realizado correctamente");
                return new ResponseEntity(result, HttpStatus.OK);

            } else {
                return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("AutorizarPublicacion - guardarAutorizacionPublicacionSolicitud", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/enviarAutorizacionPublicacionSolicitud")
    @ApiOperation(value = "enviar Autorizacion Publicacion Solicitud", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> enviarAutorizacionPublicacionSolicitud(@RequestBody EstatusProcesoPostulacionEntity request, HttpServletRequest request1){
        ResultClassEntity result = new ResultClassEntity();
        try {
            String token= request1.getHeader("Authorization");
            result = serAutorizaPublicacion.enviarAutorizacionPublicacionSolicitud(request, token);

            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Proceso Postulacion - EnviarAutorizacionPublicacionSolicitud", "Ocurrió un error al enviar la autorizacióm en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
}
