package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.OposicionAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.OposicionEntity;
import pe.gob.serfor.mcsniffs.entity.OposicionRequestEntity;
import pe.gob.serfor.mcsniffs.entity.OpositorRequestEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Oposicion.OposicionArchivoDTO;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.OposicionService;

@RestController
@RequestMapping(Urls.oposicion.BASE)
public class OposicionController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(OposicionController.class);
    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Reposotorio donde se implementa
     *               todo lo referente a las opociones hacia una postulacion}
     *
     */
    @Autowired
    private OposicionService servicio;

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Abner Valdez [06/15/2021]
     * @descripción: {Registrar la oposicion hacia una postulacion}
     * @param:OposicionEntity
     */

    @PostMapping(path = "/registrarOposicion")
    @ApiOperation(value = "registrarOposicion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarOposicion(@RequestBody OposicionEntity request) {
        log.info("Oposicion - registrarOposicion", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.registrarOposicion(request);

            if (result.getSuccess()) {
                log.info("Oposicion - registrarOposicion", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Oposicion - registrarOposicion", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Abner [10-12-2021]
     * @modificado: 
     * @descripción: {Registrar el detalle oposicion archivo}
     * @param:OposicionArchivoDTO
     */

    @PostMapping(path = "/registrarOposicionArchivo")
    @ApiOperation(value = "registrarOposicionArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarOposicionArchivo(@RequestBody OposicionArchivoDTO item) {
        log.info("Oposicion - registrarOposicion", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.registrarOposicionArchivo(item);

            if (result.getSuccess()) {
                log.info("Oposicion - registrarOposicionArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Oposicion - registrarOposicionArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Lista las oposiciones activas}
     * @param:OposicionRequestEntity
     */
    @PostMapping(path = "/listarOposicion")
    @ApiOperation(value = "listar Oposicion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<OposicionEntity>> listarOposicion(@RequestBody OposicionRequestEntity obj) {
        ResultEntity<OposicionEntity> result = new ResultEntity<OposicionEntity>();
        try {
            result = servicio.listarOposicion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - listarOposicion", "Ocurrió un error al listar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Registrar la respuesta de la oposicion}
     * @param:OposicionEntity
     */
    @PostMapping(path = "/registrarRespuestaOposicion")
    @ApiOperation(value = "registrar Respuesta Oposicion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarRespuestaOposicion(@RequestBody OposicionEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.registrarRespuestaOposicion(obj);
            // Se le envia un correo a la autoridad indicandole que ya el solicitante
            // contesto
            /*
             * if(result.getSuccess()){ //falta implementar por lo del correo
             * //Enviar correo
             * EmailEntity mail = new EmailEntity();
             * mail.setEmail(usuario.getCorreo());
             * mail.setContent(result.getData().getContenidoEmail());
             * mail.setSubject(result.getData().getAsuntoEmail());
             * Boolean enviarmail = enviarcorreo.sendEmail(mail);
             * }
             */
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - registrarOposicion", "Ocurrió un error al registrar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Abner Valdez [09-12-2021]
     * @descripción: {Obtener la oposicion por su IdOposicion}
     * @param:IdOposicion
     */
    @GetMapping(path = "/obtenerOposicion/{idOposicion}")
    @ApiOperation(value = "obtenerOposicion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerOposicion(@PathVariable Integer idOposicion) {
        log.info("OposicionController - obtenerOposicion", idOposicion);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.obtenerOposicion(idOposicion);

            if (result.getSuccess()) {
                log.info("OposicionController - obtenerOposicion", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("OposicionController - obtenerOposicion", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado: Abner Valdez [09-12-2021]
     * @descripción: {Obtener la oposicion por su IdOposicion}
     * @param:IdOposicion
     */
    @GetMapping(path = "/eliminarOposicionArchivo/{idArchivo}/{idUsuario}")
    @ApiOperation(value = "eliminarOposicionArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarOposicionArchivo(@PathVariable Integer idArchivo,@PathVariable Integer idUsuario) {
        log.info("OposicionController - eliminarOposicionArchivo", idArchivo);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.eliminarOposicionArchivo(idArchivo,idUsuario);

            if (result.getSuccess()) {
                log.info("OposicionController - eliminarOposicionArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("OposicionController - eliminarOposicionArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Obtener la oposicion respuesta por su IdOposicion}
     * @param:IdOposicion
     */
    @GetMapping(path = "/obtenerOposicionRespuesta/{IdOposicion}")
    @ApiOperation(value = "obtener Oposicion Respuesta", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<OposicionAdjuntosEntity>> obtenerOposicionRespuesta(
            @PathVariable Integer IdOposicion) {
        ResultClassEntity<OposicionAdjuntosEntity> result = new ResultClassEntity<OposicionAdjuntosEntity>();
        try {
            result = servicio.obtenerOposicionRespuesta(IdOposicion);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - obtenerOposicionRespuesta", "Ocurrió un error al obtener en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [06-07-2021]
     * @modificado:
     * @descripción: {Descargar el formato de resolucion}
     *
     */
    @GetMapping(path = "/descargarFormatoResulucion")
    @ApiOperation(value = "descargar Formato Resulucion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarFormatoResulucion() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = servicio.descargarFormatoResulucion();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - descargarFormatoResulucion", "Ocurrió un error al registrar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {adjuntar el formato de resolucion}
     *
     */
    @PostMapping(path = "/adjuntarFormatoResolucion")
    @ApiOperation(value = "adjuntar Formato Resolucion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> adjuntarFormatoResolucion(@RequestBody OposicionEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.adjuntarFormatoResolucion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - adjuntarFormatoResolucion", "Ocurrió un error al registrar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {actualizar la oposicion}
     *
     */
    @PostMapping(path = "/actualizarOposicion")
    @ApiOperation(value = "actualizar Oposicion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarOposicion(@RequestBody OposicionEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.actualizarOposicion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - actualizarOposicion", "Ocurrió un error al registrar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {registrar opositor}
     * @param:PersonaRequestEntity
     */
    @PostMapping(path = "/regitrarOpositor")
    @ApiOperation(value = "regitrar Opositor", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> regitrarOpositor(@RequestBody PersonaRequestEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.regitrarOpositor(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - regitrarOpositor", "Ocurrió un error al registrar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {Busca al opositor por su numero de documento}
     * @param:NumeroDocumento
     */
    @PostMapping(path = "/obtenerOpositor")
    @ApiOperation(value = "obtener Opositor", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<PersonaRequestEntity>> obtenerOpositor(
            @RequestBody OpositorRequestEntity obj) {
        ResultClassEntity<PersonaRequestEntity> result = new ResultClassEntity<PersonaRequestEntity>();
        try {
            result = servicio.obtenerOpositor(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("Oposicion - obtenerOpositor", "Ocurrió un error al obtener en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/registrarNuevaOposicion")
    @ApiOperation(value = "registrarNuevaOposicion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarNuevaOposicion(@RequestBody OposicionEntity request) {
        log.info("Oposicion - registrarNuevaOposicion", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.registrarNuevaOposicion(request);

            if (result.getSuccess()) {
                log.info("Oposicion - registrarNuevaOposicion", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Oposicion - registrarNuevaOposicion", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
