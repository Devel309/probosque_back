package pe.gob.serfor.mcsniffs.rest;

import org.apache.commons.lang3.StringUtils;

import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;

public abstract class AbstractRestController {

	/**
	 * @author rventocilla
	 * @return ResponseVO
	 * **/
	public static ResponseEntity<String> buildResponse(boolean estado, Object data, String message, Exception e) {

		ResponseEntity<String> response = new ResponseEntity<>();
		response.setData(data);
		response.setMessage(StringUtils.EMPTY);
		
		if(estado) {
			if(StringUtils.isBlank(message)){
				response.setMessage(Constantes.MESSAGE_SUCCESS);
			}else{
				response.setMessage(message);
			}
			
			response.setSuccess(true);
		}else {
			if(e == null) {
				response.setMessage(message);
			}else {
				response.setMessage(Constantes.MESSAGE_ERROR.concat(" ").concat(e.getMessage()));
//				response.setMessage(Constantes.MESSAGE_ERROR.concat(" ").concat(message));
			}
			
			response.setSuccess(false);
		}
		
		return response;
	}
}
