package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDetallePgmfDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.SistemaManejoAprovechamientoLaboresSilviculturales.SistemaManejoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ActividadSilviculturalService;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.Null;

@RestController
@RequestMapping(Urls.actividadSilvicultural.BASE)
public class ActividadSilviculturalController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ActividadSilviculturalController.class);

    @Autowired
    private ActividadSilviculturalService actividadSilviculturalService;

    /**
     * @autor: Harry Coa [17-08-2021]
     * @modificado:
     * @descripción: {Listar el detalle de las actividades silviculturales}
     */

    @GetMapping(path = "/listarActividadSilviculturalDet")
    @ApiOperation(value = "listarActividadSilvicultural por idPlanManejo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ActividadSilviculturalDto>> listarActividadSilvicultural(
            @RequestParam(defaultValue = "0") Integer idPlanManejo) {
        log.info("Listar parametro: {}", idPlanManejo);
        ResultClassEntity<ActividadSilviculturalDto> response = new ResultClassEntity<>();
        try {
            response = actividadSilviculturalService.ListarActividadSilviculturalDetalle(idPlanManejo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/listarActividadSilviculturalTipo")
    @ApiOperation(value = "listarActividadSilviculturalTipo por idPlanManejo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ActividadSilviculturalDto>> listarActividadSilviculturalFiltroTipo(
            @RequestParam(defaultValue = "0") Integer idPlanManejo, @RequestParam(defaultValue = "") String idTipo) {
        log.info("Listar parametro: {}, {}", idPlanManejo, idTipo);
        ResultClassEntity<ActividadSilviculturalDto> response = new ResultClassEntity<>();
        try {
            response = actividadSilviculturalService.ListarActividadSilviculturalFiltroTipo(idPlanManejo, idTipo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/configurarActividadSilviculturalPgmf")
    @ApiOperation(value = "configurarActividadSilviculturalPgmf", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity configurarActividadSilviculturalPgmf(
            @RequestBody ActividadSilviculturalEntity actividadSilviculturalEntity) {
        log.info("ActividadSilvicultural - ConfigurarActividadSilviculturalPgmf", actividadSilviculturalEntity);
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.ConfigurarActividadSilviculturalPgmf(actividadSilviculturalEntity);
            log.info("ActividadSilvicultural - configurarActividadSilviculturalPgmf",
                    "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service -configurarActividadSilviculturalPgmf", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/listarActividadSilviculturalPgmf")
    @ApiOperation(value = "listarActividadSilvicultural por idPlanManejo y tipo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ActividadSilviculturalDto>> listarActividadSilviculturalPgmf(
            @RequestParam(defaultValue = "0") Integer idPlanManejo, @RequestParam(defaultValue = "0") Integer idTipo) {
        log.info("Listar parametro: {}", idPlanManejo + " " + idTipo);
        ResultClassEntity<ActividadSilviculturalDto> response = new ResultClassEntity<>();
        try {
            response = actividadSilviculturalService.ListarActividadSilviculturalDetallePgmf(idPlanManejo, idTipo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarActividadSilviculturalPgmf")
    @ApiOperation(value = "actualizarActividadSilviculturalPgmf", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarActividadSilviculturalPgmf(
            @RequestBody List<ActividadSilviculturalDetallePgmfDto> lista) {
        log.info("ActividadSilvicultural - actualizarActividadSilvicultural", lista.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.ActualizarActividadSilviculturalPgmf(lista);
            log.info("ActividadSilvicultural - actualizarActividadSilviculturalPgmf",
                    "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service -actualizarActividadSilviculturalPgmf", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarActividadSilvicultural")
    @ApiOperation(value = "registrarActividadSilvicultural", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarActividadSilvicultural(
            @RequestBody List<ActividadSilviculturalEntity> list) {
        log.info("ProteccionBosque - registrarActividadSilvicultural", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.RegistrarActividadSilvicultural(list);
            if (response.getSuccess()) {
                log.info("ProteccionBosque - registrarActividadSilvicultural", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            log.error("ProteccionBosque -registrarActividadSilvicultural", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarActividadSilviculturalPgmf")
    @ApiOperation(value = "eliminarActividadSilviculturalPgmf", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarActividadSilviculturalPgmf(
            @RequestBody List<ActividadSilviculturalDetalleEntity> lista) {
        log.info("ActividadSilvicultural - eliminarActividadSilviculturalPgmf", lista);
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.EliminarActividadSilviculturalPgmf(lista);
            log.info("ActividadSilvicultural - eliminarActividadSilviculturalPgmf", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service -eliminarActividadSilviculturalPgmf", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/listarActividadSilvicultural")
    @ApiOperation(value = "listarActividadSilvicultural por idPlanManejo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<List<ActividadSilviculturalDetalleDto>> listarActividadSilviculturalDetalle(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codigoPlan) {
        log.info("Listar parametro: {}", idPlanManejo);

        try {
            return new ResponseEntity(true, "ok",
                    actividadSilviculturalService.ListarActividadSilvicultural(idPlanManejo,codigoPlan));
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity(false, e.getMessage(), null);
        }
    }

    @PostMapping(path = "/registrarLaborSilvicultural")
    @ApiOperation(value = "registrarActividadSilvicultural", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarLaborSilvicultural(
            @RequestBody List<ActividadSilviculturalEntity> list) {
        log.info("ProteccionBosque - registrarActividadSilvicultural", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.RegistrarLaborSilvicultural(list);
            log.info("ProteccionBosque - registrarActividadSilvicultural", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("ProteccionBosque -registrarActividadSilvicultural", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Roberto M. [15-09-2021]
     * @modificado:
     * @descripción: {eliminar labores silvicturales}
     *
     */
    @PostMapping(path = "/eliminarActividadSilviculturalDema")
    @ApiOperation(value = "eliminarActividadSilviculturalDema", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarActividadSilviculturalDema(
            @RequestBody List<ActividadSilviculturalDetalleEntity> lista) {
        log.info("ActividadSilvicultural - eliminarActividadSilviculturalDema", lista);
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.EliminarActividadSilviculturalDema(lista);
            log.info("ActividadSilvicultural - eliminarActividadSilviculturalDema", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service -eliminarActividadSilviculturalDema", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //region PFDM
    @GetMapping(path = "/listarActividadSilviculturalPFDM")
    @ApiOperation(value = "ListarActividadSilviculturalPFDM por IdPlanManejo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ActividadSilviculturalDto>> ListarActividadSilviculturalPFDM(
            @RequestParam(defaultValue = "0") Integer idPlanManejo,
            @RequestParam(defaultValue = "0") String idTipo
            ) {
        log.info("Listar parametro: {}", idPlanManejo);
        ResultClassEntity<ActividadSilviculturalDto> response = new ResultClassEntity<>();
        try {
            response = actividadSilviculturalService.ListarActividadSilviculturalPFDM(idPlanManejo, idTipo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/listarActividadSilviculturalPFDMTitular")
    @ApiOperation(value = "listarActividadSilviculturalPFDMTitular por Titular", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ActividadSilviculturalDto>> ListarActividadSilviculturalPFDM(
            @RequestParam(required = false) String tipoDocumento,
            @RequestParam(required = false) String nroDocumento,
            @RequestParam(required = false) String codigoProcesoTitular,
            @RequestParam(required = false) String codigoProceso,
            @RequestParam(required = false) Integer idPlanManejo
    ) {

        ResultClassEntity<ActividadSilviculturalDto> response = new ResultClassEntity<>();
        try {
            response = actividadSilviculturalService.ListarActividadSilviculturalPFDMTitular(tipoDocumento, nroDocumento,codigoProcesoTitular,codigoProceso,idPlanManejo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarActividadSilviculturalPFDM")
    @ApiOperation(value = "eliminarActividadSilviculturalPFDM", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarActividadSilviculturalPFDM(
            @RequestBody List<ActividadSilviculturalDetalleEntity> lista) {
        log.info("ActividadSilvicultural - eliminarActividadSilviculturalDema", lista);
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.EliminarActividadSilviculturalDema(lista);
            log.info("ActividadSilvicultural - eliminarActividadSilviculturalDema", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service -eliminarActividadSilviculturalDema", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/guardarSistemaManejoPFDM")
    @ApiOperation(value = "guardarSistemaManejoPFDM", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity GuardarSistemaManejoPFDM(
            @RequestBody SistemaManejoDto param) {
        log.info("ActividadSilvicultural - GuardarSistemaManejoPFDM", param);
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadSilviculturalService.GuardarSistemaManejoPFDM(param);
            log.info("ActividadSilvicultural - GuardarSistemaManejoPFDM", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service -GuardarSistemaManejoPFDM", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/listarActividadSilviculturalCabecera")
    @ApiOperation(value = "listarActividadSilviculturalCabecera por IdPlanManejo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<ActividadSilviculturalDto>>> ListarActividadSilviculturalCabecera(
            @RequestParam(defaultValue = "0") Integer idPlanManejo) {
        log.info("Listar parametro: {}", idPlanManejo);
        ResultClassEntity<List<ActividadSilviculturalDto>> response = new ResultClassEntity<>();
        try {
            response = actividadSilviculturalService.ListarActividadSilviculturalCabecera(idPlanManejo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //endregion PFDM

    @PostMapping(path = "/listarActSilviCulturalDetalle")
    @ApiOperation(value = "listarActSilviCulturalDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarResumen_Detalle(@RequestBody ActividadSilviculturalEntity param){
        try{
            return  new ResponseEntity (true, "ok", actividadSilviculturalService.ListarActSilviCulturalDetalle(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }


}
