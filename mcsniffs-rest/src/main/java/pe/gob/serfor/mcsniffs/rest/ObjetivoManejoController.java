package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ObjetivoEspecificoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ObjetivoManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Objetivo.ObjetivoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ObjetivoManejoService;


@RestController
@RequestMapping(Urls.objetivoManejo.BASE)
public class ObjetivoManejoController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ObjetivoManejoController.class);



    @Autowired
    private ObjetivoManejoService ObjetivoManejoService;

    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Registrar Objetivo de Manejo}
     * @param:ObjetivoManejoEntity
     */
    @PostMapping(path = "/registrarObjetivoManejo")
    @ApiOperation(value = "registrarObjetivoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarObjetivoManejo(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo - RegistrarObjetivoManejo",ObjetivoManejo.toString());
        ResultClassEntity response = new ResultClassEntity();
        try{

            response = ObjetivoManejoService.RegistrarObjetivoManejo(ObjetivoManejo);
            log.info("ObjetivoManejo - RegistrarObjetivoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ObjetivoManejo -RegistrarObjetivoManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {actualizar Objetivo de Manejo}
     * @param:ObjetivoManejoEntity
     */
    @PostMapping(path = "/actualizarObjetivoManejo")
    @ApiOperation(value = "actualizarObjetivoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarObjetivoManejo(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo - ActualizarObjetivoManejo",ObjetivoManejo.toString());
        ResultClassEntity response = new ResultClassEntity();
        try{

            response = ObjetivoManejoService.ActualizarObjetivoManejo(ObjetivoManejo);
            log.info("ObjetivoManejo - ActualizarObjetivoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ObjetivoManejo -ActualizarObjetivoManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {obtener Objetivo de Manejo}
     * @param:ObjetivoManejoEntity
     */
    @PostMapping(path = "/obtenerObjetivoManejo")
    @ApiOperation(value = "obtenerObjetivoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerObjetivoManejo(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo - ObtenerObjetivoManejo",ObjetivoManejo.toString());
        ResultClassEntity response = new ResultClassEntity();
        try{

            response = ObjetivoManejoService.ObtenerObjetivoManejo(ObjetivoManejo);
            log.info("ObjetivoManejo - ObtenerObjetivoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ObjetivoManejo -ObtenerObjetivoManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {registrar Objetivo especifico de manejo}
     * @param:list<ObjetivoEspecificoManejoEntity>
     */
    @PostMapping(path = "/registrarObjetivoEspecificoManejo")
    @ApiOperation(value = "registrarObjetivoEspecificoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarObjetivoEspecificoManejo(@RequestBody List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoList){
        log.info("ObjetivoManejo- registrarObjetivoEspecificoManejo",ObjetivoEspecificoManejoList.toString());
        ResultClassEntity response = new ResultClassEntity();
        try{

            response = ObjetivoManejoService.RegistrarObjetivoEspecificoManejo(ObjetivoEspecificoManejoList);
            log.info("ObjetivoManejo- registrarObjetivoEspecificoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ObjetivoManejo -registrarObjetivoEspecificoManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {obtener Objetivo especifico de manejo}
     * @param:ObjetivoEspecificoManejoEntity
     */
    @PostMapping(path = "/obtenerObjetivoEspecificoManejo")
    @ApiOperation(value = "obtenerObjetivoEspecificoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerObjetivoEspecificoManejo(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo- obtenerObjetivoEspecificoManejo",ObjetivoManejo.toString());
        ResultClassEntity response = new ResultClassEntity();
        try{

            response = ObjetivoManejoService.ObtenerObjetivoEspecificoManejo(ObjetivoManejo);
            log.info("ObjetivoManejo- obtenerObjetivoEspecificoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ObjetivoManejo -obtenerObjetivoEspecificoManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {actualizar Objetivo especifico de manejo}
     * @param:list<ObjetivoEspecificoManejoEntity>
     */
    @PostMapping(path = "/actualizarObjetivoEspecificoManejo")
    @ApiOperation(value = "actualizarObjetivoEspecificoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarObjetivoEspecificoManejo(@RequestBody List<ObjetivoEspecificoManejoEntity> ObjetivoEspecificoManejoList){
        log.info("ObjetivoManejo - actualizarObjetivoEspecificoManejo",ObjetivoEspecificoManejoList.toString());
        ResultClassEntity result = new ResultClassEntity();
        try{

            result = ObjetivoManejoService.ActualizarObjetivoEspecificoManejo(ObjetivoEspecificoManejoList);
            log.info("ObjetivoManejo - actualizarObjetivoEspecificoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            result.setInnerException("Ocurrió un error.");
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {combo de  Objetivo especifico}
     * @param:ParametroEntity
     */
    @PostMapping(path = "/comboPorFiltroObjetivoEspecifico")
    @ApiOperation(value = "comboPorFiltroObjetivoEspecifico" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> comboPorFiltroObjetivoEspecifico(@RequestBody ParametroEntity parametro){
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = ObjetivoManejoService.ComboPorFiltroObjetivoEspecifico(parametro);
            log.info("ObjetivoManejo - comboPorFiltroObjetivoEspecifico","Proceso realizado correctamente");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ObjetivoManejo -comboPorFiltroObjetivoEspecifico","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline DB [20-09-2021]
     * @modificado:
     * @descripción: {Elimina los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @PostMapping(path = "/eliminarObjetivoManejo")
    @ApiOperation(value = "eliminarObjetivoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarObjetivoManejo(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo - eliminarObjetivoManejo",ObjetivoManejo.toString());
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = ObjetivoManejoService.eliminarObjetivoManejo(ObjetivoManejo);
            log.info("ObjetivoManejo - eliminarObjetivoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ObjetivoManejo -eliminarObjetivoManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Jason Retamozo [20-01-2022]
     * @modificado:
     * @descripción: {listar los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @PostMapping(path = "/listarObjetivoManejoTitular")
    @ApiOperation(value = "listarObjetivoManejoTitular" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarObjetivoManejoTitular(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo - listarObjetivoManejo",ObjetivoManejo.toString());
        ResponseEntity result = null;
        ResultEntity<ObjetivoDto> response  = new ResultEntity<>();
        try{
            response = ObjetivoManejoService.listarObjetivoManejoTitular(ObjetivoManejo);
            log.info("ObjetivoManejo - listarObjetivoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ObjetivoManejo -listarObjetivoManejo","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline DB [21-09-2021]
     * @modificado:
     * @descripción: {listar los objetivos}
     * @param:ObjetivoManejoEntity
     */
    @PostMapping(path = "/listarObjetivoManejo")
    @ApiOperation(value = "listarObjetivoManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarObjetivoManejo(@RequestBody ObjetivoManejoEntity ObjetivoManejo){
        log.info("ObjetivoManejo - listarObjetivoManejo",ObjetivoManejo.toString());
        ResponseEntity result = null;
        ResultEntity<ObjetivoManejoEntity> response  = new ResultEntity<>();
        try{
            response = ObjetivoManejoService.listarObjetivoManejo(ObjetivoManejo);
            log.info("ObjetivoManejo - listarObjetivoManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ObjetivoManejo -listarObjetivoManejo","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Harry Coa 13-10-2021
     * @descripción: {Listar/Filtrar Objetivos}
     * @param: Integer idPlanManejo
     * @param: String codigoObjetivo
     */
    @GetMapping(path = "/listarObjetivo")
    @ApiOperation(value = "Listar y/o Buscar(Filtrar) Objetivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarObjetivo(@RequestParam(required = false) String codigoObjetivo, //
                                                    @RequestParam(required = false) Integer idPlanManejo //
                                               ) throws Exception {

        log.info("obtener: {}, {}", idPlanManejo, codigoObjetivo, idPlanManejo);
        ResultEntity<ObjetivoDto> response  = new ResultEntity<>();
        try {
            response = ObjetivoManejoService.listarObjetivos(codigoObjetivo,idPlanManejo);
            log.info("Service - obtener: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    @PostMapping(path = "/registrarObjetivo")
    @ApiOperation(value = "registrarObjetivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarObjetivo(
            @RequestBody List<ObjetivoManejoEntity> list) {
        log.info("Objetivos - registrarObjetivo", list.toString());
        ResultClassEntity response = new ResultClassEntity();
        try {
            response = ObjetivoManejoService.RegistrarObjetivo(list);
            if (response.getSuccess()) {
                log.info("Objetivos - registrarObjetivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            log.error("Objetivos -registrarObjetivo", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
