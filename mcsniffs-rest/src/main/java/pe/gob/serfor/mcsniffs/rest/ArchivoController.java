package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoService;

import java.io.IOException;

@RestController
@RequestMapping(Urls.archivo.BASE)
public class ArchivoController extends AbstractRestController {
    
    /**
     * @autor: JaquelineDB [21-06-2021]
     * @modificado:
     * @descripción: {Servicio de la gestion de los adjuntos}
     *
     */
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ArchivoController.class);
    @Autowired
    private FileServerConexion fileCn;

    @Autowired
    private ArchivoService ArServ;

    
    @Value("${smb.file.server.path}")
    private String fileServerPath;
    

    @PostMapping ("/cargarArchivo")
    @ApiOperation(value = "Cargar Archivo en Azure Container", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> CargarArchivo(@RequestParam("file") MultipartFile file,
                                                             @RequestParam(required = false) Integer IdUsuarioCreacion,
                                                             @RequestParam(required = false) Integer IdUsuarioEdicion, 
                                                             @RequestParam(required = false) Integer IdArchivo, 
                                                             @RequestParam(required = false) String TipoDocumento,
                                                             @RequestParam(required = false) Integer CodigoTabla) throws IOException {
      
        ResultEntity res = new ResultEntity();
       
        try {
            String nombreGenerado = fileCn.uploadFile(file);

            ArchivoEntity archivo = new ArchivoEntity();
            archivo.setIdUsuarioRegistro(IdUsuarioCreacion);
            archivo.setTipoDocumento(TipoDocumento);
            archivo.setIdUsuarioRegistro(IdUsuarioCreacion);
            archivo.setIdUsuarioModificacion(IdUsuarioEdicion);
            archivo.setIdArchivo(IdArchivo);
            archivo.setEstado("A");
                
           
            archivo.setRuta(fileServerPath + ( !nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()) ) ;
     
            archivo.setExtension(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),file.getOriginalFilename().length()));
            archivo.setNombre(file.getOriginalFilename());
            archivo.setNombreGenerado((!nombreGenerado.equals("")?nombreGenerado:file.getOriginalFilename()));
            archivo.setIdArchivo(IdArchivo);    
          
            if(!nombreGenerado.equals("")){
                res =ArServ.registroArchivo(archivo);   
                log.info("CargarArchivo", "Se cargo archivo con éxito");
            }else{
                res.setMessage("Archivo no Cargado");
                res.setIsSuccess(false);
                log.warn("CargarArchivo", "Archivo no Cargado");
            }       
            res.setInformacion(nombreGenerado);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.setIsSuccess(false);
            res.setMessage("Se produjo un error al cargar archivo en Azure Container");
            res.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado: 
     * @descripción: {obtener un archivo}
     * @param:IdArchivo
     */
    @GetMapping(path = "/obtenerArchivo/{IdArchivo}")
    @ApiOperation(value = "obtener Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<ArchivoEntity>> obtenerArchivo(@PathVariable("IdArchivo") Integer IdArchivo){
        ResultClassEntity<ArchivoEntity> result = new ResultClassEntity<ArchivoEntity>();
        try {
            result = ArServ.obtenerArchivo(IdArchivo);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ArchivoController - obtenerArchivo"+ "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    /*@PostMapping ("/descargarArchivo")
    @ApiOperation(value = "descargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> DescargarArchivo(@RequestBody ArchivoEntity request,HttpServletRequest request1) throws IOException {
        ResultEntity res = new ResultEntity(); 
        try {
            res =  ArServ.obtenerArchivo(request);           
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.setIsSuccess(false);
            res.setMessage("Se produjo un error al cargar archivo en Azure Container");
            res.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }*/

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado: Harry Coa [26-08-2021]
     * @descripción: { eliminar un archivo }
     * @param: Integer idArchivo
     * @param: Integer idUsuarioElimina
     */
    @PostMapping(path = "/eliminarArchivo")
    @ApiOperation(value = "Eliminar Archivo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<Integer>> eliminarArchivo(
            @RequestParam(required = true) Integer idArchivo,
            @RequestParam(required = true) Integer idUsuarioElimina) {
        log.info("eliminarDetalle: {}, {}", idArchivo, idUsuarioElimina);
        ResultClassEntity<Integer> response = new ResultClassEntity<>();
        try {
            response = ArServ.EliminarArchivoGeneral(idArchivo, idUsuarioElimina);
            log.info("Service - eliminar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - eliminar, Ocurrió un error: {}", e.getMessage());
            response.setError(Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

      /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado: 
     * @descripción: {listar los archivos}
     * @param:ArchivoEntity
     */
    @PostMapping(path = "/listarArchivo")
    @ApiOperation(value = "listar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ArchivoEntity>> listarArchivo(@RequestBody ArchivoEntity filtro){
        ResultEntity<ArchivoEntity> result = new ResultEntity<ArchivoEntity>();
        try {
            result = ArServ.listarArchivo(filtro);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ArchivoController - listarArchivo", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    /**
     * @autor: Harry Coa [26-08-2021]
     * @modificado:
     * @descripción: {Obtener archivo por id}
     * @param: Integer idArchivo
     */
    @GetMapping(path = "/obtenerArchivoGeneral")
    @ApiOperation(value = "Obtener archivo por id", authorizations = {  @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),  @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ArchivoEntity>> ObtenerArchivoGeneral(
            @RequestParam(defaultValue = "0") Integer idArchivo) {
        log.info("obtener: {}", idArchivo);
        ResultClassEntity<ArchivoEntity> response = new ResultClassEntity<>();
        try {
            response = ArServ.ObtenerArchivoGeneral(idArchivo);
            log.info("Service - obtener: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            response.setError(Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping ("/cargarArchivoGeneral")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity CargarArchivoGeneral(@RequestParam("file") MultipartFile file,
                                                      @RequestParam Integer IdUsuarioCreacion,
                                                      @RequestParam String TipoDocumento) throws IOException {
        ResultClassEntity<Integer> response = new ResultClassEntity();
          log.info("Archivo  - CargarArchivoGeneral  \n archivo:"+file.getOriginalFilename()+"idUsuarioRegistro:"+IdUsuarioCreacion.toString()+"tipoDocumento:"+TipoDocumento);

        try {

            response = ArServ.RegistrarArchivoGeneral(file, IdUsuarioCreacion, TipoDocumento);

            if(response.getSuccess()){
                log.error("Archivo-CargarArchivoGeneral: Proceso Correctamente");
            }
            else{
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Se produjo un error al cargar archivo en Azure Container");
            log.error("Archivo-CargarArchivoGeneral: Ocurrio Un Error:"+ e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
        /**
         * @autor: IVAN MINAYA [26-08-2021]
         * @modificado:
         * @descripción: {Descarga de archivo general}
         * @param:ArchivoEntity
         */
        @PostMapping(path = "/DescargarArchivoGeneral")
        @ApiOperation(value = "DescargarArchivoGeneral", authorizations = @Authorization(value = "JWT"))
        @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
        public ResponseEntity  DescargarArchivoGeneral(@RequestBody ArchivoEntity param){
            ResultClassEntity result = new ResultClassEntity();
            log.info("Archivo - DescargarArchivoGeneral ", param.toString());
            try {
                result = ArServ.DescargarArchivoGeneral(param);
                log.info("Archivo - DescargarArchivoGeneral: Proceso realizado correctamente");
                return new ResponseEntity<>(result, HttpStatus.OK );
            } catch (Exception Ex){
                log.error("Archivo- DescargarArchivoGeneral", "Ocurrió un error al generar en: "+Ex.getMessage());
                result.setInnerException("Ocurrió un error.");
                return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
            }
        }


    @PostMapping(path = "/eliminarArchivoGeometria")
    @ApiOperation(value = "eliminarArchivoGeometria" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarArchivoGeometria(@RequestBody ArchivoEntity archivoEntity){
        log.info("eliminarArchivoGeometria - eliminarArchivoGeometria",archivoEntity.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = ArServ.eliminarArchivoGeometria(archivoEntity);
            log.info("eliminarArchivoGeometria - eliminarArchivoGeometria","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("eliminarArchivoGeometria - eliminarArchivoGeometria","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //     /**
    //  * @autor: Abner Valdez [16-11-2021]
    //  * @modificado: 
    //  * @descripción: {obtener un archivo}
    //  * @param:NombreArchivo
    //  */
    // @PostMapping(path = "/descargarArchivo/{nombreArchivo}")
    // @ApiOperation(value = "Descargar Archivo .zip", authorizations = @Authorization(value = "JWT"))
    // @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    //     public ResponseEntity  descargarArchivo(@RequestParam(required = true) String name){
    //         ResultClassEntity result = new ResultClassEntity();
    //         log.info("Archivo - Descargar Archivo ", name.toString());
    //         try {
    //             result = ArServ.downloadFile(name);
    //             log.info("Archivo - Descargar Archivo: Proceso realizado correctamente");
    //             return new ResponseEntity<>(result, HttpStatus.OK );
    //         } catch (Exception Ex){
    //             log.error("Archivo- Descargar Archivo", "Ocurrió un error al generar en: "+Ex.getMessage());
    //             result.setInnerException("Ocurrió un error.");
    //             return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
    //         }
    //     }

    /**
     * @autor: JaquelineDB [28-03-2022]
     * @modificado: 
     * @descripción: {cargar un plantilla}
     * @param:archivo
     */
    @PostMapping ("/cargarPlantillas")
    @ApiOperation(value = "carga plantillas directamente al repositorio", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> cargarPlantillas(@RequestParam("file") MultipartFile file) throws IOException {
    
        ResultArchivoEntity res = new ResultArchivoEntity();
        try {
            String nombreGenerado = fileCn.uploadFile(file);
            if(!nombreGenerado.equals("")){  
                res.setMessage("Se cargo archivo con éxito. Guarde el nombre y la extensión del archivo.");
                res.setIsSuccess(true);
                res.setContenTypeArchivo("application/octet-stream");
                log.info("CargarArchivo", "Se cargo archivo con éxito");
            }else{
                res.setMessage("Archivo no Cargado");
                res.setIsSuccess(false);
                log.warn("CargarArchivo", "Archivo no Cargado");
            }    
            res.setInformacion("Nombre generado:" + nombreGenerado + " Extensión:"+file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),file.getOriginalFilename().length()));
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.setIsSuccess(false);
            res.setMessage("Se produjo un error al cargar archivo en el repositorio de archivos");
            res.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: JaquelineDB [28-03-2022]
     * @modificado: Harry Coa [18-04-2022]
     * @descripción: {descargar un plantilla}
     * @param:archivo
     */
    @PostMapping ("/descargarPlantillas")
    @ApiOperation(value = "descarga de plantillas subidas al fileserver", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillas(@RequestParam("nombreGeneradoExtension") String nombreGeneradoExtension) throws IOException {
    
        ResultArchivoEntity res = new ResultArchivoEntity();
        try {
            byte[] fileContent = fileCn.loadFileAsResource(nombreGeneradoExtension);
            if(fileContent.length > 0){
                res.setMessage("Se descargo la plantilla correctamente.");
                res.setIsSuccess(true);
                res.setContenTypeArchivo("application/octet-stream");
                res.setArchivo(fileContent);
                log.info("Descarga de Archivo", "Se descargo la plantilla correctamente.");
            }else{
                res.setMessage("Archivo no encontrado en el file server");
                res.setIsSuccess(false);
                log.warn("Descargar Archivo", "Archivo no encontrado");
            }       
            res.setInformacion(nombreGeneradoExtension);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.setIsSuccess(false);
            res.setMessage("Se produjo un error al descargar archivo.");
            res.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
