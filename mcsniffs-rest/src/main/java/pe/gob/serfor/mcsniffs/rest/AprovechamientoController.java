package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AprovechamientoService;
import pe.gob.serfor.mcsniffs.service.CensoForestalDetalleService;

import java.util.List;

@RestController
@RequestMapping(Urls.aprovechamiento.BASE)

public class AprovechamientoController extends AbstractRestController {

    @Autowired
    private AprovechamientoService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AprovechamientoController.class);







        @PostMapping(path = "/registrarAprovechamiento")
    @ApiOperation(value = "registrarAprovechamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAprovechamiento(@RequestBody AprovechamientoEntity entidad){
        log.info("registrarAprovechamiento", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = service.RegistrarAprovechamiento(entidad);
            log.info("RecursoForestal Registrar","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("RecursoForestal Registrar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listaAprovechamiento")
    @ApiOperation(value = "Listar Aprovechamiento", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<AprovechamientoEntity>> ListarAprovechamiento(@RequestParam("idPlanManejo") Integer idPlanManejo) {
        try {
            return service.ListarAprovechamiento(idPlanManejo);
        } catch (Exception ex) {

            ResultClassEntity<List<AprovechamientoEntity>> result = new ResultClassEntity<>();
            log.error("AprovechamientoController - listaAprovechamiento", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


}
