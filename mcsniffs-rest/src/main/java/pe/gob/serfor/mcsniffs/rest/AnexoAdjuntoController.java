package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.AnexoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AnexoAdjuntoService;
import pe.gob.serfor.mcsniffs.service.CapacitacionService;

@RestController
@RequestMapping(Urls.anexoAdjunto.BASE)
public class AnexoAdjuntoController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AnexoAdjuntoController.class);

    @Autowired
    private AnexoAdjuntoService anexoAdjuntoService;

    @PostMapping(path = "/marcarParaAmpliacionAnexoAdjunto")
    @ApiOperation(value = "marcarParaAmpliacionAnexoAdjunto" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity marcarParaAmpliacionAnexoAdjunto(@RequestBody AnexoAdjuntoEntity anexoAdjuntoEntity){
        log.info("Anexo Adjunto - MarcarParaAmpliacionAnexoAdjunto",anexoAdjuntoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = anexoAdjuntoService.MarcarParaAmpliacionAnexoAdjunto(anexoAdjuntoEntity);
            log.info("Anexo Adjunto - MarcarParaAmpliacionAnexoAdjunto","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Anexo Adjunto - MarcarParaAmpliacionAnexoAdjunto","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarAnexoAdjunto")
    @ApiOperation(value = "listarAnexoAdjunto" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarAnexoAdjunto(@RequestBody AnexoAdjuntoEntity anexoAdjuntoEntity){
        try{
            return  new ResponseEntity (true, "ok", anexoAdjuntoService.ListarAnexoAdjunto(anexoAdjuntoEntity));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

}
