package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SistemaManejoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.SistemaManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SistemaManejoForestalService;

@RestController
@RequestMapping(Urls.SistemaManejoForestal.BASE)
public class SistemaManejoForestalController extends AbstractRestController {

	private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SistemaManejoForestalController.class);

	@Autowired
	private SistemaManejoForestalService sistemaManejoForestalService;

	/**
	 * @autor: Alexis Salgado [17-08-2021]
	 * @modificado: Alexis Salgado [06-09-2021]
	 * @descripción: {Obtener Sistema Manejo Forestal por idPlanManejo y
	 *               codigoProceso}
	 * @param: Integer idPlanManejo
	 * @param: String  codigoProceso
	 */
	@GetMapping(path = "")
	@ApiOperation(value = "Obtener Sistema Manejo Forestal por idPlanManejo y codigoProceso", authorizations = {
			@Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<SistemaManejoForestalDto>> obtener(
			@RequestParam(defaultValue = "0") Integer idPlanManejo, @RequestParam String codigoProceso) {

		log.info("obtener: idPlanManejo={}, codigoProceso={} ", idPlanManejo, codigoProceso);

		ResultClassEntity<SistemaManejoForestalDto> response = new ResultClassEntity<>();
		try {

			response = sistemaManejoForestalService.obtener(idPlanManejo, codigoProceso);
			log.info("Service - obtener: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {

			log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @autor: Alexis Salgado [16-08-2021]
	 * @modificado:
	 * @descripción: {Listar Sistema Manejo Forestal}
	 * @param: Integer idPlanManejo
	 * @param: Integer idSistemaManejoForestal
	 * @param: String  descripcionFinMaderable
	 * @param: String  descripcionCicloCorta
	 * @param: String  descripcionFinNoMaderable
	 * @param: String  descripcionCicloAprovechamiento
	 */
	@GetMapping(path = "/listar")
	@ApiOperation(value = "listar", authorizations = { @Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<List<SistemaManejoForestalDto>>> listar(
			@RequestParam(defaultValue = "0") Integer idPlanManejo,
			@RequestParam(defaultValue = "0") Integer idSistemaManejoForestal,
			@RequestParam(defaultValue = "") String descripcionFinMaderable,
			@RequestParam(defaultValue = "") String descripcionCicloCorta,
			@RequestParam(defaultValue = "") String descripcionFinNoMaderable,
			@RequestParam(defaultValue = "") String descripcionCicloAprovechamiento) {

		log.info("listar: {},{},{},{},{},{}", idPlanManejo, idSistemaManejoForestal, descripcionFinMaderable,
				descripcionCicloCorta, descripcionFinNoMaderable, descripcionCicloAprovechamiento);

		ResultClassEntity<List<SistemaManejoForestalDto>> response = new ResultClassEntity<>();
		try {

			response = sistemaManejoForestalService.listar(idSistemaManejoForestal, idPlanManejo,
					descripcionFinMaderable, descripcionCicloCorta, descripcionFinNoMaderable,
					descripcionCicloAprovechamiento);

			log.info("Service - listar: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			log.error("Service - listar: Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @autor: Alexis Salgado [16-08-2021]
	 * @modificado:
	 * @descripción: {Registrar SistemaManejoForestal}
	 * @param: SistemaManejoForestalDto request
	 */
	@PostMapping(path = "")
	@ApiOperation(value = "Registrar Sistema Manejo Forestal", authorizations = { @Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<SistemaManejoForestalDto>> registrar(
			@RequestBody SistemaManejoForestalDto request) {

		log.info("registrar: {}", request);

		ResultClassEntity<SistemaManejoForestalDto> response = new ResultClassEntity<>();
		try {
			response = sistemaManejoForestalService.guardar(request);

			log.info("Service - guardar: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			log.error("Service - guardar, Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @autor: Alexis Salgado [20-08-2021]
	 * @modificado:
	 * @descripción: {Registrar SistemaManejoForestalDetalle}
	 * @param: SistemaManejoForestalDetalleEntity request
	 */
	@PostMapping(path = "/detalle")
	@ApiOperation(value = "Registrar Sistema Manejo Forestal Detalle", authorizations = {
			@Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<SistemaManejoForestalDetalleEntity>> registrarDetalle(
			@RequestBody SistemaManejoForestalDetalleEntity request) {

		log.info("registrarDetalle: {}", request);

		ResultClassEntity<SistemaManejoForestalDetalleEntity> response = new ResultClassEntity<>();
		try {
			response = sistemaManejoForestalService.guardarDetalle(request);
			log.info("Service - guardarDetalle: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Service - guardarDetalle, Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@PostMapping(path = "/detalle/eliminar")
	@ApiOperation(value = "Eliminar Sistema Manejo Forestal Detalle", authorizations = {
			@Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<Integer>> eliminarDetalle(
			@RequestParam(required = true) Integer idSistemaManejoForestalDetalle,
			@RequestParam(required = true) Integer idUsuarioElimina) {

		log.info("eliminarDetalle: {}, {}", idSistemaManejoForestalDetalle, idUsuarioElimina);

		ResultClassEntity<Integer> response = new ResultClassEntity<>();
		try {
			response = sistemaManejoForestalService.eliminarDetalle(idSistemaManejoForestalDetalle, idUsuarioElimina);
			log.info("Service - eliminarDetalle: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Service - eliminarDetalle, Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
