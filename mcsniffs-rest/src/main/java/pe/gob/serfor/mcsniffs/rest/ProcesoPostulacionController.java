package pe.gob.serfor.mcsniffs.rest;

import java.io.IOException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieFaunaEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoRespuestaEntity;
import pe.gob.serfor.mcsniffs.entity.ListarProcesoPosEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionEntity;
import pe.gob.serfor.mcsniffs.entity.ProcesoPostulacionResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.ProcesoPostulacionService;

@RestController
@RequestMapping(Urls.procesoPostulacion.BASE)
public class ProcesoPostulacionController {
    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Repository de los procesos de postulacion}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProcesoPostulacionController.class);
    @Autowired
    private ProcesoPostulacionService service;
    @Autowired
    private EmailService enviarcorreo;
    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Guardar proceso postulacion}
     * @param:ProcesoPostulacionResquestEntity
     */
    @PostMapping(path = "/guardaProcesoPostulacion")
    @ApiOperation(value = "Guarda el proceso de postulacion y sus adjuntos", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardaProcesoPostulacion(@RequestBody ProcesoPostulacionResquestEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardaProcesoPostulacion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ProcesoPostulacionController - guardaProcesoPostulacion", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [24-06-2021]
     * @modificado:
     * @descripción: {Listar proceso de porstulacion por usuario}
     * @param:AdjuntoRequestEntity
     */
    @PostMapping(path = "/listarProcesoPostulacion")
    @ApiOperation(value = "lista todos los procesos de postulacion activos", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ProcesoPostulacionEntity>> listarProcesoPostulacion(@RequestBody ListarProcesoPosEntity obj){
        ResultEntity<ProcesoPostulacionEntity> result = new ResultEntity<ProcesoPostulacionEntity>();
        try {
            result = service.listarProcesoPostulacion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("procesoPostulacion - listarProcesoPostulacion", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [30-06-2021]
     * @modificado:
     * @descripción: {Envia el documento de autorizacion para la publicacion}
     * @param:file
     */
    @PostMapping(path = "/autorizarPublicacion")
    @ApiOperation(value = "Autoriza la publicacion de su postulacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<DocumentoRespuestaEntity>> autorizarPublicacion(@RequestParam("file") MultipartFile file,
                                                                                            @RequestParam(required = false) String CodigoDoc,
                                                                                            @RequestParam(required = false) Integer IdProcesoPostulacion,
                                                                                            @RequestParam(required = false) Integer IdSolicitante,
                                                                                            @RequestParam(required = false) Integer IdUsuarioRegistro,
                                                                                            @RequestParam(required = false) Integer IdTipoDocumento,
                                                                                            @RequestParam(required = false)  String NombreArchivo,
                                                                                            @RequestParam(required = false) Integer IdPostulacionPFDM,
                                                                                            @RequestParam(required = false) Integer idDocumentoAdjunto
    ) throws IOException {
        ResultClassEntity<DocumentoRespuestaEntity> result = new ResultClassEntity<DocumentoRespuestaEntity>();
        try {
            result = service.autorizarPublicacion(file,CodigoDoc, NombreArchivo,IdProcesoPostulacion,IdSolicitante,IdUsuarioRegistro,IdTipoDocumento,IdPostulacionPFDM,idDocumentoAdjunto);
            //Enviar Correo
            //Primero se debe obtener el correo con el IdSolicitante
            /*if(result.getSuccess()){ //falta implementar por lo del correo
                //Enviar correo
                EmailEntity mail = new EmailEntity();
                mail.setEmail(usuario.getCorreo());
                mail.setContent(result.getData().getContenidoEmail());
                mail.setSubject(result.getData().getAsuntoEmail());
                Boolean enviarmail = enviarcorreo.sendEmail(mail);
            }*/
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("procesoPostulacion - autorizarPublicacion", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [01-06-2021]
     * @modificado:
     * @descripción: {EObtiene las autorizaciones de publicacion necesarias}
     * @param:file
     */
    @PostMapping(path = "/obtenerAutorizacion")
    @ApiOperation(value = "Se obtiene las uatorizaciones de publicacion activas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<DocumentoRespuestaEntity>> obtenerAutorizacion(@RequestBody AnexoRequestEntity filtro){
        ResultEntity<DocumentoRespuestaEntity> result = new ResultEntity<DocumentoRespuestaEntity>();
        try {
            result = service.obtenerAutorizacion(filtro);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("procesoPostulacion - obtenerAutorizacion", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [01-06-2021]
     * @modificado:
     * @descripción: {Adjunta la resolucion terminando con el proceso de postulacion}
     * @param:file
     */

    @PostMapping(path = "/AdjuntarResolucionDenegada")
    @ApiOperation(value = "Adjuntar la resolucion denegando la publicacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<DocumentoRespuestaEntity>> AdjuntarResolucionDenegada(@RequestParam("file") MultipartFile file,
                                                                                            @RequestParam(required = false) String CodigoDoc,
                                                                                            @RequestParam(required = false) Integer IdProcesoPostulacion,
                                                                                            @RequestParam(required = false) Integer IdSolicitante,
                                                                                            @RequestParam(required = false) Integer IdUsuarioRegistro,
                                                                                            @RequestParam(required = false) Integer IdTipoDocumento,
                                                                                          @RequestParam(required = false)  String NombreArchivo,
                                                                                          @RequestParam(required = false) Integer IdPostulacionPFDM,
                                                                                            @RequestParam(required = false) Integer idDocumentoAdjunto


    ) throws IOException {
        ResultClassEntity<DocumentoRespuestaEntity> result = new ResultClassEntity<DocumentoRespuestaEntity>();
        try {
            result = service.AdjuntarResolucionDenegada(file,CodigoDoc,NombreArchivo,IdProcesoPostulacion,IdSolicitante,IdUsuarioRegistro,IdTipoDocumento,IdPostulacionPFDM, idDocumentoAdjunto);
            //Enviar Correo
            //Primero se debe obtener el correo con el IdSolicitante
            /*if(result.getSuccess()){ //falta implementar por lo del correo
                //Enviar correo
                EmailEntity mail = new EmailEntity();
                mail.setEmail(usuario.getCorreo());
                mail.setContent(result.getData().getContenidoEmail());
                mail.setSubject(result.getData().getAsuntoEmail());
                Boolean enviarmail = enviarcorreo.sendEmail(mail);
            }*/
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("procesoPostulacion - AdjuntarResolucionDenegada", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/obtenerProcesoPostulacion")
    @ApiOperation(value = "obtenerProcesoPostulacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerProcesoPostulacion(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - obtenerProcesoPostulacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.obtenerProcesoPostulacion(param);
            log.info("ProcesoPostulacion - obtenerProcesoPostulacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - obtenerProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
/*
    @PostMapping(path = "/obtenerInformacionProcesoPostulacion")
    @ApiOperation(value = "obtenerInformacionProcesoPostulacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInformacionProcesoPostulacion(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - obtenerInformacionProcesoPostulacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.obtenerInformacionProcesoPostulacion(param);
            log.info("ProcesoPostulacion - obtenerProcesoPostulacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - obtenerInformacionProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }*/

    @PostMapping(path = "/obtenerInformacionProcesoPostulacion")
    @ApiOperation(value = "obtenerInformacionProcesoPostulacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> obtenerInformacionProcesoPostulacion(@RequestBody ProcesoPostulacionDto request){
        log.info("ProcesoPostulacion - obtenerInformacionProcesoPostulacion", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = service.obtenerInformacionProcesoPostulacion(request);
            log.info("ProcesoPostulacion - obtenerInformacionProcesoPostulacion","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("ProcesoPostulacion - obtenerInformacionProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/actualizarProcesoPostulacion")
    @ApiOperation(value = "actualizarProcesoPostulacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarProcesoPostulacion(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - actualizarProcesoPostulacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.actualizarProcesoPostulacion(param);
            log.info("ProcesoPostulacion - actualizarProcesoPostulacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - actualizarProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @PostMapping(path = "/actualizarObservacionARFFSProcesoPostulacion")
    @ApiOperation(value = "actualizarObservacionARFFSProcesoPostulacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarObservacionARFFSProcesoPostulacion (@RequestBody ProcesoPostulacionDto obj) {
        log.info("ProcesoPostulacion - actualizarObservacionARFFSProcesoPostulacion",obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.actualizarObservacionARFFSProcesoPostulacion(obj);
            log.info("ProcesoPostulacion - actualizarObservacionARFFSProcesoPostulacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - actualizarObservacionARFFSProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/actualizarResolucionProcesoPostulacion")
    @ApiOperation(value = "actualizarResolucionProcesoPostulacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarResolucionProcesoPostulacion (@RequestBody ProcesoPostulacionDto obj) {
        log.info("ProcesoPostulacion - actualizarObservacionARFFSProcesoPostulacion",obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.actualizarResolucionProcesoPostulacion(obj);
            log.info("ProcesoPostulacion - actualizarObservacionARFFSProcesoPostulacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - actualizarResolucionProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/tieneObservacion")
    @ApiOperation(value = "tieneObservacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity tieneObservacion(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - tieneObservacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.tieneObservacion(param);
            log.info("ProcesoPostulacion - tieneObservacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - tieneObservacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/obtenerProcesoPostulacionValidarRequisito")
    @ApiOperation(value = "obtenerProcesoPostulacionValidarRequisito" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerProcesoPostulacionValidarRequisito(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - obtenerProcesoPostulacionValidarRequisito",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.obtenerProcesoPostulacionValidarRequisito(param);
            log.info("ProcesoPostulacion - obtenerProcesoPostulacionValidarRequisito","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - obtenerProcesoPostulacion","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/actualizarEnvioPrueba")
    @ApiOperation(value = "actualizarEnvioPrueba" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarEnvioPrueba(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - actualizarEnvioPrueba",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.actualizarEnvioPrueba(param);
            log.info("ProcesoPostulacion - actualizarEnvioPrueba","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - actualizarEnvioPrueba","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/validarPublicacionId")
    @ApiOperation(value = "validarPublicacionId" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity validarPublicacionId(@RequestBody ProcesoPostulacionDto param){
        log.info("ProcesoPostulacion - validarPublicacionId",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = service.validarPublicacionId(param);
            log.info("ProcesoPostulacion - validarPublicacionId","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProcesoPostulacion - validarPublicacionId","Ocurrio un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


}
