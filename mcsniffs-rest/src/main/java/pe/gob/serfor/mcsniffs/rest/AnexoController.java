package pe.gob.serfor.mcsniffs.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.AdjuntoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.DocumentoAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.EliminarDocAdjuntoEntity;
import pe.gob.serfor.mcsniffs.entity.GeneralAnexoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.UsuarioSolicitanteEntity;
import pe.gob.serfor.mcsniffs.entity.ValidarDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.ObtenerDocumentoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AnexoService;
import pe.gob.serfor.mcsniffs.service.EmailService;

@RestController
@RequestMapping(Urls.anexo.BASE)
public class AnexoController  extends AbstractRestController{
    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los anexos}
     *
     */
    @Autowired
    private AnexoService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AnexoController.class);

    @Autowired
    private EmailService enviarcorreo;

    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 1 para que el usuario lo firme}
     * @param:Anexo1Entity
     */
    @PostMapping(path = "/generaranexo1")
    @ApiOperation(value = "Generar anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo1(@RequestBody AnexoRequestEntity filtro){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo1(filtro);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - generaranexo1", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [19-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 2 para que el usuario lo firme}
     * @param:Anexo1Entity
     */
    @PostMapping(path = "/generaranexo2")
    @ApiOperation(value = "Generar anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo2(@RequestBody AnexoRequestEntity filtro){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo2(filtro);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - generarAnexo2", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [19-06-2021]
     * @modificado:
     * @descripción: {Genera en pdf el anexo 3 para que el usuario lo firme}
     * @param:Anexo1Entity
     */
    @PostMapping(path = "/generaranexo3")
    @ApiOperation(value = "Generar anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo3(@RequestBody AnexoRequestEntity filtro){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo3(filtro);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - generarAnexo3", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Descarga el anexo 4}
     * @param:Anexo1Entity
     */
    @GetMapping(path = "/descargarAnexo4")
    @ApiOperation(value = "Descargar anexo 4", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarAnexo4(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.descargarAnexo4();
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - generarAnexo3", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Obtiene parametros del anexo}
     * @param:AnexoRequestEntity
     */
    @PostMapping(path = "/obtenerAnexo")
    @ApiOperation(value = "Obtener anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<GeneralAnexoEntity>> ObtenerAnexo(@RequestBody AnexoRequestEntity obj){
        ResultEntity<GeneralAnexoEntity> result = new ResultEntity<GeneralAnexoEntity>();
        try {
            result = service.ObtenerAnexo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - ObtenerAnexo", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Se adjunta el anexo firmado}
     * @param:AnexoRequestEntity
     */
    @PostMapping ("/adjuntarAnexo")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> AdjuntarAnexo(@RequestParam("file") MultipartFile file,
                                                             @RequestParam(required = false) Integer IdProcesoPostulacion,
                                                             @RequestParam(required = false) Integer IdUsuarioAdjunta,
                                                             @RequestParam(required = false) String CodigoAnexo,
                                                             @RequestParam(required = false) Integer IdTipoDocumento,
                                                             @RequestParam(required = false) String NombreArchivo,
                                                             @RequestParam(required = false) Integer IdPostulacionPFDM,
                                                             @RequestParam(required = false) Integer idDocumentoAdjunto

                                                             ) throws IOException {

        ResultClassEntity obj = new ResultClassEntity();
        try {
            obj =service.AdjuntarAnexo(file,IdProcesoPostulacion,IdUsuarioAdjunta,CodigoAnexo,NombreArchivo,IdTipoDocumento,IdPostulacionPFDM,idDocumentoAdjunto);
            return new ResponseEntity<>(obj, HttpStatus.OK);
        } catch (Exception e) {
            obj.setSuccess(false);
            obj.setMessage("Se produjo un error al cargar archivo");
            obj.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Obtiene los documentos adjuntos por el usuario}
     * @param:AdjuntoRequestEntity
     */
    @PostMapping(path = "/obtenerAdjuntos")
    @ApiOperation(value = "Obtener adjuntos", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<DocumentoAdjuntoEntity>> ObtenerAdjuntos(@RequestBody AdjuntoRequestEntity obj){
        ResultEntity<DocumentoAdjuntoEntity> result = new ResultEntity<DocumentoAdjuntoEntity>();
        try {
            result = service.ObtenerAdjuntos(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - ObtenerAdjuntos", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [23-06-2021]
     * @modificado:
     * @descripción: {Cuando la un anexo esta incorrecto}
     * @param:AnexoRequestEntity
     */
    @PostMapping(path = "/validarAnexo")
    @ApiOperation(value = "validar anexos", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> ValidarAnexo(@RequestBody ValidarDocumentoEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.ValidarAnexo(obj);
            /*if(result.getSuccess()){ //falta implementar por lo del correo
                //Enviar correo
                EmailEntity mail = new EmailEntity();
                mail.setEmail(usuario.getCorreo());
                mail.setContent("");
                mail.setSubject("Su anexo fue observado");
                Boolean enviarmail = enviarcorreo.sendEmail(mail);
            }*/
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - ValidarAnexo", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [17-06-2021]
     * @modificado:
     * @descripción: {se obtiene la informacion del usuario postulante}
     * @param:Anexo1Entity
     */
    @PostMapping(path = "/obtenerUsuarioPostulante")
    @ApiOperation(value = "obtener Usuario Postulante", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<UsuarioSolicitanteEntity>> obtenerUsuarioSolicitante(@RequestBody AnexoRequestEntity obj){
        ResultClassEntity<UsuarioSolicitanteEntity> result = new ResultClassEntity<UsuarioSolicitanteEntity>();
        try {
            result = service.obtenerUsuarioSolicitante(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - obtenerUsuarioSolicitante", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [19-07-2021]
     * @modificado:
     * @descripción: {Inactivar documentos adjuntos por el usuario}
     * @param:AdjuntoRequestEntity
     */
    @PostMapping(path = "/eliminarDocumentoAdjunto")
    @ApiOperation(value = "eliminar Documento Adjunto", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> eliminarDocumentoAdjunto(@RequestBody EliminarDocAdjuntoEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarDocumentoAdjunto(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - eliminarDocumentoAdjunto", "Ocurrió un error al eliminar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se actualiza el estatus de anexo}
     * @param:AdjuntoRequestEntity
     */
    @PostMapping(path = "/insertarEstatusAnexo")
    @ApiOperation(value = "insertar Estatus Anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> insertarEstatusAnexo(@RequestBody AnexoRequestEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.insertarEstatusAnexo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - insertarEstatusAnexo", "Ocurrió un error al insertar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
      /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene el estatus y obs del anexo}
     * @param:AdjuntoRequestEntity
     */

    @PostMapping(path = "/actualizarEstatusAnexo")
    @ApiOperation(value = "actualizar Estatus Anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarEstatusAnexo(@RequestBody AnexoRequestEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarEstatusAnexo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - actualizarEstatusAnexo", "Ocurrió un error al insertar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene registra el estatus de los anexos}
     * @param:AdjuntoRequestEntity
     */
    @PostMapping(path = "/obtenerEstatusAnexo")
    @ApiOperation(value = "obtener Estatus Anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ValidarDocumentoEntity>> obtenerEstatusAnexo(@RequestBody AnexoRequestEntity obj){
        ResultEntity<ValidarDocumentoEntity> result = new ResultEntity<ValidarDocumentoEntity>();
        try {
            result = service.obtenerEstatusAnexo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - obtenerEstatusAnexo", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [25-07-2021]
     * @modificado:
     * @descripción: {Se obtiene el detalle de las observaciones}
     * @param:AdjuntoRequestEntity
     */
    @PostMapping(path = "/obtenerDetalleObservacion")
    @ApiOperation(value = "obtener Detalle Observacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ValidarDocumentoEntity>> obtenerDetalleObservacion(@RequestBody AnexoRequestEntity obj){
        ResultEntity<ValidarDocumentoEntity> result = new ResultEntity<ValidarDocumentoEntity>();
        try {
            result = service.obtenerDetalleObservacion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - obtenerDetalleObservacion", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarDocumentos")
    @ApiOperation(value = "obtener listarDocumentos", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> listarDocumentos(@RequestBody AdjuntoRequestEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarDocumentos(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - listarDocumentos", "Ocurrio un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/guardarObservacion")
    @ApiOperation(value = "guardarObservacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarObservacion(@RequestBody List<DocumentoAdjuntoEntity> params){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardarObservacion(params);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - listarDocumentos", "Ocurrio un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    @PostMapping(path = "/adjuntarListAnexo")
    @ApiOperation(value = "adjuntarListAnexo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> adjuntarListAnexo(@RequestBody(required = false) List<DocumentoAdjuntoDto> listaObject) {

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {

            response = service.adjuntarListAnexo(listaObject);

            log.info("anexo - adjuntarListAnexo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("anexo - adjuntarListAnexo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
//    @PostMapping(path = "/adjuntarListAnexo")
//    @ApiOperation(value = "adjuntarListAnexo", authorizations = { @Authorization(value = "JWT") })
//    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
//
//    public ResponseEntity<ResultClassEntity> adjuntarListAnexo(@RequestParam("files") List<MultipartFile> files,
//                                                                @RequestParam(required = false) String listaObject) {
//
//        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
//        ResultClassEntity response;
//        try {
//
//            ObjectMapper mapper = new ObjectMapper();
//            List<DocumentoAdjuntoDto> lista = new ArrayList<>();
//            lista = Arrays.asList(mapper.readValue(listaObject, DocumentoAdjuntoDto[].class));
//            response = service.adjuntarListAnexo(files, lista);
//
//            log.info("anexo - adjuntarListAnexo","Proceso realizado correctamente");
//            if (!response.getSuccess()) {
//                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
//            } else {
//                return new ResponseEntity(response, HttpStatus.OK);
//            }
//
//        } catch (Exception e){
//            log.error("anexo - adjuntarListAnexo","Ocurrió un error :"+ e.getMessage());
//            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
//            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }


    @PostMapping(path = "/generarInformeAnexo")
    @ApiOperation(value = "generarInformeAnexo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarInformeAnexo(@RequestBody ObtenerDocumentoDto dto, HttpServletRequest request1)
            throws Exception {
        pe.gob.serfor.mcsniffs.entity.ResponseEntity res = null;                
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("Anexo: ", dto.toString());
        try {
            String token= request1.getHeader("Authorization");
            result = service.generarInformeGeneral(dto, token); 
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            log.error("Anexo - Se descargo correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e){
            log.error("anexo - generarInformeAnexo","Ocurrió un error :"+ e.getMessage());
            res = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
