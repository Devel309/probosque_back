package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolPlantacionForestalGeometriaService;

@RestController
@RequestMapping(Urls.solPlantacionForestalGeometria.BASE)
public class SolPlantacionForestalGeometriaController {

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolPlantacionForestalGeometriaController.class);

    @Autowired
    SolPlantacionForestalGeometriaService service;

    @PostMapping(path = "/registrarSolPlantacionForestalGeometria")
    @ApiOperation(value = "registrarSolPlantacionForestalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolPlantacionForestalGeometria(
            @RequestBody List<SolPlantacionForestalGeometriaEntity> request) {
        log.info("SolPlantacionForestalGeometriaController - registrarSolPlantacionForestalGeometria",
                request.toString());
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            result = service.registrarSolPlantacionForestalGeometria(request);

            if (result.getSuccess()) {
                log.info("SolPlantacionForestalGeometriaController - registrarSolPlantacionForestalGeometria",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalGeometriaController - registrarSolPlantacionForestalGeometria",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerSolPlantacionForestalGeometria")
    @ApiOperation(value = "obtenerSolPlantacionForestalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerSolPlantacionForestalGeometria(@RequestBody SolPlantacionForestalGeometriaEntity item) {
        log.info("SolPlantacionForestalGeometriaController - obtenerSolPlantacionForestalGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.obtenerSolPlantacionForestalGeometria(item);

            if (result.getSuccess()) {
                log.info("SolPlantacionForestalGeometriaController - obtenerSolPlantacionForestalGeometria",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalGeometriaController - obtenerSolPlantacionForestalGeometria",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarSolPlantacionForestalGeometria")
    @ApiOperation(value = "listarSolPlantacionForestalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolPlantacionForestalGeometria(@RequestBody SolPlantacionForestalGeometriaEntity item) {
        log.info("SolPlantacionForestalGeometriaController - listarSolPlantacionForestalGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarSolPlantacionForestalGeometria(item);

            if (result.getSuccess()) {
                log.info("SolPlantacionForestalGeometriaController - listarSolPlantacionForestalGeometria",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalGeometriaController - listarSolPlantacionForestalGeometria",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolPlantacionForestalGeometria")
    @ApiOperation(value = "Actualizar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolPlantacionForestalGeometria(@RequestBody List<SolPlantacionForestalGeometriaEntity> request) {
        log.info("SolicitudConcesionGeometriaController - actualizarSolPlantacionForestalGeometria", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarSolPlantacionForestalGeometria(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - actualizarSolPlantacionForestalGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - actualizarSolPlantacionForestalGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

    @PostMapping(path = "/eliminarSolPlantacionForestalGeometriaArchivo")
    @ApiOperation(value = "Eliminar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolPlantacionForestalGeometriaArchivo(@RequestBody SolPlantacionForestalGeometriaEntity request) {
        log.info("SolPlantacionForestalGeometriaController - eliminarSolPlantacionForestalGeometriaArchivo", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarSolPlantacionForestalGeometriaArchivo(request);

            if (result.getSuccess()) {
                log.info("SolPlantacionForestalGeometriaController - eliminarSolPlantacionForestalGeometriaArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalGeometriaController - eliminarSolPlantacionForestalGeometriaArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

}