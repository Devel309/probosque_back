package pe.gob.serfor.mcsniffs.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalAnexoEntity;

@Data
public class DtoSolPlantacionForestalAnexo {
    SolPlantacionForestalAnexoEntity solPlantacionAnexo;
    MultipartFile file;


}