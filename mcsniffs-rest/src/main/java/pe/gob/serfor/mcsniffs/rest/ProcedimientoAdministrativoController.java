package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ProcedimientoAdministrativo.ProcedimientoAdministrativoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ProcedimientoAdministrativoService;

/**
 * @autor: Carlos Tang [27-10-2021]
 * @modificado:
 * @descripción: {
 */
@RestController
@RequestMapping(Urls.procedimientoAdministrativo.BASE)
public class ProcedimientoAdministrativoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProcedimientoAdministrativoController.class);

    @Autowired
    private ProcedimientoAdministrativoService procedimientoAdministrativoService;
 
    @PostMapping(path = "/listarProcedimientoAdministrativo")
    @ApiOperation(value = "listarProcedimientoAdministrativo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarProcedimientoAdministrativo(@RequestBody ProcedimientoAdministrativoDto param){
        log.info("ProcedimientoAdministrativo - listarProcedimientoAdministrativo",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = procedimientoAdministrativoService.listarPlanManejoEvaluacion(param);
            log.info("procedimientoAdministrativo - listarProcedimientoAdministrativo","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("procedimientoAdministrativo - listarProcedimientoAdministrativo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 
}
