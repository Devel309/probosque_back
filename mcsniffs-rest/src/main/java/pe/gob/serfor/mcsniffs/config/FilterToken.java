package pe.gob.serfor.mcsniffs.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pe.gob.serfor.mcsniffs.service.SeguridadService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.io.IOException;

@Component
public class FilterToken extends OncePerRequestFilter {

    // static String auth= null;

    @Autowired
    private  TokenConfig tokenConfig;

    @Autowired
    private SeguridadService seguridadServicio;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String auth = request.getHeader("Authorization");
        //&& auth.startsWith("Bearer")
        if(auth != null ){
            //String jwt = auth.substring(7);
            String jwt = auth;
            String username = tokenConfig.getUsername(jwt);

            if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
                UserDetails userDetails = seguridadServicio.UserByUsername(username);

                if(tokenConfig.validateToken(jwt, userDetails)){
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails,null, userDetails.getAuthorities());
                    authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }
            }
        }
        /*else{
            logger.warn("Sin tokend");
        }*/
        filterChain.doFilter(request,response);
    }
}
