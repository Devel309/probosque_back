package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaRequisitosDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ListarCondicionMinimaDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CondicionMinimaService;

@RestController
@RequestMapping(Urls.condicionMinima.BASE)
public class CondicionMinimaController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CondicionMinimaController.class);

    @Autowired
    private CondicionMinimaService condicionMinimaService;

    @PostMapping(path = "/validarCondicionMinima")
    @ApiOperation(value = "validarCondicionMinima", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity validarRequisitos(@RequestBody ConsultaRequisitosDto consultaRequisitos, HttpServletRequest request1){
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            String token= request1.getHeader("Authorization");
            response = condicionMinimaService.validarCondicionesMinimas(consultaRequisitos, token);
            log.info("CondicionMinima - validarCondicionMinima","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("CondicionMinima - validarCondicionMinima","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarCondicionMinima")
    @ApiOperation(value = "registrarCondicionMinima", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCondicionMinima(@RequestBody CondicionMinimaDto request){
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = condicionMinimaService.registrarCondicionMinima(request);
            log.info("CondicionMinima - registrarCondicionMinima","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("CondicionMinima - registrarCondicionMinima","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping(path = "/obtenerCondicionMinima")
    @ApiOperation(value = "obtenerCondicionMinima por id", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ListarCondicionMinimaDto>> obtenerCondicionMinima(
            @RequestParam(required = false) Integer idCondicionMinima) {
        log.info("obtenerCondicionMinima: {}", idCondicionMinima);
        ResultClassEntity<ListarCondicionMinimaDto> response = new ResultClassEntity<>();
        try {
            response = condicionMinimaService.obtenerCondicionMinima(idCondicionMinima);
            log.info("Service - obtenerCondicionMinima: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtenerCondicionMinima: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    

}
