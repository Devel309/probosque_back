package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionBasicaDetalle.InformacionBasicaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.FaunaEntity;
import pe.gob.serfor.mcsniffs.entity.HidrografiaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.InformacionBasicaUbigeoEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TipoBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.PoblacionAledaniaDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.InformacionBasicaService;

@RestController
@RequestMapping(Urls.informacionBasica.BASE)
public class InformacionBasicaController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(InformacionBasicaController.class);

    @Autowired
    private InformacionBasicaService informacionBasicaService;

    @PostMapping(path = "/registrarInformacionBasica")
    @ApiOperation(value = "registrarInformacionBasica", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarInformacionBasica(@RequestBody List<InformacionBasicaEntity> listInformacionBasicaEntity) {
        log.info("ProteccionBosque - registrarResumenDetalle", listInformacionBasicaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.registrarInformacionBasica(listInformacionBasicaEntity);
            log.info("ProteccionBosque - registrarResumenDetalle", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("ProteccionBosque - registrarResumenDetalle", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/registrarInformacionBasicaDetalle")
    @ApiOperation(value = "registrarInformacionBasicaDetalle", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarInformacionBasicaDetalle(
            @RequestBody List<InformacionBasicaEntity> list) {
        log.info("registrarInformacionBasicaDetalle - registrarInformacionBasicaDetalle", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.registrarInformacionBasicaDetalle(list);
            log.info("Informacion basica - registrarInformacionBasicaDetalle", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Informacion basica - registrarInformacionBasicaDetalle", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/actualizarInformacionBasicaDetalle")
    @ApiOperation(value = "actualizarInformacionBasicaDetalle", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarInformacionBasicaDetalle(
            @RequestBody List<InformacionBasicaEntity> list) {
        log.info("actualizarInformacionBasicaDetalle - actualizarInformacionBasicaDetalle", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.ActualizarInformacionBasicaDetalle(list);
            log.info("Informacion basica - actualizarInformacionBasicaDetalle", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Informacion basica - actualizarInformacionBasicaDetalle", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Alexis Salgado [04-08-2021]
     * @modificado:
     * @descripción: {Listar tipos de bosque}
     * @param: Integer idTipoBosque
     * @param: String  descripcion
     * @param: String  region
     */
    @GetMapping(path = "/obtenerTipoBosque")
    @ApiOperation(value = "obtenerTipoBosque", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente."),
            @ApiResponse(code = 404, message = "No encontrado."),
            @ApiResponse(code = 500, message = "Ocurrió un Error.") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<TipoBosqueEntity>>> obtenerTipoBosque(
            @RequestParam(defaultValue = "0") Integer idTipoBosque, @RequestParam(defaultValue = "") String descripcion,
            @RequestParam(defaultValue = "") String region) {

        log.info("InformacionBasica - obtenerTipoBosque {}", region);

        ResultClassEntity<List<TipoBosqueEntity>> response = new ResultClassEntity<>();
        try {
            response = informacionBasicaService.obtenerTipoBosque(idTipoBosque, descripcion, region);
            log.info("InformacionBasica - obtenerTipoBosque: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            log.error("InformacionBasica - obtenerTipoBosque, Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Alexis Salgado [04-08-2021]
     * @modificado: Harry Coa [02-10-2021]
     * @descripción: {Listar Fauna}
     * @param: Integer idFauna
     * @param: String  tipoFauna
     * @param: String  nombre
     * @param: String  nombreCientifico
     * @param: String  familia
     * @param: String  estatus
     */
    @GetMapping(path = "/obtenerFauna")
    @ApiOperation(value = "obtenerFauna", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente."),
            @ApiResponse(code = 404, message = "No encontrado."),
            @ApiResponse(code = 500, message = "Ocurrió un Error.") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<FaunaEntity>>> obtenerFauna(
            @RequestParam(defaultValue = "0") Integer idPlanManejo, @RequestParam(defaultValue = "") String tipoFauna,
            @RequestParam(defaultValue = "") String nombre, @RequestParam(defaultValue = "") String nombreCientifico,
            @RequestParam(defaultValue = "") String familia, @RequestParam(defaultValue = "") String estatus,
            @RequestParam(defaultValue = "") String codigoTipo) {

        log.info("InformacionBasica - obtenerFauna {},{},{},{},{},{},{}", idPlanManejo, tipoFauna, nombre, nombreCientifico,
                familia, estatus, codigoTipo);

        ResultClassEntity<List<FaunaEntity>> response = new ResultClassEntity<>();

        try {
            response = informacionBasicaService.obtenerFauna(idPlanManejo, tipoFauna, nombre, nombreCientifico, familia,
                    estatus, codigoTipo);
            log.info("InformacionBasica - obtenerFauna: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            log.error("InformacionBasica - obtenerFauna, Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/obtenerHidrografia")
    @ApiOperation(value = "Listar Hidrografia", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<HidrografiaEntity>> ObtenerHidrografia(@RequestParam(required = true) Integer idHidrografia,
                                                                         @RequestParam(defaultValue = "tipoHidrografia") String tipoHidrografia) {
        try {
            return informacionBasicaService.obtenerHidrografia(idHidrografia,tipoHidrografia);
        } catch (Exception ex) {

            ResultClassEntity<List<HidrografiaEntity>> result = new ResultClassEntity<>();
            log.error("HidrografiaController - obtenerHidrografia", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: harry coa [04-08-2021]
     * @modificado:
     * @descripción: {obtener poblacion parametro}
     */
    @GetMapping(path = "/obtenerPoblacion")
    @ApiOperation(value = "obtenerPoblacion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente."),
            @ApiResponse(code = 404, message = "No encontrado."),
            @ApiResponse(code = 500, message = "Ocurrió un Error.") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<PoblacionAledaniaDto>>> obtenerPoblacion() {
        log.info("InformacionBasica - obtenerPoblacion ");
        ResultClassEntity<List<PoblacionAledaniaDto>> response = new ResultClassEntity<>();
        try {
            response = informacionBasicaService.obtenerPoblacion();
            log.info("InformacionBasica - obtenerFauna: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            log.error("ProteccionBosque - registrarResumenDetalle, Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Abner Valdez [17-08-2021]
     * @descripción: {Registrar Archivos SHP (capas Info Basica)}
     * @param: File, InformacionBasicaEntity
     * @return: ResponseEntity<Response>
     */
    @PostMapping(path = "/registrarArchivoInfBasica")
    @ApiOperation(value = "registrarArchivoInfBasica" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarArchivoInfBasica(@RequestParam("files") MultipartFile files ,
                                                                                   @RequestParam(required = false) Integer idInfBasica,
                                                                                   @RequestParam(required = false) Integer idUsuarioRegistro,
                                                                                   @RequestParam(required = false) String estado) throws Exception{
        ResultClassEntity response = new ResultClassEntity<>(); ;
        try{
            InformacionBasicaEntity obj = new InformacionBasicaEntity();
            obj.setIdInfBasica(idInfBasica);
            obj.setIdUsuarioRegistro(idUsuarioRegistro);
            obj.setEstado(estado);  
            response = informacionBasicaService.registrarArchivoInfBasica(obj,files);
            log.info("InformacionBasica - registrarArchivoInfBasica","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("InformacionBasica - registrarArchivoInfBasica","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Abner Valdez [18-08-2021]
     * @descripción: {Listar Archivos de la tabla detalle infbasica_archiv}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @GetMapping(path = "/listarInfBasicaArchivo/{id}")
    @ApiOperation(value = "listarInfBasicaArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarInfBasicaArchivo(@PathVariable Integer id){

        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = informacionBasicaService.listarInfBasicaArchivo(id);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("InformacionBasicaController -listarInfBasicaArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    /**
     * @autor:  Abner Valdez [19-08-2021]
     * @descripción: {Eliminar Archivo de la tabla detalle infbasica_archivo}
     * @param: id y idArchivo
     * @return: ResponseEntity<Response>
     */
    @GetMapping(path = "/eliminarInfBasicaArchivo/{id}/{idArchivo}/{idUsuario}")
    @ApiOperation(value = "eliminarInfBasicaArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarInfBasicaArchivo(@PathVariable Integer id,@PathVariable Integer idArchivo,@PathVariable Integer idUsuario){

        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = informacionBasicaService.eliminarInfBasicaArchivo(id,idArchivo,idUsuario);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("InformacionBasicaController -eliminarInfBasicaArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarInformacionBasica")
    @ApiOperation(value = "actualizarInformacionBasica", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarInformacionBasica(@RequestBody List<InformacionBasicaEntity> informacionBasicaEntity) {
        log.info("Informacion Basica - actualizarInformacionBasica", informacionBasicaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.actualizarInformacionBasica(informacionBasicaEntity);
            log.info("Informacion Basica - actualizarInformacionBasica", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Informacion Basica - actualizarInformacionBasica", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @autor:  Jason Retamozo [26-01-2022]
     * @descripción: {Listado de cabecera,detalle,sub para informacion socioeconomica}
     * @param: idPlanManejo y codProceso
     * @return: ResponseEntity<InformacionBasicaEntity>
     */
    @GetMapping(path = "/listarInformacionSocioeconomica")
    @ApiOperation(value = "listarInformacion Socioeconomica por idPlanManejo y codProceso", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarInformacionSocioeconomica(
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = true) String codigoTipoInfBasica,
            @RequestParam(required = false) String subCodigoTipoInfBasica){
        log.info("Listar informacion socioeconomica: {}", idPlanManejo);
        ResultClassEntity<List<List<InformacionBasicaEntity>>> response = new ResultClassEntity<>();
        try {
            response= informacionBasicaService.listarInformacionSocioeconomica(idPlanManejo,codigoTipoInfBasica,subCodigoTipoInfBasica);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            ResultClassEntity<List<List<InformacionBasicaEntity>>> result = new ResultClassEntity<>();
            log.error("InfBasicaAereaController - listarInfBasicaAerea", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/listarInformacionBasica")
    @ApiOperation(value = "listarInformacionBasica por idPlanManejo", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<InformacionBasicaEntity>>> listarInformacionBasica(
            @RequestParam(defaultValue = "0") Integer idPlanManejo) {
        log.info("Listar parametro: {}", idPlanManejo);
        ResultClassEntity<List<InformacionBasicaEntity>> response = new ResultClassEntity<>();
        try {
            response = informacionBasicaService.ListarInformacionBasica(idPlanManejo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping(path = "/listarInformacionBasicaPorFiltro")
    @ApiOperation(value = "listarInformacionBasica por idPlanManejo, codTipoInfBasica", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<InformacionBasicaEntity>>> listarInformacionBasicaPorFiltro(
            @RequestParam(required = false) Integer idPlanManejo, @RequestParam(required = false) String codigoTipoInfBasica) {
        log.info("Listar parametro: {}", idPlanManejo+" "+codigoTipoInfBasica);
        ResultClassEntity<List<InformacionBasicaEntity>> response = new ResultClassEntity<>();
        try {
            response = informacionBasicaService.ListarInformacionBasicaPorFiltro(idPlanManejo, codigoTipoInfBasica);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/eliminarInfBasicaAerea")
    @ApiOperation(value = "eliminar InfBasicaAerea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarInfBasicaAerea(@RequestBody InfBasicaAereaDetalleDto infBasicaAereaDetalleDto){
        log.info("InfBasicaAerea - eliminarInfBasicaAerea",infBasicaAereaDetalleDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = informacionBasicaService.EliminarInfBasicaAerea(infBasicaAereaDetalleDto);
            log.info("RecursoForestal - eliminarRecursoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("RecursoForestal - eliminarRecursoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/listarInfBasicaAerea")
    @ApiOperation(value = "Listar InfBasicaAerea", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<InfBasicaAereaDetalleDto>> listarInfBasicaAerea(
            @RequestParam("idInfBasica")String idInfBasica,
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = false) String codCabecera) {
        try {
            return informacionBasicaService.listarInfBasicaAerea(idInfBasica,idPlanManejo,codCabecera);
        } catch (Exception ex) {

            ResultClassEntity<List<InfBasicaAereaDetalleDto>> result = new ResultClassEntity<>();
            log.error("InfBasicaAereaController - listarInfBasicaAerea", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/listarInfBasicaAereaTitular")
    @ApiOperation(value = "Listar InfBasicaAerea", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<InfBasicaAereaDetalleDto>> listarInfBasicaAereaTitular(
            @RequestParam(required = true) String tipoDocumento,
            @RequestParam(required = true) String nroDocumento,
            @RequestParam(required = true) String codigoProcesoTitular,
            @RequestParam(required = true) String subcodigoProcesoTitular,
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = true) String codigoProceso,
            @RequestParam(required = true) String subCodigoProceso) {

        try {
            return informacionBasicaService.listarInfBasicaAereaTitular(tipoDocumento,nroDocumento,codigoProcesoTitular,subcodigoProcesoTitular,idPlanManejo,codigoProceso,subCodigoProceso);
        } catch (Exception ex) {

            ResultClassEntity<List<InfBasicaAereaDetalleDto>> result = new ResultClassEntity<>();
            log.error("InfBasicaAereaController - listarInfBasicaAerea", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
	 * @autor: Jaqueline Diaz Barrientos [04-10-2021]
	 * @modificado:
	 * @descripción: {Eliminar fauna}
	 * @param: InfBasicaAereaDetalleDto
	 */
    @PostMapping(path = "/EliminarFauna")
    @ApiOperation(value = "eliminar fauna" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarFauna(@RequestBody InfBasicaAereaDetalleDto infBasicaAereaDetalleDto){
        log.info("InfBasicaAerea - EliminarFauna",infBasicaAereaDetalleDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = informacionBasicaService.EliminarFauna(infBasicaAereaDetalleDto);
            log.info("RecursoForestal - EliminarFauna","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("RecursoForestal - EliminarFauna","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/registrarSolicitudFauna")
    @ApiOperation(value = "actualizarInformacionBasica", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarSolicitudFauna(@RequestBody FaunaEntity faunaEntity) {
        log.info("Informacion Basica - registrarSolicitudFauna", faunaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.registrarSolicitudFauna(faunaEntity);
            log.info("Informacion Basica - registrarSolicitudFauna", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Informacion Basica - registrarSolicitudFauna", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @autor:  Danny Nazario [22-12-2021]
     * @modificado:
     * @descripción: {Listar Detalle Información Básica Área}
     * @param: InfBasicaAereaDetalleDto
     */
    @PostMapping(path = "/listarInfBasicaAreaDetalle")
    @ApiOperation(value = "listarInfBasicaAreaDetalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public org.springframework.http.ResponseEntity listarInfBasicaAreaDetalle(@RequestParam String codTipo,
                                                                              @RequestParam String codSubTipo,
                                                                              @RequestParam Integer idInfBasica) {
        log.info("InformacionBasica - listarInfBasicaAreaDetalle", codTipo, codSubTipo, idInfBasica);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = informacionBasicaService.listarInfBasicaAreaDetalle(codTipo, codSubTipo, idInfBasica);

            if (result.getSuccess()) {
                log.info("InformacionBasica - listarInfBasicaAreaDetalle", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error("InformacionBasica - listarInfBasicaAreaDetalle", "Ocurrió un error : " + ex.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/registrarInformacionBasicaUbigeo")
    @ApiOperation(value = "registrarInformacionBasicaUbigeo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarInformacionBasicaUbigeo(
            @RequestBody InformacionBasicaEntity dto) {
        log.info("registrarInformacionBasicaUbigeo - registrarInformacionBasicaUbigeo", dto.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.registrarInformacionBasicaUbigeo(dto);
            log.info("Informacion basica - registrarInformacionBasicaUbigeo", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Informacion basica - registrarInformacionBasicaUbigeo", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarInfBasicaUbigeo")
    @ApiOperation(value = "listarInfBasicaUbigeo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public org.springframework.http.ResponseEntity listarInfBasicaUbigeo(@RequestBody InformacionBasicaEntity obj) {
        log.info("InformacionBasica - listarInfBasicaUbigeo", obj);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = informacionBasicaService.listarInfBasicaUbigeo(obj);

            if (result.getSuccess()) {
                log.info("InformacionBasica - listarInfBasicaUbigeo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error("InformacionBasica - listarInfBasicaUbigeo", "Ocurrió un error : " + ex.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarInformacionBasicaUbigeo")
    @ApiOperation(value = "eliminarInformacionBasicaUbigeo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarInformacionBasicaUbigeo(
            @RequestBody InformacionBasicaUbigeoEntity dto) {
        log.info("eliminarInformacionBasicaUbigeo - eliminarInformacionBasicaUbigeo", dto.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.eliminarInformacionBasicaUbigeo(dto);
            log.info("Informacion basica - eliminarInformacionBasicaUbigeo", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Informacion basica - eliminarInformacionBasicaUbigeo", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarFaunaInformacionBasicaDetalle")
    @ApiOperation(value = "listarFaunaInformacionBasicaDetalle", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarFaunaInformacionBasicaDetalle(
            @RequestBody InformacionBasicaDetalleDto dto) {
        log.info("InformacionBasica - ListarFaunaInformacionBasicaDetalle {}", dto.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = informacionBasicaService.ListarFaunaInformacionBasicaDetalle(dto);
            log.info("InformacionBasica- ListarFaunaInformacionBasicaDetalle {}", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("InformacionBasica - ListarFaunaInformacionBasicaDetalle {}", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarPorFiltrosInfBasicaAerea")
    @ApiOperation(value = "listarPorFiltrosInfBasicaAerea", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public org.springframework.http.ResponseEntity listarPorFiltrosInfBasicaAerea(@RequestBody InfBasicaAereaDetalleDto param) {

        log.info("InformacionBasica - listarPorFiltrosInfBasicaAerea", param);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = informacionBasicaService.listarPorFiltrosInfBasicaAerea(param);

            if (result.getSuccess()) {
                log.info("InformacionBasica - listarPorFiltrosInfBasicaAerea", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error("InformacionBasica - llistarPorFiltrosInfBasicaAerea", "Ocurrió un error : " + ex.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
