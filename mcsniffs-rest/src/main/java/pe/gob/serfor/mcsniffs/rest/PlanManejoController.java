package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.Colindancia.ColindanciaPredioDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarDeclaracionManejoProductosForestales.DescargarArchivoDeclaracionManejoProductosForestalesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarPGMF.DescargarPGMFDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContrato;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;

/**
 * @autor: Harry Coa [02-09-2021]
 * @modificado:
 * @descripción: { Controlador para Plan Manejo}
 */
@RestController
@RequestMapping(Urls.planManejo.BASE)
public class PlanManejoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PlanManejoController.class);

    @Autowired
    private PlanManejoService planManejoService;

    @Autowired
    private ArchivoPDFPGMFAService archivoPDFPGMFAService;

    @Autowired
    private ArchivoPDFPMFICService archivoPDFPMFICService;

    @Autowired
    private ArchivoPDFDEMAService archivoPDFDEMAService;


    @Autowired
    private ArchivoPDFPMFIService archivoPDFPMFIService;

    @Autowired
    private ArchivoPDFPOPACService archivoPDFPOPACService;

    @Autowired
    private ArchivoPDFPOACService archivoPDFPOACService;

    @Autowired
    private ArchivoPDFPOCCService archivoPDFPOCCService;

    @Autowired
    private ColindanciaPredioService colindanciaPredioService;

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ArchivoDeclaracionManejoProductosForestalesService archivoDeclaracionManejoProductosForestalesService;

    @Autowired
    private ArchivoPGMFService archivoPGMFService;

    @Autowired
    private ArchivoPDFPOPACANEXOService archivoPDFPOPACANEXOService;

    @Autowired
    private ArchivoPDFPMFICANEXOService archivoPDFPMFICANEXOService;

    @Autowired
    private RegenteService regenteService;


    @PostMapping(path = "/registrarPlanManejo")
    @ApiOperation(value = "registrarPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanManejo(@RequestBody PlanManejoEntity planManejoEntity){
        log.info("PlanManejo- RegistrarPlanManejo", planManejoEntity);
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = planManejoService.RegistrarPlanManejo(planManejoEntity);
            log.info("PlanManejo - RegistrarPlanManejo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("PlanManejo -RegistrarPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/listarPlanManejo")
    @ApiOperation(value = "listarPlanManejo por idContrato", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<PlanManejoEntity>> ListarPlanManejo(
            @RequestBody PlanManejoEntity planManejoEntity) {
        log.info("Listar ListarPlanManejo: {}", planManejoEntity);
        ResultClassEntity<PlanManejoEntity> response = new ResultClassEntity<>();
        try {
            response = planManejoService.ListarPlanManejo(planManejoEntity);
            log.info("Service - ListarPlanManejo: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - ListarPlanManejo: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
	 * @autor: Alexis Salgado 11-09-2021
	 * @modificado: Alexis Salgado 20-09-2021
	 * @descripción: {Listar/Filtrar Plan Manejo}
	 * @param: Integer idPlanManejo
	 * @param: Integer idTipoProceso
	 * @param: Integer idTipoPlan
	 * @param: Integer idContrato
	 * @param: String dniElaborador
	 * @param: String rucComunidad
	 * @param: String rucComunidad
	 * @param: String rucComunidad
	 */
    @GetMapping(path = "")
    @ApiOperation(value = "Listar y/o Buscar(Filtrar) PlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public Pageable<List<PlanManejoEntity>> filtrar(@RequestParam(required = false) Integer idPlanManejo, //
            @RequestParam(required = false) Integer idTipoProceso, //
            @RequestParam(required = false) Integer idTipoPlan, //
            @RequestParam(required = false) Integer idContrato, //
            @RequestParam(required = false) String dniElaborador, //
            @RequestParam(required = false) String rucComunidad, //
            @RequestParam(required = false) String nombreTitular, //
            @RequestParam(required = false) String codigoEstado,
            @RequestParam(required = false, defaultValue = "1") Long pageNumber, //
            @RequestParam(required = false, defaultValue = "10") Long pageSize,
            @RequestParam(required = false) String sortField,
            @RequestParam(required = false, defaultValue = "DESC") String sortType) throws Exception {
        Page p = new Page(pageNumber, pageSize, sortField, sortType);
        log.info("obtener: {}, {}, {}, {},{}, {}, pageable={}", idPlanManejo, idTipoProceso, idTipoPlan, idContrato,
                dniElaborador, rucComunidad,nombreTitular, p);
        try {
            Pageable<List<PlanManejoEntity>> response = planManejoService.filtrar(idPlanManejo, idTipoProceso,
                    idTipoPlan, idContrato, dniElaborador, rucComunidad,nombreTitular, codigoEstado, p);
            log.info("Service - obtener: Proceso realizado correctamente");
            return response;

        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {Guardar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @PostMapping(path = "/registrarEstadoPlanManejo")
    @ApiOperation(value = "registra rEstado Plan Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarEstadoPlanManejo(@RequestBody List<PlanManejoEstadoEntity> obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.registrarEstadoPlanManejo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejoController - registrarEstadoPlanManejo", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {actualizar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @PostMapping(path = "/actualizarEstadoPlanManejo")
    @ApiOperation(value = "actualizar rEstado Plan Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarEstadoPlanManejo(@RequestBody PlanManejoEstadoEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.actualizarEstadoPlanManejo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejoController - actualizarEstadoPlanManejo", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Jason Retamozo [30-03-2022]
     * @modificado:
     * @descripción: {listar informacion del plan de manejo de un titular ingresando nro y tipo de documento}
     * @param:PlanManejoEntity
     */
    @GetMapping(path = "/obtenerPlanesManejoTitular")
    @ApiOperation(value = "Informacion de Plan por nro documento y tipo documento de Titular", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PlanManejoEntity>> obtenerPlanesManejoTitular(
            @RequestParam(defaultValue = "") String nroDocumento,
            @RequestParam(defaultValue = "") String tipoDocumento) throws Exception{
        ResultEntity<PlanManejoEntity> result = new ResultEntity<PlanManejoEntity>();
        try {
            result = planManejoService.obtenerPlanesManejoTitular(nroDocumento,tipoDocumento);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejoController - obtenerPlanesManejoTitular", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Jason Retamozo [17-03-2022]
     * @modificado:
     * @descripción: {listar informacion del plan de manejo}
     * @param:PlanManejoInformacionEntity
     */
    @GetMapping(path = "/obtenerInformacionPlan")
    @ApiOperation(value = "Informacion de Plan por IdPlanManejo y tipoPlan", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PlanManejoInformacionEntity>> obtenerInformacionPlan(
            @RequestParam(defaultValue = "0") Integer idPlanManejo,
            @RequestParam(defaultValue = "") String tipoPlan) throws Exception{
        ResultEntity<PlanManejoInformacionEntity> result = new ResultEntity<PlanManejoInformacionEntity>();
        try {
            result = planManejoService.obtenerInformacionPlan(idPlanManejo,tipoPlan);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejoController - listarEstadoPlanManejo", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [15-09-2021]
     * @modificado:
     * @descripción: {listar estado del plan de manejo}
     * @param:PlanManejoEstadoEntity
     */
    @PostMapping(path = "/listarEstadoPlanManejo")
    @ApiOperation(value = "listar rEstado Plan Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PlanManejoEstadoEntity>> listarEstadoPlanManejo(@RequestBody PlanManejoEstadoEntity obj){
        ResultEntity<PlanManejoEstadoEntity> result = new ResultEntity<PlanManejoEstadoEntity>();
        try {
            result = planManejoService.listarEstadoPlanManejo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejoController - listarEstadoPlanManejo", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(path = "/DatosGeneralesPlan")
    @ApiOperation(value = "DatosGeneralesPlan por IdPlanManejo", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<PlanManejoDto>> DatosGeneralesPlan(
            @RequestParam(defaultValue = "0") Integer idPlanManejo
    ) {
        log.info("Listar parametro: {}", idPlanManejo);
        ResultClassEntity<PlanManejoDto> response = new ResultClassEntity<>();
        try {
            response = planManejoService.DatosGeneralesPlan(idPlanManejo);
            log.info("Service - Listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - Listar: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
	 * @autor: Alexis Salgado 15-09-2021
	 * @modificado:
	 * @descripción: {Obtener Archivo Consolidado DEMA}
	 * @param: Integer idPlanManejo	 
	 */
    @GetMapping(path = "/consolidado")
    @ApiOperation(value = "Obtener Archivo consolidado DEMA", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidado(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
                ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidado: idPlanManejo:{},", idPlanManejo);
        try {
            ByteArrayResource bytes = planManejoService.consolidado(idPlanManejo);
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("DemaConsolidado.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se genero el consolidado de DEMA.");
            log.error("Service - Se descargo la dema correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    @PostMapping(path = "/registrarArchivoPlanManejo")
    @ApiOperation(value = "registrarArchivoPlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarArchivoPlanManejo(
                                                                                @RequestParam(required = false) Integer idPlanManejoArchivo,
                                                                                @RequestParam(required = false) Integer idUsuarioRegistro,
                                                                                @RequestParam(required = false) Integer idPlanManejo,
                                                                                @RequestParam(required = false) String codTipo,
                                                                                @RequestParam(required = false) String codTipoDocumento,
                                                                                @RequestParam(required = false) String subCodTipo,
                                                                                @RequestParam("file") MultipartFile file
                                                                                ){
        log.info("PlanManejoController - registrarArchivoPlanManejo");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response =new ResultClassEntity();
        try{

            response = planManejoService.registrarArchivoPlanManejo(idPlanManejoArchivo,idUsuarioRegistro, idPlanManejo,codTipo,codTipoDocumento,subCodTipo, file);
            
            log.info("InformacionAreaManejo - registrarArchivoPlanManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            //return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -registrarArchivoPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, e.getMessage(), Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: IVAN MINAYA [15-09-2021]
     * @modificado:
     * @descripción: {listar por filtro Plan Manejo Archivo}
     * @param:PlanManejoEstadoEntity
     */
    @PostMapping(path = "/listarPorFiltroPlanManejoArchivo")
    @ApiOperation(value = "listarPorFiltroPlanManejoArchivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPorFiltroPlanManejoArchivo(@RequestBody PlanManejoArchivoEntity obj){
        log.info("PlanManejo - ListarPorFiltroPlanManejoArchivo",obj.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.ListarPorFiltroPlanManejoArchivo(obj);
            log.info("PlanManejo - ListarPorFiltroPlanManejoArchivo","Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejo - ListarPorFiltroPlanManejoArchivo", "Ocurrió un error: ");
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    /**
	 * @autor: Jaqueline Diaz Barrientos 28-09-2021
	 * @modificado:
	 * @descripción: {Obtener Archivo Consolidado PMFI}
	 * @param: Integer idPlanManejo	 
	 */
    @GetMapping(path = "/consolidadoPMFI")
    @ApiOperation(value = "Obtener Archivo consolidadoPMFI", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPMFI(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
                ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPMFI: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPMFI(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPMFI.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Forestal Intermedio - PMFI.");
            log.error("Service - Se descargo la dema correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
            /*HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", idPlanManejo, LocalDateTime.now());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña 19-11-2021
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado PGMFA}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoPGMFA")
    @ApiOperation(value = "Obtener Archivo consolidadoPGMFA", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPGMFA(
             @RequestParam(required = true) Integer idPlanManejo,
             @RequestParam(required = false) String extension)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPMFI: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPGMFA(idPlanManejo,extension);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            //result.setNombeArchivo("consolidadoPGMFA.docx");
            if(extension!=null){
                if(extension.equals("PDF")){
                    result.setNombeArchivo("consolidadoPGMFA.pdf");
                }else{result.setNombeArchivo("consolidadoPGMFA.docx");}
            }else{result.setNombeArchivo("consolidadoPGMFA.docx");}
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan General de Manejo Forestal - PGMFA.");
            log.error("Service - Se descargo la dema correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
            /*HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", idPlanManejo, LocalDateTime.now());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña 14-12-2021
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado POCC}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoPOCC")
    @ApiOperation(value = "Obtener Archivo consolidadoPOCC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOCC(
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = false) String extension)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOCC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPOCC(idPlanManejo,extension);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
           if(extension!=null){
               if(extension.equals("PDF")){
                   result.setNombeArchivo("consolidadoPOCC.pdf");
               }else{result.setNombeArchivo("consolidadoPOCC.docx");}
           }else{result.setNombeArchivo("consolidadoPOCC.docx");}


            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Operativo para concesiones - POCC.");
            log.error("Service - Se descargo el POCC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
            /*HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", idPlanManejo, LocalDateTime.now());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña 13-01-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado POAC}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoPOAC")
    @ApiOperation(value = "Obtener Archivo consolidadoPOAC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOAC(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPOAC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOAC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Operativo Anual - POAC.");
            log.error("Service - Se descargo el POAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
            /*HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", idPlanManejo, LocalDateTime.now());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Jason Retamozo 07-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado PMFIC}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoPMFIC")
    @ApiOperation(value = "Obtener Archivo consolidadoPMFIC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
        public ResponseEntity<ResultArchivoEntity> consolidadoPMFIC(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPMFIC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPMFIC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPMFIC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Intermedio - PMFIC.");
            log.error("Service - Se descargo el PMFIC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado Anexos PMFIC}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoAnexosPMFIC")
    @ApiOperation(value = "Obtener Archivo consolidadoAnexosPMFIC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoAnexosPMFIC(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoAnexosPMFIC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoAnexosPMFIC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoAnexosPMFIC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Intermedio - PMFIC.");
            log.error("Service - Se descargo los anexos de PMFIC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña 09-03-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado Anexos PMFIC PDF}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/pdf/consolidadoAnexosPMFIC")
    @ApiOperation(value = "Obtener Archivo consolidadoAnexosPMFIC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoAnexosPMFIC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoAnexosPMFIC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPMFICANEXOService.consolidadoPMFICANEXO_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoAnexosPMFIC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Intermedio - PMFIC.");
            log.error("Service - Se descargo los anexos de PMFIC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña 18-03-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado Anexos PMFI PDF}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/pdf/consolidadoPMFI")
    @ApiOperation(value = "Obtener Archivo consolidadoPMFI", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoAnexosPMFI_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoAnexosPMFI: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPMFIService.consolidadoPMFI_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoAnexosPMFI.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Intermedio - PMFI.");
            log.error("Service - Se descargo los anexos de PMFI correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña 09-03-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado Anexos POPAC PDF}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/pdf/consolidadoAnexosPOPAC")
    @ApiOperation(value = "Obtener Archivo consolidadoAnexosPOPAC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoAnexosPOPAC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoAnexosPOPAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPOPACANEXOService.consolidadoPOPACANEXO_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoAnexosPOPAC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del  - POPAC.");
            log.error("Service - Se descargo los anexos de POPAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña 15-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado Anexos POPAC}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoAnexosPOPAC")
    @ApiOperation(value = "Obtener Archivo consolidadoAnexosPOPAC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoAnexosPOPAC(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoAnexosPOPAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoAnexosPOPAC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoAnexosPOPAC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Intermedio - POPAC.");
            log.error("Service - Se descargo los anexos de POPAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña 11-02-2022
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado POPAC}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoPOPAC")
    @ApiOperation(value = "Obtener Archivo consolidadoPOPAC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOPAC(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOPAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPOPAC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOPAC.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Operativo PMFI en comunidades campesinas y nativas ");
            log.error("Service - Se descargo el consolidado POPAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
            /*HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", idPlanManejo, LocalDateTime.now());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña 30-12-2021
     * @modificado:
     * @descripción: {Obtener Archivo Consolidado PFCR}
     * @param: Integer idPlanManejo
     */
    @GetMapping(path = "/consolidadoPFCR")
    @ApiOperation(value = "Obtener Archivo consolidadoPFCR", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPFCR(@RequestParam(required = true) Integer idPermisoForestal)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOCC: idPermisoForestal:{},", idPermisoForestal);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPFCR(idPermisoForestal);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPFCR.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado de Permisos Forestales - PFCR.");
            log.error("Service - Se descargo el PFCR correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
            /*HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", idPlanManejo, LocalDateTime.now());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Nelson Coqchi 07-10-2021
     * @modificado:
     * @descripción: {generarEvaluacionCompiladaPGMF}
     * @param: Integer idPlanManejo
     */
    /*@GetMapping(path = "/generarEvaluacionCompiladaPGMF")
    @ApiOperation(value = "Obtener Archivo compilado evaluación del PGMF", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarEvaluacionCompiladaPGMF(@RequestParam(required = true) Integer idPlanManejo,
                                                                              @RequestParam(required = true) Integer idPlanManejoEvaluacion,
                                                                              @RequestParam(required = false) String nombreUsuarioArffs,
                                                                              @RequestParam(required = false) String tipoDocumento)*/

    @PostMapping(path = "/generarEvaluacionCompiladaPGMF")
    @ApiOperation(value = "Obtener Archivo compilado evaluación del PGMF", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity generarEvaluacionCompiladaPGMF(@RequestBody CompiladoPgmfDto request)
            throws Exception {
        ResultClassEntity result = new ResultClassEntity();

        log.info("generarEvaluacionCompiladaPGMF: PlanManejoArchivoDto=", request.toString());
        try {
            result = planManejoService.obtenerArchivosCompiladosRelacionados(request);
            /*
            ByteArrayResource bytes = planManejoService.generarEvaluacionCompiladaPGMF(idPlanManejo, idPlanManejoEvaluacion, nombreUsuarioArffs, tipoDocumento);
            log.info("Service - generarEvaluacionCompiladaPGMF");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("evaluacionCompiladaPGMF.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el compilado evaluación del PGMF correctamente.");*/
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - generarEvaluacionCompiladaPGMF: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    @PostMapping(path = "/eliminarPlanManejoArchivo")
    @ApiOperation(value = "eliminarPlanManejoArchivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarPlanManejoArchivo(@RequestBody PlanManejoArchivoDto request){
        log.info("PlanManejo - eliminarPlanManejoArchivo",request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.eliminarPlanManejoArchivo(request);
            log.info("PlanManejo - eliminarPlanManejoArchivo","Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejo - eliminarPlanManejoArchivo", "Ocurrió un error: ");
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/obtenerPlanManejoArchivo")
    @ApiOperation(value = "obtenerPlanManejoArchivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerPlanManejoArchivo(@RequestBody PlanManejoArchivoDto request){
        log.info("PlanManejo - obtenerPlanManejoArchivo",request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.obtenerPlanManejoArchivo(request);
            log.info("PlanManejo - eliminarPlanManejoArchivo","Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejo - obtenerPlanManejoArchivo", "Ocurrió un error: ");
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    @PostMapping(path = "/actualizarPlanManejoArchivo")
    @ApiOperation(value = "actualizarPlanManejoArchivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarPlanManejoArchivo(@RequestBody PlanManejoArchivoDto request){
        log.info("PlanManejo - actualizarPlanManejoArchivo",request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.actualizarPlanManejoArchivo(request);
            log.info("PlanManejo - eliminarPlanManejoArchivo","Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejo - actualizarPlanManejoArchivo", "Ocurrió un error: ");
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarPlanManejoEvaluacionIter")
    @ApiOperation(value = "listarPlanManejoEvaluacionIter", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarPlanManejoEvaluacionIter(
            @RequestBody PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto) {
        log.info("PlanManejo - listarPlanManejoEvaluacionIter: {}", planManejoEvaluacionIterDto);
        ResultEntity result =new ResultEntity();
        try {
            result = planManejoService.listarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
            log.info("PlanManejo - listarPlanManejoEvaluacionIter","Proceso realizado correctamente");

            if (result.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejo - listarPlanManejoEvaluacionIter: Ocurrió un error: {}", e.getMessage());

            result.setIsSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(path = "/registrarPlanManejoEvaluacionIter")
    @ApiOperation(value = "registrarPlanManejoEvaluacionIter" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanManejoEvaluacionIter(@RequestBody PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto){
        log.info("PlanManejo - registrarPlanManejoEvaluacionIter", planManejoEvaluacionIterDto);

        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoService.registrarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
            log.info("PlanManejo - registrarPlanManejoEvaluacionIter","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("PlanManejo - registrarPlanManejoEvaluacionIter","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarPlanManejoEvaluacionIter")
    @ApiOperation(value = "actualizarPlanManejoEvaluacionIter", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> actualizarPlanManejoEvaluacion(@RequestBody PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.actualizarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
            log.info("PlanManejoEvaluacion - actualizarPlanManejoEvaluacion","Proceso realizado correctamente");

            if (result.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e){
            log.error("PlanManejoEvaluacion - actualizarPlanManejoEvaluacion", "Ocurrió un error al guardar en: "+e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarPlanManejoEvaluacionIter")
    @ApiOperation(value = "eliminarPlanManejoEvaluacionIter" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarPlanManejoEvaluacion(@RequestBody PlanManejoEvaluacionIterDto planManejoEvaluacionIterDto){
        log.info("PlanManejo - eliminarPlanManejoEvaluacionIter",planManejoEvaluacionIterDto.toString());

        ResultClassEntity response = new ResultClassEntity();
        try{
            response = planManejoService.eliminarPlanManejoEvaluacionIter(planManejoEvaluacionIterDto);
            log.info("PlanManejo - eliminarPlanManejoEvaluacionIter","Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("PlanManejo - eliminarPlanManejoEvaluacionIter","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @GetMapping(path = "/listarPlanManejoContrato")
    @ApiOperation(value = "listarPlanManejoContrato ", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<PlanManejoObtenerContrato>> listarPlanManejoContrato(
        @RequestParam(required = false) Integer idPlanManejo) {
        log.info("Listar listarPlanManejoContrato: {}", idPlanManejo);
        ResultClassEntity<PlanManejoObtenerContrato> response = new ResultClassEntity<>();
        try {
            response = planManejoService.listarPlanManejoContrato(idPlanManejo);
            log.info("Service - ListarPlanManejo: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - ListarPlanManejo: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/guardarPlanManejoArchivo")
    @ApiOperation(value = "guardarPlanManejoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanManejoEvaluacionArchivo(
                                                                                 @RequestParam(name="file", required = false) MultipartFile file,
                                                                                 @RequestParam(required = false) Integer idPlanManejoArchivo,
                                                                                 @RequestParam(required = false) Integer idUsuario,
                                                                                 @RequestParam(required = false) String idTipoDocumento,
                                                                                 @RequestParam(required = false) Integer idArchivo,
                                                                                 HttpServletRequest request1
                                                                                 ){
        log.info("Bandeja Plan operativo - registrarPlanManejoEvaluacionArchivo");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            PlanManejoArchivoDto archivoDto = new PlanManejoArchivoDto();
            archivoDto.setIdPlanManejoArchivo(idPlanManejoArchivo);
            archivoDto.setIdUsuarioModificacion(idUsuario);
            archivoDto.setIdTipoDocumento(idTipoDocumento);
            archivoDto.setIdArchivo(idArchivo);

            response = planManejoService.guardarArchivoPlanManejoArchivo(archivoDto, file);
            log.info("PlanManejoEvaluacion - registrarPlanManejoEvaluacionArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - registrarPlanManejoEvaluacionArchivo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/obtenerPlanManejo")
    @ApiOperation(value = "obtenerPlanManejo", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarPlanManejoEvaluacionIter(
            @RequestBody PlanManejoDto planManejoEvaluacionIterDto) {
        log.info("PlanManejo - ObtenerPlanManejo: {}", planManejoEvaluacionIterDto);
        ResultClassEntity result =new ResultClassEntity();
        try {
            result = planManejoService.ObtenerPlanManejo(planManejoEvaluacionIterDto);
            log.info("PlanManejo - ObtenerPlanManejo","Proceso realizado correctamente");

            if (result.getSuccess()) {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejo - ObtenerPlanManejo: Ocurrió un error: {}", e.getMessage());

            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/actualizarPlanManejoEstado")
    @ApiOperation(value = "actualizar Plan Manejo codigo estado", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarPlanManejoEstado(@RequestBody PlanManejoDto obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planManejoService.actualizarPlanManejoEstado(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanManejoController - actualizarEstadoPlanManejo", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);

        }
    }


    @PostMapping(path = "/obtenerPlanManejoDetalleContrato")
    @ApiOperation(value = "obtenerPlanManejoDetalleContrato" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerPlanManejoDetalleContrato(@RequestBody PlanManejoArchivoDto filtro){
        log.info("planManejo - obtenerPlanManejoDetalleContrato",filtro.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = planManejoService.obtenerPlanManejoDetalleContrato(filtro);
            log.info("planManejo - obtenerPlanManejoDetalleContrato","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("planManejo - obtenerPlanManejoDetalleContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [03-12-2021]
     * @descripción: {Listar plan manejo archivo}
     * @param: PlanManejoArchivoDto
     * @return: ResponseEntity<ResponseVO>
     */


    @PostMapping(path = "/listarPlanManejoListar")
    @ApiOperation(value = "Listar PlanManejoListar", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<PlanManejoArchivoDto>> listarPlanManejoListar(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) Integer idArchivo,
            @RequestParam(required = false) String tipoDocumento,
            @RequestParam(required = false) String codigoProceso,
            @RequestParam(required = false) String extension) {
        try {
            return planManejoService.listarPlanManejoListar(idPlanManejo,idArchivo,tipoDocumento,codigoProceso,extension);
        } catch (Exception ex) {

            ResultClassEntity<List<PlanManejoArchivoDto>> result = new ResultClassEntity<>();
            log.error("ProteccionBosqueController - listarProteccionBosque", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/PlanManejoArchivoEntidad")
    @ApiOperation(value = "Listar PlanManejoListar", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<PlanManejoArchivoDto>> PlanManejoArchivoEntidad(
            @RequestParam(required = true) Integer idPlanManejoArchivo) {
        try {
            return planManejoService.PlanManejoArchivoEntidad(idPlanManejoArchivo);
        } catch (Exception ex) {

            ResultClassEntity<List<PlanManejoArchivoDto>> result = new ResultClassEntity<>();
            log.error("ProteccionBosqueController - PlanManejoArchivoEntidad", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor:  Danny Nazario [17-12-2021]
     * @descripción: {Listar Plan Manejo Anexo Archivo}
     * @param: PlanManejoArchivoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPlanManejoAnexoArchivo")
    @ApiOperation(value = "listarPlanManejoAnexoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<PlanManejoArchivoEntity>> listarOtorgamientoArchivo(@RequestBody PlanManejoArchivoEntity filtro) {
        log.info("PlanManejo - listarPlanManejoAnexoArchivo", filtro.toString());
        ResultClassEntity<PlanManejoArchivoEntity> response = new ResultClassEntity<>();
        try {
            response = planManejoService.listarPlanManejoAnexoArchivo(filtro);
            log.info("PlanManejo - listarPlanManejoAnexoArchivo", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PlanManejo - listarPlanManejoAnexoArchivo", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [29-03-2022]
     * @descripción: {Registrar Plan Manejo Contrato}
     * @param: PlanManejoContratoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarPlanManejoContrato")
    @ApiOperation(value = "registrarPlanManejoContrato" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<PlanManejoContratoEntity>> registrarPlanManejoContrato(@RequestBody PlanManejoContratoEntity obj) {
        log.info("PlanManejo - registrarPlanManejoContrato", obj.toString());
        ResultClassEntity<PlanManejoContratoEntity> response = new ResultClassEntity<>();
        try {
            response = planManejoService.registrarPlanManejoContrato(obj);
            log.info("PlanManejo - registrarPlanManejoContrato", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PlanManejo - registrarPlanManejoContrato", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerPlanManejoContrato")
    @ApiOperation(value = "obtenerPlanManejoContrato", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerPlanManejoContrato(
            @RequestBody PlanManejoContratoEntity planManejoContratoEntity) {
        log.info("PlanManejo - obtenerPlanManejoContrato: {}", planManejoContratoEntity);

        ResultClassEntity response = new ResultClassEntity();
        try {
            response = planManejoService.obtenerPlanManejoContrato(planManejoContratoEntity);
            log.info("PlanManejo - obtenerPlanManejoContrato","Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PlanManejo - obtenerPlanManejoContrato: Ocurrió un error: {}", e.getMessage());

            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/consolidadoPGMF")
    @ApiOperation(value = "Obtener Archivo consolidado PGMF", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPGMF(@RequestParam(required = true) Integer idPGMF)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPGMF: idPlanManejoForestal:{},", idPGMF);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPGMF(idPGMF);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPGMF.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan General de Manejo Forestal - PGMF.");
            log.error("Service - Se descargó el formato correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error . No se pudo generar el consolidado PGMF: {}", e.getMessage());
            throw new Exception(e);

        }
    }
    /**
     * @autor: Danny Nazario [13-01-2022]
     * @modificado: Danny Nazario [21-03-2022]
     * @descripción: {Listar PlanManejo Contrato por filtro}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroPlanManejoContrato")
    @ApiOperation(value = "listarPorFiltroPlanManejoContrato" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<OtorgamientoEntity>> listarPorFiltroPlanManejoContrato(
            @RequestParam String nroDocumento,
            @RequestParam(required = false) String codTipoPlan) {
        log.info("PlanManejo - listarPorFiltroPlanManejoContrato", nroDocumento, codTipoPlan);
        ResultClassEntity<OtorgamientoEntity> response = new ResultClassEntity<>();
        try {
            response = planManejoService.listarPorFiltroPlanManejoContrato(nroDocumento, codTipoPlan);
            log.info("PlanManejo - listarPorFiltroPlanManejoContrato", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PlanManejo - listarPorFiltroPlanManejoContrato", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarPorPlanManejoTipoBosque")
    @ApiOperation(value = "listarPorPlanManejoTipoBosque" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarPorPlanManejoTipoBosque(
            @RequestParam("idPlanDeManejo") Integer idPlanDeManejo,
            @RequestParam("tipoPlan") String  tipoPlan
    ) {

        ResultClassEntity response = new ResultClassEntity();
        try {
            response = planManejoService.listarPorPlanManejoTipoBosque(idPlanDeManejo,tipoPlan);
            log.info("PlanManejo - listarPorPlanManejoTipoBosque", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PlanManejo - listarPorPlanManejoTipoBosque", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/ListarTabsRegistrado")
    @ApiOperation(value = "ListarTabsRegistrado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public pe.gob.serfor.mcsniffs.entity.ResponseEntity ListarTabsRegistrado(@RequestBody RegistroTabEntity param){
        try{
            return  new pe.gob.serfor.mcsniffs.entity.ResponseEntity(true, "ok", planManejoService.listarTabsRegistrados(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new pe.gob.serfor.mcsniffs.entity.ResponseEntity(false,e.getMessage(),null);
        }
    }


    @GetMapping(path = "/descargarFormatoPlantacionesForestales")
    @ApiOperation(value = "descargarFormatoPlantacionesForestales", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarFormatoPlantacionesForestales() throws Exception {
                ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("formato Plantaciones Forestales");
        try {
            ByteArrayResource bytes = planManejoService.descargarFormatoPlantacionesForestales();
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("FormatoPlantacionesForestales.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se genero el archivo.");
            log.error("Service - Se descargo correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    @GetMapping(path = "/descargarPlantillaFormatoFiscalizacion2")
    @ApiOperation(value = "descargarPlantillaFormatoFiscalizacion2", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaFormatoFiscalizacion2() throws Exception {
                ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("Plantilla formato Fiscalización");
        try {
            ByteArrayResource bytes = planManejoService.descargarPlantillaFormatoFiscalizacion2();
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("PlantillaFormatoFiscalizacion2.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se genero el archivo.");
            log.error("Service - Se descargo correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Jason Retamozo [21-02-2022]
     * @creado:
     * @descripción: { Consolidado PMFIC resumido para pruebas PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/consolidadoPMFIC_resumido")
    @ApiOperation(value = "Obtener Archivo consolidadoPPMFIC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPMFIC_resumido(@RequestParam(required = true) Integer idPlanManejo) throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPMFIC_Resumido(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPMFC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Manejo Forestal Intermedio - PMFIC.");
            log.error("Service - Se descargo el PMFIC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Rafael Azaña [23-02-2022]
     * @creado:
     * @descripción: { Consolidado PMFIC resumido para pruebas PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/consolidadoPOPAC_resumido")
    @ApiOperation(value = "Obtener Archivo consolidadoPOPAC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOPAC_resumido(@RequestParam(required = true) Integer idPlanManejo) throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPOPAC_Resumido(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOPAC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Operativo PMFI en comunidades campesinas y nativas ");
            log.error("Service - Se descargo el POPAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }
    /**
     * @autor: Harry Coa [19-02-2022]
     * @modificado:
     * @descripción: { Consolidado POAC resumido Piloto para pruebas PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/consolidadoPOAC_resumido")
    @ApiOperation(value = "Obtener Archivo consolidadoPOAC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOAC_resumido(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOAC: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = planManejoService.consolidadoPOAC_Resumido(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOAC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Operativo Anual - POAC.");
            log.error("Service - Se descargo el POAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña [19-02-2022]
     * @modificado:
     * @descripción: { Consolidado PGMFA  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPGMFA")
    @ApiOperation(value = "Obtener Archivo consolidadoPGMFA resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPGMFA_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPGMFA_PDF: idPlanManejo:{},", idPlanManejo);
        try {
            ByteArrayResource bytes = archivoPDFPGMFAService.consolidadoPGMFA_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPGMFA.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan General - PGMFA.");
            log.error("Service - Se descargo el PGMFA correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }



    /**
     * @autor: Rafael Azaña  / 18-03-2022
     * @modificado:
     * @descripción: { Consolidado DEMA  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoDEMA")
    @ApiOperation(value = "Obtener Archivo consolidadoDEMA", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoDEMA_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoDEMA_PDF: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFDEMAService.consolidadoDEMA_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoDEMA.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado de la Declaracion de Manejo - DEMA correctamente.");
            log.error("Service - Se descargo el DEMA correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: RAFAEL AZAÑA
     * @modificado:
     * @descripción: { Consolidado PMFIC  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPMFIC")
    @ApiOperation(value = "Obtener Archivo consolidadoPMFIC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPMFIC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPMFIC_PDF: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPMFICService.consolidadoPMFIC_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPMFIC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan de Manejo Forestal Intermedio - PMFIC correctamente.");
            log.error("Service - Se descargo el PMFIC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: RAFAEL AZAÑA
     * @modificado:
     * @descripción: { Consolidado POPAC  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPOPAC")
    @ApiOperation(value = "Obtener Archivo consolidadoPMFIC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOPAC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPMFIC_PDF: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPOPACService.consolidadoPOPAC_PDF(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOPACPOPAC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del Plan Operativo - POPAC correctamente.");
            log.error("Service - Se descargo el POPAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña [25-02-2022]
     * @modificado:
     * @descripción: { Consolidado POAC  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPOAC")
    @ApiOperation(value = "Obtener Archivo consolidadoPOAC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOAC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOAC_PDF: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPOACService.consolidadoPOAC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOAC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del POAC.");
            log.error("Service - Se descargo el POAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña [26-02-2022]
     * @modificado:
     * @descripción: { Consolidado POCC  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPOCC")
    @ApiOperation(value = "Obtener Archivo consolidadoPOCC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOCC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOCC_PDF: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = archivoPDFPOCCService.consolidadoPOCC(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOCC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del POCC.");
            log.error("Service - Se descargo el POCC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    @PostMapping(path = "/registrarColindanciaPredio")
    @ApiOperation(value = "registrarColindanciaPredio" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarColindanciaPredio(@RequestBody List<ColindanciaPredioDto> param){
        log.info("Evaluacion - registrarColindanciaPredio",param.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = colindanciaPredioService.registrarColindanciaPredio(param);
            log.info("Evaluacion - registrarColindanciaPredio","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - registrarColindanciaPredio","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarColindanciaPredio")
    @ApiOperation(value = "listarColindanciaPredio", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<ColindanciaPredioDto>>> listarColindanciaPredio(
            @RequestBody ColindanciaPredioDto param) {
        log.info("listarColindanciaPredio: {}", param);
        ResultClassEntity<List<ColindanciaPredioDto>> response = new ResultClassEntity<>();
        try {
            response = colindanciaPredioService.listarColindanciaPredio(param);
            log.info("Service - listarColindanciaPredio: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - listarColindanciaPredio: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/descargarDeclaracionManejoProductosForestales_PDF")
    @ApiOperation(value = "descargarDeclaracionManejoProductosForestales_PDF", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> DeclaracionManejoProductosForestales_PDF(@RequestBody DescargarArchivoDeclaracionManejoProductosForestalesDto obj){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {

            ByteArrayResource bytes = archivoDeclaracionManejoProductosForestalesService.ArchivoDeclaracionManejoProductosForestales(obj);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("DeclaracionManejoProductosForestales.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó la Declaracion de Manejo Productos Forestales.");
            log.error("Service - Se descargo la Declaracion Manejo Productos Forestales correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("DeclaracionManejoProductosForestales - Descargar Declaracion", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    @PostMapping(path = "/descargarConsolidadoPGMF_PDF")
    @ApiOperation(value = "descargarConsolidadoPGMF_PDF", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarConsolidadoPGMF_PDF(@RequestBody DescargarPGMFDto obj, HttpServletRequest request1){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            String token= request1.getHeader("Authorization");
            ByteArrayResource bytes = archivoPGMFService.archivoConsolidadoPGMF(obj, token);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("ConsolidadoPGMF.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado PGMF");
            log.error("Service - Se generó el consolidado PGMF correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("consolidado PGMF - Descargar consolidado", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    /**
     * @autor: Rafael Azaña 17-03-2022
     * @modificado:
     * @descripción: {Obtener Archivo plantilla de evaluación 3.2.3}
     * @param: Integer idPlanManejo
     */

    @GetMapping(path = "/plantillaInformeGeneral/{idPlanManejo}")
    @ApiOperation(value = "plantillaInformeGeneral", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> plantillaInformeGeneral(@PathVariable(required = true) Integer idPlanManejo){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = planManejoService.plantillaInformeGeneral(idPlanManejo);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Contrato - Descargar plantillaInformeGeneral", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarDivisionAdministrativa")
    @ApiOperation(value = "listarDivisionAdministrativa" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public pe.gob.serfor.mcsniffs.entity.ResponseEntity listarDivisionAdministrativa(@RequestBody DivisionAdministrativaEntity param){
        try{
            return  new pe.gob.serfor.mcsniffs.entity.ResponseEntity(true, "ok", planManejoService.listarDivisionAdministrativa(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new pe.gob.serfor.mcsniffs.entity.ResponseEntity(false,e.getMessage(),null);
        }
    }


}

