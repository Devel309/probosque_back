package pe.gob.serfor.mcsniffs.rest;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividesEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad.CronogramaActividadDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CronogramaActividesService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(Urls.CronogramaActividades.BASE)
public class CronogramaActividadesController {
    

    @Autowired
    private CronogramaActividesService service;
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CronogramaActividadesController.class);
    /**
     * @autor: Edwin Murga [14-07-2021]
     * @modificado:
     * @descripción: {registra cronograma}
     * @param:List<CronogramaActividesEntity>
     */
    @PostMapping(path = "/RegistrarCronogramaActividades")
    @ApiOperation(value = "Registrar Cronograma Actividades", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> RegistrarCronogramaActividades(@RequestBody List<CronogramaActividesEntity> obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.RegistrarCronogramaActividades(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - RegistrarCronogramaActividades", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

  /**
     * @autor: Edwin Murga [14-07-2021]
     * @modificado:
     * @descripción: {lista cronograma }
     * @param:CronogramaActividesEntity
     */
    @PostMapping(path = "/ListarCronogramaActividades")
    @ApiOperation(value = "Listar Cronograma Actividades", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarCronogramaActividades(@RequestBody CronogramaActividesEntity obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.ListarCronogramaActividades(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - ListarCronogramaActividades", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

      /**
     * @autor: Edwin Murga [14-07-2021]
     * @modificado:
     * @descripción: {cambia el etado de item}
     * @param:CronogramaActividesEntity
     */
    @PostMapping(path = "/EliminarCronogramaActividades")
    @ApiOperation(value = "Eliminar Cronograma Actividades", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarCronogramaActividades(@RequestBody CronogramaActividesEntity obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.EliminarCronogramaActividades(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - EliminarCronogramaActividades", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }


    /***Métodos para el manejo de Cronograma Actividades usando tabla detalle*******/

    /**
     * @autor: Julio Meza [22-07-2021]
     * @modificado:
     * @descripción: {registra cronograma}
     * @param:CronogramaActividadEntity
     */
    @PostMapping(path = "/RegistrarCronogramaActividad")
    @ApiOperation(value = "Registrar Cronograma Actividad", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<CronogramaActividadEntity>> RegistrarCronogramaActividad(@RequestBody CronogramaActividadEntity obj, HttpServletRequest request1){
        ResultEntity<CronogramaActividadEntity> res = new ResultEntity<CronogramaActividadEntity>();
        try {
            res = service.RegistrarCronogramaActividad(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - RegistrarCronogramaActividad", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            res.setMessage("Ocurrió un error");
            return new ResponseEntity<>(res,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB
     * @modificado:
     * @descripción: {registra cronograma detalle}
     * @param:CronogramaActividadEntity
     */
    @PostMapping(path = "/registrarMarcaCronogramaActividadDetalle")
    @ApiOperation(value = "Registrar Marca Cronograma Actividad Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<CronogramaActividadEntity>> RegistrarMarcaCronogramaActividadDetalle(@RequestBody CronogramaActividadEntity obj, HttpServletRequest request1){
        ResultEntity<CronogramaActividadEntity> res = new ResultEntity<CronogramaActividadEntity>();
        try {
            res = service.RegistrarMarcaCronogramaActividadDetalle(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - RegistrarMarcaCronogramaActividadDetalle", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            res.setMessage("Ocurrió un error");
            return new ResponseEntity<>(res,HttpStatus.FORBIDDEN);
        }
    }


    /**
     * @autor: Julio Meza [22-07-2021]
     * @modificado:
     * @descripción: {cambia el estado de item}
     * @param:CronogramaActividadEntity
     */
    @PostMapping(path = "/EliminarCronogramaActividad")
    @ApiOperation(value = "Eliminar Cronograma Actividad", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarCronogramaActividad(@RequestBody CronogramaActividadEntity obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.EliminarCronogramaActividad(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - EliminarCronogramaActividad", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [07-09-2021]
     * @modificado:
     * @descripción: {lista el cronograma con su detalle}
     * @param:
     */
    @PostMapping(path = "/ListarCronogramaActividad")
    @ApiOperation(value = "Listar Cronograma Actividad", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarCronogramaActividad(@RequestBody CronogramaActividadEntity obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.ListarCronogramaActividad(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - ListarCronogramaActividad", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Rafael Azaña [20-01-2022]
     * @modificado:
     * @descripción: {lista el cronograma con su detalle Titular}
     * @param:
     */
    @PostMapping(path = "/ListarCronogramaActividadTitular")
    @ApiOperation(value = "Listar Cronograma Actividad Titular", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarCronogramaActividadTitular(@RequestBody CronogramaActividadEntity obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.ListarCronogramaActividadTitular(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - ListarCronogramaActividadTitular", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.FORBIDDEN);
        }
    }


    /**
     * @autor: Julio Meza [22-07-2021]
     * @modificado:
     * @descripción: {lista cronograma }
     * @param:CronogramaActividesEntity
     */
    @PostMapping(path = "/ListarCronogramaActividadDetalle")
    @ApiOperation(value = "Listar Cronograma Actividad Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarCronogramaActividadDetalle(@RequestBody CronogramaActividesEntity obj, HttpServletRequest request1){
        ResultEntity res = new ResultEntity();
        try {
            res = service.ListarCronogramaActividadDetalle(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - ListarCronogramaActividadDetalle", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: JaquelineDB [07-09-2021]
     * @modificado:
     * @descripción: {Elimina el detalle del cronograma}
     * @param:
     */
    @PostMapping(path = "/EliminarCronogramaActividadDetalle")
    @ApiOperation(value = "Eliminar Cronograma Actividad Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> EliminarCronogramaActividadDetalle(@RequestBody CronogramaActividadEntity obj, HttpServletRequest request1){
        ResultClassEntity res = new ResultClassEntity<>();
        try {
            res = service.EliminarCronogramaActividadDetalle(obj);
            return new ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CronogramaActividadesController - EliminarCronogramaActividadDetalle", "Ocurrió un error al guardar en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Danny Nazario [29-07-2021]
     * @modificado:
     * @descripción: {Registra Cronograma Actividades Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarCronogramaActividadExcel")
    @ApiOperation(value = "registrarCronogramaActividadExcel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCronogramaActividadExcel(@RequestParam("file") MultipartFile file,
                                                                                     @RequestParam("nombreHoja") String nombreHoja,
                                                                                     @RequestParam("numeroFila") Integer numeroFila,
                                                                                     @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                     @RequestParam("codigoProceso") String codigoProceso,
                                                                                     @RequestParam("codigoActividad") String codigoActividad,
                                                                                     @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                     @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        log.info("CronogramaActividades - registrarCronogramaActividadExcel");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarCronogramaActividadExcel(file, nombreHoja, numeroFila, numeroColumna, codigoProceso, codigoActividad, idPlanManejo, idUsuarioRegistro);
            if (result.getSuccess()) {
                log.info("CronogramaActividades - registrarCronogramaActividadExcel", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CronogramaActividades - registrarCronogramaActividadExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: IVAN MINAYA [29-07-2021]
     * @modificado:
     * @descripción: {registrar Configuracion CronogramaActividad}
     * @param: CronogramaActividadDto
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarConfiguracionCronogramaActividad")
    @ApiOperation(value = "registrarConfiguracionCronogramaActividad", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCronogramaActividadExcel(@RequestBody CronogramaActividadDto request) {
        log.info("CronogramaActividades - registrarCronogramaActividadExcel");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.RegistrarConfiguracionCronogramaActividad(request);
            if (result.getSuccess()) {
                log.info("CronogramaActividades - registrarConfiguracionCronogramaActividad", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CronogramaActividades - registrarConfiguracionCronogramaActividad", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     /**
     * @autor: IVAN MINAYA [29-07-2021]
     * @modificado:
     * @descripción: {registrar Configuracion CronogramaActividad}
     * @param: CronogramaActividadDto
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroCronogramaActividad")
    @ApiOperation(value = "listarPorFiltroCronogramaActividad", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarPorFiltroCronogramaActividad(@RequestBody CronogramaActividadDto request) {
        log.info("CronogramaActividades - ListarPorFiltroCronogramaActividad");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.ListarPorFiltroCronogramaActividad(request);
            if (result.getSuccess()) {
                log.info("CronogramaActividades - ListarPorFiltroCronogramaActividad", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("CronogramaActividades - ListarPorFiltroCronogramaActividad", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}



