package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo.PlanManejoObtenerContrato;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.PlanManejoEvaluacionIterDto;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * @autor: Rafael Azaña [01-03-2022]
 * @modificado:
 * @descripción: { Controlador para Consolidado DEV}
 */
@RestController
@RequestMapping(Urls.consolidadoDEV.BASE)
public class ConsolidadoDEVController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ConsolidadoDEVController.class);

    @Autowired
    private PlanManejoService planManejoService;

    @Autowired
    private ArchivoPDFPGMFAService archivoPDFPGMFAService;

    @Autowired
    private ArchivoPDFPMFICService archivoPDFPMFICService;

    @Autowired
    private ArchivoPDFPOPACService archivoPDFPOPACService;

    @Autowired
    private ArchivoPDFPOACService archivoPDFPOACService;

    @Autowired
    private ConsolidadoDEVService consolidadoDEVService;

    @Autowired
    private ArchivoPDFPOCCService archivoPDFPOCCService;


    @Autowired
    private ArchivoService serArchivo;



    /**
     * @autor: Rafael Azaña [25-02-2022]
     * @modificado:
     * @descripción: { Consolidado POAC  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPOAC")
    @ApiOperation(value = "Obtener Archivo consolidadoPOAC resumido", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPOAC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPOAC_PDF: idPlanManejo:{},", idPlanManejo);
        try {

           // ByteArrayResource bytes = consolidadoDEVService.consolidadoPOACDEV(idPlanManejo);
            ByteArrayResource bytes =null;
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPOAC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del POAC.");
            log.error("Service - Se descargo el POAC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }



    /**
     * @autor: Rafael Azaña [04-03-2022]
     * @modificado:
     * @descripción: { Consolidado PMFIC  PDF}
     * @param:PlanManejoEstadoEntity
     */
    @GetMapping(path = "/pdf/consolidadoPMFIC")
    @ApiOperation(value = "Obtener Archivo consolidadoPMFIC ", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPMFIC_PDF(@RequestParam(required = true) Integer idPlanManejo)
            throws Exception {
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("consolidadoPMFIC_PDF: idPlanManejo:{},", idPlanManejo);
        try {

            ByteArrayResource bytes = consolidadoDEVService.consolidadoPMFICDEV(idPlanManejo);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("consolidadoPMFIC.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado del PMFIC.");
            log.error("Service - Se descargo el PMFIC correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }



}

