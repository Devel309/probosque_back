package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoInfraccionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EvaluacionCampoInfraccionService;

import java.util.List;

@RestController
@RequestMapping(Urls.evaluacionCampoInfraccion.BASE)
public class EvaluacionCampoInfraccionController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EvaluacionCampoInfraccionController.class);

    @Autowired
    private EvaluacionCampoInfraccionService evaluacionCampoInfraccionService;

    @PostMapping(path = "/obtenerEvaluacionCampoInfraccion")
    @ApiOperation(value = "obtenerEvaluacionCampoInfraccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerEvaluacionCampoInfraccion(@RequestBody EvaluacionCampoInfraccionDto param){
        log.info("EvaluacionCampoInfraccion - ObtenerEvaluacionCampoInfraccion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = evaluacionCampoInfraccionService.ObtenerEvaluacionCampoInfraccion(param);
            log.info("EvaluacionCampoInfraccion - ObtenerEvaluacionCampoInfraccion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("EvaluacionCampoInfraccion -ObtenerEvaluacionCampoInfraccion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarEvaluacionCampoInfraccion")
    @ApiOperation(value = "registrarEvaluacionCampoInfraccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacionCampoInfraccion(@RequestBody List<EvaluacionCampoInfraccionDto> list){
        log.info("EvaluacionCampoInfraccion - RegistrarEvaluacionCampoInfraccion",list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = evaluacionCampoInfraccionService.RegistrarEvaluacionCampoInfraccion(list);
            log.info("EvaluacionCampoInfraccion - RegistrarEvaluacionCampoInfraccion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("EvaluacionCampoInfraccion -RegistrarEvaluacionCampoInfraccion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
