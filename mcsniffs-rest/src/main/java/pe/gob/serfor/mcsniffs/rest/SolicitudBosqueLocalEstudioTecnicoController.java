package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEstudioTecnicoEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalEstudioTecnicoService;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalService;

import java.util.Arrays;


@RestController
@RequestMapping(Urls.solicitudBosqueLocalEstudioTecnico.BASE)
public class SolicitudBosqueLocalEstudioTecnicoController extends AbstractRestController{
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalEstudioTecnicoController.class);

    @Autowired
    private SolicitudBosqueLocalEstudioTecnicoService solicitudBosqueLocalEstudioTecnicoService;

    @PostMapping(path = "/listarSolicitudBosqueLocalEstudioTecnico")
    @ApiOperation(value = "listar Actividad de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudBosqueLocalEstudioTecnico(@RequestBody SolicitudBosqueLocalEstudioTecnicoEntity request) {
        log.info("SolicitudBosqueLocalEstudioTecnico - listarSolicitudConcesionActividad", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalEstudioTecnicoService.listarSolicitudBosqueLocalEstudioTecnico(request);
            log.info("SolicitudBosqueLocalEstudioTecnico- listarSolicitudBosqueLocalEstudioTecnico", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalEstudioTecnico- listarSolicitudBosqueLocalEstudioTecnico", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolicitudBosqueLocalEstudioTecnico")
    @ApiOperation(value = "Actualizar Actividad de la Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudBosqueLocalEstudioTecnico(@RequestBody SolicitudBosqueLocalEstudioTecnicoEntity request){
        log.info("SolicitudBosqueLocalEstudioTecnico - actualizarSolicitudBosqueLocalEstudioTecnico",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalEstudioTecnicoService.actualizarSolicitudBosqueLocalEstudioTecnico(request);
            log.info("SolicitudBosqueLocalEstudioTecnico - actualizarSolicitudBosqueLocalEstudioTecnico","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocalEstudioTecnico - actualizarSolicitudBosqueLocalEstudioTecnico","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


}
