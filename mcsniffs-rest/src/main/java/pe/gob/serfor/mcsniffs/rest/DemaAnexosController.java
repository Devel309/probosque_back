package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.DemaAnexosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(Urls.demaAnexos.BASE)
public class DemaAnexosController {
    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Repository en donde se procesos los anexos referentes a dema}
     * @param:
     */
    @Autowired
    private DemaAnexosService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se relacion los archivos cargados con dema}
     * @param:PlanManejoArchivoEntity
     */
    @PostMapping(path = "/registrarArchivo")
    @ApiOperation(value = "registrar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarArchivo(@RequestBody PGMFArchivoEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarArchivo(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("DemaAnexosController - registrarArchivo", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se listan los archivos cargados con dema}
     * @param:PlanManejoArchivoEntity
     */
    @PostMapping(path = "/listarArchivoDema")
    @ApiOperation(value = "listar Archivo Dema", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PlanManejoArchivoEntity>> listarArchivoDema(@RequestBody PlanManejoArchivoEntity data){
        ResultEntity<PlanManejoArchivoEntity> result = new ResultEntity<PlanManejoArchivoEntity>();
        try {
            result = service.listarArchivoDema(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("DemaAnexosController - listarArchivoDema", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


     /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se genera el anexo2 de dema}
     * @param:PlanManejoArchivoEntity
     */
    @PostMapping(path = "/generarAnexo2Dema")
    @ApiOperation(value = "generar Anexo2 Dema", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo2Dema(@RequestBody PlanManejoArchivoEntity data){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo2Dema(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("DemaAnexosController - generarAnexo2Dema", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [13-09-2021]
     * @modificado:
     * @descripción: {Se genera el anexo3 de dema}
     * @param:PlanManejoArchivoEntity
     */
    @PostMapping(path = "/generarAnexo3Dema")
    @ApiOperation(value = "generar Anexo3 Dema", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo3Dema(@RequestBody PlanManejoArchivoEntity data){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo3Dema(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("DemaAnexosController - generarAnexo3Dema", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    
}
