package pe.gob.serfor.mcsniffs.rest;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TalaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.TalaEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SincronizacionService;
import pe.gob.serfor.mcsniffs.service.TalaService;

@RestController
@RequestMapping(Urls.sincronizacion.BASE)
public class SincronizacionController {

    @Autowired
    private SincronizacionService serviceSincronizacion;

    private static final org.apache.logging.log4j.Logger log = LogManager
    .getLogger(SincronizacionController.class);

    @PostMapping(path = "/sincronizar")
    @ApiOperation(value = "sincronizar", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity sincronizar(@RequestParam("file") MultipartFile file) {
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            
            result = serviceSincronizacion.sincronizar(file);
            log.info("SincronizacionController - sincronizar","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("SincronizacionController - sincronizar",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
