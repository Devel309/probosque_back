package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.DerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento.ListarDerechoAprovechamientoDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.DerechoAprovechamientoService;


@RestController
@RequestMapping(Urls.derecho.BASE)
public class DerechoAprovechamientoController extends AbstractRestController {
    
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(DerechoAprovechamientoController.class);

    @Autowired
    private DerechoAprovechamientoService derechoAprovechamientoService;

    @PostMapping(path = "/listarDerechoAprovechamiento")
    @ApiOperation(value = "listarDerechoAprovechamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public Pageable<List<DerechoAprovechamientoDto>> listarDerechoAprovechamiento(@RequestBody ListarDerechoAprovechamientoDto param)throws Exception{
        log.info("DerechoAprovechamiento - listarDerechoAprovechamiento",param.toString());


        try {
            
            return derechoAprovechamientoService.listarDerechoAprovechamiento(param);

        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

}