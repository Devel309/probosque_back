package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.TipoDocumentoEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ParametroService;

@RestController
@RequestMapping(Urls.parametro.BASE)
public class ParametroController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ParametroController.class);

    @Autowired
    private ParametroService parametroValorService;

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para consultar Parametros por código}
     * @param: Objeto ParametroValorEntity
     *
     * @return: ResponseEntity
     */
    @PostMapping(path = "/ListarPorCodigoParametroValor")
    @ApiOperation(value = "ListarPorCodigoParametroValor")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ListarPorCodigoParametroValor(@RequestBody ParametroValorEntity parametroValor){
        try{
            return  new ResponseEntity (true, "ok", parametroValorService.ListarPorCodigoParametroValor(parametroValor));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }



}
