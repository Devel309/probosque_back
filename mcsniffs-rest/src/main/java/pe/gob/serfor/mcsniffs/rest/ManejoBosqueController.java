package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ManejoBosqueDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;
import java.util.List;

@RestController
@RequestMapping(Urls.manejoBosque.BASE)
public class ManejoBosqueController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ManejoBosqueController.class);
    @Autowired
    private ManejoBosqueActividadService manejoBosqueActividadService;
    @Autowired
    private ManejoBosqueService manejoBosqueService;
    @Autowired
    private ManejoBosqueEspecieService manejoBosqueEspecieService;
    @Autowired
    private ManejoBosqueAprovechamientoService manejoBosqueAprovechamientoService;
    @Autowired
    private ManejoBosqueAprovechamientoCaminoService manejoBosqueAprovechamientoCaminoService;
    @Autowired
    private ManejoBosqueOperacionService manejoBosqueOperacionService;
    @Autowired
    private ManejoBosqueEspecieProtegerService manejoBosqueEspecieProtegerService;

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registra el ManejoBosqueActividad}
     * @param: Listt<ManejoBosqueActividadEntity>
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosqueActividad")
    @ApiOperation(value = "registrarManejoBosqueActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueActividad(@RequestBody List<ManejoBosqueActividadEntity> listMonitoreo){
        log.info("ManejoBosque - RegistrarManejoBosqueActividad",listMonitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{



            response = manejoBosqueActividadService.RegistrarManejoBosqueActividad(listMonitoreo);
            log.info("ManejoBosque - RegistrarManejoBosqueActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosqueActividad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar ManejoBosqueActividad}
     * @param: ManejoBosqueActividadEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/eliminarManejoBosqueActividad")
    @ApiOperation(value = "eliminarManejoBosqueActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarManejoBosqueActividad(@RequestBody ManejoBosqueActividadEntity monitoreo){
        log.info("ManejoBosque - EliminarManejoBosqueActividad",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueActividadService.EliminarManejoBosqueActividad(monitoreo);
            log.info("ManejoBosque - EliminarManejoBosqueActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        }catch (Exception e){
            log.error("ManejoBosque -EliminarManejoBosqueActividad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Lista ManejoBosqueActividad}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @PostMapping(path = "/listarManejoBosqueActividad")
    @ApiOperation(value = "listarManejoBosqueActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarManejoBosqueActividad(@RequestBody PlanManejoEntity planManejo){
        log.info("ManejoBosque - ListaroManejoBosqueActividad",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueActividadService.ListarManejoBosqueActividad(planManejo);
            log.info("ManejoBosque - ListaroManejoBosqueActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -ListaroManejoBosqueActividad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registra el ManejoBosque}
     * @param: ManejoBosqueEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosque")
    @ApiOperation(value = "registrarManejoBosque" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosque(@RequestBody ManejoBosqEntity manejoBosque){
        log.info("ManejoBosque - RegistrarManejoBosque",manejoBosque.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueService.RegistrarManejoBosque(manejoBosque);
            log.info("ManejoBosque - RegistrarManejoBosque","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosque","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registra el ManejoBosque}
     * @param: ManejoBosqueEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/obtenerManejoBosque")
    @ApiOperation(value = "obtenerManejoBosque" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerManejoBosque(@RequestBody ManejoBosqEntity manejoBosque){
        log.info("ManejoBosque - ObtenerManejoBosque",manejoBosque.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueService.ObtenerManejoBosque(manejoBosque);
            log.info("ManejoBosque - ObtenerManejoBosque","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -ObtenerManejoBosque","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registra el ManejoBosqueEspecie}
     * @param: List<ManejoBosqueEspecieEntity>
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosqueEspecie")
    @ApiOperation(value = "registrarManejoBosqueEspecie" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueEspecie(@RequestBody List<ManejoBosqueEspecieEntity> list){
        log.info("ManejoBosque - RegistrarManejoBosqueEspecie",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueEspecieService.RegistrarManejoBosqueEspecie(list);
            log.info("ManejoBosque - RegistrarManejoBosqueEspecie","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosqueEspecie","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar el ManejoBosqueEspecie}
     * @param: ManejoBosqueEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/listarManejoBosqueEspecie")
    @ApiOperation(value = "listarManejoBosqueEspecie" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarManejoBosqueEspecie(@RequestBody ManejoBosqEntity manejoBosque){
        log.info("ManejoBosque - ListarManejoBosqueEspecie",manejoBosque.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueEspecieService.ListarManejoBosqueEspecie(manejoBosque);
            log.info("ManejoBosque - ListarManejoBosqueEspecie","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -ListarManejoBosqueEspecie","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar el ManejoBosque}
     * @param: ManejoBosqueEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/eliminarManejoBosqueEspecie")
    @ApiOperation(value = "eliminarManejoBosqueEspecie" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarManejoBosqueEspecie(@RequestBody ManejoBosqueEspecieEntity manejoBosqueEspecie){
        log.info("ManejoBosque - EliminarManejoBosqueEspecie",manejoBosqueEspecie.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueEspecieService.EliminarManejoBosqueEspecie(manejoBosqueEspecie);
            log.info("ManejoBosque - EliminarManejoBosqueEspecie","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -EliminarManejoBosqueEspecie","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registrar el ManejoBosqueAprovechamiento}
     * @param: ManejoBosqueAprovechamientoEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosqueAprovechamiento")
    @ApiOperation(value = "registrarManejoBosqueAprovechamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueAprovechamiento(@RequestBody ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento){
        log.info("ManejoBosque - RegistrarManejoBosqueAprovechamiento",manejoBosqueAprovechamiento.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueAprovechamientoService.RegistrarManejoBosqueAprovechamiento(manejoBosqueAprovechamiento);
            log.info("ManejoBosque - RegistrarManejoBosqueAprovechamiento","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosqueAprovechamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Obtener el ManejoBosqueAprovechamiento}
     * @param: ManejoBosqueAprovechamientoEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/obtenerManejoBosqueAprovechamiento")
    @ApiOperation(value = "obtenerManejoBosqueAprovechamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerManejoBosqueAprovechamiento(@RequestBody ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento){
        log.info("ManejoBosque - ObtenerManejoBosqueAprovechamiento",manejoBosqueAprovechamiento.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueAprovechamientoService.ObtenerManejoBosqueAprovechamiento(manejoBosqueAprovechamiento);
            log.info("ManejoBosque - ObtenerManejoBosqueAprovechamiento","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -ObtenerManejoBosqueAprovechamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registrar el ManejoBosqueAprovechamientoCamino}
     * @param: List<ManejoBosqueAprovechamientoCaminoEntity>
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosqueAprovechamientoCamino")
    @ApiOperation(value = "registrarManejoBosqueAprovechamientoCamino" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueAprovechamientoCamino(@RequestBody List<ManejoBosqueAprovechamientoCaminoEntity> list){
        log.info("ManejoBosque - RegistrarManejoBosqueAprovechamientoCamino",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueAprovechamientoCaminoService.RegistrarManejoBosqueAprovechamientoCamino(list);
            log.info("ManejoBosque - RegistrarManejoBosqueAprovechamientoCamino","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosqueAprovechamientoCamino","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar el ManejoBosqueAprovechamientoCamino}
     * @param: ManejoBosqueAprovechamientoEntity
     * @return: ResultClassEntity<ResponseVO>
     */

     @PostMapping(path = "/listarManejoBosqueAprovechamientoCamino")
     @ApiOperation(value = "listarManejoBosqueAprovechamientoCamino" , authorizations = {@Authorization(value="JWT")})
     @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
     public org.springframework.http.ResponseEntity listarManejoBosqueAprovechamientoCamino(@RequestBody ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento){
         log.info("ManejoBosque - ListarManejoBosqueEspecie",manejoBosqueAprovechamiento.toString());
         ResponseEntity result = null;
         ResultClassEntity response ;
         try{
             response = manejoBosqueAprovechamientoCaminoService.ListarManejoBosqueAprovechamientoCamino(manejoBosqueAprovechamiento);
             log.info("ManejoBosque - ListarManejoBosqueAprovechamientoCamino","Proceso realizado correctamente");
             return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
         }catch (Exception e){
             log.error("ManejoBosque -ListarManejoBosqueAprovechamientoCamino","Ocurrió un error :"+ e.getMessage());
             result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
             return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar el ManejoBosqueAprovechamientoCamino}
     * @param: ManejoBosqueEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/eliminarManejoBosqueAprovechamientoCamino")
    @ApiOperation(value = "eliminarManejoBosqueAprovechamientoCamino" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarManejoBosqueAprovechamientoCamino(@RequestBody ManejoBosqueAprovechamientoCaminoEntity manejoBosqueAprovechamientoCamino){
        log.info("ManejoBosque - EliminarManejoBosqueAprovechamientoCamino",manejoBosqueAprovechamientoCamino.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueAprovechamientoCaminoService.EliminarManejoBosqueAprovechamientoCamino(manejoBosqueAprovechamientoCamino);
            log.info("ManejoBosque - EliminarManejoBosqueAprovechamientoCamino","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -EliminarManejoBosqueAprovechamientoCamino","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registrar el ManejoBosqueOperacion}
     * @param: List<ManejoBosqueOperacionEntity>
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosqueOperacion")
    @ApiOperation(value = "registrarManejoBosqueOperacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueOperacion(@RequestBody List<ManejoBosqueOperacionEntity> list){
        log.info("ManejoBosque - RegistrarManejoBosqueOperacion",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueOperacionService.RegistrarManejoBosqueOperacion(list);
            log.info("ManejoBosque - RegistrarManejoBosqueOperacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosqueOperacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar el ManejoBosqueOperacion}
     * @param: PlanManejoEntity
     * @return: ResultClassEntity<ResponseVO>
     */

     @PostMapping(path = "/listarManejoBosqueOperacion")
     @ApiOperation(value = "listarManejoBosqueOperacion" , authorizations = {@Authorization(value="JWT")})
     @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
     public org.springframework.http.ResponseEntity listarManejoBosqueOperacion(@RequestBody PlanManejoEntity planManejo){
         log.info("ManejoBosque - ListarManejoBosqueOperacion",planManejo.toString());
         ResponseEntity result = null;
         ResultClassEntity response ;
         try{
             response = manejoBosqueOperacionService.ListarManejoBosqueOperacion(planManejo);
             log.info("ManejoBosque - ListarManejoBosqueOperacion","Proceso realizado correctamente");
             return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
         }catch (Exception e){
             log.error("ManejoBosque -ListarManejoBosqueOperacion","Ocurrió un error :"+ e.getMessage());
             result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
             return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar el ManejoBosqueOperacion}
     * @param: ManejoBosqueOperacionEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/eliminarManejoBosqueOperacion")
    @ApiOperation(value = "eliminarManejoBosqueOperacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarManejoBosqueOperacion(@RequestBody ManejoBosqueOperacionEntity manejoBosqueOperacion){
        log.info("ManejoBosque - EliminarManejoBosqueOperacion",manejoBosqueOperacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueOperacionService.EliminarManejoBosqueOperacion(manejoBosqueOperacion);
            log.info("ManejoBosque - EliminarManejoBosqueOperacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -EliminarManejoBosqueOperacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registrar el ManejoBosqueEspecieProteger}
     * @param: List<ManejoBosqueEspecieProtegerEntity>
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarManejoBosqueEspecieProteger")
    @ApiOperation(value = "registrarManejoBosqueEspecieProteger" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueEspecieProteger(@RequestBody List<ManejoBosqueEspecieProtegerEntity> list){
        log.info("ManejoBosque - RegistrarManejoBosqueEspecieProteger",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueEspecieProtegerService.RegistrarManejoBosqueEspecieProteger(list);
            log.info("ManejoBosque - RegistrarManejoBosqueEspecieProteger","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -RegistrarManejoBosqueEspecieProteger","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar el ManejoBosqueEspecieProteger}
     * @param: PlanManejoEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/listarManejoBosqueEspecieProteger")
    @ApiOperation(value = "listarManejoBosqueEspecieProteger" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarManejoBosqueEspecieProteger(@RequestBody PlanManejoEntity planManejo){
        log.info("ManejoBosque - ListarManejoBosqueEspecieProteger",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueEspecieProtegerService.ListarManejoBosqueEspecieProteger(planManejo);
            log.info("ManejoBosque - ListarManejoBosqueEspecieProteger","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -ListarManejoBosqueEspecieProteger","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar el ManejoBosqueEspecieProteger}
     * @param: ManejoBosqueEspecieProtegerEntity
     * @return: ResultClassEntity<ResponseVO>
     */

    @PostMapping(path = "/eliminarManejoBosqueEspecieProteger")
    @ApiOperation(value = "eliminarManejoBosqueEspecieProteger" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarManejoBosqueEspecieProteger(@RequestBody ManejoBosqueEspecieProtegerEntity manejoBosqueEspecieProteger){
        log.info("ManejoBosque - EliminarManejoBosqueEspecieProteger",manejoBosqueEspecieProteger.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueEspecieProtegerService.EliminarManejoBosqueEspecieProteger(manejoBosqueEspecieProteger);
            log.info("ManejoBosque - EliminarManejoBosqueEspecieProteger","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque -EliminarManejoBosqueEspecieProteger","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {Listado de cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueDto
     */

    @PostMapping(path = "/ListarManejoBosqueDetalle")
    @ApiOperation(value = "Listar ManejoBosque", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ManejoBosqueEntity>> ListarManejoBosqueDetalle(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codigoGeneral  ,
            @RequestParam(required = false) String subCodigoGeneral ,
            @RequestParam(required = false) String subCodigoGeneralDet) {
        try {
            return manejoBosqueService.ListarManejoBosque(idPlanManejo,codigoGeneral,subCodigoGeneral,subCodigoGeneralDet);
        } catch (Exception ex) {

            ResultClassEntity<List<ManejoBosqueEntity>> result = new ResultClassEntity<>();
            log.error("ManejoBosque - ListarManejoBosque", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    @PostMapping(path = "/ListarManejoBosqueDetalleTitular")
    @ApiOperation(value = "Listar ManejoBosque Titular", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ManejoBosqueEntity>> ListarManejoBosqueDetalleTitular(
            @RequestParam(required = true) String tipoDocumento,
            @RequestParam(required = true) String nroDocumento,
            @RequestParam(required = true) String codigoProcesoTitular,
            @RequestParam(required = true) String subcodigoProcesoTitular,
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = true) String codigoProceso,
            @RequestParam(required = true) String subCodigoProceso) {
        try {
            return manejoBosqueService.ListarManejoBosqueTitular(tipoDocumento,nroDocumento,codigoProcesoTitular,subcodigoProcesoTitular,idPlanManejo,codigoProceso,subCodigoProceso);
        } catch (Exception ex) {

            ResultClassEntity<List<ManejoBosqueEntity>> result = new ResultClassEntity<>();
            log.error("ManejoBosque - ListarManejoBosque", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {Registra y actualizar la cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueEntity
     */

    @PostMapping(path = "/registrarListaManejoBosque")
    @ApiOperation(value = "registrarListaManejoBosque", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarListaManejoBosque(
            @RequestBody List<ManejoBosqueEntity> list) {
        log.info("ManejoBosque - registrarListaManejoBosque", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = manejoBosqueService.RegistrarManejoBosque(list);
            log.info("ManejoBosque - registrarListaManejoBosque", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("ManejoBosque -registrarListaManejoBosque", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [19/10-2021]
     * @modificado:
     * @descripción: {REliminar la cabecera y detalle de Manejo del bosque}
     * @param:ManejoBosqueDto
     */

    @PostMapping(path = "/eliminarManejoBosque")
    @ApiOperation(value = "eliminarManejoBosque" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarManejoBosque(@RequestBody ManejoBosqueDto manejoBosqueDto){
        log.info("ManejoBosque - eliminarManejoBosque",manejoBosqueDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = manejoBosqueService.EliminarManejoBosque(manejoBosqueDto);
            log.info("ManejoBosque - eliminarManejoBosque","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ManejoBosque - eliminarManejoBosque","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [28-12-2021]
     * @modificado:
     * @descripción: {Registra Manejo Bosque Especies a Aprovechar Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarManejoBosqueExcel")
    @ApiOperation(value = "registrarManejoBosqueExcel" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarManejoBosqueExcel(@RequestParam("file") MultipartFile file,
                                                                              @RequestParam("nombreHoja") String nombreHoja,
                                                                              @RequestParam("numeroFila") Integer numeroFila,
                                                                              @RequestParam("numeroColumna") Integer numeroColumna,
                                                                              @RequestParam("codigoManejo") String codigoManejo,
                                                                              @RequestParam("subCodigoManejo") String subCodigoManejo,
                                                                              @RequestParam("codtipoManejoDet") String codtipoManejoDet,
                                                                              @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                              @RequestParam("idManejoBosque") Integer idManejoBosque,
                                                                              @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        log.info("ManejoBosque - registrarManejoBosqueExcel");
        ResultClassEntity result = new ResultClassEntity();
        try{
            result = manejoBosqueService.registrarManejoBosqueExcel(file, nombreHoja, numeroFila, numeroColumna, codigoManejo, subCodigoManejo, codtipoManejoDet, idPlanManejo, idManejoBosque, idUsuarioRegistro);
            if (result.getSuccess()) {
                log.info("ManejoBosque - registrarManejoBosqueExcel", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ManejoBosque - registrarManejoBosqueExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
