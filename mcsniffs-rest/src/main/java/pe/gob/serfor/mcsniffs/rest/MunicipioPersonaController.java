package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.MunicipioPersonaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.MunicipioPersonaService;


@RestController
@RequestMapping(Urls.municipioPersona.BASE)
public class MunicipioPersonaController extends AbstractRestController{
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(MunicipioPersonaController.class);

    @Autowired
    private MunicipioPersonaService municipioPersonaService;

    @PostMapping(path = "/listarMunicipioPersona")
    @ApiOperation(value = "listar Actividad MUnicipio Persona" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarMunicipioPersona(@RequestBody MunicipioPersonaEntity request) {
        log.info("Municipio - listarMunicipioPersona", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = municipioPersonaService.listarMunicipioPersona(request);
            log.info("Municipio - listarMunicipioPersona", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Municipio - listarMunicipioPersona", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


  
 
}
