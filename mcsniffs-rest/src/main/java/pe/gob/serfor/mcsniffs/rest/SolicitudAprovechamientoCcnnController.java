




package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.serfor.mcsniffs.service.SolicitudAprovechamientoCcnnService;


@RestController
@RequestMapping(Urls.obtenerSolicitudAprovechamientoCcnn.BASE)
public class SolicitudAprovechamientoCcnnController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudAprovechamientoCcnnController.class);



    @Autowired
    private SolicitudAprovechamientoCcnnService solicitudAprovechamientoCcnnService;

    @ApiOperation(value = "obtenerSolicitudAprovechamientoCcnn", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/obtenerSolicitudAprovechamientoCcnn")
    public org.springframework.http.ResponseEntity<SolicitudEntity> obtenerSolicitudAprovechamientoCcnn(@RequestBody SolicitudEntity solicitud){
        log.info("SolicitudAprovechamientoCcnn - ObtenerSolicitudAprovechamientoCcnn",solicitud.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = solicitudAprovechamientoCcnnService.ObtenerSolicitudAprovechamientoCcnn(solicitud);
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("SolicitudAprovechamientoCcnn - obtenerSolicitudAprovechamientoCcnn","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        }catch (Exception e){
            log.error("SolicitudAprovechamientoCcnn -obtenerSolicitudAprovechamientoCcnn","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
