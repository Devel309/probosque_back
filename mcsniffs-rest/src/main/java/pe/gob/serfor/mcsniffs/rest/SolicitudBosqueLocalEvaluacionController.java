package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.DescargarRespuestaEvaluacionComiteDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalEvaluacionService;

@RestController
@RequestMapping(Urls.solicitudBosqueLocalEvaluacion.BASE)
public class SolicitudBosqueLocalEvaluacionController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalEvaluacionController.class);

    @Autowired
    private SolicitudBosqueLocalEvaluacionService solicitudBosqueLocalEvaluacionService;

    @PostMapping(path = "/listarSolicitudBosqueLocalEvaluacion")
    @ApiOperation(value = "listarSolicitudBosqueLocalEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarSolicitudBosqueLocalEvaluacion(@RequestBody SolicitudBosqueLocalEvaluacionDto obj){
        log.info("SolicitudBosqueLocalEvaluacion - listarSolicitudBosqueLocalEvaluacion", obj);
        ResponseEntity result = null;
        ResultClassEntity response ;

        try{
            response = solicitudBosqueLocalEvaluacionService.listarSolicitudBosqueLocalEvaluacion(obj);
            log.info("SolicitudBosqueLocalEvaluacion - ListarSolicitudBosqueLocalEvaluacion","Proceso realizado correctamente");
            
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - ListarSolicitudBosqueLocalEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudBosqueLocalEvaluacion")
    @ApiOperation(value = "registrarSolicitudBosqueLocalEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarSolicitudBosqueLocalEvaluacion(@RequestBody List<SolicitudBosqueLocalEvaluacionDto> objList){
        log.info("SolicitudBosqueLocalEvaluacion - registrarSolicitudBosqueLocalEvaluacion", objList);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudBosqueLocalEvaluacionService.registrarSolicitudBosqueLocalEvaluacion(objList);
            log.info("SolicitudBosqueLocalEvaluacion - RegistrarSolicitudBosqueLocalEvaluacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - RegistrarSolicitudBosqueLocalEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarSolicitudBosqueLocalEvaluacion")
    @ApiOperation(value = "eliminarSolicitudBosqueLocalEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarSolicitudBosqueLocalEvaluacion(@RequestBody SolicitudBosqueLocalEvaluacionDto obj){
        log.info("SolicitudBosqueLocalEvaluacion - eliminarSolicitudBosqueLocalEvaluacion", obj);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudBosqueLocalEvaluacionService.eliminarSolicitudBosqueLocalEvaluacion(obj);
            log.info("SolicitudBosqueLocalEvaluacion - EliminarSolicitudBosqueLocalEvaluacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - EliminarSolicitudBosqueLocalEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolicitudBosqueLocalEvaluacionDetalle")
    @ApiOperation(value = "actualizarSolicitudBosqueLocalEvaluacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarSolicitudBosqueLocalEvaluacionDetalle(@RequestBody List<SolicitudBosqueLocalEvaluacionDetalleDto> listObj){
        log.info("SolicitudBosqueLocalEvaluacion - actualizarSolicitudBosqueLocalEvaluacionDetalle", listObj);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudBosqueLocalEvaluacionService.actualizarSolicitudBosqueLocalEvaluacionDetalle(listObj);
            log.info("SolicitudBosqueLocalEvaluacion - actualizarSolicitudBosqueLocalEvaluacionDetalle","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - actualizarSolicitudBosqueLocalEvaluacionDetalle","Ocurrió un error :"+ e.getMessage());

            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Wilfredo Elias [24-03-2022]
     * @modificado:
     * @descripción: {Registra Info Evaluacion Detalle de Solicitud Bosque Local}
     * @param:List<SolicitudBosqueLocalEvaluacionDetalleDto>
     */
    @PostMapping(path = "/registrarInfoEvaluacionDetalleSolicitudBosqueLocal")
    @ApiOperation(value = "Registrar Informacion Evaluacion Detalle de Solicitud Bosque Local", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarInfoEvaluacionDetalleSolicitudBosqueLocal( @RequestBody List<SolicitudBosqueLocalEvaluacionDetalleDto> request) {
        log.info("SolicitudBosqueLocalEvaluacion - registrarInfoEvaluacionDetalleSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalEvaluacionService.registrarSolicitudBosqueLocalEvaluacionDetalle(request);
            log.info("SolicitudBosqueLocalEvaluacion - registrarInfoEvaluacionDetalleSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalEvaluacion - registrarInfoEvaluacionDetalleSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarSolicitudBosqueLocalEvaluacionDetalle")
    @ApiOperation(value = "listarSolicitudBosqueLocalEvaluacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarSolicitudBosqueLocalEvaluacionDetalle(@RequestBody SolicitudBosqueLocalEvaluacionDetalleDto obj){
        log.info("SolicitudBosqueLocalEvaluacion - listarSolicitudBosqueLocalEvaluacionDetalle", obj);
        ResponseEntity result = null;
        ResultClassEntity response ;

        try{
            response = solicitudBosqueLocalEvaluacionService.listarSolicitudBosqueLocalEvaluacionDetalle(obj);
            log.info("SolicitudBosqueLocalEvaluacion - listarSolicitudBosqueLocalEvaluacionDetalle","Proceso realizado correctamente");
            
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - listarSolicitudBosqueLocalEvaluacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/descargarEvaluacionComiteTecnico")
    @ApiOperation(value = "descargarEvaluacionComiteTecnico", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> descargarModeloConcesionesMaderables(@RequestBody DescargarRespuestaEvaluacionComiteDto obj){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudBosqueLocalEvaluacionService.descargarRespuestaEvaluacionComite(obj); 
            if(result.getSuccess()){
                result.setSuccess(true);
            result.setMessage("Se completo el Proceso Correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            }else{
                result.setMessage("Ocurrio un error.");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception Ex){
            log.error("Contrato - DescargarContrato", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    
}
