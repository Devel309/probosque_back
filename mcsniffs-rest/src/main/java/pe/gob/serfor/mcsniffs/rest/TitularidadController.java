package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaTitularidadDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EvaluacionArchivoService;
import pe.gob.serfor.mcsniffs.service.EvaluacionDetalleService;
import pe.gob.serfor.mcsniffs.service.EvaluacionService;
import pe.gob.serfor.mcsniffs.service.TitularidadService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * @autor: RAFAEL AZAÑA [04-01-2022]
 * @modificado:
 * @descripción: { Controlador para la titularidad }
 */
@RestController
@RequestMapping(Urls.titularidad.BASE)
public class TitularidadController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TitularidadController.class);

    @Autowired
    private TitularidadService titularidadService;

    @GetMapping(path = "/consultarTitularidad")
    @ApiOperation(value = "consultarTitularidad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity consultarTitularidad(
            @RequestParam(required = false) String tipoParticipante,
            @RequestParam(required = false) String apellidoPaterno,
            @RequestParam(required = false) String apellidoMaterno,
            @RequestParam(required = false) String nombres,
            @RequestParam(required = false) String razonSocial,
            HttpServletRequest request1){
        log.info("Titularidad - consultarTitularidad");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            String token= request1.getHeader("Authorization");
            response = titularidadService.consultarTitularidad(tipoParticipante,apellidoPaterno,apellidoMaterno,nombres,razonSocial, token);
            log.info("Titularidad - consultarTitularidad","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Titularidad - consultarTitularidad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}