package pe.gob.serfor.mcsniffs.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.repository.EvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.PermisoForestalService;
import pe.gob.serfor.mcsniffs.service.TituloHabilitanteService;

@RestController
@RequestMapping(Urls.permisoForestal.BASE)
public class PermisoForestalesController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PermisoForestalesController.class);

    @Autowired
    private PermisoForestalService permisoForestalService;

    @Autowired
    private EmailService service;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private TituloHabilitanteService tituloHabilitanteService;


    @GetMapping(path = "/obtenerPersonaPorUsuario/{idUsuario}")
    @ApiOperation(value = "obtenerPersonaPorUsuario", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity> obtenerPersonaPorUsuario(@PathVariable Integer idUsuario) {
        log.info("PermisoForestal- obtenerPersonaPorUsuario", idUsuario);

        ResultClassEntity response = null;
        try {
            response = permisoForestalService.ObtenerPersonaPorUsuario(idUsuario);
            log.info("PermisoForestal - ObtenerPersonaPorUsuario", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - ObtenerPersonaPorUsuario", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //guardarSolictud
    @PostMapping(path = "/registrarSolicitudPorAccesoSolicitud")
    @ApiOperation(value = "registrarSolicitudPorAccesoSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity<ArchivoEntity>> registrarSolicitudPorAccesoSolicitud(@RequestBody SolicitudRequestEntity entity) {
        log.info("PermisoForestal- registrarSolicitudPorAccesoSolicitud", entity);
        ResultClassEntity<ArchivoEntity> response = null;
        try {
            response = permisoForestalService.registrarSolicitudPorAccesoSolicitud(entity);
            log.info("PermisoForestal - registrarSolicitudPorAccesoSolicitud", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - registrarSolicitudPorAccesoSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //editarSolictud
    @PostMapping(path = "/editarSolicitudPorAccesoSolicitud")
    @ApiOperation(value = "editarSolicitudPorAccesoSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity<ArchivoEntity>> editarSolicitudPorAccesoSolicitud(@RequestBody SolicitudRequestEntity entity) {
        log.info("PermisoForestal- editarSolicitudPorAccesoSolicitud", entity);
        ResultClassEntity<ArchivoEntity> response = null;
        try {
            response = permisoForestalService.editarSolicitudPorAccesoSolicitud(entity);
            log.info("PermisoForestal - editarSolicitudPorAccesoSolicitud", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - editarSolicitudPorAccesoSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //guardarSolictudAdjunto
    @PostMapping(path = "/registrarArchivoSolicitud")
    @ApiOperation(value = "registrarArchivoSolicitud ", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity> registrarArchivoSolicitud(@RequestParam("file") MultipartFile file,
                                                                       @RequestParam Integer idSolicitud,
                                                                       @RequestParam String tipoArchivo,
                                                                       @RequestParam Boolean flagActualiza) {
        log.info("PermisoForestal- registrarArchivoSolicitud", idSolicitud, tipoArchivo);
        ResultClassEntity response = null;
        try {
            response = permisoForestalService.registrarArchivoSolicitud(file, idSolicitud, tipoArchivo, flagActualiza);
            log.info("PermisoForestal - registrarArchivoSolicitud", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - registrarArchivoSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarArchivoSolicitudAdjunto")
    @ApiOperation(value = "registrarArchivoSolicitudAdjunto ", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity> registrarArchivoSolicitudAdjunto(@RequestParam("file") MultipartFile file,
                                                                              @RequestParam Integer idSolicitud,
                                                                              @RequestParam String tipoArchivo,
                                                                              @RequestParam String descripcionArchivo,
                                                                              @RequestParam Boolean flagActualiza) {
        log.info("PermisoForestal- registrarArchivoSolicitudAdjunto", idSolicitud, tipoArchivo, descripcionArchivo ,flagActualiza);
        ResultClassEntity response = null;
        try {
            response = permisoForestalService.registrarArchivoSolicitudAdjunto(file, idSolicitud, tipoArchivo, descripcionArchivo,flagActualiza);
            log.info("PermisoForestal - registrarArchivoSolicitudAdjunto", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - registrarArchivoSolicitudAdjunto", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //obtenerSolicitud
    @GetMapping(path = "/obtenerSolicitudPorAccesoSolicitud/{idSolicitud}/{idSolicitudAcceso}")
    @ApiOperation(value = "obtenerSolicitudPorAccesoSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity> obtenerSolicitudPorAccesoSolicitud(@PathVariable Integer idSolicitud, @PathVariable Integer idSolicitudAcceso) {
        log.info("PermisoForestal- obtenerSolicitudPorAccesoSolicitud", idSolicitud, idSolicitudAcceso);
        ResultClassEntity response = null;
        try {
            response = permisoForestalService.obtenerSolicitudPorAccesoSolicitud(idSolicitud, idSolicitudAcceso);
            log.info("PermisoForestal - obtenerSolicitudPorAccesoSolicitud", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - obtenerSolicitudPorAccesoSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/obtenerArchivoSolicitud/{idSolicitud}")
    @ApiOperation(value = "obtenerArchivoSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<List<ArchivoSolicitudEntity>> obtenerArchivoSolicitud(@PathVariable Integer idSolicitud) {
        log.info("PermisoForestal- obtenerArchivoSolicitud", idSolicitud);
        List<ArchivoSolicitudEntity> response = null;
        try {
            response = permisoForestalService.obtenerArchivoSolicitud(idSolicitud);
            log.info("PermisoForestal - obtenerArchivoSolicitud", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - obtenerArchivoSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/obtenerArchivoSolicitudAdjunto/{idSolicitud}")
    @ApiOperation(value = "obtenerArchivoSolicitudAdjunto", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<List<ArchivoSolicitudEntity>> obtenerArchivoSolicitudAdjunto(@PathVariable Integer idSolicitud) {
        log.info("PermisoForestal- obtenerArchivoSolicitudAdjunto", idSolicitud);
        List<ArchivoSolicitudEntity> response = null;
        try {
            response = permisoForestalService.obtenerArchivoSolicitudAdjunto(idSolicitud);
            log.info("PermisoForestal - obtenerArchivoSolicitudAdjunto", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - obtenerArchivoSolicitudAdjunto", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/archivoEliminar")
    @ApiOperation(value = "archivoEliminar", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<Boolean> archivoEliminar(@RequestBody ArchivoEntity param) {
        log.info("PermisoForestal- archivoEliminar", param.getIdArchivo());
        ResultClassEntity<Boolean> response = new ResultClassEntity<>(); 
        try {
            response = permisoForestalService.archivoEliminar(param.getIdArchivo());
            log.info("PermisoForestal - archivoEliminar", "Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - obtenerArchivoSolicitudAdjunto", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping(path = "/descargarArchivo/{idArchivoSolicitud}/{idSolicitud}/{tipoArchivo}")
    @ApiOperation(value = "descargarArchivo", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Descarga archivo correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultArchivoEntity> descargarArchivoSolicitud(@PathVariable Integer idArchivoSolicitud,
                                                                         @PathVariable Integer idSolicitud,
                                                                         @PathVariable String tipoArchivo) {
        log.info("PermisoForestal - descargarArchivo", "Inicio descargarArchivo");
        ResultArchivoEntity entity = null;
        try {
            entity = permisoForestalService.descargarArchivoSolicitud(idArchivoSolicitud, idSolicitud, tipoArchivo);
            return new ResponseEntity(entity, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - descargarArchivo", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity(entity, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/descargarArchivoAdjunto/{idArchivoSolicitudAdjunto}/{idSolicitud}/{tipoArchivo}")
    @ApiOperation(value = "descargarArchivoAdjunto", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Descarga archivo correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultArchivoEntity> descargarArchivoAdjunto(@PathVariable Integer idArchivoSolicitudAdjunto,
                                                                       @PathVariable Integer idSolicitud,
                                                                       @PathVariable String tipoArchivo) {
        log.info("PermisoForestal - descargarArchivo", "Inicio descargarArchivo");
        ResultArchivoEntity entity = null;
        try {
            entity = permisoForestalService.descargarArchivoSolicitudAdjunto(idArchivoSolicitudAdjunto, idSolicitud, tipoArchivo);
            return new ResponseEntity(entity, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - descargarArchivo", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity(entity, HttpStatus.BAD_REQUEST);
        }
    }

     @PostMapping(path = "/eliminarArchivo")
    @ApiOperation(value = "eliminarArchivo", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Eliminar archivo correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity> eliminarArchivoSolicitud(@RequestBody Map<String, Object> body) {
        log.info("PermisoForestal - eliminarArchivo", "Inicio eliminarArchivo");
        ResultClassEntity entity = null;
        try {
            Integer idArchivoSolicitud = Integer.parseInt(body.get("idArchivoSolicitud").toString());
            Integer idSolicitud = Integer.parseInt(body.get("idSolicitud").toString());
            String tipoArchivo = body.get("tipoArchivo").toString();
            entity = permisoForestalService.eliminarArchivoSolicitud(idArchivoSolicitud, idSolicitud, tipoArchivo);
            return new ResponseEntity(entity, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - eliminarArchivo", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity(entity, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/eliminarArchivoAdjunto")
    @ApiOperation(value = "eliminarArchivoAdjunto", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Eliminar archivo correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity> eliminarArchivoSolicitudAdjunto(@RequestBody Map<String, Object> body) {
        log.info("PermisoForestal - eliminarArchivoAdjunto", "Inicio eliminarArchivoAdjunto");
        ResultClassEntity entity = null;
        try {
            Integer idArchivoSolicitudAjunto = Integer.parseInt(body.get("idArchivoSolicitudAjunto").toString());
            Integer idSolicitud = Integer.parseInt(body.get("idSolicitud").toString());
            String tipoArchivo = body.get("tipoArchivo").toString();
            entity = permisoForestalService.eliminarArchivoSolicitudAdjunto(idArchivoSolicitudAjunto, idSolicitud, tipoArchivo);
            return new ResponseEntity(entity, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - eliminarArchivoAdjunto", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity(entity, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(path = "/ListarPermisosForestales")
    @ApiOperation(value = "Listar Permisos Forestales", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Permisos Forestales listado"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultEntity> ListarPermisosForestales(@RequestBody SolicitudEntity entity, HttpServletRequest request) {
        log.info("PermisoForestal - ListarPermisosForestales", "Inicio descargarArchivo");
        ResultClassEntity res = new ResultClassEntity();
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        try {
            res = permisoForestalService.ListarPermisosForestales(entity);
            log.info("Permisos Forestales - ListarOrdenamientoProteccion","Proceso realizado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("MesaPartes - listarMesaPartes","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/RegistrarPermisosForestales")
    @ApiOperation(value = "Registrar Permisos Forestales", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Registro correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultEntity> RegistrarPermisosForestales(@RequestBody SolicitudEntity entity, HttpServletRequest request) {
        log.info("PermisoForestal - RegistrarPermisosForestales", "Inicio descargarArchivo");
        ResultEntity res = new ResultEntity();
        try {
            res = permisoForestalService.RegistrarPermisosForestales(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - RegistrarPermisosForestales", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping(path = "/ObtenerPermisosForestales")
    @ApiOperation(value = "Registrar Permisos Forestales", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Permisos Forestales Obtenidos"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultEntity> ObtenerPermisosForestales(@RequestBody SolicitudEntity entity, HttpServletRequest request) {
        log.info("PermisoForestal - ObtenerPermisosForestales", "Inicio descargarArchivo");
        ResultEntity res = new ResultEntity();
        try {
            res = permisoForestalService.ObtenerPermisosForestales(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - ObtenerPermisosForestales", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/validarRequisito")
    @ApiOperation(value = "validarRequisito", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ValidacionRequisitoEntity> validarRequisito(@RequestBody ValidacionRequisitoEntity entity, HttpServletRequest request) {
        log.info("PermisoForestal - ValidarRequisito", "Inicio ValidarRequisito");
        ResultClassEntity<ValidacionRequisitoEntity> res = new ResultClassEntity();
        try {
            res = permisoForestalService.validarRequisitoSolicitud(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - ValidarRequisito", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/evaluarRequisito")
    @ApiOperation(value = "evaluarRequisito", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ValidacionRequisitoEntity> evaluarRequisito(@RequestBody ValidacionRequisitoEntity entity) {
        log.info("PermisoForestal - evaluarRequisito", "Inicio evaluarRequisito");
        ResultClassEntity<ValidacionRequisitoEntity> res = new ResultClassEntity();
        try {
            res = permisoForestalService.evaluarRequisitoSolicitud(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - evaluarRequisito", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/filtrarSolicitudValidacionPorSeccion")
    @ApiOperation(value = "filtrarSolicitudValidacionPorSeccion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ValidacionRequisitoEntity> obtenerSolicitudValidacionPorSeccion(@RequestBody ValidacionRequisitoEntity entity) {
        log.info("PermisoForestal - obtenerSolicitudValidacionPorSeccion", "obtenerSolicitudValidacionPorSeccion ValidarRequisito");
        ResultClassEntity<ValidacionRequisitoEntity> res = new ResultClassEntity();
        try {
            res = permisoForestalService.obtenerSolicitudValidacionPorSeccion(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - obtenerSolicitudValidacionPorSeccion", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/filtrarSolicitudValidacionPorSolicitud")
    @ApiOperation(value = "filtrarSolicitudValidacionPorSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<List<ValidacionRequisitoEntity>> obtenerSolicitudValidacionPorSolicitud(@RequestBody ValidacionRequisitoEntity entity) {
        log.info("PermisoForestal - obtenerSolicitudValidacionPorSolicitud", "Inicio obtenerSolicitudValidacionPorSolicitud");
        ResultClassEntity<List<ValidacionRequisitoEntity>> res = new ResultClassEntity();
        try {
            res = permisoForestalService.obtenerSolicitudValidacionPorSolicitud(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - obtenerSolicitudValidacionPorSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/filtrarSolicitudEvaluacionPorSolicitud")
    @ApiOperation(value = "filtrarSolicitudEvaluacionPorSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<List<ValidacionRequisitoEntity>> obtenerSolicitudEvaluacionPorSolicitud(@RequestBody ValidacionRequisitoEntity entity) {
        log.info("PermisoForestal - filtrarSolicitudEvaluacionPorSolicitud", "Inicio filtrarSolicitudEvaluacionPorSolicitud");
        ResultClassEntity<List<ValidacionRequisitoEntity>> res = new ResultClassEntity();
        try {
            res = permisoForestalService.obtenerSolicitudEvaluacionPorSolicitud(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - filtrarSolicitudEvaluacionPorSolicitud", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(path = "/enviarCorreoSolicitudEvaluacion")
    @ApiOperation(value = "enviarCorreoSolicitudEvaluacion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ValidacionRequisitoEntity> enviarCorreoSolicitudEvaluacion(@RequestBody ValidacionRequisitoEntity entity) {
        log.info("PermisoForestal - enviarCorreoSolicitudEvaluacion", "Inicio enviarCorreoSolicitudEvaluacion");
        ResultClassEntity<Boolean> res = new ResultClassEntity();
        try {
            res = permisoForestalService.envioSolicitudEvaluacion(entity);
            return new ResponseEntity(res, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PermisoForestal - enviarCorreoSolicitudEvaluacion", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Danny Nazario [06-12-2021]
     * @modificado:
     * @descripción: {Listar/Filtrar Permiso Forestal}
     * @param: Integer idPermisoForestal
     * @param: Integer idTipoProceso
     * @param: Integer idTipoPermiso
     * @param: String codEstado
     * @param: String codTipoPersona
     * @param: String codTipoActor
     * @param: String codTipoCncc
     * @param: String numeroDocumento
     * @param: String nombrePersona
     * @param: String razonSocial
     */
    @GetMapping(path = "/listarPermisoForestal")
    @ApiOperation(value = "Listar y/o Buscar(Filtrar) PermisoForestal", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public Pageable<List<PermisoForestalEntity>> filtrar(@RequestParam(required = false) Integer idPermisoForestal,
                                                         @RequestParam(required = false) Integer idTipoProceso,
                                                         @RequestParam(required = false) Integer idTipoPermiso,
                                                         @RequestParam(required = false) String codEstado,
                                                         @RequestParam(required = false) String codTipoPersona,
                                                         @RequestParam(required = false) String codTipoActor,
                                                         @RequestParam(required = false) String codTipoCncc,
                                                         @RequestParam(required = false) String numeroDocumento,
                                                         @RequestParam(required = false) String nombrePersona,
                                                         @RequestParam(required = false) String razonSocial,
                                                         @RequestParam(required = false, defaultValue = "1") Long pageNumber, //
                                                         @RequestParam(required = false, defaultValue = "10") Long pageSize,
                                                         @RequestParam(required = false) String sortField,
                                                         @RequestParam(required = false, defaultValue = "DESC") String sortType) throws Exception {
        Page p = new Page(pageNumber, pageSize, sortField, sortType);
        log.info("obtener: {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, pageable={}", idPermisoForestal, idTipoProceso, idTipoPermiso, codEstado, codTipoPersona,
                codTipoActor, codTipoCncc, numeroDocumento, nombrePersona, razonSocial, p);
        try {
            Pageable<List<PermisoForestalEntity>> response = permisoForestalService.filtrar(idPermisoForestal, idTipoProceso, idTipoPermiso,
                    codEstado, codTipoPersona, codTipoActor, codTipoCncc, numeroDocumento, nombrePersona, razonSocial, p);
            log.info("Service - obtener: Proceso realizado correctamente");
            return response;
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor: Danny Nazario [07-12-2021]
     * @modificado:
     * @descripción: {Listar/Filtrar Evaluación Permiso Forestal}
     * @param: Integer idPermisoForestal
     * @param: Integer idTipoProceso
     * @param: Integer idTipoPermiso
     * @param: String fechaRegistro
     * @param: String codigoEstado
     */
    @GetMapping(path = "/filtrarEval")
    @ApiOperation(value = "Listar y/o Buscar(Filtrar) Evaluación PermisoForestal", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public Pageable<List<PermisoForestalEntity>> filrarEval(@RequestParam(required = false) Integer idPermisoForestal,
                                                       @RequestParam(required = false) Integer idTipoProceso,
                                                       @RequestParam(required = false) Integer idTipoPermiso,
                                                       @RequestParam(required = false) String fechaRegistro,
                                                       @RequestParam(required = false) String codigoEstado,
                                                       @RequestParam(required = false) String codigoPerfil,
                                                       @RequestParam(required = false, defaultValue = "1") Long pageNumber, //
                                                       @RequestParam(required = false, defaultValue = "10") Long pageSize,
                                                       @RequestParam(required = false) String sortField,
                                                       @RequestParam(required = false, defaultValue = "DESC") String sortType) throws Exception {
        Page p = new Page(pageNumber, pageSize, sortField, sortType);
        log.info("obtener: {}, {}, {}, {},{}, {}, pageable={}", idPermisoForestal, idTipoProceso, idTipoPermiso, fechaRegistro, codigoEstado, codigoPerfil, p);
        try {
            Pageable<List<PermisoForestalEntity>> response = permisoForestalService.filtrarEval(idPermisoForestal, idTipoProceso, idTipoPermiso, fechaRegistro, codigoEstado, codigoPerfil, p);
            log.info("Service - obtener: Proceso realizado correctamente");
            return response;
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    /**
     * @autor:  Danny Nazario [09-12-2021]
     * @modificado:
     * @descripción: {Registrar Permiso Forestal}
     * @param: List<PermisoForestalEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarPermisoForestal")
    @ApiOperation(value = "registrarPermisoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPermisoForestal(@RequestBody PermisoForestalEntity obj) {
        log.info("PermisoForestal - registrarPermisoForestal", obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = permisoForestalService.registrarPermisoForestal(obj);
            log.info("PermisoForestal - registrarPermisoForestal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PermisoForestal -registrarPermisoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [17-12-2021]
     * @modificado:
     * @descripción: {Actualizar estado Permiso Forestal}
     * @param: List<PermisoForestalEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/actualizarEstadoPermisoForestal")
    @ApiOperation(value = "actualizarEstadoPermisoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarEstadoPermisoForestal(@RequestBody PermisoForestalEntity obj) {
        log.info("PermisoForestal - actualizarEstadoPermisoForestal", obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = permisoForestalService.actualizarEstadoPermisoForestal(obj);
            if(obj.getCodigoEstado().equals("EPSFFAV") || obj.getCodigoEstado().equals("EPSFDESF")){
                String estado="";
                if(obj.getCodigoEstado().equals("EPSFFAV")){estado="FAVORABLE";}else if(obj.getCodigoEstado().equals("EPSFDESF")){estado="DESFAVORABLE";}
                String[] emailCC = new String[3];
                emailCC[0] = "rafitogglan@gmail.com";
                emailCC[1] = "rcoal@valtx.pe";
                emailCC[2] = "kpaucard@valtx.pe";
                String[] email = new String[1];
                EvaluacionPermisoForestalDto d2 = new EvaluacionPermisoForestalDto();
                d2.setIdPermisoForestal(obj.getIdPermisoForestal());
                List<EvaluacionPermisoForestalDto> listaUsuario = evaluacionRepository.listarEvaluacionPermisoForestalUsuario(d2);
                for (EvaluacionPermisoForestalDto elementUsuario: listaUsuario) {
                    email[0] = elementUsuario.getResponsable();
                }
                EmailEntity emailModel = new EmailEntity();
                emailModel.setContent("La evaluación del permiso forestal N° "+obj.getIdPermisoForestal()+", es "+estado);
                emailModel.setEmail(email);
                emailModel.setSubject("Evaluación de la solicitud de permiso forestal");
                emailModel.setEmailCc(emailCC);
                service.sendEmail(emailModel);
            }


            log.info("PermisoForestal - actualizarEstadoPermisoForestal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PlanManejo -actualizarEstadoPermisoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [09-12-2021]
     * @modificado:
     * @descripción: {Registrar Información General Permiso Forestal}
     * @param: List<InformacionForestalPermisoForestalEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarInformacionGeneral")
    @ApiOperation(value = "registrarInformacionGeneral" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarInformacionGeneral(@RequestBody InformacionGeneralPermisoForestalEntity obj) {
        log.info("PermisoForestal - registrarInformacionGeneral", obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = permisoForestalService.registrarInformacionGeneral(obj);
            if (obj.getIdPermisoForestal()>0 || obj.getIdPermisoForestal()!=null){
                List<TituloHabilitanteEntity> list =  new ArrayList<TituloHabilitanteEntity>();
                TituloHabilitanteEntity tituloHabilitanteEntity= new TituloHabilitanteEntity();

                tituloHabilitanteEntity.setIdPermisoForestal(obj.getIdPermisoForestal());

                List<TituloHabilitanteEntity>  listaTituloHabilitante=tituloHabilitanteService.listarTituloHabilitante(tituloHabilitanteEntity);

                for (TituloHabilitanteEntity objeto : listaTituloHabilitante){
                    TituloHabilitanteEntity request = new TituloHabilitanteEntity();
                    request.setIdPermisoForestal(obj.getIdPermisoForestal());
                    request.setCodigoUnico(objeto.getCodigoUnico());
                    request.setIdUsuarioRegistro(obj.getIdUsuarioRegistro());
                    list.add(request);
                    tituloHabilitanteService.actualizarCodigoCifradoTituloHabilitante(list);
                }
            }

            log.info("PermisoForestal - registrarInformacionGeneral", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PermisoForestal -registrarInformacionGeneral","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [09-12-2021]
     * @modificado:
     * @descripción: {Listar Información General Permiso Forestal}
     * @param: List<InformacionForestalPermisoForestalEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarInformacionGeneral")
    @ApiOperation(value = "listarInformacionGeneral" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<InformacionGeneralPermisoForestalEntity>> listarInformacionGeneral(
            @RequestBody InformacionGeneralPermisoForestalEntity obj) {
        log.info("PermisoForestal - listarInformacionGeneral", obj.toString());
        ResultClassEntity<InformacionGeneralPermisoForestalEntity> response = new ResultClassEntity<>();
        try {
            response = permisoForestalService.listarInformacionGeneral(obj);
            log.info("PermisoForestal - listarInformacionGeneral","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PermisoForestal - listarInformacionGeneral","Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor:  Rafael Azaña [01-02-2022]
     * @modificado:
     * @descripción: {Cifrado Rot}
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/codigoCifrado")
    @ApiOperation(value = "codigoCifrado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<CodigoCifradoEntity>> codigoCifrado(
            @RequestBody CodigoCifradoEntity obj) {
        log.info("codigoCifrado - codigoCifrado", obj.toString());
        ResultClassEntity<CodigoCifradoEntity> response = new ResultClassEntity<>();
        try {
            response = permisoForestalService.codigoCifrado(obj);
            log.info("PermisoForestal - listarInformacionGeneral","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("PermisoForestal - listarInformacionGeneral","Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarArchivo")
    @ApiOperation(value = "registrarArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarArchivo(
            @RequestBody List<SolicitudSANArchivoEntity> list) {
        log.info("registrarArchivo - registrarArchivo", list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = permisoForestalService.RegistrarArchivo(list);
            log.info("permisoForestal - registrarArchivo", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("permisoForestal -registrarArchivo", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarArchivo")
    @ApiOperation(value = "listarArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public pe.gob.serfor.mcsniffs.entity.ResponseEntity listarArchivo(@RequestBody SolicitudSANArchivoEntity param){
        try{
            return  new pe.gob.serfor.mcsniffs.entity.ResponseEntity(true, "ok", permisoForestalService.ListarArchivo(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new pe.gob.serfor.mcsniffs.entity.ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/eliminarArchivoPermisoForestal")
    @ApiOperation(value = "eliminarArchivoPermisoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarArchivoPermisoForestal(@RequestBody SolicitudSANArchivoEntity solicitudSANArchivoEntity){
        log.info("SolicitudSAN - eliminarArchivo",solicitudSANArchivoEntity.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = permisoForestalService.EliminarArchivo(solicitudSANArchivoEntity);
            log.info("SolicitudSAN - eliminarArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudSAN - eliminarArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Lista Plan Manejo por filtro }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroPlanManejo")
    @ApiOperation(value = "listarPorFiltroPlanManejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCronogramaActividadExcel(@RequestParam("nroDocumento") String nroDocumento,
                                                                                     @RequestParam("codTipoPlan") String codTipoPlan) {
        log.info("PermisoForestal - listarPorFiltroPlanManejo");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalService.listarPorFiltroPlanManejo(nroDocumento, codTipoPlan);
            if (result.getSuccess()) {
                log.info("PermisoForestal - listarPorFiltroPlanManejo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestal - listarPorFiltroPlanManejo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Registrar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @PostMapping(path = "/registrarPermisoForestalPlanManejo")
    @ApiOperation(value = "registrarPermisoForestalPlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarPermisoForestalPlanManejo(@RequestBody List<PermisoForestalPlanManejoEntity> items) {
        log.info("PermisoForestal - registrarPermisoForestalPlanManejo", items.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalService.registrarPermisoForestalPlanManejo(items);

            if (result.getSuccess()) {
                log.info("PermisoForestal - registrarPermisoForestalPlanManejo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestal - registrarPermisoForestalPlanManejo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Listar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @PostMapping(path = "/listarPermisoForestalPlanManejo")
    @ApiOperation(value = "listarPermisoForestalPlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPermisoForestalPlanManejo(@RequestBody PermisoForestalPlanManejoEntity item) {
        log.info("PermisoForestal - listarPermisoForestalPlanManejo", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalService.listarPermisoForestalPlanManejo(item);

            if (result.getSuccess()) {
                log.info("PermisoForestal - listarPermisoForestalPlanManejo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestal - listarPermisoForestalPlanManejo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [03-01-2022]
     * @modificado:
     * @descripción: { Eliminar Permiso Forestal - Plan Manejo }
     * @param: PermisoForestalPlanManejoEntity
     */
    @PostMapping(path = "/eliminarPermisoForestalPlanManejo")
    @ApiOperation(value = "eliminarPermisoForestalPlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarPlanManejoGeometriaPlanManejo(@RequestBody PermisoForestalPlanManejoEntity item) {
        log.info("PermisoForestal - eliminarPermisoForestalPlanManejo", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalService.eliminarPermisoForestalPlanManejo(item);

            if (result.getSuccess()) {
                log.info("PermisoForestal - eliminarPermisoForestalPlanManejo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PermisoForestal - eliminarPermisoForestal", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/descargarPlantillaFormatoPermiso")
    @ApiOperation(value = "descargarPlantillaFormatoPermiso", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaFormatoPermiso(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = permisoForestalService.descargarPlantillaFormatoPermiso();
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("permiso forestal - descargarPlantillaFormatoPermiso", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
 

    @GetMapping(path = "/descargarPlantillaResolAprobacion")
    @ApiOperation(value = "descargarPlantillaResolAprobacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaResolAprobacion(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = permisoForestalService.descargarPlantillaResolAprobacion();
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("permiso forestal - descargarPlantillaResolAprobacion", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(path = "/descargarPlantillaResolDesAprobacion")
    @ApiOperation(value = "descargarPlantillaResolDesAprobacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaResolDesAprobacion(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = permisoForestalService.descargarPlantillaResolDesAprobacion();
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("permiso forestal - descargarPlantillaResolDesAprobacion", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    @PostMapping(path = "/permisoForestalActualizarRemitido")
    @ApiOperation(value = "permisoForestalActualizarRemitido", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> permisoForestalActualizarRemitido(@RequestBody PermisoForestalPlanManejoEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = permisoForestalService.permisoForestalActualizarRemitido(obj);
            if (!result.getSuccess()) {
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

        } catch (Exception Ex){
            log.error("PlanManejoController - permisoForestalActualizarRemitido", "Ocurrió un error al actualizar permiso forestal remitido del plan de manejo en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Rafael Azaña 31-01-2022
     * @modificado:
     * @descripción: {Obtener Archivo plantilla de permisos forestales}
     * @param: Integer idPermisoForestal
     */

    @GetMapping(path = "/plantillaPermisosForestales/{idPermisoForestal}")
    @ApiOperation(value = "plantillaPermisosForestales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> plantillaPermisosForestales(@PathVariable(required = true) Integer idPermisoForestal){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = permisoForestalService.plantillaPermisosForestales(idPermisoForestal);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("plantillaPermisosForestales - Descargar plantillaPermisosForestales", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    @PostMapping(path = "/actualizarPermisoForestal")
    @ApiOperation(value = "actualizarPermisoForestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> actualizarPermisoForestal(@RequestBody PermisoForestalEntity request){
        ResultClassEntity result = new ResultClassEntity();
            try {
                result = permisoForestalService.actualizarPermisoForestal(request);
                if (!result.getSuccess()) {
                    return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
                } else {
                    return new ResponseEntity(result, HttpStatus.OK);
                }



        } catch (Exception Ex){
            log.error("PermisoForestal - actualizarPermisoForestal", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity(result,HttpStatus.FORBIDDEN);
        }
    }

}
