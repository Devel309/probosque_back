package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion.SolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.FileServerConexion;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.SolicitudOpinionService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(Urls.solicitudOpinion.BASE)
public class SolicitudOpinionController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudOpinionController.class);
    @Autowired
    private SolicitudOpinionService solicitudOpinionService;

    @Autowired
    private ArchivoService serArchivo;

    @PostMapping(path = "/listarSolicitudOpinion")
    @ApiOperation(value = "listarSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudOpinion(@RequestBody SolicitudOpinionDto param){
        log.info("SolicitudOpinion - ListarSolicitudOpinion \n"+param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = solicitudOpinionService.ListarSolicitudOpinion(param);
            log.info("SolicitudOpinion - ListarSolicitudOpinion \n"+"Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudOpinion - ListarSolicitudOpinion \n"+"Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerSolicitudOpinion")
    @ApiOperation(value = "obtenerSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerSolicitudOpinion(@RequestBody SolicitudOpinionDto param){
        log.info("SolicitudOpinion - ObtenerSolicitudOpinion \n"+param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = solicitudOpinionService.ObtenerSolicitudOpinion(param);
            log.info("SolicitudOpinion - ObtenerSolicitudOpinion \n"+"Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudOpinion - ObtenerSolicitudOpinion \n"+"Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudOpinion")
    @ApiOperation(value = "registrarSolicitudOpinion", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity registrarSolicitudOpinion(@RequestBody SolicitudOpinionDto param) {
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{

            response = solicitudOpinionService.RegistrarSolicitudOpinion(param);
           if(response.getSuccess()){

               return new ResponseEntity(response, HttpStatus.OK);

            }else{

               return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            log.error("SolicitudOpinion -RegistrarSolicitudOpinion \n"+"Ocurrió un error");
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolicitudOpinion")
    @ApiOperation(value = "actualizarSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudOpinion(@RequestBody SolicitudOpinionDto param){
        log.info("SolicitudOpinion - ActualizarSolicitudOpinion \n"+param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = solicitudOpinionService.ActualizarSolicitudOpinion(param);
            log.info("SolicitudOpinion - ActualizarSolicitudOpinion \n"+"Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudOpinion -ActualizarSolicitudOpinion \n"+"Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolicitudOpinionArchivo")
    @ApiOperation(value = "actualizarSolicitudOpinionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudOpinionArchivo(@RequestParam(required = false) Integer idSolicitudOpinion,
                                                             @RequestParam(required = false) String idTipoDocumento,
                                                             @RequestParam(required = false) Integer documento,
                                                             @RequestParam(required = false) Integer idUsuarioModificacion,
                                                             @RequestParam("file") MultipartFile file
    ){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = new pe.gob.serfor.mcsniffs.entity.ResponseEntity();
        log.info("SolicitudOpinion  - ActualizarSolicitudOpinionArchivo   \n archivo:"+file.getOriginalFilename()+"idSolicitudOpinion:"+idSolicitudOpinion.toString() +" idUsuarioModificacion:"+idUsuarioModificacion.toString()+" idTipoDocumento:"+idTipoDocumento);

        ResultClassEntity response =new ResultClassEntity();
        try{
            ArchivoEntity objArc=new ArchivoEntity();
            objArc.setIdArchivo(0);
            response =  serArchivo.RegistrarArchivoGeneral(file, idUsuarioModificacion, idTipoDocumento);
            if(response.getSuccess()){
                SolicitudOpinionDto obj = new SolicitudOpinionDto();
                obj.setIdSolicitudOpinion(idSolicitudOpinion);
                if(documento == 1){
                    obj.setDocumentoOficio(response.getCodigo());
                }else{
                    obj.setDocumentoRespuesta(response.getCodigo());
                }
                obj.setIdUsuarioModificacion(idUsuarioModificacion);
                response = solicitudOpinionService.ActualizarSolicitudOpinionArchivo(obj);
            }else{
                response.setMessage(response.getMessage());
            }
            log.info("SolicitudOpinion - ActualizarSolicitudOpinionArchivo \n"+"Proceso realizado correctamente");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudOpinion - ActualizarSolicitudOpinionArchivo \n"+"Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarSolicitudOpinionArchivo")
    @ApiOperation(value = "eliminarSolicitudOpinionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudOpinionArchivo(@RequestBody SolicitudOpinionDto solicitudOpinion){
        log.info("SolicitudOpinion - eliminarSolicitudOpinionArchivo");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            ArchivoEntity objArc=new ArchivoEntity();
            objArc.setIdArchivo(0);
            Integer IdArchivo=(solicitudOpinion.getDocumentoOficio()==null?solicitudOpinion.getDocumentoRespuesta():solicitudOpinion.getDocumentoOficio());
            response =  serArchivo.EliminarArchivoGeneral(IdArchivo, solicitudOpinion.getIdUsuarioElimina());
            if(response.getSuccess()){
                solicitudOpinion.setIdArchivo((solicitudOpinion.getDocumentoOficio()==null?solicitudOpinion.getDocumentoRespuesta():solicitudOpinion.getDocumentoOficio()));
                response = solicitudOpinionService.EliminarSolicitudOpinionArchivo(solicitudOpinion);
            }else{
                response.setMessage(response.getMessage());
            }
            log.info("SolicitudOpinion - eliminarSolicitudOpinionArchivo \n"+"Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudOpinion - eliminarSolicitudOpinionArchivo \n"+"Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Registrar y Actualizar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */

    @PostMapping(path = "/registrarSolicitudOpinionEvaluacion")
    @ApiOperation(value = "registrarSolicitudOpinionEvaluacion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarSolicitudOpinionEvaluacion(
            @RequestBody List<SolicitudOpinionEntity> list) {
        log.info("SolicitudOpinionController - registrarSolicitudOpinionEvaluacion", list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = solicitudOpinionService.RegistrarSolicitudOpinionEvaluacion(list);
            log.info("SolicitudOpinionController - registrarSolicitudOpinionEvaluacion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("SolicitudOpinionController -registrarSolicitudOpinionEvaluacion", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Listar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */


    @PostMapping(path = "/listarSolicitudOpinionEvaluacion")
    @ApiOperation(value = "listarSolicitudOpinionEvaluacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<SolicitudOpinionEntity>> listarSolicitudOpinionEvaluacion(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codSolicitud,  @RequestParam(required = false) String subCodSolicitud) {
        try {
            return solicitudOpinionService.ListarSolicitudOpinionEvaluacion(idPlanManejo,codSolicitud,subCodSolicitud);
        } catch (Exception ex) {

            ResultClassEntity<List<SolicitudOpinionEntity>> result = new ResultClassEntity<>();
            log.error("SolicitudOpinionController - listarSolicitudOpinionEvaluacion", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [28/10-2021]
     * @modificado:
     * @descripción: {Eliminar la solicitud de opinion de la evaluacion}
     * @param:SolicitudOpinionEntity
     */

    @PostMapping(path = "/eliminarSolicitudOpinionEvaluacion")
    @ApiOperation(value = "eliminarSolicitudOpinionEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarSolicitudOpinionEvaluacion(@RequestBody SolicitudOpinionEntity solicitudOpinionEntity){
        log.info("SolicitudOpinionController - eliminarProteccionBosque",solicitudOpinionEntity.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudOpinionService.EliminarSolicitudOpinionEvaluacion(solicitudOpinionEntity);
            log.info("SolicitudOpinionController - eliminarSolicitudOpinionEvaluacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudOpinionController - eliminarSolicitudOpinionEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
