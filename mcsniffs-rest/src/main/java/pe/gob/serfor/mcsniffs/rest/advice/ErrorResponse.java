package pe.gob.serfor.mcsniffs.rest.advice;

public class ErrorResponse {
    private Integer status;
    private String path;
    private String message;
    private String error;
    private Boolean success;

    public ErrorResponse() {
    }

    public ErrorResponse(Integer status, String path, String message, String error, Boolean success) {
        this.status = status;
        this.path = path;
        this.message = message;
        this.error = error;
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
