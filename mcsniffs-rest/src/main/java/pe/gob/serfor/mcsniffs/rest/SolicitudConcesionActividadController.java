package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionActividadEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionActividadService;


@RestController
@RequestMapping(Urls.solicitudConcesion.BASE)
public class SolicitudConcesionActividadController extends AbstractRestController{
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionActividadController.class);

    @Autowired
    private SolicitudConcesionActividadService solicitudConcesionActividadService;

    @PostMapping(path = "/listarSolicitudConcesionActividad")
    @ApiOperation(value = "listar Actividad de Solicitud Concesion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarActividadSolicitudConcesion(@RequestBody SolicitudConcesionActividadEntity request) {
        log.info("SolicitudConcesionActividad - listarSolicitudConcesionActividad", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionActividadService.listarActividadSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - listarSolicitudConcesionActividad", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - listarSolicitudConcesionActividad", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerInformacionActividadFuenteFinanciamiento")
    @ApiOperation(value = "Obtener Informacion Actividad Fuente Financiamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInformacionActividadFuenteFinanciamiento(@RequestBody SolicitudConcesionActividadEntity request) {
        log.info("SolicitudConcesionActividad - obtenerInformacionActividadFuenteFinanciamiento", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionActividadService.obtenerInformacionActividadFuenteFinanciamiento(request);
            log.info("SolicitudConcesionActividad - obtenerInformacionActividadFuenteFinanciamiento", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - obtenerInformacionActividadFuenteFinanciamiento", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarActividadSolicitudConcesion")
    @ApiOperation(value = "Registrar Actividad de la Solicitud Concesión" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarActividadSolicitudConcesion(@RequestBody SolicitudConcesionActividadEntity request){
        log.info("SolicitudConcesionActividad - registrarActividadSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionActividadService.registrarActividadSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - registrarActividadSolicitudConcesion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionActividad - registrarActividadSolicitudConcesion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/registrarInformacionActividadFuenteFinanciamiento")
    @ApiOperation(value = "Registrar Informacion Actividad Fuente Financiamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInformacionActividadFuenteFinanciamiento(@RequestBody List<SolicitudConcesionActividadEntity> request){
        log.info("SolicitudConcesionActividad - registrarInformacionActividadFuenteFinanciamiento",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionActividadService.registrarInformacionActividadFuenteFinanciamiento(request);
            log.info("SolicitudConcesionActividad - registrarInformacionActividadFuenteFinanciamiento","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionActividad - registrarInformacionActividadFuenteFinanciamiento","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarInformacionActividadFuenteFinanciamiento")
    @ApiOperation(value = "Actualizar Informacion Actividad Fuente Financiamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInformacionActividadFuenteFinanciamiento(@RequestBody SolicitudConcesionActividadEntity request){
        log.info("SolicitudConcesionActividad - actualizarInformacionActividadFuenteFinanciamiento",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionActividadService.actualizarInformacionActividadFuenteFinanciamiento(request);
            log.info("SolicitudConcesionActividad - actualizarInformacionActividadFuenteFinanciamiento","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionActividad - actualizarInformacionActividadFuenteFinanciamiento","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
