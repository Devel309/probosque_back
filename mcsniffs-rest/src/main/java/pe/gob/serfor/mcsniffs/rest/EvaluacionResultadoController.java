package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.EvaluacionResultadoEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionResultadoDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EvaluacionResultadoService;
import pe.gob.serfor.mcsniffs.service.PotencialProduccionForestalService;

import java.util.List;

@RestController
@RequestMapping(Urls.evaluacionResultado.BASE)
public class EvaluacionResultadoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EvaluacionResultadoController.class);
    
    @Autowired
    private EvaluacionResultadoService service;

    /**
     * @autor: Rafael Azaña [27/10-2021]
     * @modificado:
     * @descripción: {REGISTRAR Y ACTUALIZAR EL RESULTADO DE LA EVALUACION}
     * @param:EvaluacionResultadoEntity
     */

    @PostMapping(path = "/registrarEvaluacionResultado")
    @ApiOperation(value = "registrar EvaluacionResultado", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarEvaluacionResultado(
            @RequestBody List<EvaluacionResultadoEntity> list) {
        log.info("registrarEvaluacionResultado - registrarEvaluacionResultado", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = service.RegistrarEvaluacionResultado(list);
            log.info("EvaluacionResultadoController - registrarEvaluacionResultado", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("EvaluacionResultadoController -registrarEvaluacionResultado", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [27-10-2021]
     * @descripción: {Listar Evaluacionesultado}
     * @param: EvaluacionResultadoDetalleDto
     * @return: ResponseEntity<ResponseVO>
     */

    @PostMapping(path = "/listarEvaluacionesultado")
    @ApiOperation(value = "Listar Evaluacionesultado", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<EvaluacionResultadoDetalleDto>> listarEvaluacionesultado(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codResultado,  @RequestParam(required = false) String subCodResultado) {
        try {
            return service.ListarEvaluacionResultado(idPlanManejo,codResultado,subCodResultado);
        } catch (Exception ex) {

            ResultClassEntity<List<EvaluacionResultadoDetalleDto>> result = new ResultClassEntity<>();
            log.error("EvaluacionesultadoController - listarEvaluacionesultado", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [18/10-2021]
     * @modificado:
     * @descripción: {ELIMINAR POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */

    @PostMapping(path = "/eliminarEvaluacionesultado")
    @ApiOperation(value = "eliminarEvaluacionesultado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarEvaluacionesultado(@RequestBody EvaluacionResultadoDetalleDto evaluacionResultadoDetalleDto){
        log.info("Evaluacionesultado - eliminarEvaluacionesultado",evaluacionResultadoDetalleDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = service.EliminarEvaluacionResultado(evaluacionResultadoDetalleDto);
            log.info("Evaluacionesultado - eliminarEvaluacionesultado","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Evaluacionesultado - eliminarEvaluacionesultado","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
