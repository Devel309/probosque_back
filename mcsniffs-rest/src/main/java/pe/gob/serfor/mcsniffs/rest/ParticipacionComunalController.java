package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ParticipacionComunalService;

import java.util.List;

@RestController
@RequestMapping(Urls.participacionComunal.BASE)
public class ParticipacionComunalController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ParticipacionComunalController.class);

    @Autowired
    private ParticipacionComunalService participacionComunalService;

    /**
     * @autor: Ivan Minaya [07-07-2021]
     * @modificado:
     * @descripción: {Registrar la participación comunal}
     * @param:List<ParticipacionComunalEntity>
     */
    @PostMapping(path = "/registrarParticipacionComunal")
    @ApiOperation(value = "registrarParticipacionComunal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarParticipacionComunal(@RequestBody List<ParticipacionComunalEntity> participacionComunal){
        log.info("ParticipacionComunal- RegistrarParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = participacionComunalService.RegistrarParticipacionComunal(participacionComunal);
            log.info("ParticipacionComunal- RegistrarParticipacionComunal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ParticipacionComunal -RegistrarParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [07-07-2021]
     * @modificado:
     * @descripción: {Registrar la participación comunal}
     * @param:List<ParticipacionComunalEntity>
     */
    @PostMapping(path = "/actualizarParticipacionComunal")
    @ApiOperation(value = "actualizarParticipacionComunal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarParticipacionComunal(@RequestBody List<ParticipacionComunalEntity> participacionComunal){
        log.info("ParticipacionComunal- ActualizarParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.ActualizarParticipacionComunal(participacionComunal);
            log.info("ParticipacionComunal- ActualizarParticipacionComunal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ParticipacionComunal -ActualizarParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [07-07-2021]
     * @modificado:
     * @descripción: {Eliminar la participación comunal}
     * @param:ParticipacionComunalEntity
     */
    @PostMapping(path = "/eliminarParticipacionComunal")
    @ApiOperation(value = "eliminarParticipacionComunal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarParticipacionComunal(@RequestBody ParticipacionComunalEntity participacionComunal){
        log.info("ParticipacionComunal- EliminarParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.EliminarParticipacionComunal(participacionComunal);
            log.info("ParticipacionComunal- EliminarParticipacionComunal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ParticipacionComunal -EliminarParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [07-07-2021]
     * @modificado:
     * @descripción: {Obtener la participación comunal}
     * @param:ParticipacionComunalEntity
     */
    @PostMapping(path = "/obtenerParticipacionComunal")
    @ApiOperation(value = "obtenerParticipacionComunal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerParticipacionComunal(@RequestBody ParticipacionComunalEntity participacionComunal){
        log.info("ParticipacionComunal- ObtenerParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.ObtenerParticipacionComunal(participacionComunal);
            log.info("ParticipacionComunal- ObtenerParticipacionComunal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ParticipacionComunal -ObtenerParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Ivan Minaya [07-07-2021]
     * @modificado:
     * @descripción: {listar por filtro la participación comunal}
     * @param:ParticipacionComunalEntity
     */
    @PostMapping(path = "/listarPorFiltroParticipacionComunal")
    @ApiOperation(value = "listarPorFiltroParticipacionComunal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarPorFiltroParticipacionComunal(@RequestBody ParticipacionComunalEntity participacionComunal){
        log.info("ParticipacionComunal- ObtenerParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.ListarPorFiltroParticipacionComunal(participacionComunal);
            log.info("ParticipacionComunal- ObtenerParticipacionComunal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ParticipacionComunal -ObtenerParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Harry Coa [27-07-2021]
     * @modificado {registrar participación comunal}
     * @param:ParticipacionComunalEntity
     */

    @PostMapping(path = "/registrarParticipacionCiudadana")
    @ApiOperation(value = "registrarParticipacionCiudadana" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarParticipacionCiudadana(@RequestBody ParticipacionComunalEntity participacionComunalEntity){
        log.info("ProteccionBosque - registrarParticipacionCiudadana",participacionComunalEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.RegistrarParticipacionCiudadana(participacionComunalEntity);
            log.info("ProteccionBosque - registrarParticipacionCiudadana","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("ProteccionBosque -registrarParticipacionCiudadana","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Harry Coa [27-07-2021]
     * @modificado {actualizar participación comunal}
     * @param:ParticipacionComunalEntity
     */
    @PostMapping(path = "/actualizarParticipacionCiudadana")
    @ApiOperation(value = "actualizarParticipacionCiudadana" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarParticipacionCiudadana(@RequestBody ParticipacionComunalEntity participacionComunalEntity){
        log.info("ProteccionBosque - actualizarParticipacionCiudadana",participacionComunalEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.ActualizarParticipacionCiudadana(participacionComunalEntity);
            log.info("ProteccionBosque - actualizarParticipacionCiudadana","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ProteccionBosque -actualizarParticipacionCiudadana","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Harry Coa [27-07-2021]
     * @modificado {actualizar participación comunal}
     * @param:ParticipacionComunalEntity
     */
    @PostMapping(path = "/eliminarParticipacionCiudadana")
    @ApiOperation(value = "eliminarParticipacionCiudadana" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarParticipacionCiudadana(@RequestBody ParticipacionComunalEntity participacionComunalEntity){
        log.info("ProteccionBosque - eliminarParticipacionCiudadana",participacionComunalEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = participacionComunalService.EliminarParticipacionCiudadana(participacionComunalEntity);
            log.info("ProteccionBosque - eliminarParticipacionCiudadana","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ProteccionBosque -eliminarParticipacionCiudadana","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarParticipacionCiudadana")
    @ApiOperation(value = "listarParticipacionCiudadana" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarParticipacionCiudadana(@RequestBody ParticipacionComunalEntity participacionComunalEntity){
        try{
            return  new ResponseEntity (true, "ok", participacionComunalService.ListarParticipacionCiudadana(participacionComunalEntity));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }


    @PostMapping(path = "/registrarParticipacionCiudadanaList")
    @ApiOperation(value = "registrarParticipacionCiudadanaList" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarParticipacionCiudadanaList(@RequestBody List<ParticipacionComunalEntity> participacionComunal){
        log.info("ParticipacionComunal- RegistrarParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = participacionComunalService.RegistrarParticipacionCiudadanaList(participacionComunal);
            log.info("ParticipacionComunal- RegistrarParticipacionComunal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ParticipacionComunal -RegistrarParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarParticipacionComunalCaberaDetalle")
    @ApiOperation(value = "registrarParticipacionComunalCaberaDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarParticipacionComunalCaberaDetalle(@RequestBody List<ParticipacionComunalEntity> participacionComunal){
        log.info("ParticipacionComunal- RegistrarParticipacionComunal",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = participacionComunalService.registrarParticipacionComunalCaberaDetalle(participacionComunal);
            log.info("ParticipacionComunal- RegistrarParticipacionComunal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ParticipacionComunal -RegistrarParticipacionComunal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarParticipacionComunalCaberaDetalle")
    @ApiOperation(value = "listarParticipacionComunalCaberaDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarParticipacionComunalCaberaDetalle(@RequestBody ParticipacionComunalParamEntity participacionComunal){
        log.info("ParticipacionComunal- listarParticipacionComunalCaberaDetalle",participacionComunal.toString());
        ResponseEntity result = null;
        ResultEntity<ParticipacionComunalEntity> response ;
        try{

            response = participacionComunalService.listarParticipacionComunalCaberaDetalle(participacionComunal);
            log.info("ParticipacionComunal- listarParticipacionComunalCaberaDetalle","Proceso realizado correctamente");
            if (!response.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ParticipacionComunal -listarParticipacionComunalCaberaDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarParticipacionComunalCaberaDetalle")
    @ApiOperation(value = "eliminarParticipacionComunalCaberaDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarParticipacionComunalCaberaDetalle(@RequestBody ParticipacionComunalParamEntity participacionComunal){
        log.info("ParticipacionComunal- eliminarParticipacionComunalCaberaDetalle",participacionComunal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = participacionComunalService.eliminarParticipacionComunalCaberaDetalle(participacionComunal);
            log.info("ParticipacionComunal- eliminarParticipacionComunalCaberaDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("ParticipacionComunal -eliminarParticipacionComunalCaberaDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
