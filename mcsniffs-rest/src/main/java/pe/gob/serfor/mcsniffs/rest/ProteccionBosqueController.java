package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;

import java.util.List;

@RestController
@RequestMapping(Urls.proteccionBosque.BASE)
public class ProteccionBosqueController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProteccionBosqueController.class);



    @Autowired
    private pe.gob.serfor.mcsniffs.service.ProteccionBosqueService proteccionBosqueService;
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {configuracion de protección de bosque de demarcación}
     * @param: ProteccionBosqueDemarcacionEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/configuracionProteccionBosqueDemarcacion")
    @ApiOperation(value = "configuracionProteccionBosqueDemarcacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity configuracionProteccionBosqueDemarcacion(@RequestBody ProteccionBosqueDemarcacionEntity proteccionBosqueDemarcacion){
        log.info("ProteccionBosque - ConfiguracionProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.ConfiguracionProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
            log.info("ProteccionBosque - ConfiguracionProteccionBosqueDemarcacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ConfiguracionProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registrar protección de bosque demarcación}
     * @param: List<ProteccionBosqueDemarcacionEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarProteccionBosqueDemarcacion")
    @ApiOperation(value = "registrarProteccionBosqueDemarcacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarProteccionBosqueDemarcacion(@RequestBody List<ProteccionBosqueDemarcacionEntity>  proteccionBosqueDemarcacion){
        log.info("ProteccionBosque - RegistrarProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = proteccionBosqueService.RegistrarProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
            log.info("ProteccionBosque - RegistrarProteccionBosqueDemarcacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/actualizarProteccionBosqueDemarcacion")
    @ApiOperation(value = "actualizarProteccionBosqueDemarcacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarProteccionBosqueDemarcacion(@RequestBody List<ProteccionBosqueDemarcacionEntity>  proteccionBosqueDemarcacion){
        log.info("ProteccionBosque - ActualizarProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.ActualizarProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
            log.info("ProteccionBosque - ActualizarProteccionBosqueDemarcacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ActualizarProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/obtenerProteccionBosqueDemarcacion")
    @ApiOperation(value = "obtenerProteccionBosqueDemarcacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerProteccionBosqueDemarcacion(@RequestBody ProteccionBosqueDemarcacionEntity proteccionBosqueDemarcacion){
        log.info("ProteccionBosque - ObtenerProteccionBosqueDemarcacion",proteccionBosqueDemarcacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.ObtenerProteccionBosqueDemarcacion(proteccionBosqueDemarcacion);
            log.info("ProteccionBosque - ObtenerProteccionBosqueDemarcacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ObtenerProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/registrarProteccionBosqueAmbiental")
    @ApiOperation(value = "registrarProteccionBosqueAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarProteccionBosqueAmbiental(@RequestBody List<ProteccionBosqueGestionAmbientalEntity> proteccionBosqueGestionAmbiental){
        log.info("ProteccionBosque - RegistrarProteccionBosqueAmbiental",proteccionBosqueGestionAmbiental.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = proteccionBosqueService.RegistrarProteccionBosqueAmbiental(proteccionBosqueGestionAmbiental);
            log.info("ProteccionBosque - RegistrarProteccionBosqueAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarProteccionBosqueAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarPorFiltroProteccionBosqueAmbiental")
    @ApiOperation(value = "listarPorFiltroProteccionBosqueAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarPorFiltroProteccionBosqueAmbiental(@RequestBody ProteccionBosqueGestionAmbientalEntity proteccionBosqueGestionAmbiental){
        log.info("ProteccionBosque - ListarPorFiltroProteccionBosqueAmbiental",proteccionBosqueGestionAmbiental.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.ListarPorFiltroProteccionBosqueAmbiental(proteccionBosqueGestionAmbiental);
            log.info("ProteccionBosque - ListarPorFiltroProteccionBosqueAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarPorFiltroProteccionBosqueAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/eliminarProteccionBosqueAmbiental")
    @ApiOperation(value = "eliminarProteccionBosqueAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarProteccionBosqueAmbiental(@RequestBody ProteccionBosqueGestionAmbientalEntity proteccionBosqueDemarcacion){
        log.info("ProteccionBosque - EliminarProteccionBosqueAmbiental",proteccionBosqueDemarcacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.EliminarProteccionBosqueAmbiental(proteccionBosqueDemarcacion);
            log.info("ProteccionBosque - EliminarProteccionBosqueAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -EliminarProteccionBosqueAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registrar ProteccionBosqueImapctoAmbiental}
     * @param: ProteccionBosqueImapctoAmbientalEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarProteccionBosqueImpactoAmbiental")
    @ApiOperation(value = "registrarProteccionBosqueImpactoAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarProteccionBosqueImpactoAmbiental(@RequestBody List<ProteccionBosqueImpactoAmbientalEntity> proteccionBosqueImapctoAmbiental){
        log.info("ProteccionBosque - RegistrarProteccionBosqueImpactoAmbiental",proteccionBosqueImapctoAmbiental.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.RegistrarProteccionBosqueImpactoAmbiental(proteccionBosqueImapctoAmbiental);
            log.info("ProteccionBosque - RegistrarProteccionBosqueImpactoAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar por filtro ProteccionBosqueImapctoAmbiental}
     * @param: ProteccionBosqueImapctoAmbientalEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroProteccionBosqueImpactoAmbiental")
    @ApiOperation(value = "listarPorFiltroProteccionBosqueImpactoAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarPorFiltroProteccionBosqueImpactoAmbiental(@RequestBody ProteccionBosqueImpactoAmbientalEntity proteccionBosqueImapctoAmbiental){
        log.info("ProteccionBosque - ListarPorFiltroProteccionBosqueImpactoAmbiental",proteccionBosqueImapctoAmbiental.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.ListarPorFiltroProteccionBosqueImpactoAmbiental(proteccionBosqueImapctoAmbiental);
            log.info("ProteccionBosque - ListarPorFiltroProteccionBosqueImpactoAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarPorFiltroProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar ProteccionBosqueImapctoAmbiental}
     * @param: ProteccionBosqueImapctoAmbientalEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarProteccionBosqueImpactoAmbiental")
    @ApiOperation(value = "eliminarProteccionBosqueImpactoAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarProteccionBosqueImpactoAmbiental(@RequestBody ProteccionBosqueImpactoAmbientalEntity proteccionBosqueImapctoAmbiental){
        log.info("ProteccionBosque - EliminarProteccionBosqueImpactoAmbiental",proteccionBosqueImapctoAmbiental.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = proteccionBosqueService.EliminarProteccionBosqueImpactoAmbiental(proteccionBosqueImapctoAmbiental);
            log.info("ProteccionBosque - EliminarProteccionBosqueImpactoAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -EliminarProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [15-10-2021]
     * @descripción: {Registrar y Actualizar ProteccionBosque}
     * @param: ProteccionBosqueEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarProteccionBosque")
    @ApiOperation(value = "registrarProteccionBosque", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarProteccionBosque(
            @RequestBody List<ProteccionBosqueEntity> list) {
        log.info("registrarProteccionBosque - registrarProteccionBosque", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = proteccionBosqueService.RegistrarProteccionBosque(list);
            log.info("ProteccionBosque - registrarProteccionBosque", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("ProteccionBosque -registrarProteccionBosque", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [15-10-2021]
     * @descripción: {Listar ProteccionBosque}
     * @param: ProteccionBosqueEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @PostMapping(path = "/listarProteccionBosque")
    @ApiOperation(value = "Listar ProteccionBosque", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<ProteccionBosqueDetalleDto>> listarProteccionBosque(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codPlanGeneral,  @RequestParam(required = false) String subCodPlanGeneral) {
        try {
            return proteccionBosqueService.listarProteccionBosque(idPlanManejo,codPlanGeneral,subCodPlanGeneral);
        } catch (Exception ex) {

            ResultClassEntity<List<ProteccionBosqueDetalleDto>> result = new ResultClassEntity<>();
            log.error("ProteccionBosqueController - listarProteccionBosque", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [26/10-2021]
     * @modificado:
     * @descripción: {ELIMINAR PROTECCION DEL BOSQUE}
     * @param:ProteccionBosqueDetalleDto
     */

    @PostMapping(path = "/eliminarProteccionBosque")
    @ApiOperation(value = "eliminarProteccionBosque" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarProteccionBosque(@RequestBody ProteccionBosqueDetalleDto proteccionBosqueDetalleDto){
        log.info("ProteccionBosque - eliminarProteccionBosque",proteccionBosqueDetalleDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = proteccionBosqueService.EliminarProteccionBosque(proteccionBosqueDetalleDto);
            log.info("ProteccionBosque - eliminarProteccionBosque","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProteccionBosque - eliminarProteccionBosque","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [02-02-2022]
     * @modificado:
     * @descripción: {Registra  Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarProteccionBosqueExcel")
    @ApiOperation(value = "registrarProteccionBosqueExcel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarProteccionBosqueExcel(@RequestParam("file") MultipartFile file,
                                                                                     @RequestParam("nombreHoja") String nombreHoja,
                                                                                     @RequestParam("numeroFila") Integer numeroFila,
                                                                                     @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                     @RequestParam("codPlanGeneral") String codPlanGeneral,
                                                                                     @RequestParam("subCodPlanGeneral") String subCodPlanGeneral,
                                                                                     @RequestParam("codPlanGeneralDet") String codPlanGeneralDet,
                                                                                     @RequestParam("subCodPlanGeneralDet") String subCodPlanGeneralDet,
                                                                                     @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                     @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        log.info("ProteccionBosque - registrarProteccionBosqueExcel");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = proteccionBosqueService.registrarProteccionBosqueExcel(file, nombreHoja, numeroFila, numeroColumna, codPlanGeneral, subCodPlanGeneral, codPlanGeneralDet, subCodPlanGeneralDet, idPlanManejo, idUsuarioRegistro);
            if (result.getSuccess()) {
                log.info("PotencialProduccionForestal - registrarPotencialProduccionExcel", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PotencialProduccionForestal - registrarPotencialProduccionExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
