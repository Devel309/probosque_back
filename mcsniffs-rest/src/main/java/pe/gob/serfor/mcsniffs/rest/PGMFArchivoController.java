package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PGMFArchivoService;

@RestController
@RequestMapping(Urls.postulacionPFDM.BASE)
public class PGMFArchivoController extends AbstractRestController {
    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {Repository de los archivos relaciones con el plan de manejo}
     *
     */

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PostulacionPFDMController.class);

    @Autowired
    private PGMFArchivoService service;

    /**
     * @autor: JaquelineDB [26-08-2021]
     * @modificado:
     * @descripción: {registrar archivos}
     * @param:PGMFArchivoEntity
     */
    @PostMapping(path = "/registrarArchivo")
    @ApiOperation(value = "registrar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarArchivo(@RequestBody PGMFArchivoEntity obj) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarArchivo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("PGMFArchivoController - registrarArchivo", "Ocurrió un error al guardar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [1-09-2021]
     * @modificado:
     * @descripción: {listar los archivos del proceso}
     * @param:PGMFArchivoEntity
     */
    @PostMapping(path = "/listarArchivosPGMF")
    @ApiOperation(value = "listar Archivos PGMF", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PGMFArchivoEntity>> listarArchivosPGMF(@RequestBody PGMFArchivoEntity obj) {
        ResultEntity<PGMFArchivoEntity> result = new ResultEntity<PGMFArchivoEntity>();
        try {
            result = service.listarArchivosPGMF(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("PGMFArchivoController - listarArchivosPGMF",
                    "Ocurrió un error al listar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [2-09-2021]
     * @modificado:
     * @descripción: {se genera el anexo2pgmf}
     * @param:PGMFArchivoEntity
     */
    @PostMapping(path = "/generarAnexo2PGMF")
    @ApiOperation(value = "generar Anexo2PGMF", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo2PGMF(@RequestBody PGMFArchivoEntity obj) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo2PGMF(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("PGMFArchivoController - generarAnexo2PGMF", "Ocurrió un error al listar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [2-09-2021]
     * @modificado:
     * @descripción: {se genera el anexo3pgmf}
     * @param:PGMFArchivoEntity
     */
    @PostMapping(path = "/generarAnexo3PGMF")
    @ApiOperation(value = "generar Anexo3PGMF", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo3PGMF(@RequestBody PGMFArchivoEntity obj) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo3PGMF(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("PGMFArchivoController - generarAnexo3PGMF", "Ocurrió un error al listar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [02-09-2021]
     * @descripción: {Listar listarDetalleArchivo}
     * @param: idPlanManejo
     * @return: ResponseEntity<ResponseVO>
     */
    @GetMapping(path = "/listarDetalleArchivo/{idPlanManejo}/{codigoTipoPGMF}")
    @ApiOperation(value = "listarDetalleArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PGMFArchivoEntity>> listarDetalleArchivo(@PathVariable Integer idPlanManejo,@PathVariable String codigoTipoPGMF) {
        ResultEntity<PGMFArchivoEntity> result = new ResultEntity<PGMFArchivoEntity>();
        try {
            result = service.listarDetalleArchivo(idPlanManejo,codigoTipoPGMF);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("PGMFArchivoController - listarDetalleArchivo",
                    "Ocurrió un error al guardar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [06-09-2021]
     * @descripción: { eliminar un archivo detalle PGMF}
     * @param: Integer idArchivo
     * @param: Integer idUsuario
     */
    @GetMapping(path = "/eliminarDetalleArchivo/{idArchivo}/{idUsuario}")
    @ApiOperation(value = "Eliminar PGMFArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<Integer>> eliminarDetalleArchivo(
        @PathVariable Integer idArchivo, @PathVariable Integer idUsuario) {
        ResultClassEntity<Integer> response = new ResultClassEntity<>();
        try {
            response = service.eliminarDetalleArchivo(idArchivo, idUsuario);
            log.info("Service - eliminar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("PGMFArchivoController - eliminarDetalleArchivo", e.getMessage());
            response.setError(Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
