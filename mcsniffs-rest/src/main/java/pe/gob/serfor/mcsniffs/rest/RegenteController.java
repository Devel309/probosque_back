package pe.gob.serfor.mcsniffs.rest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import pe.gob.serfor.mcsniffs.entity.RegenteEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.RegenteService;

/**
 * @autor: Harry Coa [24-07-2021]
 * @modificado:
 * @descripción: {controller s}
 * @param:OrdenamientoProteccionEntity
 */
@RestController
@RequestMapping(Urls.regente.BASE)
public class RegenteController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(RegenteController.class);

    @Autowired
    private RegenteService regenteService;

    
    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @PostMapping(path = "/ListarRegente")
    @ApiOperation(value = "ListarRegente" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity ListarRegente(@RequestBody RegenteEntity entidad){
        log.info("Regente - ListarRegente", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultEntity<RegenteEntity>  response = new ResultEntity<>();
        try {
            response = regenteService.ListarRegente(entidad);
            log.info("Regente - ListarRegente","Proceso realizado correctamente");
            if (!response.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("Regente - ListarRegente","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [22-09-2021]
     * @modificado:
     * @descripción: {Listar regente}
     * @param:RegenteEntity
     */
    @PostMapping(path = "/registrarRegente")
    @ApiOperation(value = "registrarRegente" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarRegente(@RequestBody RegenteEntity entidad){
        log.info("Regente - registrarRegente", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = regenteService.RegistrarRegente(entidad);
            log.info("Regente - registrarRegente","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("Regente - registrarRegente","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [25-03-2022]
     * @modificado:
     * @descripción: {Listar información del regente y sus archivos}
     * @param:RegenteEntity
     */
    @PostMapping(path = "/ListarRegenteArchivos")
    @ApiOperation(value = "ListarRegenteArchivos" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity ListarRegenteArchivos(@RequestBody RegenteEntity entidad){
        log.info("Regente - ListarRegente", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultEntity<RegenteEntity>  response = new ResultEntity<>();
        try {
            response = regenteService.ListarRegenteArchivos(entidad);
            log.info("Regente - ListarRegente","Proceso realizado correctamente");
            if (!response.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("Regente - ListarRegente","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
