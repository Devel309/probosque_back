package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.UnidadAprovechamientoService;

@RestController
@RequestMapping(Urls.unidadAprovechamiento.BASE)
public class UnidadAprovechamientoController {
    /**
     * @autor: JaquelineDB [12-06-2021]
     * @modificado:
     * @descripción: {Servicio de Unidad aprovechamiento, en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    private UnidadAprovechamientoService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);


    /**
     * @autor: JaquelineDB [12-06-2021]
     * @modificado:
     * @descripción: {Lista todas las unidades de aprovechamiento}
     * @param:consulta
     */
    @PostMapping(path = "/consultauadisponible")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<UnidadAprovechamientoEntity>> ListarUnidadAprovechamiento(@RequestBody ConsultaUAEntity consulta){
        ResultEntity<UnidadAprovechamientoEntity> lst_ua = new ResultEntity<UnidadAprovechamientoEntity>();
        try {
            //lst_ua = service.ListarUnidadAprovechamiento(consulta);
            return new ResponseEntity<>(lst_ua,HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ConsultaApiController - ListarUnidadAprovechamiento", "Ocurrió un error al Listar en: "+Ex.getMessage());
            lst_ua.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_ua,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {inserta y actualiza la unidad aprovechamiento}
     * @param:obj
     */
    @PostMapping(path = "/guardarUnidadAprovechamiento")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarUnidadAprovechamiento(@RequestBody UnidadAprovechamientoEntity consulta){
        ResultClassEntity lst_ua = new ResultClassEntity();
        try {
            lst_ua = service.guardarUnidadAprovechamiento(consulta);
            return new ResponseEntity<>(lst_ua,HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ConsultaApiController - ListarUnidadAprovechamiento", "Ocurrió un error al Listar en: "+Ex.getMessage());
            lst_ua.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_ua,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [07-07-2021]
     * @modificado:
     * @descripción: {Listar Unidad de Aprovechamiento}
     * 
     */
    @GetMapping(path = "/listar")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT")) 
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PropertiesEntity>> listar(){
        ResultEntity<PropertiesEntity> items = new ResultEntity<PropertiesEntity>();
        try {
            items =service.listar();
            return new ResponseEntity<>(items, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("UnidadAprovechamientoController - Listar", "Ocurrió un error al Listar en: "+Ex.getMessage());
            items.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(items,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [19-07-2021]
     * @modificado:
     * @descripción: {Buscar Unidad de Aprovechamiento por Id de Proceso de Oferta}
     * 
     */
    @GetMapping(path = "/buscarByIdProcesoOferta/{id}")
    @ApiOperation(value = "buscarByIdProcesoOferta", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultEntity<PropertiesEntity>> buscarByIdProcesoOferta(@PathVariable Integer id)  {
        ResultEntity<PropertiesEntity> items = new ResultEntity<PropertiesEntity>();
        try {
            items = service.buscarByIdProcesoOferta(id);
            return new ResponseEntity<>(items,HttpStatus.OK);
        } catch (Exception e) {
            log.error("UnidadAprovechamientoController - buscarByIdProcesoOferta", "Ocurrió un error al buscar UA en :" + e.getMessage());
            items.setInnerException(e.getMessage());
            return new ResponseEntity<>(items,  HttpStatus.BAD_REQUEST);
        }
    }
    
    /**
     * @autor: Abner Valdez [04-08-2021]
     * @modificado:
     * @descripción: {Buscar Unidad de Aprovechamiento por Id de Plan de Manejo}
     * 
     */
    @GetMapping(path = "/buscarByIdPlanManejo/{id}")
    @ApiOperation(value = "buscarByIdPlanManejo", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultEntity<PropertiesEntity>> buscarByIdPlanManejo(@PathVariable Integer id)  {
        ResultEntity<PropertiesEntity> items = new ResultEntity<PropertiesEntity>();
        try {
            items = service.buscarByIdPlanManejo(id);
            return new ResponseEntity<>(items,HttpStatus.OK);
        } catch (Exception e) {
            log.error("UnidadAprovechamientoController - buscarByIdPlanManejo", "Ocurrió un error al buscar UA en :" + e.getMessage());
            items.setInnerException(e.getMessage());
            return new ResponseEntity<>(items,  HttpStatus.BAD_REQUEST);
        }
    }
}
