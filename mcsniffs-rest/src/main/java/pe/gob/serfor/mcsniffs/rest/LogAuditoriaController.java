package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.InformacionGeneralEntity;
import pe.gob.serfor.mcsniffs.entity.LogAuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralService;
import pe.gob.serfor.mcsniffs.service.LogAuditoriaService;

@RestController
@RequestMapping(Urls.logAuditoria.BASE)
public class LogAuditoriaController {

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(LogAuditoriaController.class);

    @Autowired
    private LogAuditoriaService logAuditoriaService;


    @PostMapping(path = "/registrarLogAuditoria")
    @ApiOperation(value = "registrarLogAuditoria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarLogAuditoria(
            @RequestBody LogAuditoriaEntity request) {

        log.info("LogAuditoria - RegistrarLogAuditoria", request.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response;
        try {
            response = logAuditoriaService.RegistrarLogAuditoria(request);
            log.info("LogAuditoria - RegistrarLogAuditoria", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        } catch (Exception e) {

            log.error("Oposicion - registrarOposicion", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


}
