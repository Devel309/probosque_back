package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoEntity;
import pe.gob.serfor.mcsniffs.entity.ActividadAprovechamientoParam;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ActividadAprovechamientoService;

@RestController
@RequestMapping(Urls.ActividadAprovechamiento.BASE)
public class ActividadAprovechamientoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ActividadSilviculturalController.class);
    /**
     * @autor: Jaqueline DiazB [26-11-2021]
     * @modificado:
     * @descripción: {Repository actividades aprovechamiento}
     */
    
    @Autowired
    private ActividadAprovechamientoService actividadAprovechamientoService;

    /**
     * @autor: Jason Retamozo [15-02-2022]
     * @creado:
     * @descripción: {Obtener total de Aprovechamiento no maderable}
     * @param: ActividadAprovechamientoParam
     */
    @PostMapping(path = "/ObtenerTotalAprovechamientoNoMaderable")
    @ApiOperation(value = "ObtenerTotalVolumenCorta", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity ObtenerTotalAprovechamientoNoMaderable(@RequestBody ActividadAprovechamientoParam request) {
        log.info("ActividadAprovechamientoController - ObtenerTotalAprovchamientoNoMaderable", request.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = actividadAprovechamientoService.ObtenerTotalAprovechamientoNoMaderable(request);
            if (response.getSuccess()) {
                log.info("ActividadAprovechamientoController - ObtenerTotalAprovechamientoNoMaderable", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -ObtenerTotalAprovechamientoNoMaderable", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jason Retamozo [08-02-2022]
     * @creado:
     * @descripción: {Obtener total de volumen de corta en Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @PostMapping(path = "/ObtenerTotalVolumenCorta")
    @ApiOperation(value = "ObtenerTotalVolumenCorta", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity ObtenerTotalVolumenCorta(@RequestBody ActividadAprovechamientoParam request) {
        log.info("ActividadAprovechamientoController - ObtenerTotalVolumenCorta", request.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadAprovechamientoService.ObtenerTotalVolumenCorta(request);
            if (response.getSuccess()) {
                log.info("ActividadAprovechamientoController - ObtenerTotalVolumenCorta", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -ObtenerTotalVolumenCorta", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {Lista Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @PostMapping(path = "/listarActvidadAprovechamiento")
    @ApiOperation(value = "ListarActvidadAprovechamiento", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity ListarActvidadAprovechamiento(@RequestBody ActividadAprovechamientoParam request) {
        log.info("ActividadAprovechamientoController - ListarActvidadAprovechamiento", request.toString());
        ResponseEntity result = null;
        ResultEntity response;
        try {
            response = actividadAprovechamientoService.ListarActvidadAprovechamiento(request);
            if (response.getIsSuccess()) {
                log.info("ActividadAprovechamientoController - ListarActvidadAprovechamiento", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -ListarActvidadAprovechamiento", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {registra Aprovechamiento Actividad}
     * @param: List<ActividadAprovechamientoEntity>
     */
    @PostMapping(path = "/registrarActvidadAprovechamiento")
    @ApiOperation(value = "RegistrarActvidadAprovechamiento", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity RegistrarActvidadAprovechamiento(@RequestBody List<ActividadAprovechamientoEntity> request) {
        log.info("ActividadAprovechamientoController - RegistrarActvidadAprovechamiento", request.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadAprovechamientoService.RegistrarActvidadAprovechamiento(request);
            if (response.getSuccess()) {
                log.info("ActividadAprovechamientoController - RegistrarActvidadAprovechamiento", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -RegistrarActvidadAprovechamiento", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad}
     * @param: ActividadAprovechamientoParam
     */
    @PostMapping(path = "/eliminarActvidadAprovechamiento")
    @ApiOperation(value = "EliminarActvidadAprovechamiento", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarActvidadAprovechamiento(@RequestBody ActividadAprovechamientoParam request) {
        log.info("ActividadAprovechamientoController - EliminarActvidadAprovechamiento", request.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadAprovechamientoService.EliminarActvidadAprovechamiento(request);
            if (response.getSuccess()) {
                log.info("ActividadAprovechamientoController - EliminarActvidadAprovechamiento", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -EliminarActvidadAprovechamiento", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad det}
     * @param: ActividadAprovechamientoParam
     */
    @PostMapping(path = "/eliminarActvidadAprovechamientoDet")
    @ApiOperation(value = "EliminarActvidadAprovechamientoDet", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarActvidadAprovechamientoDet(@RequestBody ActividadAprovechamientoParam request) {
        log.info("ActividadAprovechamientoController - EliminarActvidadAprovechamientoDet", request.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadAprovechamientoService.EliminarActvidadAprovechamientoDet(request);
            if (response.getSuccess()) {
                log.info("ActividadAprovechamientoController - EliminarActvidadAprovechamientoDet", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -EliminarActvidadAprovechamientoDet", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     /**
     * @autor: Jaqueline Diaz B [27-11-2021]
     * @modificado:
     * @descripción: {elimina Aprovechamiento Actividad det sub}
     * @param: ActividadAprovechamientoParam
     */
    @PostMapping(path = "/eliminarActvidadAprovechamientoDetSub")
    @ApiOperation(value = "EliminarActvidadAprovechamientoDetSub", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarActvidadAprovechamientoDetSub(@RequestBody ActividadAprovechamientoParam request) {
        log.info("ActividadAprovechamientoController - EliminarActvidadAprovechamientoDetSub", request.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = actividadAprovechamientoService.EliminarActvidadAprovechamientoDetSub(request);
            if (response.getSuccess()) {
                log.info("ActividadAprovechamientoController - EliminarActvidadAprovechamientoDetSub", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamientoController -EliminarActvidadAprovechamientoDetSub", "Ocurrio un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [08-01-2022]
     * @modificado:
     * @descripción: { Registra Censo Comercial Especies Excel }
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarCensoComercialEspecieExcel")
    @ApiOperation(value = "registrarCensoComercialEspecieExcel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCensoComercialEspecieExcel(@RequestParam("file") MultipartFile file,
                                                                                       @RequestParam("nombreHoja") String nombreHoja,
                                                                                       @RequestParam("numeroFila") Integer numeroFila,
                                                                                       @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                       @RequestParam("codActvAprove") String codActvAprove,
                                                                                       @RequestParam("codSubActvAprove") String codSubActvAprove,
                                                                                       @RequestParam("codActvAproveDet") String codActvAproveDet,
                                                                                       @RequestParam("idActvAprove") Integer idActvAprove,
                                                                                       @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                       @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        log.info("ActividadAprovechamiento - registrarCensoComercialEspecieExcel");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = actividadAprovechamientoService.registrarCensoComercialEspecieExcel(file, nombreHoja, numeroFila, numeroColumna, codActvAprove, codSubActvAprove, codActvAproveDet, idActvAprove, idPlanManejo, idUsuarioRegistro);
            if (result.getSuccess()) {
                log.info("ActividadAprovechamiento - registrarCensoComercialEspecieExcel", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ActividadAprovechamiento - registrarCensoComercialEspecieExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
