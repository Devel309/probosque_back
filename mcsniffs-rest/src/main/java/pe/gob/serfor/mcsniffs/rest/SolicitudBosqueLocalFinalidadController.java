package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal.SolicitudBosqueLocalFinalidadDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalFinalidadService;


@RestController
@RequestMapping(Urls.solicitudBosqueLocalFinalidad.BASE)
public class SolicitudBosqueLocalFinalidadController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalFinalidadController.class);


    @Autowired
    private SolicitudBosqueLocalFinalidadService solicitudBosqueLocalFinalidadService;

    

    @PostMapping(path = "/listarSolicitudBosqueLocalFinalidad")
    @ApiOperation(value = "listarSolicitudBosqueLocalFinalidad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarSolicitudBosqueLocalFinalidad(@RequestBody SolicitudBosqueLocalFinalidadDto obj){
        log.info("SolicitudBosqueLocalFinalidad - listarSolicitudBosqueLocalFinalidad", obj);
        ResponseEntity result = null;
        ResultClassEntity response ;

        try{
            response = solicitudBosqueLocalFinalidadService.listarSolicitudBosqueLocalFinalidad(obj);
            log.info("SolicitudBosqueLocalFinalidad - ListarSolicitudBosqueLocalFinalidad","Proceso realizado correctamente");
            
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - ListarSolicitudBosqueLocalFinalidad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudBosqueLocalFinalidad")
    @ApiOperation(value = "registrarSolicitudBosqueLocalFinalidad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarSolicitudBosqueLocalFinalidad(@RequestBody List<SolicitudBosqueLocalFinalidadDto> objList){
        log.info("SolicitudBosqueLocalFinalidad - registrarSolicitudBosqueLocalFinalidad", objList);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudBosqueLocalFinalidadService.registrarSolicitudBosqueLocalFinalidad(objList);
            log.info("SolicitudBosqueLocalFinalidad - RegistrarSolicitudBosqueLocalFinalidad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - RegistrarSolicitudBosqueLocalFinalidad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarSolicitudBosqueLocalFinalidad")
    @ApiOperation(value = "eliminarSolicitudBosqueLocalFinalidad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarSolicitudBosqueLocalFinalidad(@RequestBody SolicitudBosqueLocalFinalidadDto obj){
        log.info("SolicitudBosqueLocalFinalidad - eliminarSolicitudBosqueLocalFinalidad", obj);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudBosqueLocalFinalidadService.eliminarSolicitudBosqueLocalFinalidad(obj);
            log.info("SolicitudBosqueLocalFinalidad - EliminarSolicitudBosqueLocalFinalidad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - EliminarSolicitudBosqueLocalFinalidad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
