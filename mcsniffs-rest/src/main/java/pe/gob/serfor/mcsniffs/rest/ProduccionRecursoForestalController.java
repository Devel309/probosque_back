package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProduccionRecursoForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.OrdenamientoAreaManejoService;
import pe.gob.serfor.mcsniffs.service.ProduccionRecursoForestalService;

import java.util.List;

@RestController
@RequestMapping(Urls.produccionRecursoForestal.BASE)
public class ProduccionRecursoForestalController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProduccionRecursoForestalController.class);

    @Autowired
    private ProduccionRecursoForestalService produccionRecursoForestalService;
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar ProduccionRecursoForestal}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarOrdenamientoAreaManejo")
    @ApiOperation(value = "listarOrdenamientoAreaManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarOrdenamientoAreaManejo(@RequestBody ProduccionRecursoForestalEntity produccionRecursoForestal){
        log.info("ProduccionRecursoForestal - ListarOrdenamientoAreaManejo",produccionRecursoForestal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = produccionRecursoForestalService.ListarProduccionRecursoForestal(produccionRecursoForestal);
            log.info("ProduccionRecursoForestal - ListarProduccionRecursoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProduccionRecursoForestal -ListarProduccionRecursoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registrar ProduccionRecursoForestal}
     * @param: List<ProduccionRecursoForestalDto>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarProduccionRecursoForestal")
    @ApiOperation(value = "registrarProduccionRecursoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarProduccionRecursoForestal(@RequestBody List<ProduccionRecursoForestalDto> list){
        log.info("ProduccionRecursoForestal - RegistrarProduccionRecursoForestal",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = produccionRecursoForestalService.RegistrarProduccionRecursoForestal(list);
            log.info("ProduccionRecursoForestal - RegistrarProduccionRecursoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProduccionRecursoForestal -RegistrarProduccionRecursoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar ProduccionRecursoForestal}
     * @param: ProduccionRecursoForestalEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarProduccionRecursoForestal")
    @ApiOperation(value = "eliminarProduccionRecursoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarProduccionRecursoForestal(@RequestBody ProduccionRecursoForestalEntity produccionRecursoForestal){
        log.info("ProduccionRecursoForestal - EliminarProduccionRecursoForestal",produccionRecursoForestal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = produccionRecursoForestalService.EliminarProduccionRecursoForestal(produccionRecursoForestal);
            log.info("ProduccionRecursoForestal - EliminarProduccionRecursoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProduccionRecursoForestal -EliminarProduccionRecursoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar ProduccionRecursoForestalEspecie}
     * @param: ProduccionRecursoForestalEspecieEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarProduccionRecursoForestalEspecie")
    @ApiOperation(value = "eliminarProduccionRecursoForestalEspecie" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarProduccionRecursoForestalEspecie(@RequestBody ProduccionRecursoForestalEspecieEntity produccionRecursoForestalEsp){
        log.info("ProduccionRecursoForestal - EliminarProduccionRecursoForestalEspecie",produccionRecursoForestalEsp.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = produccionRecursoForestalService.EliminarProduccionRecursoForestalEspecie(produccionRecursoForestalEsp);
            log.info("ProduccionRecursoForestal - EliminarProduccionRecursoForestalEspecie","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProduccionRecursoForestal -EliminarProduccionRecursoForestalEspecie","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
