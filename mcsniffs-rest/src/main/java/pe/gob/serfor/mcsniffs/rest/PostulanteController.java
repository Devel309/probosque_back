package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.AnexoRequestEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AnexoService;

@RestController
@RequestMapping(Urls.postulante.BASE)
public class PostulanteController  extends AbstractRestController{
    

    @Autowired
    private AnexoService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PostulanteController.class);

    /* @PostMapping(path = "/obtenerUsuario")
    @ApiOperation(value = "Obtener Usuario", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerUsuario(@RequestBody PersonaDto obj){
        ResultClassEntity<PersonaDto> result = new ResultClassEntity<PersonaDto>();
        try {
            result = service.obtenerUsuario(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - obtenerUsuario", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    } */

    @PostMapping(path = "/obtenerUsuario")
    @ApiOperation(value = "Obtener Usuario", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<PersonaDto>> obtenerUsuario(@RequestBody AnexoRequestEntity obj){
        ResultClassEntity<PersonaDto> result = new ResultClassEntity<PersonaDto>();
        try {
            result = service.obtenerUsuario(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Usuario - obtenerUsuario", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


}
