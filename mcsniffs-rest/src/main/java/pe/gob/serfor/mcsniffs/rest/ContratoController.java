package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ContratoAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoAdjuntosRequestEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoValidacionesEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.GenerarCodContratoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.DescargarContratoPersonaJuridicaDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ContratoService;
import pe.gob.serfor.mcsniffs.service.EmailService;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping(Urls.contrato.BASE)
public class ContratoController extends AbstractRestController{
    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan los contratos}
     *
     */
    @Autowired
    private ContratoService contratoService;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    @Autowired
    private EmailService enviarcorreo;

    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {se registra el contrato}
     * @param:ContratoEntity
     */
    
    @PostMapping(path = "/registrarContrato")
    @ApiOperation(value = "Registrar Contrato" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacion(@RequestBody ContratoEntity data){
        log.info("Contrato - registrarContratos",data.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.registrarContrato(data);
            log.info("Contrato - registrarContratos","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Contrato - registrarContratos","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {se adjuntan los contratos}
     * @param:ContratoAdjuntosRequestEntity
     */
    @PostMapping(path = "/adjuntarArchivosContrato")
    @ApiOperation(value = "Adjuntar Archivos Contrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> AdjuntarArchivosContrato(@RequestBody ContratoAdjuntosRequestEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = contratoService.AdjuntarArchivosContrato(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("ContratoController - AdjuntarArchivosContrato", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Se listan los contratos}
     * @param:FiltroContratoEntity
     */
    @PostMapping(path = "/listarContrato")
    @ApiOperation(value = "Listar Contrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarContrato(@RequestBody ContratoEntity dto){
        log.info("contrato - listarContrato",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.ListarContrato(dto);
            log.info("contrato - ListarContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - listarContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  


     /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se listan los archivos por contrato}
     * @param:IdContrato
     */
    @GetMapping(path = "/obtenerArchivosContrato/{IdContrato}")
    @ApiOperation(value = "Obtener Archivos Contrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ContratoAdjuntosEntity>> ObtenerArchivosContrato(@PathVariable("IdContrato") Integer IdContrato){
        ResultEntity<ContratoAdjuntosEntity> result = new ResultEntity<ContratoAdjuntosEntity>();
        try {
            result = contratoService.ObtenerArchivosContrato(IdContrato);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Contrato - ObtenerArchivosContrato", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


//    /**
//     * @autor: JaquelineDB [23-07-2021]
//     * @modificado:
//     * @descripción: {Se obtiene un contrato}
//     * @param:IdContrato
//     */
//    @GetMapping(path = "/obtenerContrato/{IdContrato}")
//    @ApiOperation(value = "Obtener Contrato", authorizations = @Authorization(value = "JWT"))
//    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
//    public ResponseEntity ObtenerContrato(@PathVariable("IdContrato") Integer IdContrato){
//        ResultClassEntity response = new ResultClassEntity();
//        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
//        try{
//            response = contratoService.ObtenerContrato(IdContrato);
//            log.info("contrato - ObtenerContrato","Proceso realizado correctamente");
//            if (!response.getSuccess()) {
//                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
//            } else {
//                return new ResponseEntity(response, HttpStatus.OK);
//            }
//        }catch (Exception e){
//            log.error("contrato - listarContrato","Ocurrió un error :"+ e.getMessage());
//            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
//            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    // /**
    //  * @autor: JaquelineDB [23-07-2021]
    //  * @modificado:
    //  * @descripción: {Se descarga el docx con los datos importates}
    //  * @param:IdContrato
    //  */
    // @GetMapping(path = "/descargarContrato/{IdContrato}")
    // @ApiOperation(value = "Descargar Contrato", authorizations = @Authorization(value = "JWT"))
    // @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    // public ResponseEntity<ResultArchivoEntity> DescargarContrato(@PathVariable("IdContrato") Integer IdContrato){
    //     ResultArchivoEntity result = new ResultArchivoEntity();
    //     try {
    //         result = contratoService.DescargarContrato(IdContrato);
    //         return new ResponseEntity<>(result, HttpStatus.OK );
    //     } catch (Exception Ex){
    //         log.error("Contrato - DescargarContrato", "Ocurrió un error al obtener en: "+Ex.getMessage());
    //         result.setInnerException(Ex.getMessage());
    //         return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
    //     }
    // }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {Se registran las validaciones de los datos del contrato}
     * @param:ContratoValidacionesEntity
     */
    @PostMapping(path = "/registrarValidaciones")
    @ApiOperation(value = "Registrar Validaciones", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> RegistrarValidaciones(@RequestBody ContratoValidacionesEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = contratoService.RegistrarValidaciones(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("ContratoController - RegistrarValidaciones", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [23-07-2021]
     * @modificado:
     * @descripción: {se Listan las validaciones echas a los datos}
     * @param:IdContrato
     */
    @GetMapping(path = "/listarValidaciones/{IdContrato}")
    @ApiOperation(value = "Listar Validaciones", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ContratoValidacionesEntity>> ListarValidaciones(@PathVariable("IdContrato") Integer IdContrato){
        ResultEntity<ContratoValidacionesEntity> result = new ResultEntity<ContratoValidacionesEntity>();
        try {
            result = contratoService.ListarValidaciones(IdContrato);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Contrato - ListarValidaciones", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/actualizarContrato")
    @ApiOperation(value = "Actualizar Contrato" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarContrato(@RequestBody ContratoEntity data){
        log.info("Contrato - actualizarContrato",data.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.actualizarContrato(data);
            log.info("Contrato - actualizarContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Contrato - actualizarContrato","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [27-07-2021]
     * @modificado:
     * @descripción: {Se actualiza el satus del contrato}
     * @param:IdContrato
     */
    @PostMapping(path = "/actualizarStatusContrato")
    @ApiOperation(value = "Actualizar Status Contrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> ActualizarStatusContrato(@RequestBody ContratoEntity data){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = contratoService.ActualizarStatusContrato(data);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("ContratoController - ActualizarStatusContrato", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    @PostMapping(path = "/obtenerContratoGeneral")
    @ApiOperation(value = "obtenerContratoGeneral", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerContratoGeneral(@RequestBody ContratoEntity dto){
        log.info("contrato - obtenerContratoGeneral",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.obtenerContratoGeneral(dto);
            log.info("contrato - obtenerContratoGeneral","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - obtenerContratoGeneral","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarContratoArchivo")
    @ApiOperation(value = "eliminarContratoArchivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarContratoArchivo(@RequestBody ContratoArchivoDto dto){
        log.info("contrato - eliminarContratoArchivo",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.eliminarContratoArchivo(dto);
            log.info("contrato - eliminarContratoArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - eliminarContratoArchivo","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/generarCodigoContrato")
    @ApiOperation(value = "generarCodigoContrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity generarCodigoContrato(@RequestBody GenerarCodContratoDto dto){
        log.info("contrato - generarCodigoContrato",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.generarCodigoContrato(dto);
            log.info("contrato - generarCodigoContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - generarCodigoContrato","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping(path = "/listarContratoArchivo")
    @ApiOperation(value = "Listar Contrato Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarContratoArchivo(@RequestBody ContratoArchivoDto dto){
        log.info("contrato - listarContratoArchivo",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.listarContratoArchivo(dto);
            log.info("contrato - listarContratoArchivoNombre","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - listarContratoArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerArchivoContrato")
    @ApiOperation(value = "obtenerArchivoContrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerArchivoContrato(@RequestBody ContratoArchivoDto dto){
        log.info("contrato - obtenerArchivoContrato",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.obtenerContratoArchivo(dto);
            log.info("contrato - obtenerArchivoContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - obtenerArchivoContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarEstadoContrato")
    @ApiOperation(value = "actualizarEstadoContrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarEstadoContrato(@RequestBody ContratoEntity dto){
        log.info("contrato - actualizarEstadoContrato",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.actualizarEstadoContrato(dto);
            log.info("contrato - actualizarEstadoContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - actualizarEstadoContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listarComboContrato")
    @ApiOperation(value = "listarComboContrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarComboContrato(@RequestBody ContratoEntity dto){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = contratoService.listarComboContrato(dto);
            log.info("contrato - listarComboContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - listarComboContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/descargarContratoPersonaJuridica")
    @ApiOperation(value = "descargarContratoPersonaJuridica", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarContratoPersonaJuridica(@RequestBody DescargarContratoPersonaJuridicaDto obj){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = contratoService.descargarContratoPersonaJuridica(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Contrato - DescargarContrato", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [28-12-2021]
     * @modificado: 
     * @descripción: {Obtener geometrias por idcontratos}
     * @param:idsContrato
     */
    @GetMapping(path = "/listarContratoGeometria/{idsContrato}")
    @ApiOperation(value = "listarContratoGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPlanManejoGeometria(@PathVariable("idsContrato") String  idsContrato) {
        log.info("ContratoController - listarContratoGeometria", idsContrato.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = contratoService.listarContratoGeometria(idsContrato);

            if (result.getSuccess()) {
                log.info("ContratoController - listarContratoGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ContratoController - listarContratoGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarResolucion")
    @ApiOperation(value = "Registrar resolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarResolucion(@RequestBody ResolucionDto data){
        log.info("Contrato - registrarResolucion",data.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.registrarResolucion(data);
            log.info("Contrato - registrarResolucion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Contrato - registrarResolucion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/notificarResolucion")
    @ApiOperation(value = "notificar resolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity notificarResolucion(@RequestBody ResolucionDto data, HttpServletRequest http){
        log.info("Contrato - notificarResolucion",data.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            String token= http.getHeader("Authorization");
            data.setToken(token);
            response = contratoService.notificarResolucion(data);
            log.info("Contrato - notificarResolucion","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Contrato - notificarResolucion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/obtenerResolucion/{nroDocumentoGestion}")
    @ApiOperation(value = "Registrar resolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerResolucion(@PathVariable("nroDocumentoGestion") Integer nroDocumentoGestion){
        log.info("Contrato - obtenerResolucion"+nroDocumentoGestion);
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.obtenerResolucion(nroDocumentoGestion);
            log.info("Contrato - obtenerResolucion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Contrato - obtenerResolucion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarGeometria")
    @ApiOperation(value = "actualizarGeometria", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarGeometria(@RequestBody ContratoEntity dto){
        log.info("contrato - actualizarGeometria",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.actualizarGeometria(dto);
            log.info("contrato - actualizarGeometria","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - actualizarGeometria","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarEstadoRemitido")
    @ApiOperation(value = "actualizarEstadoRemitido", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarEstadoRemitido(@RequestBody ContratoEntity dto){
        log.info("contrato - actualizarEstadoRemitido",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = contratoService.actualizarEstadoRemitido(dto);
            log.info("Contrato - actualizarEstadoRemitido","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Contrato - actualizarEstadoRemitido","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/descargarModeloConcesionesMaderables/{idPlanManejo}")
    @ApiOperation(value = "descargarModeloConcesionesMaderables", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarModeloConcesionesMaderables(@PathVariable("idPlanManejo") Integer idPlanManejo){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = contratoService.descargarModeloConcesionesMaderables(idPlanManejo);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Contrato - DescargarContrato", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarGeneracionContrato")
    @ApiOperation(value = "listarGeneracionContrato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarGeneracionContrato(@RequestBody ContratoDto dto){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = contratoService.listarGeneracionContrato(dto);
            log.info("contrato - listarGeneracionContrato","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - listarGeneracionContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/obtenerPostulacion")
    @ApiOperation(value = "obtenerPostulacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerPostulacion(@RequestBody ContratoDto dto){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity();
        try{
            response = contratoService.obtenerPostulacion(dto);
            log.info("contrato - obtenerPostulacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("contrato - obtenerPostulacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
