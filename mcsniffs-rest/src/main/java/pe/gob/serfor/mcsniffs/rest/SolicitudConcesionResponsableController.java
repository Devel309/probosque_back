package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionResponsableService;

import java.util.Arrays;


@RestController
@RequestMapping(Urls.responsableSolicitudConcesion.BASE)
public class SolicitudConcesionResponsableController extends AbstractRestController {
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionResponsableController.class);

    @Autowired
    private SolicitudConcesionResponsableService solicitudConcesionResponsableService;

    @PostMapping(path = "/obtenerSolicitudConcesionResponsable")
    @ApiOperation(value = "obtener Solicitud Concesion Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerSolicitudConcesionResponsable(@RequestBody SolicitudConcesionResponsableDto request) {
        log.info("SolicitudConcesionResponsable - obtenerSolicitudConcesionResponsable", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionResponsableService.obtenerSolicitudConcesionResponsable(request);
            log.info("SolicitudConcesionResponsable - obtenerSolicitudConcesionResponsable", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - listarSolicitudConcesion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudConcesionResponsable")
    @ApiOperation(value = "Registrar  Solicitud Concesión Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudConcesion(@RequestBody SolicitudConcesionResponsableDto request){
        log.info("SolicitudConcesionResponsable - registrarSolicitudConcesionResponsable",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionResponsableService.registrarSolicitudConcesionResponsable(request);
            log.info("SolicitudConcesionResponsable - registrarSolicitudConcesionResponsable","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionResponsable - registrarSolicitudConcesionResponsable","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarSolicitudConcesionResponsable")
    @ApiOperation(value = "Actualizar  Solicitud Concesión Responsable" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudConcesionResponsable(@RequestBody SolicitudConcesionResponsableDto request){
        log.info("SolicitudConcesionResponsable - actualizarSolicitudConcesionResponsable",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionResponsableService.actualizarSolicitudConcesionResponsable(request);
            log.info("SolicitudConcesionResponsable - actualizarSolicitudConcesionResponsable","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionResponsable - actualizarSolicitudConcesionResponsable","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }



}
