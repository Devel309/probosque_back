package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.CapacitacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CapacitacionService;

import java.util.List;

@RestController
@RequestMapping(Urls.capacitacion.BASE)
public class CapacitacionController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CapacitacionController.class);

    @Autowired
    private CapacitacionService capacitacionService;

    @PostMapping(path = "/registrarCapacitacion")
    @ApiOperation(value = "registrarCapacitacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCapacitacion(@RequestBody CapacitacionEntity capacitacionEntity){
        log.info("ProteccionBosque - RegistrarCapacitacion",capacitacionEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = capacitacionService.RegistrarCapacitacion(capacitacionEntity);
            log.info("ProteccionBosque - RegistrarCapacitacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarCapacitacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarCapacitacion")
    @ApiOperation(value = "actualizarCapacitacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarCapacitacion(@RequestBody CapacitacionEntity capacitacionEntity){
        log.info("ProteccionBosque - ActualizarCapacitacion",capacitacionEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = capacitacionService.ActualizarCapacitacion(capacitacionEntity);
            log.info("ProteccionBosque - ActualizarCapacitacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProteccionBosque - ActualizarCapacitacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarCapacitacion")
    @ApiOperation(value = "eliminarCapacitacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarCapacitacion(@RequestBody CapacitacionEntity capacitacionEntity){
        log.info("ProteccionBosque - eliminarCapacitacion",capacitacionEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = capacitacionService.EliminarCapacitacion(capacitacionEntity);
            log.info("ProteccionBosque - eliminarCapacitacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProteccionBosque - eliminarCapacitacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarCapacitacion")
    @ApiOperation(value = "listarCapacitacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarCapacitacion(@RequestBody CapacitacionDto capacitacionEntity){
        try{
            return  new ResponseEntity (true, "ok", capacitacionService.ListarCapacitacion(capacitacionEntity));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/registrarCapacitacionDetalle")
    @ApiOperation(value = "registrarCapacitacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCapacitacionDetalle(@RequestBody List<CapacitacionDto> list){
        log.info("ProteccionBosque - RegistrarCapacitacionDetalle",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = capacitacionService.RegistrarCapacitacionDetalle(list);
            log.info("ProteccionBosque - RegistrarCapacitacionDetalle","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarCapacitacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/eliminarCapacitacionDetalle")
    @ApiOperation(value = "eliminarCapacitacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarCapacitacionDetalle(@RequestBody CapacitacionDetalleEntity capacitacionDetalleEntity){
        log.info("ProteccionBosque - eliminarCapacitacionDetalle",capacitacionDetalleEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = capacitacionService.EliminarCapacitacionDetalle(capacitacionDetalleEntity);
            log.info("ProteccionBosque - eliminarCapacitacionDetalle","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProteccionBosque - eliminarCapacitacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarCapacitacionDetalle")
    @ApiOperation(value = "listarCapacitacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarCapacitacionDetalle(@RequestBody CapacitacionEntity capacitacionEntity){
        log.info("ProteccionBosque - listarCapacitacionDetalle",capacitacionEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = capacitacionService.ListarCapacitacionDetalle(capacitacionEntity);
            log.info("ProteccionBosque - listarCapacitacionDetalle","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ConfiguracionProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarCapacitacionCabeceraDetalle")
    @ApiOperation(value = "Listar Capacitacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<CapacitacionDto>> listarCapacitacionCabeceraDetalle(
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam("codTipoCapacitacion") String codTipoCapacitacion) {
        try {
            return capacitacionService.ListarCapacitacionCabeceraDetalle(idPlanManejo,codTipoCapacitacion);
        } catch (Exception ex) {

            ResultClassEntity<List<CapacitacionDto>> result = new ResultClassEntity<>();
            log.error("Capacitacion - listarCapacitacion", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }
}
