package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.AreaSistemaPlantacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion.EspecieForestalSistemaPlantacionEspecieDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AreaSistemaPlantacionServiceService;


@RestController
@RequestMapping(Urls.AreaSistemaPlantacion.BASE)
public class AreaSistemaPlantacionController extends AbstractRestController{
    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Servicio donde se generan las AreaSistemaPlantacion}
     *
     */
    @Autowired
    private AreaSistemaPlantacionServiceService areaSistemaPlantacionServiceService;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AreaSistemaPlantacionController.class);

    /**
     * @autor: 
     * @modificado:
     * @descripción:
     * @param:
     */
    
    @PostMapping(path = "/registrarAreaSistemaPlantacion")
    @ApiOperation(value = "Registrar AreaSistemaPlantacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacion(@RequestBody List<AreaSistemaPlantacionDto> data){
        log.info("AreaSistemaPlantacion - registrarAreaSistemaPlantacion",data.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = areaSistemaPlantacionServiceService.registrarAreaSistemaPlantacion(data);
            log.info("AreaSistemaPlantacion - registrarAreaSistemaPlantacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("AreaSistemaPlantacion - registrarAreaSistemaPlantacion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: 
     * @modificado:
     * @descripción: 
     * @param:
     */
    @PostMapping(path = "/listarAreaSistemaPlantacion")
    @ApiOperation(value = "Listar AreaSistemaPlantacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarAreaSistemaPlantacion(@RequestBody AreaSistemaPlantacionDto dto){
        log.info("AreaSistemaPlantacion - listarAreaSistemaPlantacion",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = areaSistemaPlantacionServiceService.listarAreaSistemaPlantacion(dto);
            log.info("AreaSistemaPlantacion - listarAreaSistemaPlantacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("AreaSistemaPlantacion - listarAreaSistemaPlantacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  


    @PostMapping(path = "/eliminarAreaSistemaPlantacion")
    @ApiOperation(value = "eliminar AreaSistemaPlantacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarAreaSistemaPlantacion(@RequestBody AreaSistemaPlantacionDto dto){
        log.info("AreaSistemaPlantacion - eliminarAreaSistemaPlantacion",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = areaSistemaPlantacionServiceService.eliminarAreaSistemaPlantacion(dto);
            log.info("AreaSistemaPlantacion - eliminarAreaSistemaPlantacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("AreaSistemaPlantacion - eliminarAreaSistemaPlantacion","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarEspecieForestalSistemaPlantacionEspecie")
    @ApiOperation(value = "Listar EspecieForestalSistemaPlantacionEspecie", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEspecieForestalSistemaPlantacionEspecie(@RequestBody EspecieForestalSistemaPlantacionEspecieDto dto){
        log.info("AreaSistemaPlantacion - eliminarAreaSistemaPlantacion",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = areaSistemaPlantacionServiceService.ListarEspecieForestalSistemaPlantacionEspecie(dto);
            log.info("AreaSistemaPlantacion - listarEspecieForestalSistemaPlantacionEspecie","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("AreaSistemaPlantacion - listarEspecieForestalSistemaPlantacionEspecie","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/EliminarSistemaPlantacionEspecieLista")
    @ApiOperation(value = "Eliminar por lista SistemaPlantacionEspecie", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSistemaPlantacionEspecieLista(@RequestBody List<EspecieForestalSistemaPlantacionEspecieDto> dto){
        log.info("AreaSistemaPlantacion - eliminarSistemaPlantacionEspecieLista",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = areaSistemaPlantacionServiceService.eliminarSistemaPlantacionEspecieLista(dto);
            log.info("AreaSistemaPlantacion - eliminarSistemaPlantacionEspecieLista","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("AreaSistemaPlantacion - eliminarSistemaPlantacionEspecieLista","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/RegistrarSistemaPlantacionEspecieLista")
    @ApiOperation(value = "Guardar por lista SistemaPlantacionEspecie", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSistemaPlantacionEspecieLista(@RequestBody List<EspecieForestalSistemaPlantacionEspecieDto> dto){
        log.info("AreaSistemaPlantacion - registrarSistemaPlantacionEspecieLista",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = areaSistemaPlantacionServiceService.registrarSistemaPlantacionEspecie(dto);
            log.info("AreaSistemaPlantacion - registrarSistemaPlantacionEspecieLista","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("AreaSistemaPlantacion - registrarSistemaPlantacionEspecieLista","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
