package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;

@RestController
@RequestMapping(Urls.email.BASE)
public class EmailController {
    /**
     * @autor: JaquelineDB [21-06-2021]
     * @modificado:
     * @descripción: {Servicio que envia email}
     *
     */

    @Autowired
    private EmailService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EmailController.class);

    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Envia un correo}
     * @param:EmailModel
     */
    @PostMapping(value = "/send")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public boolean SendEmail(@RequestBody EmailEntity emailModel) {
        boolean EnvioCorreo = false;
        try {
            EnvioCorreo = service.sendEmail(emailModel);
            if (EnvioCorreo == true) {
                log.info("EmailController - SendEmail", "Se envío correo");
                return EnvioCorreo;
            } else {
                log.warn("EmailApi - SendEmail", "No se envío correo");
                return EnvioCorreo;
            }
        } catch (Exception Ex) {
            log.error("EmailApi - SendEmail", "Ocurrió un error al enviar correo en: " + Ex.toString());
        }
        return EnvioCorreo;
    }


    /**
     * @autor: Rafael Azaña [22-12-2021]
     * @modificado:
     * @descripción: {Envia un correo}
     * @param:EmailModel
     */

    @PostMapping(value = "/enviarValidacionPermisoForestal")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public boolean SendCorreo(@RequestBody EvaluacionPermisoForestalDto evaluacionPermisoForestalDto) {
        boolean EnvioCorreo = false;
        try {
            EnvioCorreo = service.sendCorreo(evaluacionPermisoForestalDto);
            if (EnvioCorreo == true) {
                log.info("EmailController - SendEmail", "Se envío correo");
                return EnvioCorreo;
            } else {
                log.warn("EmailApi - SendEmail", "No se envío correo");
                return EnvioCorreo;
            }
        } catch (Exception Ex) {
            log.error("EmailApi - SendEmail", "Ocurrió un error al enviar correo en: " + Ex.toString());
        }
        return EnvioCorreo;
    }
}
