package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalGeometriaEntity;

import org.springframework.http.ResponseEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalGeometriaService;

@RestController
@RequestMapping(Urls.solicitudBosqueLocalGeometria.BASE)
public class SolicitudBosqueLocalGeometriaController {

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudBosqueLocalGeometriaController.class);
    @Autowired
    SolicitudBosqueLocalGeometriaService service;

    @PostMapping(path = "/registrarSolicitudBosqueLocalGeometria")
    @ApiOperation(value = "registrarSolicitudBosqueLocalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudBosqueLocalGeometria(
            @RequestBody List<SolicitudBosqueLocalGeometriaEntity> request) {
        log.info("SolicitudBosqueLocalGeometriaController - registrarSolicitudBosqueLocalGeometria",
                request.toString());
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            result = service.registrarSolicitudBosqueLocalGeometria(request);

            if (result.getSuccess()) {
                log.info("SolicitudBosqueLocalGeometriaController - registrarSolicitudBosqueLocalGeometria",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalGeometriaController - registrarSolicitudBosqueLocalGeometria",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerSolicitudBosqueLocalGeometria")
    @ApiOperation(value = "obtenerSolicitudBosqueLocalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerSolicitudBosqueLocalGeometria(@RequestBody SolicitudBosqueLocalGeometriaEntity item) {
        log.info("SolicitudBosqueLocalGeometriaController - obtenerSolicitudBosqueLocalGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = null;//service.obtenerSolicitudBosqueLocalGeometria(item);

            if (result.getSuccess()) {
                log.info("SolicitudBosqueLocalGeometriaController - obtenerSolicitudBosqueLocalGeometria",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalGeometriaController - obtenerSolicitudBosqueLocalGeometria",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarSolicitudBosqueLocalGeometria")
    @ApiOperation(value = "listarSolicitudBosqueLocalGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudBosqueLocalGeometria(@RequestBody SolicitudBosqueLocalGeometriaEntity item) {
        log.info("SolicitudBosqueLocalGeometriaController - listarSolicitudBosqueLocalGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarSolicitudBosqueLocalGeometria(item);

            if (result.getSuccess()) {
                log.info("SolicitudBosqueLocalGeometriaController - listarSolicitudBosqueLocalGeometria",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalGeometriaController - listarSolicitudBosqueLocalGeometria",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolicitudBosqueLocalGeometria")
    @ApiOperation(value = "Actualizar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudBosqueLocalGeometria(@RequestBody List<SolicitudBosqueLocalGeometriaEntity> request) {
        log.info("SolicitudConcesionGeometriaController - actualizarSolicitudBosqueLocalGeometria", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarSolicitudBosqueLocalGeometria(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - actualizarSolicitudBosqueLocalGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - actualizarSolicitudBosqueLocalGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

    @PostMapping(path = "/eliminarSolicitudBosqueLocalGeometriaArchivo")
    @ApiOperation(value = "Eliminar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudBosqueLocalGeometriaArchivo(@RequestBody SolicitudBosqueLocalGeometriaEntity request) {
        log.info("SolicitudBosqueLocalGeometriaController - eliminarSolicitudBosqueLocalGeometriaArchivo", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarSolicitudBosqueLocalGeometriaArchivo(request);

            if (result.getSuccess()) {
                log.info("SolicitudBosqueLocalGeometriaController - eliminarSolicitudBosqueLocalGeometriaArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocalGeometriaController - eliminarSolicitudBosqueLocalGeometriaArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }
}
