package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AnexosPFDMService;
import pe.gob.serfor.mcsniffs.service.EmailService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Urls.anexopfdm.BASE)
public class AnexosPFDMController {
    /**
     * @autor: JaquelineDB [24-08-2021]
     * @modificado:
     * @descripción: {Repository de los anexos pfdm}
     *
     */
    @Autowired
    private AnexosPFDMService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    @Autowired
    private EmailService enviarcorreo;
    
    /**
     * @autor: JaquelineDB [24-08-2021]
     * @modificado:
     * @descripción: {Generar el anexo 1}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/generarAnexo1PFDM")
    @ApiOperation(value = "generar Anexo1 PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> generarAnexo1PFDM(@RequestBody AnexosPFDMResquestEntity filtro){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = service.generarAnexo1PFDM(filtro);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("anexo - generarAnexo1PFDM", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

}
