package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Resolucion.ResolucionResultadoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ResolucionService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(Urls.resolucion.BASE)
public class ResolucionController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ResolucionController.class);

    @Autowired
    private ResolucionService resolucionService;

    @PostMapping(path = "/listarResolucion")
    @ApiOperation(value = "listarResolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarResolucion(@RequestBody ResolucionDto param){
        log.info("Resolucion - ListarResolucion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = resolucionService.ListarResolucion(param);
            log.info("Resolucion - ListarResolucion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion - ListarResolucion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarResolucion")
    @ApiOperation(value = "registrarResolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarResolucion(@RequestBody ResolucionDto param, HttpServletRequest http){
        log.info("Resolucion - RegistrarResolucion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            String token= http.getHeader("Authorization");

            param.setToken(token);

            response = resolucionService.RegistrarResolucion(param);
            log.info("Resolucion - RegistrarResolucion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion -RegistrarResolucion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @PostMapping(path = "/registrarResolucionResultado")
    @ApiOperation(value = "registrarResolucionResultado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarResolucion(@RequestBody ResolucionResultadoDto param){
        log.info("Resolucion - RegistrarResolucionResultado",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = resolucionService.registrarResolucionResultado(param);
            log.info("Resolucion - RegistrarResolucionResultado","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion -RegistrarResolucionResultado","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarResolucion")
    @ApiOperation(value = "actualizarResolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarResolucion(@RequestBody ResolucionDto param){
        log.info("Resolucion - ActualizarResolucion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = resolucionService.ActualizarResolucion(param);
            log.info("Resolucion - ActualizarResolucion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion -ActualizarResolucion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarResolucionEstado")
    @ApiOperation(value = "actualizarResolucionEstado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarResolucionEstado(@RequestBody ResolucionDto param){
        log.info("Resolucion - ActualizarResolucionEstado",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = resolucionService.ActualizarResolucionEstado(param);
            log.info("Resolucion - ActualizarResolucionEstado","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion -ActualizarResolucionEstado","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @GetMapping(path = "/resolucionValidar/{idProcesoOferta}")
    @ApiOperation(value = "resolucionValidar" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity resolucionValidar(@PathVariable Integer idProcesoOferta){
        log.info("Resolucion - resolucionValidar",idProcesoOferta.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = resolucionService.resolucionValidar(idProcesoOferta);
            log.info("Resolucion - resolucionValidar","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion -resolucionValidar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/obtenerResolucion")
    @ApiOperation(value = "obtenerResolucion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerResolucion(@RequestBody ResolucionDto param){
        log.info("Resolucion - ObtenerResolucion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = resolucionService.ObtenerResolucion(param);
            log.info("Resolucion - ObtenerResolucion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Resolucion - ObtenerResolucion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
