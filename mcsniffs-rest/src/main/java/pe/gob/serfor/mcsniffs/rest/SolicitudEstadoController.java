package pe.gob.serfor.mcsniffs.rest;

 
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudEstadoService;

@RestController 
@RequestMapping(Urls.solicitudEstado.BASE) // AbstractRestController {
public class SolicitudEstadoController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudEstadoController.class);

    @Autowired
    private SolicitudEstadoService servicio;

    @GetMapping(path = "/ListaComboSolicitudEstado")
    @ApiOperation(value = "Lista Combo Solicitud Estado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
     public ResponseEntity<ResultEntity> ListaComboSolicitudEstado(HttpServletRequest request1){
        ResultEntity res=new ResultEntity();
        try{
            res=  servicio.ListaComboSolicitudEstado(null);
            return new ResponseEntity<>(res,HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage());
            res.setInnerException(e.getMessage());
            return new ResponseEntity<>(res ,HttpStatus.BAD_REQUEST);
        }
    }

}
