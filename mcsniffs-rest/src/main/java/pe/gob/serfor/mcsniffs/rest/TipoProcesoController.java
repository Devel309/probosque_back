package pe.gob.serfor.mcsniffs.rest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.TipoProcesoService;

@RestController
@RequestMapping(Urls.tipoProceso.BASE)
public class TipoProcesoController {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Controller de Tipo de proceso}
     *
     */
    @Autowired
    private TipoProcesoService tipoS;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    /**
     * @autor: JaquelineDB [09-06-2021]
     * @modificado:
     * @descripción: {Listar tipo de proceso}
     * @param:
     */
    @GetMapping(path = "/listarTipoProceso")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT")) 
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<TipoProcesoNivel1Entity>> listarTipoProceso(){
        ResultEntity<TipoProcesoNivel1Entity> lst_tipoproceso = new ResultEntity<TipoProcesoNivel1Entity>();
        try {
            lst_tipoproceso =tipoS.listarTipoProceso();
            return new ResponseEntity<>(lst_tipoproceso, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("TipoProcesoController - ListarTipoProceso", "Ocurrió un error al Listar en: "+Ex.getMessage());
            lst_tipoproceso.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_tipoproceso,HttpStatus.FORBIDDEN);
        }
    }
}
