package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEspecieGrupoEntity;

import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PlanManejoEspecieGrupoService;

import java.util.Arrays;

@RestController
@RequestMapping(Urls.planManejo.BASE)
public class PlanManejoEspecieGrupoController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionAreaController.class);

    @Autowired
    PlanManejoEspecieGrupoService servicePlanManejoEspecieGrupo;

    /**
     * @autor: Wilfredo Elias [04-03-2022]
     * @modificado:
     * @descripción: {Registrar Grupo Especie de Plan de Manejo}
     * @param:planManejoEspecieGrupoEntity
     */
    @PostMapping(path = "/registrarGrupoEspeciePlanManejo")
    @ApiOperation(value = "Registrar Grupo de Especie del Plan de Manejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarGrupoEspeciePlanManejo(@RequestBody PlanManejoEspecieGrupoEntity request){
        log.info("PlanManejoEspecieGrupo - registrarGrupoEspeciePlanManejo",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = servicePlanManejoEspecieGrupo.registrarPlanManejoEspecieGrupo(request);
            log.info("PlanManejoEspecieGrupo - registrarGrupoEspeciePlanManejo","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("PlanManejoEspecieGrupo - registrarGrupoEspeciePlanManejo","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

}
