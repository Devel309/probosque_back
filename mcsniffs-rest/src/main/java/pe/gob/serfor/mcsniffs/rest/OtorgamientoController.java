package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.OtorgamientoService;

import java.util.Arrays;

@RestController
@RequestMapping(Urls.otorgamiento.BASE)
public class OtorgamientoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProteccionBosqueController.class);

    @Autowired
    private OtorgamientoService otorgamientoService;

    /**
     * @autor:  Danny Nazario [08-12-2021]
     * @descripción: {Registrar Otorgamiento}
     * @param: List<OtorgamientoEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarOtorgamiento")
    @ApiOperation(value = "registrarOtorgamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOtorgamiento(@RequestBody OtorgamientoEntity obj) {
        log.info("Otorgamiento - registrarOtorgamiento", obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = otorgamientoService.registrarOtorgamiento(obj);
            log.info("Otorgamiento - registrarOtorgamiento", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Otorgamiento - registrarOtorgamiento", "Ocurrió un error : " + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [08-12-2021]
     * @descripción: {Listar Otorgamiento}
     * @param: List<OtorgamientoEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarOtorgamiento")
    @ApiOperation(value = "listarOtorgamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<OtorgamientoEntity>> listarOtorgamiento(@RequestBody OtorgamientoEntity filtro) {
        log.info("Otorgamiento - listarOtorgamiento", filtro.toString());
        ResultClassEntity<OtorgamientoEntity> response = new ResultClassEntity<>();
        try {
            response = otorgamientoService.listarOtorgamiento(filtro);
            log.info("Otorgamiento - listarOtorgamiento", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Otorgamiento - listarOtorgamiento", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [08-12-2021]
     * @descripción: {Eliminar Otorgamiento}
     * @param: List<OtorgamientoEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarOtorgamiento")
    @ApiOperation(value = "eliminarOtorgamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarOtorgamiento(@RequestBody OtorgamientoEntity obj) {
        log.info("Otorgamiento - eliminarOtorgamiento", obj.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = otorgamientoService.eliminarOtorgamiento(obj);
            log.info("Otorgamiento - eliminarOtorgamiento", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Otorgamiento - eliminarOtorgamiento", "Ocurrió un error : " + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Danny Nazario [23-12-2021]
     * @modificado: Danny Nazario [21-01-2022]
     * @descripción: {Listar Otorgamiento por filtro}
     * @param: OtorgamientoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroOtorgamiento")
    @ApiOperation(value = "listarPorFiltroOtorgamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<OtorgamientoEntity>> listarPorFiltroOtorgamiento(
            @RequestParam(required = false) String nroDocumento,
            @RequestParam(required = false) String nroRucEmpresa,
            @RequestParam(required = false) Integer idPlanManejo) {
        log.info("Otorgamiento - listarPorFiltroOtorgamiento");
        ResultClassEntity<OtorgamientoEntity> response = new ResultClassEntity<>();
        try {
            response = otorgamientoService.listarPorFiltroOtorgamiento(nroDocumento, nroRucEmpresa, idPlanManejo);
            log.info("Otorgamiento - listarPorFiltroOtorgamiento", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Otorgamiento - listarPorFiltroOtorgamiento", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
