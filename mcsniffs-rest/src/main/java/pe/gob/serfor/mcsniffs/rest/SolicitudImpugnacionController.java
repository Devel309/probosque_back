package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.*;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudImpugnacionEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudImpugnacionService;
import pe.gob.serfor.mcsniffs.entity.*;

@RestController
@RequestMapping(Urls.solicitudImpugnacion.BASE)

public class SolicitudImpugnacionController  extends AbstractRestController{
    @Autowired
    private SolicitudImpugnacionService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudImpugnacionController.class);


    /**
     * @autor: Miguel F. [25-08-2021]
     * @modificado:
     * @descripción: {listar Solicitudes Vencidas}
     *
     */
    @PostMapping(path = "/solicitudImpugnacionListar")
    @ApiOperation(value = "Solicitudes vencidas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<SolicitudImpugnacionEntity>> solicitudImpugnacionListar(@RequestBody SolicitudImpugnacionEntity param) {
        try {
            return service.solicitudImpugnacionListar(param);
        } catch (Exception ex) {
            ResultClassEntity<List<SolicitudImpugnacionEntity>> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - servicioListarSolicitudesVencidas",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [25-08-2021]
     * @modificado:
     * @descripción: {listar Solicitudes Vencidas}
     *
     */
    @PostMapping(path = "/listarArchivoSolicitudImpugnacion")
    @ApiOperation(value = "Solicitudes vencidas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> listarArchivoSolicitudImpugnacion(@RequestBody SolicitudImpugnacionArchivo2Entity param) {
        try {
            return service.listarArchivoSolicitudImpugnacion(param);
        } catch (Exception ex) {
            ResultClassEntity<List<SolicitudImpugnacionArchivo2Entity>> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - servicioListarSolicitudesVencidas",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [25-08-2021]
     * @modificado:
     * @descripción: {listar Solicitudes Vencidas}
     *
     */
    @GetMapping(path = "/listarSolicitudesVencidas")
    @ApiOperation(value = "Solicitudes vencidas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<Solicitud2Entity>> listarSolicitudesVencidas() {
        try {
            return service.listarSolicitudesVencidas();
        } catch (Exception ex) {
            ResultClassEntity<List<Solicitud2Entity>> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - servicioListarSolicitudesVencidas",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Miguel F. [18-08-2021]
     * @modificado:
     * @descripción: {obtener Solicitud Impugnacion Obtener}
     *
     */
    @PostMapping(path = "/solicitudImpugnacionActualizar")
    @ApiOperation(value = "Solicitud inpugnacion - actualizar estados", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> SolicitudImpugnacionActualizar(
            @RequestBody SolicitudImpugnacionEntity param) {
        try {
            return service.SolicitudImpugnacionActualizar(param);
        } catch (Exception ex) {
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - SolicitudImpugnacionActualizar",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /*

    @PostMapping(path = "/configurarAspectofisicofisiografia")
    @ApiOperation(value = "configurarAspectofisicofisiografia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity configurarAspectofisicofisiografia(@RequestBody AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity){
        log.info("AspectoFisicoBiologico - configurarAspectofisicofisiografia",aspectoFisicoFisiografiaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = aspectoFisicoFisiografiaService.ConfigurarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
            log.info("AspectoFisicoBiologico - ConfigurarAspectoFisicoFisiografia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ConfigurarAspectoFisicoFisiografia","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    * */
    @PostMapping(path = "/solicitudImpugnacionObtener")
    @ApiOperation(value = "solicitudImpugnacionObtener", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity solicitudImpugnacionObtener(@RequestBody SolicitudImpugnacionEntity solicitudImpugnacionEntity) {

        log.info("SolicitudImpugnacion - solicitudImpugnacionObtener",solicitudImpugnacionEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response =  service.SolicitudImpugnacionObtener(solicitudImpugnacionEntity);
            log.info("SolicitudImpugnacion - SolicitudImpugnacionObtener","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - SolicitudImpugnacionObtener","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Roberto M. [13-08-2021]
     * @modificado:
     * @descripción: {obtener lista de solicitudes vencidas}
     *
     */
    @PostMapping(path = "/listaSolicitudesVencidasObtener")
    @ApiOperation(value = "Lista Solicitudes Vencidas", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<SolicitudImpugnacionEntity>> ListaSolicitudesVencidasObtener() {
        try {
            return service.ListaSolicitudesVencidasObtener();
        } catch (Exception ex) {
            ResultClassEntity<List<SolicitudImpugnacionEntity>> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - ListaSolicitudesVencidasObtener",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Roberto M. [13-08-2021]
     * @modificado:
     * @descripción: {enviar correo de solicitudes vencidas && registra}
     *
     */
    @PostMapping(path = "/solicitudesVencidasEnviarCorreo")
    @ApiOperation(value = "Solicitudes Vencidas Enviar Correo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> SolicitudesVencidasEnviarCorreo( @RequestBody SolicitudImpugnacionEntity param) {
        try {
            // SolicitudImpugnacionEntity param = new SolicitudImpugnacionEntity(idSolicitudImpugnacion,
            //         codigoTipoSolicitud, asunto, descripcion, auditoria, idUsuarioRegistro);

            return service.SolicitudImpugnacionEnviarCorreo(param);
        } catch (Exception ex) {
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - SolicitudesVencidasEnviarCorreo",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

     /**
     * @autor: Roberto M. [13-08-2021]
     * @modificado:
     * @descripción: {enviar correo de solicitudes vencidas && registra}
     *
     */
    @PostMapping(path = "/solicitudImpugnacionRegistrarActualizar")
    @ApiOperation(value = "Solicitudes Vencidas Enviar Correo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> solicitudImpugnacionRegistrarActualizar( @RequestBody SolicitudImpugnacionEntity param) {
        try {
            // SolicitudImpugnacionEntity param = new SolicitudImpugnacionEntity(idSolicitudImpugnacion,
            //         codigoTipoSolicitud, asunto, descripcion, auditoria, idUsuarioRegistro);

            return service.SolicitudImpugnacionRegistrarActualizar(param);
        } catch (Exception ex) {
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("SolicitudImpugacionController - SolicitudesVencidasEnviarCorreo",
                    "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

}
