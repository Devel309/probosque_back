package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;

import java.util.List;

@RestController
@RequestMapping(Urls.monitoreo.BASE)
public class MonitoreoController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(MonitoreoController.class);


    @Autowired
    private pe.gob.serfor.mcsniffs.service.MonitoreoService monitoreoService;
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {registra el Monitoreo}
     * @param: MonitoreoEntity
     * @return: MonitoreoEntity<ResponseVO>
     */

    @PostMapping(path = "/registrarMonitoreoPgmfea")
    @ApiOperation(value = "registrarMonitoreoPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarMonitoreoPgmfea(@RequestBody List<MonitoreoPgmfeaEntity> listMonitoreo){
        log.info("Monitoreo - RegistrarMonitoreoPgmfea",listMonitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = monitoreoService.RegistrarMonitoreoPgmfea(listMonitoreo);
            log.info("Monitoreo - RegistrarMonitoreoPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("Monitoreo -RegistrarMonitoreoPgmfea","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar el Monitoreo Pgmfea}
     * @param: MonitoreoPgmfeaEntity
     * @return: MonitoreoPgmfeaEntity<ResponseVO>
     */

    @PostMapping(path = "/eliminarMonitoreoPgmfea")
    @ApiOperation(value = "eliminarMonitoreoPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarMonitoreoPgmfea(@RequestBody MonitoreoPgmfeaEntity monitoreo){
        log.info("Monitoreo - EliminarMonitoreoPgmfea",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = monitoreoService.EliminarMonitoreoPgmfea(monitoreo);
            log.info("Monitoreo - EliminarMonitoreoPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("Monitoreo -EliminarMonitoreoPgmfea","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Lista el Monitoreo Pgmfea}
     * @param: MonitoreoEntity
     * @return: MonitoreoPgmfeaEntity<ResponseVO>
     */

    @PostMapping(path = "/listarMonitoreoPgmfea")
    @ApiOperation(value = "listarMonitoreoPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarMonitoreoPgmfea(@RequestBody MonitoreoEntity monitoreo){
        log.info("Monitoreo - ListaroMonitoreoPgmfea",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = monitoreoService.ListarMonitoreoPgmfea(monitoreo);
            log.info("Monitoreo - ListaroMonitoreoPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("Monitoreo -ListaroMonitoreoPgmfea","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Obtener el Monitoreo Pgmfea}
     * @param: MonitoreoPgmfeaEntity
     * @return: MonitoreoPgmfeaEntity<ResponseVO>
     */

    @PostMapping(path = "/obtenerMonitoreoPgmfea")
    @ApiOperation(value = "obtenerMonitoreoPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerMonitoreoPgmfea(@RequestBody MonitoreoPgmfeaEntity monitoreo){
        log.info("Monitoreo - ObtenerMonitoreoPgmfea",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = monitoreoService.ObtenerMonitoreoPgmfea(monitoreo);
            log.info("Monitoreo - ObtenerMonitoreoPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("Monitoreo -ObtenerMonitoreoPgmfea","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Registra la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @PostMapping(path = "/registrarMonitoreo")
    @ApiOperation(value = "Registrar Monitoreo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity RegistrarMonitoreo(@RequestBody MonitoreoEntity monitoreo){
        log.info("Monitoreo - RegistrarMonitoreo",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = monitoreoService.RegistrarMonitoreo(monitoreo);
            log.info("Monitoreo - RegistrarMonitoreo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        }catch (Exception e){
            log.error("Monitoreo -RegistrarMonitoreo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarMonitoreoPOCC")
    @ApiOperation(value = "registrarMonitoreoPOCC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarMonitoreoPOCC(
            @RequestBody List<MonitoreoEntity> list) {
        log.info("registrarMonitoreoPOCC - registrarMonitoreoPOCC", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = monitoreoService.RegistrarMonitoreoPOCC(list);
            log.info("monitoreo - registrarMonitoreoPOCC", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("monitoreo -registrarMonitoreoPOCC", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/actualizarMonitoreoPOCC")
    @ApiOperation(value = "actualizarMonitoreoPOCC", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarMonitoreoPOCC(
            @RequestBody List<MonitoreoEntity> list) {
        log.info("registrarMonitoreoPOCC - actualizarMonitoreoPOCC", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = monitoreoService.ActualizarMonitoreoPOCC(list);
            log.info("monitoreo - actualizarMonitoreoPOCC", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("monitoreo -actualizarMonitoreoPOCC", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Actualiza la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @PostMapping(path = "/ActualizarMonitoreo")
    @ApiOperation(value = "Actualizar Monitoreo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity ActualizarMonitoreo(@RequestBody MonitoreoEntity monitoreo){
        log.info("Monitoreo - ActualizarMonitoreo",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = monitoreoService.ActualizarMonitoreo(monitoreo);
            log.info("Monitoreo - ActualizarMonitoreo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        }catch (Exception e){
            log.error("Monitoreo -ActualizarMonitoreo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Lista la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @PostMapping(path = "/listarMonitoreo")
    @ApiOperation(value = "Listar Monitoreo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarMonitoreo(@RequestBody MonitoreoEntity monitoreo){
        log.info("Monitoreo - listarMonitoreo",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = monitoreoService.listarMonitoreo(monitoreo);
            log.info("Monitoreo - listarMonitoreo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        }catch (Exception e){
            log.error("Monitoreo -listarMonitoreo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [22-11-2021]
     * @modificado:
     * @descripción: {Lista la cabecera y el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @PostMapping(path = "/listarMonitoreoPOCC")
    @ApiOperation(value = "Listar MonitoreoPOCC", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<MonitoreoEntity>> listarMonitoreoPOCC(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codigoMonitoreo) {
        try {
            return monitoreoService.listarMonitoreoPOCC(idPlanManejo,codigoMonitoreo);
        } catch (Exception ex) {

            ResultClassEntity<List<MonitoreoEntity>> result = new ResultClassEntity<>();
            log.error("MonitoreoController - listarMonitoreoPOCC", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }




     /**
     * @autor: Jaqueline DB [14-10-2021]
     * @modificado:
     * @descripción: {Eliminar el detalle de monitoreo}
     * @param:MonitoreoEntity
     */
    @PostMapping(path = "/EliminarDetalleMonitoreo")
    @ApiOperation(value = "Eliminar Monitoreo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarDetalleMonitoreo(@RequestBody MonitoreoDetalleEntity monitoreo){
        log.info("Monitoreo - EliminarDetalleMonitoreo",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = monitoreoService.EliminarDetalleMonitoreo(monitoreo);
            log.info("Monitoreo - EliminarDetalleMonitoreo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            
        }catch (Exception e){
            log.error("Monitoreo -EliminarDetalleMonitoreo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jaqueline DB [26-11-2021]
     * @modificado:
     * @descripción: {elimina el monitoreo}
     * @param:MonitoreoEntity
     */
    @PostMapping(path = "/EliminarMonitoreoCabera")
    @ApiOperation(value = "Eliminar Monitoreo cabera" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarMonitoreoCabera(@RequestBody MonitoreoEntity monitoreo){
        log.info("Monitoreo - EliminarMonitoreoCabera",monitoreo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = monitoreoService.EliminarMonitoreoCabera(monitoreo);
            log.info("Monitoreo - EliminarMonitoreoCabera","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            
        }catch (Exception e){
            log.error("Monitoreo -EliminarMonitoreoCabera","Ocurrio un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

