package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalComiteTecnicoEntity;

import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalComiteTecnicoService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.solicitudBosqueLocalComiteTecnico.BASE)
public class SolicitudBosqueLocalComiteTecnicoController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionAreaController.class);

    @Autowired
    SolicitudBosqueLocalComiteTecnicoService serviceSolicitudBosqueLocalComiteTecnico;

    @PostMapping(path = "/listarSolicitudBosqueLocalComiteTecnico")
    @ApiOperation(value = "listar Actividad de Solicitud Bosque Local comite Tecnico" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudBosqueLocalComiteTecnico(@RequestBody SolicitudBosqueLocalComiteTecnicoEntity request) {
        log.info("SolicitudBosqueLocal - listarSolicitudBosqueLocalComiteTecnico", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudBosqueLocalComiteTecnico.listarSolicitudBosqueLocalComiteTecnico(request);
            log.info("SolicitudBosqueLocal- listarSolicitudBosqueLocalComiteTecnico", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- listarSolicitudBosqueLocalComiteTecnico", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor: Wilfredo Elias [23-03-2022]
     * @modificado:
     * @descripción: {Registrar Comité Técnico de Solicitud Bosque Local}
     * @param: SolicitudBosqueLocalComiteTecnicoEntity
     */
    @PostMapping(path = "/registrarComiteTecnicoSolicitudBosqueLocal")
    @ApiOperation(value = "Registrar Comité Técnico de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarComiteTecnicoSolicitudBosqueLocal(@RequestBody List<SolicitudBosqueLocalComiteTecnicoEntity> request){
        log.info("SolicitudBosqueLocalComiteTecnico - registrarComiteTecnicoSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudBosqueLocalComiteTecnico.registrarComiteTecnicoSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocalComiteTecnico - registrarComiteTecnicoSolicitudBosqueLocal","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocalComiteTecnico - registrarComiteTecnicoSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarSolicitudBosqueLocalComiteTecnico")
    @ApiOperation(value = "Eliminar Comité Técnico de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudBosqueLocalComiteTecnico(@RequestBody SolicitudBosqueLocalComiteTecnicoEntity request){
        log.info("SolicitudBosqueLocalComiteTecnico - eliminarComiteTecnicoSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudBosqueLocalComiteTecnico.eliminarSolicitudBosqueLocalComiteTecnico(request);
            log.info("SolicitudBosqueLocalComiteTecnico - eliminarComiteTecnicoSolicitudBosqueLocal","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocalComiteTecnico - eliminarComiteTecnicoSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
