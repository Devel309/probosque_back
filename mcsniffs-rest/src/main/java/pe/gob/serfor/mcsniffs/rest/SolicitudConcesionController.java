package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ClonarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarResAdminFavDesfavDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAnexoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionService;

@RestController
@RequestMapping(Urls.solicitudConcesion.BASE)
public class SolicitudConcesionController extends AbstractRestController {
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionController.class);

    @Autowired
    private SolicitudConcesionService solicitudConcesionService;

    @PostMapping(path = "/listarSolicitudConcesionPorFiltro")
    @ApiOperation(value = "listar Solicitud Concesion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudConcesionPorFiltro(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - listarSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.listarSolicitudConcesionPorFiltro(request);
            log.info("SolicitudConcesionActividad - listarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - listarSolicitudConcesion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarSolicitudConcesion")
    @ApiOperation(value = "listar Solicitud Concesion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudConcesion(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - listarSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.listarSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - listarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - listarSolicitudConcesion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerInformacionSolicitanteSolicitado")
    @ApiOperation(value = "Obtener Informacion Solicitante y sobre lo solicitado - Anexo 1", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInformacionSolicitanteSolicitado(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesion - obtenerInformacionSolicitanteSolicitado", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(request);
            log.info("SolicitudConcesion - obtenerInformacionSolicitanteSolicitado", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - obtenerInformacionSolicitanteSolicitado",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudConcesion")
    @ApiOperation(value = "Registrar  Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudConcesion(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - registrarSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.registrarSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - registrarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - registrarSolicitudConcesion",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/registrarInformacionSolicitante")
    @ApiOperation(value = "Registrar Informacion Solicitante - Anexo 1", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInformacionSolicitante(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesion - registrarInformacionSolicitante", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.registrarInformacionSolicitante(request);
            log.info("SolicitudConcesion - registrarInformacionSolicitante", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - registrarInformacionSolicitante", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/registrarInformacionSobreSolicitado")
    @ApiOperation(value = "Registrar Informacion sobre lo solicitado - Anexo 1", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInformacionSobreSolicitado(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesion - registrarInformacionSobreSolicitado", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.registrarInformacionSobreSolicitado(request);
            log.info("SolicitudConcesion - registrarInformacionSobreSolicitado", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - registrarInformacionSobreSolicitado",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarSolicitudConcesion")
    @ApiOperation(value = "Actualizar  Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudConcesion(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - actualizarSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.actualizarSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - actualizarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - actualizarSolicitudConcesion",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarInformacionSolicitante")
    @ApiOperation(value = "Actualizar Informacion Solicitante - Anexo 1", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInformacionSolicitante(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesion - actualizarInformacionSolicitante", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.actualizarInformacionSolicitante(request);
            log.info("SolicitudConcesion - actualizarInformacionSolicitante", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - actualizarInformacionSolicitante", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarInformacionSobreSolicitado")
    @ApiOperation(value = "Actualizar Informacion sobre lo solicitado - Anexo 1", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInformacionSobreSolicitado(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesion - actualizarInformacionSobreSolicitado", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.actualizarInformacionSobreSolicitado(request);
            log.info("SolicitudConcesion - actualizarInformacionSobreSolicitado", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - actualizarInformacionSobreSolicitado",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarSolicitudConcesion")
    @ApiOperation(value = "Eliminar  Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudConcesion(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - eliminarSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.eliminarSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - eliminarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - eliminarSolicitudConcesion",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/obtenerInfoAdjuntosSolicitudConcesion")
    @ApiOperation(value = "Obtener Informacion Archivos Adjuntos de Solicitud Concesión", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoAdjuntosSolicitudConcesion(@RequestBody SolicitudConcesionArchivoEntity request) {
        log.info("SolicitudConcesion - obtenerInfoAdjuntosSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.obtenerArchivoSolicitudConcesion(request);
            log.info("SolicitudConcesion - obtenerInfoAdjuntosSolicitudConcesion", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - obtenerInfoAdjuntosSolicitudConcesion",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarInfoAdjuntosSolicitudConcesion")
    @ApiOperation(value = "Registrar Informacion Archivos Adjuntos de Solicitud Concesión", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInfoAdjuntosSolicitudConcesion(
            @RequestBody List<SolicitudConcesionArchivoEntity> request) {
        log.info("SolicitudConcesion - registrarInfoAdjuntosSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.registrarArchivosSolicitudConcesion(request);
            log.info("SolicitudConcesion - registrarInfoAdjuntosSolicitudConcesion", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - registrarInfoAdjuntosSolicitudConcesion",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarInfoAdjuntosSolicitudConcesion")
    @ApiOperation(value = "actualizar Informacion Archivos Adjuntos de Solicitud Concesión", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInfoAdjuntosSolicitudConcesion(
            @RequestBody SolicitudConcesionArchivoEntity request) {
        log.info("SolicitudConcesion - registrarInfoAdjuntosSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.actualizarArchivoSolicitudConcesion(request);
            log.info("SolicitudConcesion - actualizarInfoAdjuntosSolicitudConcesion",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - actualizarInfoAdjuntosSolicitudConcesion",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarInfoAdjuntoSolicitudConcesion")
    @ApiOperation(value = "Eiminar Archivo Adjunto de Solicitud Concesión", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarInfoAdjuntoSolicitudConcesion(@RequestBody SolicitudConcesionArchivoEntity request) {
        log.info("SolicitudConcesion - eliminarInfoAdjuntoSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.eliminarArchivoSolicitudConcesion(request);
            log.info("SolicitudConcesion - eliminarInfoAdjuntoSolicitudConcesion", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - eliminarInfoAdjuntoSolicitudConcesion",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/descargarSolicitudConcesionAnexo")
    @ApiOperation(value = "descargarSolicitudConcesionAnexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarSolicitudConcesionAnexo(
            @RequestBody DescargarSolicitudConcesionDto obj) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudConcesionService.descargarSolicitudConcesionAnexo(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudConcesion - descargarSolicitudConcesionAnexo",
                    "Ocurrió un error al obtener en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/descargarPlantillaCriteriosCalificacionPropuesta")
    @ApiOperation(value = "descargarPlantillaCriteriosCalificacionPropuesta", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaCriteriosCalificacionPropuesta(
            @RequestBody DescargarSolicitudConcesionDto obj) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudConcesionService.descargarPlantillaCriteriosCalificacionPropuesta(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudConcesion - descargarPlantillaCriteriosCalificacionPropuesta",
                    "Ocurrió un error al obtener en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/descargarPlantillaResolucionEvaluacionSolictud")
    @ApiOperation(value = "descargarPlantillaResolucionEvaluacionSolictud", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaResolucionEvaluacionSolictud(
            @RequestBody DescargarResAdminFavDesfavDto obj) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudConcesionService.descargarPlantillaResolucionEvaluacionSolictud(obj);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudConcesion - descargarPlantillaResolucionEvaluacionSolicitud",
                    "Ocurrió un error al obtener en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarSolicitudConcesionAnexo")
    @ApiOperation(value = "listar Solicitud Concesion Anexo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudConcesionAnexo(@RequestBody SolicitudConcesionAnexoDto request) {
        log.info("SolicitudConcesion - listarSolicitudConcesionAnexo", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.listarSolicitudConcesionAnexo(request);
            log.info("SolicitudConcesion - listarSolicitudConcesionAnexo", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - listarSolicitudConcesionAnexo", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudConcesionAnexo")
    @ApiOperation(value = "registrarSolicitudConcesionAnexo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudConcesionAnexo(@RequestBody List<SolicitudConcesionAnexoDto> request) {
        log.info("SolicitudConcesion - registrarSolicitudConcesionAnexo", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.registrarSolicitudConcesionAnexo(request);
            log.info("SolicitudConcesion - registrarSolicitudConcesionAnexo", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - registrarSolicitudConcesionAnexo", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/enviarSolicitudConcesion")
    @ApiOperation(value = "Enviar  Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity enviarSolicitudConcesion(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - enviarSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.enviarSolicitudConcesion(request);
            log.info("SolicitudConcesionActividad - enviarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - enviarSolicitudConcesion", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/enviarComunicado")
    @ApiOperation(value = "Enviar Comunicado Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity enviarComunicado(@RequestBody SolicitudConcesionDto request) {
        log.info("SolicitudConcesionActividad - enviarComunicado", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.enviarComunicado(request);
            log.info("SolicitudConcesionActividad - enviarComunicado", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - enviarComunicado", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping(path = "/descargarPlantillaManejo")
    @ApiOperation(value = "descargarPlantillaManejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaManejo() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudConcesionService.descargarPlantillaManejo();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudConcesion - descargarPlantillaManejo",
                    "Ocurrió un error al generar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(path = "/descargarPlantillaPresupuesto")
    @ApiOperation(value = "descargarPlantillaPresupuesto", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarPlantillaPresupuesto() {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudConcesionService.descargarPlantillaPresupuesto();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudConcesion - descargarPlantillaPresupuesto",
                    "Ocurrió un error al generar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/clonarSolicitudConcesion")
    @ApiOperation(value = "Clonar  Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity clonarSolicitudConcesion(@RequestBody ClonarSolicitudConcesionDto request) {
        log.info("SolicitudConcesion - ClonarSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionService.clonarSolicitudConcesion(request);
            log.info("SolicitudConcesion - clonarSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesion - clonarSolicitudConcesion", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/listarSolicitudConcesionCalificacionPorFiltro")
    @ApiOperation(value = "listar Solicitud Concesion Calificacion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudConcesionCalificacionPorFiltro(
            @RequestBody SolicitudConcesionCalificacionDto request) {
        log.info("listarSolicitudConcesionCalificacionPorFiltro - listarSolicitudConcesionCalificacionPorFiltro",
                request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionService.listarSolicitudConcesionCalificacionPorFiltro(request);
            log.info("listarSolicitudConcesionCalificacionPorFiltro - listarSolicitudConcesionCalificacionPorFiltro",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("listarSolicitudConcesionCalificacionPorFiltro - listarSolicitudConcesionCalificacionPorFiltro",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
