package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DescargarSolicitudPlantacionForestal.DescargarSolicitudPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoPDFSolPlantacionForestalService;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.ImpugnacionService;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;
import pe.gob.serfor.mcsniffs.service.SolPlantacionForestalService;

//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;

@RestController
@RequestMapping(Urls.solPlantacionForestal.BASE)
public class SolPlantacionForestalController extends AbstractRestController{

    @Autowired
    private SolPlantacionForestalService repo;

    // @Autowired
    // private SolPlantacionForestalAreaService repoAr;

    // @Autowired
    // private SolPlantacionForestalDetService repoDt;

    // @Autowired
    // private SolPlantacionForestalDetCoordenadaService repoDtCoord;

    // @Autowired
    // private SolPlantacionForestalAreaPlantadaService repoArPlant;

    // @Autowired
    // ArchivoService ArServ;

    // @Autowired
    // SolPlantacionForestalAnexoService AnexServ;

    // @Autowired
    // FileServerConexion fileServ;

    @Autowired
    private ArchivoService serArchivo;

    @Autowired
    private ImpugnacionService impugnacionService;
    
    @Autowired
    private ServicioExternoService serExt;

    @Autowired
    private EmailService serMail;
    
    @Autowired
    ArchivoPDFSolPlantacionForestalService PDFSolPlanForService;

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolPlantacionForestalController.class);

    @PostMapping(path = "/registrarSolicitudPlantacacion")
    @ApiOperation(value = "Registrar Solicitud Plantacacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registrarSolicitudPlantacacion(@RequestBody SolPlantacionForestalEntity request,
            HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolicitudPlantacacion", "inicio");
        ResultEntity res = new ResultEntity();
        try {
            res = repo.registroSolicitudPlantacionForestal(request);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolicitudPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registrarSolicitudPlantacacionArea")
    @ApiOperation(value = "Registrar Solicitud Plantacacion Area", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarSolicitudPlantacacionArea(
            @RequestBody SolPlantacionForestalAreaEntity request, HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolicitudPlantacacionArea", "inicio");
        ResultClassEntity res = new ResultClassEntity<>();
        try {

            res = repo.registroSolPlantacionForestalArea(request);

            log.info("Service - obtener: Proceso realizado correctamente");
            if (!res.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK);
            }

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolicitudPlantacacionArea",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registrarSolicitudPlantacacionAreaPlant")
    @ApiOperation(value = "Registrar Solicitud Plantacacion AreaPlant", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registrarSolicitudPlantacacionAreaPlant(
            @RequestBody List<SolPlantacionForestalAreaPlantadaEntity> request, HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolicitudPlantacacionAreaPlant", "inicio");
        ResultEntity res = new ResultEntity();
        try {

            res = repo.registroSolicitudPlantacionForestalAreaPlantada(request);

            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - AreaPlant",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registrarSolicitudPlantacacionDet")
    @ApiOperation(value = "Registrar Solicitud Plantacacion AreaPlant", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registrarSolicitudPlantacacionDet(
            @RequestBody List<SolPlantacionForestalDetalleEntity> request, HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolicitudPlantacacionDet", "inicio");
        ResultEntity res = new ResultEntity();
        try {

            res = repo.registroSolicitudPlantacionForestalDet(request);

            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolicitudPlantacacionDet",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registrarSolicitudPlantacacionDetCoord")
    @ApiOperation(value = "Registrar Solicitud Plantacacion AreaPlant", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registrarSolicitudPlantacacionDetCoord(
            @RequestBody List<SolPlantacionForestalDetCoordenadaEntity> request, HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolicitudPlantacacionDetCoord", "inicio");
        ResultEntity res = new ResultEntity();
        try {

            res = repo.registroSolicitudPlantacionForestalDetCoord(request);

            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolicitudPlantacacionDetCoord",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    // @PostMapping(path = "/registrarSolicitudPlantacacionAnexo" )
    @RequestMapping(value = "/registrarSolicitudPlantacacionAnexo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Registrar Solicitud Plantacacion AreaPlant", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registrarSolicitudPlantacacionAnexo(
            @RequestParam("file") MultipartFile file,
            @RequestParam(required = false) Integer id_archivo_solplantacionforest,
            @RequestParam(required = false) Integer id_solplantforest,
            @RequestParam(required = false) Integer id_archivo,
            @RequestParam(required = false) Integer id_tipodetalleseccion,
            @RequestParam(required = false) Integer id_tipodetalleanexo,
            @RequestParam(required = false) Integer idUsuario,
            @RequestParam(required = false) Integer idUsuarioEdicion,
            HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolicitudPlantacacionAnexo", "inicio");
        ResultEntity res = new ResultEntity();
        try {
            SolPlantacionForestalAnexoEntity anexo = new SolPlantacionForestalAnexoEntity();

            anexo.setIdArchivo(id_archivo);
            anexo.setDescripcion(file.getOriginalFilename());
            anexo.setIdUsuarioRegistro(idUsuario);
            anexo.setIdUsuarioModificacion(idUsuarioEdicion);
            anexo.setId_archivo_solplantacionforest(id_archivo_solplantacionforest);
            anexo.setId_solplantforest(id_solplantforest);
            anexo.setId_tipodetalleanexo(id_tipodetalleanexo);
            anexo.setId_tipodetalleseccion(id_tipodetalleseccion);

            res = repo.registroSolPlantacionForestalAnexo(anexo, file);

            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolicitudPlantacacionAnexo",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registroSolPlantacionForestalObservacion")
    @ApiOperation(value = "Registrar Observacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registroSolPlantacionForestalObservacion(
            @RequestBody List<SolPlantacionForestalObservacionEntity> request, HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registroSolPlantacionForestalObservacion", "inicio");
        ResultEntity res = new ResultEntity();
        try {

            res = repo.registroSolPlantacionForestalObservacion(request);

            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registroSolPlantacionForestalObservacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/obtenerSolicitudPlantacacion")
    @ApiOperation(value = "Obtener Solicitud Plantacacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerSolicitudPlantacacion(@RequestBody SolPlantacionForestalDto request,
            HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - obtenerSolicitudPlantacacion", "inicio");
        ResultClassEntity res = new ResultClassEntity();
        try {
            res = repo.obtenerSolicitudPlantacionForestal(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - obtenerSolicitudPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/clonarPlantacionForestal")
    @ApiOperation(value = "Registrar Solicitud Plantacacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity clonarPlantacionForestal(@RequestBody SolPlantacionForestalDto request,
                                                       HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - clonarPlantacionForestal", "inicio");
        ResultClassEntity res = new ResultClassEntity();
        try {
            res = repo.clonarPlantacionForestal(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - clonarPlantacionForestal",
                    "Ocurrió un error al registrar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/validarClonPlantacionForestal")
    @ApiOperation(value = "Registrar Solicitud Plantacacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity validarClonPlantacionForestal(@RequestBody SolPlantacionForestalDto request,
                                                   HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - validarClonPlantacionForestal", "inicio");
        ResultClassEntity res = new ResultClassEntity();
        try {
            res = repo.validarClonPlantacionForestal(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - validarClonPlantacionForestal",
                    "Ocurrió un error al registrar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }





    @PostMapping(path = "/obtenerInformacionTitular")
    @ApiOperation(value = "Obtener Informacion Titular - Anexo 1", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInformacionTitular(@RequestBody SolPlantacionForestalEntity request) {
        log.info("SolPlantacionForestal - obtenerInformacionTitular", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.obtenerInformacionTitular(request);
            log.info("SolPlantacionForestal - obtenerInformacionTitular", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestal - obtenerInformacionTitular",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerSolicitudPlantacacionAreaPant")
    @ApiOperation(value = "Obtener  AreaPant", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> obtenerSolicitudPlantacacionAreaPant(
            @RequestBody SolPlantacionForestalAreaPlantadaEntity request, HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - obtenerSolicitudPlantacacionAreaPant", "inicio");
        ResultEntity<SolPlantacionForestalAreaPlantadaEntity> res = new ResultEntity<SolPlantacionForestalAreaPlantadaEntity>();
        try {
            res = repo.obtenerSolicitudPlantacionForestalAreaPlantada(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - obtenerSolicitudPlantacacionAreaPant",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/listarSolPlantacacionForestalArea")
    @ApiOperation(value = "listar solicitud plantacion forestal area", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> obtenerSolicitudPlantacacionDet(
            @RequestBody SolPlantacionForestalAreaEntity request, HttpServletRequest request1) {
        ResultEntity<SolPlantacionForestalAreaEntity> res = new ResultEntity<SolPlantacionForestalAreaEntity>();
        try {
            res = repo.listarSolPlantacionForestalArea(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("listarSolPlantacionForestalArea - obtenerSolicitudPlantacacionDet",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/listarSolicitudPlantacionForestal")
    @ApiOperation(value = "listar solicitud plantacion forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> obtenerSolicitudPlantacacionDetasd(
            @RequestBody SolPlantacionForestalEntity request) {

                ResultClassEntity<SolPlantacionForestalEntity> res = new ResultClassEntity<SolPlantacionForestalEntity>();
        try {
            res = repo.listarSolicitudPlantacionForestal(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("listarSolicitudPlantacionForestal - obtenerSolicitudPlantacacionDet",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/obtenerSolicitudPlantacacionDetCoord")
    @ApiOperation(value = "Obtener Det coordenada", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> obtenerSolicitudPlantacacionDetCoord(
            @RequestBody SolPlantacionForestalDetCoordenadaEntity request, HttpServletRequest request1) {
        ResultEntity<SolPlantacionForestalDetCoordenadaEntity> res = new ResultEntity<SolPlantacionForestalDetCoordenadaEntity>();
        try {
            res = repo.obtenerSolicitudPlantacionForestalDetCoord(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - obtenerSolicitudPlantacacionDetCoord",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/obtenerSolicitudPlantacacionAnexo")
    @ApiOperation(value = "obtener Anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> obtenerSolicitudPlantacacionAnexo(
            @RequestBody SolPlantacionForestalAnexoEntity request, HttpServletRequest request1) {
        ResultEntity<SolPlantacionForestalAnexoEntity> res = new ResultEntity<SolPlantacionForestalAnexoEntity>();
        try {
            res = repo.obtenerSolPlantacionForestalAnexo(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - obtenerSolicitudPlantacacionDetCoord",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/obtenerSolPlantacionForestalObservacion")
    @ApiOperation(value = "obtener observacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> obtenerSolPlantacionForestalObservacion(
            @RequestBody SolPlantacionForestalObservacionEntity request, HttpServletRequest request1) {
        ResultEntity<SolPlantacionForestalObservacionEntity> res = new ResultEntity<SolPlantacionForestalObservacionEntity>();
        try {
            res = repo.obtenerSolPlantacionForestalObservacion(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - obtenerSolPlantacionForestalObservacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/listarBandejaSolicitudPlantacionForestal")
    @ApiOperation(value = "listar Bandeja Solicitud Plantacion Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarBandejaSolicitudPlantacionForestal(
            @RequestBody SolPlantacionForestalDto request) {
            ResultClassEntity response = new ResultClassEntity();
            try {
                response = repo.listarBandejaSolicitudPlantacionForestal(request);
                log.info("SolPlantacionForestal - obtenerInformacionTitular", "Proceso realizado correctamente");
                if (response.getSuccess()) {
                    return new ResponseEntity<>(response, HttpStatus.OK);

                } else {
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }
            } catch (Exception e) {
                log.error("SolPlantacionForestal - obtenerInformacionTitular",
                        "Ocurrió un error : " + e.getMessage());
                response.setSuccess(Constantes.STATUS_ERROR);
                response.setMessage(Constantes.MESSAGE_ERROR_500);
                response.setStackTrace(Arrays.toString(e.getStackTrace()));
                response.setMessageExeption(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }

    @PostMapping(path = "/listarTipoComboSistemaPlantacionForestal")
    @ApiOperation(value = "listar TipoCombo Sistema Plantacion Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> listarTipoComboSistemaPlantacionForestal(HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {
            res = repo.listarTipoComboSistemaPlantacionForestal(null);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - listaSistemaPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/RegistrarSolicitudPlantacionForestalEstado")
    @ApiOperation(value = "listar TipoCombo Sistema Plantacion Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> RegistrarSolicitudPlantacionForestalEstado(
            @RequestBody SolPlantacionForestalEstadoEntity request, HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {

            res = repo.RegistrarSolicitudPlantacionForestalEstado(null);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - listaSistemaPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/actualizarInformacionTitular")
    @ApiOperation(value = "Actualiar Informacion Titular - Anexo 1", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInformacionTitular(@RequestBody SolPlantacionForestalEntity request) {
        log.info("SolPlantacionForestal - actualizarInformacionTitular", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.actualizarInformacionTitular(request);
            log.info("SolPlantacionForestal - actualizarInformacionTitular", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestal - actualizarInformacionTitular",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/EliminarSistemaPlantacionForestal")
    @ApiOperation(value = "Sistema Plantacion Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarSistemaPlantacionForestal(
            @RequestBody SolPlantacionForestalEntity request, HttpServletRequest request1) {
        log.info("InfBasicaAerea - EliminarSistemaPlantacionForestal",request.toString());
        ResponseEntity result = null;
        ResultClassEntity response = new ResultClassEntity<>();
        try {

            response = repo.eliminarSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestalController - EliminarSistemaPlantacionForestal",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.OK);
            } else {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - EliminarSistemaPlantacionForestal",
                    "Ocurrió un error al eliminar en: " + Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarSistemaPlantacionForestalDet")
    @ApiOperation(value = "Eliminar detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarSistemaPlantacionForestalDet(
            @RequestBody SolPlantacionForestalDetalleEntity request, HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {

            res = repo.eliminarSolicitudPlantacionForestalDet(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - listaSistemaPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarSistemaPlantacionForestalAreaPlant")
    @ApiOperation(value = "Eliminar Area Plantada", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarSistemaPlantacionForestalAreaPlant(
            @RequestBody SolPlantacionForestalAreaPlantadaEntity request, HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {

            res = repo.eliminarSolicitudPlantacionForestalAreaPlantada(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - listaSistemaPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarSistemaPlantacionForestalDetCoord")
    @ApiOperation(value = "Eliminar detalle coordenada", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarSistemaPlantacionForestalDetCoord(
            @RequestBody SolPlantacionForestalDetCoordenadaEntity request, HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {

            res = repo.eliminarSolicitudPlantacionForestalDetCoord(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - listaSistemaPlantacacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarSistemaPlantacionForestalAnexo")
    @ApiOperation(value = "Eliminar  Anexo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarSistemaPlantacionForestalAnexo(
            @RequestBody SolPlantacionForestalAnexoEntity request, HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {

            res = repo.eliminarSolPlantacionForestalAnexo(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - EliminarSistemaPlantacionForestalAnexo",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/eliminarSolPlantacionForestalObservacion")
    @ApiOperation(value = "Eliminar  Observacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> eliminarSolPlantacionForestalObservacion(
            @RequestBody SolPlantacionForestalObservacionEntity request, HttpServletRequest request1) {
        ResultEntity res = new ResultEntity();
        try {

            res = repo.eliminarSolPlantacionForestalObservacion(request);

            // if(res.getIsSuccess()){
            return new ResponseEntity<>(res, HttpStatus.OK);
            /*
             * }else{
             * return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST );
             * }
             */
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - eliminarSolPlantacionForestalObservacion",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/obtenerInfoAdjuntosSolPlantacionForestal")
    @ApiOperation(value = "Obtener Informacion Archivos Adjuntos de Solicitud Plantación Forestal", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoAdjuntosSolPlantacionForestal(
            @RequestBody SolPlantacionForestalArchivoEntity request) {
        log.info("SolPlantacionForestalController - obtenerInfoAdjuntosSolPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.obtenerArchivoSolPlantacionForestal(request);
            log.info("SolPlantacionForestalController - obtenerArchivoSolPlantacionForestal",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalController - obtenerInfoAdjuntosSolPlantacionForestal",
                    "Ocurrió un error : " + e.getMessage());

            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarInfoAdjuntosSolPlantacionForestal")
    @ApiOperation(value = "Registrar Informacion Archivos Adjuntos de Solicitud Plantación Forestal", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInfoAdjuntosSolPlantacionForestal(
            @RequestBody List<SolPlantacionForestalArchivoEntity> request) {
        log.info("SolPlantacionForestalController - registrarInfoAdjuntosSolPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.registrarArchivosSolPlantacionForestal(request);
            log.info("SolPlantacionForestalController - registrarArchivosSolPlantacionForestal",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalController - registrarInfoAdjuntosSolPlantacionForestal",
                    "Ocurrió un error : " + e.getMessage());

            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarInfoAdjuntosSolPlantacionForestal")
    @ApiOperation(value = "actualizar Informacion Archivos Adjuntos de Solicitud Plantacion Forestal", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInfoAdjuntosSolPlantacionForestal(
            @RequestBody SolPlantacionForestalArchivoEntity request) {
        log.info("SolPlantacionForestalController - actualizarInfoAdjuntosSolPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.actualizarArchivoSolPlantacionForestal(request);
            log.info("SolPlantacionForestalController - actualizarArchivoSolPlantacionForestal",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalController - actualizarInfoAdjuntosSolPlantacionForestal",
                    "Ocurrió un error : " + e.getMessage());

            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarInfoAdjuntoSolPlantacionForestal")
    @ApiOperation(value = "Eiminar Archivo Adjunto de Solicitud Plantación Forestal", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarInfoAdjuntoSolPlantacionForestal(
            @RequestBody SolPlantacionForestalArchivoEntity request) {
        log.info("SolPlantacionForestalController - eliminarInfoAdjuntoSolPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.eliminarArchivoSolPlantacionForestal(request);
            log.info("SolPlantacionForestalController - eliminarArchivoSolPlantacionForestal",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalController - eliminarInfoAdjuntoSolPlantacionForestal",
                    "Ocurrió un error : " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolPlantacacionForestal")
    @ApiOperation(value = "Registrar Solicitud Plantacacion Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarSolPlantacacionForestal(
            @RequestBody SolicitudPlantacionForestalEntity request,
            HttpServletRequest request1) {
        log.info("SolPlantacionForestalController - registrarSolPlantacacionForestal", "inicio");
        ResultClassEntity res = new ResultClassEntity();
        try {
            res = repo.registrarSolPlantacacionForestal(request);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolPlantacacionForestal",
                    "Ocurrió un error al Listar en: " + Ex.getMessage());
            res.setMessage(Ex.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/actualizarSolicitudPlantacionForestal")
    @ApiOperation(value = "Actualiar solicitud", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudPlantacionForestal(@RequestBody SolPlantacionForestalEntity request) {
        log.info("SolPlantacionForestal - actualizarSolicitudPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.actualizarSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestal - actualizarSolicitudPlantacionForestal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestal - actualizarSolicitudPlantacionForestal",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/enviarSolicitud")
    @ApiOperation(value = "Enviar  Solicitud Concesión", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity enviarSolicitud(@RequestBody SolPlantacionForestalDto request) {
        log.info("SolicitudConcesionActividad - enviarSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = repo.enviarSolicitud(request);
            log.info("SolPlantacionForestal - enviarSolicitud\n", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestal -enviarSolicitud\n", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping(path = "/obtenerEspeciesSistemaPlantacion/{idSolForestPlant}")
    @ApiOperation(value = "Obtener especies Sistema de Plantanación", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerEspeciesSistemaPlantacion(@PathVariable("idSolForestPlant") Integer idSolForestPlant) {
        log.info("SolPlantacionForestalController - listarEspeciesSistemaPlantacion", idSolForestPlant.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = repo.listarEspeciesSistemaPlantacion(idSolForestPlant);
            log.info("SolPlantacionForestalController - listarEspeciesSistemaPlantacion\n", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalController - listarEspeciesSistemaPlantacion\n", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/registrarSolPlantacacionForestalDetalle")
    @ApiOperation(value = "Registrar Detalle Plantacacion Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarSolPlantacacionForestalDetalle(
            @RequestBody List<SolicitudPlantacionForestalDetalleEntity> request) {

        log.info("SolPlantacionForestalController - registrarSolPlantacacionForestalDetalle", "inicio");

        ResultClassEntity response = new ResultClassEntity();

        try {
            response = repo.registrarSolPlantacacionForestalDetalle(request);
            
            log.info("SolPlantacionForestalController - registrarSolPlantacacionForestalDetalle\n", "Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolPlantacacionForestalDetalle",
                    "Ocurrió un error al registrar en: " + Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/notificarSolicitante")
    @ApiOperation(value = "notificarSolicitante" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarResolucion(@RequestBody SolPlantacionForestalEntity param, HttpServletRequest http){
        log.info("SolPlantacionForestalController - notificarSolicitante",param.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            String token= http.getHeader("Authorization");

            param.setToken(token);

            response = repo.notificarSolicitante(param);

            log.info("SolPlantacionForestalController - notificarSolicitante","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolPlantacionForestalController - notificarSolicitante","Ocurrió un error :"+ e.getMessage());
            
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/notificarPasSolicitante")
    @ApiOperation(value = "notificarPasSolicitante" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity notificarPasSolicitante(@RequestBody SolPlantacionForestalEntity param, HttpServletRequest http){
        log.info("SolPlantacionForestalController - notificarPasSolicitante",param.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            String token= http.getHeader("Authorization");

            param.setToken(token);

            response = repo.notificarPasSolicitante(param);

            log.info("SolPlantacionForestalController - notificarPasSolicitante","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolPlantacionForestalController - notificarPasSolicitante","Ocurrió un error :"+ e.getMessage());
            
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //@Value("${spring.urlSeguridad}")
    private String urlSeguridad;
    @PostMapping(path = "/notificarPassSerfor")
    @ApiOperation(value = "notificarPassSerfor", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity notificarPassSerfor(@RequestBody SolPlantacionForestalEntity param, HttpServletRequest http
    ) {
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = new pe.gob.serfor.mcsniffs.entity.ResponseEntity();
        ResultClassEntity archivoResponse= new ResultClassEntity();
        ResultClassEntity archivoResponse2 = new ResultClassEntity();
        ResultClassEntity response = new ResultClassEntity();
        try{
            String token= http.getHeader("Authorization");

            param.setToken(token);

            response = repo.notificarPassSerfor(param);

            log.info("SolPlantacionForestalController - notificarPassSerfor","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);


        } catch (Exception e) {
            log.error("Impugnacion -RegistrarImpugnacion", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarAreaBloque")
    @ApiOperation(value = "Registrar bloques de area", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarAreaBloque(@RequestBody AreaBloqueEntity request) {
        log.info("SolPlantacionForestalAreaController - registrarAreaBloque", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.registrarAreaBloque(request);
            log.info("SolPlantacionForestalAreaController - registrarAreaBloque",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalAreaController - registrarAreaBloque", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarAreaBloque")
    @ApiOperation(value = "Listart bloques de area", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarAreaBloque(@RequestBody AreaBloqueEntity request) {
        log.info("SolPlantacionForestalAreaController - listarAreaBloque", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.listarAreaBloque(request);
            log.info("SolPlantacionForestalAreaController - listarAreaBloque",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalAreaController - listarAreaBloque", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/descargarSolicitudPlantacionesForestales")
    @ApiOperation(value = "descargarSolicitudPlantacionesForestales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarSolicitudPlantacionesForestales(@RequestBody DescargarSolicitudPlantacionForestalDto obj){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = repo.descargarSolicitudPlantacionesForestales(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("SolicitudPlantacionesForestales - DescargarSolicitud", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/descargarSolicitudPlantacionForestal_PDF")
    @ApiOperation(value = "descargarSolicitudPlantacionesForestales", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoSolPlantForestal_PDF(@RequestBody DescargarSolicitudPlantacionForestalDto obj){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            //result = repo.descargarSolicitudPlantacionesForestales(obj);
            //return new ResponseEntity<>(result, HttpStatus.OK );

            ByteArrayResource bytes = PDFSolPlanForService.consolidadoSolPlantForestal_PDF(obj);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("plantacionForestal.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el consolidado de la Solicitud Plantacion Forestal.");
            log.error("Service - Se descargo la Solicitud Plantacion Forestal correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("SolicitudPlantacionesForestales - DescargarSolicitud", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    @PostMapping(path = "/generarNroRegistro")
    @ApiOperation(value = "generarNroRegistro", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity generarNroRegistro(@RequestBody SolPlantacionForestalEntity request) {
        log.info("SolPlantacionForestalAreaController - listarAreaBloque", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = repo.generarNroRegistro(request);
            log.info("SolPlantacionForestalController - generarNroRegistro", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalController - generarNroRegistro", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarSolPlantacacionForestalPersona")
    @ApiOperation(value = "Registrar Solicitud Plantacacion Forestal Persona", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarSolPlantacionForestalPersona(
            @RequestBody List<SolPlantacionForestalPersonaEntity> request) {

        log.info("SolPlantacionForestalController - registrarSolPlantacacionForestalPersona", request.toString());

        ResultClassEntity response = new ResultClassEntity();

        try {
            response = repo.registrarSolPlantacionForestalPersona(request);

            log.info("SolPlantacionForestalController - registrarSolPlantacacionForestalPersona\n", "Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - registrarSolPlantacacionForestalPersona",
                    "Ocurrió un error al registrar en: " + Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(path = "/listarSolPlantacacionForestalPersona")
    @ApiOperation(value = "Listar Solicitud Plantacacion Forestal Persona", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> listarSolPlantacionForestalPersona(
            @RequestBody SolPlantacionForestalPersonaEntity request) {

        log.info("SolPlantacionForestalController - listarSolPlantacacionForestalPersona", request.toString());

        ResultClassEntity response = new ResultClassEntity();

        try {
            response = repo.listarSolPlantacionForestalPersona(request);

            log.info("SolPlantacionForestalController - listarSolPlantacacionForestalPersona\n", "Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - listarSolPlantacacionForestalPersona",
                    "Ocurrió un error al listar en: " + Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(path = "/eliminarSolPlantacacionForestalPersona")
    @ApiOperation(value = "Eliminar Solicitud Plantacacion Forestal Persona", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> eliminarSolPlantacionForestalPersona(
            @RequestBody SolPlantacionForestalPersonaEntity request) {

        log.info("SolPlantacionForestalController - eliminarSolPlantacacionForestalPersona", request.toString());

        ResultClassEntity response = new ResultClassEntity();

        try {
            response = repo.eliminarSolPlantacionForestalPersona(request);

            log.info("SolPlantacionForestalController - eliminarSolPlantacacionForestalPersona\n", "Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        } catch (Exception Ex) {
            log.error("SolPlantacionForestalController - eliminarSolPlantacacionForestalPersona",
                    "Ocurrió un error al eliminar en: " + Ex.getMessage());
            response.setMessage(Ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }




}
