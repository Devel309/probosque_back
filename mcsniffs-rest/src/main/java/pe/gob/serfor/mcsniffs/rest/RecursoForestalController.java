package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.ActividadSilviculturalDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.RecursoForestalService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.recursoForestal.BASE)
public class RecursoForestalController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(RecursoForestalController.class);

    @Autowired
    private RecursoForestalService recursoForestalService;

    /**
     * @autor: Rafael Azaña [08-09-2021]
     * @modificado:
     * @descripción: {Listar los recursos forestales}
     */

    @PostMapping(path = "/registrarRecursoForestal")
    @ApiOperation(value = "registrarRecursoForestal", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarRecursoForestal(
            @RequestBody List<RecursoForestalEntity> list) {
        log.info("registrarRecursoForestal - registrarRecursoForestal", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = recursoForestalService.RegistrarRecursoForestal(list);
            log.info("ProteccionBosque - registrarActividadSilvicultural", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("ProteccionBosque -registrarActividadSilvicultural", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
/*
    @PostMapping(path = "/registrarRecursoForestal")
    @ApiOperation(value = "registrarRecursoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarRecursoForestal(@RequestBody RecursoForestalEntity entidad){
        log.info("RecursoForestal Registrar", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = recursoForestalService.RegistrarRecursoForestal(entidad);
            log.info("RecursoForestal Registrar","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("RecursoForestal Registrar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
*/


    @PostMapping(path = "/eliminarRecursoForestal")
    @ApiOperation(value = "eliminarRecursoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarRecursoForestal(@RequestBody RecursoForestalDetalleDto recursoForestalDetalleDto){
        log.info("RecursoForestal - eliminarRecursoForestal",recursoForestalDetalleDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = recursoForestalService.EliminarRecursoForestal(recursoForestalDetalleDto);
            log.info("RecursoForestal - eliminarRecursoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("RecursoForestal - eliminarRecursoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/listarRecursoForestal")
    @ApiOperation(value = "Listar RecursoForestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<RecursoForestalDetalleDto>> listarRecursoForestal(
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = true) Integer idRecursoForestal,
            @RequestParam("codCabecera") String codCabecera) {
        try {
            return recursoForestalService.listarRecursoForestal(idPlanManejo,idRecursoForestal,codCabecera);
        } catch (Exception ex) {

            ResultClassEntity<List<RecursoForestalDetalleDto>> result = new ResultClassEntity<>();
            log.error("CensoForestalDetalleController - listarRecursoForestal", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


}
