package pe.gob.serfor.mcsniffs.rest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.EvaluacionAmbientalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.serfor.mcsniffs.service.EvaluacionAmbientalService;

import java.util.List;

/**
 * @autor: Harry Coa [18-07-2021]
 * @modificado:
 * @descripción: {Controller Evaluacion Ambiental}
 * @param:OrdenamientoProteccionEntity
 */
@RestController
@RequestMapping(Urls.evaluacionAmbiental.BASE)
public class EvaluacionAmbientalController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EvaluacionAmbientalController.class);

    @Autowired
    private EvaluacionAmbientalService evaluacionAmbientalService;

    /**
     * @autor: Harry Coa [18-07-2021]
     * @modificado:
     * @descripción: {Registrar Evaluacion Ambiental}
     * @param:OrdenamientoProteccionEntity
     */

    @PostMapping(path = "/registrarEvaluacionAmbiental")
    @ApiOperation(value = "registrarEvaluacionAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarEvaluacionAmbiental(@RequestBody List<EvaluacionAmbientalEntity> list){
        log.info("EvaluacionAmbiental - RegistrarEvaluacionAmbiental", list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarEvaluacionAmbientalDetalleSub(list);
            log.info("EvaluacionAmbiental - RegistrarEvaluacionAmbiental","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("EvaluacionAmbiental - RegistrarEvaluacionAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarEvaluacionAmbientalXls")
    @ApiOperation(value = "registrarEvaluacionAmbientalXls" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarEvaluacionAmbientalDetalleSubXls(@RequestBody EvaluacionAmbientalEntity entidad){
        log.info("EvaluacionAmbiental - RegistrarEvaluacionAmbientalDetalleSubXls", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarEvaluacionAmbientalDetalleSubXls(entidad);
            log.info("EvaluacionAmbiental - RegistrarEvaluacionAmbientalDetalleSubXls","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("EvaluacionAmbiental - RegistrarEvaluacionAmbientalDetalleSubXls","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Harry Coa [24-07-2021]
     * @modificado:
     * @descripción: {listar por filtros}
     * @param:OrdenamientoProteccionEntity
     */

    @PostMapping(path = "/listarFiltrosEvalAmbientalAprovechamiento")
    @ApiOperation(value = "listarFiltrosEvalAmbientalAprovechamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarFiltrosEvaluacionAmbiental(@RequestBody EvaluacionAmbientalAprovechamientoEntity entidad){
        try{
            return  new ResponseEntity (true, "ok", evaluacionAmbientalService.ListarEvaluacionAprovechamientoFiltro(entidad));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    /**
     * @autor: Harry Coa [24-07-2021]
     * @modificado:
     * @descripción: {registrar por filtros}
     * @param:OrdenamientoProteccionEntity
     */
    @PostMapping(path = "/registrarAprovechamientoEvalAmbiental")
    @ApiOperation(value = "registrarAprovechamientoEvalAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAprovechamientoEvalAmbiental(@RequestBody EvaluacionAmbientalAprovechamientoEntity entidad){
        log.info("EvaluacionAmbiental - RegistrarEvaluacionAmbiental", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarAprovechamientoEvaluacionAmbiental(entidad);
            log.info("EvaluacionAmbiental - RegistrarEvaluacionAmbiental","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("EvaluacionAmbiental - RegistrarEvaluacionAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarPlanGestionExcel")
    @ApiOperation(value = "registrarPlanGestionExcel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanGestionExcel(@RequestParam("file") MultipartFile file,
                                                                            @RequestParam("nombreHoja") String nombreHoja,
                                                                            @RequestParam("numeroFila") Integer numeroFila,
                                                                            @RequestParam("numeroColumna") Integer numeroColumna,
                                                                            @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                            @RequestParam("codTipoAprovechamiento") String codTipoAprovechamiento,
                                                                            @RequestParam("tipoAprovechamiento") String tipoAprovechamiento,
                                                                            @RequestParam("tipoNombreAprovechamiento") String tipoNombreAprovechamiento,
                                                                            @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarPlanGestionExcel(file, nombreHoja, numeroFila, numeroColumna, idPlanManejo, codTipoAprovechamiento, tipoAprovechamiento, tipoNombreAprovechamiento, idUsuarioRegistro);
            log.info("EvaluacionAmbiental - RegistrarPlanGestionExcel","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("EvaluacionAmbiental - RegistrarPlanGestionExcel","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarPlanAccionExcel")
    @ApiOperation(value = "registrarPlanAccionExcel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanAccionExcel(@RequestParam("file") MultipartFile file,
                                                                  @RequestParam("nombreHoja") String nombreHoja,
                                                                  @RequestParam("numeroFila") Integer numeroFila,
                                                                  @RequestParam("numeroColumna") Integer numeroColumna,
                                                                  @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                  @RequestParam("codTipoAprovechamiento") String codTipoAprovechamiento,
                                                                  @RequestParam("tipoAprovechamiento") String tipoAprovechamiento,
                                                                  @RequestParam("tipoNombreAprovechamiento") String tipoNombreAprovechamiento,
                                                                  @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarPlanAccionExcel(file, nombreHoja, numeroFila, numeroColumna, idPlanManejo, codTipoAprovechamiento, tipoAprovechamiento, tipoNombreAprovechamiento, idUsuarioRegistro);
            log.info("EvaluacionAmbiental - RegistrarPlanAccionExcel","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("EvaluacionAmbiental - RegistrarPlanAccionExcel","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarPlanVigilanciaExcel")
    @ApiOperation(value = "registrarPlanVigilanciaExcel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanVigilanciaExcel(@RequestParam("file") MultipartFile file,
                                                                            @RequestParam("nombreHoja") String nombreHoja,
                                                                            @RequestParam("numeroFila") Integer numeroFila,
                                                                            @RequestParam("numeroColumna") Integer numeroColumna,
                                                                            @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                            @RequestParam("codTipoAprovechamiento") String codTipoAprovechamiento,
                                                                            @RequestParam("tipoAprovechamiento") String tipoAprovechamiento,
                                                                            @RequestParam("tipoNombreAprovechamiento") String tipoNombreAprovechamiento,
                                                                            @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarPlanVigilanciaExcel(file, nombreHoja, numeroFila, numeroColumna, idPlanManejo, codTipoAprovechamiento, tipoAprovechamiento, tipoNombreAprovechamiento, idUsuarioRegistro);
            log.info("EvaluacionAmbiental - RegistrarPlanVigilanciaExcel","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("EvaluacionAmbiental - RegistrarPlanVigilanciaExcel","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarPlanContingenciaExcel")
    @ApiOperation(value = "registrarPlanContingenciaExcel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanContingenciaExcel(@RequestParam("file") MultipartFile file,
                                                                                @RequestParam("nombreHoja") String nombreHoja,
                                                                                @RequestParam("numeroFila") Integer numeroFila,
                                                                                @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                @RequestParam("codTipoAprovechamiento") String codTipoAprovechamiento,
                                                                                @RequestParam("tipoAprovechamiento") String tipoAprovechamiento,
                                                                                @RequestParam("tipoNombreAprovechamiento") String tipoNombreAprovechamiento,
                                                                                @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarPlanContingenciaExcel(file, nombreHoja, numeroFila, numeroColumna, idPlanManejo, codTipoAprovechamiento, tipoAprovechamiento, tipoNombreAprovechamiento, idUsuarioRegistro);
            log.info("EvaluacionAmbiental - RegistrarPlanContingenciaExcel","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("EvaluacionAmbiental - RegistrarPlanContingenciaExcel","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Harry Coa [24-07-2021]
     * @modificado:
     * @descripción: {listar por filtros}
     * @param:OrdenamientoProteccionEntity
     */
    @PostMapping(path = "/listarFiltrosEvalAmbientalActividad")
    @ApiOperation(value = "listarFiltrosEvalAmbientalActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarFiltrosEvalAmbientalActividad(@RequestBody EvaluacionAmbientalActividadEntity entidad){
        try{
            return  new ResponseEntity (true, "ok", evaluacionAmbientalService.ListarEvaluacionActividadFiltro(entidad));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }


    /**
     * @autor: Harry Coa [25-07-2021]
     * @modificado:
     * @descripción: {registrar por filtros}
     * @param:OrdenamientoProteccionEntity
     */
    @PostMapping(path = "/registrarActividadEvalAmbiental")
    @ApiOperation(value = "registrarActividadEvalAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarActividadEvalAmbiental(@RequestBody EvaluacionAmbientalActividadEntity entidad){
        log.info("EvaluacionAmbiental - registrarActividadEvalAmbiental", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarActividadEvaluacionAmbiental(entidad);
            log.info("EvaluacionAmbiental - registrarActividadEvalAmbiental","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("EvaluacionAmbiental - RegistrarEvaluacionAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Harry Coa [26-07-2021]
     * @modificado:
     * @descripción: {listar Evaluacion ambiental tabla intermedia que relaciona la activad con el aprovechamiento}
     * @param:OrdenamientoProteccionEntity
     */

    @PostMapping(path = "/listarEvaluacionAmbiental")
    @ApiOperation(value = "listarEvaluacionAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacionAmbiental(@RequestBody EvaluacionAmbientalDto param){
        try{
            return  new ResponseEntity (true, "ok", evaluacionAmbientalService.ListarEvaluacionAmbiental(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    /**
     * @autor: Roberto Meiggs [07-02-2022]
     * @modificado:
     * @descripción: {listar Evaluacion ambiental tabla intermedia que relaciona la activad con el aprovechamiento}
     * @param:OrdenamientoProteccionEntity
     */

    @PostMapping(path = "/eliminarEvaluacionAmbientalAprovechamiento")
    @ApiOperation(value = "eliminarEvaluacionAmbientalAprovechamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity EliminarEvaluacionAmbientalAprovechamiento(@RequestBody EvaluacionAmbientalAprovechamientoEntity param){
        try{
            return new ResponseEntity (true, "ok", evaluacionAmbientalService.EliminarEvaluacionAmbientalAprovechamiento(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    /**
     * @autor: Roberto Meiggs [07-02-2022]
     * @modificado:
     * @descripción: {Registrar Aprovechamiento Evaluacion Ambiental}
     * @param:List<EvaluacionAmbientalAprovechamientoEntity>
     */

    @PostMapping(path = "/registrarListEvaluacionAmbiental")
    @ApiOperation(value = "registrarListEvaluacionAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity RegistrarListAprovechamientoEvaluacionAmbiental(@RequestBody List<EvaluacionAmbientalAprovechamientoEntity> list){
        log.info("EvaluacionAmbiental - RegistrarListAprovechamientoEvaluacionAmbiental", list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = evaluacionAmbientalService.RegistrarListAprovechamientoEvaluacionAmbiental(list);
            log.info("EvaluacionAmbiental - RegistrarListAprovechamientoEvaluacionAmbiental","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("EvaluacionAmbiental - RegistrarListAprovechamientoEvaluacionAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacionAmbientalActividad")
    @ApiOperation(value = "eliminarEvaluacionAmbientalActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarEvaluacionAmbientalActividad(@RequestBody EvaluacionAmbientalActividadEntity evaluacionAmbientalActividadEntity){
        log.info("EvaluacionAmbiental - eliminarEvaluacionAmbientalActividad",evaluacionAmbientalActividadEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = evaluacionAmbientalService.eliminarEvaluacionAmbientalActividad(evaluacionAmbientalActividadEntity);
            log.info("EvaluacionAmbiental - eliminarEvaluacionAmbientalActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("EvaluacionAmbiental - eliminarEvaluacionAmbientalActividad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacionAmbientalAprovechamientoList")
    @ApiOperation(value = "eliminarEvaluacionAmbientalAprovechamientoList" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarEvaluacionAmbientalAprovechamientoList(@RequestBody EvaluacionAmbientalAprovechamientoEntity evaluacionAmbientalAprovechamientoEntity){
        log.info("EvaluacionAmbiental - eliminarEvaluacionAmbientalAprovechamientoList",evaluacionAmbientalAprovechamientoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = evaluacionAmbientalService.eliminarEvaluacionAmbientalAprovechamientoList(evaluacionAmbientalAprovechamientoEntity);
            log.info("EvaluacionAmbiental - eliminarEvaluacionAmbientalAprovechamientoList","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("EvaluacionAmbiental - eliminarEvaluacionAmbientalAprovechamientoList","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacionAmbiental")
    @ApiOperation(value = "eliminarEvaluacionAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarEvaluacionAmbiental(@RequestBody EvaluacionAmbientalEntity evaluacionAmbientalEntity){
        log.info("EvaluacionAmbiental - eliminarEvaluacionAmbiental",evaluacionAmbientalEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = evaluacionAmbientalService.eliminarEvaluacionAmbiental(evaluacionAmbientalEntity);
            log.info("EvaluacionAmbiental - eliminarEvaluacionAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("EvaluacionAmbiental - eliminarEvaluacionAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
