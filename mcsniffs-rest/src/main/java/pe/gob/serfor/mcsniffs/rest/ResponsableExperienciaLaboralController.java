package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.ResponsableExperienciaLaboralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ResponsableExperienciaLaboralService;

import java.util.Arrays;

@RestController
@RequestMapping(Urls.responsableExperienciaLaboral.BASE)
public class ResponsableExperienciaLaboralController extends AbstractRestController {
    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(ResponsableExperienciaLaboralController.class);

    @Autowired
    private ResponsableExperienciaLaboralService responsableExperienciaLaboralService;

    @PostMapping(path = "/obtenerResponsableExperienciaLaboral")
    @ApiOperation(value = "obtener responsable experiencia laboral", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerResponsableExperienciaLaboral(@RequestBody ResponsableExperienciaLaboralDto request) {
        log.info("obtenerResponsableExperienciaLaboral - listarResponsableExperienciaLaboral", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = responsableExperienciaLaboralService.obtenerResponsableExperienciaLaboral(request);
            log.info("obtenerResponsableExperienciaLaboral - listarResponsableExperienciaLaboral",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("obtenerResponsableExperienciaLaboral - listarResponsableExperienciaLaboral",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarResponsableExperienciaLaboral")
    @ApiOperation(value = "listar responsable experiencia laboral", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarResponsableExperienciaLaboral(@RequestBody ResponsableExperienciaLaboralDto request) {
        log.info("ResponsableExperienciaLaboral - listarResponsableExperienciaLaboral", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = responsableExperienciaLaboralService.listarResponsableExperienciaLaboral(request);
            log.info("ResponsableExperienciaLaboral - listarResponsableExperienciaLaboral",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboral - listarResponsableExperienciaLaboral",
                    "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarResponsableExperienciaLaboral")
    @ApiOperation(value = "registrar responsable experiencia laboral", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarResponsableExperienciaLaboral(
            @RequestBody SolicitudConcesionResponsableDto request) {
        log.info("ResponsableExperienciaLaboral - registrarResponsableExperienciaLaboral", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = responsableExperienciaLaboralService.registrarResponsableExperienciaLaboral(request);
            log.info("ResponsableExperienciaLaboral - registrarResponsableExperienciaLaboral",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboral - registrarResponsableExperienciaLaboral",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarResponsableExperienciaLaboral")
    @ApiOperation(value = "Actualizar responsable experiencia laboral", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarResponsableExperienciaLaboral(
            @RequestBody SolicitudConcesionResponsableDto request) {
        log.info("ResponsableExperienciaLaboral - actualizarResponsableExperienciaLaboral", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = responsableExperienciaLaboralService.actualizarResponsableExperienciaLaboral(request);
            log.info("ResponsableExperienciaLaboral - actualizarResponsableExperienciaLaboral",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboral - actualizarResponsableExperienciaLaboral",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarResponsableExperienciaLaboral")
    @ApiOperation(value = "eliminar responsable experiencia laboral", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarResponsableExperienciaLaboral(
            @RequestBody ResponsableExperienciaLaboralDto request) {
        log.info("ResponsableExperienciaLaboral - eliminarResponsableExperienciaLaboral", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = responsableExperienciaLaboralService.eliminarResponsableExperienciaLaboral(request);
            log.info("ResponsableExperienciaLaboral - eliminarResponsableExperienciaLaboral",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboral - eliminarResponsableExperienciaLaboral",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

}
