package pe.gob.serfor.mcsniffs.rest.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ErrorResponse showCustomMessage(Exception e, HttpServletRequest request) {

        ErrorResponse response = new ErrorResponse();

        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setPath(request.getRequestURL().toString());
        response.setSuccess(false);
        response.setMessage(e.getMessage());
        response.setError(Arrays.toString(e.getStackTrace()));
        return response;
    }
}