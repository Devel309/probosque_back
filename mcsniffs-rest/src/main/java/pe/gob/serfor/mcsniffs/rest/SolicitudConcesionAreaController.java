package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionAreaDto;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionCriterioAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionCriterioAreaService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionAreaService;

import java.util.Arrays;

@RestController
@RequestMapping(Urls.solicitudConcesion.BASE)
public class SolicitudConcesionAreaController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionAreaController.class);

    @Autowired
    SolicitudConcesionCriterioAreaService serviceSolicitudConcesionCriterioArea;

    @Autowired
    SolicitudConcesionAreaService serviceSolicitudConcesionArea;

    /**
     * @autor: Wilfredo Elias [18-01-2022]
     * @modificado:
     * @descripción: {Obtener SolicitudConcesionArea por  filtros}
     * @param:solicitudConcesionAreaEntity
     */
    @PostMapping(path = "/listarAreaSolicitudConcesion")
    @ApiOperation(value = "listar Area de Solicitud Concesion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarAreaSolicitudConcesion(@RequestBody SolicitudConcesionAreaEntity request) {
        log.info("SolicitudConcesionArea - listarAreaSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudConcesionArea.listarSolicitudConcesionArea(request);
            log.info("SolicitudConcesionArea - listarAreaSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionArea - listarAreaSolicitudConcesion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [18-01-2022]
     * @modificado:
     * @descripción: {Obtener SolicitudConcesionCriterioArea por  filtros}
     * @param:solicitudConcesionCriterioAreaEntity
     */
    @PostMapping(path = "/listarCriterioAreaSolicitudConcesion")
    @ApiOperation(value = "listar Criterios de Areaa de Solicitud Concesion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarCriterioAreaSolicitudConcesion(@RequestBody SolicitudConcesionCriterioAreaEntity request) {
        log.info("SolicitudConcesionArea - listarCriterioAreaSolicitudConcesion", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudConcesionCriterioArea.listarSolicitudConcesionCriterioArea(request);
            log.info("SolicitudConcesionArea - listarCriterioAreaSolicitudConcesion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionArea - listarCriterioAreaSolicitudConcesion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerInformacionAreaSolicitada")
    @ApiOperation(value = "Obtener Informacion Area Solicitada" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInformacionAreaSolicitada(@RequestBody SolicitudConcesionAreaEntity request) {
        log.info("SolicitudConcesionArea - obtenerInformacionAreaSolicitada", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudConcesionArea.obtenerInformacionAreaSolicitada(request);
            log.info("SolicitudConcesionArea - obtenerInformacionAreaSolicitada", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionArea - obtenerInformacionAreaSolicitada", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [18-01-2022]
     * @modificado:
     * @descripción: {Registrar SolicitudConcesionArea}
     * @param:solicitudConcesionAreaEntity
     */
    @PostMapping(path = "/registrarAreaSolicitudConcesion")
    @ApiOperation(value = "Registrar Area de la Solicitud Concesión" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarAreaSolicitudConcesion(@RequestBody SolicitudConcesionAreaEntity request){
        log.info("SolicitudConcesionArea - registrarAreaSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionArea.registrarSolicitudConcesionArea(request);
            log.info("SolicitudConcesionArea - registrarAreaSolicitudConcesion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - registrarAreaSolicitudConcesion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/registrarInformacionAreaSolicitada")
    @ApiOperation(value = "Registrar Informacion Area Solicitada" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInformacionAreaSolicitada(@RequestBody SolicitudConcesionAreaEntity request){
        log.info("SolicitudConcesionArea - registrarInformacionAreaSolicitada",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionArea.registrarInformacionAreaSolicitada(request);
            log.info("SolicitudConcesionArea - registrarInformacionAreaSolicitada","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - registrarInformacionAreaSolicitada","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Wilfredo Elias [18-01-2022]
     * @modificado:
     * @descripción: {Registrar SolicitudConcesionCriterioArea}
     * @param:solicitudConcesionCriterioAreaEntity
     */
    @PostMapping(path = "/registrarCriterioAreaSolicitudConcesion")
    @ApiOperation(value = "Registrar Criterio de Area de la Solicitud Concesión" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarCriterioAreaSolicitudConcesion(@RequestBody SolicitudConcesionCriterioAreaEntity request){
        log.info("SolicitudConcesionArea - registrarCriterioAreaSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionCriterioArea.registrarSolicitudConcesionCriterioArea(request);
            log.info("SolicitudConcesionArea - registrarCriterioAreaSolicitudConcesion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - registrarCriterioAreaSolicitudConcesion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarSolicitudConcesionCriterioArea")
    @ApiOperation(value = "Actualizar Criterio de Area de la Solicitud Concesión" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudConcesionCriterioArea(@RequestBody SolicitudConcesionCriterioAreaEntity request){
        log.info("SolicitudConcesionArea - actualizarSolicitudConcesionCriterioArea",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionCriterioArea.actualizarSolicitudConcesionCriterioArea(request);
            log.info("SolicitudConcesionArea - registrarCriterioAreaSolicitudConcesion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - actualizarSolicitudConcesionCriterioArea","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarInformacionAreaSolicitada")
    @ApiOperation(value = "Actualizar Informacion Area Solicitada" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInformacionAreaSolicitada(@RequestBody SolicitudConcesionAreaEntity request){
        log.info("SolicitudConcesionArea - actualizarInformacionAreaSolicitada",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionArea.actualizarInformacionAreaSolicitada(request);
            log.info("SolicitudConcesionArea - actualizarInformacionAreaSolicitada","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - actualizarInformacionAreaSolicitada","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarInfoAreaSolicitudConcesion")
    @ApiOperation(value = "Actualizar Informacion Area Solicitud Concesion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInfoAreaSolicitudConcesion(@RequestBody SolicitudConcesionAreaDto request){
        log.info("SolicitudConcesionArea - actualizarInfoAreaSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionArea.actualizarInformacionAreaSolicitudConcesion(request);
            log.info("SolicitudConcesionArea - actualizarInfoAreaSolicitudConcesion","Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - actualizarInfoAreaSolicitudConcesion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Wilfredo Elias [18-01-2022]
     * @modificado:
     * @descripción: {Eliminar SolicitudConcesionArea}
     * @param:solicitudConcesionAreaEntity
     */
    @PostMapping(path = "/eliminarAreaSolicitudConcesion")
    @ApiOperation(value = "Eliminar Area de la Solicitud Concesión" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarAreaSolicitudConcesion(@RequestBody SolicitudConcesionAreaEntity request){
        log.info("SolicitudConcesionArea - eliminarAreaSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionArea.eliminarSolicitudConcesionArea(request);
            log.info("SolicitudConcesionArea - eliminarAreaSolicitudConcesion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - eliminarAreaSolicitudConcesion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Wilfredo Elias [18-01-2022]
     * @modificado:
     * @descripción: {Eliminar SolicitudConcesionCriterioArea}
     * @param:solicitudConcesionCriterioAreaEntity
     */
    @PostMapping(path = "/eliminarCriterioAreaSolicitudConcesion")
    @ApiOperation(value = "Eliminar Criterio de Area de la Solicitud Concesión" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarCriterioAreaSolicitudConcesion(@RequestBody SolicitudConcesionCriterioAreaEntity request){
        log.info("SolicitudConcesionArea - eliminarCriterioAreaSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = serviceSolicitudConcesionCriterioArea.eliminarSolicitudConcesionCriterioArea(request);
            log.info("SolicitudConcesionArea - eliminarCriterioAreaSolicitudConcesion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionArea - eliminarCriterioAreaSolicitudConcesion","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
