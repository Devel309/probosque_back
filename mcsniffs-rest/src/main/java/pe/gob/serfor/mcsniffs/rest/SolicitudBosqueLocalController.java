package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalService;


@RestController
@RequestMapping(Urls.solicitudBosqueLocal.BASE)
public class SolicitudBosqueLocalController extends AbstractRestController{
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalController.class);

    @Autowired
    private SolicitudBosqueLocalService solicitudBosqueLocalService;

    @PostMapping(path = "/listarSolicitudBosqueLocal")
    @ApiOperation(value = "listar Actividad de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request) {
        log.info("SolicitudBosqueLocal - listarSolicitudConcesionActividad", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalService.listarSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal- listarSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- listarSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarSolicitudBosqueLocal")
    @ApiOperation(value = "Registrar Actividad de la Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request){
        log.info("SolicitudBosqueLocal - registrarActividadSolicitudConcesion",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalService.registrarSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal - registrarSolicitudBosqueLocal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - registrarSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarSolicitudBosqueLocal")
    @ApiOperation(value = "Actualizar Actividad de la Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request){
        log.info("SolicitudBosqueLocal - actualizarSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalService.actualizarSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal - actualizarSolicitudBosqueLocal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - actualizarSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarSolicitudBosqueLocal")
    @ApiOperation(value = "Eliminar Actividad de la Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request){
        log.info("SolicitudBosqueLocal - eliminarSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalService.eliminarSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal - eliminarSolicitudBosqueLocal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - eliminarSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/obtenerSolicitudBosqueLocal")
    @ApiOperation(value = "Obtener Actividad de la Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request){
        log.info("SolicitudBosqueLocal - obtenerSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalService.obtenerSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal - obtenerSolicitudBosqueLocal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - obtenerSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarEstadoSolicitudBosqueLocal")
    @ApiOperation(value = "Actualizar Estado de la Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarEstadoSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request){
        log.info("SolicitudBosqueLocal - actualizarEstadoSolicitudBosqueLocal",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal - actualizarEstadoSolicitudBosqueLocal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - actualizarEstadoSolicitudBosqueLocal","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/generarTHSolicitudBosqueLocal")
    @ApiOperation(value = "listar Usuario de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity generarTHSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request) {
        log.info("SolicitudBosqueLocal - listarUsuarioSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalService.generarTHSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal- listarUsuarioSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- listarUsuarioSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarUsuarioSolicitudBosqueLocal")
    @ApiOperation(value = "listar Usuario de Solicitud Bosque Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarUsuarioSolicitudBosqueLocal(@RequestBody SolicitudBosqueLocalEntity request) {
        log.info("SolicitudBosqueLocal - listarUsuarioSolicitudBosqueLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalService.listarUsuarioSolicitudBosqueLocal(request);
            log.info("SolicitudBosqueLocal- listarUsuarioSolicitudBosqueLocal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- listarUsuarioSolicitudBosqueLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/enviarGobiernoLocal")
    @ApiOperation(value = "enviar Gobierno Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity enviarGobiernoLocal(@RequestBody SolicitudBosqueLocalEntity request) {
        log.info("SolicitudBosqueLocal - enviarGobiernoLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalService.enviarGobiernoLocal(request);
            log.info("SolicitudBosqueLocal- enviarGobiernoLocal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- enviarGobiernoLocal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/notificarCreacionTH")
    @ApiOperation(value = "enviar Gobierno Local" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity notificarCreacionTH(@RequestBody SolicitudBosqueLocalEntity request) {
        log.info("SolicitudBosqueLocal - enviarGobiernoLocal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalService.notificarCreacionTH(request);
            log.info("SolicitudBosqueLocal- notificarCreacionTH", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- notificarCreacionTH", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
