package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.*;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoDocumentoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion.EvaluacionGeneralDto;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion.CompiladoPgmfDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Page;
import pe.gob.serfor.mcsniffs.entity.Parametro.Pageable;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EvaluacionArchivoService;
import pe.gob.serfor.mcsniffs.service.EvaluacionDetalleService;
import pe.gob.serfor.mcsniffs.service.EvaluacionService;

/**
 * @autor: Renzo Gabriel Meneses Gamarra [24-09-2021]
 * @modificado:
 * @descripción: { Controlador para el Plan Manejo Evaluación }
 */
@RestController
@RequestMapping(Urls.evaluacion.BASE)
public class EvaluacionController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EvaluacionController.class);

    @Autowired
    private EvaluacionService evaluacionService;

    @Autowired
    private EvaluacionDetalleService evaluacionDetalleService; 

    @Autowired
    private EvaluacionArchivoService evaluacionArchivoService;

    @PostMapping(path = "/listarEvaluacion")
    @ApiOperation(value = "listarEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacion(@RequestBody ListarEvaluacionDto param){
        log.info("Evaluacion - listarEvaluacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.listarEvaluacion(param);
            log.info("Evaluacion - listarEvaluacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - listarEvaluacion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarEvaluacionResumido")
    @ApiOperation(value = "listarEvaluacionResumido" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacionResumido(@RequestBody EvaluacionResumidoDto param){
        log.info("Evaluacion - listarEvaluacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.listarEvaluacionResumido(param);
            log.info("Evaluacion - listarEvaluacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - listarEvaluacion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarEvaluacionPermisoForestal")
    @ApiOperation(value = "listarEvaluacionPermisoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacionPermisoForestal(@RequestBody EvaluacionPermisoForestalDto param){
        log.info("Evaluacion - listarEvaluacionPermisoForestal",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.listarEvaluacionPermisoForestal(param);
            log.info("Evaluacion - listarEvaluacionPermisoForestal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - listarEvaluacionPermisoForestal","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarEvaluacionDetalle")
    @ApiOperation(value = "listarEvaluacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacionDetalle(@RequestBody EvaluacionDetalleDto dto){
        log.info("Evaluacion - listarEvaluacionDetalle",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionDetalleService.listarEvaluacionDetalle(dto);
            log.info("evaluacionDetalleService - listarEvaluacionDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("evaluacionDetalleService - listarEvaluacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarEvaluacion")
    @ApiOperation(value = "registrarEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacion(@RequestBody EvaluacionDto param){
        log.info("Evaluacion - registrarEvaluacion",param.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.registrarEvaluacion(param);
            log.info("Evaluacion - registrarEvaluacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - registrarEvaluacion","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarEvaluacionPermisoForestal")
    @ApiOperation(value = "registrarEvaluacionPermisoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacionPermisoForestal(@RequestBody EvaluacionPermisoForestalDto param){
        log.info("EvaluacionPermisoForestal - registrarEvaluacionPermisoForestal",param.toString());

        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.registrarEvaluacionPermisoForestal(param);
            log.info("Evaluacion - registrarEvaluacionPermisoForestal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - registrarEvaluacionPermisoForestal","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacion")
    @ApiOperation(value = "eliminarEvaluacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarEvaluacion(@RequestBody EvaluacionDto param){
        log.info("Evaluacion - eliminarEvaluacion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.eliminarEvaluacion(param);
            log.info("Evaluacion - eliminarEvaluacion","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - eliminarEvaluacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacionPermisoForestal")
    @ApiOperation(value = "eliminarEvaluacionPermisoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarEvaluacionPermisoForestal(@RequestBody EvaluacionPermisoForestalDto param){
        log.info("Evaluacion - eliminarEvaluacionPermisoForestal",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.eliminarEvaluacionPermisoForestal(param);
            log.info("Evaluacion - eliminarEvaluacionPermisoForestal","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - eliminarEvaluacionPermisoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacionDetalle")
    @ApiOperation(value = "eliminarEvaluacionDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarEvaluacionDetalle(@RequestBody List<EvaluacionDetalleDto> dto){
        log.info("Evaluacion - eliminarEvaluacionDetalle",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionDetalleService.eliminarEvaluacionDetalle(dto);
            log.info("Evaluacion - eliminarEvaluacionDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - eliminarEvaluacionDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarInspeccionTipoDocumento")
    @ApiOperation(value = "listarInspeccionTipoDocumento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarInspeccionTipoDocumento(@RequestBody TipoDocumentoEntity param){
        log.info("Evaluacion - listarInspeccionTipoDocumento",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.listarInspeccionTipoDocumento(param);
            log.info("Evaluacion - listarInspeccionTipoDocumento","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - listarInspeccionTipoDocumento","Ocurrió un error :"+ e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Rafael Azaña [31-10-2021]
     * @descripción: {Listado de tab para la evaluacion}
     * @param: EvaluacionDto
     * @return: ResponseEntity<ResponseVO>
     */

    @PostMapping(path = "/listarEvaluacionTab")
    @ApiOperation(value = "Listar Evaluacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<EvaluacionDetalleDto>> listarEvaluacionTab(
            @RequestParam(required = false) Integer idPlanManejo,
            @RequestParam(required = false) String codigo,
            @RequestParam(required = false) String codigoEvaluacionDet,
            @RequestParam(required = false) String conforme,
            @RequestParam(required = false) String codigoEvaluacionDetSub) {
        try {
            return evaluacionService.listarEvaluacionTab(idPlanManejo,codigo,codigoEvaluacionDet,conforme, codigoEvaluacionDetSub);
        } catch (Exception ex) {

            ResultClassEntity<List<EvaluacionDetalleDto>> result = new ResultClassEntity<>();
            log.error("EvaluacionController - listarEvaluacionTab", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }



    @PostMapping(path = "/listarEvaluacionGeneral")
    @ApiOperation(value = "Listar Evaluacion General" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> listarEvaluacionGeneral(@RequestBody EvaluacionGeneralDto request){
        log.info("EvaluacionController - listarEvaluacionGeneral", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = evaluacionService.listarEvaluacionGeneral(request);
            log.info("EvaluacionController - listarEvaluacionGeneral","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("EvaluacionController - listarEvaluacionGeneral","Ocurrió un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/listarEvaluacionArchivo")
    @ApiOperation(value = "listarEvaluacionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacionArchivo(@RequestBody EvaluacionArchivoDto dto){
        log.info("Evaluacion - listarEvaluacionArchivo",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionArchivoService.listarEvaluacionArchivo(dto);
            log.info("evaluacionArchivoService - listarEvaluacionArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("evaluacionArchivoService - listarEvaluacionArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarEvaluacionArchivo")
    @ApiOperation(value = "eliminarEvaluacionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarEvaluacionArchivo(@RequestBody List<EvaluacionArchivoDto> dto){
        log.info("Evaluacion - eliminarEvaluacionArchivo",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionArchivoService.eliminarEvaluacionArchivo(dto);
            log.info("Evaluacion - eliminarEvaluacionArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - eliminarEvaluacionArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarEvaluacionArchivo")
    @ApiOperation(value = "registrarEvaluacionArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacionArchivo(@RequestBody List<EvaluacionArchivoDto> dto){
        log.info("Evaluacion - registrarEvaluacionArchivo",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionArchivoService.registrarEvaluacionArchivo(dto);
            log.info("Evaluacion - registrarEvaluacionArchivo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - registrarEvaluacionArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
	 * @autor: Jaqueline Diaz Barrientos 28-09-2021
	 * @modificado:
	 * @descripción: {Obtener Archivo Consolidado PMFI}
	 * @param: Integer idPlanManejo	 
	 */
    @PostMapping(path = "/generarInformacionEvaluacion")
    @ApiOperation(value = "generar Informacion Evaluacion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> consolidadoPMFI(@RequestBody CompiladoPgmfDto compiladoPgmfDto)
            throws Exception {
                ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("Evaluacion: idPlanManejo:{},", compiladoPgmfDto.toString());
        try {
            result = evaluacionService.generarInformacionEvaluacion(compiladoPgmfDto);
            log.info("Service - consolidado: Proceso realizado correctamente");
           result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            log.error("Service - Se descargo la dema correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
             /*ByteArrayResource bytes = new ByteArrayResource(result.getArchivo());
            HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            String fileName = String.format("consolidado-%s-%s.docx", compiladoPgmfDto.getIdPlanManejo(),new Date());
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return new ResponseEntity<>(bytes, header, HttpStatus.CREATED);*/
        } catch (Exception e) {
            log.error("Evaluacion - generarInformacionEvaluacion","Ocurrió un error :"+ e.getMessage());
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setInformacion(e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/obtenerRegente")
    @ApiOperation(value = "obtenerRegente" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerRegente(@RequestParam(required = false) String numeroDocumento, HttpServletRequest request1){
        log.info("Evaluacion - obtenerRegente");
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            String token= request1.getHeader("Authorization");
            response = evaluacionService.obtenerRegente(numeroDocumento, token);
            log.info("evaluacionArchivoService - obtenerRegente","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("evaluacionArchivoService - obtenerRegente","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:   Jason Retamozo 09-03-2022
     * @descripción: {Listar/Filtrar Plan Manejo}
     * @param: Integer idPlanManejo
     * @param: Integer idTipoProceso
     * @param: Integer idTipoPlan
     * @param: Integer idContrato
     * @param: String dniElaborador
     * @param: String rucComunidad
     * @param: String rucComunidad
     * @param: String rucComunidad
     */
    @GetMapping(path = "/filtrarEvaluacionPlan")
    @ApiOperation(value = "Listar y/o Buscar(Filtrar) PlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public Pageable<List<PlanManejoEntity>> filtrarEvaluacionPlan(@RequestParam(required = false) Integer idPlanManejo, //
            @RequestParam(required = false) Integer idTipoProceso, //
            @RequestParam(required = false) Integer idTipoPlan, //
            @RequestParam(required = false) Integer idContrato, //
            @RequestParam(required = false) String dniElaborador, //
            @RequestParam(required = false) String rucComunidad, //
            @RequestParam(required = false) String nombreTitular, //
            @RequestParam(required = false) String codigoEstado,
            @RequestParam(required = false, defaultValue = "1") Long pageNumber, //
            @RequestParam(required = false, defaultValue = "10") Long pageSize,
            @RequestParam(required = false) String sortField,
            @RequestParam(required = false, defaultValue = "DESC") String sortType) throws Exception {
        Page p = new Page(pageNumber, pageSize, sortField, sortType);
        log.info("obtener: {}, {}, {}, {},{}, {}, pageable={}", idPlanManejo, idTipoProceso, idTipoPlan, idContrato,
                dniElaborador, rucComunidad,nombreTitular, p);
        Pageable<List<PlanManejoEntity>> response= new Pageable<List<PlanManejoEntity>>();
        try {
            response = evaluacionService.filtrarEvaluacionPlan(idPlanManejo, idTipoProceso,
                    idTipoPlan, idContrato, dniElaborador, rucComunidad,nombreTitular, codigoEstado, p);
            log.info("Service - obtener: Proceso realizado correctamente");
            return response;

        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setMessage(e.getMessage());
            throw new Exception(e);
        }
    }

    /**
	 * @autor:   11-09-2021
	 * @modificado:   20-09-2021
	 * @descripción: {Listar/Filtrar Plan Manejo}
	 * @param: Integer idPlanManejo
	 * @param: Integer idTipoProceso
	 * @param: Integer idTipoPlan
	 * @param: Integer idContrato
	 * @param: String dniElaborador
	 * @param: String rucComunidad
	 * @param: String rucComunidad
	 * @param: String rucComunidad
	 */
    @GetMapping(path = "/filtrarEval")
    @ApiOperation(value = "Listar y/o Buscar(Filtrar) PlanManejo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public Pageable<List<PlanManejoEntity>> filrarEval(@RequestParam(required = false) Integer idPlanManejo, //
            @RequestParam(required = false) Integer idTipoProceso, //
            @RequestParam(required = false) Integer idTipoPlan, //
            @RequestParam(required = false) Integer idContrato, //
            @RequestParam(required = false) String dniElaborador, //
            @RequestParam(required = false) String rucComunidad, //
            @RequestParam(required = false) String nombreTitular, //
            @RequestParam(required = false) String codigoEstado,
            @RequestParam(required = false) String codigoUnico,
            @RequestParam(required = false) String modalidadTH,
            @RequestParam(required = false, defaultValue = "1") Long pageNumber, //
            @RequestParam(required = false, defaultValue = "10") Long pageSize,
            @RequestParam(required = false) String sortField,
            @RequestParam(required = false, defaultValue = "DESC") String sortType) throws Exception {
        Page p = new Page(pageNumber, pageSize, sortField, sortType);
        log.info("obtener: {}, {}, {}, {},{}, {}, pageable={}", idPlanManejo, idTipoProceso, idTipoPlan, idContrato,
                dniElaborador, rucComunidad,nombreTitular, p);
        try {
            Pageable<List<PlanManejoEntity>> response = evaluacionService.filrarEval(idPlanManejo, idTipoProceso,
                    idTipoPlan, idContrato, dniElaborador, rucComunidad,nombreTitular, codigoEstado,codigoUnico,modalidadTH, p);
            log.info("Service - obtener: Proceso realizado correctamente");
            return response;

        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }

    @PostMapping(path = "/diasHabiles")
    @ApiOperation(value = "diasHabiles", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity diasHabiles(@RequestBody DiasHabilesDto param) {
        log.info("Evaluación - diasHabiles", param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = evaluacionService.diasHabiles(param);
            log.info("Evaluación - diasHabiles", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Evaluación - diasHabiles", "Ocurrió un error :" + e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/evaluacionActualizarPlan")
    @ApiOperation(value = "evaluacionActualizarPlan" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity evaluacionActualizarPlan(@RequestBody EvaluacionActualizarPlan param){
        log.info("Evaluacion - evaluacionActualizarPlan",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.evaluacionActualizarPlan(param);
            log.info("Evaluacion - evaluacionActualizarPlan","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - evaluacionActualizarPlan","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/evaluacionActualizarPlanManejo")
    @ApiOperation(value = "evaluacionActualizarPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity evaluacionActualizarPlanManejo(@RequestBody PlanManejoDto param){
        log.info("Evaluacion - evaluacionActualizarPlanManejo",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = evaluacionService.evaluacionActualizarPlanManejo(param);
            log.info("Evaluacion - evaluacionActualizarPlanManejo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("Evaluacion - evaluacionActualizarPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Jason Retamozo [22-03-2022]
     * @modificado:
     * @descripción: { Plantilla de Informe docx}
     */
    @GetMapping(path = "/plantillaInformeEvaluacion")
    @ApiOperation(value = "Obtener Archivo plantillaInformeEvaluacion", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> plantillaInformeEvaluacion(@RequestParam(required = true) Integer idPlanManejo,
                                                                          @RequestParam(required = true) String tipoProceso)
            throws Exception{
        ResultArchivoEntity result = new ResultArchivoEntity();
        log.info("plantillaInformeEvaluacion: idPlanManejo:{},", idPlanManejo+" tipoProceso  "+tipoProceso);
        try {

            ByteArrayResource bytes = evaluacionService.plantillaInformeEvaluacion(idPlanManejo,tipoProceso);
            log.info("Service - consolidado: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("plantillaInformeEvaluacion.docx");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el Informe de Evaluacion.");
            log.error("Service - Se descargo el Informe de Evaluacion correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            throw new Exception(e);
        }
    }


    /**
     * @autor: Rafael Azaña 22-03-2022
     * @modificado:
     * @descripción: {Obtener Archivo plantilla de RESOLUCION}
     * @param: Integer idPlanManejo
     */

    @GetMapping(path = "/plantillaResolucion/{idPlanManejo}")
    @ApiOperation(value = "plantillaResolucion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> plantillaResolucion(
            @PathVariable(required = true) Integer idPlanManejo,
            @RequestParam(required = false)  String  tipoPlan
    ){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = evaluacionService.plantillaResolucion(idPlanManejo,tipoPlan);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("plantillaPermisosForestales - Descargar plantillaPermisosForestales", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


}