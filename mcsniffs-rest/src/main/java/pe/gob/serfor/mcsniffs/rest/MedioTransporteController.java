package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CensoForestalDetalleService;
import pe.gob.serfor.mcsniffs.service.MedioTransporteService;

import java.util.List;

@RestController
@RequestMapping(Urls.medioTransporte.BASE)

public class MedioTransporteController extends AbstractRestController {

    @Autowired
    private MedioTransporteService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(MedioTransporteController.class);




    @PostMapping(path = "/ListarMedioTransporte")
    @ApiOperation(value = "Listar MedioTransporte", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<MedioTransporteEntity>> ListarMedioTransporte(@RequestParam("idMedio") Integer idMedio) {
        try {
            return service.ListarMedioTransporte(idMedio);
        } catch (Exception ex) {

            ResultClassEntity<List<MedioTransporteEntity>> result = new ResultClassEntity<>();
            log.error("MedioTransporteController - ListarMedioTransporte", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


}
