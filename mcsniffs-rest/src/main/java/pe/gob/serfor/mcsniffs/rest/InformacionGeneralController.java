package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion.ProcesoPostulacionDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.InformacionGeneralDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.informacionGeneral.BASE)
public class InformacionGeneralController extends AbstractRestController {
	private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(InformacionGeneralController.class);

	@Autowired
	private InformacionGeneralService informacionGeneralService;

	/**
	 * @autor: Alexis Salgado [22-08-2021]
	 * @modificado:
	 * @descripción: {Obtener Información General por idPlanManejo}
	 * @param: Integer idPlanManejo
	 */
	@GetMapping(path = "")
	@ApiOperation(value = "Obtener Información General por idPlanManejo", authorizations = {
			@Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<InformacionGeneralDto>> obtener(
			@RequestParam(required = true) Integer idPlanManejo) {

		log.info("obtener: {}", idPlanManejo);

		ResultClassEntity<InformacionGeneralDto> response = new ResultClassEntity<>();
		try {
			response = informacionGeneralService.obtener(idPlanManejo);
			log.info("Service - obtener: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @autor: Alexis Salgado [22-08-2021]
	 * @modificado:
	 * @descripción: {Guardar Información General}
	 * @param: InformacionGeneralDto dto
	 */
	@PostMapping(path = "")
	@ApiOperation(value = "Guardar Información General", authorizations = { @Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultClassEntity<InformacionGeneralDto>> guardar(
			@RequestBody InformacionGeneralDto dto) {

		log.info("guardar: {}", dto);

		ResultClassEntity<InformacionGeneralDto> response = new ResultClassEntity<>();
		try {
			response = informacionGeneralService.guardar(dto);
			log.info("Service - guardar: Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Service - guardar: Ocurrió un error: {}", e.getMessage());
			response.setError(Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @autor: Harry Coa [18-07-2021]
	 * @modificado:
	 * @descripción: {Registrar Resumen Actividad PO Anterior}
	 * @param: InformacionGeneralEntity
	 */

	@PostMapping(path = "/actualizarInformacionGeneral")
	@ApiOperation(value = "actualizarInformacionGeneral", authorizations = { @Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity actualizarInformacionGeneral(
			@RequestBody InformacionGeneralEntity informacionGeneralEntity) {
		log.info("InformacionGeneral - actualizarInformacionGeneral", informacionGeneralEntity.toString());
		pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
		ResultClassEntity response;
		try {
			response = informacionGeneralService.ActualizarInformacionGeneral(informacionGeneralEntity);
			log.info("InformacionGeneral - actualizarInformacionGeneral", "Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

		} catch (Exception e) {
			log.error("InformacionGeneral - RegistrarInformacionGeneral", "Ocurrió un error :" + e.getMessage());
			result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping(path = "/registrarInformacionGeneral")
	@ApiOperation(value = "registrarInformacionGeneral", authorizations = { @Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity registrarInformacionGeneral(
			@RequestBody InformacionGeneralEntity informacionGeneralEntity) {

		log.info("InformacionGeneral - RegistrarInformacionGeneral", informacionGeneralEntity.toString());
		pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
		ResultClassEntity response;
		try {
			response = informacionGeneralService.RegistrarInformacionGeneral(informacionGeneralEntity);
			log.info("InformacionGeneral - RegistrarInformacionGeneral", "Proceso realizado correctamente");
			return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

		} catch (Exception e) {
			log.error("InformacionGeneral - RegistrarInformacionGeneral", "Ocurrió un error :" + e.getMessage());
			result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}



	@PostMapping(path = "/obtenerInformacionPlanOperativo")
	@ApiOperation(value = "obtenerInformacionPlanOperativo" , authorizations = {@Authorization(value="JWT")})
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity<ResultEntity> obtenerInformacionPlanOperativo(@RequestBody InformacionGeneralDEMAEntity request){
		log.info("InformacionGeneral - obtenerInformacionPlanOperativo", request.toString());
		//ResponseEntity<String> result = null;
		ResultEntity res = new ResultEntity() ;
		try {
			res = informacionGeneralService.obtenerInformacionPlanOperativo(request);
			log.info("InformacionGeneral - obtenerInformacionPlanOperativo","Proceso realizado correctamente");

			if (res.getIsSuccess()) {
				return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
			} else {
				return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e){
			log.error("InformacionGeneral - obtenerInformacionPlanOperativo","Ocurrio un error :"+ e.getMessage());
			res.setIsSuccess(Constantes.STATUS_ERROR);
			res.setMessage(Constantes.MESSAGE_ERROR_500);
			res.setStackTrace(Arrays.toString(e.getStackTrace()));
			res.setMessageExeption(e.getMessage());
			res.setTotalrecord(res.getTotalrecord());
			return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@PostMapping(path = "/actualizarInfGeneralResumido")
	@ApiOperation(value = "actualizarInfGeneralResumido", authorizations = { @Authorization(value = "JWT") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
			@ApiResponse(code = 404, message = "No encontrado") })
	public org.springframework.http.ResponseEntity actualizarInfGeneralResumido(
			@RequestBody List<InfGeneralEntity> list) {
		log.info("actualizarInfGeneralResumido - actualizarInfGeneralResumido", list.toString());
		pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
		ResultClassEntity response;
		try {
			response = informacionGeneralService.actualizarInfGeneralResumido(list);


			log.info("actualizarInfGeneralResumido - actualizarInfGeneralResumido", "Proceso realizado correctamente");
			if (!response.getSuccess()) {
				return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
			} else {
				return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
			}

		} catch (Exception e) {
			log.error("actualizarInfGeneralResumido -actualizarInfGeneralResumido", "Ocurrió un error :" + e.getMessage());
			result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
			return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/listarInfGeneralResumido")
	@ApiOperation(value = "listarInfGeneralResumido" , authorizations = {@Authorization(value="JWT")})
	@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
	public ResponseEntity listarInfGeneralResumido(@RequestBody InfGeneralEntity param){
		try{
			return  new ResponseEntity (true, "ok", informacionGeneralService.listarInfGeneralResumido(param));
		}catch (Exception e){
			log.error(e.getMessage());
			return new ResponseEntity(false,e.getMessage(),null);
		}
	}

}
