package pe.gob.serfor.mcsniffs.config;


import javax.validation.constraints.NotBlank;

public class TokenRefreshtoken {
    @NotBlank
    private String refreshToken;

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
