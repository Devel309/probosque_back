package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PGMFAbreviadoService;


@RestController
@RequestMapping(Urls.PGMFAbreviado.BASE)
public class PGMFAbreviadoController {
    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @Autowired
    private PGMFAbreviadoService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    
    /**
     * @autor: Miguel F. [31-07-2021]
     * @modificado:
     * @descripción: {obtener anexo 4 PGMF}
     *
     */
    @PostMapping(path = "/protecionVigilanciaObtener")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<OrdenamientoDetalleEntity>> protecionVigilanciaObtener(@RequestBody OrdenamientoDetalleEntity params){    
        try{
            return service.protecionVigilanciaObtener(  params);
        }catch(Exception ex){
            ResultClassEntity<List<OrdenamientoDetalleEntity>> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - cronogramaActividadEliminar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [24-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 15 PGMF} actualmetne no se usa
     *
     */
    @PostMapping(path = "/protecionVigilanciaArchivo")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<String> protecionVigilanciaArchivo(@RequestParam("file") MultipartFile file, OrdenamientoDetalleEntity params){    
        try{
            return service.protecionVigilanciaArchivo(file, params);
        }catch(Exception ex){
            ResultClassEntity<String> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - cronogramaActividadEliminar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [24-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 15 PGMF} actualmetne no se usa
     *
     */
    @PostMapping(path = "/cronogramaActividadEliminar")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> cronogramaActividadEliminar(@RequestBody CronogramaActividadEntity param){    
        try{
            return service.cronogramaActividadEliminar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - cronogramaActividadEliminar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Miguel F. [24-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 15 PGMF} actualmetne no se usa
     *
     */
    @PostMapping(path = "/cronogramaActividadDetalle_listarDetalle")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<CronogramaActividadDetalleEntity>> cronogramaActividadDetalle_ListarDetalle(@RequestBody CronogramaActividadDetalleEntity param){    
        try{
            return service.cronogramaActividadDetalle_ListarDetalle(param);
        }catch(Exception ex){
            ResultClassEntity<List<CronogramaActividadDetalleEntity>> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - cronogramaActividadDetalle_listarDetalle", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }
    
    /**
     * @autor: Miguel F. [22-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 15 PGMF} actualmetne no se usa
     *
     */
    @PostMapping(path = "/cronogramaActividadDetalle")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> cronogramaActividadDetalleRegistar(@RequestBody CronogramaActividadDetalleEntity param){    
        try{
            return service.cronogramaActividadDetalleRegistar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - cronogramaActividadDetalleRegistar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [22-07-2021]
     * @modificado:
     * @descripción: {actualizar anexo 15 PGMF} actualmetne no se usa
     *
     */
    @PostMapping(path = "/cronogramaActividadActualizar")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> cronogramaActividadActualizar(@RequestBody CronogramaActividadEntity param){    
        try{
            return service.cronogramaActividadActualizar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - cronogramaActividadActualizar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }
    
    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {registrar anexo 12 PGMF} actualmetne no se usa
     *
     */
    @PostMapping(path = "/cronogramaActividadRegistrar")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> cronogramaActividadRegistrar(@RequestBody CronogramaActividadEntity param){    
        try{
            return service.cronogramaActividadRegistrar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - ProgramaInversionObtener", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }
   
    
    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {registrar anexo 12 PGMF} 
     *
     */
    @PostMapping(path = "/programaInversionObtener")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<ProgramaInversionEntity> ProgramaInversionObtener(@RequestBody ProgramaInversionEntity param){    
        try{
            return service.ProgramaInversionObtener(param);
        }catch(Exception ex){
            ResultClassEntity<ProgramaInversionEntity> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - ProgramaInversionObtener", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

     /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {registrar anexo 12 PGMF}
     *
     */
    @PostMapping(path = "/programaInversionRegistrar")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> ProgramaInversionRegistrar(@RequestBody ProgramaInversionEntity param){    
        try{
            return service.ProgramaInversionRegistrar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - ProgramaInversionRegistrar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [16-07-2021]
     * @modificado:
     * @descripción: {eliminar anexo 12 PGMF}
     *
     */
    @PostMapping(path = "/programaInversionEliminar")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> ProgramaInversionEliminar(@RequestBody ProgramaInversionEntity param){
                
        try{
            return service.ProgramaInversionEliminar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoController - ProgramaInversionEliminar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
        
    }


    /**
     * @autor: Miguel F. [12-07-2021]
     * @modificado:
     * @descripción: {obtener anexo 11 PGMF}
     *
     */
    @PostMapping(path = "/organizacionManejo")
    @ApiOperation(value = "Organizacion Manejo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<OrganizacionManejoEntity>> OrganizacionManejo(@RequestBody OrganizacionManejoEntity param){
                
        try{
            return service.OrganizacionManejo(param);
        }catch(Exception ex){
            ResultClassEntity<List<OrganizacionManejoEntity>> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoService - OrganizacionManejoEntity", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
        
    }

    /**
     * @autor: Miguel F. [12-07-2021]
     * @modificado:
     * @descripción: {obtener anexo 8 PGMF}
     *
     */
    @PostMapping(path = "/monitoreo")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<MonitoreoAbreviadoEntity>> Monitoreo(@RequestBody MonitoreoAbreviadoEntity param){
                
        try{
            return service.Monitoreo(param);
        }catch(Exception ex){
            ResultClassEntity<List<MonitoreoAbreviadoEntity>> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoService - Monitoreo", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
        
    }

    /**
     * @autor: Miguel F. [12-07-2021]
     * @modificado:
     * @descripción: {obtener anexo 8 PGMF}
     *
     */
    @PostMapping(path = "/monitoreoActualizar")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<Boolean> MonitoreoActualizar(@RequestBody List< MonitoreoAbreviadoEntity> param){
                
        try{
            return service.MonitoreoActualizar(param);
        }catch(Exception ex){
            ResultClassEntity<Boolean> result = new ResultClassEntity<>();
            log.error("PGMFAbreviadoService - MonitoreoActualizar", "Ocurrió un error en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
        
    }
    
    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {obtener anexo2 PGMF}
     *
     */
    @PostMapping(path = "/obtenerObjetivo2")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<PGMFAbreviadoObjetivo2Entity> obtenerObjetivo2(@RequestBody PGMFAbreviadoObjetivo2Entity param){
                
        try{
            return service.obtenerObjetivo2(param);
        }catch(Exception ex){
            ResultClassEntity<PGMFAbreviadoObjetivo2Entity> result = new ResultClassEntity<>();
            log.error("anexo - generaranexo1", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
        
    }

    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {insertar anexo2 PGMF}
     *
     */
    @PostMapping(path = "/insertarObjetivo2")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<String> insertarObjetivo2(@RequestBody PGMFAbreviadoObjetivo2Entity param){
        try{
            return service.insertarObjetivo2(param);
        }catch(Exception ex){
            ResultClassEntity<String> result = new ResultClassEntity<>();
            log.error("anexo - generaranexo1", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
        
    }

    /**
     * @autor: Miguel F. [01-07-2021]
     * @modificado:
     * @descripción: {insertar anexo2 PGMF}
     *
     */
    @PostMapping(path = "/obtenerObjetivo1")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<PGMFAbreviadoObjetivo1Entity> pgmfAbreviadoObj1(@RequestBody PGMFAbreviadoObjetivo1Entity param){
        try{
            return service.pgmfAbreviadoObj1(param);
        }catch(Exception ex){
            ResultClassEntity<PGMFAbreviadoObjetivo1Entity> result = new ResultClassEntity<>();
            log.error("anexo - generaranexo1", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @PostMapping(path = "/participacionCiudadana")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List< ParticipacionCiudadanaEntity>> participacionCiudadana(@RequestBody ParticipacionCiudadanaEntity param){
        try{
            return service.participacionCiudadana(param);
        }catch(Exception ex){
            ResultClassEntity<List< ParticipacionCiudadanaEntity>> result = new ResultClassEntity<>();
            log.error("anexo - participacionCiudadana", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @PostMapping(path = "/capacitacion")
    @ApiOperation(value = "capacitación", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List< CapacitacionPGMFAbreviadoEntity>> Capacitacion(@RequestBody CapacitacionPGMFAbreviadoEntity param){
        try{
            return service.Capacitacion(param);
        }catch(Exception ex){
            ResultClassEntity<List< CapacitacionPGMFAbreviadoEntity>> result = new ResultClassEntity<>();
            log.error("anexo - Capacitacion", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Miguel F. [07-07-2021]
     * @modificado:
     * @descripción: {obtener anexo11 PGMF}
     *
     */
    @PostMapping(path = "/PGMFAbreviadoDetalleRegistrar")
    @ApiOperation(value = "detalle registrar", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<String> PGMFAbreviadoDetalleRegistrar(@RequestBody PGMFAbreviadoDetalleRegistrarEntity param){
        try{
            return service.PGMFAbreviadoDetalleRegistrar(param);
        }catch(Exception ex){
            ResultClassEntity<String> result = new ResultClassEntity<>();
            log.error("PGMFAbreviado - PGMFAbreviadoDetalleRegistrar", "Ocurrió un error al generar en: "+ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }


    /**
     * @autor: Miguel F. [08-07-2021]
     * @modificado:
     * @descripción: {eliminar detalle PGMF}
     *
     */
    @PostMapping(path = "/detalleEliminar")
    @ApiOperation(value = "detalle eliminar", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public org.springframework.http.ResponseEntity PGMFAbreviadoDetalleEliminar(@RequestBody PGMFAbreviadoDetalleEntity param){
        log.info("Plan general manejo forestal - PGMFAbreviadoDetalleEliminar", param);
        ResponseEntity result = null;
        ResultClassEntity response ;

        try{
            response = service.PGMFAbreviadoDetalleEliminar(param);
            log.info("Plan general manejo forestal - PGMFAbreviadoDetalleEliminar","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch(Exception ex){
            response = new ResultClassEntity();
            log.error("Plan general manejo forestal - PGMFAbreviadoDetalleElimina", "Ocurrió un error al eliminar el requerimiento personal en: " + ex.getMessage());
            response.setInnerException(ex.getMessage());
            response.setSuccess(false);
            response.setData(null);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
