package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoSolicitudEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoSolicitudAccesoService;

import java.net.MalformedURLException;

@RestController
@RequestMapping(Urls.archivoSolicitud.BASE)
public class ArchivoSolicitudAccesoController {

    @Autowired
    private ArchivoSolicitudAccesoService archivoSolicitudService;


   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ArchivoSolicitudAccesoController.class);

    @PostMapping(path = "/cargarArchivo")
    @ApiOperation(value = "cargarArchivo", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Carga archivo correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity<ArchivoEntity>> insertarArchivoSolicitud(@RequestParam("file") MultipartFile file, @RequestParam(required = false) Integer idSolicitudAcceso,@RequestParam(required = false) String tipoArchivo) {
        log.info("ArchivoSolicitudAcceso - cargarArchivo", "Inicio cargarArchivo");
        ResultClassEntity<ArchivoEntity> entity = new ResultClassEntity<>();
        try {
            entity = archivoSolicitudService.insertarArchivoSolicitudAcceso(file, idSolicitudAcceso,tipoArchivo);
            log.info("ArchivoSolicitudAcceso - cargarArchivo", "Proceso realizado correctamente");
            return new ResponseEntity<>(entity, HttpStatus.OK);
        } catch (Exception e) {
            log.error("ArchivoSolicitudAcceso - cargarArchivo", "Ocurrió un error al cargar archivo en :" + e.getMessage());
            return new ResponseEntity<>(entity, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/descargarArchivo/{idSolicitudAcceso}")
    @ApiOperation(value = "descargarArchivo", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Descarga archivo correctamente"), @ApiResponse(code = 400, message = "No encontrado")})
    public ResponseEntity<ResultArchivoEntity> descargarArchivoSolicitud(@PathVariable Integer idSolicitudAcceso)  {
        log.info("ArchivoSolicitudAcceso - descargarArchivo", "Inicio descargarArchivo");
        ResultArchivoEntity entity = null;
        try {
            entity = archivoSolicitudService.descargarArchivoSolicitudAcceso(idSolicitudAcceso);
            return new ResponseEntity<>(entity,HttpStatus.OK);
        } catch (Exception e) {
            log.error("ArchivoSolicitudAcceso - descargarArchivo", "Ocurrió un error al descargar archivo en :" + e.getMessage());
            return new ResponseEntity<>(entity,  HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/obtenerArchivoSolicitud/{idArchivoSolicitud}")
    @ApiOperation(value = "obtenerArchivoSolicitud", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Descarga archivo correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity<ResultClassEntity<ArchivoEntity>> obtenerArchivoSolicitud(@PathVariable Integer idArchivoSolicitud)  {
        log.info("ArchivoSolicitudAcceso - obtenerArchivoSolicitud", "Inicio obtenerArchivoSolicitud");
        ResultClassEntity<ArchivoEntity> entity = new ResultClassEntity<>();
        try {
           entity = archivoSolicitudService.obtenerArchivoSolicitud(idArchivoSolicitud);
            return new ResponseEntity<>(entity,HttpStatus.OK);
        } catch (Exception e) {
            log.error("ArchivoSolicitudAcceso - obtenerArchivoSolicitud", "Ocurrió un error al consultar archivo en :" + e.getMessage());
            return new ResponseEntity<>(entity,  HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Abner Valdez [18-11-2021]
     * @modificado:
     * @descripción: {registrar detalle archivo}
     * @param:ArchivoSolicitudEntity
     */
    @PostMapping(path = "/registrarDetalleArchivo")
    @ApiOperation(value = "registrar detalle Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarDetalleArchivo(@RequestBody ArchivoSolicitudEntity item) {
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = archivoSolicitudService.registrarDetalleArchivo(item);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("ArchivoSolicitudAccesoController - registrarDetalleArchivo", "Ocurrió un error al guardar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [18-11-2021]
     * @descripción: {Listar listarDetalleArchivo}
     * @param: idSolicitud
     * @return: ResponseEntity<ResponseVO>
     */
    @GetMapping(path = "/listarDetalleArchivo/{idSolicitud}")
    @ApiOperation(value = "listarDetalleArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ArchivoSolicitudEntity>> listarDetalleArchivo(@PathVariable Integer idSolicitud) {
        ResultEntity<ArchivoSolicitudEntity> result = new ResultEntity<ArchivoSolicitudEntity>();
        try {
            result = archivoSolicitudService.listarDetalleArchivo(idSolicitud);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("ArchivoSolicitudAccesoController - listarDetalleArchivo",
                    "Ocurrió un error al listar en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Abner Valdez [18-11-2021]
     * @descripción: { eliminar un archivo detalle solicitud}
     * @param: Integer idArchivo
     * @param: Integer idUsuario
     */
    @GetMapping(path = "/eliminarDetalleArchivo/{idArchivo}/{idUsuario}")
    @ApiOperation(value = "Eliminar SolicitudArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<Integer>> eliminarDetalleArchivo(
        @PathVariable Integer idArchivo, @PathVariable Integer idUsuario) {
        ResultClassEntity<Integer> response = new ResultClassEntity<>();
        try {
            response = archivoSolicitudService.eliminarDetalleArchivo(idArchivo, idUsuario);
            log.info("Service - eliminar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("ArchivoSolicitudAccesoController - eliminarDetalleArchivo", e.getMessage());
            response.setError(Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
