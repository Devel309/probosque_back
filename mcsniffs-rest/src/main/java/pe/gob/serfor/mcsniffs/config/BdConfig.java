package pe.gob.serfor.mcsniffs.config;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

@Configuration
@EnableTransactionManagement
public class BdConfig {

    @Autowired
    private Environment env;

    /*Conexion a BDMCSNIFFS*/

    @Primary
    @Bean(name = "dataSourceBDMCSNIFFS")
    public DataSource datasourceBDMCSNIFFS() {
        if (env.getProperty("spring.datasource.jndi-mcsniffs") != null) {
            JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
            DataSource dataSource = dataSourceLookup.getDataSource(env.getProperty("spring.datasource.jndi-mcsniffs"));
            return dataSource;
        }
        else {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setUrl(env.getProperty("spring.datasource.SERFOR_BDMCSNIFFS.url"));
            dataSource.setUsername(env.getProperty("spring.datasource.SERFOR_BDMCSNIFFS.username"));
            dataSource.setPassword(env.getProperty("spring.datasource.SERFOR_BDMCSNIFFS.password"));
            dataSource.setDriverClassName(env.getProperty("spring.datasource.SERFOR_BDMCSNIFFS.driver-class-name"));
            return dataSource;
        }




    }

    @Primary
    @Bean(name = "entityManagerFactoryBDMCSNIFFS")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBDMCSNIFFS() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(datasourceBDMCSNIFFS());
        em.setPackagesToScan("pe.gob.serfor.mcsniffs.repository");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.show-sql", env.getProperty("jpa.show-sql"));
        properties.put("hibernate.dialect", env.getProperty("jpa.database-platform"));
    
        em.setJpaPropertyMap(properties);

        return em;

    }
    @Primary
    @Bean(name = "transactionManagerBDMCSNIFFS")
    public PlatformTransactionManager transactionManagerBDMCSNIFFS() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBDMCSNIFFS().getObject());

        return transactionManager;
    }



    /*Conexion a BDCORECENTRAL*/


    @Bean(name = "DataSourceBDCORECENTRAL")
    public DataSource datasourceBDCORECENTRAL() {

        if (env.getProperty("spring.datasource.jndi-corecentral") != null) {
            JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
            DataSource dataSource = dataSourceLookup.getDataSource(env.getProperty("spring.datasource.jndi-corecentral"));
            return dataSource;
        } else {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setUrl(env.getProperty("spring.datasource.SERFOR_BDCORECENTRAL.url"));
            dataSource.setUsername(env.getProperty("spring.datasource.SERFOR_BDCORECENTRAL.username"));
            dataSource.setPassword(env.getProperty("spring.datasource.SERFOR_BDCORECENTRAL.password"));
            dataSource.setDriverClassName(env.getProperty("spring.datasource.SERFOR_BDCORECENTRAL.driver-class-name"));

            return dataSource;
        }

    }

    @Bean(name = "EntityManagerFactoryBDCORECENTRAL")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBDCORECENTRAL() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(datasourceBDCORECENTRAL());
        em.setPackagesToScan("pe.gob.serfor.mcsniffs.repository");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.show-sql", env.getProperty("jpa.show-sql"));
        properties.put("hibernate.dialect", env.getProperty("jpa.database-platform"));

        em.setJpaPropertyMap(properties);

        return em;

    }

    @Bean(name = "transactionManagerBDCORECENTRAL")
    public PlatformTransactionManager transactionManagerBDCORECENTRAL() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBDCORECENTRAL().getObject());

        return transactionManager;
    }


    /*Conexion a BDSEGURIDAD*/
    @Bean(name = "DataSourceBDSEGURIDAD")
    public DataSource datasourceBDSEGURIDAD() {

        if (env.getProperty("spring.datasource.jndi-seguridad") != null) {
            JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
            DataSource dataSource = dataSourceLookup.getDataSource(env.getProperty("spring.datasource.jndi-seguridad"));
            return dataSource;
        } else {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setUrl(env.getProperty("spring.datasource.SERFOR_BDSEGURIDAD.url"));
            dataSource.setUsername(env.getProperty("spring.datasource.SERFOR_BDSEGURIDAD.username"));
            dataSource.setPassword(env.getProperty("spring.datasource.SERFOR_BDSEGURIDAD.password"));
            dataSource.setDriverClassName(env.getProperty("spring.datasource.SERFOR_BDSEGURIDAD.driver-class-name"));

            return dataSource;
        }

    }
    
        
    @Bean(name = "EntityManagerFactoryBDSEGURIDAD")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBDSEGURIDAD() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(datasourceBDSEGURIDAD());
        em.setPackagesToScan("pe.gob.serfor.mcsniffs.repository");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.show-sql", env.getProperty("jpa.show-sql"));
        properties.put("hibernate.dialect", env.getProperty("jpa.database-platform"));

        em.setJpaPropertyMap(properties);

        return em;

    }

    @Bean(name = "transactionManagerBDSEGURIDAD")
    public PlatformTransactionManager transactionManagerBDSEGURIDAD() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBDSEGURIDAD().getObject());

        return transactionManager;
    }

    
}
