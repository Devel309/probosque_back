package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Anexo1PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo2PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo3PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo4PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo5PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.Anexo6PFDMEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMAdjuntosEntity;
import pe.gob.serfor.mcsniffs.entity.AnexosPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.PostulacionPFDMResquestEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.PostulacionPFDMService;

@RestController
@RequestMapping(Urls.postulacionPFDM.BASE)
public class PostulacionPFDMController {
    /**
     * @autor: JaquelineDB [06-08-2021]
     * @modificado:
     * @descripción: {Repository de las postulacion pfdme}
     *
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PostulacionPFDMController.class);
    @Autowired
    private PostulacionPFDMService service;
    @Autowired
    private EmailService enviarcorreo;

    /**
     * @autor: JaquelineDB [06-08-2021]
     * @modificado:
     * @descripción: {Guardar postulacion}
     * @param:PostulacionPFDMEResquestEntity
     */
    @PostMapping(path = "/guardaPostulacion")
    @ApiOperation(value = "guarda Postulacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardaPostulacion(@RequestBody PostulacionPFDMResquestEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardaPostulacion(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - guardaPostulacion", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [09-08-2021]
     * @modificado:
     * @descripción: {Obtener anexo 1 pfdm}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/obtenerAnexo1PFDM")
    @ApiOperation(value = "obtener Anexo1PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<Anexo1PFDMEntity>> obtenerAnexo1PFDM(@RequestBody AnexosPFDMResquestEntity obj){
        ResultClassEntity<Anexo1PFDMEntity> result = new ResultClassEntity<Anexo1PFDMEntity>();
        try {
            result = service.obtenerAnexo1PFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - obtenerAnexo1PFDM", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [10-08-2021]
     * @modificado:
     * @descripción: {se adjuntan los archivos relacionados con la postulacion pfdm}
     * @param:ContratoAdjuntosRequestEntity
     */
    @PostMapping(path = "/adjuntarArchivosPostulacionPFDM")
    @ApiOperation(value = "Adjuntar Archivos PostulacionPFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> AdjuntarArchivosPostulacionPFDM(@RequestBody AnexosPFDMAdjuntosEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.AdjuntarArchivosPostulacionPFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - AdjuntarArchivosPostulacionPFDM", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-08-2021]
     * @modificado:
     * @descripción: {se registran los datos del anexo 2}
     * @param:Anexo2PFDMEntity
     */
    @PostMapping(path = "/guardarAnexo2")
    @ApiOperation(value = "guardar Anexo2", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarAnexo2(@RequestBody Anexo2PFDMEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardarAnexo2(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - guardarAnexo2", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [12-08-2021]
     * @modificado:
     * @descripción: {se registran los datos del anexo 3}
     * @param:Anexo3PFDMEntity
     */
    @PostMapping(path = "/guardarAnexo3")
    @ApiOperation(value = "guardar Anexo3", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarAnexo3(@RequestBody Anexo3PFDMEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardarAnexo3(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - guardarAnexo3", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [16-08-2021]
     * @modificado:
     * @descripción: {se obtienen los datos del anexo 2}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/obtenerAnexo2PFDM")
    @ApiOperation(value = "obtener Anexo2PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<Anexo2PFDMEntity>> obtenerAnexo2PFDM(@RequestBody AnexosPFDMResquestEntity obj){
        ResultClassEntity<Anexo2PFDMEntity> result = new ResultClassEntity<Anexo2PFDMEntity>();
        try {
            result = service.obtenerAnexo2PFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - obtenerAnexo2PFDM", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [16-08-2021]
     * @modificado:
     * @descripción: {se obtienen los datos del anexo 3}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/obtenerAnexo3PFDM")
    @ApiOperation(value = "obtener Anexo3PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<Anexo3PFDMEntity>> obtenerAnexo3PFDM(@RequestBody AnexosPFDMResquestEntity obj){
        ResultClassEntity<Anexo3PFDMEntity> result = new ResultClassEntity<Anexo3PFDMEntity>();
        try {
            result = service.obtenerAnexo3PFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - obtenerAnexo3PFDM", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [17-08-2021]
     * @modificado:
     * @descripción: {guardar anexo 4}
     * @param:Anexo4PFDMEntity
     */
    @PostMapping(path = "/guardarAnexo4")
    @ApiOperation(value = "guardar Anexo4", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarAnexo4(@RequestBody Anexo4PFDMEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardarAnexo4(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - guardarAnexo4", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [17-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 4}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/obtenerAnexo4PFDM")
    @ApiOperation(value = "obtener Anexo4PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<Anexo4PFDMEntity>> obtenerAnexo4PFDM(@RequestBody AnexosPFDMResquestEntity obj){
        ResultClassEntity<Anexo4PFDMEntity> result = new ResultClassEntity<Anexo4PFDMEntity>();
        try {
            result = service.obtenerAnexo4PFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - obtenerAnexo4PFDM", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [18-08-2021]
     * @modificado:
     * @descripción: {guardas los datos del anexo 5}
     * @param:Anexo5PFDMEntity
     */
    @PostMapping(path = "/guardarAnexo5")
    @ApiOperation(value = "guardar Anexo5", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarAnexo5(@RequestBody Anexo5PFDMEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardarAnexo5(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - guardarAnexo5", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [18-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 5}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/obtenerAnexo5PFDM")
    @ApiOperation(value = "obtener Anexo5PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<Anexo5PFDMEntity>> obtenerAnexo5PFDM(@RequestBody AnexosPFDMResquestEntity obj){
        ResultClassEntity<Anexo5PFDMEntity> result = new ResultClassEntity<Anexo5PFDMEntity>();
        try {
            result = service.obtenerAnexo5PFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - obtenerAnexo5PFDM", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [19-08-2021]
     * @modificado:
     * @descripción: {guardar datos del anexo 6}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/guardarAnexo6")
    @ApiOperation(value = "guardar Anexo6", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarAnexo6(@RequestBody Anexo6PFDMEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.guardarAnexo6(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - guardarAnexo6", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [19-08-2021]
     * @modificado:
     * @descripción: {obtener datos del anexo 6}
     * @param:AnexosPFDMResquestEntity
     */
    @PostMapping(path = "/obtenerAnexo6PFDM")
    @ApiOperation(value = "obtener Anexo6PFDM", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<Anexo6PFDMEntity>> obtenerAnexo6PFDM(@RequestBody AnexosPFDMResquestEntity obj){
        ResultClassEntity<Anexo6PFDMEntity> result = new ResultClassEntity<Anexo6PFDMEntity>();
        try {
            result = service.obtenerAnexo6PFDM(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PostulacionPFDMController - obtenerAnexo6PFDM", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

}
