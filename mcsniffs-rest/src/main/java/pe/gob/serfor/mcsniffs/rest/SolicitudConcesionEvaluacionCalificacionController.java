package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionCalificacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionEvaluacionCalificacionService;

@RestController
@RequestMapping(Urls.solicitudConcesionEvaluacionCalificacion.BASE)
public class SolicitudConcesionEvaluacionCalificacionController extends AbstractRestController {
        private static final org.apache.logging.log4j.Logger log = LogManager
                        .getLogger(SolicitudConcesionEvaluacionCalificacionController.class);

        @Autowired
        private SolicitudConcesionEvaluacionCalificacionService solicitudConcesionEvaluacionCalificacionService;

        @PostMapping(path = "/listarSolicitudConcesionEvaluacionCalificacion")
        @ApiOperation(value = "listar Calificacion de Solicitud Concesion", authorizations = {
                        @Authorization(value = "JWT") })
        @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
                        @ApiResponse(code = 404, message = "No encontrado") })
        public ResponseEntity listarSolicitudConcesionEvaluacionCalificacion(
                        @RequestBody SolicitudConcesionEvaluacionCalificacionDto request) {
                log.info("SolicitudConcesionEvaluacionCalificacion - listarSolicitudConcesionEvaluacionCalificacion",
                                request.toString());
                ResultClassEntity response = new ResultClassEntity<>();
                try {
                        response = solicitudConcesionEvaluacionCalificacionService
                                        .listarSolicitudConcesionEvaluacionCalificacion(request);
                        log.info("SolicitudConcesionEvaluacionCalificacion - listarSolicitudConcesionEvaluacionCalificacion",
                                        "Proceso realizado correctamente");
                        if (!response.getSuccess()) {
                                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                        } else {
                                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
                        }
                } catch (Exception e) {
                        log.error("SolicitudConcesionEvaluacionCalificacion - listarSolicitudConcesionEvaluacionCalificacion",
                                        "Ocurrió un error : " + e.getMessage());
                        response.setSuccess(Constantes.STATUS_ERROR);
                        response.setMessage(Constantes.MESSAGE_ERROR_500);
                        response.setStackTrace(Arrays.toString(e.getStackTrace()));
                        response.setMessageExeption(e.getMessage());
                        return new org.springframework.http.ResponseEntity<>(response,
                                        HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }

        @PostMapping(path = "/registrarSolicitudConcesionEvaluacionCalificacion")
        @ApiOperation(value = "Registrar Calificación de la Solicitud Concesión", authorizations = {
                        @Authorization(value = "JWT") })
        @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
                        @ApiResponse(code = 404, message = "No encontrado") })
        public ResponseEntity registrarSolicitudConcesionEvaluacionCalificacion(
                        @RequestBody SolicitudConcesionEvaluacionDto request) {
                log.info("SolicitudConcesionEvaluacionCalificacion - registrarSolicitudConcesionEvaluacionCalificacion",
                                request.toString());

                ResultClassEntity response = new ResultClassEntity<>();

                try {
                        response = solicitudConcesionEvaluacionCalificacionService
                                        .registrarSolicitudConcesionEvaluacionCalificacion(request);
                        log.info("SolicitudConcesionEvaluacionCalificacion - registrarSolicitudConcesionEvaluacionCalificacion",
                                        "Proceso realizado correctamente");
                        if (!response.getSuccess()) {
                                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                        } else {
                                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
                        }
                } catch (Exception e) {
                        log.error("SolicitudConcesionEvaluacionCalificacion - registrarSolicitudConcesionEvaluacionCalificacion",
                                        "Ocurrió un error :" + e.getMessage());
                        response.setSuccess(Constantes.STATUS_ERROR);
                        response.setMessage(Constantes.MESSAGE_ERROR_500);
                        response.setStackTrace(Arrays.toString(e.getStackTrace()));
                        response.setMessageExeption(e.getMessage());
                        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

                }
        }

        @PostMapping(path = "/actualizarSolicitudConcesionEvaluacionCalificacion")
        @ApiOperation(value = "Actualizar Informacion Califiación Solicitud Concesion", authorizations = {
                        @Authorization(value = "JWT") })
        @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
                        @ApiResponse(code = 404, message = "No encontrado") })
        public ResponseEntity actualizarSolicitudConcesionEvaluacionCalificacion(
                        @RequestBody SolicitudConcesionEvaluacionDto request) {
                log.info("SolicitudConcesionEvaluacionCalificacion - actualizarSolicitudConcesionEvaluacionCalificacion",
                                request.toString());

                ResultClassEntity response = new ResultClassEntity<>();

                try {
                        response = solicitudConcesionEvaluacionCalificacionService
                                        .actualizarSolicitudConcesionEvaluacionCalificacion(request);
                        log.info("SolicitudConcesionEvaluacionCalificacion - actualizarSolicitudConcesionEvaluacionCalificacion",
                                        "Proceso realizado correctamente");
                        if (response.getSuccess()) {
                                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
                        } else {
                                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                        }
                } catch (Exception e) {
                        log.error("SolicitudConcesionEvaluacionCalificacion - actualizarSolicitudConcesionEvaluacionCalificacion",
                                        "Ocurrió un error :" + e.getMessage());
                        response.setSuccess(Constantes.STATUS_ERROR);
                        response.setMessage(Constantes.MESSAGE_ERROR_500);
                        response.setStackTrace(Arrays.toString(e.getStackTrace()));
                        response.setMessageExeption(e.getMessage());
                        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

                }
        }
}
