package pe.gob.serfor.mcsniffs.rest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import pe.gob.serfor.mcsniffs.entity.InformacionGeneralPlanificacionBosqueEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.InformacionGeneralPlanificacionBosqueService;


/**
 * @autor: Harry Coa [02-08-2021]
 * @modificado:
 * @descripción: { controller}
 * @param:OrdenamientoProteccionEntity
 */
@RestController
@RequestMapping(Urls.informacionGeneralPlanificacion.BASE)
public class InformacionGeneralPlanificacionBosqueController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(InformacionGeneralPlanificacionBosqueController.class);

    @Autowired
    private InformacionGeneralPlanificacionBosqueService infoService;

    @PostMapping(path = "/registrarInfoGeneral")
    @ApiOperation(value = "registrarInfoGeneral" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarInfoGeneral(@RequestBody InformacionGeneralPlanificacionBosqueEntity entidad){
        log.info("EvaluacionAmbiental - registrarRegente", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = infoService.RegistrarInformacionGeneral(entidad);
            log.info("EvaluacionAmbiental - registrarRegente","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("EvaluacionAmbiental - registrarRegente","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
