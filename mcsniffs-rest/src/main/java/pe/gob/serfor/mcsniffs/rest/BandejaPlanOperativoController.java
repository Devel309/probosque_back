package pe.gob.serfor.mcsniffs.rest;


import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PlanManejoService;


@RestController 
@RequestMapping(Urls.bandejaPlanOperativo.BASE)
public class BandejaPlanOperativoController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudController.class);

    @Autowired
    private PlanManejoService planManejoService;

 

    @PostMapping(path = "/listarPlanManejo")
    @ApiOperation(value = "listarPlanManejo por idContrato", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<PlanManejoEntity>> ListarPlanManejo(
            @RequestBody PlanManejoEntity planManejoEntity) {
        log.info("Listar ListarPlanManejo: {}", planManejoEntity);
        ResultClassEntity<PlanManejoEntity> response = new ResultClassEntity<>();
        try {
            response = planManejoService.ListarPlanManejo(planManejoEntity);
            log.info("Service - ListarPlanManejo: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - ListarPlanManejo: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarPlanManejo")
    @ApiOperation(value = "registrarPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPlanManejo(@RequestBody PlanManejoEntity planManejoEntity){
        log.info("Bandeja Plan operativo - registrarPlanManejo", planManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = planManejoService.RegistrarPlanManejo(planManejoEntity);
            log.info("Bandeja Plan operativo - registrarPlanManejo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("Bandeja Plan operativo -registrarPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @PostMapping(path = "/actualizarPlanManejo")
    @ApiOperation(value = "actualizarPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarPlanManejo(@RequestBody PlanManejoEntity planManejoEntity){
        log.info("Bandeja Plan operativo - actualizarPlanManejo", planManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = planManejoService.ActualizarPlanManejo(planManejoEntity);
            log.info("Bandeja Plan operativo - actualizarPlanManejo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("Bandeja Plan operativo - actualizarPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/eliminarPlanManejo")
    @ApiOperation(value = "eliminarPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarPlanManejo(@RequestBody PlanManejoEntity planManejoEntity){
        log.info("Bandeja Plan operativo - eliminarPlanManejo", planManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = planManejoService.EliminarPlanManejo(planManejoEntity);
            log.info("Bandeja Plan operativo - eliminarPlanManejo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        }catch (Exception e){
            log.error("Bandeja Plan operativo - eliminarPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
