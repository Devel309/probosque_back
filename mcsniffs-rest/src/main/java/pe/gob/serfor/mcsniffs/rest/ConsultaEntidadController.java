package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.ConsultaEntidadEntity;
import pe.gob.serfor.mcsniffs.entity.ConsultaUAEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.TipoProcesoNivel1Entity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ConsultaEntidadService;

@RestController
@RequestMapping(Urls.consultaEntidad.BASE)
public class ConsultaEntidadController {
    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {repositorio de la tabla Consulta entidad}
     *
     */

    @Autowired
    private ConsultaEntidadService serv;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ConsultaEntidadController.class);

    /**
     * @autor: JaquelineDB [11-06-2021]
     * @modificado:
     * @descripción: {Lista las entidades a las cuales se les puede hacer consultas}
     * @param:
     */
    @GetMapping(path = "/listarEntidades")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<ConsultaEntidadEntity>> listarEntidades(){
        ResultEntity<ConsultaEntidadEntity> lst_entidades = new ResultEntity<ConsultaEntidadEntity>();
        try {
            lst_entidades =serv.listarEntidades();
            return new ResponseEntity<>(lst_entidades, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("TipoProcesoController - ListarTipoProceso", "Ocurrió un error al Listar en: "+Ex.getMessage());
            lst_entidades.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_entidades,HttpStatus.FORBIDDEN);
        }
    }
}
