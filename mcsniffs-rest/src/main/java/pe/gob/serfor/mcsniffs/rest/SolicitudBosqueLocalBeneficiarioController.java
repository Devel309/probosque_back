package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudBosqueLocalBeneficiarioEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.DecargarPDFSolBosqueLocalBeneficiario.DescargarPDFSolBeneficiario;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoPDFSolicitudBosqueLocalBeneficiarioService;
import pe.gob.serfor.mcsniffs.service.SolicitudBosqueLocalBeneficiarioService;


@RestController
@RequestMapping(Urls.solicitudBosqueLocal.BASE)
public class SolicitudBosqueLocalBeneficiarioController extends AbstractRestController{
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudBosqueLocalBeneficiarioController.class);

    @Autowired
    private SolicitudBosqueLocalBeneficiarioService solicitudBosqueLocalBeneficiarioService;

    @Autowired
    ArchivoPDFSolicitudBosqueLocalBeneficiarioService archivoPDFSolicitudBosqueLocalBeneficiarioService;

    @PostMapping(path = "/listarSolicitudBosqueLocalBeneficiario")
    @ApiOperation(value = "listar Actividad de Solicitud Bosque Local beneficiario" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudBosqueLocalBeneficiario(@RequestBody SolicitudBosqueLocalBeneficiarioEntity request) {
        log.info("SolicitudBosqueLocal - listarSolicitudConcesionBeneficiario", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudBosqueLocalBeneficiarioService.listarSolicitudBosqueLocalBeneficiario(request);
            log.info("SolicitudBosqueLocal- listarSolicitudBosqueLocalBeneficiario", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudBosqueLocal- listarSolicitudConcesionBeneficiario", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


  /*  @PostMapping(path = "/registrarSolicitudBosqueLocalBeneficiario")
    @ApiOperation(value = "Registrar Actividad de la Solicitud Bosque Local Beneficiario" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudBosqueLocalBeneficiario(@RequestBody SolicitudBosqueLocalBeneficiarioEntity request){
        log.info("SolicitudBosqueLocal - registrarSolicitudBosqueLocalBeneficiario",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalBeneficiarioService.registrarSolicitudBosqueLocalBeneficiario(request);
            log.info("SolicitudBosqueLocal - registrarSolicitudBosqueLocalBeneficiario","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - registrarSolicitudBosqueLocalBeneficiario","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }*/

 
    @PostMapping(path = "/registrarSolicitudBosqueLocalBeneficiario")
    @ApiOperation(value = "Registrar Actividad de la Solicitud Bosque Local Beneficiario" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarSolicitudBosqueLocalBeneficiario(@RequestBody List<SolicitudBosqueLocalBeneficiarioEntity> SolicitudBosqueLocalBeneficiarioEntity){
        log.info("InformacionAreaManejo - perfilOpcionAccionRegistrar",SolicitudBosqueLocalBeneficiarioEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = solicitudBosqueLocalBeneficiarioService.registrarSolicitudBosqueLocalBeneficiario(SolicitudBosqueLocalBeneficiarioEntity);
            log.info("InformacionAreaManejo - registrarSolicitudBosqueLocalBeneficiario","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - registrarSolicitudBosqueLocalBeneficiario","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
  

    @PostMapping(path = "/eliminarSolicitudBosqueLocalBeneficiario")
    @ApiOperation(value = "Eliminar Actividad de la Solicitud Bosque Local Beneficiario" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudBosqueLocalBeneficiario(@RequestBody SolicitudBosqueLocalBeneficiarioEntity request){
        log.info("SolicitudBosqueLocal - eliminarSolicitudBosqueLocalBeneficiario",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudBosqueLocalBeneficiarioService.eliminarSolicitudBosqueLocalBeneficiario(request);
            log.info("SolicitudBosqueLocal - eliminarSolicitudBosqueLocalBeneficiario","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudBosqueLocal - eliminarSolicitudBosqueLocalBeneficiario","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/descargarArchivoPDFSolicitudBosqueLocalBeneficiario")
    @ApiOperation(value = "descargarArchivoPDFSolicitudBosqueLocalBeneficiario", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> ArchivoPDFSolicitudBosqueLocalBeneficiario(@RequestBody DescargarPDFSolBeneficiario obj){ //, HttpServletRequest request1
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            //String token= request1.getHeader("Authorization");
            ByteArrayResource bytes = archivoPDFSolicitudBosqueLocalBeneficiarioService.ArchivoPDFSolicitudBosqueLocalBeneficiario(obj);
            log.info("Service - PDF SolicitudBosqueLocalBeneficiario: Proceso realizado correctamente");
            result.setArchivo(bytes.getByteArray());
            result.setNombeArchivo("ArchivoPDFSolicitudBosqueLocalBeneficiario.pdf");
            result.setContenTypeArchivo("application/octet-stream");
            result.setSuccess(true);
            result.setMessage("Se generó el PDF de Solicitud Bosque Local Beneficiario");
            log.error("Service - Se generó el PDF de Solicitud Bosque Local Beneficiario correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("Bosque Local Beneficiario  - Descargar Bosque Local Beneficiario PDF", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

 
}
