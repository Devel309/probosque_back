package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.Parametro.ProteccionBosqueDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.RecursoForestalDetalleDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalEntity;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PotencialProduccionForestalService;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;

import java.util.List;

@RestController
@RequestMapping(Urls.potencialProduccionForestal.BASE)
public class PotencialProduccionForestalController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PotencialProduccionForestalController.class);
    
    @Autowired
    private PotencialProduccionForestalService service;

     /**
     * @autor:  Abner Valdez [31-08-2021]
     * @descripción: {Registrar PotencialProduccionForestalMaderable}
     * @param: PotencialProduccionForestalEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrar")
    @ApiOperation(value = "registrar" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrar(@RequestParam(required = false, name = "file") MultipartFile file,
    @RequestParam(required = true) Integer idPlanManejo,
    @RequestParam(required = true) Integer idPotProdForestal,
    String codigoTipoPotProdForestal,
    String especie,
    String variable,
    String anexo,
    String disenio,
    Double diametroMinimoInventariadaCM,
    Double tamanioParcela,
    Integer nroParcela,
    Double distanciaParcela,
    Double totalAreaInventariada,
    Double rangoDiametroCM,
    Double areaMuestreada,
    String metodoMuestreo,
    Double intensidadMuestreoPorcentaje,
    Double errorMuestreoPorcentaje,
    @RequestParam(required = true) Integer idUsuario) throws Exception{
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            PlanManejoEntity planManejo = new PlanManejoEntity();
            planManejo.setIdPlanManejo(idPlanManejo);
            PotencialProduccionForestalEntity item = new PotencialProduccionForestalEntity();
            item.setPlanManejo(planManejo);
            item.setIdPotProdForestal(idPotProdForestal);
            item.setCodigoTipoPotProdForestal(codigoTipoPotProdForestal);
            item.setEspecie(especie);
            item.setVariable(variable);
            item.setAnexo(anexo);
            item.setDisenio(disenio);
            item.setDiametroMinimoInventariadaCM(diametroMinimoInventariadaCM);
            item.setTamanioParcela(tamanioParcela);
            item.setNroParcela(nroParcela);
            item.setDistanciaParcela(distanciaParcela);
            item.setTotalAreaInventariada(totalAreaInventariada);
            item.setRangoDiametroCM(rangoDiametroCM);
            item.setAreaMuestreada(areaMuestreada);
            item.setMetodoMuestreo(metodoMuestreo);
            item.setIntensidadMuestreoPorcentaje(intensidadMuestreoPorcentaje);
            item.setErrorMuestreoPorcentaje(errorMuestreoPorcentaje);
            item.setIdUsuarioRegistro(idUsuario);
            response = service.registrar(item,file);
            log.info("PotencialProduccionForestalController - registrar","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PotencialProduccionForestalController -registrar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     /**
     * @autor:  Abner Valdez [31-08-2021]
     * @descripción: {Listar Listar Potencial Produccion Forestal Maderable }
     * @param: idPlanManejo
     * @return: ResponseEntity<ResponseVO>
     */
    @GetMapping(path = "/listar/{idPlanManejo}")
    @ApiOperation(value = "listar" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarUnidadFisiografica(@PathVariable Integer idPlanManejo){
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = service.listar(idPlanManejo);
            log.info("PotencialProduccionForestalController - Listar","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PotencialProduccionForestalController -listar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [16/10-2021]
     * @modificado: Rafael Azaña [18/10-2021]
     * @descripción: {LISTAR POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */

    @PostMapping(path = "/listarPotencialProducForestal")
    @ApiOperation(value = "Listar PotencialProducForestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<PotencialProduccionForestalEntity>> listarPotencialProducForestal(
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(name = "codigo") String codigo    ) {
        try {
            return service.ListarPotencialProducForestal(idPlanManejo,codigo);
        } catch (Exception ex) {

            ResultClassEntity<List<PotencialProduccionForestalEntity>> result = new ResultClassEntity<>();
            log.error("PotencialProduccionForestalController - listarPotencialProducForestal", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [24-01-2022]
     * @descripción: {LISTAR POTENCIAL FORESTAL TITULAR}
     * @param:PotencialProduccionForestalDto
     */

    @PostMapping(path = "/listarPotencialProducForestalTitular")
    @ApiOperation(value = "Listar PotencialProducForestal Titular", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<PotencialProduccionForestalEntity>> listarPotencialProducForestalTitular(
            @RequestParam(required = true) String tipoDocumento,
            @RequestParam(required = true) String nroDocumento,
            @RequestParam(required = true) String codigoProcesoTitular,
            @RequestParam(required = true) Integer idPlanManejo,
            @RequestParam(required = true) String codigoProceso) {
        try {
            return service.ListarPotencialProducForestalTitular(tipoDocumento,nroDocumento,codigoProcesoTitular,idPlanManejo,codigoProceso);
        } catch (Exception ex) {

            ResultClassEntity<List<PotencialProduccionForestalEntity>> result = new ResultClassEntity<>();
            log.error("PotencialProduccionForestalController - listarPotencialProducForestal", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [18/10-2021]
     * @modificado:
     * @descripción: {REGISTRAR Y ACTUALIZAR POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */

    @PostMapping(path = "/registrarPotencialProducForestal")
    @ApiOperation(value = "registrar PotencialProducForestal", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPotencialProducForestal(
            @RequestBody List<PotencialProduccionForestalEntity> list) {
        log.info("registrarPotencialProducForestal - registrarPotencialProducForestal", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = service.RegistrarPotencialProducForestal(list);
            log.info("PotencialProduccionForestalController - registrarPotencialProducForestal", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("PotencialProduccionForestalController -registrarPotencialProducForestal", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Rafael Azaña [18/10-2021]
     * @modificado:
     * @descripción: {ELIMINAR POTENCIAL FORESTAL}
     * @param:PotencialProduccionForestalDto
     */

    @PostMapping(path = "/eliminarPotencialProducForestal")
    @ApiOperation(value = "eliminarPotencialProducForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarPotencialProducForestal(@RequestBody PotencialProduccionForestalDto potencialProduccionForestalDto){
        log.info("PotencialProducForestal - eliminarPotencialProducForestal",potencialProduccionForestalDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = service.EliminarPotencialProducForestal(potencialProduccionForestalDto);
            log.info("PotencialProducForestal - eliminarPotencialProducForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("PotencialProducForestal - eliminarPotencialProducForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Danny Nazario [28-12-2021]
     * @modificado:
     * @descripción: {Registra Potencial Producción Recurso Forestal Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarPotencialProduccionExcel")
    @ApiOperation(value = "registrarPotencialProduccionExcel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPotencialProduccionExcel(@RequestParam("file") MultipartFile file,
                                                                                     @RequestParam("nombreHoja") String nombreHoja,
                                                                                     @RequestParam("numeroFila") Integer numeroFila,
                                                                                     @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                     @RequestParam("codigoTipo") String codigoTipo,
                                                                                     @RequestParam("codigoSubTipo") String codigoSubTipo,
                                                                                     @RequestParam("codigoTipoDet") String codigoTipoDet,
                                                                                     @RequestParam("codigoSubTipoDet") String codigoSubTipoDet,
                                                                                     @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                     @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        log.info("PotencialProduccionForestal - registrarPotencialProduccionExcel");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarPotencialProduccionExcel(file, nombreHoja, numeroFila, numeroColumna, codigoTipo, codigoSubTipo, codigoTipoDet, codigoSubTipoDet, idPlanManejo, idUsuarioRegistro);
            if (result.getSuccess()) {
                log.info("PotencialProduccionForestal - registrarPotencialProduccionExcel", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("PotencialProduccionForestal - registrarPotencialProduccionExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
