package pe.gob.serfor.mcsniffs.rest;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.config.TokenConfig;
import pe.gob.serfor.mcsniffs.entity.SeguridadEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SeguridadService;

@RestController
@RequestMapping(Urls.autenticacion.BASE)
public class SeguridadController {
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private SeguridadService login;

	private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SeguridadController.class);

    @Autowired
    private TokenConfig tokenConfig;

    @PostMapping(path = "/token")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<SeguridadEntity> Token(@RequestBody SeguridadEntity seguridad, HttpServletRequest request1){
		log.info("SeguridadApi - Token - Inicio Token");
    	try {
			SeguridadEntity usuariologueado = login.LoginUser(seguridad);
			if(usuariologueado.getUsername() !=null) {
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(usuariologueado.getUsername(),usuariologueado.getPassword()));
    			 UserDetails userDetails= new User(usuariologueado.getUsername(),usuariologueado.getPassword(),new ArrayList<>());
        		 String token = tokenConfig.genToken(userDetails);
				 seguridad.setMensaje("Token generado!!");
				 log.info("SeguridadApi - Token", "Token generado");
        		 return  new ResponseEntity<>( new SeguridadEntity("","",token,"Token generado") ,HttpStatus.OK );
    		 }else {
				 log.warn("SeguridadApi - Token", "Las credenciales son incorrectas " + usuariologueado.getMensaje());
    			 return new ResponseEntity<>( new SeguridadEntity("","","","Las credenciales son incorrectas " + usuariologueado.getMensaje()) ,HttpStatus.BAD_REQUEST );
    		 }
	    } catch (BadCredentialsException Ex){
			log.error("SeguridadApi - Token", "Ocurrió un error al generar el token en: "+Ex.getMessage());
    		return new ResponseEntity<>( new SeguridadEntity(seguridad.getUsername(),seguridad.getPassword(),"","Ocurrió un error al generar el token en: "+Ex.getMessage()),HttpStatus.BAD_REQUEST);
    	}
    }
}
