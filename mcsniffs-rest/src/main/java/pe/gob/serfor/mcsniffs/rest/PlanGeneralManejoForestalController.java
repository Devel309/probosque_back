package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.PlanGeneralManejoForestalEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoContratoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.ContratoEntity;
import pe.gob.serfor.mcsniffs.entity.UbigeoArffsEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal.PGMFEnvioCorreo;
import pe.gob.serfor.mcsniffs.entity.Parametro.PlanManejoForestalContratoDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.PlanGeneralManejoForestalService;

import javax.servlet.http.HttpServletRequest;

/*
 *  A partir del 02/09/2021, Este controlador no se debe actualizar ni utilizar,
 * para trabajar con planes de manejo utilizar el controlador: PlanManejoController
 */

@RestController
@RequestMapping(Urls.planGeneralManejoForestal.BASE)
public class PlanGeneralManejoForestalController {
    /**
     * @autor: JaquelineDB [27-08-2021]
     * @modificado: 
     * @descripción: {repositorio del plan de manejo forestal}
     */
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PlanGeneralManejoForestalController.class);

    @Autowired  
    private PlanGeneralManejoForestalService planGeneralManejoForestalService;


     /**
     * @autor: JaquelineDB [27-08-2021]
     * @modificado: 
     * @descripción: {inserta el plan de manejo forestal}
     * @param:PlanGeneralManejoForestalEntity
     */
    @PostMapping(path = "/registrarPlanManejoForestal")
    @ApiOperation(value = "registra un Plan Manejo Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> registrarPlanManejoForestal(@RequestBody List<PlanGeneralManejoForestalEntity> list){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planGeneralManejoForestalService.registrarPlanManejoForestal(list);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanGeneralManejoForestalController - registrarPlanManejoForestal", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Harry Coa [31-08-2021]
     * @modificado:
     * @descripción: { ontiene el contrato seleccionado para PGMF}
     * @param: idContrato
     */
    @PostMapping(path = "/obtenerContrato")
    @ApiOperation(value = "Obtener Contrato por idContrato", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> obtenerContrato(@RequestBody ContratoEntity dto) {
        log.info("obtener: {}", dto.getIdContrato());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = planGeneralManejoForestalService.obtenerContrato(dto);
            log.info("Service - obtener: Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            }


        } catch (Exception e) {
            log.error("Service - obtener: Ocurrió un error: {}", e.getMessage());
            response.setError(Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Harry Coa [31-08-2021]
     * @modificado:
     * @descripción: { Lista los contratos  para PGMF}
     * @param: idContrato
     */
    @GetMapping(path = "/listarContrato")
    @ApiOperation(value = "listarContrato", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<List<PlanManejoForestalContratoDto>>> listarContrato() {
        log.info("listar");
        ResultClassEntity<List<PlanManejoForestalContratoDto>> response = new ResultClassEntity<>();        try {
            response = planGeneralManejoForestalService.listarFiltroContrato();
            log.info("Service - listar: Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Service - listar: Ocurrió un error: {}", e.getMessage());
            response.setError(Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [31-08-2021]
     * @modificado: 
     * @descripción: {lista los contratos vigentes sin plan de manejo}
     * @param:idContrato
     */
    @GetMapping(path = "/listarContratosVigentes/{idContrato}")
    @ApiOperation(value = "listar Contratos Vigentes", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<PlanManejoContratoEntity>> listarContratosVigentes(@PathVariable("idContrato") Integer idContrato){
        ResultEntity<PlanManejoContratoEntity> result = new ResultEntity<PlanManejoContratoEntity>();
        try {
            if(idContrato.equals(0)){idContrato=null;}
            result = planGeneralManejoForestalService.listarContratosVigentes(idContrato);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanGeneralManejoForestalController- listarContratosVigentes", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [31-08-2021]
     * @modificado: 
     * @descripción: {actualizar el plan de manejo forestal}
     * @param:PlanGeneralManejoForestalEntity
     */
    @PostMapping(path = "/actualizarPlanManejoForestal")
    @ApiOperation(value = "actualizar Plan Manejo Forestal", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> actualizarPlanManejoForestal(@RequestBody PlanGeneralManejoForestalEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planGeneralManejoForestalService.actualizarPlanManejoForestal(obj);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanGeneralManejoForestalController - actualizarPlanManejoForestal", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }


    @PostMapping(path = "/enviarEvaluacion")
    @ApiOperation(value = "enviarEvaluacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> enviarEvaluacion(@RequestBody PGMFEnvioCorreo obj, HttpServletRequest request1){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = planGeneralManejoForestalService.enviarEvaluacion(obj,request1);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanGeneralManejoForestalController - enviarEvaluacion", "Ocurrió un error al guardar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [29-09-2021]
     * @modificado: 
     * @descripción: {listar el ubigeo de un contrato}
     * @param:idContrato
     */
    @GetMapping(path = "/listarUbigeoContratos/{idContrato}")
    @ApiOperation(value = "listar ubigeo del Contratos Vigentes", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<UbigeoArffsEntity>> listarUbigeoContratos(@PathVariable("idContrato") Integer idContrato){
        ResultEntity<UbigeoArffsEntity> result = new ResultEntity<UbigeoArffsEntity>();
        try {
            if(idContrato.equals(0)){idContrato=null;}
            result = planGeneralManejoForestalService.listarUbigeoContratos(idContrato);
            return new ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("PlanGeneralManejoForestalController- listarUbigeoContratos", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
}
