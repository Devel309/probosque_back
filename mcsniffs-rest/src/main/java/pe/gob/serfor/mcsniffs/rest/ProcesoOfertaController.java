package pe.gob.serfor.mcsniffs.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ProcesoOfertaService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(Urls.procesoOferta.BASE)
public class ProcesoOfertaController {
    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Servicio de Proceso oferta en esta clase consultan
     * todo lo referente a esta tabla}
     *
     */

    @Autowired
    private ProcesoOfertaService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TipoProcesoController.class);

    /**
     * @autor: JaquelineDB [13-06-2021]
     * @modificado:
     * @descripción: {Guarda e proceso en oferta}
     * @param:ProcesoOfertaEntity
     */
    @PostMapping(path = "/guardarProcesoOferta")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarProcesoOferta(@RequestBody ProcesoOfertaEntity obj){
        ResultClassEntity lst_ua = new ResultClassEntity();
        try {
            lst_ua = service.guardarProcesoOferta(obj);
            return new ResponseEntity<>(lst_ua, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ProcesoOfertaController - guardarProcesoOferta", "Ocurrió un error al guardar en: "+Ex.getMessage());
            lst_ua.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_ua,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [15-06-2021]
     * @modificado:
     * @descripción: {Lista los procesos en oferta}
     * @param:ProcesoOfertaRequestEntity
     */
    @PostMapping(path = "/listarProcesoOferta")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> listarProcesoOferta(@RequestBody ProcesoOfertaRequestEntity obj){
        ResultClassEntity lst_po = new ResultClassEntity();
        try {
            lst_po = service.listarProcesoOferta(obj);
            return new ResponseEntity<>(lst_po, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ProcesoOfertaController - listarProcesoOferta", "Ocurrió un error al guardar en: "+Ex.getMessage());
            lst_po.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_po,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [15-06-2021]
     * @modificado:
     * @descripción: {Actualiza el estatus del proceso en oferta}
     * @param:ProcesoOfertaRequestEntity
     */
    @PostMapping(path = "/actualizarEstatusProcesoOferta")
    @ApiOperation(value = "actualizar Estatus ProcesoOferta", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> actualizarEsttatusProcesoOferta(@RequestBody EstatusProcesoPostulacionEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarEsttatusProcesoOferta(obj.getIdProcesoOferta(), obj.getIdStatus(), obj.getIdUsuarioModificacion());
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("procesooferta - actualizarEsttatusProcesoOferta", "Ocurrió un error al aprobar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: Julio Meza [16-11-2021]
     * @modificado:
     * @descripción: {Elimina un Proceso de oferta, tb elimina el registro de las uas seleccionadas}
     * @param:ProcesoOfertaRequestEntity
     */
    @PostMapping(path = "/eliminarProcesoOferta")
    @ApiOperation(value = "Elimina Proceso Oferta", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> eliminarProcesoOferta(@RequestBody ProcesoOfertaRequestEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarProcesoOferta(obj);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("procesooferta - eliminarProcesoOferta", "Ocurrió un error en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

   
}
