package pe.gob.serfor.mcsniffs.rest;


import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.LibroOperacionesService;


@RestController
@RequestMapping(Urls.libroOperaciones.BASE)
public class LibroOperacionesController extends AbstractRestController {

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(LibroOperacionesController.class);

    @Autowired
    private LibroOperacionesService libroOperacionesService;

    @PostMapping(path = "/listarLibroOperaciones")
    @ApiOperation(value = "listarLibroOperaciones" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarManejoBosqueActividad(@RequestBody LibroOperacionesDto dto){
        log.info("LibroOperaciones - listarLibroOperaciones",dto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.listarLibroOperaciones(dto);
            log.info("LibroOperaciones - listarLibroOperaciones","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("LibroOperaciones -listarLibroOperaciones","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarLibroOperaciones")
    @ApiOperation(value = "registrarLibroOperaciones" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarLibroOperaciones(@RequestBody LibroOperacionesDto obj){
        log.info("Libro Operaciones - registrarLibroOperaciones",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.registrarLibroOperaciones(obj);
            log.info("Libro Operaciones - registrarLibroOperaciones","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Libro Operaciones - registrarLibroOperaciones","Ocurrió un error :"+ e.getMessage());

            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarLibroOperaciones")
    @ApiOperation(value = "actualizarLibroOperaciones" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarLibroOperaciones(@RequestBody LibroOperacionesDto obj){
        log.info("Libro Operaciones - actualizarLibroOperaciones",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.actualizarLibroOperaciones(obj);
            log.info("Libro Operaciones - actualizarLibroOperaciones","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Libro Operaciones - actualizarLibroOperaciones","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarLibroOperaciones")
    @ApiOperation(value = "eliminarLibroOperaciones" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarLibroOperaciones(@RequestBody LibroOperacionesDto obj){
        log.info("Libro Operaciones - eliminarLibroOperaciones",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.eliminarLibroOperaciones(obj);
            log.info("Libro Operaciones - eliminarLibroOperaciones","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Libro Operaciones - eliminarLibroOperaciones","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/validarNroRegistroLibroOperaciones")
    @ApiOperation(value = "validarNroRegistroLibroOperaciones" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity validarNroRegistroLibroOperaciones(@RequestBody LibroOperacionesDto obj){
        log.info("Libro Operaciones - validarNroRegistroLibroOperaciones",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.validarNroRegistroLibroOperaciones(obj);
            log.info("Libro Operaciones - validarNroRegistroLibroOperaciones","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Libro Operaciones - validarNroRegistroLibroOperaciones","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarLibroOperacionesPlanManejo")
    @ApiOperation(value = "listarLibroOperacionesPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarLibroOperacionesPlanManejo(@RequestBody PlanManejoEntity obj){
        log.info("Libro Operaciones - listarLibroOperacionesPlanManejo",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.listarLibroOperacionesPlanManejo(obj);
            if (response.getSuccess()) {
                log.info("Libro Operaciones - listarLibroOperacionesPlanManejo","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("Libro Operaciones - listarLibroOperacionesPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarEvaluacionLibroOperaciones")
    @ApiOperation(value = "listarEvaluacionLibroOperaciones" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarEvaluacionLibroOperaciones(@RequestBody LibroOperacionesDto obj){
        log.info("Libro Operaciones - listarEvaluacionLibroOperaciones",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesService.listarEvaluacionLibroOperaciones(obj);
            if (response.getSuccess()) {
                log.info("Libro Operaciones - listarEvaluacionLibroOperaciones","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            log.error("Libro Operaciones - listarEvaluacionLibroOperaciones","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
