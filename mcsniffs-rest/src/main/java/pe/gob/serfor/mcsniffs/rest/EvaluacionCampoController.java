package pe.gob.serfor.mcsniffs.rest;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Usuario.UsuarioDto;
import pe.gob.serfor.mcsniffs.entity.EmailEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion.EvaluacionCampoDto;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.EvalucionCampo.EvaluacionCampoEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.EvaluacionCampoService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

// import com.google.common.base.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.serfor.mcsniffs.service.ServicioExternoService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(Urls.evaluacionCampo.BASE)
public class EvaluacionCampoController {

@Autowired
EvaluacionCampoService serEvaCampo;

@Autowired
private ArchivoService serArchivo;

@Autowired
private EmailService serMail;

@Autowired
private ServicioExternoService serExt;

@Value("${spring.urlSeguridad}")
private String urlSeguridad;

private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EvaluacionCampoController.class);


@PostMapping(path = "/notificacionEvaluacionCampo")
@ApiOperation(value = "notificacionEvaluacionCampo" , authorizations = {@Authorization(value="JWT")})
@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity notificacionEvaluacionCampo(@RequestBody EvaluacionCampoEntity request, HttpServletRequest request1){
        log.info("EvaluacionCampo - notificacionEvaluacionCampo",request.toString());
        ResultClassEntity result =new ResultClassEntity();
        try{
             result = serEvaCampo.obtenerEvaluacionCampo(request);
            Gson gson = new Gson();
            if(result.getSuccess()){
                EvaluacionCampoEntity obj1 =(EvaluacionCampoEntity) result.getData();

                if(obj1.getNotificacion()!=true && request.getNotificacion() && obj1.getIdEvaluacionCampoEstado() !=3) {
                    UsuarioDto user= new UsuarioDto();
                    user.setIdusuario(obj1.getIdUsuarioRegistro());
                    String token= request1.getHeader("Authorization");
                    ResultEntity responseApiExt = serExt.ejecutarServicio(urlSeguridad+"usuario/ObtenerUsuarioID", gson.toJson(user), token);
                    ResultEntity resultApiExt = gson.fromJson( responseApiExt.getMessage(), ResultEntity.class);
                    Integer cont=0;
                    List<EvaluacionCampoEntity> list= new ArrayList<>();
                    list.add(obj1);
                    String[] mails=new String[list.size()];
                    for(Object item: resultApiExt.getData().stream().toArray()){
                        LinkedTreeMap<Object,Object> val = (LinkedTreeMap) item;
                        mails[cont]=val.get("correoElectronico").toString();
                        cont++;
                    }
                    EmailEntity mail=new EmailEntity();
                    mail.setSubject("SERFOR::Evaluación de Campo Nº "+obj1.getIdEvaluacionCampo()!=null?(obj1.getIdEvaluacionCampo()).toString():"");
                    mail.setEmail(mails);
                    String body="<html><head>"
                            +"<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>"
                            +"<meta name=Generator content='Microsoft Word 15 (filtered)'>"
                            +"</head>"
                            +"<body lang=ES-PE style='word-wrap:break-word'>"
                            +"<div class=WordSection1>"
                            +"<p class=MsoNormal><span lang=ES>Estimado(a):</span></p>"
                            +"<p class=MsoNormal><span lang=ES>  El Código Evaluación de Campo : "+ obj1.getIdEvaluacionCampo()!=null?(obj1.getIdEvaluacionCampo()).toString():""	+" El Código Plan de Manejo : "+  obj1.getIdPlanManejo()!=null?(obj1.getIdPlanManejo()).toString():""+" </span></p>"
                            +"<p class=MsoNormal><span lang=ES>  Se encuentra pendiente.</span></p>"
                            +"<p class=MsoNormal><b><i><span lang=ES>Nota:</span></i></b><i> no responder este correo automático.</i></p>"
                            +"</div></body></html>";
                    mail.setContent(body);
                    Boolean smail = serMail.sendEmail(mail);
                    if(smail){
                        obj1.setNotificacion(true);
                        obj1.setIdUsuarioRegistro(request.getIdUsuarioRegistro());
                        obj1.setIdEvaluacionCampo(request.getIdEvaluacionCampo());
                        result=      serEvaCampo.NotificacionEvaluacionCampo(obj1);
                        //  result.setInformacion("Correo Enviado Evaluación de Campo Nº "+result.getCodigo().toString());
                    }
                }
            }

            if (result.getSuccess()) {
                log.info("EvaluacionCampo - notificacionEvaluacionCampo","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("EvaluacionCampo - notificacionEvaluacionCampo","Ocurrió un error :"+ e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

@PostMapping(path = "/registroEvaluacionCampo")
@ApiOperation(value = "registroEvaluacionCampo" , authorizations = {@Authorization(value="JWT")})
@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
public ResponseEntity  registroEvaluacionCampo(@RequestBody EvaluacionCampoEntity request){
    log.info("EvaluacionCampo - registroEvaluacionCampo",request.toString());
    ResultClassEntity result =new ResultClassEntity();
    try{
        result = serEvaCampo.registroEvaluacionCampo(request);

        if (result.getSuccess()) {
            log.info("EvaluacionCampo - registroEvaluacionCampo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

        } else {
            return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
    }catch (Exception e){
        log.error("EvaluacionCampo - registroEvaluacionCampo","Ocurrió un error :"+ e.getMessage());
        result.setSuccess(Constantes.STATUS_ERROR);
        result.setMessage(Constantes.MESSAGE_ERROR_500);
        return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

@PostMapping(path = "/obtenerEvaluacionCampo")
@ApiOperation(value = "obtenerEvaluacionCampo" , authorizations = {@Authorization(value="JWT")})
@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
public ResponseEntity obtenerEvaluacionCampo(@RequestBody EvaluacionCampoEntity request){
    log.info("EvaluacionCampo - obtenerEvaluacionCampo",request.toString());
    ResultClassEntity result =new ResultClassEntity();
    try{
        result = serEvaCampo.obtenerEvaluacionCampo(request);

        if (result.getSuccess()) {
            log.info("EvaluacionCampo - obtenerEvaluacionCampo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

        } else {
            return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
    }catch (Exception e){
        log.error("PlanManejoEvaluacion - actualizarEstadoPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
        result.setSuccess(Constantes.STATUS_ERROR);
        result.setMessage(Constantes.MESSAGE_ERROR_500);
        return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
    @PostMapping(path = "/eliminarEvluacionCampoArchivo")
    @ApiOperation(value = "eliminarEvluacionCampoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarEvluacionCampoArchivo(@RequestBody EvaluacionCampoArchivoEntity request){
        log.info("EvaluacionCampo - eliminarEvluacionCampoArchivo",request.toString());
        ResultClassEntity result =new ResultClassEntity();
        try{
            result = serEvaCampo.eliminarEvaluacionCampoArchivo(request);

            if (result.getSuccess()) {
                log.info("EvaluacionCampo - eliminarEvluacionCampoArchivo","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("PlanManejoEvaluacion - actualizarEstadoPlanManejoEvaluacion","Ocurrió un error :"+ e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarEvaluacionCampoArchivoInspeccion")
    @ApiOperation(value = "listarEvaluacionCampoArchivoInspeccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEvaluacionCampoArchivoInspeccion(@RequestBody EvaluacionCampoArchivoEntity request){
        log.info("EvaluacionCampo - listarEvaluacionCampoArchivoInspeccion",request.toString());
        ResultClassEntity result =new ResultClassEntity();
        try{
            result = serEvaCampo.listarEvaluacionCampoArchivoInspeccion(request);

            if (result.getSuccess()) {
                log.info("EvaluacionCampo - listarEvaluacionCampoArchivoInspeccion","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("EvaluacionCampo - listarEvaluacionCampoArchivoInspeccion","Ocurrió un error :"+ e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
@PostMapping(path = "/registroEvaluacionCampoArchivo")
@ApiOperation(value = "registroEvaluacionCampoArchivo" , authorizations = {@Authorization(value="JWT")})
@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
public ResponseEntity registroEvaluacionCampoArchivo(@RequestParam(required = false) Integer idEvaluacionCampoArchivo,
                                                        @RequestParam(required = false) Integer idEvaluacionCampo,
                                                        @RequestParam(required = false) Integer idTipoSeccionArchivo,
                                                        @RequestParam(required = false) String idTipoDocumento,
                                                        @RequestParam(required = false) Integer idUsuarioRegistro,
                                                        @RequestParam("file") MultipartFile file
                                                        ){
    log.info("EvaluacionCampo - registroEvaluacionCampoArchivo");

    ResultClassEntity result =new ResultClassEntity();
    try{
        ArchivoEntity objArc=new ArchivoEntity();
            objArc.setIdArchivo(0);
            result =  serArchivo.RegistrarArchivoGeneral(file, idUsuarioRegistro, idTipoDocumento);
            if(result.getSuccess()){
                EvaluacionCampoArchivoEntity objEC=new EvaluacionCampoArchivoEntity();
                objEC.setIdArchivo(result.getCodigo());
                objEC.setIdEvaluacionCampo(idEvaluacionCampo);
                objEC.setIdTipoDocumento(idTipoDocumento);
                objEC.setIdUsuarioRegistro(idUsuarioRegistro);
                objEC.setIdEvaluacionCampoArchivo(idEvaluacionCampoArchivo);
                objEC.setIdTipoSeccionArchivo(idTipoSeccionArchivo);
                result = serEvaCampo.registroEvaluacionCampoArchivo(objEC);
            }else{

                result.setMessage(result.getMessage());
            }
        if (result.getSuccess()) {
            log.info("EvaluacionCampo - registroEvaluacionCampoArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

        } else {
            return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
    }catch (Exception e){
        log.error("EvaluacionCampo - registroEvaluacionCampoArchivo","Ocurrió un error :"+ e.getMessage());
        result.setSuccess(Constantes.STATUS_ERROR);
        result.setMessage(Constantes.MESSAGE_ERROR_500);
        return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

@PostMapping(path = "/obtenerEvaluacionCampoArchivo")
@ApiOperation(value = "obtenerEvaluacionCampoArchivo" , authorizations = {@Authorization(value="JWT")})
@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
public ResponseEntity obtenerEvaluacionCampoArchivo(@RequestBody EvaluacionCampoArchivoEntity request){
    log.info("EvaluacionCampo - obtenerEvaluacionCampoArchivo",request.toString());
    ResultClassEntity result =new ResultClassEntity();
    try{
        result = serEvaCampo.obtenerEvaluacionCampoArchivo(request);

        if (result.getSuccess()) {
            log.info("EvaluacionCampo - obtenerEvaluacionCampoArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

        } else {
            return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
    }catch (Exception e){
        log.error("EvaluacionCampo - obtenerEvaluacionCampoArchivo","Ocurrió un error :"+ e.getMessage());
        result.setSuccess(Constantes.STATUS_ERROR);
        result.setMessage(Constantes.MESSAGE_ERROR_500);
        return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
    @PostMapping(path = "/listarPorSeccionEvaluacionCampoArchivo")
    @ApiOperation(value = "listarPorSeccionEvaluacionCampoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPorSeccionEvaluacionCampoArchivo(@RequestBody EvaluacionCampoArchivoEntity request){
        log.info("EvaluacionCampo - listarPorSeccionEvaluacionCampoArchivo",request.toString());
        ResultClassEntity result =new ResultClassEntity();
        try{
            result = serEvaCampo.listarPorSeccionEvaluacionCampoArchivo(request);
            if (result.getSuccess()) {
                log.info("EvaluacionCampo - listarPorSeccionEvaluacionCampoArchivo","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("EvaluacionCampo - listarPorSeccionEvaluacionCampoArchivo","Ocurrió un error :"+ e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


@PostMapping(path = "/listarEvaluacionCampo")
@ApiOperation(value = "listarEvaluacionCampo" , authorizations = {@Authorization(value="JWT")})
@ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
public ResponseEntity listarEvaluacionCampo(@RequestBody EvaluacionCampoDto request){
    log.info("EvaluacionCampo - obtenerEvaluacionCampoInfraccion",request.toString());
    ResultClassEntity result =new ResultClassEntity();
    try{
        result = serEvaCampo.listarEvaluacionCampo(request);
        if (result.getSuccess()) {
            log.info("EvaluacionCampo - listarEvaluacionCampo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

        } else {
            return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
    }catch (Exception e){
        log.error("EvaluacionCampo - listarEvaluacionCampo","Ocurrió un error :"+ e.getMessage());
        result.setSuccess(Constantes.STATUS_ERROR);
        result.setMessage(Constantes.MESSAGE_ERROR_500);
        return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
 }



    @PostMapping(path = "/obtenerEvaluacionOcular")
    @ApiOperation(value = "obtenerEvaluacionOcular" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerEvaluacionOcular(@RequestBody EvaluacionCampoEntity request){
        log.info("EvaluacionCampo - obtenerEvaluacionOcular",request.toString());
        ResultClassEntity result =new ResultClassEntity();
        try{
            result = serEvaCampo.obtenerEvaluacionOcular(request);

            if (result.getSuccess()) {
                log.info("EvaluacionCampo - obtenerEvaluacionOcular","Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("obtenerEvaluacionOcular - obtenerEvaluacionOcular","Ocurrió un error :"+ e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
