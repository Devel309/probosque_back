package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.Parametro.RentabilidadManejoForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.RentabilidadManejoForestalService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(Urls.rentabilidadManejoForestal.BASE)
public class RentabilidadManejoForestalController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(RentabilidadManejoForestalController.class);



    @Autowired
    private RentabilidadManejoForestalService rentabilidadManejoForestalService;

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Combo rubro rentabilidad forestal}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/comboRubroRentabilidadForestal")
    @ApiOperation(value = "comboRubroRentabilidadForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity comboRubroRentabilidadForestal(@RequestBody ParametroEntity parametro){
        log.info("ProteccionBosque - ComboRubroRentabilidadForestal",parametro.toString());
        ResponseEntity result = null;
        List<Dropdown> response ;
        try{

            response = rentabilidadManejoForestalService.ComboRubroRentabilidadForestal(parametro);
            log.info("ProteccionBosque - ComboRubroRentabilidadForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ComboRubroRentabilidadForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Combo rubro rentabilidad forestal}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarRentabilidadManejoForestal")
    @ApiOperation(value = "registrarRentabilidadManejoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarRentabilidadManejoForestal(@RequestBody List<RentabilidadManejoForestalDto> rentabilidadManejoForestalDtos){
        log.info("ProteccionBosque - RegistrarRentabilidadManejoForestal",rentabilidadManejoForestalDtos.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = rentabilidadManejoForestalService.RegistrarRentabilidadManejoForestal(rentabilidadManejoForestalDtos);
            log.info("ProteccionBosque - RegistrarRentabilidadManejoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarRentabilidadManejoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Jason Retamozo [21-01-2022]
     * @descripción: {listado de rentabilidad forestal en relacion a titular}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarRentabilidadManejoForestalTitular")
    @ApiOperation(value = "listarRentabilidadManejoForestalTitular" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarRentabilidadManejoForestalTitular(@RequestBody PlanManejoEntity planManejo){
        log.info("ProteccionBosque - ListarRentabilidadManejoForestalTitular",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = rentabilidadManejoForestalService.ListarRentabilidadManejoForestalTitular(planManejo);
            log.info("ProteccionBosque - ListarRentabilidadManejoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarRentabilidadManejoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Combo rubro rentabilidad forestal}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarRentabilidadManejoForestal")
    @ApiOperation(value = "listarRentabilidadManejoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarRentabilidadManejoForestal(@RequestBody PlanManejoEntity planManejo){
        log.info("ProteccionBosque - ListarRentabilidadManejoForestal",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = rentabilidadManejoForestalService.ListarRentabilidadManejoForestal(planManejo);
            log.info("ProteccionBosque - ListarRentabilidadManejoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarRentabilidadManejoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Combo rubro rentabilidad forestal}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarRentabilidadManejoForestal")
    @ApiOperation(value = "eliminarRentabilidadManejoForestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarRentabilidadManejoForestal(@RequestBody RentabilidadManejoForestalEntity rentabilidadManejoForestal){
        log.info("ProteccionBosque - EliminarRentabilidadManejoForestal",rentabilidadManejoForestal.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = rentabilidadManejoForestalService.EliminarRentabilidadManejoForestal(rentabilidadManejoForestal);
            log.info("ProteccionBosque - EliminarRentabilidadManejoForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -EliminarRentabilidadManejoForestal","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping(path = "/descargarFormato")
    @ApiOperation(value = "descargarFormato", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> descargarFormato(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = rentabilidadManejoForestalService.descargarFormato();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ProteccionBosque - descargarFormato", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping ("/registrarProgramaInversionesExcel")
    @ApiOperation(value = "registrarProgramaInversionesExcel", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> registrarProgramaInversionesExcel(@RequestParam("file") MultipartFile file,
                                                                                    @RequestParam(required = false) String codigoRentabilidad,
                                                                                    @RequestParam(required = false) Integer idPlanManejo,
                                                                                    @RequestParam(required = false) Integer idUsuarioRegistro


    ) throws IOException {

        ResultClassEntity obj = new ResultClassEntity();
        try {
            obj =rentabilidadManejoForestalService.registrarProgramaInversionesExcel(file, codigoRentabilidad, idPlanManejo, idUsuarioRegistro);
            return new org.springframework.http.ResponseEntity<>(obj, HttpStatus.OK);
        } catch (Exception e) {
            obj.setSuccess(false);
            obj.setMessage("Se produjo un error al cargar archivo");
            obj.setMessageExeption(e.getMessage());
            log.error("CargarArchivo", e.getMessage());
            return new org.springframework.http.ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @autor: Danny Nazario [28-12-2021]
     * @modificado: Danny Nazario [25-01-2022]
     * @descripción: {Registra Rentabilidad Manejo Forestal Excel}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarRentabilidadManejoForestalExcel")
    @ApiOperation(value = "registrarRentabilidadManejoForestalExcel" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarRentabilidadManejoForestalExcel(@RequestParam("file") MultipartFile file,
                                                                                            @RequestParam("nombreHoja") String nombreHoja,
                                                                                            @RequestParam("numeroFila") Integer numeroFila,
                                                                                            @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                            @RequestParam("codigoRentabilidad") String codigoRentabilidad,
                                                                                            @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                            @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro,
                                                                                            @RequestParam("vigencia") Integer vigencia) {
        log.info("RentabilidadManejoForestal - registrarRentabilidadManejoForestalExcel");
        ResultClassEntity result = new ResultClassEntity();
        try{
            result = rentabilidadManejoForestalService.registrarRentabilidadManejoForestalExcel(file, nombreHoja, numeroFila, numeroColumna, codigoRentabilidad, idPlanManejo, idUsuarioRegistro, vigencia);
            if (result.getSuccess()) {
                log.info("RentabilidadManejoForestal - registrarRentabilidadManejoForestalExcel", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("RentabilidadManejoForestal - registrarRentabilidadManejoForestalExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
