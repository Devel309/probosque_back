package pe.gob.serfor.mcsniffs.dto;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalAreaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalAreaPlantadaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalDetCoordenadaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal.SolPlantacionForestalEntity;

public class DtoSolPlantacionForestal extends ResultEntity {
    SolPlantacionForestalEntity solPlantacionForestal;
    SolPlantacionForestalAreaEntity solPlantacionForestalArea;

    public SolPlantacionForestalEntity getSolPlantacionForestal() {
        return solPlantacionForestal;
    }

    public void setSolPlantacionForestal(SolPlantacionForestalEntity solPlantacionForestal) {
        this.solPlantacionForestal = solPlantacionForestal;
    }

    public SolPlantacionForestalAreaEntity getSolPlantacionForestalArea() {
        return solPlantacionForestalArea;
    }

    public void setSolPlantacionForestalArea(SolPlantacionForestalAreaEntity solPlantacionForestalArea) {
        this.solPlantacionForestalArea = solPlantacionForestalArea;
    }

}
