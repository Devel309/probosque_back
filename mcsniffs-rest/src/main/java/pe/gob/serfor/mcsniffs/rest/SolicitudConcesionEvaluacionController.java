package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.DescargarSolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionEvaluacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionEvaluacionDetalleService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionEvaluacionService;

@RestController
@RequestMapping(Urls.solicitudConcesionEvaluacion.BASE)
public class SolicitudConcesionEvaluacionController extends AbstractRestController {

    private static final org.apache.logging.log4j.Logger log = LogManager
            .getLogger(SolicitudConcesionEvaluacionController.class);

    @Autowired
    private SolicitudConcesionEvaluacionService solicitudConcesionEvaluacionService;
    @Autowired
    private SolicitudConcesionEvaluacionDetalleService solicitudConcesionEvaluacionDetalleService;

    @PostMapping(path = "/obtenerInfoEvaluacionSolicitudConcesion")
    @ApiOperation(value = "Obtener Informacion de Evaluación de Solicitud Concesión ", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoEvaluacionSolicitudConcesion(
            @RequestBody SolicitudConcesionEvaluacionDto request) {
        log.info("SolicitudConcesionEvaluacion - obtenerInfoEvaluacionSolicitudConcesion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionEvaluacionService.listarSolicitudConcesionEvaluacion(request);
            log.info("SolicitudConcesionEvaluacion - obtenerInfoEvaluacionSolicitudConcesion",
                    "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboral - obtenerInfoEvaluacionSolicitudConcesion",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/actualizarSolicitudConcesionEvaluacion")
    @ApiOperation(value = "Actualizar solicitud concesión evaluación", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudConcesionEvaluacion(
            @RequestBody SolicitudConcesionEvaluacionDto request) {
        log.info("SolicitudConcesionEvaluacion - actualizarSolicitudConcesionEvaluacion", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionEvaluacionService.actualizarSolicitudConcesionEvaluacion(request);
            log.info("SolicitudConcesionEvaluacion - actualizarSolicitudConcesionEvaluacion",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("ResponsableExperienciaLaboral - actualizarSolicitudConcesionEvaluacion",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @autor: Jordy Zamata [31-01-2022]
     * @modificado:
     * @descripción: {Listar Detalle }
     * @param:
     */
    @PostMapping(path = "/listarSolicitudConcesionEvaluacionDetalle")
    @ApiOperation(value = "listarSolicitudConcesionEvaluacionDetalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public org.springframework.http.ResponseEntity listarSolicitudConcesionEvaluacionDetalle(
            @RequestBody SolicitudConcesionEvaluacionDto request) {
        log.info("SolicitudConcesionEvaluacionDetalle - listarSolicitudConcesionEvaluacionDetalle", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = solicitudConcesionEvaluacionService.listarSolicitudConcesionEvaluacionDetalle(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionEvaluacionDetalle - listarSolicitudConcesionEvaluacionDetalle",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error("SolicitudConcesionEvaluacionDetalle - listarSolicitudConcesionEvaluacionDetalle",
                    "Ocurrió un error : " + ex.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(ex.getStackTrace()));
            result.setMessageExeption(ex.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarSolicitudConcesionEvaluacionDetalle")
    @ApiOperation(value = "Actualizar  Solicitud Concesión Evaluacion Detalle", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarSolicitudConcesionEvaluacionDetalle(
            @RequestBody List<SolicitudConcesionEvaluacionDetalleDto> request) {
        log.info("SolicitudConcesionEvaluacion - actualizarSolicitudConcesionEvaluacionDetalle", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {
            response = solicitudConcesionEvaluacionDetalleService
                    .actualizarSolicitudConcesionEvaluacionDetalle(request);
            log.info("SolicitudConcesionEvaluacion - actualizarSolicitudConcesionEvaluacionDetalle",
                    "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionEvaluacion - actualizarSolicitudConcesionEvaluacionDetalle",
                    "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/registrarSolicitudConcesionEvaluacionDetalle")
    @ApiOperation(value = "Registrar  Solicitud Concesión Evaluacion Detalle", authorizations = {
            @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudConcesionEvaluacionDetalle(
            @RequestBody List<SolicitudConcesionEvaluacionDetalleDto> request) {
        log.info("SolicitudConcesionEvaluacion - registrarSolicitudConcesionEvaluacionDetalle", request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try {

            response = solicitudConcesionEvaluacionDetalleService.registrarSolicitudConcesionEvaluacionDetalle(request);
            log.info("SolicitudConcesionEvaluacion - registrarSolicitudConcesionEvaluacionDetalle",
                    "Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {

            log.error("SolicitudConcesionEvaluacion - registrarSolicitudConcesionEvaluacionDetalle",
                    "Ocurrió un error :" + e.getMessage());

            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/descargarSolicitudConcesionEvaluacion")
    @ApiOperation(value = "descargarSolicitudConcesionEvaluacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultArchivoEntity> descargarSolicitudConcesionEvaluacion(
            @RequestBody DescargarSolicitudConcesionEvaluacionDto request) {
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = solicitudConcesionEvaluacionService.descargarSolicitudConcesionEvaluacion(request);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("SolicitudConcesion - descargarSolicitudConcesion",
                    "Ocurrió un error al obtener en: " + Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
        }
    }

}
