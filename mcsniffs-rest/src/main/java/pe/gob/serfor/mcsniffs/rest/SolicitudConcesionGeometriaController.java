package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionGeometriaEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;

import pe.gob.serfor.mcsniffs.service.SolicitudConcesionGeometriaService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.SolicitudConcesionGeometria.BASE)
public class SolicitudConcesionGeometriaController {

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionGeometriaController.class);
    @Autowired
    SolicitudConcesionGeometriaService service;

    @PostMapping(path = "/registrarSolicitudConcesionGeometria")
    @ApiOperation(value = "registrarSolicitudConcesionGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudConcesionGeometria(@RequestBody List<SolicitudConcesionGeometriaEntity> request) {
        log.info("SolicitudConcesionGeometria - registrarSolicitudConcesionGeometria", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarSolicitudConcesionGeometria(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometria - registrarSolicitudConcesionGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometria - registrarSolicitudConcesionGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Abner Valdez [15-12-2021]
     * @modificado:
     * @descripción: {Obtener SolicitudConcesionGeometria por  filtros}
     * @param:solicitudConcesionGeometriaDto
     */
    @PostMapping(path = "/listarSolicitudConcesionGeometria")
    @ApiOperation(value = "listarSolicitudConcesionGeometria", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudConcesionGeometria(@RequestBody SolicitudConcesionGeometriaEntity item) {
        log.info("SolicitudConcesionGeometriaController - listarSolicitudConcesionGeometria", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.listarSolicitudConcesionGeometria(item);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - listarSolicitudConcesionGeometria", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - listarSolicitudConcesionGeometria", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerInformacionAreaGeometriaSolicitada")
    @ApiOperation(value = "Obtener Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInformacionAreaGeometriaSolicitada(@RequestBody SolicitudConcesionGeometriaEntity item) {
        log.info("SolicitudConcesionGeometriaController - obtenerInformacionAreaGeometriaSolicitada", item.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.obtenerInformacionAreaGeometriaSolicitada(item);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - obtenerInformacionAreaGeometriaSolicitada", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - obtenerInformacionAreaGeometriaSolicitada", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarInformacionAreaGeometriaSolicitada")
    @ApiOperation(value = "Registrar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarInformacionAreaGeometriaSolicitada(@RequestBody List<SolicitudConcesionGeometriaEntity> request) {
        log.info("SolicitudConcesionGeometriaController - registrarInformacionAreaGeometriaSolicitada", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.registrarInformacionAreaGeometriaSolicitada(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - registrarInformacionAreaGeometriaSolicitada", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - registrarInformacionAreaGeometriaSolicitada", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

    @PostMapping(path = "/actualizarInformacionAreaGeometriaSolicitada")
    @ApiOperation(value = "Actualizar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarInformacionAreaGeometriaSolicitada(@RequestBody List<SolicitudConcesionGeometriaEntity> request) {
        log.info("SolicitudConcesionGeometriaController - actualizarInformacionAreaGeometriaSolicitada", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.actualizarInformacionAreaGeometriaSolicitada(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - actualizarInformacionAreaGeometriaSolicitada", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - actualizarInformacionAreaGeometriaSolicitada", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

    @PostMapping(path = "/eliminarInformacionAreaGeometriaSolicitada")
    @ApiOperation(value = "Eliminar Informacion de Area/Geometria Solicitada", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarInformacionAreaGeometriaSolicitada(@RequestBody SolicitudConcesionGeometriaEntity request) {
        log.info("SolicitudConcesionGeometriaController - eliminarInformacionAreaGeometriaSolicitada", request.toString());
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarInformacionAreaGeometriaSolicitada(request);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - eliminarInformacionAreaGeometriaSolicitada", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - eliminarInformacionAreaGeometriaSolicitada", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            result.setStackTrace(Arrays.toString(e.getStackTrace()));
            result.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }
    /**
     * @autor: Abner Valdez [17-12-2021]
     * @modificado:
     * @descripción: {Eliminar SolicitudConcesionGeometria archivo y/o capa}
     * @param:idArchivo, idUsuario
     */
    /*
    @GetMapping(path = "/eliminarSolicitudConcesionGeometriaArchivo/{idArchivo}/{idUsuario}")
    @ApiOperation(value = "eliminarSolicitudConcesionGeometriaArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudConcesionGeometriaArchivo(@PathVariable Integer idArchivo,@PathVariable Integer idUsuario) {
        log.info("SolicitudConcesionGeometriaController - eliminarSolicitudConcesionGeometriaArchivo", idArchivo);
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = service.eliminarSolicitudConcesionGeometriaArchivo(idArchivo,idUsuario);

            if (result.getSuccess()) {
                log.info("SolicitudConcesionGeometriaController - eliminarSolicitudConcesionGeometriaArchivo", "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionGeometriaController - eliminarSolicitudConcesionGeometriaArchivo", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    */

}
