package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CensoForestalDetalleService;
import pe.gob.serfor.mcsniffs.service.FeriadoService;

import java.util.List;

@RestController
@RequestMapping(Urls.censoForestal.BASE)

public class FeriadoController extends AbstractRestController {

    @Autowired
    private FeriadoService service;

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(FeriadoController.class);

    /**
         * @autor: Rafael Azaña [14-09-2021]
     * @modificado:
     * @descripción: {Método creado para listar los feriados}
     * @param: MultipartFile file, Integer codUbigeo
     * @return: ResultClassEntity<String>
     */

    @PostMapping(path = "/listaFeriados")
    @ApiOperation(value = "Listar Feriados", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public ResultClassEntity<List<FeriadoEntity>> ListarFeriados(@RequestParam("codUbigeo") String codUbigeo) {
        try {
            return service.ListarFeriados(codUbigeo);
        } catch (Exception ex) {

            ResultClassEntity<List<FeriadoEntity>> result = new ResultClassEntity<>();
            log.error("FeriadoController - listaFeriados", "Ocurrió un error en: " + ex.getMessage());
            result.setInnerException(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            return result;
        }
    }

    /**
     * @autor: Rafael Azaña [14-09-2021]
     * @modificado:
     * @descripción: {Método creado parregistrar y actualizar los feriados}
     * @param: MultipartFile file, FeriadoEntity
     * @return: ResultClassEntity<String>
     */

    @PostMapping(path = "/registrarFeriado")
    @ApiOperation(value = "registrarFeriado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarFeriado(@RequestBody FeriadoEntity entidad){
        log.info("RecursoForestal Registrar", entidad.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity<String> result = null;
        ResultClassEntity response ;
        try {
            response = service.RegistrarFeriados(entidad);
            log.info("RecursoForestal Registrar","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e){
            log.error("RecursoForestal Registrar","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
