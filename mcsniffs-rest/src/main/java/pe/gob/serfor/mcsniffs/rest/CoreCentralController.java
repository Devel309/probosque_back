package pe.gob.serfor.mcsniffs.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CoreCentralService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping(Urls.coreCentral.BASE)
public class CoreCentralController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CoreCentralController.class);

    @Autowired
    private CoreCentralService coreCentral;

    /**
     * @autor: miguel [09-06-2021]
     * @descripción: {lista la maestra de Departamento}
     * @param: DepartamentoEntity
     * @return: List<DepartamentoEntity>
     */
    @PostMapping(path = "/ListarPorFiltroDepartamento")
    @ApiOperation(value = "ListarPorFiltroDepartamento")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ListarPorFiltroDepartamento(@RequestBody DepartamentoEntity param){
        try{

            return  new ResponseEntity (true, "ok", coreCentral.ListarPorFiltroDepartamentos(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    /**
     * @autor: miguel [09-06-2021]
     * @descripción: {lista la maestra de AutoridadForestal}
     * @param: AutoridadForestalEntity
     * @return: List<AutoridadForestalEntity>
     */
    @PostMapping(path = "/ListarPorFiltroAutoridadForestal")
    @ApiOperation(value = "peticionCompraTotal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ListarPorFiltroAutoridadForestal(@RequestBody AutoridadForestalEntity param){
        try{

            return  new ResponseEntity (true, "ok", coreCentral.ListarPorFiltroAutoridadForestal(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }
    /**
     * @autor: miguel [09-06-2021]
     * @descripción: {lista la maestra de tipo-comunidad}
     * @param: String descripcion, String estado
     * @return: List<TipoComunidadEntity>
     */
    @PostMapping(path = "/ListarPorFiltroTipoComunidad")
    @ApiOperation(value = "peticionCompraTotal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ListarPorFiltroTipoComunidad(@RequestBody TipoComunidadEntity param){
        try{
            log.info("tipoComunidad : ok");
            return  new ResponseEntity (true, "ok", coreCentral.ListarPorFiltroTipoComunidad( param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    /**
     * @autor: miguel [09-06-2021]
     * @descripción: {lista la maestra de tipo-trasporte}
     * @param: String descripcion
     * @return: List<TipoTrasporteEntity>
     */
    @PostMapping(path = "/ListarPorFiltroTipoTrasporte")
    @ApiOperation(value = "peticionCompraTotal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ListarPorFiltroTipoTrasporte(@RequestBody TipoTrasporteEntity param){
        try {
            log.info("tipoTrasporte : ok");
            return  new ResponseEntity(true, "ok",coreCentral.ListarPorFiltroTipoTrasporte(param));
        }
        catch (Exception e){
            log.error(e.getMessage());
            return  new ResponseEntity(false, e.getMessage(),null);
        }
    }
    /**
     * @autor: miguel [09-06-2021]
     * @descripción: {lista la maestra de tipo-Bosque}
     * @param: String descripcion
     * @return: List<TipoZonaEntity>
     */
    @PostMapping(path = "/listarPorFiltroTipoZona")
    @ApiOperation(value = "peticionCompraTotal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPorFiltroTipoZona(@RequestBody TipoZonaEntity param){
        try {
            return new ResponseEntity(true, "ok", coreCentral.listarPorFiltroTipoZona(param));
        }
        catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }


    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoBosque}
     * @param: TipoBosqueEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @ApiOperation(value = "listarPorFiltroTipoBosque", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoBosque")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoBosque(@RequestBody TipoBosqueEntity tipoBosqueEntity){
        log.info("CoreCentral - listarPorFiltroTipoBosque",tipoBosqueEntity.toString());
        ResponseEntity result = null;
        List<TipoBosqueEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoBosque(tipoBosqueEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoBosque","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            log.error("CoreCentral -ListapeticionCompraDetalle","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoConcesion}
     * @param: TipoConcesionEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @ApiOperation(value = "listarPorFiltroTipoConcesion", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoConcesion")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoConcesion(@RequestBody TipoConcesionEntity tipoConcesionEntity){
        log.info("CoreCentral - listarPorFiltroTipoConcesion",tipoConcesionEntity.toString());
        ResponseEntity result = null;
        List<TipoConcesionEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoConcesion(tipoConcesionEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoConcesion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoConcesion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoContrato}
     * @param: TipoContratoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoContrato", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoContrato")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoContrato(@RequestBody TipoContratoEntity tipoContratoEntity){
        log.info("CoreCentral - listarPorFiltroTipoContrato",tipoContratoEntity.toString());
        ResponseEntity result = null;
        List<TipoContratoEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoContrato(tipoContratoEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoContrato","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoContrato","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoDocGestion}
     * @param: TipoDocGestionEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoDocGestion", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoDocGestion")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoDocGestion(@RequestBody TipoDocGestionEntity tipoDocGestionEntity){
        log.info("CoreCentral - listarPorFiltroTipoDocGestion",tipoDocGestionEntity.toString());
        ResponseEntity result = null;
        List<TipoDocGestionEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoDocGestion(tipoDocGestionEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoDocGestion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoDocGestion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoDocGestionErgtf}
     * @param: TipoDocGestionErgtfEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoDocGestionErgtf", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoDocGestionErgtf")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoDocGestionErgtf(@RequestBody TipoDocGestionErgtfEntity tipoDocGestionErgtfEntity){
        log.info("CoreCentral - listarPorFiltroTipoDocGestionErgtf",tipoDocGestionErgtfEntity.toString());
        ResponseEntity result = null;
        List<TipoDocGestionErgtfEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoDocGestionErgtf(tipoDocGestionErgtfEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoDocGestionErgtf","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoDocGestionErgtf","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoDocumento}
     * @param: TipoDocumentoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoDocumento", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoDocumento")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoDocumento(@RequestBody TipoDocumentoEntity tipoDocumentoEntity){
        log.info("CoreCentral - listarPorFiltroTipoDocumento",tipoDocumentoEntity.toString());
        ResponseEntity result = null;
        List<TipoDocumentoEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoDocumento(tipoDocumentoEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoDocumento","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoDocumento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoEscala}
     * @param: TipoEscalaEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoEscala", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoEscala")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoEscala(@RequestBody TipoEscalaEntity tipoEscalaEntity){
        log.info("CoreCentral - listarPorFiltroTipoEscala",tipoEscalaEntity.toString());
        ResponseEntity result = null;
        List<TipoEscalaEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoEscala(tipoEscalaEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoEscala","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoEscala","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoMoneda}
     * @param: TipoMonedaEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoMoneda", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoMoneda")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoMoneda(@RequestBody TipoMonedaEntity tipoMonedaEntity){
        log.info("CoreCentral - listarPorFiltroTipoMoneda",tipoMonedaEntity.toString());
        ResponseEntity result = null;
        List<TipoMonedaEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoMoneda(tipoMonedaEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoMoneda","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoMoneda","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoOficina}
     * @param: TipoOficinaEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoOficina", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoOficina")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoOficina(@RequestBody TipoOficinaEntity tipoOficinaEntity){
        log.info("CoreCentral - listarPorFiltroTipoOficina",tipoOficinaEntity.toString());
        ResponseEntity result = null;
        List<TipoOficinaEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoOficina(tipoOficinaEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoOficina","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoOficina","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoPago}
     * @param: TipoPagoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoPago", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoPago")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoPago(@RequestBody TipoPagoEntity tipoPagoEntity){
        log.info("CoreCentral - listarPorFiltroTipoPago",tipoPagoEntity.toString());
        ResponseEntity result = null;
        List<TipoPagoEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoPago(tipoPagoEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoPago","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoPago","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoPermiso}
     * @param: TipoPermisoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoPermiso", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoPermiso")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoPermiso(@RequestBody TipoPermisoEntity tipoPermisoEntity){
        log.info("CoreCentral - listarPorFiltroTipoPermiso",tipoPermisoEntity.toString());
        ResponseEntity result = null;
        List<TipoPermisoEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoPermiso(tipoPermisoEntity);
            log.info("CoreCentral - listarPorFiltroTipoPermiso","Proceso realizado correctamente");
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoPermiso","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoProcedimientoConces}
     * @param: TipoProcedimientoConcesEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoProcedimientoConces", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoProcedimientoConces")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoProcedimientoConces(@RequestBody TipoProcedimientoConcesEntity tipoProcedimientoConcesEntity){
        log.info("CoreCentral - listarPorFiltroTipoProcedimientoConces",tipoProcedimientoConcesEntity.toString());
        ResponseEntity result = null;
        List<TipoProcedimientoConcesEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoProcedimientoConces(tipoProcedimientoConcesEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoProcedimientoConces","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoProcedimientoConces","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoProducto}
     * @param: TipoProductoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroTipoProducto", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroTipoProducto")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroTipoProducto(@RequestBody TipoProductoEntity tipoProductoEntity){
        log.info("CoreCentral - listarPorFiltroTipoProducto",tipoProductoEntity.toString());
        ResponseEntity result = null;
        List<TipoProductoEntity> response ;
        try {
            response = coreCentral.listarPorFiltroTipoProducto(tipoProductoEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroTipoProducto","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroTipoProducto","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el UbigeoArffs}
     * @param: UbigeoArffsEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroUbigeoArffs", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroUbigeoArffs")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroUbigeoArffs(@RequestBody UbigeoArffsEntity ubigeoArffsEntity){
        log.info("CoreCentral - listarPorFiltroUbigeoArffs",ubigeoArffsEntity.toString());
        ResponseEntity result = null;
        List<UbigeoArffsEntity> response ;
        try {
            response = coreCentral.listarPorFiltroUbigeoArffs(ubigeoArffsEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroUbigeoArffs","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroUbigeoArffs","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el UnidadMedida}
     * @param: UnidadMedidaEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroUnidadMedida", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroUnidadMedida")
    public org.springframework.http.ResponseEntity<ResponseEntity> listarPorFiltroUnidadMedida(@RequestBody UnidadMedidaEntity unidadMedidaEntity){
        log.info("CoreCentral - listarPorFiltroUnidadMedida",unidadMedidaEntity.toString());
        ResponseEntity result = null;
        List<UnidadMedidaEntity> response ;
        try {
            response = coreCentral.listarPorFiltroUnidadMedida(unidadMedidaEntity);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroUnidadMedida","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroUnidadMedida","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Lista las Provincias}
     * @param:ProvinciaEntity
     */
    @PostMapping(path = "/listarPorFilroProvincia")
    @ApiOperation(value = "listarPorFilroProvincia")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<ProvinciaEntity>> listarPorFilroProvincia(@RequestBody ProvinciaEntity obj){
        ResultEntity<ProvinciaEntity> result = new ResultEntity();
        try {
            result = coreCentral.listarPorFilroProvincia(obj);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CoreCentral - listarPorFilroProvincia", "Ocurrió un error Listar las provincias: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [22-06-2021]
     * @modificado:
     * @descripción: {Lista los Distritos}
     * @param:DistritoEntity
     */
    @PostMapping(path = "/listarPorFilroDistrito")
    @ApiOperation(value = "listarPorFilroDistrito")
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<DistritoEntity>> listarPorFilroDistrito(@RequestBody DistritoEntity obj){
        ResultEntity<DistritoEntity> result = new ResultEntity<>();
        try {
            result = coreCentral.listarPorFilroDistrito(obj);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CoreCentral - listarPorFilroDistrito", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    /**
     * @autor: Ivan Minaya [22-06-2021]
     * @modificado:
     * @descripción: {Lista por filtro Regente Comunidad}
     * @param:RegenteComunidadEntity
     */
    @PostMapping(path = "/listarPorFiltroRegenteComunidad")
    @ApiOperation(value = "listarPorFiltroRegenteComunidad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<DistritoEntity>> listarPorFiltroRegenteComunidad(@RequestBody RegenteComunidadEntity regenteComunidad){
        log.info("CoreCentral - listarPorFiltroRegenteComunidad",regenteComunidad.toString());
        ResponseEntity result = null;
        List<RegenteComunidadEntity> response ;
        try {
            response = coreCentral.listarPorFiltroRegenteComunidad(regenteComunidad);
            result = buildResponse(Constantes.STATUS_SUCCESS, response, "", null);
            log.info("CoreCentral - listarPorFiltroRegenteComunidad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("CoreCentral -listarPorFiltroRegenteComunidad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    /**
     * @autor:  Edwin M.C [05-08-2021]
     * @descripción: {ListaPorFiltroEspecieForestal}
     * @param: EspecieForestalEntity
     * @return: ResponseEntity<ResultEntity>
     */
    @ApiOperation(value = "ListaPorFiltroEspecieForestal", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/ListaPorFiltroEspecieForestal")
    public org.springframework.http.ResponseEntity<ResultEntity> ListaPorFiltroEspecieForestal(@RequestBody EspecieForestalEntity request){
        log.info("CoreCentral - listarPorFiltroPersona",request.toString());
        ResultEntity result = new ResultEntity();
        try {
            result = coreCentral.ListaPorFiltroEspecieForestal(request);
            log.info("CoreCentral - ListaPorFiltroEspecieForestal","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CoreCentral - ListaPorFiltroEspecieForestal", "Ocurrió un error Listar : "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
        /**
     * @autor:  Edwin M.C [05-08-2021]
     * @descripción: {obtenerVistaEspecieForestal}
     * @param: EspecieForestalEntity
     * @return: ResponseEntity<ResultEntity>
     */
    @ApiOperation(value = "listaPorFiltroEspecieFauna", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listaPorFiltroEspecieFauna")
    public org.springframework.http.ResponseEntity<ResultEntity> listaPorFiltroEspecieFauna(@RequestBody EspecieFaunaEntity request){
        log.info("CoreCentral - ListaPorFiltroEspecieFauna",request.toString());
        ResultEntity result = new ResultEntity();
        try {
            result = coreCentral.ListaPorFiltroEspecieFauna(request);

            log.info("CoreCentral - ListaPorFiltroEspecieFauna","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CoreCentral - ListaPorFiltroEspecieFauna", "Ocurrió un error Listar : "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listaEspecieFaunaPorFiltro")
    @ApiOperation(value = "listaEspecieFaunaPorFiltro" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listaEspecieFaunaPorFiltro(@RequestBody EspecieFaunaEntity request){
        log.info("CoreCentral - listaEspecieFaunaPorFiltro", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = coreCentral.ListaEspecieFaunaPorFiltro(request);
            log.info("CoreCentral - listaEspecieFaunaPorFiltro","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("CoreCentral - listaEspecieFaunaPorFiltro","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

/*
    @PostMapping(path = "/ListaEspecieForestalPorFiltro")
    @ApiOperation(value = "ListaEspecieForestalPorFiltro" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ListaEspecieForestalPorFiltro(@RequestBody EspecieForestalEntity param){
        try{
            return  new ResponseEntity (true, "ok" ,coreCentral.ListaEspecieForestalPorFiltro(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }*/


    @PostMapping(path = "/ListaEspecieForestalPorFiltro")
    @ApiOperation(value = "ListaEspecieForestalPorFiltro" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> ListaEspecieForestalPorFiltro(@RequestBody EspecieForestalEntity request){
        log.info("EspecieForestal - ListaEspecieForestalPorFiltro", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = coreCentral.ListaEspecieForestalPorFiltro(request);
            log.info("EspecieForestal - ListaEspecieForestalPorFiltro","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("EspecieForestal - ListaEspecieForestalPorFiltro","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/listarEspecieForestalSincronizacion")
    @ApiOperation(value = "listarEspecieForestalSincronizacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarEspecieForestalSincronizacion(@RequestBody EspecieForestalEntity param){
        log.info("EspecieForestal - listarEspecieForestalSincronizacion", param.toString());
        //ResponseEntity<String> result = null;
        ResultClassEntity res = new ResultClassEntity() ;
        try {
            res = coreCentral.listarEspecieForestalSincronizacion(param);
            log.info("EspecieForestal - listarEspecieForestalSincronizacion","Proceso realizado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("EspecieForestal - listarEspecieForestalSincronizacion","Ocurrio un error :"+ e.getMessage());
            res.setSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());

            return new org.springframework.http.ResponseEntity(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/listarCuencaSubcuenca")
    @ApiOperation(value = "listarCuencaSubcuenca" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarCuencaSubcuenca(@RequestBody CuencaEntity param){
        log.info("EspecieForestal - listarEspecieForestalSincronizacion", param.toString());
        //ResponseEntity<String> result = null;
        ResultClassEntity res = new ResultClassEntity() ;
        try {
            res = coreCentral.listarCuencaSubcuenca(param);
            log.info("EspecieForestal - listarCuencaSubcuenca","Proceso realizado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("EspecieForestal - listarCuencaSubcuenca","Ocurrio un error :"+ e.getMessage());
            res.setSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());

            return new org.springframework.http.ResponseEntity(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }




}
