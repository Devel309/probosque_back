package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoBiologicoFaunaSilvestrePgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActEconomicaPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaActividadesPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaConfictosPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaInfraestructuraPgmf;
import pe.gob.serfor.mcsniffs.entity.AspectoEconomico.InfoEconomicaPgmf;
import pe.gob.serfor.mcsniffs.entity.Parametro.AccesibilidadManejoMatrizDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.AccesibilidadService;
import pe.gob.serfor.mcsniffs.service.ActividadSilviculturalService;
import pe.gob.serfor.mcsniffs.service.ArchivoService;
import pe.gob.serfor.mcsniffs.service.AspectoHidrograficoService;
import pe.gob.serfor.mcsniffs.service.CoordenadaUtmService;
import pe.gob.serfor.mcsniffs.service.InfoEconomicaPgmfService;
import pe.gob.serfor.mcsniffs.service.UnidadManejoArchivoService;
import pe.gob.serfor.mcsniffs.service.UnidadManejoService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.informacionAreaManejo.BASE)
public class InformacionAreaManejoController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(InformacionAreaManejoController.class);

    @Autowired
    private UnidadManejoService unidadManejoService;

    @Autowired
    private CoordenadaUtmService coordenadaUtmService;

    @Autowired
    private AccesibilidadService accesibilidadService;

    @Autowired
    private UnidadManejoArchivoService unidadManejoArchivoService;

    @Autowired
    private AspectoHidrograficoService aspectoHidrograficoService;

    @Autowired
    InfoEconomicaPgmfService serInfEco;
    @Autowired
    ArchivoService serArchivo;

    /**
     * @autor: Carlos Tang [23-08-2021]
     * @modificado:
     * @descripción: {}
     */


    @PostMapping(path = "/registrarUnidadManejo")
    @ApiOperation(value = "registrarUnidadManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarUnidadManejo(@RequestBody UnidadManejoEntity unidadManejoEntity){
        log.info("InformacionAreaManejo - registrarUnidadManejo", unidadManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = unidadManejoService.RegistrarUnidadManejo(unidadManejoEntity);
            log.info("InformacionAreaManejo - registrarUnidadManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - registrarUnidadManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarUnidadManejo")
    @ApiOperation(value = "actualizarUnidadManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarUnidadManejo(@RequestBody UnidadManejoEntity unidadManejoEntity){
        log.info("InformacionAreaManejo - actualizarUnidadManejo", unidadManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = unidadManejoService.ActualizarUnidadManejo(unidadManejoEntity);
            log.info("InformacionAreaManejo - registrarUnidadManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - registrarUnidadManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerUnidadManejo")
    @ApiOperation(value = "obtenerUnidadManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerUnidadManejo(@RequestBody UnidadManejoEntity unidadManejoEntity){
        log.info("InformacionAreaManejo - obtenerUnidadManejo", unidadManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try {
            response = unidadManejoService.ListarUnidadManejo(unidadManejoEntity);
            log.info("InformacionAreaManejo - ListarUnidadManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e){
            log.error("service - ListarUnidadManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarCoordenadaUtm")
    @ApiOperation(value = "registrarCoordenadaUtm" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCoordenadaUtm(@RequestBody List<CoordenadaUtmEntity> lista){
        log.info("InformacionAreaManejo - registrarCoordenadaUtm", lista);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = coordenadaUtmService.RegistrarCoordenadaUtm(lista);
            log.info("InformacionAreaManejo - registrarCoordenadaUtm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - registrarCoordenadaUtm","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   /* @PostMapping(path = "/actualizarCoordenadaUtm")
    @ApiOperation(value = "actualizarCoordenadaUtm" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarCoordenadaUtm(@RequestBody List<CoordenadaUtmEntity> lista){
        log.info("InformacionAreaManejo - actualizarCoordenadaUtm", lista);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = coordenadaUtmService.ActualizarCoordenadaUtm(lista);
            log.info("InformacionAreaManejo - actualizarCoordenadaUtm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - actualizarCoordenadaUtm","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }*/

    @PostMapping(path = "/eliminarCoordenadaUtm")
    @ApiOperation(value = "eliminarCoordenadaUtm" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarCoordenadaUtm(@RequestBody CoordenadaUtmEntity coordenadaUtmEntity){
        log.info("InformacionAreaManejo - eliminarCoordenadaUtm", coordenadaUtmEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = coordenadaUtmService.EliminarCoordenadaUtm(coordenadaUtmEntity);
            log.info("InformacionAreaManejo - eliminarCoordenadaUtm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -eliminarCoordenadaUtm","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarCoordenadaUtm")
    @ApiOperation(value = "listarCoordenadaUtm" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarCoordenadaUtm(@RequestBody CoordenadaUtmEntity coordenadaUtmEntity){
        log.info("InformacionAreaManejo - listarCoordenadaUtm", coordenadaUtmEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = coordenadaUtmService.ListarCoordenadaUtm(coordenadaUtmEntity);
            log.info("InformacionAreaManejo - listarCoordenadaUtm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -listarCoordenadaUtm","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/listarAccesibilidadmanejo")
    @ApiOperation(value = "listarAccesibilidadmanejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarAccesibilidadmanejo(@RequestBody AccesibilidadManejoEntity accesibilidadManejoEntity){
        log.info("InformacionAreaManejo - listarAccesibilidadmanejo", accesibilidadManejoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.ListarAccesibilidadManejo(accesibilidadManejoEntity);
            log.info("InformacionAreaManejo - ListarAccesibilidadmanejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ListarAccesibilidadManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // @PostMapping(path = "/registrarUnidadManejoArchivo")
    // @ApiOperation(value = "registrarUnidadManejoArchivo" , authorizations = {@Authorization(value="JWT")})
    // @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    // public org.springframework.http.ResponseEntity registrarUnidadManejoArchivo(
    //                                                                             @RequestParam(required = false) Integer idUnidadManejo,
    //                                                                             @RequestParam(required = false) Integer idUsuarioRegistro,
    //                                                                             @RequestParam("attachments") MultipartFile[] attachments
    //                                                                             ){
    //     log.info("InformacionAreaManejo - registrarUnidadManejoArchivo");
    //     ResponseEntity result = null;
    //     ResultClassEntity response =new ResultClassEntity();
    //     try{
    //         int idTipo=7;
    //         UnidadManejoArchivoEntity obj = new UnidadManejoArchivoEntity();
    //         obj.setIdUnidadManejo(idUnidadManejo);
    //         obj.setIdUsuarioRegistro(idUsuarioRegistro);

    //             for( MultipartFile listaFile : attachments){

    //                 response =  serArchivo.RegistrarArchivoGeneral(listaFile, idUsuarioRegistro, "");
                   
    //                 if(response.getSuccess()){
    //                     obj.setIdArchivo(response.getCodigo());
    //                     obj.setEstado("A");
    //                     obj.setIdTipo(idTipo);
    //                     response = unidadManejoArchivoService.RegistrarUnidadManejo(obj);
    //                     if(!response.getSuccess()){
    //                         log.info("InformacionAreaManejo - registrarUnidadManejoArchivo","Proceso realizado con errores "+response.getMessage() );
    //                         return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);    
    //                     }
    //                 }else{
    //                     log.info("InformacionAreaManejo - registrarUnidadManejoArchivo","Proceso realizado con errores "+response.getMessage() );
    //                 }  
    //                 idTipo+=1;        
    //             }
            
    //         log.info("InformacionAreaManejo - registrarUnidadManejoArchivo","Proceso realizado correctamente");
    //         return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
    //         //return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
    //     }catch (Exception e){
    //         log.error("Service -registrarUnidadManejoArchivo","Ocurrió un error :"+ e.getMessage());
    //         result = buildResponse(Constantes.STATUS_ERROR, e.getMessage(), Constantes.MESSAGE_ERROR_500, e);
    //         return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    //     }
    // }

    @PostMapping(path = "/eliminarUnidadManejoArchivo")
    @ApiOperation(value = "eliminarUnidadManejoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarUnidadManejoArchivo(@RequestBody UnidadManejoArchivoEntity unidadManejoArchivoEntity){
        log.info("InformacionAreaManejo - eliminarUnidadManejoArchivo", unidadManejoArchivoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = unidadManejoArchivoService.EliminarUnidadManejoArchivo(unidadManejoArchivoEntity);
            log.info("InformacionAreaManejo - eliminarUnidadManejoArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -eliminarUnidadManejoArchivo","Ocurrió un error :"+ e.getMessage());

            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarUnidadManejoArchivo")
    @ApiOperation(value = "listarUnidadManejoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarUnidadManejoArchivo(@RequestBody UnidadManejoArchivoEntity request){
        log.info("InformacionAreaManejo - ListarUnidadManejoArchivo", request);
        ResultClassEntity result = null;
        try{
            result = unidadManejoArchivoService.ListarUnidadManejoArchivo(request);
            log.info("InformacionAreaManejo - ListarUnidadManejoArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
        }catch (Exception e){
            log.error("InformacionAreaManejo -ListarUnidadManejoArchivo","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, e.getMessage(), Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public static class FileUpload{

        private List<MultipartFile> listaFile;

        public List<MultipartFile> getListaFile() {
            return listaFile;
        }

        public void setListaFile(List<MultipartFile> listaFile) {
            this.listaFile = listaFile;
        }
    }


    @PostMapping(path = "/registrarAccesibilidadmanejo")
    @ApiOperation(value = "registrarAccesibilidadmanejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAccesibilidadmanejo(@RequestBody AccesibilidadManejoEntity accesibilidadManejoEntity){
        log.info("InformacionAreaManejo - registrarAccesibilidadmanejo",accesibilidadManejoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.RegistrarAccesibilidadManejo(accesibilidadManejoEntity);
            log.info("InformacionAreaManejo - RegistrarAccesibilidadManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -RegistrarAccesibilidadManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

 
    @PostMapping(path = "/registrarAspectoHidrografico")
    @ApiOperation(value = "registrarAspectoHidrografico" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAspectoHidrografico(@RequestBody List<AspectoHidrograficoEntity> lista){
        log.info("InformacionAreaManejo - registrarAspectoHidrografico", lista);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = aspectoHidrograficoService.RegistrarAspectoHidrografico(lista);
            log.info("InformacionAreaManejo - registrarCoordenadaUtm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - registrarCoordenadaUtm","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 
    @PostMapping(path = "/actualizarAccesibilidadmanejo")
    @ApiOperation(value = "actualizarAccesibilidadmanejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarAccesibilidadmanejo(@RequestBody AccesibilidadManejoEntity accesibilidadManejoEntity){
        log.info("InformacionAreaManejo - actualizarAccesibilidadmanejo",accesibilidadManejoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.ActualizarAccesibilidadManejo(accesibilidadManejoEntity);
            log.info("InformacionAreaManejo - ActualizarAccesibilidadManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ActualizarAccesibilidadManejo","Ocurrió un error :"+ e.getMessage()); 
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarAspectoHidrografico")
    @ApiOperation(value = "actualizarAspectoHidrografico" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarAspectoHidrografico(@RequestBody List<AspectoHidrograficoEntity> lista){
        log.info("InformacionAreaManejo - actualizarAspectoHidrografico", lista);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = aspectoHidrograficoService.ActualizarAspectoHidrografico(lista);
            log.info("InformacionAreaManejo - actualizarAspectoHidrografico","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - actualizarAspectoHidrografico","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarAspectoHidrografico")
    @ApiOperation(value = "eliminarAspectoHidrografico" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarAspectoHidrografico(@RequestBody AspectoHidrograficoEntity aspectoHidrograficoEntity){
        log.info("InformacionAreaManejo - eliminarAspectoHidrografico", aspectoHidrograficoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = aspectoHidrograficoService.EliminarAspectoHidrografico(aspectoHidrograficoEntity);
            log.info("InformacionAreaManejo - eliminarAspectoHidrografico","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -eliminarAspectoHidrografico","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/listarAccesibilidadmanejorequisito")
    @ApiOperation(value = "listarAccesibilidadmanejorequisito" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarAccesibilidadmanejorequisito(@RequestBody AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity){
        log.info("InformacionAreaManejo - listarAccesibilidadmanejorequisito", accesibilidadManejoRequisitoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.ListarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
            log.info("InformacionAreaManejo - ListarAccesibilidadManejoRequisito","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ListarAccesibilidadManejoRequisito","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarAccesibilidadmanejorequisito")
    @ApiOperation(value = "registrarAccesibilidadmanejorequisito" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAccesibilidadmanejorequisito(@RequestBody List<AccesibilidadManejoRequisitoEntity> accesibilidadManejoRequisitoEntity){
        log.info("InformacionAreaManejo - registrarAccesibilidadmanejorequisito",accesibilidadManejoRequisitoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.RegistrarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
            log.info("InformacionAreaManejo - RegistrarAccesibilidadManejoRequisito","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - RegistrarAccesibilidadManejoRequisito","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarAccesibilidadmanejorequisito")
    @ApiOperation(value = "actualizarAccesibilidadmanejorequisito" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarAccesibilidadmanejorequisito(@RequestBody List<AccesibilidadManejoRequisitoEntity> accesibilidadManejoRequisitoEntity){
        log.info("InformacionAreaManejo - actualizarAccesibilidadmanejo",accesibilidadManejoRequisitoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.ActualizarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
            log.info("InformacionAreaManejo - ActualizarAccesibilidadManejoRequisito","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ActualizarAccesibilidadManejoRequisito","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarAccesibilidadmanejorequisito")
    @ApiOperation(value = "eliminarAccesibilidadmanejorequisito" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarAccesibilidadmanejorequisito(@RequestBody AccesibilidadManejoRequisitoEntity accesibilidadManejoRequisitoEntity){
        log.info("InformacionAreaManejo - actualizarAccesibilidadmanejo",accesibilidadManejoRequisitoEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = accesibilidadService.EliminarAccesibilidadManejoRequisito(accesibilidadManejoRequisitoEntity);
            log.info("InformacionAreaManejo - EliminarAccesibilidadManejoRequisito","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - EliminarAccesibilidadManejoRequisito","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarAspectoHidrografico")
    @ApiOperation(value = "listarAspectoHidrografico" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarAspectoHidrografico(@RequestBody AspectoHidrograficoEntity aspectoHidrograficoEntity){
        log.info("InformacionAreaManejo - listarAspectoHidrografico", aspectoHidrograficoEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = aspectoHidrograficoService.ListarAspectoHidrografico(aspectoHidrograficoEntity);
            log.info("InformacionAreaManejo - listarAspectoHidrografico","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service -listarAspectoHidrografico","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarAccesibilidadmanejomatriz")
    @ApiOperation(value = "listarAccesibilidadmanejomatriz" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarAccesibilidadmanejomatriz(@RequestBody AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto) {

        log.info("InformacionAreaManejo - listarAccesibilidadmanejomatriz",accesibilidadManejoMatrizDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.ListarAccesibilidadManejoMatriz(accesibilidadManejoMatrizDto);
            log.info("InformacionAreaManejo - ListarAccesibilidadManejoMatriz","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ListarAccesibilidadManejoMatriz","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/registrarAccesibilidadmanejomatriz")
    @ApiOperation(value = "registrarAccesibilidadmanejomatriz" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAccesibilidadmanejomatriz(@RequestBody List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) {
        log.info("InformacionAreaManejo - registrarAccesibilidadmanejomatriz",listAccesibilidadManejoMatrizDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = accesibilidadService.RegistrarAccesibilidadManejoMatriz(listAccesibilidadManejoMatrizDto);
            log.info("InformacionAreaManejo - RegistrarAccesibilidadManejoMatriz","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - RegistrarAccesibilidadManejoMatriz","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarAccesibilidadmanejomatriz")
    @ApiOperation(value = "actualizarAccesibilidadmanejomatriz" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarAccesibilidadmanejomatriz(@RequestBody List<AccesibilidadManejoMatrizDto> listAccesibilidadManejoMatrizDto) {
        log.info("InformacionAreaManejo - actualizarAccesibilidadmanejomatriz",listAccesibilidadManejoMatrizDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;

        try{
            response = accesibilidadService.ActualizarAccesibilidadManejoMatriz(listAccesibilidadManejoMatrizDto);
            log.info("InformacionAreaManejo - ActualizarAccesibilidadManejoMatriz","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ActualizarAccesibilidadManejoMatriz","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/eliminarAccesibilidadmanejomatriz")
    @ApiOperation(value = "eliminarAccesibilidadmanejomatriz" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarAccesibilidadmanejomatriz(@RequestBody AccesibilidadManejoMatrizDto accesibilidadManejoMatrizDto){
        log.info("InformacionAreaManejo - eliminarAccesibilidadmanejomatriz",accesibilidadManejoMatrizDto.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = accesibilidadService.EliminarAccesibilidadManejoMatriz(accesibilidadManejoMatrizDto);
            log.info("InformacionAreaManejo - EliminarAccesibilidadManejoMatriz","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - EliminarAccesibilidadManejoMatriz","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarAccesibilidadmanejomatrizdetalle")
    @ApiOperation(value = "eliminarAccesibilidadmanejomatrizdetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarAccesibilidadmanejomatrizdetalle(@RequestBody AccesibilidadManejoMatrizDetalleEntity accesibilidadManejoMatrizDetalleEntity){
        log.info("InformacionAreaManejo - eliminarAccesibilidadmanejomatrizdetalle",accesibilidadManejoMatrizDetalleEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = accesibilidadService.EliminarAccesibilidadManejoMatrizDetalle(accesibilidadManejoMatrizDetalleEntity);
            log.info("InformacionAreaManejo - EliminarAccesibilidadManejoMatriz","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - EliminarAccesibilidadManejoMatrizDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*************************************** */
    @PostMapping(path = "/registroInfoEconomicaPgmf")
    @ApiOperation(value = "Registrar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> registroInfoEconomicaPgmf(@RequestBody InfoEconomicaPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - registroInfoEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.registroInfoEconomicaPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - registroInfoEconomicaPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/obtenerInfoEconomicaPgmf")
    @ApiOperation(value = "Obtener ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerInfoEconomicaPgmf(@RequestBody InfoEconomicaPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - obtenerInfoEconomicaPgmf", "inicio");
        ResultClassEntity res = new ResultClassEntity();
        try {
          
            res =serInfEco.obtenerInfoEconomicaPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - obtenerInfoEconomicaPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    
    @PostMapping(path = "/registroInfoEconomicaActEconomicaPgmf")
    @ApiOperation(value = "Registrar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> registroInfoEconomicaActEconomicaPgmf(@RequestBody List<InfoEconomicaActEconomicaPgmf>  request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - registroInfoEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.registroInfoEconomicaActEconomicaPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - registroInfoEconomicaActEconomicaPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/obtenerInfoEconomicaActEconomicaPgmf")
    @ApiOperation(value = "Obtener ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> obtenerInfoEconomicaActEconomicaPgmf(@RequestBody InfoEconomicaActEconomicaPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - obtenerInfoEconomicaActEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.obtenerInfoEconomicaActEconomicaPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - obtenerInfoEconomicaActEconomicaPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registroInfoEconomicaInfraestructuraPgmf")
    @ApiOperation(value = "Registrar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> registroInfoEconomicaInfraestructuraPgmf(@RequestBody List<InfoEconomicaInfraestructuraPgmf>  request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - registroInfoEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.registroInfoEconomicaInfraestructuraPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - registroInfoEconomicaInfraestructuraPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/obtenerInfoEconomicaInfraestructuraPgmf")
    @ApiOperation(value = "Obtener ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> obtenerInfoEconomicaInfraestructuraPgmf(@RequestBody InfoEconomicaInfraestructuraPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - obtenerInfoEconomicaInfraestructuraPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.obtenerInfoEconomicaInfraestructuraPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - obtenerInfoEconomicaInfraestructuraPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage() );
            
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/registroInfoEconomicaActividadesPgmf")
    @ApiOperation(value = "Registrar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> registroInfoEconomicaActividadesPgmf(@RequestBody List<InfoEconomicaActividadesPgmf>  request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - registroInfoEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.registroInfoEconomicaActividadesPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - registroInfoEconomicaActividadesPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/obtenerInfoEconomicaActividadesPgmf")
    @ApiOperation(value = "Obtener ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> obtenerInfoEconomicaActividadesPgmf(@RequestBody InfoEconomicaActividadesPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - obtenerInfoEconomicaInfraestructuraPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.obtenerInfoEconomicaActividadesPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - obtenerInfoEconomicaActividadesPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/registroInfoEconomicaConfictosPgmf")
    @ApiOperation(value = "Registrar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> registroInfoEconomicaConfictosPgmf(@RequestBody List<InfoEconomicaConfictosPgmf>  request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - registroInfoEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.registroInfoEconomicaConfictosPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - registroInfoEconomicaConfictosPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/obtenerInfoEconomicaConfictosPgmf")
    @ApiOperation(value = "Obtener ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> obtenerInfoEconomicaConfictosPgmf(@RequestBody InfoEconomicaConfictosPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - obtenerInfoEconomicaConfictosPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.obtenerInfoEconomicaConfictosPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - obtenerInfoEconomicaConfictosPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarInfoEconomicaConfictosPgmf")
    @ApiOperation(value = "Eliminar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> EliminarInfoEconomicaConfictosPgmf(@RequestBody InfoEconomicaConfictosPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - EliminarInfoEconomicaConfictosPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.EliminarInfoEconomicaConfictosPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - EliminarInfoEconomicaConfictosPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping(path = "/registroInfoBiologicoFaunaSilvestrePgmf")
    @ApiOperation(value = "Registrar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> registroInfoBiologicoFaunaSilvestrePgmf(@RequestBody List<InfoBiologicoFaunaSilvestrePgmf> request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - registroInfoBiologicoFaunaSilvestrePgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.registroInfoBiologicoFaunaSilvestrePgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - registroInfoBiologicoFaunaSilvestrePgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/obtenerInfoBiologicoFaunaSilvestrePgmf")
    @ApiOperation(value = "Obtener ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> obtenerInfoBiologicoFaunaSilvestrePgmf(@RequestBody  InfoBiologicoFaunaSilvestrePgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - obtenerInfoBiologicoFaunaSilvestrePgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.obtenerInfoBiologicoFaunaSilvestrePgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - obtenerInfoBiologicoFaunaSilvestrePgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(path = "/EliminarBiologicoFaunaSilvestrePgmf")
    @ApiOperation(value = "Eliminar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> EliminarBiologicoFaunaSilvestrePgmf(@RequestBody InfoBiologicoFaunaSilvestrePgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - EliminarBiologicoFaunaSilvestrePgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.EliminarBiologicoFaunaSilvestrePgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - EliminarBiologicoFaunaSilvestrePgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarInfoEconomicaInfraestructuraPgmf")
    @ApiOperation(value = "Eliminar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> EliminarInfoEconomicaInfraestructuraPgmf(@RequestBody InfoEconomicaInfraestructuraPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - EliminarInfoEconomicaInfraestructuraPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.EliminarInfoEconomicaInfraestructuraPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - EliminarInfoEconomicaInfraestructuraPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarInfoEconomicaActEconomicaPgmf")
    @ApiOperation(value = "Eliminar ", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> EliminarInfoEconomicaActEconomicaPgmf(@RequestBody InfoEconomicaActEconomicaPgmf request,HttpServletRequest request1){
        log.info("InformacionAreaManejoController - EliminarInfoEconomicaActEconomicaPgmf", "inicio");
        ResultEntity res = new ResultEntity();
        try {
          
            res =serInfEco.EliminarInfoEconomicaActEconomicaPgmf(request);   
            
                return new org.springframework.http.ResponseEntity<>(res, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("InformacionAreaManejoController - EliminarInfoEconomicaActEconomicaPgmf", "Ocurrió un error en: "+Ex.getMessage());
            res.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }




}
