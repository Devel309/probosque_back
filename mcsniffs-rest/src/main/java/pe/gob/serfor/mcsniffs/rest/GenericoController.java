package pe.gob.serfor.mcsniffs.rest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.CondicionMinimaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima.ConsultaRequisitosDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Generico.ParametroDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.GenericoService;
import pe.gob.serfor.mcsniffs.service.client.FeignPideApi;


@RestController
@RequestMapping(Urls.generico.BASE)
public class GenericoController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(GenericoController.class);



    @Autowired
    private GenericoService genericoService;

 
    /**
     * @autor: Ivan Minaya [28-06-2021]
     * @modificado:
     * @descripción: {Método listar por filtro los parametros}
     * @param: ParametroEntity
     *
     * @return: ResponseEntity
     */
    @PostMapping(path = "/listarPorFiltroParametro")
    @ApiOperation(value = "listarPorFiltroParametro" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPorFiltroParametro(@RequestBody ParametroEntity parametro){
        try{
            return  new ResponseEntity (true, "ok", genericoService.ListarPorFiltroParametro(parametro));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {lista por filtro el TipoRegente}
     * @param: TipoRegenteEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @ApiOperation(value = "listarPorFiltroPersona", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/listarPorFiltroPersona")
    public org.springframework.http.ResponseEntity<ResultEntity<PersonaEntity>> listarPorFiltroPersona(@RequestBody PersonaEntity persona){
        log.info("CoreCentral - listarPorFiltroPersona",persona.toString());
        ResultEntity<PersonaEntity> result = new ResultEntity();
        try {
            result = genericoService.listarPorFilroPersona(persona);
            log.info("CoreCentral - listarPorFiltroPersona","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CoreCentral - listarPorFilroProvincia", "Ocurrió un error Listar las provincias: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "descargarPdf", authorizations = @Authorization(value = "JWT"))
    @PostMapping("/descargarPdf")
    public org.springframework.http.ResponseEntity<ResultClassEntity<String>> descargarPdf(@RequestBody PersonaEntity persona){
         
        ResultClassEntity<String> result = new ResultClassEntity();
        try {
            result = genericoService.descargarPdf();
            
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("CoreCentral - descargarPdf", "Ocurrió un error Listar las provincias: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/listarParametroPorPrefijo")
    @ApiOperation(value = "listarParametroPorPrefijo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarParametroPorPrefijo(@RequestBody ParametroDto prefijo){
        log.info("Evaluacion - listarParametroPorPrefijo",prefijo.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = genericoService.listarParametroPorPrefijo(prefijo);
            log.info("evaluacionDetalleService - listarParametroPorPrefijo","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("evaluacionDetalleService - listarParametroPorPrefijo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarParametroLineamiento")
    @ApiOperation(value = "listarParametroLineamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarParametroLineamiento(@RequestBody ParametroNivel1Entity param){
        log.info("Evaluacion - listarParametroPorPrefijo",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = genericoService.listarParametroLineamiento(param);
            log.info("listarParametroLineamiento - listarParametroLineamiento","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("listarParametroLineamiento - listarParametroLineamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 
}
