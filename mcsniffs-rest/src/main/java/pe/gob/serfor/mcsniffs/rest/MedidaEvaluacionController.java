package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion.MedidaDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.MedidaDetalleService;
import pe.gob.serfor.mcsniffs.service.MedidaService;

/**
 * @autor: Renzo Gabriel Meneses Gamarra [24-09-2021]
 * @modificado:
 * @descripción: { Controlador para el Plan Manejo Evaluación }
 */
@RestController
@RequestMapping(Urls.medidaEvaluacion.BASE)
public class MedidaEvaluacionController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(MedidaEvaluacionController.class);

    @Autowired
    private MedidaService medidaService;

    @Autowired
    private MedidaDetalleService medidaDetalleService;
 
    @PostMapping(path = "/listarMedida")
    @ApiOperation(value = "listarMedida" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarMedida(@RequestBody MedidaDto param){
        log.info("MedidaEvaluacion - listarMedida",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = medidaService.listarMedida(param);
            log.info("MedidaEvaluacion - listarMedida","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MedidaEvaluacion - listarMedida","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarMedida")
    @ApiOperation(value = "registrarMedida" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarMedida(@RequestBody MedidaDto param){
        log.info("MedidaEvaluacion - registrarMedida",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = medidaService.registrarMedida(param);
            log.info("MedidaEvaluacion - registrarMedida","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MedidaEvaluacion - registrarMedida","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarMedida")
    @ApiOperation(value = "eliminarMedida" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarMedida(@RequestBody MedidaDto param){
        log.info("MedidaEvaluacion - eliminarMedida",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = medidaService.eliminarMedida(param);
            log.info("MedidaEvaluacion - eliminarMedida","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MedidaEvaluacion - eliminarMedida","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @PostMapping(path = "/listarMedidaDetalle")
    @ApiOperation(value = "listarMedidaDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarMedidaDetalle(@RequestBody MedidaDetalleDto param){
        log.info("MedidaEvaluacion - listarMedidaDetalle",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = medidaDetalleService.listarMedidaDetalle(param);
            log.info("MedidaEvaluacion - listarMedidaDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MedidaEvaluacion - listarMedidaDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarMedidaDetalle")
    @ApiOperation(value = "eliminarMedidaDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarMedidaDetalle(@RequestBody List<MedidaDetalleDto> param){
        log.info("MedidaEvaluacion - eliminarMedidaDetalle",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = medidaDetalleService.eliminarMedidaDetalle(param);
            log.info("MedidaEvaluacion - eliminarMedidaDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MedidaEvaluacion - eliminarMedidaDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 
}
