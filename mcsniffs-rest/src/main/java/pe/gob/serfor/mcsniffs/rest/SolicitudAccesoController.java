package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.SolicitudAccesoService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(Urls.solicitudAcceso.BASE)
public class SolicitudAccesoController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudAccesoController.class);

    @Autowired
    private SolicitudAccesoService solicitudAccesoService;

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para consultar Solicitudes de Acceso}
     * @param: Objeto SolicitudAccesoEntity
     * @return: ResponseEntity
     */
    @PostMapping(path = "/ListarSolicitudAcceso")
    @ApiOperation(value = "ListarSolicitudAcceso", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity ListarSolicitudAcceso(@RequestBody SolicitudAccesoEntity solicitudAcceso) {
        try {
            return new ResponseEntity(true, "ok", solicitudAccesoService.ListarSolicitudAcceso(solicitudAcceso));
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity(false, e.getMessage(), null);
        }
    }

    @PostMapping(path = "/ObtenerSolicitudAcceso")
    @ApiOperation(value = "ObtenerSolicitudAcceso", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public ResponseEntity ObtenerSolicitudAcceso(@RequestBody SolicitudAccesoEntity solicitudAcceso) {
        try {
            return new ResponseEntity(true, "ok", solicitudAccesoService.ObtenerSolicitudAcceso(solicitudAcceso));
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity(false, e.getMessage(), null);
        }
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para registrar Solicitudes de Acceso}
     * @param: Objeto SolicitudAccesoEntity
     * @return: ResponseEntity
     */
    @PostMapping(path = "/RegistrarSolicitudAcceso")
    @ApiOperation(value = "RegistrarSolicitudAcceso")
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public org.springframework.http.ResponseEntity<ResponseEntity> RegistrarSolicitudAcceso(@RequestBody SolicitudAccesoEntity solicitudAcceso, HttpServletRequest request1) {
        log.info("SolicitudAcceso - RegistrarSolicitudAcceso", solicitudAcceso.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {

            response = solicitudAccesoService.RegistrarSolicitudAcceso(solicitudAcceso);

            String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
            token = "";
            if(response.getSuccess()) {
                solicitudAcceso.setIdSolicitudAcceso(response.getCodigo());
                solicitudAccesoService.enviarEmailRegistroSolicitudAcceso(solicitudAcceso, token);
            }
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("SolicitudAcceso - RegistrarSolicitudAcceso", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        } catch (Exception e) {
            log.error("SolicitudAcceso - RegistrarSolicitudAcceso", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Julio Meza Vela [28-06-2021]
     * @modificado:
     * @descripción: {Método creada para actualizar Solicitudes de Acceso cambiando su estado y datos de sus revisión.}
     * @param: Objeto SolicitudAccesoEntity
     * @return: ResponseEntity
     */
    @PostMapping(path = "/ActualizarRevisionSolicitudAcceso")
    @ApiOperation(value = "ActualizarRevisionSolicitudAcceso", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public org.springframework.http.ResponseEntity<ResponseEntity> ActualizarRevisionSolicitudAcceso(@RequestBody SolicitudAccesoEntity solicitudAcceso) {
        log.info("SolicitudAcceso - ActualizarRevisionSolicitudAcceso", solicitudAcceso.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {

            response = solicitudAccesoService.ActualizarRevisionSolicitudAcceso(solicitudAcceso);
            boolean boolEnvio = solicitudAccesoService.envioEmail(solicitudAcceso);
            if (boolEnvio) {
                log.info("EmailController - SendEmail", "Se envío correo");

            } else {
                log.warn("EmailApi - SendEmail", "No se envío correo");
            }
            result = buildResponse(response.getSuccess(), response, response.getMessage(), null);
            log.info("SolicitudAcceso - ActualizarRevisionSolicitudAcceso", "Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);


        } catch (Exception e) {
            log.error("SolicitudAcceso - RegistrarSolicitudAcceso", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/ListarSolicitudAccesoUsuario")
    @ApiOperation(value = "ListarSolicitudAccesoUsuario", authorizations = {@Authorization(value = "JWT")})
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado")})
    public org.springframework.http.ResponseEntity<ResultClassEntity> ListarSolicitudAccesoUsuario(@RequestBody SolicitudAccesoEntity solicitudAcceso) {
        ResultClassEntity resultClassEntity = null;
        try {
            resultClassEntity = solicitudAccesoService.listarSolicitudAccesoUsuario(solicitudAcceso);
            return new org.springframework.http.ResponseEntity(resultClassEntity,HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new org.springframework.http.ResponseEntity(resultClassEntity,HttpStatus.NOT_FOUND);
        }
    }
}
