package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal.SolPlantacionForestalEvaluacionDetalleDto;

import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.service.SolPlantacionForestalEvaluacionService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.solPlantacionForestalEvaluacion.BASE)
public class SolPlantacionForestalEvaluacionController extends AbstractRestController{

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolPlantacionForestalEvaluacionController.class);

    @Autowired
    SolPlantacionForestalEvaluacionService serviceSolicitudPlantacionForestalEvaluacion;

    /**
     * @autor: Wilfredo Elias [17-01-2022]
     * @modificado:
     * @descripción: {Obtener Evaluaciones de Solicitud Plantacion Forestal por filtros}
     * @param:SolPlantacionForestalEvaluacionDto
     */
    @PostMapping(path = "/obtenerInfoEvaluacionSolicitudPlantacionForestal")
    @ApiOperation(value = "obtener Información de Evaluación de Solicitud Plantación Forestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoEvaluacionSolicitudPlantacionForestal(@RequestBody SolPlantacionForestalEvaluacionDto request) {
        log.info("SolPlantacionForestalEvaluacion - obtenerInfoEvaluacionSolicitudPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudPlantacionForestalEvaluacion.obtenerEvaluacionSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestalEvaluacion - obtenerInfoEvaluacionSolicitudPlantacionForestal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalEvaluacion - obtenerInfoEvaluacionSolicitudPlantacionForestal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [17-01-2022]
     * @modificado:
     * @descripción: {Obtener Detalle de las Evaluaciones de Solicitud Plantacion Forestal por filtros}
     * @param:SolPlantacionForestalEvaluacionDto
     */
    @PostMapping(path = "/obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal")
    @ApiOperation(value = "Obtener Información del Detalle de las Evaluaciones de Solicitud Plantación Forestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal(@RequestBody SolPlantacionForestalEvaluacionDetalleDto request) {
        log.info("SolPlantacionForestalEvaluacion - obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudPlantacionForestalEvaluacion.obtenerDetalleEvaluacionSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestalEvaluacion - obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalEvaluacion - obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [17-01-2022]
     * @modificado:
     * @descripción: {Registrar Evaluación de Solicitud Plantación Forestal}
     * @param:SolPlantacionForestalEvaluacionDto
     */
    @PostMapping(path = "/registrarEvaluacionSolicitudPlantacionForestal")
    @ApiOperation(value = "Registrar Evaluación de Solicitud Plantación Forestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacionSolicitudPlantacionForestal(@RequestBody SolPlantacionForestalEvaluacionDto request) {
        log.info("SolPlantacionForestalEvaluacion - registrarEvaluacionSolicitudPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudPlantacionForestalEvaluacion.registrarEvaluacionSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestalEvaluacion - registrarEvaluacionSolicitudPlantacionForestal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalEvaluacion - registrarEvaluacionSolicitudPlantacionForestal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [17-01-2022]
     * @modificado:
     * @descripción: {Actualizar Evaluación de Solicitud Plantación Forestal}
     * @param:SolPlantacionForestalEvaluacionDto
     */
    @PostMapping(path = "/actualizarEvaluacionSolicitudPlantacionForestal")
    @ApiOperation(value = "Actualizar Evaluación de Solicitud Plantación Forestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarEvaluacionSolicitudPlantacionForestal(@RequestBody SolPlantacionForestalEvaluacionDto request) {
        log.info("SolPlantacionForestalEvaluacion - actualizarEvaluacionSolicitudPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudPlantacionForestalEvaluacion.actualizarEvaluacionSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestalEvaluacion - actualizarEvaluacionSolicitudPlantacionForestal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalEvaluacion - actualizarEvaluacionSolicitudPlantacionForestal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: Wilfredo Elias [17-01-2022]
     * @modificado:
     * @descripción: {Actualizar Evaluación de Solicitud Plantación Forestal}
     * @param:SolPlantacionForestalEvaluacionDto
     */
    @PostMapping(path = "/registrarInfoDetalleEvaluacionSolicitudPlantacionForestal")
    @ApiOperation(value = "Registrar Información Detalle de Evaluación de Solicitud Plantación Forestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarDetalleEvaluacionSolicitudPlantacionForestal(@RequestBody List<SolPlantacionForestalEvaluacionDetalleDto> request) {
        log.info("SolPlantacionForestalEvaluacion - registrarDetalleEvaluacionSolicitudPlantacionForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = serviceSolicitudPlantacionForestalEvaluacion.registrarDetalleEvaluacionSolicitudPlantacionForestal(request);
            log.info("SolPlantacionForestalEvaluacion - registrarDetalleEvaluacionSolicitudPlantacionForestal", "Proceso realizado correctamente");
            if (response.getSuccess()) {
                response.setMessage("Se procesó el detalle de evaluación de la solicitud de plantación forestal correctamente");
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
            } else {
                response.setMessage("No se pudo procesar el detalle de evaluación de la solicitud de plantación forestal.");
                return new org.springframework.http.ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("SolPlantacionForestalEvaluacion - registrarDetalleEvaluacionSolicitudPlantacionForestal", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
