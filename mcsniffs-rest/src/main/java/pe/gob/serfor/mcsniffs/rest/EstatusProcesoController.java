package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.Dto.EstatusProceso.ListarEstadoDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EstatusProcesoService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.estatusProceso.BASE)
public class EstatusProcesoController extends AbstractRestController{


   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EstatusProcesoController.class);

    @Autowired
    private EstatusProcesoService estatusProcesoService;


    /**
     * @autor: Jordy Zamata [16-11-2021]
     * @modificado:
     * @descripción: {Servicio donde se listan los estados del proceso según el tipo de estatus}
     *
     */

    @PostMapping(path = "/listarEstadoEstatusProceso")
    @ApiOperation(value = "listarEstadoEstatusProceso por tipoStatus", authorizations = {@Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),@ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarEstadoEstatusProceso(@RequestBody ListarEstadoDto request) {
        log.info("listarEstado: {}", request);

        ResultClassEntity response = new ResultClassEntity();
        try {

            response = estatusProcesoService.listarEstadoEstatusProceso(request);
            log.info("Service - listarEstadoEstatusProceso: Proceso realizado correctamente");


            if (!response.getSuccess()) {
                return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Service - listarEstado: Ocurrió un error: {}", e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}


