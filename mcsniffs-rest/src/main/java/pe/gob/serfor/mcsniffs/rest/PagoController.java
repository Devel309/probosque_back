package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.EspecieForestalEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.repository.EvaluacionRepository;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.PagoService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.pago.BASE)
public class PagoController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(PagoController.class);
    @Autowired
    private EmailService service;

    @Autowired
    private PagoService pagoService;


    @PostMapping(path = "/registrarPago")
    @ApiOperation(value = "registrarPago", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarPago(
            @RequestBody List<PagoEntity> list) {
        log.info("registrarPago - registrarPago", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = pagoService.RegistrarPago(list);
            log.info("registrarPago - registrarPago", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("registrarPago -registrarPago", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



/*
    @PostMapping(path = "/listarPago")
    @ApiOperation(value = "listarPago" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarPago(@RequestBody PagoEntity param){
        try{
            return  new ResponseEntity (true, "ok", pagoService.ListarPago(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }
*/

    @PostMapping(path = "/listarPago")
    @ApiOperation(value = "listarPago" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarPago(@RequestBody PagoEntity request){
        log.info("listarPago - listarPago", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = pagoService.ListarPago(request);
            log.info("listarPago - listarPago","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("listarPago - listarPago","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarPago")
    @ApiOperation(value = "eliminarPago" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarPago(@RequestBody PagoPeriodoEntity param){
        log.info("eliminarPago - eliminarPago",param.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = pagoService.EliminarPago(param);
            log.info("eliminarPago - eliminarPago","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("eliminarPago - eliminarPago","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/registrarDescuento")
    @ApiOperation(value = "registrarDescuento", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarDescuento(
            @RequestBody List<DescuentoEntity> list) {
        log.info("registrarDescuento - registrarDescuento", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = pagoService.RegistrarDescuento(list);
            log.info("registrarDescuento - registrarDescuento", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("registrarDescuento -registrarDescuento", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @PostMapping(path = "/listarDescuento")
    @ApiOperation(value = "listarDescuento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarDescuento(@RequestBody DescuentoEntity param){
        try{
            return  new ResponseEntity (true, "ok", pagoService.ListarDescuento(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/eliminarDescuento")
    @ApiOperation(value = "eliminarDescuento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarDescuento(@RequestBody DescuentoEntity param){
        log.info("eliminarDescuento - eliminarDescuento",param.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = pagoService.EliminarDescuento(param);
            log.info("eliminarDescuento - eliminarDescuento","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("eliminarDescuento - eliminarDescuento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   /* @PostMapping(path = "/listarTH")
    @ApiOperation(value = "listarTH" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarTH(@RequestBody THEntity param){
        try{
            return  new ResponseEntity (true, "ok", pagoService.listarTH(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }*/

    @PostMapping(path = "/listarTH")
    @ApiOperation(value = "listarTH" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarTH(@RequestBody THEntity request){
        log.info("listarTH - listarTH", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = pagoService.listarTH(request);
            log.info("listarTH - listarTH","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("listarTH - listarTH","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/registrarArchivo")
    @ApiOperation(value = "registrarArchivo", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarArchivo(
            @RequestBody List<PagoArchivoEntity> list) {
        log.info("registrarArchivo - registrarArchivo", list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = pagoService.RegistrarArchivo(list);
            log.info("permisoForestal - registrarArchivo", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("permisoForestal -registrarArchivo", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarArchivo")
    @ApiOperation(value = "listarArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public pe.gob.serfor.mcsniffs.entity.ResponseEntity listarArchivo(@RequestBody PagoArchivoEntity param){
        try{
            return  new pe.gob.serfor.mcsniffs.entity.ResponseEntity(true, "ok", pagoService.ListarArchivo(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new pe.gob.serfor.mcsniffs.entity.ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/eliminarArchivo")
    @ApiOperation(value = "eliminarArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarArchivo(@RequestBody PagoArchivoEntity pagoArchivoEntity){
        log.info("SolicitudSAN - eliminarArchivo",pagoArchivoEntity.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = pagoService.EliminarArchivo(pagoArchivoEntity);
            log.info("SolicitudSAN - eliminarArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("SolicitudSAN - eliminarArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarPagoCronogramaAnios")
    @ApiOperation(value = "listarPagoCronogramaAnios" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarPagoCronogramaAnios(@RequestBody PagoCronogramaEntity request){
        log.info("listarPagoCronogramaAnios - listarPagoCronogramaAnios", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = pagoService.listarPagoCronogramaAnios(request);
            log.info("listarPagoCronogramaAnios - listarPagoCronogramaAnios","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("listarPagoCronogramaAnios - listarPagoCronogramaAnios","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping(path = "/registrarGuiaPago")
    @ApiOperation(value = "registrarGuiaPago", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarGuiaPago(
            @RequestBody List<PagoGuiaEntity> list) {
        log.info("registrarGuiaPago - registrarGuiaPago", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = pagoService.RegistrarGuiaPago(list);
            log.info("registrarGuiaPago - registrarGuiaPago", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("registrarGuiaPago -registrarGuiaPago", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarGuiaPago")
    @ApiOperation(value = "listarGuiaPago" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> listarGuiaPago(@RequestBody PagoGuiaEntity request){
        log.info("listarGuiaPago - listarGuiaPago", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = pagoService.ListarGuiaPago(request);
            log.info("listarGuiaPago - listarGuiaPago","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("listarGuiaPago - listarGuiaPago","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            res.setTotalrecord(res.getTotalrecord());
            return new org.springframework.http.ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/listarModalidad")
    @ApiOperation(value = "listarModalidad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarModalidad(@RequestBody PagoEntity param){
        try{
            return  new ResponseEntity (true, "ok", pagoService.listarModalidad(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/listarOperativosPorPGM")
    @ApiOperation(value = "listarOperativosPorPGM" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarOperativosPorPGM(@RequestBody THEntity param){
        try{
            return  new ResponseEntity (true, "ok", pagoService.listarOperativosPorPGM(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

}
