package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.CensoForestalListarDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.ContratoTHDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CensoForestalListarService;

import java.util.Arrays;


@RestController
@RequestMapping(Urls.censoForestal.BASE)
public class CensoForestalController extends AbstractRestController {
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionResponsableController.class);

    @Autowired
    private CensoForestalListarService censoForestalListarService;
    
    @PostMapping(path = "/censoForestalListar")
    @ApiOperation(value = "Listar Censo Forestal" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity censoForestalListar(@RequestBody CensoForestalListarDto request) {
        log.info("censoForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = censoForestalListarService.CensoForestalListar(request);
            log.info("SolicitudConcesionResponsable - obtenerSolicitudConcesionResponsable", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionActividad - listarSolicitudConcesion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(path = "/listaTHContratoPorFiltro")
    @ApiOperation(value = "listaTHContratoPorFiltro", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado"), @ApiResponse(code = 403, message = "No autorizado") })
    public org.springframework.http.ResponseEntity listaTHContratoPorFiltro(@RequestBody ContratoTHDto param) {

        log.info("Impugnacion - listaTHContratoPorFiltro", param.toString());

        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = censoForestalListarService.listaTHContratoPorFiltro(param);
            log.info("Impugnacion - validarPlazoImpugnacionResolucion", "Proceso realizado correctamente");

            if (response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Impugnacion - validarPlazoImpugnacionResolucion", "Ocurrió un error :" + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
