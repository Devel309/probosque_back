package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.N313_HU03.InfBasicaAereaDetalleDto;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionCategoriaUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProteccionDivAdmUMFEntity;
import pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF.OrdenProyecionVigilanciaUMFEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.serfor.mcsniffs.service.OrdenamientoProteccionService;

@RestController
@RequestMapping(Urls.ordenamientoProteccion.BASE)
public class OrdenamientoProteccionController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(OrdenamientoProteccionController.class);

    @Autowired
    private OrdenamientoProteccionService serv;

    /**
     * @autor: Harry Coa [18-07-2021]
     * @modificado: Edwin M.
     * @descripción: {Registrar Ordenamiento Proteccion}
     * @param:OrdenamientoProteccionEntity
     */

    @PostMapping(path = "/registrarOrdenamientoProteccionCategoria")
    @ApiOperation(value = "registrar Categoria" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> registrarOrdenamientoProteccionCategoria(@RequestBody List<OrdenProteccionCategoriaUMFEntity> request){
        log.info("OrdenamientoProteccion - registrarOrdenamientoProteccionCategoria", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.RegistrarOrdenamientoProteccionCategoria(request);
            log.info("OrdenamientoProteccion - registrarOrdenamientoProteccionCategoria","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - registrarOrdenamientoProteccionCategoria","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarOrdenamientoProteccionCategoria")
    @ApiOperation(value = "Eliminar Categoria" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarOrdenamientoProteccionCategoria(@RequestBody OrdenProteccionCategoriaUMFEntity request){
        log.info("OrdenamientoProteccion - EliminarOrdenamientoProteccionCategoria", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.EliminarOrdenamientoProteccionCategoria(request);
            log.info("OrdenamientoProteccion - EliminarOrdenamientoProteccionCategoria","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - EliminarOrdenamientoProteccionCategoria","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/ObtenerOrdenamientoProteccionCategoria")
    @ApiOperation(value = "Obtener Categoria" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ObtenerOrdenamientoProteccionCategoria(@RequestBody OrdenProteccionCategoriaUMFEntity request){
        log.info("OrdenamientoProteccion - ObtenerOrdenamientoProteccionCategoria", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ObtenerOrdenamientoProteccionCategoria(request);
            log.info("OrdenamientoProteccion - ObtenerOrdenamientoProteccionCategoria","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - ObtenerOrdenamientoProteccionCategoria","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping(path = "/RegistrarOrdenamientoProteccionDivAdm")
    @ApiOperation(value = "registrar divicion Administracion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> RegistrarOrdenamientoProteccionDivAdm(@RequestBody List<OrdenProteccionDivAdmUMFEntity> request){
        log.info("OrdenamientoProteccion - RegistrarOrdenamientoProteccionDivAdm", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.RegistrarOrdenamientoProteccionDivAdm(request);
            log.info("OrdenamientoProteccion - RegistrarOrdenamientoProteccionDivAdm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - RegistrarOrdenamientoProteccionDivAdm","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarOrdenamientoProteccionDivAdm")
    @ApiOperation(value = "registrar divicion Administracion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarOrdenamientoProteccionDivAdm(@RequestBody OrdenProteccionDivAdmUMFEntity request){
        log.info("OrdenamientoProteccion - EliminarOrdenamientoProteccionDivAdm", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.EliminarOrdenamientoProteccionDivAdm(request);
            log.info("OrdenamientoProteccion - EliminarOrdenamientoProteccionDivAdm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - EliminarOrdenamientoProteccionDivAdm","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/ObtenerOrdenamientoProteccionDivAdm")
    @ApiOperation(value = "registrar divicion Administracion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ObtenerOrdenamientoProteccionDivAdm(@RequestBody OrdenProteccionDivAdmUMFEntity request){
        log.info("OrdenamientoProteccion - ObtenerOrdenamientoProteccionDivAdm", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ObtenerOrdenamientoProteccionDivAdm(request);
            log.info("OrdenamientoProteccion - ObtenerOrdenamientoProteccionDivAdm","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - ObtenerOrdenamientoProteccionDivAdm","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(path = "/RegistrarOrdenamientoProteccionVigilancia")
    @ApiOperation(value = "registrar Vigilancia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> RegistrarOrdenamientoProteccionVigilancia(@RequestBody List<OrdenProyecionVigilanciaUMFEntity> request){
        log.info("OrdenamientoProteccion - RegistrarOrdenamientoProteccionVigilancia", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.RegistrarOrdenamientoProteccionVigilancia(request);
            log.info("OrdenamientoProteccion - RegistrarOrdenamientoProteccionVigilancia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - RegistrarOrdenamientoProteccionVigilancia","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/EliminarOrdenamientoProteccionVigilancia")
    @ApiOperation(value = "Eliminar Vigilancia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> EliminarOrdenamientoProteccionVigilancia(@RequestBody OrdenProyecionVigilanciaUMFEntity request){
        log.info("OrdenamientoProteccion - EliminarOrdenamientoProteccionVigilancia", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.EliminarOrdenamientoProteccionVigilancia(request);
            log.info("OrdenamientoProteccion - EliminarOrdenamientoProteccionVigilancia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - EliminarOrdenamientoProteccionVigilancia","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/ObtenerOrdenamientoProteccionVigilancia")
    @ApiOperation(value = "Obtener Vigilancia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ObtenerOrdenamientoProteccionVigilancia(@RequestBody OrdenProyecionVigilanciaUMFEntity request){
        log.info("OrdenamientoProteccion - ObtenerOrdenamientoProteccionVigilancia", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ObtenerOrdenamientoProteccionVigilancia(request);
            log.info("OrdenamientoProteccion - ObtenerOrdenamientoProteccionVigilancia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);

        } catch (Exception e){
            log.error("OrdenamientoProteccion - ObtenerOrdenamientoProteccionVigilancia","Ocurrió un error :"+ e.getMessage());
            // result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            res.setMessage(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/ListarOrdenamientoProteccion")
    @ApiOperation(value = "Listar Ordenamiento Proteccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarOrdenamientoProteccion(@RequestBody OrdenamientoProteccionEntity request){
        log.info("OrdenamientoProteccion - ListarOrdenamientoProteccion", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ListarOrdenamientoProteccion(request);
            log.info("OrdenamientoProteccion - ListarOrdenamientoProteccion","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoProteccion - ListarOrdenamientoProteccion","Ocurrió un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/ListarOrdenamientoInterno")
    @ApiOperation(value = "Listar Ordenamiento Interno" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarOrdenamientoInterno(@RequestBody OrdenamientoProteccionEntity request){
        log.info("OrdenamientoInterno - ListarOrdenamientoInterno", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ListarOrdenamientoInterno(request);
            log.info("OrdenamientoInterno - ListarOrdenamientoInterno","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoInterno - ListarOrdenamientoInterno","Ocurrió un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/ListarOrdenamientoInternoDetalle")
    @ApiOperation(value = "Listar Ordenamiento Interno Detalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarOrdenamientoInternoDetalle(@RequestBody OrdenamientoProteccionEntity request){
        log.info("OrdenamientoInterno - ListarOrdenamientoInternoDetalle", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ListarOrdenamientoInternoDetalle(request);
            log.info("OrdenamientoInterno - ListarOrdenamientoInternoDetalle","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoInterno - ListarOrdenamientoInternoDetalle","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/ListarOrdenamientoInternoDetalleTitular")
    @ApiOperation(value = "Listar Ordenamiento Interno Detalle Titular" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity> ListarOrdenamientoInternoDetalleTitular(@RequestBody OrdenamientoProteccionEntity request){
        log.info("OrdenamientoInterno - ListarOrdenamientoInternoDetalleTitular", request.toString());
        //ResponseEntity<String> result = null;
        ResultEntity res = new ResultEntity() ;
        try {
            res = serv.ListarOrdenamientoInternoDetalleTitular(request);
            log.info("OrdenamientoInterno - ListarOrdenamientoInternoDetalleTitular","Proceso realizado correctamente");

            if (res.getIsSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoInterno - ListarOrdenamientoInternoDetalleTitular","Ocurrio un error :"+ e.getMessage());
            res.setIsSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/RegistrarOrdenamientoProteccion")
    @ApiOperation(value = "Registrar Ordenamiento Proteccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> RegistrarOrdenamientoProteccion(@RequestBody List<OrdenamientoProteccionEntity> request){
        log.info("OrdenamientoProteccion - RegistrarOrdenamientoProteccion", request.toString());

        ResultClassEntity res = new ResultClassEntity();
        try {
            res = serv.RegistrarOrdenamientoProteccion(request);
            log.info("OrdenamientoProteccion - RegistrarOrdenamientoProteccion","Proceso realizado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoProteccion - RegistrarOrdenamientoProteccion","Ocurrió un error :"+ e.getMessage());
            res.setSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/ActualizarOrdenamientoProteccion")
    @ApiOperation(value = "Actualizar Ordenamiento Proteccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> ActualizarOrdenamientoProteccion(@RequestBody List<OrdenamientoProteccionEntity> request){
        log.info("OrdenamientoProteccion - ActualizarOrdenamientoProteccion", request.toString());

        ResultClassEntity res = new ResultClassEntity();
        try {
            res = serv.ActualizarOrdenamientoProteccion(request);
            log.info("OrdenamientoProteccion - ActualizarOrdenamientoProteccion","Proceso realizado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoProteccion - ActualizarOrdenamientoProteccion","Ocurrió un error :"+ e.getMessage());
            res.setSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping(path = "/ListarOrdenamientoProteccionPorFiltro")
    @ApiOperation(value = "Listar Ordenamiento Proteccion por filtro" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> ListarOrdenamientoProteccionPorFiltro(@RequestParam(required = false) Integer idPlanManejo,
    @RequestParam(required = false) String codTipoOrdenamiento){
        log.info("OrdenamientoProteccion - ListarOrdenamientoProteccionPorFiltro" + idPlanManejo+" "+codTipoOrdenamiento);
        
        ResultClassEntity res = new ResultClassEntity();
        try {
            res = serv.ListarOrdenamientoProteccionPorFiltro(idPlanManejo, codTipoOrdenamiento);
            log.info("OrdenamientoProteccion - ListarOrdenamientoProteccionPorFiltro","listado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoProteccion - ListarOrdenamientoProteccionPorFiltro","Ocurrió un error :"+ e.getMessage());
            res.setSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/ActualizarOrdenamientoProteccionDetalle")
    @ApiOperation(value = "Actualizar Ordenamiento Proteccion Detalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> ActualizarOrdenamientoProteccionDetalle(@RequestBody List<OrdenamientoProteccionEntity> request){
        log.info("OrdenamientoProteccion - ActualizarOrdenamientoProteccionDetalle", request.toString());

        ResultClassEntity res = new ResultClassEntity();
        try {
            res = serv.ActualizarOrdenamientoProteccionDetalle(request);
            log.info("OrdenamientoProteccion - ActualizarOrdenamientoProteccionDetalle","Proceso realizado correctamente");

            if (res.getSuccess()) {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(res, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e){
            log.error("OrdenamientoProteccion - ActualizarOrdenamientoProteccionDetalle","Ocurrió un error :"+ e.getMessage());
            res.setSuccess(Constantes.STATUS_ERROR);
            res.setMessage(Constantes.MESSAGE_ERROR_500);
            res.setStackTrace(Arrays.toString(e.getStackTrace()));
            res.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(res,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @PostMapping(path = "/registrarOrdenamientoInterno")
    @ApiOperation(value = "registrarOrdenamientoInterno", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOrdenamientoInterno(
            @RequestBody List<OrdenamientoProteccionEntity> list) {
        log.info("registrarOrdenamientoInterno - ordenamientoProteccionDetalle", list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = serv.registrarOrdenamientoInterno(list);
            log.info("Informacion basica - ordenamientoProteccionDetalle", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("Ordenamiento - ordenamientoProteccionDetalle", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/proteccionVigilancia/cargarExcel")
    @ApiOperation(value = "proteccionVigilanciaCargarExcel", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity proteccionVigilanciaCargarExcel(@RequestParam("file") MultipartFile file,
                                                                                     @RequestParam("nombreHoja") String nombreHoja,
                                                                                     @RequestParam("numeroFila") Integer numeroFila,
                                                                                     @RequestParam("numeroColumna") Integer numeroColumna,
                                                                                     @RequestParam("codTipoOrdenamiento") String codTipoOrdenamiento,
                                                                                     @RequestParam("subCodTipoOrdenamiento") String subCodTipoOrdenamiento,
                                                                                     @RequestParam("codigoTipoOrdenamientoDet") String codigoTipoOrdenamientoDet,
                                                                                     @RequestParam("idPlanManejo") Integer idPlanManejo,
                                                                                     @RequestParam("idUsuarioRegistro") Integer idUsuarioRegistro) {
        log.info("Ordenamiento Protección - proteccionVigilanciaCargarExcel");
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = serv.proteccionVigilanciaCargarExcel(file, nombreHoja, numeroFila, numeroColumna, codTipoOrdenamiento, subCodTipoOrdenamiento, codigoTipoOrdenamientoDet, idPlanManejo, idUsuarioRegistro);
            if (result.getSuccess()) {
                log.info("PotencialProduccionForestal - registrarPotencialProduccionExcel", "Proceso realizado correctamente.");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);
            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Ordenamiento Protección -  registrarPotencialProduccionExcel", "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/eliminarOrdenamientoInterno")
    @ApiOperation(value = "eliminar OrdenamientoInterno" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity EliminarOrdenamientoInterno(@RequestBody OrdenamientoProteccionDetalleEntity ordenamientoProteccionDetalleEntity){
        log.info("OrdenamientoInterno - eliminarOrdenamientoInterno",ordenamientoProteccionDetalleEntity.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = serv.EliminarOrdenamientoInterno(ordenamientoProteccionDetalleEntity);
            log.info("OrdenamientoInterno - eliminarOrdenamientoInterno","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("OrdenamientoInterno - eliminarOrdenamientoInterno","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
