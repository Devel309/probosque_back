package pe.gob.serfor.mcsniffs.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto.DocumentoAdjuntoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CoreCentralService;
import pe.gob.serfor.mcsniffs.service.EmailService;
import pe.gob.serfor.mcsniffs.service.ProcesoPostulacionService;
import pe.gob.serfor.mcsniffs.service.SolicitudService;

import java.util.List;


@RestController 
@RequestMapping(Urls.solicitud.BASE)
public class SolicitudController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudController.class);

    @Autowired
    private SolicitudService servicio;

    @Autowired
    private EmailService email;

    @Autowired
    private ProcesoPostulacionService pp;
    /**
     * @autor: miguel [14-06-2021]
     * @descripción: {lista Solicitudes}
     * @param: -
     * @return: ResponseEntity
     */
    @PostMapping(path = "/ListarPorFiltroSolicitud")
    @ApiOperation(value = "Listar Solicitud" , authorizations = {@Authorization(value="JWT")})
    public ResponseEntity ListarPorFiltroSolicitud(){
        try{
            return  servicio.ListarPorFiltroSolicitud();
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    /**
     * @autor: JaquelineDB [02-07-2021]
     * @modificado:
     * @descripción: {Registrar solicitud de ampliacion de plazo de entrega de pruebas de publicacion}
     * @param:file
     */
    @PostMapping(path = "/registrarSolicitudAmpliacion")
    @ApiOperation(value = "registrar solicitud de ampliacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> registrarSolicitudAmpliacion(@RequestBody SolicitudDto obj){
        ResultClassEntity response = new ResultClassEntity();
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
            log.info("fiscalizacion - registrarSolicitudAmpliacion",obj.toString());
            try{
                response = servicio.RegistrarSolicitudAmpliacion(obj);
                log.info("fiscalizacion - registrarSolicitudAmpliacion","Proceso realizado correctamente");

                if (!response.getSuccess()) {
                    return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
                } else {
                    return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
                }
            }catch (Exception e){
                log.error("fiscalizacion - registrarSolicitudAmpliacion","Ocurrió un error :"+ e.getMessage());
                result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
                return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
            }

    }
    @PostMapping(path = "/actualizarSolicitudAmpliacion")
    @ApiOperation(value = "actualizarSolicitudAmpliacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> actualizarSolicitudAmpliacion(@RequestBody SolicitudDto obj){
        ResultClassEntity response = new ResultClassEntity();
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        log.info("fiscalizacion - ActualizarSolicitudAmpliacion",obj.toString());
        try{
            response = servicio.ActualizarSolicitudAmpliacion(obj);
            log.info("fiscalizacion - ActualizarSolicitudAmpliacion","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("fiscalizacion - ActualizarSolicitudAmpliacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @autor: JaquelineDB [03-07-2021]
     * @modificado:
     * @descripción: {Aprobar o rechazar las solicitudes de ampliacion de plazos}
     * @param:file
     */
    @PostMapping(path = "/aprobarRechazarSolicitud")
    @ApiOperation(value = "aprobar o rechazar solicitud de ampliacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> AprobarRechazarSolicitud(@RequestBody SolicitudDto obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            //Se le envia un correo al solicitandole que su solicitud fue aprobada o rechazada
            //y que tiene que resolverla
            /*if(result.getSuccess()){ //falta implementar por lo del correo
                //Enviar correo
                EmailEntity mail = new EmailEntity();
                mail.setEmail(usuario.getCorreo());
                mail.setContent(result.getData().getContenidoEmail());
                mail.setSubject(result.getData().getAsuntoEmail());
                Boolean enviarmail = enviarcorreo.sendEmail(mail);
            }*/
            result = servicio.AprobarRechazarSolicitud(obj);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - AprobarRechazarSolicitud", "Ocurrió un error al aprobar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [03-07-2021]
     * @modificado:
     * @descripción: {Listar las solicitudes de ampliacion}
     * @param:file
     */
    @PostMapping(path = "/listarSolicitudesAmpliacion")
    @ApiOperation(value = "listar solicitudes de ampliacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<SolicitudDto>> listarSolicitudesAmpliacion(@RequestBody SolicitudDto param){
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;

        log.info("fiscalizacion - listarSolicitudesAmpliacion",param.toString());
        ResultClassEntity response;
        try{
            response = servicio.listarSolicitudesAmpliacion(param);
            log.info("fiscalizacion - listarSolicitudesAmpliacion","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("fiscalizacion - listarSolicitudesAmpliacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [03-07-2021]
     * @modificado:
     * @descripción: {Obtener una solicitud de ampliacion}
     * @param:file
     */
    @PostMapping(path = "/obtenerSolicitudAmpliacion")
    @ApiOperation(value = "Obtener solicitud de ampliacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<SolicitudDto>> obtenerSolicitudAmpliacion(@RequestBody  SolicitudDto request){
        ResultClassEntity<SolicitudDto> result = new ResultClassEntity<SolicitudDto>();
        try {
            result = servicio.ObtenerSolicitudAmpliacion(request);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - ObtenerSolicitudAmpliacion", "Ocurrió un error al obtener en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Listar tipo solicitud}
     * @param:file
     */
    @GetMapping(path = "/listarTipoSolicitud")
    @ApiOperation(value = "Listar Tipo Solicitud", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<TipoSolicitudEntity>> ListarTipoSolicitud(){
        ResultEntity<TipoSolicitudEntity> result = new ResultEntity<TipoSolicitudEntity>();
        try {
            result = servicio.ListarTipoSolicitud();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - ListarTipoSolicitud", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Listar tipo documento}ac
     * @param:file
     */
    @GetMapping(path = "/listarTipoDocumento")
    @ApiOperation(value = "Listar Tipo Solicitud", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<TipoDocumentoEntity>> ListarTipoDocumento(){
        ResultEntity<TipoDocumentoEntity> result = new ResultEntity<TipoDocumentoEntity>();
        try {
            result = servicio.ListarTipoDocumento();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - ListarTipoDocumento", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {descargar declaracion jurada}
     * @param:file
     */
    @GetMapping(path = "/descagarDeclaracionJurada")
    @ApiOperation(value = "DescagarDeclaracionJurada", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> DescagarDeclaracionJurada(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = servicio.DescagarDeclaracionJurada();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - DescagarDeclaracionJurada", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    @GetMapping(path = "/descagarPlantillaResolucionResumen")
    @ApiOperation(value = "descagarPlantillaResolucionResumen", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> descagarPlantillaResolucionResumen(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = servicio.DescagarPlantillaResolucionResumen();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - DescagarPlantillaResolucionResumen", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }
    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Descargar propuesta exploracion}
     * @param:file
     */
    @GetMapping(path = "/descargarPropuestaExploracion")
    @ApiOperation(value = "DescargarPropuestaExploracion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> DescargarPropuestaExploracion(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = servicio.DescargarPropuestaExploracion();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - DescargarPropuestaExploracion", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [05-07-2021]
     * @modificado:
     * @descripción: {Descargar formato de aprobacion}
     * @param:file
     */
    @GetMapping(path = "/descargarFormatoAprobacion")
    @ApiOperation(value = "DescargarFormatoAprobacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultArchivoEntity> DescargarFormatoAprobacion(){
        ResultArchivoEntity result = new ResultArchivoEntity();
        try {
            result = servicio.DescargarFormatoAprobacion();
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - DescargarFormatoAprobacion", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {Relaciona los archivos adjuntos con la solicitud}
     * @param:SolicitudRequestEntity
     */
    @PostMapping(path = "/registrarDocumentoAdjunto")
    @ApiOperation(value = "registrarDocumentoAdjunto", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> registrarDocumentoAdjunto(@RequestBody SolicitudEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = servicio.RegistrarDocumentoAdjunto(obj);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - RegistrarDocumentoAdjunto", "Ocurrió un error al listar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [07-07-2021]
     * @modificado:
     * @descripción: {Obtener archivos de las solicitudes}
     * @param:IdSolicitud
     */
    @GetMapping(path = "/ObtenerArchivosSolicitudes/{IdSolicitud}")
    @ApiOperation(value = "Obtener Archivos Solicitudes", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity<SolicitudAdjuntosEntity>> ObtenerArchivosSolicitudes(@PathVariable("IdSolicitud") Integer IdSolicitud){
        ResultEntity<SolicitudAdjuntosEntity> result = new ResultEntity<SolicitudAdjuntosEntity>();
        try {
            result = servicio.ObtenerArchivosSolicitudes(IdSolicitud);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - ObtenerArchivosSolicitudes", "Ocurrió un error al obtener en: "+Ex.getMessage());
            //result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

     /**
     * @autor: JaquelineDB [01-07-2021]
     * @modificado:
     * @descripción: {Actualiza el estado del proceso de proceso de postulacion}
     * @param:file
     */
    @PostMapping(path = "/actualizarEstadoProcesoPostulacion")
    @ApiOperation(value = "actualizar Estado Proceso Postulacion", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity> actualizarEstadoProcesoPostulacion(@RequestBody EstatusProcesoPostulacionEntity obj){
        ResultClassEntity result = new ResultClassEntity();
        try {
            //actualmente se usa cuando la solicitud de inpugnacion es fundada.
            //ahora tambien quiero usarlo para cuando se termine de llenar todos los anexos, se mande el publicar
            result = pp.actualizarEstadoProcesoPostulacion(obj);

            
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("Solicitud - AprobarRechazarSolicitud", "Ocurrió un error al aprobar en: "+Ex.getMessage());
            result.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor: JaquelineDB [22-07-2021]
     * @modificado:
     * @descripción: {Se obtiene al nuevo usuario ganador, luego de ser aprobada una inpugnacion al anterior ganador}
     * @param:FiltroSolicitudEntity
     */
    @PostMapping(path = "/obtenerNuevoUsuarioGanador")
    @ApiOperation(value = "Obtener Nuevo Usuario Ganador", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultClassEntity<ImpugnacionNuevoGanadorEntity>> ObtenerNuevoUsuarioGanador(@RequestBody FiltroSolicitudEntity filtro){
        ResultClassEntity response = new ResultClassEntity();
        try {
            response = servicio.ObtenerNuevoUsuarioGanador(filtro);
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception Ex){
            log.error("Solicitud - ObtenerNuevoUsuarioGanador", "Ocurrió un error al listar en: "+Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity(response,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/actualizarAprobadoSolicitud")
    @ApiOperation(value = "actualizarAprobadoSolicitud", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarAprobadoSolicitud(@RequestBody SolicitudDto solicitud){
        ResultClassEntity response = new ResultClassEntity();
        log.info("Solicitud - ActualizarAprobadoSolicitud", "Ingreso datos: "+solicitud.toString());
        try {
            response = servicio.ActualizarAprobadoSolicitud(solicitud);
            if (!response.getSuccess()) {
                log.info("Solicitud - ActualizarAprobadoSolicitud", "Ocurrió un error al listar en: ");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception Ex){
            log.error("Solicitud - ActualizarAprobadoSolicitud", "Ocurrió un error al listar en: "+Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity(response,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/registrarObservacionDocumentoAdjunto")
    @ApiOperation(value = "registrarObservacionDocumentoAdjunto", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarObservacionDocumentoAdjunto(@RequestBody List<DocumentoAdjuntoDto> documento){
        ResultClassEntity response = new ResultClassEntity();
        log.info("Solicitud - RegistrarObservacionDocumentoAdjunto", "Ingreso datos: "+documento.toString());
        try {
            response = servicio.RegistrarObservacionDocumentoAdjunto(documento);
            if (!response.getSuccess()) {
                log.info("Solicitud - RegistrarObservacionDocumentoAdjunto", "Ocurrió un error al listar en: ");
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        } catch (Exception Ex){
            log.error("Solicitud - RegistrarObservacionDocumentoAdjunto", "Ocurrió un error al listar en: "+Ex.getMessage());
            response.setInnerException(Ex.getMessage());
            return new org.springframework.http.ResponseEntity(response,HttpStatus.FORBIDDEN);
        }
    }
}
