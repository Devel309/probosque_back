package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH.InformacionTHDto;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionPermisoForestalDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(Urls.tituloHabilitante.BASE)
public class TituloHabilitanteController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(TituloHabilitanteController.class);


    @Autowired
    private pe.gob.serfor.mcsniffs.service.TituloHabilitanteService tituloHabilitanteService;


    @PostMapping(path = "/registrarTituloHabilitante")
    @ApiOperation(value = "registrarTituloHabilitante", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarTituloHabilitante(
            @RequestBody List<TituloHabilitanteEntity> list) {
        log.info("registrarTituloHabilitante - registrarTituloHabilitante", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = tituloHabilitanteService.registrarTituloHabilitante(list);


            log.info("registrarTituloHabilitante - registrarTituloHabilitante", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("registrarTituloHabilitante -registrarTituloHabilitante", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/actualizarTituloHabilitante")
    @ApiOperation(value = "actualizarTituloHabilitante", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity actualizarTituloHabilitante(
            @RequestBody List<TituloHabilitanteEntity> list) {
        log.info("actualizarTituloHabilitante - actualizarTituloHabilitante", list.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try {
            response = tituloHabilitanteService.actualizarTituloHabilitante(list);


            log.info("actualizarTituloHabilitante - actualizarTituloHabilitante", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error("actualizarTituloHabilitante -actualizarTituloHabilitante", "Ocurrió un error :" + e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping(path = "/listarTituloHabilitante")
    @ApiOperation(value = "listarTituloHabilitante" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarTituloHabilitante(@RequestBody TituloHabilitanteEntity param){
        try{
            return  new ResponseEntity (true, "ok", tituloHabilitanteService.listarTituloHabilitante(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity(false,e.getMessage(),null);
        }
    }

    @PostMapping(path = "/eliminarTituloHabilitante")
    @ApiOperation(value = "eliminarTituloHabilitante" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarTituloHabilitante(@RequestBody TituloHabilitanteEntity tituloHabilitanteEntity){
        log.info("eliminarTituloHabilitante - eliminarTituloHabilitante",tituloHabilitanteEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = tituloHabilitanteService.eliminarTituloHabilitante(tituloHabilitanteEntity);
            log.info("eliminarTituloHabilitante - eliminarTituloHabilitante","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("eliminarTituloHabilitante - eliminarTituloHabilitante","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarInformacionTH")
    @ApiOperation(value = "listarInformacionTH" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarInformacionTH(@RequestBody InformacionTHDto request) {
        log.info("tituloHabilitante - listarInformacionTH", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = tituloHabilitanteService.listarInformacionTH(request);
            log.info("tituloHabilitante- listarInformacionTH", "Proceso realizado correctamente");

            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error("tituloHabilitante- listarInformacionTH", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new org.springframework.http.ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

