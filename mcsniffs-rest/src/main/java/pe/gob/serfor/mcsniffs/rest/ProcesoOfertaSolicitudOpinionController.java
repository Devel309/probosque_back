package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.Dto.ProcesoOfertaSolicitudOpinion.ProcesoOfertaSolicitudOpinionDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ProcesoOfertaSolicitudOpinionService;

@RestController
@RequestMapping(Urls.ProcesoOfertaSolicitudOpinion.BASE)
public class ProcesoOfertaSolicitudOpinionController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProcesoOfertaSolicitudOpinionController.class);

    @Autowired
    private ProcesoOfertaSolicitudOpinionService ProcesoOfertaSolicitudOpinionService;


    @PostMapping(path = "/listarProcesoOfertaSolicitudOpinion")
    @ApiOperation(value = "listarProcesoOfertaSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarProcesoOfertaSolicitudOpinion(@RequestBody ProcesoOfertaSolicitudOpinionDto param){
        log.info("ProcesoOfertaSolicitudOpinion - ListarProcesoOfertaSolicitudOpinion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = ProcesoOfertaSolicitudOpinionService.ListarProcesoOfertaSolicitudOpinion(param);
            log.info("ProcesoOfertaSolicitudOpinion - ListarProcesoOfertaSolicitudOpinion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProcesoOfertaSolicitudOpinion - ListarProcesoOfertaSolicitudOpinion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarProcesoOfertaSolicitudOpinion")
    @ApiOperation(value = "registrarProcesoOfertaSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarProcesoOfertaSolicitudOpinion(@RequestBody ProcesoOfertaSolicitudOpinionDto param){
        log.info("ProcesoOfertaSolicitudOpinion - RegistrarProcesoOfertaSolicitudOpinion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = ProcesoOfertaSolicitudOpinionService.RegistrarProcesoOfertaSolicitudOpinion(param);
            log.info("ProcesoOfertaSolicitudOpinion - RegistrarProcesoOfertaSolicitudOpinion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProcesoOfertaSolicitudOpinion -RegistrarProcesoOfertaSolicitudOpinion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/actualizarProcesoOfertaSolicitudOpinion")
    @ApiOperation(value = "actualizarProcesoOfertaSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity actualizarProcesoOfertaSolicitudOpinion(@RequestBody ProcesoOfertaSolicitudOpinionDto param){
        log.info("ProcesoOfertaSolicitudOpinion - ActualizarProcesoOfertaSolicitudOpinion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = ProcesoOfertaSolicitudOpinionService.ActualizarProcesoOfertaSolicitudOpinion(param);
            log.info("ProcesoOfertaSolicitudOpinion - ActualizarProcesoOfertaSolicitudOpinion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProcesoOfertaSolicitudOpinion -ActualizarProcesoOfertaSolicitudOpinion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarProcesoOfertaSolicitudOpinion")
    @ApiOperation(value = "eliminarProcesoOfertaSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarProcesoOfertaSolicitudOpinion(@RequestBody ProcesoOfertaSolicitudOpinionDto param){
        log.info("ProcesoOfertaSolicitudOpinion - EliminarProcesoOfertaSolicitudOpinion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = ProcesoOfertaSolicitudOpinionService.EliminarProcesoOfertaSolicitudOpinion(param);
            log.info("ProcesoOfertaSolicitudOpinion - EliminarProcesoOfertaSolicitudOpinion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProcesoOfertaSolicitudOpinion -EliminarProcesoOfertaSolicitudOpinion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/obtenerProcesoOfertaSolicitudOpinion")
    @ApiOperation(value = "obtenerProcesoOfertaSolicitudOpinion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity ObtenerProcesoOfertaSolicitudOpinion(@RequestBody ProcesoOfertaSolicitudOpinionDto param){
        log.info("ProcesoOfertaSolicitudOpinion - ObtenerProcesoOfertaSolicitudOpinion",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = ProcesoOfertaSolicitudOpinionService.ObtenerProcesoOfertaSolicitudOpinion(param);
            log.info("ProcesoOfertaSolicitudOpinion - ObtenerProcesoOfertaSolicitudOpinion","Proceso realizado correctamente");
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("ProcesoOfertaSolicitudOpinion - ObtenerProcesoOfertaSolicitudOpinion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
