package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.ProcesoOfertaDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ProcesoOfertaDetalleService;

@RestController
@RequestMapping(Urls.procesoOfertaDetalle.BASE)
public class ProcesoOfertaDetalleController {
    /**
     * @autor: Abner Valdez [26-06-2021]
     * @modificado:
     * @descripción: {Servicio de Proceso oferta detalle
     * todo lo referente a esta tabla}
     *
     */
    @Autowired
    private ProcesoOfertaDetalleService service;
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ProcesoOfertaDetalleController.class);
    /**
     * @autor: Abner Valdez [26-06-2021]
     * @modificado:
     * @descripción: {Guarda e proceso en oferta detalle}
     * @param:ProcesoOfertaDetalleEntity
     */
    @PostMapping(path = "/guardarProcesoOfertaDetalle")
    @ApiOperation(value = "Cargar Archivo", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> guardarProcesoOferta(@RequestBody ProcesoOfertaDetalleEntity obj){
        ResultClassEntity lst_ua = new ResultClassEntity();
        try {
            lst_ua = service.guardarProcesoOfertaDetalle(obj);
            return new ResponseEntity<>(lst_ua, HttpStatus.OK );
        } catch (Exception Ex){
            log.error("ProcesoOfertaController - guardarProcesoOferta", "Ocurrió un error al guardar en: "+Ex.getMessage());
            lst_ua.setInnerException(Ex.getMessage());
            return new ResponseEntity<>(lst_ua,HttpStatus.FORBIDDEN);
        }
    }
}
