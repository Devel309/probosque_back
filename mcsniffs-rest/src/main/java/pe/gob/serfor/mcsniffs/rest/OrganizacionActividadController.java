package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.OrganizacionActividadEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.OrganizacionActividadService;

import java.util.List;

@RestController
@RequestMapping(Urls.organizacionActividad.BASE)
public class OrganizacionActividadController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(OrganizacionActividadController.class);



    @Autowired
    private OrganizacionActividadService organizacionActividadService;
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registrar la organización de actividad}
     * @param: List<OrganizacionActividadEntity>
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarOrganizacionActividad")
    @ApiOperation(value = "registrarOrganizacionActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOrganizacionActividad(@RequestBody List<OrganizacionActividadEntity> organizacionActividad){
        log.info("ProteccionBosque - RegistrarOrganizacionActividad",organizacionActividad.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = organizacionActividadService.RegistrarOrganizacionActividad(organizacionActividad);
            log.info("ProteccionBosque - RegistrarOrganizacionActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarProteccionBosqueImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar por filtro la organización de actividad}
     * @param: OrganizacionActividadEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarPorFiltroOrganizacionActividad")
    @ApiOperation(value = "listarPorFiltroOrganizacionActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOrganizacionActividad(@RequestBody OrganizacionActividadEntity organizacionActividad){
        log.info("ProteccionBosque - ListarPorFiltroOrganizacionActividad",organizacionActividad.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = organizacionActividadService.ListarPorFiltroOrganizacionActividad(organizacionActividad);
            log.info("ProteccionBosque - ListarPorFiltroOrganizacionActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarPorFiltroOrganizacionActividad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar la organización de actividad}
     * @param: OrganizacionActividadEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarOrganizacionActividad")
    @ApiOperation(value = "eliminarOrganizacionActividad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarOrganizacionActividad(@RequestBody OrganizacionActividadEntity organizacionActividad){
        log.info("ProteccionBosque - EliminarOrganizacionActividad",organizacionActividad.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = organizacionActividadService.EliminarOrganizacionActividad(organizacionActividad);
            log.info("ProteccionBosque - EliminarOrganizacionActividad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -EliminarOrganizacionActividad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registarar la organización de actividad archivo}
     * @param: MultipartFile ,PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarOrganizacionActividadArchivo")
    @ApiOperation(value = "registrarOrganizacionActividadArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOrganizacionActividadArchivo(@RequestParam("file") MultipartFile file ,
                                                                                 @RequestParam(required = false) Integer idPlanManejo,
                                                                                 @RequestParam(required = false) Integer idUsuarioRegistro){

        PlanManejoEntity plan = new PlanManejoEntity();
        plan.setIdPlanManejo(idPlanManejo);
        plan.setIdUsuarioRegistro(idUsuarioRegistro);
        log.info("ProteccionBosque - RegistrarOrganizacionActividadArchivo",plan.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = organizacionActividadService.RegistrarOrganizacionActividadArchivo(file,plan);
            log.info("ProteccionBosque - RegistrarOrganizacionActividadArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarOrganizacionActividadArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar la organización de actividad archivo}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarOrganizacionActividadArchivo")
    @ApiOperation(value = "listarOrganizacionActividadArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarOrganizacionActividadArchivo(@RequestBody PlanManejoEntity planManejo){
        log.info("ProteccionBosque - ListarOrganizacionActividadArchivo",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = organizacionActividadService.ListarOrganizacionActividadArchivo(planManejo);
            log.info("ProteccionBosque - ListarOrganizacionActividadArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarOrganizacionActividadArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
      /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar la organización de actividad archivo}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/ListarPorFiltroOrganizacionActividadArchivo")
    @ApiOperation(value = "ListarPorFiltroOrganizacionActividadArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity<ResultEntity> ListarPorFiltroOrganizacionActividadArchivo(@RequestBody PlanManejoEntity planManejo){
        log.info("ProteccionBosque - ListarPorFiltroOrganizacionActividadArchivo",planManejo.toString());
        ResultEntity res=new ResultEntity();
        ResponseEntity result = null;
        try{

            res = organizacionActividadService.ListarPorFiltroOrganizacionActividadArchivo(planManejo);
            log.info("ProteccionBosque - ListarPorFiltroOrganizacionActividadArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(res, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ListarPorFiltroOrganizacionActividadArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
 