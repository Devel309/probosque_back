package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.CapacitacionEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.CapacitacionDto;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CapacitacionPgmfeaService;

import java.util.List;

@RestController
@RequestMapping(Urls.capacitacionPgmfea.BASE)
public class CapacitacionPgmfeaController  extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CapacitacionPgmfeaController.class);



    @Autowired
    private CapacitacionPgmfeaService capacitacionPgmfeaService;

    @PostMapping(path = "/registrarCapacitacionPgmfea")
    @ApiOperation(value = "registrarCapacitacionPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarCapacitacionPgmfea(@RequestBody List<CapacitacionDto> list){
        log.info("ProteccionBosque - RegistrarCapacitacionPgmfea",list.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = capacitacionPgmfeaService.RegistrarCapacitacionPgmfea(list);
            log.info("ProteccionBosque - RegistrarCapacitacionPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -RegistrarCapacitacionPgmfea","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarCapacitacionPgmfea")
    @ApiOperation(value = "listarCapacitacionPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarCapacitacionPgmfea(@RequestBody PlanManejoEntity planManejo){
        log.info("ProteccionBosque - ListarCapacitacionPgmfea",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = capacitacionPgmfeaService.ListarCapacitacionPgmfea(planManejo);
            log.info("ProteccionBosque - ListarCapacitacionPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -ConfiguracionProteccionBosqueDemarcacion","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarCapacitacionPgmfea")
    @ApiOperation(value = "eliminarCapacitacionPgmfea" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarCapacitacionPgmfea(@RequestBody CapacitacionEntity capacitacion){
        log.info("ProteccionBosque - EliminarCapacitacionPgmfea",capacitacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = capacitacionPgmfeaService.EliminarCapacitacionPgmfea(capacitacion);
            log.info("ProteccionBosque - EliminarCapacitacionPgmfea","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("ProteccionBosque -EliminarCapacitacionPgmfea","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
