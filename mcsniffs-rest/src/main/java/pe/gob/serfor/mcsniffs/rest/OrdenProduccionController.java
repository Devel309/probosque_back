package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;
import java.util.List;
 
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ProdTerminadoEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.service.OrdenProduccionService;
@RestController
@RequestMapping(Urls.ordenProduccion.BASE)
public class OrdenProduccionController extends AbstractRestController {

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(OrdenProduccionController.class);
    @Autowired
    private OrdenProduccionService ordenProduccionService;

    @PostMapping(path = "/registraProductoTerminado")
    @ApiOperation(value = "registraProductoTerminado", authorizations = { @Authorization(value = "JWT") })
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registraProductoTerminado(@RequestBody List<ProdTerminadoEntity> request) {

        log.info("OrdenProduccionController - registraProductoTerminado",request.toString());
        ResultClassEntity result = new ResultClassEntity<>();
        try {
            System.out.println("asdasd");
            result = ordenProduccionService.registrarOrdenProduccionProductoTerminado(request);

            if (result.getSuccess()) {
                log.info("OrdenProduccionController - registraProductoTerminado",
                        "Proceso realizado correctamente");
                return new org.springframework.http.ResponseEntity(result, HttpStatus.OK);

            } else {
                return new org.springframework.http.ResponseEntity(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("OrdenProduccionController - registraProductoTerminado",
                    "Ocurrió un error :" + e.getMessage());
            result.setSuccess(Constantes.STATUS_ERROR);
            result.setMessage(Constantes.MESSAGE_ERROR_500);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarOrdenProduccion")
    @ApiOperation(value = "registrarOrdenProduccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarOrdenProduccion(@RequestBody List<OrdenProduccionEntity> listOrdenProd){
        log.info("OrdenProduccion - registrarOrdenProduccion",listOrdenProd.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = ordenProduccionService.registrarOrdenProduccion(listOrdenProd);
            log.info("OrdenProduccion - registrarOrdenProduccion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

        }catch (Exception e){
            log.error("Orden Produccion -registrarOrdenProduccion","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Lista ManejoBosqueActividad}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */

    @PostMapping(path = "/listarOrdenProduccion")
    @ApiOperation(value = "listarOrdenProduccion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarOrdenProduccion(@RequestBody OrdenProduccionEntity ordenProduccion){
        log.info("OrdenProduccion - listarOrdenProduccion",ordenProduccion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = ordenProduccionService.listarOrdenProduccion(ordenProduccion);
            log.info("OrdenProduccion - listarOrdenProduccion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);

          }catch (Exception e){
            log.error("Orden Produccion -listarOrdenProduccion","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

  
    @PostMapping(path = "/listarOrdenProduccionProductoTerminado")
    @ApiOperation(value = "listarOrdenProduccionProductoTerminado" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOrganizacionActividad(@RequestBody ProdTerminadoEntity filtro){
        log.info("ProteccionBosque - listarOrdenProduccionProductoTerminado",filtro.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = ordenProduccionService.listarOrdenProduccionProductoTerminado(filtro);
            log.info("Orden Produccion - listarOrdenProduccionProductoTerminado","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("Orden Produccion -listarOrdenProduccionProductoTerminado","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
