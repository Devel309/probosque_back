package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.centroTansformacion.CentroTransformacionDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.CentroTransformacionService;


@RestController
@RequestMapping(Urls.centroTransformacion.BASE)
public class CentroTransformacionController extends AbstractRestController {
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(CentroTransformacionController.class);

    @Autowired
    private CentroTransformacionService centroTransformacionService;
    
    @PostMapping(path = "/listarCentroTransformacion")
    @ApiOperation(value = "Listar centro Transformacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity centroTransformacion(@RequestBody CentroTransformacionDto request) {
        log.info("censoForestal", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = centroTransformacionService.ListarCentroTransformacion(request);
            log.info("centroTransformacion - obtenercentroTransformacion", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("centroTransformacion - listar centro Transformacion", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
