package pe.gob.serfor.mcsniffs.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.RequisitoEvaluacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.ResultEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.RequisitoEvaluacionService;

@RestController
@RequestMapping(Urls.requisitoEvaluacion.BASE)
public class RequisitoEvaluacionController {
    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {Repository de los requisitos de las evaluaciones}
     *
     */

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(RequisitoEvaluacionController.class);

    @Autowired
    private RequisitoEvaluacionService service;

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {registra los requisitos de las evaluaciones}
     * @param:RequisitoEvaluacionEntity
     */
    @PostMapping(path = "/registrarRequisito")
    @ApiOperation(value = "registrar Requisito", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<List<RequisitoEvaluacionEntity>>> registrarRequisito(@RequestBody List<RequisitoEvaluacionEntity> obj) {
        ResultClassEntity<List<RequisitoEvaluacionEntity>> result = new ResultClassEntity<List<RequisitoEvaluacionEntity>>();
        try {
            result = service.registrarRequisito(obj);
            log.info("RequisitoEvaluacionController - registrarRequisito: Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("RequisitoEvaluacionController - registrarRequisito", Ex.getMessage());
            result.setError(Constantes.MESSAGE_ERROR_500, Ex);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {lista los requisitos de las evaluaciones}
     * @param:RequisitoEvaluacionEntity
     */
    @PostMapping(path = "/listarRequisito")
    @ApiOperation(value = "listar Requisito", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultEntity<RequisitoEvaluacionEntity>> listarRequisito(@RequestBody RequisitoEvaluacionEntity obj) {
        ResultEntity<RequisitoEvaluacionEntity> result = new ResultEntity<RequisitoEvaluacionEntity>();
        try {
            result = service.listarRequisito(obj);
            log.info("RequisitoEvaluacionController - listarRequisito: Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("RequisitoEvaluacionController - listarRequisito", Ex.getMessage());
            result.setInnerException(Constantes.MESSAGE_ERROR_500 +" - "+ Ex.getMessage());
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor: JaquelineDB [27-10-2021]
     * @modificado:
     * @descripción: {elimina los requisitos detalle de las evaluaciones}
     * @param:RequisitoEvaluacionDetalleEntity
     */
    @PostMapping(path = "/eliminarRequisitoDetalle")
    @ApiOperation(value = "eliminar Requisito Detalle", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"),
            @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity<RequisitoEvaluacionDetalleEntity>> eliminarRequisitoDetalle(@RequestBody RequisitoEvaluacionDetalleEntity obj) {
        ResultClassEntity<RequisitoEvaluacionDetalleEntity> result = new ResultClassEntity<RequisitoEvaluacionDetalleEntity>();
        try {
            result = service.eliminarRequisitoDetalle(obj);
            log.info("RequisitoEvaluacionController - eliminarRequisitoDetalle: Proceso realizado correctamente");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex) {
            log.error("RequisitoEvaluacionController - eliminarRequisitoDetalle", Ex.getMessage());
            result.setError(Constantes.MESSAGE_ERROR_500, Ex);
            return new org.springframework.http.ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
