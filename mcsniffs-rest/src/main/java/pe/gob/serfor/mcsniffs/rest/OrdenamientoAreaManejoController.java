package pe.gob.serfor.mcsniffs.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.OrdenamientoAreaManejoService;

import java.util.List;

@RestController
@RequestMapping(Urls.ordenamientoAreaManejo.BASE)
public class OrdenamientoAreaManejoController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(OrdenamientoAreaManejoController.class);

    @Autowired
    private OrdenamientoAreaManejoService ordenamientoAreaManejoService;
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Combo categoria de ordenamiento}
     * @param: ParametroEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/comboCategoriaOrdenamiento")
    @ApiOperation(value = "comboCategoriaOrdenamiento" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity comboCategoriaOrdenamiento(@RequestBody ParametroEntity parametro){
        log.info("OrdenamientoAreaManejo - ComboCategoriaOrdenamiento",parametro.toString());
        ResponseEntity result = null;
        List<Dropdown> response ;
        try{

            response = ordenamientoAreaManejoService.ComboCategoriaOrdenamiento(parametro);
            log.info("OrdenamientoAreaManejo - ComboCategoriaOrdenamiento","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("OrdenamientoAreaManejo -ComboCategoriaOrdenamiento","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registrar OrdenamientoAreaManejo}
     * @param: OrdenamientoAreaManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarOrdenamientoAreaManejo")
    @ApiOperation(value = "registrarOrdenamientoAreaManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarOrdenamientoAreaManejo(@RequestParam("files") MultipartFile files ,
                                                                                   @RequestParam(required = false) Integer idPlanManejo,
                                                                                   @RequestParam(required = false) Integer idOrdAreaManejo,
                                                                                   @RequestParam(required = false) String catOrdenamiento,
                                                                                   @RequestParam(required = false) Double area,
                                                                                   @RequestParam(required = false) Double porcentaje,
                                                                                   @RequestParam(required = false) Boolean accion,
                                                                                   @RequestParam(required = false) Boolean file,
                                                                                   @RequestParam(required = false) Integer idUsuarioRegistro,
                                                                                   @RequestParam(required = false) String estado) throws Exception{

       //log.info("ProteccionBosque - RegistrarOrdenamientoAreaManejo",param.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            OrdenamientoAreaManejoEntity obj = new OrdenamientoAreaManejoEntity();
            PlanManejoEntity plan = new PlanManejoEntity();
            plan.setIdPlanManejo(idPlanManejo);
            obj.setPlanManejo(plan);
            obj.setIdOrdAreaManejo(idOrdAreaManejo);
            obj.setCatOrdenamiento(catOrdenamiento);
            obj.setArea(area);
            obj.setPorcentaje(porcentaje);
            obj.setAccion(accion);
            obj.setFile(file);
            obj.setIdUsuarioRegistro(idUsuarioRegistro);
            obj.setEstado(estado);
            //  OrdenamientoAreaManejoEntity obj = new ObjectMapper().readValue(param, OrdenamientoAreaManejoEntity.class);
          //  ObjectMapper mapper = new ObjectMapper();
          //  List<OrdenamientoAreaManejoEntity> participantJsonList = mapper.readValue(params, new TypeReference<List<OrdenamientoAreaManejoEntity>>(){});
             response = ordenamientoAreaManejoService.RegistrarOrdenamientoAreaManejo(obj,files);
            log.info("ProteccionBosque - RegistrarOrdenamientoAreaManejo","Proceso realizado correctamente");

            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("OrdenamientoAreaManejo -RegistrarOrdenamientoAreaManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Listar OrdenamientoAreaManejo}
     * @param: PlanManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/listarOrdenamientoAreaManejo")
    @ApiOperation(value = "listarOrdenamientoAreaManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarOrdenamientoAreaManejo(@RequestBody PlanManejoEntity planManejo){
        log.info("OrdenamientoAreaManejo - ListarOrdenamientoAreaManejo",planManejo.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = ordenamientoAreaManejoService.ListarOrdenamientoAreaManejo(planManejo);
            log.info("OrdenamientoAreaManejo - ListarOrdenamientoAreaManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("OrdenamientoAreaManejo -ListarOrdenamientoAreaManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Eliminar OrdenamientoAreaManejo}
     * @param: OrdenamientoAreaManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/eliminarOrdenamientoAreaManejo")
    @ApiOperation(value = "eliminarOrdenamientoAreaManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarOrdenamientoAreaManejo(@RequestBody OrdenamientoAreaManejoEntity parametro){
        log.info("OrdenamientoAreaManejo - EliminarOrdenamientoAreaManejo",parametro.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = ordenamientoAreaManejoService.EliminarOrdenamientoAreaManejo(parametro);
            log.info("OrdenamientoAreaManejo - EliminarOrdenamientoAreaManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("OrdenamientoAreaManejo -EliminarOrdenamientoAreaManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Obtener OrdenamientoAreaManejoArchivo}
     * @param: OrdenamientoAreaManejoEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/obtenerOrdenamientoAreaManejoArchivo")
    @ApiOperation(value = "obtenerOrdenamientoAreaManejoArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity obtenerOrdenamientoAreaManejoArchivo(@RequestBody OrdenamientoAreaManejoEntity parametro){
        log.info("OrdenamientoAreaManejo - ObtenerOrdenamientoAreaManejoArchivo",parametro.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = ordenamientoAreaManejoService.ObtenerOrdenamientoAreaManejoArchivo(parametro);
            log.info("OrdenamientoAreaManejo - ObtenerOrdenamientoAreaManejoArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("OrdenamientoAreaManejo -ObtenerOrdenamientoAreaManejoArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
