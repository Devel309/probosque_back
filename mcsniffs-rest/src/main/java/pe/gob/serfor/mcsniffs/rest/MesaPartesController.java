package pe.gob.serfor.mcsniffs.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDetalleDto;
import pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes.MesaPartesDto;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.MesaPartesDetalleService;
import pe.gob.serfor.mcsniffs.service.MesaPartesService;

/**
 * @autor: Renzo Gabriel Meneses Gamarra [24-09-2021]
 * @modificado:
 * @descripción: { Controlador para el Plan Manejo Evaluación }
 */
@RestController
@RequestMapping(Urls.mesaPartes.BASE)
public class MesaPartesController extends AbstractRestController {
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(MesaPartesController.class);

    @Autowired
    private MesaPartesService mesaPartesService;

    @Autowired
    private MesaPartesDetalleService mesaPartesDetalleService;

    /**
     * @autor: Jason Retamozo [15-03-2022]
     * @creado:
     * @descripción: { listar mesa de partes PMFI-DEMA }
     */
    @PostMapping(path = "/listarMesaPartesPMFIDEMA")
    @ApiOperation(value = "listarMesaPartesPMFIDEMA" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarMesaPartesPMFIDEMA(@RequestBody MesaPartesDto param){
        log.info("MesaPartes - listarMesaPartes PMFI-DEMA",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesService.listarMesaPartesPMFIDEMA(param);
            log.info("MesaPartes - listarMesaPartes PMFI-DEMA","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - listarMesaPartes PMFI-DEMA","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarMesaPartes")
    @ApiOperation(value = "listarMesaPartes" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarMesaPartes(@RequestBody MesaPartesDto param){
        log.info("MesaPartes - listarMesaPartes",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesService.listarMesaPartes(param);
            log.info("MesaPartes - listarMesaPartes","Proceso realizado correctamente");

            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - listarMesaPartes","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @autor: Jason Retamozo [15-03-2022]
     * @creado:
     * @descripción: { listar mesa de partes PMFI-DEMA }
     */
    @PostMapping(path = "/registrarMesaPartesPMFIDEMA")
    @ApiOperation(value = "registrarMesaPartesPMFIDEMA" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarMesaPartesPMFIDEMA(@RequestBody MesaPartesDto param){
        log.info("MesaPartes - registrarMesaPartes",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesService.registrarMesaPartesPMFIDEMA(param);
            log.info("MesaPartes - registrarMesaPartes PMFI-DEMA","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - registrarMesaPartes PMFI-DEMA","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarMesaPartes")
    @ApiOperation(value = "registrarMesaPartes" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarMesaPartes(@RequestBody MesaPartesDto param){
        log.info("MesaPartes - registrarMesaPartes",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesService.registrarMesaPartes(param);
            log.info("MesaPartes - registrarMesaPartes","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - registrarMesaPartes","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/eliminarMesaPartes")
    @ApiOperation(value = "eliminarMesaPartes" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarMesaPartes(@RequestBody MesaPartesDto param){
        log.info("MesaPartes - eliminarMesaPartes",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesService.eliminarMesaPartes(param);
            log.info("MesaPartes - eliminarMesaPartes","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - eliminarMesaPartes","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarMesaPartesDetalle")
    @ApiOperation(value = "listarMesaPartesDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarMesaPartesDetalle(@RequestBody MesaPartesDetalleDto dto){
        log.info("MesaPartes - listarMesaPartesDetalle",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesDetalleService.listarMesaPartesDetalle(dto);
            log.info("MesaPartes - listarMesaPartesDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - listarMesaPartesDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   

    @PostMapping(path = "/eliminarMesaPartesDetalle")
    @ApiOperation(value = "eliminarMesaPartesDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarMesaPartesDetalle(@RequestBody List<MesaPartesDetalleDto> dto){
        log.info("MesaPartes - eliminarMesaPartesDetalle",dto.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = mesaPartesDetalleService.eliminarMesaPartesDetalle(dto);
            log.info("MesaPartes - eliminarMesaPartesDetalle","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.BAD_REQUEST);
            } else {
                return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("MesaPartes - eliminarMesaPartesDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
